﻿using System;
using System.ComponentModel;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.SessionState;
using InstanceManager.BusinessLayer;
using SIDLibraries.Utilities;
using SIDWebLibraries.HelperClasses;

namespace SIDWebV2.Services {
  /// <summary>
  /// Summary description for AppService
  /// </summary>
  [ScriptService]
  [WebService(Namespace = "http://tempuri.org/")]
  [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
  [ToolboxItem(false)]
  public class CoreService : WebService, IRequiresSessionState {

    protected readonly ModuleInfo InstanceModuleInfo;

    public CoreService() {
      InstanceModuleInfo = SessionVariablesCore.InstanceManager.InstanceModuleInfo;
      var sessionValidator = new UniqueSessionValidator(InstanceModuleInfo);
      if (SessionVariablesCore.CustomerInfo == null || sessionValidator.CheckUniqueSessionId(new UniqueSessionValidator.SessionData(SessionVariablesCore.UniqueKey, SessionVariablesCore.CustomerInfo.CustomerID)) != 0) return;
      SystemLog.WriteAccessLog("Session validator/CoreService(): Double Session", "Another users took over the session, loggin out");
      SessionVariablesCore.LogOut();  
    }

    public static void Log(Exception ex) {
      SystemLog.WriteToSystemLog(ex);
    }

  }
}