﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using SIDWebV2.HelperClasses;
using SIDWebLibraries.HelperClasses;
using SystemLog = SIDWebLibraries.HelperClasses.SystemLog;

namespace SIDWebV2.Services {
  /// <summary>
  /// Summary description for LoginManager
  /// </summary>
  [WebService(Namespace = "http://tempuri.org/")]
  [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
  // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
  [ScriptService]
  public class LoginService : CoreService {

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer TemporalToken() {
      var currentToken = _randomString(16);
      Session["currentToken"] = currentToken;
      var result = new ResultContainer { Code = ResultContainer.ResultCode.Success, Message = currentToken };
      return result;
    }

    private string _randomString(int len) {
      var random = new Random();
      var buffer = new byte[len / 2];
      random.NextBytes(buffer);
      var result = String.Concat(buffer.Select(x => x.ToString("X2")).ToArray());
      if (len % 2 == 0)
        return result;
      return result + random.Next(16).ToString("X");
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void LogOut() {
      SystemLog.WriteAccessLog("Logout Successful", null);
      SessionVariablesCore.LogOut();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer ValidateCustomer(string customerId, string password) {
      var result = new ResultContainer { Code = ResultContainer.ResultCode.Success, Message = SessionVariablesCore.Translate("VALID_LOGIN") };
      spCstWebGetCustomerInfo_Result customerInfo;
      spAgWebGetAgent_Result agentInfo;
      var status = ValidateCustomer(customerId, password, out customerInfo, out agentInfo);
      if (status == LoginVerification.CustomerStatus.Valid) return result;
      result.Code = ResultContainer.ResultCode.Fail;
      result.Message = SessionVariablesCore.Translate("INVALID_LOGIN");
      return result;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer CustomerSignIn(string customerId, string password, string encryptedKey, string agentId = null) {
      if (Convert.ToString(Session["currentToken"]) != encryptedKey)
        return new ResultContainer {
          Code = ResultContainer.ResultCode.Fail,
          Data = Session["currentToken"],
          Message = "Invalid Token"
        };
      SessionVariablesCore.LastSessionAction = DateTime.Now;
      var result = new ResultContainer();
      password = DecryptStringAES(password, encryptedKey, encryptedKey);
      Session["currentToken"] = string.Empty;

      SessionVariablesCore.WebTarget = WebConfigManager.GetInetTarget();
      spCstWebGetCustomerInfo_Result customerInfo;
      spAgWebGetAgent_Result agentInfo;
      var status = ValidateCustomer(customerId, password, out customerInfo, out agentInfo, agentId);

      // var rootWebConfig = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration(null);
      SetCustomerLanguage();
      switch (status) {
        case LoginVerification.CustomerStatus.Valid:
          if (customerInfo != null) {
            var session = new SessionVariablesLoader();
            session.EstablishSessionVariables(customerInfo);
            SessionVariablesCore.WagerType = WagerType.GetFromWebCode(WagerType.WEBSTRAIGHT);
            result.Code = ResultContainer.ResultCode.Success;

            var sessionValidator = new UniqueSessionValidator(SessionVariablesCore.InstanceManager.InstanceModuleInfo);
            SessionVariablesCore.UniqueKey = sessionValidator.CreateLoginUniqueSessionId(new UniqueSessionValidator.SessionData(SessionVariablesCore.UniqueKey, SessionVariablesCore.CustomerInfo.CustomerID));
            result.Data = SessionVariablesCore.UniqueKey;
            SystemLog.WriteAccessLog("Login Successful", SessionVariablesCore.DeviceInfo.DeviceAlias);

          }
          else if (agentInfo != null) {
            result.Data = "Agent login"; //A o M
            result.Code = ResultContainer.ResultCode.Success;
          }
          else {
            result.Code = ResultContainer.ResultCode.Fail;
            result.Message = SessionVariablesCore.Translate("INVALID LOGIN");
          }
          break;

        case LoginVerification.CustomerStatus.Inactive:
          SystemLog.WriteAccessLog("Attempting to Place Bet", "Inactive Account.");
          result.Code = ResultContainer.ResultCode.Fail;
          result.Message = SessionVariablesCore.Translate("INACTIVE_ACCOUNT");
          return new ResultContainer(ResultContainer.ResultCode.Warning, SessionVariablesCore.Translate("INACTIVE_ACCOUNT"));
        case LoginVerification.CustomerStatus.LossCapExceeded:
          SystemLog.WriteAccessLog("Attempting to Place Bet", "Exceeded loss cap.");
          result.Code = ResultContainer.ResultCode.Fail;
          result.Message = SessionVariablesCore.Translate("EXCEEDED_LOSS_CAP");
          return new ResultContainer(ResultContainer.ResultCode.Warning, SessionVariablesCore.Translate("EXCEEDED_LOSS_CAP"));
        case LoginVerification.CustomerStatus.Suspended:
          SystemLog.WriteAccessLog("Attempting to Place Bet", "Suspended Account");
          result.Code = ResultContainer.ResultCode.Fail;
          result.Message = SessionVariablesCore.Translate("SUSPENDED_ACCOUNT");
          using (var cust = new Customers(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
            var customerMessages = cust.GetWebCustomerMessages(customerId).OrderBy(u => u.MsgSentDateTime).Where(u => u.MsgPriority == "S");
            return new ResultContainer((customerMessages.LastOrDefault() == null ? (object)"" : customerMessages.LastOrDefault()), ResultContainer.ResultCode.Warning, "Account has been suspended, please contact your representative.");
          }
        default:
          SystemLog.WriteAccessLog("Attempting to Place Bet", "Invalid Password.");
          result.Code = ResultContainer.ResultCode.Fail;
          result.Message = SessionVariablesCore.Translate("INVALID PASSWORD");
          break;
      }
      return result;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer CheckCustomerPassword(string customerId, string password, string obsoleteKey) {
      var rc = new ResultContainer();
      try {
        if (Convert.ToString(Session["currentToken"]) == obsoleteKey) {
          password = DecryptStringAES(password, obsoleteKey, obsoleteKey);
          Session["currentToken"] = string.Empty;
          spCstWebGetCustomerInfo_Result customerInfo;
          spAgWebGetAgent_Result agentInfo;
          var status = ValidateCustomer(customerId, password, out customerInfo, out agentInfo);

          // var rootWebConfig = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration(null);
          SetCustomerLanguage();
          switch (status) {
            case LoginVerification.CustomerStatus.Valid:
              rc.Code = ResultContainer.ResultCode.Success;
              break;
            default:
              rc.Code = ResultContainer.ResultCode.Fail;
              rc.Message = SessionVariablesCore.Translate("INVALID_LOGIN");
              break;
          }
        }
        else {
          rc.Code = ResultContainer.ResultCode.Fail;
          rc.Message = SessionVariablesCore.Translate("INVALID_TOKEN");
        }
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer ObsoleteBrowserLog() {
      var result = new ResultContainer();
      SystemLog.WriteAccessLog("Obsolete browser", string.Empty);
      return result;
    }

    private static void SetCustomerLanguage() {
      SessionVariablesCore.LoadLanguageFile();
    }

    public LoginVerification.CustomerStatus ValidateCustomer(string customerId, string password, out spCstWebGetCustomerInfo_Result customerInfo, out spAgWebGetAgent_Result agentInfo, string agentId = null) {
      LoginVerification.CustomerStatus status;
      using (var lv = new LoginVerification(InstanceModuleInfo)) {

        lv.CustomerId = customerId.Trim();
        lv.Password = password.Trim();
        lv.WebTarget = SessionVariablesCore.WebTarget;
        if (agentId != null) lv.AgentId = agentId.Trim();
        if (HttpContext.Current.Request.UserHostAddress != null)
          lv.IpAddress = HttpContext.Current.Request.UserHostAddress;
        var clientStatus = lv.CheckCustomerStatus(out customerInfo);
        var agentStatus = LoginVerification.CustomerStatus.Failed;
        agentInfo = null;

        if (clientStatus == LoginVerification.CustomerStatus.Failed) {
          agentStatus = lv.CheckAgentStatus(out agentInfo);
        }

        if (clientStatus == LoginVerification.CustomerStatus.Failed && agentStatus == LoginVerification.CustomerStatus.Failed) {
          SystemLog.WriteAccessLog("Invalid Password Attempt", lv.Password, lv.CustomerId);
        }

        status = (clientStatus == LoginVerification.CustomerStatus.Failed ? agentStatus : clientStatus);
      }
      return status;
    }

    private string DecryptStringAES(string cipherText, string key, string iv) {
      var encKeybytes = Encoding.UTF8.GetBytes(key);
      var encIv = Encoding.UTF8.GetBytes(iv);
      var encrypted = Convert.FromBase64String(cipherText);
      var decriptedFromJavascript = DecryptStringFromBytes(encrypted, encKeybytes, encIv);
      return string.Format(decriptedFromJavascript);
    }

    private string DecryptStringFromBytes(byte[] cipherText, byte[] key, byte[] iv) {
      // Check arguments.  
      if (cipherText == null || cipherText.Length <= 0) {
        throw new ArgumentNullException("cipherText");
      }
      if (key == null || key.Length <= 0) {
        throw new ArgumentNullException("key");
      }
      if (iv == null || iv.Length <= 0) {
        throw new ArgumentNullException("key");
      }

      // Declare the string used to hold  
      // the decrypted text.  

      // Create an RijndaelManaged object  
      // with the specified key and IV.  
      using (var rijAlg = new RijndaelManaged()) {
        string plaintext;
        try {
          //Settings  
          rijAlg.Mode = CipherMode.CBC;
          rijAlg.Padding = PaddingMode.PKCS7;
          rijAlg.FeedbackSize = 128;

          rijAlg.Key = key;
          rijAlg.IV = iv;

          // Create a decrytor to perform the stream transform.  
          var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

          try {
            // Create the streams used for decryption.  
            using (var msDecrypt = new MemoryStream(cipherText)) {
              using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read)) {

                using (var srDecrypt = new StreamReader(csDecrypt)) {
                  // Read the decrypted bytes from the decrypting stream  
                  // and place them in a string.  
                  plaintext = srDecrypt.ReadToEnd();

                }

              }
            }
          }
          catch {
            plaintext = "keyError";
          }
        }
        catch (Exception e) {
          throw e;

        }

        return plaintext;
      }
    }

  }
}