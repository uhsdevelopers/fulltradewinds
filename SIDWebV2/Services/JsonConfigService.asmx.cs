﻿using System.ComponentModel;
using System.IO;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using SIDWebLibraries.HelperClasses;

namespace SIDWebV2.Services {
  /// <summary>
  /// Summary description for JsonConfigService
  /// </summary>
  [ScriptService]
  [WebService(Namespace = "http://tempuri.org/")]
  [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
  [ToolboxItem(false)]
  public class JsonConfigService : CoreService {

    private const string JSON_PATH = "/ConfigEditor/data/config.json";

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetConfig() {
      var path = HttpContext.Current.Request.MapPath(JSON_PATH);
      var rc = new ResultContainer { Data = File.ReadAllText(path) };
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer SaveConfig(string config) {
      var path = HttpContext.Current.Request.MapPath(JSON_PATH);
      var fileName = HttpContext.Current.Request.MapPath(path);
      var sw = new StreamWriter(fileName);
      sw.WriteLine(config);
      sw.Close();
      return new ResultContainer();
    }


  }
}
