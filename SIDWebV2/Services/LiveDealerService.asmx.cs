﻿using System;
using System.Web.Script.Services;
using System.Web.Services;
using SIDWebLibraries.HelperClasses;

namespace SIDWebV2.Services {
  /// <summary>
  /// Summary description for AppService
  /// </summary>
  [ScriptService]
  [WebService(Namespace = "http://tempuri.org/")]
  [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
  [System.ComponentModel.ToolboxItem(false)]
  public class LiveDealerService : CoreService {

    private const string CLIENT_USER = "TRADEWINDS";
    private const string CLIENT_PASSWORD = "77eb68b88bd42ab5d8fd7fcdc7e4dc1b";

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer RetrieveSecurityToken() {
      var rc = new ResultContainer();
      if (rc.SessionExpired) return rc;
      try {

        using (var srv = new LiveDealerServiceRef.MasterClientWebServiceSoapClient()) {
          rc.Data = srv.getEncryptedToken(CLIENT_USER, CLIENT_PASSWORD, SessionVariablesCore.CustomerInfo.CustomerID, SessionVariablesCore.CustomerInfo.AgentID, "");
        }
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }
  }
}