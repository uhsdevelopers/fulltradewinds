﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using SIDWebV2.HelperClasses;
using SIDWebLibraries.HelperClasses;
using LineOffering = SIDLibraries.BusinessLayer.LineOffering;
using LineOfferingWeb = SIDWebV2.HelperClasses.LineOffering;
using SystemLog = SIDWebLibraries.HelperClasses.SystemLog;
using WagerItem = SIDWebLibraries.HelperClasses.WagerItem;

namespace SIDWebV2.Services {
  /// <summary>
  /// Summary description for AppService
  /// </summary>
  [ScriptService]
  [WebService(Namespace = "http://tempuri.org/")]
  [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
  [ToolboxItem(false)]
  public class TicketService : CoreService {


    #region Public Services

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer StartNewTicket(double wSID) {
      if (!SessionVariablesCore.IsSessionValid()) return new ResultContainer();

      //LGR MW
      var wager = SessionVariables.Ticket.GetWager(0);

      _RemoveAllWagerItems(wager);
      SessionVariablesCore.InetWagerNumber = wSID;
      SessionVariables.Ticket.Reset();
      SystemLog.WriteAccessLog("Starting New Ticket", "");
      return new ResultContainer();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer AddGameWagerItem(double wSID, string subWagerType, int gameNumber, int periodNumber, string chosenTeamIdx, string riskAmt, string toWinAmt, bool isLineUpdate) {

      var rc = new ResultContainer();
      if (!SessionVariablesCore.IsSessionValid()) return rc;
      if (SessionVariablesCore.WebRobotDetector.IdleTimeReached(SessionVariablesCore.SiteCode)) return new ResultContainer();

      try {

        using (var wSidResult = _IsInetWagerNumberOk(wSID)) if (wSidResult.Code == ResultContainer.ResultCode.Fail) return wSidResult;
        var lineUpdateMsg = isLineUpdate ? (SessionVariablesCore.Translate("Line change: ")) : string.Empty;

        if (SessionVariables.GamesAndContestsLines == null || !SessionVariables.GamesAndContestsLines.Any())
          return new ResultContainer(null, ResultContainer.ResultCode.Fail, "No active lines present");

        spGLWebGetGameOfferings_Result looAryItem;

        //LGR MW
        var wager = SessionVariables.Ticket.GetWager(0);

        rc.Message = WebWagerItem.CreateWagerItem(wager, subWagerType + chosenTeamIdx, subWagerType, gameNumber, periodNumber, out looAryItem, riskAmt, toWinAmt);
        if (!isLineUpdate && (string.IsNullOrEmpty(rc.Message) || rc.Message.ToUpper() != "OK")) {
          rc.SetError(SessionVariablesCore.Translate("Overlapping periods detected for the same (or correlated) game.") + " " + SessionVariablesCore.WagerType.Name);
          return rc;
        }

        var wiToProcess = (from w in wager.WebGameWagerItems
                           where w.ControlCode == subWagerType + chosenTeamIdx
                                    && w.Loo != null
                                    && w.Loo.GameNum == gameNumber
                                    && w.Loo.PeriodNumber == periodNumber
                           select w).FirstOrDefault();
        if (WagerType.IsWebCombinedWager(SessionVariablesCore.WagerType.Name)) {
          var items = new List<WagerItem>();
          items.AddRange(wager.WebGameWagerItems);
          if (wager.OpenPlay.Any()) {
            SystemLog.WriteAccessLog("Add open wager item", "GameNum: " + gameNumber + "SubWager: " + subWagerType + " Chosen teamIdx: " + chosenTeamIdx + " Period: " + periodNumber);
            items.AddRange(SessionVariables.Ticket.OpenPlay.ToList());
          }

          if (rc.Message.ToUpper() == "OK" && items.Count > 1) {
            for (var i = 0; i < items.Count - 1; i++) {
              for (var j = i + 1; j < items.Count; j++) {
                if (!Utilities.CheckForInvalidPickCombinations(SessionVariablesCore.InstanceManager.InstanceModuleInfo, items[i], items[j], SessionVariablesCore.WagerType.Name))
                  continue;
                rc.SetError(lineUpdateMsg + SessionVariablesCore.Translate("INVALID_SELECTION_WITHIN") + " " + SessionVariablesCore.WagerType.Name);

                if (wiToProcess == null)
                  continue;
                wager.WebGameWagerItems.Remove(wiToProcess);
                SystemLog.WriteAccessLog(lineUpdateMsg + "Item not accepted for " + SessionVariablesCore.WagerType.Name,
                                                          _CreateNotAcceptedMsg(SessionVariablesCore.WagerType, wiToProcess, SessionVariablesCore.EasternLineFlag, rc.Message));
              }
            }
          }
          if (rc.Message.ToUpper() == "OK") {
            var maxPicks = 0;
            var itemsCount = (from wi in wager.WebGameWagerItems select wi).Count();

            if (SessionVariablesCore.WagerType.Name == WagerType.PARLAYNAME)
              maxPicks = SessionVariablesCore.ParlayMaxPicks;
            if (SessionVariablesCore.WagerType.Name == WagerType.TEASERNAME)
              maxPicks = SessionVariablesCore.TeaserSpecs.MaxPicks;
            if (SessionVariablesCore.WagerType.Name == WagerType.ACTREVERSENAME)
              maxPicks = 2;
            if (WagerType.IsIfWinFamily(SessionVariablesCore.WagerType))
              maxPicks = 6;
            if (itemsCount > maxPicks) {
              rc.SetWarning(lineUpdateMsg + SessionVariablesCore.Translate("SELECT_NO_MORE_THAN") + " "
                            + maxPicks + " "
                            + SessionVariablesCore.Translate("GAMES_FOR_THIS") + " "
                            + SessionVariablesCore.WagerType.Name);
              if (wiToProcess != null) {
                wager.WebGameWagerItems.Remove(wiToProcess);
                SystemLog.WriteAccessLog(lineUpdateMsg + "Item not accepted for " + SessionVariablesCore.WagerType.Name, _CreateNotAcceptedMsg(SessionVariablesCore.WagerType, wiToProcess, SessionVariablesCore.EasternLineFlag, rc.Message));
              }
            }
          }
          if (wiToProcess != null && rc.Message.ToUpper() == "OK")
            SystemLog.WriteAccessLog(lineUpdateMsg + "Item added to " + SessionVariablesCore.WagerType.Name, "Item #" + wiToProcess.ItemNumber + ": "
                                        + LineOfferingWeb.FormatShortDescription(wiToProcess, SessionVariablesCore.WagerType.Name, SessionVariablesCore.EasternLineFlag) + ", Risk: " + wiToProcess.RiskAmt
                                        + ", To Win: " + wiToProcess.ToWinAmt);
        }
        rc.Data = wiToProcess;

        if (SessionVariablesCore.WagerType.Name != WagerType.STRAIGHTBETNAME && SessionVariablesCore.WagerType.Name != WagerType.WEBPROPSNAME) return rc;

        if (rc.Message.ToUpper() != "OK") {
          if (wiToProcess == null)
            return rc;
          wager.WebGameWagerItems.Remove(wiToProcess);
          SystemLog.WriteAccessLog(lineUpdateMsg + "Item removed from memory for " + SessionVariablesCore.WagerType.Name,
                                      "Item #" + wiToProcess.ItemNumber + ": "
                                      + LineOfferingWeb.FormatShortDescription(wiToProcess, SessionVariablesCore.WagerType.Name, SessionVariablesCore.EasternLineFlag));
        }
        else {
          if (wiToProcess != null)
            SystemLog.WriteAccessLog(lineUpdateMsg + "Item added to " + SessionVariablesCore.WagerType.Name,
                                        "Item #" + wiToProcess.ItemNumber + ": "
                                        + LineOfferingWeb.FormatShortDescription(wiToProcess, SessionVariablesCore.WagerType.Name, SessionVariablesCore.EasternLineFlag));
        }
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }

      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer AddOpenGameWagerItem(double wSID, string subWagerType, int gameNumber, int periodNumber, string controlCode, double? riskAmt, double? toWinAmt, bool isLineUpdate, int wagerPosition, spTkWWebGetWagerItemsOpenPlays_Result looOpenAryItem, int ticketNumber) {
      var rc = new ResultContainer();
      if (!SessionVariablesCore.IsSessionValid()) return rc;

      try {
        using (var wSidResult = _IsInetWagerNumberOk(wSID)) if (wSidResult.Code == ResultContainer.ResultCode.Fail) return wSidResult;
        spGLWebGetGameOfferings_Result looAryItem;
        if (SessionVariables.Ticket == null) {
          StartNewTicket(wSID);
        }
        if (SessionVariables.Ticket != null) SessionVariables.Ticket.OpenPlay.OpenTicketNumber = ticketNumber;
        rc.Message = WebWagerItem.CreateOpenWagerItem(controlCode, subWagerType, gameNumber, periodNumber, out looAryItem, looOpenAryItem, riskAmt, toWinAmt, isLineUpdate, wagerPosition);
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }

      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public ResultContainer ProcessTicket(double wSID, String password, bool useFreePlay, List<WagerValidation.WagerItemUpdateData> wagersData, bool keepOpenPlay, int openPlaysCount) {

      var rc = new ResultContainer();
      if (!SessionVariablesCore.IsSessionValid()) return rc;
      try {
        var sessionValidator = new UniqueSessionValidator(SessionVariablesCore.InstanceManager.InstanceModuleInfo);
        var i = sessionValidator.CheckUniqueSessionId(new UniqueSessionValidator.SessionData(SessionVariablesCore.UniqueKey, SessionVariablesCore.CustomerInfo.CustomerID));
        switch (i) {
          case 0:
            rc.Code = ResultContainer.ResultCode.DoubleSession;
            rc.Message = "Another user has taken over your session. You will now be logged out.";
            SystemLog.WriteAccessLog("Session validator/ProcessTicket(): Double Session", "Another user took over the session, loggin out");
            return rc;

          case 1:
            rc.Code = ResultContainer.ResultCode.UniqueSession;
            rc.Message = "Another user has taken over your session. You can still see reports and other things, but you cannot place wagers.";
            SystemLog.WriteAccessLog("Session validator/ProcessTicket(): Unique Session", "Another user took over the session, place wager disabled");
            return rc;

        }

        using (var wSidResult = _IsInetWagerNumberOk(wSID))
          if (wSidResult.Code == ResultContainer.ResultCode.Fail) return wSidResult;

        if (SessionVariables.Ticket != null && SessionVariables.Ticket.WagerTimerExpired(SessionVariablesCore.ServerDateTime)) {
          SystemLog.WriteAccessLog("Attempting to Place Bet", "Wager Timer Expired");
          return new ResultContainer(null, ResultContainer.ResultCode.Fail, SessionVariablesCore.Translate("WAGER_TIMER_EXPIRED"));
        }

        //LGR MW
        var wager = SessionVariables.Ticket.GetWager(0);

        if (SessionVariables.Ticket != null && wager.TotalWagerItems != wagersData.Count() && wager.TotalWagerItems == 0) {
          SystemLog.WriteAccessLog("Attempting to Place a " + SessionVariablesCore.WagerType.Name,
              "Not all the wagers were validated");
          return new ResultContainer(null, ResultContainer.ResultCode.Fail,
              SessionVariablesCore.Translate("PLACING_A") + " " + SessionVariablesCore.WagerType.Name + SessionVariablesCore.Translate("NOT_ALL_ITEMS_VALIDATED"));
        }

        if (SessionVariablesCore.CustomerSettings.WebRememberPassword == true && SessionVariablesCore.IsMover == false) password = SessionVariablesCore.CustomerInfo.Password;

        spCstWebGetCustomerInfo_Result customerInfo;
        var lv = new LoginVerification(SessionVariablesCore.InstanceManager.InstanceModuleInfo) {
          CustomerId = SessionVariablesCore.CustomerInfo.CustomerID,
          Password = password,
          WebTarget = SessionVariablesCore.WebTarget,
          IpAddress = SessionVariablesCore.ClientIpAddress
        };
        var status = lv.CheckCustomerStatus(out customerInfo);
        switch (status) {
          case LoginVerification.CustomerStatus.Failed:
            SystemLog.WriteAccessLog("Attempting to Place Bet", "Invalid Password.");
            return new ResultContainer(ResultContainer.ResultCode.SessionExpired, SessionVariablesCore.Translate("INVALID_PASSWORD"));
          case LoginVerification.CustomerStatus.Inactive:
            SystemLog.WriteAccessLog("Attempting to Place Bet", "Inactive Account.");
            return new ResultContainer(ResultContainer.ResultCode.SessionExpired, SessionVariablesCore.Translate("INACTIVE_ACCOUNT"));
          case LoginVerification.CustomerStatus.LossCapExceeded:
            SystemLog.WriteAccessLog("Attempting to Place Bet", "Exceeded loss cap.");
            return new ResultContainer(ResultContainer.ResultCode.SessionExpired, SessionVariablesCore.Translate("EXCEEDED_LOSS_CAP"));
          case LoginVerification.CustomerStatus.Suspended:
            SystemLog.WriteAccessLog("Attempting to Place Bet", "Suspended Account");
            return new ResultContainer(ResultContainer.ResultCode.SessionExpired, SessionVariablesCore.Translate("SUSPENDED_ACCOUNT"));
        }
        SessionVariablesCore.FreePlay = useFreePlay;

        //Open plays variables Allan
        if (SessionVariables.Ticket != null) {
            wager.OpenPlay = SessionVariables.Ticket.OpenPlay;
          wager.OpenPlay.KeepOpenPlay = keepOpenPlay;
          wager.OpenPlay.OpenPlayCount = openPlaysCount;
          if (SessionVariables.Ticket.OpenPlay != null) {
            SessionVariables.Ticket.OpenPlay.KeepOpenPlay = keepOpenPlay;
            SessionVariables.Ticket.OpenPlay.OpenPlayCount = openPlaysCount;
          }
        }
        //if (!keepOpenPlay) OpenPlay.WebOpenGameWagerItems.Clear();

        WagerValidation.UpdateWagerItems(wagersData, wager);

        var processed = false;
        switch (SessionVariablesCore.WagerType.Name) {
          case WagerType.STRAIGHTBETNAME:
            rc = WagerValidation.ValidateStraightAndContestBets(wager);
            if (rc.SessionExpired) return rc;
            if (rc.Code == ResultContainer.ResultCode.Success) {
              WagerValidation.ProcessStraightAndContestBets(wager);
              processed = true;
            }
            else SystemLog.WriteAccessLog("Item not accepted for " + SessionVariablesCore.WagerType.Name, "Placing the wager: " + rc.Message);
            break;
          default:
            rc = WagerValidation.ValidateAccumWagerItems(wagersData, wager);
            if (rc.SessionExpired) return rc;
            if (SessionVariables.Ticket != null && wager.OpenPlay.Any()) wager.WebGameWagerItems.AddRange(wager.OpenPlay.ToList());
            if (rc.Code == ResultContainer.ResultCode.Success) {

              if (SessionVariables.Ticket == null || !Utilities.CheckForInvalidPicksCombinations(SessionVariablesCore.WagerType.Name, wager.WebGameWagerItems, SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
                rc.SetError(SessionVariablesCore.Translate("INVALID_SELECTION_WITHIN") + " " + SessionVariablesCore.WagerType.Name);
                SystemLog.WriteAccessLog("Item not accepted for " + SessionVariablesCore.WagerType.Name, "Placing the wager: " + rc.Message);
              }

              switch (SessionVariablesCore.WagerType.Name) {
                case WagerType.PARLAYNAME:
                  //Allan Open Plays
                  WagerValidation.ProcessParlays(wager);
                  break;
                case WagerType.TEASERNAME:
                  //Allan Open Plays
                  WagerValidation.ProcessTeasers(wager);
                  break;
                case WagerType.ACTREVERSENAME:
                case WagerType.IFWINONLYNAME:
                case WagerType.IFWINORPUSHNAME:
                  WagerValidation.ProcessIfBets(wager);
                  break;
              }

              processed = true;

              if (SessionVariablesCore.RoundRobinValue > 0) SessionVariablesCore.TicketRiskAmount = SessionVariablesCore.RiskAmount * SessionVariablesCore.PlayCount;
              else SessionVariablesCore.TicketRiskAmount = SessionVariablesCore.RiskAmount;
              SessionVariablesCore.TicketToWinAmount = SessionVariablesCore.ToWinAmount;
            }
            else SystemLog.WriteAccessLog("Item not accepted for " + SessionVariablesCore.WagerType.Name, rc.Message);

            break;
        }
        rc.Data = new { TicketNumber = SessionVariables.Ticket != null && processed ? SessionVariables.Ticket.TicketNumber : 0, GameWagerItems = SessionVariables.Ticket != null ? wager.WebGameWagerItems : null, WagerOrContestItem = rc.Code == ResultContainer.ResultCode.Warning ? rc.Data : null };
        if (rc.Code == ResultContainer.ResultCode.Success) SessionVariablesLoader.ResetWagerSession();
        if (SessionVariablesCore.ActionAlertSent) {
          WagerValidation.AddActionAlertInfo(wager, "2");
          SystemLog.WriteAccessLog("Ticket Process", "Action Alert - Wager Completed");
        }

      }
      catch (Exception e) {
        Log(e);
        rc.SetError(e.Message);
      }

      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public ResultContainer Continue(List<WagerValidation.WagerItemUpdateData> wagersData) {
      var rc = new ResultContainer();
      if (rc.SessionExpired) return rc;
      try {

        //LGR MW
        var wager = SessionVariables.Ticket.GetWager(0);

        SystemLog.WriteAccessLog("Validating Mover", "");
        var sessionValidator = new UniqueSessionValidator(SessionVariablesCore.InstanceManager.InstanceModuleInfo);
        var i = sessionValidator.CheckUniqueSessionId(new UniqueSessionValidator.SessionData(SessionVariablesCore.UniqueKey, SessionVariablesCore.CustomerInfo.CustomerID));
        switch (i) {
          case 0:
            rc.Code = ResultContainer.ResultCode.DoubleSession;
            rc.Message = "Another user has taken over your session. You will now be logged out.";
            SystemLog.WriteAccessLog("Session validator/Continue(): Double Session", "Another user took over the session, loggin out");
            return rc;

          case 1:
            rc.Code = ResultContainer.ResultCode.UniqueSession;
            rc.Message = "Another user has taken over your session. You can still see reports and other things, but you cannot place wagers.";
            SystemLog.WriteAccessLog("Session validator/Continue(): Unique Session", "Another user took over the session, place wager disabled");
            return rc;
        }

        if (SessionVariablesCore.IsMover) {
          SystemLog.WriteAccessLog("Validating Mover", "Updating WagerItems");
          WagerValidation.UpdateWagerItems(wagersData, wager);
          WagerValidation.AddActionAlertInfo(wager, "1");
          SystemLog.WriteAccessLog("Validating Mover", "Action Alert Sent");
        }
        rc.Code = ResultContainer.ResultCode.Success;
        return rc;

      }
      catch (Exception e) {
        Log(e);
        rc.SetError(e.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public ResultContainer DetermineActionReverseWagerAmount(double wSID, string wagerAmount) {
      var rc = new ResultContainer();
      if (!SessionVariablesCore.IsSessionValid()) return rc;
      try {
        String retValue;
        SystemLog.WriteAccessLog("Validating " + SessionVariablesCore.WagerType.Name + " Wagers", "Amount entered: " + wagerAmount);

        using (var wSidResult = _IsInetWagerNumberOk(wSID)) if (wSidResult.Code == ResultContainer.ResultCode.Fail) return wSidResult;

        //LGR MW
        var wager = SessionVariables.Ticket.GetWager(0);

        if (wager.HasGameWagers && wager.WebGameWagerItems.Count == 2) {
          double toWinAmount = 0;
          foreach (var wi in wager.WebGameWagerItems) {
            if (wi.FinalPrice < 0) {
              wi.AmountEntered = WagerItem.AmountEnteredType.ToWinAmt;
              wi.ToWinAmt = double.Parse(wagerAmount);
              wi.RiskAmt = 0;
            }
            else {
              wi.AmountEntered = WagerItem.AmountEnteredType.RiskAmt;
              wi.ToWinAmt = 0;
              wi.RiskAmt = double.Parse(wagerAmount);
            }
            WagerValidation.CalculateWiWagerAmount(wi);
            WagerValidation.SetMaximumWagerLimit(wi.WagerType, wi);

            if (!WagerValidation.CheckMaximumWager(wi, true, out retValue)) {
              SystemLog.WriteAccessLog("Validating " + SessionVariablesCore.WagerType.Name + " Wagers", "Wager Limit exceeded. " + wagerAmount + " The limit is " + SessionVariablesCore.MaxWagerLimit / 100);
              rc.Code = ResultContainer.ResultCode.Fail;
              rc.Data = "1=" + SessionVariablesCore.Translate("PLACING_ACTION_REVERSE") + " " + SessionVariablesCore.Translate("WAGER_LIMIT_EXCEEDED") + " " + wagerAmount + " " + SessionVariablesCore.Translate("WAGER_LIMIT_IS") + " " + SessionVariablesCore.MaxWagerLimit / 100;
              return rc;
            }
            toWinAmount += wi.ToWinAmt * 2;
          }
          SessionVariablesCore.MaxRiskAmount1 = WagerValidation.CalculateMaxRisk(wager, false);
          SessionVariablesCore.MaxRiskAmount2 = WagerValidation.CalculateMaxRisk(wager, true);
          SessionVariablesCore.MaxRiskAmount = SessionVariablesCore.MaxRiskAmount1 + SessionVariablesCore.MaxRiskAmount2;
          SessionVariablesCore.MaxToWinAmount = toWinAmount;
          retValue = SessionVariablesCore.MaxRiskAmount.ToString(CultureInfo.InvariantCulture);
          retValue += "|";
          retValue += toWinAmount.ToString(CultureInfo.InvariantCulture);
        }
        else {
          rc.Code = ResultContainer.ResultCode.Fail;
          rc.Data = "2=" + SessionVariablesCore.Translate("PROBLEMS_CALCULATING");
          return rc;
        }
        rc.Data = retValue;
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer RemoveGameWagerItem(double wSID, int gameNum, int periodNumber, string subWagerType, int teamPos) {
      var rc = new ResultContainer();
      try {

        //LGR MW
        var wager = SessionVariables.Ticket.GetWager(0);

        if (rc.Code == ResultContainer.ResultCode.SessionExpired ||
          SessionVariables.Ticket == null ||
          wager.WebGameWagerItems == null ||
          !wager.WebGameWagerItems.Any()) return rc;

        var controlCode = subWagerType.Trim() + teamPos.ToString(CultureInfo.InvariantCulture);
        var wiToRemove = (from w in wager.WebGameWagerItems
                          where w.Loo.GameNum == gameNum &&
                                w.ControlCode.Trim() == controlCode &&
                                w.Loo.PeriodNumber == periodNumber
                          select w).FirstOrDefault();
        if (wager.OpenPlay.Any()) {
          SystemLog.WriteAccessLog("Remove open wager item", "GameNum: " + gameNum + " SubWager: " + subWagerType + " Chosen teamIdx: " + teamPos + " Period: " + periodNumber);
        }
        if (wiToRemove != null) _RemoveGameWagerItem(wager, wiToRemove);
        else {
          rc.Message = SessionVariablesCore.Translate("PROBLEMS_REMOVING_ITEM") + "[GameNum: " + gameNum + ", ControlCode: " + controlCode + ", Period: " + periodNumber + "]";
          rc.Code = ResultContainer.ResultCode.Warning;
          SystemLog.WriteAccessLog("Problems removing Item from memory for " + SessionVariablesCore.WagerType.Name, rc.Message);
        }
        _FixGameWagerItemNumbers(wager);
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer AddContestWagerItem(int contestNumber, int contestantNumber) {
      var rc = new ResultContainer();
      if (rc.SessionExpired) return rc;
      if (SessionVariablesCore.WebRobotDetector.IdleTimeReached(SessionVariablesCore.SiteCode)) return new ResultContainer();
      try {
        if (!SessionVariables.GamesAndContestsLines.Any()) return rc;

        //LGR MW
        var wager = SessionVariables.Ticket.GetWager(0);

        spCnWebGetContestantOfferings_Result clooAryItem;
        rc.Message = WebWagerItem.CreateWagerContestItem(wager, contestNumber, contestantNumber, out clooAryItem);

        var wiToProcess = (from w in wager.WebContestantWagerItems
                           where w.Loo.ContestNum == contestNumber
                                    && w.Loo.ContestantNum == contestantNumber
                           select w).FirstOrDefault();
        if (rc.Message.ToUpper() != "OK") {
          if (wiToProcess == null) return rc;
          wager.WebContestantWagerItems.Remove(wiToProcess);
          SystemLog.WriteAccessLog("Item removed from memory", "Item #" + wiToProcess.ItemNumber + ": " + LineOfferingWeb.FormatShortDescription(wiToProcess));
        }
        else if (wiToProcess != null) {
          wiToProcess.MaxWagerLimit = (int)(wiToProcess.Loo.MaxWagerLimit ?? 0);
          rc.Data = wiToProcess;
          SystemLog.WriteAccessLog("Item added to " + SessionVariablesCore.WagerType.Name, "Item #" + wiToProcess.ItemNumber + ": " + LineOfferingWeb.FormatShortDescription(wiToProcess));
        }
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer RemoveContestWagerItem(double wSID, int contestNumber, int contestantNumber) {
      var rc = new ResultContainer();
      if (!SessionVariablesCore.IsSessionValid()) return rc;
      try {
        if (SessionVariables.Ticket == null) return rc;

        //LGR MW
        var wager = SessionVariables.Ticket.GetWager(0);

        var wiToRemove = (from w in wager.WebContestantWagerItems
                          where w.Loo.ContestNum == contestNumber
                                && w.Loo.ContestantNum == contestantNumber
                          select w).FirstOrDefault();
        if (wiToRemove != null) {
          try {
            wager.WebContestantWagerItems.Remove(wiToRemove);
            rc.Message = "Ok";
            rc.Code = ResultContainer.ResultCode.Success;
            SystemLog.WriteAccessLog("Item removed", "Item #" + wiToRemove.ItemNumber + ": " + LineOfferingWeb.FormatShortDescription(wiToRemove));
          }
          catch (Exception e) {
            rc.Message = SessionVariablesCore.Translate("PROBLEMS_REMOVING_ITEM") + " Error: " + e.Message;
            rc.Code = ResultContainer.ResultCode.Fail;
            SystemLog.WriteAccessLog("Problems removing Item from memory", rc.Message);
          }
        }
        else {
          rc.Message = SessionVariablesCore.Translate("PROBLEMS_REMOVING_ITEM");
          rc.Code = ResultContainer.ResultCode.Warning;
          SystemLog.WriteAccessLog("Problems removing Item from memory for " + SessionVariablesCore.WagerType.Name, rc.Message);
        }
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer RemoveAllWagerItems() {
      var rc = new ResultContainer();
      if (!SessionVariablesCore.IsSessionValid()) return rc;
      try {
        //LGR MW
        var wager = SessionVariables.Ticket.GetWager(0);
        _RemoveAllWagerItems(wager);
        SessionVariables.Ticket.OpenPlay.Clear();
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      SystemLog.WriteAccessLog("Wager cancelled", string.Empty);
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public ResultContainer GetLeagueBuyPointsInfo(string sportType, string sportSubType, string controlCode, string wagerType, string chosenTeamId, int periodNumber) {
      var rc = new ResultContainer();
      if (!SessionVariablesCore.IsSessionValid()) return rc;
      try {
        using (var bp = new BuyPoints(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
          var buyPoints = bp.GetCostToBuyPointsBySportSubTypeAndCustomer(sportType.Trim(), sportSubType.Trim(), SessionVariablesCore.CustomerInfo.CustomerID.Trim());
          var costToBuy = new BuyPoints.CostToBuyPoints {
            ProgressivePointBuyingFlag = buyPoints.ProgressivePointBuyingFlag
          };

          //LGR MW
          var wager = SessionVariables.Ticket.GetWager(0);

          var actualWager = wager.WebGameWagerItems.FirstOrDefault(w => w.ControlCode == controlCode && w.ChosenTeamId == chosenTeamId && w.Loo.PeriodNumber == periodNumber);
          if (actualWager != null) {
            var currentGameInfo = new spGLGetActiveGamesByCustomer_Result {
              SportType = actualWager.Loo.SportType,
              SportSubType = actualWager.Loo.SportSubType,
              PreventPointBuyingFlag = actualWager.Loo.PreventPointBuyingFlag,
              Team1ID = actualWager.Loo.Team1ID,
              Team2ID = actualWager.Loo.Team2ID,
              FavoredTeamID = actualWager.Loo.FavoredTeamID,
              Spread = actualWager.Loo.Spread,
              SpreadAdj1 = actualWager.Loo.SpreadAdj1,
              SpreadAdj2 = actualWager.Loo.SpreadAdj2,
              TotalPoints = actualWager.Loo.TotalPoints,
              TtlPtsAdj1 = actualWager.Loo.TtlPtsAdj1,
              TtlPtsAdj2 = actualWager.Loo.TtlPtsAdj2
            };
            var progressiveBuyPoint = bp.GetProgressivePointCost(buyPoints.ProgressiveChartName, sportType, sportSubType, wagerType);

            if (!BuyPoints.SetBaskeballAndFootballBuyPointsExceptions(bp, costToBuy, wagerType, sportType, sportSubType, SessionVariablesCore.CustomerInfo.CustomerID.Trim(), SessionVariablesCore.InstanceManager.InstanceModuleInfo.Parameters.SportSubTypeBasketballCollege, SessionVariablesCore.InstanceManager.InstanceModuleInfo.Parameters.SportSubTypeFootballCollege)) {

              if (controlCode.Substring(0, 1) == WagerType.SPREAD) {
                if (buyPoints.SpreadBuy != null) costToBuy.CostToBuySpread = (int)buyPoints.SpreadBuy;
                if (buyPoints.SpreadBuy != null && buyPoints.SpreadBuyMax != null && buyPoints.SpreadBuyOff3 != null && buyPoints.SpreadBuyOff7 != null && buyPoints.SpreadBuyOn7 != null) {
                  costToBuy.CostToBuyMax = (int)buyPoints.SpreadBuyMax;
                  costToBuy.CostToBuyOnThree = buyPoints.SpreadBuyOn3 ?? 0;
                  costToBuy.CostToBuyOffThree = (int)buyPoints.SpreadBuyOff3;
                  costToBuy.CostToBuyOffSeven = (int)buyPoints.SpreadBuyOff7;
                  costToBuy.CostToBuyOnSeven = (int)buyPoints.SpreadBuyOn7;
                  costToBuy.CostToBuyTotals = (int)buyPoints.SpreadBuy;
                }
              }
              else {
                if (buyPoints.TotalBuy != null) costToBuy.CostToBuyTotals = (int)buyPoints.TotalBuy;
                if (buyPoints.TotalBuy != null && buyPoints.TotalBuyMax != null) {
                  costToBuy.CostToBuyMax = buyPoints.TotalBuyMax ?? 0;
                  costToBuy.CostToBuyOffThree = (int)buyPoints.TotalBuy;
                  costToBuy.CostToBuyOffSeven = (int)buyPoints.TotalBuy;
                  costToBuy.CostToBuyOnSeven = (int)buyPoints.TotalBuy;
                  costToBuy.CostToBuyTotals = (int)buyPoints.TotalBuy;
                }
              }
            }
            rc.Data = BuyPoints.CreateBuyPointsOptionsObject(controlCode.Substring(0, 1), costToBuy, currentGameInfo, progressiveBuyPoint, Int32.Parse(controlCode.Substring(1, 1)));
          }
        }
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public ResultContainer GetParlayInfo(int pickCount = 0, string parlayName = null) {
      var rc = new ResultContainer();
      if (!SessionVariablesCore.IsSessionValid()) return rc;
      try {
        List<spLOGetParlayPayCard_Result> pc;
        if (string.IsNullOrEmpty(parlayName)) parlayName = SessionVariablesCore.ParlayName;

        using (var ln = new LineOffering.Parlay(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
          pc = ln.GetParlayPayCard(pickCount, parlayName);
          var maxParlayPayout = SessionVariablesCore.InstanceManager.InstanceModuleInfo.Parameters.MaxParlayPayout;
          SessionVariablesCore.MaxParlayPayout = ln.GetPayCardMaxPayout(SessionVariablesCore.ParlayName, pickCount, SessionVariablesCore.RiskAmount);
          if (SessionVariablesCore.MaxParlayPayout == 0 || (maxParlayPayout > 0 && maxParlayPayout < SessionVariablesCore.MaxParlayPayout)) SessionVariablesCore.MaxParlayPayout = maxParlayPayout;
        }
        using (var pl = new LineOffering(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
          pl.GetParlaySpecs(parlayName);
        }
        rc.Data = new {
          ParlayPayCards = pc,
          SessionVariablesCore.CustMaxParlayPayout,
          SessionVariablesCore.ParlayPayoutType,
          MaxParlayPayout = SessionVariablesCore.MaxParlayPayout * SessionVariablesCore.CustomerInfo.ExchangeRate,
          SessionVariablesCore.CustMaxParlayBet,
          SessionVariablesCore.ParlayDefaultPrice,
          SessionVariablesCore.ShowRifOption,
          SessionVariablesCore.ParlayMaxPicks
        };
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public ResultContainer GetTeaserInfo(string teaserName, int pickCount, int gamesWon = 0) {
      var rc = new ResultContainer();
      if (!SessionVariablesCore.IsSessionValid()) return rc;
      try {
        List<spLOGetTeaserPayCard_Result> tpc;
        spLOGetTeaserSpecs_Result ts;

        if (string.IsNullOrEmpty(teaserName)) teaserName = SessionVariablesCore.TeaserName;
        using (var ln = new LineOffering.Teaser(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
          tpc = ln.GetTeaserPayCard(teaserName, pickCount, gamesWon);
          ts = ln.GetTeaserSpecs(teaserName);
        }
        rc.Data = new { teaserPayCards = tpc, teaserSpecs = ts };
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetWagerPicks() {
      var minPicks = 1;
      var maxPicks = 999;
      var rc = new ResultContainer();
      try {
        if (!SessionVariablesCore.IsSessionValid()) return rc;

        if (SessionVariablesCore.WagerType.Name == WagerType.PARLAYNAME ||
            SessionVariablesCore.WagerType.Name == WagerType.ACTREVERSENAME ||
            SessionVariablesCore.WagerType.Name == WagerType.IFWINONLYNAME ||
                      SessionVariablesCore.WagerType.Name == WagerType.IFWINORPUSHNAME) {
          minPicks = 2;
          if (SessionVariablesCore.WagerType.Name == WagerType.PARLAYNAME) maxPicks = SessionVariablesCore.ParlayMaxPicks;
        }

        if (SessionVariablesCore.WagerType.Name == WagerType.GetFromWebCode(WagerType.WEBTEASER).Name) {
          minPicks = SessionVariablesCore.TeaserSpecs.MinPicks;
          maxPicks = SessionVariablesCore.TeaserSpecs.MaxPicks;
        }

        SessionVariablesCore.WagerMinPicks = minPicks;
        rc.Data = new { MinPicks = minPicks, MaxPicks = maxPicks };
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetOpenPlays() {
      var rc = new ResultContainer();
      if (!SessionVariablesCore.IsSessionValid()) return rc;
      try {
        using (var tkw = new TicketsAndWagers(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
          rc.Data = tkw.GeOpenPlays(SessionVariablesCore.CustomerInfo.CustomerID, (SessionVariablesCore.WagerType.Name == "Parlay" ? "P" : "T"));
        }
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;

    }

    #endregion

    #region Private Common Methods

    private static string _CreateNotAcceptedMsg(WagerType wagerType, WagerItem wiToProcess, bool? easternLineFlag, string message) {
      return "Item #" + wiToProcess.ItemNumber + ": "
             + LineOfferingWeb.FormatShortDescription(wiToProcess, wagerType.Name, easternLineFlag)
             + " " + message;
    }

    private void _FixGameWagerItemNumbers(WebWager wager) {
      if (SessionVariables.Ticket == null || !wager.HasGameWagers) return;
      var idx = 1;
      foreach (var wi in wager.WebGameWagerItems.OrderBy(w => w.ItemNumber)) {
        wi.ItemNumber = idx;
        idx++;
      }
    }

    private void _RemoveGameWagerItem(WebWager wager, WagerItem wagerItemToRemove, bool log = true) {
      var rc = new ResultContainer();
      if (wagerItemToRemove == null) return;
      try {
        wager.WebGameWagerItems.Remove(wagerItemToRemove);
        rc.Message = "Ok";
        rc.Code = ResultContainer.ResultCode.Success;
        if (log) SystemLog.WriteAccessLog("Item removed from " + SessionVariablesCore.WagerType.Name,
          "Item #" + wagerItemToRemove.ItemNumber + ": "
          + LineOfferingWeb.FormatShortDescription(wagerItemToRemove, SessionVariablesCore.WagerType.Name, SessionVariablesCore.EasternLineFlag));
      }
      catch (Exception e) {
        rc.Message = SessionVariablesCore.Translate("PROBLEMS_REMOVING_ITEM") + " Error: " + e.Message;
        rc.Code = ResultContainer.ResultCode.Fail;
        if (log) SystemLog.WriteAccessLog("Problems removing Item from memory for " + SessionVariablesCore.WagerType.Name, rc.Message);
      }
    }

    private void _RemoveContestantWagerItem(WebWager wager, ContestantWagerItem wagerItemToRemove, bool log = true) {
      var rc = new ResultContainer();
      if (wagerItemToRemove == null) return;
      try {
        wager.WebContestantWagerItems.Remove(wagerItemToRemove);
        rc.Message = "Ok";
        rc.Code = ResultContainer.ResultCode.Success;
        if (log) SystemLog.WriteAccessLog("Item removed", "Item #" + wagerItemToRemove.ItemNumber + ": " + LineOfferingWeb.FormatShortDescription(wagerItemToRemove));
      }
      catch (Exception e) {
        rc.Message = SessionVariablesCore.Translate("PROBLEMS_REMOVING_ITEM") + " Error: " + e.Message;
        rc.Code = ResultContainer.ResultCode.Fail;
        if (log) SystemLog.WriteAccessLog("Problems removing Item from memory", rc.Message);
      }
    }

    private void _RemoveAllWagerItems(WebWager wager) {

      if (wager == null) return;

      if (wager.HasGameWagers) {
        for (var i = wager.WebGameWagerItems.Count() - 1; i >= 0; i--) {
          _RemoveGameWagerItem(wager, wager.WebGameWagerItems[i], false);
        }
        wager.WebGameWagerItems.Clear();
      }

      if (!wager.HasContestantWagers) return;
      for (var i = wager.WebContestantWagerItems.Count() - 1; i >= 0; i--) {
        _RemoveContestantWagerItem(wager, wager.WebContestantWagerItems[i]);
      }
      wager.WebContestantWagerItems.Clear();
      SystemLog.WriteAccessLog("All wagers were removed from bet slip. (" + SessionVariablesCore.WagerType.Name + ")", string.Empty);

    }

    private static ResultContainer _IsInetWagerNumberOk(double wSid) {
      if (WebTicket.IsInetWagerNumberOk(wSid)) return new ResultContainer();
      SystemLog.WriteAccessLog("Attempting to Place Bet", "Wager already processed.");
      return new ResultContainer(null, ResultContainer.ResultCode.Fail, SessionVariablesCore.Translate("WAGER_PROCESSED"));
    }

    #endregion

    #region Private Game Lines Methods

    private static void _SetMaximumWagerLimit(string wagerType, double? circledMaxWager, double? storeMaxWager, string sportType, string sportSubType, int periodNumber, double contestantMaxBet) {
      double wagerLimit = 10000000; // instead of 10000000
      var wagerLimitDesc = " " + SessionVariablesCore.Translate("FOR_THIS_WAGER");

      switch (wagerType) {
        case WagerType.PARLAY:
          storeMaxWager = SessionVariablesCore.InstanceManager.InstanceModuleInfo.Parameters.MaxInetParlayBet;
          break;
        case WagerType.TEASER:
          storeMaxWager = SessionVariablesCore.InstanceManager.InstanceModuleInfo.Parameters.MaxInetTeaserBet;
          break;
        case WagerType.CONTEST:
          storeMaxWager = SessionVariablesCore.InstanceManager.InstanceModuleInfo.Parameters.MaxInetContestBet;
          break;
      }
      if (storeMaxWager != null && storeMaxWager > 0) {
        if (wagerLimit > storeMaxWager * SessionVariablesCore.ExchangeRate)
          wagerLimit = (double)storeMaxWager * SessionVariablesCore.ExchangeRate;
      }
      if (wagerLimit > SessionVariablesCore.CustWagerLimit * SessionVariablesCore.ExchangeRate && SessionVariablesCore.CustWagerLimit > 0) {
        wagerLimit = SessionVariablesCore.CustWagerLimit * SessionVariablesCore.ExchangeRate;
        wagerLimitDesc = " " + SessionVariablesCore.Translate("BASED_ON_ACCT");
      }
      var modifiedWagerType = WagerType.IsStraightBetFamily(wagerType) ? WagerType.STRAIGHTBET : wagerType;

      switch (modifiedWagerType) {
        case WagerType.STRAIGHTBET:
          double? dtlLimit;

          using (var cust = new Customers(SessionVariablesCore.InstanceManager.InstanceModuleInfo))
            dtlLimit = cust.GetCustomerWebDetailsLimits(SessionVariablesCore.CustomerInfo.CustomerID, sportType, sportSubType, periodNumber, wagerType);

          if (dtlLimit != null && dtlLimit > 0) {
            wagerLimit = (double)dtlLimit * SessionVariablesCore.ExchangeRate;
            wagerLimitDesc = " " + SessionVariablesCore.Translate("BASED_ON_ACCT_AND_WAGER");
          }
          break;
        case WagerType.PARLAY:
          if (SessionVariablesCore.CustMaxParlayBet > 0) {
            wagerLimit = SessionVariablesCore.CustMaxParlayBet * SessionVariablesCore.ExchangeRate;
            wagerLimitDesc = " " + SessionVariablesCore.Translate("BASED_ON_ACCT_AND_WAGER");
          }
          break;
        case WagerType.TEASER:
          if (SessionVariablesCore.CustMaxTeaserBet > 0) {
            wagerLimit = SessionVariablesCore.CustMaxTeaserBet * SessionVariablesCore.ExchangeRate;
            wagerLimitDesc = " " + SessionVariablesCore.Translate("BASED_ON_ACCT_AND_WAGER");
          }
          break;
        case WagerType.CONTEST:
          if (SessionVariablesCore.CustMaxContestBet > 0) {
            wagerLimit = SessionVariablesCore.CustMaxContestBet * SessionVariablesCore.ExchangeRate;
            wagerLimitDesc = " " + SessionVariablesCore.Translate("BASED_ON_ACCT_AND_WAGER");

            if (contestantMaxBet > 0 && contestantMaxBet < wagerLimit) {
              wagerLimit = contestantMaxBet * SessionVariablesCore.ExchangeRate;
              wagerLimitDesc = " " + SessionVariablesCore.Translate("FOR_THIS_WAGER");
            }
          }
          break;
      }
      if (circledMaxWager != null && circledMaxWager > 0) {
        if (wagerLimit > (circledMaxWager * SessionVariablesCore.ExchangeRate)) {
          wagerLimit = ((double)circledMaxWager * SessionVariablesCore.ExchangeRate);
          wagerLimitDesc = " " + SessionVariablesCore.Translate("GAME_IS_CIRCLED");
        }
      }
      SessionVariablesCore.MaxWagerLimit = wagerLimit;
      SessionVariablesCore.MaxWagerLimitDesc = wagerLimitDesc;
      // SessionVariablesCore.NightCircleLimitWithDesc = ""; // Set, but never used DMN 20150305
      // SessionVariablesCore.MaxWagerLimitBeforeCircle = wagerLimit; // Set, but never used DMN 20150305

      if (!_CheckEveningTime() ||
          !(SessionVariablesCore.MaxWagerLimit > SessionVariablesCore.EveningGameLimit * SessionVariablesCore.ExchangeRate)) return;
      // SessionVariablesCore.NightCircleLimitWithDesc = " (" + SessionVariablesCore.EveningGameLimit * SessionVariablesCore.CurrencyRate + " " + SessionVariablesCore.Translate("NIGHT_CIRCLE"] + ")"; // Set, but never used DMN 20150305
      SessionVariablesCore.MaxWagerLimit = SessionVariablesCore.EveningGameLimit * SessionVariablesCore.ExchangeRate;
      SessionVariablesCore.MaxWagerLimitDesc = " " + SessionVariablesCore.Translate("BASED_ON_EVENING_LIMITS");
    }

    private static bool _CheckEveningTime() {
      // Exceptions Section Begins.  If For Some reason, Night Circles will not apply Date for that should be written here.

      var exceptionDates = new List<WagerValidation.DateRange>();
      var dt = new WagerValidation.DateRange {
        StartingDate = SessionVariablesCore.ServerDateTime.AddDays(-100),
        EndingDate = SessionVariablesCore.ServerDateTime.AddDays(-99)
      };
      exceptionDates.Add(dt);

      var currentTime = SessionVariablesCore.ServerDateTime;

      for (var i = 0; i < exceptionDates.Count; i++) {
        if (currentTime >= exceptionDates[i].StartingDate && currentTime < exceptionDates[i].EndingDate)
          return false;
      }
      // Exceptions Section Ends.

      _SetEveningGameBetLimit();
      //SessionVariablesCore.EveningEndTime
      //SessionVariablesCore.EveningEndTime

      if (int.Parse(SessionVariablesCore.EveningStartTime) > int.Parse(SessionVariablesCore.EveningEndTime)) {
        if (currentTime.Hour > int.Parse(SessionVariablesCore.EveningStartTime.Substring(0, 2)) ||
           (currentTime.Hour == int.Parse(SessionVariablesCore.EveningStartTime.Substring(0, 2)) &&
            currentTime.Minute > int.Parse(SessionVariablesCore.EveningStartTime.Substring(2, 2))) ||
           currentTime.Hour < int.Parse(SessionVariablesCore.EveningEndTime.Substring(0, 2)) ||
           (currentTime.Hour == int.Parse(SessionVariablesCore.EveningEndTime.Substring(0, 2)) &&
            currentTime.Minute < int.Parse(SessionVariablesCore.EveningEndTime.Substring(2, 2))))
          return true;
      }
      else {
        if ((currentTime.Hour > int.Parse(SessionVariablesCore.EveningStartTime.Substring(0, 2)) ||
            (currentTime.Hour == int.Parse(SessionVariablesCore.EveningStartTime.Substring(0, 2)) &&
             currentTime.Minute > int.Parse(SessionVariablesCore.EveningStartTime.Substring(2, 2)))) &&
           (currentTime.Hour < int.Parse(SessionVariablesCore.EveningEndTime.Substring(0, 2)) ||
            (currentTime.Hour == int.Parse(SessionVariablesCore.EveningEndTime.Substring(0, 2)) &&
             currentTime.Minute < int.Parse(SessionVariablesCore.EveningEndTime.Substring(2, 2)))))
          return true;
      }
      return false;
    }

    private static void _SetEveningGameBetLimit() {
      using (var lo = new LineOffering(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
        var res = lo.GetCustomerNightCircleSchedule(SessionVariablesCore.ServerDateTime.AddHours(4).ToString("MM/dd/yyyy HH:mm:ss"));

        SessionVariablesCore.EveningStartTime = res.StartTime;
        SessionVariablesCore.EveningEndTime = res.EndTime;
        SessionVariablesCore.EveningGameLimit = res.GameLimit ?? 0;
        SessionVariablesCore.EveningGameLimit = SessionVariablesCore.EveningGameLimit * 100;
      }
    }

    #endregion

  }
}