﻿using System.Web.Script.Services;
using System.Web.Services;
using SIDLibraries.Utilities;
using SIDWebLibraries.HelperClasses;

namespace SIDWebV2.Services {
  /// <summary>
  /// Summary description for ErrorService
  /// </summary>
  [WebService(Namespace = "http://tempuri.org/")]
  [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
  [System.ComponentModel.ToolboxItem(false)]
  // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
  // [System.Web.Script.Services.ScriptService]
  public class ErrorService : CoreService {

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer Error(string appName, string stackTrace) {
      using (var lg = new LogWriter(SessionVariablesCore.InstanceManager.InstanceModuleInfo, SessionVariablesCore.SiteCode, SessionVariablesCore.UniqueKey)) {
        if (SessionVariablesCore.CustomerInfo != null) stackTrace = "CustomerId: " + SessionVariablesCore.CustomerInfo.CustomerID + "\n" + stackTrace;
        if (!string.IsNullOrEmpty(ServerVariables.Referer)) stackTrace = "Referer: " + ServerVariables.Referer + "\n" + stackTrace;
        lg.WriteToSystemLog("Error", appName, stackTrace, SessionVariablesCore.InstanceManager.InstanceModuleInfo.WindowsUserId, ServerVariables.ServerName);
      }
      return new ResultContainer();
    }
  }
}
