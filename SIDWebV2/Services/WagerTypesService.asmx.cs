﻿using System;
using System.Collections.Generic;
using System.Web.Script.Services;
using System.Web.Services;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using SIDWebV2.HelperClasses;
using WebLibs = SIDWebLibraries.HelperClasses;

namespace SIDWebV2.Services {
  /// <summary>
  /// Summary description for AppService
  /// </summary>
  [ScriptService]
  [WebService(Namespace = "http://tempuri.org/")]
  [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
  [System.ComponentModel.ToolboxItem(false)]
  public class WagerTypesService : CoreService {

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public WebLibs.ResultContainer GetActiveWagerType() {
      var rc = new WebLibs.ResultContainer();
      try {
        if (rc.Code != WebLibs.ResultContainer.ResultCode.SessionExpired) rc.Data = WebLibs.SessionVariablesCore.WagerType;
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public WebLibs.ResultContainer ChangeWagerType(int wagerTypeId) {
      var rc = new WebLibs.ResultContainer();
      try {
        if (rc.Code == WebLibs.ResultContainer.ResultCode.SessionExpired) return rc;

        //LGR MW
        var wager = SessionVariables.Ticket.GetWager(0);

        var previousWagerType = WebLibs.SessionVariablesCore.WagerType;
        WebLibs.SessionVariablesCore.WagerType = WagerType.GetFromWebCode(wagerTypeId);
        if (SessionVariables.Ticket != null) {
          wager.WebGameWagerItems = new List<WebLibs.WagerItem>();
          wager.WebContestantWagerItems = new List<ContestantWagerItem>();
        }

        WebLibs.SessionVariablesCore.ParlayMaxPicks = 0;
        WebLibs.SessionVariablesCore.ParlayDefaultPrice = 0;
        WebLibs.SystemLog.WriteAccessLog("Selected WagerType ", "Changed From " + previousWagerType.Name + " to " + WebLibs.SessionVariablesCore.WagerType.Name);
        switch (wagerTypeId) {
          case 0:
          case WagerType.WEBACTREVERSE:
            break;
          case WagerType.WEBPARLAY:
            var lo = new SIDLibraries.BusinessLayer.LineOffering(WebLibs.SessionVariablesCore.InstanceManager.InstanceModuleInfo);
            spLOWebGetParlaySpecs_Result parlaySpecs = null;
            WebLibs.SessionVariablesCore.ParlayMaxPicks = 99;
            WebLibs.SessionVariablesCore.ParlayDefaultPrice = 0;
            if (!string.IsNullOrEmpty(WebLibs.SessionVariablesCore.ParlayName))
              parlaySpecs = lo.GetParlaySpecs(WebLibs.SessionVariablesCore.ParlayName);
            if (parlaySpecs != null) {
              WebLibs.SessionVariablesCore.ParlayMaxPicks = parlaySpecs.MaxPicks ?? 99;
              WebLibs.SessionVariablesCore.ParlayDefaultPrice = parlaySpecs.DefaultPrice ?? 0;
            }
            break;
          case WagerType.WEBTEASER:
            break;
          case WagerType.WEBIFWINORPUSH:
          case WagerType.WEBIFWINONLY:
            break;
          case WagerType.WEBPROPS:
            break;
        }
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

  }
}