﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using SIDLibraries.BusinessLayer;
using SIDWebLibraries.HelperClasses;
using SIDWebV2.HelperClasses;
using SystemLog = SIDWebLibraries.HelperClasses.SystemLog;

namespace SIDWebV2.Services {
  /// <summary>
  /// Summary description for ReportsService
  /// </summary>
  [WebService(Namespace = "http://tempuri.org/")]
  [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
  [ToolboxItem(false)]
  // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
  // [System.Web.Script.Services.ScriptService]
  public class ReportsService : CoreService {

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetCasinoTransactionsByDate(string customerId, string date) {
      var rc = new ResultContainer();
      if (rc.SessionExpired) return rc;
      try {
        date = new string(date.Where(c => char.IsNumber(c) || c == '/').ToArray());
        using (var ta = new Transactions(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
          rc.Data = ta.GetCasinoTransactionsByDate(customerId, DateTime.Parse(date));
          SystemLog.WriteAccessLog("Customer " + customerId.Trim() + " viewed report", "Daily figures/Casino transactions");
        }
      }
      catch (Exception ex) {
        Log(new Exception(ex.Message + ". Date: " + date));
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetCashTransactionsByDate(string customerId, string date) {
      var rc = new ResultContainer();
      if (rc.SessionExpired) return rc;
      try {
        date = new string(date.Where(c => char.IsNumber(c) || c == '/').ToArray());
        using (var ta = new Transactions(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
          var res = ta.GetCashTransactionsByDate(customerId, DateTime.Parse(date));

          if (res == null) return rc;
          var report = (from r in res
                        select new {
                          r.DocumentNumber, r.TranCode, TranType = new {
                            code = r.TranType,
                            name = (r.TranType == "E" ? "Deposit" :
                            r.TranType == "C" ? "Credit Adjustment" :
                            r.TranType == "T" ? "Transfer Credit" :
                            r.TranType == "I" ? "Withdrawal" :
                            r.TranType == "D" ? "Debit Adjustment" :
                            r.TranType == "U" ? "Transfer Debit" :
                            r.TranType == "Q" ? "Agent Distribution" : "Refound")
                          },
                          r.Amount, r.ShortDesc, r.TranDateTimeString
                        }).ToList();

          rc.Data = report;
          SystemLog.WriteAccessLog("Customer " + customerId.Trim() + " viewed report", "Daily figures/Cash transactions");
        }
      }
      catch (Exception ex) {
        Log(new Exception(ex.Message + ". Date: " + date));
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetCustomerPendingBets(string customerId) {
      var rc = new ResultContainer();
      try {
        using (var ta = new TicketsAndWagers(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
          rc.Data = ta.GetCustomerPendingBets(customerId);
          SystemLog.WriteAccessLog("Customer " + customerId.Trim() + " viewed report", "Open Bets");
        }
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetCustomerTransactionListByDays(string customerId, int numDays) {
      var rc = new ResultContainer();
      try {
        using (var ta = new Transactions(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
          rc.Data = ta.GetCustomerTransactionListByDays(customerId, numDays);
          SystemLog.WriteAccessLog("Customer " + customerId.Trim() + " viewed report", "All Transactions");
        }
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetCustomerTransactionListByDateRange(string customerId, DateTime initDate, DateTime finalDate) {
      var rc = new ResultContainer();
      try {
        using (var ta = new Transactions(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
          rc.Data = ta.GetCustomerTransactionListByDateRange(customerId, initDate, finalDate);
          SystemLog.WriteAccessLog("Customer " + customerId.Trim() + " viewed report", "All Transactions by date range");
        }
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetCustomerTransactionListByDate(string customerId, string date) {
      var rc = new ResultContainer();
      if (rc.SessionExpired) return rc;
      try {
        date = new string(date.Where(c => char.IsNumber(c) || c == '/').ToArray());
        using (var ta = new Transactions(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
          DateTime ddate;
          if (!DateTime.TryParse(date, out ddate)) ddate = DateTime.Now;
          rc.Data = ta.GetTransactionListByDate(customerId, ddate);
          SystemLog.WriteAccessLog("Customer " + customerId.Trim() + " viewed report", "Transactios by date");
        }
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetCustomerDailyFigures(string customerId, int weekOffset, string currencyCode) {
      var rc = new ResultContainer();
      try {
        using (var df = new DailyFigures(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
          var res = df.GetByCustomer(customerId, weekOffset, currencyCode);

          if (res == null)
            return rc;
          var report = new {
            CashInOutTotal = (from r in res
                              select r.CashInOut.HasValue ? r.CashInOut.Value : 0).Sum(),
            EndingBalance = 0.0,
            StartingBalance = res.First().StartingBalace,
            WeekOffset = weekOffset,
            WinLossTotal = (from r in res
                            select r.WinLoss.HasValue ? r.WinLoss.Value : 0).Sum(),
            ZeroBalance = res.First().ZeroBal,
            ValuesPerDay = (from r in res
                            select new {
                              r.ThisDate,
                              r.CashInOut,
                              r.WinLoss,
                              r.CasinoWinLoss,
                              Wagers = new List<object>(),
                              r.WagersCount
                            }).ToList(),
            CasinoWinLossTotal = (from r in res
                                  select r.CasinoWinLoss.HasValue ? r.CasinoWinLoss.Value : 0).Sum()
          };
          rc.Data = report;

          SystemLog.WriteAccessLog("Customer " + customerId.Trim() + " viewed report", "Daily Figures");
        }
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetDailyWagers(string customerId, DateTime dailyFigureDate, string store) {
      var rc = new ResultContainer();
      try {
        using (var df = new DailyFigures(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
          var res = df.GetDailyWagers(customerId, dailyFigureDate);

          if (res == null)
            return rc;
          rc.Data = (from r in res
                     select new {
                       r.AmountLost,
                       r.AmountWon,
                       r.ARBirdcageLink,
                       r.ARLink,
                       r.BoxLink,
                       r.DailyFigureDate,
                       Description = r.Description.Split('\n'),
                       r.FreePlayFlag,
                       //r.GradeDateTime,
                       r.PlayNumber,
                       r.RoundRobinLink,
                       r.TicketNumber,
                       r.TicketWriter,
                       r.WagerNumber,
                       WagerType = WagerType.GetFromCode(r.WagerType.Trim()).Name
                     }).ToList();
          SystemLog.WriteAccessLog("Customer " + customerId.Trim() + " viewed report", "Daily Figures Win/Loss details");
        }
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetWagerItemsOpenPlays(int ticketNumber, int wagerNumber, double wSID) {
      var rc = new ResultContainer();
      try {
        using (var ta = new TicketsAndWagers(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
          var data = ta.GetWagerItemsOpenPlays(ticketNumber, wagerNumber);
          SystemLog.WriteAccessLog("Get open wager", "Ticket No. " + ticketNumber);
          using (var tk = new TicketService()) {
            SessionVariables.Ticket.OpenPlay.Clear();
            foreach (var d in data) {
              SessionVariables.Ticket.OpenPlay.OpenTicketNumber = ticketNumber;
              SessionVariables.Ticket.OpenPlay.OpenWagerNumber = wagerNumber;
              d.TicketNumber = ticketNumber;
              tk.AddOpenGameWagerItem(wSID, d.WagerType.Trim(), Convert.ToInt32(d.GameNum), Convert.ToInt32(d.PeriodNumber), d.ControlCode.Trim(), d.AmountWagered, d.ToWinAmount, false, -1, d, ticketNumber);

            }
          }
          rc.Data = data;
        }
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }
  }
}
