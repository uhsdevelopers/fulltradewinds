﻿using System;
using System.Web.Script.Services;
using System.Web.Services;
using SIDLibraries.BusinessLayer;
using SIDWebV2.HelperClasses;
using SIDWebLibraries.HelperClasses;
using SystemLog = SIDWebLibraries.HelperClasses.SystemLog;

namespace SIDWebV2.Services {
  /// <summary>
  /// Summary description for inboxService
  /// </summary>
  [WebService(Namespace = "http://tempuri.org/")]
  [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
  [System.ComponentModel.ToolboxItem(false)]
  public class InboxService : CoreService {

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void FlagCustomerMessage(int messageId, string targetAction) {
      try {
        using (var cust = new Customers(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
          if (SessionVariablesCore.CustomerInfo == null)
            return;
          var changeSource = SessionVariablesCore.SiteCode;
          cust.FlagWebCustomerMessage(SessionVariablesCore.CustomerInfo.CustomerID, messageId, targetAction, changeSource);
        }
      }
      catch (Exception ex) {
        Log(ex);
      }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetCustomerMessages(string customerId) {

      var rc = new ResultContainer(false);
      if (rc.SessionExpired) return rc;
      try {
        using (var cust = new Customers(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
          var customerMessages = cust.GetWebCustomerMessages(customerId);
          rc.Data = customerMessages;

          if (customerMessages == null || customerMessages.Count <= 0)
            return rc;
          if (customerMessages.Count == 1)
            SystemLog.WriteAccessLog("Showing messages page", "Showing 1 pending message");
          else
            SystemLog.WriteAccessLog("Showing messages page", "Showing " + customerMessages.Count + " pending messages");
        }
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetMessageNotification() {
      var rc = new ResultContainer(false);
      if (rc.SessionExpired) return rc;
      try {
        using (var sp = new SystemParameters(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
          var data = sp.GetSiteMessage(SessionVariablesCore.WebTarget, SessionVariablesCore.SiteCode);
          rc.Data = data;
        }
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetMessageNotificationBySport(string sport, string subSport) {
      var rc = new ResultContainer(false);
      if (rc.SessionExpired) return rc;
      try {
        using (var sp = new SystemParameters(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
          var data = sp.GetSiteMessageBySport(SessionVariablesCore.CustomerInfo.CustomerID, sport, subSport, SessionVariablesCore.WebTarget, SessionVariablesCore.SiteCode);
          rc.Data = data;
        }
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }
  }
}