﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using SIDWebV2.HelperClasses;
using SIDWebV2.Models;
using SIDWebLibraries.HelperClasses;
using LineOffering = SIDLibraries.BusinessLayer.LineOffering;
using LineOfferingSIDWeb = SIDWebV2.HelperClasses.LineOffering;
using SystemLog = SIDWebLibraries.HelperClasses.SystemLog;

namespace SIDWebV2.Services {
  /// <summary>
  /// Summary description for AppService
  /// </summary>
  [ScriptService]
  [WebService(Namespace = "http://tempuri.org/")]
  [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
  [System.ComponentModel.ToolboxItem(false)]
  public class SportsAndContestsService : CoreService {

    private class GameLinesAndLimits {
      public List<spGLWebGetGameOfferings_Result> GameLines { get; set; }
      public List<spGLGetSportLimitsDetail_Result> SportLimits { get; set; }
    }

    private class TeaserGameLines : GameLinesAndLimits {
      public int SportSubTypeId { get; set; }
      public List<spGLGetGamePeriod_Result> Periods { get; set; }
      public spGLGetGamePeriod_Result ActivePeriod { get; set; }
      public string WagerType { get; set; }
      public string PeriodDesc { get; set; }
      public string SportType { get; set; }
      public string SportSubType { get; set; }
      public string GroupValue { get; set; }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetLinkedSportsAndContests(bool getInactiveSports) {
      var rc = new ResultContainer();
      try {
        if (rc.SessionExpired) return rc;
        using (var spt = new SportTypes(InstanceModuleInfo))
          rc.Data = SessionVariables.AvailableSportsAndContests = spt.GetLinkedSportsAndContests(SessionVariablesCore.CustomerInfo.CustomerID, SessionVariablesCore.CustomerInfo.Store, SessionVariablesCore.ServerDateTime, SessionVariablesCore.WagerType.Name == WagerType.STRAIGHTBETNAME, SessionVariablesCore.WagerType.Name != WagerType.STRAIGHTBETNAME, getInactiveSports);
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public ResultContainer GetActiveTeasers() {
      var rc = new ResultContainer();
      try {
        if (!SessionVariablesCore.IsSessionValid()) return rc;
        var lo = new LineOffering(InstanceModuleInfo);

        var availableTeasers = lo.GetAvailableTeasers(SessionVariablesCore.CustomerInfo.CustomerID);

        var result = (from at in availableTeasers
                      group at by new { at.Category } into grp
                      select new SportTypeModel.SportTypeInfo { SportSubTypeId = 0, SportType = grp.Key.Category, Active = 0 }).ToList();

        foreach (var sti in result) {
          sti.Sub = (from at in availableTeasers
                     where at.Category == sti.SportType
                     select new SportTypeModel.SportTypeInfo { SportSubTypeId = 0, SportType = at.TeaserName.Trim() }).ToList();
        }
        rc.Data = result;
        SystemLog.WriteAccessLog("Teasers loaded", string.Empty);
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public ResultContainer GetContestantLine(int contestNum, int contestantNum, string custProfile, string store, string customerId) {
      var rc = new ResultContainer();
      try {
        spCnWebGetContestantOfferings_Result newContestant;

        using (var cn = new Contests(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
          newContestant = cn.GetWebContestantOffering(store, customerId, 0, SessionVariablesCore.ServerDateTime.ToString("yyyy-MM-dd"), contestNum, contestantNum).FirstOrDefault();

          if (newContestant == null)
            return rc;
        }
        rc.Data = newContestant;
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public ResultContainer GetGameLine(int gameNum, int periodNumber, string sportType, string sportSubType, string custProfile, string wagerType, string store, string customerId) {
      throw new NotImplementedException();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public ResultContainer LoadGameLines(string sportType, string sportSubType, string wagerType, int? periodNumber, int? gameNum, int hoursAdjustment) {
      var rc = new ResultContainer();
      try {
        if (rc.SessionExpired) return rc;

        if (SessionVariables.AvailableSportsAndContests == null || SessionVariables.AvailableSportsAndContests.Count <= 0) return rc;

        SystemLog.WriteAccessLog("Sport Selected", sportType + " - " + sportSubType);
        rc.Data = _GetGameLines(sportType, sportSubType, wagerType, hoursAdjustment, periodNumber);
        return rc;
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
        return rc;
      }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public ResultContainer LoadSpotLightGameLines(string wagerType, int? gameNum, int hoursAdjustment) {
      var rc = new ResultContainer();
      try {
        if (rc.SessionExpired) return rc;

        if (SessionVariables.AvailableSportsAndContests != null && SessionVariables.AvailableSportsAndContests.Count > 0) {
          return rc;
        }
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public ResultContainer LoadContestsLines(string contestType, string contestType2, string contestType3) {
      var rc = new ResultContainer();
      try {
        if (rc.SessionExpired) return rc;
        if (SessionVariables.AvailableSportsAndContests == null || SessionVariables.AvailableSportsAndContests.Count <= 0) return rc;
        rc.Data = _GetContestsLines(contestType, contestType2);
        SystemLog.WriteAccessLog("Contest loaded", contestType + " - " + contestType2 + " - " + contestType3);
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public ResultContainer LoadContestsLinesByCorrelation(string correlation) {
      var rc = new ResultContainer();
      try {
        if (rc.SessionExpired) return rc;
        if (SessionVariables.AvailableSportsAndContests == null || SessionVariables.AvailableSportsAndContests.Count <= 0) return rc;
        rc.Data = _GetContestsLines(correlation);
        SystemLog.WriteAccessLog("Correlated Contest loaded", correlation);
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public ResultContainer LoadSpotlightContests(int? contestNum) {
      var rc = new ResultContainer();
      try {
        if (rc.SessionExpired) return rc;
        if (SessionVariables.AvailableSportsAndContests == null || SessionVariables.AvailableSportsAndContests.Count <= 0) return rc;
        rc.Data = _GetSpotlightContests(contestNum);
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public ResultContainer LoadTeaserGames(string teaserName, string periodWagerCutoff, int hoursAdjustment, bool isOpenTeaser) {
      var rc = new ResultContainer();
      try {
        var result = new List<TeaserGameLines>();
        if (SessionVariablesCore.CustomerInfo != null) {
          SessionVariablesCore.TeaserName = teaserName;
          var sportSpecs = _GetTeaserSportSpecs(teaserName);

          if (SessionVariablesCore.TssAry == null || !SessionVariablesCore.TssAry.Any()) {
            var ex = new Exception("Error loading teaser sports specs on Service: LoadTeaserGames (" + teaserName + ")");
            Log(ex);
            rc.SetError(ex.Message);
            return rc;
          }

          foreach (var sportSpec in sportSpecs) {
            var gameLines = _GetGameLines(sportSpec.SportType, sportSpec.SportSubType, SessionVariablesCore.WagerType.Name, hoursAdjustment, null);
            if (!gameLines.GameLines.Any() || !gameLines.SportLimits.Any()) continue;
            result.Add(new TeaserGameLines {
              Periods = new List<spGLGetGamePeriod_Result> { new spGLGetGamePeriod_Result { PeriodNumber = sportSpec.DefaultPeriodNumber, PeriodDescription = "Game" } },
              SportType = sportSpec.SportSubTypeId.SportType,
              SportSubType = sportSpec.SportSubTypeId.SportSubType,
              SportSubTypeId = sportSpec.SportSubTypeId.SportSubTypeId,
              GroupValue = "",
              WagerType = SessionVariablesCore.WagerType.Name,
              GameLines = gameLines.GameLines,
              SportLimits = gameLines.SportLimits
            });
          }
          rc.Data = new { TeaserGameLines = result };
          SystemLog.WriteAccessLog(isOpenTeaser ? "Selected Open Teaser" : "Selected Teaser", SessionVariablesCore.TeaserName);
        }
        else rc.Code = ResultContainer.ResultCode.SessionExpired;
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public ResultContainer AppendGamesAndContestsLines(Object newItem, string eventOffering, string wagerType) {
      var rc = new ResultContainer();
      try {
        var customerInfo = SessionVariablesCore.CustomerInfo;
        var item = (IDictionary)newItem;
        if (customerInfo == null || customerInfo.Store == null || SessionVariables.GamesAndContestsLines == null) return new ResultContainer();
        var lineChange = DictionaryLastLineChange(item);
        if (lineChange == null) return new ResultContainer();

        //LGR MW
        var wager = SessionVariables.Ticket.GetWager(0);

        switch (eventOffering) {
          case "C":
            spCnWebGetContestOfferings_Result contestantLine;
            using (var cn = new Contests(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
              contestantLine = cn.GetWebContestOffering(lineChange.ContestType, lineChange.Store, SessionVariablesCore.ServerDateTime,
                  lineChange.GameNum ?? 0, lineChange.ContestantNum ?? 0, SessionVariablesCore.CustomerInfo.CustomerID, SessionVariablesCore.ServerDateTime.ToString("yyyy-MM-dd hh:mm:ss"));

              var contestItem = (from c in SessionVariables.GamesAndContestsLines
                                 where c.EventOffering == "C"
                                       && c.Contest.ContestNum == lineChange.GameNum
                                 select c.Contest).FirstOrDefault();
              if (contestItem != null) return _RemoveContestWagerItem(wager, lineChange.GameNum ?? 0, lineChange.ContestantNum ?? 0, contestantLine);
              if (contestantLine != null) SessionVariables.GamesAndContestsLines.Add(new LineOfferingContainer(contestantLine, eventOffering));

            }
            return new ResultContainer(new { NLoo = contestantLine }, ResultContainer.ResultCode.Success, "");
          case "G":
            spGLWebGetGameOfferings_Result nloo;
            using (var gl = new GamesAndLines(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
              nloo = gl.GetWebGameOffering(SessionVariablesCore.CustomerInfo.CustomerID, lineChange.GameNum ?? 0,
                  lineChange.SportType, lineChange.SportSubType, wagerType, lineChange.PeriodNumber ?? 0,
                  SessionVariablesCore.ServerDateTime, lineChange.Store, lineChange.CustProfile, 0);

              if (nloo == null) return new ResultContainer(ResultContainer.ResultCode.Warning, "Line is not available anymore.");


              double spreadAdj, totalsAdj;
              SetTeaserPoints(lineChange.SportType, lineChange.SportSubType, out spreadAdj, out totalsAdj);
              if (wagerType == WagerType.GetFromCode(WagerType.TEASER).Name && SessionVariablesCore.TssAry == null) {
                return new ResultContainer(ResultContainer.ResultCode.Warning, "Line is not available anymore.");
              }

              nloo.UsePuckLineFlag = (SessionVariablesCore.UsePuckLineFlag ? "Y" : "N") == "Y";

              if (SessionVariablesCore.ShowSpread) nloo.SpreadAdj = spreadAdj;
              else {
                nloo.Spread = nloo.Spread1 = nloo.Spread2 = null;
                nloo.SpreadAdj1 = nloo.SpreadAdj2 = null;
                nloo.SpreadDecimal1 = nloo.SpreadDecimal2 = null;
                nloo.SpreadDenominator1 = nloo.SpreadDenominator2 = nloo.SpreadNumerator1 = nloo.SpreadNumerator2 = null;
              }
              if (SessionVariablesCore.ShowTotals) nloo.TotalsAdj = totalsAdj;
              else {
                nloo.TotalPoints = nloo.TotalPoints1 = nloo.TotalPoints2 = null;
                nloo.TotalsAdj = nloo.TtlPtsAdj1 = nloo.TtlPtsAdj2 = null;
                nloo.TtlPointsDecimal1 = nloo.TtlPointsDecimal1 = null;
                nloo.TtlPointsDenominator1 = nloo.TtlPointsDenominator2 = nloo.TtlPointsNumerator1 = nloo.TtlPointsNumerator2 = null;
              }
              if (!SessionVariablesCore.ShowMoneyLine) {
                nloo.MoneyLine1 = nloo.MoneyLine2 = null;
                nloo.MoneyLineDecimal1 = nloo.MoneyLineDecimal2 = null;
                nloo.MoneyLineNumerator1 = nloo.MoneyLineNumerator2 = nloo.MoneyLineDenominator1 = nloo.MoneyLineDenominator2 = null;
              }

              nloo.UseVigDiscount = (SessionVariablesCore.WagerType.Name == WagerType.STRAIGHTBETNAME || SessionVariablesCore.VigDiscountForAllWagerTypes);
              nloo.VigDiscountAry = SessionVariablesCore.VigDiscountAry;
              nloo.VigDiscountPercent = SessionVariablesCore.VigDiscountPercent;

              nloo.EasternLineFlag = SessionVariablesCore.EasternLineFlag ? "Y" : "N";

              nloo.EasternLineConversionAry = SessionVariablesCore.EasternLineConversionAry;
              nloo.Initialize();

              //Session cleaner
              SessionVariables.GamesAndContestsLines.RemoveAll(w => w.GameLine == null && w.Contest == null);

              var lineContainer = new LineOfferingContainer(nloo, eventOffering);
              var looAryItem = (from g in SessionVariables.GamesAndContestsLines
                                where g.EventOffering == "G"
                                      && g.GameLine.GameNum == lineContainer.GameLine.GameNum
                                      && g.GameLine.PeriodNumber == lineContainer.GameLine.PeriodNumber
                                select g).OrderByDescending(c => c.GameLine.LineSeq).FirstOrDefault();
              if (looAryItem != null) {
                //Estas dos lineas siguientes quitan el objeto de GamesAndContestsLines y ponen el nuevo. *IMPORTANTE*
                SessionVariables.GamesAndContestsLines.Remove(looAryItem);
                SessionVariables.GamesAndContestsLines.Add(lineContainer);

                return _SpotWagerItemIfExists(wager, lineChange.GameNum ?? 0, Convert.ToInt32(lineChange.PeriodNumber), lineChange.WagerType, lineChange.Team1Or2 ?? 0, nloo);
              }
              SessionVariables.GamesAndContestsLines.Add(lineContainer);
            }
            return new ResultContainer(new { NLoo = nloo }, ResultContainer.ResultCode.Success, "");
        }
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return new ResultContainer();
    }

    private ResultContainer _SpotWagerItemIfExists(WebWager wager, int gameNum, int periodNumber, string wagerType, int team1OrTeam2, spGLWebGetGameOfferings_Result nLoo) {
      var rc = new ResultContainer();
      if (rc.Code == ResultContainer.ResultCode.SessionExpired || SessionVariables.Ticket == null ||
                wager.WebGameWagerItems == null) {
        rc.Data = new { NLoo = nLoo };
        return rc;
      }
      var wagerItemToUpdate = (from w in wager.WebGameWagerItems
                               where w.ControlCode.Substring(0, 1) == wagerType &&
                                     w.Loo.GameNum == gameNum &&
                                     w.Loo.PeriodNumber == periodNumber
                               select w).ToList();
      if (wagerItemToUpdate.Any()) {
        foreach (var wi in wagerItemToUpdate) {
          WagerValidation.CheckGameLine(wi);
        }
      }
      var lineRemoved = false;
      switch (wagerType) {
        case WagerType.SPREAD:
          lineRemoved = nLoo.Spread == null || nLoo.SpreadAdj1 == null || nLoo.SpreadAdj2 == null;
          break;
        case WagerType.MONEYLINE:
          lineRemoved = nLoo.MoneyLine1 == null || nLoo.MoneyLine2 == null;
          break;
        case WagerType.TOTALPOINTS:
          lineRemoved = nLoo.TotalPoints == null || nLoo.TtlPtsAdj1 == null || nLoo.TtlPtsAdj2 == null;
          break;
        case WagerType.TEAMTOTALPOINTS:
          if (team1OrTeam2 == 1) lineRemoved = nLoo.Team1TotalPoints == null || nLoo.Team1TtlPtsAdj1 == null || nLoo.Team1TtlPtsAdj2 == null;
          else lineRemoved = nLoo.Team2TotalPoints == null || nLoo.Team2TtlPtsAdj1 == null || nLoo.Team2TtlPtsAdj2 == null;
          break;
      }

      rc.Data = new { NLoo = nLoo, wagerItems = wagerItemToUpdate, lineRemoved };
      return rc;
    }

    private ResultContainer _RemoveContestWagerItem(WebWager wager, int contestNumber, int contestantNumber, spCnWebGetContestOfferings_Result contestant) {
      var rc = new ResultContainer();
      if (rc.Code == ResultContainer.ResultCode.SessionExpired || SessionVariables.Ticket == null || wager.WebContestantWagerItems == null) {
        rc.Data = new { NLoo = contestant };
        return rc;
      }

      var wiToRemove = (from w in wager.WebContestantWagerItems
                        where w.Loo.ContestNum == contestNumber
                              && w.Loo.ContestantNum == contestantNumber
                        select w).FirstOrDefault();
      if (wiToRemove != null) {
        wager.WebContestantWagerItems.Remove(wiToRemove);
      }
      rc.Data = new { NLoo = contestant };
      return rc;
    }

    public static void SetTeaserPoints(string sportType, string sportSubType, out double spreadAdj, out double totalsAdj) {
      spreadAdj = totalsAdj = 0;
      SessionVariablesCore.ShowSpread = SessionVariablesCore.ShowTotals = SessionVariablesCore.ShowMoneyLine = true;
      try {
        if (SessionVariablesCore.WagerType.Name != WagerType.GetFromCode(WagerType.TEASER).Name) return;
      SessionVariablesCore.ShowMoneyLine = false;
        var tss = (from li in SessionVariablesCore.TssAry
                   where (li.SportSubTypeId.SportType ?? "").Trim() == (sportType ?? "").Trim()
                         && (li.SportSubTypeId.SportSubType ?? "").Trim() == (sportSubType ?? "").Trim()
                   select li).First();
        SessionVariablesCore.ShowSpread = tss.ShowSpread && tss.SpreadAdj > 0;
        SessionVariablesCore.ShowTotals = tss.ShowTotals && tss.TotalsAdj > 0;
        spreadAdj = tss.SpreadAdj;
        totalsAdj = tss.TotalsAdj;
      }
      catch (Exception ex) {
        SessionVariablesCore.ShowSpread = SessionVariablesCore.ShowTotals = false;
        Log(ex);
    }
    }

    private static GameLinesAndLimits _GetGameLines(string sportType, string sportSubType, string wagerType, int hoursAdjustment, int? periodNumber) {
      var customerInfo = SessionVariablesCore.CustomerInfo;

      if (customerInfo == null || customerInfo.CustomerID == null || SessionVariablesCore.WagerType == null)
        return null;
      var im = SessionVariablesCore.InstanceManager;
      List<spGLWebGetGameOfferings_Result> looAry;

      double spreadAdj, totalsAdj;

      SetTeaserPoints(sportType, sportSubType, out spreadAdj, out totalsAdj);
      if (SessionVariablesCore.WagerType.Name == WagerType.GetFromCode(WagerType.TEASER).Name && spreadAdj == 0 && totalsAdj == 0)
        return new GameLinesAndLimits { GameLines = new List<spGLWebGetGameOfferings_Result>(), SportLimits = new List<spGLGetSportLimitsDetail_Result>() };

      List<spGLGetSportLimitsDetail_Result> sportLimits;
      using (var gl = new GamesAndLines(im.InstanceModuleInfo)) {

        var wtCode = WagerType.GetFromName(wagerType);

        sportLimits = gl.GetSportLimitsDetail(customerInfo.CustomerID, sportType, sportSubType, wtCode.Code, customerInfo.Store, "I", customerInfo.WagerLimit ?? 0, wtCode.Code == "P" ? (customerInfo.ParlayMaxBet ?? 0) : wtCode.Code == "T" ? (customerInfo.TeaserMaxBet ?? 0) : 0, wtCode.Code == "P" ? (customerInfo.EnforceParlayMaxBetFlag == "Y") : wtCode.Code == "T" && customerInfo.EnforceTeaserMaxBetFlag == "Y");
        looAry = gl.GetWebGameOfferings(customerInfo.CustomerID, sportType, sportSubType, wagerType, customerInfo.Store, hoursAdjustment, periodNumber);

        foreach (var loo in looAry) {
          loo.UsePuckLineFlag = (SessionVariablesCore.UsePuckLineFlag ? "Y" : "N") == "Y";

          if (SessionVariablesCore.ShowSpread) loo.SpreadAdj = spreadAdj;
          else {
            loo.Spread = loo.Spread1 = loo.Spread2 = null;
            loo.SpreadAdj1 = loo.SpreadAdj2 = null;
            loo.SpreadDecimal1 = loo.SpreadDecimal2 = null;
            loo.SpreadDenominator1 = loo.SpreadDenominator2 = loo.SpreadNumerator1 = loo.SpreadNumerator2 = null;
          }
          if (SessionVariablesCore.ShowTotals) loo.TotalsAdj = totalsAdj;
          else {
            loo.TotalPoints = loo.TotalPoints1 = loo.TotalPoints2 = null;
            loo.TotalsAdj = loo.TtlPtsAdj1 = loo.TtlPtsAdj2 = null;
            loo.TtlPointsDecimal1 = loo.TtlPointsDecimal1 = null;
            loo.TtlPointsDenominator1 = loo.TtlPointsDenominator2 = loo.TtlPointsNumerator1 = loo.TtlPointsNumerator2 = null;
          }
          if (!SessionVariablesCore.ShowMoneyLine) {
            loo.MoneyLine1 = loo.MoneyLine2 = null;
            loo.MoneyLineDecimal1 = loo.MoneyLineDecimal2 = null;
            loo.MoneyLineNumerator1 = loo.MoneyLineNumerator2 = loo.MoneyLineDenominator1 = loo.MoneyLineDenominator2 = null;
          }
          loo.UseVigDiscount = (SessionVariablesCore.WagerType.Name == WagerType.STRAIGHTBETNAME || SessionVariablesCore.VigDiscountForAllWagerTypes);
          //loo.VigDiscountAry = SessionVariablesCore.VigDiscountAry;
          //loo.VigDiscountAry = SessionVariablesCore.VigDiscountAry.Where(v => v.SportType == sportType && v.SportSubType == sportSubType && v.PeriodNumber == periodNumber).ToList();
          loo.VigDiscountAry = SessionVariablesCore.VigDiscountAry.Where(v => (v.SportType ?? "").Trim() == (sportType ?? "").Trim() && (v.SportSubType ?? "").Trim() == (sportSubType ?? "").Trim() && (v.PeriodNumber == periodNumber || periodNumber == null)).ToList();
          loo.VigDiscountPercent = SessionVariablesCore.VigDiscountPercent;

          loo.EasternLineFlag = SessionVariablesCore.EasternLineFlag ? "Y" : "N";

          loo.EasternLineConversionAry = SessionVariablesCore.EasternLineConversionAry;
          loo.Initialize();
        }

        if (SessionVariables.GamesAndContestsLines != null)
          _AppendGameOfferings(looAry);
        else
          SessionVariables.GamesAndContestsLines = (from l in looAry
                                                    select new LineOfferingContainer(l, "G")).ToList();
      }

      return new GameLinesAndLimits { GameLines = looAry, SportLimits = sportLimits };
    }

    /*
    private static List<spGLWebGetGameOfferings_Result> _GetSpotlightGameLines(string wagerType, int hoursAdjustment, int? gameNum = null) {
      var customerInfo = SessionVariablesCore.CustomerInfo;

      if (customerInfo == null || customerInfo.CustomerID == null || SessionVariablesCore.WagerType == null)
        return null;
      var im = SessionVariablesCore.InstanceManager;
      List<spGLWebGetGameOfferings_Result> looAry;

      SessionVariablesCore.ShowSpread = true;
      SessionVariablesCore.ShowMoneyLine = true;
      SessionVariablesCore.ShowTotals = true;
      double? spreadAdj = 0;
      double? totalsAdj = 0;

      if (SessionVariablesCore.WagerType.Name == WagerType.GetFromCode(WagerType.TEASER).Name) {
        if (SessionVariablesCore.TssAry != null) {
          SessionVariablesCore.ShowSpread = (from li in SessionVariablesCore.TssAry
                                             select li.ShowSpread).FirstOrDefault();
          SessionVariablesCore.ShowTotals = (from li in SessionVariablesCore.TssAry
                                             select li.ShowTotals).FirstOrDefault();
          SessionVariablesCore.ShowMoneyLine = false;

          spreadAdj = (from li in SessionVariablesCore.TssAry
                       select li.SpreadAdj).FirstOrDefault();
          totalsAdj = (from li in SessionVariablesCore.TssAry
                       select li.TotalsAdj).FirstOrDefault();
        }
      }
      else
        SessionVariablesCore.TssAry = null;

      using (var gl = new GamesAndLines(im.InstanceModuleInfo)) {
        looAry = gl.GetWebSpotlightGameOfferings(customerInfo.CustomerID, wagerType, customerInfo.Store, hoursAdjustment, gameNum);

        foreach (var loo in looAry) {
          loo.UsePuckLineFlag = (SessionVariablesCore.UsePuckLineFlag ? "Y" : "N") == "Y";
          if (SessionVariablesCore.ShowSpread) loo.SpreadAdj = spreadAdj;
          if (SessionVariablesCore.ShowTotals) loo.TotalsAdj = totalsAdj;
          if (!SessionVariablesCore.ShowMoneyLine) {
            loo.MoneyLine1 = loo.MoneyLine2 = null;
            loo.MoneyLineDecimal1 = loo.MoneyLineDecimal2 = null;
            loo.MoneyLineNumerator1 = loo.MoneyLineNumerator2 = loo.MoneyLineDenominator1 = loo.MoneyLineDenominator2 = null;
          }
          loo.UseVigDiscount = SessionVariablesCore.WagerType.Name == WagerType.STRAIGHTBETNAME;
          loo.VigDiscountAry = SessionVariablesCore.VigDiscountAry;
          loo.VigDiscountPercent = SessionVariablesCore.VigDiscountPercent;

          if (SessionVariablesCore.EasternLineFlag)
            loo.EasternLineFlag = SessionVariablesCore.EasternLineFlag ? "Y" : "N";

          loo.EasternLineConversionAry = SessionVariablesCore.EasternLineConversionAry;
          loo.Initialize();
        }
        if (SessionVariables.GamesAndContestsLines != null)
          _AppendGameOfferings(looAry);
        else
          SessionVariables.GamesAndContestsLines = (from l in looAry
                                                    select new LineOfferingContainer(l, "G")).ToList();
      }
      return looAry;
    }
    */

    private static void _AppendGameOfferings(IEnumerable<spGLWebGetGameOfferings_Result> looAry) {
      foreach (var res in looAry) {
        var gameNumber = res.GameNum;
        var periodNumber = res.PeriodNumber;
        var custProfile = res.CustProfile;
        SessionVariables.GamesAndContestsLines.RemoveAll(w => w.GameLine == null && w.Contest == null);
        var sessionLoo = (from loo in SessionVariables.GamesAndContestsLines
                          where loo.EventOffering == "G"
                                    && loo.GameLine.GameNum == gameNumber
                                    && loo.GameLine.PeriodNumber == periodNumber
                                    && loo.GameLine.CustProfile.Trim() == custProfile.Trim()
                          select loo).FirstOrDefault();
        if (sessionLoo != null)
          SessionVariables.GamesAndContestsLines.Remove(sessionLoo);
        SessionVariables.GamesAndContestsLines.Add(new LineOfferingContainer(res, "G"));
      }
    }

    private static List<spCnWebGetContestOfferings_Result> _GetContestsLines(string contestType, string contestType2) {
      var customerInfo = SessionVariablesCore.CustomerInfo;

      if (customerInfo == null || customerInfo.Store == null)
        return null;

      var im = SessionVariablesCore.InstanceManager;
      List<spCnWebGetContestOfferings_Result> contests;

      using (var cn = new Contests(im.InstanceModuleInfo)) {
        contests = cn.GetWebContestOfferings(contestType, contestType2, customerInfo.Store, SessionVariablesCore.ServerDateTime.ToString("yyyy-MM-dd hh:mm:ss"), customerInfo.CustomerID, SessionVariablesCore.ServerDateTime.ToString("yyyy-MM-dd"));
        if (SessionVariables.GamesAndContestsLines != null)
          AppendContestOfferings(contests);
        else
          SessionVariables.GamesAndContestsLines = (from c in contests
                                                    select new LineOfferingContainer(c, "C")).ToList();
      }
      return contests;
    }

    private static List<spCnWebGetContestOfferings_Result> _GetContestsLines(string correlation) {
      var customerInfo = SessionVariablesCore.CustomerInfo;

      if (customerInfo == null || customerInfo.Store == null)
        return null;

      var im = SessionVariablesCore.InstanceManager;
      List<spCnWebGetContestOfferings_Result> contests;

      using (var cn = new Contests(im.InstanceModuleInfo)) {
        contests = cn.GetWebContestOfferingsByCorrelationId(correlation, customerInfo.Store, SessionVariablesCore.ServerDateTime.ToString("yyyy-MM-dd hh:mm:ss"), customerInfo.CustomerID, SessionVariablesCore.ServerDateTime.ToString("yyyy-MM-dd"));
        if (SessionVariables.GamesAndContestsLines != null)
          AppendContestOfferings(contests);
        else
          SessionVariables.GamesAndContestsLines = (from c in contests
                                                    select new LineOfferingContainer(c, "C")).ToList();
      }
      return contests;
    }

    private static List<spCnWebGetContestOfferings_Result> _GetSpotlightContests(int? contestNum) {
      var customerInfo = SessionVariablesCore.CustomerInfo;

      if (customerInfo == null || customerInfo.Store == null)
        return null;

      var im = SessionVariablesCore.InstanceManager;
      List<spCnWebGetContestOfferings_Result> contests;

      using (var cn = new Contests(im.InstanceModuleInfo)) {
        contests = cn.GetWebSpotlightContestOfferings(customerInfo.Store, SessionVariablesCore.ServerDateTime.ToString("yyyy-MM-dd"), customerInfo.CustomerID, contestNum);
        if (SessionVariables.GamesAndContestsLines != null)
          AppendContestOfferings(contests);
        else
          SessionVariables.GamesAndContestsLines = (from c in contests
                                                    select new LineOfferingContainer(c, "C")).ToList();
      }
      return contests;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod]
    public static void AppendContestOfferings(IEnumerable<spCnWebGetContestOfferings_Result> contests) {
      foreach (var res in contests) {
        var contest = (from c in SessionVariables.GamesAndContestsLines
                       where c.EventOffering == "C"
                                && c.Contest.ContestNum == res.ContestNum
                       select c).FirstOrDefault();
        if (contest != null)
          SessionVariables.GamesAndContestsLines.Remove(contest);
        SessionVariables.GamesAndContestsLines.Add(new LineOfferingContainer(res, "C"));
      }
    }

    private IEnumerable<LineOfferingWeb.TeaserSportSpecs> _GetTeaserSportSpecs(string teaserName) {

      using (var lo = new LineOffering(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {

      var sportsList = new List<LineOfferingWeb.TeaserSportSpecs>();

      var teaserSpecs = lo.GetTeaserSpecs(teaserName);

      var teaserSpecsObj = new LineOfferingWeb.TeaserSpecs();

      if (teaserSpecs != null) {
        teaserSpecsObj.BaseOfferingFlag = teaserSpecs.BaseOfferingFlag;
        teaserSpecsObj.CallUnitOnlyFlag = teaserSpecs.CallUnitOnlyFlag;
        teaserSpecsObj.Description = teaserSpecs.Description.Trim();
        teaserSpecsObj.InternetOnlyFlag = teaserSpecs.InternetOnlyFlag;
        teaserSpecsObj.MaxPicks = teaserSpecs.MaxPicks ?? 8;
        teaserSpecsObj.MinPicks = teaserSpecs.MinPicks ?? 8;
        teaserSpecsObj.SuspendFlag = teaserSpecs.SuspendFlag;
        teaserSpecsObj.TeaserName = teaserSpecs.TeaserName.Trim();
        teaserSpecsObj.TeaserTies = teaserSpecs.TeaserTies ?? 0;
      }
      SessionVariablesCore.TeaserSpecs = teaserSpecsObj;
      //SessionVariables.WagerType = WagerType.GetFromName(WagerType.TEASER);

      var holdSportSubTypeId = 0;

      foreach (var res in lo.GetTeaserSports(teaserName)) {
        LineOfferingWeb.TeaserSportSpecs sportTeaserSpec;
        if (holdSportSubTypeId != res.SportSubTypeId) {
          sportTeaserSpec = new LineOfferingWeb.TeaserSportSpecs {
            SportType = res.SportType.Trim(),
            SportSubType = res.SportSubType.Trim(),
            SportSubTypeId = {
              SportType = res.SportType,
              SportSubType = res.SportSubType,
              SportSubTypeId = res.SportSubTypeId
            },
            DefaultPeriodNumber = 0
          };
          switch (res.WagerType) {
            case WagerType.SPREAD:
              sportTeaserSpec.ShowSpread = true;
              sportTeaserSpec.SpreadAdj = res.Points ?? 0;
              break;
            case WagerType.TOTALPOINTS:
              sportTeaserSpec.ShowTotals = true;
              sportTeaserSpec.TotalsAdj = res.Points ?? 0;
              break;
          }
          sportsList.Add(sportTeaserSpec);
        }
        else {
          sportTeaserSpec = (from t in sportsList where t.SportSubTypeId.SportSubTypeId == res.SportSubTypeId select t).First();
          var itemIdx = sportsList.IndexOf(sportTeaserSpec);
          switch (res.WagerType) {
            case WagerType.SPREAD:
              sportTeaserSpec.ShowSpread = true;
              sportTeaserSpec.SpreadAdj = res.Points ?? 0;
              break;
            case WagerType.TOTALPOINTS:
              sportTeaserSpec.ShowTotals = true;
              sportTeaserSpec.TotalsAdj = res.Points ?? 0;
              break;
          }
          sportsList[itemIdx] = sportTeaserSpec;
        }
        holdSportSubTypeId = res.SportSubTypeId;
      }
      SessionVariablesCore.TeaserName = teaserName;
      SessionVariablesCore.TssAry = sportsList;
      return sportsList;
    }
    }

    private spGLGetLastLinesChange_Result DictionaryLastLineChange(IDictionary item) {
      return new spGLGetLastLinesChange_Result {
        ContestType = Convert.ToString(item["ContestType"]),
        Store = Convert.ToString(item["Store"]),
        GameNum = Convert.ToInt32(item["GameNum"]),
        ContestantNum = Convert.ToInt32(item["ContestantNum"]),
        WagerType = Convert.ToString(item["WagerType"]),
        Team1Or2 = Convert.ToInt32(item["Team1Or2"]),
        SportType = Convert.ToString(item["SportType"]),
        SportSubType = Convert.ToString(item["SportSubType"]),
        PeriodNumber = Convert.ToInt32(item["PeriodNumber"]),
        CustProfile = Convert.ToString(item["CustProfile"])
      };
    }


  }
}