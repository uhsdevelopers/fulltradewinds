﻿using System.ComponentModel;
using System.Web.Script.Services;
using System.Web.Services;
using InstanceManager.BusinessLayer;
using SIDWebLibraries.HelperClasses;
namespace SIDWebV2.Services {
  /// <summary>
  /// Summary description for LoginManager
  /// </summary>
  [ScriptService]
  [WebService(Namespace = "http://tempuri.org/")]
  [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
  [ToolboxItem(false)]
  public class SystemService : CoreService {
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetSystemParameters() {
      var rc = new ResultContainer();
      if (rc.SessionExpired) return rc;
      rc.Data = SessionVariablesCore.InstanceManager.InstanceModuleInfo.Parameters;
      return rc;
    }


  }
}