﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using SIDWebV2.ColchianServiceRef;
//using SIDWebV2.LiveBettingServiceRef;
using SIDWebV2.LiveDealerServiceRef;
using SIDWebLibraries.HelperClasses;
using SV = SIDWebV2.HelperClasses.SessionVariables;
using System.Security.Cryptography;
using System.Text;
using SystemLog = SIDWebLibraries.HelperClasses.SystemLog;

namespace SIDWebV2.Services {
  /// <summary>
  /// Summary description for AppService
  /// </summary>
  [ScriptService]
  [WebService(Namespace = "http://tempuri.org/")]
  [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
  [ToolboxItem(false)]
  public class CustomerService : CoreService {

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetCustomerInfo(bool resetLastSession) {
      var rc = new ResultContainer(resetLastSession);
      if (rc.SessionExpired) return rc;
      try {
        using (var cust = new Customers(InstanceModuleInfo)) {
          var currencyRate = SessionVariablesCore.CustomerInfo.ExchangeRate;
          SessionVariablesCore.CustomerInfo = cust.GetCustomerInfo(SessionVariablesCore.CustomerInfo.CustomerID, SessionVariablesCore.CustomerInfo.Password, SessionVariablesCore.WebTarget, SessionVariablesCore.WebTarget, SessionVariablesCore.ClientIpAddress);
          SessionVariablesCore.CustomerInfo.ExchangeRate = currencyRate;

          SessionVariablesCore.CustomerSettings = cust.GetWebCustomerSettings(SessionVariablesCore.CustomerInfo.CustomerID);
          SessionVariablesCore.CustomerInfo.PriorityMessages = cust.GetWebCustomerPriorityMessages(SessionVariablesCore.CustomerInfo.CustomerID);
          var lastMsg = SessionVariablesCore.CustomerInfo.PriorityMessages != null && SessionVariablesCore.CustomerInfo.PriorityMessages.Any() ? SessionVariablesCore.CustomerInfo.PriorityMessages.Max(m => m.CustomerMessageId) : 0;
          if (lastMsg <= (SessionVariablesCore.LastPriorityMsgShown ?? 0))
            if (SessionVariablesCore.CustomerInfo.PriorityMessages != null) SessionVariablesCore.CustomerInfo.PriorityMessages.Clear();
            else SessionVariablesCore.CustomerInfo.PriorityMessages = new List<spCstWebGetCustomerMessages_Result>();
          else
            SessionVariablesCore.LastPriorityMsgShown = lastMsg;

          var balance = cust.GetCustomerBalances(SessionVariablesCore.CustomerInfo.CustomerID);
          var restrictions = cust.GetRestrictionsByCustomer(SessionVariablesCore.CustomerInfo.CustomerID);
          SessionVariablesCore.VigDiscountForAllWagerTypes = restrictions.Any(r => r.Code == "VIGDISCALL");
          SessionVariablesCore.OfferHighLimits = restrictions.Any(r => r.Code == "HIGHLIMITS");
          SessionVariablesCore.ChangeLineInCustomerFavor = restrictions.Any(r => r.Code == "CSTFVRLINE");
          var circleOverringRestriction = restrictions.FirstOrDefault(r => r.Code == "CIRCLELOVR");
          SessionVariablesCore.IsMover = restrictions.Any(r => r.Code == "MOVER");
          SessionVariablesCore.AutoAccept = restrictions.Any(r => r.Code == "AUTOLINECH");
          SessionVariablesCore.CircleOverrindingValue = circleOverringRestriction != null && double.Parse(circleOverringRestriction.ParamValue) > 0 ? double.Parse(circleOverringRestriction.ParamValue) * 100 : 0;
          WebCustomerBalance.LoadCustomerAvailableBalance(SessionVariablesCore.CustomerInfo);
          balance.AvailableBalance = SessionVariablesCore.AvailableBalance;
          balance.CreditLimit = SessionVariablesCore.CustomerInfo.CreditLimit;
          HelperClasses.WagerValidation.GetMinimumWagerAmt();
          rc.Data = new { SessionVariablesCore.CustomerInfo, SessionVariablesCore.CustomerSettings, CustomerBalances = balance, CustomerRestrictions = restrictions, MinimumWagerAmt = SIDWebLibraries.HelperClasses.SessionVariablesCore.MinimumWagerAmt };
          if (SessionVariablesCore.CustomerDefaultPageShown) return rc;
          SystemLog.WriteAccessLog("Showing Default Page", SessionVariablesCore.CustomerInfo.WebHomePage);
          SessionVariablesCore.CustomerDefaultPageShown = true;
          SetCustomerLimits();
        }
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void LogOut() {

      var sessionValidator = new UniqueSessionValidator(SessionVariablesCore.InstanceManager.InstanceModuleInfo);
      var sv = sessionValidator.CheckUniqueSessionId(new UniqueSessionValidator.SessionData(SessionVariablesCore.UniqueKey, SessionVariablesCore.CustomerInfo.CustomerID));
      SystemLog.WriteAccessLog(sv != null ? "Second Logout Succesful" : "Logout Successful", null);

      if (SessionVariablesCore.InstanceManager != null)
        SessionVariablesCore.InstanceManager.DeleteAppInUse(WebConfigManager.GetApplicationId());

      HttpContext.Current.Session.Clear();
      HttpContext.Current.Session.Abandon(); //Session_OnEnd event is triggered
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void SaveCustomerSettings(string webLanguage, string webHomePage, string favoriteSport, string favoriteSportSubType, string favoriteTeamId, string baseballAction, Boolean? webRememberPassword, string custTimeZone) {

      try {
        WriteToLogCustomerSettingsChange(webLanguage, webHomePage, favoriteSport, favoriteSportSubType, favoriteTeamId, baseballAction, webRememberPassword ?? false, custTimeZone);

        SessionVariablesCore.CustomerInfo.WebHomePage = webHomePage;
        SessionVariablesCore.CustomerInfo.FavoriteSport = favoriteSport;
        SessionVariablesCore.CustomerInfo.FavoriteSportSubType = favoriteSportSubType;
        SessionVariablesCore.CustomerInfo.FavoriteTeamID = favoriteTeamId;
        SessionVariablesCore.CustomerInfo.BaseballAction = baseballAction;

        int tZ;
        int.TryParse(custTimeZone, out tZ);
        SessionVariablesCore.CustomerInfo.TimeZone = tZ;

        if (webRememberPassword != null) SessionVariablesCore.CustomerInfo.WebRememberPassword = webRememberPassword;

        if (SessionVariablesCore.CustomerInfo != null && webLanguage != SessionVariablesCore.CustomerInfo.WebLanguage) {
          if (String.IsNullOrEmpty(webLanguage)) webLanguage = "ENG";
          SessionVariablesCore.LoadLanguageFile();
          SessionVariablesCore.CustomerInfo.WebLanguage = webLanguage;

          var sidLangCookie = new HttpCookie("WebLanguage");
          var now = DateTime.Now;
          sidLangCookie.Value = webLanguage;
          sidLangCookie.Expires = now.AddMonths(12);
          HttpContext.Current.Response.Cookies.Add(sidLangCookie);
        }

        using (var cust = new Customers(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
          cust.UpdateWebCustomerSettings(SessionVariablesCore.CustomerInfo.CustomerID, favoriteSport, favoriteSportSubType, favoriteTeamId, webLanguage, webHomePage, baseballAction, webRememberPassword ?? false, custTimeZone);
        }
        SystemLog.WriteAccessLog("Customer Settings Updated", string.Empty);
      }
      catch (Exception ex) {
        Log(ex);
      }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void FlagCustomerMessage(int messageId, String targetAction) {
      try {
        using (var cust = new Customers(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
          if (SessionVariablesCore.CustomerInfo == null) return;
          var changeSource = SessionVariablesCore.SiteCode;

          cust.FlagWebCustomerMessage(SessionVariablesCore.CustomerInfo.CustomerID, messageId, targetAction, changeSource);
        }
      }
      catch (Exception ex) {
        Log(ex);
      }
    }

    private void SetCustomerLimits() {
      SessionVariablesCore.WagerLimitValidator = new WagerLimitValidator(SessionVariablesCore.InstanceManager.InstanceModuleInfo,
      new WagerLimitValidator.ValidatorArgs() {
        CustomerId = SessionVariablesCore.CustomerInfo.CustomerID,
        CurrencyCode = SessionVariablesCore.CustomerInfo.Currency,
        ExchangeRate = SessionVariablesCore.CustomerInfo.ExchangeRate ?? 1,
        MaxContestBet = SessionVariablesCore.CustMaxContestBet,
        CPL = SessionVariablesCore.CustMaxParlayBet,
        CTL = SessionVariablesCore.CustMaxTeaserBet,
        SYPL = SessionVariablesCore.InstanceManager.InstanceModuleInfo.Parameters.MaxParlayBet,
        SYTL = SessionVariablesCore.InstanceManager.InstanceModuleInfo.Parameters.MaxTeaserBet,
        CEPTL = SessionVariablesCore.WagerType.Code == "P" ? SessionVariablesCore.CustomerInfo.EnforceParlayMaxBetFlag == "Y" : (SessionVariablesCore.WagerType.Code == "T" && SessionVariablesCore.CustomerInfo.EnforceTeaserMaxBetFlag == "Y"),
        CQL = SessionVariablesCore.CustWagerLimit,
        Store = SessionVariablesCore.CustomerInfo.Store,
        CircleOverridingValue = SessionVariablesCore.CircleOverrindingValue,
        OfferHighLimitsActive = SessionVariablesCore.OfferHighLimits
      }, WagerLimitValidator.LimitsSourceList.Inet);

    }

    private static void WriteToLogCustomerSettingsChange(string webLanguage, string webHomePage, string favoriteSport, string favoriteSportSubType, string favoriteTeamId, string baseballAction, bool webRememberPassword, string timeZone) {
      if (SessionVariablesCore.CustomerInfo == null) return;
      if ((webLanguage ?? "").Trim() != (SessionVariablesCore.CustomerInfo.WebLanguage ?? "").Trim()) {
        SystemLog.WriteAccessLog("Customer " + SessionVariablesCore.CustomerInfo.CustomerID.Trim() + " changed Default Language", "from " + (SessionVariablesCore.CustomerInfo.WebLanguage ?? "").Trim() + " to " + (webLanguage ?? "").Trim());
      }

      if ((webHomePage ?? "").Trim() != (SessionVariablesCore.CustomerInfo.WebHomePage ?? "").Trim()) {
        SystemLog.WriteAccessLog("Customer " + SessionVariablesCore.CustomerInfo.CustomerID.Trim() + " changed Default Home Page", "from " + (SessionVariablesCore.CustomerInfo.WebHomePage ?? "").Trim() + " to " + (webHomePage ?? "").Trim());
      }

      if ((favoriteSport ?? "").Trim() != (SessionVariablesCore.CustomerInfo.FavoriteSport ?? "").Trim()) {
        SystemLog.WriteAccessLog("Customer " + SessionVariablesCore.CustomerInfo.CustomerID.Trim() + " changed Favorite Sport", "from " + (SessionVariablesCore.CustomerInfo.FavoriteSport ?? "").Trim() + " to " + (favoriteSport ?? "").Trim());
      }

      if ((favoriteSportSubType ?? "").Trim() != (SessionVariablesCore.CustomerInfo.FavoriteSportSubType ?? "").Trim()) {
        SystemLog.WriteAccessLog("Customer " + SessionVariablesCore.CustomerInfo.CustomerID.Trim() + " changed Favorite Sport SubType", "from " + (SessionVariablesCore.CustomerInfo.FavoriteSportSubType ?? "").Trim() + " to " + (favoriteSportSubType ?? "").Trim());
      }

      if ((favoriteTeamId ?? "").Trim() != (SessionVariablesCore.CustomerInfo.FavoriteTeamID ?? "").Trim()) {
        SystemLog.WriteAccessLog("Customer " + SessionVariablesCore.CustomerInfo.CustomerID.Trim() + " changed Favorite Team", "from " + (SessionVariablesCore.CustomerInfo.FavoriteTeamID ?? "").Trim() + " to " + (favoriteTeamId ?? "").Trim());
      }

      if ((baseballAction ?? "").Trim() != (SessionVariablesCore.CustomerInfo.BaseballAction ?? "").Trim()) {
        SystemLog.WriteAccessLog("Customer " + SessionVariablesCore.CustomerInfo.CustomerID.Trim() + " changed Default Baseball Action Type", "from " + (SessionVariablesCore.CustomerInfo.BaseballAction ?? "").Trim() + " to " + (baseballAction ?? "").Trim());
      }

      if (webRememberPassword != SessionVariablesCore.CustomerInfo.WebRememberPassword) {
        var currentSetting = SessionVariablesCore.CustomerInfo.WebRememberPassword == true ? "\"Remember\"" : "\"Do not Remember\"";
        var newSetting = webRememberPassword ? "\"Remember\"" : "\"Do not Remember\"";

        SystemLog.WriteAccessLog("Customer " + SessionVariablesCore.CustomerInfo.CustomerID.Trim() + " changed Default Password Option", "from " + currentSetting + " to " + newSetting);
      }

      if (timeZone.Trim() != SessionVariablesCore.CustomerInfo.TimeZone.ToString().Trim()) {
        SystemLog.WriteAccessLog("Customer " + SessionVariablesCore.CustomerInfo.CustomerID.Trim() + " changed Default Time Zone", "from " + SessionVariablesCore.CustomerInfo.TimeZone.ToString().Trim() + " to " + timeZone.Trim());
      }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetLiveBettingToken() {
      var rc = new ResultContainer();
      if (rc.Code != ResultContainer.ResultCode.Success) return rc;
      try {
        /*
        var lbClient = new ClientAPIClient();
        var result = lbClient.generateToken(SessionVariablesCore.CustomerInfo.CustomerID);
        rc.Data = result.tokenId;
        */
          var customeid = SessionVariablesCore.CustomerInfo.CustomerID.Trim();
          var id = customeid.Substring(2);
        var tstamp = DateTime.UtcNow.ToString("o");
        rc.Data = new {
            CustomerId = customeid,
          TimeStamp = tstamp,
            Hash = GetHashSha256(customeid + tstamp + ConfigurationManager.AppSettings["LiveBettingKey"])
            //Hash = GetHashSha256(SessionVariablesCore.CustomerInfo.CustomerID.Trim() + tstamp + ConfigurationManager.AppSettings["LiveBettingKey"])
          //Platform = "asi"
        };
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetLiveDealerToken() {
      var rc = new ResultContainer();
      if (rc.Code != ResultContainer.ResultCode.Success) return rc;
      try {
        var lbClient = new MasterClientWebServiceSoapClient();
        rc.Data = lbClient.getEncryptedToken("TRADEWINDS", SessionVariablesCore.CustomerInfo.Password,
            SessionVariablesCore.CustomerInfo.CustomerID, SessionVariablesCore.CustomerInfo.AgentID, "");
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetColchianToken(string sourceId, string sourcePassword, string playerId, string password, string userId, string applicationId, int logonSource, int websiteId, string partnerId, string thirdPartyToken) {
      var colchianClient = new IncomingIntegrationServiceClient();
      var rc = new ResultContainer();
      try {
        var result = colchianClient.PlayerLogin(sourceId, sourcePassword, playerId, password, userId, ServerVariables.ClientIp, applicationId, logonSource, websiteId, partnerId, thirdPartyToken);
        rc.Data = result.Token;
        SystemLog.WriteAccessLog("Horses", string.Empty);
      }
      catch (Exception ex) {
        rc.Code = ResultContainer.ResultCode.Fail;
        rc.Message = ex.Message;
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer CustomerActionLogger(string action, string data) {
      var rc = new ResultContainer();
      if (rc.Code != ResultContainer.ResultCode.Success) return rc;
      try {
        SystemLog.WriteAccessLog(action, data);
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void ToggleCustomerAction(string code) {
      SessionVariablesCore.AutoAccept = !SessionVariablesCore.AutoAccept;
      using (var c = new Customers(InstanceModuleInfo)) {
        var restrictions = c.GetRestrictionsByCustomer(SessionVariablesCore.CustomerInfo.CustomerID);
        int actionId = Convert.ToInt16(from r in restrictions.Where(r => r.Code == code) select r.ParamValue);
        c.ToggleCustomerAction(SessionVariablesCore.CustomerInfo.CustomerID, actionId, SessionVariablesCore.SiteCode);
      }

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void AutoAcceptSwitch(bool temporal) {
      SessionVariablesCore.AutoAccept = !SessionVariablesCore.AutoAccept;
      if (temporal) return;
      using (var cst = new Customers(InstanceModuleInfo)) {
        if (SessionVariablesCore.AutoAccept) cst.RestrictAction(SessionVariablesCore.CustomerInfo.CustomerID, 27, string.Concat(SessionVariablesCore.CustomerInfo.CustomerID, '/', SessionVariablesCore.SiteCode));
        else cst.AllowAction(SessionVariablesCore.CustomerInfo.CustomerID, 27, string.Concat(SessionVariablesCore.CustomerInfo.CustomerID, '/', SessionVariablesCore.SiteCode));
        SystemLog.WriteAccessLog("Auto Accept Wagers Updated", SessionVariablesCore.AutoAccept.ToString());
      }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetCustomerNonPostedCasinoPlays() {
      using (var cst = new Casinos(InstanceModuleInfo)) {
        return new ResultContainer { Data = cst.GetCustomerNonPostedCasinoPlays(SessionVariablesCore.CustomerInfo.CustomerID) };
      }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer TrackStatus() {
      var rc = new ResultContainer(false);
      if (rc.SessionExpired) SystemLog.WriteAccessLog("Session expired", string.Empty);
      return rc;
    }

    /*
    private static string GetHashSha256(string text) {
      var bytes = Encoding.Unicode.GetBytes(text);
      var hashstring = new SHA256Managed();
      var hash = hashstring.ComputeHash(bytes);
      return hash.Aggregate(string.Empty, (current, x) => current + String.Format("{0:x2}", x));
    }
    */

    static string GetHashSha256(string rawData) {
      // Create a SHA256   
      using (SHA256 sha256Hash = SHA256.Create()) {
        // ComputeHash - returns byte array  
        byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

        // Convert byte array to a string   
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < bytes.Length; i++) {
          builder.Append(bytes[i].ToString("x2"));
        }
        return builder.ToString();
      }
    }

    /*
    private static string GetHashSha256(string text) {
      using (var sha = SHA256.Create()) {
        var computedHash = sha.ComputeHash(Encoding.Unicode.GetBytes(text));
        return Convert.ToBase64String(computedHash);
      }
    }
    */

  }
}