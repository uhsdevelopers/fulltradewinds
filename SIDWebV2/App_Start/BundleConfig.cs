﻿using System.Web.Optimization;
using SIDWebLibraries.HelperClasses;

namespace SIDWebV2 {
  public class BundleConfig {

    // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
    public static void RegisterBundles(BundleCollection bundles) {

      bundles.IgnoreList.Clear();

      var target = SessionVariablesCore.WebTarget;
      //var appLastUpdate = SessionVariablesCore.AppLastUpdate ?? "";

      bundles.Add(new StyleBundle("~/Content/logginStyle").Include(
                  "~/assets/fonts/googleFonts.css",
                  "~/assets/css/animate.css",
                  "~/assets/css/login_" + target + ".css"
                  ));

      bundles.Add(new StyleBundle("~/Content/style").Include(
          "~/assets/css/normalize.css",  //8
          "~/assets/libs/bootstrap/css/bootstrap.css", //114
          "~/assets/libs/bootstrap/css/daterangepicker.css", //8
          "~/assets/css/animate.css", //74
          //"~/assets/libs/angular/loading/angular-loading.css", //1
          "~/assets/libs/angular/loading-bar/loading-bar.css", //4
          "~/assets/css/simple-sidebar.css", //2k
          //"~/assets/libs/slideoutjs/slideout.css",
          "~/assets/css/style_" + target + ".css" //136
      ));

      bundles.Add(new ScriptBundle("~/Content/appLibs").Include(
                  "~/assets/js/dateFormat.js",
                  "~/assets/js/errorHandler.js",
                  "~/assets/js/utilities.js",
                  //"~/assets/js/blockUI.js",
                  "~/assets/js/translator.js",
                  "~/assets/js/app.UI.js",
                  "~/assets/js/moment.js",
                  "~/assets/js/moment-timezone.js",
                  "~/app/serviceCaller.js",
                  "~/assets/js/cryptoJS/rollups/hmac-sha1.js",
                  "~/assets/js/cryptoJS/rollups/aes.js"/*,
                  "~/assets/libs/websocket/swfobject.js",
                  "~/assets/libs/websocket/web_socket.js"*/
                  ));

      bundles.Add(new ScriptBundle("~/Content/angular").Include(
                  "~/assets/libs/jquery/jquery-2.1.4.js",
                  "~/assets/libs/jquery/jquery.signalR-2.3.0.js",
                  "~/assets/libs/angular/angular.js",
                  "~/assets/libs/angular/angular-route.js",
                  "~/assets/libs/angular/angular-animate.js",
                  "~/assets/js/spin.js",
                  //"~/assets/libs/angular/loading/angular-loading.js",
                  "~/assets/libs/angular/loading-bar/loading-bar.js"));

      bundles.Add(new ScriptBundle("~/Content/bootstrap").Include(
                  "~/assets/libs/bootstrap/js/bootstrap.js",
                  "~/assets/libs/bootstrap/js/bootstrap-notify.js",
                  "~/assets/libs/bootstrap/js/boostrap-dialog.js",
                  "~/assets/libs/bootstrap/js/daterangepicker.js"));

      bundles.Add(new ScriptBundle("~/Content/appCore").Include(
                  "~/app/core/appSettings_" + target + ".js",
                  "~/app/core/wagerType.js",
                  "~/app/core/commonFunctions.js",
                  "~/app/core/lineOffering.js",
                  "~/app/core/parlayFunctions.js",
                  "~/app/core/teaserFunctions.js",
                  "~/app/core/arFunctions.js",
                  "~/app/core/ifBetFunctions.js",
                  "~/app/core/contestFunctions.js",
                  "~/app/models/app.viewsManager.js",
                  "~/app/core/directives.js",
                  "~/app/core/filters.js"));

      bundles.Add(new ScriptBundle("~/Content/appControllers").Include(
                  "~/app/app.main.js",
                  "~/app/app.controller.js",
                  "~/app/langMaintenace.js",
                  "~/app/components/betOffering/betOfferingController.js",
                  "~/app/components/rules/rulesController.js",
                  "~/app/components/nonPostedCasinoplays/nonPostedCasinoplaysController.js",
                  "~/app/components/betSlip/betSlipController.js",
                  "~/app/components/inbox/inboxController.js",
                  "~/app/components/casinos/casinosController.js",
                  "~/app/components/reports/reportsController.js",
        //"~/app/components/horses/horsesController.js",
                  "~/app/components/liveBetting/liveBettingController.js",
        //"~/app/components/settings/langMaintController.js",
                  "~/app/components/settings/settingsController.js"));



      bundles.Add(new ScriptBundle("~/Content/appServices").Include(
                  "~/app/services/app.customerService.js",
                  "~/app/services/app.inboxService.js",
                  "~/app/services/app.reportsService.js",
                  "~/app/services/app.rulesService.js",
                  "~/app/services/app.settingsService.js",
                  "~/app/services/app.sportsAndContestsService.js",
                  "~/app/services/app.webSocketService.js",
                  "~/app/services/app.systemService.js",
                  "~/app/services/app.ticketService.js",
                  "~/app/services/app.translatorService.js",
                  "~/app/services/app.wagerTypesService.js",
                  "~/app/services/app.encryptionService.js",
                  "~/app/services/app.storageService.js",
                  "~/app/services/app.errorHandlerService.js",
                  "~/app/services/app.liveDealerService.js"
                  ));

      bundles.Add(new ScriptBundle("~/Content/appLogin").Include(
                  "~/app/core/appSettings_" + target + ".js",
                  "~/app/app.login.js",
                  "~/app/services/app.translatorService.js",
                  "~/app/components/login/loginService.js",
                  "~/app/components/login/loginController.js"));


      bundles.Add(new ScriptBundle("~/Data/appVersion").Include(
                  "~/data/appVersion.js"));
      #if DEBUG
        BundleTable.EnableOptimizations = false;
      #else
        BundleTable.EnableOptimizations = true;
      #endif

    }
  }
}