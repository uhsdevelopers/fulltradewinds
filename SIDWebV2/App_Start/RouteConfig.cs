﻿using System.Web.Mvc;
using System.Web.Routing;

namespace SIDWebV2 {
  public class RouteConfig {
    public static void RegisterRoutes(RouteCollection routes) {
      routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
      routes.IgnoreRoute("{directory}/{resource}.asmx/{*pathInfo}");
      routes.IgnoreRoute("{directory}/{resource}/{filename}.html");

      /*routes.MapRoute(
          name: "Session Expired",
          url: "{action}",
          defaults: new { controller = "Login", action = "SessionExpired", ac = "se" }
      );*/

      routes.MapRoute(
          "Default",
          "{controller}/{action}/{id}",
          new { controller = "BettingApp", action = "Index", id = UrlParameter.Optional },
          new [] {"SIDWebV2.Controllers"}
      );
      

    }
  }
}