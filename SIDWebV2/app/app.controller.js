﻿var appCtrl = appModule.controller(appModule.Controllers.app, ['$scope', '$rootScope', '$location', '$timeout', '$customerService', '$wagerTypesService', '$sportsAndContestsService', '$translatorService', '$systemService', '$ticketService', '$webSocketService', '$inboxService', '$http', '$webStorageService', function ($scope, $rootScope, $location, $timeout, $customerService, $wagerTypesService, $sportsAndContestsService, $translatorService, $systemService, $ticketService, $webSocketService, $inboxService, $http, $webStorageService) {

  var _prevParentIndex = 0;
  var _prevIndex = 0;
  var _idTeaser;
  var _actualTeaser;
  var _lastPath = '/';
  var _caller = new ServiceCaller($http, null, 'CustomerService');
  var _togglesArray = $webStorageService.session.getItem(0);
  var _savedSelected = $webStorageService.session.getItem(1);
  var _isOpenTeaser;
  var _lastPackage = -1;

  //_togglesArray = (CommonFunctions.ArrayIncludes(_togglesArray, '0') && CommonFunctions.ArrayIncludes(_togglesArray, 'sp_0') ? _togglesArray : _togglesArray.concat(["0", "sp_0"]));

  $scope.TicketServiceView = $ticketService;
  $scope.Selections = [];
  $scope.BetOfferingTemplate = appModule.Templates.betOffering;
  $scope.betSlipTemplate = appModule.Templates.betSlip;
  $scope.rulesMenuMobileTemplate = appModule.Templates.leftSidebar.rulesMenu;
  $scope.BetOfferingTemplateMobile = appModule.Templates.betOfferingMobile;
  $scope.Templates = appModule.Templates;
  $scope.ActiveSideBarPane = 0;
  $scope.PageWrapperClass = 'wrapper';
  $scope.path = _lastPath;
  $scope.DialogTemplate = "";
  $scope.ModalClass = "";
  $scope.PendingMsgs = 0;
  $scope.TeaserDesc = {
    NFLTeaser: "",
    StandardTeaser: "",
    SpecialTeaser: ""
  };
  $scope.ShowSideBar = true;
  $scope.OfferingVisible = true;
  $rootScope.IsMobileDevice = false;
  $scope.ShowClassicButton = SETTINGS.ShowClassicButton;
  $scope.LiveChatAvailable = SETTINGS.LiveChatAvailable;
  $scope.ShowSearchBar = SETTINGS.ShowSearchBar;
  $scope.AllSelected = false;
  $scope.PageLoading = true;
  $rootScope.ShowGrades = false;
  $rootScope.bannedMessages = $webStorageService.session.getItem(2);
  $rootScope.LiveBettingToken = "";
  $rootScope.ErrorMessage = "";
  $rootScope.IV = "";
  $rootScope.Key = "";
  $rootScope.SetPassword = false;
  $rootScope.FirstLoad = true;
  $rootScope.TeaserLoaded = true;
  $rootScope.ShowTeamTotals = [];
  $rootScope.Page = "";
  $rootScope.RecentTicketProcessed = false;
  $rootScope.SearchFilter = {
    value: ""
  };
  $rootScope.spotlight = {
    Switch: false,
    GameStatus: false,
    ContestStatus: false
  };
  $rootScope.menuTab = 1;
  $scope.sideBarId = "sidebar-wrapper";

  $scope.LiveDealer = null;
  $scope.VirtualCasino = null;
  $scope.LiveBetting = null;
  $scope.HorsesActive = null;

  $(document).ready(function () {
    $location.path("/");
    var onSizeChanges = [{
      element: "lineDiv",
      type: "id", //id / class
      threshold: 991.98,
      lowerClass: "page-content-wrapper-mob",
      upperClass: "page-content-wrapper"
    }];

    var xDown = null;
    var yDown = null;

    function handleTouchStart(evt) {
      if (!$rootScope.IsMobileDevice) return;
      xDown = evt.touches[0].clientX;
      yDown = evt.touches[0].clientY;
    };

    function handleTouchMove(evt) {
      if (!xDown || !yDown || !$rootScope.IsMobileDevice || $('#modalDialog').is(':visible')) {
        return;
      }
      var xUp = evt.touches[0].clientX;
      var yUp = evt.touches[0].clientY;

      var xDiff = xDown - xUp;
      var yDiff = yDown - yUp;

      if (Math.abs(xDiff) > Math.abs(yDiff)) {/*most significant*/
        if (xDiff > 0) {
          /* left swipe */
          if ($("#wrapper").hasClass("active")) $scope.ToggleMenu();
        } else {
          /* right swipe */
          if (!$("#wrapper").hasClass("active")) $scope.ToggleMenu();
        }
      }
      /* reset values */
      xDown = null;
      yDown = null;
    };

    function setTouchActions() {
      document.addEventListener('touchstart', handleTouchStart, false);
      document.addEventListener('touchmove', handleTouchMove, false);
    }

    function onSizeChange() {
      var wW = $(window).width();
      var oIsMobileDevice = $rootScope.IsMobileDevice;
      for (var i = 0; i < onSizeChanges.length; i++) {
        var change = onSizeChanges[i];
        var el;
        if (change.type == "id") el = $("#" + change.element);
        else el = $("." + change.element);
        if (el != null) {
          if (wW > change.threshold) {
            $rootScope.IsMobileDevice = false;
            $scope.sideBarId = "sidebar-wrapper-desk";
            if (!el.hasClass(change.upperClass)) {
              el.removeClass(change.lowerClass);
              el.addClass(change.upperClass);
            }
            $("#logout").hide();
          } else {
            $rootScope.IsMobileDevice = true;
            $scope.PageWrapperClass = 'wrapper';
            $scope.sideBarId = "sidebar-wrapper";
            if (!el.hasClass(change.lowerClass)) {
              $("#selectionMen").addClass("active");
              el.removeClass(change.upperClass);
              el.addClass(change.lowerClass);
            }
            $("#logout").hide();
            setTimeout(function () {
              $("#logout").show();
            }, 500);
          }

          $rootScope.safeApply();
        }
      }
      if (oIsMobileDevice !== $rootScope.IsMobileDevice) {
        $scope.CloseDialog();
        $rootScope.menuTab = 1;
      }
    }

    $(window).resize(onSizeChange);
    onSizeChange();
    setTouchActions();
    $customerService.CustomerActionLogger("WebSite loaded");
  });

  function init() {
    $rootScope.ViewsManager = new ViewsManager($scope, $location, $timeout);
  }

  function sessionSportInfo(sport, subSport) {
    return {
      Sport: sport,
      SubSport: subSport
    };
  };

  function showTooltip(time) {
    setTimeout(function () {
      $('[data-toggle="tooltip"]').tooltip();
    }, time);
  }

  function subSportInSelection(subSport) {
    return $sportsAndContestsService.Selections.some(function (sel) { return subSport.Offering.SportSubTypeId == sel.SportSubTypeId; });
  }

  function linkSportsAndSelectionsFn() {
    if (!$wagerTypesService.Selected) return;
    $scope.Selections = $sportsAndContestsService.Selections;
    $scope.SportsAndContests = $sportsAndContestsService.SportsAndContests;
    switch ($wagerTypesService.Selected.name) {
      case "Parlay":
      case "Teaser":
      case "If Win or Push":
      case "If Win Only":
      case "Action Reverse":
        $webSocketService.SetAsiansAndRestrictedParlay();
        break;
      default:
        $webSocketService.RemoveAsiansAndRestrictedParlay();
        break;
    }
    if ($ticketService.Ticket.OpenWager && $wagerTypesService.IsTeaser())
      $sportsAndContestsService.Teasers.forEach(function (partA) {
        partA.Sub.forEach(function (partB) {
          if (partB.SportType == $ticketService.Ticket.OpenWager.TeaserName.trim() && partA.OpenAllowed) {
            _isOpenTeaser = true;
            $sportsAndContestsService.ToggleTeasers(partB, true).then();
            partA.Selected = true;
            partB.Selected = true;
          } else {
            partA.Selected = false;
            partB.Selected = false;
          }
        });
      });
    $scope.Teasers = $sportsAndContestsService.Teasers;
    $scope.TeaserDesc.NFLTeaser = $wagerTypesService.List[2].descriptions.NFLTeaser;
    $scope.TeaserDesc.StandardTeaser = $wagerTypesService.List[2].descriptions.StandardTeaser;
    $scope.TeaserDesc.SpecialTeaser = $wagerTypesService.List[2].descriptions.SpecialTeaser;
  };

  function popAllSubscriptions () {
    if ($scope.Selections != null && $scope.Selections.length > 0) {
      var soportsToUnsubscribe = [];
      angular.forEach($scope.Selections, function (value) {
        soportsToUnsubscribe.push({
          "SportType": value.SportType,
          "SportSubType": value.SportSubType
        });
      });
      $webSocketService.UnsubscribeSports(soportsToUnsubscribe);
    }
  };

  function loadTeaser (teaser, parentIndex, index, isOpenAllowed) {
    if (isOpenAllowed) $ticketService.StartNewTicket();
    _isOpenTeaser = isOpenAllowed;
    $ticketService.SetOpenPlaysVal($ticketService.OpenPlayDropDown[0]);
    _actualTeaser = teaser.SportType;
    popAllSubscriptions();
    //Avoid the blink in teasers
    $scope.Teasers.forEach(function (partA) {
      partA.Sub.forEach(function (partB) {
        partB.Selected = false;
        partB.Selected = false;
      });
    });
    $rootScope.TeaserLoaded = false;
    $scope.Teasers[_prevParentIndex].Sub[_prevIndex].Selected = false;
    $scope.Teasers[parentIndex].Sub[index].Selected = true;
    _prevParentIndex = parentIndex;
    _prevIndex = index;
    _idTeaser = parentIndex.toString() + index.toString();
    $ticketService.ShowOpenPlayItems = false;
    $sportsAndContestsService.ToggleTeasers(teaser, _isOpenTeaser);
  };

  function tabsAvailable () {
    $scope.HorsesActive = true;
    $scope.LiveDealer = true;
    $scope.VirtualCasino = true;
    $scope.LiveBetting = true;
    $scope.WageringDisabled = false;
    for (var i = 0; $scope.Customer.Restrictions.length > i ; i++) {
      if ($scope.Customer.Restrictions[i].Code == "NOWAGERING") {
        $scope.WageringDisabled = true;
      }
      if ($scope.Customer.Restrictions[i].Code == "HORSES") {
        $scope.HorsesActive = false;
      }
      if ($scope.Customer.Restrictions[i].Code == "LDEALER") {
        $scope.LiveDealer = false;
      }
      if ($scope.Customer.Restrictions[i].Code == "VCASINO") {
        $scope.VirtualCasino = false;
      }
      if ($scope.Customer.Restrictions[i].Code == "LBETTING") {
        $scope.LiveBetting = false;
      }
    }

    $scope.HorsesActive = $scope.HorsesActive && !$scope.WageringDisabled;
    $scope.LiveDealer = $scope.LiveDealer && !$scope.WageringDisabled;
    $scope.VirtualCasino = $scope.VirtualCasino && !$scope.WageringDisabled;
    $scope.LiveBetting = $scope.LiveBetting && !$scope.WageringDisabled;

    $rootScope.safeApply();
  };

  function getMessageNotification () {
    $inboxService.GetMessageNotificationBySport(null, null).then(function (result) {
      if (!result.data.d.Data || result.data.d.Data.length == 0) return null;
      var allowed = [];
      if ($rootScope.bannedMessages != null) {
        result.data.d.Data.forEach(function (e) {
          var found = false;
          if (($wagerTypesService.IsTeaser() && !(e.SportType.trim() == 'Football' || e.SportType.trim() == 'Basketball'))) found = true;
          $rootScope.bannedMessages.forEach(function (b) {
            if (e.MessageID == b.MessageID && $customerService.Info.CustomerID.trim() == b.CustomerID) found = true;
          });
          if (!found) allowed.push(e);
        });
        $scope.MessagesNotficationList = allowed;
      } else return $scope.MessagesNotficationList = result.data.d.Data;
      return null;
    });
  };

  function updateLastServiceMessage () {
    if ($rootScope.lastPackageDate) _lastPackage = Math.floor(Math.abs(($rootScope.lastPackageDate - new Date().getTime()) / 1000));
    $scope.ServerInfo = ($scope.WebServer ? $scope.WebServer : 'localhost') + '-' + $rootScope.GetCurrentSocketServer() + '-' + _lastPackage + 's';
  };

  function toggleSports () {
    if (!_togglesArray) return;
    for (var i = 0; i < _togglesArray.length; i++) {
      var collapsible = $('#' + _togglesArray[i]);
      if (collapsible.length == 0) continue;
      collapsible.addClass("in");
      collapsible.removeClass("collapsed");
    }
  };

  function manualSelectFix (sport, treeSelection) {
    _savedSelected = $webStorageService.session.getItem(1);
    var found = false;
    for (var i = 0; i < _savedSelected.length; i++) {
      if (_savedSelected[i].SubSport.SequenceNumber == treeSelection.SequenceNumber) {
        _savedSelected.splice(i, 1);
        found = true;
        break;
      } else if (_savedSelected[i].SubSport.ContestType2 && _savedSelected[i].SubSport.ContestType2 == treeSelection.ContestType2) {
        _savedSelected.splice(i, 1);
        found = true;
        break;
      }
    }
    if (!found) {
      _savedSelected.push(sessionSportInfo(sport, treeSelection));
    }
    $webStorageService.session.setItem(1, _savedSelected);
  };

  var SchedulingSessionTrack = setTimeout(function tick() {
    $customerService.TrackStatus().then();
    SchedulingSessionTrack = setTimeout(tick, SETTINGS.TrackTime);
  }, SETTINGS.TrackTime);

//Watchers
  $scope.$on('$routeChangeStart', function () {
    $scope.ChangeWrapperLayout();
  });

  $rootScope.$on('customerInfoLoaded', function () {
    if ($customerService.Info) {
      $scope.Customer = {
        Info: $customerService.Info,
        Settings: $customerService.Settings,
        Balances: $customerService.Balances,
        Restrictions: $customerService.Restrictions,
        IsDemo: $customerService.Info.CustomerID.trim() === "DEMO",
        MinimumWagerAmt: $customerService.MinimumWagerAmt

      };
      tabsAvailable();
      $translatorService.ChangeLanguage($scope.Customer.Settings.WebLanguage);
      $scope.ShowPriorityMessages();
    }

  });

  $rootScope.$on('contestSubscribed', function (event, data) {
    $webSocketService.SubscribeContest(data.ContestType, data.ContestType2, $scope.Customer.Info.Store.trim());
  });

  $rootScope.$on('contestUnsubscribed', function (event, data) {
    $webSocketService.UnsubscribeContest(data.ContestType, data.ContestType2);
  });

  $rootScope.$on('customerInfoChanged', function (resetLastSession) {
    $customerService.GetCustomerInfo(resetLastSession);
  });

  $rootScope.$on('sportSubscribed', function (event, data) {
    if ($scope.Customer) $webSocketService.SubscribeSport(data.SportType, data.SportSubType, $scope.Customer.Info.Store.trim());
  });

  $rootScope.$on('sportUnsubscribed', function (event, data) {
    if ($sportsAndContestsService.Selections.length > 0) {
      var prevLast = ($sportsAndContestsService.Selections[$sportsAndContestsService.Selections.length - 1].SportSubType || '').RemoveSpecials();
      $('#subSportArrow_' + prevLast).toggleClass('collapsed');
      $('#subSport_' + prevLast).toggleClass('in');
    }
    $webSocketService.UnsubscribeSport(data.SportType, data.SportSubType);
  });

  $rootScope.$on('SportsAndContestsLoaded', function () {
    setTimeout(function () {
      toggleSports();
      if ($scope.Customer && $scope.Customer.IsDemo) $scope.Events.ToggleSubSport($sportsAndContestsService.SportsAndContests[0], $sportsAndContestsService.SportsAndContests[0].SportSubTypes[0], true);

    }, 500);
  });

  $rootScope.$on('wagerTypeChanged', function () {
    if (!$wagerTypesService.Selected || !$scope.ViewsManager.IsSportView(true)) return;
    if (!$scope.WagerTypesView) $scope.WagerTypesView = $wagerTypesService;
    //if (!$scope.PageLoading) {
      $ticketService.StartNewTicket().then(function () {
        $sportsAndContestsService.RemoveWagerSelections();
        //$sportsAndContestsService.SpotlightContest();
        //$sportsAndContestsService.SpotlightGame();
        if ($wagerTypesService.IsTeaser()) $sportsAndContestsService.GetActiveTeasers().then(linkSportsAndSelectionsFn);
        else $sportsAndContestsService.GetActiveSportsAndContests(SETTINGS.ShowInactiveSports && SETTINGS.ShowInactiveSports.length > 0).then(linkSportsAndSelectionsFn);
        showTooltip(1000);
      });
    //}
    //else $scope.PageLoading = false;
  });

  $rootScope.$on('customerMsgsLoaded', function () {
    $scope.PendingMsgs = $inboxService.PendingMsgs.Unread;
    $rootScope.safeApply();
  });

  $rootScope.$on('TeaserLoaded', function () {
    popAllSubscriptions();
    if ($sportsAndContestsService.Selections.length == 0) {
      document.getElementById('btnTeaser_' + _idTeaser).setAttribute("disabled", true);
      UI.Notify($translatorService.Translate(_actualTeaser) + ' ' + $translatorService.Translate("has no lines"), UI.Position.Top, UI.Position.Center, 200, UI.Type.Alert);
      $scope.ShowNoTeasersMessage = true;
      return;
    }
    $scope.ShowNoTeasersMessage = false;
    var sportsToSubscribe = [];
    for (var i = 0; i < $sportsAndContestsService.Selections.length; i++) {
      var el = $sportsAndContestsService.Selections[i];
      sportsToSubscribe.push({
        "SportType": el.SportType,
        "SportSubType": el.SportSubType,
        "Store": $scope.Customer.Info.Store.trim()
      });
    }
    $webSocketService.SubscribeSports(sportsToSubscribe);
    showTooltip(1000);

  });

  //Translator
  $scope.Translate = function (text) {
    return $translatorService.Translate(text);
  };

  $scope.Reports = $scope.Reports || {};

  $scope.Reports.CleanReports = function () {
    $scope.Templates.reports.selected = "";
    // broadcast load data*
  };

  $scope.SwitchOffering = function () {
    $scope.OfferingVisible = !$scope.OfferingVisible;
  }

  //Events
  $scope.Events = $scope.Events || {};

  $scope.Events.ChangeWagerType = function () {
    if ($ticketService.Ticket.Any() && !$ticketService.Ticket.Posted()) {
      UI.Confirm("Change wager type will remove your current selections, continue", function () {
        $scope.ShowNoTeasersMessage = false;
        $ticketService.ShowOpenPlayItems = false;
        $ticketService.OpenPlays = [];
        $ticketService.Ticket.OpenWagerItems = [];
        $rootScope.ErrorMessage = "";
        $wagerTypesService.Change();
        popAllSubscriptions();
        $ticketService.SetOpenPlaysVal($ticketService.OpenPlayDropDown[0]);
        setTimeout(function () { toggleSports(); }, 500);
        getMessageNotification();
      }, function () {
        $wagerTypesService.RevertChange();
        $rootScope.safeApply();
      });
      return;
    }
    getMessageNotification();
    $rootScope.ErrorMessage = "";
    $wagerTypesService.Change();
    popAllSubscriptions();
    $scope.ShowNoTeasersMessage = false;
    $ticketService.ShowOpenPlayItems = false;
    $ticketService.OpenPlays = [];
    $ticketService.Ticket.OpenWagerItems = [];
    $ticketService.SetOpenPlaysVal($ticketService.OpenPlayDropDown[0]);
    setTimeout(function () { toggleSports(); }, 500);
  };

  $scope.Events.ToggleSubSport = function (sport, subSport, autoSelect) {
    if (!subSport.Active) return;
    $sportsAndContestsService.Selections.forEach(function (s) {
      var acc = $('#subSport_' + (s.SportSubType || '').RemoveSpecials());
      if (acc.hasClass('in')) {
        acc.removeClass('in');
        $('#subSportArrow_' + (s.SportSubType || '').RemoveSpecials()).addClass('collapsed');
      }
    });
    getMessageNotification();
    if (typeof autoSelect === "undefined") autoSelect = false;
    $rootScope.FirstLoad = false;
    $sportsAndContestsService.ToggleSubSport(sport, subSport);
    var selectedCount = 0;
    var unSelectedCount = 0;
    for (var j = 0; j < sport.SportSubTypes.length; j++) {
      if (sport.SportSubTypes[j].Selected)
        selectedCount++;
      else unSelectedCount++;
    }
    sport.allSelected = (selectedCount == sport.SportSubTypes.length ? true : (unSelectedCount == sport.SportSubTypes.length ? false : sport.allSelected));
    if (!autoSelect) manualSelectFix(sport, subSport);
    if (subSport.Offering) {
      setTimeout(function () {
        if (subSportInSelection(subSport)) {
          var anchor = $("#divSubSport_" + subSport.Offering.SportSubTypeId);
          if (!anchor.offset()) return;
          $('#lineDiv').animate({
            scrollTop: $('#lineDiv').scrollTop() + anchor.offset().top - 100
          }, 'fast');
        }
        showTooltip(1);
      }, 1000);
    }
  };

  $scope.Events.ToggleContest = function (contest, contest2, autoSelect) {
    var found = false;
    $sportsAndContestsService.Selections.forEach(function (s) {
      if (found) return;
      if (s.Lines) {
        s.Lines.forEach(function (l) {
          if (l.Correlation)
            l.Correlation.forEach(function (c) {
              if (c.ContestType2 == contest2.ContestType2) {
                found = true;
                return;
              }
            });
        });
      }
    });
    if (found) {
      UI.Notify($translatorService.Translate('Contest already open'),
                          UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
      contest2.Selected = false;
      return;
    }
    if (typeof autoSelect === "undefined") autoSelect = false;
    $rootScope.FirstLoad = false;
    $sportsAndContestsService.ToggleContest(contest2);
    var selectedCount = 0;
    var unSelectedCount = 0;
    for (var j = 0; j < contest.ContestTypes2.length; j++) {
      if (contest.ContestTypes2[j].Selected)
        selectedCount++;
      else unSelectedCount++;
    }
    contest.allSelected = contest2.allSelected = (selectedCount == contest.ContestTypes2.length ? true : (unSelectedCount == contest.ContestTypes2.length ? false : contest.allSelected));
    if (!autoSelect) manualSelectFix(contest, contest2);
    setTimeout(function () {
      if (contest2.Offering) {
        var anchor = $("#divContest_" + contest2.Offering[0].ContestNum);
        if (!anchor.offset()) return;
        $('#lineDiv').animate({
          scrollTop: $('#lineDiv').scrollTop() + anchor.offset().top - 100
        }, 'fast');
        showTooltip(1);
      }
    }, 1000);

  };

  $scope.Events.ToggleContestByCorrelation = function (gameLine) {
    $sportsAndContestsService.ToggleContestByCorrelation(gameLine);
  };

  $scope.Events.toggleAllContestsInProgress = false;
  $scope.Events.ToggleAllContests = function (contest, cursor) {
    if (!contest || !contest.ContestTypes2 || typeof contest.ContestTypes2 === "undefined") return null;
    $customerService.CustomerActionLogger($scope.AllSelected ? "All Contest Selected" : "All Contest Unselected", "Contest type: " + contest.ContestType);
    if (typeof cursor === "undefined") cursor = 0;
    if (cursor < contest.ContestTypes2.length) {
      $scope.Events.toggleAllContestsInProgress = true;
      if (!contest.allSelected && !contest.ContestTypes2[cursor].Selected) {
        manualSelectFix(contest.ContestTypes2[cursor], contest.ContestTypes2[cursor]);
        $sportsAndContestsService.ShowContest(contest.ContestTypes2[cursor]).then(function () {
          //recursividad
          contest.ContestTypes2[cursor].Selected = !contest.allSelected;
          return $scope.Events.ToggleAllContests(contest, ++cursor);
        });
      } else if (contest.allSelected) {
        $sportsAndContestsService.HideContest(contest.ContestTypes2[cursor]);
        manualSelectFix(contest.ContestTypes2[cursor], contest.ContestTypes2[cursor]);
        //recursividad
        contest.ContestTypes2[cursor].Selected = !contest.allSelected;
        return $scope.Events.ToggleAllContests(contest, ++cursor);
      }
      else if (contest.ContestTypes2[cursor].Selected) {
        return $scope.Events.ToggleAllContests(contest, ++cursor);
      }
    } else {
      $scope.Events.toggleAllContestsInProgress = false;
      contest.allSelected = !contest.allSelected;
    }
    return null;
  };

  $scope.Events.toggleAllSubSportsInProgress = false;
  $scope.Events.toggleAllSubSports = function (sport, cursor) {

    if (!sport || !sport.SportSubTypes || typeof sport.SportSubTypes === "undefined") return null;
    if (typeof cursor === "undefined") cursor = 0;
    if (cursor < sport.SportSubTypes.length) {
      $scope.Events.toggleAllSubSportsInProgress = true;
      if (!sport.allSelected && !sport.SportSubTypes[cursor].Selected) {
        manualSelectFix(sport, sport.SportSubTypes[cursor]);
        $sportsAndContestsService.ShowSubSport(sport, sport.SportSubTypes[cursor]).then(function () {
          //recursividad
          sport.SportSubTypes[cursor].Selected = !sport.allSelected;
          return $scope.Events.toggleAllSubSports(sport, ++cursor);
        });
      } else if (sport.allSelected) {
        $sportsAndContestsService.HideSubSport(sport, sport.SportSubTypes[cursor]);
        manualSelectFix(sport, sport.SportSubTypes[cursor]);
        //recursividad
        sport.SportSubTypes[cursor].Selected = !sport.allSelected;
        return $scope.Events.toggleAllSubSports(sport, ++cursor);
      }
      else if (sport.SportSubTypes[cursor].Selected) {
        return $scope.Events.toggleAllSubSports(sport, ++cursor);
      }
    } else {
      $scope.Events.toggleAllSubSportsInProgress = false;
      sport.allSelected = !sport.allSelected;
    }
    return null;
  };

  $scope.Events.ToggleTeasers = function (teaser, parentIndex, index, isOpenAllowed) {
    if (($ticketService.Ticket.Any() || $ticketService.IsOpenPlays()) && !$ticketService.Ticket.Posted()) {
      UI.Confirm("Change teaser type will close your current " + ($ticketService.IsOpenPlays() ? "open " : "") + "bet process, continue", function () {
        loadTeaser(teaser, parentIndex, index, isOpenAllowed);
      }, function () {
        return;
      });
    } else
      loadTeaser(teaser, parentIndex, index, isOpenAllowed);
  };

  $scope.ChangeWrapperLayout = function (path) {
    if (!path) path = $location.path();
    if (path == "") return;
    switch (path) {
      case 'casino':
      case '/casinos/livedealer':
      case '/casinos/virtualcasino':
        $scope.HideBetSlip();
        break;
      case '/liveBetting':
      case '/horsesApp':
        $scope.HideSideBars();
        break;
      default:
        if ($scope.ModalClass == "") $scope.ShowSideBars();

    }
  };

  $scope.GetCustomerRestriction = function (code) {
    for (var i = 0; $scope.Customer.Restrictions.length > i; i++) {
      if ($scope.Customer.Restrictions[i].Code == code) return $scope.Customer.Restrictions[i];
    }
    return null;
  };

  $scope.HideBetSlip = function () {
    $scope.PageWrapperClass = 'wrapper-left-sidebar';
  };

  $scope.HideSideBars = function () {
    $scope.PageWrapperClass = 'wrapper-no-sidebars';
  };

  $scope.ShowSideBars = function () {
    $scope.PageWrapperClass = 'wrapper';
  };

  $scope.MobileContinueOnClick = function () {
    $scope.ShowDialog('betSlip');
    $customerService.CustomerActionLogger("Mobile Continue");
  };

  $scope.CloseDialog = function () {
    $('#modalDialog').modal('hide');
    $scope.DialogTemplate = "";
  };

  $scope.SetActiveSideBarPane = function (paneNum) {
    $scope.ActiveSideBarPane = paneNum;
  };

  $scope.ChangePath = function (route) {
    switch (route) {
      case '/horsesApp':
        $customerService.CustomerActionLogger("Visit horses");
        _lastPath = route;
        if ($rootScope.IsMobileDevice) {
          var hform = document.createElement("form");
          hform.method = "GET";
          hform.action = SETTINGS.HorsesSite + '/BOSSWagering/Racebook/InternetBetTaker/';
          hform.target = $rootScope.IsMobileDevice ? "_blank" : "horsesIframe";

          var htk = document.createElement("input");
          htk.name = "SBUserName";
          htk.value = $customerService.Info.CustomerID.trim();
          htk.type = "hidden";
          hform.appendChild(htk);

          htk = document.createElement("input");
          htk.name = "SBPassword";
          htk.value = $customerService.Info.Password;
          htk.type = "hidden";
          hform.appendChild(htk);

          htk = document.createElement("input");
          htk.name = "SiteId";
          htk.value = "trdwd";
          htk.type = "hidden";
          hform.appendChild(htk);

          document.body.appendChild(hform);

          hform.submit();

          return;
        }
        $scope.path = route;
        $scope.HideSideBars();
        $scope.BetOfferingTemplate = appModule.Templates.thirdParties.horses;
        break;
      case '/liveDealer':
        $customerService.CustomerActionLogger("Visit liveDealer");
        $scope.path = '/casinosMenu';
        $scope.HideSideBars();
        $scope.BetOfferingTemplate = appModule.Templates.thirdParties.liveDealer;
        break;
      case '/liveBetting':
        $customerService.CustomerActionLogger("Visit live betting");

        if (!$rootScope.IsMobileDevice) {
          $scope.HideSideBars();
          $scope.BetOfferingTemplate = appModule.Templates.thirdParties.liveBetting;
        }

        var data;
        _caller.POST({}, 'GetLiveBettingToken').then(function (result) {
          data = result.data.d.Data;
        });

        setTimeout(function () {

          var lform = document.createElement("form");
          lform.method = "GET";
          lform.action = SETTINGS.LiveBettingSite + '/';
          lform.target = $rootScope.IsMobileDevice ? "_blank" : "liveBettingIframe";

          var tk = document.createElement("input");
          tk.name = "customerId";
          tk.value = data.CustomerId;
          tk.type = "hidden";
          lform.appendChild(tk);

          tk = document.createElement("input");
          tk.name = "tstamp";
          tk.value = data.TimeStamp;
          tk.type = "hidden";
          lform.appendChild(tk);

          tk = document.createElement("input");
          tk.name = "hash";
          tk.value = data.Hash;
          tk.type = "hidden";
          lform.appendChild(tk);

          tk = document.createElement("input");
          tk.name = "platform";
          tk.value = data.Platform;
          tk.type = "hidden";
          lform.appendChild(tk);

          document.body.appendChild(lform);
          lform.submit();
        }, 800);
        break;
      case '/casinosMenu':
        $scope.path = route;
        break;
      case '/casinos':
        $customerService.CustomerActionLogger("Visit casinos");
        _lastPath = route;
        $scope.path = '/casinosMenu';
        $scope.HideSideBars();
        $scope.BetOfferingTemplate = appModule.Templates.thirdParties.virtualCasino;
        break;
      case '/blur':
        $scope.path = ($scope.path == '/liveDealer' || $scope.path == '/horses' ? $scope.path : _lastPath);
        $('#casinoTab').removeClass("active");
        break;
      default:
        _lastPath = route;
        $scope.path = route;
        $scope.ShowSideBars();
        $scope.BetOfferingTemplate = appModule.Templates.betOffering;
        break;
    }
    //$location.path(route);
  };

  $scope.ShowPriorityMessages = function () {
    setTimeout(function () {
      if ($scope.Customer.Info != null && $scope.Customer.Info.PriorityMessages.length > 0) {
        $scope.Customer.Info.NewMessagesCount -= $scope.Customer.Info.PriorityMessages.length;
        var msgs = "";
        for (var i = 0; i < $scope.Customer.Info.PriorityMessages.length; i++) {
          msgs += $scope.Customer.Info.PriorityMessages[i].MsgBody + "<br\>";
        }
      }
    }, 500);
  };

  $scope.SpotlightSwitch = function (spotlightSwitch) {
    $scope.spotlight.Switch = !spotlightSwitch;
    if ($scope.spotlight.Switch) {
      $sportsAndContestsService.SpotlightContest();
      $sportsAndContestsService.SpotlightGame();
    } else {
      $sportsAndContestsService.ToggleSubSport(null, null, true);
    }
  };

  $scope.RemoveSpecials = function (text) {
    text.split(' ').join('_').replace(/[^\w\s]/gi, '');
  };

  $scope.BanMessageOnSession = function (messageId, messagesList) {
    if ($rootScope.bannedMessages == null) $scope.bannedMessages = [];
    messagesList.forEach(function (m, i) {
      if (m.MessageID == messageId) messagesList.splice(i, 1);
    });
    var bennedMessage =
    {
      MessageID: messageId,
      CustomerID: $customerService.Info.CustomerID.trim()
    };
    $rootScope.bannedMessages.push(bennedMessage);
    $webStorageService.session.setItem(2, $scope.bannedMessages);
  };

  $scope.SaveToggles = function (event, sufix) {
    var found = false;
    var id = sufix + event.target.id;
    for (var i = 0; i < _togglesArray.length; i++) {
      if (_togglesArray[i] == id) {
        _togglesArray.splice(i, 1);
        found = true;
      }
    }
    if (!found) _togglesArray.push(id);
    $webStorageService.session.setItem(0, _togglesArray);
  };

  $scope.SavedSelectedSport = function (sport, sub) {
    for (var i = 0; i < _savedSelected.length; i++) {
      if (_savedSelected[i].SubSport.SequenceNumber == sub.SequenceNumber && sub.Selectable) {
        sport.allSelected = _savedSelected[i].Sport.allSelected;
        $scope.Events.ToggleSubSport(sport, sub, true);
        return true;
      }
    }
    return false;
  };

  $scope.SavedSelectedContest = function (cnt, cnt2) {
    for (var i = 0; i < _savedSelected.length; i++) {
      if (_savedSelected[i].SubSport.ContestType2 == cnt2.ContestType2 && cnt2.Selectable) {
        cnt.allSelected = _savedSelected[i].Sport.allSelected;
        $scope.Events.ToggleContest(cnt, cnt2, true);
        return true;
      }
    }
    return false;
  };

  $scope.SelectOpenWager = function (wagerToOpen) {
    if ($rootScope.IsMobileDevice) {
      $scope.ChangeTap(1);
      $location.path("/");
      if (wagerToOpen.WagerType == 'P') $scope.ToggleMenu();
      setTimeout(function () {
        $scope.MobileContinueOnClick();
      }, 500);
    }
    $ticketService.TicketProcessed = false;
    $wagerTypesService.Selected = wagerToOpen.WagerType == 'P' ? $wagerTypesService.List[1] : $wagerTypesService.List[2];
    $ticketService.Ticket.OpenWager = wagerToOpen;
    $scope.CloseDialog();
    $wagerTypesService.Change();
  };

  $scope.OpenAllowed = function (wagerType) {
    if (!($wagerTypesService.Selected.id == 1 || $wagerTypesService.Selected.id == 2)) return false;
    if (typeof wagerType == "undefined") wagerType = $wagerTypesService.IsParlay() ? 'P' : 'T';
    if (!$wagerTypesService.IsAccumWager()) return false;
    if ($wagerTypesService.IsTeaser() && !_isOpenTeaser) return false;
    return $scope.GetCustomerRestriction(wagerType == "P" ? "PLOPEN" : "TEOPEN");
  };

  $scope.ToggleMenu = function () {
    $scope.CollapseCustomerBalanceInfo();
    $rootScope.ShowGrades = false;
    var deviceWidth = $(window).width();
    var wrapper = $("#wrapper");
    var header = $("#topHeader");
    var logout = $(".logout");
    //var footer = $("#page_footer");
    //var mToogle = $("#menu-toggle");
    if (deviceWidth < 767) {
      wrapper.toggleClass("active");
      if (header.hasClass("sb-expanded")) {
        setTimeout(function () {
          logout.hide();
        }, 20);
      } else {
        setTimeout(function () {
          logout.show();
        }, 500);
      }
      setTimeout(function () {
        header.toggleClass("sb-expanded");
      }, 1);
    } else {
      if (!wrapper.hasClass("active")) wrapper.toggleClass("active");
      if (!header.hasClass("sb-expanded")) header.toggleClass("sb-expanded");
      if (!$('#logout').is(':visible')) {
        setTimeout(function () {
          $("#logout").show();
        }, 500);
      }
    }
  };

  $scope.ShowMenu = function () {
    var deviceWidth = $(window).width();
    var wrapper = $("#wrapper");
    var header = $("#topHeader");
    var logout = $(".logout");
    if (deviceWidth < 767) {
      if (!wrapper.hasClass("active")) wrapper.addClass("active");
      if (!header.hasClass("sb-expanded")) {
        setTimeout(function () {
          logout.show();
        }, 500);
      }
      setTimeout(function () {
        header.addClass("sb-expanded");
      }, 500);
    }
  }

  $scope.ChangeTap = function (tab) {
    $rootScope.menuTab = tab;
    $rootScope.safeApply();
  };

  $scope.OpenLiveChat = function () {
    $customerService.CustomerActionLogger('Open live chat', $rootScope.IsMobileDevice ? "Mobile" : "Main");
    //lz_data = { overwrite: false, 111: $customerService.Info.CustomerID };
    window.open(SETTINGS.LiveChatSite + '/chat.php?ptn=' + $customerService.Info.CustomerID, '', 'width=590,height=610,left=0,top=0,resizable=yes,menubar=no,location=no,status=yes,scrollbars=yes');
  };

  $scope.PendingOpenBets = function () {
    if (!$ticketService.IsOpenPlays() || !$ticketService.openPlaysVal || ($ticketService.openPlaysVal.value > 8)
        || !$wagerTypesService.Selected || typeof $ticketService.openPlaysVal == "undefined") return "";
    //var totalWagerCount = $ticketService.TotalWagers() + $ticketService.openPlaysVal.value;
    var openvalue = $ticketService.openPlaysVal.value;
    return $translatorService.Translate("FILL") + ": " + openvalue + " "
            + $translatorService.Translate("OPEN_SPOTS");
  };

  var _searchLogOnce = true;
  $scope.SearchBlur = function () {
    if (_searchLogOnce) {
      _searchLogOnce = false;
      $customerService.CustomerActionLogger('Search term', $scope.SearchFilter.value).then(function () {
        _searchLogOnce = true;
      });
    }
  };

  var _scheduling = setTimeout(function tick() {
    $customerService.TrackStatus().then();
    _scheduling = setTimeout(tick, SETTINGS.TrackTime);
  }, SETTINGS.TrackTime);

  $scope.CollapseCustomerBalanceInfo = function () {
    if (!$rootScope.IsMobileDevice) return;
    $('#custBalInfo').collapse("hide");
  }

  if (window.sip && window.sip.split(".").length >= 3) $scope.WebServer = window.sip.split(".")[3];

  //RootScope Functions

  $rootScope.ShowDialog = function (page) {
    $rootScope.Page = page;
    $customerService.CustomerActionLogger("Visit " + page);
    switch (page) {
      case 'dailyfigures':
        $scope.DialogTemplate = $scope.Templates.reports.reports;
        $scope.ModalClass = 'printable';
        $scope.DefaultReport = 'dailyfigures';
        break;
      case 'openBets':
        $scope.DialogTemplate = $scope.Templates.reports.reports;
        $scope.ModalClass = '';
        $scope.DefaultReport = 'openbets';
        break;
      case 'rules':
        $scope.DialogTemplate = $scope.Templates.rules;
        $scope.ModalClass = 'in';
        break;
      case 'inbox':
        $scope.DialogTemplate = $scope.Templates.inbox;
        $scope.ModalClass = 'modal-inbox';
        break;
      case 'settings':
        $scope.DialogTemplate = $scope.Templates.settingsView;
        $scope.ModalClass = 'modal-settings in';
        updateLastServiceMessage();
        break;
      case 'pending':
        $scope.DialogTemplate = $scope.Templates.reports.reports;
        $scope.ModalClass = '';
        $scope.DefaultReport = 'openbets';
        break;
      case 'nonPostedCasino':
        $scope.DialogTemplate = $scope.Templates.nonpostedCasinoplays;
        $scope.ModalClass = 'modal-settings in';
        break;
      case 'betSlip':
        $scope.DialogTemplate = $scope.Templates.betSlip;
        $scope.ModalClass = 'modal-settings in modal-bs';
        break;
    }
    $rootScope.safeApply();
    $('#modalDialog').modal({
      backdrop: 'static',
      keyboard: false
    });

    $('#modalDialog').removeData("modal").modal({ backdrop: 'static', keyboard: false });

  };

  $rootScope.Logout = function (stopRedirect) {
    return $customerService.Logout().then(function () {
      window.ClearPersistendData();
      if (!stopRedirect) CommonFunctions.RedirecToPage('/');
    });
  };

  $rootScope.SwitchToClassic = function (mobile) {
    var customerId = $customerService.Info.CustomerID.trim();
    var customerPwd = $customerService.Info.Password;
    var login = $("#loginform");
    $customerService.CustomerActionLogger("Opening Classic", $rootScope.IsMobileDevice ? "Mobile" : "Main");
    $rootScope.Logout().then(function () {
      $('#customerID').val(customerId);
      $('#password').val(customerPwd);
      if (mobile) login.attr('action', SETTINGS.MobileSite);
      login.submit();
    });
  };

  $rootScope.IsWebSocketConnected = function () {
    return $webSocketService.IsConnected();
  };

  $rootScope.IsWebSocketSupported = function () {
    return $webSocketService.IsSupported();
  };

  $rootScope.GetWebSocketState = function () {
    return $webSocketService.connectionSate;
  };

  $rootScope.GetCurrentSocketServer = function () {
    return $webSocketService.GetCurrentServer();
  };

  $rootScope.GetLastMessage = function () {
    return $webSocketService.lastMessage;
  };

  window.ChangePath = function (route) {
    $scope.ChangePath(route);
  };

  window.ClearPersistendData = function () {
    $webStorageService.session.removeItem(0);
    $webStorageService.session.removeItem(1);
  };

  getMessageNotification();

  init();

}]);