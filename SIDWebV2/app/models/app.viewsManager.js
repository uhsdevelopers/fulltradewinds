﻿function ViewsManager($scope, $location, $timeout) {
    this._scope = $scope;
    this._localtion = $location;
    this._timeout = $timeout;
};

//Document_Function
ViewsManager.prototype.IsSportView = function (redirect) {
    if (!redirect) redirect = false;
    if (!betOfferingCtrl) {
        if (redirect) {
            log("redirect due No sport view");
            CommonFunctions.RedirecToPage("/");
        }
        return false;
    }
    return true;
};

//Document_Function
ViewsManager.prototype.ChangeView = function (view) {
    var parent = this;
    parent._timeout(function () {
        parent._localtion.path(view);
        if (!parent._scope.$$phase) {
            parent._scope.$apply();
        }
    });
};