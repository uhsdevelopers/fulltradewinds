﻿var appModule = angular.module("mainModule", ['ngRoute', 'angular-loading-bar']);
var appVersion = '20191004-1';
appModule.run(['$rootScope', function ($rootScope) {
  $rootScope.safeApply = function (fn) {
    var phase = this.$root.$$phase;
    if (phase == '$apply' || phase == '$digest') {
      if (fn && (typeof (fn) === 'function')) {
        fn();
      }
    } else {
      this.$apply(fn);
    }
  };

  window.innerDocClick = true;

  document.body.onmouseleave = function () {
    //User's mouse has left the page.
    window.innerDocClick = false;
  }

  document.body.onmouseover = function () {
    window.innerDocClick = true;
  }

  document.addEventListener('click', function () {
    window.innerDocClick = true;
  });

  $rootScope.$on('$locationChangeStart', function (event) {
    if (!window.innerDocClick) {
      event.preventDefault(); // This prevents the navigation from happening
    }

  });


}]);

appModule.Controllers = {
  app: "appController",
  header: "headerController",
  betOffering: "betOfferingController",
  betSlip: "betSlipController",
  inbox: "inboxController",
  reports: "reportsController",
  rules: "rulesController",
  nonPostedCasinoplays: "nonPostedCasinoplaysController",
  casinos: "casinosController",
  horses: "horsesController",
  liveBetting: "liveBettingCtrl",
  settings: "settingsController",
  loginCtrl: "loginController"
};

appModule.Templates = {
  main: "/app/components/main/mainView.html?v=" + appVersion,
  header: "/app/shared/header/header.html?v=" + appVersion,
  mobileHeader: "/app/shared/header/mobileHeader.html?v=" + appVersion,
  betSlip: "/app/components/betSlip/betSlipView.html?v=" + appVersion,
  inbox: "/app/components/inbox/inboxView.html?v=" + appVersion,
  rules: "/app/components/rules/rulesView.html?v=" + appVersion,
  nonpostedCasinoplays: "/app/components/nonPostedCasinoplays/nonPostedCasinoplaysView.html?v=" + appVersion,
  settingsView: "/app/components/settings/settingsView.html?v=" + appVersion,
  thirdParties: {
    horses: "/app/components/betOffering/horsesView.html?v=" + appVersion,
    liveDealer: "/app/components/betOffering/liveDealerView.html?v=" + appVersion,
    virtualCasino: "/app/components/betOffering/virtualCasinoView.html?v=" + appVersion,
    liveBetting: "/app/components/betOffering/liveBetting.html?v=" + appVersion
  },
  leftSidebar: {
    main: "/app/shared/sidebar/main.html?v=" + appVersion,
    settings: "/app/shared/sidebar/settings.html?v=" + appVersion,
    sportsMenu: "/app/shared/sidebar/sportsMenu.html?v=" + appVersion,
    sportsMenuAll: "/app/shared/sidebar/sportsMenuAll.html?v=" + appVersion,
    reportsMenu: "/app/shared/sidebar/reportsMenu.html?v=" + appVersion,
    rulesMenu: "/app/shared/sidebar/rulesMenu.html?v=" + appVersion,
    casinosMenu: "/app/shared/sidebar/casinosMenu.html?v=" + appVersion
  },
  reports: {
    reports: "/app/components/reports/reports.html?v=" + appVersion,
    selected: "",
    casinosMenu: "/app/shared/sidebar/casinosMenu.html?v=" + appVersion
  },
  betOffering: "/app/components/betOffering/betOfferingView.html?v=" + appVersion,
  betOfferingMobile: "/app/components/betOffering/betOfferingMobileView.html?v=" + appVersion
};


appModule.config(['$routeProvider',
  function ($routeProvider) {
    $routeProvider.
      when('/', {
        templateUrl: '/app/components/betOffering/betOfferingMobileView.html?v=' + appVersion,
        controller: 'betOfferingController',
        reloadOnSearch: false
      }).
      when('/rules/:ruleId', {
        templateUrl: '/app/components/rules/index.html?v=' + appVersion,
        controller: 'rulesController',
        reloadOnSearch: false
      }).
      when('/dailyFigures', {
        templateUrl: '/app/components/reportsMobile/DailyFigures.html?v=' + appVersion,
        controller: 'reportsController',
        reloadOnSearch: false
      }).
      when('/openBets', {
        templateUrl: '/app/components/reportsMobile/OpenBets.html?v=' + appVersion,
        controller: 'reportsController',
        reloadOnSearch: false
      }).
      otherwise({
        redirectTo: '/'
      });

    String.prototype.RemoveSpecials = function () {
      return this.replace(/[^\w\s]/gi, '').trim().split(' ').join('_');
    };

    Date.prototype.ToUTC = function () {
      return new Date(this.getUTCFullYear(), this.getUTCMonth(), this.getUTCDate(), this.getUTCHours(), this.getUTCMinutes(), this.getUTCSeconds());
    };


  }]);