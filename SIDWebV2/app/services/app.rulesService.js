﻿appModule.factory('$rulesService', ['$http', '$rootScope', function ($http, $rootScope) {

    var _caller = new ServiceCaller($http, null);
    var _rulesService = {};

    _rulesService.Rules = [];

    function groupMenuData(menuData) {
      var groupedMenu = new Array();
      var currentParent = null;

      $.each(menuData, function (idx, el) {
        if (groupedMenu.length == 0 || (groupedMenu.length > 0 && currentParent.id != el.parent_id)) {
          el.subMenu = new Array();
          currentParent = el;
          groupedMenu.push(currentParent);
        } else {
          currentParent.subMenu.push(el);
        }
      });
      return groupedMenu;
    };

    _rulesService.LoadMenus = function (lang) {

        lang = lang.toLowerCase();
        _rulesService.Rules = [];

        _caller.GET(SETTINGS.RulesSite + '/api/menus/get_menu/?menu_id=' + 3 + '&lang=' + lang).then(function (result) {
            _rulesService.Rules = [];
            _rulesService.Rules.push({ "label": "Sports Rules", "menu": groupMenuData(result.data.menu) });
            $rootScope.$broadcast("SportRules");
        });

        _rulesService.GetRuleContent = function (pageId, agentId) {
          return _caller.GET(SETTINGS.RulesSite + '/api/get_page/?id=' + pageId + '&custAgentId=' + agentId).then();
        };

    };

    return _rulesService;

}]);