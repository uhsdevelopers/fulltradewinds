appModule.factory('$encryptionService', ['$http', function ($http) {

  var _caller = new ServiceCaller($http, null, 'LoginService', 'CustomerSignIn');

  var _encryption = {};

  _encryption.CustomerPassCheck = function (customerId, password, obsoleteKey) {
    return _caller.POST({ 'customerId': customerId, 'password': password, 'obsoleteKey': obsoleteKey }, 'CheckCustomerPassword', null, true);
  };

  _encryption.RequestToken = function () {
    return _caller.POST({}, 'TemporalToken', null, true);
  };

  return _encryption;

}]);
