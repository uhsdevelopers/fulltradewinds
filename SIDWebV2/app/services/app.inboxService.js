﻿appModule.factory('$inboxService', ['$http', '$rootScope', '$customerService', '$sce', function ($http, $rootScope, $customerService, $sce) {

  var _caller = new ServiceCaller($http, null, 'inboxService');
  var _showInbox;
  var _inboxService = {
    ExpandClass: []
  };

  function getMessageIndex(customerMessageId) {
    var idx = -1;

    for (var i = 0; i < _inboxService.PendingMsgs.length; i++) {
      if (_inboxService.PendingMsgs[i].CustomerMessageId != customerMessageId)
        continue;
      idx = i;
      break;
    }
    return idx;
  };

  $rootScope.$on('customerInfoLoaded', function () {
    _inboxService.GetCustomerMessages();
  });

  _inboxService.InboxChanged = function (data) {
    _inboxService.ExpandClass = [];
    switch (data.MsgStatus) {
      case "D":
        _inboxService.RemoveMessage(data.CustomerMessageId);
        $rootScope.$broadcast("messageDeleted");
        break;
      case "U":
        if (data.MsgStatus.trim() == "D")
          break;
        _inboxService.InsertMessage(data);
        break;
    }
    $rootScope.safeApply();
  };

  _inboxService.FlagCustomerMessage = function (messageId, targetAction, internal) {
    if (typeof internal == "undefined") internal = false;
    _caller.POST({ 'messageId': messageId, 'targetAction': targetAction }, 'FlagCustomerMessage', null, true).then(function () { if (!internal) _inboxService.GetCustomerMessages() });
  };

  _inboxService.GetCustomerMessages = function () {
    if ($rootScope.RecentTicketProcessed) return;
    if ($customerService.Info) {
      _caller.POST({ customerId: $customerService.Info.CustomerID }, 'GetCustomerMessages').then(function (result) {
        _inboxService.PendingMsgs = result.data.d.Data;
        var i = 0;
        _inboxService.PendingMsgs.Unread = i;
        angular.forEach(_inboxService.PendingMsgs, function (msg) {
          msg.MsgBody = $sce.trustAsHtml(msg.MsgBody);
          if ((msg.MsgPriority == "H") || (msg.MsgPriority == "M" && msg.MsgStatus == "U")) {
            _inboxService.ExpandClass[i] = true;
            _showInbox = true;
            if (msg.MsgPriority == "H") {
              msg.MsgStatus = "R";
              _inboxService.FlagCustomerMessage(msg.CustomerMessageId, 'Read', true);
            }
          }
          if (msg.MsgStatus == "U")
            _inboxService.PendingMsgs.Unread++;
          i++;
        });
        $rootScope.$broadcast('customerMsgsLoaded');
        if (_showInbox) {
          $rootScope.ShowDialog("inbox");
        };
      });
    }
  };

  _inboxService.InsertMessage = function (message) {
    message.MsgBody = $sce.trustAsHtml(message.MsgBody);
    _inboxService.PendingMsgs.unshift(message);
    var i = 0;
    _inboxService.PendingMsgs.Unread = 0;
    angular.forEach(_inboxService.PendingMsgs, function (msg) {
      if ((msg.MsgPriority == "H") || (msg.MsgPriority == "M" && msg.MsgStatus == "U")) {
        _inboxService.ExpandClass[i] = true;
        _showInbox = true;
      }
      if (msg.MsgStatus == "U")
        _inboxService.PendingMsgs.Unread++;
      i++;
    });
    $rootScope.$broadcast('customerMsgsLoaded');
    if (_showInbox) {
      $rootScope.ShowDialog("inbox");
    };
  };

  _inboxService.RemoveMessage = function (customerMessageId) {
    var idx = getMessageIndex(customerMessageId);
    if (idx >= 0)
      _inboxService.PendingMsgs.splice(idx, 1);
    var i = 0;
    _inboxService.PendingMsgs.Unread = 0;
    angular.forEach(_inboxService.PendingMsgs, function (msg) {
      if (msg.MsgStatus == "U")
        _inboxService.PendingMsgs.Unread++;
      i++;
    });
    $rootScope.$broadcast('customerMsgsLoaded');
  };

  _inboxService.GetMessageNotification = function () {
    return _caller.POST({}, 'GetMessageNotification');
  };

  _inboxService.GetMessageNotificationBySport = function (a, b) {
    return _caller.POST({ sport: a, subSport: b }, 'GetMessageNotificationBySport', null, true);
  };

  _inboxService.GetMessageNotification = function () {
    return _caller.POST({}, 'GetMessageNotification');
  };

  _inboxService.GetMessageNotificationBySport = function (a, b) {
    return _caller.POST({ sport: a, subSport: b }, 'GetMessageNotificationBySport', null, true);
  };

  return _inboxService;

}]);