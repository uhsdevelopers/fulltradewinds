﻿appModule.factory('$ticketService', ['$http', '$wagerTypesService', '$translatorService', '$systemService', '$customerService', '$rootScope', '$errorHandler', function ($http, $wagerTypesService, $translatorService, $systemService, $customerService, $rootScope, $errorHandler) {

    var _caller = new ServiceCaller($http, '', 'TicketService');

    var _ticketService = {
        Ticket: {
            TotalRiskAmount: 0,
            TotalToWinAmount: 0,
            UseFreePlay: false,
            PlayCount: 0,
            TicketNumber: null,
            WagerItems: [],
            OpenWagerItems: [],
            TeaserName: null,
            TeaserInfo: null,
            ParlayInfo: null,
            RoundRobin: {
                Selected: null,
                Options: []
            },
            AllowedWagerPicks: {
                MinPicks: 1,
                MaxPicks: 999,
                OpenPicks: 999
            },
            wSID: 0,
            Password: "",
            RiskMax: 0,
            ToWinMax: 0,
            KeepOpenPlay: false
        },
        SubWagerTypes: {
            Spread: 'S',
            MoneyLine: 'M',
            TotalPoints: 'L',
            TeamTotals: 'E'
        },
        openPlays: [],
        ShowOpenPlayItems: false,
        openPlaysVal: {
            name: "",
            value: 0
        },
        OpenWager: null,
        TicketProcessed: true,
        IsMoverReady: false,
        OpenDisable: false,
        PlacingBet: false
    };

    var _teamsString = ' ' + $translatorService.Translate('open spots');

    _ticketService.openPlaysSelection = [
      {
          name: $translatorService.Translate('No Open Spot'),
          value: _ticketService.Ticket.AllowedWagerPicks.MaxPicks
      }, {
          name: 1 + _teamsString,
          value: 1
      }, {
          name: 2 + _teamsString,
          value: 2
      }, {
          name: 3 + _teamsString,
          value: 3
      }, {
          name: 4 + _teamsString,
          value: 4
      }, {
          name: 5 + _teamsString,
          value: 5
      }, {
          name: 6 + _teamsString,
          value: 6
      }, {
          name: 7 + _teamsString,
          value: 7
      }, {
          name: 8 + _teamsString,
          value: 8
      }
    ];
    _ticketService.OpenPlayDropDown = [];
    _ticketService.ContinuePressed = false;


    function prepareWagerData() {
        var wagerItemsData = [];
        var i;
        for (i = 0; i < _ticketService.Ticket.WagerItems.length; i++) {
            var wagerItem = _ticketService.Ticket.WagerItems[i];
            if (wagerItem.Available == false) continue;
            if ($wagerTypesService.IsAccumWager()) {
                var indexWagerItems = _ticketService.TotalWagers() - 1;
                if ($wagerTypesService.IsParlay()) window.WagerAmountOnChange(wagerItem, 'R', indexWagerItems);
                else if ($wagerTypesService.IsTeaser()) window.WagerAmountOnChange(wagerItem, 'R', indexWagerItems);
                wagerItem.RiskAmt = _ticketService.Ticket.TotalRiskAmount;
                wagerItem.ToWinAmt = _ticketService.Ticket.TotalToWinAmount;
                if ($wagerTypesService.IsActionReverse()) wagerItem.AmountEntered = "RiskAmt";
            }

            var finalLine = wagerItem.FinalLine;
            var finalPrice = wagerItem.FinalPrice;

            if (wagerItem.SelectedLine != null && wagerItem.WagerType != "M" && wagerItem.WagerType != "E") { // In case of buy points
                finalLine = parseFloat(wagerItem.SelectedLine.points);
                finalPrice = wagerItem.SelectedLine.lineAdj;
                wagerItem.FinalLine = wagerItem.SelectedLine.points;
                wagerItem.FinalPrice = wagerItem.SelectedLine.cost;
            }
            if (!wagerItem.ToWinAmt) wagerItem.ToWinAmt = 0;
            var obj;
            if (wagerItem.Type == 'G') {
                var roundRobin = _ticketService.GetSelectedRoundRobin();
                var rrAmt = 0;
                if (roundRobin != null && roundRobin.value != 0) {
                    var rrValues = ParlayFunctions.CalculateRoundRobinToWin(_ticketService.Ticket.WagerItems, _ticketService.Ticket.ParlayInfo, _ticketService.Ticket.TotalRiskAmount, roundRobin.value);
                    rrAmt = wagerItem.RiskAmt * rrValues.PlayCount;
                }
                obj = {
                    'RiskAmount': wagerItem.RiskAmt,
                    'ToWinAmount': wagerItem.ToWinAmt,
                    'RrAmount': rrAmt,
                    'WagerAmt': wagerItem.RiskAmt,
                    'ControlCode': wagerItem.ControlCode,
                    'FinalLine': finalLine,
                    'FinalPrice': finalPrice,
                    'GameNum': wagerItem.Loo.GameNum,
                    'PeriodNumber': wagerItem.Loo.PeriodNumber,
                    'AmountEntered': wagerItem.AmountEntered,
                    'pitcher1ReqFlag': wagerItem.WagerType == "M" ? wagerItem.Pitcher1ReqFlag : true,
                    'pitcher2ReqFlag': wagerItem.WagerType == "M" ? wagerItem.Pitcher2ReqFlag : true,
                    'RoundRobinValue': (roundRobin != null) ? roundRobin.value : 0,
                    'PlayCount': _ticketService.Ticket.PlayCount,
                    'ArAmount': _ticketService.Ticket.ArAmount
                };
            } else
                obj = {
                    'RiskAmount': wagerItem.RiskAmt,
                    'ToWinAmount': wagerItem.ToWinAmt,
                    'WagerAmt': wagerItem.RiskAmt,
                    'ContestNum': wagerItem.Loo.ContestNum,
                    'ContestantNum': wagerItem.Loo.ContestantNum,
                    'FinalPrice': wagerItem.Loo.MoneyLine,
                    'FinalLine': wagerItem.Loo.ThresholdLine,
                    'ToBase': wagerItem.Loo.ToBase
                };
            wagerItemsData.push(obj);
        };
        return wagerItemsData;
    };

    function newWagerItem(wager) {
        wager.Loo = [];
        return wager;
    };

    function emptyTicket() {
        _ticketService.Ticket.TotalRiskAmount = 0;
        _ticketService.Ticket.TotalToWinAmount = 0;
        _ticketService.Ticket.RRTotalRiskAmount = 0;
        _ticketService.Ticket.TicketNumber = null;
        _ticketService.Ticket.WagerItems = [];
        _ticketService.Ticket.OpenWagerItems = [];
        _ticketService.Ticket.RoundRobin = {};
        _ticketService.Ticket.wSID = Math.random() * (70 - 1) + 1;
        _ticketService.Ticket.UseFreePlay = false;
        _ticketService.Ticket.KeepOpenPlay = false;
        _ticketService.PlacingBet = false;
        _ticketService.SetOpenPlaysVal(_ticketService.OpenPlayDropDown[0]);
    };

    function getLeagueBuyPointsInfo(sportType, sportSubType, controlCode, wagerType, chosenTeamId, periodNumber) {
        return _caller.POST({ 'sportType': sportType, 'sportSubType': sportSubType, 'controlCode': controlCode, 'wagerType': wagerType, 'chosenTeamId': chosenTeamId, 'periodNumber': periodNumber }, 'GetLeagueBuyPointsInfo', null, true).then();
    };

    function createBuyPointsOptions(wi) {
        if (wi.Loo.PeriodNumber > 0 ||
            wi.Loo.PreventPointBuyingFlag == 'Y' ||
            (wi.Loo.SportType != "Football" && wi.Loo.SportType != "Basketball") ||
            wi.WagerType == 'M' ||
            wi.WagerType == 'E' ||
            (!$wagerTypesService.IsParlay() && !$wagerTypesService.IsStraightBet())) return false;

        var bpInfo = null;
        getLeagueBuyPointsInfo(wi.Loo.SportType, wi.Loo.SportSubType, wi.ControlCode, $wagerTypesService.Selected.name, wi.ChosenTeamId, wi.Loo.PeriodNumber).then(function (result) {
            bpInfo = result.data.d.Data;
            if (bpInfo != null && bpInfo.length > 0) {
                var cont = 0;
                wi.BuyPoints = new Array();
                bpInfo.forEach(function (bi) {
                    var adjustment = "";
                    var spreadOrTotal = bi.SpreadOrTotal == 0 ? 'pk' : LineOffering.ConvertToHalfSymbol(bi.SpreadOrTotal, SETTINGS.MaxDenominator);
                    switch (wi.PriceType) {
                        case "A":
                            adjustment = bi.Adjustment > 0 ? '+' + bi.Adjustment : bi.Adjustment;
                            break;
                        case "D":
                            var decimalPrice = CommonFunctions.ConvertPriceToDecimal(bi.Adjustment, SETTINGS.DecimalPrecision); //pending decimalPrecision
                            adjustment += decimalPrice;
                            if (decimalPrice == Math.floor(decimalPrice)) {
                                adjustment += ".0";
                            }
                            break;
                        case "F":
                            decimalPrice = CommonFunctions.ConvertPriceToDecimal(bi.Adjustment, 0);
                            var fractionAry = CommonFunctions.ConvertDecimalToFraction(decimalPrice, SETTINGS.MaxDenominator);
                            adjustment += fractionAry[0];
                            adjustment += "/";
                            adjustment += fractionAry[1];
                            break;
                    }
                    wi.BuyPoints[cont++] = {
                        'lineAdj': bi.Adjustment,
                        'points': bi.SpreadOrTotal,
                        'strPoints': bi.SpreadOrTotal > 0 ? '+' + spreadOrTotal : spreadOrTotal,
                        'cost': adjustment
                    };
                    if (bi.SpreadOrTotal == wi.FinalLine) wi.SelectedLine = wi.BuyPoints[cont - 1];
                });
                if (!wi.SelectedLine) wi.BuyPoints = null;
            }
            else wi.SelectedLine = null;
            $rootScope.$broadcast('buyPointsInfoComplete');
        });
        return true;
    };

    function getBlockSTFlags() {
        return {
            QuarterInetParlayFlag: $systemService.Parameters.BlockStQuarterInetParlayFlag,
            BaseballFlag: $systemService.Parameters.BlockStBaseballFlag,
            HockeyFlag: $systemService.Parameters.BlockStHockeyFlag
        };
    };

    function revertGameWagerSelection(wagerItem, subWagerType, teamPos) {
        switch (subWagerType) {
            case _ticketService.SubWagerTypes.Spread:
                if (teamPos == 1) wagerItem.Spread1Selected = false;
                else wagerItem.Spread2Selected = false;
                break;
            case _ticketService.SubWagerTypes.MoneyLine:
                if (teamPos == 1) wagerItem.MoneyLine1Selected = false;
                else if (teamPos == 2) wagerItem.MoneyLine2Selected = false;
                else wagerItem.MoneyLine3Selected = false;
                break;
            case _ticketService.SubWagerTypes.TotalPoints:
                if (teamPos == 1) wagerItem.TotalPoints1Selected = false;
                else wagerItem.TotalPoints2Selected = false;
                break;
            case _ticketService.SubWagerTypes.TeamTotals:
                if (teamPos == 1) wagerItem.Team1TtlPtsAdj1Selected = false;
                else if (teamPos == 2) wagerItem.Team2TtlPtsAdj1Selected = false;
                else if (teamPos == 3) wagerItem.Team1TtlPtsAdj2Selected = false;
                else wagerItem.Team2TtlPtsAdj2Selected = false;
                break;
        }
    };

    function validatePicks() {
        var totalPicks = _ticketService.Ticket.WagerItems.length + _ticketService.Ticket.OpenWagerItems.length;
        if (_ticketService.IsOpenPlays() && totalPicks > _ticketService.OpenTotalPicks) {
            $rootScope.ErrorMessage = "PLEASE SELECT THE NUMBER OF TEAMS OF YOUR OPEN PLAY";
            $errorHandler.Error($rootScope.ErrorMessage + ' Teaser: ' + _ticketService.Ticket.TeaserName, '_ValidatePicks');
            return false;
        }
        if (!_ticketService.IsOpenPlays() && _ticketService.Ticket.WagerItems.length + (_ticketService.Ticket.KeepOpenPlay ? _ticketService.openPlaysVal.value : 0) < _ticketService.Ticket.AllowedWagerPicks.MinPicks) {
            $rootScope.ErrorMessage = "MINIMUM WAGER PICKS " + (_ticketService.Ticket.AllowedWagerPicks.MinPicks > 1 ? "ARE " : "IS ") + _ticketService.Ticket.AllowedWagerPicks.MinPicks;
            return false;
        } else if (_ticketService.Ticket.WagerItems.length > _ticketService.Ticket.AllowedWagerPicks.MaxPicks) {
            $rootScope.ErrorMessage = "MAXIMUM WAGER PICKS " + (_ticketService.Ticket.AllowedWagerPicks.MaxPicks > 1 ? "ARE " : "IS ") + _ticketService.Ticket.AllowedWagerPicks.MaxPicks;
            return false;
        } else
            return true;
    };

    function validateWagerItemsAmounts() {
        if (!_ticketService.Ticket.Any()) return false;
        var ret = true;
        if ($wagerTypesService.IsAccumWager()) {
            if (!CommonFunctions.IsNumeric(_ticketService.Ticket.TotalRiskAmount)) {
                ret = false;
            }
            else if (!$wagerTypesService.IsActionReverse() && !CommonFunctions.IsNumeric(_ticketService.Ticket.TotalToWinAmount)) {
                ret = false;
            }
        } else {
            for (var i = 0; i < _ticketService.Ticket.WagerItems.length; i++) {
                var wagerItem = _ticketService.Ticket.WagerItems[i];
                if (!CommonFunctions.IsNumeric(wagerItem.RiskAmt)) {
                    ret = false;
                    break;
                }
                else if (!CommonFunctions.IsNumeric(wagerItem.ToWinAmt)) {
                    ret = false;
                    break;
                }
                if (i > 0 && $wagerTypesService.IsIfBet()) {
                    var previousWagerItem = _ticketService.Ticket.WagerItems[i - 1];
                    //Allan
                    if ($customerService.Info.CreditAcctFlag != "Y" && wagerItem.RiskAmt > previousWagerItem.RiskAmt) {
                        ret = false;
                    }
                }
            }
        }
        if (!ret) {
            $rootScope.ErrorMessage = $translatorService.Translate("INVALID_AMOUNT");
        }
        return ret;
    };

    function removeAllWagerItems() {
        for (var i = _ticketService.Ticket.WagerItems.length - 1; i >= 0; i--) {
            var wi = _ticketService.Ticket.WagerItems[i];
            if (wi.Type == "G") {
                var wagerItemTeamPos = wi.ControlCode.substring(2, 1);
                revertGameWagerSelection(wi.Loo, wi.WagerType, wagerItemTeamPos);
            } else {
                wi.Loo.Selected = false;
            }
            _ticketService.Ticket.WagerItems.splice(i, 1);

        }
    };

    function unSelectAllWagerItems() {
        for (var i = _ticketService.Ticket.WagerItems.length - 1; i >= 0; i--) {
            var wi = _ticketService.Ticket.WagerItems[i];
            var wagerItemTeamPos;
            if (wi.Type == "G") {
                wagerItemTeamPos = wi.ControlCode.substring(2, 1);
                revertGameWagerSelection(wi.Loo, wi.WagerType, wagerItemTeamPos);
            }
            if (wi.Type == "C") {
                wi.Loo.Selected = false;
            }
        }
    };

    function getRoundRobinOptions() {
        if (!$wagerTypesService.IsParlay()) return null;
        _ticketService.Ticket.RoundRobin.Options = ParlayFunctions.GetRoundRobin(_ticketService.Ticket.WagerItems.length, $translatorService);
        _ticketService.Ticket.RoundRobin.Selected = _ticketService.Ticket.RoundRobin.Options[0];
        _ticketService.OpenDisable = false;
        return true;
    };

    function getParlayInfo(parlayName) {
        if (!$wagerTypesService.IsParlay()) return null;
        var data = { 'parlayName': parlayName, 'pickCount': 0 };
        getRoundRobinOptions();
        return _caller.POST(data, 'GetParlayInfo', null, true).then(function (result) {
            _ticketService.Ticket.ParlayInfo = result.data.d.Data;
        });
    };

    function getTeaserInfo(teaserName) {
        if (!$wagerTypesService.IsTeaser()) return null;
        var data = {
            'teaserName': teaserName, 'pickCount': (_ticketService.Ticket.KeepOpenPlay && _ticketService.openPlaysVal ? _ticketService.openPlaysVal.value + _ticketService.TotalWagers() : _ticketService.TotalWagers()), 'gamesWon': 0
        };
        return _caller.POST(data, 'GetTeaserInfo', null, true).then(function (result) {
            _ticketService.Ticket.TeaserInfo = result.data.d.Data;
        });
    };

    function replaceGameWager(wagerItem) {
        wagerItem.Type = "G";
        wagerItem.RiskPlaceHolder = $translatorService.Translate("Risk_Amt");
        wagerItem.WinPlaceHolder = $translatorService.Translate("Win_Amt");
        wagerItem.RiskAmt = 0;
        wagerItem.ToWinAmt = 0;
        createBuyPointsOptions(wagerItem);
        var found = false;
        for (var i = 0; i < _ticketService.Ticket.WagerItems.length; i++) {
            if (_ticketService.Ticket.WagerItems[i].Loo.GameNum == wagerItem.Loo.GameNum
                && _ticketService.Ticket.WagerItems[i].Loo.PeriodNumber == wagerItem.Loo.PeriodNumber
                && _ticketService.Ticket.WagerItems[i].WagerType == wagerItem.WagerType
                && _ticketService.Ticket.WagerItems[i].ChosenTeamId == wagerItem.ChosenTeamId
                && _ticketService.Ticket.WagerItems[i].ControlCode == wagerItem.ControlCode) {
                _ticketService.Ticket.WagerItems[i] = wagerItem;
                found = true;
                break;
            }
        }
        return found;
    };

    function riskChanged() {
        var indexWagerItems = _ticketService.Ticket.WagerItems.length + _ticketService.Ticket.OpenWagerItems.length - 1;
        if (_ticketService.Ticket.WagerItems.length > 0)
            window.WagerAmountOnChange(_ticketService.Ticket.WagerItems[_ticketService.Ticket.WagerItems.length - 1], 'R', indexWagerItems);
        else if (_ticketService.Ticket.OpenWagerItems.length > 0)
            window.WagerAmountOnChange(_ticketService.Ticket.OpenWagerItems[_ticketService.Ticket.OpenWagerItems.length - 1], 'R', indexWagerItems);
    };

    _ticketService.StartNewTicket = function () {
        emptyTicket();
        var startNewTicketCall = function () {
            return _caller.POST({ 'wSID': _ticketService.Ticket.wSID }, 'StartNewTicket', null, true).then(function () {
                _ticketService.GetWagerPicks().then();
                if (_ticketService.Ticket.OpenWager) {
                    setTimeout(function () {
                        _ticketService.GetOpenPlaysItems(_ticketService.Ticket.OpenWager);
                        _ticketService.Ticket.OpenWager = null;
                    }, 1000);
                };
            });
        };
        _ticketService.Ticket.UseFreePlay = false;
        if ($wagerTypesService.IsParlay()) return getParlayInfo(null).then(function () { startNewTicketCall(); });
        else if ($wagerTypesService.IsTeaser()) return getTeaserInfo(_ticketService.Ticket.TeaserName).then(function () { startNewTicketCall(); });
        else return startNewTicketCall().then();
    };

    _ticketService.UpdateGameWager = function (wagerItem, isAutoAccept, isFromSocket) {
        wagerItem.Type = "G";
        wagerItem.RiskPlaceHolder = $translatorService.Translate("Risk_Amt");
        wagerItem.WinPlaceHolder = $translatorService.Translate("Win_Amt");
        var found = false;
        var wi;
        var priceChanged;
        var i;
        for (i = 0; i < _ticketService.Ticket.WagerItems.length; i++) {
            wi = _ticketService.Ticket.WagerItems[i];
            if (wi.Loo.GameNum == wagerItem.Loo.GameNum
                && wi.Loo.PeriodNumber == wagerItem.Loo.PeriodNumber
                && wi.WagerType == wagerItem.WagerType
                && wi.ChosenTeamId == wagerItem.ChosenTeamId
                && (!wagerItem.Available || !wagerItem.IsOk || wagerItem.Changed || wagerItem.ChangedInCostumerFavor || _ticketService.Ticket.Posted())) {
                wi.AdjustabeOddsFlag = wagerItem.AdjustabeOddsFlag;
                //wi.AmountEntered = wagerItem.AmountEntered;
                wi.Changed = (wagerItem.Changed || !wagerItem.Available || !wagerItem.IsOk);
                wi.DecimalPrice = wagerItem.DecimalPrice;
                wi.Denominator = wagerItem.Denominator;
                wi.EasternLine = wagerItem.EasternLine;
                wi.EasternLineFlag = wagerItem.EasternLineFlag;
                wi.FinalDecimal = wagerItem.FinalDecimal;
                wi.FinalDenominator = wagerItem.FinalDenominator;
                wi.FinalLine = wagerItem.FinalLine;
                wi.FinalNumerator = wagerItem.FinalNumerator;
                wi.FinalPrice = wagerItem.FinalPrice;
                wi.HalfPointAdded = wagerItem.HalfPointAdded;
                wi.HalfPointValue = wagerItem.HalfPointValue;
                wi.Line = wagerItem.Line;
                wi.MaxWagerLimit = wagerItem.MaxWagerLimit;
                wi.Numerator = wagerItem.Numerator;
                wi.Pitcher1ReqFlag = wagerItem.Pitcher1ReqFlag;
                wi.Pitcher2ReqFlag = wagerItem.Pitcher2ReqFlag;
                wi.PointsBought = wagerItem.PointsBought;
                priceChanged = wi.Price != wagerItem.Price;
                wi.Price = wagerItem.Price;
                wi.RifTicketNumber = wagerItem.RifTicketNumber;
                wi.RifWagerNumber = wagerItem.RifWagerNumber;
                wi.RifWinOnly = wagerItem.RifWinOnly;
                wi.RiskPlaceHolder = wagerItem.RiskPlaceHolder;
                wi.WinPlaceHolder = wagerItem.WinPlaceHolder;
                wi.Available = wagerItem.Available;
                wi.IsOk = wagerItem.IsOk;
                wi.RiskAmt = wagerItem.riskAmt;
                wi.ToWinAmt = wagerItem.toWinAmt;
                found = true;
                if (wagerItem.Changed && !_ticketService.Ticket.Posted()) UI.Notify($translatorService.Translate("Line") + ": " + wi.ChosenTeamId + '. '
                    + $translatorService.Translate("Has changed, please review."), UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
                break;
            }
        }
        if (!wagerItem.IsExistingOpenItem && found) {
            window.UpdateLineOffering(wagerItem.Loo, wi, isFromSocket);
            if ((!isAutoAccept) && ((!_ticketService.Ticket.Posted() && !wagerItem.ChangedInCostumerFavor) || priceChanged)) {
                wi.RiskAmt = null;
                wi.ToWinAmt = null;
            } else {
                if (priceChanged) {
                    $rootScope.$on('buyPointsInfoComplete', function () {
                        window.WagerAmountOnChange(wi, wi.AmountEntered == 'ToWinAmount' ? 'W' : 'R', i);
                    });
                } else {
                    wi.RiskAmt = wagerItem.RiskAmt > 0 ? CommonFunctions.RoundNumber(wagerItem.RiskAmt) : wi.RiskAmt;
                    wi.ToWinAmt = wagerItem.ToWinAmt > 0 ? CommonFunctions.RoundNumber(wagerItem.ToWinAmt) : wi.ToWinAmt;
                }
            }
            $rootScope.safeApply();
        }
        return i;
    };

    _ticketService.AddGameWagerItem = function (gameItem, subWagerType, teamPos, wagerType) {
        if (typeof wagerPos == "undefined") wagerPos = -1;
        if (typeof update == "undefined") update = false;
        var toWinAmount = 0;
        var riskAmount = 0;
        _ticketService.Ticket.ArAmount = null;
        var addWagerItemFn = function () {
            if ($wagerTypesService.IsAccumWager() && _ticketService.IsOpenPlays()) {
                var maxToEval;
                var amountToEval;
                var totalWagers = (_ticketService.IsOpenPlays() ? _ticketService.OpenTotalPicks : _ticketService.TotalWagers());
                if ($wagerTypesService.IsParlay()) amountToEval = _ticketService.Ticket.TotalRiskAmount;
                else amountToEval = (($wagerTypesService.IsTeaser() && totalWagers > 2) ? _ticketService.Ticket.TotalRiskAmount : _ticketService.Ticket.TotalToWinAmount) * 100;
                switch (subWagerType) {
                    case "S":
                        maxToEval = gameItem.CircledMaxWagerSpread && gameItem.CircledMaxWagerSpread != "" && gameItem.CircledMaxWagerSpread < gameItem.SportLimits.MaxWagerSpread ? gameItem.CircledMaxWagerSpread : gameItem.SportLimits.MaxWagerSpread;
                        if (amountToEval > maxToEval) {
                            UI.Alert($translatorService.Translate("Cannot add this item, game is circled or has a lower limit."));
                            revertGameWagerSelection(gameItem, subWagerType, teamPos);
                            return false;
                        }
                        break;
                    case "M":
                        maxToEval = gameItem.CircledMaxWagerMoneyLine && gameItem.CircledMaxWagerMoneyLine != "" && gameItem.CircledMaxWagerMoneyLine < gameItem.SportLimits.MaxWagerMoneyLine ? gameItem.CircledMaxWagerMoneyLine : gameItem.SportLimits.MaxWagerMoneyLine;
                        if (amountToEval > maxToEval) {
                            UI.Alert($translatorService.Translate("Cannot add this item, game is circled or has a lower limit."));
                            revertGameWagerSelection(gameItem, subWagerType, teamPos);
                            return false;
                        }
                        break;
                    case "L":
                        maxToEval = gameItem.CircledMaxWagerTotal && gameItem.CircledMaxWagerTotal != "" && gameItem.CircledMaxWagerTotal < gameItem.SportLimits.MaxWagerTotal ? gameItem.CircledMaxWagerTotal : gameItem.SportLimits.MaxWagerTotal;
                        if (amountToEval > maxToEval) {
                            UI.Alert($translatorService.Translate("Cannot add this item, game is circled or has a lower limit."));
                            revertGameWagerSelection(gameItem, subWagerType, teamPos);
                            return false;
                        }
                }
                if (wagerType.id == $wagerTypesService.Ids.ActionReverse) {

                    if (_ticketService.Ticket.WagerItems.length >= 2) {
                        UI.Alert($translatorService.Translate("MAX_AR_BET_ITEMS_EXCEEDED"));
                        revertGameWagerSelection(gameItem, subWagerType, teamPos);
                        return false;
                    }

                    var currentItem = new Object();
                    currentItem.SubWagerType = subWagerType;
                    currentItem.Loo = gameItem;

                    if (!IfBetFunctions.ValidateIfBetTeamSelection(_ticketService.Ticket.WagerItems, currentItem, wagerType.name, getBlockSTFlags())) {
                        UI.Alert($translatorService.Translate("INVALID_BET_SELECTION"));
                        revertGameWagerSelection(gameItem, subWagerType, teamPos);
                        return false;
                    }
                }
            }
            return _caller.POST({
                'subWagerType': subWagerType,
                'gameNumber': gameItem.GameNum,
                'periodNumber': gameItem.PeriodNumber,
                'chosenTeamIdx': teamPos,
                'riskAmt': riskAmount,
                'toWinAmt': toWinAmount,
                'wSID': _ticketService.Ticket.wSID,
                'isLineUpdate': update,
                'keepOpenPlay': _ticketService.Ticket.KeepOpenPlay || _ticketService.IsOpenPlays()
            }, 'AddGameWagerItem', null, true).then(function (result) {
                if (result.data.d.Code != ServiceCaller.ResultCode.Success) {
                    revertGameWagerSelection(gameItem, subWagerType, teamPos);
                    $rootScope.ErrorMessage = $translatorService.Translate(result.data.d.Message);
                    if ($rootScope.IsMobileDevice) UI.Alert($rootScope.ErrorMessage);
                    return false;
                } else {
                    $rootScope.ErrorMessage = "";
                    if ($wagerTypesService.IsTeaser() || $wagerTypesService.IsParlay())
                        if (_ticketService.IsOpenPlays()) {
                            _ticketService.SetOpenPlaysVal(_ticketService.OpenPlayDropDown[_ticketService.OpenPlayDropDown.indexOf(_ticketService.openPlaysVal) - 1]);
                        } else {
                            if (_ticketService.openPlaysVal.value == _ticketService.OpenPlayDropDown[_ticketService.OpenPlayDropDown.length - 1].value)
                                _ticketService.SetOpenPlaysVal(_ticketService.OpenPlayDropDown[_ticketService.OpenPlayDropDown.length - 2]);
                            _ticketService.OpenPlayDropDown.splice(-1, 1);

                        }
                    var wagerItem = result.data.d.Data;
                    wagerItem.Available = true;
                    wagerItem.Loo = gameItem;
                    if (!replaceGameWager(wagerItem)) {
                        _ticketService.Ticket.WagerItems.push(wagerItem);
                    }
                    if ($wagerTypesService.IsAccumWager()) {
                        if (_ticketService.Ticket.KeepOpenPlay && _ticketService.openPlaysVal && _ticketService.openPlaysVal.value < 9 && _ticketService.openPlaysVal.value != _ticketService.openPlaysSelection[0].value)
                            _ticketService.Ticket.AllowedWagerPicks.OpenPicks = _ticketService.openPlaysVal.value + _ticketService.TotalWagers();
                        var indexWagerItems = _ticketService.TotalWagers() - 1;
                        if ($wagerTypesService.IsParlay()) getParlayInfo(null).then(function () { window.WagerAmountOnChange(wagerItem, 'R', indexWagerItems); });
                        else if ($wagerTypesService.IsTeaser()) getTeaserInfo(_ticketService.Ticket.TeaserName).then(function () { window.WagerAmountOnChange(wagerItem, 'R', indexWagerItems); });


                    }
                    if (_ticketService.Ticket.WagerItems.length >= 3 && !$rootScope.IsMobileDevice) UI.ScrollDown("betSlipController", "wagerItem_" + wagerItem.ChosenTeamId.RemoveSpecials() + "_" + wagerItem.ControlCode + "_" + wagerItem.Loo.PeriodNumber);
                    return true;
                }
            });
        };

        if (_ticketService.Ticket.Posted()) {
            return _ticketService.StartNewTicket().then(function () {
                return addWagerItemFn();
            });
        } else return addWagerItemFn();
    };

    _ticketService.GetParlayInfo = function (data) {
        getParlayInfo(data);
    };

    _ticketService.GetTeaserInfo = function (data) {
        if (!data) data = _ticketService.Ticket.TeaserName;
        getTeaserInfo(data);
    };

    _ticketService.AddContestWagerItem = function (contest, contestantLine) {
        var found = false;
        if (contest == null || contestantLine == null) return false;
        var addContestWagerItemFn = function () {
            return _caller.POST({
                'contestNumber': contestantLine.ContestNum,
                'contestantNumber': contestantLine.ContestantNum,
                'wSID': _ticketService.Ticket.wSID
            }, 'AddContestWagerItem', null, true).then(function (result) {
                var wagerItem = result.data.d.Data;
                if (!wagerItem) return null;
                wagerItem.Type = "C";
                wagerItem.Available = true;
                wagerItem.Loo = contestantLine;
                wagerItem.RiskPlaceHolder = $translatorService.Translate("Risk_Amt");
                wagerItem.WinPlaceHolder = $translatorService.Translate("Win_Amt");
                wagerItem.RiskAmt = null;
                wagerItem.ToWinAmt = null;
                wagerItem.ContestType = contest.ContestType;
                wagerItem.ContestType2 = contest.ContestType2;
                wagerItem.ContestType3 = contest.ContestType3;
                wagerItem.ContestDesc = contest.ContestDesc;
                for (var i = 0; i < _ticketService.Ticket.WagerItems.length; i++) {
                    if (_ticketService.Ticket.WagerItems[i].Loo.ContestantNum == wagerItem.Loo.ContestantNum && _ticketService.Ticket.WagerItems[i].Loo.ContestNum == wagerItem.Loo.ContestNum) {
                        _ticketService.Ticket.WagerItems[i] = wagerItem;
                        wagerItem.IsOk = true;
                        found = true;
                    }
                };
                if (!found) _ticketService.Ticket.WagerItems.push(wagerItem);
                if (!$rootScope.IsMobileDevice) UI.ScrollDown("betSlipController");
                return result.data.d.Code;
            });
        };

        if (_ticketService.Ticket.Posted())
            return _ticketService.StartNewTicket().then(function () {
                return addContestWagerItemFn();
            });
        else return addContestWagerItemFn();
    };

    _ticketService.RemoveGameWagerItem = function (loo, subWagerType, teamPos, keepUi) {
        if (typeof keepUi === "undefined") keepUi = false;
        return _caller.POST({
            "wSID": _ticketService.Ticket.wSID,
            "gameNum": loo.GameNum,
            "periodNumber": loo.PeriodNumber,
            "subWagerType": subWagerType,
            "teamPos": teamPos
        }, 'RemoveGameWagerItem', null, true).then(function () {
            _ticketService.RemoveUiItem(loo, subWagerType, teamPos, keepUi);
            $rootScope.ErrorMessage = "";
        });
    };

    _ticketService.RemoveUiItem = function (loo, subWagerType, teamPos, keepUi) {
        for (var i = 0; i < _ticketService.Ticket.WagerItems.length; i++) {
            var wi = _ticketService.Ticket.WagerItems[i];
            if (wi.Type == "G") {
                var wiTeamPos = wi.ControlCode.substring(2, 1);
                if (wi.Loo.GameNum == loo.GameNum &&
                    wi.Loo.PeriodNumber == loo.PeriodNumber &&
                    wi.Loo.Store == loo.Store &&
                    wi.WagerType == subWagerType &&
                    wiTeamPos == teamPos) {
                    wi.IsOk = true;
                    revertGameWagerSelection(wi.Loo, wi.WagerType, teamPos);
                    if (keepUi) _ticketService.Ticket.WagerItems[i].Available = false;
                    else _ticketService.Ticket.WagerItems.splice(i, 1);
                    if (_ticketService.IsOpenPlays() && _ticketService.IsOpenFull(true)) {
                        _ticketService.SetOpenPlaysVal(_ticketService.OpenPlayDropDown[_ticketService.OpenPlayDropDown.indexOf(_ticketService.openPlaysVal) + 1]);
                    } else {
                        if (_ticketService.openPlaysVal && _ticketService.openPlaysVal.value < 9 && _ticketService.openPlaysVal.value != _ticketService.openPlaysSelection[0].value)
                            _ticketService.Ticket.AllowedWagerPicks.OpenPicks = _ticketService.openPlaysVal.value + _ticketService.TotalWagers();
                        var totalWagersPick = _ticketService.TotalWagers();
                        var elements = _ticketService.Ticket.AllowedWagerPicks.MaxPicks - totalWagersPick + 1;
                        for (var j = 1; j < elements && j < 9; j++) {
                            if ((_ticketService.openPlaysSelection[j].value > _ticketService.OpenPlayDropDown[_ticketService.OpenPlayDropDown.length - 1].value) || _ticketService.OpenPlayDropDown.length == 1) {
                                _ticketService.OpenPlayDropDown.push(_ticketService.openPlaysSelection[j]);
                                break;
                            }
                        }
                    }
                    if ($wagerTypesService.IsAccumWager()) {
                        if (!_ticketService.IsOpenPlays()) _ticketService.ResetAmounts();
                        if ($wagerTypesService.IsParlay()) getParlayInfo(null).then(function () { riskChanged(); });
                        else if ($wagerTypesService.IsTeaser()) getTeaserInfo(_ticketService.Ticket.TeaserName).then(function () { riskChanged(); });
                    }
                    break;
                }
            }
        }
        _ticketService.CalculateTotalAmounts();
    };

    _ticketService.RemoveLocalContestWagerItem = function (contestNum, contestantNum) {
        for (var i = 0; i < _ticketService.Ticket.WagerItems.length; i++) {
            var wagerItem = _ticketService.Ticket.WagerItems[i];
            if (wagerItem.Type == "C" && wagerItem.Loo.ContestNum == contestNum && wagerItem.Loo.ContestantNum == contestantNum) {
                wagerItem.IsOk = true;
                wagerItem.Loo.Selected = false;
                _ticketService.Ticket.WagerItems.splice(i, 1);
                _ticketService.CalculateTotalAmounts();
                return;
            }
        }
    };

    _ticketService.RemoveContestWagerItem = function (contestantLine) {
        return _caller.POST({
            'contestNumber': contestantLine.ContestNum,
            'contestantNumber': contestantLine.ContestantNum,
            'wSID': _ticketService.Ticket.wSID
        }, 'RemoveContestWagerItem', null, true).then(function () {
            _ticketService.RemoveLocalContestWagerItem(contestantLine.ContestNum, contestantLine.ContestantNum);
            $rootScope.ErrorMessage = "";
        });
    };

    _ticketService.RemoveAllWagerItems = function () {
        return _caller.POST({}, 'RemoveAllWagerItems', null, true).then(function () {
            removeAllWagerItems();
        });
    };

    _ticketService.Ticket.Any = function () {
        return _ticketService.Ticket.WagerItems.length > 0 || _ticketService.Ticket.OpenWagerItems.length > 0;
    };

    _ticketService.Ticket.Posted = function () {
        return _ticketService.Ticket.TicketNumber != null && _ticketService.Ticket.TicketNumber > 0;
    };

    _ticketService.CalculateMaxRiskAmount = function (wagerItem, tempLimit) {
        if (typeof tempLimit == "undefined") tempLimit = wagerItem.MaxWagerLimit / 100;
        if (wagerItem.Type == "G") {
            var finalPrice = (wagerItem.SelectedLine != null) ? parseInt(wagerItem.SelectedLine.cost) : wagerItem.FinalPrice;
            if (finalPrice < 0 || $wagerTypesService.IsTeaser()) {
                switch ($wagerTypesService.Selected.id) {
                    case $wagerTypesService.Ids.StraightBet:
                    case $wagerTypesService.Ids.IfWinOrPush:
                    case $wagerTypesService.Ids.IfWinOnly:
                        tempLimit = CommonFunctions.RoundNumber(LineOffering.CalculateRiskAmtUsingWi(null, tempLimit, 'A'/*wagerItem.PriceType*/, finalPrice));
                        break;
                    case $wagerTypesService.Ids.Teaser:
                        var pc = TeaserFunctions.GetPayCard(_ticketService.Ticket.TeaserInfo.teaserPayCards, _ticketService.Ticket.lowestWager.length);
                        if (pc != null) {
                            var tempRiskLimit = TeaserFunctions.CalculateRisk(pc, tempLimit);
                            if (tempRiskLimit > tempLimit) tempLimit = tempRiskLimit;
                        }
                        break;
                }
            }
        }
        return tempLimit;
    };

    _ticketService.CalculateMaxToWinAmount = function (wagerItem) {
        var lowestWager = wagerItem;
        if ($wagerTypesService.IsStraightBet() || $wagerTypesService.IsIfBet())
            _ticketService.Ticket.WagerItems.forEach(function (wager) {
                if (wager.MaxWagerLimit < lowestWager.MaxWagerLimit)
                    lowestWager = wager;
            });
        var tempLimit = (lowestWager.MaxWagerLimit ? lowestWager.MaxWagerLimit : 0) / 100;
        if (lowestWager.Type == "G") {
            var finalPrice = (lowestWager.SelectedLine != null) ? parseInt(lowestWager.SelectedLine.cost) : lowestWager.FinalPrice;
            if (finalPrice < 0 || $wagerTypesService.IsTeaser()) {
                switch ($wagerTypesService.Selected.id) {
                    case $wagerTypesService.Ids.IfWinOrPush:
                    case $wagerTypesService.Ids.IfWinOnly:
                    case $wagerTypesService.Ids.StraightBet:
                        tempLimit = CommonFunctions.RoundNumber(LineOffering.CalculateToWinAmtUsingWi(null, tempLimit, lowestWager.PriceType, finalPrice));
                        break;
                    case $wagerTypesService.Ids.Parlay:
                        var roundRobin = _ticketService.GetSelectedRoundRobin();
                        if (roundRobin == null) {
                            tempLimit = ParlayFunctions.CalculateParlayToWinAmt(_ticketService.Ticket.WagerItems, _ticketService.Ticket.ParlayInfo, ParlayFunctions.GetPayCardMaxPayout(_ticketService.Ticket.WagerItems.length, _ticketService.Ticket.ParlayInfo, tempLimit), tempLimit);
                        }
                        else {
                            var rrValues = ParlayFunctions.CalculateRoundRobinToWin(_ticketService.Ticket.WagerItems, _ticketService.Ticket.ParlayInfo, tempLimit, roundRobin.value);
                            tempLimit = rrValues.ToWinAmt;
                        }
                        break;
                    case $wagerTypesService.Ids.Teaser:
                        var pc = TeaserFunctions.GetPayCard(_ticketService.Ticket.TeaserInfo.teaserPayCards, _ticketService.Ticket.WagerItems.length);
                        if (pc != null) {
                            var tempWinLimit = TeaserFunctions.CalculateToWin(pc, tempLimit);
                            if (tempWinLimit > tempLimit) tempLimit = tempWinLimit;
                        }
                        break;
                }
            }
        }
        return tempLimit;
    };

    _ticketService.CalculateTotalAmounts = function () {
        if ($wagerTypesService.Selected.id == $wagerTypesService.Ids.Teaser || $wagerTypesService.Selected.id == $wagerTypesService.Ids.Parlay) return;
        var totalRisk = 0, totalToWin = 0;
        for (var i = 0; i < _ticketService.Ticket.WagerItems.length; i++) {
            var wi = _ticketService.Ticket.WagerItems[i];
            totalRisk += (isNaN(wi.RiskAmt) || wi.RiskAmt == "" ? 0 : CommonFunctions.RoundNumber(wi.RiskAmt, false));
            totalToWin += (isNaN(wi.ToWinAmt) || wi.ToWinAmt == "" ? 0 : CommonFunctions.RoundNumber(wi.ToWinAmt, false));
        }
        _ticketService.Ticket.TotalToWinAmount = totalToWin;
        switch ($wagerTypesService.Selected.id) {
            case $wagerTypesService.Ids.IfWinOnly:
                var totalToWinIfBet = 0;
                var ifBetCalc = _ticketService.Ticket.WagerItems[0].RiskAmt;
                for (var j = 1; j < _ticketService.Ticket.WagerItems.length; j++) {
                    var currentRisk = parseFloat(_ticketService.Ticket.WagerItems[j].RiskAmt);
                    totalToWinIfBet += CommonFunctions.RoundNumber(_ticketService.Ticket.WagerItems[j - 1].ToWinAmt, false);
                    if (currentRisk > totalToWinIfBet && ifBetCalc < (currentRisk - totalToWinIfBet)) {
                        ifBetCalc = currentRisk - totalToWinIfBet;
                    }
                }
                _ticketService.Ticket.TotalRiskAmount = ifBetCalc;
                break;
            case $wagerTypesService.Ids.IfWinOrPush:
                var highestRisk = 0;
                _ticketService.Ticket.WagerItems.forEach(function (val) {
                    if (val.RiskAmt > highestRisk) highestRisk = val.RiskAmt;
                });
                _ticketService.Ticket.TotalRiskAmount = highestRisk;
                break;
            case $wagerTypesService.Ids.StraightBet:
                _ticketService.Ticket.TotalRiskAmount = totalRisk;
                break;
        }
    };

    _ticketService.SportHasWagersSelected = function (sportType, sportSubType) {
        if (_ticketService.Ticket.Posted()) return false;
        for (var i = 0; i < _ticketService.Ticket.WagerItems.length; i++) {
            var wagerItem = _ticketService.Ticket.WagerItems[0];
            if (wagerItem.Loo.SportType == sportType && wagerItem.Loo.SportSubType == sportSubType) return true;
        }
        return false;
    };

    _ticketService.ProcessTicket = function () {
        if (!validatePicks() || !validateWagerItemsAmounts()) return false;
        var j;
        var wagerItemsData = prepareWagerData();
        _ticketService.TicketProcessed = false;
        _ticketService.PlacingBet = true;
        return _caller.POST({
            'wSID': _ticketService.Ticket.wSID,
            'password': $customerService.Info.Password,
            'useFreePlay': _ticketService.Ticket.UseFreePlay,
            'wagersData': wagerItemsData,
            'keepOpenPlay': _ticketService.Ticket.KeepOpenPlay || _ticketService.IsOpenPlays(),
            'openPlaysCount': _ticketService.Ticket.AllowedWagerPicks.OpenPicks
        }, 'ProcessTicket').then(function (result) {
            _ticketService.TicketProcessed = true;
            _ticketService.PlacingBet = false;
            var wi;
            if (!$rootScope.IsWebSocketSupported()) $errorHandler.Error("Socket " + $rootScope.GetCurrentSocketServer() + " is not supported by browser. State: " + $rootScope.GetWebSocketState(), 'ProcessTicket');
            if (result.data.d.Code == 0 && result.data.d.Data.TicketNumber && result.data.d.Data.TicketNumber > 0) {
                _ticketService.Ticket.TicketNumber = result.data.d.Data.TicketNumber;
                if (result.data.d.Data && result.data.d.Data.GameWagerItems)
                    for (j = 0; j < result.data.d.Data.GameWagerItems.length; j++) {
                        wi = result.data.d.Data.GameWagerItems[j];
                        _ticketService.WagerAvailable(wi, _ticketService.UpdateGameWager(wi, true, false));
                    }
                unSelectAllWagerItems();
                $rootScope.ErrorMessage = "";
                _ticketService.OpenPlays = [];
                _ticketService.IsMoverReady = false;
                _ticketService.Ticket.ArAmount = null;
                $customerService.GetCustomerInfo();
                setTimeout(function () {
                    $('#betSlipBody').animate({
                        scrollTop: $('#betSlipBody').scrollTop() + $('#alertSuccess').offset().top - 100
                    }, 'fast');
                }, 200);
            } else {
                $rootScope.ErrorMessage = result.data.d.Message;
                if (!_ticketService.IsOpenPlays() && $wagerTypesService.IsAccumWager()) {
                    _ticketService.Ticket.TotalRiskAmount = null;
                    _ticketService.Ticket.RRTotalRiskAmount = null;
                    _ticketService.Ticket.TotalToWinAmount = null;
                    _ticketService.Ticket.ArAmount = null;
                }
                switch (result.data.d.Code) {
                    case 4:
                        if (result.data.d.Data && result.data.d.Data.WagerPos) {
                            if (result.data.d.Message.indexOf('changed') > 0) {

                                _ticketService.Ticket.WagerItems[result.data.d.Data.WagerPos - 1].Changed = true;
                                setTimeout(function () { _ticketService.Ticket.WagerItems[result.data.d.Data.WagerPos - 1].Changed = false; }, 30000);
                            }
                        }
                        for (i = 0; i < _ticketService.Ticket.WagerItems.length; i++) {
                            wi = _ticketService.Ticket.WagerItems[i];
                            if (result.data.d.Data.WagerOrContestItem && result.data.d.Data.WagerOrContestItem.WagerItem)
                                if (wi.Loo.GameNum == result.data.d.Data.WagerOrContestItem.WagerItem.Loo.GameNum)
                                    wi.IsOk = false;
                            if (result.data.d.Data.WagerOrContestItem && result.data.d.Data.WagerOrContestItem.ContestItem)
                                if (wi.Loo.ContestNum == result.data.d.Data.WagerOrContestItem.ContestItem.ContestNum)
                                    wi.IsOk = false;
                        }
                        break;
                    case 1:
                    case 5:
                        setTimeout(function () {
                            $('#betSlipBody').animate({
                                scrollTop: 9999
                            }, 'fast');
                        }, 200);
                        return;
                    case 6:
                        UI.Alert(result.data.d.Message, 'Warning', function () {
                            $rootScope.Logout();
                        });
                        break;
                }
                if (result.data.d.Data && result.data.d.Data.GameWagerItems && result.data.d.Message.indexOf('changed') >= 0)
                    for (j = 0; j < result.data.d.Data.GameWagerItems.length; j++) {
                        wi = result.data.d.Data.GameWagerItems[j];
                        if (wi.Changed || !wi.Available) {
                            UI.ScrollDown('betSlipController', 'wagerItem_' + wi.ChosenTeamId.RemoveSpecials() + '_' + wi.ControlCode);
                        }
                        _ticketService.WagerAvailable(wi, _ticketService.UpdateGameWager(wi, $customerService.AutoAccept, false));
                    }
                setTimeout(function () {
                    $('#betSlipBody').animate({
                        scrollTop: 9999
                    }, 'fast');
                }, 200);
            }
        });
    };

    _ticketService.WagerAvailable = function (wager) {
        if (wager.IsExistingOpenItem) return true;
        var isAvailable;
        var typeName = "";
        if (wager.Type == "C") {
            return false; //wager.Loo.LineChanged || wager.Changed;
        }
        var wagerSubType = wager.ControlCode.substring(0, 1);
        switch (wagerSubType) {
            case "M":
                isAvailable = wager.Loo.MoneyLine1 != null && wager.Loo.MoneyLine2 != null ? true : false;
                typeName = "Money Line";
                break;
            case "S":
                isAvailable = wager.Loo.Spread1 != null && wager.Loo.Spread2 != null ? true : false;
                typeName = "Spread";
                break;
            case "E":
                isAvailable = wager.Loo.Team1TtlPtsAdj1 != null && wager.Loo.Team1TtlPtsAdj2 != null && wager.Loo.Team2TtlPtsAdj1 != null && wager.Loo.Team2TtlPtsAdj2 != null ? true : false;
                typeName = "Team Total";
                break;
            case "L":
                isAvailable = wager.Loo.TtlPtsAdj1 != null && wager.Loo.TtlPtsAdj2 != null ? true : false;
                typeName = "Total";
                break;
            default:
                isAvailable = false;
        }
        if (!isAvailable) {
            UI.Notify($translatorService.Translate("Line") + ": " + wager.ChosenTeamId + "/" + $translatorService.Translate(typeName) + " " +
               $translatorService.Translate("Is not available any more"), UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
            _ticketService.RemoveGameWagerItem(wager.Loo, wager.WagerType, wager.ControlCode.substring(2, 1), true).then();
        }
        return isAvailable;
    };

    _ticketService.GetSelectedRoundRobin = function () {
        if (_ticketService.Ticket.RoundRobin == null ||
            _ticketService.Ticket.RoundRobin.Options == null ||
            _ticketService.Ticket.RoundRobin.Selected == null ||
            _ticketService.Ticket.RoundRobin.Selected == _ticketService.Ticket.RoundRobin.Options[0]) return null;
        return _ticketService.Ticket.RoundRobin.Selected;
    };

    _ticketService.ResetAmounts = function (wagerItem) {
        if (wagerItem != null) {
            wagerItem.RiskAmt = 0;
            wagerItem.ToWinAmt = 0;
            if (wagerItem.SelectedLine != null) {
                wagerItem.FinalPrice = parseInt(wagerItem.SelectedLine.cost);
                wagerItem.FinalLine = parseInt(wagerItem.SelectedLine.points);
            }
        } else {
            _ticketService.Ticket.TotalRiskAmount = 0;
            _ticketService.Ticket.RRTotalRiskAmount = 0;
            _ticketService.Ticket.TotalToWinAmount = 0;
        }
    };

    _ticketService.GetWagerPicks = function () {

        return _caller.POST({}, 'GetWagerPicks').then(function (result) {
            var wagerPicks = result.data.d.Data;
            if (wagerPicks != null) {
                _ticketService.Ticket.AllowedWagerPicks.MinPicks = (wagerPicks.MinPicks <= 0 && ($wagerTypesService.IsTeaser() || $wagerTypesService.IsParlay()) ? 2 : wagerPicks.MinPicks);
                _ticketService.Ticket.AllowedWagerPicks.MaxPicks = (wagerPicks.MaxPicks <= 0) ? 999 : wagerPicks.MaxPicks;
                var totalWagersPick = _ticketService.TotalWagers();
                var elements = _ticketService.Ticket.AllowedWagerPicks.MaxPicks - totalWagersPick + 1;
                _ticketService.OpenPlayDropDown = [];
                for (var j = 0; j < elements && j < 9; j++) {
                    _ticketService.OpenPlayDropDown.push(_ticketService.openPlaysSelection[j]);
                }
                if (!_ticketService.IsOpenPlays()) {
                    _ticketService.SetOpenPlaysVal(_ticketService.OpenPlayDropDown[0]);
                }
            }
        });


    };

    _ticketService.GetOpenPlays = function () {
        return _caller.POST({}, 'GetOpenPlays').then(function (result) {
            _ticketService.OpenPlays = result.data.d.Data;
            var wagerType = ($wagerTypesService.Selected.id == 1 ? 'Parlay' : 'Teaser');
            _ticketService.OpenPlays.forEach(function (part) {
                part.OpenName = part.TotalPicks + ' ' + $translatorService.Translate('Teams') + ' ' + $translatorService.Translate(wagerType);
            });
            $rootScope.$broadcast("OpenPlaysLoaded");
        });
    };

    _ticketService.GetOpenPlaysItems = function (openWager) {
        _ticketService.Ticket.TotalRiskAmount = openWager.AmountWagered / 100;
        _ticketService.Ticket.TotalToWinAmount = openWager.ToWinAmount / 100;
        _ticketService.OpenTotalPicks = openWager.totalPicks;
        _ticketService.Ticket.OpenWagerItems = [];
        _ticketService.Ticket.TicketNumber = null;
        var groupedItems = null;
        var holdTicketNumber = null;
        var holdWagerNumber = null;
        var callerOpen = new ServiceCaller($http, '', 'reportsService');
        //var getOpenPlays = function () {
        callerOpen.POST({ 'ticketNumber': openWager.TicketNumber, 'wagerNumber': openWager.WagerNumber, 'wSID': _ticketService.Ticket.wSID }, 'GetWagerItemsOpenPlays').then(function (result) {
            var allOpenBets = result.data.d.Data;
            var groupedOpenBets = new Array();
            if (allOpenBets != null && allOpenBets.length > 0) {
                groupedOpenBets = new Array();
                groupedItems = new Array();
                holdTicketNumber = allOpenBets[0].TicketNumber;
                holdWagerNumber = allOpenBets[0].WagerNumber;
                for (var i = 0; i < allOpenBets.length; i++) {
                    if (holdTicketNumber != allOpenBets[i].TicketNumber || holdWagerNumber != allOpenBets[i].WagerNumber) {
                        groupedOpenBets.push(allOpenBets[i - 1]);
                        if (groupedOpenBets.length > 0 && groupedItems != null && groupedItems.length > 0)
                            groupedOpenBets[groupedOpenBets.length - 1].Items = groupedItems;
                        groupedItems = new Array();
                        groupedItems.push(newWagerItem(allOpenBets[i]));
                    } else {
                        groupedItems.push(newWagerItem(allOpenBets[i]));
                    }
                    holdTicketNumber = allOpenBets[i].TicketNumber;
                    holdWagerNumber = allOpenBets[i].WagerNumber;

                    if (i == allOpenBets.length - 1) {
                        groupedOpenBets.push(newWagerItem(allOpenBets[i]));
                        groupedOpenBets[groupedOpenBets.length - 1].Items = groupedItems;
                    }
                }
            }
            if (groupedOpenBets && groupedOpenBets.length > 0)
                for (var j = 0; j < groupedOpenBets[0].Items.length; j++) {
                    groupedOpenBets[0].Items[j].FinalPrice = groupedOpenBets[0].Items[j].FinalLine;
                    groupedOpenBets[0].Items[j].Type = 'G';
                    groupedOpenBets[0].Items[j].Loo = groupedOpenBets[0].Items[j];
                    _ticketService.Ticket.OpenWagerItems.push(groupedOpenBets[0].Items[j]);
                }
            _ticketService.OpenPlayDropDown = [];
            _ticketService.Ticket.AllowedWagerPicks.OpenPicks = openWager.totalPicks - _ticketService.TotalWagers();
            for (j = 0; j <= _ticketService.Ticket.AllowedWagerPicks.OpenPicks && j < 8; j++) {
                _ticketService.OpenPlayDropDown.push(_ticketService.openPlaysSelection[j]);
            };
            _ticketService.SetOpenPlaysVal(_ticketService.OpenPlayDropDown[_ticketService.OpenPlayDropDown.length - 1]);
            _ticketService.TicketProcessed = true;
        });
    };

    _ticketService.IsOpenPlays = function () {
        var isOpen = _ticketService.Ticket.OpenWagerItems.length > 0;
        return isOpen;
    };

    _ticketService.IsOpenFull = function () {
        var isOpen = _ticketService.IsOpenPlays();
        if (!isOpen) _ticketService.ShowOpenPlayItems = false;
        var isFull = (isOpen && _ticketService.OpenTotalPicks >= _ticketService.TotalWagers() + 1) || (!isOpen && _ticketService.Ticket.AllowedWagerPicks.OpenPicks >= _ticketService.TotalWagers() + 1);
        if (isFull) _ticketService.Ticket.KeepOpenPlay = false;
        return isFull;
    };

    _ticketService.GetTeaserInfo = function (teaserName) {
        return getTeaserInfo(teaserName);
    };

    _ticketService.GetOpenPlay = function (wagerToOpen) {
        _ticketService.Ticket.OpenWager = wagerToOpen;
        $wagerTypesService.Selected = wagerToOpen.WagerType == 'P' ? $wagerTypesService.List[1] : $wagerTypesService.List[2];
    };

    _ticketService.TotalWagers = function () {
        var wagerCount = 0;
        _ticketService.Ticket.WagerItems.forEach(function (a) {
            if (!a.Available == false) wagerCount++;
        });
        return _ticketService.Ticket.OpenWagerItems.length + wagerCount;
    };

    _ticketService.UnSelectAllWagerItems = function () {
        return unSelectAllWagerItems();
    };

    _ticketService.SetOpenPlaysVal = function (val) {
        if (typeof val == "undefined") return;
        _ticketService.openPlaysVal = val;
        if (_ticketService.openPlaysVal.value > 8) {
            _ticketService.Ticket.KeepOpenPlay = false;
            _ticketService.Ticket.AllowedWagerPicks.OpenPicks = _ticketService.openPlaysVal.value;
        } else {
            if (_ticketService.Ticket.RoundRobin.Options && _ticketService.Ticket.RoundRobin.Selected != _ticketService.Ticket.RoundRobin.Options[0]) {
                _ticketService.Ticket.RoundRobin.Selected = _ticketService.Ticket.RoundRobin.Options[0];
                UI.Notify($translatorService.Translate("OPEN SPOTS ARE ONLY AVAILABLE FOR SINGLE PARLAYS"), UI.Position.Top, UI.Position.Center, 200, UI.Type.Info);
            }
            _ticketService.Ticket.KeepOpenPlay = _ticketService.Ticket.AllowedWagerPicks.OpenPicks > _ticketService.TotalWagers();
            _ticketService.Ticket.AllowedWagerPicks.OpenPicks = _ticketService.openPlaysVal.value + _ticketService.TotalWagers();
        }
    };

    _ticketService.Continue = function () {
        if ($rootScope.SetPassword) _ticketService.IsMoverReady = true;
        var wagerItemsData = prepareWagerData();
        return _caller.POST({ 'wagersData': wagerItemsData }, 'Continue').then(function (result) {
            if (result.data.d.Code == 0) _ticketService.IsMoverReady = true;
        });
    };

    _ticketService.CreateBuyPointsOptions = function (wi) {
        createBuyPointsOptions(wi);
    };

    return _ticketService;

}]);