﻿appModule.factory('$wagerTypesService', ['$http', '$rootScope', function ($http, $rootScope) {

  var _caller = new ServiceCaller($http, null, 'wagerTypesService');

  var _wagerTypesService = {};

  _wagerTypesService.Ids = {
        StraightBet: 0,
        Parlay: 1,
        Teaser: 2,
        IfWinOrPush: 3,
        IfWinOnly: 4,
        ActionReverse: 5,
        Contest: 6
    };

  _wagerTypesService.SubWagerTypeCode = {
        Spread: "S",
        MoneyLine: "M",
        Total: "L",
        TeamTotal: "E"
    };

  _wagerTypesService.Previous = null;
  _wagerTypesService.Selected = null;

  //Private Methods

  function getWagerTypeById(wtId) {
    if (!_wagerTypesService.List) return false;
    for (var i = 0; i < _wagerTypesService.List.length; i++) {
      if (_wagerTypesService.List[i].id == wtId) {
        return _wagerTypesService.List[i];
      }
    }
    return null;
  };

  function setSelected(getFromSession) {
    if (getFromSession)
      return _caller.POST({}, 'GetActiveWagerType', null, true).then(function (result) {
        var wtId = result.data.d.Data.WebCode;
        var previous = _wagerTypesService.Selected;
        _wagerTypesService.Selected = getWagerTypeById(wtId);
        _wagerTypesService.Previous = previous != null ? previous : _wagerTypesService.Selected;
        $rootScope.$broadcast('wagerTypeChanged');
    });
    else {
      _wagerTypesService.Previous = _wagerTypesService.Selected;
      $rootScope.$broadcast('wagerTypeChanged');
    }
    return true;
  };

    //Public Methods

  _wagerTypesService.IsTeaser = function () {
    return (_wagerTypesService.List && _wagerTypesService.Selected && _wagerTypesService.Selected.id == _wagerTypesService.Ids.Teaser);
    };

  _wagerTypesService.IsActionReverse = function () {
    return (_wagerTypesService.List && _wagerTypesService.Selected && _wagerTypesService.Selected.id == _wagerTypesService.Ids.ActionReverse);
    };

  _wagerTypesService.IsStraightBet = function () {
    return (_wagerTypesService.List && _wagerTypesService.Selected && _wagerTypesService.Selected.id == _wagerTypesService.Ids.StraightBet);
    };

  _wagerTypesService.IsParlay = function () {
    return (_wagerTypesService.List && _wagerTypesService.Selected && _wagerTypesService.Selected.id == _wagerTypesService.Ids.Parlay);
    };

  _wagerTypesService.IsIfBet = function () {
    return (_wagerTypesService.List &&
        _wagerTypesService.Selected &&
        (_wagerTypesService.Selected.id == _wagerTypesService.Ids.IfWinOrPush ||
        _wagerTypesService.Selected.id == _wagerTypesService.Ids.IfWinOnly));
    };

  _wagerTypesService.Change = function () {
    _caller.POST({ 'wagerTypeId': _wagerTypesService.Selected.id }, 'ChangeWagerType').then(function () {
      setSelected(false);
        });
    };

  _wagerTypesService.IsAccumWager = function () {
    if (!_wagerTypesService.Selected) return false;
    return _wagerTypesService.Selected.id != _wagerTypesService.Ids.StraightBet &&
        _wagerTypesService.Selected.id != _wagerTypesService.Ids.IfWinOrPush &&
        _wagerTypesService.Selected.id != _wagerTypesService.Ids.IfWinOnly;
    };

  _wagerTypesService.RevertChange = function () {
    if (!_wagerTypesService.Selected || _wagerTypesService.Selected.id == _wagerTypesService.Previous.id) return false;
    _wagerTypesService.Selected = _wagerTypesService.Previous;
        return true;
    };

  _caller.GET('/data/wagerTypes.json').then(function (response) {
    _wagerTypesService.List = response.data.wagerTypes;
    setSelected(true).then();
            });

  return _wagerTypesService;

}]);