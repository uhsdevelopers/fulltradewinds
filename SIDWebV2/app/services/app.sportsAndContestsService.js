﻿appModule.factory('$sportsAndContestsService', ['$http', '$rootScope', '$wagerTypesService', '$ticketService', '$customerService', '$translatorService', '$inboxService', function ($http, $rootScope, $wagerTypesService, $ticketService, $customerService, $translatorService, $inboxService) {

  var _caller = new ServiceCaller($http, null, 'sportsAndContestsService');
  var _sportsAndContestsService = {};

  _sportsAndContestsService.SportsAndContests = [];
  _sportsAndContestsService.Selections = [];

  //Private Methods

  function removeContestant(selection, contestNum, contestantNum) {
    for (var i = 0; i < selection.ContestantsLines.length; i++) {
      var contestant = selection.ContestantsLines[i];
      if (contestant.ContestNum == contestNum && contestant.ContestantNum == contestantNum) {
        selection.ContestantsLines.splice(i, 1);
        return;
      }
    }
  }

  function restoreContestantChangedLine(contestant) {
    setTimeout(function () {
      contestant.LineChanged = false;
      $rootScope.safeApply();
    }, 30000);
  };

  function updateContestantLineData(c, cl) {
    if (c.MoneyLine != cl.MoneyLine) {
      c.MoneyLine = cl.MoneyLine;
      c.Decimal = cl.DecimalOdds;
      c.Numerator = cl.Numerator;
      c.Denominator = cl.Denominator;
      c.LineChanged = true;
    }
    if (c.LineChanged)
      restoreContestantChangedLine(c);
  };

  function contestLineUpdate(lChanged, selection) {
    var wt = $wagerTypesService.Selected;
    var shadeLineFound = false;
    for (var a = 0; a < selection.ContestantsLines.length; a++) {
      if (selection.ContestantsLines[a].ContestantNum == lChanged.ContestantNum) {
        if (selection.ContestantsLines[a].CustProfile.trim() == lChanged.CustProfile.trim()) {
          if (lChanged.Status == "H") {
            selection.ContestantsLines.splice(a, 1);
            for (var b = 0; b < $ticketService.Ticket.WagerItems.length; b++) {
              var wagerItem = $ticketService.Ticket.WagerItems[b];
              if (wagerItem.Type == "C" && wagerItem.Loo.ContestantNum == lChanged.ContestantNum) {
                wagerItem.Loo.Selected = false;
                $ticketService.RemoveContestWagerItem(wagerItem.Loo, b);
                $ticketService.Ticket.WagerItems.splice(b, 1);
                $ticketService.CalculateTotalAmounts();
              }
            }
            if (selection.ContestType3 != ".") {
              for (var k = 0; k < _sportsAndContestsService.Selections.length; k++) {
                if (_sportsAndContestsService.Selections[k].ContestType2 == lChanged.ContestType2 && _sportsAndContestsService.Selections[k].ContestType3 == lChanged.ContestType3) {
                  _sportsAndContestsService.Selections.splice(k, 1);
                  break;
                }
              };
            }
            $ticketService.CalculateTotalAmounts();
            break;
          }
          else {
            _caller.POST({ 'newItem': lChanged, 'eventOffering': 'C', 'wagerType': wt.name }, 'AppendGamesAndContestsLines', null, true).then(function (result) {
              var contest = result.data.d.Data != null && result.data.d.Data.NLoo != null ? result.data.d.Data.NLoo : null;
              if (contest != null) updateContestantLineData(selection.ContestantsLines[a], contest.ContestantsLines[0]);
              else removeContestant(selection, lChanged.GameNum, lChanged.ContestantNum);
              if (!$ticketService.Ticket.Posted()) {
                $ticketService.RemoveLocalContestWagerItem(lChanged.GameNum, lChanged.ContestantNum);
              }
              $rootScope.safeApply();
            });
            return true;
          }

        }
        else {
          if (selection.ContestantsLines[a].CustProfile.trim() == CommonFunctions.DefaultCustProfile) {
            selection.ContestantsLines[a].disabled = true;
          }
          else
            shadeLineFound = true;
        }
      }

      if (a == selection.ContestantsLines.length - 1 && lChanged.Status != "H") {
        _caller.POST({ 'newItem': lChanged, 'eventOffering': 'C', 'wagerType': wt.name }, 'AppendGamesAndContestsLines', null, true).then(function (result) {
          var contestant = result.data.d.Data.NLoo.ContestantsLines[0];
          contestant.disabled = contestant.CustProfile.trim() == CommonFunctions.DefaultCustProfile && shadeLineFound;
          contestant.isTitle = false;
          selection.ContestantsLines.push(contestant);
          $rootScope.safeApply();
        });
        return false;
      };
    };
    return true;
  }

  function isIfBetWagerItem(gameNum, periodNumber, wt) {
    if (wt.code == LineOffering.WTIfWinPush || wt.code == LineOffering.WTIfWin) {
      var found = false;

      angular.forEach(_IfBetItems, function (wi) {
        if (wi.Loo.GameNum == gameNum && wi.Loo.PeriodNumber == periodNumber) {
          found = true;
          return found;
        }
        return false;
      });
      return found;
    } else return false;
  };

  function isCircledVisible(gl) {

    if (gl.CircledMaxWagerSpreadType == "H" || gl.CircledMaxWagerSpreadType == "L" || gl.CircledMaxWagerMoneyLineType == "H" ||
      gl.CircledMaxWagerMoneyLineType == "L" || gl.CircledMaxWagerTotalType == "H" || gl.CircledMaxWagerTotalType == "L" || gl.CircledMaxWagerTeamTotalType == "H"
      || gl.CircledMaxWagerTeamTotalType == "L") return true;
    return false;
  };

  function updateGameLineData(gl, newGl, wagerType, controlCode, isUpdate) {

    gl.Status = newGl.Status;
    if (newGl.CustProfile) gl.CustProfile = newGl.CustProfile.trim();

    gl.CircledMaxWagerSpreadType = newGl.CircledMaxWagerSpread > 0 ? newGl.CircledMaxWagerSpreadType : "";
    gl.CircledMaxWagerTotalType = newGl.CircledMaxWagerTotal > 0 ? newGl.CircledMaxWagerTotalType : "";
    gl.CircledMaxWagerMoneyLineType = newGl.CircledMaxWagerMoneyLine > 0 ? newGl.CircledMaxWagerMoneyLineType : "";
    gl.CircledMaxWagerTeamTotalType = newGl.CircledMaxWagerTeamTotal > 0 ? newGl.CircledMaxWagerTeamTotalType : "";

    gl.CircledMaxWagerSpread = newGl.CircledMaxWagerSpread;
    gl.CircledMaxWagerMoneyLine = newGl.CircledMaxWagerMoneyLine;
    gl.CircledMaxWagerTotal = newGl.CircledMaxWagerTotal;
    gl.CircledMaxWagerTeamTotal = newGl.CircledMaxWagerTeamTotal;


    //newGl.CircledInfoVisible = isCircledVisible(gl);

    if (!isUpdate || ((wagerType == "S" || wagerType == null) && ((Math.abs(gl.Spread1) != Math.abs(newGl.Spread1)
      || Math.abs(gl.Spread2) != Math.abs(newGl.Spread2)
      || gl.SpreadAdj1 != newGl.SpreadAdj1
      || gl.SpreadAdj2 != newGl.SpreadAdj2) && gl.FavoredTeamID != null))) {

      newGl.WagerType = "S";
      gl.CircledMaxWagerSpread = newGl.CircledMaxWagerSpread;

      if (!controlCode || controlCode == 'S1') {
        gl.OrigSpreadAdj1 = gl.SpreadAdj1;
        gl.Spread1 = newGl.Spread1;//(gl.FavoredTeamID.trim() == gl.Team1ID.trim() ? newGl.Spread : newGl.Spread * -1);
        gl.SpreadAdj1 = newGl.SpreadAdj1;
        gl.SpreadDecimal1 = newGl.SpreadDecimal1;
        gl.SpreadNumerator1 = newGl.SpreadNumerator1;
        gl.SpreadDenominator1 = newGl.SpreadDenominator1;
      }

      if (!controlCode || controlCode == 'S2') {
        gl.OrigSpreadAdj2 = gl.SpreadAdj2;
        gl.Spread2 = newGl.Spread2;//(gl.FavoredTeamID.trim() == gl.Team2ID.trim() ? newGl.Spread : newGl.Spread * -1);
        gl.Spread = newGl.Spread;
        gl.SpreadAdj2 = newGl.SpreadAdj2;
        gl.SpreadDecimal2 = newGl.SpreadDecimal2;
        gl.SpreadNumerator2 = newGl.SpreadNumerator2;
        gl.SpreadDenominator2 = newGl.SpreadDenominator2;
      }

      gl.SpreadChanged = isUpdate;
      gl.MaxWagerSpreadType = newGl.MaxWagerSpreadType;
      gl.MaxWagerSpread = newGl.MaxWagerSpread;

      //Circled fields 
      if (isUpdate) gl.MaxWagerSpread = (newGl.MaxWagerSpread < gl.MaxWagerSpread ? newGl.MaxWagerSpread : gl.MaxWagerSpread);
      else gl.MaxWagerSpread = newGl.MaxWagerSpread;
    }

    if (!isUpdate || ((wagerType == "M" || wagerType == null) && (gl.MoneyLine1 != newGl.MoneyLine1 || gl.MoneyLine2 != newGl.MoneyLine2 || gl.MoneyLineDraw != newGl.MoneyLineDraw))) {
      newGl.WagerType = "M";

      gl.CircledMaxWagerMoneyLine = newGl.CircledMaxWagerMoneyLine;
      if (!controlCode || controlCode == 'M1') {
        gl.MoneyLine1 = newGl.MoneyLine1;
        gl.OrigMoneyLine1 = gl.MoneyLine1;
        gl.MoneyLineDecimal1 = newGl.MoneyLineDecimal1;
        gl.MoneyLineNumerator1 = newGl.MoneyLineNumerator1;
        gl.MoneyLineDenominator1 = newGl.MoneyLineDenominator1;
      }

      if (!controlCode || controlCode == 'M2') {
        gl.MoneyLine2 = newGl.MoneyLine2;
        gl.OrigMoneyLine2 = gl.MoneyLine2;
        gl.MoneyLineDecimal2 = newGl.MoneyLineDecimal2;
        gl.MoneyLineNumerator2 = newGl.MoneyLineNumerator2;
        gl.MoneyLineDenominator2 = newGl.MoneyLineDenominator2;
      }
      if (!controlCode || controlCode == 'M3') {
        gl.MoneyLineDraw = newGl.MoneyLineDraw;
        gl.MoneyLineDecimalDraw = newGl.MoneyLineDecimalDraw;
        gl.MoneyLineNumeratorDraw = newGl.MoneyLineNumeratorDraw;
        gl.MoneyLineDenominatorDraw = newGl.MoneyLineDenominatorDraw;
      }
      gl.MoneyLineChanged = isUpdate;
      gl.MaxWagerMoneyLineType = newGl.MaxWagerMoneyLineType;
      gl.MaxWagerMoneyLine = newGl.MaxWagerMoneyLine;

      //Circled fields 
      if (isUpdate) gl.MaxWagerMoneyLine = (newGl.MaxWagerMoneyLine < gl.MaxWagerMoneyLine ? newGl.MaxWagerMoneyLine : gl.MaxWagerMoneyLine);
      else gl.MaxWagerMoneyLine = newGl.MaxWagerMoneyLine;
    }

    if (!isUpdate || ((wagerType == "L" || wagerType == null) && (gl.TotalPoints1 != newGl.TotalPoints1 || gl.TotalPoints2 != newGl.TotalPoints2
      || gl.TotalPoints != newGl.TotalPoints || gl.TtlPtsAdj1 != newGl.TtlPtsAdj1 || gl.TtlPtsAdj2 != newGl.TtlPtsAdj2))) {
      newGl.WagerType = "L";
      if (!controlCode || controlCode == 'L1') {
        gl.TotalPoints1 = newGl.TotalPoints1;
        gl.OrigTtlPtsAdj1 = gl.TtlPtsAdj1;
        gl.TtlPtsAdj1 = newGl.TtlPtsAdj1;
        gl.TtlPointsDecimal1 = newGl.TtlPointsDecimal1;
        gl.TtlPointsNumerator1 = newGl.TtlPointsNumerator1;
        gl.TtlPointsDenominator1 = newGl.TtlPointsDenominator1;
      }

      if (!controlCode || controlCode == 'L2') {
        gl.TotalPoints2 = newGl.TotalPoints2;
        gl.OrigTtlPtsAdj2 = gl.TtlPtsAdj2;
        gl.TtlPtsAdj2 = newGl.TtlPtsAdj2;
        gl.TtlPointsDecimal2 = newGl.TtlPointsDecimal2;
        gl.TtlPointsNumerator2 = newGl.TtlPointsNumerator2;
        gl.TtlPointsDenominator2 = newGl.TtlPointsDenominator2;
      }

      gl.TotalPoints = newGl.TotalPoints;
      gl.TotalPointsChanged = isUpdate;
      gl.MaxWagerTotalType = newGl.MaxWagerTotalType;
      gl.MaxWagerTotal = newGl.MaxWagerTotal;

      //Circled fields 
      if (isUpdate) gl.MaxWagerTotal = (newGl.MaxWagerTotal < gl.MaxWagerTotal ? newGl.MaxWagerTotal : gl.MaxWagerTotal);
      else gl.MaxWagerTotal = newGl.MaxWagerTotal;
    }

    if (newGl.PeriodNumber == 0 && (!isUpdate || ((wagerType == "E" || wagerType == null) && (gl.Team1TotalPoints != newGl.Team1TotalPoints || gl.Team1TtlPtsAdj1 != newGl.Team1TtlPtsAdj1 ||
      gl.Team1TtlPtsAdj2 != newGl.Team1TtlPtsAdj2)))) {
      newGl.WagerType = "E";

      gl.CircledMaxWagerTeamTotal = newGl.CircledMaxWagerTeamTotal;
      gl.Team1TotalPoints = newGl.Team1TotalPoints;

      if (!controlCode || controlCode == 'E1') {
        gl.OrigTeam1TtlPtsAdj1 = gl.Team1TtlPtsAdj1;
        gl.Team1TtlPtsAdj1 = newGl.Team1TtlPtsAdj1;
        gl.Team1TtlPtsDecimal1 = newGl.Team1TtlPtsDecimal1;
        gl.Team1TtlPtsNumerator1 = newGl.Team1TtlPtsNumerator1;
        gl.Team1TtlPtsDenominator1 = newGl.Team1TtlPtsDenominator1;
      }

      if (!controlCode || controlCode == 'E4') {
        gl.OrigTeam1TtlPtsAdj2 = gl.Team1TtlPtsAdj2;
        gl.Team1TtlPtsAdj2 = newGl.Team1TtlPtsAdj2;
        gl.Team1TtlPtsDecimal2 = newGl.Team1TtlPtsDecimal2;
        gl.Team1TtlPtsNumerator2 = newGl.Team1TtlPtsNumerator2;
        gl.Team1TtlPtsDenominator2 = newGl.Team1TtlPtsDenominator2;
      }

      gl.TeamTotalChanged = isUpdate;
      gl.MaxWagerTeamTotal = newGl.MaxWagerTeamTotal;
      //Circled fields 
      if (isUpdate) gl.MaxWagerTeamTotal = (newGl.MaxWagerTeamTotal < gl.MaxWagerTeamTotal ? newGl.MaxWagerTeamTotal : gl.MaxWagerTeamTotal);
      else gl.MaxWagerTeamTotal = newGl.MaxWagerTeamTotal;
    }
    if (!isUpdate || gl.Team2TotalPoints != newGl.Team2TotalPoints || gl.Team2TtlPtsAdj1 != newGl.Team2TtlPtsAdj1 || gl.Team2TtlPtsAdj2 != newGl.Team2TtlPtsAdj2) {
      newGl.WagerType = "E";

      gl.CircledMaxWagerTeamTotal = newGl.CircledMaxWagerTeamTotal;

      gl.Team2TotalPoints = newGl.Team2TotalPoints;

      if (!controlCode || controlCode == 'E2') {
        gl.OrigTeam2TtlPtsAdj1 = newGl.Team2TtlPtsAdj1;
        gl.Team2TtlPtsAdj1 = newGl.Team2TtlPtsAdj1;
        gl.Team2TtlPtsDecimal1 = newGl.Team2TtlPtsDecimal1;
        gl.Team2TtlPtsNumerator1 = newGl.Team2TtlPtsNumerator1;
        gl.Team2TtlPtsDenominator1 = newGl.Team2TtlPtsDenominator1;
      }

      if (!controlCode || controlCode == 'E3') {
        gl.OrigTeam2TtlPtsAdj2 = newGl.Team2TtlPtsAdj2;
        gl.Team2TtlPtsAdj2 = newGl.Team2TtlPtsAdj2;
        gl.Team2TtlPtsDecimal2 = newGl.Team2TtlPtsDecimal2;
        gl.Team2TtlPtsNumerator2 = newGl.Team2TtlPtsNumerator2;
        gl.Team2TtlPtsDenominator2 = newGl.Team2TtlPtsDenominator2;
      }
      gl.TeamTotalChanged = isUpdate;
      gl.MaxWagerTeamTotal = newGl.MaxWagerTeamTotal;
      //Circled fields 
      if (isUpdate) gl.MaxWagerTeamTotal = (newGl.MaxWagerTeamTotal < gl.MaxWagerTeamTotal ? newGl.MaxWagerTeamTotal : gl.MaxWagerTeamTotal);
      else gl.MaxWagerTeamTotal = newGl.MaxWagerTeamTotal;
    }

    if (gl.SpreadChanged || gl.MoneyLineChanged || gl.TotalPointsChanged || gl.TeamTotalChanged) {
      var _RestoreChangedLine = function (game) {
        setTimeout(function () {
          if (game.SpreadChanged) game.SpreadChanged = false;
          if (game.MoneyLineChanged) game.MoneyLineChanged = false;
          if (game.TeamTotalChanged) game.TeamTotalChanged = false;
          if (game.TotalPointsChanged) game.TotalPointsChanged = false;
          $rootScope.safeApply();
        }, 30000);
      };
      _RestoreChangedLine(gl);
    }

    syncLineCircles(gl);

  };

  function removeEmptyLines() {
    for (var a = 0; a < _sportsAndContestsService.Selections.length; a++) {
      var selection = _sportsAndContestsService.Selections[a];
      if (typeof selection.Lines == "undefined") continue;
      for (var j = selection.Lines.length - 1; j >= 0; j--) {
        var l = selection.Lines[j];
        if (l.Status == 'H' || (l.Spread1 == null && l.Spread2 == null && l.MoneyLine1 == null && l.MoneyLine2 == null && l.TotalPoints == null && !l.IsTitle)) {
          selection.Lines.splice(j, 1);
          for (var k = 0; k < $ticketService.Ticket.WagerItems.length; k++) {
            var wi = $ticketService.Ticket.WagerItems[k];
            if (wi.Type == "G") {
              if (wi.Loo.GameNum == l.GameNum &&
                wi.Loo.PeriodNumber == l.PeriodNumber &&
                wi.Loo.Store == l.Store) {
                $ticketService.Ticket.WagerItems.splice(k, 1);

                if ($wagerTypesService.IsAccumWager()) {
                  $ticketService.ResetAmounts();
                  if ($wagerTypesService.IsParlay()) $ticketService.GetParlayInfo(null);
                  else if ($wagerTypesService.IsTeaser()) $ticketService.GetTeaserInfo();
                }
                break;
              }
            }
          }
          $ticketService.CalculateTotalAmounts();
          return true;
        }
      };
    };
    return false;
  };

  function gameLineUpdate(lChanged, selection) {
    var wt = $wagerTypesService.Selected;
    for (var i = 0; i < selection.Lines.length; i++) {
      var line = selection.Lines[i];
      if (line.GameNum == lChanged.GameNum && line.PeriodNumber == lChanged.PeriodNumber) {
        if (!isIfBetWagerItem(line.GameNum, line.PeriodNumber, wt)) {
          //if (line.PeriodNumber == lChanged.PeriodNumber) {
          _caller.POST({ 'newItem': lChanged, 'eventOffering': 'G', 'wagerType': wt.name }, 'AppendGamesAndContestsLines', null, true).then(function (result) {
            var nLoo = result.data.d.Data != null && result.data.d.Data.NLoo != null ? result.data.d.Data.NLoo : null;
            var wagerItems = result.data.d.Data != null && result.data.d.Data.wagerItems != null ? result.data.d.Data.wagerItems : null;
            if (wagerItems) {
              wagerItems.forEach(function (wager) {
                if (!wager.IsOk && wager.Changed) wager.IsOk = true;
                if (wager.Changed || !wager.IsOk) {
                  $ticketService.Ticket.RRTotalRiskAmount = 0;
                  $ticketService.Ticket.TotalRiskAmount = 0;
                  $ticketService.Ticket.TotalToWinAmount = 0;
                  wager.RiskAmt = 0;
                  wager.ToWinAmt = 0;
                  $ticketService.Ticket.ArAmount = null;
                }
                $ticketService.WagerAvailable(wager, $ticketService.UpdateGameWager(wager, $customerService.AutoAccept, true));
              });
            }
            if (nLoo) updateGameLineData(line, nLoo, null, null, true);
            else line.Status = 'H';
            syncLineCircles(line);
            removeEmptyLines();
            $rootScope.safeApply();
          });
          //}
          return;
        }
      }
    }

    //If line is not found then it is added.
    _caller.POST({ 'newItem': lChanged, 'eventOffering': 'G', 'wagerType': wt.name }, 'AppendGamesAndContestsLines', null, true).then(function (result) {
      var nLoo = result.data.d.Data != null && result.data.d.Data.NLoo != null ? result.data.d.Data.NLoo : null;
      if (nLoo != null) {
        nLoo.ShowDate = false;
        nLoo.ScheduleDateF = CommonFunctions.FormatDateTime(nLoo.ScheduleDateString, 1);
        updateGameLineData(nLoo, nLoo, null, null, false);
        var periodFound = false;
        for (var p = 0; p < selection.Periods.length; p++) {
          if (selection.Periods[p].PeriodNumber == nLoo.PeriodNumber) { periodFound = true; break; }
        }
        if (!periodFound) selection.Periods.push({ PeriodNumber: nLoo.PeriodNumber, PeriodDescription: nLoo.PeriodDescription });
        selection.Lines.push(nLoo);
        $rootScope.safeApply();
      }
    });

  }

  function groupSportsAndContests(rawSportsData) {
    if (!rawSportsData)
      return null;

    var groupedData = new Array();
    var currentSport = null;
    var holdSportType = null;
    var holdSportSubType = null;
    var holdContestType = null;
    var selectable = false;
    var inactiveSportsOrder = 0;
    var isInactiveSport = false;

    for (var i = 0; i < rawSportsData.length; i++) {
      var el = rawSportsData[i];
      if (holdSportType != el.SportType) {
        isInactiveSport = $.inArray(el.SportType, SETTINGS.ShowInactiveSports) >= 0;
        if (!el.Active && !isInactiveSport) continue;
        if (isInactiveSport) inactiveSportsOrder++;
        if (holdSportType != null && currentSport != null) groupedData.push(currentSport);
        currentSport = { "SportType": el.SportType, "SequenceNumber": isInactiveSport ? inactiveSportsOrder : el.SequenceNumber, "SportSubTypes": new Array(), "ContestTypes": new Array() };
        holdSportSubType = null;
      }
      if (!el.Active && !isInactiveSport) continue;
      if (el.SportSubType != null && el.SportSubType != '') {
        selectable = el.ContestType == null && el.ContestType2 == null || el.ContestType2 == '.';
        if (holdSportSubType != el.SportSubType) currentSport.SportSubTypes.push({ "SportSubType": el.SportSubType, "SequenceNumber": el.SequenceNumber, "ContestTypes": new Array(), "Selected": false, "Selectable": selectable, "Type": "G", "Active": el.Active });
        if (el.ContestType2 != null && holdContestType != el.ContestType) {
          selectable = el.SportSubType == null && el.ContestType2 == null || el.ContestType2 == ".";
          currentSport.SportSubTypes[currentSport.SportSubTypes.length - 1].ContestTypes.push({ "FirstRotNum": el.FirstRotNum, "SportType": el.SportType, "ContestType": el.ContestType, "ContestType2": el.ContestType2, "ContestTypes2": new Array(), "Selected": false, "Selectable": selectable, "Type": "C", "Active": el.Active });
        }
        if (el.ContestType2 != null && el.ContestType2 != '.') {
          currentSport.SportSubTypes[currentSport.SportSubTypes.length - 1].ContestTypes[currentSport.SportSubTypes[currentSport.SportSubTypes.length - 1].ContestTypes.length - 1].ContestTypes2.push({ "FirstRotNum": el.FirstRotNum, "SportType": el.SportType, "ContestType": el.ContestType, "ContestType2": el.ContestType2, "Selected": false, "Selectable": true, "Type": "C", "Active": el.Active });
        }
      } else {
        var contestParent = currentSport.ContestTypes;
        if (holdContestType != el.ContestType) {
          selectable = false; //el.SportSubType == null && (el.ContestType2 == null || el.ContestType2 == ".");
          contestParent.push({ "FirstRotNum": el.FirstRotNum, "SportType": el.SportType, "ContestType": el.ContestType, "ContestType2": el.ContestType2, "ContestTypes2": new Array(), "Selected": false, "Selectable": selectable, "Type": "C", "Active": el.Active });
        }
        //if (el.ContestType2 != null && el.ContestType2 != '.') {
        contestParent[contestParent.length - 1].ContestTypes2.push({ "FirstRotNum": el.FirstRotNum, "SportType": el.SportType, "ContestType": el.ContestType, "ContestType2": el.ContestType2, "Selected": false, "Selectable": true, "Type": "C", "Active": el.Active });
        //}
      }
      holdSportType = el.SportType;
      holdSportSubType = el.SportSubType;
      holdContestType = el.ContestType;
    }
    if (currentSport != null)
      groupedData.push(currentSport);
    return groupedData;
  };

  function groupTeaserSports(teaserSports) {
    if (!teaserSports) return [];
    var groupedData = new Array();
    var holdSport;

    for (var i = 0; i < teaserSports.length; i++) {
      var sport = teaserSports[i];
      sport.GameLines.forEach(function (line) {
        line.ActivePeriod = sport.Periods[0];
        line.SportLimits = sport.SportLimits;
      });
      var offering = { "AddingSport": false, "GroupValue": "", "IncludeTeamTotals": false, "Lines": sport.GameLines, "OrderIndex": 0, "Periods": sport.Periods, "ActivePeriod": sport.Periods[0], "SportSubType": sport.SportSubType, "SportType": sport.SportType, "Type": "G", "SportLimits": sport.SportLimits };
      var leagueData = { "SportSubType": sport.SportSubType, "SequenceNumber": i, "Selected": false, "Selectable": false, "Type": "G", "Offering": offering };
      if (holdSport != sport.SportType) {
        var sportData = { "SportType": sport.SportType, "SequenceNumber": i, "SportSubTypes": [leagueData] };
        groupedData.push(sportData);
        continue;
      }
      groupedData[groupedData.length - 1].SportSubTypes.push(leagueData);
    }
    return groupedData;
  };

  function bindTicketContestWagers(contestantLine) {
    contestantLine.Selected = false;
    for (var i = 0; i < $ticketService.Ticket.WagerItems.length; i++) {
      var wagerItem = $ticketService.Ticket.WagerItems[i];
      if (contestantLine.ContestNum == wagerItem.Loo.ContestNum && contestantLine.ContestantNum == wagerItem.Loo.ContestantNum) {
        contestantLine.Selected = true;
        wagerItem.Loo = contestantLine;
        break;
      }
    }
  };

  function bindGameLines(subSport) {
    if (!subSport || !subSport.Offering) return;
    for (var i = 0; i < _sportsAndContestsService.Selections.length; i++) {
      if (_sportsAndContestsService.Selections[i].Lines) {
        for (var j = 0; j < _sportsAndContestsService.Selections[i].Lines.length; j++) {
          for (var k = 0; k < subSport.Offering.Lines.length; k++) {
            if (_sportsAndContestsService.Selections[i].Lines[j].GameNum == subSport.Offering.Lines[k].GameNum) subSport.Offering.Lines.splice(k, 1);
          }
        }
      }
    }
    var previusDate = "";
    for (var l = 0; l < subSport.Offering.Lines.length; l++) {
      var gameLine = subSport.Offering.Lines[l];
      gameLine.ScheduleDateF = CommonFunctions.FormatDateTime(gameLine.ScheduleDateString, 1);
      gameLine.ShowDate = false;
      gameLine.Available = true;
      var toDate = new Date(gameLine.GameDateTimeString);
      var actualDate = toDate.getDate().toString() + "" + toDate.getMonth().toString();
      if (l == 0) {
        previusDate = actualDate;
        gameLine.ShowDate = true;
      }
      if (previusDate != actualDate) {
        previusDate = actualDate;
        gameLine.ShowDate = true;
      }
    }
    if (subSport.Offering.Lines.length > 0)
      _sportsAndContestsService.Selections.push(subSport.Offering);
    subSport.SelectionIndex = _sportsAndContestsService.Selections.length - 1;
  };

  function unbindGameLines(sport, subSport) {
    for (var i = _sportsAndContestsService.Selections.length - 1; i >= 0; i--) {
      if (_sportsAndContestsService.Selections[i].Type == 'G' && subSport && subSport.Offering && _sportsAndContestsService.Selections[i].SportSubType == subSport.Offering.SportSubType)
        _sportsAndContestsService.Selections.splice(i, 1);
    }
  };

  function bindContestLines(contests, isSpotlight) {
    isSpotlight = typeof isSpotlight != 'undefined' ? isSpotlight : false;
    for (var i = 0; i < contests.Offering.length; i++) {
      var holdContestNum = 0;
      var holdContestantNum = 0;
      var toDelete = new Array();
      for (var j = 0; j < contests.Offering[i].ContestantsLines.length; j++) {
        if (holdContestNum == contests.Offering[i].ContestNum && holdContestantNum == contests.Offering[i].ContestantsLines[j].ContestantNum)
          toDelete.push(j);
        else
          bindTicketContestWagers(contests.Offering[i].ContestantsLines[j]);
        holdContestNum = contests.Offering[i].ContestNum;
        holdContestantNum = contests.Offering[i].ContestantsLines[j].ContestantNum;
      }
      CommonFunctions.DeleteFromArray(contests.Offering[i].ContestantsLines, toDelete);
      contests.Offering[i].IsSpotlight = isSpotlight;
      _sportsAndContestsService.Selections.push(contests.Offering[i]);
    }
  };

  function unbindContestLines(contest) {
    for (var i = _sportsAndContestsService.Selections.length - 1; i >= 0; i--)
      if (_sportsAndContestsService.Selections[i].Type == 'C' && _sportsAndContestsService.Selections[i].ContestType == contest.ContestType && _sportsAndContestsService.Selections[i].ContestType2 == contest.ContestType2) {

        _sportsAndContestsService.Selections.splice(i, 1);

      }
    contest.Offering = null;
  };

  //Private Game Lines Methods

  function formatGameLines(linesOffering) {
    var periods = new Array();
    var glToDelete = new Array();
    var holdGameNum = 0;
    var holdPeriodNumber = -1;
    var ttFlag = false;
    var wtId = $wagerTypesService.Selected.id;
    for (var i = 0; i < linesOffering.length; i++) {
      var gameNum = linesOffering[i].GameNum;
      var periodNumber = linesOffering[i].PeriodNumber;
      var objP = { "PeriodNumber": periodNumber, "PeriodDescription": linesOffering[i].PeriodDescription };
      var found = false;

      for (var j = 0; j < periods.length; j++) {
        if (periodNumber == periods[j].PeriodNumber) {
          found = true;
          break;
        }
      }
      if (!found && linesOffering[i].PeriodDescription != null) periods.push(objP);
      if (gameNum == holdGameNum && holdPeriodNumber == periodNumber) glToDelete.push(i);
      holdGameNum = gameNum;
      holdPeriodNumber = periodNumber;

      linesOffering[i].Spread1Selected = false;
      linesOffering[i].Spread2Selected = false;
      linesOffering[i].MoneyLine1Selected = false;
      linesOffering[i].MoneyLine2Selected = false;
      linesOffering[i].MoneyLine3Selected = false;
      linesOffering[i].TotalPoints1Selected = false;
      linesOffering[i].TotalPoints2Selected = false;
      linesOffering[i].Team1TtlPtsAdj1Selected = false;
      linesOffering[i].Team1TtlPtsAdj2Selected = false;
      linesOffering[i].Team2TtlPtsAdj1Selected = false;
      linesOffering[i].Team2TtlPtsAdj2Selected = false;
      if (wtId == $wagerTypesService.Ids.StraightBet && !ttFlag && ((linesOffering[i].Team1TtlPtsAdj1 != null && linesOffering[i].Team1TtlPtsAdj2 != null) || (linesOffering[i].Team2TtlPtsAdj1 && linesOffering[i].Team2TtlPtsAdj2))) { //Only Straight bets are allowed for team totals
        ttFlag = true;
      }
    }
    periods.sort(CommonFunctions.DynamicSort("PeriodNumber"));
    CommonFunctions.DeleteFromArray(linesOffering, glToDelete);
    return { 'Periods': periods, 'IncludeTeamTotals': ttFlag };
  };

  function syncLineCircles(line) {
    var subSportLimit = line.SportLimits;
    line.CircledMaxWagerSpreadType = (!line.CircledMaxWagerSpread || line.Status != "I" ? '' : (subSportLimit.MaxWagerSpread > line.CircledMaxWagerSpread ? line.CircledMaxWagerSpreadType : subSportLimit.MaxWagerSpreadType));
    line.CircledMaxWagerTotalType = (!line.CircledMaxWagerTotal || line.Status != "I" ? '' : (subSportLimit.MaxWagerTotal > line.CircledMaxWagerTotal ? line.CircledMaxWagerTotalType : subSportLimit.MaxWagerTotalType));
    line.CircledMaxWagerMoneyLineType = (!line.CircledMaxWagerMoneyLine || line.Status != "I" ? '' : ((subSportLimit.MaxWagerMoneyLine || 0) > line.CircledMaxWagerMoneyLine ? line.CircledMaxWagerMoneyLineType : subSportLimit.MaxWagerMoneyLineType));
    line.CircledMaxWagerTeamTotalType = (!line.CircledMaxWagerTeamTotal || line.Status != "I" ? '' : (subSportLimit.MaxWagerTeamTotal > line.CircledMaxWagerTeamTotal ? line.CircledMaxWagerTeamTotalType : subSportLimit.MaxWagerTeamTotalType));

    line.CircledMaxWagerSpread = (!line.CircledMaxWagerSpread || line.Status != "I" ? null : (subSportLimit.MaxWagerSpread > line.CircledMaxWagerSpread ? line.CircledMaxWagerSpread : subSportLimit.MaxWagerSpread));
    line.CircledMaxWagerTotal = (!line.CircledMaxWagerTotal || line.Status != "I" ? null : (subSportLimit.MaxWagerTotal > line.CircledMaxWagerTotal ? line.CircledMaxWagerTotal : subSportLimit.MaxWagerTotal));
    line.CircledMaxWagerMoneyLine = (!line.CircledMaxWagerMoneyLine || line.Status != "I" ? null : ((subSportLimit.MaxWagerMoneyLine || 0) > line.CircledMaxWagerMoneyLine ? line.CircledMaxWagerMoneyLine : subSportLimit.MaxWagerMoneyLine));
    line.CircledMaxWagerTeamTotal = (!line.CircledMaxWagerTeamTotal || line.Status != "I" ? null : (subSportLimit.MaxWagerTeamTotal > line.CircledMaxWagerTeamTotal ? line.CircledMaxWagerTeamTotal : subSportLimit.MaxWagerTeamTotal));

    line.CircledInfoVisible = isCircledVisible(line);

  }

  function syncSportLineCircles(subSport) {

    for (var i = 0; i < subSport.Offering.Lines.length; i++) {
      var line = subSport.Offering.Lines[i];
      line.ActivePeriod = subSport.Offering.ActivePeriod;
      line.SportLimits = subSport.Offering.SportLimits[line.PeriodNumber];
      syncLineCircles(line);
    }

  }

  function loadSportLines(sport, subSport, extraData) {
    var wagerType = $wagerTypesService.Selected;
    return _caller.POST({ 'sportType': sport.SportType, 'sportSubType': subSport.SportSubType, 'wagerType': wagerType.name, 'hoursAdjustment': 0, 'periodNumber': (extraData == null ? null : extraData.PeriodNumber), 'gameNum': (extraData == null ? null : extraData.GameNum) }, 'LoadGameLines').then(function (result) {
      var response = result.data.d.Data;
      if (response == null || response == "" || response.length == 0 || response.GameLines.length == 0) return false;
      var params = formatGameLines(response.GameLines);
      if (extraData == null) {
        subSport.Offering = {
          Type: "G",
          GroupValue: "",
          IncludeTeamTotals: params.IncludeTeamTotals,
          Periods: params.Periods,
          ActivePeriod: params.Periods[0],
          Lines: response.GameLines,
          OrderIndex: 0,
          PeriodDesc: "",
          SportSubTypeId: response.GameLines[0].SportSubTypeId,
          SportType: sport.SportType,
          SportSubType: subSport.SportSubType,
          TeamTotalLines: null,
          TeamTotalSelected: false,
          WagerType: wagerType,
          AddingSport: false,
          SportLimits: response.SportLimits,
          ScheduleDateString: response.GameLines[0].ScheduleDateString,
          FirstRotNum: response.GameLines[0].Team1RotNum
        };
        syncSportLineCircles(subSport);
      }
      else {
        sport.Periods.push(params.Periods[0]);
        response.GameLines.forEach(function (l) {
          sport.Lines.push(l);
        });
      }
      return true;
    });
  };

  function loadSpotlightSportLines() {
    var wagerType = $wagerTypesService.Selected;
    return _caller.POST({ 'wagerType': wagerType.name, 'hoursAdjustment': 0, 'gameNum': null }, 'LoadSpotLightGameLines');
  };

  function groupSportLines(response) {
    var params = formatGameLines(response);
    var wagerType = $wagerTypesService.Selected;
    response[0].CircledInfoVisible = isCircledVisible(response[0], response[0]);
    return {
      Type: "G",
      GroupValue: "",
      IncludeTeamTotals: params.IncludeTeamTotals,
      Periods: params.Periods,
      ActivePeriod: params.Periods[0],
      Lines: response,
      OrderIndex: 0,
      PeriodDesc: "",
      SportSubTypeId: response[0].SportSubTypeId,
      SportType: response[0].SportType,
      SportSubType: response[0].SportSubType,
      TeamTotalLines: null,
      TeamTotalSelected: false,
      WagerType: wagerType,
      AddingSport: false,
      MaxWagerSpreadType: response[0].MaxWagerSpreadType,
      MaxWagerTotalType: response[0].MaxWagerTotalType,
      MaxWagerMoneyLineType: response[0].MaxWagerMoneyLineType,
      MaxWagerTeamTotalType: response[0].MaxWagerTeamTotalType
    };

  };

  //Private Contest Lines Methods

  function groupContests(contestList) {
    var contests = [];
    var contestType, contestType2, contestType3, contestDesc;
    for (var i = 0; i < contestList.length; i++) {
      if (contestType != contestList[i].ContestType ||
        contestType2 != contestList[i].ContestType2 ||
        contestType3 != contestList[i].ContestType3 ||
        contestDesc != contestList[i].ContestDesc) {
        contests.push(contestList[i]);
        contestList[i].Type = 'C';
      }
      else contests[contests.length - 1].ContestantsLines = contests[contests.length - 1].ContestantsLines.concat(contestList[i].ContestantsLines);

      contestType = contestList[i].ContestType;
      contestType2 = contestList[i].ContestType2;
      contestType3 = contestList[i].ContestType3;
      contestDesc = contestList[i].ContestDesc;
    }
    return contests;
  };

  function loadContestLines(contest) {
    return _caller.POST({ 'contestType': contest.ContestType, 'contestType2': contest.ContestType2 ? contest.ContestType2 : ".", 'contestType3': (typeof contest.ContestType3 == "undefined" ? null : contest.ContestType3) }, 'LoadContestsLines').then(function (result) {
      var response = result.data.d.Data;
      if (response == null || response == "" || response.length == 0) return false;
      response[0].SportType = contest.SportType;
      contest.Offering = groupContests(response);
      return true;
    });
  };

  function loadContestLinesByCorrelation(gameLine) {
    return _caller.POST({ 'correlation': gameLine.CorrelationID }, 'LoadContestsLinesByCorrelation').then(function (result) {
      var response = result.data.d.Data;
      if (!response) return false;
      var found = false;
      _sportsAndContestsService.Selections.forEach(function (s) {
        if (found) return;
        response.forEach(function (r) {
          if (s.ContestType2 == r.ContestType2) {
            found = true;
            return;
          }
        });
      });
      if (found) {
        UI.Notify($translatorService.Translate('Contest already open'),
          UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
        gameLine.Correlation = null;
        return false;
      }
      if (response.length == 0) return false;
      response[0].SportType = gameLine.SportType;
      gameLine.Correlation = groupContests(response);
      return true;
    });
  };

  function loadSpotlightContests() {
    return _caller.POST({ 'contestNum': null }, 'LoadSpotlightContests');
  };

  function isSportSelected(sportType, sportSubType) {
    for (var i = 0; i < _sportsAndContestsService.Selections.length; i++) {
      var selection = _sportsAndContestsService.Selections[i];
      if (selection.SportType == sportType && selection.SportSubType == sportSubType)
        return true;
    }
    return false;
  };

  function subSportInSelection(subSport) {
    return _sportsAndContestsService.Selections.some(function (sel) { return subSport.SportSubTypeId == sel.SportSubTypeId; });
  };

  //Public Methods

  window.UpdateLineOffering = function (newLoo, wagerItem, isFromSocket) {
    for (var i = 0; i < _sportsAndContestsService.SportsAndContests.length; i++) {
      var sport = _sportsAndContestsService.SportsAndContests[i];
      if (sport.SportType.trim() == newLoo.SportType) {
        for (var j = 0; j < sport.SportSubTypes.length; j++) {
          var subSport = sport.SportSubTypes[j];
          if (subSport.SportSubType.trim() == newLoo.SportSubType && subSport.Offering && subSport.Offering.Lines) {
            for (var k = 0; k < subSport.Offering.Lines.length; k++) {
              var loo = subSport.Offering.Lines[k];
              if (loo.GameNum == newLoo.GameNum && loo.PeriodNumber == newLoo.PeriodNumber) {
                if (!isFromSocket) updateGameLineData(loo, newLoo, wagerItem.WagerType, wagerItem ? wagerItem.ControlCode : null, true);
                syncLineCircles(loo);
                if (wagerItem) $ticketService.CreateBuyPointsOptions(wagerItem);
                removeEmptyLines();
                return;
              }
            }
          }
        }
      }
    }
  };

  _sportsAndContestsService.AppendContest = function (rawContestData) {
    var selectable, i;
    if (rawContestData.ContestType3 != '.') {
      for (i = 0; _sportsAndContestsService.Selections.length; i++) {
        var selection = _sportsAndContestsService.Selections[i];
        if (selection.ContestType == rawContestData.ContestType && selection.ContestType2 == rawContestData.ContestType2) {
          loadContestLines(rawContestData).then(function () {
            _sportsAndContestsService.Selections.unshift(rawContestData.Offering[0]);
          });
          break;
        }
      }
      return true;
    }
    var newContest = { "SportType": rawContestData.SportType, "ContestType": rawContestData.ContestType, "ContestType2": rawContestData.ContestType2, "ContestTypes2": new Array(), "Selectable": false, "Selected": false, "Type": "C" };
    newContest.ContestTypes2.push({ "SportType": rawContestData.SportType, "ContestType": rawContestData.ContestType, "ContestType2": rawContestData.ContestType2, "Selectable": true, "Selected": false, "Type": "C" });
    for (i = 0; i < _sportsAndContestsService.SportsAndContests.length; i++) {
      var contest = _sportsAndContestsService.SportsAndContests[i];
      if (contest.SportType == rawContestData.SportType) {
        if (rawContestData.SportSubType == null) {
          if (contest.ContestTypes.length > 0) {
            for (var k = 0; k < contest.ContestTypes.length; k++) {
              if (contest.ContestTypes[k].ContestType == rawContestData.ContestType) {
                if (contest.ContestTypes[k].ContestType2 != rawContestData.ContestType2) {
                  selectable = newContest.SportSubType == null && newContest.ContestType2 == null || newContest.ContestType2 == ".";
                  contest.ContestTypes[k].ContestTypes2.push({ "SportType": newContest.SportType, "ContestType": newContest.ContestType, "ContestType2": newContest.ContestType2, "ContestTypes2": new Array(), "Selected": false, "Selectable": selectable, "Type": "C" });
                  UI.Notify($translatorService.Translate("New contest added") + ": " +
                    selectable ? $translatorService.Translate(newContest.ContestType) : $translatorService.Translate(newContest.ContestType2), UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
                }
                return true;
              } else if (k == contest.ContestTypes.length - 1) {
                //selectable = newContest.SportSubType == null && (newContest.ContestType2 == null || newContest.ContestType2 == ".");
                contest.ContestTypes.push(newContest);
                UI.Notify($translatorService.Translate("New contest added") + ": " + $translatorService.Translate((newContest.ContestType2 == "." ? newContest.ContestType : newContest.ContestType2)), UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
                return true;
              }
            }
          } else {
            UI.Notify($translatorService.Translate("New contest added") + ": " + $translatorService.Translate(newContest.ContestType2), UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
            selectable = newContest.SportSubType == null && newContest.ContestType2 == null || newContest.ContestType2 == ".";
            contest.ContestTypes.push({ "SportType": newContest.SportType, "ContestType": newContest.ContestType, "ContestType2": newContest.ContestType, "ContestTypes2": newContest.ContestTypes2, "Selected": false, "Selectable": selectable, "Type": "C" });
          }
        } else {
          for (var j = 0; j < contest.SportSubTypes.length; j++) {
            if (contest.SportSubTypes[j].SportSubType == rawContestData.SportSubType) {
              if (contest.SportSubTypes[j].ContestTypes.length > 0)
                for (var l = 0; l < contest.SportSubTypes[j].ContestTypes.length; l++) {
                  if (contest.SportSubTypes[j].ContestTypes[l].ContestType == rawContestData.ContestType) {
                    UI.Notify($translatorService.Translate("New contest added") + ": " + $translatorService.Translate(newContest.ContestType2), UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
                    contest.SportSubTypes[j].ContestTypes[l].ContestTypes2.push(newContest.ContestTypes2[0]);
                    return true;
                  }
                  if (l == contest.SportSubTypes[j].ContestTypes.length - 1) {
                    UI.Notify($translatorService.Translate("New contest added") + ": " + $translatorService.Translate(newContest.ContestType2), UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
                    contest.SportSubTypes[j].ContestTypes.push(newContest);
                    return true;
                  }
                }
              else {
                UI.Notify($translatorService.Translate("New contest added") + ": " + $translatorService.Translate(newContest.ContestType2), UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
                contest.SportSubTypes[j].ContestTypes.push(newContest);
              }
              return true;
            }
          }
        }
      }
      if (i == _sportsAndContestsService.SportsAndContests.length - 1) {
        _sportsAndContestsService.CreateNewSportsAndContestsRow(rawContestData);
        break;
      }
    }
    return true;
  };

  _sportsAndContestsService.AppendLeague = function (i, data) {

    if (_sportsAndContestsService.SportsAndContests[i].SportType == data.SportType) {
      for (var j = 0; j < _sportsAndContestsService.SportsAndContests[i].SportSubTypes.length; j++) {
        if (_sportsAndContestsService.SportsAndContests[i].SportSubTypes[j].SportSubType == data.SportSubType) return;
      }
      _sportsAndContestsService.SportsAndContests[i].SportSubTypes.push({
        "SportSubType": data.SportSubType,
        "SequenceNumber": data.SequenceNumber,
        "ContestTypes": new Array(),
        "Selected": false,
        "Selectable": true,
        "Type": "G"
      });
      UI.Notify($translatorService.Translate("New league added") + ": " + $translatorService.Translate(_sportsAndContestsService.SportsAndContests[i]
        .SportSubTypes[_sportsAndContestsService.SportsAndContests[i].SportSubTypes.length - 1].SportSubType), UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
    }
    $rootScope.safeApply();
  };

  _sportsAndContestsService.LeagueChange = function (leagues) {
    var i;
    if (leagues.activeLeagues.length > 0) {
      var groupedData = groupSportsAndContests(leagues.activeLeagues);
      _sportsAndContestsService.ModifyPeriod(leagues.activeLeagues)
      _sportsAndContestsService.ModifyLeagueOrSport(groupedData);
      _sportsAndContestsService.ModifyContestOrSport(groupedData);

    }
    $rootScope.safeApply();
  }

  _sportsAndContestsService.ModifyPeriod = function (data) {
    if (_sportsAndContestsService.Selections.length > 0) {
      for (var j = 0; j < data.length; j++) {
        for (var i = 0; i < _sportsAndContestsService.Selections.length; i++) {
          var selection = _sportsAndContestsService.Selections[i];
          if (selection.SportType == data[j].SportType && selection.SportSubType == data[j].SportSubType) {
            for (var k = 0; k < selection.Periods.length; k++) {
              if (selection.Periods[k].PeriodNumber == data[j].PeriodNumber) break;
              if (k == selection.Periods.length - 1) {
                loadSportLines(selection, selection, { PeriodNumber: data[j].PeriodNumber, GameNum: null });
                return
              }
            }
          }
        }
      }
    }

    if (_sportsAndContestsService.Selections.length > 0) {
      for (var i = 0; i < _sportsAndContestsService.Selections.length; i++) {
        var selection = _sportsAndContestsService.Selections[i];
        for (var k = 0; k < selection.Periods.length; k++) {
          var found = false;
          for (var j = 0; j < data.length; j++) {
            if (data[j].SportType == _sportsAndContestsService.Selections[i].SportType && data[j].SportSubType == _sportsAndContestsService.Selections[i].SportSubType) {
              if (selection.Periods[k].PeriodNumber == data[j].PeriodNumber) {
                found = true;
                break;
              }
            }
          }
          if (!found) {
            _sportsAndContestsService.Selections[i].Periods.splice(k, 1);
            return;
          }
        }



      }
    }
  }

  _sportsAndContestsService.ModifyLeagueOrSport = function (data) {

    for (var j = 0; j < data.length; j++) {
      var found = false;
      for (var i = 0; i < _sportsAndContestsService.SportsAndContests.length; i++) {
        if (data[j].SportType == _sportsAndContestsService.SportsAndContests[i].SportType) {
          found = true;
          for (var l = 0; l < data[j].SportSubTypes.length; l++) {
            var newOne = data[j].SportSubTypes[l].SportSubType;
            for (var k = 0; k < _sportsAndContestsService.SportsAndContests[i].SportSubTypes.length; k++) {
              var current = _sportsAndContestsService.SportsAndContests[i].SportSubTypes[k].SportSubType;
              if (current == newOne) break;
              if (k == _sportsAndContestsService.SportsAndContests[i].SportSubTypes.length - 1 && data[j].SportSubTypes[l].ContestTypes.length == 0) {
                _sportsAndContestsService.SportsAndContests[i].SportSubTypes.push(data[j].SportSubTypes[l]);
                UI.Notify($translatorService.Translate("League added") + ": " + $translatorService.Translate(newOne),
                  UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
                return;
              }
            }
          }
        }
      }
      if (!found) {
        _sportsAndContestsService.SportsAndContests.push(data[j]);
        UI.Notify($translatorService.Translate("Sport added") + ": " + $translatorService.Translate(data[j].SportType),
          UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
        return;
      }
    }

    for (var j = 0; j < _sportsAndContestsService.SportsAndContests.length; j++) {
      var found = false;
      for (var i = 0; i < data.length; i++) {
        if (_sportsAndContestsService.SportsAndContests[j].SportType == data[i].SportType) {
          found = true;
          for (var l = 0; l < _sportsAndContestsService.SportsAndContests[j].SportSubTypes.length; l++) {
            var newOne = _sportsAndContestsService.SportsAndContests[j].SportSubTypes[l].SportSubType;
            for (var k = 0; k < data[i].SportSubTypes.length; k++) {
              var current = data[i].SportSubTypes[k].SportSubType;
              if (current == newOne) break;
              if (k == data[i].SportSubTypes.length - 1) {
                if (_sportsAndContestsService.SportsAndContests[j].SportSubTypes[l].Selected)
                  _sportsAndContestsService.ToggleSubSport(_sportsAndContestsService.SportsAndContests[j], _sportsAndContestsService.SportsAndContests[j].SportSubTypes[l]);
                _sportsAndContestsService.SportsAndContests[j].SportSubTypes.splice(l, 1);

                UI.Notify($translatorService.Translate("League removed") + ": " + $translatorService.Translate(newOne),
                  UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
                return;
              }
            }
          }
          if (data[i].SportSubTypes.length == 0 && data[i].ContestTypes.length == 0) {
            UI.Notify($translatorService.Translate("Sport removed") + ": " + $translatorService.Translate(_sportsAndContestsService.SportsAndContests[j].SportType),
              UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
            _sportsAndContestsService.SportsAndContests.splice(j, 1);
            return;
          }
        }
        if (i == data.length - 1 && !found && _sportsAndContestsService.SportsAndContests[j].ContestTypes.length == 0) {
          _sportsAndContestsService.ToggleSubSport(_sportsAndContestsService.SportsAndContests[j], _sportsAndContestsService.SportsAndContests[j].SportSubTypes[0]);
          UI.Notify($translatorService.Translate("Sport removed") + ": " + $translatorService.Translate(_sportsAndContestsService.SportsAndContests[j].SportType),
            UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
          _sportsAndContestsService.SportsAndContests.splice(j, 1);
          return;
        }
      }
    }
  };

  _sportsAndContestsService.ModifyContestOrSport = function (data) {

    for (var j = 0; j < data.length; j++) {
      var found = false;
      for (var i = 0; i < _sportsAndContestsService.SportsAndContests.length; i++) {
        if (data[j].SportType == _sportsAndContestsService.SportsAndContests[i].SportType) {
          found = true;
          var l, k, current, newOne, n, m;
          for (l = 0; l < data[j].ContestTypes.length; l++) {
            if (_sportsAndContestsService.SportsAndContests[i].ContestTypes.length == 0) {
              _sportsAndContestsService.SportsAndContests[i].ContestTypes.push(data[j].ContestTypes[l]);
              UI.Notify($translatorService.Translate("Contest added") + ": " + $translatorService.Translate(data[j].ContestTypes[l].ContestType),
                UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
              return;
            }
            for (k = 0; k < _sportsAndContestsService.SportsAndContests[i].ContestTypes.length; k++) {
              current = _sportsAndContestsService.SportsAndContests[i].ContestTypes[k].ContestType;
              newOne = data[j].ContestTypes[l].ContestType;
              if (current == newOne || k == _sportsAndContestsService.SportsAndContests[i].ContestTypes.length - 1) {
                if (data[j].ContestTypes[l].ContestTypes2.length > 0) {
                  for (m = 0; m < data[j].ContestTypes[l].ContestTypes2.length; m++) {
                    for (n = 0; n < _sportsAndContestsService.SportsAndContests[i].ContestTypes[k].ContestTypes2.length; n++) {
                      current = _sportsAndContestsService.SportsAndContests[i].ContestTypes[k].ContestTypes2[n].ContestType2;
                      newOne = data[j].ContestTypes[l].ContestTypes2[m].ContestType2;
                      if (current == newOne) break;
                      if (n == _sportsAndContestsService.SportsAndContests[i].ContestTypes[k].ContestTypes2.length - 1) {
                        _sportsAndContestsService.SportsAndContests[i].ContestTypes[k].ContestTypes2.push(data[j].ContestTypes[l].ContestTypes2[m]);
                        UI.Notify($translatorService.Translate("Contest added") + ": " + $translatorService.Translate(newOne),
                          UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
                        return;
                      }
                    }
                  }
                } else {
                  current = _sportsAndContestsService.SportsAndContests[i].ContestTypes[k].ContestType;
                  newOne = data[j].ContestTypes[l].ContestType;
                  if (current == newOne) break;
                  if (k == _sportsAndContestsService.SportsAndContests[i].ContestTypes.length - 1) {
                    _sportsAndContestsService.SportsAndContests[i].ContestTypes.push(data[j].ContestTypes[l]);
                    UI.Notify($translatorService.Translate("Contest added") + ": " + $translatorService.Translate(newOne),
                      UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
                    return;
                  }
                }
              }
            }
          }
          for (l = 0; l < data[j].SportSubTypes.length; l++) {
            if (data[j].SportSubTypes[l].ContestTypes.length == 0) continue;
            var anyContest = true;
            for (n = 0; n < data[j].SportSubTypes[l].ContestTypes.length; n++) {
              for (k = 0; k < _sportsAndContestsService.SportsAndContests[i].SportSubTypes.length; k++) {
                if (data[j].SportSubTypes[l].SportSubType != _sportsAndContestsService.SportsAndContests[i].SportSubTypes[k].SportSubType) continue;
                if (_sportsAndContestsService.SportsAndContests[i].SportSubTypes[k].ContestTypes.length == 0) {
                  _sportsAndContestsService.SportsAndContests[i].SportSubTypes[k].ContestTypes.push(data[j].SportSubTypes[l].ContestTypes[n]);
                  UI.Notify($translatorService.Translate("Contest added") + ": " + $translatorService.Translate(data[j].SportSubTypes[l].ContestTypes[n].ContestType),
                    UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
                  return;
                }

                for (m = 0; m < _sportsAndContestsService.SportsAndContests[i].SportSubTypes[k].ContestTypes.length; m++) {
                  if (_sportsAndContestsService.SportsAndContests[i].SportSubTypes[k].ContestTypes[m].ContestType == data[j].SportSubTypes[l].ContestTypes[n].ContestType) {
                    if (_sportsAndContestsService.SportsAndContests[i].SportSubTypes[k].ContestTypes[m].ContestTypes2.length > 0) {
                      for (var p = 0; p < data[j].SportSubTypes[l].ContestTypes[n].ContestTypes2.length; p++) {
                        for (var q = 0; q < _sportsAndContestsService.SportsAndContests[i].SportSubTypes[k].ContestTypes[m].ContestTypes2.length; q++) {
                          current = _sportsAndContestsService.SportsAndContests[i].SportSubTypes[k].ContestTypes[m].ContestTypes2[q].ContestType2;
                          newOne = data[j].SportSubTypes[l].ContestTypes[n].ContestTypes2[p].ContestType2;
                          if (current == newOne) break;
                          if (q == _sportsAndContestsService.SportsAndContests[i].SportSubTypes[k].ContestTypes[m].ContestTypes2.length - 1) {
                            _sportsAndContestsService.SportsAndContests[i].SportSubTypes[k].ContestTypes[m].ContestTypes2.push(data[j].SportSubTypes[l].ContestTypes[n].ContestTypes2[p]);
                            UI.Notify($translatorService.Translate("Contest added") + ": " + $translatorService.Translate(newOne),
                              UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
                            return;
                          }
                        }
                      }
                    }
                    else {
                      anyContest = true;
                      current = _sportsAndContestsService.SportsAndContests[i].SportSubTypes[k].ContestTypes[m].ContestType2;
                      newOne = data[j].SportSubTypes[l].ContestTypes[n].ContestType2;
                      if (current == newOne) break;
                      var ll = _sportsAndContestsService.SportsAndContests[i].SportSubTypes[k].ContestTypes;
                      if (m == ll.length - 1) {
                        _sportsAndContestsService.SportsAndContests[i].SportSubTypes[k].ContestTypes.push(data[j].SportSubTypes[l].ContestTypes[n]);
                        UI.Notify($translatorService.Translate("Contest added") + ": " + $translatorService.Translate(newOne),
                          UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
                        return;
                      }
                    }
                  }
                }
              }
            }
            if (!anyContest && l == data[j].SportSubTypes.length - 1) {
              _sportsAndContestsService.SportsAndContests[i].SportSubTypes.push(data[j].SportSubTypes[l]);
              UI.Notify($translatorService.Translate("Contest added") + ": " + $translatorService.Translate(data[j].SportType),
                UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
              return;
            }
          }
        }
      }
      if (!found) {
        _sportsAndContestsService.SportsAndContests.push(data[j]);
        UI.Notify($translatorService.Translate("Sport added") + ": " + $translatorService.Translate(data[j].SportType),
          UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
        return;
      }
    }


    for (var j = 0; j < _sportsAndContestsService.SportsAndContests.length; j++) {
      var found = false;
      for (var i = 0; i < data.length; i++) {
        if (_sportsAndContestsService.SportsAndContests[j].SportType == data[i].SportType) {
          found = true;
          var l, k, current, newOne, n, m;
          for (l = 0; l < _sportsAndContestsService.SportsAndContests[j].ContestTypes.length; l++) {
            if (data[i].ContestTypes.length == 0) {
              if (_sportsAndContestsService.SportsAndContests[j].ContestTypes[l].Selected)
                _sportsAndContestsService.ToggleContest(data[i].ContestTypes[k]);
              UI.Notify($translatorService.Translate("Contest removed") + ": " + $translatorService.Translate(_sportsAndContestsService.SportsAndContests[j].ContestTypes[0].ContestType),
                UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
              _sportsAndContestsService.SportsAndContests[j].ContestTypes.splice(l, 1);
              return;
            }
            for (k = 0; k < data[i].ContestTypes.length; k++) {
              current = data[i].ContestTypes[k].ContestType;
              newOne = _sportsAndContestsService.SportsAndContests[j].ContestTypes[l].ContestType;
              if (current == newOne) {
                if (_sportsAndContestsService.SportsAndContests[j].ContestTypes[l].ContestTypes2.length > 0) {
                  for (m = 0; m < _sportsAndContestsService.SportsAndContests[j].ContestTypes[l].ContestTypes2.length; m++) {
                    for (n = 0; n < data[i].ContestTypes[k].ContestTypes2.length; n++) {
                      current = data[i].ContestTypes[k].ContestTypes2[n].ContestType2;
                      newOne = _sportsAndContestsService.SportsAndContests[j].ContestTypes[l].ContestTypes2[m].ContestType2;
                      if (current == newOne) break;
                      if (n == data[i].ContestTypes[k].ContestTypes2.length - 1) {
                        if (_sportsAndContestsService.SportsAndContests[j].ContestTypes[l].ContestTypes2[m].Selected)
                          _sportsAndContestsService.ToggleContest(data[i].ContestTypes[k].ContestTypes2[n]);
                        _sportsAndContestsService.SportsAndContests[j].ContestTypes[l].ContestTypes2.splice(m, 1);
                        UI.Notify($translatorService.Translate("Contest removed") + ": " + $translatorService.Translate(newOne),
                          UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
                        return;
                      }

                    }
                  }
                  if (data[i].ContestTypes[k].ContestTypes2.length == 0) {
                    UI.Notify($translatorService.Translate("Contest removed") + ": " + $translatorService.Translate(data[i].ContestTypes[k].ContestType),
                      UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
                    _sportsAndContestsService.SportsAndContests[j].ContestTypes.splice(l, 1);
                    return;
                  }
                } else {
                  _sportsAndContestsService.SportsAndContests[j].ContestTypes.splice(l, 1);
                  UI.Notify($translatorService.Translate("Contest removed") + ": " + $translatorService.Translate(newOne),
                    UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
                  return;
                }
              }

            }

          }

          for (l = 0; l < _sportsAndContestsService.SportsAndContests[j].SportSubTypes.length; l++) {
            var anyContest = false;
            if (_sportsAndContestsService.SportsAndContests[j].SportSubTypes[l].ContestTypes.length == 0) continue;
            for (n = 0; n < _sportsAndContestsService.SportsAndContests[j].SportSubTypes[l].ContestTypes.length; n++) {
              for (k = 0; k < data[i].SportSubTypes.length; k++) {
                current = data[i].SportSubTypes[k].SportSubType;
                newOne = _sportsAndContestsService.SportsAndContests[j].SportSubTypes[l].SportSubType;
                if (current == newOne || l == _sportsAndContestsService.SportsAndContests[j].SportSubTypes.length - 1) {

                  if (data[i].SportSubTypes[k].ContestTypes.length == 0) {

                    if (_sportsAndContestsService.SportsAndContests[j].SportSubTypes[l].ContestTypes[n].Selected)
                      _sportsAndContestsService.ToggleContest(_sportsAndContestsService.SportsAndContests[j].SportSubTypes[l].ContestTypes[n]);
                    UI.Notify($translatorService.Translate("Contest removed") + ": " + $translatorService.Translate(_sportsAndContestsService.SportsAndContests[j].SportSubTypes[l].ContestTypes[n].ContestType),
                      UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
                    _sportsAndContestsService.SportsAndContests[j].SportSubTypes[l].ContestTypes.splice(n, 1);
                    return;
                  }

                  for (m = 0; m < data[i].SportSubTypes[k].ContestTypes.length; m++) {

                    if (data[i].SportSubTypes[k].ContestTypes[m].ContestTypes2.length > 0) {
                      for (var p = 0; p < _sportsAndContestsService.SportsAndContests[j].SportSubTypes[l].ContestTypes[n].ContestTypes2.length; p++) {
                        for (var q = 0; q < data[i].SportSubTypes[k].ContestTypes[m].ContestTypes2.length; q++) {
                          current = data[i].SportSubTypes[k].ContestTypes[m].ContestTypes2[q].ContestType2;
                          newOne = _sportsAndContestsService.SportsAndContests[j].SportSubTypes[l].ContestTypes[n].ContestTypes2[p].ContestType2;
                          if (current == newOne) break;
                          if (q == data[i].SportSubTypes[k].ContestTypes[m].ContestTypes2.length - 1) {
                            if (_sportsAndContestsService.SportsAndContests[j].SportSubTypes[l].ContestTypes[n].ContestTypes2[p].Selected)
                              _sportsAndContestsService.ToggleContest(_sportsAndContestsService.SportsAndContests[j].SportSubTypes[l].ContestTypes[n].ContestTypes2[p]);
                            _sportsAndContestsService.SportsAndContests[j].SportSubTypes[l].ContestTypes[n].ContestTypes2.splice(p, 1);
                            UI.Notify($translatorService.Translate("Contest removed") + ": " + $translatorService.Translate(newOne),
                              UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
                            return;
                          }
                        }
                      }
                    } else {
                      anyContest = true;
                      current = data[i].SportSubTypes[k].ContestTypes[m].ContestType2;
                      newOne = _sportsAndContestsService.SportsAndContests[j].SportSubTypes[l].ContestTypes[n].ContestType2;
                      if (current == newOne) break;
                      if (l == data[i].SportSubTypes.length - 1) {
                        if (data[i].SportSubTypes[k].ContestTypes[m].Selected)
                          _sportsAndContestsService.ToggleContest(data[i].SportSubTypes[k].ContestTypes[m]);
                        _sportsAndContestsService.SportsAndContests[j].SportSubTypes[k].ContestTypes.splice(m, 1);
                        if (!data[i].SportSubTypes[k].Selectable
                          && data[i].SportSubTypes[k].ContestTypes.length == 0) {
                          _sportsAndContestsService.SportsAndContests[j].SportSubTypes.splice(k, 1);
                        }
                        UI.Notify($translatorService.Translate("Contest removed") + ": " + $translatorService.Translate(newOne),
                          UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
                        return;
                      }
                    }
                  }
                }
              }
            }
          }
          if (data[i].SportSubTypes.length == 0 && data[i].ContestTypes.length == 0) {
            UI.Notify($translatorService.Translate("Sport removed") + ": " + $translatorService.Translate(data[i].SportType),
              UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
            _sportsAndContestsService.SportsAndContests.splice(j, 1);
            return
          }
        }
        if (i == data.length - 1 && !found) {
          UI.Notify($translatorService.Translate("Sport removed") + ": " + $translatorService.Translate(_sportsAndContestsService.SportsAndContests[j].SportType),
            UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
          _sportsAndContestsService.SportsAndContests.splice(j, 1);
          return
        }
      }
    }
  };

  _sportsAndContestsService.CreateNewSportsAndContestsRow = function (rawSportsData) {
    var sportsAndContests = groupSportsAndContests([rawSportsData]);
    for (var i = 0; i < sportsAndContests.length; i++) {
      _sportsAndContestsService.SportsAndContests.push(sportsAndContests[i]);
      UI.Notify($translatorService.Translate("New sport added") + ": " + $translatorService.Translate(sportsAndContests[i].SportType), UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
    }
    $rootScope.safeApply();
  };

  _sportsAndContestsService.RemoveContest = function (rawContestData) {
    if (rawContestData.ContestType3 != ".") return true;
    // it can be in the sporttype root, or under a subsportype
    var sportIdx = -1;
    var subSportIdx = -1;

    for (var i = 0; i < _sportsAndContestsService.SportsAndContests.length; i++) {
      if (_sportsAndContestsService.SportsAndContests[i].SportType != rawContestData.SportType)
        continue;
      sportIdx = i;
      var j;
      if (rawContestData.SportSubType == null) {
        loop1: for (j = 0; j < _sportsAndContestsService.SportsAndContests[i].ContestTypes.length; j++) {
          var contestTypes2 = _sportsAndContestsService.SportsAndContests[i].ContestTypes[j].ContestTypes2;
          if (contestTypes2.length == 0 && _sportsAndContestsService.SportsAndContests[i].ContestTypes[j].ContestType2 == '.') {
            if (_sportsAndContestsService.SportsAndContests[i].ContestTypes[j].ContestType2 == rawContestData.ContestType2
              && _sportsAndContestsService.SportsAndContests[i].ContestTypes[j].ContestType == rawContestData.ContestType) {
              UI.Notify($translatorService.Translate("Contest removed") + ": " + $translatorService.Translate(rawContestData.ContestType),
                UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
              _sportsAndContestsService.SportsAndContests[i].ContestTypes.splice(j, 1);
            }
          } else {
            for (var l = 0; l < contestTypes2.length; l++) {
              if (typeof contestTypes2[l].Offering == 'undefined') {
                if (contestTypes2[l].ContestType2 != rawContestData.ContestType2)
                  continue;
                UI.Notify($translatorService.Translate("Contest removed in") + ": " + $translatorService.Translate(rawContestData.ContestType2 == "." ? rawContestData.ContestType : rawContestData.ContestType2),
                  UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
                contestTypes2.splice(l, 1);
                if (contestTypes2.length == 0) _sportsAndContestsService.SportsAndContests[i].ContestTypes.splice(j, 1);
                break loop1;
              } else {
                for (var n = 0; contestTypes2[l].Offering.length; n++) {
                  if (contestTypes2[l].Offering[n].ContestType2 != rawContestData.ContestType2 &&
                    contestTypes2[l].Offering[n].ContestType3 != rawContestData.ContestType3)
                    continue;
                  UI.Notify($translatorService.Translate("Contest removed in") + ": " + $translatorService.Translate(rawContestData.ContestType2 == "." ? rawContestData.ContestType : rawContestData.ContestType2),
                    UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
                  contestTypes2[l].Offering.splice(n, 1);
                  if (contestTypes2[l].Offering.length == 0) contestTypes2[l].splice(l, 1);
                  if (contestTypes2.length == 0) _sportsAndContestsService.SportsAndContests[i].ContestTypes.splice(j, 1);
                  break loop1;
                }
              }
            }
          }
        }
      } else {
        loop2: for (j = 0; j < _sportsAndContestsService.SportsAndContests[i].SportSubTypes.length; j++) {
          if (_sportsAndContestsService.SportsAndContests[i].SportSubTypes[j].SportSubType != rawContestData.SportSubType)
            continue;
          subSportIdx = j;

          for (var k = 0; k < _sportsAndContestsService.SportsAndContests[i].SportSubTypes[j].ContestTypes.length; k++) {
            if (_sportsAndContestsService.SportsAndContests[i].SportSubTypes[j].ContestTypes[k].ContestType != rawContestData.ContestType
              && _sportsAndContestsService.SportsAndContests[i].SportSubTypes[j].ContestTypes[k].ContestType2 != rawContestData.ContestType2)
              continue;
            UI.Notify($translatorService.Translate("Contest removed") + ": " + $translatorService.Translate(rawContestData.ContestType2 == "." ? rawContestData.ContestType : rawContestData.ContestType2),
              UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
            _sportsAndContestsService.SportsAndContests[sportIdx].SportSubTypes[subSportIdx].ContestTypes.splice(k, 1);
            break loop2;
          }
        }
      }
      if (_sportsAndContestsService.SportsAndContests[i].SportSubTypes.length == 0 && _sportsAndContestsService.SportsAndContests[i].ContestTypes.length == 0) _sportsAndContestsService.SportsAndContests.splice(i, 1);
      for (var x = 0; x < _sportsAndContestsService.Selections.length; x++) {
        var selection = _sportsAndContestsService.Selections[x];
        if (selection.ContestType2 == rawContestData.ContestType2 && selection.ContestType3 == rawContestData.ContestType3) {
          _sportsAndContestsService.Selections.splice(x, 1);
          break;
        }
      }
    }
    $rootScope.safeApply();
    return null;
  };

  _sportsAndContestsService.RemovePeriod = function (periodData) {
    angular.forEach(_sportsAndContestsService.Selections, function (selection) {
      if (selection.SportType == periodData.SportType && selection.SportSubType == periodData.SportSubType) {
        var periodIdx = -1;

        for (var i = 0; i < selection.Periods.length; i++) {
          if (selection.Periods[i].PeriodNumber != periodData.PeriodNumber)
            continue;
          periodIdx = i;
          break;
        }
        if (periodIdx >= 0) {
          UI.Notify($translatorService.Translate("Period removed to") + ": " + $translatorService.Translate(selection.SportSubType), UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
          selection.Periods.splice(periodIdx, 1);
          $rootScope.safeApply();
        }
      }
    });
  };

  _sportsAndContestsService.RemoveSportsAndContestsRow = function (rawsportData) {

    for (var i = 0; _sportsAndContestsService.SportsAndContests.length > i; i++) {
      var sc = _sportsAndContestsService.SportsAndContests[i];
      if (rawsportData.length == 0) {
        for (var j = 0; sc.SportSubTypes.length > j; j++) {
          if (!sc.SportSubTypes[j].ContestTypes || sc.SportSubTypes[j].ContestTypes.length == 0) {
            sc.SportSubTypes.splice(j, 1);
            j--;
          }
        };
      }
      rawsportData.forEach(function (d) {
        var found = false;
        d.SportSubTypes.forEach(function (ds) {
          if (d.SportType == sc.SportType) {
            found = true;
            sc.SportSubTypes.forEach(function (sst, indexB) {
              if (ds.SportSubType == sst.SportSubType)
                return;
              if (indexB == sc.SportSubTypes.length - 1) {
                UI.Notify($translatorService.Translate("SubSport removed") + ": "
                  + $translatorService.Translate(sst.SportSubType), UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
                sc.SportSubTypes.splice(indexB, 1);
              }
            });
          }
          if (!found) {
            for (var j = 0; sc.SportSubTypes.length > j; j++) {
              if (sc.SportSubTypes[j].ContestTypes.length == 0) {
                sc.SportSubTypes.splice(j, 1);
                j--;
              }
            }
          }
        });
      });
      if (sc.SportSubTypes.length == 0 && (!sc.ContestTypes || sc.ContestTypes.length == 0)) {
        UI.Notify($translatorService.Translate("Sport removed") + ": "
          + $translatorService.Translate(sc.SportType), UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
        _sportsAndContestsService.SportsAndContests.splice(i, 1);
        i--;
        continue;
      }
    }

    $rootScope.safeApply();
  }

  _sportsAndContestsService.RemoveLeague = function (rawLeagueData) {
    var sportIdx = -1;
    var subSportIdx = -1;

    for (var i = 0; i < _sportsAndContestsService.SportsAndContests.length; i++) {
      if (_sportsAndContestsService.SportsAndContests[i].SportType != rawLeagueData.SportType)
        continue;
      sportIdx = i;

      for (var j = 0; j < _sportsAndContestsService.SportsAndContests[i].SportSubTypes.length; j++) {
        if (_sportsAndContestsService.SportsAndContests[i].SportSubTypes[j].SportSubType != rawLeagueData.SportSubType)
          continue;
        subSportIdx = j;
        break;
      }
      break;
    }
    if (sportIdx >= 0 && subSportIdx >= 0) {
      UI.Notify($translatorService.Translate("League removed") + ": " + $translatorService.Translate(_sportsAndContestsService.SportsAndContests[sportIdx].SportSubTypes[subSportIdx].SportSubType), UI.Position.Top, UI.Position.Center, 200, UI.Type.Warning);
      _sportsAndContestsService.SportsAndContests[sportIdx].SportSubTypes.splice(subSportIdx, 1);
      $rootScope.safeApply();
    }
  };

  _sportsAndContestsService.ToggleSubSport = function (sport, subSport, isSpotlight) {
    subSport.Selected = !subSport.Selected;
    isSpotlight = typeof isSpotlight != 'undefined' ? isSpotlight : false;
    if (!$rootScope.ViewsManager.IsSportView(true) || $wagerTypesService.IsTeaser()) return;
    if (!isSpotlight) {
      if (!isSportSelected(sport.SportType, subSport.SportSubType)) {
        if (subSport.Offering == null) {
          loadSportLines(sport, subSport, null).then(function () {
            bindGameLines(subSport);
            $inboxService.GetMessageNotificationBySport(sport.SportType, subSport.SportSubType).then(function (result) {
              if (!result.data.d.Data || result.data.d.Data.length == 0) return null;
              var allowed = [];
              var found = false;
              if ($rootScope.bannedMessages != null) {
                result.data.d.Data.forEach(function (e) {
                  $rootScope.bannedMessages.forEach(function (b) {
                    if (e.MessageID == b.MessageID && $customerService.Info.CustomerID.trim() == b.CustomerID) found = true;
                  });
                  if (!found) allowed.push(e);
                });
                subSport.Offering.MessagesNotficationList = allowed;
              } else return subSport.Offering.MessagesNotficationList = result.data.d.Data;
              return found;
            });
            subSport.Selected = subSport.Offering ? true : false;
          });
        } else
          bindGameLines(subSport);

        $rootScope.$broadcast('sportSubscribed', {
          "SportType": sport.SportType,
          "SportSubType": subSport.SportSubType
        });
      } else {
        unbindGameLines(sport, subSport);
        if (!$ticketService.SportHasWagersSelected(sport.SportType, subSport.SportSubType))
          subSport.Offering = null;
        subSport.Selected = false;
        $rootScope.$broadcast('sportUnsubscribed', {
          "SportType": sport.SportType,
          "SportSubType": subSport.SportSubType
        });
      }
    } else {
      var i = 0;
      while (i < _sportsAndContestsService.Selections.length && _sportsAndContestsService.Selections.length > 0)
        if (_sportsAndContestsService.Selections[i].IsSpotlight) {
          if (_sportsAndContestsService.Selections[i].Type == "G"
            && !$ticketService.SportHasWagersSelected(_sportsAndContestsService.Selections[i].SportType, _sportsAndContestsService.Selections[i].SportSubType)) {
            $rootScope.$broadcast('sportUnsubscribed', {
              "SportType": _sportsAndContestsService.Selections[i].SportType,
              "SportSubType": _sportsAndContestsService.Selections[i].SportSubType
            });

          } /*else if (sportsAndContestsService.Selections[i].Type == "C"
              && sportsAndContestsService.Selections[i].ContestType == sportsAndContestsService.Selections[i].ContestType
            && sportsAndContestsService.Selections[i].ContestType2 == sportsAndContestsService.Selections[i].ContestType2) {
            $rootScope.$broadcast('contestUnsubscribed', {
              "ContestType": sportsAndContestsService.Selections[i].ContestType,
              "ContestType2": sportsAndContestsService.Selections[i].ContestType2
            });
          }*/
          _sportsAndContestsService.Selections.splice(i, 1);
          i = 0;
        } else i++;
    }
  };

  _sportsAndContestsService.ShowContestByCorrelation = function (gameLine) {
    if (gameLine.Offering != null) return null;
    return loadContestLinesByCorrelation(gameLine).then(function () {
      if (!gameLine.Correlation) return;
    });
  };

  _sportsAndContestsService.ShowContest = function (contest) {
    if (contest.Offering != null) return null;
    return loadContestLines(contest).then(function () {
      if (contest.Offering != null) bindContestLines(contest);
      contest.Selected = contest.Offering != null;
      $rootScope.$broadcast('contestSubscribed', {
        "ContestType": contest.ContestType,
        "ContestType2": contest.ContestType2
      });
    });
  };

  _sportsAndContestsService.HideContestByCorrelation = function (gameLine) {
    if (gameLine.Correlation == null) return;
    $rootScope.$broadcast('contestUnsubscribed', {
      "ContestType": gameLine.Correlation.Offering.ContestType,
      "ContestType2": gameLine.Correlation.Offering.ContestType2
    });
  };

  _sportsAndContestsService.HideContest = function (contest) {
    if (contest.Offering == null) return;
    unbindContestLines(contest);
    contest.Selected = contest.Offering != null;
    $rootScope.$broadcast('contestUnsubscribed', {
      "ContestType": contest.ContestType,
      "ContestType2": contest.ContestType2
    });
  };

  _sportsAndContestsService.ShowSubSport = function (sport, subSport) {
    if (subSport.Offering != null) return null;
    return loadSportLines(sport, subSport, null).then(function () {
      bindGameLines(subSport);
      sport.Selected = sport.Offering != null;
      $rootScope.$broadcast('sportSubscribed', {
        "SportType": sport.SportType,
        "SportSubType": subSport.SportSubType
      });
    });

  };

  _sportsAndContestsService.HideSubSport = function (sport, subSport) {
    if (subSport.Offering == null) return;
    unbindGameLines(sport, subSport);
    if (!$ticketService.SportHasWagersSelected(sport.SportType, subSport.SportSubType))
      subSport.Offering = null;
    $rootScope.$broadcast('sportUnsubscribed', {
      "SportType": sport.SportType,
      "SportSubType": subSport.SportSubType
    });
  };

  _sportsAndContestsService.ToggleContest = function (contest) {
    if (!$rootScope.ViewsManager.IsSportView(true) || !$wagerTypesService.IsStraightBet())
      return;
    if (!contest.Offering) {
      _sportsAndContestsService.ShowContest(contest);
    } else {
      _sportsAndContestsService.HideContest(contest);
    }
  };

  _sportsAndContestsService.ToggleContestByCorrelation = function (gameLine) {
    if (!$rootScope.ViewsManager.IsSportView(true) || !$wagerTypesService.IsStraightBet())
      return;
    if (gameLine.Correlation) {
      gameLine.Correlation = null;
      return;
    }
    _sportsAndContestsService.ShowContestByCorrelation(gameLine);

  };

  _sportsAndContestsService.SpotlightContest = function () {
    if (!$rootScope.ViewsManager.IsSportView(true) || !$wagerTypesService.IsStraightBet())
      return;
    loadSpotlightContests().then(function (result) {
      if (result == null) return;
      var response = result.data.d.Data;
      if (response == null || response.length == 0) {
        $rootScope.spotlight.ConstestStatus = false;
        return;
      }
      var contest = {
        Offering: groupContests(response)
      };
      bindContestLines(contest, true);
      $rootScope.spotlight.Switch = true;
      $rootScope.$broadcast('contestSubscribed', {
        "ContestType": contest.Offering[0].ContestType,
        "ContestType2": contest.Offering[0].ContestType2
      });
    });
  };

  _sportsAndContestsService.SpotlightGame = function () {
    if (!$rootScope.ViewsManager.IsSportView(true) || !$wagerTypesService.IsStraightBet())
      return;
    loadSpotlightSportLines().then(function (result) {
      if (result == null) return;
      var response = result.data.d.Data;
      if (response == null || response.length == 0) {
        $rootScope.spotlight.GameStatus = false;
        return;
      }
      $rootScope.spotlight.Switch = true;
      var tempResponse = [];
      for (var i = 0; i < response.length; i++) {
        var anchor;
        //response[i].SportSubType = "Spotlight/" + response[i].SportSubType;
        tempResponse.push(response[i]);
        if (response.length - 1 == i || (response[i + 1].SportSubType != response[i].SportSubType)) {
          var currentGame = {
            Offering: groupSportLines(tempResponse)
          };
          currentGame.Offering.IsSpotlight = true;
          bindGameLines(currentGame);
          $rootScope.$broadcast('sportSubscribed', {
            "SportType": response[i].SportType,
            "SportSubType": response[i].SportSubType
          });
          setTimeout(function () {
            var selection = response[0];
            if (subSportInSelection(selection)) {
              anchor = "#divSubSport_" + response[0].SportSubTypeId;
              $('#lineDiv').animate({
                scrollTop: $('#lineDiv').scrollTop() + $(anchor).offset().top - 100
              }, 'fast');
            };
          }, 500);
          tempResponse = [];
        }
      };
    });
  };

  _sportsAndContestsService.GetActiveTeasers = function () {
    return _caller.POST({}, 'GetActiveTeasers').then(function (response) {
      _sportsAndContestsService.Teasers = response.data.d.Data;
      var addOnce = true;
      var toggleRestriction = false;
      for (var i = 0; $customerService.Restrictions.length > i; i++) {
        if ($customerService.Restrictions[i].Code == "TEOPEN") {
          toggleRestriction = true;
          break;
        }
      }
      _sportsAndContestsService.Teasers.forEach(function (teaser) {
        teaser.OpenAllowed = false;
        if (teaser.SportType == "NFL Teaser" && addOnce && toggleRestriction) {
          var openteaser = jQuery.extend(true, {}, teaser);
          openteaser.OpenAllowed = true;
          _sportsAndContestsService.Teasers.push(openteaser);
          addOnce = false;
        }
      });
      _sportsAndContestsService.SportsAndContests = null;
    });
  };

  _sportsAndContestsService.ToggleTeasers = function (teaser, isOpenTeaser) {
    $rootScope.FirstLoad = false;
    if (_sportsAndContestsService.Selections)
      while (_sportsAndContestsService.Selections.length > 0) _sportsAndContestsService.Selections.pop();
    if (_sportsAndContestsService.SportsAndContests)
      while (_sportsAndContestsService.SportsAndContests.length > 0) _sportsAndContestsService.SportsAndContests.pop();
    $ticketService.Ticket.OpenWagerItems = [];
    $ticketService.Ticket.TeaserName = teaser.SportType;
    return _caller.POST({ 'teaserName': teaser.SportType, 'periodWagerCutoff': 0, 'hoursAdjustment': 0, 'isOpenTeaser': isOpenTeaser }, 'LoadTeaserGames').then(function (response) {
      _sportsAndContestsService.SportsAndContests = groupTeaserSports(response.data.d.Data.TeaserGameLines);

      for (var i = 0; i < _sportsAndContestsService.SportsAndContests.length; i++)
        for (var j = 0; j < _sportsAndContestsService.SportsAndContests[i].SportSubTypes.length; j++) {
          var subSport = _sportsAndContestsService.SportsAndContests[i].SportSubTypes[j];
          for (var l = 0; l < subSport.Offering.Lines.length; l++) {
            var line = subSport.Offering.Lines[l];
            line.SportLimits = subSport.Offering.SportLimits;
            syncLineCircles(line);
            line.ActivePeriod = subSport.Offering.ActivePeriod;

          }
          bindGameLines(_sportsAndContestsService.SportsAndContests[i].SportSubTypes[j]);
        }
      $rootScope.TeaserLoaded = true;
      if (!isOpenTeaser) $ticketService.StartNewTicket();
      else $ticketService.GetWagerPicks().then();
      $rootScope.$broadcast('TeaserLoaded');
    });
  };

  _sportsAndContestsService.GetActiveSportsAndContests = function (getInactiveSports) {
    return _caller.POST({ getInactiveSports: getInactiveSports }, 'GetLinkedSportsAndContests').then(function (response) {
      _sportsAndContestsService.Teasers = null;
      _sportsAndContestsService.SportsAndContests = groupSportsAndContests(response.data.d.Data);
      $rootScope.$broadcast('SportsAndContestsLoaded');
      if (!$customerService.Settings || !$customerService.Settings.FavoriteSport) return false;
      for (var i = 0; i < _sportsAndContestsService.SportsAndContests.length; i++) {
        for (var j = 0; _sportsAndContestsService.SportsAndContests != null && j < _sportsAndContestsService.SportsAndContests[i].SportSubTypes.length; j++) {
          if (_sportsAndContestsService.SportsAndContests[i].SportSubTypes[j] &&
            _sportsAndContestsService.SportsAndContests[i].SportSubTypes[j].Type == "G" && _sportsAndContestsService.SportsAndContests[i].SportSubTypes[j].Selectable &&
            $customerService.Settings.FavoriteSport == _sportsAndContestsService.SportsAndContests[i].SportType &&
            $customerService.Settings.FavoriteSportSubType == _sportsAndContestsService.SportsAndContests[i].SportSubTypes[j].SportSubType
          ) {
            _sportsAndContestsService.ToggleSubSport(_sportsAndContestsService.SportsAndContests[i], _sportsAndContestsService.SportsAndContests[i].SportSubTypes[j]);
            setTimeout(function () {
              _sportsAndContestsService.SportsAndContests[i].SportSubTypes[j].Selected = true;
              $('[data-toggle="tooltip"]').tooltip();
              $rootScope.safeApply();
            }, 500);
            return true;
          }
        }
      }
      return false;
    });
  };

  _sportsAndContestsService.RemoveWagerSelections = function () {
    _sportsAndContestsService.Selections = [];
  };

  _sportsAndContestsService.UpdateLines = function (lChanges) { // adjust: linechanges are the real lines now
    if (!lChanges.length) {
      var t = lChanges;
      lChanges = [];
      lChanges.push(t);
    }
    for (var i = 0; i < lChanges.length; i++) {
      var lChanged = lChanges[i];
      if (lChanged.Store.trim() != $customerService.Info.Store.trim()) continue;
      for (var x = 0; x < _sportsAndContestsService.Selections.length; x++) {
        var selection = _sportsAndContestsService.Selections[x];
        if (lChanged.ContestantNum > 0) {
          if (selection.Type == "C" && selection.ContestType == lChanged.ContestType && selection.ContestType2 == lChanged.ContestType2 && selection.ContestType3 == lChanged.ContestType3) {
            contestLineUpdate(lChanged, selection);
          }
        }
        else {
          if (selection.Type == "G" && selection.SportType.trim() == lChanged.SportType.trim() && selection.SportSubType.trim() == lChanged.SportSubType.trim()) {
            gameLineUpdate(lChanged, selection);
          }
        }
      }
    };
  };

  return _sportsAndContestsService;

}]);
