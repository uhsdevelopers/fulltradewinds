﻿appModule.factory('$customerService', ['$http', '$rootScope', '$translatorService', function ($http, $rootScope, $translatorService) {

  var _caller = new ServiceCaller($http, null, 'customerService');

  var _customerService = { AutoAccept: false, Restrictions: [] };

  function currentBalance (balance) {
    return {
      AvailableBalance: balance != null ? balance.AvailableBalance : null,
      CurrentBalance: balance != null ? balance.CurrentBalance : null,
      CreditLimit: balance != null ? balance.CreditLimit : null,
      CasinoBalance: balance != null ? balance.CasinoBalance : null,
      FreePlayBalance: balance != null ? balance.FreePlayBalance : null,
      PendingWagerBalance: balance != null ? balance.PendingWagerBalance : null,
      FreePlayPendingBalance: balance != null ? balance.FreePlayPendingBalance : null
    };
  };

  var _currentInfo = currentBalance(null);

  _customerService.RestrictionCodes = {
    Wagering: 'WAGERING',
    VirtualCasino: 'VCASINO',
    LiveDealer: 'LDEALER',
    Horses: 'HORSES',
    LiveBetting: 'LBETTING',
    SpecialParlayTeaserAccess: 'PTKEYRULE',
    FootballMonelyDisablePoints: 'FOOTMLDIS',
    BasketballMonelyDisablePoints: 'BASKMLDIS',
    ChangeLineInCustomerFavor: 'CSTFVRLINE',
    DebugActions: 'CSTDEBUG',
    AgentDistribution: 'AGDISTRIB',
    AgentControlCasino: 'CTRLCASINO',
    AgentPostFreePlay: 'POSTFPTRN',
    AgentHidePassword: 'HIDEPWD'
  };

  
  _customerService.GetCustomerInfo = function (resetLastSession) {
    if (typeof resetLastSession == "undefined") resetLastSession = true;
    _caller.POST({ resetLastSession: resetLastSession }, 'GetCustomerInfo', null, true).then(function (result) {
      _customerService.Info = result.data.d.Data.CustomerInfo;
      _customerService.Settings = result.data.d.Data.CustomerSettings;
      _customerService.Balances = result.data.d.Data.CustomerBalances;
      _customerService.Restrictions = result.data.d.Data.CustomerRestrictions;
      _customerService.MinimumWagerAmt = result.data.d.Data.MinimumWagerAmt;
      if (_customerService.Info.Active == "N") {
        UI.Notify($translatorService.Translate("Your user has been inactived. You will be logged out"), UI.Position.Top, UI.Position.Center, 200, UI.Type.Danger);
        return setTimeout(function () {
          $rootScope.Logout();
        }, 8000);
      }
      if (!$rootScope.RecentTicketProcessed)
        if (_currentInfo.CurrentBalance != null && _currentInfo != currentBalance(_customerService.Balances)) {
          if (_currentInfo.CreditLimit != result.data.d.Data.CustomerBalances.CreditLimit)
            UI.Notify($translatorService.Translate("Credit_Limit") + ' ' + $translatorService.Translate("has_changed"), UI.Position.Top, UI.Position.Center, 200, UI.Type.Info);

          if (_currentInfo.FreePlayBalance != result.data.d.Data.CustomerBalances.FreePlayBalance)
            UI.Notify($translatorService.Translate("FreePlay_Balance") + ' ' + $translatorService.Translate("has_changed"), UI.Position.Top, UI.Position.Center, 200, UI.Type.Info);
        }
      _currentInfo = currentBalance(_customerService.Balances);
      _customerService.Info.CurrencyCode = _customerService.Info.Currency && _customerService.Info.Currency.length > 3 ? _customerService.Info.Currency.substring(0, 3) : "";
      CommonFunctions.CustomerTimeZone = _customerService.Info.TimeZone;
      _customerService.AutoAccept = _customerService.GetCustomerRestriction('AUTOLINECH');
      $rootScope.$broadcast('customerInfoLoaded');
      return null;
    });
  };

  _customerService.GetCustomerInfo();

  _customerService.GetCustomerRestriction = function (code) {
    for (var i = 0; _customerService.Restrictions.length > i; i++) {
      if (_customerService.Restrictions[i].Code == code) return _customerService.Restrictions[i] != null;
    }
    return null;
  };

  _customerService.GetRestriction = function (restrictionCode) {
    if (!this.Restrictions || this.Restrictions.length == 0) return null;
    var restriction = null;
    for (var i = 0; i < this.Restrictions.length; i++) {
      if (this.Restrictions[i].Code == restrictionCode) {
        if (!restriction) {
          restriction = this.Restrictions[i];
          restriction.Params = [];
        }
        restriction.Params.push({ ParamValue: this.Restrictions[i].ParamValue, ParamName: this.Restrictions[i].ParamName });
      }
    }
    return restriction;
  };
  
  _customerService.GetRestrictionParamValue = function (restriction, paramName) {
    if (!restriction || !paramName || !restriction.Params || restriction.Params.length == 0) return null;
    for (var i = 0; i < restriction.Params.length; i++) {
      if (restriction.Params[i].ParamName == paramName) return restriction.Params[i].ParamValue;
    }
    return null;
  };

  _customerService.FlagCustomerMessage = function (messageId, targetAction) {
    _caller.POST({ 'messageId': messageId, 'targetAction': targetAction }, 'FlagCustomerMessage', null, true).success(function (response) {
      return response;
    });
  },

  _customerService.Logout = function () {
    return _caller.POST({}, 'Logout').then(function () { });
  };

  _customerService.CustomerActionLogger = function (action, data) {
    return _caller.POST({ 'action': action, 'data': data || '' }, 'CustomerActionLogger', null, true).then();
  };

  _customerService.ColchianLoginPlayer = function (customerId, password) {
    return _caller.POST({
      sourceId: 'TRADEWINDSID',
      sourcePassword: '7R4D3W1ND5',
      playerId: customerId,
      password: password,
      userId: 'EXTERNALAU',
      applicationId: 'COLC',
      logonSource: 1,
      websiteId: 7431,
      partnerId: '',
      thirdPartyToken: ''
    }, 'GetColchianToken').then();

  };

  _customerService.GetCustomerNonPostedCasinoPlays = function () {
    return _caller.POST({}, 'GetCustomerNonPostedCasinoPlays', null, true).then(function (result) {
      return result;
    });
  };

  _customerService.TrackStatus = function () {
    return _caller.POST({}, 'TrackStatus', null, true);
  }

  return _customerService;

}]);