﻿appModule.factory('$webSocketService', ['$sportsAndContestsService', '$customerService', '$inboxService', '$errorHandler', '$rootScope', function ($sportsAndContestsService, $customerService, $inboxService, $errorHandler, $rootScope) {

  var _webSocketService = {
    connectionSate: 0,
    lastMessage: ""
  };
  var _connectionSate = 0;
  var _servers = SETTINGS.SocketServers;
  var _selectedServer;
  var _customerSubscribed = false;
  var _errorAttempts = 0;

  function getSelectServer () {
    if (!_servers || _servers.length == 0) return "";
    if (_selectedServer) return _servers[_selectedServer];
    else _selectedServer = Math.floor(Math.random() * _servers.length);
    return _servers[_selectedServer];
  };

  function logLastAction(data) {
    if (!data) return;
    _webSocketService.lastMessage = data.Context ? data.Context : data;
    var d = new Date();
    _webSocketService.lastMessage += " / " + d.toLocaleTimeString();
  }

  jQuery.support.cors = true;

  var _ws = $ != null ? $.connection : null;

  var _server = getSelectServer();

  if (_ws && _server != "") _ws.hub.url = _server;

  var _sidHub = _ws != null ? _ws.sidHub : null;

  var _hubReady = null;

  function hubIsActive() {
    if (_errorAttempts > 3) {
      _webSocketService.connectionSate = 3;
      _ws.hub.stop();
      return false;
    }
    return _sidHub;
  };

  if (hubIsActive() && _ws) {
    _ws.hub.logging = true;
    _hubReady = _ws.hub.start(function () {
    }).fail(function (reason) {
      _webSocketService.connectionSate = 2;
      logLastAction("Connection Fail: " + reason);
    });
    _ws.hub.stateChanged(function (change) {
      _webSocketService.connectionSate = change.newState;
      logLastAction("Connection Changed");
    });

    window.onbeforeunload = function () {
      _ws.hub.stop();
      _webSocketService.connectionSate = 0;
    };
  }
  else {
    _webSocketService.connectionSate = 2;
    logLastAction("Hub is not active");
  }

  _webSocketService.SubscribeCustomer = function (customerId, agentId, store) {
    if (!hubIsActive() || _customerSubscribed) return;
    _hubReady.done(function () {
      try {
        logLastAction("SubscribeCustomer");
        _customerSubscribed = true;
        _sidHub.server.subscribeCustomer({ CustomerId: customerId, AgentId: agentId, Store: store, Platform: $rootScope.IsMobileDevice ? 'Mobile' : 'WEB' }).fail(function (reason) {
          _errorAttempts++;
          logLastAction("SubscribeCustomer fail: " + reason);
        });
      } catch (ex) {
        console.log(ex);
      }
    });
  };

  _webSocketService.RemoveAsiansAndRestrictedParlay = function () {
    if (!hubIsActive()) return;
    /*hubReady.done(function () {
      sidHub.server.removeAsiansAndRestrictedParlay();
    });*/
  };

  _webSocketService.SetAsiansAndRestrictedParlay = function () {
    if (!hubIsActive()) return;
    /*hubReady.done(function () {
      sidHub.server.setAsiansAndRestrictedParlay();
    });*/
  };

  _webSocketService.SubscribeContest = function (contestType, contestType2, store) {
    if (!hubIsActive()) return;
    _hubReady.done(function () {
      logLastAction("SubscribeContest");
      _sidHub.server.subscribeSport({ ContestType: contestType, ContestType2: contestType2, Store: store, Type: 2 }).fail(function (reason) {
        _errorAttempts++;
        logLastAction("SubscribeCustomer fail: " + reason);
      });
    });
  };

  _webSocketService.SubscribeSport = function (sportType, sportSubType, store) {
    try {
      if (!hubIsActive()) return;
      _hubReady.done(function () {
        logLastAction("SubscribeSport");
        _sidHub.server.subscribeSport({ SportType: sportType, SportSubType: sportSubType, Store: store, Type: 1 }).fail(function (reason) {
          _errorAttempts++;
          logLastAction("SubscribeCustomer fail: " + reason);
        });
      });
    } catch (ex) {
      console.log(ex);
    }
  };

  _webSocketService.SubscribeSports = function (sports) {
    if (!hubIsActive()) return;
    _hubReady.done(function () {
      logLastAction("SubscribeSports");
      sports.forEach(function (s) {
        console.log(s);
        _sidHub.server.subscribeSport({ SportType: s.SportType, SportSubType: s.SportSubType, Store: s.Store, Type: 1 }).fail(function (reason) {
          _errorAttempts++;
          logLastAction("SubscribeSports fail: " + reason);
        });
      });
    });
  };

  _webSocketService.UnsubscribeContest = function (contestType, contestType2) {
    if (!hubIsActive()) return;
    _hubReady.done(function () {
      logLastAction("UnsubscribeContest");
      _sidHub.server.unsubscribeContest({ ContestType: contestType, ContestType2: contestType2, Type: 2 }).fail(function (reason) {
        _errorAttempts++;
        logLastAction("UnsubscribeContest fail: " + reason);
      });
    });
  };

  _webSocketService.UnsubscribeSport = function (sportType, sportSubType) {
    if (!hubIsActive()) return;
    _hubReady.done(function () {
      logLastAction("UnsubscribeSport");
      _sidHub.server.unsubscribeSport({ SportType: sportType, SportSubType: sportSubType, Type: 1 }).fail(function (reason) {
        _errorAttempts++;
        logLastAction("UnsubscribeSport fail: " + reason);
      });
    });
  };

  _webSocketService.UnsubscribeSports = function (sports) {
    if (!hubIsActive()) return;
    _hubReady.done(function () {
      logLastAction("UnsubscribeSports");
      sports.forEach(function (s) {
        _sidHub.server.unsubscribeSport({ SportType: s.SportType, SportSubType: s.SportSubType, Type: 1 }).fail(function (reason) {
          _errorAttempts++;
          logLastAction("UnsubscribeSports fail: " + reason);
        });
      });
    });
  };

  _webSocketService.IsSupported = function () {
    if ("WebSocket" in window) return true;
    //_ReportError("WebSocket NOT supported by your Browser!");
    return false;
  };

  _webSocketService.IsConnected = function () {
    return _webSocketService.connectionSate != 4;
  };

  _webSocketService.GetState = function () {
    return _webSocketService.connectionSate;
  };

  if (hubIsActive()) {

    _sidHub.client.leagueChange = function (leagues) {
      $sportsAndContestsService.LeagueChange(leagues);
    }

    _sidHub.client.gamesChanged = function (games) {
      $sportsAndContestsService.GamesChanged(games);
    }



    _sidHub.client.broadcastMessage = function (message) {
      message = $.parseJSON(message);
      logLastAction(message);
      switch (message.Context) {
        case "CustomerInboxChanges":
          $inboxService.InboxChanged(message.Data);
          break;
        case "CustomerInfoChanges":
          $rootScope.$broadcast('customerInfoChanged', { resetLastSession: false });
          break;
        case "CustomerBalanceChanges":
          $customerService.CustomerBalanceChanges(message.Data);
          break;
        case "LineChanges":
          $sportsAndContestsService.UpdateLines(message.Data);
          break;
        case "New Contest":
          $sportsAndContestsService.AppendContest(message.Data);
          $rootScope.safeApply();
          break;
        case "New League":
        case "New Period":
          $sportsAndContestsService.AppendPeriod(message.Data);
          break;
        case "New Sport":
          $sportsAndContestsService.CreateNewSportsAndContestsRow(message.Data);
          break;
        case "Removed Contest":
          $sportsAndContestsService.RemoveContest(message.Data);
          break;
        case "Removed League":
          $sportsAndContestsService.RemoveLeague(message.Data);
          break;
        case "Removed Period":
          $sportsAndContestsService.RemovePeriod(message.Data);
          break;
        case "Removed Sport":
          $sportsAndContestsService.RemoveSportsAndContestsRow(message.Data);
          break;
        case "Request User Subscribtion":
          _webSocketService.SubscribeCustomer($customerService.Info.CustomerID.trim(), $customerService.Info.Store.trim());
          break;
      }
    }

    _sidHub.client.closeSession = function () {
      $rootScope.Logout();
    };
  }

  _webSocketService.GetCurrentServer = function () {
    return _servers[_selectedServer];
  }

  $rootScope.$on("customerInfoLoaded", function () {
    _webSocketService.SubscribeCustomer($customerService.Info.CustomerID.trim(), $customerService.Info.AgentID.trim(), $customerService.Info.Store.trim());
  });

  return _webSocketService;

}]);