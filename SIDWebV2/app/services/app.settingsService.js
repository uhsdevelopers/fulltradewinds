﻿appModule.factory('$settingsService', ['$http', '$rootScope', function ($http, $rootScope) {

  var _caller = new ServiceCaller($http, null, 'CustomerService');
  var _gbAppVersion = "20150628-2";
  var _settingsService = {};

  _settingsService.Languages = [];
  _settingsService.AvailableHomePages = [];
  _settingsService.BaseballActionTypes = [];
  _settingsService.TimeZones = [];
  _settingsService.MainSports = [];

  _settingsService.GetLanguages = function () {
    return _caller.GET('/Data/LangFiles.json?v=' + _gbAppVersion).then(function (result) {
      _settingsService.Languages = result.data;
      $rootScope.$broadcast('Languages');
    });

  };

  _settingsService.GetAvailableHomePages = function () {
    _caller.GET('/Data/HomePages.json?v=' + _gbAppVersion).then(function (result) {
      _settingsService.AvailableHomePages = result.data;
      $rootScope.$broadcast('HomePages');
    });
  };

  _settingsService.GetBaseballActionTypes = function () {
    _caller.GET('/Data/BaseballAction.json?v=' + _gbAppVersion).then(function (result) {
      _settingsService.BaseballActionTypes = result.data;
      $rootScope.$broadcast('BaseballActions');
    });
  };

  _settingsService.GetTimeZones = function () {
    _caller.GET('/Data/TimeZones.json?v=' + _gbAppVersion).then(function (result) {
      _settingsService.TimeZones = result.data;
      $rootScope.$broadcast('TimeZones');
    });
  };

  _settingsService.GetMainSports = function () {
    _caller.GET('/Data/MainSports.json?v=' + _gbAppVersion).then(function (result) {
      _settingsService.MainSports = result.data;
      $rootScope.$broadcast('MainSports');
    });
  };

  _settingsService.GetSplitBanners = function () {
    _caller.GET('/Data/betSlipBanners.json?v=' + _gbAppVersion).then(function (result) {
      var today = new Date;
      _settingsService.SplitBanners = [];
      for (var i = 0; i < result.data.Banners.length; i++) {
        if (result.data.Banners[i].Active || (Date(result.data.Banners[i].ActiveFrom) < today && Date(result.data.Banners[i].ActiveTo) > today)) _settingsService.SplitBanners.push(result.data.Banners[i]);
      }
      $rootScope.$broadcast('SplitBanners');
    });
  };

  _settingsService.SaveCustomerSettings = function (language, homePage, sportType, sportSubType, teamId, baseballAction, rememberPassword, customerTimeZone) {
    return _caller.POST({
      'webLanguage': language,
      'webHomePage': homePage,
      'favoriteSport': sportType,
      'favoriteSportSubType': sportSubType,
      'favoriteTeamId': teamId,
      'baseballAction': baseballAction,
      'webRememberPassword': rememberPassword,
      'custTimeZone': customerTimeZone
    }, 'SaveCustomerSettings').then(function (result) {
      $rootScope.$broadcast('SettingsSaved');
      return result;
    });
  };

  _settingsService.AutoAcceptSwitch = function (temporal) {
    return _caller.POST({ 'temporal': temporal }, 'AutoAcceptSwitch').then(function () {
      return false;
    });
  };

  return _settingsService;


}]);