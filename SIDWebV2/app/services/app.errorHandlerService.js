﻿appModule.factory('$errorHandler', ['$rootScope', '$http', function ($rootScope, $http) {

    var _caller = new ServiceCaller($http, null, 'ErrorService');

    var _errorHandler = {};

    _errorHandler.stackLinesToReport = 4;
    _errorHandler.output = "service"; // console, service or screen
    _errorHandler.active = true;
    _errorHandler.appName = "SIDWeb";

    _errorHandler.GetStackLines = function (stack) {
        var strRet = "";
        var stackArray = stack.split("\n");
        this.stackLinesToReport = 4;
        for (var i = 0; i < this.stackLinesToReport; i++) {
            strRet += stackArray[i];
            if (i < this.stackLinesToReport - 1) strRet += "\n";
        }
        return strRet;
    };

    _errorHandler.SubmitError = function (data) {
        var stackTrace = data.Message + "\n" + data.StackLines;
        _caller.POST({ 'appName': _errorHandler.appName, 'stackTrace': stackTrace }, 'Error', null, true).then();
    };

    _errorHandler.Error = function (data, method) {
        if (typeof method === "undefined") method = "Undefined";
        if (_errorHandler.active) {
            var stackLines = "";
            var msg = "Method: " + method + ". ";
            if (typeof data == 'string') {
                msg += "str: " + data;
            } else if (data.type != null && data.target != null && data.type === "error") msg += JSON.stringify(data);
            else if (typeof data == 'object') {
                if (data.stack) stackLines = _errorHandler.GetStackLines(data.stack);
                try {
                    msg += data.toString();
                } catch (ex) {
                }
            }
            var obj = { StackLines: stackLines, Message: msg };
            if (_errorHandler.output === "console") log(data);
            else if (_errorHandler.output === "screen") alert(data);
            else _errorHandler.SubmitError(obj);
        }
    };

    return _errorHandler;

}]);