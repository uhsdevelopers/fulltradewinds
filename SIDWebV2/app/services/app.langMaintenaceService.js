﻿appModule.factory('$JsonManagerService', ['$http', function ($http) {
  var _maintanance = {
    GetLanguages: function () {
      return $http.get('/Data/LangFiles.json').success(function (response) {
        return response;
      });
    },
    GetTranslations: function (langCode) {
      var rnd = Math.floor(Math.random() * 100000) + 1;
      return $http.get('/Data/Lang/' + langCode + '.json?v=' + rnd).success(function (response) {
        return response;
      });
    },
    SaveTranslations: function (lang, translations) {

      return $http({
        method: 'POST',
        dataType: 'json',
        headers: { "Content-Type": "application/json" },
        data: { "lang": lang, "json": translations },
        url: '/Handlers/JSONManager.asmx/SaveTranslations'
      }).success(function (response) {
        return response;
      });
    }
  };
  return _maintanance;
}]);

