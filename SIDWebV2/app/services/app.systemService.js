﻿appModule.factory('$systemService', ['$http', function ($http) {

  var _caller = new ServiceCaller($http, null, 'SystemService');
  var _systemService = {
    Parameters: null
  };

  //Private Methods

  function getSystemParameters() {
    return _caller.POST({}, 'GetSystemParameters').then(function (result) {
      _systemService.Parameters = result.data.d.Data;
    });
  };

  getSystemParameters();

  return _systemService;

}]);