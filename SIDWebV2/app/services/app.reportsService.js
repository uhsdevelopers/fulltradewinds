﻿appModule.factory('$reportsService', ['$http', function ($http) {

  var _caller = new ServiceCaller($http, null, 'reportsService');
  var _reportsService = {};

  _reportsService.DailyFigures = null;
  _reportsService.GroupedOpenBets = [];
  _reportsService.TransactionList = [];
  _reportsService.CasinoTransactionList = [];
  _reportsService.CashTransactionList = [];

  function transaction(transaction) {
    return {
      Amount: transaction.Amount,
      AmountLost: transaction.AmountLost,
      AmountWagered: transaction.AmountWagered,
      AmountWon: transaction.AmountWon,
      CurrentBalance: transaction.CurrentBalance,
      Comments: transaction.Comments,
      Description: transaction.Description,
      DocumentNumber: transaction.DocumentNumber,
      EnteredBy: transaction.EnteredBy,
      FreePlayFlag: transaction.FreePlayFlag,
      HoldAmount: transaction.HoldAmount,
      ItemWagerType: transaction.ItemWagerType,
      Items: transaction.Items,
      Outcome: transaction.Outcome,
      PeriodDescription: transaction.PeriodDescription,
      ShortDesc: transaction.ShortDesc,
      SportSubType: transaction.SportSubType,
      SportType: transaction.SportType,
      Team1ID: transaction.Team1ID,
      Team1Score: transaction.Team1Score,
      Team2ID: transaction.Team2ID,
      Team2Score: transaction.Team2Score,
      TranCode: transaction.TranCode,
      TranDateTimeString: transaction.TranDateTimeString,
      AcceptedDateTimeString: transaction.AcceptedDateTimeString,
      TranType: transaction.TranType,
      WagerNumber: (transaction.WagerNumber ? '-' + transaction.WagerNumber : ''),
      WagerType: transaction.WagerType,
      WinnerID: transaction.WinnerID
    };
  };

  function newWagerItem(wager) {
    return {
      Comments: (wager.Comments == null || wager.Comments == "" ? null : wager.Comments),
      ContinueOnPushFlag: wager.ContinueOnPushFlag,
      Description: wager.Description,
      FreePlayFlag: wager.FreePlayFlag,
      SportType: wager.SportType,
      PeriodDescription: wager.PeriodDescription,
      Team1Score: wager.Team1Score,
      Team2Score: wager.Team2Score,
      Team1ID: wager.Team1ID,
      Team2ID: wager.Team2ID,
      Outcome: (wager.Outcome == "W" ? 'WON' : wager.Outcome == "L" ? 'LOST' : wager.Outcome == "X" ? 'PUSH' : ''),
      Result: (wager.Outcome == "W" || wager.TranType == "W" ? 'WON' : wager.Outcome == "L" || wager.TranType == "L" ? 'LOST' : wager.Outcome == "X" ? 'PUSH' : ''),
      WagerType: wager.WagerType,
      WinnerID: wager.WinnerID,
      EventDateTime: wager.EventDateTime
    };
  };

  function createManualItems(fullDescription) {
    var arr = fullDescription.split("|");
    var items = [];
    for (var i = 0; i < arr.length; i++) {
      items.push({ Description: arr[i] });
    }
    return items;
  }

  _reportsService.GetCustomerDailyFigures = function (customerId, weekOffset, currencyCode) {
    return _caller.POST({ 'customerId': customerId, 'weekOffset': weekOffset, 'currencyCode': currencyCode }, 'GetCustomerDailyFigures').then(function (result) {
      _reportsService.DailyFigures = result.data.d.Data;
      if (_reportsService.DailyFigures.ZeroBalance != null)
        _reportsService.DailyFigures.ZeroBalance = CommonFunctions.RoundNumber(_reportsService.DailyFigures.ZeroBalance);
      for (var i = 0; i < _reportsService.DailyFigures.ValuesPerDay.length; i++) {
        _reportsService.DailyFigures.ValuesPerDay[i].ThisDate = CommonFunctions.FormatDateTime(_reportsService.DailyFigures.ValuesPerDay[i].ThisDate, 1, 2); // timezone is temp, fix *

        if (_reportsService.DailyFigures.ValuesPerDay[i].CashInOut != null)
          _reportsService.DailyFigures.ValuesPerDay[i].CashInOut = CommonFunctions.RoundNumber(_reportsService.DailyFigures.ValuesPerDay[i].CashInOut);

        if (_reportsService.DailyFigures.ValuesPerDay[i].WinLoss != null)
          _reportsService.DailyFigures.ValuesPerDay[i].WinLoss = CommonFunctions.RoundNumber(_reportsService.DailyFigures.ValuesPerDay[i].WinLoss);

        if (_reportsService.DailyFigures.ValuesPerDay[i].CasinoWinLoss != null)
          _reportsService.DailyFigures.ValuesPerDay[i].CasinoWinLoss = CommonFunctions.RoundNumber(_reportsService.DailyFigures.ValuesPerDay[i].CasinoWinLoss);
      }
    });
    //actualWeek = weekOffset;
  };

  _reportsService.GetCustomerPendingBets = function (customerId) {
    var groupedItems = null;
    var holdTicketNumber = null;
    var holdWagerNumber = null;
    return _caller.POST({ 'customerId': customerId }, 'GetCustomerPendingBets').then(function (result) {

      var allOpenBets = result.data.d.Data;
      var groupedOpenBets = new Array();

      if (allOpenBets != null && allOpenBets.length > 0) {

        groupedOpenBets = new Array();
        groupedItems = new Array();
        holdTicketNumber = allOpenBets[0].TicketNumber;
        holdWagerNumber = allOpenBets[0].WagerNumber;
        for (var i = 0; i < allOpenBets.length; i++) {
          if (holdTicketNumber != allOpenBets[i].TicketNumber || holdWagerNumber != allOpenBets[i].WagerNumber) {
            groupedOpenBets.push(allOpenBets[i - 1]);
            if (allOpenBets[i - 1].WagerType == 'A') {
              groupedOpenBets[groupedOpenBets.length - 1].Items = createManualItems(allOpenBets[i - 1].Description);
            }
            else {
              if (groupedOpenBets.length > 0 && groupedItems != null && groupedItems.length > 0)
                groupedOpenBets[groupedOpenBets.length - 1].Items = groupedItems;
            }
            groupedItems = new Array();
            groupedItems.push(newWagerItem(allOpenBets[i]));
          } else {
            groupedItems.push(newWagerItem(allOpenBets[i]));
          }
          holdTicketNumber = allOpenBets[i].TicketNumber;
          holdWagerNumber = allOpenBets[i].WagerNumber;

          if (i == allOpenBets.length - 1) {
            groupedOpenBets.push(allOpenBets[i]);
            groupedOpenBets[groupedOpenBets.length - 1].Items = groupedItems;
          }
        }
      }

      _reportsService.GroupedOpenBets = groupedOpenBets;
    });
  };

  _reportsService.GetCasinoTransactionsByDate = function (customerId, date) {
    return _caller.POST({ 'customerId': customerId, 'date': date }, 'GetCasinoTransactionsByDate').then(function (result) {
      var casinoTransactions = result.data.d.Data;
      if (casinoTransactions != null && casinoTransactions.length > 0) {
        _reportsService.CasinoTransactionList = [];
        for (var i = 0; i < casinoTransactions.length; i++) {
          _reportsService.CasinoTransactionList.push(transaction(casinoTransactions[i]));
        }
      } else {
        _reportsService.CasinoTransactionList = [];
      }
    });
  };

  _reportsService.GetCashTransactionsByDate = function (customerId, date) {
    return _caller.POST({ 'customerId': customerId, 'date': date }, 'GetCashTransactionsByDate').then(function (result) {
      var cashTransactions = result.data.d.Data;
      if (cashTransactions != null && cashTransactions.length > 0) {
        _reportsService.CashTransactionList = [];
        for (var i = 0; i < cashTransactions.length; i++) {
          _reportsService.CashTransactionList.push(transaction(cashTransactions[i]));
        }
      } else {
        _reportsService.CashTransactionList = null;
      }
    });
  };

  _reportsService.GetCustomerTransactionListByDays = function (customerId, numDays) {

    var groupedItems = null;
    var holdDocumentNumber = null;
    var holdWagerNumber = null;
    return _caller.POST({ 'customerId': customerId, 'numDays': numDays }, 'GetCustomerTransactionListByDays').then(function (result) {

      var allTransactions = result.data.d.Data;
      var groupedTransactions = new Array();
      var accumTran = 0;

      if (allTransactions != null && allTransactions.length > 0) {

        groupedTransactions = new Array();
        groupedItems = new Array();
        holdDocumentNumber = allTransactions[allTransactions.length - 1].DocumentNumber;
        holdWagerNumber = allTransactions[allTransactions.length - 1].WagerNumber;

        for (var i = allTransactions.length - 1 ; i >= 0; i--) {
          if (holdDocumentNumber != allTransactions[i].DocumentNumber || holdWagerNumber != allTransactions[i].WagerNumber) {
            groupedTransactions.push(allTransactions[i + 1]);
            if (groupedTransactions.length > 0 && groupedItems != null && groupedItems.length > 0)
              groupedTransactions[groupedTransactions.length - 1].Items = groupedItems;
            groupedItems = new Array();
            groupedItems.push(newWagerItem(allTransactions[i]));
            if (i < allTransactions.length - 1) groupedTransactions[groupedTransactions.length - 1].CurrentBalance = groupedTransactions[groupedTransactions.length - 1].CurrentBalance + accumTran;
            accumTran += groupedTransactions[groupedTransactions.length - 1].TranCode == "C" ? groupedTransactions[groupedTransactions.length - 1].Amount * -1 : groupedTransactions[groupedTransactions.length - 1].Amount;


          } else {
            groupedItems.push(newWagerItem(allTransactions[i]));
          }
          holdDocumentNumber = allTransactions[i].DocumentNumber;
          holdWagerNumber = allTransactions[i].WagerNumber;
          if (i == 0) {
            groupedTransactions.push(allTransactions[i]);
            groupedTransactions[groupedTransactions.length - 1].Items = groupedItems;
            groupedTransactions[groupedTransactions.length - 1].CurrentBalance = groupedTransactions[groupedTransactions.length - 1].CurrentBalance + accumTran;
          }
        }
      }
      _reportsService.TransactionList = groupedTransactions;
    });
  };

  _reportsService.GetCustomerTransactionListByDateRange = function (customerId, initDate, finalDate) {

    var groupedItems = null;
    var holdDocumentNumber = null;
    var holdWagerNumber = null;
    return _caller.POST({ 'customerId': customerId, 'initDate': initDate, 'finalDate': finalDate }, 'GetCustomerTransactionListByDateRange').then(function (result) {

      var allTransactions = result.data.d.Data;
      var groupedTransactions = new Array();
      var accumTran = 0;

      if (allTransactions != null && allTransactions.length > 0) {

        groupedTransactions = new Array();
        groupedItems = new Array();
        holdDocumentNumber = allTransactions[allTransactions.length - 1].DocumentNumber;
        holdWagerNumber = allTransactions[allTransactions.length - 1].WagerNumber;

        for (var i = allTransactions.length - 1 ; i >= 0; i--) {
          if (holdDocumentNumber != allTransactions[i].DocumentNumber || holdWagerNumber != allTransactions[i].WagerNumber) {
            groupedTransactions.push(allTransactions[i + 1]);
            if (groupedTransactions.length > 0 && groupedItems != null && groupedItems.length > 0)
              groupedTransactions[groupedTransactions.length - 1].Items = groupedItems;
            groupedItems = new Array();
            groupedItems.push(newWagerItem(allTransactions[i]));
            if (i < allTransactions.length - 1) groupedTransactions[groupedTransactions.length - 1].CurrentBalance = groupedTransactions[groupedTransactions.length - 1].CurrentBalance + accumTran;
            accumTran += groupedTransactions[groupedTransactions.length - 1].TranCode == "C" ? groupedTransactions[groupedTransactions.length - 1].Amount * -1 : groupedTransactions[groupedTransactions.length - 1].Amount;


          } else {
            groupedItems.push(newWagerItem(allTransactions[i]));
          }
          holdDocumentNumber = allTransactions[i].DocumentNumber;
          holdWagerNumber = allTransactions[i].WagerNumber;
          if (i == 0) {
            groupedTransactions.push(allTransactions[i]);
            groupedTransactions[groupedTransactions.length - 1].Items = groupedItems;
            groupedTransactions[groupedTransactions.length - 1].CurrentBalance = groupedTransactions[groupedTransactions.length - 1].CurrentBalance + accumTran;
          }
        }
      }
      _reportsService.TransactionList = groupedTransactions;
    });
  };

  _reportsService.GetCustomerTransactionListByDate = function (customerId, date) {

    var groupedItems = null;
    var holdDocumentNumber = null;
    var holdWagerNumber = null;
    return _caller.POST({ 'customerId': customerId, 'date': date }, 'GetCustomerTransactionListByDate').then(function (result) {

      var allTransactions = result.data.d.Data;
      var groupedTransactions = new Array();

      if (allTransactions != null && allTransactions.length > 0) {

        groupedTransactions = new Array();
        groupedItems = new Array();
        holdDocumentNumber = allTransactions[0].DocumentNumber;
        holdWagerNumber = allTransactions[0].WagerNumber;

        for (var i = 0; i < allTransactions.length; i++) {
          if (holdDocumentNumber != allTransactions[i].DocumentNumber || holdWagerNumber != allTransactions[i].WagerNumber) {
            groupedTransactions.push(transaction(allTransactions[i - 1]));
            if (groupedTransactions.length > 0 && groupedItems != null && groupedItems.length > 0)
              groupedTransactions[groupedTransactions.length - 1].Items = groupedItems;
            groupedItems = new Array();
            groupedItems.push(newWagerItem(allTransactions[i]));
          } else {
            groupedItems.push(newWagerItem(allTransactions[i]));
          }
          holdDocumentNumber = allTransactions[i].DocumentNumber;
          holdWagerNumber = allTransactions[i].WagerNumber;
          if (i == allTransactions.length - 1) {
            groupedTransactions.push(transaction(allTransactions[i]));
            groupedTransactions[groupedTransactions.length - 1].Items = groupedItems;
          }
        }
      }
      _reportsService.TransactionList = groupedTransactions;
    });
  };

  _reportsService.GetDailyWagers = function (customerId, dailyFigureDate, store) {
    return _caller.POST({ 'customerId': customerId, 'dailyFigureDate': dailyFigureDate, 'store': store }, 'GetDailyWagers').then(function (result) {
      for (var i = 0; i < _reportsService.DailyFigures.ValuesPerDay.length; i++) {
        if (_reportsService.DailyFigures.ValuesPerDay[i].ThisDate == dailyFigureDate) {
          _reportsService.DailyFigures.ValuesPerDay[i].Wagers = result.data.d.Data;

          angular.forEach(_reportsService.DailyFigures.ValuesPerDay[i].Wagers, function (wager) {
            if (wager.AmountLost != null)
              wager.AmountLost /= 100;
            if (wager.AmountWon != null)
              wager.AmountWon /= 100;
            if (wager.AcceptedDateTime != null)
              wager.AcceptedDateTime = CommonFunctions.FormatDateTime(wager.AcceptedDateTime, 1) + " " + CommonFunctions.FormatDateTime(wager.AcceptedDateTime, 2);
            if (wager.AmountWagered != null)
              wager.AmountWagered /= 100;
            if (wager.GradeDateTime != null)
              wager.GradeDateTime = CommonFunctions.FormatDateTime(wager.GradeDateTime, 1) + " " + CommonFunctions.FormatDateTime(wager.GradeDateTime, 2);
            if (wager.ToWinAmount != null)
              wager.ToWinAmount /= 100;

            angular.forEach(wager.Details, function (detail) {
              if (detail.AmountWagered != null)
                detail.AmountWagered /= 100;
              if (detail.ContestDateTime != null)
                detail.ContestDateTime = CommonFunctions.FormatDateTime(wager.ContestDateTime, 1) + " " + CommonFunctions.FormatDateTime(wager.ContestDateTime, 2);
              if (detail.GameDateTime != null)
                detail.GameDateTime = CommonFunctions.FormatDateTime(wager.GameDateTime, 1) + " " + CommonFunctions.FormatDateTime(wager.GameDateTime, 2);
              if (detail.GradeMoney != null)
                detail.GradeMoney /= 100;
              if (detail.GradeToWinAmount != null)
                detail.GradeToWinAmount /= 100;
              if (detail.ToWinAmount != null)
                detail.ToWinAmount /= 100;
            });
            wager.GradeDateTime = CommonFunctions.FormatDateTime(wager.GradeDateTime, 1);
          });
          _reportsService.DdailyFigureDate = dailyFigureDate;
          break;
        }
      }
    });
  };

  return _reportsService;

}]);