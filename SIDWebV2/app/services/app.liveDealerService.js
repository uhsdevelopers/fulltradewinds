﻿appModule.factory('$liveDealerService', ['$http', function ($http) {
  var _caller = new ServiceCaller($http, null, 'LiveDealerService');

  var _liveDealerService = {};

  _liveDealerService.RetrieveSecurityToken = function () {
    return _caller.POST({}, 'RetrieveSecurityToken').then(function (result) {
      return result;
    });
  };

  return _liveDealerService;

}
]);