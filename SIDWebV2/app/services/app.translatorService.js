﻿function TranslatorApi($http) {

  var _caller = new ServiceCaller($http, null, 'translatorService');

  var _dataFolder = '/data/';
  var _translatorService = {
    DefaultLanguage: SETTINGS.DefaultLanguage,
    LearningMode: SETTINGS.TextLearningModeOn,
    _currentLang: SETTINGS.DefaultLanguage,
    TranslationFileSufix: SETTINGS.TranslationFileSufix,
    _translations: []
  };

  //Private Methods

  function addTextToTranslate (text) {
    _translatorService._translations[text] = "[NT]";
  };

  function getTranslation(text) {
    var txt = _translatorService._translations[text];
    if (txt || txt == "[NT]") return txt;
    txt = _translatorService._translations[text.toUpperCase()];
    return txt;
  };

  function loadLanguage() {
    _caller.GET(_dataFolder + 'lang/' + _translatorService._currentLang + _translatorService.TranslationFileSufix + '.json').then(function (response) {
      _translatorService._translations = response.data;
    });
  };

  function makeTextSafeToTranslate(text) {
    var translatedName;
    text = text.replace(/ /g, "_").replace(".5", "H").replace(/½/g, "H").replace(/-/g, "_").toUpperCase();
    var firstChar = text.charAt(0);
    if (/^([0-9])$/.test(firstChar)) {
      translatedName = text.replace(firstChar, CommonFunctions.SubstituteDigitByLetter(firstChar));
    } else {
      translatedName = text;
    }
    return translatedName;
  };

  function saveTranslations() {
    _caller.POST({ 'lang': _translatorService._currentLang + _translatorService.TranslationFileSufix, 'json': JSON.stringify(_translatorService._translations) }, "SaveTranslations").then();
  };

  _translatorService.ChangeLanguage = function (newLang) {
    if (_translatorService._currentLang == newLang) return;
    _translatorService._currentLang = newLang;
    loadLanguage();
  };

  _translatorService.Translate = function (text) {
    if (!text || !_translatorService._translations) return text;
    var safeText = makeTextSafeToTranslate(text);
    var txt = getTranslation(safeText);
    if (txt && txt != "[NT]") return txt;
    else {
      if (_translatorService.LearningMode && txt == "[NT]") {
        //translatorService._translations[safeText] = "[NT]";
        text = "[" + text + "]";
      }
      else if (_translatorService.LearningMode) {
        addTextToTranslate(safeText);
        text = "[" + text + "*NEW*]";

      }
      return text.replace('_', ' ');
    }
  };

  _translatorService.SaveLearnedTexts = function () {
    saveTranslations();
  };

  loadLanguage();

  return _translatorService;

}

if (typeof appModule != "undefined")
  appModule.factory('$translatorService', ['$http', '$rootScope', TranslatorApi]);
else if (typeof loginModule != "undefined")
  loginModule.factory('$translatorService', ['$http', '$rootScope', TranslatorApi || {}]);