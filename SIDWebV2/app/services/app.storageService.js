﻿appModule.factory('$webStorageService', [function () {

  var _webStorageService = {
    local: {},
    session: {},
    active: true
  };

  _webStorageService.IsSupported = function () {
    return typeof (Storage) != "undefined";
  };

  _webStorageService.local.setItem = function (item, value) {
    if (!_webStorageService.active) return;
    localStorage[item] = value;
  };

  _webStorageService.local.getItem = function (item) {
    if (!_webStorageService.active) return null;
    return localStorage[item];
  };

  _webStorageService.local.setObject = function (item, object) {
    if (!_webStorageService.active) return;
    localStorage[item] = JSON.stringify(object);
  };

  _webStorageService.local.getObject = function (item) {
    if (!_webStorageService.active) return null;
    var strItem = localStorage[item];
    if (strItem && strItem.length > 0)
      return JSON.parse(strItem);
    else return null;
  };

  _webStorageService.local.removeObject = function (item) {
    if (!_webStorageService.active) return;
    localStorage.removeItem(item);
  };

  _webStorageService.session.removeItem = function (item) {
    if (!_webStorageService.active) return;
    sessionStorage.removeItem(item);
  };

  _webStorageService.session.setItem = function (item, object) {
    if (!_webStorageService.active) return;
    sessionStorage[item] = JSON.stringify(object);
  };

  _webStorageService.session.getItem = function (item) {
    if (!_webStorageService.active) return null;
    var strItem = sessionStorage[item];
    if (strItem && strItem.length > 0)
      return JSON.parse(strItem);
    //return sessionStorage[item];
    return [];
  };

  return _webStorageService;

}]);