﻿var betOfferingCtrl = appModule.controller(appModule.Controllers.betOffering, ['$scope', '$rootScope', '$ticketService', '$customerService', '$wagerTypesService', '$sportsAndContestsService', '$translatorService', function ($scope, $rootScope, $ticketService, $customerService, $wagerTypesService, $sportsAndContestsService, $translatorService) {
  $scope.HidePeriods = [];
  betOfferingCtrl.scope = $scope;
  $scope.ContactInfoTemplate = SETTINGS.ContactInfoTemplate;
  $scope.ShowContactInfo = SETTINGS.ShowContactInfo;
  $scope.TicketServiceView = $ticketService;

  //Private Methods

  function removeLine (gameLine, subWagerType, teamPos) {
    switch (subWagerType) {
      case 'S':
        gameLine.SpreadAdj1 = null;
        gameLine.SpreadAdj2 = null;
        break;
      case 'M':
        gameLine.MoneyLine1 = null;
        gameLine.MoneyLine2 = null;
        break;
      case 'L':
        gameLine.TtlPtsAdj1 = null;
        gameLine.TtlPtsAdj2 = null;
        break;
      case 'E':
        if (teamPos == 1) {
          gameLine.Team1TtlPtsAdj1 = null;
          gameLine.Team1TtlPtsAdj2 = null;
        } else {
          gameLine.Team2TtlPtsAdj1 = null;
          gameLine.Team2TtlPtsAdj2 = null;
        }
        break;
    }
  };

  function lineIsValid(gameLine, subWagerType, teamPos) {
    switch (subWagerType) {
      case 'S':
        return gameLine.SpreadAdj1 != null && gameLine.SpreadAdj2 != null && gameLine.SpreadLineStatus != 'H';
      case 'M':
        return gameLine.MoneyLine1 != null && gameLine.MoneyLine2 != null && gameLine.MoneyLineStatus != 'H';
      case 'L':
        return gameLine.TtlPtsAdj1 != null && gameLine.TtlPtsAdj2 != null && gameLine.TotalsStatus != 'H';
      case 'E':
        if (teamPos == 1) return gameLine.Team1TtlPtsAdj1 != null && gameLine.Team1TtlPtsAdj2 != null && gameLine.TeamTotalsLineStatus != 'H';
        else return gameLine.Team2TtlPtsAdj1 != null && gameLine.Team2TtlPtsAdj2 != null && gameLine.TeamTotalsLineStatus != 'H';
    }
    return false;
  };

  function revertGameLineSelection (gameItem, subWagerType, teamPos) {
    var added = false;
    switch (subWagerType) {
      case "S":
        if (teamPos == 1) added = gameItem.Spread1Selected = !gameItem.Spread1Selected;
        else added = gameItem.Spread2Selected = !gameItem.Spread2Selected;
        break;
      case "M":
        if (teamPos == 1) added = gameItem.MoneyLine1Selected = !gameItem.MoneyLine1Selected;
        else if (teamPos == 2) added = gameItem.MoneyLine2Selected = !gameItem.MoneyLine2Selected;
        else added = gameItem.MoneyLine3Selected = !gameItem.MoneyLine3Selected;
        break;
      case "L":
        if (teamPos == 1) added = gameItem.TotalPoints1Selected = !gameItem.TotalPoints1Selected;
        else added = gameItem.TotalPoints2Selected = !gameItem.TotalPoints2Selected;
        break;
      case "E":
        if (teamPos == 1) added = gameItem.Team1TtlPtsAdj1Selected = !gameItem.Team1TtlPtsAdj1Selected;
        else if (teamPos == 2) added = gameItem.Team2TtlPtsAdj1Selected = !gameItem.Team2TtlPtsAdj1Selected;
        else if (teamPos == 3) added = gameItem.Team1TtlPtsAdj2Selected = !gameItem.Team1TtlPtsAdj2Selected;
        else added = gameItem.Team2TtlPtsAdj2Selected = !gameItem.Team2TtlPtsAdj2Selected;
        break;
    }
    added = (added && !$ticketService.Ticket.Posted() && $ticketService.IsOpenPlays() ? $ticketService.IsOpenFull() : added);
    setTimeout(function () {
      if (!added) {
        var el = $('#' + subWagerType + teamPos + '_' + gameItem.GameNum + '_' + gameItem.PeriodNumber);
        el.removeClass('active');
      }
    }, 200);
    return added;
  };

  function revertContestantLineSelection (contestantLine) {
    contestantLine.Selected = contestantLine.Selected = !contestantLine.Selected;
    return contestantLine.Selected;
  };

  //Public Events

  $scope.GameLineAction = function (gameItem, subWagerType, teamPos) {
    $ticketService.ContinuePressed = false;
    $ticketService.IsMoverReady = false;
    if (!$ticketService.TicketProcessed || $scope.Customer.IsDemo) return;
    if ($scope.WageringDisabled) {
      UI.Alert($translatorService.Translate("Your wagering has been suspended please contact your representative!"));
      return;
    }
    var wagerType = $wagerTypesService.Selected;
    switch (subWagerType) {
      case "M":
        gameItem.MoneyLineChanged = false;
        break;
      case "S":
        gameItem.SpreadChanged = false;
        break;
      case "L":
        gameItem.TotalPointsChanged = false;
        break;
      case "E":
        gameItem.TeamTotalChanged = false;
        break;
    }
    var added = revertGameLineSelection(gameItem, subWagerType, teamPos);
    if (added) $ticketService.AddGameWagerItem(gameItem, subWagerType, teamPos, wagerType, false);
    else $ticketService.RemoveGameWagerItem(gameItem, subWagerType, teamPos, false, null);
  };

  $scope.ContestantLineAction = function (contest, contestantLine) {
    if (!$ticketService.TicketProcessed) return;
    if ($scope.WageringDisabled === true) {
      UI.Alert($translatorService.Translate("Your wagering has been suspended please contact your representative!"));
      return;
    }
    contestantLine.MoneyLineChanged = false;
    var added = revertContestantLineSelection(contestantLine);
    if (added) $ticketService.AddContestWagerItem(contest, contestantLine).then();
    else $ticketService.RemoveContestWagerItem(contestantLine).then();
  };

  $scope.ChangePeriod = function (league, period) {
    league.TeamTotalSelected = false;
    league.ActivePeriod = period;
    setTimeout(function () {
      $('[data-toggle="tooltip"]').tooltip();
    }, 500);
    $customerService.CustomerActionLogger("Period Changed ", league.SportType + " - " + league.SportSubType + " -> " + period.PeriodDescription);
  };

  $scope.ToggleTeamTotals = function (league) {
    league.TeamTotalSelected = !league.TeamTotalSelected;
    $customerService.CustomerActionLogger("Team Totals Changed ", " Visible -> " + league.TeamTotalSelected);
  };

  $scope.CollapsePeriods = function (league) {
    var el = $('#subSport_' + league.SportSubTypeId);
    if (el.hasClass('collapsing')) return;
    if (!el.hasClass('in')) {
      el.collapse("show");
      $('#periods_' + league.SportSubTypeId).show();
    } else {
      el.collapse("hide");
      $('#periods_' + league.SportSubTypeId).hide();
    }
    //}
  };

  //Rendering Methods

  $scope.GetGamesTitle = function (gameLine) {
    return gameLine.Team1ID.replace('*', "") + (gameLine.Team2ID != "" && gameLine.Team2ID != "*" ? " - " + gameLine.Team2ID.replace('*', "") : "");
  };

  $scope.IsGameLineDisabled = function (gameLine, subWagerType, teamPos) {
    if (subWagerType != "L" && $wagerTypesService.IsTeaser() && gameLine.SportType == "Baseball") { //Only totals should be available for MLB Teasers
      removeLine();
    }

    if ((gameLine.SportType == "Football" || gameLine.SportType == "Basketball") && subWagerType == "M" && !isNaN(gameLine.SpreadAdj1) && !isNaN(gameLine.SpreadAdj2)) {
      var restriction = $customerService.GetRestriction(gameLine.SportType == "Football" ?
          $customerService.RestrictionCodes.FootballMonelyDisablePoints :
          $customerService.RestrictionCodes.BasketballMonelyDisablePoints);
      var maxSpreadPoints = parseInt($customerService.GetRestrictionParamValue(restriction, 'MaxSpreadPoints'));
      if (Math.abs(gameLine.Spread1) >= maxSpreadPoints || Math.abs(gameLine.Spread2) >= maxSpreadPoints) {
        removeLine(gameLine, "M");
      }
    }
    var lineInvalid = !lineIsValid(gameLine, subWagerType, teamPos);
    if (!$ticketService.Ticket.Posted() && !lineInvalid && $ticketService.IsOpenPlays()) {
      $ticketService.Ticket.OpenWagerItems.forEach(function (e) {
        if ((e.Team2RotNum == gameLine.Team2RotNum || e.Team1RotNum == gameLine.Team1RotNum) && e.WagerType == subWagerType) {
          lineInvalid = true;
          return;
        }
      });
    }
    return lineInvalid;
  };

  $scope.GetContestDescription = function (contest) {
    return ContestFunctions.GetContestDescription(contest);
  };

  $scope.GetContestantName = function (contestantLine, league) {
    return ContestFunctions.GetContestantName(contestantLine, league);
  };

  $scope.GetContestantLine = function (contestant) {
    return ContestFunctions.GetContestantLine(contestant, $customerService.Info.PriceType);
  };

  // Rendering Filters

  $scope.Filters = $scope.Filters || {};

  $scope.Filters.Game = function (league) {
    return function (gameLine) {
      if (!league ||
          !gameLine ||
          league.ActivePeriod &&
          (gameLine.PeriodNumber != league.ActivePeriod.PeriodNumber && league.ActivePeriod.PeriodNumber != null) ||
          gameLine.disabled === true) return false;

      if (league.TeamTotalSelected) {
        if (gameLine.IsTitle) return true;
        else {
          if ((gameLine.Team1TtlPtsAdj1 != null &&
              gameLine.Team1TtlPtsAdj2 != null) ||
              (gameLine.Team2TtlPtsAdj1 != null &&
              gameLine.Team2TtlPtsAdj2 != null)) return true;
          else return false;
        }
      } else {
        if ($rootScope.SearchFilter.value && $rootScope.SearchFilter.value != "") {
          if (gameLine.Team1ID.toLowerCase().indexOf($rootScope.SearchFilter.value.toLowerCase()) >= 0 ||
              gameLine.Team2ID.toLowerCase().indexOf($rootScope.SearchFilter.value.toLowerCase()) >= 0 ||
              gameLine.Team1RotNum.toString().indexOf($rootScope.SearchFilter.value) >= 0 ||
              gameLine.Team2RotNum.toString().indexOf($rootScope.SearchFilter.value) >= 0) return true;
          else return false;
        } else return true;
      }
    };
  };

  $scope.Filters.Contest = function (league) {
    return function (contestLine) {
      if (!league ||
          !contestLine ||
          contestLine.Disabled === true) return false;
      if ($rootScope.SearchFilter.value && $rootScope.SearchFilter.value != "") {
        if (contestLine.ContestantName.toLowerCase().indexOf($rootScope.SearchFilter.value.toLowerCase()) >= 0 ||
            contestLine.RotNum.toString().toLowerCase().indexOf($rootScope.SearchFilter.value.toLowerCase()) >= 0) return true;
        else return false;
      } else return true;

    };
  };

  $scope.IsSpotlight = function (isSpotlight) {
    return typeof isSpotlight !== 'undefined' ? isSpotlight : false;
  };

  $scope.CircledInfoVisible = function () {
    for (var i = 0; i < $sportsAndContestsService.Selections.length; i++) {
      if ($sportsAndContestsService.Selections[i].Type == "C") continue;
      if (typeof $sportsAndContestsService.Selections[i].Lines === "undefined") return true;
      for (var j = 0; j < $sportsAndContestsService.Selections[i].Lines.length; j++)
        if ($sportsAndContestsService.Selections[i].Lines[j].CircledInfoVisible && $sportsAndContestsService.Selections[i].Lines[j].Status == "I")
          return true;
    };
    return false;
  };

  $scope.GetSportsContainerClass = function () {
    var cnt = ($scope.LiveDealer || $scope.VirtualCasino ? 1 : 0) + ($scope.LiveBetting ? 1 : 0) + ($scope.HorsesActive ? 1 : 0);
    switch (cnt) {
      case 0:
        return 'col-md-12';
      case 1:
        return 'col-md-9';
      case 2:
        return 'col-md-6';
      default:
        return 'col-md-3';
    }
  };

  $scope.GetSportsImageClass = function () {
    var cnt = ($scope.LiveDealer || $scope.VirtualCasino ? 1 : 0) + ($scope.LiveBetting ? 1 : 0) + ($scope.HorsesActive ? 1 : 0);
    return cnt < 2 ? 'bo_sports_solo' : 'bo_sports';
  };

  $scope.GetCustomerCurrencyCode = function () {
    return $customerService.Info ? $customerService.Info.CurrencyCode : "";
  };

  $scope.AllowedMessages = function (messagesList) {
    if (typeof messagesList === "undefined" || messagesList == null) return [];
    var allowed = [];
    if ($scope.bannedMessages != null) {
      messagesList.forEach(function (e) {
        var found = false;
        $scope.bannedMessages.forEach(function (b) {
          if (alert.Id == b) found = true;
        });
        if (!found) allowed.push(e);
      });
    } else return messagesList;
    return [];
  };

}]);
