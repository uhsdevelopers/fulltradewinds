﻿appModule.factory('$bettingService', ['$http', function ($http) {
    var caller = new ServiceCaller($http, 'bettingService'); //Should be call BettingServices
    return {
        AddWagerContestItemToWi: function (contestant, riskAmt, toWinAmt) {
            return caller.POST({
                'contestNumber': contestant.ContestNum,
                'contestantNumber': contestant.ContestantNum,
                'riskAmt': riskAmt,
                'toWinAmt': toWinAmt
            }, 'AddWagerContestItem', null, true);
        },

        AddWagerItemToWi: function (gameItem, wagertype, teamPos, riskAmt, toWinAmt) {
            return caller.POST({
                'wagerTypeCode': wagertype,
                'gameNumber': gameItem.GameNum,
                'periodNumber': gameItem.PeriodNumber,
                'chosenTeamIdx': teamPos,
                'riskAmt': riskAmt,
                'toWinAmt': toWinAmt
            }, 'AddWagerItem', null, true);
        },

        GetContests: function (contestType, contestType2) {
            return caller.POST({ 'contestType': contestType, 'contestType2': contestType2 }, 'LoadContests');
        },

        GetGamesBySport: function (sportType, sportSubType, wagerType, hoursAdjustment) {
            return caller.POST({ 'sportType': sportType, 'sportSubType': sportSubType, 'wagerType': wagerType, 'hoursAdjustment': hoursAdjustment }, 'LoadGameLines');
        },

        GetIntersectingPeriodsInfo: function (sportType, sportSubType, periodNum1, periodNum2) {
            return caller.POST({
                'sportType': sportType, 'sportSubType': sportSubType, 'periodNum1': periodNum1, 'periodNum2': periodNum2
            });
        },

        GetMaximumWagerLimit: function (wagerType, sportType, sportSubType, periodNumber, circleLimit, contestantLimit) {
            return caller.POST({
                'wagerType': wagerType, 'sportType': sportType, 'sportSubType': sportSubType, 'periodNumber': periodNumber, 'circleLimit': circleLimit, 'contestantLimit': contestantLimit
            }, 'GetMaximumWagerLimit', null, true);
        },

        LoadTeaserGames: function (teaserName, periodWagerCutoff, hoursAdjustment) {
            return caller.POST({ 'teaserName': teaserName, 'PeriodWagerCutoff': periodWagerCutoff, 'HoursAdjustment': hoursAdjustment });
        },

        RemoveGames: function (sportSubTypeId, removeSelectedSport) {
            return caller.POST({ 'sportSubTypeId': sportSubTypeId, 'removeSelectedSport': removeSelectedSport }, 'RemoveGames');
        },

        RemoveWagerContestItemFromWi: function (contestNumber, contestantNumber) {
            return caller.POST({
                'contestNumber': contestNumber,
                'contestantNumber': contestantNumber
            }, 'RemoveWagerContestItem', null, true);
        },

        RemoveWagerItemFromWi: function (gameNum, periodNumber, wagertype, teamPos) {
            return caller.POST({
                'gameNum': gameNum,
                'periodNumber': periodNumber,
                'wagertype': wagertype,
                'teamPos': teamPos
            }, 'RemoveWagerItem', null, true);
        },

        UpdateContestantLine: function (contestNum, contestantNum, custProfile) {
            return caller.POST({
                'contestNum': contestNum,
                'contestantNum': contestantNum,
                'custProfile': custProfile
            }, 'UpdateContestantLine', null, true);
        },

        UpdateGameLine: function (gameNum, periodNumber, sportType, sportSubType, custProfile, wagerType) {
            return caller.POST({
                'gameNum': gameNum,
                'periodNumber': periodNumber,
                'sportType': sportType,
                'sportSubType': sportSubType,
                'custProfile': custProfile,
                'wagerType': wagerType
            }, 'UpdateGameLine', null, true);
        }
    };
}]);