﻿var inboxCtrl = appModule.controller(appModule.Controllers.inbox, ['$scope', '$rootScope', '$inboxService', function ($scope, $rootScope, $inboxService) {

  function getCustomerMessages() {
    if ($inboxService.PendingMsgs) {
      $scope.Inbox = {
        PendingMsgs: $inboxService.PendingMsgs,
        PendingMsgsCnt: $inboxService.PendingMsgs.length
      };
      angular.forEach($scope.Inbox.PendingMsgs, function (msg) {
        if (msg.MsgPriority == "M") {
          msg.MsgStatus = "R";
          $inboxService.FlagCustomerMessage(msg.CustomerMessageId, "Read");
          return false;
        }
        return true;
      });
      $rootScope.safeApply();
    }
  };

  getCustomerMessages();

  //Watchers

  $rootScope.$on('messageDeleted', function () {
    $scope.Inbox.PendingMsgsCnt--;
  });

  //Methods

  $scope.FormatDate = function (message) {
    return CommonFunctions.FormatDateTime(message.MsgSentDateTime, 4);
  };

  $scope.FormatTime = function (message) {
    return CommonFunctions.FormatDateTime(message.MsgSentDateTime, 2);
  };

  $scope.FlagCustomerMessage = function (messageId, targetAction, divIdx) {
    switch (targetAction) {
      case "Delete":
        var divId = "mail_" + divIdx;
        $("#" + divId).hide('slow');

        angular.forEach($scope.Inbox.PendingMsgs, function (msg) {
          var currMessageId = msg.CustomerMessageId;

          if (currMessageId == messageId) {
            msg.MsgStatus = "D";
            $inboxService.FlagCustomerMessage(messageId, targetAction);
            return false;
          }
          return true;
        });
        break;
      case "Read":
        angular.forEach($scope.Inbox.PendingMsgs, function (msg) {
          var currMessageId = msg.CustomerMessageId;
          var currStatus = msg.MsgStatus;

          if (currMessageId == messageId) {
            if (currStatus != "R") {
              msg.MsgStatus = "R";
              $inboxService.FlagCustomerMessage(messageId, targetAction);
              return false;
            }
          }
          return true;
        });
        break;
    }
  };

  $scope.CollapseClass = function (index) {

    if ($inboxService.ExpandClass[index]) return "panel-collapse collapse in";
    else return "panel-collapse collapse";
  };

}]);