﻿var reportsCtrl = appModule.controller(appModule.Controllers.reports, [
    '$scope', '$rootScope', '$location', '$reportsService', '$customerService', '$translatorService', '$ticketService', '$wagerTypesService', '$sce', '$route', function ($scope, $rootScope, $location, $reportsService, $customerService, $translatorService, $ticketService, $wagerTypesService, $sce, $route) {
      var _isCalendarPick = false;

      $scope.DailyWinLossList = [];
      $scope.DailyCasinoWinLossList = [];
      $scope.DailyCashInOutList = [];
      $scope.ticketServiceView = $ticketService;
      $scope.currentDate = new Date();
      $scope.Arrow = -1;
      $scope.EndingBalance = 0;
      $scope.Report = {
        CasinoTransactions: [],
        CashTransactions: [],
        DailyFigures: null,
        OpenBets: null,
        AllTransactions: null,
        TransactionsByDate: null
      };

      var DailyWinLoss = function (hasValue, value) {
        return {
          hasValue: hasValue,
          value: value
        };
      };
      var CasinoWinLoss = function (hasValue, value) {
        return {
          hasValue: hasValue,
          value: value
        };
      };
      var CashInOut = function (hasValue, value) {
        return {
          hasValue: hasValue,
          value: value
        };
      };
      var Balance = function (hasValue, value) {
        return {
          hasValue: hasValue,
          value: value
        };
      };

      function cashTransactionsLoaded() {
        $scope.Report.CashTransactions = $reportsService.CashTransactionList;
        if (($scope.setCategory == 'bal' || ($scope.setCategory == 'cash' && $scope.IsMobileDevice)) && $scope.Report.CashTransactions) {
          if ($scope.setCategory == 'cash') {
            $scope.Report.TransactionsByDate = $scope.Report.CashTransactions;
          }
          else {
            for (var i = $scope.Report.CashTransactions.length - 1; i >= 0; i--) {
              var inserted = false;
              for (var j = 0; j < $scope.Report.TransactionsByDate.length; j++) {
                if (CommonFunctions.FormatDateTime($scope.Report.TransactionsByDate[j].TranDateTimeString, 10) <
                     CommonFunctions.FormatDateTime($scope.Report.CashTransactions[i].TranDateTimeString, 10)) {
                  $scope.Report.TransactionsByDate.splice(1, 0, $scope.Report.CashTransactions[i]);
                  inserted = true;
                  break;
                }
              }
              if (!inserted) {
                $scope.Report.TransactionsByDate.push($scope.Report.CashTransactions[i]);
              }
            }
            $scope.Total += $scope.ShowCashInOutTotal();
          }
        }
        $rootScope.safeApply();

        return true;
      };

      function showCashTransactions(index) {
        if ($customerService.Info)
          $reportsService.GetCashTransactionsByDate($customerService.Info.CustomerID, $scope.Report.DailyFigures.ValuesPerDay[index].ThisDate).then(cashTransactionsLoaded);
      };

      function casinoTransactionsLoaded() {
        $scope.Report.CasinoTransactions = $reportsService.CasinoTransactionList;
        if (($scope.setCategory == 'bal' || ($scope.setCategory == 'winloss' && $scope.IsMobileDevice)) && $scope.Report.CasinoTransactions) {
          var inserted = false;
          for (var i = 0; i < $scope.Report.CasinoTransactions.length; i++) {
            for (var j = 0; j < $scope.Report.TransactionsByDate.length; j++) {
              if (CommonFunctions.FormatDateTime($scope.Report.TransactionsByDate[j].TranDateTimeString, 1) <
                   CommonFunctions.FormatDateTime($scope.Report.CasinoTransactions[i], 1)) {
                $scope.Report.TransactionsByDate.splice(1, 0, $scope.Report.CasinoTransactions[i]);
                inserted = true;
                break;
              }

            }
            if (!inserted) {
              $scope.Report.TransactionsByDate.push($scope.Report.CasinoTransactions[i]);
            }
          }
          showCashTransactions($scope.Arrow);
        }
        else if ($scope.setCategory == 'bal') showCashTransactions($scope.Arrow);
        $rootScope.safeApply();

        return true;
      };

      function showDailyBalance() {
        if ($scope.Report.DailyFigures == null) return;
        var balance = $scope.Report.DailyFigures.StartingBalance != null ? $scope.Report.DailyFigures.StartingBalance : 0;
        for (var i = 0; i < 7; i++) {
          balance += ($scope.Report.DailyFigures.ValuesPerDay[i].WinLoss != null ? $scope.Report.DailyFigures.ValuesPerDay[i].WinLoss : 0)
              + ($scope.Report.DailyFigures.ValuesPerDay[i].CashInOut != null ? $scope.Report.DailyFigures.ValuesPerDay[i].CashInOut : 0)
              + ($scope.Report.DailyFigures.ValuesPerDay[i].CasinoWinLoss != null ? $scope.Report.DailyFigures.ValuesPerDay[i].CasinoWinLoss : 0);
          if (!$scope.Report.DailyFigures.ValuesPerDay[i].WagersCount && $scope.Report.DailyFigures.ValuesPerDay[i].WinLoss == null && $scope.Report.DailyFigures.ValuesPerDay[i].CashInOut == null && $scope.Report.DailyFigures.ValuesPerDay[i].CasinoWinLoss == null)
            $scope.DailyBalanceList.push(Balance(false, balance));
          else
            $scope.DailyBalanceList.push(Balance(true, balance));
        }
      };

      function showDailyCashInOut() {
        if ($scope.Report.DailyFigures == null) return;
        for (var i = 0; i < 7; i++) {
          $scope.DailyCashInOutList.push($scope.Report.DailyFigures.ValuesPerDay[i].CashInOut != null ? CashInOut(true, $scope.Report.DailyFigures.ValuesPerDay[i].CashInOut) : CashInOut(false, null));
        }
      };

      function showDailyWinLoss() {
        if ($scope.Report.DailyFigures == null) return;
        for (var i = 0; i < 7; i++) {
          var dailyWinLoss = $scope.Report.DailyFigures.ValuesPerDay[i].WinLoss != null || $scope.Report.DailyFigures.ValuesPerDay[i].WagersCount > 0 ? DailyWinLoss(true, $scope.Report.DailyFigures.ValuesPerDay[i].WinLoss) : DailyWinLoss(false, null);
          var casino = $scope.Report.DailyFigures.ValuesPerDay[i].CasinoWinLoss != null ? CasinoWinLoss(true, $scope.Report.DailyFigures.ValuesPerDay[i].CasinoWinLoss) : CasinoWinLoss(false, null);
          if ($scope.IsMobileDevice) {
            dailyWinLoss.hasValue = dailyWinLoss.hasValue || casino.hasValue;
            dailyWinLoss.value = (dailyWinLoss.value ? dailyWinLoss.value : 0) + (casino.value ? casino.value : 0);
          }
          $scope.DailyWinLossList.push(dailyWinLoss);
        }
      };

      function showDailyCasinoWinLoss() {
        if ($scope.Report.DailyFigures == null) return;
        for (var i = 0; i < 7; i++) {
          $scope.DailyCasinoWinLossList.push($scope.Report.DailyFigures.ValuesPerDay[i].CasinoWinLoss != null ? CasinoWinLoss(true, $scope.Report.DailyFigures.ValuesPerDay[i].CasinoWinLoss) : CasinoWinLoss(false, null));
        }
      };

      function showEndingBalance() {
        if ($scope.Report.DailyFigures == null) return;
        $scope.EndingBalance = $scope.Report.DailyFigures.StartingBalance + $scope.Report.DailyFigures.WinLossTotal + $scope.Report.DailyFigures.CashInOutTotal + $scope.Report.DailyFigures.CasinoWinLossTotal;
        if ($scope.Report.DailyFigures.ZeroBalance != null)
          $scope.EndingBalance += $scope.Report.DailyFigures.ZeroBalance;
      };

      function dailyFiguresLoaded() {
        $scope.DailyWinLossList = [];
        $scope.DailyCasinoWinLossList = [];
        $scope.DailyCashInOutList = [];
        $scope.DailyBalanceList = [];
        $scope.Report.DailyFigures = $reportsService.DailyFigures;
        showDailyWinLoss();
        showDailyCashInOut();
        showDailyCasinoWinLoss();
        showDailyBalance();
        showEndingBalance();
      };

      function dailyWagersLoaded() {
        for (var i = 0; i < $reportsService.DailyFigures.ValuesPerDay.length; i++) {
          if ($reportsService.DailyFigures.ValuesPerDay[i].ThisDate == $reportsService.DailyFigureDate) {
            $scope.Report.DailyFigures.Details = $reportsService.DailyFigures.ValuesPerDay[i].Wagers;
            break;
          }
        }
      };

      function openBetsLoaded() {
        $scope.Report.OpenBets = $reportsService.GroupedOpenBets;
        $rootScope.safeApply();
        return true;
      };

      function transactionListLoaded() {
        $scope.Report.AllTransactions = $reportsService.TransactionList;
        $rootScope.safeApply();

        return true;
      };

      function showCasinoTransactions (index) {
        if ($customerService.Info)
          $reportsService.GetCasinoTransactionsByDate($customerService.Info.CustomerID, $scope.Report.DailyFigures.ValuesPerDay[index].ThisDate).then(casinoTransactionsLoaded);
      };

      function transactionListByDateLoaded() {
        $scope.Report.TransactionsByDate = $reportsService.TransactionList;
        if ($scope.setCategory == 'bal' && $scope.Report.TransactionsByDate) {
          showCasinoTransactions($scope.Arrow);
        }
        if ($scope.setCategory == 'winloss') {
          showCasinoTransactions($scope.Arrow);
        }
        $rootScope.safeApply();

        return true;
      };

      function showTransactionsbyDate(date) {
        if ($customerService.Info)
          $reportsService.GetCustomerTransactionListByDate($customerService.Info.CustomerID, date).then(transactionListByDateLoaded);
      };

      function openDailyFiguresDetails() {
        var l = document.getElementById('monday');
        l.className = ('collapse in');
      };

      $scope.RenderHtml = function (htmlCode) {
        if (!htmlCode) return "";
        return $sce.trustAsHtml(htmlCode);
      };

      $scope.HighlightedClass = function (day) {

        if ($scope.Report.DailyFigures)
          return ($scope.FormatDateTime($scope.currentDate, 1) == $scope.FormatDateTime($scope.Report.DailyFigures.ValuesPerDay[day].ThisDate, 1) ? 'day_selected' : 'day_unselected');
        return null;
      };

      $scope.ShowCasinoDetail = function (casinoDetail) {
        return casinoDetail.split('-')[0];

      };

      $scope.HideGrades = function () {
        $rootScope.ShowGrades = false;
      }

      $scope.ShowMoreDetails = function (index, category) {
        if ($scope.IsMobileDevice) $rootScope.ShowGrades = true;
        if ($scope.Report.DailyFigures == null)
          return false;
        if ($scope.Arrow != index || category != $scope.setCategory) {
          $scope.Report.CasinoTransactions = [];
          $scope.Report.CashTransactions = [];
          $scope.Report.TransactionsByDate = null;

          openDailyFiguresDetails();
          $scope.Arrow = index;
          $scope.TransactionDate = $scope.Report.DailyFigures.ValuesPerDay[index].ThisDate;
          switch (category) {
            case "cashintout":
              $scope.Total = $scope.DailyCashInOutList[index].value;
              $scope.setCategory = "cash";
              showCashTransactions(index);
              break;
            case "winloss":
              $scope.Total = $scope.DailyWinLossList[index].value;
              $scope.setCategory = "winloss";
              showTransactionsbyDate($scope.Report.DailyFigures.ValuesPerDay[index].ThisDate);
              break;
            case "casino":
              $scope.Total = $scope.DailyCasinoWinLossList[index].value;
              $scope.setCategory = "casino";
              showCasinoTransactions(index);
              break;
            case "bal":
              $scope.Total = $scope.DailyWinLossList[index].value;
              $scope.setCategory = "bal";
              showTransactionsbyDate($scope.Report.DailyFigures.ValuesPerDay[index].ThisDate);
              break;
          }
        }
        return false;
      };

      $scope.ShowCasinoLine = function () {
        for (var i = 0; $scope.DailyCasinoWinLossList.length > i ; i++)
          if ($scope.DailyCasinoWinLossList[i].hasValue) return true;
        return false;
      };

      $scope.FormatDateTime = function (acceptedDateTime, formatCode) {
        return CommonFunctions.FormatDateTime(acceptedDateTime, formatCode);
      };

      $scope.GetChosenTeamRotNumAndName = function (openBetItem, returnedString) {
        var teamRotNumAndName = "";
        //returnedString can be rotNumber, teamId, or both
        switch (openBetItem.WagerType) {
          case "S":
          case "M":
          case "E": //team Totals
          case "P":
          case "T":
          case "I":
            switch (returnedString) {
              case "both":
                if (openBetItem.ChosenTeamID == openBetItem) {
                  teamRotNumAndName = openBetItem.Team1RotNum + " " + openBetItem.ChosenTeamID;
                } else {
                  teamRotNumAndName = openBetItem.Team2RotNum + " " + openBetItem.ChosenTeamID;
                }
                break;
              case "rotNum":
                if (openBetItem.ChosenTeamID == openBetItem) {
                  teamRotNumAndName = openBetItem.Team1RotNum;
                } else {
                  teamRotNumAndName = openBetItem.Team2RotNum;
                }
                break;
              case "teamId":
                if (openBetItem.ChosenTeamID == openBetItem) {
                  teamRotNumAndName = openBetItem.ChosenTeamID;
                } else {
                  teamRotNumAndName = openBetItem.ChosenTeamID;
                }
                break;
            }
            break;
          case "C":
            break;
          case "L": //Total points
            teamRotNumAndName = openBetItem.ChosenTeamID;
            break;
        }
        return teamRotNumAndName;
      };

      $scope.GetWagerStatus = function (openBet) {
        var wagerStatus = "";

        if (openBet.WagerStatus == "O") {
          wagerStatus = " (" + $scope.Translate("OPEN_BET_LABEL") + ")";
        }
        return wagerStatus;
      };

      $scope.GetWinLossDetails = function (index) {
        $scope.Report.DailyFigures.DetailType = "winloss";

        if ($customerService.Info)
          $reportsService.GetDailyWagers($customerService.Info.CustomerID, $scope.Report.DailyFigures.ValuesPerDay[index].ThisDate, $customerService.Info.Store).then(dailyWagersLoaded);

      };

      $scope.ShowAllTransactions = function () {
        $scope.Templates.reports.selected = "/app/components/reports/allTransactions.html";

      };

      $scope.ShowCurrentMonths = function () {
        if ($scope.Report.DailyFigures == null)
          return "";
        var months = ["JAN", "FEB ", "MAR", "APR", "MAY ", "JUNE", "JULY", "AUG", "SEPT", "OCT ", "NOV", "DEC"];
        var output = [];

        for (var i = 0; i < $scope.Report.DailyFigures.ValuesPerDay.length; i++) {
          var monthNum = $scope.Report.DailyFigures.ValuesPerDay[i].ThisDate.substring(0, $scope.Report.DailyFigures.ValuesPerDay[i].ThisDate.indexOf("/")) - 1;

          if (output.indexOf(months[monthNum]) < 0)
            output.push(months[monthNum]);
        }
        return output.join(" / ");
      };

      $scope.ShowDailyFigures = function (weekOffset) {
        if ($customerService.Info)
          $reportsService.GetCustomerDailyFigures($customerService.Info.CustomerID, weekOffset, $customerService.Info.Currency.substring(0, 3)).then(dailyFiguresLoaded);
        $scope.Templates.reports.selected = "/app/components/reports/dailyFigures.html";
      };

      $scope.ShowDateNumber = function (index) {
        if ($scope.Report.DailyFigures == null)
          return "";
        var date = $scope.Report.DailyFigures.ValuesPerDay[index].ThisDate.substring($scope.Report.DailyFigures.ValuesPerDay[index].ThisDate.indexOf("/") + 1);
        return date.substring(0, date.indexOf("/"));
      };

      $scope.ShowShortDate = function (index) {
        if ($scope.Report.DailyFigures == null)
          return "";
        return $scope.Report.DailyFigures.ValuesPerDay[index].ThisDate;
      }

      $rootScope.ShowOpentBets = function () {
        if ($customerService.Info)
          $reportsService.GetCustomerPendingBets($customerService.Info.CustomerID).then(openBetsLoaded);
        $scope.Templates.reports.selected = "/app/components/reports/openBets.html";

      };

      $scope.ShowStartingBalance = function () {
        if (!$scope.Report.DailyFigures || $scope.Report.DailyFigures.StartingBalance == null)
          return "";
        return $scope.Report.DailyFigures.StartingBalance != null ? $scope.Report.DailyFigures.StartingBalance : 0;
      };

      $scope.ShowWinLossTotal = function () {
        if ($scope.Report.DailyFigures == null)
          return "";
        return $scope.Report.DailyFigures.WinLossTotal != null ? $scope.Report.DailyFigures.WinLossTotal : 0;
      };

      $scope.ShowCashInOutTotal = function () {
        if ($scope.Report.DailyFigures == null)
          return "";
        return $scope.Report.DailyFigures.CashInOutTotal != null ? $scope.Report.DailyFigures.CashInOutTotal : 0;
      };

      $scope.ShowCasinoWinLossTotal = function () {
        if ($scope.Report.DailyFigures == null)
          return "";
        return $scope.Report.DailyFigures.CasinoWinLossTotal != null ? $scope.Report.DailyFigures.CasinoWinLossTotal : 0;
      };

      $scope.ShowZeroBalance = function () {
        if ($scope.Report.DailyFigures == null)
          return "";
        return $scope.Report.DailyFigures.ZeroBalance != null ? $scope.Report.DailyFigures.ZeroBalance : 0;
      };

      $scope.CloseDailyFiguresDetails = function () {
        document.getElementById('monday').className = ('collapse');
        $scope.Arrow = -1;
      };

      $scope.ShowNextWeek = function () {
        $scope.CloseDailyFiguresDetails();
        if ($scope.Report.DailyFigures == null || $scope.Report.DailyFigures.WeekOffset == 0)
          return false;
        _isCalendarPick = false;
        $scope.ShowDailyFigures($scope.Report.DailyFigures.WeekOffset - 1);
        return true;
      };

      $scope.ShowThisWeek = function () {
        $scope.ShowDailyFigures(0);
        return true;
      }

      $scope.ShowPreviousWeek = function () {
        $scope.CloseDailyFiguresDetails();
        if ($scope.Report.DailyFigures == null)
          return false;
        _isCalendarPick = false;
        $scope.ShowDailyFigures($scope.Report.DailyFigures.WeekOffset + 1);
        return true;
      };

      $scope.ShowWagerDetails = function (outcome, oBi) {
        if (outcome == 'P') {
          return $scope.ScoredPtsHeader(oBi) + "<br />" + $scope.WriteTeamsScores(oBi);
        }
        return "";
      };

      $scope.WriteChosenTeamId = function (openBetItem) {
        var chosenTeam;

        if (openBetItem.WagerType == "C") {
          chosenTeam = openBetItem.Description;
        } else {
          chosenTeam = $scope.GetChosenTeamRotNumAndName(openBetItem, 'both');
        }

        return chosenTeam;
      };

      $scope.WriteShowDailyCasinoWinLosselectedLine = function (openBetItem) {
        var selectedLine = "";
        var origSpread = 0.0;
        var finalSpread = 0.0;
        var pointsBought = 0.0;
        var line = 0.0;
        var price = 0;
        var priceStr = "";
        var decimalPrice = 0.0;
        var numerator = 0;
        var denominator = 1;

        price = openBetItem.FinalMoney == null ? 0 : openBetItem.FinalMoney;
        decimalPrice = openBetItem.FinalDecimal == null ? 0 : openBetItem.FinalDecimal;
        numerator = openBetItem.FinalNumerator == null ? 0 : openBetItem.FinalNumerator;
        denominator = openBetItem.FinalDenominator == null ? 1 : openBetItem.FinalDenominator;

        switch (openBetItem.ItemWagerType) {
          case "S":
            origSpread = openBetItem.OrigSpread == null ? 0 : openBetItem.OrigSpread;
            finalSpread = openBetItem.AdjSpread == null ? 0 : openBetItem.AdjSpread;
            pointsBought = Math.abs(Math.abs(origSpread) - Math.abs(finalSpread));

            line = finalSpread;
            if (openBetItem.WagerType == "T")
              line += openBetItem.TeaserPoints == null ? 0 : openBetItem.TeaserPoints;
            selectedLine = LineOffering.FormatAhSpread(line, ",", true, pointsBought);
            switch (openBetItem.PriceType) {
              case "A":
                if (price > 0) {
                  priceStr += "+";
                }
                priceStr += price;
                break;
              case "D":
                priceStr += decimalPrice;
                if (decimalPrice == Math.floor(decimalPrice)) {
                  priceStr += ".0";
                }
                break;
              case "F":
                priceStr += numerator;
                priceStr += "/";
                priceStr += denominator;
                break;
            }
            selectedLine += " " + priceStr;
            break;
          case "M":
            if (openBetItem.SportType == "Baseball" && openBetItem.EasternLine != null) {
              if (openBetItem.EasternLine > 0) {
                priceStr += "+";
              }
              priceStr += LineOffering.ConvertToHalfSymbol(openBetItem.EasternLine == null ? 0 : openBetItem.EasternLine);
            } else {
              switch (openBetItem.PriceType) {
                case "A":
                  if (price > 0) {
                    priceStr += "+";
                  }
                  priceStr += price;
                  break;
                case "D":
                  priceStr += decimalPrice;
                  if (decimalPrice == Math.floor(decimalPrice)) {
                    priceStr += ".0";
                  }
                  break;
                case "F":
                  priceStr += numerator;
                  priceStr += "/";
                  priceStr += denominator;
                  break;
              }
              selectedLine += " " + priceStr;
            }
            break;
          case "L":
            var origTpoints = openBetItem.OrigTotalPoints == null ? 0 : openBetItem.OrigTotalPoints;
            var finalTpoints = openBetItem.AdjTotalPoints == null ? 0 : openBetItem.AdjTotalPoints;
            pointsBought = Math.abs(Math.abs(origTpoints) - Math.abs(finalTpoints));
            line = finalTpoints;
            if (openBetItem.TotalPointsOU != null && openBetItem.TotalPointsOU == "O") {
              selectedLine += $scope.Translate("OVER_LABEL") + " "; //"Over ";
              if (openBetItem.WagerType == "T") {
                line -= openBetItem.TeaserPoints == null ? 0 : openBetItem.TeaserPoints;
              }
            } else {
              selectedLine += $scope.Translate("UNDER_LABEL") + " "; //"Under ";
              if (openBetItem.WagerType == "T") {
                line += openBetItem.TeaserPoints == null ? 0 : openBetItem.TeaserPoints;
              }
            }
            selectedLine += LineOffering.FormatAhTotal(line, ",", true, pointsBought);
            switch (openBetItem.PriceType) {
              case "A":
                if (price > 0) {
                  priceStr += "+";
                }
                priceStr += price;
                break;
              case "D":
                priceStr += decimalPrice;
                if (decimalPrice == Math.floor(decimalPrice)) {
                  priceStr += ".0";
                }
                break;
              case "F":
                priceStr += numerator;
                priceStr += "/";
                priceStr += denominator;
                break;
            }
            selectedLine += " " + priceStr;
            break;
          case "E":
            line = openBetItem.AdjTotalPoints == null ? 0 : openBetItem.AdjTotalPoints;
            if (openBetItem.TotalPointsOU != null && openBetItem.TotalPointsOU == "O") {
              selectedLine += $scope.Translate("TEAM_TOTAL") + " " + $scope.Translate("OVER_LABEL") + " "; //"Team Total Over ";
              if (openBetItem.WagerType == "T") {
                line -= openBetItem.TeaserPoints == null ? 0 : openBetItem.TeaserPoints;
              }
            } else {
              selectedLine += $scope.Translate("TEAM_TOTAL") + " " + $scope.Translate("UNDER_LABEL") + " "; //"Team Total Under ";
              if (openBetItem.WagerType == "T") {
                line += openBetItem.TeaserPoints == null ? 0 : openBetItem.TeaserPoints;
              }
            }
            selectedLine += LineOffering.ConvertToHalfSymbol(line);

            if (openBetItem.WagerType != "T") {
              switch (openBetItem.PriceType) {
                case "A":
                  if (price > 0) {
                    priceStr += "+";
                  }
                  priceStr += price;
                  break;
                case "D":
                  priceStr += decimalPrice;
                  if (decimalPrice == Math.floor(decimalPrice)) {
                    priceStr += ".0";
                  }
                  break;
                case "F":
                  priceStr += numerator;
                  priceStr += "/";
                  priceStr += denominator;
                  break;
              }
              selectedLine += " " + priceStr;
            }
            break;
        }

        selectedLine += " " + $scope.Translate("FOR") + " " + openBetItem.PeriodDescription;

        return selectedLine;
      };

      $scope.WritePitchersInfo = function (openBetItem) {
        var pitchersInfo = "";

        if (openBetItem.SportType != null && openBetItem.SportType == "Baseball") {
          if (openBetItem.ListedPitcher1 != null) {
            pitchersInfo += openBetItem.ListedPitcher1;
            if (openBetItem.Pitcher1ReqFlag != null && openBetItem.Pitcher1ReqFlag == "Y") {
              pitchersInfo += " " + $scope.Translate("PITCHER_MUST_START"); //" must start";
            } else {
              pitchersInfo += " " + $scope.Translate("PITCHER_ACTION"); //" action";
            }
            pitchersInfo += " / ";
          }

          if (openBetItem.ListedPitcher2 != null) {
            pitchersInfo += openBetItem.ListedPitcher2;
            if (openBetItem.Pitcher2ReqFlag != null && openBetItem.Pitcher2ReqFlag == "Y") {
              pitchersInfo += " " + $scope.Translate("PITCHER_MUST_START"); //" must start";
            } else {
              pitchersInfo += " " + $scope.Translate("PITCHER_ACTION"); //" action";
            }
          }

        }

        return pitchersInfo;
      };

      $scope.ScoredPtsHeader = function (openBetItem) {
        if (openBetItem.Team1Score == null || isNaN(openBetItem.Team1Score) || openBetItem.Team2Score == null || isNaN(openBetItem.Team2Score))
          return "";
        var resultsTitle = "";
        if (openBetItem.SportType != null && openBetItem.SportType == "Soccer") {
          resultsTitle += $scope.Translate("GOALS") + " "; //"Goals ";
        } else {
          resultsTitle += $scope.Translate("POINTS") + " "; //"Points ";
        }

        var periodDescripion = "";
        if (openBetItem.PeriodDescription != null)
          periodDescripion = openBetItem.PeriodDescription;

        if (openBetItem.PeriodDescription != null) {
          resultsTitle += $scope.Translate("SCORED_IN") + " " + $scope.Translate(periodDescripion); //" period:";
        }

        return resultsTitle;
      };

      $scope.WriteTeamsScores = function (openBetItem) {
        if (openBetItem.Team1Score == null || isNaN(openBetItem.Team1Score) || openBetItem.Team2Score == null || isNaN(openBetItem.Team2Score))
          return openBetItem.Outcome == "P" ? $scope.Translate("Pending") : "";

        var teamsResults = "";
        var team1Id = "";
        var team2Id = "";
        var team1Score = "";
        var team2Score = "";
        if (!openBetItem) return "";
        if (openBetItem.Team1ID != null)
          team1Id = openBetItem.Team1ID;
        if (openBetItem.Team2ID != null)
          team2Id = openBetItem.Team2ID;
        if (openBetItem.Team1Score != null)
          team1Score = openBetItem.Team1Score;
        if (openBetItem.Team2Score != null)
          team2Score = openBetItem.Team2Score;
        teamsResults += team1Id + " - " + team1Score + " / " + team2Id + " - " + team2Score;
        if (openBetItem.WagerType == "E" || openBetItem.WagerType == "L" || openBetItem.WagerType == "T") {
          teamsResults += " " + $scope.Translate("TOTAL_POINTS") + ": " + (team1Score + team2Score);
        }
        return teamsResults;
      };

      $scope.WriteWinner = function (openBetItem) {


        var team1Score = "";
        var team2Score = "";
        var winner = "";
        if (openBetItem.Team1Score != null)
          team1Score = openBetItem.Team1Score;
        if (openBetItem.Team2Score != null)
          team2Score = openBetItem.Team2Score;
        if (team1Score != team2Score) {
          var winnerId = "";
          if (openBetItem.WinnerID != null)
            winnerId = openBetItem.WinnerID;
          winner += winnerId + " " + $scope.Translate("WON PERIOD BY") + " " + Math.abs(team1Score - team2Score);
        }

        return winner;
      };

      $scope.WriteWagerDescription = function (openBet) {
        var description = CommonFunctions.WriteWagerDescription(openBet, $translatorService);
        return description;
      };

      $scope.PointsBought = function (wi) {
        var pointsbought = 0;
        var lineStr = "";
        switch (wi.ItemWagerType) {
          case "S":
            var origSpread = wi.OrigSpread;
            var finalSpread = wi.AdjSpread;
            if (origSpread != finalSpread) pointsbought = (origSpread > 0 ? origSpread - finalSpread : Math.abs(finalSpread) - Math.abs(origSpread));
            pointsbought = Math.abs(pointsbought);
            if (wi.PeriodNumber == 0) lineStr += LineOffering.PointsBought(pointsbought);
            break;
          case "L":
            var origtpoints = wi.OrigTotalPoints;
            var finaltpoints = wi.AdjTotalPoints;
            if (origtpoints != finaltpoints) pointsbought = (origtpoints > 0 ? origtpoints - finaltpoints : finaltpoints + origtpoints);
            pointsbought = Math.abs(pointsbought);
            if (wi.PeriodNumber == 0) lineStr += LineOffering.PointsBought(pointsbought);
            break;
        }
        return lineStr;
      };

      $scope.GetWagerItemDescription = function (fullWagerDescription, wagerItemIndex) {
        var descArray = fullWagerDescription.split('\r\n');
        if (descArray.length > 0 && descArray[wagerItemIndex])
          return descArray[wagerItemIndex].replace("Credit Adjustment", '').replace("Debit Adjustment", '');
        else return fullWagerDescription.replace("Credit Adjustment", '').replace("Debit Adjustment", '');
      };

      $scope.$watch('Templates.reports.selected', function () {
        if ($scope.Templates.reports.selected == "/app/components/reports/allTransactions.html") {
          setTimeout(function () {

            function cb(start, end) {
              $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
              if (end.format('L') == moment().format('L')) {
                var diff = end.diff(start, 'days');
                if ($customerService.Info)
                  $reportsService.GetCustomerTransactionListByDays($customerService.Info.CustomerID, diff + 1).then(transactionListLoaded);
              } else {
                $reportsService.GetCustomerTransactionListByDateRange($customerService.Info.CustomerID, start.toDate(), end.toDate()).then(transactionListLoaded);
              }
            }

            cb(moment().subtract(6, 'days'), moment());
            $('#reportrange').daterangepicker({
              locale: {
                firstDay: 1
              },
              startDate: moment().subtract(6, 'days'),
              ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment()],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'Last 60 Days': [moment().subtract(59, 'days'), moment()],
                'Last 90 Days': [moment().subtract(89, 'days'), moment()]
              }
            }, cb);
          }, 500);
        }

        else if ($scope.Templates.reports.selected == "/app/components/reports/dailyFigures.html") {
          setTimeout(function () {

            $('input[name="ReportDate"]').daterangepicker({
              locale: {
                firstDay: 1
              },
              singleDatePicker: true,
              showDropdowns: true,
              opens: 'left'
            },
            function (start, end) {
              _isCalendarPick = true;
              start.isoWeekday(1);
              var currentWeek = start.toDate().getYear() < moment().toDate().getYear() ? start.weeksInYear() + moment().week() : moment().week();
              $scope.ShowDailyFigures(currentWeek - start.week());
              $scope.currentDate = end.toDate();
              $rootScope.safeApply();

            });

          }, 500);
        }

      });

      $scope.isXTransaction = false;
      $scope.WagerType = function (t) {
        var twriter = t.EnteredBy || t.TicketWriter;
        if (twriter) {

          if (twriter.indexOf("HORSE") != -1) {
            $scope.isXTransaction = true;
            return $scope.Translate("Horses");
          }
          if (twriter.indexOf("GSLIVE") != -1) {
            $scope.isXTransaction = true;
            return $scope.Translate("Live Betting");
          }
          if (twriter.indexOf("System") != -1) {
            $scope.isXTransaction = true;
            return $scope.Translate("Casino");
          }
          /*switch (twriter) {
                  case "System":
                      $scope.isXTransaction = true;
                      return $scope.Translate("Casino");
                  case "LIVE":
                      $scope.isXTransaction = true;
                      return $scope.Translate("Live Betting");
                  default:
                      if (twriter.indexOf("COLCHIAN") != -1) {
                          $scope.isXTransaction = true;
                          return $scope.Translate("Horses");
                      }
          }*/
        }
        $scope.isXTransaction = false;
        return LineOffering.WagerTypes.GetByCode(t.WagerType, t.TeaserName, t.ContinueOnPushFlag, t.ARLink, t.RoundRobinLink);
      };

      $scope.OpenButtonText = function (totalPicks, totalItems) {
        var pendingBets = totalPicks - totalItems;
        return $scope.Translate('Fill') + ' (' + pendingBets + ') ' + $scope.Translate('Open Spots');
      };

      $scope.Init = function () {
        setTimeout(function () {
          if (!$scope.DefaultReport || typeof $scope.DefaultReport == "undefined") {
            if ($route.current.originalPath == "/openBets")
              $scope.ShowOpentBets();
            else $scope.ShowDailyFigures(0);
          }
          else {
            switch ($scope.DefaultReport) {
              case "openbets":
                $scope.ShowOpentBets();
                break;
              default:
                $scope.ShowDailyFigures(0);
            }
          }
          if ($rootScope.IsMobileDevice) $rootScope.menuTab = 2;
        }, 100);
      };

      $scope.Init();

    }]);

changeIcon = function (el) {
  var st;
  if (el.className.indexOf('icon_viewresults_minus') != -1) {
    st = el.className.toString().replace("icon_viewresults_minus", "");
    el.className = "icon_viewresults " + st;
  }
  else {
    st = el.className.toString().replace("icon_viewresults", "");
    el.className = "icon_viewresults_minus " + st;
  }
};