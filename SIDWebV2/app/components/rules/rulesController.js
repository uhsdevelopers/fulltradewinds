﻿var rulesCtrl = appModule.controller(appModule.Controllers.rules, ['$scope', '$rootScope', '$routeParams', '$sce', '$rulesService', function ($scope, $rootScope, $routeParams, $sce, $rulesService) {

  $scope.rulesMenus = [];

  function getWordpressLangCode(lang) {
    switch (lang) {
      case "SPA": return "es";
      case "TH": return "th";
      case "ENG": return "en";
      case "ZH": return "zh-hant";
    }
    return "";
  }

  $scope.RenderHtml = function (htmlCode) {
    if (!htmlCode) return "";
    return $sce.trustAsHtml(htmlCode);
  };

  $scope.ShowRule = function (ruleObj, lastLevel) {
    if (lastLevel)
      $scope.ToggleMenu();
    if (ruleObj) {
      var timeOut = 200;
      var pageId = ruleObj.page_id !== "" ? ruleObj.page_id : ruleObj.object_id;
      if ($scope.currentRuleId !== pageId) {
        $scope.currentRuleId = pageId;
        $rootScope.pageContent = "";
        $rulesService.GetRuleContent($scope.currentRuleId, $scope.Customer.Info.AgentID).then(function (result) {
          if (result.data.page)
            $rootScope.pageContent = $scope.RenderHtml(result.data.page.post_content);
        });
        timeOut = 500;
        location.hash = "#/";
      }
      if (ruleObj.url && ruleObj.url.indexOf("#") >= 0) {
        setTimeout(function () {
          var startPos = ruleObj.url.indexOf("#");
          var endPos = ruleObj.url.length;
          var anchor = ruleObj.url.substring(startPos, endPos);
          location.hash = anchor;
        }, timeOut);
      }
    }
    return false;
  };

  $rulesService.LoadMenus(getWordpressLangCode($scope.Customer.Settings.WebLanguage));

  $rootScope.$on('SportRules', function () {
    $scope.rulesMenus = $rulesService.Rules;
    for (var i = 0; $rulesService.Rules.length > i; i++) {
      if ($rulesService.Rules[i].label === "Sports Rules") {
        $scope.ShowRule($rulesService.Rules[i].menu[0]);
      }
    }

  });

}]);