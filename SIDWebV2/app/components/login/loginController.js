﻿var loginCtrl = loginModule.controller(loginModule.loginCtrl, ['$scope', '$LoginService', '$translatorService', function ($scope, $loginService, $translatorService) {

  $scope.loginView = loginModule.loginView;
  $scope.loginMessage = "";
  $scope.Mail = "";
  $scope.credentials = {
    customerId: "",
    password: ""
  };
  $scope.ErrorCounter = 0;

  var _encryptedMessage;
  var _currentPath = window.location.pathname;
  if (_currentPath.toString().indexOf("SessionExpired") >= 0) { $scope.loginMessage = "Session expired. Please login."; $scope.ErrorCounter = 1; }

  if (document.getElementById("user").value.toString().trim() != "" && document.getElementById("pass").value.toString().trim() != "") $scope.InputsFilled = true;

  function autoLogIn() {
    var form = document.createElement("form");
    var element1 = document.createElement("input");
    var element2 = document.createElement("input");
    form.method = "POST";
    form.action = SETTINGS.AgentSite;

    element1.value = $scope.credentials.customerId;
    element1.name = "customerID1";
    element1.type = "hidden";
    form.appendChild(element1);

    element2.value = $scope.credentials.password;
    element2.name = "password1";
    element2.type = "hidden";
    form.appendChild(element2);

    document.body.appendChild(form);

    form.submit();
  }

  $scope.Translate = function (text) {
    return $translatorService.Translate(text);
  }

  $scope.CustomerSignIn = function () {
    $loginService.RequestToken().then(function (resp) {
      var key = resp.data.d.Message;
      var iv = resp.data.d.Message;
      _encryptedMessage = CryptoJS.AES.encrypt($scope.credentials.password, CryptoJS.enc.Utf8.parse(key), {
        keySize: 128 / 8,
        iv: CryptoJS.enc.Utf8.parse(iv),
        mode: CryptoJS.mode.CBC,
        padding: CryptoJS.pad.Pkcs7
      });
      $loginService.CustomerSignIn($scope.credentials.customerId, _encryptedMessage.ciphertext.toString(CryptoJS.enc.Base64), key).then(function (response) {
        if (response.data.d.Code == 0) {
          if (response.data.d.Data == "Agent login") {
            autoLogIn();
            return;
          }
          $scope.ErrorCounter = 0;
          Utilities.Redirect("/");
        } else if (response.data.d.Data != null) {
          if ($scope.ErrorCounter == 0) {
            $scope.ErrorCounter = 3;
          };
          $scope.Mail = (response.data.d.Data == "" ? "N/D" : response.data.d.Data.MsgBody);
        } else {
          $scope.Mail = "";
          $scope.loginMessage = response.data.d.Message;
          if ($scope.ErrorCounter == 0) {
            $scope.ErrorCounter = 1;
          }
        }
        if ($scope.ErrorCounter > 0) {
          $scope.ErrorCounter = 2;
          setTimeout(function () {
            $scope.ErrorCounter = 1;
          }, 500);
        }
        return;
      });
    });
  };

  window.ObsoleteBrowserLog = function () {
    $loginService.ObsoleteBrowserLog().then();
  };

}]);