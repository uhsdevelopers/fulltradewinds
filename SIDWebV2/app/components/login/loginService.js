﻿loginModule.factory('$LoginService', ['$http', function ($http) {

    var _caller = new ServiceCaller($http, null, 'LoginService', 'CustomerSignIn');

    return {

        //Document_Function
        RequestToken: function () {
            return _caller.POST({}, 'TemporalToken');
        },

        //Document_Function
        CustomerSignIn: function (customerId, password, obsoleteKey) {
            return _caller.POST({ 'customerId': 'tw'+customerId, 'password': password, 'encryptedKey': obsoleteKey, 'agentId': null }, 'CustomerSignIn');
        },

        //Document_Function
        ObsoleteBrowserLog: function () {
            return _caller.POST({ }, 'ObsoleteBrowserLog');
        }

    };
}]);