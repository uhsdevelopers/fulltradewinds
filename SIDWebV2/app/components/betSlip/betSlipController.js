﻿var betSlipCtrl = appModule.controller(appModule.Controllers.betSlip, ['$scope', '$rootScope', '$ticketService', '$translatorService', '$wagerTypesService', '$customerService', '$settingsService', '$encryptionService', function ($scope, $rootScope, $ticketService, $translatorService, $wagerTypesService, $customerService, $settingsService, $encryptionService) {

    $scope.ticketServiceView = $ticketService;
    $scope.customerServiceView = $customerService;
    $scope.wagerTypesServiceView = $wagerTypesService;
    $scope.RiskPlaceHolder = $translatorService.Translate("Risk_Amt");
    $scope.WinPlaceHolder = $translatorService.Translate("Win_Amt");
    $scope.Banners = [];
    var _winMax = 0;
    var _riskMax = 0;
    var _onBlurCalls = 0;
    var _applyToAllMessageAlreadyShown = false;

    //#region Private Methods

    function checkAllFieldsFilled () {
        switch ($wagerTypesService.Selected.id) {
            case $wagerTypesService.Ids.Parlay:
            case $wagerTypesService.Ids.Teaser:
                return $ticketService.Ticket.TotalRiskAmount > 0 && $ticketService.Ticket.TotalToWinAmount > 0;
            case $wagerTypesService.Ids.IfWinOrPush:
            case $wagerTypesService.Ids.IfWinOnly:
                return true;
            case $wagerTypesService.Ids.ActionReverse:
                return $ticketService.Ticket.TotalRiskAmount > 0;
            default:
                var cnt = 0;
                for (var i = 0; i < $ticketService.Ticket.WagerItems.length; i++) {
                    var wi = $ticketService.Ticket.WagerItems[i];
                    if (wi.RiskAmt > 0 && wi.ToWinAmt > 0) cnt++;
                    else return false;
                }
                return cnt == $ticketService.Ticket.WagerItems.length;
        }
    };

    function calculateToWinAmount (wagerItem) {
        if (wagerItem == null || wagerItem.Type == "G") {
            var wagersArray = [];
            switch ($wagerTypesService.Selected.id) {
                case $wagerTypesService.Ids.StraightBet:
                case $wagerTypesService.Ids.IfWinOnly:
                case $wagerTypesService.Ids.IfWinOrPush:
                    if (wagerItem == null) return;
                    wagerItem.ToWinAmt = CommonFunctions.RoundNumber(LineOffering.CalculateToWinAmtUsingWi(wagerItem));
                    break;
                case $wagerTypesService.Ids.Parlay:
                    var roundRobin = $ticketService.GetSelectedRoundRobin();
                    if (roundRobin == null || roundRobin.value == 0) {
                        if ($ticketService.IsOpenPlays()) { wagersArray.push.apply(wagersArray, $ticketService.Ticket.OpenWagerItems); wagersArray.push.apply(wagersArray, $ticketService.Ticket.WagerItems); }
                        else wagersArray = $ticketService.Ticket.WagerItems;
                        $ticketService.Ticket.TotalToWinAmount = CommonFunctions.RoundNumber(ParlayFunctions.CalculateParlayToWinAmt(wagersArray, $ticketService.Ticket.ParlayInfo, ParlayFunctions.GetPayCardMaxPayout(wagersArray.length, $ticketService.Ticket.ParlayInfo, $ticketService.Ticket.TotalRiskAmount), $ticketService.Ticket.TotalRiskAmount));
                        $ticketService.Ticket.PlayCount = 1;

                    }
                    else {
                        var rrValues = ParlayFunctions.CalculateRoundRobinToWin($ticketService.Ticket.WagerItems, $ticketService.Ticket.ParlayInfo, $ticketService.Ticket.TotalRiskAmount, roundRobin.value);
                        $ticketService.Ticket.TotalToWinAmount = CommonFunctions.RoundNumber(rrValues.ToWinAmt);
                        $ticketService.Ticket.PlayCount = rrValues.PlayCount;
                    }
                    break;
                case $wagerTypesService.Ids.Teaser:
                    if ($ticketService.IsOpenPlays()) { wagersArray.push.apply(wagersArray, $ticketService.Ticket.OpenWagerItems); wagersArray.push.apply(wagersArray, $ticketService.Ticket.WagerItems); }
                    else wagersArray = $ticketService.Ticket.WagerItems;
                    var pc = TeaserFunctions.GetPayCard($ticketService.Ticket.TeaserInfo.teaserPayCards, ($ticketService.Ticket.KeepOpenPlay && $ticketService.openPlaysVal ? $ticketService.openPlaysVal.value + $ticketService.TotalWagers() : $ticketService.TotalWagers()));
                    if (pc != null) {
                        $ticketService.Ticket.TotalToWinAmount = CommonFunctions.RoundNumber(TeaserFunctions.CalculateToWin(pc, $ticketService.Ticket.TotalRiskAmount));
                    }
                    break;
                case $wagerTypesService.Ids.ActionReverse:
                    $ticketService.Ticket.TotalToWinAmount = null;
                    $('#ARCalculate_0').removeAttr('disabled');
                    break;
            }
        }
        //$ticketService.Ticket.TotalToWinAmount = CommonFunctions.RoundNumber($ticketService.Ticket.TotalToWinAmount);
        $scope.AllFieldsFilled = checkAllFieldsFilled();
        if (wagerItem != null) wagerItem.AmountEntered = 'RiskAmt';
        $ticketService.CalculateTotalAmounts();
    };

    function calculateRiskAmount (wagerItem) {
        if (wagerItem == null || wagerItem.Type == "G") {
            switch ($wagerTypesService.Selected.id) {
                case $wagerTypesService.Ids.StraightBet:
                case $wagerTypesService.Ids.IfWinOnly:
                case $wagerTypesService.Ids.IfWinOrPush:
                    if (wagerItem == null) return false;
                    wagerItem.RiskAmt = CommonFunctions.RoundNumber(LineOffering.CalculateRiskAmtUsingWi(wagerItem));
                    break;
                case $wagerTypesService.Ids.Parlay:
                    return 0;
                case $wagerTypesService.Ids.Teaser:
                    var pc = TeaserFunctions.GetPayCard($ticketService.Ticket.TeaserInfo.teaserPayCards, ($ticketService.Ticket.KeepOpenPlay ? $ticketService.openPlaysVal.value + $ticketService.TotalWagers() : $ticketService.TotalWagers()));
                    if (pc != null) $ticketService.Ticket.TotalRiskAmount = CommonFunctions.RoundNumber(TeaserFunctions.CalculateRisk(pc, $ticketService.Ticket.TotalToWinAmount));
                    break;
            }
        }
        else {
            log('Pending contestant Risk calculation');
        }
        //$ticketService.Ticket.TotalRiskAmount = CommonFunctions.RoundNumber($ticketService.Ticket.TotalRiskAmount);
        $scope.AllFieldsFilled = checkAllFieldsFilled();
        if (wagerItem) wagerItem.AmountEntered = 'ToWinAmt';
        $ticketService.CalculateTotalAmounts();
        return true;
    };

    function loadSlipBanners () {
        $settingsService.GetSplitBanners();
    };

    function restoreChangedLine (game) {
        if (game.Loo.SpreadChanged) game.Loo.SpreadChanged = false;
        if (game.Loo.MoneyLineChanged) game.Loo.MoneyLineChanged = false;
        if (game.Loo.TeamTotalChanged) game.Loo.TeamTotalChanged = false;
        if (game.Loo.TotalPointsChanged) game.Loo.TotalPointsChanged = false;
        game.Changed = false;
        $rootScope.safeApply();
    };

    function loadMiniBlackJack (customer) {
        $customerService.ColchianLoginPlayer(customer.CustomerID, customer.Password).then(function (result) {
            if (!result.data.d.Data || result.data.d.Code != 0) {
                $scope.BlackJackVisible = false;
                return false;
            }
            var iframe = $('#bjFrame');
            if (iframe.length) {
                iframe.attr('src', SETTINGS.HorsesSite + "/BOSSWagering/Casino/InternetGaming/?SiteId=trdwd&fm=1&Responsive=1&stoken=" + result.data.d.Data);
                return false;
            }
            return true;
        });
    }

    function confirmApplyAmountToAll (index, amount) {
        if (_applyToAllMessageAlreadyShown ||
            $wagerTypesService.IsParlay() || $wagerTypesService.IsActionReverse() || $wagerTypesService.IsTeaser() || $wagerTypesService.IsAccumWager() ||
            $ticketService.Ticket.WagerItems.length == 1 ||
            amount <= 0) return false;
        var cnt = 0;
        var wi, i;
        for (i = 0; i < $ticketService.Ticket.WagerItems.length; i++) {
            wi = $ticketService.Ticket.WagerItems[i];
            if (wi.RiskAmt == 0 || wi.ToWinAmt == 0) {
                cnt++;
                break;
            }
        }
        if (cnt == 0) return false;
        _applyToAllMessageAlreadyShown = true;
        UI.Confirm($translatorService.Translate("APPLY_SAME_AMOUNT"), function () {
            for (i = 0; i < $ticketService.Ticket.WagerItems.length; i++) {
                wi = $ticketService.Ticket.WagerItems[i];
                wi.IsOk = true;
                $scope.SetWagerMaxLimits(wi);
                if (i != index) {
                    if (wi.Type == "G") {
                        if (!wi.FinalPrice || wi.FinalPrice < 0) {
                            $customerService.CustomerActionLogger('Apply same amount to all', 'At wager item index: ' + (i + 1) + '. Chosen Team:  ' + wi.ChosenTeamId + '. To win amount: ' + amount);
                            wi.ToWinAmt = amount;
                            wi.AmountEntered = 'ToWinAmt';
                            wi.RiskAmt = CommonFunctions.RoundNumber(LineOffering.CalculateRiskAmtUsingWi(wi));
                            if (wi.RiskMax < wi.RiskAmt) wi.IsOk = false;
                        }
                        else {
                            $customerService.CustomerActionLogger('Apply same amount to all', 'At wager item index: ' + (i + 1) + '. Chosen Team:  ' + wi.ChosenTeamId + '. Risk amount: ' + amount);
                            wi.RiskAmt = amount;
                            wi.AmountEntered = 'RiskAmt';
                            wi.ToWinAmt = CommonFunctions.RoundNumber(LineOffering.CalculateToWinAmtUsingWi(wi));
                            if (wi.ToWinMax < wi.ToWinAmt) wi.IsOk = false;
                        }
                    } else {
                        ContestFunctions.CalculateContestWagerAmount(wi, amount);
                        if (wi.Loo.MoneyLine < 0) {
                            $scope.WagerAmoutOnFocus('W', wi, true);
                            $customerService.CustomerActionLogger('Apply same amount to all', 'At wager item index: ' + (i + 1) + '. Chosen Team:  ' + wi.ChosenTeamId + '. Risk amount: ' + wi.RiskAmt);
                            if (wi.MaxWagerAmount < wi.RiskAmt) wi.IsOk = false;
                        }
                        else {
                            $scope.WagerAmoutOnFocus('R', wi, true);
                            $customerService.CustomerActionLogger('Apply same amount to all', 'At wager item index: ' + (i + 1) + '. Chosen Team:  ' + wi.ChosenTeamId + '. To win amount: ' + wi.ToWinAmt);
                            if (wi.MaxWagerAmount < wi.ToWinAmt) wi.IsOk = false;
                        }
                    }
                }
                if (wi.RiskAmt && wi.RiskAmt < $customerService.MinimumWagerAmt / 100) wi.IsOk = false;
            }
            $ticketService.CalculateTotalAmounts();
            _applyToAllMessageAlreadyShown = false;
            $scope.AllFieldsFilled = true;
            $rootScope.safeApply();
            $("#password").focus();
        });
        return true;
    };

    function contestWagerAmountOnFocus (wagerItem, fieldType) {
        var circle = wagerItem.MaxWagerLimit / 100;
        _riskMax = parseInt(circle);
        //var valueToUse = wagerItem.MaxWagerLimit / 100;
        if (fieldType == 'R') wagerItem.RiskPlaceHolder = $translatorService.Translate("Max") + ": " + CommonFunctions.FormatNumber(parseInt(circle), false, false);
        else wagerItem.WinPlaceHolder = $translatorService.Translate("Max") + ": " + CommonFunctions.FormatNumber(parseInt(circle), false, false);
    };

    loadSlipBanners();

    //#endregion

    $rootScope.$on('customerInfoLoaded', function () {
        $rootScope.SetPassword = !$customerService.Settings.WebRememberPassword;
        var bjRestriction = $scope.GetCustomerRestriction("MINIBJ");
        $scope.BlackJackVisible = false;
        if (bjRestriction == null) {
            $scope.BlackJackVisible = loadMiniBlackJack($customerService.Info);
            $scope.BlackJackVisible = true;
        }
    });

    $rootScope.$on('wagerTypeChanged', function () {
        $scope.RiskPlaceHolder = $translatorService.Translate("Risk_Amt");
        $scope.WinPlaceHolder = $translatorService.Translate("Win_Amt");
    });

    $rootScope.$on('SplitBanners', function () {
        if ($settingsService.SplitBanners.length == 0) return;
        $scope.Banners = $settingsService.SplitBanners;
    });

    $scope.FreePlayAction = function () {
        $customerService.CustomerActionLogger('freeplay', $ticketService.Ticket.UseFreePlay ? 'Free play selected' : 'Free play unseleected');
    }

    $scope.CancelAction = function (remove) {
        if ($ticketService.IsOpenPlays()) {
            window.location.href = "#/openBets";
            if ($rootScope.IsMobileDevice) $rootScope.menuTab = 2;
        }
        else $scope.ShowMenu();
        if (remove) $scope.RemoveAllWagerItems();
    };

    $scope.RemoveWagerItem = function (wagerItem) {
        $ticketService.ContinuePressed = false;
        $ticketService.IsMoverReady = false;
        if (wagerItem.Type == "G") {
            wagerItem.IsOk = true;
            $ticketService.RemoveGameWagerItem(wagerItem.Loo, wagerItem.WagerType, wagerItem.ControlCode.substring(2, 1), false).then(function () {
                if ($ticketService.Ticket.WagerItems.length == 0) $scope.CloseDialog();

            });
        }
        else {
            $ticketService.RemoveContestWagerItem(wagerItem.Loo).then(function () {
                if ($ticketService.Ticket.WagerItems.length == 0) $scope.CloseDialog();

            });
        }
        $scope.CloseAlertBox();
    };

    $scope.CalculateTotalRisk = function () {
        $ticketService.Ticket.RRTotalRiskAmount = $ticketService.Ticket.TotalRiskAmount * $scope.RoundRobinCombinations.TotalCombinations;
    }

    $scope.ShowWagerItemAmountFields = function () {
        return $wagerTypesService.Selected.id == $wagerTypesService.Ids.StraightBet ||
            $wagerTypesService.Selected.id == $wagerTypesService.Ids.IfWinOrPush ||
            $wagerTypesService.Selected.id == $wagerTypesService.Ids.IfWinOnly;
    };

    $scope.IsBuyingPoints = function (wi) {
        var length = wi.BuyPoints ? wi.BuyPoints.length - 1 : 0;
        return wi.BuyPoints && length > 0 && wi.SelectedLine && wi.SelectedLine.points != wi.BuyPoints[length].points;
    };

    $scope.GetBuyingPoints = function (wi) {
        if (!$scope.IsBuyingPoints(wi)) return 0;
        var length = wi.BuyPoints.length - 1;
        return Math.abs(wi.BuyPoints[length].points - wi.SelectedLine.points);
    };

    $scope.GetGameWagerItemInfo = function (wi, info) {
        var useFinalLine = $ticketService.Ticket.Posted();
        var wagerType = $wagerTypesService.Selected;
        var i = wi.ControlCode != null && typeof wi.ControlCode != "undefined" ? parseInt(wi.ControlCode.substring(1, 2)) : 1;
        var wt = wi.ControlCode != null && typeof wi.ControlCode != "undefined" ? wi.ControlCode.substring(0, 1) : "L";
        var isDraw = wt == "M" && i == 3;
        var retStr = "";
        var pointsBought = 0.0;
        var contestDesc = "";
        switch (info) {
            case "rot":
                return i == 1 ? (!wi.Loo.Team1RotNum ? '' : wi.Loo.Team1RotNum) : (!wi.Loo.Team2RotNum ? '' : wi.Loo.Team2RotNum);
            case "teamName":
                if (wt == "L") return $translatorService.Translate(wi.Loo.Team1ID) + " / " + $translatorService.Translate(wi.Loo.Team2ID);
                else if (isDraw) return $translatorService.Translate("DRAW") + " (" + $translatorService.Translate(wi.Loo.Team1ID) + " " + $translatorService.Translate("VS") + " " + $translatorService.Translate(wi.Loo.Team2ID) + ")";
                else if (wi.Loo.Team1ID || wi.Loo.Team2ID) return i == 1 || i == 3 ? $translatorService.Translate(wi.Loo.Team1ID) : $translatorService.Translate(wi.Loo.Team2ID);
                else return $translatorService.Translate(wi.Loo.ChosenTeamID);
            case "period":
                var periodDesc = wi.Loo.PeriodDescription;
                if (wi.Loo.SportType != null && wi.Loo.SportSubType != null && ($.trim(wi.Loo.SportType) == "Baseball" || $.trim(wi.Loo.SportType) == "Basketball")
                    && $.trim(wi.Loo.SportSubType) == "SeriesPrices") {
                    periodDesc = "Series";
                }
                return $translatorService.Translate(periodDesc);
            case "sport":
                return $translatorService.Translate(wi.Loo.SportType);
            case "subSport":
                return $translatorService.Translate(wi.Loo.SportSubType);
            case "contestType":
                if (wi.Loo != null && wi.Loo.CategoryInfo != null) {
                    if (wi.Loo.CategoryInfo.ContestType != null) {
                        contestDesc += $.trim(wi.Loo.CategoryInfo.ContestType);
                    }
                }
                return $translatorService.Translate(contestDesc);
            case "subContestType":
                if (wi.Loo != null && wi.Loo.CategoryInfo != null) {
                    if (wi.Loo.CategoryInfo.ContestType2 != null && $.trim(wi.Loo.CategoryInfo.ContestType2) != '.') {
                        contestDesc += ' ' + $.trim(wi.Loo.CategoryInfo.ContestType2);
                    }
                    if (wi.Loo.CategoryInfo.ContestType3 != null && $.trim(wi.Loo.CategoryInfo.ContestType3) != '.') {
                        contestDesc += ' ' + $.trim(wi.Loo.CategoryInfo.ContestType3);
                    }
                }
                return $translatorService.Translate(contestDesc);
            case "contestantName":
                var contestantName = "";
                if (wi.Loo != null) {
                    contestantName = $.trim(wi.Loo.ContestantName);
                }
                return $translatorService.Translate(contestantName);
            case "S":
                var origSpread = wi.Line == null ? 0 : wi.Line;
                var finalSpread = useFinalLine ? wi.FinalLine : (wi.SelectedLine == null || wi.SelectedLine.points == null ? wi.FinalLine : wi.SelectedLine.points);

                if (wagerType.id != $wagerTypesService.Ids.Teaser) pointsBought = Math.abs(Math.abs(origSpread) - Math.abs(finalSpread));
                else pointsBought = 0;
                if (wi.HalfPointAdded && pointsBought > 0) pointsBought -= 0.5;
                //if (useFinalLine)
                  return LineOffering.FormatSpreadOffer(wi.Loo, i, true, true, true, wi.PriceType, wi, wagerType, pointsBought);
                //else return LineOffering.FormatSpreadOffer(wi.Loo, i, true, true, true, wi.PriceType, null, wagerType, pointsBought);
            case "M":
                //if (useFinalLine)
                return (isDraw ? $translatorService.Translate("DRAW") + " " : "") + LineOffering.FormatMoneyLineOffer(wi.Loo, i, true, true, wi.PriceType, wi);
                //else return (isDraw ? $translatorService.Translate("DRAW") + " " : "") + LineOffering.FormatMoneyLineOffer(wi.Loo, i, true, true, wi.PriceType, null, wagerType);
            case "MO":
                if (wi.FinalPrice == 0) {
                    retStr = "";
                }
                if (wi.FinalPrice >= 0) retStr += "+";
                retStr += wi.FinalPrice;
                return (isDraw ? $translatorService.Translate("DRAW") + " " : "") + retStr;
            case "L":
                var origTotalPoints = wi.Line == null ? 0 : wi.Line;
                var finalTotal = useFinalLine ? wi.FinalLine : (wi.SelectedLine == null || wi.SelectedLine.points == null ? wi.FinalLine : wi.SelectedLine.points);

                if (wagerType.id != $wagerTypesService.Ids.Teaser) pointsBought = Math.abs(Math.abs(origTotalPoints) - Math.abs(finalTotal));
                else pointsBought = 0;
                if (wi.HalfPointAdded && pointsBought > 0) pointsBought -= 0.5;

                //if (useFinalLine)
                return LineOffering.FormatTotalOffer(wi.Loo, i, true, true, true, wi.PriceType, wi, wagerType, pointsBought);
                //else return LineOffering.FormatTotalOffer(wi.Loo, i, true, true, true, wi.PriceType, null, wagerType, pointsBought);
            case "LO":
                pointsBought = Math.abs(Math.abs(wi.OrigTotalPoints) - Math.abs(wi.AdjTotalPoints));
                if (wi.HalfPointAdded && pointsBought > 0) pointsBought -= 0.5;
                retStr += LineOffering.FormatAhTotal(wi.AdjTotalPoints + (wi.TotalPointsOU == "O" ? wi.TeaserPoints * -1 : wi.TeaserPoints), ",", true, pointsBought);
                retStr += "  ";
                if (!wi.AdjTotalPoints || wi.AdjTotalPoints == 0) {
                    retStr = "";
                    break;
                }
                if (wi.FinalPrice >= 0) {
                    retStr += "+";
                }
                retStr += wi.FinalPrice;
                return retStr;
            case "E":
                //if (useFinalLine)
                return LineOffering.GetTeamTotalsOffer(wi.Loo, i, true, true, true, wagerType.name, wi.PriceType, wi);
                //else return LineOffering.GetTeamTotalsOffer(wi.Loo, i, true, true, true, wagerType.name, wi.PriceType, null);
            case "C":
                return CommonFunctions.GetGameInfo(wi, info);
            case "OU":
                if (wt == "L") return i == 1 ? $translatorService.Translate("OVER") : $translatorService.Translate("UNDER");
                else if (wt == "E") return i == 1 || i == 2 ? $translatorService.Translate("OVER") : $translatorService.Translate("UNDER");
                break;
            case "risk":
                return CommonFunctions.RoundNumber(wi.RiskAmt);
            case "toWin":
                return CommonFunctions.RoundNumber(wi.ToWinAmt);
            case "SO":
                pointsBought = Math.abs(Math.abs(wi.OrigSpread) - Math.abs(wi.AdjSpread));
                if (wi.HalfPointAdded && pointsBought > 0) pointsBought -= 0.5;
                retStr = LineOffering.FormatAhSpread(wi.AdjSpread + wi.TeaserPoints, ",", true, pointsBought);
                if (wi.FinalPrice == 0) {
                    retStr = "";
                }
                if (wi.FinalPrice >= 0) retStr += "+";
                retStr += wi.FinalPrice;
                return retStr;
        }
        return "";
    };

    $scope.GetWagerItemTitle = function (wagerItem) {
        if (wagerItem.Type == "G") {
            return $scope.GetGameWagerItemInfo(wagerItem, 'rot') + " " + $scope.GetGameWagerItemInfo(wagerItem, 'teamName');
        } else {
            return wagerItem.Loo.RotNum + " " + ContestFunctions.GetShortContestDescription(wagerItem);
        }
    };

    $scope.GetContestantLine = function (wagerItem) {
        return ContestFunctions.GetContestantName(wagerItem.Loo, wagerItem) + " " + ContestFunctions.GetContestantLine(wagerItem.Loo, "A");
    };

    $scope.SubString = function (str, beg, end) {
        return CommonFunctions.SubString(str, beg, end);
    };

    $scope.SetWagerMaxLimits = function (wagerItem) {
        var tempLimit = wagerItem.MaxWagerLimit / 100;
        var finalPrice = (wagerItem.selectedLine != null) ? parseInt(wagerItem.selectedLine.cost) : wagerItem.FinalPrice;
        wagerItem.RiskMax = $ticketService.CalculateMaxRiskAmount(wagerItem);
        wagerItem.ToWinMax = CommonFunctions.RoundNumber(LineOffering.CalculateToWinAmtUsingWi(null, tempLimit, wagerItem.PriceType, finalPrice));
    };

    $scope.WagerAmoutOnFocus = function (fieldType, wagerItem, isContest, isReverse) {
        var accumWt = false;
        if (wagerItem == null) {
            wagerItem = $ticketService.Ticket.WagerItems[$ticketService.Ticket.WagerItems.length - 1];
            accumWt = true;
        }
        if (isContest) return contestWagerAmountOnFocus(wagerItem, fieldType);
        var finalPrice = (wagerItem.selectedLine != null) ? parseInt(wagerItem.selectedLine.cost) : wagerItem.FinalPrice;
        var tempLimit = wagerItem.MaxWagerLimit / 100;
        var lowestWager = wagerItem;
        if ($ticketService.Ticket.WagerItems.length > 1 && $wagerTypesService.IsParlay() || $wagerTypesService.IsTeaser() || $wagerTypesService.IsActionReverse()) {
            $ticketService.Ticket.WagerItems.forEach(function (wager) {
                if (wager.MaxWagerLimit < lowestWager.MaxWagerLimit) {
                    lowestWager = wager;
                    tempLimit = (lowestWager.MaxWagerLimit ? lowestWager.MaxWagerLimit : 0) / 100;
                }
            });
        } else tempLimit = wagerItem.MaxWagerLimit / 100;
        if (isReverse) {
            $scope.ReversePlaceHolder = $translatorService.Translate("Max") + ": " + CommonFunctions.FormatNumber(tempLimit, false, false);
            _riskMax = tempLimit;
            return false;
        }
        var pc;
        //Pending round robins
        if (wagerItem == null) return false;
        if (fieldType == "R") {
            $scope.WinPlaceHolder = $translatorService.Translate("Win_Amt");
            if (finalPrice < 0 || $wagerTypesService.IsTeaser()) {
                if ($wagerTypesService.IsTeaser() && ($ticketService.TotalWagers() + ($ticketService.Ticket.KeepOpenPlay ? $ticketService.openPlaysVal.value : 0) == 2)) {
                    _winMax = CommonFunctions.RoundNumber(tempLimit, false);
                    $scope.WinPlaceHolder = $translatorService.Translate("Max") + ": " + CommonFunctions.FormatNumber(_winMax, false, false);
                    pc = TeaserFunctions.GetPayCard($ticketService.Ticket.TeaserInfo.teaserPayCards, ($ticketService.Ticket.KeepOpenPlay && $ticketService.openPlaysVal ? $ticketService.openPlaysVal.value + $ticketService.TotalWagers() : $ticketService.TotalWagers()));
                    if (pc != null) {
                        $scope.RiskPlaceHolder = $translatorService.Translate("Max") + ": " + CommonFunctions.RoundNumber(TeaserFunctions.CalculateRisk(pc, _winMax));
                    }
                    return false;
                }
                if ($wagerTypesService.IsStraightBet() || $wagerTypesService.IsIfBet()) {
                    tempLimit = $ticketService.CalculateMaxRiskAmount(wagerItem, tempLimit);
                } else if ($wagerTypesService.IsTeaser()) {
                    if (tempRiskLimit > tempLimit) tempLimit = tempRiskLimit;
                    pc = TeaserFunctions.GetPayCard($ticketService.Ticket.TeaserInfo.teaserPayCards, $ticketService.Ticket.WagerItems.length);
                    if (pc != null) {
                        var tempRiskLimit = TeaserFunctions.CalculateRisk(pc, tempLimit);
                        if (tempRiskLimit > tempLimit) tempLimit = tempRiskLimit;
                    }
                }
            } else if ($wagerTypesService.IsStraightBet() || $wagerTypesService.IsIfBet())
                tempLimit = wagerItem.MaxWagerLimit / 100;
            _riskMax = CommonFunctions.RoundNumber(tempLimit, false);
            if (accumWt) $scope.RiskPlaceHolder = $translatorService.Translate("Max") + ": " + CommonFunctions.FormatNumber(_riskMax, false, false);
            else wagerItem.RiskPlaceHolder = $translatorService.Translate("Max") + ": " + CommonFunctions.FormatNumber(_riskMax, false, false);
        }
        else {
            $scope.RiskPlaceHolder = $translatorService.Translate("Risk_Amt");
            if (finalPrice > 0 || $wagerTypesService.IsTeaser()) {
                if ($wagerTypesService.IsStraightBet() || $wagerTypesService.IsIfBet()) {
                    if (wagerItem != null) {
                        tempLimit = wagerItem.MaxWagerLimit / 100;
                        tempLimit = CommonFunctions.RoundNumber(LineOffering.CalculateToWinAmtUsingWi(null, tempLimit, 'A'/*wagerItem.PriceType*/, finalPrice));
                    }
                }
                else if ($wagerTypesService.IsParlay()) {
                    if ($scope.RoundRobinOptions.Selected == null || $scope.RoundRobinOptions.Selected.value == 0) {
                        tempLimit = ParlayFunctions.CalculateParlayToWinAmt($scope.WiAry, $scope.ParlayWebInfo, ParlayFunctions.GetPayCardMaxPayout($ticketService.Ticket.WagerItems.length, $scope.ParlayWebInfo, tempLimit), tempLimit);
                    } else {
                        var rrValue = 0;
                        if ($scope.RoundRobinOptions.selected != null && $scope.RoundRobinOptions.selected.value != null)
                            rrValue = $scope.RoundRobinOptions.selected.value;
                        var rrValues = ParlayFunctions.CalculateRoundRobinToWin($ticketService.Ticket.WagerItems.length, $scope.ParlayWebInfo, tempLimit, $scope.RoundRobinOptions.Selected.value);
                        tempLimit = rrValues.ToWinAmt;
                    }
                }
                else if ($wagerTypesService.IsTeaser())
                    pc = TeaserFunctions.GetPayCard($ticketService.Ticket.TeaserInfo.teaserPayCards, $ticketService.Ticket.WagerItems.length);
                if (pc != null) {
                    var tempWinLimit = TeaserFunctions.CalculateToWin(pc, tempLimit);
                    if (tempWinLimit > tempLimit) tempLimit = tempWinLimit;
                }
            }
            _winMax = CommonFunctions.RoundNumber(tempLimit, false);
            if (accumWt) $scope.WinPlaceHolder = $translatorService.Translate("Max") + ": " + CommonFunctions.FormatNumber(_winMax, false, false);
            else wagerItem.WinPlaceHolder = $translatorService.Translate("Max") + ": " + CommonFunctions.FormatNumber(_winMax, false, false);
        }


        return false;
    };

    $scope.WagerAmountOnBlur = function (fieldType, wagerItem, idx) {
        //Allan. On blur se llama 2 veces, para que no pase sería mejor pasar todo eso a directivas y aligerar peso de procesos
        if (_onBlurCalls > 0) {
            _onBlurCalls = 0;
            return false;
        }
        _onBlurCalls++;
        var amt;
        if ($wagerTypesService.IsAccumWager()) {
            amt = (fieldType == "R" ? $ticketService.Ticket.TotalRiskAmount : $ticketService.Ticket.TotalToWinAmount);
            $ticketService.Ticket.WagerItems.forEach(function (wagerItem) {
                if (amt > 0) $customerService.CustomerActionLogger("Amount Log", "Amount: " + amt + " / Field type: " + (fieldType == "R" ? "Risk" : "To win") + " / SportSubType: " + wagerItem.Loo.SportSubType + " / Chosen team: " + wagerItem.ChosenTeamId + " / WagerType: " + $wagerTypesService.Selected.id + " / ControlCode: " + wagerItem.ControlCode);
            });
        } else {
            amt = (fieldType == "R" ? wagerItem.RiskAmt : wagerItem.ToWinAmt);
            if (amt > 0) {
                if (wagerItem.Type == "G") $customerService.CustomerActionLogger("Amount Log", "Amount: " + amt + " / Field type: " + (fieldType == "R" ? "Risk" : "To win") + " / SportSubType: " + wagerItem.Loo.SportSubType + " / Chosen team: " + wagerItem.ChosenTeamId + " / WagerType: " + $wagerTypesService.Selected.id + " / ControlCode: " + wagerItem.ControlCode);
                else $customerService.CustomerActionLogger("Amount Log", "Amount: " + amt + " / Field type: " + (fieldType == "R" ? "Risk" : "To win") + " / Contest Type: " + wagerItem.ContestType2 + '  ' + wagerItem.ContestType3 + " / Contestant: " + wagerItem.Loo.ContestantName);
            }
            if (wagerItem.RiskAmt && wagerItem.RiskAmt < $customerService.MinimumWagerAmt / 100) wagerItem.IsOk = false;
        }
        if ($wagerTypesService.IsIfBet()) return false;
        var accumWt = false;
        if (wagerItem == null) {
            wagerItem = $ticketService.Ticket.WagerItems[0];
            accumWt = true;
        }
        if (wagerItem == null) return false;
        if (!accumWt) confirmApplyAmountToAll(idx, amt);
        return false;
    };

    $scope.WagerArOnBlur = function () {

        //Allan. On blur se llama 2 veces, para que no pase sería mejor pasar todo eso a directivas y aligerar peso de procesos
        if (_onBlurCalls > 0) {
            _onBlurCalls = 0;
            return;
        };
        _onBlurCalls++;
        if ($ticketService.Ticket.ArAmount > 0) {
            $ticketService.Ticket.WagerItems.forEach(function (wagerItem) {
                $customerService.CustomerActionLogger("Amount Log", "Amount: " + $ticketService.Ticket.ArAmount + " / Action Reverse" + " / Sport SubType: " + wagerItem.Loo.SportSubType + " / Chosen team: " + wagerItem.ChosenTeamId + " / WagerType: " + $wagerTypesService.Selected.id + " / ControlCode: " + wagerItem.ControlCode);
            });
        }
    }
  
    $scope.BuyPointsOnChange = function (wagerItem) {
        if (wagerItem != null) {
            wagerItem.RiskAmt = 0;
            wagerItem.ToWinAmt = 0;
            if (wagerItem.SelectedLine != null) {
                var cost = CommonFunctions.RoundNumber(wagerItem.SelectedLine.cost);
                var points = CommonFunctions.RoundNumber(wagerItem.SelectedLine.points);
                wagerItem.FinalPrice = cost;
                wagerItem.FinalLine = points;
                $customerService.CustomerActionLogger("Buy Points", $scope.GetWagerItemTitle(wagerItem) + " " + points + " " + cost + " for " + $scope.GetGameWagerItemInfo(wagerItem, 'period'));
            }
            if ($wagerTypesService.IsParlay()) {
                var wagersArray = [];
                wagersArray.push.apply(wagersArray, $ticketService.Ticket.OpenWagerItems);
                wagersArray.push.apply(wagersArray, $ticketService.Ticket.WagerItems);
                $ticketService.Ticket.TotalToWinAmount = CommonFunctions.RoundNumber(ParlayFunctions.CalculateParlayToWinAmt(wagersArray, $ticketService.Ticket.ParlayInfo, ParlayFunctions.GetPayCardMaxPayout(wagersArray.length, $ticketService.Ticket.ParlayInfo, $ticketService.Ticket.TotalRiskAmount), $ticketService.Ticket.TotalRiskAmount));
            }
        }
        else {
            $ticketService.Ticket.RRTotalRiskAmount = 0;
            $ticketService.Ticket.TotalRiskAmount = 0;
            $ticketService.Ticket.TotalToWinAmount = 0;
        }
    };

    $scope.AllowBuyPoints = function (wagerItem) {
        var firstCondition = !$ticketService.Ticket.Posted() &&
            wagerItem.BuyPoints &&
            (wagerItem.Loo.SportType == 'Football' || wagerItem.Loo.SportType == 'Basketball') &&
            wagerItem.Loo.PreventPointBuyingFlag != 'Y' &&
            wagerItem.Loo.PeriodNumber == 0 &&
            ($wagerTypesService.IsParlay() || $wagerTypesService.IsStraightBet());
        return firstCondition;
    };
  
    $scope.WagerAmountOnChange = function (wagerItem, fieldType, index) {
        if (wagerItem) restoreChangedLine(wagerItem);
        //if ($wagerTypesService.IsTeaser() || $wagerTypesService.IsParlay() || $wagerTypesService.IsActionReverse()) $scope.wagerTypesServiceView.WagerErrorList = [];
        if (wagerItem != null && wagerItem.Type == "G") {
            if (fieldType == "R") {
                if (_riskMax < wagerItem.RiskAmt) {
                    wagerItem.IsOk = false;
                    $customerService.CustomerActionLogger("Amount Exceeded", "Amount: " + wagerItem.RiskAmt + " / Risk max: " + _riskMax + " / SportSubType: " + wagerItem.Loo.SportSubType
                        + " / Chosen team: " + wagerItem.ChosenTeamId + " / WagerType: " + $wagerTypesService.Selected.id + " / ControlCode: " + wagerItem.ControlCode);

                }
                else {
                    wagerItem.IsOk = true;
                    calculateToWinAmount(wagerItem, index);
                }

            }
            else {
                if (_winMax < wagerItem.ToWinAmt) {
                    wagerItem.IsOk = false;
                    $customerService.CustomerActionLogger("Amount Exceeded", "Amount: " + wagerItem.RiskAmt + " / To win max: " + _winMax + " / SportSubType: " + wagerItem.Loo.SportSubType
                        + " / Chosen team: " + wagerItem.ChosenTeamId + " / WagerType: " + $wagerTypesService.Selected.id + " / ControlCode: " + wagerItem.ControlCode);
                }
                else {
                    wagerItem.IsOk = true;
                    calculateRiskAmount(wagerItem, index);
                }
            }
            return;
        }
        else if (wagerItem == null || wagerItem.Type == "G") {
          $ticketService.Ticket.WagerItems.forEach(function (w) {
            w.Changed = false;
            w.IsOk = true;
          });
            if (fieldType == "R") {
                if ($wagerTypesService.IsTeaser() && ($ticketService.TotalWagers() + ($ticketService.openPlaysVal.value < 999 ? $ticketService.openPlaysVal.value : 0)) == 2) {
                    calculateToWinAmount(wagerItem, index);
                    if (_winMax < $ticketService.Ticket.TotalToWinAmount) {
                        if (wagerItem == null) $ticketService.Ticket.WagerItems[0].IsOk = false;
                        else wagerItem.IsOk = false;
                        if (wagerItem) $customerService.CustomerActionLogger("Amount Exceeded", "Amount: " + $ticketService.Ticket.TotalToWinAmount + " / To win max: " + _winMax + " / SportSubType: " + wagerItem.Loo.SportSubType
                            + " / Chosen team: " + wagerItem.ChosenTeamId + " / WagerType: " + $wagerTypesService.Selected.id + " / ControlCode: " + wagerItem.ControlCode);
                    }
                    else {
                        if (wagerItem == null) $ticketService.Ticket.WagerItems[0].IsOk = true;
                        else wagerItem.IsOk = true;
                    }
                    return;
                }
                else if (_riskMax < $ticketService.Ticket.TotalRiskAmount) {
                    if (wagerItem == null) $ticketService.Ticket.WagerItems[0].IsOk = false;
                    else wagerItem.IsOk = false;
                    if (wagerItem) $customerService.CustomerActionLogger("Amount Exceeded", "Amount: " + $ticketService.Ticket.TotalRiskAmount + " / Risk max: " + _riskMax + " / SportSubType: " + wagerItem.Loo.SportSubType
                        + " / Chosen team: " + wagerItem.ChosenTeamId + " / WagerType: " + $wagerTypesService.Selected.id + " / ControlCode: " + wagerItem.ControlCode);
                }
                else {
                    if (wagerItem == null) $ticketService.Ticket.WagerItems[0].IsOk = true;
                    else wagerItem.IsOk = true;
                    calculateToWinAmount(wagerItem, index);
                }
            } else {
                if (_winMax < $ticketService.Ticket.TotalToWinAmount) {
                    if (wagerItem == null) $ticketService.Ticket.WagerItems[0].IsOk = false;
                    else wagerItem.IsOk = false;
                    if (wagerItem)
                        $customerService.CustomerActionLogger("Amount Exceeded", "Amount: " + $ticketService.Ticket.TotalToWinAmount + " / To win max: " + _winMax + " / SportSubType: " + wagerItem.Loo.SportSubType
                            + " / Chosen team: " + wagerItem.ChosenTeamId + " / WagerType: " + $wagerTypesService.Selected.id + " / ControlCode: " + wagerItem.ControlCode);
                }
                else {
                    if (wagerItem == null) $ticketService.Ticket.WagerItems[0].IsOk = true;
                    else wagerItem.IsOk = true;
                    calculateRiskAmount(wagerItem, index);
                }
            }
            return;
        }
        else {
            wagerItem.IsOk = true;
            ContestFunctions.CalculateContestWagerAmount(wagerItem);
            if (wagerItem.Loo.MoneyLine < 0) {
                if (_riskMax < wagerItem.ToWinAmt) wagerItem.IsOk = false;
                else $ticketService.CalculateTotalAmounts();

            }
            else if (_riskMax < wagerItem.RiskAmt) wagerItem.IsOk = false;
            else $ticketService.CalculateTotalAmounts();
        }
    };

    $scope.ArWagerAmountOnChange = function () {
      $ticketService.Ticket.WagerItems.forEach(function (w) {
        w.Changed = false;
        w.IsOk = true;
      });
        if ($ticketService.Ticket.ArAmount > _riskMax) {
        $ticketService.Ticket.WagerItems[0].IsOk = false;
            return false;
      } else $ticketService.Ticket.WagerItems[0].IsOk = true;

        if ($ticketService.Ticket.WagerItems.length != 2) return false;

        var wagerItem1 = $ticketService.Ticket.WagerItems[0];
        var wagerItem2 = $ticketService.Ticket.WagerItems[1];

        if (wagerItem1.FinalPrice < 0) {
            wagerItem1.ToWinAmt = $ticketService.Ticket.ArAmount;
            wagerItem1.RiskAmt = 0;
        } else {
            wagerItem1.ToWinAmt = 0;
            wagerItem1.RiskAmt = $ticketService.Ticket.ArAmount;
        }
        LineOffering.CalculateWiWagerAmount(wagerItem1);

        if (wagerItem2.FinalPrice < 0) {
            wagerItem2.ToWinAmt = $ticketService.Ticket.ArAmount;
            wagerItem2.RiskAmt = 0;
        } else {
            wagerItem2.ToWinAmt = 0;
            wagerItem2.RiskAmt = $ticketService.Ticket.ArAmount;
        }
        LineOffering.CalculateWiWagerAmount(wagerItem2);

        var maxRisk1 = LineOffering.CalculateMaxRisk(false, $ticketService.Ticket.WagerItems);
        var maxRisk2 = LineOffering.CalculateMaxRisk(true, $ticketService.Ticket.WagerItems);

        var totalRisk = maxRisk1 + maxRisk2;
        var totalToWin = (wagerItem1.ToWinAmt * 2) + (wagerItem2.ToWinAmt * 2);
        $ticketService.Ticket.TotalRiskAmount = totalRisk;
        $ticketService.Ticket.TotalToWinAmount = totalToWin;

        return true;
    };

    //ALLAN OPEN PLAYS
    window.WagerAmountOnChange = function (wagerItem, fieldType, index) {
        $scope.WagerAmountOnChange(wagerItem, fieldType, index);
    };

    $scope.RemoveAllWagerItems = function () {
        $ticketService.ContinuePressed = false;
        $ticketService.RemoveAllWagerItems().then(function () {
            $("#arWagerAmount").val('');
            $("#totalRisk").val('');
            $("#totalToWin").val('');
            $rootScope.ErrorMessage = "";
            $ticketService.ResetAmounts();
            $ticketService.Ticket.OpenWagerItems = [];
            $ticketService.openPlaysVal = $ticketService.OpenPlayDropDown[0];
            $ticketService.GetWagerPicks();
            $ticketService.Ticket.KeepOpenPlay = false;
            $ticketService.IsMoverReady = false;
            $scope.RiskPlaceHolder = $translatorService.Translate("Risk_Amt");
            $scope.WinPlaceHolder = $translatorService.Translate("Win_Amt");
            $ticketService.StartNewTicket();
        });
    };
  
    $scope.ProcessTicket = function () {
        if ($rootScope.IsMobileDevice && !$scope.IsSafeToPostTicket()) return;
        $ticketService.ContinuePressed = false;
        if ($rootScope.SetPassword || $scope.IsMover()) {
            $encryptionService.RequestToken().then(function (resp) {
                var key = resp.data.d.Message;
                var iv = resp.data.d.Message;
                var encryptedMessage = CryptoJS.AES.encrypt($scope.ticketServiceView.Ticket.Password, CryptoJS.enc.Utf8.parse(key),
                {
                    keySize: 128 / 8,
                    iv: CryptoJS.enc.Utf8.parse(iv),
                    mode: CryptoJS.mode.CBC,
                    padding: CryptoJS.pad.Pkcs7
                });
                $encryptionService.CustomerPassCheck($customerService.Info.CustomerID.toString().trim(), encryptedMessage.ciphertext.toString(CryptoJS.enc.Base64), key).then(function (response) {
                    if (response.data.d.Code == 0) {
                        $ticketService.ProcessTicket();
                        $scope.CloseAlertBox();
                        $scope.ticketServiceView.Ticket.Password = "";
                    } else {
                        $rootScope.ErrorMessage = $translatorService.Translate("Incorrect_password");
                        setTimeout(function () {
                            $('#betSlipBody').animate({
                                scrollTop: 9999
                            }, 'fast');
                        }, 200);
                    }
                });
            });
        }
        else {
            $scope.CloseAlertBox();
            $scope.ticketServiceView.Ticket.Password = "";
            $rootScope.RecentTicketProcessed = true;
            $ticketService.ProcessTicket();
            setTimeout(function () { $rootScope.RecentTicketProcessed = false; }, 3000);
        }
        $ticketService.IsMoverReady = false;
    };

    $scope.PreProcessTicket = function () {
        $ticketService.ContinuePressed = true;
        $ticketService.Continue();

    };

    $scope.IsSafeToPostTicket = function () {
        if ($ticketService.PlacingBet) return false;
        if (!$ticketService.Ticket.Any()) return false;
        var wagerItem, i;
        if ($wagerTypesService.IsAccumWager()) {
            for (i = 0; i < $ticketService.Ticket.WagerItems.length; i++) {
                wagerItem = $ticketService.Ticket.WagerItems[i];
                if (!wagerItem.IsOk) return false;
            }
            return CommonFunctions.IsNumeric($ticketService.Ticket.TotalToWinAmount) &&
                CommonFunctions.IsNumeric($ticketService.Ticket.TotalRiskAmount) &&
                $ticketService.Ticket.TotalToWinAmount > 0 &&
                $ticketService.Ticket.TotalRiskAmount > 0 &&
                $ticketService.Ticket.WagerItems.length > 0;
        } else {
            for (i = 0; i < $ticketService.Ticket.WagerItems.length; i++) {
                wagerItem = $ticketService.Ticket.WagerItems[i];
                if (!wagerItem.IsOk) return false;
                if ((!CommonFunctions.IsNumeric(wagerItem.RiskAmt) ||
                    !CommonFunctions.IsNumeric(wagerItem.ToWinAmt) ||
                    wagerItem.RiskAmt == 0 || wagerItem.ToWinAmt == 0) && wagerItem.Available === true) return false;
            }
        }
        return true;
    };

    $scope.IsWagerItemAmountDisabled = function (idx) {
        if (!$wagerTypesService.IsIfBet() || idx == 0 ||
            ($ticketService.Ticket.WagerItems[idx - 1].RiskAmt > 0 && $ticketService.Ticket.WagerItems[idx - 1].ToWinAmt > 0)) return false;

        for (var i = idx; i < $ticketService.Ticket.WagerItems.length; i++) {
            $ticketService.Ticket.WagerItems[i].RiskAmt = 0;
            $ticketService.Ticket.WagerItems[i].ToWinAmt = 0;
        }

        return true;
    };

    $scope.ShowPitchers = function (wagerItem) {
        return wagerItem.Loo.SportType == "Baseball" &&
            wagerItem.Loo.ListedPitcher1 &&
            wagerItem.Loo.ListedPitcher2 &&
            wagerItem.Loo.ListedPitcher1.length > 0 &&
            wagerItem.Loo.ListedPitcher1.length > 0;
    };

    $scope.ResetAmounts = function (wagerItem) {

        $ticketService.ResetAmounts(wagerItem);
        return true;
    };

    $scope.CloseAlertBox = function () {
        $rootScope.ErrorMessage = "";
        return true;
    };

    $scope.WagerChanged = function (wager) {
        return wager.Changed;
        /*if (wager.Type == "C") {
          return wager.Loo.LineChanged || wager.Changed;
        }
        var wagerSubType = wager.ControlCode.substring(0, 1);
        switch (wagerSubType) {
          case "M":
            return wager.Loo.MoneyLineChanged;
          case "S":
            return wager.Loo.SpreadChanged;
          case "L":
            return wager.Loo.TotalPointsChanged;
          case "E":
            return wager.Loo.TeamTotalChanged;
          default:
            return false;
        }*/
    };

    $scope.PitcherDetail = function (pitcher, flag) {
        if (!pitcher) return "";
        if (!$ticketService.Ticket.Posted()) return pitcher;
        return pitcher + " " + (flag ? $translatorService.Translate("Must start") : $translatorService.Translate("Action"));
    };

    $scope.AutoAcceptSwitch = function () {
        $settingsService.AutoAcceptSwitch(true);
        $customerService.CustomerActionLogger("Auto accept", $customerService.AutoAccept ? "Auto accept selected" : "Auto accept unseleected");
    };

    $scope.BannerActive = function (banner) {
        var activeFrom = new Date(banner.ActiveFrom);
        var activeTo = new Date(banner.ActiveTo);
        if (banner.Name == "LiveBetting" && !$scope.LiveBetting) return false;
        if (banner.Name == "LiveDealer" && !$scope.LiveDealer) return false;
        if (banner.Active && Date.now() >= activeFrom && Date.now() <= activeTo)
            return true;
        return false;

    };

    $scope.BannerAction = function (banner) {
        if (banner.Action != "") eval(banner.Action);
        if (banner.Link != "") window.open(banner.Link, '', 'width=590,height=610,left=0,top=0,resizable=yes,menubar=no,location=no,status=yes,scrollbars=yes');
    };

    $scope.CircledVal = function (wagerItem) {
        if (!wagerItem || wagerItem.Loo.Status != "I") return null;
        switch (wagerItem.WagerType) {

            case "S":
                return wagerItem.Loo.CircledMaxWagerSpreadType;
            case "M":
                return wagerItem.Loo.CircledMaxWagerMoneyLineType;
            case "L":
                return wagerItem.Loo.CircledMaxWagerTotalType;
            case "E":
                return wagerItem.Loo.CircledMaxWagerTeamTotalType;
            default: return null;
        }
    };

    $scope.ChangeMaximunPicks = function () {
        if (!$ticketService.IsOpenPlays()) {
            $scope.ResetAmounts(null);
        }
        if ($ticketService.openPlaysVal.value > 8) {
            $ticketService.Ticket.KeepOpenPlay = false;
            $ticketService.Ticket.AllowedWagerPicks.OpenPicks = $ticketService.openPlaysVal.value;
        } else {
            if ($ticketService.Ticket.RoundRobin.Options && $ticketService.Ticket.RoundRobin.Selected != $ticketService.Ticket.RoundRobin.Options[0]) {
                $ticketService.Ticket.RoundRobin.Selected = $ticketService.Ticket.RoundRobin.Options[0];
                UI.Notify($translatorService.Translate("OPEN SPOTS ARE ONLY AVAILABLE FOR SINGLE PARLAYS"), UI.Position.Top, UI.Position.Center, 200, UI.Type.Info);
            }
            $customerService.CustomerActionLogger("Select open spots", "Spots selected: " + $ticketService.openPlaysVal.value);
            $ticketService.Ticket.KeepOpenPlay = $ticketService.Ticket.AllowedWagerPicks.OpenPicks > $ticketService.TotalWagers();
            $ticketService.Ticket.AllowedWagerPicks.OpenPicks = $ticketService.openPlaysVal.value + $ticketService.TotalWagers();
        }
        if ($wagerTypesService.IsTeaser()) $ticketService.GetTeaserInfo($ticketService.Ticket.TeaserName);
        if ($wagerTypesService.IsParlay()) $ticketService.GetTeaserInfo(null);
    };

    $scope.TotalRiskFix = function (toRisk) {
        if ($wagerTypesService.IsParlay()) {
            var roundRobin = $ticketService.GetSelectedRoundRobin();
            if (roundRobin != null && roundRobin.value != 0) {
                var rrValues = ParlayFunctions.CalculateRoundRobinToWin($ticketService.Ticket.WagerItems, $ticketService.Ticket.ParlayInfo, $ticketService.Ticket.TotalRiskAmount, roundRobin.value);
                return toRisk * rrValues.PlayCount * 100;
            }
            else return toRisk * 100;
        } else return toRisk * 100;

    };

    $scope.BetCount = function () {
        if (!$wagerTypesService.Selected || typeof $ticketService.openPlaysVal === "undefined") return '';
        var totalWagerCount = $ticketService.TotalWagers() + $ticketService.openPlaysVal.value;
        var openvalue = $ticketService.openPlaysVal.value;
        $ticketService.Ticket.KeepOpenPlay = $ticketService.openPlaysVal.value > 8 ? false : true;
        if (!$ticketService.Ticket.KeepOpenPlay) {
            return $translatorService.Translate('TOTAL') + ': ' + $ticketService.TotalWagers() + ' '
            + $translatorService.Translate('TEAMS');
        }
        return $translatorService.Translate('TOTAL') + ': ' + totalWagerCount + ' '
            + $translatorService.Translate('TEAMS') + ' ' + openvalue + ' ' + $translatorService.Translate("OPEN");
    };

    $scope.OnRoundRobinChange = function () {
        $scope.ResetAmounts(null);
        $ticketService.OpenDisable = ($ticketService.Ticket.RoundRobin.Selected.name !== "SingleParlay" ? true : false);
        $customerService.CustomerActionLogger("Round Robin Changed ", $ticketService.Ticket.RoundRobin.Selected.name);
        var selectedRoundRobin = $ticketService.GetSelectedRoundRobin();
        if (selectedRoundRobin && selectedRoundRobin.value > 0) {
            var selectedRoundRobinName = selectedRoundRobin.name.replace($scope.Translate("Round Robin with"), "").replace(" ", "");
            $scope.RoundRobinCombinations = ParlayFunctions.GetRoundRobinCombinations(selectedRoundRobinName, $ticketService.TotalWagers());
        }

    };

    $scope.IsMover = function () {
        if ($customerService.GetCustomerRestriction('MOVER')) return true;
        return false;
    };

    $scope.ShowProcessButton = function () {
        if (!$scope.IsMover()) return true;
        if ($ticketService.ContinuePressed && !$rootScope.SetPassword) return true;
        else return $ticketService.IsMoverReady;
    };

    $scope.ShowPasswordInput = function () {
        if (!$scope.IsMover()) return $rootScope.SetPassword;
        return $ticketService.ContinuePressed && ($rootScope.SetPassword || $ticketService.IsMoverReady);
    };

    $scope.ShowContinue = function () {
        if (!$scope.IsMover()) return false;
        if (!$ticketService.ContinuePressed && ($rootScope.SetPassword || !$ticketService.IsMoverReady)) return true;
        else return false;
    };

    $scope.OpenBets = function () {
        if ($rootScope.IsMobileDevice) $rootScope.menuTab = 2;
        $rootScope.safeApply();
        $scope.CloseDialog();
    };

}]);

appModule.directive('formatNumber', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ctrl) {

            ctrl.$formatters.push(function (modelValue) {
                if (modelValue == 0) return "";
                else return modelValue;
            });
        }

    };
});