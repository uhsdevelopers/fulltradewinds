﻿var nonpostedCasinoplaysCtrl = appModule.controller(appModule.Controllers.nonPostedCasinoplays, ['$scope', '$rootScope', '$routeParams', '$customerService', function ($scope, $rootScope, $routeParams, $customerService) {
    $scope.npCasinoPlays = null;
    $scope.Init = function () {
        $scope.GetCustomerNonPostedCasinoPlays();
        $scope.CustomerId = $customerService.Info.CustomerID;
    };

    $scope.GetCustomerNonPostedCasinoPlays = function () {
        $customerService.GetCustomerNonPostedCasinoPlays().then(function (result) {
            $scope.npCasinoPlays = result.data.d.Data;
        });
    };

    $scope.FormatDateTime = function (acceptedDateTime, formatCode, timeZone, showFourDigitsYear) {
        return CommonFunctions.FormatDateTime(acceptedDateTime, formatCode, timeZone, showFourDigitsYear);
    };

    $scope.Init();
}]);