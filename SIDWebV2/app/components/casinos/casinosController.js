﻿var casinosCtrl = appModule.controller(appModule.Controllers.casinos, [
    '$scope', '$liveDealerService', function ($scope, $liveDealerService) {

      var siteLobby = "https://www.golivedealer.net/LiveDealer/LiveDealerLobby.aspx?data=";
      var windowOptions = "toolbar=no,location=no,directories=no,status=no, menubar=no,scrollbars=no,resizable=no,copyhistory=no,width=1024,height=720";
      var windowTitle = "";

      $scope.LoadLiveDealer = function () {
        $liveDealerService.RetrieveSecurityToken().then(function (result) {
          window.open(siteLobby + result.data.d.Data, windowTitle, windowOptions);

        });
      };

    }
]);