﻿var settingsCtrl = appModule.controller(appModule.Controllers.settings, [
    '$scope', '$rootScope', '$settingsService', '$customerService', '$translatorService', function ($scope, $rootScope, $settingsService, $customerService, $translatorService) {

      $scope.selectedSport = null;
      $scope.selectedHomePage = null;
      $scope.selectedBaseballAction = null;
      $scope.selectedLanguage = null;
      $scope.autoAccept = $customerService.AutoAccept;
      $scope.rememberPassword = $customerService.Settings && $customerService.Settings.WebRememberPassword;
      $scope.autoAccept = $customerService.GetCustomerRestriction('AUTOLINECH');
      $scope.Settings = {
        HomePages: [],
        BaseballActionTypes: [],
        TimeZones: null,
        Languages: null,
        MainSports: []
      };

      function loadSettings () {

        $settingsService.GetLanguages();
        $settingsService.GetAvailableHomePages();
        $settingsService.GetBaseballActionTypes();
        $settingsService.GetTimeZones();
        $settingsService.GetMainSports();

      };

      loadSettings();

      $rootScope.$on('Languages', function () {
        $scope.Settings.Languages = $settingsService.Languages.Files;
        var lang = $customerService.Settings && $customerService.Settings.WebLanguage ? $customerService.Settings.WebLanguage.trim() : "";
        for (var i = 0; $scope.Settings.Languages.length > i; i++) {
          if ($scope.Settings.Languages[i].Code === lang) {
            $scope.selectedLanguage = $scope.Settings.Languages[i];
            break;
          }
        }
      });

      $rootScope.$on('customerInfoLoaded', function () {
        $scope.autoAccept = $customerService.AutoAccept;
        $scope.rememberPassword = $customerService.Settings && $customerService.Settings.WebRememberPassword;
      });

      $rootScope.$on('HomePages', function () {
        $scope.Settings.HomePages = $settingsService.AvailableHomePages.pages;
        var homepage = $customerService.Settings && $customerService.Settings.WebHomePage ? $customerService.Settings.WebHomePage.trim() : "";
        for (var i = 0; $scope.Settings.HomePages.length > i; i++) {
          if ($scope.Settings.HomePages[i].name.replace(/ /g, '') === homepage) {
            $scope.selectedHomePage = $scope.Settings.HomePages[i];
            break;
          }
        }
      });

      $rootScope.$on('BaseballActions', function () {
        $scope.Settings.BaseballActionTypes = $settingsService.BaseballActionTypes.Names;
        var pitcher = $customerService.Settings && $customerService.Settings.WebPitcherOption ? $customerService.Settings.WebPitcherOption.trim() : "";
        for (var i = 0; $scope.Settings.BaseballActionTypes.length > i; i++) {
          if ($scope.Settings.BaseballActionTypes[i].name.trim() === pitcher) {
            $scope.selectedBaseballAction = $scope.Settings.BaseballActionTypes[i];
            break;
          }
        }
      });

      $rootScope.$on('TimeZones', function () {
        $scope.Settings.TimeZones = $settingsService.TimeZones;
      });

      $rootScope.$on('MainSports', function () {
        $scope.Settings.MainSports = $settingsService.MainSports.Sports;
        var sport = $customerService.Settings && $customerService.Settings.FavoriteSportSubType ? $customerService.Settings.FavoriteSportSubType.trim() : "";
        for (var i = 0; $scope.Settings.MainSports.length > i; i++) {
          if ($scope.Settings.MainSports[i].SportSubType === sport) {
            $scope.selectedSport = $scope.Settings.MainSports[i];
            break;
          }
        }

      });

      $scope.ChangeLanguage = function (language) {
        $scope.selectedLanguage = language;
        $rootScope.safeApply();
        $scope.SaveSettings();
      };

      $scope.SaveSettings = function () {
        $rootScope.SetPassword = !$scope.rememberPassword;
        $settingsService.SaveCustomerSettings(
             $scope.selectedLanguage.Code,
             $scope.selectedHomePage ? $scope.selectedHomePage.name.replace(/ /g, '') : "",
             $scope.selectedSport.SportType,
             $scope.selectedSport.SportSubType,
             $customerService.Settings.FavoriteTeamID,
             $scope.selectedBaseballAction ? $scope.selectedBaseballAction.name.trim() : "",
             $scope.rememberPassword,
             $customerService.Info.TimeZone).then();
        $scope.Customer.Settings.WebLanguage = $scope.selectedLanguage.Code;
        try {
          $customerService.CustomerActionLogger("Customer Settings Changed", "[language: " + $scope.selectedLanguage.Code + ", favorite team: " + $customerService.Settings.FavoriteTeamID + ", pitcher: " + $scope.selectedBaseballAction.name.trim() + ", remember password: " + $scope.rememberPassword + "]");
        } catch (e) {

        }

      };

      $rootScope.$on('SettingsSaved', function () {
        $translatorService.ChangeLanguage($scope.selectedLanguage.Code);
      });

      $scope.LangMaintenance = function () {
        window.open("app/components/settings/langMaintanence.html");
      };

      $scope.IsAutoAccept = function () {
        return $customerService.GetCustomerRestriction('AUTOLINECH');
      };

      $scope.SaveLearnedTexts = function () {
        $translatorService.SaveLearnedTexts();
      };

      $scope.AutoAcceptSwitch = function () {
        $settingsService.AutoAcceptSwitch(false);
      };

    }]);