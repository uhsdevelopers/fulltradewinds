﻿var LangMaintApp = angular.module('LangMaintApp', []);

LangMaintApp.factory('$JsonManagerService', ['$http', function ($http) {
  return {
    GetLanguages: function () {
      return $http.get('/Data/LangFiles.json').success(function (response) {
        return response;
      });
    },
    GetTranslations: function (langCode) {
      var rnd = Math.floor(Math.random() * 100000) + 1;
      return $http.get('/Data/Lang/' + langCode + '.json?v=' + rnd).success(function (response) {
        return response;
      });
    },
    SaveTranslations: function (lang, translations) {
      return $http({
        method: 'POST',
        dataType: 'json',
        headers: { "Content-Type": "application/json" },
        data: { "lang": lang, "json": translations },
        url: '../../../Services/TranslatorService.asmx/SaveTranslations'
      }).success(function (response) {
        return response;
      });
    }
  };
}]);

var langMaintCtrl = LangMaintApp.controller('langMaintCtrl', ['$scope', '$JsonManagerService', function ($scope, $JsonManagerService) {
  $scope.txtSearchFilter = null;

  main.ShowLoading();

  $JsonManagerService.GetLanguages().then(function (result) {
    $scope.LangFiles = result.data;
    $scope.LangFiles.Selected = $scope.LangFiles.Files[0];
    $scope.LoadLanguageTranslations();
  });

  $scope.ChangeLanguageFile = function (lng) {
    if ($scope.LangFiles.Selected.Modified === false) {
      $scope.LangFiles.Selected.Modified = false;
      $scope.LangFiles.Selected = lng;
      $scope.LoadLanguageTranslations();
    } else {
      BootstrapDialog.confirm($scope.LangFiles.Selected.Name + " file was modified, do you want to continue without saving?", function (result) {
        if (result) {
          $scope.LangFiles.Selected.Modified = false;
          $scope.LangFiles.Selected = lng;
          $scope.LoadLanguageTranslations();
          $rootScope.safeApply();
        }
      });
    }
  };

  $scope.LoadLanguageTranslations = function () {
    if ($scope.LangFiles.Selected) {
      main.ShowLoading();
      $JsonManagerService.GetTranslations($scope.LangFiles.Selected.Code).then(function (result) {
        $scope.Translations = new Array();
        angular.forEach(result.data, function (value, key) {
          $scope.Translations.push({ "key": key, "value": value, "status": "Original" });
        });
        main.HideLoading();
      });
    }
  };

  //$scope.SearchFilter = function (k, v) {

  //};

  $scope.AddTranslation = function () {
    $scope.Translations.push({ "key": "NEW", "value": "", "status": "New" });
    $scope.LangFiles.Selected.Modified = true;
    window.scrollTo(0, document.body.scrollHeight);
  };

  $scope.SaveTranslations = function () {
    var translationsToSave = "{";
    main.ShowLoading();
    angular.forEach($scope.Translations, function (trn) {
      if (translationsToSave.length > 1) translationsToSave += ",";
      translationsToSave += '"' + trn.key + '": "' + trn.value + '"';
    });
    translationsToSave += "}";

    $JsonManagerService.SaveTranslations($scope.LangFiles.Selected.Code, translationsToSave).then(function () {
      $scope.LangFiles.Selected.Modified = false;
      main.HideLoading();
    });

  };

  $scope.TranslationChanged = function (ev) {
    if (ev.Code !== 9 && ev.Code !== 13) {
      if (!$scope.LangFiles.Selected.Modified) $scope.LangFiles.Selected.Modified = true;
    }
  };

  $scope.ValidateTranslation = function (ev, trn) {
    if (!ev.shiftkey && !ev.metakey && !ev.ctrlkey && !ev.altkey && ev.key.length === 1) {
      trn.value += ev.key;
      ev.target.value = ev.target.value.replace(" ", "_").toUpperCase();
    }
  };

  $scope.RemoveTranslation = function (translation) {
    var pos = $scope.Translations.map(function (e) { return e.key; }).indexOf(translation.key);
    if (pos >= 0)
      $scope.Translations.splice(pos, 1);
  };

}]);

function main() { };

main.ShowLoading = function () {
  $.blockUI({
    message: 'Please Wait ...',
    css: {
      border: 'none',
      padding: '15px',
      backgroundColor: '#000',
      '-webkit-border-radius': '10px',
      '-moz-border-radius': '10px',
      opacity: .5,
      color: '#fff'
    }
  });
};

main.HideLoading = function (delay) {
  if (typeof delay == "undefined") delay = 100;
  setTimeout(function () {
    $.unblockUI();
  }, delay);
}