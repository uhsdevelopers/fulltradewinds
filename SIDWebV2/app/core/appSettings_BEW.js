﻿SETTINGS = {
  ConsoleActive: false,
    // SocketServers: ['https://m1.trdwd.ec/signalr', 'https://m1.trdwd.ec/signalr'],
  SocketServers: ['https://ws.trdwd.ec/signalr'],
  AgentSite: 'https://agent.trdwd.ec/Autologin',
  MobileSite: 'https://m.trdwd.ec/Autologin',
  RulesSite: 'https://rules.webtools.co.cr',
  LiveChatSite: 'https://support.trdwd.ec',
  LiveBettingSite: 'https://plive.trdwd.ec/live',
    //LiveBettingSite: 'https://plive.asi.phnserv.eu/betLobbyV2/',
  HorsesSite: 'https://racing.trdwd.ec',
  ShowContactInfo: false,
  ShowClassicButton: false,
  LiveChatAvailable: false,
  TextLearningModeOn: false,
  TranslationFileSufix: "_mn",
  ShowSearchBar: false,
  DefaultLanguage: 'TH',
  CurrencySign: '',
  DecimalPrecision: 3,
  MaxDenominator: 20,
  ShowInactiveSports: ["Soccer"],
  TrackTime: 10000
};