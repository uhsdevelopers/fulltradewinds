﻿TeaserFunctions = function() {};

TeaserFunctions.CalculateToWin = function (teaserPayCard, riskAmount) {
    var toWinAmount = 0;
    if (teaserPayCard != null) {
        toWinAmount = riskAmount * teaserPayCard.MoneyLine / teaserPayCard.ToBase;
    }
    return (toWinAmount * 100) / 100;
};

TeaserFunctions.CalculateRisk = function(teaserPayCard, toWinAmount) {
    var riskAmount = 0;
    if (teaserPayCard != null) {
        riskAmount = toWinAmount * teaserPayCard.ToBase / teaserPayCard.MoneyLine;
    }
    return (riskAmount * 100) / 100;
};

TeaserFunctions.GetPayCard = function(payCards, gamesPicked) {
    var pc = null;
    $.each(payCards, function(idx, spc) {
        if (spc.GamesPicked == gamesPicked) {
            pc = spc;
            return false;
        }
    });
    return pc;
};