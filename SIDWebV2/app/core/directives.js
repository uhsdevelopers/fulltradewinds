﻿/* angular functions */
appModule.directive('format', ['$filter', function ($filter) {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return;

            ctrl.$formatters.unshift(function () {
                if (ctrl.$modelValue == "") return "";
                else {
                    var f = $filter(attrs.format)(ctrl.$modelValue);
                    var pos = f.indexOf('.');
                    if (pos >= 0) f = f.substring(0, pos);
                    return f;
                }
            });

            ctrl.$parsers.unshift(function (viewValue) {
                if (viewValue == "" || parseInt(viewValue) == 0) return "";
                var plainNumber = parseInt(viewValue.replace(/[^\d|\-+|\.+]/g, ''));
                elem.val($filter('number')(plainNumber));
                return plainNumber;
            });
        }
    };
}]);

appModule.directive("formatNum", function () {
    return {
        restrict: "E",
        link: function (scope, element, attrs) {
            attrs.$observe('num', function (num) {
                var newNum;
                newNum = CommonFunctions.FormatNumber(num, typeof attrs.divide != "undefined");
                element.text(newNum);
            });

        }
    };
});

appModule.directive('myPostRepeatDirective', function () {
    return function (scope) {
        if (scope.$last) {
            scope.PeriodOrTeamTotalChanged(scope.sportOffering);
        }
    };
});

appModule.directive('ngRepeatEvents', function () {
    return function (scope) {
        if (scope.$first == true) {
            //$('#spinner').show();
        }
        else if (scope.$last == true) {
            menu.HideLoading();
        }
    };
});


appModule.directive('ngBlur', function () {
    return function (scope, elem, attrs) {
        elem.bind('blur', function () {
            scope.$apply(attrs.ngBlur);
        });
    };
});

appModule.directive('ngFocus', function () {
    return function (scope, elem) {
        elem.bind('focus', function () {
            $(window).scrollTop(0);
        });
    };
});

appModule.directive('dynamicPlaceholder', function () {
    return {
        restrict: 'A',
        link: function ($scope, element, attrs) {
            attrs.$observe('dynamicPlaceholder', function (value) {
                element.attr('placeholder', value);
            });
        }
    };
});

appModule.directive('formatNumber', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attr, ctrl) {

            ctrl.$formatters.push(function (modelValue) {
                if (!modelValue || modelValue == 0 || isNaN(modelValue)) return "";
                else return modelValue.toFixed ? modelValue.toFixed(2) : modelValue;
                //return modelValue.toString().replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
            });
        }
    };
});

appModule.directive('bindOnce', function () {
    return {
        scope: true,
        link: function ($scope) {
            setTimeout(function () {
                $scope.$destroy();
            }, 0);
        }
    };
});


appModule.directive('customPopover', ['$compile', function ($compile) {
    return {
        restrict: 'A',
        link: function (scope, el) {
            $(el).popover({
                html: true,
                content: function () {
                    return $compile($("#casino_popover_opt_content").html())(scope);
                    //return $("#casino_popover_opt_content").html();
                }
            });
        }
    };
}]);


//appModule.directive('offeringButton', function () {
//    return {
//        restrict: 'A',
//        scope: {
//            wagerType: '&wagerType',
//            limitTitle: '&limit-title'
//        },
//        link: function (scope, el, attrs) {
//            if (scope.wagerType() == 'L' || scope.wagerType() == 'H') {
//                attrs.$set('tittle', scope.limitTitle());
//            }
//        }
//    };
//});


appModule.directive('offeringButton', function () {
    return {
        restrict: 'E',
        scope: {
            limitType: '=limitType',
            limitTitle: '=limitTitle',
            customClass: '@customClass',
            changed: '=changed',
            active: '=active',
            bind: '=bind',
            subType: '@subType',
            position: '@position',
            maxWager: '=maxWager',
            isGameLineDisabled: '&',
            line: '=line',
            disabled: '=',
            typeId: '=',
            isDemo: '='
        },
        transclude: true,
        link: function (scope) {
            scope.typeId = scope.subType + scope.position;
            scope.disabled = scope.isGameLineDisabled(
                {
                    gameLine: scope.line,
                    subWagerType: scope.subType,
                    teamPos: scope.position
                });
            scope.limitType = (!scope.disabled ? scope.limitType : "");
            scope.maxWager = (!scope.disabled && scope.line.Status == 'I' && (scope.limitType == 'L' || scope.limitType == 'H') ? scope.maxWager : "");
            //scope.tooltipTrigger = CommonFunctions.isIOS() ? "focus" : "hover";
        },
        templateUrl: "/app/components/betOffering/betOfferingButton.html"
    };
});


var cancelClick = false;
appModule.directive('ngClick', function () {
    return {
        restrict: 'A',
        replace: false,
        priority: -1,
        link: function (scope, el) {
            el.bind('click', function (e) {

                if (cancelClick) {
                    cancelClick = false;
                    e.preventDefault();
                    e.stopImmediatePropagation();
                    return;
                }
                cancelClick = true;
                setTimeout(function () {
                    if (cancelClick) {
                        cancelClick = false;
                        return;
                    }

                    //clean up
                    cancelClick = false;
                }, 500);
                return;
            });
        }
    };
});