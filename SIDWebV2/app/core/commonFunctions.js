﻿//debug functions
var CommonFunctions = CommonFunctions || {};

CommonFunctions.DefaultCustProfile = ".";
CommonFunctions.CustomerTimeZone = 3;

function log(data) {
  if (!SETTINGS.ConsoleActive) return;
  var caller = arguments.callee.caller.name;
  if (!caller || caller == "") caller = arguments.callee.caller.toString();
}

CommonFunctions.ServiceCodes = {
  success: 0,
  fail: 1,
  sessionExpired: 2,
  timeOut: 3,
  warning: 4
};

CommonFunctions.ApplyTimeZone = function (date, timeZone) {
  var offSet = 0;
  switch (timeZone) {
    case 2:
      offSet = 1;
      break;
    case 1:
      offSet = 2;
      break;
    case 0:
      offSet = 3;
      break;
  }
  return new Date(date.getTime() + (offSet * 60 * 60 * 1000));
};

//Document_Function
CommonFunctions.DeleteFromArray = function (array, posToDelete) {
  var cnt = 0;
  $.each(posToDelete, function (i, el) {
    array.splice(el - cnt, 1);
    cnt++;
  });
};

//Document_Function
CommonFunctions.DynamicSort = function (property) {
  var sortOrder = 1;

  if (property[0] == "-") {
    sortOrder = -1;
    property = property.substr(1);
  }
  return function (a, b) {
    var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
    return result * sortOrder;
};
};

//Document_Function
CommonFunctions.FormatDateTime = function (dateTime, formatCode, timeZone, showFourDigitsYear) {
  var ret = null;
  var retDate = null;
  if (dateTime == null || formatCode == null)
    return null;
  if (timeZone == null || typeof timeZone == "undefined") timeZone = CommonFunctions.CustomerTimeZone;
  if (dateTime.toString().indexOf("Date(") >= 0) {
    var ticks = parseFloat(dateTime.replace("/Date(", "").replace(")/", ""));
    dateTime = new Date(ticks);
    dateTime = CommonFunctions.ApplyTimeZone(dateTime, timeZone);
  }
  else {
    dateTime = new Date(dateTime);
    if (formatCode != 11) dateTime = CommonFunctions.ApplyTimeZone(dateTime, timeZone);

  }
  var dd, mm, yyyy, hh, mi, ampm, weekday, month;
  switch (formatCode) {
    case 1:
      return dateTime.toLocaleDateString("en-US");
    case 2:
      return dateTime.format("shortTime", false) + CommonFunctions.GetTimeZoneDesc(timeZone);
    case 3:
      dd = dateTime.getDate();
      mm = dateTime.getMonth() + 1;
      yyyy = dateTime.getFullYear();

      if (showFourDigitsYear != null && showFourDigitsYear == "true") {
        if (dd < 10)
          dd = '0' + dd;
        if (mm < 10)
          mm = '0' + mm;
        retDate = mm + '/' + dd + '/' + yyyy.toString();
      } else {
        if (dd < 10)
          dd = '0' + dd;
        if (mm < 10)
          mm = '0' + mm;
        retDate = mm + '/' + dd + '/' + yyyy.toString().substr(2, 2);
      }
      hh = dateTime.getHours();
      mi = dateTime.getMinutes();
      ampm = hh >= 12 ? "PM" : "AM";

      if (hh > 12)
        hh = hh - 12;
      if (mi < 10)
        mi = '0' + mi;
      retDate += " / " + hh + ":" + mi + " " + ampm;

      return retDate;
    case 4:
      dd = dateTime.getDate();
      mm = dateTime.getMonth() + 1;
      yyyy = dateTime.getFullYear();

      retDate = null;
      if (dd < 10)
        dd = '0' + dd;
      if (mm < 10)
        mm = '0' + mm;
      return mm + '/' + dd + '/' + yyyy.toString();
    case 5:
      dd = dateTime.getDate();
      mm = dateTime.getMonth() + 1;
      yyyy = dateTime.getFullYear();

      if (dd < 10)
        dd = '0' + dd;
      if (mm < 10)
        mm = '0' + mm;
      return yyyy.toString() + '-' + mm + '-' + dd;
    case 6:
      dd = dateTime.getDate();
      mm = dateTime.getMonth() + 1;

      weekday = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"];

      return weekday[dateTime.getDay()] + " " + mm + '/' + dd;
    case 7:
      dd = dateTime.getDate();
      mm = dateTime.getMonth() + 1;

      weekday = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"];

      return weekday[dateTime.getDay()] + " " + mm + '/' + dd;
    case 8:
      hh = dateTime.getHours();
      mi = dateTime.getMinutes();
      ampm = hh >= 12 ? "PM" : "AM";

      if (hh > 12)
        hh = hh - 12;
      if (mi < 10)
        mi = '0' + mi;
      return hh + ":" + mi + " " + ampm;

    case 9:
      dd = dateTime.getDate();
      mm = dateTime.getMonth() + 1;
      yyyy = dateTime.getFullYear();
      return yyyy + mm + dd;

    case 10:
      return dateTime;


    case 11:
      dd = dateTime.getDate();
      mm = dateTime.getMonth();
      weekday = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
      month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
      return weekday[dateTime.getDay()] + " " + dd + " " + month[dateTime.getMonth()];

    case 12:
      hh = dateTime.getHours();
      mi = dateTime.getMinutes();
      ampm = hh >= 12 ? "PM" : "AM";

      if (hh > 12)
        hh = hh - 12;
      if (mi < 10)
        mi = '0' + mi;
      dd = dateTime.getDate();
      mm = dateTime.getMonth();
      weekday = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
      month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
      return weekday[dateTime.getDay()] + " " + dd + " " + month[dateTime.getMonth()] + " " + hh + ":" + mi + " " + ampm;

    case 13:
      return dateTime.format("shortTime", false);
    case 14:
      return CommonFunctions.GetTimeZoneDesc(timeZone);
    case 15:
      var parts = dateTime.toLocaleDateString("en-US").split("/");
      return new Date(parts[2], parts[0] - 1, parts[1]);
    case 16:
      dd = dateTime.getDate();
      mm = dateTime.getMonth() + 1;
      return mm + '/' + dd;
  }
  return ret;
};

//Document_Function
CommonFunctions.GetTimeZoneDesc = function (timeZone) {
  var desc = "";
  switch (timeZone) {
    case 3:
      desc = " (PST)";
      break;
    case 2:
      desc = " (MST)";
      break;
    case 1:
      desc = " (CST)";
      break;
    case 0:
      desc = " (EST)";
      break;
  }
  return desc;
};


//function getDateString(jsonDate) {
//    return toPstTime(jsonDate);
//}

//Document_Function
CommonFunctions.JSONToDate = function (dateTime, removeTime) {
  if (typeof dateTime == "undefined" || dateTime == null)
    return null;
  try {
    //getDateString(dateTime);
    if (removeTime == null) removeTime = false;
    var ticks = parseInt(dateTime.replace("/Date(", "").replace(")/", ""));
    var dt = moment.utc(ticks)._d;

    if (removeTime) dt.setHours(0, 0, 0, 0);
    return dt;
  } catch (e) {
    log(e);
  }
  return null;
};

//Document_Function
CommonFunctions.SubstituteDigitByLetter = function (digit) {
  switch (digit) {
    case "0":
      return "Z";
    case "1":
      return "A";
    case "2":
      return "B";
    case "3":
      return "C";
    case "4":
      return "D";
    case "5":
      return "E";
    case "6":
      return "F";
    case "7":
      return "G";
    case "8":
      return "H";
    case "9":
      return "I";
  }
  return "";
};

//Document_Function
CommonFunctions.SubString = function (str, beg, end) {
  if (str == null) return "";
  if (beg == null) beg = 0;
  if (end == null) end = str.toString().length;

  return str.toString().substring(beg, end);

};

//Document_Function
CommonFunctions.AdjustLine = function (line, lineAdj, evForZero, showSign) {
  var x = line + lineAdj;
  if (evForZero == true && x == 0) {
    return "pk";
  }
  var returnStr = "";
  if (x > 0 && showSign) {
    returnStr = "+";
  }

  returnStr += LineOffering.ConvertToHalfSymbol(x);
  return returnStr;
};

//Document_Function
CommonFunctions.CalculatePriceAdj = function (line, lineAdj, buyPointsAry, wagerType, sportSubType, progressivePointBuyingFlag, progressivePointCostAry) {
  var buy = 0;
  var buyMax = 0;
  var buyOn3 = 0;
  var buyOff3 = 0;
  var buyOn7 = 0;
  var buyOff7 = 0;

  if (wagerType == "S") {
    buy = buyPointsAry.SpreadBuy;
    buyMax = buyPointsAry.SpreadBuyMax;
    buyOn3 = buyPointsAry.SpreadBuyOn3;
    buyOff3 = buyPointsAry.SpreadBuyOff3;
    buyOn7 = buyPointsAry.SpreadBuyOn7;
    buyOff7 = buyPointsAry.SpreadBuyOff7;
  }
  else {
    buy = buyPointsAry.TotalBuy;
    buyMax = buyPointsAry.TotalBuyMax;
    buyOn3 = buyPointsAry.TotalBuy;
    buyOff3 = buyPointsAry.TotalBuy;
    buyOn7 = buyPointsAry.TotalBuy;
    buyOff7 = buyPointsAry.TotalBuy;
  }

  if (progressivePointBuyingFlag == null) progressivePointBuyingFlag = "N";

  var cost = 0;
  var wrkLineAdj = lineAdj;

  if (wrkLineAdj < 0) wrkLineAdj *= -1;
  var i;
  if (progressivePointBuyingFlag == 'Y' && progressivePointCostAry != null) {
    for (i = 1; i <= parseInt(progressivePointBuyingFlag) && i <= wrkLineAdj * 2; i++) {
      if (line + (i * .5) == -3 || line + (i * .5) == 3)
        cost += Math.round((progressivePointCostAry[i - 1].pricePerHalfPoint * progressivePointCostAry[i - 1].onOff3Ratio), 0);
      else {
        if (line + (i * .5) == -2.5 || line + (i * .5) == 3.5)
          cost += Math.round((progressivePointCostAry[i - 1].pricePerHalfPoint * progressivePointCostAry[i - 1].off3Ratio), 0);
        else {
          if (line + (i * .5) == -7 || line + (i * .5) == 7)
            cost += Math.round((progressivePointCostAry[i - 1].pricePerHalfPoint * progressivePointCostAry[i - 1].onOff7Ratio), 0);
          else {
            if (line + (i * .5) == -6.5 || line + (i * .5) == 7.5)
              cost += Math.round((progressivePointCostAry[i - 1].pricePerHalfPoint * progressivePointCostAry[i - 1].off7Ratio));
            else cost += progressivePointCostAry[i - 1].pricePerHalfPoint;
          }
        }
      }
    }
  }
  else {
    for (i = 0.5; i <= wrkLineAdj; i += 0.5) {
      switch (wagerType) {
        case 'L':
          cost += buy;
          break;
        case 'S':
          if (line + i == 3 || line + i == -3) cost += buyOn3;
          else {
            if (line + i - 0.5 == 3 || line + i - 0.5 == -3) cost += buyOff3;
            else {
              if (line + i == 7 || line + i == -7) cost += buyOn7;
              else {
                if (line + i - 0.5 == 7 || line + i - 0.5 == -7) cost += buyOff7;
                else cost += buy;
              }
            }
        }
          break;
    }
            }
    }
  return cost;
};

//Document_Function
CommonFunctions.ConvertPriceToDecimal = function (price, precision) {

  var decimalPrice = 0.0;

  if (precision == 0) {
    if (price < 0) {
      decimalPrice = -100.0 / price;
    }
    else {
      decimalPrice = price / 100.0;
    }
    decimalPrice += 1.0;
    return decimalPrice;
    }

  var multiplier = CommonFunctions.GetPrecisionMultiplier(precision);
  if (price < 0) decimalPrice += (Math.floor(-100.0 / price * multiplier));
  else decimalPrice += (Math.floor(price / 100.0 * multiplier));

  switch (precision) {
    case 2:
      decimalPrice += 100;
      break;
    case 3:
      decimalPrice += 1000;
      break;
    case 4:
      decimalPrice += 10000;
      break;
  }

  decimalPrice /= multiplier;

  return decimalPrice;

};

//Document_Function
CommonFunctions.GetPrecisionMultiplier = function (precision) {
  var multiplier = 10000;

  switch (precision) {
    case 2:
      multiplier = 100;
      break;
    case 3:
      multiplier = 1000;
      break;
    case 4:
      multiplier = 10000;
      break;
  }
  return multiplier;
};

//Document_Function
CommonFunctions.RoundNumber = function (val) {
  var parsed = parseFloat(val, 10);
  if (parsed !== parsed) { return null; } // check for NaN
  if (CommonFunctions.showCents) parsed *= 100;
  var rounded = Math.round(parsed) / (CommonFunctions.showCents ? 100 : 1);
  return rounded;
};

CommonFunctions.showCents = true;
//Document_Function
CommonFunctions.FormatNumber = function (num, divideByHundred, applyFloor, showCents) {
  if (num == null) num = 0;
  if (CommonFunctions.IsNumeric(num)) {
    if (typeof divideByHundred == "undefined") divideByHundred = false;
    if (typeof CommonFunctions.showCents == "undefined") CommonFunctions.showCents = false;
    if (showCents != null) CommonFunctions.showCents = showCents;
    var neg = num < 0;
    var f = parseFloat(Math.abs(num));


    if (divideByHundred) f = f / 100;
    if (applyFloor) f = Math.floor(f).toFixed();
    else f = CommonFunctions.RoundNumber(f);
    //else if (!CommonFunctions.showCents) f = Math.round(f).toFixed();
    //else f = f.toFixed(2);
    var p = (f + "").split(".");
    var ret = p[0].split("").reverse().reduce(function (acc, number, i) {
      return number + (i && !(i % 3) ? "," : "") + acc;
    }, "");
    if (CommonFunctions.showCents && p[1]) ret += "." + (p[1].length == 1 ? p[1] + '0' : p[1]);
    if (neg && ret != "0") ret = "-" + ret;
    if (f == parseInt(f, 10)) ret = ret + '.00';
    return ret;

  }
  else return num;
};

//Document_Function
CommonFunctions.IsNumeric = function (data) {
  return parseFloat(data) == data;
};

//Document_Function
CommonFunctions.RedirecToPage = function (page, $location, redirectToSamePage) {
  if (typeof redirectToSamePage == "undefined") redirectToSamePage = false;
  if (window.location.href.indexOf(page) == -1 || redirectToSamePage || page.length < 2) {
    if ($location != null) {
      $location.path(page);
    }
    else {
      window.location.href = page;
    }
    }
};

CommonFunctions.WriteWagerDescription = function (wagerInfo, translator) {
  var description = "";

  var twriter = wagerInfo.EnteredBy || wagerInfo.TicketWriter;
  if (twriter) {

    if (twriter.indexOf("HORSE") != -1) {
      $scope.isXTransaction = true;
      return "Horses";
    }
    if (twriter.indexOf("GSLIVE") != -1) {
      $scope.isXTransaction = true;
      return "Live Betting";
    }
    if (twriter.indexOf("System") != -1) {
      $scope.isXTransaction = true;
      return "Casino";
    }
    }

  switch (wagerInfo.WagerType) {
    case "S":
      if (wagerInfo.SportType != null && wagerInfo.SportType == "Baseball")
        description = translator.Translate("RUN_LINE");
      else
        description = translator.Translate("SPREAD");
      break;
    case "M":
      description = translator.Translate("MONEY LINE");
      break;
    case "L": //Total points
      description = translator.Translate("TOTAL POINTS");
      break;
    case "E": //team Totals
      description = translator.Translate("TEAM TOTALS");
      break;
    case "P":
      if (wagerInfo.RoundRobinLink > 0) description = wagerInfo.totalPicks + " " + translator.Translate("TEAMS") + " " + translator.Translate("ROUND ROBIN");
      else description = wagerInfo.totalPicks + " " + translator.Translate("TEAMS") + " " + translator.Translate("PARLAY");
      break;
    case "T":
      var ties = "";
      switch (wagerInfo.ties) {
        case 0:
          ties = translator.Translate("PUSHES");
          break;
        case 1:
          ties = translator.Translate("WINS");
          break;
        case 2:
          ties = translator.Translate("LOSES");
          break;
      }
      var teaserName = "";
      if (wagerInfo.TeaserName != null)
        teaserName = wagerInfo.TeaserName;
      description = wagerInfo.totalPicks + " " + translator.Translate("TEAMS") + " " + teaserName + " " + translator.Translate("TEASER") + ", " + translator.Translate("TIES") + " " + ties;
      break;
    case "I":
      description = "If-Bet";
      if (wagerInfo.ContinueOnPushFlag != null) {
        if (wagerInfo.ContinueOnPushFlag == "Y") {
          description += " (" + translator.Translate("IF WIN OR PUSH") + ")";
        } else {
          description += " (" + translator.Translate("IF WIN ONLY") + ")";
        }
      }
      break;
    case "C":
      description = translator.Translate("PROPS TITLE");
      break;
    case "G":
      description = translator.Translate("Horses");
      break;
  }
  return description;
};

CommonFunctions.Translate = function (text) {
  return text;
  //Pending
};

CommonFunctions.ArrayIncludes = function (array, val) {
  if (!array || !val) return false;
  for (var idx = 0; idx < array.length; idx++) {
    if (array[idx] == val) return true;
  }
  return false;
};

CommonFunctions.isIOS = function () {
  return !!navigator.platform.match(/iPhone|iPod|iPad/);
};