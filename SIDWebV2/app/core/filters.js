﻿//Document_Filter
appModule.filter("formatGameDate", function () {
    return function (dateTime) {
        dateTime = CommonFunctions.FormatDateTime(dateTime, 11);
        return dateTime;
    };
});

//Document_Filter
appModule.filter("formatGameTime", function () {
    return function (dateTime) {
        //dateTime = CommonFunctions.FormatDateTime(CommonFunctions.JSONToDate(dateTime, false), 2);
        dateTime = CommonFunctions.FormatDateTime(dateTime, 13);
        return dateTime;
    };
});

//Document_Filter
appModule.filter("formatGameZone", function () {
    return function (dateTime) {
        //dateTime = CommonFunctions.FormatDateTime(CommonFunctions.JSONToDate(dateTime, false), 2);
        dateTime = CommonFunctions.FormatDateTime(dateTime, 14);
        return dateTime;
    };
});

//Document_Filter
appModule.filter("formatLine", ['$customerService', '$wagerTypesService', function ($customerService, $wagerTypesService) {
    return function (value, scope, game, wagerType) {
        var teamPos = parseInt(wagerType.substring(1, 2));
        var priceType = $customerService.Info ? $customerService.Info.PriceType : "A";
        switch (wagerType.substring(0, 1)) {
            case "S":
                value = LineOffering.FormatSpreadOffer(game, teamPos, true, true, true, priceType);
                break;
            case "M":
                value = LineOffering.FormatMoneyLineOffer(game, teamPos, true, true, priceType);
                break;
            case "L":
                value = LineOffering.FormatTotalOffer(game, teamPos, true, true, true, priceType);
                if (value != LineOffering.DisabledLineLabel) {
                    if (teamPos == 1) value = LineOffering.OverLabel + " " + value;
                    else value = LineOffering.UnderLabel + " " + value;
                }
                break;
            case "E":
                var wt = $wagerTypesService.Selected.name;
                value = LineOffering.GetTeamTotalsOffer(game, teamPos, true, true, true, wt, priceType);
                if (value != LineOffering.DisabledLineLabel) {
                    if (teamPos == 1 || teamPos == 2) value = LineOffering.OverLabel + " " + value;
                    else value = LineOffering.UnderLabel + " " + value;
                }
                break;

        }
        return value;
    };
}]);

appModule.filter('dollar', function () {
    return function (value) {
        return SETTINGS.CurrencySign + (CommonFunctions.FormatNumber(value, true));
    };
});

appModule.filter('formatNumber', ['$customerService', function ($customerService) {
    return function (value, applyFloor, showCents) {
        var currency = $customerService.Info ? " " + $customerService.Info.Currency.substring(0, 3) : "";
        if (typeof applyFloor == "undefined") applyFloor = false;
        if (typeof showCents == "undefined") showCents = true;
        if (currency == " USD") currency = "";
        return CommonFunctions.FormatNumber(value, true, applyFloor, showCents) + currency;
    };
}]);

appModule.filter('reverse', function () {
    return function (items) {
        return items.slice().reverse();
    };
});