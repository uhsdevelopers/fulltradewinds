﻿ARFunctions = function () { };

ARFunctions.DetermineARToWinAmount = function (wiAry, riskAmt) {
  var toWinAmount = 0;

  angular.forEach(wiAry, function (wi) {
    if (wi.FinalPrice < 0) {
      wi.ToWinAmt = riskAmt;
      wi.RiskAmt = 0;
    } else {
      wi.ToWinAmt = 0;
      wi.RiskAmt = riskAmt;
    }
    ARFunctions.CalculateWiWagerAmount(wi);
    toWinAmount += wi.ToWinAmt * 2;
  });

  return toWinAmount;
};

ARFunctions.CalculateWiWagerAmount = function (wi) {
  if (wi.RiskAmt == 0) {
    wi.RiskAmt = LineOffering.CalculateRiskAmt(wi.ToWinAmt, wi.PriceType, wi.FinalPrice, wi.FinalDecimal, wi.FinalNumerator, wi.FinalDenominator);
    return true;
  } else {
    wi.ToWinAmt = LineOffering.CalculateToWinAmt(wi.RiskAmt, wi.PriceType, wi.FinalPrice, wi.FinalDecimal, wi.FinalNumerator, wi.FinalDenominator);
    return true;
  }

};

ARFunctions.CalculateARWagerAmounts = function (inetWagerNumber, riskAmt, $SessionManagerService) {
  var retValue = "";
  $SessionManagerService.GetARWagerAmounts(inetWagerNumber, riskAmt).then(function (result) {
    retValue = result.data.d;
  });

  return retValue;
}