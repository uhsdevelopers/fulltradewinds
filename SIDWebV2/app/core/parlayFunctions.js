﻿ParlayFunctions = function () { };

ParlayFunctions.GetRoundRobin = function (selCount, translator) {
  var roundRobinSel = new Array();
  var cnt = 0;

  roundRobinSel[cnt++] = { value: 0, name: translator.Translate("SingleParlay") };
  if (selCount > 2) {
    roundRobinSel[cnt++] = { value: 1, name: translator.Translate("Round Robin with") + " 2's" };
    roundRobinSel[cnt++] = { value: 2, name: translator.Translate("Round Robin with") + " 2's, 3's" };
  }
  if (selCount > 3) roundRobinSel[cnt++] = { value: 3, name: translator.Translate("Round Robin with") + " 2's, 3's, 4's" };
  if (selCount > 4) roundRobinSel[cnt++] = { value: 4, name: translator.Translate("Round Robin with") + " 2's, 3's, 4's, 5's" };
  if (selCount > 5) roundRobinSel[cnt++] = { value: 5, name: translator.Translate("Round Robin with") + " 2's, 3's, 4's, 5's, 6's" };
  if (selCount > 3) {
    roundRobinSel[cnt++] = { value: 6, name: translator.Translate("Round Robin with") + " 3's" };
    roundRobinSel[cnt++] = { value: 7, name: translator.Translate("Round Robin with") + " 3's, 4's" };
  }
  if (selCount > 4) roundRobinSel[cnt++] = { value: 8, name: translator.Translate("Round Robin with") + " 3's, 4's, 5's" };
  if (selCount > 5) roundRobinSel[cnt++] = { value: 9, name: translator.Translate("Round Robin with") + " 3's, 4's, 5's, 6's" };

  return roundRobinSel;
};


// Move this function to a service
ParlayFunctions.CalculateParlayCardToWin = function (wageredAmt, pickCount, parlayPayCard) {
  var toWinAmt;
  if (parlayPayCard != null) {
    var moneyLine = parlayPayCard.MoneyLine * 1;
    var toBase = parlayPayCard.ToBase * 1;
    toWinAmt = wageredAmt * moneyLine / toBase;
  } else {
    toWinAmt = 0;
  }
  return toWinAmt;

};

//Pending
// riskAmt
// parlayDefaultPrice

ParlayFunctions.CalculateRatioParlayToWin = function (wiAry, parlayInfo, riskAmt) {
  riskAmt = parseFloat(riskAmt);
  var rollingBalance = riskAmt;
  var cardSelCount = 0;
  var wi;
  var i;
  for (i = 0; i < wiAry.length; i++) {
    wi = wiAry[i];
    var finalPrice = wi.SelectedLine != null ? parseInt(wi.SelectedLine.lineAdj) : wi.FinalPrice;
    if ((wi.Loo.SportType.trim() == "Football" || wi.Loo.SportType.trim() == "Basketball") && wi.WagerType != "M") {
      if (finalPrice == parlayInfo.ParlayDefaultPrice * 1) {
        wi.ToWinAmt = LineOffering.CalculateToWinAmtUsingWi(wi, rollingBalance, "A");
        rollingBalance += wi.ToWinAmt;
      } else {
        wi.ToWinAmt = LineOffering.CalculateToWinAmtUsingWi(wi, rollingBalance, null);
        rollingBalance += wi.ToWinAmt;
      }
      cardSelCount++;
    }
  }
  var selPricePayout = rollingBalance - riskAmt;

  rollingBalance = riskAmt;
  for (i = 0; i < wiAry.length; i++) {
    wi = wiAry[i];
    if ((wi.Loo.SportType.trim() == "Football" || wi.Loo.SportType.trim() == "Basketball") && wi.WagerType != "M") {
      wi.ToWinAmt = LineOffering.CalculateToWinAmtUsingWi(wi, rollingBalance, "A", parlayInfo.ParlayDefaultPrice);
      rollingBalance += wi.ToWinAmt;
    }
  }
  var flatPricePayout = rollingBalance - riskAmt;

  var payout = 0;
  if (flatPricePayout > 0)
    payout = ParlayFunctions.CalculateParlayCardToWin(riskAmt, cardSelCount, ParlayFunctions.GetPayCardInfo(parlayInfo.ParlayPayCards, cardSelCount)) * selPricePayout / flatPricePayout;

  var payoutWithRisk = payout + riskAmt;

  if (payout == 0) payoutWithRisk = riskAmt;

  for (i = 0; i < wiAry.length; i++) {
    wi = wiAry[i];
    if ((wi.Loo.SportType.trim() != "Football" && wi.Loo.SportType.trim() != "Basketball") || wi.WagerType == "M" || payout == 0) {
      wi.ToWinAmt = LineOffering.CalculateToWinAmtUsingWi(wi, payoutWithRisk);
      payoutWithRisk += wi.ToWinAmt;
    }
  }

  payout = payoutWithRisk - riskAmt;
  return payout;
};

//Pending
/*
    riskAmt
    parlayDefaultPrice
    ParlayPriceRange -> GetIntParameter("ParlayPriceRange")
*/

ParlayFunctions.CalculateOriginalParlayToWin = function (wiAry, parlayInfo, riskAmt) {
  var defaultPriceCount = 0;
  var rollingBalance = riskAmt;
  var wi;
  var i;
  for (i = 0; i < wiAry.length; i++) {
    wi = wiAry[i];
    var parlayDefaultPrice = parlayInfo.ParlayDefaultPrice * 1;
    var parlayDefaultPrice2 = parlayDefaultPrice - parlayInfo.ParlayPriceRange;
    if (wi.Price <= parlayDefaultPrice && wi.Price >= parlayDefaultPrice2 && (wi.Loo.SportType == "Football" || wi.Loo.SportType == "Basketball")) {
      defaultPriceCount++;
    } else {
      wi.ToWinAmt = LineOffering.CalculateToWinAmtUsingWi(wi, rollingBalance);
      rollingBalance += wi.ToWinAmt;
    }
  }

  var defaultToWin = ParlayFunctions.CalculateParlayCardToWin(rollingBalance, defaultPriceCount);

  if (defaultToWin == 0) {
    rollingBalance = riskAmt;
    for (i = 0; i < wiAry.length; i++) {
      wi = wiAry[i];
      wi.ToWinAmt = LineOffering.CalculateToWinAmtUsingWi(wi, rollingBalance);
      rollingBalance += wi.ToWinAmt;
    }
  } else rollingBalance += defaultToWin;

  rollingBalance = Math.round((rollingBalance - riskAmt * 1) * 100) / 100;

  return rollingBalance;
};

ParlayFunctions.CalculateParlayToWinAmt = function (wiAry, parlayWebInfo, pcMaxPayout, riskAmt) {
  var parlayPayoutType = parlayWebInfo.ParlayPayoutType;

  var rollingBalance, maxPayout;

  if (parlayPayoutType == "R") rollingBalance = ParlayFunctions.CalculateRatioParlayToWin(wiAry, parlayWebInfo, riskAmt);
  else rollingBalance = ParlayFunctions.CalculateOriginalParlayToWin(wiAry, parlayWebInfo, riskAmt);

  if (pcMaxPayout > 0 && rollingBalance > pcMaxPayout) {
    rollingBalance = pcMaxPayout;
  }

  if (parlayWebInfo.CustMaxParlayPayout > 0) {
    maxPayout = parlayWebInfo.CustMaxParlayPayout / 100;
    if (rollingBalance > maxPayout) {
      rollingBalance = maxPayout;
    }
  } else {
    maxPayout = parlayWebInfo.MaxParlayPayout / 100;
    if (maxPayout > 0 && rollingBalance > maxPayout) {
      rollingBalance = maxPayout;
    }
  }

  return rollingBalance;

};


ParlayFunctions.GetPayCardInfo = function (parlayPayCards, picks) {
  for (var i = 0; i < parlayPayCards.length; i++) {
    if (parlayPayCards[i].GamesPicked == picks) {
      return parlayPayCards[i];
    }
  }
  return null;
};

ParlayFunctions.GetPayCardMaxPayout = function (picks, parlayInfo, riskAmt) {
  var moneyLine;
  var toBase;
  var toWinAmt = 0;
  var payCardInfo = ParlayFunctions.GetPayCardInfo(parlayInfo.ParlayPayCards, picks);
  if (payCardInfo != null) {
    if (payCardInfo.MaxPayoutMoneyLine != null && payCardInfo.MaxPayoutToBase != null) {
      moneyLine = payCardInfo.MaxPayoutMoneyLine * 1;
      toBase = payCardInfo.MaxPayoutToBase * 1;
      toWinAmt = riskAmt * moneyLine / toBase;
    }
  }
  return toWinAmt;
};

ParlayFunctions.RoundRobinToWinValues = function (toWinAmt, playCount) {
  this.ToWinAmt = toWinAmt;
  this.PlayCount = playCount;
};

ParlayFunctions.CalculateRoundRobinToWin = function (wiAry, parlayInfo, riskAmt, combos) {
  var rollingBalance = 0;
  var skipTwos = false;
  var playCount = 0;
  var pcMaxPayout = 0;

  if (combos >= 6) {
    combos -= 4;
    skipTwos = true;
  }
  var plAry;
  var a, b, c, d, e;
  switch (combos) {
    case 5: {
      pcMaxPayout = ParlayFunctions.GetPayCardMaxPayout(6, parlayInfo, riskAmt);
      for (a = 0; a < wiAry.length - 5; a++) {
        for (b = a + 1; b < wiAry.length - 4; b++) {
          for (c = b + 1; c < wiAry.length - 3; c++) {
            for (d = c + 1; d < wiAry.length - 2; d++) {
              for (e = d + 1; e < wiAry.length - 1; e++) {
                for (var f = e + 1; f < wiAry.length; f++) {
                  plAry = new Array();
                  plAry[0] = wiAry[a];
                  plAry[1] = wiAry[b];
                  plAry[2] = wiAry[c];
                  plAry[3] = wiAry[d];
                  plAry[4] = wiAry[e];
                  plAry[5] = wiAry[f];
                  rollingBalance += ParlayFunctions.CalculateParlayToWinAmt(plAry, parlayInfo, pcMaxPayout, riskAmt);
                  playCount++;
                }
              }
            }
          }
        }
      }
    }
    case 4: {
      pcMaxPayout = ParlayFunctions.GetPayCardMaxPayout(5, parlayInfo, riskAmt);
      for (a = 0; a < wiAry.length - 4; a++) {
        for (b = a + 1; b < wiAry.length - 3; b++) {
          for (c = b + 1; c < wiAry.length - 2; c++) {
            for (d = c + 1; d < wiAry.length - 1; d++) {
              for (e = d + 1; e < wiAry.length; e++) {
                plAry = new Array();
                plAry[0] = wiAry[a];
                plAry[1] = wiAry[b];
                plAry[2] = wiAry[c];
                plAry[3] = wiAry[d];
                plAry[4] = wiAry[e];
                rollingBalance += ParlayFunctions.CalculateParlayToWinAmt(plAry, parlayInfo, pcMaxPayout, riskAmt);
                playCount++;
              }
            }
          }
        }
      }
    }
    case 3: {
      pcMaxPayout = ParlayFunctions.GetPayCardMaxPayout(4, parlayInfo, riskAmt);
      for (a = 0; a < wiAry.length - 3; a++) {
        for (b = a + 1; b < wiAry.length - 2; b++) {
          for (c = b + 1; c < wiAry.length - 1; c++) {
            for (d = c + 1; d < wiAry.length; d++) {
              plAry = new Array();
              plAry[0] = wiAry[a];
              plAry[1] = wiAry[b];
              plAry[2] = wiAry[c];
              plAry[3] = wiAry[d];
              rollingBalance += ParlayFunctions.CalculateParlayToWinAmt(plAry, parlayInfo, pcMaxPayout, riskAmt);
              playCount++;
            }
          }
        }
      }
    }
    case 2: {
      pcMaxPayout = ParlayFunctions.GetPayCardMaxPayout(3, parlayInfo, riskAmt);
      for (a = 0; a < wiAry.length - 2; a++) {
        for (b = a + 1; b < wiAry.length - 1; b++) {
          for (c = b + 1; c < wiAry.length; c++) {
            plAry = new Array();
            plAry[0] = wiAry[a];
            plAry[1] = wiAry[b];
            plAry[2] = wiAry[c];
            rollingBalance += ParlayFunctions.CalculateParlayToWinAmt(plAry, parlayInfo, pcMaxPayout, riskAmt);
            playCount++;
          }
        }
      }
    }
    case 1: {
      if (skipTwos == false) {
        pcMaxPayout = ParlayFunctions.GetPayCardMaxPayout(2, parlayInfo, riskAmt);
        for (a = 0; a < wiAry.length - 1; a++) {
          for (b = a + 1; b < wiAry.length; b++) {
            plAry = new Array();
            plAry[0] = wiAry[a];
            plAry[1] = wiAry[b];
            rollingBalance += ParlayFunctions.CalculateParlayToWinAmt(plAry, parlayInfo, pcMaxPayout, riskAmt);
            playCount++;
          }
        }
      }
    }
  }
  var maxPayout = 500000;
  if (parlayInfo.CustMaxParlayPayout > 0) {
    maxPayout = parlayInfo.CustMaxParlayPayout / 100;
    if (rollingBalance > maxPayout) {
      rollingBalance = maxPayout;
    }
  } else {
    maxPayout = parlayInfo.MaxParlayPayout / 100;
    if (maxPayout > 0 && rollingBalance > maxPayout) {
      rollingBalance = maxPayout;
    }
  }

  return new ParlayFunctions.RoundRobinToWinValues(rollingBalance, playCount);

};

ParlayFunctions.Factorial = function (number) {
  if (number <= 1 || isNaN(number)) {
    return 1;
  }
  return number * ParlayFunctions.Factorial(number - 1);
};

ParlayFunctions.RoundRobinCombination = function(number, numberChosen) {
  if (number <= 1 || isNaN(number)) {
    return 1;
  }
  return ParlayFunctions.Factorial(number) / (ParlayFunctions.Factorial(numberChosen) * ParlayFunctions.Factorial(number - numberChosen));
};

ParlayFunctions.GetRoundRobinCombinations = function (selectedRoundRobin, numTeams) {
  var itemsPerParlayList = selectedRoundRobin.split(',');
  var result = "";
  var totalCombinations = 0;

  for (var i = 0; i < itemsPerParlayList.length; i++) { // (var itemsPerParlayText in itemsPerParlayList) {
    var itemsPerParlayText = itemsPerParlayList[i].replace("'s", "");
    var itemsPerParlay = parseInt(itemsPerParlayText);
    var combination = ParlayFunctions.RoundRobinCombination(numTeams, itemsPerParlay);
    if (combination == 0) break;

    result += combination + " - " + itemsPerParlayText + "'s, ";
    totalCombinations += combination;
  }
  return { CombinationsLabel: result.substr(0, result.length - 2), TotalCombinations: totalCombinations };

};