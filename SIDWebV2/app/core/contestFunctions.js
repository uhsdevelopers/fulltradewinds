﻿var ContestFunctions = function() {};

//Document_Function
ContestFunctions.GetContestDescription = function (contest) {
    if (!contest) return "";
    var desc = contest.ContestType + ": ";
    if (contest.ContestType2 != "." && contest.ContestType2 != "") desc += contest.ContestType2;
    if (contest.ContestType3 != "." && contest.ContestType3 != "") desc += " - " + contest.ContestType3;
    if (contest.ContestDesc != "." && contest.ContestDesc != "") desc += " - " + contest.ContestDesc;
    return desc;
};

//Document_Function
ContestFunctions.GetShortContestDescription = function (contest) {
    var desc = "";
    if (contest.ContestType3 != "." && contest.ContestType3 != "") desc += " " + contest.ContestType3;
    else if (contest.ContestType2 != "." && contest.ContestType2 != "") desc += + " " + contest.ContestType2;
    else desc = contest.ContestType;
    return desc;
};

//Document_Function
ContestFunctions.GetContestantName = function (contestant, league) {
    var contestantDesc = $.trim(contestant.ContestantName);
    if (league.ThresholdType == "P") {
        contestantDesc += ' ' + LineOffering.FormatAhTotal(Number(contestant.ThresholdLine), ',', true)
                       + ' ' + $.trim(league.ThresholdUnits);
    }
    if (league.ThresholdType == "S") {
        contestantDesc += ' ';
        contestantDesc += LineOffering.FormatAhSpread(Number(contestant.ThresholdLine), ',', true);
        contestantDesc += ' ';
        contestantDesc += $.trim(league.ThresholdUnits);
    }
    return contestantDesc;
};

//Document_Function
ContestFunctions.GetContestantLine = function (contestant, priceType) {
    if (typeof priceType == "undefined")
        priceType = "A"; // temp, DMN *

    var contestantLine = "";

    switch (priceType) {
        case "A":
            if (contestant.ToBase > 0) {
                contestantLine = contestant.MoneyLine;
                contestantLine += " " + CommonFunctions.Translations.TO + " ";
                contestantLine += contestant.ToBase;
            } else {
                if (contestant.MoneyLine > 0) {
                    contestantLine += "+";
                }
                contestantLine += contestant.MoneyLine;
            }
            break;
        case "D":
            contestantLine += contestant.DecimalOdds;
            if (Number(contestant.DecimalOdds) == Math.floor(Number(contestant.DecimalOdds))) {
                contestantLine += ".0";
            }
            break;
        case "F":
            contestantLine += contestant.Numerator;
            contestantLine += "/";
            contestantLine += contestant.Denominator;
            break;
    }
    return contestantLine;
};

//Document_Function
ContestFunctions.CalculateContestRiskAmt = function (toWinAmt, priceType, price, toBase, decimalOdds, numerator, denominator) {
    var d;
    var factor;
    var retValue = 0.0;
    switch (priceType) {
        case "A":
            if (toBase == 0) {
                factor = 100 / price;
                if (factor > 0) {
                    d = toWinAmt * factor;
                }
                else {
                    d = toWinAmt / factor * -1;
                }
                retValue = d;
            }
            else {
                factor = toBase;
                factor /= price;
                d = toWinAmt * factor;
                retValue = Math.round(d);
            }
            break;
        case "D":
            retValue = Math.round(toWinAmt / (decimalOdds - 1));
            break;
        case "F":
            retValue = Math.round(toWinAmt / numerator * denominator);
            break;
    }

    return CommonFunctions.RoundNumber(retValue);
};

//Document_Function
ContestFunctions.CalculateContestToWinAmt = function (riskAmt, priceType, price, toBase, decimalOdds, numerator, denominator) {
    var d;
    var factor;
    var retValue = 0.0;
    switch (priceType) {
        case "A":
            if (toBase == 0) {
                factor = price / 100.00;
                if (factor > 0) {
                    d = riskAmt * factor;
                }
                else {
                    d = riskAmt / factor * -1;
                }
                retValue = d;

            }
            else {
                factor = price;
                factor /= toBase;
                d = riskAmt * factor;
                retValue = Math.round(d);
            }
            break;
        case "D":
            retValue = Math.round(riskAmt * (decimalOdds - 1));
            break;
        case "F":
            retValue = Math.round(riskAmt * numerator / denominator);
            break;
    }
    if (retValue > 50000) retValue = 50000;
    return CommonFunctions.RoundNumber(retValue);
};

//Document_Function
ContestFunctions.CalculateContestWagerAmount = function (cwi, amount) {
    if (cwi.Loo.MoneyLine < 0) {
        cwi.ToWinAmt = amount || cwi.ToWinAmt;
        cwi.RiskAmt = ContestFunctions.CalculateContestRiskAmt(cwi.ToWinAmt, "A", cwi.Loo.MoneyLine, cwi.Loo.ToBase, cwi.Loo.DecimalOdds, cwi.Loo.Numerator, cwi.Loo.Denominator);
        cwi.AmountEntered = 'ToWinAmt';
    }
    else {
        cwi.RiskAmt = amount || cwi.RiskAmt;
        cwi.ToWinAmt = ContestFunctions.CalculateContestToWinAmt(cwi.RiskAmt, "A", cwi.Loo.MoneyLine, cwi.Loo.ToBase, cwi.Loo.DecimalOdds, cwi.Loo.Numerator, cwi.Loo.Denominator);
        cwi.AmountEntered = 'RiskAmt';
    }
};