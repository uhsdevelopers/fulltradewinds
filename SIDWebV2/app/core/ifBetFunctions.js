﻿var IfBetFunctions = IfBetFunctions || {};

//Document_Function
IfBetFunctions.ValidateIfBetTeamSelection = function (wagers, gameItem, wagerTypeName, blockST) {
    //try {
        if (wagers == null || wagers.length == 0)
            return true;

        for (var i = 0 ; i < wagers.length; i++) {
            if (IfBetFunctions.CheckForInvalidPickCombinations(gameItem, wagers[i], wagerTypeName, blockST) == true) {
                return false;
            }
        }
    /*}
    catch (e) {
        ErrorHandler.Error(e);
    }*/
    return true;
};

//Document_Function
IfBetFunctions.CheckForInvalidPickCombinations = function (wi1, wi2, wagerTypeName, blockST) {

    var sportType1 = "";
    var sportType2 = "";

    sportType1 += wi1.Loo.SportType;
    sportType2 += wi2.Loo.SportType;

    if (sportType1 != sportType2) {
        return false;
    }

    var errorPending = false;

    if (wi1.Loo.GameNum == wi2.Loo.GameNum || (wi1.Loo.CorrelationID == wi2.Loo.CorrelationID && wi1.Loo.CorrelationID != null)) {
        if (wi1.Loo.ParlayRestriction == "S" &&
           (wagerTypeName == "Parlay" ||
            wagerTypeName == "If Win or Push" ||
            wagerTypeName == "If Win Only" ||
            wagerTypeName == "Action Reverse" ||
            wagerTypeName == "Teaser")) {
            errorPending = true;
        }
        else {
            if (blockST.QuarterInetParlayFlag &&
               wi1.Loo.PeriodNumber == wi2.Loo.PeriodNumber &&
               wi1.Loo.PeriodNumber > 2 &&
               (wi1.wagerType == "S" && wi2.wagerType == "L" ||
                wi1.wagerType == "L" && wi2.wagerType == "S")) {
                errorPending = true;
            }
            if ((wi1.wagerType == wi2.wagerType &&
                !(wagerTypeName == "Teaser" || wagerTypeName == "Action Reverse")) ||
               wi1.wagerType == "S" && wi2.wagerType == "M" ||
               wi1.wagerType == "M" && wi2.wagerType == "S") {
                if (IfBetFunctions.CheckForIntersectingPeriod(wi1.Loo.SportType, wi1.Loo.SportSubType,
                                         wi1.Loo.PeriodNumber, wi2.Loo.PeriodNumber) == true) {
                    errorPending = true;
                }
            }
            if (wi1.wagerType == wi2.wagerType &&
               wagerTypeName == "Teaser" &&
               wi1.Loo.GameNum != wi2.Loo.GameNum) {
                errorPending = true;
            }
            if (blockST.BaseballFlag == "Y") {
                if ((wi1.Loo.SportType == "Baseball" && wi2.Loo.SportType == "Baseball") &&
                   (wi1.wagerType == "S" && wi2.wagerType == "L" ||
                    wi1.wagerType == "L" && wi2.wagerType == "S")) {
                    if (IfBetFunctions.CheckForIntersectingPeriod(wi1.Loo.SportType, wi1.Loo.SportSubType,
                                             wi1.Loo.PeriodNumber, wi2.Loo.PeriodNumber) == true) {
                        errorPending = true;
                    }
                }
            }
            if (blockST.HockeyFlag == "Y") {
                if ((wi1.Loo.SportType == "Hockey" && wi2.Loo.SportType == "Hockey") &&
                   (wi1.wagerType == "S" && wi2.wagerType == "L" ||
                    wi1.wagerType == "L" && wi2.wagerType == "S")) {
                    if (IfBetFunctions.CheckForIntersectingPeriod(wi1.Loo.SportType, wi1.Loo.SportSubType,
                                             wi1.Loo.PeriodNumber, wi2.Loo.PeriodNumber) == true) {
                        errorPending = true;
                    }
                }
            }
        }
    }

    return errorPending;
};

//Document_Function
IfBetFunctions.CheckForIntersectingPeriod = function (sportType, sportSubType, periodNum1, periodNum2) {
    if (periodNum1 == periodNum2) {
        return true;
    }

    $SessionManagerService.GetIntersectingPeriodsInfo(sportType, sportSubType, periodNum1, periodNum2).then(function (result) {
        return result.data.d.Data;
    });
};

