﻿var WagerType = WagerType || {
    CONTEST: "C",
    STRAIGHTBET: "SB",
    SPREAD: "S",
    MONEYLINE: "M",
    TOTALPOINTS: "L",
    TEAMTOTALPOINTS: "E",
    PARLAY: "P",
    IFBET: "I",
    TEASER: "T"
};