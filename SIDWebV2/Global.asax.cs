﻿using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using SIDWebV2.HelperClasses;
using SIDWebLibraries.HelperClasses;

namespace SIDWebV2 {
  // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
  // visit http://go.microsoft.com/?LinkId=9394801

  public class WebApiApplication : HttpApplication {

    protected void Application_Start() {
      AreaRegistration.RegisterAllAreas();
      WebApiConfig.Register(GlobalConfiguration.Configuration);
      FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
      RouteConfig.RegisterRoutes(RouteTable.Routes);
      BundleConfig.RegisterBundles(BundleTable.Bundles);
    }

    void Session_Start(object sender, EventArgs e) {
      // Code that runs when a new session is started
      var context = HttpContext.Current;
      context.Session.Timeout = 15;
      if (context.Session == null) return;
      SessionVariablesCore.InstanceManager = CommonFunctions.GetInstance();
      SessionVariablesCore.WebTarget = WebConfigManager.GetInetTarget();
      SessionVariables.Ticket = new WebTicket();
    }



    void Session_End(object sender, EventArgs e) {
      // Code that runs when a session ends. 
      // Note: The Session_End event is raised only when the sessionstate mode
      // is set to InProc in the Web.config file. If session mode is set to StateServer 
      // or SQLServer, the event is not raised.
      if (SessionVariablesCore.InstanceManager == null) return;
      SessionVariablesCore.InstanceManager.DeleteAppInUse(WebConfigManager.GetApplicationId());
      SystemLog.WriteAccessLog("Session Finished", null);
    }

  }
}