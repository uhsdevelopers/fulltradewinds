﻿using System;

namespace SIDWebLibraries.HelperClasses {
  public class ResultContainer : IDisposable {
    //public enum ErroCode {get; set;}

    public enum ResultCode { Success = 0, Fail = 1, SessionExpired = 2, TimeOut = 3, Warning = 4, UniqueSession = 5, DoubleSession = 6 };

    public object Data { get; set; }
    public ResultCode Code { get; set; }
    public string Message { get; set; }
    public string Server {
      get { return ServerVariables.LocalAddress; }
    }

    public string AppLastUpdate {
      get {
        return SessionVariablesCore.AppLastUpdate;
      }
    }
    public bool SessionExpired {
      get { return Code == ResultCode.SessionExpired; }
    }

    public ResultContainer(bool resetLastSessionAction = true) {
      Code = ResultCode.Success;
      Data = null;
      Message = string.Empty;
      var span = DateTime.Now.Subtract(SessionVariablesCore.LastSessionAction);
      if (!SessionVariablesCore.IsSessionValid() || span.TotalMinutes > WebConfigManager.GetTimeOut()) SetSessionExpired();
      else if (resetLastSessionAction) {
        SessionVariablesCore.LastSessionAction = DateTime.Now;
      }
    }

    public ResultContainer(object data, ResultCode code, string message) {
      Data = data;
      Message = message;
      Code = code;
      if (!SessionVariablesCore.IsSessionValid()) SetSessionExpired();
    }

    public ResultContainer(ResultCode code, string message) {
      Message = message;
      Code = code;
    }

    public void SetError(string message) {
      Code = ResultCode.Fail;
      Message = message;
    }

    public void SetWarning(string message) {
      Code = ResultCode.Warning;
      Message = message;
    }

    /*public void SetSessionExpired(string message) {
      Code = ResultCode.SessionExpired;
      Message = message;
    }*/

    private void SetSessionExpired() {
      Code = ResultCode.SessionExpired;
      Message = string.Empty;
    }

    public void Dispose() {
      Data = null;
    }

  }
}
