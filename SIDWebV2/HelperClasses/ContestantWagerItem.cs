﻿using System;
using SIDLibraries.Entities;
using SIDWebLibraries.HelperClasses;

namespace SIDWebV2.HelperClasses {
  [Serializable()]
  public class ContestantWagerItem : WagerItem {

    #region Public Properties

    public int ContestTypeId { get; set; }

    public double? CircledMaxWager { get; set; }

    public spCnWebGetContestantOfferings_Result Loo { get; set; }

    public String PriceType { get; set; }

    public DateTime? ContestDateTime { get; set; }

    public string ContestDesc { get; set; }

    public int ContestNum { get; set; }

    public int ContestantNum { get; set; }

    public string ContestType { get; set; }

    public string ContestType2 { get; set; }

    public string ContestType3 { get; set; }

    public string Status { get; set; }

    public string ThresholdType { get; set; }

    public string ThresholdUnits { get; set; }

    public double? MaxWagerLimit {
      get;
      set;
    }

    #endregion

    #region Constructors

    public ContestantWagerItem(spCnWebGetContestantOfferings_Result loo) {
      Loo = loo;
    }


    #endregion

  }
}
