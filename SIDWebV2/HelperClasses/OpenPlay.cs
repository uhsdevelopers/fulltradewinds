﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebLibs = SIDWebLibraries.HelperClasses;

namespace SIDWebV2.HelperClasses {
  [Serializable()]
  public class OpenPlay {
    private readonly List<WebLibs.WagerItem> _webOpenGameWagerItems;

    public bool KeepOpenPlay { get; set; }
    public int OpenTicketNumber { get; set; }
    public int OpenWagerNumber { get; set; }

    public int OpenPlayCount { get; set; }

    public List<WebLibs.WagerItem> WebOpenGameWagerItems {
      get { return _webOpenGameWagerItems; }
    }

    public OpenPlay() {
      _webOpenGameWagerItems = new List<WebLibs.WagerItem>();
    }

    public void Reset() {
      OpenTicketNumber = 0;
      OpenWagerNumber = 0;
      Clear();
    }

    public bool Any() {
      return _webOpenGameWagerItems != null && _webOpenGameWagerItems.Any();
    }

    public int Count() {
      return _webOpenGameWagerItems.Count;
    }

    public void Clear() {
      _webOpenGameWagerItems.Clear();

    }

    public void Add(WebLibs.WagerItem newOpenWagerItem) {
      _webOpenGameWagerItems.Add(newOpenWagerItem);
    }

    public List<WebLibs.WagerItem> ToList() {
      return _webOpenGameWagerItems;
    }

    public WebLibs.WagerItem Get(int index) {
      return _webOpenGameWagerItems[index];
    }

    public void Set(WebLibs.WagerItem wagerItem, int? index = null) {
      if (index == null) Add(wagerItem);
      else _webOpenGameWagerItems[(int) index] = wagerItem;
    }


  }
}