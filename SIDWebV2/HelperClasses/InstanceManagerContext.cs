﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InstanceManager.Entities;

namespace InstanceManager.BusinessLayer {
  class InstanceManagerContext : Core {

    public InstanceManagerContext() {
      Context = new InstanceManagerEntities {CommandTimeout = 120};
    }

    public DateTime? ServerDateTime {
      get {
        var dateTime = ((InstanceManagerEntities)Context).spPrmGetServerDateTime().FirstOrDefault();
        return dateTime ?? DateTime.Now;
      }
    }

    public List<spIMGetApplicationExtensions_Result> GetApplicationExtensions(int applicationId, int instanceId) {
      return ((InstanceManagerEntities)Context).spIMGetApplicationExtensions(applicationId, instanceId).ToList();
    }

    public tbApplicationsInUse GetAppToUse(int applicationid) {
      if (Context == null) return null;
      return (from i in ((InstanceManagerEntities)Context).tbInstances
                    join a in ((InstanceManagerEntities)Context).tbApplicationsInUses on i.InstanceId equals a.InstanceId
                    where a.UserName.Trim() == Environment.UserName && a.ApplicationId == applicationid && a.RunDateTime == null //&& i.InstanceId == _defaultInstance
                    select a).FirstOrDefault();
    }

    public void DeleteAppInUse(decimal? applicationInUseRecordId) {
      if (Context == null) return;
      if (applicationInUseRecordId == null) return;
      var appsInUse = (from p in ((InstanceManagerEntities)Context).tbApplicationsInUses where p.Id == applicationInUseRecordId select p).FirstOrDefault();

      if (appsInUse == null) return;
      ((InstanceManagerEntities)Context).tbApplicationsInUses.DeleteObject(appsInUse);
      Context.SaveChanges();
    }

    public String GetDatabaseInUse(int applicationId, int defaultInstance, Boolean callFromWeb = false) {
      if (Context == null) return null;
      String instanceDbName;
      if (!callFromWeb) {
        instanceDbName = (from i in ((InstanceManagerEntities)Context).tbInstances
                          join a in ((InstanceManagerEntities)Context).tbApplicationsInUses on i.InstanceId equals a.InstanceId
                          where a.UserName.Trim() == Environment.UserName && a.ApplicationId == applicationId && a.RunDateTime == null //&& i.InstanceId == _defaultInstance
                          select i.DatabaseName).FirstOrDefault();
      }
      else {
        instanceDbName = (from i in ((InstanceManagerEntities)Context).tbInstances
                          where i.InstanceId == defaultInstance
                          select i.DatabaseName).FirstOrDefault();
      }
      return instanceDbName;
    }

    public InstanceInfo GetDatabaseInUseOrDefault(String applicationName, int instanceId, Boolean callFromWeb = false) {
      if (Context == null) return null;
      InstanceInfo instanceDbInfo = null;
      if (!callFromWeb) {
        instanceDbInfo = (from i in ((InstanceManagerEntities)Context).tbInstances
                          join a in ((InstanceManagerEntities)Context).tbApplicationsInUses on i.InstanceId equals a.InstanceId
                          join b in ((InstanceManagerEntities)Context).tbApplications on a.ApplicationId equals b.ApplicationId
                          where a.UserName.Trim().ToUpper() == Environment.UserName.Trim().ToUpper() && a.MachineName.Trim().ToUpper() == Environment.MachineName.Trim().ToUpper() && b.Name.Trim().ToUpper() == applicationName.Trim().ToUpper() && a.InstanceId == instanceId
                          select new InstanceInfo { DatabaseName = i.DatabaseName, InstanceName = i.Name, ServerName = i.ServerName, DbUser = i.DbUser, DbPassword = i.DbPassword, DSN = i.DbDSN, IsProduction = i.IsProduction }).FirstOrDefault();
      }
      if (callFromWeb || instanceDbInfo == null) {
        instanceDbInfo = (from i in ((InstanceManagerEntities)Context).tbInstances
                          where i.InstanceId == instanceId
                          select new InstanceInfo { DatabaseName = i.DatabaseName, ServerName = i.ServerName, DbUser = i.DbUser, DbPassword = i.DbPassword, DSN = i.DbDSN, IsProduction = i.IsProduction }).FirstOrDefault();
      }
      return instanceDbInfo;
    }

    public List<spIMGetSystemInstances_Result> GetInstancesList(int systemId) {
      List<spIMGetSystemInstances_Result> dbs = null;
      try {
        if (Context != null)
          dbs = ((InstanceManagerEntities)Context).spIMGetSystemInstances(systemId).ToList();
      }
      catch {
        // ignored
      }
      return dbs;
    }

    public List<spIMGetSystemApplications_Result> GetModulesList(int systemId) {
      List<spIMGetSystemApplications_Result> modules = null;

      if (Context != null)
        modules = ((InstanceManagerEntities)Context).spIMGetSystemApplications(systemId).ToList();

      return modules;
    }

    public List<spIMGetSystems_Result> GetSystemsList() {
      List<spIMGetSystems_Result> dbs = null;

      if (Context != null) {
        dbs = ((InstanceManagerEntities)Context).spIMGetSystems().ToList();

      }
      return dbs;
    }

    public decimal? RequestAppInUse(int instanceId, int applicationId) {
      if (Context != null) {
        return ((InstanceManagerEntities)Context).spIMInsertApplicationInUse(
            Environment.UserName,
            Environment.MachineName,
            instanceId,
            applicationId
            ).FirstOrDefault();
      }
      return null;
    }

    public decimal? SetAppInUse(int instanceId, int applicationId) {
      if (Context != null) {
        return ((InstanceManagerEntities)Context).spIMInsertApplicationInUse(
            Environment.UserName,
            Environment.MachineName,
            instanceId,
            applicationId
            ).FirstOrDefault();
      }
      return null;
    }

    public void UpdateAppInUse(decimal? applicationInUseRecordId, DateTime currentDateTime) {
      if (Context == null) return;
      var appInUse = (from ap in ((InstanceManagerEntities)Context).tbApplicationsInUses where ap.Id == applicationInUseRecordId select ap).FirstOrDefault();
      if (appInUse != null) appInUse.RunDateTime = currentDateTime;
      Context.SaveChanges();
    }

    public void LockSystemForMaintenance(int systemId, byte[] maintenanceSettings) {
      if (Context == null) return;
      var targetSystem = (from sy in ((InstanceManagerEntities)Context).tbSystems where sy.SystemId == systemId select sy).FirstOrDefault();
      if (targetSystem != null) {
        targetSystem.FlagForMaintenance = true;
        targetSystem.MaintenanceSettings = Encoding.UTF8.GetString(maintenanceSettings);
      }
      Context.SaveChanges();
    }

    public void UnlockSystemFromMaintenance(int systemId) {
      if (Context == null) return;
      var targetSystem = (from sy in ((InstanceManagerEntities)Context).tbSystems where sy.SystemId == systemId select sy).FirstOrDefault();
      if (targetSystem != null) {
        targetSystem.FlagForMaintenance = false;
        //targetSystem.MaintenanceSettings = null;
      }

      Context.SaveChanges();
    }

    public ModuleInfo GetModuleInfo(int applicationId, int defaultInsanceId) {
      ModuleInfo moduleInfo = null;

      if (Context != null) {
        try {
          moduleInfo = (from i in ((InstanceManagerEntities)Context).tbInstances
                        join s in ((InstanceManagerEntities)Context).tbSystems on i.SystemId equals s.SystemId
                        join p in ((InstanceManagerEntities)Context).tbApplications on s.SystemId equals p.SystemId
                        where p.ApplicationId == applicationId && i.InstanceId == defaultInsanceId
                        select new ModuleInfo { Id = p.ApplicationId, Name = p.Name, Description = p.Description, ExeName = p.ExeName, ActiveSharedFolder = i.AppsPath, ShortName = p.ShortName, Icon = p.Icon, IconIdx = p.IconIndex, FlagForMaintenance = s.FlagForMaintenance }).FirstOrDefault();
        }
        catch {
          return moduleInfo;
        }

      }
      if (moduleInfo == null) return null;
      moduleInfo.WindowsUserId = Environment.UserName;
      moduleInfo.MachineId = Environment.MachineName;
      return moduleInfo;
    }

  }
}
