﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using SIDLibraries.Utilities;
using SIDWebV2.Services;
using LineOfferingWeb = SIDWebV2.HelperClasses.LineOffering;
using LineOfferingBL = SIDLibraries.BusinessLayer.LineOffering;
using WebLibs = SIDWebLibraries.HelperClasses;

namespace SIDWebV2.HelperClasses {

  public class WagerValidation {

    #region Public Properties

    private static ModuleInfo ModuleInformation {
      get {
        return WebLibs.SessionVariablesCore.InstanceManager != null ? WebLibs.SessionVariablesCore.InstanceManager.InstanceModuleInfo : null;
      }
    }

    /*public static DateTime? ServerDateTime {
      get {
        using (var prm = new SystemParameters(ModuleInformation)) {
          return prm.GetServerDateTime();
        }
      }
    }*/

    #endregion

    #region Structures

    /*public struct WagerOptionAndValue {
      public string PropertyName;
      public string PropertyValue;
    }*/

    public struct DateRange {
      public DateTime StartingDate;
      public DateTime EndingDate;
    }

    public class WagerItemUpdateData {
      public string EventOfferingType { get; set; } // C = Contest, G = Game
      public double? ArAmount { get; set; }
      public double RiskAmount { get; set; }
      public double ToWinAmount { get; set; }
      public double WagerAmt { get; set; }
      public string ControlCode { get; set; }
      public int FinalPrice { get; set; }
      public double? FinalLine { get; set; }
      public double? RrAmount { get; set; }
      public int GameNum { get; set; }
      public int PeriodNumber { get; set; }
      public bool Pitcher1ReqFlag { get; set; }
      public bool Pitcher2ReqFlag { get; set; }
      public int RoundRobinValue { get; set; }
      public int PlayCount { get; set; }
      public readonly WebLibs.WagerItem.AmountEnteredType AmountEntered = WebLibs.WagerItem.AmountEnteredType.None;

      public int ContestNum { get; set; }
      public int ContestantNum { get; set; }
      public int ToBase { get; set; }
    }

    public class ReturnData {
      public int Code { get; set; }
      public string Message { get; set; }

      public ReturnData(int code, string message) {
        Code = code;
        Message = message;
      }
      public ReturnData() { }

    }

    #endregion

    #region Constructors

    /*public stWagerValidation() {
      UpdateCustomerInfo();
      Console.WriteLine("With socket server running; this might not be needed.");
      WebLibs.WebCustomerBalance.LoadCustomerAvailableBalance(WebLibs.SessionVariablesCore.CustomerInfo.CustomerID, WebLibs.SessionVariablesCore.CustomerInfo);

      using (var cust = new Customers(WebLibs.SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
        if (WebLibs.SessionVariablesCore.CustomerInfo != null)
          WebLibs.SessionVariablesCore.CustomerSettings = cust.GetWebCustomerSettings(WebLibs.SessionVariablesCore.CustomerInfo.CustomerID);
      }
    }*/

    #endregion

    #region Public Methods

    private static bool CheckMaximumContestantWager(ContestantWagerItem cwi, Boolean accumulateRisk, out String errorMsg) {
      if (accumulateRisk) {
        GetAccumulatedContestantAmounts(cwi.Loo.ContestNum, cwi.Loo.ContestantNum);
      }
      else {
        WebLibs.SessionVariablesCore.AccumRisk = 0;
        WebLibs.SessionVariablesCore.AccumToWin = 0;
      }
      if (cwi.RiskAmt <= cwi.ToWinAmt) {
        if (cwi.RiskAmt + WebLibs.SessionVariablesCore.AccumRisk > WebLibs.SessionVariablesCore.MaxWagerLimit / 100) {
          cwi.ValidationErrorMessage = errorMsg = WebLibs.SessionVariablesCore.MaxWagerLimitDesc;
          return false;

        }
      }
      else {
        if (cwi.ToWinAmt + WebLibs.SessionVariablesCore.AccumToWin > WebLibs.SessionVariablesCore.MaxWagerLimit / 100) {
          cwi.ValidationErrorMessage = errorMsg = WebLibs.SessionVariablesCore.Translate("ACCUM_WAGER_LIM_EXCEEDED");
          return false;
        }
      }
      errorMsg = "";
      return true;
    }

    public static void SetMaximumWagerLimit(string wagerType, WebLibs.WagerItem wi, double contestantMaxBet = 0) {
      if (wagerType == "C") WebLibs.SessionVariablesCore.MaxWagerLimit = WebLibs.SessionVariablesCore.WagerLimitValidator.GetMaximumContestLimit(contestantMaxBet).Limit;
      else  //Este if siguiente trata los wager indivualmente y no acumulados, el else los trata en grupos, no es necesario porque angular se hará cargo de ver el límite menor
        /* if ("S,L,M,E,I".Contains(wagerType))*/
        wi.MaxWagerLimit = WebLibs.SessionVariablesCore.MaxWagerLimit = WebLibs.SessionVariablesCore.WagerLimitValidator.GetMaximumWagerLimit(wi.Loo, wagerType, wi.WagerType, WebLibs.SessionVariablesCore.WagerType.Code == "P" ? WebLibs.SessionVariablesCore.CustomerInfo.EnforceParlayMaxBetFlag == "Y" : (WebLibs.SessionVariablesCore.WagerType.Code == "T" && WebLibs.SessionVariablesCore.CustomerInfo.EnforceTeaserMaxBetFlag == "Y")).Limit;
      //else SetMaximumWagerLimit(wagerType, contestantMaxBet);
      WebLibs.CommonFunctions.SetMaximumWagerLimitDescription(wagerType, WebLibs.SessionVariablesCore.WagerLimitValidator.WagerLimitType);
    }

    private static void SetMaximumWagerLimit(WebWager wager, string wagerType, double contestantMaxBet = 0) {
      if (wagerType == "C") WebLibs.SessionVariablesCore.MaxWagerLimit = WebLibs.SessionVariablesCore.WagerLimitValidator.GetMaximumContestLimit(contestantMaxBet).Limit;
      else {
        var wagersInfo = wager.WebGameWagerItems.Select(a => new WagerLimitValidator.WagerItemLimitsInfo {
          SportType = a.Loo.SportType,
          SportSubType = a.Loo.SportSubType,
          WagerType = a.WagerType,
          PeriodNumber = a.Loo.PeriodNumber,
          CircledMaxWagerMoneyLine = a.Loo.CircledMaxWagerMoneyLine,
          CircledMaxWagerMoneyLineType = a.Loo.CircledMaxWagerMoneyLineType,
          CircledMaxWagerSpread = a.Loo.CircledMaxWagerSpread,
          CircledMaxWagerSpreadType = a.Loo.CircledMaxWagerSpreadType,
          CircledMaxWagerTeamTotal = a.Loo.CircledMaxWagerTeamTotal,
          CircledMaxWagerTeamTotalType = a.Loo.CircledMaxWagerTeamTotalType,
          CircledMaxWagerTotal = a.Loo.CircledMaxWagerTotal,
          CircledMaxWagerTotalType = a.Loo.CircledMaxWagerTotalType,
          GameStatus = a.Loo.Status,
          GameNum = a.Loo.GameNum,
          ChosenTeamId = a.ChosenTeamId
        }).ToList();
        WebLibs.SessionVariablesCore.MaxWagerLimit = WebLibs.SessionVariablesCore.WagerLimitValidator.GetMaximumWagerLimit(wagersInfo, wagerType).Limit;
        foreach (var wagerItem in wager.WebGameWagerItems) {
          wagerItem.MaxWagerLimit = (from w in wagersInfo
                                     where w.GameNum == wagerItem.Loo.GameNum &&
                                     w.PeriodNumber == wagerItem.Loo.PeriodNumber && wagerItem.ChosenTeamId == w.ChosenTeamId &&
                                     w.WagerType == wagerItem.WagerType
                                     select w.MaxWagerLimit).FirstOrDefault();
        }
      }
      WebLibs.CommonFunctions.SetMaximumWagerLimitDescription(wagerType, WebLibs.SessionVariablesCore.WagerLimitValidator.WagerLimitType);
    }

    #endregion

    #region Private Methods

    public static WebLibs.ResultContainer ValidateStraightAndContestBets(WebWager wager) {
      string itemReturnedMsg;
      int wagerPos, contestPos;
      WebLibs.WagerItem wagerItem;
      ContestantWagerItem contestItem;

      if (!MaxWagerLimitsOk(wager, out itemReturnedMsg, out wagerItem, out contestItem))
        return new WebLibs.ResultContainer(new { WagerItem = wagerItem, ContestItem = contestItem }, WebLibs.ResultContainer.ResultCode.Warning, itemReturnedMsg);

      WebLibs.SessionVariablesCore.MaxRiskAmount = wager.HasGameWagers ? wager.WebGameWagerItems.Sum(p => p.RiskAmt) : 0;
      WebLibs.SessionVariablesCore.MaxRiskAmount += wager.HasContestantWagers ? wager.WebContestantWagerItems.Sum(p => p.RiskAmt) : 0;

      if (WebLibs.SessionVariablesCore.FreePlay) {
        if (WebLibs.SessionVariablesCore.MaxRiskAmount > WebLibs.SessionVariablesCore.CustomerInfo.FreePlayBalance / 100 && !SessionVariables.Ticket.OpenPlay.Any()) {
          WebLibs.SystemLog.WriteAccessLog("Attempting to Place Bet [FreePlay]", "Customer has insufficient free play funds.");
          return new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Warning, WebLibs.SessionVariablesCore.Translate("INSUFFICIENT_FP_FUNDS"));
        }
      }
      else {
        if (WebLibs.SessionVariablesCore.MaxRiskAmount > WebLibs.SessionVariablesCore.AvailableBalance / 100 && !SessionVariables.Ticket.OpenPlay.Any()) {
          WebLibs.SystemLog.WriteAccessLog("Attempting to Place Bet", "Customer has insufficient available balance.");
          return new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Warning, WebLibs.SessionVariablesCore.Translate("INSUFFICIENT_BET_BALANCE"));
        }
      }

      if (WebLibs.SessionVariablesCore.CustomerInfo.AccessedBy != null) {
        WebLibs.SystemLog.WriteAccessLog("Attempting to Place Bet [AccessedBy]", "Attempting to place bet while on the phone with a ticket writer.");
        return new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Warning, WebLibs.SessionVariablesCore.Translate("BETS_ON_THE_PHONE"));
      }

      WebLibs.SessionVariablesCore.FixedWagerFlag = false;
      String errorMsg;
      if (!CheckGameLines(wager, out errorMsg)) {
        WebLibs.SystemLog.WriteAccessLog("Attempting to Place Bet [CheckGameLines]", errorMsg);
        return string.IsNullOrEmpty(errorMsg) ?
              new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Warning, WebLibs.SessionVariablesCore.Translate("LINE_HAS_CHANGED")) :
              new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Warning, errorMsg);
      }

      //Second Mins validation added, CheckGamelines could set zeros in Risk/ToWin 
      if (!MinWagerLimitsOk(wager, out itemReturnedMsg, out wagerPos))
        return new WebLibs.ResultContainer(wagerPos, WebLibs.ResultContainer.ResultCode.Warning, itemReturnedMsg);

      if (WebLibs.SessionVariablesCore.FixedWagerFlag) {
        WebLibs.SystemLog.WriteAccessLog("Attempting to Place Bet [FixedWagerFlag]", "Redirected due to changed line in Straight Bet. " + errorMsg);
        return new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Warning, WebLibs.SessionVariablesCore.Translate("BUYING_PTS_LINE_CHANGED"));
      }

      if (!ValidateBuyPointsOptions(wager)) {
        WebLibs.SystemLog.WriteAccessLog("Attempting to Place Bet", "Buying Points - line has changed - timeout.");
        return new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Warning, WebLibs.SessionVariablesCore.Translate("BUYING_PTS_LINE_CHANGED"));
      }

      WebLibs.SessionVariablesCore.FixedWagerFlag = false;
      if (!CheckContestLines(wager, out errorMsg)) {
        WebLibs.SystemLog.WriteAccessLog("Attempting to Place Bet [CheckContestLines]", errorMsg);
        return new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Warning, errorMsg);
      }

      if (WebLibs.SessionVariablesCore.FixedWagerFlag) {
        WebLibs.SystemLog.WriteAccessLog("Attempting to Place Bet [FixedWagerFlag]", "Redirected due to changed line in Contest.");
        return new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Warning, WebLibs.SessionVariablesCore.Translate("LINE_HAS_CHANGED"));
      }

      using (var gL = new GamesAndLines(ModuleInformation)) {
        if (WebLibs.Utilities.CheckForGameGrade(gL, wager.WebGameWagerItems)) {
          WebLibs.SystemLog.WriteAccessLog("Attempting to Place Bet [CheckForGameGrade]", "Game/Period already graded.");
          return new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Fail, WebLibs.SessionVariablesCore.Translate("GAME_GRADED"));
        }
      }

      if (!wager.HasGameWagers) return new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Success, WebLibs.SessionVariablesCore.Translate("WAGER_ACCEPTED"));
      if (!WebLibs.SessionVariablesCore.IsMover) {
        AddActionAlertInfo(wager,"1");
        WebLibs.SystemLog.WriteAccessLog("Validating Wager", "Action Alert Sent");
      }
      if (!WebLibs.CommonFunctions.DelayWagerInsertion(WebLibs.SessionVariablesCore.ConfirmationDelay))
        return new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Success, WebLibs.SessionVariablesCore.Translate("WAGER_ACCEPTED"));

      WebLibs.SessionVariablesCore.FixedWagerFlag = false;
      if (!CheckGameLines(wager, out errorMsg)) {
        WebLibs.SystemLog.WriteAccessLog("Attempting to Place Bet [CheckGameLines]", errorMsg);

        return string.IsNullOrEmpty(errorMsg) ?
          new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Warning, WebLibs.SessionVariablesCore.Translate("LINE_HAS_CHANGED")) :
          new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Warning, errorMsg);
      }

      //Third Mins validation added, CheckGamelines could set zeros in Risk/ToWin 
      if (!MinWagerLimitsOk(wager, out itemReturnedMsg, out wagerPos))
        return new WebLibs.ResultContainer(wagerPos, WebLibs.ResultContainer.ResultCode.Warning, itemReturnedMsg);

      if (!WebLibs.SessionVariablesCore.FixedWagerFlag) return new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Success, WebLibs.SessionVariablesCore.Translate("WAGER_ACCEPTED"));
      WebLibs.SystemLog.WriteAccessLog("Attempting to Place Bet [FixedWagerFlag]", "Redirected due to changed line in Contest.");
      return new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Warning, WebLibs.SessionVariablesCore.Translate("LINE_HAS_CHANGED"));
    }

    public static WebLibs.ResultContainer ValidateAccumWagerItems(List<WagerItemUpdateData> wagersData, WebWager wager) {
      WebLibs.ResultContainer validationResult;
      WebLibs.SessionVariablesCore.RiskAmount = WebLibs.SessionVariablesCore.WagerType.Name == WagerType.ACTREVERSENAME ? wagersData[0].ArAmount ?? 0 : wagersData[0].RiskAmount;
      WebLibs.SessionVariablesCore.ToWinAmount = wagersData[0].ToWinAmount;
      var i = 0;
      var allWagers = new List<WebLibs.WagerItem>();
      WebLibs.WagerItem wItem;
      ContestantWagerItem contestItem;
      switch (WebLibs.SessionVariablesCore.WagerType.Name) {
        case WagerType.IFWINONLYNAME:
        case WagerType.IFWINORPUSHNAME:
          if (wagersData.Count() < 2 || wagersData.Count() > 6) {
            validationResult = new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Fail, WebLibs.SessionVariablesCore.Translate("INVALID_NUMBER_OF_ITEMS"));
            WebLibs.SystemLog.WriteAccessLog("Attempting to Place a " + WebLibs.SessionVariablesCore.WagerType.Name, validationResult.Message);
            return validationResult;
          }

          foreach (var wiUpd in wagersData) {
            if (wiUpd.WagerAmt <= 0) {
              validationResult = new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Warning, WebLibs.SessionVariablesCore.Translate("INCORRECT_WAGER_AMT"));
              WebLibs.SystemLog.WriteAccessLog("Attempting to Place a " + WebLibs.SessionVariablesCore.WagerType.Name, validationResult.Message);
              return validationResult;
            }
            wager.WebGameWagerItems[i].RiskAmt = wiUpd.WagerAmt;
            CalculateWiWagerAmount(wager.WebGameWagerItems[i++]);
          }

          WebLibs.SessionVariablesCore.MaxRiskAmount = CalculateMaxRisk(wager, false);

          string itemReturnedMsg;
          int wagerPos, contestPos;
          if (!MaxWagerLimitsOk(wager, out itemReturnedMsg, out wItem, out contestItem))
            return new WebLibs.ResultContainer(new { WagerItem = wItem, ContestItem = contestItem }, WebLibs.ResultContainer.ResultCode.Warning, itemReturnedMsg);

          break;
        case WagerType.ACTREVERSENAME:
          if (wagersData.Count() != 2) {
            validationResult = new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Fail, WebLibs.SessionVariablesCore.Translate("INVALID_NUMBER_OF_ITEMS"));
            WebLibs.SystemLog.WriteAccessLog("Attempting to Place a " + WebLibs.SessionVariablesCore.WagerType.Name, validationResult.Message);
            return validationResult;
          }

          double maxToWinAmount = 0;
          foreach (var wagerItem in wager.WebGameWagerItems) {
            if (wagerItem.FinalPrice < 0) {
              wagerItem.RiskAmt = 0;
              wagerItem.AmountEntered = WebLibs.WagerItem.AmountEnteredType.ToWinAmt;
            }
            else {
              wagerItem.ToWinAmt = 0;
              wagerItem.AmountEntered = WebLibs.WagerItem.AmountEnteredType.RiskAmt;
            }
            CalculateWiWagerAmount(wagerItem);
            SetMaximumWagerLimit(wagerItem.WagerType, wagerItem);

            maxToWinAmount += wagerItem.ToWinAmt * 2;
          }
          WebLibs.SessionVariablesCore.MaxRiskAmount1 = CalculateMaxRisk(wager, false);
          WebLibs.SessionVariablesCore.MaxRiskAmount2 = CalculateMaxRisk(wager, true);
          WebLibs.SessionVariablesCore.MaxRiskAmount = WebLibs.SessionVariablesCore.MaxRiskAmount1 + WebLibs.SessionVariablesCore.MaxRiskAmount2;
          WebLibs.SessionVariablesCore.MaxToWinAmount = maxToWinAmount;

          break;
        case WagerType.PARLAYNAME:
          if (WebLibs.SessionVariablesCore.RoundRobinValue == 0) WebLibs.SessionVariablesCore.PlayCount = 1;
          //Allan kevin's translations
          WebLibs.SessionVariablesCore.MaxRiskAmount = WebLibs.SessionVariablesCore.RiskAmount * WebLibs.SessionVariablesCore.PlayCount;
          WebLibs.SessionVariablesCore.MaxRiskAmount = WebLibs.SessionVariablesCore.RiskAmount;
          SetMaximumWagerLimit(wager, "P");
          if (wagersData.Any(wiUpd => wiUpd.WagerAmt <= 0)) {
            validationResult = new WebLibs.ResultContainer(null,
                WebLibs.ResultContainer.ResultCode.Warning,
                WebLibs.SessionVariablesCore.Translate("INCORRECT_WAGER_AMT"));
            WebLibs.SystemLog.WriteAccessLog(
                "Attempting to Place a " + WebLibs.SessionVariablesCore.WagerType.Name,
                validationResult.Message);
            return validationResult;
          }
          foreach (var wiUpd in wager.WebGameWagerItems) {
            wiUpd.RiskAmt = wagersData[0].WagerAmt;
          }
          allWagers.AddRange(wager.WebGameWagerItems);
          allWagers.AddRange(wager.OpenPlay.WebOpenGameWagerItems);
          WebLibs.SessionVariablesCore.ToWinAmount = CalculateLocalParlayToWinAmount(allWagers);
          break;
        case WagerType.TEASERNAME:
          var playCount = (wager.OpenPlay.KeepOpenPlay
                        ? wager.OpenPlay.OpenPlayCount < 999 ? wager.OpenPlay.OpenPlayCount : wager.OpenPlay.Count() + wagersData.Count
                        : wagersData.Count);
          WebLibs.SessionVariablesCore.MaxRiskAmount = WebLibs.SessionVariablesCore.RiskAmount;
          SetMaximumWagerLimit(wager,"T");
          if (wagersData.Any(wiUpd => wiUpd.WagerAmt <= 0)) {
            validationResult = new WebLibs.ResultContainer(null,
                WebLibs.ResultContainer.ResultCode.Warning,
                WebLibs.SessionVariablesCore.Translate("INCORRECT_WAGER_AMT"));
            WebLibs.SystemLog.WriteAccessLog(
                "Attempting to Place a " + WebLibs.SessionVariablesCore.WagerType.Name,
                validationResult.Message);
            return validationResult;
          }
          foreach (var wiUpd in wager.WebGameWagerItems) {
            wiUpd.RiskAmt = wagersData[0].WagerAmt;
          }
          CalculateTeaserWiWagerAmount(wager.WebGameWagerItems[i++], playCount);

          WebLibs.SessionVariablesCore.ToWinAmount = wager.WebGameWagerItems[0].ToWinAmt;
          break;
      }

      if (WebLibs.SessionVariablesCore.FreePlay) {
        if (!((wagersData[0].RrAmount ?? WebLibs.SessionVariablesCore.MaxRiskAmount) > WebLibs.SessionVariablesCore.CustomerInfo.FreePlayBalance / 100))
          return new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Success, WebLibs.SessionVariablesCore.Translate("WAGER_ACCEPTED"));
        validationResult = new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Warning, WebLibs.SessionVariablesCore.Translate("INSUFFICIENT_FP_FUNDS"));
        WebLibs.SystemLog.WriteAccessLog("Attempting to Place a " + WebLibs.SessionVariablesCore.WagerType.Name, validationResult.Message);
        return validationResult;
      }
      if (wager.WebGameWagerItems[0].RifTicketNumber == 0) {
        if ((wagersData[0].RrAmount ?? WebLibs.SessionVariablesCore.MaxRiskAmount) > WebLibs.SessionVariablesCore.AvailableBalance / 100 && !wager.OpenPlay.Any()) {
          validationResult = new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Warning, WebLibs.SessionVariablesCore.Translate("INSUFFICIENT_BET_BALANCE"));
          WebLibs.SystemLog.WriteAccessLog("Attempting to Place a " + WebLibs.SessionVariablesCore.WagerType.Name, validationResult.Message);
          return validationResult;
        }
      }
      else {
        if (WebLibs.SessionVariablesCore.MaxRiskAmount > 0) {
          validationResult = new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Fail, WebLibs.SessionVariablesCore.Translate("RIF_NOT_IMPLEMENTED_YET"));
          WebLibs.SystemLog.WriteAccessLog("Attempting to Place a " + WebLibs.SessionVariablesCore.WagerType.Name, validationResult.Message);
          return validationResult;
        }
      }

      if (WebLibs.SessionVariablesCore.DisplayNightCircleMoneyLimit ||
          WebLibs.SessionVariablesCore.DisplayNightCircleSpreadLimit ||
          WebLibs.SessionVariablesCore.DisplayNightCircleTotalsLimit ||
                WebLibs.SessionVariablesCore.DisplayNightCircleTeamTotalsLimit) {
        WebLibs.SessionVariablesCore.MaxWagerLimit = WebLibs.SessionVariablesCore.EveningGameLimit * WebLibs.SessionVariablesCore.ExchangeRate;
      }

      double currentRiskAmount;

      validationResult = ValidateSpecialParlayTeaserAccess(wager, WebLibs.SessionVariablesCore.WagerType.Name, out currentRiskAmount);

      if (validationResult.Code != WebLibs.ResultContainer.ResultCode.Success)
        return validationResult;

      if (currentRiskAmount == 0)
        currentRiskAmount = WebLibs.SessionVariablesCore.MaxRiskAmount;



      if (WebLibs.SessionVariablesCore.WagerType.Name == WagerType.IFWINONLYNAME ||
          WebLibs.SessionVariablesCore.WagerType.Name == WagerType.IFWINORPUSHNAME) {

            foreach (var riskAmt in (from wi in wager.WebGameWagerItems where wi.RiskAmt > WebLibs.SessionVariablesCore.MaxWagerLimit / 100 select wi.RiskAmt)) {
          validationResult = new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Warning, WebLibs.SessionVariablesCore.Translate("WAGER_LIMIT_EXCEEDED") + " " +
                                                                                           DisplayNumber.Format(riskAmt, false, 1, true) + " " + WebLibs.SessionVariablesCore.CurrencyCode + " " + WebLibs.SessionVariablesCore.Translate("WAGER_LIMIT_IS") + " " + DisplayNumber.Format(WebLibs.SessionVariablesCore.MaxWagerLimit ?? 0, true, 100, true) + " " + WebLibs.SessionVariablesCore.CurrencyCode);
          WebLibs.SystemLog.WriteAccessLog("Attempting to Place a " + WebLibs.SessionVariablesCore.WagerType.Name, validationResult.Message);
          return validationResult;
        }

            foreach (var riskAmt in (from wi in wager.WebGameWagerItems
                                 where wi.RiskAmt * 100 < WebLibs.SessionVariablesCore.MinimumWagerAmt
                                 select wi.RiskAmt)) {
          validationResult = new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Warning,
              WebLibs.SessionVariablesCore.Translate("BELOW_MINIMUM") + " " +
              DisplayNumber.Format(riskAmt, false, 1, true) + " " + WebLibs.SessionVariablesCore.CurrencyCode +
              " " + WebLibs.SessionVariablesCore.Translate("WAGER_LIMIT_IS") + " " +
              DisplayNumber.Format(WebLibs.SessionVariablesCore.MinimumWagerAmt, true, 100, true) + " " +
              WebLibs.SessionVariablesCore.CurrencyCode);
          WebLibs.SystemLog.WriteAccessLog(
              "Attempting to Place a " + WebLibs.SessionVariablesCore.WagerType.Name, validationResult.Message);
          return validationResult;
        }
      }
      else if (!SessionVariables.Ticket.OpenPlay.Any()) {
        var playCount = (SessionVariables.Ticket.OpenPlay.KeepOpenPlay
            ? SessionVariables.Ticket.OpenPlay.OpenPlayCount
            : wagersData.Count);
        var toEval = (WebLibs.SessionVariablesCore.WagerType.Name == WagerType.TEASERNAME &&
                      (playCount == 2 || WebLibs.SessionVariablesCore.TeaserName.Contains("SPECIAL"))
            ? WebLibs.SessionVariablesCore.ToWinAmount
            : WebLibs.SessionVariablesCore.RiskAmount);
        if (toEval * 100 > WebLibs.SessionVariablesCore.MaxWagerLimit) {
          validationResult = new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Warning, WebLibs.SessionVariablesCore.Translate("WAGER_LIMIT_EXCEEDED") + " " +
                                                                                           DisplayNumber.Format(toEval, false, 1, true) + " " + WebLibs.SessionVariablesCore.CurrencyCode + " " + WebLibs.SessionVariablesCore.Translate("WAGER_LIMIT_IS") + " " + DisplayNumber.Format(WebLibs.SessionVariablesCore.MaxWagerLimit ?? 0, true, 100, true) + " " + WebLibs.SessionVariablesCore.CurrencyCode);
          WebLibs.SystemLog.WriteAccessLog("Attempting to Place a " + WebLibs.SessionVariablesCore.WagerType.Name, validationResult.Message);
          return validationResult;
        }
        if (toEval * 100 < WebLibs.SessionVariablesCore.MinimumWagerAmt) {
          validationResult = new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Warning,
              WebLibs.SessionVariablesCore.Translate("BELOW_MINIMUM") + " " +
              DisplayNumber.Format(toEval, false, 1, true) + " " + WebLibs.SessionVariablesCore.CurrencyCode +
              " " + WebLibs.SessionVariablesCore.Translate("WAGER_LIMIT_IS") + " " +
              DisplayNumber.Format(WebLibs.SessionVariablesCore.MinimumWagerAmt, true, 100, true) + " " +
              WebLibs.SessionVariablesCore.CurrencyCode);
          WebLibs.SystemLog.WriteAccessLog(
              "Attempting to Place a " + WebLibs.SessionVariablesCore.WagerType.Name, validationResult.Message);
          return validationResult;
        }
      }

      WebLibs.SessionVariablesCore.FixedWagerFlag = false;
      String errorMsg;
      if (WebLibs.SessionVariablesCore.WagerType.Name == WagerType.GetFromWebCode(WagerType.WEBACTREVERSE).Name ||
        WebLibs.SessionVariablesCore.WagerType.Name == WagerType.GetFromWebCode(WagerType.WEBIFWINONLY).Name ||
              WebLibs.SessionVariablesCore.WagerType.Name == WagerType.GetFromWebCode(WagerType.WEBIFWINORPUSH).Name) {
        if (wager.HasGameWagers) {
          if (!WebLibs.SessionVariablesCore.IsMover) {
            AddActionAlertInfo(wager, "1");
            WebLibs.SystemLog.WriteAccessLog("Validating Accum Wager", "Action Alert Sent");
          }
          if (WebLibs.CommonFunctions.DelayWagerInsertion(WebLibs.SessionVariablesCore.ConfirmationDelay)) {
            WebLibs.SessionVariablesCore.FixedWagerFlag = false;
            if (CheckGameLines(wager, out errorMsg) == false) {

              validationResult = string.IsNullOrEmpty(errorMsg) ?
              new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Warning, WebLibs.SessionVariablesCore.Translate("LINE_HAS_CHANGED")) :
              new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Warning, errorMsg);
              WebLibs.SystemLog.WriteAccessLog("Attempting to Place a " + WebLibs.SessionVariablesCore.WagerType.Name + " [CheckGameLines]", validationResult.Message);

              return validationResult;
            }

            if (WebLibs.SessionVariablesCore.FixedWagerFlag) {
              validationResult = new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Fail, WebLibs.SessionVariablesCore.Translate("REDIRECTED_DUE_TO") + " " + WebLibs.SessionVariablesCore.WagerType.Name + ". " + errorMsg);
              WebLibs.SystemLog.WriteAccessLog("Attempting to Place a " + WebLibs.SessionVariablesCore.WagerType.Name, validationResult.Message);
              return validationResult;
            }
          }
        }
      }
      WebLibs.SessionVariablesCore.FixedWagerFlag = false;
      if (CheckGameLines(wager, out errorMsg) == false) {
        validationResult = string.IsNullOrEmpty(errorMsg) ?
              new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Warning, WebLibs.SessionVariablesCore.Translate("LINE_HAS_CHANGED")) :
              new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Warning, errorMsg);

        WebLibs.SystemLog.WriteAccessLog("Attempting to Place a " + WebLibs.SessionVariablesCore.WagerType.Name + " [CheckGameLines]", errorMsg);
        return validationResult;
      }

      if (WebLibs.SessionVariablesCore.FixedWagerFlag) {
        validationResult = new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Warning, WebLibs.SessionVariablesCore.Translate("REDIRECTED_DUE_TO") + " " + WebLibs.SessionVariablesCore.WagerType.Name + ". " + errorMsg);
        WebLibs.SystemLog.WriteAccessLog("Attempting to Place a " + WebLibs.SessionVariablesCore.WagerType.Name, validationResult.Message);
        return validationResult;
      }

      if (WebLibs.SessionVariablesCore.CustomerInfo.AccessedBy != null) {
        validationResult = new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Fail, WebLibs.SessionVariablesCore.Translate("BETS_ON_THE_PHONE"));
        WebLibs.SystemLog.WriteAccessLog("Attempting to Place a " + WebLibs.SessionVariablesCore.WagerType.Name, validationResult.Message);
        return validationResult;
      }

      using (var gL = new GamesAndLines(ModuleInformation)) {
        if (WebLibs.Utilities.CheckForGameGrade(gL, wager.WebGameWagerItems)) {
          validationResult = new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Fail, WebLibs.SessionVariablesCore.Translate("GAME_GRADED"));
          WebLibs.SystemLog.WriteAccessLog("Attempting to Place a " + WebLibs.SessionVariablesCore.WagerType.Name, validationResult.Message);
          return validationResult;
        }
      }

      if (wager.WebGameWagerItems.Count
          + (wager.OpenPlay.Any() ? wager.OpenPlay.Count() :
                +(wager.OpenPlay.KeepOpenPlay ? wager.OpenPlay.OpenPlayCount : 0)) < WebLibs.SessionVariablesCore.WagerMinPicks) {
        validationResult = new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Fail, WebLibs.SessionVariablesCore.Translate("INVALID_NUMBER_OF_ITEMS") + " " + WebLibs.SessionVariablesCore.WagerType.Name);
        WebLibs.SystemLog.WriteAccessLog("Attempting to Place a " + WebLibs.SessionVariablesCore.WagerType.Name, validationResult.Message);
        return validationResult;
      }
      return new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Success, WebLibs.SessionVariablesCore.Translate("WAGER_ACCEPTED"));
    }

    private static WebLibs.ResultContainer ValidateSpecialParlayTeaserAccess(WebWager wager, string wagerTypeName, out double currentRiskAmount) {
      currentRiskAmount = 0;
      if (wagerTypeName != WagerType.GetFromWebCode(WagerType.WEBPARLAY).Name &&
           wagerTypeName != WagerType.GetFromWebCode(WagerType.WEBTEASER).Name &&
           wagerTypeName != WagerType.GetFromWebCode(WagerType.WEBACTREVERSE).Name) return new WebLibs.ResultContainer();


      double accumAmt;
      if (wagerTypeName == WagerType.GetFromWebCode(WagerType.WEBPARLAY).Name || wagerTypeName == WagerType.GetFromWebCode(WagerType.WEBACTREVERSE).Name) accumAmt = WebLibs.SessionVariablesCore.RiskAmount;
      else accumAmt = WebLibs.SessionVariablesCore.RiskAmount < WebLibs.SessionVariablesCore.ToWinAmount ? WebLibs.SessionVariablesCore.RiskAmount : WebLibs.SessionVariablesCore.ToWinAmount;


      WebLibs.ResultContainer validationResult;
      if (accumAmt > WebLibs.SessionVariablesCore.MaxWagerLimit / 100) {
        validationResult = new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Warning, WebLibs.SessionVariablesCore.MaxWagerLimitDesc + " " + DisplayNumber.Format(accumAmt, false, 1, true) + " " + WebLibs.SessionVariablesCore.CurrencyCode + " " + WebLibs.SessionVariablesCore.Translate("WAGER_LIMIT_IS") + " " + DisplayNumber.Format(WebLibs.SessionVariablesCore.MaxWagerLimit ?? 0, true, 100, true) + " " + WebLibs.SessionVariablesCore.CurrencyCode);
        WebLibs.SystemLog.WriteAccessLog("Attempting to Place a " + wagerTypeName, validationResult.Message);
        return validationResult;
      }

      var wagers = new List<TicketsAndWagers.GameDataForAccumRiskAmount>();
      var cnt = 0;
      foreach (var wi in wager.WebGameWagerItems) {
        if (cnt == 16) break;
        wagers.Add(new TicketsAndWagers.GameDataForAccumRiskAmount {
          GameNum = wi.Loo.GameNum,
          PeriodNumber = wi.Loo.PeriodNumber,
          WagerType = wi.WagerType,
          ChosenTeamId = wi.ChosenTeamId,
          TotalPointsOu = wi.TotalPointsOu
        });

        cnt++;
      }

      var comboWagers = new List<TicketsAndWagers.GameDataForAccumRiskAmount>();
      cnt = 0;
      var items = new List<WebLibs.WagerItem>();
      items.AddRange(wager.WebGameWagerItems);
      if (SessionVariables.Ticket.OpenPlay.Any()) {
        items.AddRange(wager.OpenPlay.ToList());
      }
      foreach (var wi in items) {
        if (cnt == 16) break;
        comboWagers.Add(new TicketsAndWagers.GameDataForAccumRiskAmount {
          GameNum = wi.Loo.GameNum,
          PeriodNumber = wi.Loo.PeriodNumber,
          WagerType = wi.WagerType,
          ChosenTeamId = wi.ChosenTeamId,
          TotalPointsOu = wi.TotalPointsOu
        });

        cnt++;
      }

      var wagerType = WebLibs.SessionVariablesCore.WagerType.Name == "Parlay" ? WagerType.PARLAY : WebLibs.SessionVariablesCore.WagerType.Name == "Teaser" ? WagerType.TEASER : WebLibs.SessionVariablesCore.WagerType.Name == "Action Reverse" ? WagerType.IFBET : "";
      if (WebLibs.SessionVariablesCore.RoundRobinValue > 0) wagerType = "R";

      double totalBet, totalAccumBet = 0;
      using (var tkw = new TicketsAndWagers(WebLibs.SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
        totalBet = accumAmt + tkw.GetAccumRiskAmount(WebLibs.SessionVariablesCore.CustomerInfo.CustomerID, wagerType, comboWagers) / 100;
        if (SessionVariables.SpecialParlayTeaserAccess)
          totalAccumBet = accumAmt + tkw.GetAccumRiskAmount(WebLibs.SessionVariablesCore.CustomerInfo.CustomerID.Trim(), wagerType, wagers, true) / 100;
      }

      if (totalBet > WebLibs.SessionVariablesCore.MaxWagerLimit / 100) {
        validationResult = new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Warning, WebLibs.SessionVariablesCore.Translate("Sorry, wager will not be accepted. An identical combination has been already placed for the max limit."));
        WebLibs.SystemLog.WriteAccessLog("Attempting to Place a " + wagerTypeName, validationResult.Message);
        return validationResult;
      }

      if (SessionVariables.SpecialParlayTeaserAccess && totalAccumBet > WebLibs.SessionVariablesCore.MaxWagerLimit / 100 * SessionVariables.MaxWagerLimitTimes) {
        validationResult = new WebLibs.ResultContainer(null, WebLibs.ResultContainer.ResultCode.Warning, WebLibs.SessionVariablesCore.Translate("Sorry, wager will not be accepted . A Team Has Been Keyed (3) Times For The Max Limit"));
        WebLibs.SystemLog.WriteAccessLog("Attempting to Place a " + wagerTypeName, validationResult.Message);

        return validationResult;
      }

      currentRiskAmount = accumAmt;

      return new WebLibs.ResultContainer();
    }

    /*private static void UpdateCustomerInfo() {
      if (WebLibs.SessionVariablesCore.CustomerInfo == null) return;
      using (var cust = new Customers(WebLibs.SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
        var c = cust.GetCustomerInfo(WebLibs.SessionVariablesCore.CustomerInfo.CustomerID).FirstOrDefault();
        if (c == null) return;
        WebLibs.SessionVariablesCore.CustomerInfo.ConfirmationDelay = c.ConfirmationDelay;
        WebLibs.SessionVariablesCore.CustomerInfo.AccessedBy = string.IsNullOrWhiteSpace(c.AccessedBy) ? null : c.AccessedBy.Trim();
        WebLibs.SessionVariablesCore.CustomerInfo.BaseballAction = c.BaseballAction;
        WebLibs.SessionVariablesCore.CustomerInfo.EasternLineFlag = c.EasternLineFlag;
        WebLibs.SessionVariablesCore.CustomerInfo.EnableRifFlag = c.EnableRifFlag;
        WebLibs.SessionVariablesCore.CustomerInfo.WeeklyLimitFlag = c.WeeklyLimitFlag;
        WebLibs.SessionVariablesCore.CustomerInfo.WiseActionFlag = c.WiseActionFlag;
      }
    }*/

    private static void SetLineAdjUnit(string controlCode) {
      WebLibs.SessionVariablesCore.LineAdjUnit = 0;
      switch (controlCode.Substring(0, 1)) {
        case WagerType.SPREAD:
          WebLibs.SessionVariablesCore.LineAdjUnit = .5;
          break;
        case WagerType.TOTALPOINTS:
          if (controlCode.Substring(1, 1) == "1") {
            WebLibs.SessionVariablesCore.LineAdjUnit = -.5;
          }
          else {
            WebLibs.SessionVariablesCore.LineAdjUnit = .5;
          }
          break;
      }
    }

    public static void AddActionAlertInfo(WebWager wager, string alertFlagType) {
      if (wager == null || wager.WebGameWagerItems == null) return;
      foreach (var wi in wager.WebGameWagerItems) {
        double volumeAmount;

        if (wi.ToWinAmt > wi.RiskAmt || wi.ToWinAmt == 0)
          volumeAmount = WebLibs.CommonFunctions.Round(wi.RiskAmt * 100);
        else
          volumeAmount = WebLibs.CommonFunctions.Round(wi.ToWinAmt * 100);

        WebLibs.Utilities.UpdateActionAlert(ModuleInformation, WebLibs.SessionVariablesCore.CustomerInfo.CustomerID,
            WebLibs.SessionVariablesCore.CustomerInfo.Store,
            WebLibs.SessionVariablesCore.CurrencyCode,
            volumeAmount,
            LineOfferingWeb.FormatShortDescription(wi, WebLibs.SessionVariablesCore.WagerType.Name, WebLibs.SessionVariablesCore.EasternLineFlag),
            "* Action Alert *",
            SessionVariables.Ticket.InetWagerNumber.ToString(CultureInfo.InvariantCulture),
            1,
            1,
            alertFlagType);
      }
      WebLibs.SessionVariablesCore.ActionAlertSent = alertFlagType == "1";
    }

    private static double CalculateLocalParlayToWinAmount(List<WebLibs.WagerItem> wiAry) {
      double toWinAmt;

      var lo = new LineOfferingBL.Parlay(ModuleInformation);

      var itemsCount = (from p in wiAry select p).Count();
      var parlayName = WebLibs.SessionVariablesCore.ParlayName;
      double maxParlayPayout;
      var riskAmount = WebLibs.SessionVariablesCore.RiskAmount;
      int maxPicks;
      double parlayDefaultPrice;
      lo.GetSpecs(parlayName, out maxPicks, out parlayDefaultPrice);
      var pcMaxPayout = lo.GetPayCardMaxPayout(WebLibs.SessionVariablesCore.ParlayName, itemsCount, WebLibs.SessionVariablesCore.RiskAmount);
      double custMaxParlayPayout = WebLibs.SessionVariablesCore.CustMaxParlayPayout;

      if (WebLibs.SessionVariablesCore.ParlayPayoutType == "R") {
        toWinAmt = lo.CalculateParlayToWin(GetParlayStrWis(wiAry), riskAmount, parlayDefaultPrice, pcMaxPayout, custMaxParlayPayout, parlayName, out maxParlayPayout, WebLibs.SessionVariablesCore.ExchangeRate);
      }
      else {
        int playCount;
        toWinAmt = lo.CalculateRoundRobinToWin(GetParlayStrWis(wiAry), riskAmount, itemsCount, WebLibs.SessionVariablesCore.RoundRobinValue, parlayDefaultPrice, custMaxParlayPayout, parlayName, out playCount, out maxParlayPayout, WebLibs.SessionVariablesCore.ExchangeRate);
      }
      return toWinAmt;
    }

    private static double CalculateCnRiskAmt(double toWinAmt, string priceType, double? price, double? toBase, double? decimalOdds, int? numerator, int? denominator) {
      var retValue = 0.0;
      switch (priceType) {
        case LineOfferingBL.PRICE_AMERICAN:
          double d;
          double factor = 0;
          if (toBase == 0) {
            if (price != null) factor = 100.00 / (double)price;
            if (factor > 0) {
              d = toWinAmt * factor;
            }
            else {
              d = toWinAmt / factor * -1;
            }
            retValue = WebLibs.CommonFunctions.Round(d);
          }
          else {
            if (toBase != null) factor = (double)toBase;
            if (price != null) factor /= (double)price;
            d = toWinAmt * factor;
            retValue = WebLibs.CommonFunctions.Round(d);
          }
          break;
        case LineOfferingBL.PRICE_DECIMAL:
          if (decimalOdds != null) retValue = WebLibs.CommonFunctions.Round(toWinAmt / ((double)decimalOdds - 1));
          break;
        case LineOfferingBL.PRICE_FRACTIONAL:
          if (numerator != null && denominator != null) retValue = WebLibs.CommonFunctions.Round(toWinAmt / (int)numerator * (int)denominator);
          break;
      }

      return retValue;
    }

    private static double CalculateCnToWinAmt(double riskAmt, string priceType, double? price, double? toBase, double? decimalOdds, int? numerator, int? denominator) {
      var factor = 0.0;
      var retValue = 0.0;
      switch (priceType) {
        case LineOfferingBL.PRICE_AMERICAN:
          double d;
          if (toBase == 0) {
            if (price != null) factor = (double)price / 100.00;
            if (factor > 0) {
              d = riskAmt * factor;
            }
            else {
              d = riskAmt / factor * -1;
            }
            retValue = WebLibs.CommonFunctions.Round(d);
          }
          else {
            if (price != null) factor = (double)price;
            if (toBase != null) factor /= (double)toBase;
            d = riskAmt * factor;
            retValue = WebLibs.CommonFunctions.Round(d);
          }
          break;
        case LineOfferingBL.PRICE_DECIMAL:
          if (decimalOdds != null) retValue = WebLibs.CommonFunctions.Round(riskAmt * ((double)decimalOdds - 1));
          break;
        case LineOfferingBL.PRICE_FRACTIONAL:
          if (numerator != null && denominator != null) retValue = WebLibs.CommonFunctions.Round(riskAmt * (int)numerator / (int)denominator);
          break;
      }

      return retValue;
    }

    private static void CalculateCnWagerAmount(ContestantWagerItem cwi) {
      if (cwi == null) return;
      if (cwi.RiskAmt == 0) {
        cwi.RiskAmt = CalculateCnRiskAmt(cwi.ToWinAmt, cwi.PriceType, cwi.Loo.MoneyLine, cwi.Loo.ToBase ?? 0,
          cwi.Loo.DecimalOdds, cwi.Loo.Numerator, cwi.Loo.Denominator);
      }
      else {
        cwi.ToWinAmt = CalculateCnToWinAmt(cwi.RiskAmt, cwi.PriceType, cwi.Loo.MoneyLine, cwi.Loo.ToBase ?? 0,
          cwi.Loo.DecimalOdds, cwi.Loo.Numerator, cwi.Loo.Denominator);
      }
    }

    public static double CalculateMaxRisk(WebWager wager, bool backwardCalc) {
      var startIdx = 0;
      var endIdx = wager.WebGameWagerItems.Count - 1;
      var inc = 1;

      if (backwardCalc) {
        endIdx = 0;
        startIdx = wager.WebGameWagerItems.Count - 1;
        inc = -1;
      }

      var firstAdjustableIdx = 9999;

      if (WebLibs.SessionVariablesCore.WagerType.Name == WagerType.GetFromWebCode(WagerType.WEBIFWINONLY).Name) {
        for (var i = startIdx; ; i += inc) {
          if (wager.WebGameWagerItems[i].AdjustableOddsFlag == "Y") {
            firstAdjustableIdx = i;
            break;
          }
          if (i == endIdx) {
            break;
          }
        }
      }
      else {
        firstAdjustableIdx = startIdx;
      }

      var accumulatedToWin = 0.0;

      var maxRiskAmt = 0.0;

      for (var i = startIdx; ; i += inc) {
        var adjustedRisk = wager.WebGameWagerItems[i].RiskAmt;
        adjustedRisk -= accumulatedToWin;
        if (adjustedRisk > maxRiskAmt) {
          maxRiskAmt = adjustedRisk;
        }
        if (i < firstAdjustableIdx) {
          accumulatedToWin += wager.WebGameWagerItems[i].ToWinAmt;
        }
        if (i == endIdx) {
          break;
        }
      }

      return maxRiskAmt;

    }

    private static double CalculateRiskAmt(double toWinAmt, string priceType, int americanPrice, double decimalPrice, int numerator, int denominator) {
      var riskAmt = 0.0;
      if (toWinAmt == 0) return 0;
      switch (priceType) {
        case LineOfferingBL.PRICE_AMERICAN:
          if (americanPrice == 0) return 0;
          var factor = 100.00 / americanPrice;
          if (factor > 0) {
            riskAmt = toWinAmt * factor;
          }
          else {
            riskAmt = toWinAmt / factor * -1;
          }
          break;
        case LineOfferingBL.PRICE_DECIMAL:
          riskAmt = toWinAmt / (decimalPrice - 1);
          break;
        case LineOfferingBL.PRICE_FRACTIONAL:
          riskAmt = toWinAmt / numerator * denominator;
          break;
      }
      return riskAmt;
    }

    private static double CalculateToWinAmt(double riskAmt, string priceType, int americanPrice, double decimalPrice, int numerator, int denominator) {
      var toWinAmt = 0.0;
      if (riskAmt == 0) return 0;
      switch (priceType) {
        case LineOfferingBL.PRICE_AMERICAN:
          if (americanPrice == 0) return 0;
          var factor = americanPrice / 100.00;
          if (factor > 0) {
            toWinAmt = riskAmt * factor;
          }
          else {
            toWinAmt = riskAmt / factor * -1;
          }
          break;
        case LineOfferingBL.PRICE_DECIMAL:
          toWinAmt = riskAmt * (decimalPrice - 1);
          break;
        case LineOfferingBL.PRICE_FRACTIONAL:
          toWinAmt = riskAmt * numerator / denominator;
          break;
      }
      return toWinAmt;
    }

    public static void CalculateWiWagerAmount(WebLibs.WagerItem wi) {
      if (wi.AmountEntered == WebLibs.WagerItem.AmountEnteredType.ToWinAmt && wi.ToWinAmt > 0) {
        wi.RiskAmt = CalculateRiskAmt(wi.ToWinAmt, wi.PriceType, wi.FinalPrice, wi.FinalDecimal, wi.FinalNumerator, wi.FinalDenominator);
      }
      else { //if (wi.ToWinAmt == 0) {
        wi.ToWinAmt = CalculateToWinAmt(wi.RiskAmt, wi.PriceType, wi.FinalPrice, wi.FinalDecimal, wi.FinalNumerator, wi.FinalDenominator);
      }
    }

    private static void CalculateTeaserWiWagerAmount(WebLibs.WagerItem wi, int pickCount) {
      var teaserName = WebLibs.SessionVariablesCore.TeaserName;
      using (var ln = new LineOfferingBL.Teaser(WebLibs.SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
        var tpc = ln.GetTeaserPayCard(teaserName, pickCount, 0).FirstOrDefault();
        if (tpc == null || wi.RiskAmt == 0) wi.ToWinAmt = 0;
        else wi.ToWinAmt = (wi.RiskAmt * (tpc.MoneyLine ?? 0)) / (tpc.ToBase ?? 1);
      }

    }

    private static bool CheckContestLines(WebWager wager, out String errorMsg) {
      errorMsg = "";
      if (!wager.HasContestantWagers) return true;

      var limitTime = WebLibs.SessionVariablesCore.ServerDateTime;
      if (limitTime >= SessionVariables.Ticket.WagerLimitTime) {
        errorMsg = WebLibs.SessionVariablesCore.Translate("TIMEOUT");
        return false;
      }

      var cn = new Contests(ModuleInformation);

      foreach (var cItem in wager.WebContestantWagerItems) {
        var contestLine = cn.GetContestantLine(cItem.Loo.Store.Trim(), cItem.Loo.CustProfile.Trim(), cItem.Loo.ContestNum, cItem.Loo.ContestantNum, cItem.Status);

        if (contestLine == null) {
          errorMsg = WebLibs.SessionVariablesCore.Translate("CONTEST_NOT_AVAILABLE");
          return false;
        }
        if (contestLine.MoneyLine == null) {
          errorMsg = WebLibs.SessionVariablesCore.Translate("CONTEST_MONEYLINE_NULL");
          return false;
        }

        if (contestLine.ThresholdLine != null) {
          if (contestLine.ThresholdType == "P") {
            if (cItem.Loo.ContestantName != null && cItem.Loo.ContestantName.Trim() == "Over") {
              if (contestLine.ThresholdLine > cItem.Loo.ThresholdLine) {
                errorMsg = WebLibs.SessionVariablesCore.Translate("CONTEST_THRESHOLDLINE_CHANGED");
                return false;
              }
            }
            else {
              if (contestLine.ThresholdLine < cItem.Loo.ThresholdLine) {
                errorMsg = WebLibs.SessionVariablesCore.Translate("CONTEST_THRESHOLDLINE_CHANGED");
                return false;
              }
            }
          }
          else {
            if (contestLine.ThresholdLine < cItem.Loo.ThresholdLine) {
              errorMsg = WebLibs.SessionVariablesCore.Translate("CONTEST_THRESHOLDLINE_CHANGED");
              return false;
            }
          }
        }

        if (contestLine.ToBase != null && contestLine.ToBase != 0) {
          if (!(contestLine.MoneyLine / contestLine.ToBase < cItem.Loo.MoneyLine / cItem.Loo.ToBase)) continue;
          errorMsg = WebLibs.SessionVariablesCore.Translate("CONTEST_THRESHOLDLINE_CHANGED");
          return false;
        }
        if (!(contestLine.MoneyLine < cItem.Loo.MoneyLine)) continue;
        errorMsg = WebLibs.SessionVariablesCore.Translate("CONTEST_MONEYLINE_CHANGED");
        return false;
      }

      if (WebLibs.SessionVariablesCore.FixedWagerFlag) {
        errorMsg = WebLibs.SessionVariablesCore.Translate("CONTEST_CHANGED");
        return false;
      }

      errorMsg = "";
      return true;
    }

    #region CheckGameLine
    //socket server
    public static bool CheckGameLine(WebLibs.WagerItem item) {
      try {
        using (var gl = new GamesAndLines(ModuleInformation)) {

          //var gameLines = new List<spGLWebGetGameOfferings_Result>();

          var gameLine = gl.GetWebGameOffering(WebLibs.SessionVariablesCore.CustomerInfo.CustomerID,
              item.Loo.GameNum, item.Loo.SportType, item.Loo.SportSubType, item.WagerType, item.Loo.PeriodNumber,
              DateTime.Now, item.Loo.Store, item.Loo.CustProfile, 0);
          // StringF.HandleOdbcSingleQuotes(item.Loo.Store), StringF.HandleOdbcSingleQuotes(item.Loo.CustProfile), item.Loo.Status
          if (gameLine != null) {
            gameLine.UsePuckLineFlag = WebLibs.SessionVariablesCore.UsePuckLineFlag;
            gameLine.Initialize();
            //gameLines.Add(gameLine);
          }
          else {
            item.Available = false;
            return item.Available;
          }
          var wi = item;
          wi = new WebLibs.WagerItem().UpdateWagerItem(gameLine, wi);
          SetMaximumWagerLimit(wi.WagerType, wi);
          if (WebLibs.SessionVariablesCore.FixedWagerFlag &&
              (WebLibs.SessionVariablesCore.WagerType.Name == "If Win or Push" ||
               WebLibs.SessionVariablesCore.WagerType.Name == "If Win Only" ||
               WebLibs.SessionVariablesCore.WagerType.Name == "Action Reverse")) {
                 wi.IsOk = false;
          }
          return wi.IsOk;
        }
      }
      catch (Exception ex) {
        CoreService.Log(ex);
        return false;
      }
    }

    //proces ticket
    private static bool CheckGameLines(WebWager wager, out string errorMsg) {
      errorMsg = string.Empty;
      try {
        var limitTime = WebLibs.SessionVariablesCore.ServerDateTime;

        if (limitTime >= SessionVariables.Ticket.WagerLimitTime) {
          errorMsg = WebLibs.SessionVariablesCore.Translate("WagerLimitTime TIMEOUT");
          return false;
        }
        var lineOk = true;
        var gameLines = new List<spGLWebGetGameOfferings_Result>();
        using (var gl = new GamesAndLines(ModuleInformation)) {
          foreach (var item in wager.WebGameWagerItems) {
            var gameLine = gl.GetWebGameOffering(WebLibs.SessionVariablesCore.CustomerInfo.CustomerID,
                item.Loo.GameNum, item.Loo.SportType, item.Loo.SportSubType, item.WagerType,
                item.Loo.PeriodNumber, DateTime.Now, item.Loo.Store, item.Loo.CustProfile, 0);
            // StringF.HandleOdbcSingleQuotes(item.Loo.Store), StringF.HandleOdbcSingleQuotes(item.Loo.CustProfile), item.Loo.Status
            if (gameLine != null) {
              gameLine.Initialize();
              gameLines.Add(gameLine);
              gameLine.UsePuckLineFlag = WebLibs.SessionVariablesCore.UsePuckLineFlag;
              continue;
            }
            else {
              item.ErrorMessage = WebLibs.SessionVariablesCore.Translate("LINE_NOT_AVAILABLE");
              item.Available = false;
            }
            lineOk = false;
          }
        }

        /*if (lineOk == false || gameLines.Count != wager.WebGameWagerItems.Count) {
          errorMsg = WebLibs.SessionVariablesCore.Translate("LINE_NOT_AVAILABLE");
          return false;
        }*/
        var cnt = 0;
        for (var i = 0; i < wager.WebGameWagerItems.Count; i++) {
          var wi = wager.WebGameWagerItems[i];
          if (!wi.Available)
          {
              lineOk = false;
              continue;
          }
          var gameLine = gameLines[cnt++];
          wi.Changed = false;
          wi = new WebLibs.WagerItem().UpdateWagerItem(gameLine, wi);
          lineOk = lineOk && wi.IsOk;
          SetMaximumWagerLimit(wi.WagerType, wi);
        }
        if (WebLibs.SessionVariablesCore.FixedWagerFlag &&
            (WebLibs.SessionVariablesCore.WagerType.Name == "If Win or Push" ||
             WebLibs.SessionVariablesCore.WagerType.Name == "If Win Only" ||
             WebLibs.SessionVariablesCore.WagerType.Name == "Action Reverse")) {
          lineOk = false;
        }
        errorMsg = "";
        if (!WebLibs.SessionVariablesCore.AutoAccept) return lineOk;
        WebLibs.SessionVariablesCore.FixedWagerFlag = false;
        return gameLines.Count == wager.WebGameWagerItems.Count;
      }
      catch (Exception ex) {
        CoreService.Log(ex);
        return false;
      }
    }

    #endregion

    public static bool CheckMaximumWager(WebLibs.WagerItem wi, bool accumulateRisk, out String errorMsg) {
      if (accumulateRisk) {
        GetAccumulatedStraightBetGameAmounts(wi);
      }
      else {
        WebLibs.SessionVariablesCore.AccumRisk = 0.0;
        WebLibs.SessionVariablesCore.AccumToWin = 0.0;
      }

      if (wi.RiskAmt <= wi.ToWinAmt) {
        if ((wi.RiskAmt * 100) + (WebLibs.SessionVariablesCore.AccumRisk * 100) > WebLibs.SessionVariablesCore.MaxWagerLimit) {
          errorMsg = WebLibs.SessionVariablesCore.Translate("WAGER_LIMIT_EXCEEDED") + " " + DisplayNumber.Format((wi.RiskAmt + WebLibs.SessionVariablesCore.AccumRisk) * 100, true, 100, false, false) + " " +
            (WebLibs.SessionVariablesCore.AccumRisk > 0 ? "[Considering previous wagers on this game] " : "") +
            WebLibs.SessionVariablesCore.Translate("WAGER_LIMIT_IS") + " " + DisplayNumber.Format(WebLibs.SessionVariablesCore.MaxWagerLimit) + " (" + WebLibs.SessionVariablesCore.MaxWagerLimitDesc + ")";
          wi.ValidationErrorMessage = errorMsg;
          return false;
        }
      }
      else {
        if ((wi.ToWinAmt * 100) + (WebLibs.SessionVariablesCore.AccumToWin * 100) > WebLibs.SessionVariablesCore.MaxWagerLimit) {
          errorMsg = WebLibs.SessionVariablesCore.Translate("WAGER_LIMIT_EXCEEDED") + " " + DisplayNumber.Format((wi.ToWinAmt + WebLibs.SessionVariablesCore.AccumToWin) * 100, true, 100, false, false) + " " +
            (WebLibs.SessionVariablesCore.AccumToWin > 0 ? "[Considering previous wagers on this game] " : "") +
              WebLibs.SessionVariablesCore.Translate("WAGER_LIMIT_IS") + " " + DisplayNumber.Format(WebLibs.SessionVariablesCore.MaxWagerLimit) + " (" + WebLibs.SessionVariablesCore.MaxWagerLimitDesc + ")";
          wi.ValidationErrorMessage = errorMsg;
          return false;
        }
      }
      errorMsg = "";
      return true;
    }

    private static bool CheckMinimumWager(WebLibs.WagerItem wi, double minWagerAmount) {
      if (minWagerAmount <= 0) minWagerAmount = 1;
      if (wi.RiskAmt <= wi.ToWinAmt || wi.ToWinAmt == 0) {
        if (wi.RiskAmt * 100 < minWagerAmount)
          return false;
      }
      else if (wi.ToWinAmt * 100 < minWagerAmount)
        return false;
      return true;
    }

    private static void GetAccumulatedContestantAmounts(int contestNum, int contestantNum) {
      using (var tA = new TicketsAndWagers(ModuleInformation)) {
        var accums = tA.GetAccumulatedContestWagerAmt(WebLibs.SessionVariablesCore.CustomerInfo.CustomerID, contestNum, contestantNum);

        if (accums.AccumRisk == null)
          WebLibs.SessionVariablesCore.AccumRisk = 0;
        else
          WebLibs.SessionVariablesCore.AccumRisk = ((double)accums.AccumRisk) / 100;

        if (accums.AccumToWin == null)
          WebLibs.SessionVariablesCore.AccumToWin = 0;
        else
          WebLibs.SessionVariablesCore.AccumToWin = ((double)accums.AccumToWin) / 100;
      }
    }

    /*
    public static WebLibs.WagerItem CreateHalfPointWager(WebLibs.WagerItem wi, double origFinalLine) {
      var newWi = new WebLibs.WagerItem(wi.ControlCode, wi.Loo, wi.WagerType, wi.PriceType, wi.EasternLineFlag, WebLibs.SessionVariablesCore.CustomerInfo.BaseballAction) {
        Line = wi.Line,
        Price = wi.Price,
        DecimalPrice = wi.DecimalPrice,
        Numerator = wi.Numerator,

        Denominator = wi.Denominator,
        FinalLine = origFinalLine,
        FinalPrice = wi.FinalPrice,
        FinalDecimal = wi.FinalDecimal,

        FinalNumerator = wi.FinalNumerator,
        FinalDenominator = wi.FinalDenominator,
        EasternLine = wi.EasternLine,
        PriceType = wi.PriceType,

        RiskAmt = wi.RiskAmt,
        ToWinAmt = wi.ToWinAmt,
        AdjustableOddsFlag = wi.AdjustableOddsFlag,

        Pitcher1ReqFlag = wi.Pitcher1ReqFlag,
        Pitcher2ReqFlag = wi.Pitcher2ReqFlag,

        HalfPointAdded = false,
        HalfPointNewWager = true,
        PointsBought = wi.PointsBought,

        RifTicketNumber = wi.RifTicketNumber,
        RifWagerNumber = wi.RifWagerNumber,
        RifWinOnly = wi.RifWinOnly,
        Changed = wi.Changed
      };
      FillWagerAmount(newWi, wi.ToWinAmt - WebLibs.SessionVariablesCore.HalfPointMaxBet, "");
      FillWagerAmount(wi, WebLibs.SessionVariablesCore.HalfPointMaxBet, "");

      CalculateWiWagerAmount(newWi);
      CalculateWiWagerAmount(wi);

      return newWi;
    }*/

    /*
    private static void FillWagerAmount(WebLibs.WagerItem wi, double wagerAmt, string customOption) {
      switch (customOption) {
        case "Default":
        case null:
        case "":
          if (wi.FinalPrice < 0) {
            wi.RiskAmt = 0;
            wi.ToWinAmt = wagerAmt;
            wi.AmountEntered = WebLibs.WagerItem.AmountEnteredType.ToWinAmt;
          }
          else {
            wi.RiskAmt = wagerAmt;
            wi.ToWinAmt = 0;
            wi.AmountEntered = WebLibs.WagerItem.AmountEnteredType.RiskAmt;
          }
          break;
        case "riskType":
          wi.RiskAmt = wagerAmt;
          wi.ToWinAmt = 0;
          wi.AmountEntered = WebLibs.WagerItem.AmountEnteredType.RiskAmt;
          break;
        default:
          wi.RiskAmt = 0;
          wi.ToWinAmt = wagerAmt;
          wi.AmountEntered = WebLibs.WagerItem.AmountEnteredType.ToWinAmt;
          break;
      }
    }*/

    private static String FormatWagerAccessLog(WebLibs.WagerItem wi, int num, Boolean showAmount) {
      var ret = " Selection # " + num;
      var pointsBought = wi.Line != wi.FinalLine;

      if ((wi.RiskAmt > 0 || wi.ToWinAmt > 0) && showAmount) {
        if (wi.ToWinAmt > wi.RiskAmt || wi.ToWinAmt == 0 || WebLibs.SessionVariablesCore.WagerType.Name == "Parlay" ||
        WebLibs.SessionVariablesCore.WagerType.Name == "Teaser")
          ret += " | Risk Amount: " + WebLibs.CommonFunctions.Round(wi.RiskAmt);
        else
          ret += " | To Win Amount: " + WebLibs.CommonFunctions.Round(wi.ToWinAmt);
      }
      if (WebLibs.SessionVariablesCore.WagerType.Name == "Parlay" && showAmount) {
        ret += " | " + WebLibs.Utilities.BuildParlayWagerTypeDesc(WebLibs.SessionVariablesCore.RoundRobinValue, WebLibs.SessionVariablesCore.FreePlay);
      }
      switch (wi.WagerType) {
        case WagerType.SPREAD:
          ret += " | Spread ";
          ret += LineOfferingBL.FormatAhSpread(wi.Line);
          if (pointsBought) {
            ret += " Final Spread: " + LineOfferingBL.FormatAhSpread(wi.FinalLine);
          }
          break;
        case WagerType.MONEYLINE:
          ret += " | Money Line ";
          break;
        case "L":
          ret += " | Total Points ";
          if (wi.ControlCode.Substring(1, 1) == "1") ret += "Over ";
          else ret += "Under ";
          ret += LineOfferingBL.FormatAhTotal(wi.Line);
          if (pointsBought) {
            ret += " Final Points: " + LineOfferingBL.FormatAhTotal(wi.FinalLine);
          }
          break;
        case "E":
          ret += " | Team Totals ";
          break;
      }
      if (wi.ControlCode.Substring(1, 1) == "1") ret += " | Team: " + wi.Loo.Team1ID;
      else ret += " | Team: " + wi.Loo.Team2ID;
      ret += " | Price: ";
      if (wi.Price > 0) ret += "+";
      ret += wi.Price;
      if (pointsBought) {
        ret += " Final Price: " + wi.FinalPrice;
      }

      return ret;
    }

    private static void GetAccumulatedStraightBetGameAmounts(WebLibs.WagerItem wi) {
      var customerId = WebLibs.SessionVariablesCore.CustomerInfo.CustomerID;
      var wagerType = wi.WagerType;
      var gameNum = wi.Loo.GameNum;

      var chosenTeamId = wi.ChosenTeamId;
      var totalPointsOu = wi.TotalPointsOu;
      var periodNumber = wi.Loo.PeriodNumber;

      using (var taW = new TicketsAndWagers(ModuleInformation)) {
        if (WebLibs.SessionVariablesCore.EnforceAccumWagerLimitsByLineFlag == "Y") {
          double spread = 0;
          double totalPoints = 0;
          var moneyLine = 0;

          switch (wi.WagerType) {
            case WagerType.SPREAD:
              if (wi.Loo.Spread != null)
                spread = (double)wi.Loo.Spread;

              totalPoints = 0;

              if (chosenTeamId == wi.Loo.Team1ID.Trim()) {
                if (wi.Loo.OrigSpreadAdj1 != null)
                  moneyLine = (int)wi.Loo.OrigSpreadAdj1;
              }
              else {
                if (wi.Loo.OrigSpreadAdj2 != null)
                  moneyLine = (int)wi.Loo.OrigSpreadAdj2;
              }
              break;
            case WagerType.MONEYLINE:
              spread = 0;
              totalPoints = 0;

              if (chosenTeamId == wi.Loo.Team1ID.Trim()) {
                if (wi.Loo.OrigMoneyLine1 != null)
                  moneyLine = (int)wi.Loo.OrigMoneyLine1;
              }
              else {
                if (wi.Loo.OrigMoneyLine2 != null)
                  moneyLine = (int)wi.Loo.OrigMoneyLine2;
              }
              break;
            case WagerType.TOTALPOINTS:
              spread = 0;

              if (wi.Loo.TotalPoints != null)
                totalPoints = (double)wi.Loo.TotalPoints;

              if (wi.ControlCode.Substring(1, 1) == "1") {
                if (wi.Loo.OrigTtlPtsAdj1 != null)
                  moneyLine = (int)wi.Loo.OrigTtlPtsAdj1;
              }
              else {
                if (wi.Loo.OrigTtlPtsAdj2 != null)
                  moneyLine = (int)wi.Loo.OrigTtlPtsAdj2;
              }
              break;
            case WagerType.TEAMTOTALPOINTS:
              spread = 0;

              if (chosenTeamId == wi.Loo.Team1ID.Trim()) {
                if (wi.Loo.Team1TotalPoints != null) totalPoints = (int)wi.Loo.Team1TotalPoints;
                if (wi.ControlCode.Substring(1, 1) == "1" || wi.ControlCode.Substring(1, 1) == "2") {
                  if (wi.Loo.OrigTeam1TtlPtsAdj1 != null) moneyLine = (int)wi.Loo.OrigTeam1TtlPtsAdj1;
                }
                else {
                  if (wi.Loo.OrigTeam1TtlPtsAdj2 != null) moneyLine = (int)wi.Loo.OrigTeam1TtlPtsAdj2;
                }
              }
              else {
                if (wi.Loo.Team2TotalPoints != null) totalPoints = (int)wi.Loo.Team2TotalPoints;
                if (wi.ControlCode.Substring(1, 1) == "1" || wi.ControlCode.Substring(1, 1) == "2") {
                  if (wi.Loo.OrigTeam2TtlPtsAdj1 != null) moneyLine = (int)wi.Loo.OrigTeam2TtlPtsAdj1;
                }
                else {
                  if (wi.Loo.OrigTeam2TtlPtsAdj2 != null) moneyLine = (int)wi.Loo.OrigTeam2TtlPtsAdj2;
                }
              }
              break;
          }
          var accumAmountsbyLine = taW.GetAccumulatedSbGameAmtByLine(customerId, wagerType, gameNum, chosenTeamId, totalPointsOu, periodNumber, spread, totalPoints, moneyLine);
          WebLibs.SessionVariablesCore.AccumRisk = 0;
          WebLibs.SessionVariablesCore.AccumToWin = 0;

          if (accumAmountsbyLine == null)
            return;
          if (accumAmountsbyLine.AccumRisk != null)
            WebLibs.SessionVariablesCore.AccumRisk = ((double)accumAmountsbyLine.AccumRisk) / 100;
          if (accumAmountsbyLine.AccumRisk != null)
            WebLibs.SessionVariablesCore.AccumToWin = (accumAmountsbyLine.AccumToWin ?? 0) / 100;
        }
        else {
          var accumAmounts = taW.GetAccumulatedSbGameAmt(customerId, wagerType, gameNum, chosenTeamId, totalPointsOu, periodNumber);
          WebLibs.SessionVariablesCore.AccumRisk = 0;
          WebLibs.SessionVariablesCore.AccumToWin = 0;

          if (accumAmounts == null)
            return;

          if (accumAmounts.AccumRisk != null)
            WebLibs.SessionVariablesCore.AccumRisk = (double)accumAmounts.AccumRisk / 100;

          if (accumAmounts.AccumRisk == null)
            return;

          if (accumAmounts.AccumToWin != null)
            WebLibs.SessionVariablesCore.AccumToWin = (double)accumAmounts.AccumToWin / 100;
        }
      }
    }

    public static int GetMinimumWagerAmt() {
      int mwa;

      using (var cst = new Customers(WebLibs.SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
        mwa = cst.GetCustomerMinWebWagerAmount(WebLibs.SessionVariablesCore.CustomerInfo.CustomerID) * (int)WebLibs.SessionVariablesCore.ExchangeRate;
      }
      WebLibs.SessionVariablesCore.MinimumWagerAmt = mwa;
      return mwa;
    }

    /*private static string GetChosenTeamId(WebLibs.WagerItem wi) {
      string chosenTeamId;

      switch (wi.WagerType) {
        case WagerType.MONEYLINE:
        case WagerType.SPREAD:
          switch (wi.ControlCode.Substring(1, 1)) {
            case "1":
              chosenTeamId = wi.Loo.Team1ID; // StringF.HandleOdbcSingleQuotes(wi.Loo.Team1ID)
              break;
            case "2":
              chosenTeamId = wi.Loo.Team2ID; // StringF.HandleOdbcSingleQuotes(wi.Loo.Team2ID)
              break;
            default:
              chosenTeamId = WebLibs.SessionVariablesCore.Translate("DRAW"] + " (";
              if (wi.Loo.Team1ID.Length > 35) chosenTeamId += wi.Loo.Team1ID.Substring(0, 35); // += StringF.HandleOdbcSingleQuotes(wi.Loo.Team1ID.Substring(0, 35))
              else chosenTeamId += wi.Loo.Team1ID.Trim(); // StringF.HandleOdbcSingleQuotes(wi.Loo.Team1ID.Trim())
              chosenTeamId += " vs ";
              if (wi.Loo.Team2ID.Length > 35) chosenTeamId += wi.Loo.Team2ID.Substring(0, 35); // StringF.HandleOdbcSingleQuotes(wi.Loo.Team2ID.Substring(0, 35))
              else chosenTeamId += wi.Loo.Team2ID.Trim(); // StringF.HandleOdbcSingleQuotes(wi.Loo.Team2ID.Trim())
              chosenTeamId += ")";
              break;
          }
          break;
        case WagerType.TEAMTOTALPOINTS:
          if (wi.ControlCode.Substring(1, 1) == "1" || wi.ControlCode.Substring(1, 1) == "3") {
            chosenTeamId = wi.Loo.Team1ID; // StringF.HandleOdbcSingleQuotes(wi.Loo.Team1ID)
          }
          else {
            chosenTeamId = wi.Loo.Team2ID; // StringF.HandleOdbcSingleQuotes(wi.Loo.Team2ID)
          }
          break;
        default:
          chosenTeamId = wi.Loo.Team1ID; // StringF.HandleOdbcSingleQuotes(wi.Loo.Team1ID)
          chosenTeamId += "/";
          chosenTeamId += wi.Loo.Team2ID; // StringF.HandleOdbcSingleQuotes(wi.Loo.Team2ID)
          break;
      }

      return chosenTeamId;
    }*/

    /* :not used:
    private static double GetLineFromChosen(string chosenLine) {
      double retPrice = -1;
      string[] chosenLineAry = chosenLine.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);
      retPrice = double.Parse(chosenLineAry[0].Replace("pk", "0").Replace("½", ".5"));
      return retPrice;
    }
     */

    private static List<LineOfferingBL.StrWagerItem> GetParlayStrWis(IEnumerable<WebLibs.WagerItem> wiAry) {
      return wiAry.Select(item => new LineOfferingBL.StrWagerItem {
        DecimalPrice = item.DecimalPrice,
        Denominator = item.Denominator,
        FinalDenominator = item.FinalDenominator,
        FinalNumerator = item.FinalNumerator,
        FinalPrice = item.FinalPrice,
        Loo = new LineOfferingBL.StrLineOffering {
          SportType = item.Loo.SportType.Trim(),
          SportSubType = item.Loo.SportSubType.Trim()
        },
        Numerator = item.Numerator,
        Price = item.Price,
        PriceType = item.PriceType,
        RiskAmt = item.RiskAmt,
        ToWinAmt = item.ToWinAmt,
        WagerType = item.WagerType
      }).ToList();
    }

    /* :not used:
    private static string GetPriceFromChosen(string chosenLine) {
      string retPrice = "";
      string[] chosenLineAry = chosenLine.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);
      retPrice = chosenLineAry[1];
      return retPrice;
    }
    */

    /*private static string GetTotalPointsOU(WebLibs.WagerItem wi) {
      var ouValue = "";

      switch (wi.WagerType) {
        case "L":
          if (wi.ControlCode.Substring(1, 1) == "1") {
            ouValue += "O";
          }
          else {
            ouValue += "U";
          }
          break;
        case "E":
          if (wi.ControlCode.Substring(1, 1) == "1" || wi.ControlCode.Substring(1, 1) == "2") {
            ouValue += "O";
          }
          else {
            ouValue += "U";
          }
          break;
      }
      return ouValue;
    }*/

    private static void InsertRoundRobinWagers(TicketsAndWagers taW, int docNum, List<WebLibs.WagerItem> wiAry, int wiAryCount, string freePlayFlag,
                                                    String creditAccountFlag, String currencyCode, String valueDateStr, String parlayPayoutType) {
      double toWin;
      var skipTwos = false;
      var playCount = 0;
      double pcMaxPayout;

      var combos = WebLibs.SessionVariablesCore.RoundRobinValue;

      if (combos >= 6) {
        combos -= 4;
        skipTwos = true;
      }

      double parlayDefaultPrice;
      var lo = new LineOfferingBL.Parlay(ModuleInformation);
      int maxPicks;
      var parlayName = WebLibs.SessionVariablesCore.ParlayName;
      lo.GetSpecs(parlayName, out maxPicks, out parlayDefaultPrice);

      var agentId = "";

      if (WebLibs.SessionVariablesCore.CustomerInfo.AgentID != null)
        agentId = WebLibs.SessionVariablesCore.CustomerInfo.AgentID;

      var store = "";
      if (WebLibs.SessionVariablesCore.CustomerInfo.Store != null)
        store = WebLibs.SessionVariablesCore.CustomerInfo.Store;

      //switch (combos) {
      if (combos == 5) {
        pcMaxPayout = lo.GetPayCardMaxPayout(WebLibs.SessionVariablesCore.ParlayName, 6, WebLibs.SessionVariablesCore.RiskAmount);
        if (WebLibs.SessionVariablesCore.CustMaxParlayPayout > 0 && WebLibs.SessionVariablesCore.CustMaxParlayPayout < pcMaxPayout) {
          pcMaxPayout = WebLibs.SessionVariablesCore.CustMaxParlayPayout;
        }
        for (var a = 0; a < wiAryCount - 5; a++) {
          for (var b = a + 1; b < wiAryCount - 4; b++) {
            for (var c = b + 1; c < wiAryCount - 3; c++) {
              for (var d = c + 1; d < wiAryCount - 2; d++) {
                for (var e = d + 1; e < wiAryCount - 1; e++) {
                  for (var f = e + 1; f < wiAryCount; f++) {
                    playCount++;
                    var plAry = new List<WebLibs.WagerItem> { wiAry[a], wiAry[b], wiAry[c], wiAry[d], wiAry[e], wiAry[f] };
                    toWin = CalculateLocalParlayToWinAmount(plAry);
                    var descStr = LineOfferingWeb.FormatShortDescription(wiAry[a], WebLibs.SessionVariablesCore.WagerType.Name, WebLibs.SessionVariablesCore.EasternLineFlag);
                    descStr += "\r\n";
                    descStr += LineOfferingWeb.FormatShortDescription(wiAry[b], WebLibs.SessionVariablesCore.WagerType.Name, WebLibs.SessionVariablesCore.EasternLineFlag);
                    descStr += "\r\n";
                    descStr += LineOfferingWeb.FormatShortDescription(wiAry[c], WebLibs.SessionVariablesCore.WagerType.Name, WebLibs.SessionVariablesCore.EasternLineFlag);
                    descStr += "\r\n";
                    descStr += LineOfferingWeb.FormatShortDescription(wiAry[d], WebLibs.SessionVariablesCore.WagerType.Name, WebLibs.SessionVariablesCore.EasternLineFlag);
                    descStr += "\r\n";
                    descStr += LineOfferingWeb.FormatShortDescription(wiAry[e], WebLibs.SessionVariablesCore.WagerType.Name, WebLibs.SessionVariablesCore.EasternLineFlag);
                    descStr += "\r\n";
                    descStr += LineOfferingWeb.FormatShortDescription(wiAry[f], WebLibs.SessionVariablesCore.WagerType.Name, WebLibs.SessionVariablesCore.EasternLineFlag);
                    taW.InsertWebWager(docNum, playCount, WebLibs.CommonFunctions.Round(WebLibs.SessionVariablesCore.RiskAmount * 100), "P", descStr,
                            WebLibs.CommonFunctions.Round(toWin * 100), 1, "0", 6, "", WebLibs.SessionVariablesCore.ParlayName, "", creditAccountFlag, 1,
                            0, 1, 0, currencyCode, valueDateStr, int.Parse(pcMaxPayout.ToString(CultureInfo.InvariantCulture)) * 100, parlayPayoutType, freePlayFlag,
                            0, 0, "");
                    InsertWagerItem(taW, docNum, wiAry[a], playCount, 1, store, currencyCode, valueDateStr, agentId, WebLibs.SessionVariablesCore.WagerType.Name);
                    InsertWagerItem(taW, docNum, wiAry[b], playCount, 2, store, currencyCode, valueDateStr, agentId, WebLibs.SessionVariablesCore.WagerType.Name);
                    InsertWagerItem(taW, docNum, wiAry[c], playCount, 3, store, currencyCode, valueDateStr, agentId, WebLibs.SessionVariablesCore.WagerType.Name);
                    InsertWagerItem(taW, docNum, wiAry[d], playCount, 4, store, currencyCode, valueDateStr, agentId, WebLibs.SessionVariablesCore.WagerType.Name);
                    InsertWagerItem(taW, docNum, wiAry[e], playCount, 5, store, currencyCode, valueDateStr, agentId, WebLibs.SessionVariablesCore.WagerType.Name);
                    InsertWagerItem(taW, docNum, wiAry[f], playCount, 6, store, currencyCode, valueDateStr, agentId, WebLibs.SessionVariablesCore.WagerType.Name);
                    taW.InsertWebStage(docNum, playCount);
                  }
                }
              }
            }
          }
        }
      }
      if (combos >= 4) {
        pcMaxPayout = lo.GetPayCardMaxPayout(WebLibs.SessionVariablesCore.ParlayName, 5, WebLibs.SessionVariablesCore.RiskAmount);
        if (WebLibs.SessionVariablesCore.CustMaxParlayPayout > 0 && WebLibs.SessionVariablesCore.CustMaxParlayPayout < pcMaxPayout) {
          pcMaxPayout = WebLibs.SessionVariablesCore.CustMaxParlayPayout;
        }
        for (var a = 0; a < wiAryCount - 4; a++) {
          for (var b = a + 1; b < wiAryCount - 3; b++) {
            for (var c = b + 1; c < wiAryCount - 2; c++) {
              for (var d = c + 1; d < wiAryCount - 1; d++) {
                for (var e = d + 1; e < wiAryCount; e++) {
                  playCount++;
                  var plAry = new List<WebLibs.WagerItem> { wiAry[a], wiAry[b], wiAry[c], wiAry[d], wiAry[e] };
                  toWin = CalculateLocalParlayToWinAmount(plAry);
                  var descStr = LineOfferingWeb.FormatShortDescription(wiAry[a], WebLibs.SessionVariablesCore.WagerType.Name, WebLibs.SessionVariablesCore.EasternLineFlag);
                  descStr += "\r\n";
                  descStr += LineOfferingWeb.FormatShortDescription(wiAry[b], WebLibs.SessionVariablesCore.WagerType.Name, WebLibs.SessionVariablesCore.EasternLineFlag);
                  descStr += "\r\n";
                  descStr += LineOfferingWeb.FormatShortDescription(wiAry[c], WebLibs.SessionVariablesCore.WagerType.Name, WebLibs.SessionVariablesCore.EasternLineFlag);
                  descStr += "\r\n";
                  descStr += LineOfferingWeb.FormatShortDescription(wiAry[d], WebLibs.SessionVariablesCore.WagerType.Name, WebLibs.SessionVariablesCore.EasternLineFlag);
                  descStr += "\r\n";
                  descStr += LineOfferingWeb.FormatShortDescription(wiAry[e], WebLibs.SessionVariablesCore.WagerType.Name, WebLibs.SessionVariablesCore.EasternLineFlag);
                  taW.InsertWebWager(docNum, playCount, WebLibs.CommonFunctions.Round(WebLibs.SessionVariablesCore.RiskAmount * 100), "P", descStr,
                          WebLibs.CommonFunctions.Round(toWin * 100), 1, "0", 5, "", WebLibs.SessionVariablesCore.ParlayName, "", creditAccountFlag, 1,
                          0, 1, 0, currencyCode, valueDateStr, int.Parse(pcMaxPayout.ToString(CultureInfo.InvariantCulture)) * 100, parlayPayoutType, freePlayFlag,
                          0, 0, "");
                  InsertWagerItem(taW, docNum, wiAry[a], playCount, 1, store, currencyCode, valueDateStr, agentId, WebLibs.SessionVariablesCore.WagerType.Name);
                  InsertWagerItem(taW, docNum, wiAry[b], playCount, 2, store, currencyCode, valueDateStr, agentId, WebLibs.SessionVariablesCore.WagerType.Name);
                  InsertWagerItem(taW, docNum, wiAry[c], playCount, 3, store, currencyCode, valueDateStr, agentId, WebLibs.SessionVariablesCore.WagerType.Name);
                  InsertWagerItem(taW, docNum, wiAry[d], playCount, 4, store, currencyCode, valueDateStr, agentId, WebLibs.SessionVariablesCore.WagerType.Name);
                  InsertWagerItem(taW, docNum, wiAry[e], playCount, 5, store, currencyCode, valueDateStr, agentId, WebLibs.SessionVariablesCore.WagerType.Name);
                  taW.InsertWebStage(docNum, playCount);
                }
              }
            }
          }
        }
      }
      if (combos >= 3) {
        pcMaxPayout = lo.GetPayCardMaxPayout(WebLibs.SessionVariablesCore.ParlayName, 4, WebLibs.SessionVariablesCore.RiskAmount);
        if (WebLibs.SessionVariablesCore.CustMaxParlayPayout > 0 && WebLibs.SessionVariablesCore.CustMaxParlayPayout < pcMaxPayout) {
          pcMaxPayout = WebLibs.SessionVariablesCore.CustMaxParlayPayout;
        }
        for (var a = 0; a < wiAryCount - 3; a++) {
          for (var b = a + 1; b < wiAryCount - 2; b++) {
            for (var c = b + 1; c < wiAryCount - 1; c++) {
              for (var d = c + 1; d < wiAryCount; d++) {
                playCount++;
                var plAry = new List<WebLibs.WagerItem> { wiAry[a], wiAry[b], wiAry[c], wiAry[d] };
                toWin = CalculateLocalParlayToWinAmount(plAry);
                var descStr = LineOfferingWeb.FormatShortDescription(wiAry[a], WebLibs.SessionVariablesCore.WagerType.Name, WebLibs.SessionVariablesCore.EasternLineFlag);
                descStr += "\r\n";
                descStr += LineOfferingWeb.FormatShortDescription(wiAry[b], WebLibs.SessionVariablesCore.WagerType.Name, WebLibs.SessionVariablesCore.EasternLineFlag);
                descStr += "\r\n";
                descStr += LineOfferingWeb.FormatShortDescription(wiAry[c], WebLibs.SessionVariablesCore.WagerType.Name, WebLibs.SessionVariablesCore.EasternLineFlag);
                descStr += "\r\n";
                descStr += LineOfferingWeb.FormatShortDescription(wiAry[d], WebLibs.SessionVariablesCore.WagerType.Name, WebLibs.SessionVariablesCore.EasternLineFlag);
                taW.InsertWebWager(docNum, playCount, WebLibs.CommonFunctions.Round(WebLibs.SessionVariablesCore.RiskAmount * 100), "P", descStr,
                        WebLibs.CommonFunctions.Round(toWin * 100), 1, "0", 4, "", WebLibs.SessionVariablesCore.ParlayName, "", creditAccountFlag, 1,
                        0, 1, 0, currencyCode, valueDateStr, int.Parse(pcMaxPayout.ToString(CultureInfo.InvariantCulture)) * 100, parlayPayoutType, freePlayFlag,
                        0, 0, "");
                InsertWagerItem(taW, docNum, wiAry[a], playCount, 1, store, currencyCode, valueDateStr, agentId, WebLibs.SessionVariablesCore.WagerType.Name);
                InsertWagerItem(taW, docNum, wiAry[b], playCount, 2, store, currencyCode, valueDateStr, agentId, WebLibs.SessionVariablesCore.WagerType.Name);
                InsertWagerItem(taW, docNum, wiAry[c], playCount, 3, store, currencyCode, valueDateStr, agentId, WebLibs.SessionVariablesCore.WagerType.Name);
                InsertWagerItem(taW, docNum, wiAry[d], playCount, 4, store, currencyCode, valueDateStr, agentId, WebLibs.SessionVariablesCore.WagerType.Name);
                taW.InsertWebStage(docNum, playCount);
              }
            }
          }
        }
      }
      if (combos >= 2) {
        pcMaxPayout = lo.GetPayCardMaxPayout(WebLibs.SessionVariablesCore.ParlayName, 3, WebLibs.SessionVariablesCore.RiskAmount);
        if (WebLibs.SessionVariablesCore.CustMaxParlayPayout > 0 && WebLibs.SessionVariablesCore.CustMaxParlayPayout < pcMaxPayout) {
          pcMaxPayout = WebLibs.SessionVariablesCore.CustMaxParlayPayout;
        }
        for (var a = 0; a < wiAryCount - 2; a++) {
          for (var b = a + 1; b < wiAryCount - 1; b++) {
            for (var c = b + 1; c < wiAryCount; c++) {
              playCount++;
              var plAry = new List<WebLibs.WagerItem> { wiAry[a], wiAry[b], wiAry[c] };
              toWin = CalculateLocalParlayToWinAmount(plAry);
              var descStr = LineOfferingWeb.FormatShortDescription(wiAry[a], WebLibs.SessionVariablesCore.WagerType.Name, WebLibs.SessionVariablesCore.EasternLineFlag);
              descStr += "\r\n";
              descStr += LineOfferingWeb.FormatShortDescription(wiAry[b], WebLibs.SessionVariablesCore.WagerType.Name, WebLibs.SessionVariablesCore.EasternLineFlag);
              descStr += "\r\n";
              descStr += LineOfferingWeb.FormatShortDescription(wiAry[c], WebLibs.SessionVariablesCore.WagerType.Name, WebLibs.SessionVariablesCore.EasternLineFlag);
              taW.InsertWebWager(docNum, playCount, WebLibs.CommonFunctions.Round(WebLibs.SessionVariablesCore.RiskAmount * 100), "P", descStr,
                      WebLibs.CommonFunctions.Round(toWin * 100), 1, "0", 3, "", WebLibs.SessionVariablesCore.ParlayName, "", creditAccountFlag, 1,
                      0, 1, 0, currencyCode, valueDateStr, int.Parse(pcMaxPayout.ToString(CultureInfo.InvariantCulture)) * 100, parlayPayoutType, freePlayFlag,
                      0, 0, "");
              InsertWagerItem(taW, docNum, wiAry[a], playCount, 1, store, currencyCode, valueDateStr, agentId, WebLibs.SessionVariablesCore.WagerType.Name);
              InsertWagerItem(taW, docNum, wiAry[b], playCount, 2, store, currencyCode, valueDateStr, agentId, WebLibs.SessionVariablesCore.WagerType.Name);
              InsertWagerItem(taW, docNum, wiAry[c], playCount, 3, store, currencyCode, valueDateStr, agentId, WebLibs.SessionVariablesCore.WagerType.Name);
              taW.InsertWebStage(docNum, playCount);
            }
          }
        }
      }
      if (combos < 1) return;
      if (skipTwos) return;
      pcMaxPayout = lo.GetPayCardMaxPayout(WebLibs.SessionVariablesCore.ParlayName, 2, WebLibs.SessionVariablesCore.RiskAmount);
      if (WebLibs.SessionVariablesCore.CustMaxParlayPayout > 0 && WebLibs.SessionVariablesCore.CustMaxParlayPayout < pcMaxPayout) {
        pcMaxPayout = WebLibs.SessionVariablesCore.CustMaxParlayPayout;
      }
      for (var a = 0; a < wiAryCount - 1; a++) {
        for (var b = a + 1; b < wiAryCount; b++) {
          playCount++;
          var plAry = new List<WebLibs.WagerItem> { wiAry[a], wiAry[b] };
          toWin = CalculateLocalParlayToWinAmount(plAry);
          var descStr = LineOfferingWeb.FormatShortDescription(wiAry[a], WebLibs.SessionVariablesCore.WagerType.Name, WebLibs.SessionVariablesCore.EasternLineFlag);
          descStr += "\r\n";
          descStr += LineOfferingWeb.FormatShortDescription(wiAry[b], WebLibs.SessionVariablesCore.WagerType.Name, WebLibs.SessionVariablesCore.EasternLineFlag);
          taW.InsertWebWager(docNum, playCount, WebLibs.CommonFunctions.Round(WebLibs.SessionVariablesCore.RiskAmount * 100), "P", descStr,
            WebLibs.CommonFunctions.Round(toWin * 100), 1, "0", 2, "", WebLibs.SessionVariablesCore.ParlayName, "", creditAccountFlag, 1,
            0, 1, 0, currencyCode, valueDateStr, int.Parse(pcMaxPayout.ToString(CultureInfo.InvariantCulture)) * 100, parlayPayoutType, freePlayFlag,
            0, 0, "");
          InsertWagerItem(taW, docNum, wiAry[a], playCount, 1, store, currencyCode, valueDateStr, agentId, WebLibs.SessionVariablesCore.WagerType.Name);
          InsertWagerItem(taW, docNum, wiAry[b], playCount, 2, store, currencyCode, valueDateStr, agentId, WebLibs.SessionVariablesCore.WagerType.Name);
          taW.InsertWebStage(docNum, playCount);
        }
      }
    }

    private static bool InsertTicket(TicketsAndWagers taW, int docNum, int playCount, String store, String agentId) {
      var ticket = new spTkWGetCustomerTicketInformation_Result { TicketNumber = docNum, CustomerID = WebLibs.SessionVariablesCore.CustomerInfo.CustomerID };

      ticket.StartDateTime = ticket.PostedDateTime = WebLibs.SessionVariablesCore.ServerDateTime;
      ticket.TicketWriter = WebLibs.SessionVariablesCore.SiteCode;
      ticket.Store = store;
      ticket.PlayCount = playCount;
      ticket.IPAddress = WebLibs.ServerVariables.ClientIp;
      ticket.AgentID = agentId;
      ticket.SessionId = WebLibs.SessionVariablesCore.UniqueKey;
      return taW.InsertInetTicket(ticket);
    }

    /*
    public static void InsertWagerItemFromDuplicate(WebWager wager, IEnumerable<WebLibs.WagerItem> wiDuplicates) {
      foreach (var wi in wiDuplicates) {
        var controlCode = wi.ControlCode;
        var gameNumber = wi.Loo.GameNum;
        var periodNumber = wi.Loo.PeriodNumber;

        var origWi = (from p in wager.WebGameWagerItems
                      where p.ControlCode == controlCode
                                && p.Loo.GameNum == gameNumber
                                && p.Loo.PeriodNumber == periodNumber
                      select p).FirstOrDefault();
        var origWiIdx = wager.WebGameWagerItems.IndexOf(origWi);

        if (origWiIdx >= 0)
          wager.WebGameWagerItems.Insert(origWiIdx, wi);
      }
    }
    */

    private static void InsertWagerItem(TicketsAndWagers taW, int docNum, WebLibs.WagerItem wi, int wagerNumber, int itemNumber,
                                            String store, String currencyCode, String valueDateStr, String agentId, String wagerTypeName) {
      double adjSpread = 0;

      if (wi.WagerType == WagerType.SPREAD)
        adjSpread = wi.FinalLine;

      double adjTotalPoints = 0;

      if (wi.WagerType == WagerType.TOTALPOINTS || wi.WagerType == WagerType.TEAMTOTALPOINTS)
        adjTotalPoints = wi.FinalLine;

      var overUnder = "";

      switch (wi.WagerType) {
        case WagerType.TOTALPOINTS:
          overUnder = wi.ControlCode.Substring(1, 1) == "1" ? "O" : "U";
          break;
        case WagerType.TEAMTOTALPOINTS:
          if (wi.ControlCode.Substring(1, 1) == "1" || wi.ControlCode.Substring(1, 1) == "2")
            overUnder = "O";
          else
            overUnder = "U";

          break;
      }
      var chosenTeamName = WebLibs.Utilities.GetChosenTeamName(wi.Loo, int.Parse(wi.ControlCode.Substring(1, 1)), wi.ControlCode.Substring(0, 1));
      double teaserPoints = 0;

      if (wagerTypeName == WagerType.TEASERNAME && WebLibs.SessionVariablesCore.TssAry != null) {
        foreach (var tssAry in WebLibs.SessionVariablesCore.TssAry.Where(tssAry => tssAry.SportType.Trim() == wi.Loo.SportType.Trim()
                                                       && tssAry.SportSubType.Trim() == wi.Loo.SportSubType.Trim())) {
          switch (wi.WagerType) {
            case WagerType.SPREAD:
              teaserPoints = tssAry.SpreadAdj;
              break;
            case WagerType.TOTALPOINTS:
              teaserPoints = tssAry.TotalsAdj;
              break;
          }
        }
      }
      string adjustableOddsFlag;

      if (WebLibs.SessionVariablesCore.EasternLineFlag && wi.WagerType == WagerType.MONEYLINE && wi.Loo.SportType.Trim() == "Baseball")
        adjustableOddsFlag = "N";
      else
        adjustableOddsFlag = wi.AdjustableOddsFlag;

      var listedPitcher1 = "";
      var listedPitcher2 = "";
      var pitcher1ReqFlag = "";
      var pitcher2ReqFlag = "";

      if (wi.Loo.ListedPitcher1 != null) {
        listedPitcher1 = wi.Loo.ListedPitcher1; // StringF.HandleOdbcSingleQuotes(wi.Loo.ListedPitcher1)

        pitcher1ReqFlag = wi.Pitcher1ReqFlag ? "Y" : "N";
      }
      if (wi.Loo.ListedPitcher2 != null) {
        listedPitcher2 = wi.Loo.ListedPitcher2; // StringF.HandleOdbcSingleQuotes(wi.Loo.ListedPitcher2)

        pitcher2ReqFlag = wi.Pitcher2ReqFlag ? "Y" : "N";
      }
      var percentBook = 0;

      if (WebLibs.SessionVariablesCore.CustomerInfo.PercentBook != null)
        percentBook = (int)WebLibs.SessionVariablesCore.CustomerInfo.PercentBook;

      var volumeAmount = wi.ToWinAmt > wi.RiskAmt || wi.ToWinAmt == 0 ? WebLibs.CommonFunctions.Round(wi.RiskAmt * 100) : WebLibs.CommonFunctions.Round(wi.ToWinAmt * 100);
      var showOnChartFlag = "N";

      switch (wagerTypeName) {
        case WagerType.CORRELATIONSTRAIGHTBETS:
        case WagerType.STRAIGHTBETNAME:
          if (wi.RifTicketNumber == 0)
            showOnChartFlag = "Y";
          break;
        case WagerType.ACTREVERSENAME:
        case WagerType.IFWINORPUSHNAME:
        case WagerType.IFWINONLYNAME:
          if (itemNumber == 1)
            showOnChartFlag = "Y";
          break;
      }
      double easternLine = 0;

      if (WebLibs.SessionVariablesCore.EasternLineFlag && wi.WagerType == WagerType.MONEYLINE && wi.Loo.SportType.Trim() == "Baseball")
        easternLine = wi.EasternLine;

      double origSpread = 0;
      double origTotalPoints = 0;
      double origMoney = 0;
      var origDecimalPrice = 0.0;
      var origNumerator = 0;
      var origDenominator = 0;

      switch (wi.WagerType) {
        case WagerType.SPREAD:
          origSpread = (double)wi.Loo.Spread;
          if (chosenTeamName == wi.Loo.Team1ID) {
            origMoney = (double)wi.Loo.OrigSpreadAdj1;
            origDecimalPrice = (double)wi.Loo.SpreadDecimal1;
            origNumerator = (int)wi.Loo.SpreadNumerator1;
            origDenominator = (int)wi.Loo.SpreadDenominator1;
          }
          else {
            origMoney = (double)wi.Loo.OrigSpreadAdj2;
            origDecimalPrice = (double)wi.Loo.SpreadDecimal2;
            origNumerator = (int)wi.Loo.SpreadNumerator2;
            origDenominator = (int)wi.Loo.SpreadDenominator2;
          }

          break;
        case "M":
          if (chosenTeamName.Trim() == wi.Loo.Team1ID.Trim()) {
            origMoney = (double)wi.Loo.OrigMoneyLine1;
            origDecimalPrice = (double)wi.Loo.MoneyLineDecimal1;
            origNumerator = (int)wi.Loo.MoneyLineNumerator1;
            origDenominator = (int)wi.Loo.MoneyLineDenominator1;
          }
          else {
            origMoney = (double)wi.Loo.OrigMoneyLine2;
            origDecimalPrice = (double)wi.Loo.MoneyLineDecimal2;
            origNumerator = (int)wi.Loo.MoneyLineNumerator2;
            origDenominator = (int)wi.Loo.MoneyLineDenominator2;
          }
          break;
        case "L":
          origTotalPoints = (double)wi.Loo.TotalPoints;
          if (overUnder == "O") {
            origMoney = (double)wi.Loo.OrigTtlPtsAdj1;
            origDecimalPrice = (double)wi.Loo.TtlPointsDecimal1;
            origNumerator = (int)wi.Loo.TtlPointsNumerator1;
            origDenominator = (int)wi.Loo.TtlPointsDenominator1;
          }
          else {
            origMoney = (double)wi.Loo.OrigTtlPtsAdj2;
            origDecimalPrice = (double)wi.Loo.TtlPointsDecimal2;
            origNumerator = (int)wi.Loo.TtlPointsNumerator2;
            origDenominator = (int)wi.Loo.TtlPointsDenominator2;
          }
          break;
        case "E":
          if (chosenTeamName.Trim() == wi.Loo.Team1ID.Trim()) {
            origTotalPoints = (double)wi.Loo.Team1TotalPoints;
            if (overUnder == "O") {
              origMoney = (double)wi.Loo.OrigTeam1TtlPtsAdj1;
              origDecimalPrice = (double)wi.Loo.Team1TtlPtsDecimal1;
              origNumerator = (int)wi.Loo.Team1TtlPtsNumerator1;
              origDenominator = (int)wi.Loo.Team1TtlPtsDenominator1;
            }
            else {
              origMoney = (double)wi.Loo.OrigTeam1TtlPtsAdj2;
              origDecimalPrice = (double)wi.Loo.Team1TtlPtsDecimal2;
              origNumerator = (int)wi.Loo.Team1TtlPtsNumerator2;
              origDenominator = (int)wi.Loo.Team1TtlPtsDenominator2;
            }

          }
          else {
            origTotalPoints = (double)wi.Loo.Team2TotalPoints;
            if (overUnder == "O") {
              origMoney = (double)wi.Loo.OrigTeam2TtlPtsAdj1;
              origDecimalPrice = (double)wi.Loo.Team2TtlPtsDecimal1;
              origNumerator = (int)wi.Loo.Team2TtlPtsNumerator1;
              origDenominator = (int)wi.Loo.Team2TtlPtsDenominator1;
            }
            else {
              origMoney = (double)wi.Loo.OrigTeam2TtlPtsAdj2;
              origDecimalPrice = (double)wi.Loo.Team2TtlPtsDecimal2;
              origNumerator = (int)wi.Loo.Team2TtlPtsNumerator2;
              origDenominator = (int)wi.Loo.Team2TtlPtsDenominator2;
            }
          }
          break;
      }
      string wiseActionFlag = null;
      int? tickRef = null;
      int doc;
      if (SessionVariables.Ticket.OpenPlay.Any() && SessionVariables.Ticket.OpenPlay.OpenTicketNumber != 0) {
        doc = SessionVariables.Ticket.OpenPlay.OpenTicketNumber;
        wagerNumber = SessionVariables.Ticket.OpenPlay.OpenWagerNumber;
        tickRef = docNum;
      }
      else {
        doc = docNum;
      }
      taW.InsertWebWagerItem(doc, wagerNumber, itemNumber, wi.Loo.GameNum, ((DateTime)wi.Loo.GameDateTime).ToString("MM/dd/yyyy hh:mm:ss tt"), wi.Loo.SportType,
    wi.Loo.SportSubType, wi.WagerType, adjSpread, adjTotalPoints, overUnder, wi.PriceType, wi.FinalPrice, wi.FinalDecimal, wi.FinalNumerator, wi.FinalDenominator,
    chosenTeamName, WebLibs.CommonFunctions.Round(wi.RiskAmt) * 100, WebLibs.CommonFunctions.Round(wi.ToWinAmt) * 100, teaserPoints, store, wi.Loo.CustProfile, wi.Loo.PeriodNumber, wi.Loo.PeriodDescription,
    adjustableOddsFlag, listedPitcher1, pitcher1ReqFlag, listedPitcher2, pitcher2ReqFlag, percentBook, volumeAmount, showOnChartFlag, currencyCode, valueDateStr,
          agentId, easternLine, origSpread, origTotalPoints, origMoney, origDecimalPrice, origNumerator, origDenominator, wiseActionFlag, tickRef, wi.HalfPointAdded);
    }

    private static bool MaxWagerLimitsOk(WebWager wager, out string errorMsg, out WebLibs.WagerItem wagerItem, out ContestantWagerItem contestItem)
    {
      string wagerMessage, contestMessage;
      var contestsValidationResult = MaxContestWagerLimitsOk(wager, out contestMessage, out contestItem);
      var gamesValidationResult = MaxGameWagerLimitsOk(wager, out wagerMessage, out wagerItem);
      errorMsg = !string.IsNullOrEmpty(wagerMessage) ? "Wager - " + wagerMessage : "Contest - " + contestMessage;
      if (!contestsValidationResult || !gamesValidationResult)
        WebLibs.SystemLog.WriteAccessLog("Attempting to Place Bet [MaxWagerLimitsOk]", errorMsg);
      return contestsValidationResult && gamesValidationResult;
    }

    private static bool MaxContestWagerLimitsOk(WebWager wager, out String errorMsg, out ContestantWagerItem contestItem) {
      contestItem = null;
      if (wager.WebContestantWagerItems != null) {
        foreach (var cwi in wager.WebContestantWagerItems) {
          var limit = (cwi.Loo.ContestantMaxWager ?? 0) < (cwi.Loo.MaxWagerLimit ?? 0) && (cwi.Loo.ContestantMaxWager ?? 0) > 0 ? cwi.Loo.ContestantMaxWager ?? 0 : cwi.Loo.MaxWagerLimit ?? 0;
          limit = (cwi.Loo.CircledMaxWager ?? 0) < limit && (cwi.Loo.CircledMaxWager ?? 0) > 0 ? cwi.Loo.CircledMaxWager ?? 0 : limit;
          SetMaximumWagerLimit(wager, "C", limit);

          if (CheckMaximumContestantWager(cwi, true, out errorMsg)) {
            continue;
          }
          cwi.IsOk = false;
          contestItem = cwi;
          return false;
        }
      }
      errorMsg = "";
      return true;
    }

    private static bool MaxGameWagerLimitsOk(WebWager wager, out String errorMsg, out WebLibs.WagerItem wagerItem) {
      errorMsg = "";
      wagerItem = null;
      if (!wager.HasGameWagers) return true;
      foreach (var wi in wager.WebGameWagerItems) {
        SetMaximumWagerLimit(wi.WagerType, wi);
        if (CheckMaximumWager(wi, true, out errorMsg) == false) {
          wagerItem = wi;
          return false;
        }
      }
      return true;
    }

    private static bool MinWagerLimitsOk(WebWager wager, out string errorMsg, out int wagerPos) {
      errorMsg = "";
      wagerPos = 0;
      if (wager == null) return false;
      var minLimitOk = true;
      if (wager.HasGameWagers) {
        var minWager = GetMinimumWagerAmt();

        foreach (var wi in from wi in wager.WebGameWagerItems where !CheckMinimumWager(wi, minWager) select wi) {
          var minAmt = wi.RiskAmt < wi.ToWinAmt ? wi.RiskAmt : wi.ToWinAmt;
          wagerPos = wi.ItemNumber;
          errorMsg = WebLibs.SessionVariablesCore.Translate("BELOW_MINIMUM") + " " + DisplayNumber.Format(minAmt, false, 1, true) + " " + WebLibs.SessionVariablesCore.Translate("WAGER_LIMIT_IS") + " " + (WebLibs.SessionVariablesCore.MinimumWagerAmt / 100);
          minLimitOk = false;
          break;
        }
      }

      if (wager.HasContestantWagers) {
        var minWager = GetMinimumWagerAmt();

        foreach (var wi in from wi in wager.WebContestantWagerItems where !CheckMinimumWager(wi, minWager) select wi) {
          var minAmt = wi.RiskAmt > 0 ? wi.RiskAmt : wi.ToWinAmt;
          wagerPos = wi.ItemNumber;
          errorMsg = WebLibs.SessionVariablesCore.Translate("BELOW_MINIMUM") + " " + DisplayNumber.Format(minAmt, false, 1, true) + " " + WebLibs.SessionVariablesCore.Translate("WAGER_LIMIT_IS") + " " + (WebLibs.SessionVariablesCore.MinimumWagerAmt / 100);
          minLimitOk = false;
          break;
        }
      }

      if (!minLimitOk) {
        WebLibs.SystemLog.WriteAccessLog("Attempting to Place Bet [MinWagerLimitsOk]", errorMsg);
      }

      return minLimitOk;
    }

    private static void ProcessContestWagers(TicketsAndWagers taW, WebWager wager, string agentId, int itemsCnt) {
      if (SessionVariables.Ticket == null || !wager.HasContestantWagers) return;

      foreach (var cwi in wager.WebContestantWagerItems) {
        var descStr = LineOfferingWeb.FormatShortContestDescription(cwi);

        const int minPicks = 1;
        const string ties = "0";
        const int itemCount = 1;
        const string continueOnPushFlag = "";
        const string parlayName = "";
        const string teaserName = "";
        const int arLink = 0;
        const int boxLink = 0;
        const int maxPayout = 0;
        const int itemNumber = 1;
        const string showOnChartFlag = "Y";
        var creditAccountFlag = "";

        if (WebLibs.SessionVariablesCore.CustomerInfo.CreditAcctFlag != null)
          creditAccountFlag = WebLibs.SessionVariablesCore.CustomerInfo.CreditAcctFlag;
        int? rounRobinLink = null;
        var currencyCode = "";

        if (WebLibs.SessionVariablesCore.CurrencyCode != null)
          currencyCode = WebLibs.SessionVariablesCore.CurrencyCode;
        var valueDateStr = WebLibs.SessionVariablesCore.ServerDateTime.ToShortDateString();
        String parlayPayoutType = null;
        var freePlayFlag = "N";
        if (WebLibs.SessionVariablesCore.FreePlay)
          freePlayFlag = "Y";
        String rifWinOnlyFlag = null;

        var wagerNumber = itemsCnt + 1;
        taW.InsertWebWager(SessionVariables.Ticket.TicketNumber, wagerNumber, WebLibs.CommonFunctions.Round(cwi.RiskAmt) * 100, "C", descStr, WebLibs.CommonFunctions.Round(cwi.ToWinAmt) * 100, minPicks, ties,
                           itemCount, continueOnPushFlag, parlayName, teaserName, creditAccountFlag, itemsCnt + 1, arLink, rounRobinLink, boxLink,
                           currencyCode, valueDateStr, maxPayout, parlayPayoutType, freePlayFlag, 0, 0, rifWinOnlyFlag);

        var volumeAmount = cwi.RiskAmt > cwi.ToWinAmt ? WebLibs.CommonFunctions.Round(cwi.ToWinAmt) * 100 : WebLibs.CommonFunctions.Round(cwi.RiskAmt) * 100;
        var thresholdType = "";
        var thresholdUnits = "";
        double? thresholdLine = null;

        if (cwi.ThresholdType != null) {
          thresholdType = cwi.ThresholdType;
          thresholdUnits = cwi.ThresholdUnits;
          thresholdLine = cwi.Loo.ThresholdLine;
        }
        double toBase = 0;

        if (cwi.Loo.ToBase != null)
          toBase = (double)cwi.Loo.ToBase;

        taW.InsertWebContestWagerItem(SessionVariables.Ticket.TicketNumber, wagerNumber, itemNumber, cwi.Loo.ContestNum, cwi.ContestDateTime.Value.ToString("MM/dd/yyyy hh:mm:ss tt"), cwi.Loo.CategoryInfo.ContestDesc,
            cwi.Loo.ContestantName, "P", cwi.PriceType, (double)cwi.Loo.MoneyLine, toBase, (double)cwi.Loo.DecimalOdds,
                                        (int)cwi.Loo.Numerator, (int)cwi.Loo.Denominator, WebLibs.CommonFunctions.Round(cwi.RiskAmt) * 100, WebLibs.CommonFunctions.Round(cwi.ToWinAmt) * 100, (double)cwi.Loo.MoneyLine,
                                        toBase, (double)cwi.Loo.DecimalOdds, (int)cwi.Loo.Numerator, (int)cwi.Loo.Denominator, cwi.Loo.Store, cwi.Loo.CustProfile,
                                        cwi.Loo.XToYLineRep, cwi.Loo.CategoryInfo.ContestType, cwi.Loo.CategoryInfo.ContestType2, cwi.Loo.CategoryInfo.ContestType3,
                                        cwi.Loo.ContestantNum, WebLibs.SessionVariablesCore.CustomerInfo.PercentBook ?? 0, volumeAmount, showOnChartFlag, thresholdType, thresholdUnits, thresholdLine,
                                        WebLibs.SessionVariablesCore.CurrencyCode, valueDateStr, agentId);
        cwi.TicketNumber = SessionVariables.Ticket.TicketNumber;
        WebLibs.SystemLog.WriteAccessLog("Final Item accepted for " + WebLibs.SessionVariablesCore.WagerType.Name, "Item #" + cwi.ItemNumber + ": " + LineOfferingWeb.FormatShortContestDescription(cwi) + ". Risking $" + DisplayNumber.Format(cwi.RiskAmt, true, 1, true) + " to win $" + DisplayNumber.Format(cwi.ToWinAmt, true, 1, true));
        var availableBalance = WebLibs.SessionVariablesCore.AvailableBalance;

        availableBalance -= cwi.RiskAmt;
        WebLibs.SessionVariablesCore.AvailableBalance = availableBalance;
        taW.InsertWebStage(SessionVariables.Ticket.TicketNumber, itemsCnt + 1);
        itemsCnt++;
      }
      if (itemsCnt > 0) WebLibs.SystemLog.WriteAccessLog("Future(s) /Prop(s) Accepted", "Ticket Number -" + SessionVariables.Ticket.TicketNumber);
    }

    public static void ProcessIfBets(WebWager wager) {
      int docNum;
      using (var tr = new Transactions(ModuleInformation)) {
        docNum = tr.GetNextDocumentNumber();
      }
      using (var taW = new TicketsAndWagers(ModuleInformation)) {

        const int playCount = 1;

        var store = "";
        if (WebLibs.SessionVariablesCore.CustomerInfo.Store != null)
          store = WebLibs.SessionVariablesCore.CustomerInfo.Store;

        String agentId = null;
        if (WebLibs.SessionVariablesCore.CustomerInfo.AgentID != null)
          agentId = WebLibs.SessionVariablesCore.CustomerInfo.AgentID;

        var wiAry = ((from li in wager.WebGameWagerItems select li).ToList());
        var wiAryCount = (from p in wager.WebGameWagerItems select p).Count();

        if (!InsertTicket(taW, docNum, playCount, store, agentId)) return;

        var totalToWinAmt = (from p in wiAry select p.ToWinAmt).Sum();

        var descStr = "";
        for (var i = 0; i < wiAryCount; i++) {
          if (i > 0) {
            descStr += "\r\n";
          }
          descStr += LineOfferingWeb.FormatShortDescription(wiAry[i], WebLibs.SessionVariablesCore.WagerType.Name, WebLibs.SessionVariablesCore.EasternLineFlag);
        }

        var wagerNumber = 1;
        const string wagerType = "I";
        const string ties = "0";
        const string parlayName = "";
        const string teaserName = "";
        const int playNumber = 1;
        const int boxLink = 0;
        const int maxPayout = 0;
        const int rifTicketNumber = 0;
        const int rifWagerNumber = 0;

        var minPicks = WebLibs.SessionVariablesCore.WagerMinPicks;
        var itemCount = wiAryCount;
        var continueOnPushFlag = "Y";
        var arLink = 0;

        if (WebLibs.SessionVariablesCore.WagerType.Name == WagerType.ACTREVERSENAME) {
          arLink = 1;
          minPicks = 1;
        }

        if (WebLibs.SessionVariablesCore.WagerType.Name == WagerType.IFWINONLYNAME) {
          continueOnPushFlag = "N";
        }

        var creditAccountFlag = "";
        if (WebLibs.SessionVariablesCore.CustomerInfo.CreditAcctFlag != null)
          creditAccountFlag = WebLibs.SessionVariablesCore.CustomerInfo.CreditAcctFlag;
        var currencyCode = "";
        if (WebLibs.SessionVariablesCore.CurrencyCode != null)
          currencyCode = WebLibs.SessionVariablesCore.CurrencyCode;
        var valueDateStr = WebLibs.SessionVariablesCore.ServerDateTime.ToShortDateString();
        var freePlayFlag = "N";
        if (WebLibs.SessionVariablesCore.FreePlay)
          freePlayFlag = "Y";

        double riskAmount;

        if (WebLibs.SessionVariablesCore.WagerType.Name == WagerType.IFWINONLYNAME || WebLibs.SessionVariablesCore.WagerType.Name == WagerType.IFWINORPUSHNAME) {
          riskAmount = WebLibs.SessionVariablesCore.MaxRiskAmount;
        }
        else {
          riskAmount = WebLibs.SessionVariablesCore.MaxRiskAmount1;
        }

        taW.InsertWebWager(docNum, wagerNumber, WebLibs.CommonFunctions.Round(riskAmount * 100), wagerType, descStr, WebLibs.CommonFunctions.Round(totalToWinAmt * 100), minPicks, ties,
                           itemCount, continueOnPushFlag, parlayName, teaserName, creditAccountFlag, playNumber, arLink, null, boxLink,
                           currencyCode, valueDateStr, maxPayout, null, freePlayFlag, rifTicketNumber, rifWagerNumber, null);

        var descLog = "";
        for (var i = 0; i < wiAryCount; i++) {
          if (wiAry[i].HalfPointValue > 0 && wiAry[i].HalfPointAdded) {
            wiAry[i].FinalLine += wiAry[i].HalfPointValue;
          }
          InsertWagerItem(taW, docNum, wiAry[i], wagerNumber, i + 1, store, currencyCode, valueDateStr, agentId, WebLibs.SessionVariablesCore.WagerType.Name);
          WebLibs.SystemLog.WriteAccessLog("Final Item accepted for " + WebLibs.SessionVariablesCore.WagerType.Name, FormatWagerAccessLog(wiAry[i], i + 1, i == 0) + ". Risking $" + DisplayNumber.Format(wiAry[i].RiskAmt, true, 1, true) + " to win $" + DisplayNumber.Format(wiAry[i].ToWinAmt, true, 1, true));
          descLog = descLog + FormatWagerAccessLog(wiAry[i], i + 1, i == 0);
        }

        taW.InsertWebStage(docNum, 1);

        if (WebLibs.SessionVariablesCore.WagerType.Name == WagerType.ACTREVERSENAME) {
          wiAry.Reverse();

          descStr = "";
          for (var i = 0; i < wiAryCount; i++) {
            if (i > 0) {
              descStr += "\r\n";
            }
            descStr += LineOfferingWeb.FormatShortDescription(wiAry[i], WebLibs.SessionVariablesCore.WagerType.Name, WebLibs.SessionVariablesCore.EasternLineFlag);
          }

          wagerNumber = 2;

          taW.InsertWebWager(docNum, wagerNumber, WebLibs.CommonFunctions.Round(WebLibs.SessionVariablesCore.MaxRiskAmount2 * 100), wagerType, descStr, WebLibs.CommonFunctions.Round(totalToWinAmt * 100), minPicks, ties,
                 itemCount, continueOnPushFlag, parlayName, teaserName, creditAccountFlag, playNumber, arLink, null, boxLink,
                 currencyCode, valueDateStr, maxPayout, null, freePlayFlag, rifTicketNumber, rifWagerNumber, null);


          for (var i = 0; i < wiAryCount; i++) {
            InsertWagerItem(taW, docNum, wiAry[i], wagerNumber, i + 1, store, currencyCode, valueDateStr, agentId, WebLibs.SessionVariablesCore.WagerType.Name);
            WebLibs.SystemLog.WriteAccessLog("Final Item accepted for " + WebLibs.SessionVariablesCore.WagerType.Name, FormatWagerAccessLog(wiAry[i], i + 1, i == 0) + ". Risking $" + DisplayNumber.Format(wiAry[i].RiskAmt, true, 1, true) + " to win $" + DisplayNumber.Format(wiAry[i].ToWinAmt, true, 1, true));
          }

          taW.InsertWebStage(docNum, 2);
        }

        taW.InsertWebStage(docNum, 0);
        var availableBalance = WebLibs.SessionVariablesCore.AvailableBalance;
        availableBalance -= riskAmount;
        WebLibs.SessionVariablesCore.AvailableBalance = availableBalance;

        WebLibs.SystemLog.WriteAccessLog("Action Reverse(s) Accepted", "Ticket Number -" + docNum + " :" + descLog);
      }
      SessionVariables.Ticket.TicketNumber = docNum;
    }

    private static int ProcessStraightBets(TicketsAndWagers taW, WebWager wager, string store, string agentId) {
      try {
        if (!wager.HasGameWagers) return 0;

        var wagerNumber = 1;
        foreach (var wi in wager.WebGameWagerItems) {

          wi.WagerNumber = wagerNumber++;

          var t = wager.WebGameWagerItems
            .Where(w => w.WagerNumber != wi.WagerNumber && w.WagerNumber > 0 && w.Loo.GameNum == wi.Loo.GameNum && w.Loo.PeriodNumber == wi.Loo.PeriodNumber && w.WagerType == wi.WagerType)
            .Sum(w => w.RiskAmt <= w.ToWinAmt ? w.RiskAmt : w.ToWinAmt);

          wi.HalfPointAdded = WebLibs.Utilities.ApplyHalfPointAdjustment(wi, t, ModuleInformation);
        }

        var itemsCnt = 0;
        foreach (var wi in wager.WebGameWagerItems) {
          /*if (!wi.HalfPointAdded) {
            descStr = LineOfferingWeb.FormatShortDescription(wi, WebLibs.SessionVariablesCore.WagerType.Name, WebLibs.SessionVariablesCore.EasternLineFlag);
          }*/
          const int minPicks = 1;
          const string ties = "0";
          const int itemCount = 1;
          const string continueOnPushFlag = "";
          const string parlayName = "";
          const string teaserName = "";
          const int arLink = 0;
          const int boxLink = 0;
          const int maxPayout = 0;

          var creditAccountFlag = WebLibs.SessionVariablesCore.CustomerInfo.CreditAcctFlag ?? "Y";

          var currencyCode = WebLibs.SessionVariablesCore.CurrencyCode ?? string.Empty;
          var valueDateStr = WebLibs.SessionVariablesCore.ServerDateTime.ToShortDateString();
          var freePlayFlag = WebLibs.SessionVariablesCore.FreePlay ? "Y" : "N";
          var rifWinOnlyFlag = wi.RifWinOnly ? "Y" : null;

          if (wi.HalfPointValue > 0 && wi.HalfPointAdded) {
            wi.FinalLine += wi.HalfPointValue;
          }
          var descStr = LineOfferingWeb.FormatShortDescription(wi, WebLibs.SessionVariablesCore.WagerType.Name, WebLibs.SessionVariablesCore.EasternLineFlag);
          wagerNumber = itemsCnt + 1;
          taW.InsertWebWager(SessionVariables.Ticket.TicketNumber, wagerNumber, WebLibs.CommonFunctions.Round(wi.RiskAmt) * 100, wi.WagerType, descStr, WebLibs.CommonFunctions.Round(wi.ToWinAmt) * 100, minPicks, ties,
                             itemCount, continueOnPushFlag, parlayName, teaserName, creditAccountFlag, itemsCnt + 1, arLink, null, boxLink,
                             currencyCode, valueDateStr, maxPayout, null, freePlayFlag, wi.RifTicketNumber, wi.RifWagerNumber, rifWinOnlyFlag);

          const int itemNumber = 1;
          InsertWagerItem(taW, SessionVariables.Ticket.TicketNumber, wi, wagerNumber, itemNumber, store, currencyCode, valueDateStr, agentId, WebLibs.SessionVariablesCore.WagerType.Name);
          var availableBalance = WebLibs.SessionVariablesCore.AvailableBalance;

          availableBalance -= wi.RiskAmt;
          WebLibs.SessionVariablesCore.AvailableBalance = availableBalance;
          wi.TicketNumber = SessionVariables.Ticket.TicketNumber;
          wi.WagerNumber = wagerNumber;
          wi.ItemNumber = itemNumber;
          taW.InsertWebStage(SessionVariables.Ticket.TicketNumber, ++itemsCnt);
          WebLibs.SystemLog.WriteAccessLog("Final Item accepted for " + WebLibs.SessionVariablesCore.WagerType.Name, "Item #" + wi.WagerNumber + ": " + descStr + ". Risking $" + DisplayNumber.Format(wi.RiskAmt, true, 1, true) + " to win $" + DisplayNumber.Format(wi.ToWinAmt, true, 1, true));
        }
        WebLibs.SystemLog.WriteAccessLog("Straight Bet(s) Accepted", "Ticket Number - " + SessionVariables.Ticket.TicketNumber);
        return itemsCnt;
      }
      catch (Exception ex) {
        SystemLog.Log(ex);
        return 0;
      }
    }

    public static void ProcessStraightAndContestBets(WebWager wager) {
      try {
        using (var tr = new Transactions(ModuleInformation)) {
          SessionVariables.Ticket.TicketNumber = tr.GetNextDocumentNumber();
        }
        using (var taW = new TicketsAndWagers(ModuleInformation)) {

          var store = WebLibs.SessionVariablesCore.CustomerInfo.Store ?? string.Empty;
          var agentId = WebLibs.SessionVariablesCore.CustomerInfo.AgentID ?? string.Empty;

          if (!InsertTicket(taW, SessionVariables.Ticket.TicketNumber, wager.TotalWagerItems, store, agentId)) return;

          var itemsCnt = ProcessStraightBets(taW, wager, store, agentId);
          ProcessContestWagers(taW, wager, agentId, itemsCnt);

          taW.InsertWebStage(SessionVariables.Ticket.TicketNumber, 0);

        }
        WebLibs.SessionVariablesCore.TicketToWinAmount = WebLibs.SessionVariablesCore.ToWinAmount;
        WebLibs.SessionVariablesCore.TicketRiskAmount = WebLibs.SessionVariablesCore.RiskAmount;
      }
      catch (Exception ex) {
        CoreService.Log(ex);
      }
    }

    public static void ProcessParlays(WebWager wager) {
      int docNum;
      using (var tr = new Transactions(ModuleInformation)) {
        docNum = tr.GetNextDocumentNumber();
      }
      using (var taW = new TicketsAndWagers(ModuleInformation)) {

        foreach (var wi in wager.WebGameWagerItems.Where(wi => !wi.IsExistingOpenItem))
          CalculateWiWagerAmount(wi);

        var store = "";
        if (WebLibs.SessionVariablesCore.CustomerInfo.Store != null)
          store = WebLibs.SessionVariablesCore.CustomerInfo.Store;

        String agentId = null;
        if (WebLibs.SessionVariablesCore.CustomerInfo.AgentID != null)
          agentId = WebLibs.SessionVariablesCore.CustomerInfo.AgentID;

        if (!InsertTicket(taW, docNum, 1, store, agentId)) return;

        var freePlayFlag = "N";
        if (WebLibs.SessionVariablesCore.FreePlay)
          freePlayFlag = "Y";

        var wiAry = (!wager.OpenPlay.Any() ? (from li in wager.WebGameWagerItems select li).ToList() : (from li in wager.WebGameWagerItems select li).Except(wager.OpenPlay.ToList()).ToList());
        var wiAryCount = wiAry.Count();

        var creditAccountFlag = "";
        if (WebLibs.SessionVariablesCore.CustomerInfo.CreditAcctFlag != null)
          creditAccountFlag = WebLibs.SessionVariablesCore.CustomerInfo.CreditAcctFlag;

        var currencyCode = "";
        if (WebLibs.SessionVariablesCore.CurrencyCode != null)
          currencyCode = WebLibs.SessionVariablesCore.CurrencyCode;
        var valueDateStr = WebLibs.SessionVariablesCore.ServerDateTime.ToShortDateString();

        var parlayPayoutType = WebLibs.SessionVariablesCore.ParlayPayoutType;
        var descLog = "";

        if (WebLibs.SessionVariablesCore.RoundRobinValue > 0) {
          InsertRoundRobinWagers(taW, docNum, wiAry, wiAryCount, freePlayFlag, creditAccountFlag, currencyCode, valueDateStr, parlayPayoutType);
        }
        else {
          var descStr = "";
          for (var i = 0; i < wiAryCount; i++) {
            if (wiAry[i].HalfPointValue > 0 && wiAry[i].HalfPointAdded) {
              wiAry[i].FinalLine += wiAry[i].HalfPointValue;
            }
            if (i > 0) {
              descStr += "\r\n";
            }
            descStr += LineOfferingWeb.FormatShortDescription(wiAry[i], WebLibs.SessionVariablesCore.WagerType.Name, WebLibs.SessionVariablesCore.EasternLineFlag);
          }

          String rifWinOnlyFlag = null;
          if (wiAry[0].RifWinOnly)
            rifWinOnlyFlag = "Y";

          const int wagerNumber = 1;
          const string wagerType = "P";
          const string ties = "0";
          const string continueOnPushFlag = "";
          const string teaserName = "";
          const int playNumber = 1;
          const int arLink = 0;
          const int boxLink = 0;
          var minPicks = WebLibs.SessionVariablesCore.WagerMinPicks;

          if ((WebLibs.SessionVariablesCore.CustomerInfo.ParlayMaxPayout ?? 0) > 0 &&
              WebLibs.SessionVariablesCore.ToWinAmount > (WebLibs.SessionVariablesCore.CustomerInfo.ParlayMaxPayout) / 100)
            WebLibs.SessionVariablesCore.ToWinAmount = (WebLibs.SessionVariablesCore.CustomerInfo.ParlayMaxPayout ?? 0) / 100;

          if (!SessionVariables.Ticket.OpenPlay.Any())
            taW.InsertWebWager(docNum, wagerNumber, WebLibs.CommonFunctions.Round(WebLibs.SessionVariablesCore.RiskAmount) * 100,
                wagerType, descStr,
                WebLibs.CommonFunctions.Round(double.Parse(WebLibs.SessionVariablesCore.ToWinAmount.ToString(CultureInfo.InvariantCulture))) * 100,
                minPicks,
                ties,
                SessionVariables.Ticket.OpenPlay.KeepOpenPlay ? SessionVariables.Ticket.OpenPlay.OpenPlayCount : wiAryCount,
                continueOnPushFlag, WebLibs.SessionVariablesCore.ParlayName, teaserName,
                creditAccountFlag, playNumber, arLink, null, boxLink, currencyCode, valueDateStr,
                int.Parse(WebLibs.SessionVariablesCore.MaxParlayPayout.ToString(CultureInfo.InvariantCulture)) *
                100,
                parlayPayoutType, freePlayFlag, wiAry[0].RifTicketNumber, wiAry[0].RifWagerNumber,
                rifWinOnlyFlag, (SessionVariables.Ticket.OpenPlay.KeepOpenPlay ? "O" : "P"));
          else taW.UpdateWagerDescription(SessionVariables.Ticket.OpenPlay.OpenTicketNumber, SessionVariables.Ticket.OpenPlay.OpenWagerNumber, descStr, WebLibs.CommonFunctions.Round(double.Parse(WebLibs.SessionVariablesCore.ToWinAmount.ToString(CultureInfo.InvariantCulture))) * 100);
          for (var i = 0; i < wiAryCount; i++) {
            var itemNumber = (SessionVariables.Ticket.OpenPlay.Any() ? SessionVariables.Ticket.OpenPlay.Count() + i + 1 : i + 1);
            InsertWagerItem(taW, docNum, wiAry[i], 1, itemNumber, store, currencyCode, valueDateStr, agentId,
                WebLibs.SessionVariablesCore.WagerType.Name);
            wager.WebGameWagerItems[i].RiskAmt = WebLibs.SessionVariablesCore.RiskAmount;
            wager.WebGameWagerItems[i].ToWinAmt = WebLibs.SessionVariablesCore.ToWinAmount;
            WebLibs.SystemLog.WriteAccessLog(
                "Final Item accepted for " + WebLibs.SessionVariablesCore.WagerType.Name,
                "Item #" + wiAry[i].ItemNumber + ": " +
                LineOfferingWeb.FormatShortDescription(wiAry[i], WebLibs.SessionVariablesCore.WagerType.Name,
                    WebLibs.SessionVariablesCore.EasternLineFlag) + ". Risking $" +
                DisplayNumber.Format(wiAry[i].RiskAmt, true, 1, true) + " to win $" +
                DisplayNumber.Format(wiAry[i].ToWinAmt, true, 1, true) + " / Parlay status:" + (SessionVariables.Ticket.OpenPlay.KeepOpenPlay ? "Open" : "Pending"));
            descLog = descLog + FormatWagerAccessLog(wiAry[i], itemNumber, i == 0);
          }
          taW.InsertWebStage(SessionVariables.Ticket.OpenPlay.Any() ? SessionVariables.Ticket.OpenPlay.OpenTicketNumber : docNum, SessionVariables.Ticket.OpenPlay.Any() ? SessionVariables.Ticket.OpenPlay.OpenWagerNumber : 1, SessionVariables.Ticket.OpenPlay.Any() ? docNum : (int?)null);

        }

        var availableBalance = WebLibs.SessionVariablesCore.AvailableBalance;
        availableBalance -= WebLibs.SessionVariablesCore.RiskAmount;
        WebLibs.SessionVariablesCore.AvailableBalance = availableBalance;
        taW.InsertWebStage(SessionVariables.Ticket.OpenPlay.Any() ? SessionVariables.Ticket.OpenPlay.OpenTicketNumber : docNum, 0, SessionVariables.Ticket.OpenPlay.Any() ? docNum : (int?)null);
        WebLibs.SystemLog.WriteAccessLog("Parlay(s) Accepted", "Ticket Number -" + docNum + " :" + descLog);
      }
      SessionVariables.Ticket.TicketNumber = docNum;
    }

    public static void ProcessTeasers(WebWager wager) {

      if (WebLibs.SessionVariablesCore.TssAry == null || !WebLibs.SessionVariablesCore.TssAry.Any()) {
        var ex = new Exception("Error loading teaser sports specs on Service: ProcessTeasers");
        if (WebLibs.SessionVariablesCore.InstanceManager != null) WebLibs.SystemLog.WriteToSystemLog(ex);
        throw ex;
      }

      int docNum;
      using (var tr = new Transactions(ModuleInformation)) {
        docNum = tr.GetNextDocumentNumber();
      }
      const int playCount = 1;
      using (var taW = new TicketsAndWagers(ModuleInformation)) {

        var store = "";
        if (WebLibs.SessionVariablesCore.CustomerInfo.Store != null)
          store = WebLibs.SessionVariablesCore.CustomerInfo.Store;
        String agentId = null;
        if (WebLibs.SessionVariablesCore.CustomerInfo.AgentID != null)
          agentId = WebLibs.SessionVariablesCore.CustomerInfo.AgentID;

        foreach (var wi in wager.WebGameWagerItems) {
          wi.RiskAmt = WebLibs.SessionVariablesCore.RiskAmount;
          wi.ToWinAmt = 0;
        }

        if (!InsertTicket(taW, docNum, playCount, store, agentId)) return;

        var freePlayFlag = "N";
        if (WebLibs.SessionVariablesCore.FreePlay)
          freePlayFlag = "Y";

        var wiAry = (!SessionVariables.Ticket.OpenPlay.Any() ? (from li in wager.WebGameWagerItems select li).ToList() : (from li in wager.WebGameWagerItems select li).Except(wager.OpenPlay.ToList()).ToList());
        var wiAryCount = wiAry.Count();

        var creditAccountFlag = "";
        if (WebLibs.SessionVariablesCore.CustomerInfo.CreditAcctFlag != null)
          creditAccountFlag = WebLibs.SessionVariablesCore.CustomerInfo.CreditAcctFlag;

        var currencyCode = "";
        if (WebLibs.SessionVariablesCore.CurrencyCode != null)
          currencyCode = WebLibs.SessionVariablesCore.CurrencyCode;

        var valueDateStr = WebLibs.SessionVariablesCore.ServerDateTime.ToShortDateString();

        var descLog = "";

        var descStr = "";
        for (var i = 0; i < wiAryCount; i++) {
          if (wiAry[i].HalfPointValue > 0 && wiAry[i].HalfPointAdded) {
            wiAry[i].FinalLine += wiAry[i].HalfPointValue;
          }
          if (i > 0) descStr += "\r\n";
          descStr += LineOfferingWeb.FormatShortDescription(wiAry[i], WebLibs.SessionVariablesCore.WagerType.Name, WebLibs.SessionVariablesCore.EasternLineFlag);
        }

        String rifWinOnlyFlag = null;
        if (wiAry[0].RifWinOnly)
          rifWinOnlyFlag = "Y";

        const int wagerNumber = 1;
        const string wagerType = "T";
        const string continueOnPushFlag = "Y";
        const int playNumber = 1;
        const int arLink = 0;
        const int boxLink = 0;
        const int maxPayout = 0;

        var minPicks = WebLibs.SessionVariablesCore.TeaserSpecs.MinPicks;
        var teaserTies = WebLibs.SessionVariablesCore.TeaserSpecs.TeaserTies;
        var teaserName = WebLibs.SessionVariablesCore.TeaserSpecs.TeaserName;
        if (!SessionVariables.Ticket.OpenPlay.Any()) taW.InsertWebWager(docNum, wagerNumber, WebLibs.CommonFunctions.Round(WebLibs.SessionVariablesCore.RiskAmount * 100),
             wagerType, descStr,
             WebLibs.CommonFunctions.Round(double.Parse(WebLibs.SessionVariablesCore.ToWinAmount.ToString(CultureInfo.InvariantCulture)) * 100), minPicks,
             teaserTies.ToString(CultureInfo.InvariantCulture), (SessionVariables.Ticket.OpenPlay.KeepOpenPlay ? SessionVariables.Ticket.OpenPlay.OpenPlayCount : wiAryCount), continueOnPushFlag, null,
             teaserName, creditAccountFlag, playNumber, arLink, null, boxLink, currencyCode, valueDateStr,
             maxPayout, null, freePlayFlag, wiAry[0].RifTicketNumber, wiAry[0].RifWagerNumber, rifWinOnlyFlag, (SessionVariables.Ticket.OpenPlay.KeepOpenPlay ? "O" : "P"));
        else taW.UpdateWagerDescription(SessionVariables.Ticket.OpenPlay.OpenTicketNumber, SessionVariables.Ticket.OpenPlay.OpenWagerNumber, descStr, WebLibs.CommonFunctions.Round(double.Parse(WebLibs.SessionVariablesCore.ToWinAmount.ToString(CultureInfo.InvariantCulture))) * 100);
        for (var i = 0; i < wiAryCount; i++) {
          var itemNumber = (SessionVariables.Ticket.OpenPlay.Any() ? SessionVariables.Ticket.OpenPlay.Count() + i + 1 : i + 1);
          wiAry[i].FinalLine = WebLibs.Utilities.GetBaseLineByControlId(wiAry[i]);
          InsertWagerItem(taW, docNum, wiAry[i], 1, itemNumber, store, currencyCode, valueDateStr, agentId,
              WebLibs.SessionVariablesCore.WagerType.Name);
          WebLibs.SystemLog.WriteAccessLog(
              "Final Item accepted for " + WebLibs.SessionVariablesCore.WagerType.Name,
              "Item #" + wiAry[i].ItemNumber + ": " +
              LineOfferingWeb.FormatShortDescription(wiAry[i], WebLibs.SessionVariablesCore.WagerType.Name,
                  WebLibs.SessionVariablesCore.EasternLineFlag) + ". Risking $" +
              DisplayNumber.Format(wiAry[i].RiskAmt, true, 1, true) + " to win $" +
              DisplayNumber.Format(wiAry[i].ToWinAmt, true, 1, true));
          descLog += FormatWagerAccessLog(wiAry[i], itemNumber, i == 0);
        }
        taW.InsertWebStage(SessionVariables.Ticket.OpenPlay.Any() ? SessionVariables.Ticket.OpenPlay.OpenTicketNumber : docNum, SessionVariables.Ticket.OpenPlay.Any() ? SessionVariables.Ticket.OpenPlay.OpenWagerNumber : 1, SessionVariables.Ticket.OpenPlay.Any() ? docNum : (int?)null);

        var availableBalance = WebLibs.SessionVariablesCore.AvailableBalance;
        availableBalance -= WebLibs.SessionVariablesCore.RiskAmount;
        WebLibs.SessionVariablesCore.AvailableBalance = availableBalance;
        taW.InsertWebStage(SessionVariables.Ticket.OpenPlay.Any() ? SessionVariables.Ticket.OpenPlay.OpenTicketNumber : docNum, 0, SessionVariables.Ticket.OpenPlay.Any() ? docNum : (int?)null);
        WebLibs.SystemLog.WriteAccessLog("Teaser(s) Accepted", "Ticket Number -" + docNum + " :" + descLog);
      }
      SessionVariables.Ticket.TicketNumber = docNum;
    }

    public static void UpdateWagerItems(IReadOnlyList<WagerItemUpdateData> wagersData, WebWager wager) {
      UpdateContestWagerItems(wagersData, wager);
      UpdateGameWagerItems(wagersData, wager);
    }

    private static void UpdateContestWagerItems(IReadOnlyList<WagerItemUpdateData> wagersData, WebWager wager) {
      if (wagersData == null || !wagersData.Any(c => c.ContestNum > 0) || SessionVariables.Ticket == null || wager.WebContestantWagerItems == null) return;
      foreach (var wagerItemUpdate in wagersData.Where(c => c.ContestNum > 0)) {
        var wagerItem = (from wi in wager.WebContestantWagerItems
                         where wi.ContestNum == wagerItemUpdate.ContestNum &&
                         wi.ContestantNum == wagerItemUpdate.ContestantNum
                         select wi).FirstOrDefault();
        if (wagerItem == null) return;
        wagerItem.RiskAmt = wagerItemUpdate.RiskAmount;
        wagerItem.ToWinAmt = wagerItemUpdate.ToWinAmount;
        wagerItem.Loo.MoneyLine = wagerItemUpdate.FinalPrice;
        wagerItem.Loo.ThresholdLine = wagerItemUpdate.FinalLine;
        wagerItem.Loo.ToBase = wagerItemUpdate.ToBase;

        CalculateCnWagerAmount(wagerItem);
      }

    }

    private static void UpdateGameWagerItems(IReadOnlyList<WagerItemUpdateData> wagersData, WebWager wager) {
      if (wagersData == null || !wagersData.Any(g => g.GameNum > 0) || SessionVariables.Ticket == null || !wager.HasGameWagers) return;

      WebLibs.SessionVariablesCore.RoundRobinValue = wagersData[0].RoundRobinValue;
      var idx = 0;
      foreach (var wagerItemUpdate in wagersData.Where(g => g.GameNum > 0)) {
        idx++;
        var wagerItem = (from wIt in wager.WebGameWagerItems
                         where wIt.ControlCode == wagerItemUpdate.ControlCode && wIt.Loo.GameNum == wagerItemUpdate.GameNum &&
                               wIt.Loo.PeriodNumber == wagerItemUpdate.PeriodNumber
                         select wIt).FirstOrDefault();

        if (wagerItem == null) continue;

        if (WebLibs.SessionVariablesCore.WagerType.WebCode == WagerType.WEBACTREVERSE) {

          if (wagerItem.FinalPrice < 0) {
            wagerItem.ToWinAmt = wagerItemUpdate.ArAmount ?? 0;
            wagerItem.AmountEntered = WebLibs.WagerItem.AmountEnteredType.ToWinAmt;
          }
          else {
            wagerItem.RiskAmt = wagerItemUpdate.ArAmount ?? 0;
            wagerItem.AmountEntered = WebLibs.WagerItem.AmountEnteredType.RiskAmt;
          }
          CalculateWiWagerAmount(wagerItem);

          //          wagerItem.RiskAmt = wagerItemUpdate.RiskAmount / 2;
          //wagerItem.ToWinAmt = wagerItemUpdate.ToWinAmount / 2;
        }
        else {
          wagerItem.RiskAmt = wagerItemUpdate.RiskAmount;
          wagerItem.ToWinAmt = wagerItemUpdate.ToWinAmount;

          if (WebLibs.SessionVariablesCore.WagerType.WebCode == WagerType.WEBSTRAIGHT) {
            if (wagerItem.HalfPointAdded) {
              var volumeAmt = wagerItem.RiskAmt > wagerItem.ToWinAmt ? wagerItem.ToWinAmt : wagerItem.RiskAmt;
              if (volumeAmt > WebLibs.SessionVariablesCore.HalfPointMaxBet + (WebLibs.SessionVariablesCore.MinimumWagerAmt / 100)) wagerItem.HalfPointNewWager = true; // not used
            }
          }
        }
        wagerItem.AmountEntered = wagerItemUpdate.AmountEntered;
        wagerItem.FinalLine = wagerItemUpdate.FinalLine ?? 0;
        wagerItem.FinalPrice = wagerItemUpdate.FinalPrice;

        if (wagerItem.Loo.SportType.Trim() == "Baseball") {
          wagerItem.Pitcher1ReqFlag = wagerItemUpdate.Pitcher1ReqFlag;
          wagerItem.Pitcher2ReqFlag = wagerItemUpdate.Pitcher2ReqFlag;
          wagerItem.AdjustableOddsFlag = DetermineAdjustableOddsFlag(wagerItem, wagerItem.Pitcher1ReqFlag, wagerItem.Pitcher2ReqFlag, WebLibs.SessionVariablesCore.CustomerInfo.BaseballAction);
        }
        else {
          wagerItem.AdjustableOddsFlag = DetermineAdjustableOddsFlag(wagerItem, false, false, WebLibs.SessionVariablesCore.CustomerInfo.BaseballAction);
          wagerItem.Pitcher1ReqFlag = wagerItem.Pitcher2ReqFlag = false;
        }

        if (WebLibs.SessionVariablesCore.RoundRobinValue > 0)
          WebLibs.SessionVariablesCore.PlayCount = wagerItemUpdate.PlayCount;
        else if (WebLibs.SessionVariablesCore.WagerType.WebCode == WagerType.WEBPARLAY)
          WebLibs.SessionVariablesCore.PlayCount = 1;

        /* if (adjustFinalLine)
            CalculateFinalPrice(wi, line, price); */

        CalculateWiWagerAmount(wagerItem);

        if (WebLibs.SessionVariablesCore.WagerType.WebCode == WagerType.WEBSTRAIGHT || WebLibs.SessionVariablesCore.WagerType.WebCode == WagerType.WEBIFWINONLY || WebLibs.SessionVariablesCore.WagerType.WebCode == WagerType.WEBIFWINORPUSH)
          WebLibs.SystemLog.WriteAccessLog("Updating " + WebLibs.SessionVariablesCore.WagerType.Name + " Wagers", "Selection #: " + idx + ", WagerType:" + wagerItem.WagerType + ", Risking: " + wagerItem.RiskAmt + ", To Win: " + wagerItem.ToWinAmt);
      }
      if (wagersData.Count > 0 && WebLibs.SessionVariablesCore.WagerType.WebCode != WagerType.WEBSTRAIGHT && WebLibs.SessionVariablesCore.WagerType.WebCode != WagerType.WEBIFWINONLY && WebLibs.SessionVariablesCore.WagerType.WebCode != WagerType.WEBIFWINORPUSH)
        WebLibs.SystemLog.WriteAccessLog("Updating" + WebLibs.SessionVariablesCore.WagerType.Name + " Wagers", "Risking: " + wagersData[0].RiskAmount + ", To Win: " + wagersData[0].ToWinAmount);
    }

    private static string DetermineAdjustableOddsFlag(WebLibs.WagerItem wi, bool pitcher1ReqFlag, bool pitcher2ReqFlag, String sessionBaseballAction) {
      if (wi.Loo.SportType.Trim() != "Baseball" || wi.Loo.ListedPitcher1 == "null" || wi.Loo.ListedPitcher2 == "null" ||
          sessionBaseballAction == "Fixed" || wi.WagerType != "M")
        return "N";

      if (!pitcher1ReqFlag || !pitcher2ReqFlag)
        return "Y";

      return "N";
    }

    private static bool ValidateBuyPointsOptions(WebWager wager) {
      if (wager.WebGameWagerItems == null || !wager.WebGameWagerItems.Any()) return true;
      foreach (var wi in wager.WebGameWagerItems.Where(wi => wi.PointsBought > 0 && (wi.Loo.SportType == SportTypes.FOOTBALL || wi.Loo.SportType == SportTypes.BASKETBALL))) {
        SetLineAdjUnit(wi.ControlCode);
        WebWagerItem.GetBuyInfo(wi.Loo.SportType.Trim(), wi.Loo.SportSubType.Trim());
        if (ValidateBuyPointsOption(wi.PointsBought, wi.Line, wi.ControlCode)) continue;
        wi.ValidationErrorMessage = WebLibs.SessionVariablesCore.Translate("BUYING_PTS_LINE_CHANGED");
        return false;
      }
      return true;
    }

    private static bool ValidateBuyPointsOption(double buyValue, double line, string controlCode) {
      int buyMax;
      int buyOn3;
      int buyOff3;
      int buyOn7;
      int buyOff7;
      var lineAdj = WebLibs.SessionVariablesCore.LineAdjUnit;

      if (controlCode.Substring(0, 1) == WagerType.SPREAD) {
        buyMax = WebLibs.SessionVariablesCore.SpreadBuyMax;
        buyOn3 = WebLibs.SessionVariablesCore.SpreadBuyOn3;
        buyOff3 = WebLibs.SessionVariablesCore.SpreadBuyOff3;

        buyOn7 = WebLibs.SessionVariablesCore.SpreadBuyOn7;
        buyOff7 = WebLibs.SessionVariablesCore.SpreadBuyOff7;
      }
      else {
        buyMax = WebLibs.SessionVariablesCore.TotalBuyMax;
        buyOn3 = WebLibs.SessionVariablesCore.TotalBuy;
        buyOff3 = WebLibs.SessionVariablesCore.TotalBuy;

        buyOn7 = WebLibs.SessionVariablesCore.TotalBuy;
        buyOff7 = WebLibs.SessionVariablesCore.TotalBuy;

        if (controlCode.Substring(1, 1) == "1")
          lineAdj = -.5;
      }
      int decimalPrecision; // 3; // GetDecimalOddsPrecision();
      int.TryParse(WebLibs.SessionVariablesCore.DecimalOddsPrecision.ToString(CultureInfo.InvariantCulture), out decimalPrecision);

      int maxDenominator; //GetMaxDenominator(); 
      int.TryParse(WebLibs.SessionVariablesCore.MaxDenominator.ToString(CultureInfo.InvariantCulture), out maxDenominator);

      for (var i = 0; i <= buyMax; i++) {
        if (buyOn3 == 0 && Math.Abs(line + lineAdj * i) == 3)
          break;
        if (buyOff3 == 0 && Math.Abs(line + lineAdj * (i - 1)) == 3)
          break;
        if (buyOn7 == 0 && Math.Abs(line + lineAdj * i) == 7)
          break;
        if (buyOff7 == 0 && Math.Abs(line + lineAdj * (i - 1)) == 7)
          break;

        if ((lineAdj * i) == buyValue)
          return true;
      }
      return false;
    }

    #endregion

  }
}