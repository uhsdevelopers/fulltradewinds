﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using WebLibs = SIDWebLibraries.HelperClasses;
using SIDWebLibraries.HelperClasses;

namespace SIDWebV2.HelperClasses {
  public class SessionVariablesLoader : IRequiresSessionState {

    #region Private Properties

    SystemParameters Sp { get; set; }

    #endregion

    #region Constructors

    public SessionVariablesLoader() {
      Sp = new SystemParameters(SessionVariablesCore.InstanceManager.InstanceModuleInfo);
    }

    #endregion

    #region Public Methods

    public static void ResetWagerSession() {
      SessionVariablesCore.RiskAmount = 0;
      SessionVariablesCore.ToWinAmount = 0;

      if (SessionVariables.Ticket != null) {
        /*if (SessionVariablesCore.WagerType == null || SessionVariablesCore.WagerType.WebCode != WagerType.WEBPROPS)
          SessionVariablesCore.WiPostedAry = SessionVariables.Ticket.WebGameWagerItems;
        else
          SessionVariables.CwiPostedAry = SessionVariables.Ticket.WebContestantWagerItems;*/

        SessionVariables.Ticket.Reset();
      }
      else SessionVariables.Ticket = new WebTicket();
    }

    public void EstablishSessionVariables(spCstWebGetCustomerInfo_Result customerInfo) {
      SessionVariablesCore.CustomerInfo = new spCstWebGetCustomerInfo_Result {
        CustomerID = customerInfo.CustomerID.ToLower().Trim(),
        Password = customerInfo.Password == null ? "" : customerInfo.Password.Trim(),
        AgentType = customerInfo.AgentType ?? ""
      };

      ResetWagerSession();

      String clientIp = null;
      if (ServerVariables.ClientIp != null)
        clientIp = ServerVariables.ClientIp;
      String forwardedFor = null;
      if (ServerVariables.ForwardedFor != null)
        forwardedFor = ServerVariables.ForwardedFor;
      String remoteAddress = null;
      if (ServerVariables.RemoteAddress != null)
        remoteAddress = ServerVariables.RemoteAddress;

      SessionVariablesCore.ClientIpAddress = !string.IsNullOrEmpty(forwardedFor) ? forwardedFor : remoteAddress;

      if (!Utilities.IsPrivateIp(clientIp, forwardedFor, remoteAddress)) {
        LoadSessionControlSettings();
      }


      SessionVariablesCore.CustomerInfo.WebHomePage = customerInfo.WebHomePage ?? "";
      SessionVariablesCore.CustomerDefaultPageShown = false;
      SessionVariablesCore.CustomerInfo.WebLanguage = customerInfo.WebLanguage ?? "";
      SessionVariablesCore.CustomerInfo.BaseballAction = customerInfo.BaseballAction ?? "";
      SessionVariablesCore.CustomerInfo.WebRememberPassword = customerInfo.WebRememberPassword ?? false;
      SessionVariablesCore.CustomerInfo.FavoriteSport = customerInfo.FavoriteSport ?? "";
      SessionVariablesCore.CustomerInfo.FavoriteSportSubType = customerInfo.FavoriteSportSubType ?? "";
      SessionVariablesCore.CustomerInfo.FavoriteTeamID = customerInfo.FavoriteTeamID ?? "";
      SessionVariablesCore.CustomerInfo.EnforceParlayMaxBetFlag = customerInfo.EnforceParlayMaxBetFlag;
      SessionVariablesCore.CustomerInfo.EnforceTeaserMaxBetFlag = customerInfo.EnforceTeaserMaxBetFlag;

      SessionVariablesCore.LoadLanguageFile();

      SessionVariablesCore.ParlayName = customerInfo.ParlayName == null ? "" : customerInfo.ParlayName.Trim();
      var store = "";
      store += customerInfo.Store == null ? "" : customerInfo.Store.Trim();
      SessionVariablesCore.CustomerInfo.Store = store;
      SessionVariablesCore.CustomerInfo.PercentBook = customerInfo.PercentBook == null ? 0 : int.Parse(customerInfo.PercentBook.ToString());
      SessionVariablesCore.CustomerInfo.AgentID = customerInfo.AgentID == null ? "" : customerInfo.AgentID.Trim();
      SessionVariablesCore.VigDiscountPercent = customerInfo.InetVigDiscountPercent == null ? 0 : int.Parse(customerInfo.InetVigDiscountPercent.ToString());

      if (customerInfo.UsePuckLineFlag != null)
        SessionVariablesCore.UsePuckLineFlag = customerInfo.UsePuckLineFlag.Trim() == "Y";

      var currency = customerInfo.Currency == null ? "" : customerInfo.Currency.Trim();
      SessionVariablesCore.CustomerInfo.Currency = currency.Substring(0, 3);
      var effectiveDate = DateTime.Parse(SessionVariablesCore.ServerDateTime.ToShortDateString());
      double rate = 1;
      spCurGetCurrencyRateByDate_Result ratesList;

      using (var cur = new Currencies(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
        ratesList = cur.GetCurrencyRateByDate(SessionVariablesCore.CustomerInfo.Currency, effectiveDate);
      }
      if (ratesList != null) {
        rate = ratesList.Rate;
      }
      SessionVariablesCore.ExchangeRate = rate > 0 ? rate : 1;
      SessionVariablesCore.HalfPointBasketballFlag = customerInfo.HalfPointInetBasketballFlag == null ? "" : customerInfo.HalfPointInetBasketballFlag.Trim();
      SessionVariablesCore.HalfPointBasketballDow = customerInfo.HalfPointInetBasketballDow == null ? 0 : int.Parse(customerInfo.HalfPointInetBasketballDow.ToString());
      SessionVariablesCore.HalfPointFootballFlag = customerInfo.HalfPointInetFootballFlag == null ? "" : customerInfo.HalfPointInetFootballFlag.Trim();
      SessionVariablesCore.HalfPointFootballDow = customerInfo.HalfPointInetFootballDow == null ? 0 : int.Parse(customerInfo.HalfPointInetFootballDow.ToString());
      SessionVariablesCore.HalfPointMaxBet = customerInfo.HalfPointMaxBet == null ? 0 : int.Parse(customerInfo.HalfPointMaxBet.ToString());
      SessionVariablesCore.HalfPointWagerLimitFlag = customerInfo.HalfPointWagerLimitFlag ?? "N";

      if (customerInfo.EasternLineFlag != null)
        SessionVariablesCore.EasternLineFlag = customerInfo.EasternLineFlag.Trim() == "Y";

      SessionVariablesCore.CustomerInfo.TimeZone = customerInfo.TimeZone == null ? 0 : int.Parse(customerInfo.TimeZone.ToString());
      SessionVariablesCore.CurrencyCode = customerInfo.Currency;

      if (SessionVariablesCore.EasternLineFlag)
        LoadEasternLineConversionInfo();

      SessionVariablesCore.CustomerInfo.NameLast = customerInfo.NameLast == null ? "" : customerInfo.NameLast.Trim();
      SessionVariablesCore.CustomerInfo.NameFirst = customerInfo.NameFirst == null ? "" : customerInfo.NameFirst.Trim();

      SessionVariablesCore.CustomerInfo.UsePayoutPassword = customerInfo.UsePayoutPassword;

      SessionVariablesCore.CustWagerLimit = (customerInfo.WagerLimit ?? 0);
      SessionVariablesCore.CustMaxParlayBet = (customerInfo.ParlayMaxBet ?? 0);
      SessionVariablesCore.CustMaxParlayPayout = float.Parse(((customerInfo.ParlayMaxPayout ?? 0)).ToString(CultureInfo.InvariantCulture));
      SessionVariablesCore.CustMaxTeaserBet = (customerInfo.TeaserMaxBet ?? 0);
      SessionVariablesCore.CustMaxContestBet = (customerInfo.ContestMaxBet ?? 0);

      customerInfo.EnforceAccumWagerLimitsFlag = "N";
      if (customerInfo.EnforceAccumWagerLimitsFlag != null && customerInfo.EnforceAccumWagerLimitsFlag.Trim() == "Y") {
        customerInfo.EnforceAccumWagerLimitsFlag = "Y";
      }

      SessionVariablesCore.EnforceAccumWagerLimitsByLineFlag = "N";
      if (customerInfo.EnforceAccumWagerLimitsByLineFlag != null && customerInfo.EnforceAccumWagerLimitsByLineFlag.Trim() == "Y") {
        SessionVariablesCore.EnforceAccumWagerLimitsByLineFlag = "Y";
      }

      SessionVariablesCore.CustomerInfo.CreditAcctFlag = "N";
      if (customerInfo.CreditAcctFlag != null && customerInfo.CreditAcctFlag.Trim() == "Y") {
        SessionVariablesCore.CustomerInfo.CreditAcctFlag = "Y";
      }

      SessionVariablesCore.CustomerInfo.CommentsForCustomer = "";
      if (!string.IsNullOrEmpty(customerInfo.CommentsForCustomer)) {
        SessionVariablesCore.CustomerInfo.CommentsForCustomer = customerInfo.CommentsForCustomer;
      }

      SessionVariablesCore.CustomerInfo.Active = customerInfo.Active == null ? "N" : customerInfo.Active.Trim();

      SetCookieToClient("asisystem", SessionVariablesCore.CustomerInfo.CustomerID, new DateTime(2038, 01, 18));

      SetHorseRacingCustomerWagerLimits(SessionVariablesCore.CustomerInfo.CustomerID);

      LoadCustomerVigDiscountInfo(SessionVariablesCore.CustomerInfo.CustomerID);

      WebCustomerBalance.LoadCustomerAvailableBalance(customerInfo);

      int outTzValue;
      int.TryParse(SessionVariablesCore.LocalTimeZone.ToString(CultureInfo.InvariantCulture), out outTzValue);
      SessionVariablesCore.ShowRifOption = false;

      SetTeasersParlaysWageringInitialLimitations(SessionVariablesCore.CustomerInfo.CustomerID);

      if (SessionVariablesCore.CustomerInfo.AgentType != null && SessionVariablesCore.CustomerInfo.AgentType == "M") {
        SetReporterMasterAgentsList(SessionVariablesCore.CustomerInfo.CustomerID);
      }

      SessionVariablesCore.SbLimitMinutes = 0;
      SessionVariablesCore.PlLimitMinutes = 0;
      SessionVariablesCore.TrLimitMinutes = 0;
      SessionVariablesCore.IbLimitMinutes = 0;

      SessionVariablesCore.CustomerInfo.WiseActionFlag = customerInfo.WiseActionFlag == null ? "N" : customerInfo.WiseActionFlag.Trim();

      SessionVariablesCore.DecimalOddsPrecision = Utilities.GetDecimalOddsPrecision(SessionVariablesCore.InstanceManager.InstanceModuleInfo);
      SessionVariablesCore.MaxDenominator = Utilities.GetMaxDenominator(SessionVariablesCore.InstanceManager.InstanceModuleInfo);

      using (var cst = new Customers(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
        SessionVariablesCore.VigDiscountAry = cst.GetCustomerVigDiscount(SessionVariablesCore.CustomerInfo.CustomerID);
      }
    }

    public static DateTime StartWagerTimer() {
      var maxMinutes = SessionVariablesCore.InstanceManager.InstanceModuleInfo.Parameters.ConfirmationTimeLimit;
      if (maxMinutes == 0)
        maxMinutes = 30;
      var wagerLimitTime = SessionVariablesCore.ServerDateTime.AddMinutes(maxMinutes);
      return wagerLimitTime;
    }

    #endregion

    #region Private Methods

    private void CreateLoginUniqueSessionID(String customerId) {
      if (SessionVariablesCore.AllowMultipleSessions == 1) return;

      var newGuid = Utilities.CreateSessionKey(SessionVariablesCore.InstanceManager);
      var guidStr = "";

      if (newGuid != null) {
        guidStr = newGuid.ToString().Replace("{", "").Replace("}", "");
      }

      Utilities.StoreSessionKeyForCustomer(customerId, guidStr, SessionVariablesCore.InstanceManager);
    }

    private void LoadCustomerVigDiscountInfo(String customerId) {

      using (var cust = new Customers(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
        SessionVariablesCore.VigDiscountAry = cust.GetCustomerVigDiscount(customerId);
      }

    }

    private void LoadEasternLineConversionInfo() {
      using (var gL = new GamesAndLines(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
        SessionVariablesCore.EasternLineConversionAry = gL.GetEasternLineConversionInfo();
      }

    }

    private void LoadSessionControlSettings() {
      List<spPrmWebGetSessionControlSettings_Result> sessionParams;
      using (var sp = new SystemParameters(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
        sessionParams = sp.GetSessionControlParameters(SessionVariablesCore.WebTarget);
      }

      int? allowMultipleSessions = null;

      if (sessionParams != null) {
        allowMultipleSessions = (from i in sessionParams where i.ParmName == "AllowMultipleSessions" select i.ParmInteger).First() ?? 1;
      }

      SessionVariablesCore.AllowMultipleSessions = allowMultipleSessions ?? 0;
      CreateLoginUniqueSessionID(SessionVariablesCore.CustomerInfo.CustomerID);
    }

    private void SetCookieToClient(String cookieName, String cookieValue, DateTime cookieExpDate) {
      var aCookie = new HttpCookie(cookieName) { Value = cookieValue, Expires = cookieExpDate };
    }

    [Obsolete]
    private void SetHorseRacingCustomerWagerLimits(String customerId) {
      return;
    }

    private void SetReporterMasterAgentsList(String customerId) {
    }

    private void SetTeasersParlaysWageringInitialLimitations(String customerId) {
      const string accessName = "ExtraParlayTeaserValidation";
      SessionVariables.SpecialParlayTeaserAccess = false;
      SessionVariables.MaxWagerLimitTimes = 1;
      if (SessionVariablesCore.WebTarget == "PLM") {
        SessionVariables.SpecialParlayTeaserAccess = true;
        SessionVariables.MaxWagerLimitTimes = 4;
      }

      if (SessionVariablesCore.WebTarget != "TRC" && SessionVariablesCore.WebTarget != "BEW") return;
      SessionVariables.SpecialParlayTeaserAccess = true;
      SessionVariables.MaxWagerLimitTimes = 3;
    }

    #endregion

  }
}