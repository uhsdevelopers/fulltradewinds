﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.SessionState;
using SIDLibraries.Entities;
using SIDLibraries.BusinessLayer;
using SIDWebV2.Services;
using WebLibs = SIDWebLibraries.HelperClasses;

namespace SIDWebV2.HelperClasses {
  public class WebWagerItem : IRequiresSessionState {
    #region Public Methods

    private static void CheckForLooLineChanges(spGLWebGetGameOfferings_Result looAryItem, out bool lineRemoved) {
      lineRemoved = true;
      if (looAryItem == null) return;
      using (var gl = new GamesAndLines(WebLibs.SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
        var temp = gl.GetWebGameOffering(WebLibs.SessionVariablesCore.CustomerInfo.CustomerID, looAryItem.GameNum, looAryItem.SportType ?? string.Empty, looAryItem.SportSubType ?? string.Empty, WebLibs.SessionVariablesCore.WagerType.Name, looAryItem.PeriodNumber, WebLibs.SessionVariablesCore.ServerDateTime, looAryItem.Store, looAryItem.CustProfile.Trim(), 0);


        if (temp == null) {
          return;
        }

        temp.UseVigDiscount = WebLibs.SessionVariablesCore.WagerType.Name == WagerType.STRAIGHTBETNAME;
        temp.VigDiscountAry = WebLibs.SessionVariablesCore.VigDiscountAry;
        temp.VigDiscountPercent = WebLibs.SessionVariablesCore.VigDiscountPercent;

        temp.EasternLineFlag = WebLibs.SessionVariablesCore.EasternLineFlag ? "Y" : "N";

        temp.EasternLineConversionAry = WebLibs.SessionVariablesCore.EasternLineConversionAry;
        double spreadAdj, totalsAdj;
        SportsAndContestsService.SetTeaserPoints(looAryItem.SportType, looAryItem.SportSubType, out spreadAdj, out totalsAdj);
        if (WebLibs.SessionVariablesCore.ShowSpread) temp.SpreadAdj = spreadAdj;
        if (WebLibs.SessionVariablesCore.ShowTotals) temp.TotalsAdj = totalsAdj;

        temp.Initialize();

        looAryItem.SpreadChanged = false;
        looAryItem.MoneyLineChanged = false;
        looAryItem.TotalPointsChanged = false;
        looAryItem.TeamTotalChanged = false;

        if (looAryItem.FavoredTeamID != temp.FavoredTeamID || looAryItem.Spread != temp.Spread || looAryItem.SpreadAdj1 != temp.SpreadAdj1 || looAryItem.SpreadAdj2 != temp.SpreadAdj2) {
          looAryItem.BaseSpread1 = temp.BaseSpread1;
          looAryItem.BaseSpread2 = temp.BaseSpread2;
          looAryItem.OrigSpreadAdj1 = temp.OrigSpreadAdj1;
          looAryItem.OrigSpreadAdj2 = temp.OrigSpreadAdj2;

          looAryItem.Spread = temp.Spread;
          looAryItem.Spread1 = temp.Spread1;
          looAryItem.Spread2 = temp.Spread2;

          looAryItem.SpreadAdj = temp.SpreadAdj;
          looAryItem.SpreadAdj1 = temp.SpreadAdj1;
          looAryItem.SpreadAdj2 = temp.SpreadAdj2;

          looAryItem.SpreadDecimal1 = temp.SpreadDecimal1;
          looAryItem.SpreadDecimal2 = temp.SpreadDecimal2;

          looAryItem.SpreadDenominator1 = temp.SpreadDenominator1;
          looAryItem.SpreadDenominator2 = temp.SpreadDenominator2;

          looAryItem.SpreadNumerator1 = temp.SpreadNumerator1;
          looAryItem.SpreadNumerator2 = temp.SpreadNumerator2;
          looAryItem.SpreadChanged = true;
        }

        if (looAryItem.MoneyLine1 != temp.MoneyLine1 || looAryItem.MoneyLine2 != temp.MoneyLine2 || looAryItem.MoneyLineDraw != temp.MoneyLineDraw) {
          looAryItem.MoneyLine1 = temp.MoneyLine1;
          looAryItem.MoneyLine2 = temp.MoneyLine2;

          looAryItem.MoneyLineDecimal1 = temp.MoneyLineDecimal1;
          looAryItem.MoneyLineDecimal2 = temp.MoneyLineDecimal2;

          looAryItem.MoneyLineDecimalDraw = temp.MoneyLineDecimalDraw;
          looAryItem.MoneyLineDenominator1 = temp.MoneyLineDenominator1;
          looAryItem.MoneyLineDenominator2 = temp.MoneyLineDenominator2;
          looAryItem.MoneyLineDenominatorDraw = temp.MoneyLineDenominatorDraw;

          looAryItem.MoneyLineDraw = temp.MoneyLineDraw;
          looAryItem.MoneyLineNumerator1 = temp.MoneyLineNumerator1;
          looAryItem.MoneyLineNumerator2 = temp.MoneyLineNumerator2;
          looAryItem.MoneyLineNumeratorDraw = temp.MoneyLineNumeratorDraw;

          looAryItem.OrigMoneyLine1 = temp.OrigMoneyLine1;
          looAryItem.OrigMoneyLine2 = temp.OrigMoneyLine2;
          looAryItem.MoneyLineChanged = true;
        }

        if (looAryItem.TotalPoints != temp.TotalPoints || looAryItem.TtlPtsAdj1 != temp.TtlPtsAdj1 || looAryItem.TtlPtsAdj2 != temp.TtlPtsAdj2) {
          looAryItem.BaseTotalPoints1 = temp.BaseTotalPoints1;
          looAryItem.BaseTotalPoints2 = temp.BaseTotalPoints2;
          looAryItem.OrigTtlPtsAdj1 = temp.OrigTtlPtsAdj1;
          looAryItem.OrigTtlPtsAdj2 = temp.OrigTtlPtsAdj2;
          looAryItem.TotalPoints = temp.TotalPoints;
          looAryItem.TotalPoints1 = temp.TotalPoints1;
          looAryItem.TotalPoints2 = temp.TotalPoints2;
          looAryItem.TotalsAdj = temp.TotalsAdj;
          looAryItem.TtlPointsDecimal1 = looAryItem.TtlPointsDecimal1;
          looAryItem.TtlPointsDecimal2 = looAryItem.TtlPointsDecimal2;
          looAryItem.TtlPointsDenominator1 = temp.TtlPointsDenominator1;
          looAryItem.TtlPointsDenominator2 = temp.TtlPointsDenominator2;
          looAryItem.TtlPointsNumerator1 = temp.TtlPointsNumerator1;
          looAryItem.TtlPointsNumerator2 = temp.TtlPointsNumerator2;
          looAryItem.TtlPtsAdj1 = temp.TtlPtsAdj1;
          looAryItem.TtlPtsAdj2 = temp.TtlPtsAdj2;
          looAryItem.TotalPointsChanged = true;
        }

        if (looAryItem.Team1TotalPoints != temp.Team1TotalPoints || looAryItem.Team2TotalPoints != temp.Team2TotalPoints || looAryItem.Team1TtlPtsAdj1 != temp.Team1TtlPtsAdj1 || looAryItem.Team1TtlPtsAdj2 != temp.Team1TtlPtsAdj2 || looAryItem.Team2TtlPtsAdj1 != temp.Team2TtlPtsAdj1 || looAryItem.Team2TtlPtsAdj2 != temp.Team2TtlPtsAdj2) {
          looAryItem.OrigTeam1TtlPtsAdj1 = temp.OrigTeam1TtlPtsAdj1;
          looAryItem.OrigTeam1TtlPtsAdj2 = temp.OrigTeam1TtlPtsAdj2;
          looAryItem.OrigTeam2TtlPtsAdj1 = temp.OrigTeam2TtlPtsAdj1;
          looAryItem.OrigTeam2TtlPtsAdj2 = temp.OrigTeam2TtlPtsAdj2;
          looAryItem.Team1TotalPoints = temp.Team1TotalPoints;
          looAryItem.Team1TtlPtsAdj1 = temp.Team1TtlPtsAdj1;
          looAryItem.Team1TtlPtsAdj2 = temp.Team1TtlPtsAdj2;
          looAryItem.Team1TtlPtsDecimal1 = temp.Team1TtlPtsDecimal1;
          looAryItem.Team1TtlPtsDecimal2 = temp.Team1TtlPtsDecimal2;
          looAryItem.Team1TtlPtsDenominator1 = temp.Team1TtlPtsDenominator1;
          looAryItem.Team1TtlPtsDenominator2 = temp.Team1TtlPtsDenominator2;
          looAryItem.Team1TtlPtsNumerator1 = temp.Team1TtlPtsNumerator1;
          looAryItem.Team1TtlPtsNumerator2 = temp.Team1TtlPtsNumerator2;
          looAryItem.Team2TotalPoints = temp.Team2TotalPoints;
          looAryItem.Team2TtlPtsAdj1 = temp.Team2TtlPtsAdj1;
          looAryItem.Team2TtlPtsAdj2 = temp.Team2TtlPtsAdj2;
          looAryItem.Team2TtlPtsDecimal1 = temp.Team2TtlPtsDecimal1;
          looAryItem.Team2TtlPtsDecimal2 = temp.Team2TtlPtsDecimal2;
          looAryItem.Team2TtlPtsDenominator1 = temp.Team2TtlPtsDenominator1;
          looAryItem.Team2TtlPtsDenominator2 = temp.Team2TtlPtsDenominator2;
          looAryItem.Team2TtlPtsNumerator1 = temp.Team2TtlPtsNumerator1;
          looAryItem.Team2TtlPtsNumerator2 = temp.Team2TtlPtsNumerator2;
          looAryItem.TeamTotalChanged = true;
        }

        looAryItem.CircledMaxWagerSpread = temp.CircledMaxWagerSpread;
        looAryItem.CircledMaxWagerSpreadType = temp.CircledMaxWagerSpreadType;
        looAryItem.CircledMaxWagerMoneyLine = temp.CircledMaxWagerMoneyLine;
        looAryItem.CircledMaxWagerMoneyLineType = temp.CircledMaxWagerMoneyLineType;
        looAryItem.CircledMaxWagerTotal = temp.CircledMaxWagerTotal;
        looAryItem.CircledMaxWagerTotalType = temp.CircledMaxWagerTotalType;
        looAryItem.CircledMaxWagerTeamTotal = temp.CircledMaxWagerTeamTotal;
        looAryItem.CircledMaxWagerTeamTotalType = temp.CircledMaxWagerTeamTotalType;

        looAryItem.FavoredTeamID = temp.FavoredTeamID;
        looAryItem.Comments = temp.Comments;

        looAryItem.GameDateTime = temp.GameDateTime;
        looAryItem.LineSeq = temp.LineSeq;
        looAryItem.ListedPitcher1 = temp.ListedPitcher1;
        looAryItem.ListedPitcher2 = temp.ListedPitcher2;
        looAryItem.ParlayRestriction = temp.ParlayRestriction;
        looAryItem.PeriodWagerCutoff = temp.PeriodWagerCutoff;

        looAryItem.ScheduleDate = temp.ScheduleDate;
        looAryItem.ScheduleText = temp.ScheduleText;
        looAryItem.Status = temp.Status;

        //looAryItem.CorrelationID = temp.CorrelationID;
        //looAryItem.EasternLine1 = temp.EasternLine1;
        //looAryItem.EasternLine2 = temp.EasternLine2;
        //looAryItem.EasternLineFlag = temp.EasternLineFlag;
        //looAryItem.EventBetsFlag = temp.EventBetsFlag;
        //looAryItem.PreventPointBuyingFlag = temp.PreventPointBuyingFlag;
        //looAryItem.PuckLine = temp.PuckLine;
        //looAryItem.UsePuckLineFlag = temp.UsePuckLineFlag;
        //looAryItem.UseVigDiscount = temp.UseVigDiscount;
        //looAryItem.VigDiscountPercent = temp.VigDiscountPercent;

        if (!looAryItem.SpreadChanged && !looAryItem.MoneyLineChanged && !looAryItem.TotalPointsChanged && !looAryItem.TeamTotalChanged) return;
        SportsAndContestsService.SetTeaserPoints(looAryItem.SportType, looAryItem.SportSubType, out spreadAdj, out totalsAdj);
        if (WebLibs.SessionVariablesCore.ShowSpread) looAryItem.SpreadAdj = spreadAdj;
        if (WebLibs.SessionVariablesCore.ShowTotals) looAryItem.TotalsAdj = totalsAdj;
        looAryItem.Initialize();
      }
    }

    public static string CreateWagerContestItem(WebWager wager, int contestNumber, int contestantNumber, out spCnWebGetContestantOfferings_Result contestantLine) {
      contestantLine = null;
      if (SessionVariables.GamesAndContestsLines != null && contestantNumber > 0) {
        var contestItem = (from c in SessionVariables.GamesAndContestsLines
                           where c.EventOffering == "C" && c.Contest.ContestNum == contestNumber
                           select c.Contest).FirstOrDefault();

        if (contestItem == null) return "No contest Item";

        contestantLine = UpdateClooLineChanges(WebLibs.SessionVariablesCore.CustomerInfo.Store, contestNumber, contestantNumber);
        if (contestantLine == null) return "No contestant line";

        contestantLine.CategoryInfo = contestItem.CategoryInfo;

        var wi = new ContestantWagerItem(contestantLine) {
          CircledMaxWager = contestItem.CircledMaxWager,
          //ContestDateTime = contestItem.AdjContestDateTime, //pending

          ContestDesc = contestItem.ContestDesc,
          ContestType = contestItem.ContestType,
          ContestType2 = contestItem.ContestType2,

          ContestType3 = contestItem.ContestType3,
          Status = contestItem.Status,

          ThresholdType = contestItem.ThresholdType,
          ThresholdUnits = contestItem.ThresholdUnits,
          ContestNum = contestNumber,
          ContestantNum = contestantNumber,
          ContestDateTime = contestItem.ContestDateTime,
          PriceType = SIDLibraries.BusinessLayer.LineOffering.PRICE_AMERICAN,
          MaxWagerLimit = WebLibs.SessionVariablesCore.WagerLimitValidator.GetMaximumContestLimit(contestantLine.MaxWagerLimit ?? 0).Limit

        };
        // contestant validation? ------------ *

        if (wager.WebContestantWagerItems != null) {
          wi.ToWinAmt = 0;
          wi.RiskAmt = 0;
          wi.ItemNumber = wager.WebContestantWagerItems.Count() + 1;
          wager.WebContestantWagerItems.Add(wi);
        }
        else {
          wager.WebContestantWagerItems = new List<ContestantWagerItem>();
          wi.ItemNumber = wager.WebContestantWagerItems.Count() + 1;
          wager.WebContestantWagerItems.Add(wi);
        }
      }
      else
        return "Problems getting contest info";

      return "Ok";
    }

    public static string CreateWagerItem(WebWager wager, string controlCode, string wagerTypeCode, int gameNumber, int periodNumber, out spGLWebGetGameOfferings_Result looAryItem, string riskAmount = null, string toWinAmount = null, bool reloadLineOffering = false) {
      const string status = "Ok";

      if (SessionVariables.GamesAndContestsLines != null && gameNumber > 0 && periodNumber > -1) {
        looAryItem = (from g in SessionVariables.GamesAndContestsLines
                      where g.EventOffering == "G"
                                && g.GameLine.GameNum == gameNumber
                                && g.GameLine.PeriodNumber == periodNumber
                      select g.GameLine).OrderByDescending(c => c.LineSeq).FirstOrDefault();
        if (looAryItem == null) return string.Empty;
        if (reloadLineOffering) {
          bool lineRemoved;
          CheckForLooLineChanges(looAryItem, out lineRemoved);
          if (lineRemoved) return string.Empty;
        }

        var wi = new WebLibs.WagerItem(controlCode, looAryItem, wagerTypeCode, WebLibs.SessionVariablesCore.PriceType, WebLibs.SessionVariablesCore.EasternLineFlag, WebLibs.SessionVariablesCore.CustomerInfo.BaseballAction);

        //var storeMaxWager = WagerValidation.GetStoreMaxWager(wi.Loo, wi.WagerType);
        var wt = wi.WagerType;

        switch (WebLibs.SessionVariablesCore.WagerType.WebCode) {
          case WagerType.WEBPARLAY:
            wt = "P";
            break;
          case WagerType.WEBTEASER:
            wt = "T";
            break;
        }
        if (wager.HasGameWagers) {
          wi.ToWinAmt = toWinAmount != null ? double.Parse(toWinAmount) : 0;
          wi.RiskAmt = riskAmount != null ? double.Parse(riskAmount) : 0;
          var itemNumber = (SessionVariables.Ticket.OpenPlay.Any()
                ? wager.WebGameWagerItems.Count() +
                  wager.OpenPlay.Count() + 1
                : wager.WebGameWagerItems.Count() + 1);
          wi.ItemNumber = itemNumber;
          wager.WebGameWagerItems.Add(wi);

        }
        else {
          wager.WebGameWagerItems = new List<WebLibs.WagerItem>();
          wi.ItemNumber = wager.WebGameWagerItems.Count() + 1;
          wager.WebGameWagerItems.Add(wi);
        }

        WagerValidation.SetMaximumWagerLimit(wt, wi);

      }
      else
        throw new Exception("Problems getting game info");
      return status;
    }

    public static string CreateOpenWagerItem(string controlCode, string wagerTypeCode, int gameNumber, int periodNumber, out spGLWebGetGameOfferings_Result looAryItem, spTkWWebGetWagerItemsOpenPlays_Result looOpenAryItem, double? riskAmount = null, double? toWinAmount = null, bool isLineUpdate = false, int wagerPosition = -1) {
      const string status = "Ok";

      if (SessionVariables.GamesAndContestsLines != null && gameNumber > 0 && periodNumber > -1) {
        looAryItem = new spGLWebGetGameOfferings_Result();
        if (looOpenAryItem != null) {
          looAryItem.Spread = looOpenAryItem.AdjSpread;
          looAryItem.OrigSpreadAdj1 = Convert.ToInt32(looOpenAryItem.OrigSpread);
          looAryItem.CorrelationID = looOpenAryItem.CorrelationID;
          looAryItem.SportType = looOpenAryItem.SportType.Trim();
          looAryItem.SportSubType = looOpenAryItem.SportSubType.Trim();
          looAryItem.ParlayRestriction = looOpenAryItem.ParlayRestriction;
          looAryItem.GameNum = Convert.ToInt32(looOpenAryItem.GameNum);
          looAryItem.PeriodNumber = Convert.ToInt32(looOpenAryItem.PeriodNumber);
          looAryItem.Team1RotNum = looOpenAryItem.Team1RotNum;
          looAryItem.Team2RotNum = looOpenAryItem.Team2RotNum;
          looAryItem.Team1ID = looOpenAryItem.Team1ID;
          looAryItem.Team2ID = looOpenAryItem.Team2ID;
          looAryItem.PeriodNumber = Convert.ToInt32(looOpenAryItem.PeriodNumber);
        }
        var wi = new WebLibs.WagerItem {
          ControlCode = controlCode, 
          Loo = looAryItem, 
          IsExistingOpenItem = true, 
          WagerType = wagerTypeCode, 
          Line = (double) looOpenAryItem.FinalLine
        };
        wi.Price = wi.FinalPrice = (int)looOpenAryItem.FinalLine;
        wi.DecimalPrice = wi.FinalDecimal = (double)looOpenAryItem.FinalDecimal;
        wi.Numerator = wi.FinalNumerator = (int)looOpenAryItem.FinalNumerator;
        wi.Denominator = wi.FinalDenominator = (int)looOpenAryItem.FinalDenominator;
        wi.FinalLine = (double)looOpenAryItem.FinalLine;
        wi.EasternLineFlag = WebLibs.SessionVariablesCore.EasternLineFlag;
        //wi.EasternLine = (double) looOpenAryItem.EasterLine;
        wi.PriceType = looOpenAryItem.PriceType;
        wi.Pitcher1ReqFlag = WebLibs.SessionVariablesCore.CustomerInfo.BaseballAction == "Listed" || looOpenAryItem.PeriodNumber > 0;
        wi.Pitcher2ReqFlag = WebLibs.SessionVariablesCore.CustomerInfo.BaseballAction == "Listed" || looOpenAryItem.PeriodNumber > 0;
        wi.WagerCode = WebLibs.SessionVariablesCore.WagerType;

        //var storeMaxWager = WagerValidation.GetStoreMaxWager(wi.Loo, wi.WagerType);
        if (SessionVariables.Ticket.OpenPlay.Any()) {
          wi.ToWinAmt = toWinAmount ?? 0;
          wi.RiskAmt = riskAmount ?? 0;
          if (wagerPosition >= 0) {
            wi.ItemNumber = SessionVariables.Ticket.OpenPlay.Get(wagerPosition).ItemNumber;
            SessionVariables.Ticket.OpenPlay.Set(wi, wagerPosition);
          }
          else {
            wi.ItemNumber = SessionVariables.Ticket.OpenPlay.Count() + 1;
            SessionVariables.Ticket.OpenPlay.Add(wi);
          }
        }
        else {
          wi.ItemNumber = SessionVariables.Ticket.OpenPlay.Count() + 1;
          SessionVariables.Ticket.OpenPlay.Add(wi);
        }
        //WagerValidation.SetMaximumWagerLimit(wt, wi.Loo, 0);
        //wi.MaxWagerLimit = WebLibs.SessionVariablesCore.MaxWagerLimit;

      }
      else
        throw new Exception("Problems getting game info");
      return status;
    }

    public static void GetBuyInfo(string sportType, string sportSubType) {
      spBPWebGetBuyInfo_Result buyPointsInfo;

      using (var bp = new BuyPoints(WebLibs.SessionVariablesCore.InstanceManager.InstanceModuleInfo))
        buyPointsInfo = bp.GetCostToBuyPointsBySportSubTypeAndCustomer(sportType, sportSubType, WebLibs.SessionVariablesCore.CustomerInfo.CustomerID);

      WebLibs.SessionVariablesCore.SpreadBuyMax = 0;
      WebLibs.SessionVariablesCore.SpreadBuyOn3 = 0;
      WebLibs.SessionVariablesCore.SpreadBuyOff3 = 0;
      WebLibs.SessionVariablesCore.SpreadBuyOn7 = 0;
      WebLibs.SessionVariablesCore.SpreadBuyOff7 = 0;
      WebLibs.SessionVariablesCore.TotalBuy = 0;
      WebLibs.SessionVariablesCore.TotalBuyMax = 0;

      if (buyPointsInfo == null)
        return;
      WebLibs.SessionVariablesCore.SpreadBuyMax = buyPointsInfo.SpreadBuyMax ?? 0;
      WebLibs.SessionVariablesCore.SpreadBuyOn3 = buyPointsInfo.SpreadBuyOn3 ?? 0;
      WebLibs.SessionVariablesCore.SpreadBuyOff3 = buyPointsInfo.SpreadBuyOff3 ?? 0;
      WebLibs.SessionVariablesCore.SpreadBuyOn7 = buyPointsInfo.SpreadBuyOn7 ?? 0;
      WebLibs.SessionVariablesCore.SpreadBuyOff7 = buyPointsInfo.SpreadBuyOff7 ?? 0;
      WebLibs.SessionVariablesCore.TotalBuy = buyPointsInfo.TotalBuy ?? 0;
      WebLibs.SessionVariablesCore.TotalBuyMax = buyPointsInfo.TotalBuyMax ?? 0;
    }

    private static spCnWebGetContestantOfferings_Result UpdateClooLineChanges(string store, int contestNum, int contestantNum) {
      using (var c = new Contests(WebLibs.SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
        var temp = c.GetWebContestantOffering(store, WebLibs.SessionVariablesCore.CustomerInfo.CustomerID, 0, WebLibs.SessionVariablesCore.ServerDateTime.ToString("yyyy-MM-dd hh:mm:ss"), contestNum, contestantNum).FirstOrDefault();
        return temp;
      }
    }

    #endregion
  }
}