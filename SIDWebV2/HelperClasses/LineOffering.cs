﻿using System;
using SIDLibraries.BusinessLayer;
using WagerItem = SIDWebLibraries.HelperClasses.WagerItem;

namespace SIDWebV2.HelperClasses {
  public static class LineOffering {

    #region Public Methods

    public static string FormatShortDescription(ContestantWagerItem wi) {
      return wi.Loo.ContestantName + " " + wi.Loo.MoneyLine;
    }

    public static string FormatShortDescription(WagerItem wi, String sessionWagerType, bool? sessionEasternLineFlag) {
      var descStr = "";
      var pointsbought = wi.PointsBought;
      if (wi.HalfPointAdded && pointsbought > 0) pointsbought -= 0.5;
      descStr += wi.Loo.SportType.Trim() + " - ";

      switch (wi.WagerType) {
        case WagerType.SPREAD:
          switch (wi.ControlCode.Substring(1, 1)) {
            case "1":
              descStr += wi.Loo.Team1RotNum + " " + wi.Loo.Team1ID;
              break;
            case "2":
              descStr += wi.Loo.Team2RotNum + " " + wi.Loo.Team2ID;
              break;
          }
          descStr += " ";

          if (wi.Loo.PeriodDescription.Trim() == "Game" && sessionWagerType != WagerType.TEASERNAME)
            descStr += SIDLibraries.BusinessLayer.LineOffering.FormatAhSpread(wi.FinalLine, ",", true, pointsbought);
          else
            descStr += SIDLibraries.BusinessLayer.LineOffering.FormatAhSpread(wi.FinalLine, ",", true);
          break;
        case WagerType.MONEYLINE:
          switch (wi.ControlCode.Substring(1, 1)) {
            case "1":
              descStr += wi.Loo.Team1RotNum + " " + wi.Loo.Team1ID.Trim();
              break;
            case "2":
              descStr += wi.Loo.Team2RotNum + " " + wi.Loo.Team2ID.Trim();
              break;
            default:
              descStr += "Draw (" + wi.Loo.Team1ID.Trim() // "Draw (" + StringF.HandleOdbcSingleQuotes(wi.Loo.Team1ID.Trim())
                      + " vs " + wi.Loo.Team2ID.Trim() + ")"; // + " vs " + StringF.HandleOdbcSingleQuotes(wi.Loo.Team2ID.Trim()) + ")"
              break;
          }
          break;
        case WagerType.TOTALPOINTS:
          descStr += wi.Loo.Team1RotNum + " " + wi.Loo.Team1ID.Trim()
                  + "/" + wi.Loo.Team2ID.Trim() + " ";

          if (wi.ControlCode.Substring(1, 1) == "1")
            descStr += "over";
          else
            descStr += "under";
          descStr += " ";

          if (wi.Loo.PeriodDescription.Trim() == "Game" && sessionWagerType != WagerType.TEASERNAME)
            descStr += SIDLibraries.BusinessLayer.LineOffering.FormatAhTotal(wi.FinalLine, ", ", false, pointsbought);
          else
            descStr += SIDLibraries.BusinessLayer.LineOffering.FormatAhTotal(wi.FinalLine, ", ");
          break;
        case WagerType.TEAMTOTALPOINTS:
          if (wi.ControlCode.Substring(1, 1) == "1" || wi.ControlCode.Substring(1, 1) == "3")
            descStr += wi.Loo.Team1RotNum + " " + wi.Loo.Team1ID.Trim() + " ";
          else
            descStr += wi.Loo.Team2RotNum + " " + wi.Loo.Team2ID.Trim() + " ";

          if (wi.ControlCode.Substring(1, 1) == "1" || wi.ControlCode.Substring(1, 1) == "2")
            descStr += "team total over" + " " + SIDLibraries.BusinessLayer.LineOffering.FormatAhTotal(wi.FinalLine, ", ");
          else
            descStr += "team total under" + " " + SIDLibraries.BusinessLayer.LineOffering.FormatAhTotal(wi.FinalLine, ", ");
          break;
      }
      if (sessionWagerType != WagerType.TEASERNAME) {
        descStr += " ";

        if (wi.WagerType == WagerType.MONEYLINE && sessionEasternLineFlag.HasValue && sessionEasternLineFlag.Value && wi.Loo.SportType.Trim() == "Baseball" && wi.EasternLine != 0) {
          if (wi.EasternLine > 0)
            descStr += "+";
          descStr += SIDLibraries.BusinessLayer.LineOffering.ConvertToHalfSymbol(wi.EasternLine);
        }
        else {
          switch (wi.PriceType) {
            case SIDLibraries.BusinessLayer.LineOffering.PRICE_AMERICAN:
              if (wi.FinalPrice == 0)
                break;
              if (wi.FinalPrice >= 0)
                descStr += "+";

              descStr += wi.FinalPrice;
              break;
            case SIDLibraries.BusinessLayer.LineOffering.PRICE_DECIMAL:
              if (wi.FinalDecimal == 0)
                break;
              descStr += wi.FinalDecimal;

              if (wi.FinalDecimal == Math.Floor(wi.FinalDecimal))
                descStr += ".0";
              break;
            case SIDLibraries.BusinessLayer.LineOffering.PRICE_FRACTIONAL:
              if (wi.FinalNumerator == 0 || wi.FinalDenominator == 0)
                break;
              descStr += wi.FinalNumerator + "/" + wi.FinalDenominator;
              break;
          }
        }
      }

      if (wi.Loo.PeriodDescription != null) {
        if (wi.Loo.SportType != null && wi.Loo.SportSubType != null &&
            (wi.Loo.SportType.Trim() == SportTypes.BASEBALL || wi.Loo.SportType.Trim() == SportTypes.BASKETBALL) &&
            wi.Loo.SportSubType.Trim() == SportTypes.SERIES_PRICES) {
          descStr += " for Series";
        }
        else
          descStr += " for " + wi.Loo.PeriodDescription;
      }
      if (wi.HalfPointAdded) descStr += " *** Free Half Point Added ***";

      if (!GamesAndLines.SportIncludesPitchers(wi.Loo.SportType.Trim())) return descStr;
      var pitcher1Required = wi.Pitcher1ReqFlag;
      var pitcher2Required = wi.Pitcher2ReqFlag;

      if (wi.Loo.SportSubType != null && wi.Loo.SportSubType.Trim() == SportTypes.SERIES_PRICES) {
        if (wi.AdjustableOddsFlag == "N" || (String.IsNullOrEmpty(wi.Loo.ListedPitcher1) && String.IsNullOrEmpty(wi.Loo.ListedPitcher2))) {
          descStr += ". Price is Fixed.";
        }
        return descStr;
      }
      else
        if (wi.AdjustableOddsFlag == "N") descStr += " Price Is Fixed ";

      descStr += (wi.Loo.ListedPitcher1 != null ? " Pitchers: " + wi.Loo.ListedPitcher1.Trim() + " (" + (pitcher1Required ? "must start" : "action") + ")" : string.Empty);

      if (pitcher2Required)
      {
          //descStr += (wi.Loo.ListedPitcher1.Trim().Length > 0 ? ", " : " ") + wi.Loo.ListedPitcher2.Trim() + " (must start)";
          descStr += (wi.Loo.ListedPitcher1 != null ? (wi.Loo.ListedPitcher1.Trim().Length > 0 ? ", " : " ") + wi.Loo.ListedPitcher2.Trim() + " (must start)" : string.Empty);
      }

      else descStr += (wi.Loo.ListedPitcher1 != null ? (wi.Loo.ListedPitcher1.Trim().Length > 0 ? ", " : " ") + wi.Loo.ListedPitcher2.Trim() + " (action)" : string.Empty);

      return descStr;
    }

    public static string FormatShortContestDescription(ContestantWagerItem cwi) {
      var descStr = cwi.Loo.CategoryInfo.ContestType.Trim();

      if (cwi.Loo.CategoryInfo.ContestType2 != null && cwi.Loo.CategoryInfo.ContestType2 != ".") {
        descStr += " - ";
        descStr += cwi.Loo.CategoryInfo.ContestType2.Trim();
      }
      if (cwi.Loo.CategoryInfo.ContestType3 != null && cwi.Loo.CategoryInfo.ContestType3 != ".") {
        descStr += " - ";
        descStr += cwi.Loo.CategoryInfo.ContestType3.Trim();
      }
      descStr += " - ";

      if (cwi.Loo.CategoryInfo.ContestDesc != null)
        descStr += cwi.Loo.CategoryInfo.ContestDesc.Trim();

      descStr += " - ";

      if (cwi.Loo.ContestantName != null)
        descStr += cwi.Loo.ContestantName.Trim();

      descStr += " ";

      if (cwi.ThresholdType == "P") {
        descStr += SIDLibraries.BusinessLayer.LineOffering.FormatAhTotal(cwi.Loo.ThresholdLine)
                + " " + cwi.ThresholdUnits.Trim();
      }
      if (cwi.ThresholdType == "S") {
        descStr += SIDLibraries.BusinessLayer.LineOffering.FormatAhSpread(cwi.Loo.ThresholdLine)
                   + " " + cwi.ThresholdUnits.Trim();
      }
      descStr += "  ";

      switch (cwi.PriceType) {
        case SIDLibraries.BusinessLayer.LineOffering.PRICE_AMERICAN:
          if (cwi.Loo.ToBase > 0) {
            descStr += cwi.Loo.MoneyLine;
            descStr += " to ";
            descStr += cwi.Loo.ToBase;
          }
          else {
            if (cwi.Loo.MoneyLine > 0) {
              descStr += "+";
            }
            descStr += cwi.Loo.MoneyLine;
          }
          break;
        case SIDLibraries.BusinessLayer.LineOffering.PRICE_DECIMAL:
          descStr += cwi.Loo.DecimalOdds;

          if (cwi.Loo.DecimalOdds == Math.Floor(cwi.Loo.DecimalOdds ?? 0))
            descStr += ".0";
          break;
        case SIDLibraries.BusinessLayer.LineOffering.PRICE_FRACTIONAL:
          descStr += cwi.Loo.Numerator;
          descStr += "/";
          descStr += cwi.Loo.Denominator;
          break;
      }
      return descStr;
    }

    #endregion

  }
}
