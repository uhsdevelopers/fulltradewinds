﻿/*
using System;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using SIDWebLibraries.HelperClasses;
using SystemLog = SIDWebLibraries.HelperClasses.SystemLog;

namespace SIDWebV2.HelperClasses {
  public static class Customer {


    public static LoginVerification.CustomerStatus IsValid(String password, out ResultContainer validationResult, out spCstWebGetCustomerInfo_Result customerInfo) {
      var lv = new LoginVerification(SessionVariablesCore.InstanceManager.InstanceModuleInfo) {
            CustomerId = SessionVariablesCore.CustomerInfo.CustomerID,
        Password = password,
        WebTarget = SessionVariablesCore.WebTarget,
        IpAddress = SessionVariablesCore.ClientIpAddress
      };
      validationResult = null;

      var status = lv.CheckCustomerStatus(out customerInfo);

      switch (status) {
        case LoginVerification.CustomerStatus.Valid:
          //var session = new SessionVariablesLoader();
          //if (establishSession) session.EstablishSessionVariables(customerInfo);
          break;
        case LoginVerification.CustomerStatus.Failed:
          validationResult = new ResultContainer(null, ResultContainer.ResultCode.Warning, SessionVariablesCore.Translate("INVALID_PASSWORD"));
          SystemLog.WriteAccessLog("Attempting to Place Bet", validationResult.Message);
          break;
        case LoginVerification.CustomerStatus.Inactive:
          validationResult = new ResultContainer(null, ResultContainer.ResultCode.Warning, SessionVariablesCore.Translate("INACTIVE_ACCOUNT"));
          SystemLog.WriteAccessLog("Attempting to Place Bet", validationResult.Message);
          break;
        case LoginVerification.CustomerStatus.LossCapExceeded:
          validationResult = new ResultContainer(null, ResultContainer.ResultCode.Warning, SessionVariablesCore.Translate("EXCEEDED_LOSS_CAP"));
          SystemLog.WriteAccessLog("Attempting to Place Bet", validationResult.Message);
          break;
        case LoginVerification.CustomerStatus.Suspended:
          validationResult = new ResultContainer(null, ResultContainer.ResultCode.Warning, SessionVariablesCore.Translate("SUSPENDED_ACCOUNT"));
          SystemLog.WriteAccessLog("Attempting to Place Bet", validationResult.Message);
          break;
      }
      return status;
    }

  }
}
*/