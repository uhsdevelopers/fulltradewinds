﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using Newtonsoft.Json;

namespace SIDWebV2.HelperClasses {
  public static class JSONManager  {
     
      public static Dictionary<string, string> Read(String filePath)
      {
          //filePath = "Data/Lang/ENG.json";
          var jSonFile = File.ReadAllText(HttpContext.Current.Server.MapPath("~/" + filePath));
        return JsonConvert.DeserializeObject<Dictionary<string, string>>(jSonFile);
      }


  }
}