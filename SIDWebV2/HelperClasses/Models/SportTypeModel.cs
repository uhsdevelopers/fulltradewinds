﻿using System;
using System.Collections.Generic;

namespace SIDWebV2.Models {
    public class SportTypeModel {
        #region Structs

        public struct CustomerInfo
        {
            public string CustomerId { get; set; }
            public string Store { get; set; }
        }

        public struct ContestantInfo
        {
            public DateTime? AdjContestDateTime { get; set; }
            public double? CircledMaxWager { get; set; }
            public string Comments { get; set; }
            public string ContestantName { get; set; }
            public int ContestantNum { get; set; }
            public DateTime? ContestDateTime { get; set; }
            public int ContestNum { get; set; }
            public int? FirstRotNum { get; set; }
            public string MoneyLine { get; set; }
            public int? RotNum { get; set; }
            public string Status { get; set; }
            public string ThresholdType { get; set; }
            public string ThresholdUnits { get; set; }
            public string XToYLineRep { get; set; }
        }

        public struct ContestTypeInfo
        {
            // public DateTime? ContestDateTime { get; set; }
            public string ContestDesc { get; set; }
            // public int ContestTypeId { get; set; }
            public string ContestType { get; set; }
            public string ContestType2 { get; set; }
            public string ContestType3 { get; set; }
            public string EventType { get { return "C"; } }
            public bool Selected { get; set; }
            public int SequenceNumber { get; set; }
            public int SportSubTypeId { get; set; }
            public string SportType { get; set; }
            public List<ContestantInfo> Contestants { get; set; }
        }

        public class ContestSportTypeInfo
        {
            public int SportSubTypeId { get; set; }
            public string SportType { get; set; }
            public int Active { get; set; }
            public string EventType { get { return "C"; } }
            public int SequenceNumber { get; set; }
            public List<ContestTypeInfo> Sub { get; set; }
            public bool Selected { get; set; }
        }

        public class SportTypeInfo
        {
            public int SportSubTypeId { get; set; }
            public string SportType { get; set; }
            public int Active { get; set; }
            public string EventType { get { return "G"; } }
            public int SequenceNumber { get; set; }
            public List<SportTypeInfo> Sub { get; set; }
            public bool Selected { get; set; }
            public string Description { get; set; }

        }

        #endregion
    }
}