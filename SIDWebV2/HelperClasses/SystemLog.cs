﻿using System;
using SIDLibraries.Utilities;

namespace SIDWebLibraries.HelperClasses {
  public class SystemLog {

    public static void WriteToSystemLog(Exception ex) {
      using (var lg = new LogWriter(SessionVariablesCore.InstanceManager.InstanceModuleInfo, SessionVariablesCore.SiteCode, SessionVariablesCore.UniqueKey) {
        ClientCode = SessionVariablesCore.WebTarget,
        LoginId = SessionVariablesCore.CustomerInfo != null ? SessionVariablesCore.CustomerInfo.CustomerID : SessionVariablesCore.AgentInfo != null ? SessionVariablesCore.AgentInfo.AgentID : null,
        ClientIpAddress = ServerVariables.ClientIp
      }) {
        lg.WriteToSystemLog(ex);
      }
    }


    #region Inet Log Methods

    public static void WriteAccessLog(string operation, string data, string loginId = null, int? notificationId = null) {
      using (var lg = new LogWriter(SessionVariablesCore.InstanceManager.InstanceModuleInfo, SessionVariablesCore.SiteCode, SessionVariablesCore.UniqueKey) {
        ClientCode = SessionVariablesCore.WebTarget,
        LoginId = loginId ?? (SessionVariablesCore.CustomerInfo != null ? SessionVariablesCore.CustomerInfo.CustomerID : SessionVariablesCore.AgentInfo != null ? SessionVariablesCore.AgentInfo.AgentID : null),
        ClientIpAddress = ServerVariables.ClientIp
      }) {
        if (notificationId != null)
          lg.WriteAccessLog(operation, data, notificationId);
        else
          lg.WriteAccessLog(operation, data);
      }
    }

    public static int? WriteToWebNotificationLog(string customerId, string subject, string body) {
      using (var lg = new LogWriter(SessionVariablesCore.InstanceManager.InstanceModuleInfo, SessionVariablesCore.SiteCode, SessionVariablesCore.UniqueKey)) {
        return lg.WriteToWebNotificationLog(customerId, subject, body);
      }
    }

    #endregion

  }
}
