﻿using System;
using SIDLibraries.BusinessLayer;
using System.Collections.Generic;
using System.Linq;
using WebLibs = SIDWebLibraries.HelperClasses;

namespace SIDWebV2.HelperClasses {
  [Serializable()]
  public class WebTicket : Ticket {

    #region Public properties

    public List<WebWager> Wagers { get; set; }

    /*public List<ContestantWagerItem> WebContestantWagerItems { get; set; }

    public List<WebLibs.WagerItem> WebGameWagerItems { get; set; }
     * */

    public OpenPlay OpenPlay { get; set; }

    public WebTicket() {
      Wagers = new List<WebWager>();
      OpenPlay = new OpenPlay();
      Reset();
    }

    public WebWager GetWager(int wagerNumber) {
      if (!Wagers.Any() || Wagers.Count() < wagerNumber) return null;
      return Wagers[wagerNumber];
    }

    public void Reset() {
      Wagers = new List<WebWager> {
        new WebWager()
      };

      OpenPlay.Reset();
      WagerLimitTime = SessionVariablesLoader.StartWagerTimer();
      TicketNumber = 0;
    }

    public double InetWagerNumber {
      get {
        if (_inetWagerNumber.HasValue)
          return _inetWagerNumber.Value;
        if (Wagers == null || Wagers.Count == 0)
          return 0;
        var rnd = new Random();
        _inetWagerNumber = rnd.NextDouble();

        return _inetWagerNumber.Value;
      }
    }

    public DateTime WagerLimitTime { get; private set; }

    #endregion

    #region Public methods

    public static bool IsInetWagerNumberOk(double fromForm) // , double fromSession)
    {
      return fromForm == WebLibs.SessionVariablesCore.InetWagerNumber && WebLibs.SessionVariablesCore.InetWagerNumber > 0;
    }

    public bool WagerTimerExpired(DateTime limit) {
      return false;
      // return WagerLimitTime < limit;
    }

    #endregion

    #region Private properties

    private double? _inetWagerNumber;

    #endregion

  }
}