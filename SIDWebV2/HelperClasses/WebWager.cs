﻿using System;
using System.Collections.Generic;
using WebLibs = SIDWebLibraries.HelperClasses;

namespace SIDWebV2.HelperClasses {
  [Serializable()]
  public class WebWager {

    #region Public properties

    public List<ContestantWagerItem> WebContestantWagerItems { get; set; }

    public List<WebLibs.WagerItem> WebGameWagerItems { get; set; }

    public OpenPlay OpenPlay { get; set; }

    public WebWager() {
      OpenPlay = new OpenPlay();
      Reset();
    }

    public void Reset() {
      OpenPlay.Reset();
      WebContestantWagerItems = new List<ContestantWagerItem>();
      WebGameWagerItems = new List<WebLibs.WagerItem>();
    }

    public int TotalWagerItems {
      get {
        return (HasContestantWagers ? WebContestantWagerItems.Count : 0) + (HasGameWagers ? WebGameWagerItems.Count : 0);
      }
    }

    public bool HasGameWagers {
      get {
        return WebGameWagerItems != null && WebGameWagerItems.Count > 0;
      }
    }

    public bool HasContestantWagers {
      get {
        return WebContestantWagerItems != null && WebContestantWagerItems.Count > 0;
      }
    }

    #endregion

    #region Public methods


    #endregion

    #region Private properties

    #endregion

  }
}