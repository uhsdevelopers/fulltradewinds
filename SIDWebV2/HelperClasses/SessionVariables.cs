﻿using System.Collections.Generic;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using SIDWebLibraries.HelperClasses;
using UniqueSessionValidator = SIDWebV2.Extensions.UniqueSessionValidator.UniqueSessionValidator;

namespace SIDWebV2.HelperClasses {
  public class SessionVariables : SessionVariablesCore {

    #region not categorized

    //private const string SiteCode = "Main";

    public static UniqueSessionValidator UniqueSessionValidator {
      get { return (UniqueSessionValidator)GetSessionValue("UniqueSessionValidator", new UniqueSessionValidator(InstanceManager.InstanceModuleInfo)); }
      set { SetSessionValue("UniqueSessionValidator", value); }
    }

    public static List<spSptWebGetSportsAndContests_Result> AvailableSportsAndContests {
      get { return (List<spSptWebGetSportsAndContests_Result>)GetSessionValue("availableSportsAndContests"); }
      set { SetSessionValue("availableSportsAndContests", value); }
    }

    public static List<LineOfferingContainer> GamesAndContestsLines {
      get { return (List<LineOfferingContainer>)GetSessionValue("gamesAndContestsLines", new List<LineOfferingContainer>()); }
      set { SetSessionValue("gamesAndContestsLines", value); }
    }

    public static WebTicket Ticket {
      get { return (WebTicket)GetSessionValue("webTicket"); }
      set { SetSessionValue("webTicket", value); }
    }

    public static bool SpecialParlayTeaserAccess {
      get { return (bool)GetSessionValue("SpecialParlayTeaserAccess", true); }
      set { SetSessionValue("SpecialParlayTeaserAccess", value); }
    }

    public static double MaxWagerLimitTimes {
      get { return (double)GetSessionValue("maxWagerLimitTimes", 9999); }
      set { SetSessionValue("maxWagerLimitTimes", value); }
    }

    #endregion

    #region Contest Variables

    /*
    public static List<ContestantWagerItem> CwiPostedAry {
      get { return (List<ContestantWagerItem>)GetSessionValue("cwiPostedAry"); }
      set { SetSessionValue("cwiPostedAry", value); }
    }*/

    #endregion

    /*
     * public static bool IsMobile { get; set; }
    */
  }
}