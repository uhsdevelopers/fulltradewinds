﻿using System.Web.Mvc;
using SIDWebLibraries.HelperClasses;
using SIDWebV2.HelperClasses;
using SIDWebV2.LiveDealerServiceRef;

namespace SIDWebV2.Controllers {
  public class LiveDealerController : Controller {
    //
    // GET: /LiveDealer/

    public ActionResult Index() {
      var lbClient = new MasterClientWebServiceSoapClient();
      ViewData["data"] = lbClient.getEncryptedToken("TRADEWINDS", "77eb68b88bd42ab5d8fd7fcdc7e4dc1b",
          SessionVariablesCore.CustomerInfo.CustomerID.Trim(), SessionVariablesCore.CustomerInfo.AgentID, "");
      return View();
    }

  }
}
