﻿using System.Web.Mvc;
using SIDWebV2.HelperClasses;
using SIDWebLibraries.HelperClasses;

namespace SIDWebV2.Controllers {
  public class BettingAppController : Controller {
    //
    // GET: /BettingApp/

    public ActionResult Index() {

      if (SessionVariablesCore.WebRobotDetector == null) return SessionVariablesCore.CustomerInfo != null ? View() : View("~/Views/Login/Index.aspx");
      if (SessionVariablesCore.WebRobotDetector.RobotDetected(SessionVariablesCore.SiteCode)) return View("~/Views/Login/Index.aspx");

      return SessionVariablesCore.CustomerInfo != null ? View() : View("~/Views/Login/Index.aspx");
    }
  }
}