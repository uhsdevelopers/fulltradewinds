﻿using System.Web.Mvc;
using SIDWebLibraries.HelperClasses;

namespace SIDWebV2.Controllers
{
    public class HorsesController : Controller
    {
        //
        // GET: /Horses/

        public ActionResult Index() {
          if (!SessionVariablesCore.IsSessionValid()) return View();
          ViewData["SBUserName"] = SessionVariablesCore.CustomerInfo.CustomerID.Trim();
          ViewData["SBPassword"] = SessionVariablesCore.CustomerInfo.Password.Trim();
          ViewData["SiteId"] = "trdwd";
          return View();
        }

    }
}
