﻿using System.Web.Mvc;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using SIDWebLibraries.HelperClasses;
using SIDWebV2.HelperClasses;
using SIDWebV2.Services;

namespace SIDWebV2.Controllers {
  public class DebugController : Controller {

    public ActionResult Index() {

      using (var ls = new LoginService()) {
        spCstWebGetCustomerInfo_Result customerInfo;
        spAgWebGetAgent_Result agentInfo;
        var status = ls.ValidateCustomer("66", "66", out customerInfo, out agentInfo, "ytest");

        var session = new SessionVariablesLoader();
        session.EstablishSessionVariables(customerInfo);
        SessionVariablesCore.WagerType = WagerType.GetFromWebCode(WagerType.WEBSTRAIGHT);

        var sessionValidator = new UniqueSessionValidator(SessionVariablesCore.InstanceManager.InstanceModuleInfo);
        SessionVariablesCore.UniqueKey = sessionValidator.CreateLoginUniqueSessionId(new UniqueSessionValidator.SessionData(SessionVariablesCore.UniqueKey, SessionVariablesCore.CustomerInfo.CustomerID));
      }

      if (SessionVariablesCore.WebRobotDetector == null) return SessionVariablesCore.CustomerInfo != null ? View() : View("~/Views/Login/Index.aspx");
      if (SessionVariablesCore.WebRobotDetector.RobotDetected(SessionVariablesCore.SiteCode)) return View("~/Views/Login/Index.aspx");

      return SessionVariablesCore.CustomerInfo != null ? View() : View("~/Views/Login/Index.aspx");
    }
  }
}