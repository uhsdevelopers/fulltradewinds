﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;
using SIDWebV2.Services;

namespace SIDWebV2.Controllers {
  public class AutoLoginController : Controller {
    //
    // GET: /AutoLogin/

    public ActionResult Index() {
      ViewResult view;
      var customerId = HttpContext.Request["customerID"];
      var password = HttpContext.Request["password"];
      var agentId = HttpContext.Request["agentID"];
      if (customerId == "DEMO") {
        password = "d3m000";
        view = View("~/Views/DemoApp/Index.aspx");
      }
      else {
        view = View("~/Views/BettingApp/Index.aspx");
      }
      var ls = new LoginService();
      var key = ls.TemporalToken().Message;
      // Encrypt the string to an array of bytes. 
      byte[] encryptedPassword = EncryptStringToBytes(password, Encoding.UTF8.GetBytes(key), Encoding.UTF8.GetBytes(key));
      ls.CustomerSignIn(customerId, Convert.ToBase64String(encryptedPassword), key, agentId);
      return view;
    }

    private static byte[] EncryptStringToBytes(string plainText, byte[] Key, byte[] IV) {
      // Check arguments. 
      if (plainText == null || plainText.Length <= 0)
        throw new ArgumentNullException("plainText");
      if (Key == null || Key.Length <= 0)
        throw new ArgumentNullException("Key");
      if (IV == null || IV.Length <= 0)
        throw new ArgumentNullException("IV");
      byte[] encrypted;
      // Create an RijndaelManaged object 
      // with the specified key and IV. 
      using (RijndaelManaged rijAlg = new RijndaelManaged()) {
        rijAlg.Key = Key;
        rijAlg.IV = IV;

        // Create a decryptor to perform the stream transform.
        ICryptoTransform encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

        // Create the streams used for encryption. 
        using (MemoryStream msEncrypt = new MemoryStream()) {
          using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write)) {
            using (StreamWriter swEncrypt = new StreamWriter(csEncrypt)) {

              //Write all data to the stream.
              swEncrypt.Write(plainText);
            }
            encrypted = msEncrypt.ToArray();
          }
        }
      }


      // Return the encrypted bytes from the memory stream. 
      return encrypted;

    }

  }
}
