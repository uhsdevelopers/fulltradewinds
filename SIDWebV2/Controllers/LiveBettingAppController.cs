﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using SIDWebV2.LiveBettingServiceRef;
using SIDWebLibraries.HelperClasses;

namespace SIDWebV2.Controllers {
  public class LiveBettingAppController : Controller {
    //
    // GET: /LiveBetting/

    public ActionResult Index() {

      using (var client = new ClientAPIClient())
      {
          var customerId = SessionVariablesCore.CustomerInfo.CustomerID.Trim();
          var timestamp = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
          string timestampFormat ="";

          timestampFormat = DateTime.Now.ToString("yyyy’-‘MM’-‘dd’T’HHns");
          timestampFormat = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ssZ");
          //timestampFormat = "2019-08-01T19:56:15Z";


         

          var hash = GetHashSha256(customerId + "2019-08-01T19:56:15Z" + "acB+r=x!sV-x%8{QpP");
          var hash2 = sha256(customerId + timestampFormat + "acB+r=x!sV-x%8{QpP");
        //var token = client.generateToken(SessionVariablesCore.CustomerInfo.CustomerID.Trim()).tokenId;
          Redirect("https://plive.asi.phnserv.eu/betLobbyV2/?customerId=" + customerId + "&tstamp=" + timestampFormat + "&hash=" + hash2 + "&platform=asi");
      }

        // original code
      //using (var client = new ClientAPIClient())
      //{
      //    var token = client.generateToken(SessionVariablesCore.CustomerInfo.CustomerID.Trim()).tokenId;
      //    Redirect("http://live.trdwd.ec/live/login/token/" + token);
      //}


      return View();
    }

    static string sha256(string randomString)
    {
        var crypt = new System.Security.Cryptography.SHA256Managed();
        var hash = new System.Text.StringBuilder();
        byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(randomString));
        foreach (byte theByte in crypto)
        {
            hash.Append(theByte.ToString("x2"));
        }
        return hash.ToString();
    }

    static string GetHashSha256(string rawData)
    {
        // Create a SHA256   
        using (SHA256 sha256Hash = SHA256.Create())
        {
            // ComputeHash - returns byte array  
            byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

            // Convert byte array to a string   
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < bytes.Length; i++)
            {
                builder.Append(bytes[i].ToString("x2"));
            }
            return builder.ToString();
        }
    }
  }
}
