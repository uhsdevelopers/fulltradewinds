﻿var Utilities = Utilities || {};

Utilities.Redirect = function (path) {
    window.location.href = path;
};