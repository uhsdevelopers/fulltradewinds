﻿var ErrorHandler = ErrorHandler || {};

ErrorHandler.stackLinesToReport = 4;
ErrorHandler.output = "service"; // console, service or screen
ErrorHandler.active = true;
ErrorHandler.appName = "SIDWeb";

ErrorHandler.GetStackLines = function(stack) {
    var strRet = "";
    var stackArray = stack.split("\n");
    this.stackLinesToReport = 4;
    for (var i = 0; i < this.stackLinesToReport; i++) {
        strRet += stackArray[i];
        if (i < this.stackLinesToReport - 1) strRet += "\n";
    }
    return strRet;
};

ErrorHandler.SubmitError = function(data, refresh) {
    if (ErrorHandler.active) {
        if (typeof jQuery == 'function') {
            var stackTrace = data.Message + "\n" + data.StackLines;
            $.ajax({
                type: 'POST',
                url: '/Handlers/ErrorHandlerManager.asmx/Error',
                data: 'appName=' + ErrorHandler.appName + '&stackTrace=' + stackTrace,
                dataType: 'text',
                success: function() {
                    if (refresh) document.location.reload();
                }
            });
        } else log('JQ Not Found');
    }
};

ErrorHandler.Error = function (data) {
    if (ErrorHandler.active) {
        var stackLines = "";
        var msg = "";
        var refresh = false;
        if (typeof data == 'string') {
            msg = "str: " + data;
            if (data.indexOf("modulerr")) refresh = true;
        }
        else if (data.type != null && data.target != null && data.type == "error") msg = "Socket Error: " + data.target.url;
        else if (typeof data == 'object') {
            if (data.stack) stackLines = ErrorHandler.GetStackLines(data.stack);
            try { msg = data.toString(); }
            catch (ex) { }
        }
        var obj = { StackLines: stackLines, Message: msg };
        if (ErrorHandler.output == "console") log(data);
        else if (ErrorHandler.output == "screen") alert(data);
        else ErrorHandler.SubmitError(obj, refresh);
    }
}