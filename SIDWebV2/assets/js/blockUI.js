﻿(function () {
    var BlockUI = BlockUI || {}
    BlockUI._loadingDisplayed = false;
    BlockUI._defaultDelay = 100;
    BlockUI._message = 'Please Wait ...';

    BlockUI.ShowLoading = function() {
        if (!_loadingDisplayed) {
            $.blockUI({
                message: _message,
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }
            });
            BlockUI._loadingDisplayed = true;
        }
    };

    BlockUI.HideLoading = function(delay) {
        if (_loadingDisplayed) {
            if (typeof delay == "undefined") delay = _defaultDelay;
            setTimeout(function() {
                $.unblockUI();
                BlockUI._loadingDisplayed = false;
            }, delay);
        }
    };
});