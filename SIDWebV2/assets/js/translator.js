﻿var Translator = function (language, translations) {
    this.translations = translations;
    this.learningMode = false;

    this.Translate = function (text) {

        if (text && Translator.translations) {
            var safeText = this.MakeTextSafe(text);
            var txt = this.translations[safeText];
            if (txt && txt != "[NT]") return txt;
            else {
                if (this.langLearningMode) {
                    this.translations[safeText] = "[NT]";
                    text = "[" + text + "]";
                }
                return text.replace('_', ' ');
            }
        }
        else return text;
    };

    this.MakeTextSafe = function (text) {
        var translatedName;
        var firstChar;
        //var varName = text.replace(/ /g, "_").replace(".5", "H").replace(/½/g, "H").replace(/-/g, "_").toUpperCase();
        firstChar = text.charAt(0);
        if (/^([0-9])$/.test(firstChar)) {
            translatedName = text.replace(firstChar, CommonFunctions.SubstituteDigitByLetter(firstChar));
        } else {
            translatedName = text;
        }
        return translatedName;
    };

    this.SaveTranslations = function () {
        var TranslationsToSave = "{";
        angular.forEach(CommonFunctions.Translations, function (value, key) {
            if (TranslationsToSave.length > 1) TranslationsToSave += ","
            TranslationsToSave += '"' + key + '": "' + value + '"';
        });
        TranslationsToSave += "}";
        $SessionManagerService.SaveTranslations($rootScope.CustomerSettings.WebLanguage, TranslationsToSave).then();
    }

};