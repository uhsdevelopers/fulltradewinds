﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <%
//Just for Testing
//Session("customerID")="LiveD";
//Session("agentID")="YTEST";
    %>
    <title>LiveDealer</title>
    <script language="JavaScript" type="text/JavaScript">
        function loadLDC(data) {
            window.open("https://www.golivedealer.net/LiveDealer/LiveDealerLobby.aspx?data=" + data, "LiveDealer", "toolbar=no,location=no,directories=no,status=no, menubar=no,scrollbars=no,resizable=no,copyhistory=no,width=1024,height=720")
        }

        function MM_openBrWindow(theURL, winName, features) { //v2.0
            window.open(theURL, winName, features);
        }

        function openMD() {
            window.open('https://www.deal4realcasino.com/sitioEvodp/dealers/Kimberly.html', "newWin", "toolbar=no,location=no,directories=no,status=no, menubar=no,scrollbars=no,resizable=no,copyhistory=no,width=900,height=680");
        }
    </script>
    <style>
        <!--
        * {
            margin: 0;
            padding: 0;
        }

        body {
            background: #476EA9 url(/assets/img/bg_livedealer.jpg) top left repeat-x;
            font-family: Arial, Helvetica, sans-serif;
            margin: 0 auto;
            padding: 0;
            text-align: center;
            color: #FFF;
            font-family: Geneva, Arial, Helvetica, sans-serif;
            font-size: 14px;
        }

        .annoucement {
            width: 630px;
            margin: 15px auto 0 auto;
            padding: 0 30px 0 0;
            text-align: center;
        }

            .annoucement strong {
                color: #FC0;
            }

        .livedealer {
            width: 690px;
            height: 502px;
            background: transparent url(/assets/img/bg_livedealer_trw.png) top left repeat-x;
            margin: 15px auto 0 auto;
            padding: 0;
            text-align: left;
            border-top: 1px solid #000;
        }

        .playnow {
            margin-top: 120px;
            padding: 0;
        }

        .games {
            margin-top: 3px;
            text-align: center;
        }

            .games img {
                margin-right: 10px;
            }

        .req {
            margin-top: 125px;
            text-align: center;
        }

        img {
            border: none;
        }

        .clear {
            clear: both;
        }
        -->
    </style>
</head>

<body>

    <div class="annoucement">
        <strong>- Effective Monday August 16th -<br />
            The Live Dealer Casino will be open 24 hours a day , 7 days a week.</strong><br />
        <!--Live Dealer will be doing Maintenance:<br>
August 20th,24th and 25th from 11:00 pm (PST) to
5:00 am (PST). During this time, there may be brief periods of service
interruption. We apologize for any inconvenience this may cause.-->
    </div>
    <div class="livedealer">
        <div class="playnow"><a href="#" onclick="loadLDC('<%= ViewData["data"] %>');window.close();">
            <img src="/assets/img/button_playnow.jpg" /></a></div>

        <div class="games">
            <a href="#" onclick="loadLDC('<%= ViewData["data"] %>');window.close();">
                <img src="/assets/img/button_blackjack.png" border="0" /></a>
            <a href="#" onclick="loadLDC('<%= ViewData["data"] %>');window.close();">
                <img src="/assets/img/button_baccarat.png" border="0" /></a>
            <a href="#" onclick="loadLDC('<%= ViewData["data"] %>');window.close();">
                <img src="/assets/img/button_roulette.png" border="0" /></a>
        </div>

        <div class="req">
            <a href="#" onclick="MM_openBrWindow('PopupBlockerHelp.html','','scrollbars=yes,width=900,height=600')">
                <img src="/assets/img/button_popup.png" border="0" /></a>
            <a href="https://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash&amp;promoid=BIOW" alt="Get Flash!" target="_blank">
                <img src="/assets/img/button_getflash.png" border="0" /></a>
            <a href="liveDealer_systemr.html" target="_blank">
                <img src="/assets/img/button_system.png" border="0" /></a>
        </div>


    </div>
</body>
</html>

