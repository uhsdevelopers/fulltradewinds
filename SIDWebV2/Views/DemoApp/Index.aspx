﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<!DOCTYPE html>
<html>
<head id="Head1" runat="server">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>Trdwd.ec</title>
    <link rel="icon" type="image/x-icon" href="/favicon.ico" />
    <%: Styles.Render("~/Content/style") %>
    <%: Scripts.Render("~/Content/angular") %>
    <%: Scripts.Render("~/Content/appLibs") %>
    <%: Scripts.Render("~/Content/bootstrap") %>
    <%: Scripts.Render("~/Content/appControllers") %>
    <%: Scripts.Render("~/Content/appCore") %>
    <%: Scripts.Render("~/Content/appServices") %>
    <script>
      var sip = "<% Response.Write(Request.ServerVariables["LOCAL_ADDR"]); %>";
    </script>

</head>
<body ng-app="mainModule" id="body_bg" class="agent-view-lines">
    <div ng-controller="appController" id="app"  dw-loading="Rep" class="<%= SIDWebLibraries.HelperClasses.SessionVariablesCore.InstanceManager.InstanceModuleInfo.InstanceInfo.IsProduction ? "" : "test_warning"%>">

        <div id="wrapper" class="{{PageWrapperClass}}" ng-class="{active: !IsMobileDevice}">

			<div class="sidebar-wrapper hidden-print" id="sidebar-wrapper" ng-include="Templates.leftSidebar.main"></div>

            <div class="page-content-wrapper" id="lineDiv" ng-class="{dashboard: Selections.length == 0 || FirstLoad, 'hidden-print': DialogTemplate != ''}">

                <div class="page-content-wrapper-inner">
                
                    <div class="content-header navbar-fixed-top sb-expanded" id="topHeader" ng-include="Templates.mobileHeader"></div>
                <!-- Mobile Header-->

                <div ng-if="!IsMobileDevice" ng-include="BetOfferingTemplate"></div>
                    <div ng-if="IsMobileDevice" ng-show="TicketServiceView.TotalWagers() > 0 && menuTab == 1 && !TicketServiceView.Ticket.Posted()" class="foo_continue" id="Div1">
                        <div class="continue">
                            <button type="button" class="btn btn-continue" ng-click="MobileContinueOnClick()" ng-bind="Translate('CONTINUE')"></button>
                        </div>
                    </div>
                <ng-view ng-if="IsMobileDevice"></ng-view>
                </div>

            </div>

            <div class="foo_options" id="page_footer"></div>
            <!-- footer -->
        </div>

        <!-- Modal -->
        <div class="modal fade modal-wide" ng-class="ModalClass" id="modalDialog" role="dialog"  data-keyboard="false" data-backdrop="static">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div ng-include="DialogTemplate"></div>
                </div>
            </div>
        </div>
    </div>
    <script src="/assets/js/googleAnalytics.js"></script>

    <form name="LoginForm" method="post" action="https://classic.trdwd.ec/asi/LoginVerify.asp" id="loginform" style="display: none">
      <input type="text" name="customerID1" id="customerID1"/>
		  <input type="password" name="password1" id="password1"/>
    </form>


</body>

</html>