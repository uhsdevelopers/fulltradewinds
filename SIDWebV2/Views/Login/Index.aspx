﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Login.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div id="fullscreen_bg" class="fullscreen_bg"/>

    <div class="container" ng-controller="loginController">
        <h1 class="form-signin-heading text-muted">Welcome</h1>
        <noscript>
            <meta http-equiv="refresh" content="0;url=https://classic.trdwd.ec?nm=1&njs=1">
        </noscript>
        <div class="login-form" ng-include="loginView"></div>
    </div>

</asp:Content>