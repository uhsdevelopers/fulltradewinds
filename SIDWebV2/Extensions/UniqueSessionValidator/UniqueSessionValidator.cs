﻿using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;
using System;
using SIDLibraries.UI;
using SIDWebLibraries.HelperClasses;
using SystemLog = SIDWebLibraries.HelperClasses.SystemLog;


namespace SIDWebV2.Extensions.UniqueSessionValidator {
  public class UniqueSessionValidator : ExtensionCore {
    private readonly ModuleInfo _moduleInfo;

    public UniqueSessionValidator(ModuleInfo moduleInfo)
      : base(moduleInfo, "UniqueSessionValidator") {
      _moduleInfo = moduleInfo;
    }

    public struct SessionData {
      public readonly string CustomerSessionKey;
      public readonly string CustomerId;

      public SessionData(string customerSessionKey, string customerId) {
        CustomerSessionKey = customerSessionKey;
        CustomerId = customerId;
      }

    }

    public int? CheckUniqueSessionId(SessionData sessionData) {
      try {
        var settings = SessionVariablesCore.CustomerSessionSettings;
        int? action = null;
        if (IsPrivateIp() || settings[0] != 0) return null;
        // ignore the multiple session settings
        if (!IsSessionKeyValid(sessionData)) {
          //result.Add("Another user has taken over your session.");
          action = (settings[1] == 1 ? settings[1] : 0);
        }
        return action;
      }
      catch (Exception ex) {
        SystemLog.WriteToSystemLog(ex);
        return null;
      }
    }

    public bool ValidateUniqueSession() {
      return IsActive();
    }

    private static bool IsPrivateIp() {
      String httpClientIp = null;
      if (ServerVariables.ClientIp != null)
        httpClientIp = ServerVariables.ClientIp;
      String forwardedFor = null;
      if (ServerVariables.ForwardedFor != null)
        forwardedFor = ServerVariables.ForwardedFor;
      String remoteAddress = null;
      if (ServerVariables.RemoteAddress != null)
        remoteAddress = ServerVariables.RemoteAddress;

      string ip;

      if (httpClientIp != null)
        ip = httpClientIp;
      else if (forwardedFor != null)
        ip = forwardedFor;
      else
        ip = remoteAddress;

      return ip != null && ((ip.Contains("192.168.")) || (ip.Contains("10.0.0.")));
    }

    private bool IsSessionKeyValid(SessionData sessionData) {
      if (IsPrivateIp()) return true;
      try {
        using (var customers = new Customers(_moduleInfo)) {
          var sessionObj = customers.GetSessionLastActivity(sessionData.CustomerId);//GetSessionKeyForCustomer(Session("CustomerID"));

          var isValid = true;
          if (sessionObj == null) return true;
          if (Convert.ToString(sessionData.CustomerSessionKey) == Convert.ToString(sessionObj.SessionID)) {
            customers.UpdateSessionTimeStamp(sessionData.CustomerId);
          }
          else {
            if (sessionObj.SessionLastActivity == null || GetTheLastActivityInMinutes((DateTime)sessionObj.SessionLastActivity) > 20) {
              StoreSessionKeyForCustomer(sessionData.CustomerId);
            }
            else {
              isValid = false;
            }
          }

          return isValid;
        }
      }
      catch (Exception ex) {
        SIDLibraries.BusinessLayer.SystemLog.Log(ex, _moduleInfo);
        return true;
      }
    }

    private DateTime GetServerDate() {
      return SessionVariablesCore.InstanceManager.InstanceModuleInfo.ServerDateTime;
    }

    private int GetTheLastActivityInMinutes(DateTime lastActivityTimeStamp) {

      var currentDate = GetServerDate();
      var span = currentDate.Subtract(lastActivityTimeStamp);
      return span.Minutes;

    }

    public string CreateLoginUniqueSessionId(SessionData sessionData) {
      if (IsPrivateIp()) return null;
      var settings = SessionVariablesCore.CustomerSessionSettings;
      // ignore the multiple session settings
      if (settings[0] == 1) return null;

      return StoreSessionKeyForCustomer(sessionData.CustomerId);
    }

    private string StoreSessionKeyForCustomer(string customerId) {
      using (var customers = new Customers(_moduleInfo)) {
        return customers.InsertSessionKeyForCustomer(customerId).Replace("{", "").Replace("}", "");
      }

    }



  }
}