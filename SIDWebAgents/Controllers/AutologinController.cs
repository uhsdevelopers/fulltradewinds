﻿using System;
using System.Linq;
using System.Web.Mvc;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Utilities;
using SIDWebAgents.HelperClasses;
using SIDWebLibraries.HelperClasses;
using SystemLog = SIDWebLibraries.HelperClasses.SystemLog;

namespace SIDWebAgents.Controllers {
  public class AutologinController : Controller {
    //
    // GET: /Autologin/
    public ActionResult Index() {
        var customerId = Request["customerID1"];
        var password = Request["password1"];
        customerId = "TW" + customerId;
        SessionVariablesCore.LastSessionAction = DateTime.Now;
      return Handler(customerId, password);
    }

    [HttpPost]
    public ActionResult Index(string customerID1, string password1) {
        customerID1 = "TW" + customerID1;
      return Handler(customerID1, password1);
    }

    private ActionResult Handler(string customerId, string password) {

     if (!new ResultContainer().SessionExpired) return Redirect("~/");

        SessionVariables.AgentId = customerId;
        SessionVariables.Password = password;
        var im = new InstanceManager.BusinessLayer.InstanceManager(WebConfigManager.GetApplicationId(), WebConfigManager.GetDefaultInstance());
        SystemParameters.LoadModuleParameters(im.InstanceModuleInfo);
        SessionVariablesCore.InstanceManager = im;

      using (var cst = new Customers(im.InstanceModuleInfo)) {
            SessionVariables.AgentAsCustomerInfo = cst.GetCustomerInfo(SessionVariables.AgentId).FirstOrDefault();
        }
      using (var ag = new Agents(im.InstanceModuleInfo)) {
            SessionVariablesCore.AgentInfo = ag.GetAgent(SessionVariables.AgentId, SessionVariables.Password, "");
        }
      if (SessionVariables.AgentAsCustomerInfo == null) return Redirect("~/");
      if (SessionVariables.AgentAsCustomerInfo != null) {
        //SessionVariablesCore.LogWriter.LoginId = SessionVariables.AgentAsCustomerInfo.CustomerID.Trim();
        SessionVariables.CurrentBalance = DisplayNumber.Format(SessionVariables.AgentAsCustomerInfo.CurrentBalance, true, 100, true, false);
      }
      SystemLog.WriteAccessLog("Login Successful", SessionVariablesCore.DeviceInfo.DeviceAlias);
      return Redirect("~/");
    }

  }
}
