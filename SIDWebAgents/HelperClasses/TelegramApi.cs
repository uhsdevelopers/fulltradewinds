﻿using SIDWebAgents.TelegramService;
using SIDWebLibraries.HelperClasses;

namespace SIDWebAgents.HelperClasses {
  public static class TelegramApi {

    public static void SendTelegram(string message) {
        return;
      var s = new Service();
        
      s.SendMessageToGroup(message, WebConfigManager.GetTelegramGroupId());


    }

  }
}