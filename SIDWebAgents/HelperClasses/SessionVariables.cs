﻿using System;
using System.Configuration;
using SIDLibraries.Entities;
using SIDWebLibraries.HelperClasses;

namespace SIDWebAgents.HelperClasses {
  public static class SessionVariables {

    public static spCstGetCustomerInfoNoNulls_Result AgentAsCustomerInfo {
      get { return (spCstGetCustomerInfoNoNulls_Result)SessionVariablesCore.GetSessionValue("AgentAsCustomerInfo"); }
      set { SessionVariablesCore.SetSessionValue("AgentAsCustomerInfo", value); }
    }

    public static String AgentId {
      get { return (String)SessionVariablesCore.GetSessionValue("AgentId"); }
      set { SessionVariablesCore.SetSessionValue("AgentId", value); }
    }

    public static String CustomerId {
      get { return (String)SessionVariablesCore.GetSessionValue("CustomerId"); }
      set { SessionVariablesCore.SetSessionValue("CustomerId", value); }
    }

    public static String Password {
      get { return (String)SessionVariablesCore.GetSessionValue("Password"); }
      set { SessionVariablesCore.SetSessionValue("Password", value); }
    }

    public static string CurrentBalance {
      get { return (String)SessionVariablesCore.GetSessionValue("CurrentBalance"); }
      set { SessionVariablesCore.SetSessionValue("CurrentBalance", value); }
    }

    public static string CurrentMakeup {
      get { return (String)SessionVariablesCore.GetSessionValue("CurrentMakeup"); }
      set { SessionVariablesCore.SetSessionValue("CurrentMakeup", value); }
    }

    public static string SiteCode {
      get { return ConfigurationManager.AppSettings["SiteCode"]; }
      set { SessionVariablesCore.SetSessionValue("SiteCode", value); }
    }
  }
}