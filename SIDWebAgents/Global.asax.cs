﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using SIDLibraries.BusinessLayer;
using SIDWebLibraries.HelperClasses;

namespace SIDWebAgents {
  // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
  // visit http://go.microsoft.com/?LinkId=9394801

  public class MvcApplication : HttpApplication {
    protected void Application_Start() {
      AreaRegistration.RegisterAllAreas();

      WebApiConfig.Register(GlobalConfiguration.Configuration);
      FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
      RouteConfig.RegisterRoutes(RouteTable.Routes);
      BundleConfig.RegisterBundles(BundleTable.Bundles);
    }

    void Session_Start(object sender, EventArgs e) {
      // Code that runs when a new session is started
      var context = HttpContext.Current;
      context.Session.Timeout = 15;
      if (context.Session == null) return;
      //SessionVariablesCore.WebTarget = ConfigurationManager.AppSettings["WebTarget"];
      //SessionVariablesCore.ClientCode = SessionVariablesCore.WebTarget;

      //Use the following code block if starting this project directly w/o SIDWeb begin.
      //SessionVariables.AgentId = ConfigurationManager.AppSettings["AgentId"];
      //SessionVariables.Password = ConfigurationManager.AppSettings["Password"];
      var im = new InstanceManager.BusinessLayer.InstanceManager(WebConfigManager.GetApplicationId(), WebConfigManager.GetDefaultInstance());
      SystemParameters.LoadModuleParameters(im.InstanceModuleInfo);
      SessionVariablesCore.InstanceManager = im;
      /*using (var ag = new Agents(im.InstanceModuleInfo)) {
        SessionVariablesCore.AgentInfo = ag.GetAgent(SessionVariables.AgentId, SessionVariables.Password, ServerVariables.ClientIp);
      }

      using (var cst = new Customers(im.InstanceModuleInfo)) {
        SessionVariables.AgentAsCustomerInfo = cst.GetCustomerInfo(SessionVariables.AgentId).FirstOrDefault();
      }
      SessionVariables.CurrentBalance = DisplayNumber.Format(SessionVariables.AgentAsCustomerInfo != null ? SessionVariables.AgentAsCustomerInfo.CurrentBalance : 0, true, 100, true, false);
      //SessionVariables.CurrentMakeup = DisplayNumber.Format((double)SessionVariables.AgentInfo.CurrentMakeup, true, 100, true, false);
      //Use the following code block if starting this project directly w/o SIDWeb end.
      */
    }

    private void Session_End(object sender, EventArgs e) {
      // Code that runs when a session ends. 
      // Note: The Session_End event is raised only when the sessionstate mode
      // is set to InProc in the Web.config file. If session mode is set to StateServer 
      // or SQLServer, the event is not raised.
      if (SessionVariablesCore.InstanceManager != null)
        SessionVariablesCore.InstanceManager.DeleteAppInUse(WebConfigManager.GetApplicationId());
    }

  }
}