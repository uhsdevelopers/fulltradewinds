﻿using System.Web.Optimization;
using SIDWebLibraries.HelperClasses;

namespace SIDWebAgents {
  public class BundleConfig {
    // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
    public static void RegisterBundles(BundleCollection bundles) {

      //var appLastUpdate = WebConfigManager.GetAppLastUpdate();

      bundles.Add(new StyleBundle("~/Content/Style").Include(
        "~/assets/libs/bootstrap/css/bootstrap.css",
        "~/assets/libs/bootstrap/css/daterangepicker.css",
        "~/assets/css/simple-sidebar.css",
        "~/assets/css/font-awesome.css",
        "~/assets/libs/datatables/datatables.css",
        "~/assets/libs/tables/tablesaw.css",
        //"~/assets/libs/tables/table-fixed-header.css",
        "~/assets/libs/tables/demo.css",
        "~/assets/css/style.css",
        "~/assets/css/custom-select-style.css",
        "~/assets/libs/morrisjs/morris.css",
        "~/assets/libs/angular/loading/angular-loading.css",
        "~/assets/libs/angular/loading-bar/loading-bar.css"
        ));

      bundles.Add(new ScriptBundle("~/Content/Libs").Include(
        //"~/assets/libs/tables/tablesaw.js",
        "~/assets/libs/bootstrap/js/bootstrap.js",
        "~/assets/libs/bootstrap/js/boostrap-dialog.js",
        "~/assets/libs/bootstrap/js/daterangepicker.js",
        "~/assets/libs/raphael/raphael.js",
        "~/assets/libs/morrisjs/morris.js",
        "~/assets/js/sidebar_menu.js",
        "~/assets/js/app.UI.js"
        ));

      bundles.Add(new ScriptBundle("~/Content/TableSaw").Include(
        "~/assets/libs/tables/tablesaw.js",
        /*"~/assets/libs/tables/tablesaw.jquery.js",*/
        "~/assets/libs/tables/tablesaw-init.js"
        ));

      bundles.Add(new ScriptBundle("~/Content/DataTables").Include(
        "~/assets/libs/datatables/datatables.js"
        ));


      bundles.Add(new ScriptBundle("~/Content/angular").Include(
          "~/assets/libs/jquery/jquery-3.3.1.js",
        "~/assets/libs/jquery/jquery.signalR-2.3.0.js",
        "~/assets/libs/tables/table-fixed-header.js",
        "~/assets/libs/angular/angular.js",
        "~/assets/libs/angular/angular-route.js",
        "~/assets/libs/angular/angular-animate.js",
        "~/assets/js/spin.js",
        "~/assets/libs/angular/loading/angular-loading.js",
        "~/assets/libs/angular/loading-bar/loading-bar.js",
        "~/assets/js/customSelect.js"
        ));

      bundles.Add(new ScriptBundle("~/Content/appLibs").Include(
                  "~/app/serviceCaller.js",
                  "~/assets/libs/websocket/swfobject.js",
                  "~/assets/libs/websocket/web_socket.js",
                  "~/assets/js/moment.js",
                  "~/assets/js/moment-timezone.js"
                  ));

      bundles.Add(new ScriptBundle("~/Content/appControllers").Include(
                  "~/app/app.main.js",
                  "~/app/app.controller.js",
                  "~/app/components/actions/actionsController.js",
                  "~/app/components/dashboard/dashboardController.js",
                  "~/app/components/agentdistribution/agentdistributionController.js",
                  "~/app/components/agentposition/agentpositionController.js",
                  "~/app/components/cashtransactions/cashtransactionsController.js",
                  "~/app/components/customers/customersController.js",
                  "~/app/components/customers/sportsLimitsController.js",
                  "~/app/components/customer/customerController.js",
                  "~/app/components/customertransactions/customertransactionsController.js",
                  "~/app/components/dailyfigures/dailyFiguresController.js",
                  "~/app/components/liveticker/livetickerController.js",
                  "~/app/components/openbets/openbetsController.js"
                  ));


      bundles.Add(new ScriptBundle("~/Content/appCore").Include(
                  "~/app/core/appSettings.js",
                  "~/app/core/commonFunctions.js",
                  "~/app/core/lineOffering.js",
                  "~/app/core/angular-filter.js",
                  "~/app/core/directives.js",
                  "~/app/directives/custActions/custActions.js",
                  "~/app/directives/performance/performance.js",
                  "~/app/core/filters.js"));


      bundles.Add(new ScriptBundle("~/Content/appServices").Include(
                  "~/app/services/agentService.js",
                  "~/app/services/app.errorHandlerService.js",
                  "~/app/services/app.translatorService.js",
                  "~/app/services/app.webSocketService.js"
                  ));

#if DEBUG
            BundleTable.EnableOptimizations = false;
#else
      BundleTable.EnableOptimizations = true;
#endif


    }
  }
}