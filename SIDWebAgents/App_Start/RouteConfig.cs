﻿using System.Web.Mvc;
using System.Web.Routing;

namespace SIDWebAgents {
  public class RouteConfig {
    public static void RegisterRoutes(RouteCollection routes) {
      routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
      routes.IgnoreRoute("{directory}/{resource}.asmx/{*pathInfo}");
      routes.IgnoreRoute("{directory}/{resource}/{filename}.html");

      routes.MapRoute(
          name: "Default",
          url: "{controller}/{action}/{id}",
          defaults: new { controller = "Main", action = "Index", id = UrlParameter.Optional }
      );
    }
  }
}