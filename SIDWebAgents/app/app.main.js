﻿var appModule = angular.module("mainModule", ['ngRoute', 'angular-loading-bar', 'ngAnimate', 'darthwade.loading', 'AxelSoft']);
var appVersion = '20181119-1';
appModule.run(['$rootScope', function ($rootScope) {
  $rootScope.safeApply = function (fn) {
    var phase = this.$root.$$phase;
    if (phase == '$apply' || phase == '$digest') {
      if (fn && (typeof (fn) === 'function')) {
        fn();
      }
    } else {
      this.$apply(fn);
    }
  };

}]);

appModule.config(['$routeProvider',
  function ($routeProvider) {
    $routeProvider.
      when('/', {
        templateUrl: 'app/components/customers/customersView.html?v=' + appVersion,
        controller: 'customersController',
        reloadOnSearch: false
      }).
      when('/dashboard', {
        templateUrl: 'app/components/dashboard/dashboardView.html?v=' + appVersion,
        controller: 'dashboardController',
        reloadOnSearch: false
      }).
        when('/agentdistribution', {
          templateUrl: 'app/components/agentdistribution/agentdistributionView.html?v=' + appVersion,
          controller: 'agentdistributionController',
          reloadOnSearch: false
        }).
        when('/agentposition', {
          templateUrl: 'app/components/agentposition/agentpositionView.html?v=' + appVersion,
          controller: 'agentpositionController',
          reloadOnSearch: false
        }).
        when('/summarytransations/:SummaryOnly', {
          templateUrl: 'app/components/agentdistribution/agentdistributionView.html?v=' + appVersion,
          controller: 'agentdistributionController',
          reloadOnSearch: false
        }).
        when('/cashtransactions/:AgentIdArg', {
          templateUrl: 'app/components/cashtransactions/cashtransactionsView.html?v=' + appVersion,
          controller: 'cashtransactionsController',
          reloadOnSearch: false
        }).
        when('/customers', {
          templateUrl: 'app/components/customers/customersView.html?v=' + appVersion,
          controller: 'customersController',
          reloadOnSearch: false
        }).
        when('/customertransactions/', {
          templateUrl: 'app/components/customertransactions/customertransactionsView.html?v=' + appVersion,
          controller: 'customertransactionsController',
          reloadOnSearch: false
        }).
        when('/customertransactions/:CustomerIdArg/:IsAgent', {
          templateUrl: 'app/components/customertransactions/customertransactionsView.html?v=' + appVersion,
          controller: 'customertransactionsController',
          reloadOnSearch: false
        }).
      when('/dailyfigures', {
        templateUrl: 'app/components/dailyfigures/dailyFiguresView.html?v=' + appVersion,
        controller: 'dailyFiguresController',
        reloadOnSearch: false
      }).
        when('/dailyfiguresws/:ShowSettleFigure', {
          templateUrl: 'app/components/dailyfigures/dailyFiguresView.html?v=' + appVersion,
          controller: 'dailyFiguresController',
          reloadOnSearch: false
        }).
         when('/liveticker', {
           templateUrl: 'app/components/liveticker/livetickerView.html?v=' + appVersion,
           controller: 'livetickerController',
           reloadOnSearch: false
         }).
        when('/openbets', {
          templateUrl: 'app/components/openbets/openbetsView.html?v=' + appVersion,
          controller: 'openbetsController',
          reloadOnSearch: false
        }).
        when('/performance', {
          templateUrl: 'app/components/performance/performanceView.html?v=' + appVersion,
          //controller: 'performanceController',
          reloadOnSearch: false
        }).
        when('/sportslimits', {
          templateUrl: 'app/components/customers/sportsLimitsView.html?v=' + appVersion,
          controller: 'sportsLimitsController',
          reloadOnSearch: false
        }).
      otherwise({
        redirectTo: '/ag/'
      });

    String.prototype.RemoveSpecials = function () {
      return this.replace(/[^\w\s]/gi, '').split(' ').join('_');
    };
  }]);

function displayTooltips() {
  setTimeout(function () {
    jQuery('[data-toggle="tooltip"]').tooltip();
  }, 1000);
}

jQuery.noConflict();


(function ($) {
  $.fn.hasScrollBar = function () {
    return this.get(0).scrollHeight > this.get(0).clientHeight;
  };
})(jQuery);