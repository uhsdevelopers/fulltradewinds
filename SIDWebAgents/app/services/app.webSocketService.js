﻿appModule.factory('$webSocketService', ['$agentService', function ($agentService) {
  var webSocketService = {};
  var connectionSate = 0;
  var _servers = SETTINGS.SocketServers;
  var _selectedServer;
  var _GetSelectServer = function () {
    if (_selectedServer) return _servers[_selectedServer];
    else _selectedServer = Math.floor(Math.random() * _servers.length);
    return _servers[_selectedServer];
  };

  jQuery.support.cors = true;

  var ws = jQuery != null ? jQuery.connection : null;

  if (ws) ws.hub.url = _GetSelectServer();

  var sidHub = ws != null ? ws.sidHub : null;

  var hubReady = null;

  var hubIsActive = function () {
    return sidHub;
  };

  if (hubIsActive()) hubReady = ws.hub.start();

  if (hubIsActive())
    ws.hub.stateChanged(function (change) {
      connectionSate = change.newState;
    });

  webSocketService.SubscribeCustomer = function (agentId) {
    if (!hubIsActive()) return;
    hubReady.done(function () {
      sidHub.server.subscribeCustomer({ CustomerId: agentId, Platform: 'WEB', IsAgent: true });
    });
  };

  webSocketService.IsSupported = function () {
    if ("WebSocket" in window) return true;
    _ReportError("WebSocket NOT supported by your Browser!");
    return false;
  };

  webSocketService.IsConnected = function () {
    return connectionSate != 4;
  };


  if (hubIsActive()) {
    sidHub.client.broadcastMessage = function (message) {
      message = jQuery.parseJSON(message);
      switch (message.Context) {
        case "WagersInfoChange":
          $agentService.PrependWagersToBetTicker(message.Data);
          break;
      }
    }
  }

  return webSocketService;

}]);