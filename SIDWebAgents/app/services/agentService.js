﻿appModule.factory('$agentService', ['$http', '$rootScope', '$translatorService', function ($http, $rootScope, $translatorService) {

    var caller = new ServiceCaller($http, null, 'AgentService');
    var agentService = {};

    agentService.DailyFigures = null;
    agentService.CashTransactionList = null;
    agentService.TransactionList = null;
    agentService.CasinoTransactionList = null;
    agentService.CasinoTransactionList = null;
    agentService.GroupedOpenBets = null;
    agentService.TransactionList = null;
    agentService.TransactionListSummary = null;
    agentService.Restrictions = null;

    agentService.GetAgent = function () {
        return caller.POST({}, 'GetAgentInfo', null, true).then(function (result) {
            agentService.AgentInfo = result.data.d.Data;
            setTimeout(function () {
                $rootScope.$broadcast('AgentInfoLoaded');
            }, 900);
        });
    };

    agentService.GetAgentAsCustomer = function () {
        return caller.POST({}, 'GetAgentAsCustomerInfo', null, true).then(function (result) {
            agentService.AgentAsCustomerInfo = result.data.d.Data.AgentAsCustomerInfo;
            agentService.Restrictions = result.data.d.Data.Restrictions;
            agentService.Settings = result.data.d.Data.Settings;
            $translatorService.ChangeLanguage(agentService.Settings.WebLanguage);
            setTimeout(function () {
                $rootScope.$broadcast('AgentAsCutomerInfoLoaded');
            }, 900);
        });
    };

    agentService.GetAgentsCurrentBalance = function () {
        return caller.POST({}, 'GetAgentsCurrentBalance', null, true).then();
    };

    agentService.LogWritter = function (a, b) {
        return caller.POST({ operation: a, data: b }, 'LogWritter', null, true).then();
    };

    agentService.TrackStatus = function () {
        return caller.POST({}, 'TrackStatus', null, true).then();
    };

    agentService.LiveTickerFilters = {
        WagerType: {},
        WagerAmount: {}
    };

    var _Transaction = function (transaction) {
        return {
            Amount: transaction.Amount,
            AmountLost: transaction.AmountLost,
            AmountWagered: transaction.AmountWagered,
            AmountWon: transaction.AmountWon,
            CurrentBalance: transaction.CurrentBalance,
            Comments: transaction.Comments,
            Description: transaction.Description,
            DocumentNumber: transaction.DocumentNumber,
            EnteredBy: transaction.EnteredBy,
            FreePlayFlag: transaction.FreePlayFlag,
            HoldAmount: transaction.HoldAmount,
            ItemWagerType: transaction.ItemWagerType,
            Items: transaction.Items,
            Outcome: transaction.Outcome,
            PeriodDescription: transaction.PeriodDescription,
            ShortDesc: transaction.ShortDesc,
            SportSubType: transaction.SportSubType,
            SportType: transaction.SportType,
            Team1ID: transaction.Team1ID,
            Team1Score: transaction.Team1Score,
            Team2ID: transaction.Team2ID,
            Team2Score: transaction.Team2Score,
            TranCode: transaction.TranCode,
            TranDateTime: transaction.TranDateTime,
            TranType: transaction.TranType,
            WagerNumber: (transaction.WagerNumber ? '-' + transaction.WagerNumber : ''),
            WagerType: transaction.WagerType,
            WinnerID: transaction.WinnerID
        };
    };

    var _newWagerItem = function (wager) {
        return {
            Comments: (wager.Comments == null || wager.Comments === '' ? null : wager.Comments),
            Description: wager.Description,
            FreePlayFlag: wager.FreePlayFlag,
            SportType: wager.SportType,
            PeriodDescription: wager.PeriodDescription,
            Team1Score: wager.Team1Score,
            Team2Score: wager.Team2Score,
            Team1ID: wager.Team1ID,
            Team2ID: wager.Team2ID,
            Outcome: (wager.Outcome == "W" ? 'WON' : wager.Outcome == "L" ? 'LOST' : wager.Outcome == "X" ? 'PUSH' : ''),
            Result: (wager.Outcome == "W" || wager.TranType == "W" ? 'WON' : wager.Outcome == "L" || wager.TranType == "L" ? 'LOST' : wager.Outcome == "X" ? 'PUSH' : ''),
            WagerType: wager.WagerType,
            WinnerID: wager.WinnerID,
            EventDateTime: wager.EventDateTime
        };
    };

    agentService.AddCustomerMessage = function (a, b, c, d, e) {
        return caller.POST({ customerId: a, msgExpirationDateTime: b, priority: c, msgSubject: d, msgBody: e }, 'AddCustomerMessage').then();
    };

    agentService.GetCustomersList = function (a) {
        return caller.POST({ weekNumber: a }, 'GetCustomersInfo', null, true).then(function (result) {
            agentService.AgentsList = agentService.GroupCustomersByAgent(result.data.d.Data);
            $rootScope.$broadcast('CustomersListUpdated');
        });
    };

    agentService.GetCustomersDailyFigures = function (a) {
        return caller.POST({ weekNumber: a }, 'GetCustomersDailyFigures').then();
    };

    agentService.GetCustDailyFigureByAgentDashboard = function (a) {
        return caller.POST({ weekNumber: a }, 'GetCustDailyFigureByAgentDashboard').then();
    };

    agentService.GetCustomerBalance = function (a) {
        return caller.POST({ customerId: a }, 'GetCustomerBalance', null, true).then();
    };

    agentService.GetActivePlayersCount = function (a) {
        return caller.POST({}, 'GetActivePlayersCount', null, true).then();
    };

    agentService.GetWinLossByWeek = function (a) {
        return caller.POST({ weekNumber: a }, 'GetWinLossByWeek').then();
    };

    agentService.GetCustomerDailyFigures = function (a, b, c) {
        caller.POST({ 'customerId': a, 'weekOffset': b, 'currencyCode': c }, 'GetCustomerDailyFigures').then(function (result) {
            agentService.DailyFigures = result.data.d.Data;
            if (agentService.DailyFigures.ZeroBalance != null)
                agentService.DailyFigures.ZeroBalance = CommonFunctions.RoundNumber(agentService.DailyFigures.ZeroBalance);
            for (var i = 0; i < agentService.DailyFigures.ValuesPerDay.length; i++) {
                agentService.DailyFigures.ValuesPerDay[i].ThisDate = CommonFunctions.FormatDateTime(agentService.DailyFigures.ValuesPerDay[i].ThisDate, 1, 2); // timezone is temp, fix *

                if (agentService.DailyFigures.ValuesPerDay[i].CashInOut != null)
                    agentService.DailyFigures.ValuesPerDay[i].CashInOut = CommonFunctions.RoundNumber(agentService.DailyFigures.ValuesPerDay[i].CashInOut);

                if (agentService.DailyFigures.ValuesPerDay[i].WinLoss != null)
                    agentService.DailyFigures.ValuesPerDay[i].WinLoss = CommonFunctions.RoundNumber(agentService.DailyFigures.ValuesPerDay[i].WinLoss);

                if (agentService.DailyFigures.ValuesPerDay[i].CasinoWinLoss != null)
                    agentService.DailyFigures.ValuesPerDay[i].CasinoWinLoss = CommonFunctions.RoundNumber(agentService.DailyFigures.ValuesPerDay[i].CasinoWinLoss);
            }
            $rootScope.$broadcast('dailyFiguresLoaded');
        });
        actualWeek = b;
    };

    agentService.GetAgentPerformance = function (a, b, c) {
        return caller.POST({ period: a, wagerType: b, customerId: c }, 'GetAgentPerformance').then();
    };

    agentService.GetAgentCashTransactions = function (a, b) {
        return caller.POST({ weekNumber: a, agentId: b }, 'GetAgentCashTransactions').then();
    };

    agentService.GetCashTransactionsByDate = function (a, b) {
        return caller.POST({ 'customerId': a, 'date': CommonFunctions.FormatDateTime(b, 4) }, 'GetCashTransactionsByDate').then();
    };

    agentService.GetAgentsList = function () {
        return caller.POST({}, 'GetAgentsList', null, true).then();
    };

    agentService.GetCustomerTransactions = function (a, b) {
        return caller.POST({ customerId: a, weekNumber: b }, 'GetCustomerTransactions').then();
    };

    agentService.GetCustomerTransactionListByDate = function (a, b, c) {
        var groupedItems = null;
        var holdDocumentNumber = null;
        var holdWagerNumber = null;
        return caller.POST({ 'customerId': a, 'date': CommonFunctions.FormatDateTime(b, 4), 'includeCasino': c }, 'GetCustomerTransactionListByDate').then(function (result) {

            var allTransactions = result.data.d.Data;
            var groupedTransactions = new Array();

            if (allTransactions != null && allTransactions.length > 0) {

                groupedTransactions = new Array();
                groupedItems = new Array();
                holdDocumentNumber = allTransactions[0].DocumentNumber;
                holdWagerNumber = allTransactions[0].WagerNumber;

                for (var i = 0; i < allTransactions.length; i++) {
                    if (holdDocumentNumber != allTransactions[i].DocumentNumber || holdWagerNumber != allTransactions[i].WagerNumber) {
                        groupedTransactions.push(_Transaction(allTransactions[i - 1]));
                        if (groupedTransactions.length > 0 && groupedItems != null && groupedItems.length > 0)
                            groupedTransactions[groupedTransactions.length - 1].Items = groupedItems;
                        groupedItems = new Array();
                        groupedItems.push(_newWagerItem(allTransactions[i]));
                    } else {
                        groupedItems.push(_newWagerItem(allTransactions[i]));
                    }
                    holdDocumentNumber = allTransactions[i].DocumentNumber;
                    holdWagerNumber = allTransactions[i].WagerNumber;
                    if (i == allTransactions.length - 1) {
                        groupedTransactions.push(_Transaction(allTransactions[i]));
                        groupedTransactions[groupedTransactions.length - 1].Items = groupedItems;
                    }
                }
            }
            agentService.TransactionList = groupedTransactions;
            $rootScope.$broadcast('transactionListByDateLoaded');
        });
    };

    agentService.GetCustomerTransactionListByDays = function (a, b) {
        var groupedItems = null;
        var holdDocumentNumber = null;
        var holdWagerNumber = null;
        caller.POST({ 'customerId': a, 'numDays': b }, 'GetCustomerTransactionListByDays').then(function (result) {

            var allTransactions = result.data.d.Data;
            var groupedTransactions = new Array();
            var accumTran = 0;

            if (allTransactions != null && allTransactions.length > 0) {

                groupedTransactions = new Array();
                groupedItems = new Array();
                holdDocumentNumber = allTransactions[allTransactions.length - 1].DocumentNumber;
                holdWagerNumber = allTransactions[allTransactions.length - 1].WagerNumber;

                for (var i = allTransactions.length - 1; i >= 0; i--) {
                    if (holdDocumentNumber != allTransactions[i].DocumentNumber || holdWagerNumber != allTransactions[i].WagerNumber) {
                        groupedTransactions.push(allTransactions[i + 1]);
                        if (groupedTransactions.length > 0 && groupedItems != null && groupedItems.length > 0)
                            groupedTransactions[groupedTransactions.length - 1].Items = groupedItems;
                        groupedItems = new Array();
                        groupedItems.push(_newWagerItem(allTransactions[i]));
                        if (i < allTransactions.length - 1) groupedTransactions[groupedTransactions.length - 1].CurrentBalance = groupedTransactions[groupedTransactions.length - 1].CurrentBalance + accumTran;
                        accumTran += groupedTransactions[groupedTransactions.length - 1].TranCode == "C" ? groupedTransactions[groupedTransactions.length - 1].Amount * -1 : groupedTransactions[groupedTransactions.length - 1].Amount;


                    } else {
                        groupedItems.push(_newWagerItem(allTransactions[i]));
                    }
                    holdDocumentNumber = allTransactions[i].DocumentNumber;
                    holdWagerNumber = allTransactions[i].WagerNumber;
                    if (i == 0) {
                        groupedTransactions.push(allTransactions[i]);
                        groupedTransactions[groupedTransactions.length - 1].Items = groupedItems;
                        groupedTransactions[groupedTransactions.length - 1].CurrentBalance = groupedTransactions[groupedTransactions.length - 1].CurrentBalance + accumTran;
                    }
                }
            }
            agentService.TransactionList = groupedTransactions;

            $rootScope.$broadcast('transactionListLoaded');
        });
    };

    agentService.GetAgentTransactionListByDays = function (a) {
        caller.POST({ 'numDays': a }, 'GetAgentTransactionListByDays').then(function (result) {

            var allTransactions = result.data.d.Data;
            var accumTran = null;
            if (allTransactions != null && allTransactions.length > 0) {
                for (var i = allTransactions.length - 1; i >= 0; i--) {
                    var e = allTransactions[i];
                    if (accumTran) e.CurrentBalance += accumTran;
                    accumTran += e.TranCode == "C" ? e.Amount * -1 : e.Amount;
                    e.Won = e.TranCode == "C" ? e.Amount : 0;
                    e.Lost = e.TranCode == "D" ? e.Amount * -1 : 0;
                    e.TranDateTimeString = CommonFunctions.FormatDateTime(e.TranDateTimeString, 4);
                };
            }
            agentService.TransactionListSummary = allTransactions;

            $rootScope.$broadcast('transactiontListSummaryLoaded');
        });
    };

    agentService.GetCasinoTransactionsByDate = function (a, b) {
        caller.POST({ 'customerId': a, 'date': CommonFunctions.FormatDateTime(b, 4) }, 'GetCasinoTransactionsByDate', null, true).then(function (result) {
            var casinoTransactions = result.data.d.Data;
            if (casinoTransactions != null && casinoTransactions.length > 0) {
                agentService.CasinoTransactionList = [];
                for (var i = 0; i < casinoTransactions.length; i++) {
                    agentService.CasinoTransactionList.push(_Transaction(casinoTransactions[i]));
                }
            } else {
                agentService.CasinoTransactionList = [];
            }
            $rootScope.$broadcast('casinoTransactionsLoaded');

        });
    };

    agentService.GetCustomerTransactionByGradeNumber = function (a) {
        return caller.POST({ gradeNumber: a }, 'GetCustomerTransactionByGradeNumber').then();
    };

    agentService.GetAgentDistribution = function () {
        return caller.POST({}, 'GetAgentDistribution').then();
    };

    agentService.GetAgentCustomersList = function () {
        return caller.POST({}, 'GetAgentCustomersList').then();
    };

    agentService.GetSportTypesList = function () {
        return caller.POST({}, 'GetSportTypesList').then();
    };

    agentService.GetCustomerOpenBets = function (a, b, c, d, e, f) {
        return caller.POST({ customerId: a, wagerType: b, continueOnPush: c, checkForAR: d, sportType: e, noProps: f }, 'GetCustomerOpenBets').then();
    };

    agentService.GetCustomerPendingBets = function (a) {
        var groupedItems = null;
        var holdTicketNumber = null;
        var holdWagerNumber = null;
        return caller.POST({ 'customerId': a }, 'GetCustomerPendingBets').then(function (result) {

            var allOpenBets = result.data.d.Data;
            var groupedOpenBets = new Array();

            if (allOpenBets != null && allOpenBets.length > 0) {

                groupedOpenBets = new Array();
                groupedItems = new Array();
                holdTicketNumber = allOpenBets[0].TicketNumber;
                holdWagerNumber = allOpenBets[0].WagerNumber;

                for (var i = 0; i < allOpenBets.length; i++) {
                    if (holdTicketNumber != allOpenBets[i].TicketNumber || holdWagerNumber != allOpenBets[i].WagerNumber) {
                        groupedOpenBets.push(allOpenBets[i - 1]);
                        if (groupedOpenBets.length > 0 && groupedItems != null && groupedItems.length > 0)
                            groupedOpenBets[groupedOpenBets.length - 1].Items = groupedItems;
                        groupedItems = new Array();
                        groupedItems.push(_newWagerItem(allOpenBets[i]));
                    } else {
                        groupedItems.push(_newWagerItem(allOpenBets[i]));
                    }
                    holdTicketNumber = allOpenBets[i].TicketNumber;
                    holdWagerNumber = allOpenBets[i].WagerNumber;

                    if (i == allOpenBets.length - 1) {
                        groupedOpenBets.push(allOpenBets[i]);
                        groupedOpenBets[groupedOpenBets.length - 1].Items = groupedItems;
                    }
                }
            }
            agentService.GroupedOpenBets = groupedOpenBets;
            $rootScope.$broadcast('openBetsLoaded');
        });
    };

    agentService.UpdateCustomerAccess = function (a, b, c) {
        return caller.POST({ customerId: a, accessCode: b, accessValue: c }, 'UpdateCustomerAccess').then();
    };

    agentService.UpdateCustomer = function (a, b, c, d, e, f, g, h, i) {
        return caller.POST({ customerId: a, password: b, firstName: c, middleName: d, lastName: e, address: f, state: g, city: h, zip: i }, 'UpdateCustomer').then();
    };

    agentService.GetWeeksRange = function () {
        var weeksArray = new Array();
        for (var i = 0; i < 11; i++) {
            weeksArray.push({ DateRange: CommonFunctions.WeekbyDatesRange(i), Index: i });
        }
        return weeksArray;
    };

    agentService.GetActivePeriodsAndSports = function (a) {
        return caller.POST({ weekNumber: a }, 'GetActivePeriodsAndSports', null, true).then();
    };

    agentService.GetAgentPositionByGame = function (a, b, c, d) {
        return caller.POST({ sportType: a, sportSubType: b, periodNumber: c, weekNum: d }, 'GetAgentPositionByGame', null, true).then();
    };

    agentService.GetDailyFigureDatesRange = function () {
        return caller.POST({}, 'GetDatesSinceCloseOfWeek', null, true).then();
    };

    agentService.InsertCustomerTransaction = function (a, b, c, d, e, f, g, h) {
        return caller.POST({ customerId: a, tranCode: b, tranType: c, amount: d, paymentBy: e, description: f, dailyFigureDate: g, freePlayTransaction: h }, 'InsertCustomerTransaction').then();
    };

    agentService.ChangeCustomerSettleFigure = function (a, b, c) {
        return caller.POST({ customerId: a, settleFigure: b }, 'ChangeCustomerSettleFigure').then();
    };

    agentService.ChangeCustomerCreditLimit = function (a, b, c) {
        return caller.POST({ customerId: a, creditLimit: b, tempCreditAdjExpDate: c }, 'ChangeCustomerCreditLimits').then();
    };

    agentService.ChangeCustomerTempCreditLimit = function (a, b, c) {
        return caller.POST({ customerId: a, creditLimit: b, tempCreditAdjExpDate: CommonFunctions.FormatDateTime(c, 4) }, 'ChangeCustomerTempCreditLimits').then();
    };

    agentService.ChangeCustomerQuickLimit = function (a, b, c) {
        return caller.POST({ customerId: a, quickLimit: b }, 'ChangeCustomerQuickLimits').then();
    };

    agentService.ChangeCustomerTempQuickLimit = function (a, b, c) {
        return caller.POST({ customerId: a, quickLimit: b, tempQuickAdjExpDate: CommonFunctions.FormatDateTime(c, 4) }, 'ChangeCustomerTempQuickLimits').then();
    };

    agentService.GetLiveTickerInfo = function (a, b) {
        return caller.POST({ wagerTypeIdx: a, wagerAmtsIdx: b }, 'GetLiveTickerInfo').then(function (result) {
            agentService.LiveTickerInfo = buildLiveTickerInfoObject(result.data.d.Data);
        });
    };

    agentService.GetGameActionByLine = function (gameNum, periodNum, chosenTeamId, wagerType) {
        return caller.POST({ 'gameNum': gameNum, 'periodNum': periodNum, 'chosenTeamId': chosenTeamId, 'wagerType': wagerType }, 'GetGameActionByLine').then();
    };

    agentService.GetDetailWagerLimits = function (userId, sportType, period) {
        return caller.POST({ 'userId': userId, 'sportType': sportType, 'period': period }, 'GetDetailWagerLimits', null, true).then();
    };

    agentService.UpdateCustomerSportsLimits = function (cuSpread, inetSpread, cuMoney, inetMoney, cuTotal, inetTotal, period, sportType, sportSubType, customerId) {
        return caller.POST({ 'cuSpread': cuSpread, 'inetSpread': inetSpread, 'cuMoney': cuMoney, 'inetMoney': inetMoney, 'cuTotal': cuTotal, 'inetTotal': inetTotal, 'period': period, 'sportType': sportType, 'sportSubType': sportSubType, 'customerId': customerId }, 'UpdateCustomerSportsLimits').then();
    };

    function buildLiveTickerInfoObject(rawData) {

        var wagerTypeCode = null;
        var wagerAmountCode = null;

        if (agentService.LiveTickerFilters.WagerType) {
            wagerTypeCode = typeof agentService.LiveTickerFilters.WagerType.Index === "undefined" ? 0 : agentService.LiveTickerFilters.WagerType.Index;
        }

        if (agentService.LiveTickerFilters.WagerAmount) {
            wagerAmountCode = typeof agentService.LiveTickerFilters.WagerAmount.Index === "undefined" ? 0 : agentService.LiveTickerFilters.WagerAmount.Index;
        }

        var WagerItems = new Array();
        var WagerItem = {};
        var LiveTickerInfo = [];
        var calledDateTime = new Date();
        if (rawData) {
            var holdTicketNumber;
            var holdWagerNumber;
            for (var i = 0; i < rawData.length; i++) {
                if ((holdTicketNumber != parseInt(rawData[i].TicketNumber)) ||
                (holdTicketNumber == parseInt(rawData[i].TicketNumber) && holdWagerNumber != parseInt(rawData[i].WagerNumber))) {
                    WagerItems = new Array();
                    var Wager = {
                        TicketNumber: rawData[i].TicketNumber,
                        WagerNumber: rawData[i].WagerNumber,
                        CustomerId: rawData[i].CustomerId,
                        PostedDateTimeString: rawData[i].PostedDateTimeString,
                        TicketWriter: rawData[i].TicketWriter,
                        WagerType: rawData[i].WagerType,
                        AmountWagered: rawData[i].AmountWagered,
                        ToWinAmount: rawData[i].ToWinAmount,
                        Description: rawData[i].Description,
                        ShowedDateTime: calledDateTime,
                        ARLink: rawData[i].ARLink,
                        ContinueOnPushFlag: rawData[i].ContinueOnPushFlag,
                        FreePlayFlag: rawData[i].FreePlayFlag
                    };
                    Wager.WagerItems = [];
                    if (rawData[i].GameNum != null) {
                        var WagerItem = {
                            ItemNumber: rawData[i].ItemNumber,
                            GameNum: rawData[i].GameNum,
                            GameDateTime: rawData[i].GameDateTime,
                            SportType: rawData[i].SportType,
                            SportSubType: rawData[i].SportSubType,
                            ItemWagerType: rawData[i].ItemWagerType,
                            AdjSpread: rawData[i].AdjSpread,
                            AdjTotalPoints: rawData[i].AdjTotalPoints,
                            TotalPointsOU: rawData[i].TotalPointsOU,
                            FinalMoney: rawData[i].FinalMoney,
                            Team1ID: rawData[i].Team1ID,
                            Team1RotNum: rawData[i].Team1RotNum,
                            Team2ID: rawData[i].Team2ID,
                            Team2RotNum: rawData[i].Team2RotNum,
                            ChosenTeamID: rawData[i].ChosenTeamID,
                            Outcome: rawData[i].Outcome,
                            ItemAmountWagered: rawData[i].ItemAmountWagered,
                            ItemToWinAmount: rawData[i].ItemToWinAmount,
                            TeaserPoints: rawData[i].TeaserPoints,
                            GradeNum: rawData[i].GradeNum,
                            Store: rawData[i].Store,
                            CustProfile: rawData[i].CustProfile,
                            PeriodNumber: rawData[i].PeriodNumber,
                            PeriodDescription: rawData[i].PeriodDescription,
                            FinalAmountWagered: rawData[i].FinalAmountWagered,
                            FinalAmountWon: rawData[i].FinalAmountWon,
                            OriginatingTicketNumber: rawData[i].OriginatingTicketNumber,
                            AdjustableOddsFlag: rawData[i].AdjustableOddsFlag,
                            ListedPitcher1: rawData[i].ListedPitcher1,
                            ListedPitcher2: rawData[i].ListedPitcher2,
                            Pitcher1ReqFlag: rawData[i].Pitcher1ReqFlag,
                            Pitcher2ReqFlag: rawData[i].Pitcher2ReqFlag,
                            GradeMoney: rawData[i].GradeMoney,
                            GradeToWinAmount: rawData[i].GradeToWinAmount,
                            PercentBook: rawData[i].PercentBook,
                            VolumeAmount: rawData[i].VolumeAmount,
                            ShowOnChartFlag: rawData[i].ShowOnChartFlag,
                            DailyFigureDate: rawData[i].DailyFigureDate,
                            CurrencyCode: rawData[i].CurrencyCode,
                            ValueDate: rawData[i].ValueDate,
                            ArchiveFlag: rawData[i].ArchiveFlag,
                            LayoffFlag: rawData[i].LayoffFlag,
                            AgentID: rawData[i].AgentID,
                            EasternLine: rawData[i].EasternLine,
                            FreePlayFlag: rawData[i].FreePlayFlag,
                            WiseActionFlag: rawData[i].WiseActionFlag,
                            OrigSpread: rawData[i].OrigSpread,
                            OrigTotalPoints: rawData[i].OrigTotalPoints,
                            OrigMoney: rawData[i].OrigMoney,
                            FinalDecimal: rawData[i].FinalDecimal,
                            FinalNumerator: rawData[i].FinalNumerator,
                            FinalDenominator: rawData[i].FinalDenominator,
                            OrigDecimal: rawData[i].OrigDecimal,
                            OrigNumerator: rawData[i].OrigNumerator,
                            OrigDenominator: rawData[i].OrigDenominator,
                            PriceType: rawData[i].PriceType,
                            GradeDecimal: rawData[i].GradeDecimal,
                            GradeNumerator: rawData[i].GradeNumerator,
                            GradeDenominator: rawData[i].GradeDenominator,
                            AhResultFlag: rawData[i].AhResultFlag,
                            Description: rawData[i].Description
                        };
                        WagerItems.push(WagerItem);
                        Wager.WagerItems = WagerItems;
                    }
                    LiveTickerInfo.push(Wager);
                } else if (rawData[i].GameNum != null) { //apend Item to wager
                    var WagerItem = {
                        ItemNumber: rawData[i].ItemNumber,
                        GameNum: rawData[i].GameNum,
                        GameDateTime: rawData[i].GameDateTime,
                        SportType: rawData[i].SportType,
                        SportSubType: rawData[i].SportSubType,
                        ItemWagerType: rawData[i].ItemWagerType,
                        AdjSpread: rawData[i].AdjSpread,
                        AdjTotalPoints: rawData[i].AdjTotalPoints,
                        TotalPointsOU: rawData[i].TotalPointsOU,
                        FinalMoney: rawData[i].FinalMoney,
                        Team1ID: rawData[i].Team1ID,
                        Team1RotNum: rawData[i].Team1RotNum,
                        Team2ID: rawData[i].Team2ID,
                        Team2RotNum: rawData[i].Team2RotNum,
                        ChosenTeamID: rawData[i].ChosenTeamID,
                        Outcome: rawData[i].Outcome,
                        ItemAmountWagered: rawData[i].ItemAmountWagered,
                        ItemToWinAmount: rawData[i].ItemToWinAmount,
                        TeaserPoints: rawData[i].TeaserPoints,
                        GradeNum: rawData[i].GradeNum,
                        Store: rawData[i].Store,
                        CustProfile: rawData[i].CustProfile,
                        PeriodNumber: rawData[i].PeriodNumber,
                        PeriodDescription: rawData[i].PeriodDescription,
                        FinalAmountWagered: rawData[i].FinalAmountWagered,
                        FinalAmountWon: rawData[i].FinalAmountWon,
                        OriginatingTicketNumber: rawData[i].OriginatingTicketNumber,
                        AdjustableOddsFlag: rawData[i].AdjustableOddsFlag,
                        ListedPitcher1: rawData[i].ListedPitcher1,
                        ListedPitcher2: rawData[i].ListedPitcher2,
                        Pitcher1ReqFlag: rawData[i].Pitcher1ReqFlag,
                        Pitcher2ReqFlag: rawData[i].Pitcher2ReqFlag,
                        GradeMoney: rawData[i].GradeMoney,
                        GradeToWinAmount: rawData[i].GradeToWinAmount,
                        PercentBook: rawData[i].PercentBook,
                        VolumeAmount: rawData[i].VolumeAmount,
                        ShowOnChartFlag: rawData[i].ShowOnChartFlag,
                        DailyFigureDate: rawData[i].DailyFigureDate,
                        CurrencyCode: rawData[i].CurrencyCode,
                        ValueDate: rawData[i].ValueDate,
                        ArchiveFlag: rawData[i].ArchiveFlag,
                        LayoffFlag: rawData[i].LayoffFlag,
                        AgentID: rawData[i].AgentID,
                        EasternLine: rawData[i].EasternLine,
                        FreePlayFlag: rawData[i].FreePlayFlag,
                        WiseActionFlag: rawData[i].WiseActionFlag,
                        OrigSpread: rawData[i].OrigSpread,
                        OrigTotalPoints: rawData[i].OrigTotalPoints,
                        OrigMoney: rawData[i].OrigMoney,
                        FinalDecimal: rawData[i].FinalDecimal,
                        FinalNumerator: rawData[i].FinalNumerator,
                        FinalDenominator: rawData[i].FinalDenominator,
                        OrigDecimal: rawData[i].OrigDecimal,
                        OrigNumerator: rawData[i].OrigNumerator,
                        OrigDenominator: rawData[i].OrigDenominator,
                        PriceType: rawData[i].PriceType,
                        GradeDecimal: rawData[i].GradeDecimal,
                        GradeNumerator: rawData[i].GradeNumerator,
                        GradeDenominator: rawData[i].GradeDenominator,
                        AhResultFlag: rawData[i].AhResultFlag,
                        Description: rawData[i].Description
                    };
                    LiveTickerInfo[LiveTickerInfo.length - 1].WagerItems.push(WagerItem);

                }
                holdTicketNumber = parseInt(rawData[i].TicketNumber);
                holdWagerNumber = parseInt(rawData[i].WagerNumber);
            }
        }
        return LiveTickerInfo;
    };

    agentService.IncludeWager = function (wager, wagerTypeCode, wagerAmountCode) {

        var wagerType = wager.WagerType;
        var wagerAmount = wager.AmountWagered;

        var wagerTypes;
        if (wagerTypeCode == 1)
            wagerTypes = 'SLME';
        else
            wagerTypes = 'SLMEPTI';

        if (wagerTypes.indexOf(wagerType) < 0) {
            return false;
        }

        switch (wagerAmountCode) {
            case 0:
                return true;
            case 1:
                if (wagerAmount / 100 > 0 && wagerAmount / 100 <= 1000)
                    return true;
                else
                    return false;
            case 2:
                if (wagerAmount / 100 > 1000 && wagerAmount / 100 <= 9999.99)
                    return true;
                else
                    return false;
        }
        return true;
    };

    agentService.ExcludeWager = function (wagerInfoItem, deletedWagers) {
        if (deletedWagers == null || deletedWagers.length == 0)
            return false;
        for (var i = 0; i < deletedWagers.length; i++) {
            var ticketNumber = deletedWagers[i].TicketNumber;
            var wagerNumber = deletedWagers[i].WagerNumber;
            if (parseInt(ticketNumber) == parseInt(wagerInfoItem.TicketNumber) && parseInt(wagerNumber) == parseInt(wagerInfoItem.WagerNumber))
                return true;
        }
        return false;
    };

    agentService.PrependWagersToBetTicker = function (raw) {
        var latestRawData = raw.data;
        agentService.TotalPlayers = raw.userCount;
        var insertedWagers = [];
        var deletedWagers = [];
        var i;
        for (i = 0; i < latestRawData.length; i++) {
            if (latestRawData[i].Inserted == true) {
                insertedWagers.push(latestRawData[i]);
            } else {
                deletedWagers.push(latestRawData[i]);
            }
        }
        var tmpNewLiveTickerInfo = buildLiveTickerInfoObject(insertedWagers);

        if (agentService.LiveTickerInfo == null || agentService.LiveTickerInfo.length == 0) {
            agentService.LiveTickerInfo = tmpNewLiveTickerInfo;
        }
        else {
            for (i = 0; i < agentService.LiveTickerInfo.length; i++) {

                if (!excludeWager(agentService.LiveTickerInfo[i], deletedWagers)) {
                    tmpNewLiveTickerInfo.push(agentService.LiveTickerInfo[i]);
                }
            }
            agentService.LiveTickerInfo = tmpNewLiveTickerInfo;
        }

        if (agentService.LiveTickerInfo) {
            $rootScope.$broadcast('NewWagersFound');
        }
    };

    agentService.DisplayWagerTypeName = function (t) {

        var Items = [
            {
                Name: "Contest",
                Code: "C"
            }, {
                Name: "Spread",
                Code: "S"
            }, {
                Name: "Money Line",
                Code: "M"
            }, {
                Name: "Total Points",
                Code: "L"
            }, {
                Name: "Team Totals",
                Code: "E"
            }, {
                Name: "Parlay",
                Code: "P"
            }, {
                Name: "Teaser",
                Code: "T"
            }, {
                Name: "If Bet",
                Code: "I"
            }, {
                Name: "Horses",
                Code: "G"
            }, {
                Name: "Manual Play",
                Code: "A"
            }
        ];
        for (var i = 0; i < Items.length; i++) {
            if (t.WagerType === Items[i].Code) {
                if (t.ARLink == 1) {
                    return 'Action Reverse';
                } else return Items[i].Name + ' ' + (t.WagerType == 'T' && t.TeaserName ? t.TeaserName : t.WagerType == 'I' && t.ContinueOnPushFlag == 'Y' ? ' or Push' : '');

            }
        }
        return null;

        /*switch (wager.WagerType) {
            case "S":
                return "Spread";
            case "M":
                return "Money Line";
            case "E":
                return "Team Totals";
            case "L":
                return "Total Points";
            case "P":
                return "Parlay";
            case "I":
                return "If Bet";
            case "T":
                return "Teaser";
            case "C":
                return "Future/Prop";
        }
        return "";*/
    };

    agentService.KillTheSession = function () {
        return caller.POST({}, 'Logout').then(function () {
            CommonFunctions.RedirecToPage(SETTINGS.LoginSite);
        });
    };

    agentService.DisplayPeriod = function (periodRange, strDate) {
        var customFormmattedDate = "";
        var datePart;
        var monthPart;
        var dayOfWeekPart;
        var firstDate;
        var secondDate;
        switch (periodRange) {
            case "D":
                if (strDate.indexOf(" ") < 0) return "";
                datePart = strDate.split(" ")[0];
                dayOfWeekPart = strDate.split(" ")[1];
                customFormmattedDate = dayOfWeekPart.substring(0, 3) + " " + datePart.split("/")[1] + " " + agentService.GetMonthName(datePart.split("/")[0] - 1);
                //"10/04/2016 Tuesday"
                break;
            case "W":
                if (strDate.indexOf("-") < 0) return "";
                firstDate = strDate.split("-")[0];
                secondDate = strDate.split("-")[1];
                if (secondDate != null) {
                    //customFormmattedDate = agentService.GetDayOfWeekName(firstDate);
                    customFormmattedDate += " " +
                        firstDate.split("/")[1] + " " + agentService.GetMonthName(firstDate.split("/")[0] - 1) + " to ";
                    //customFormmattedDate += agentService.GetDayOfWeekName(secondDate);
                    customFormmattedDate += " " +
                        +secondDate.split("/")[1] + " " + agentService.GetMonthName(secondDate.split("/")[0] - 1);
                }
                //"05/09/2016 - 05/15/2016"
                break;
            case "M":
                if (strDate.indexOf(" ") < 0) return "";
                datePart = strDate.split(" ")[0];
                monthPart = strDate.split(" ")[1];
                customFormmattedDate = datePart + " " + monthPart.substring(0, 3);
                //"2015 October"
                break;
            case "Y":
                customFormmattedDate = strDate;
                break;
        }
        return customFormmattedDate;
    };

    agentService.GetMonthName = function (idx) {
        return CommonFunctions.GetMonthName(idx);
    };

    agentService.GetDayOfWeekName = function (date) {
        return CommonFunctions.GetDayOfWeekName(date);
    };

    agentService.GetWeekDayName = function (idx) {
        var weekday = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
        return String(weekday[idx]).toUpperCase();
    };

    agentService.GetPerformanceTotal = function (performanceObj) {
        var total = 0;
        if (!performanceObj)
            return 0;
        for (var i = 0; i < performanceObj.length; i++) {
            var ap = performanceObj[i];
            if (ap.LostSelected) total += ap.Lost;
            if (ap.WonSelected) total += ap.Won;
        }
        return total;
    };

    agentService.GetMonthNameFromDate = function (startingDate, endingDate) {
        var month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        var retMonth = month[new Date().getMonth()];
        if (startingDate) {
            startingDate = new Date(parseInt(startingDate.substr(6)));
            endingDate = new Date(parseInt(endingDate.substr(6)));
            retMonth = month[startingDate.getMonth()].toUpperCase();
            if (startingDate.getMonth() != endingDate.getMonth()) {
                retMonth += " / " + month[endingDate.getMonth()].toUpperCase();
            }
        }
        return retMonth.toUpperCase();
    };

    agentService.GetDayFigure = function (custDf, dayIdx, breakoutSportsAndCasinos) {
        /*
             Sunday is 0, Monday is 1
         */
        var dayFigure = 0;
        if (custDf == null)
            return dayFigure;
        switch (dayIdx) {
            case 0:
                dayFigure = (custDf.AmountWonSunday - custDf.AmountLostSunday);
                custDf.df0 = (dayFigure + (custDf.AmountWonCasinoSunday - custDf.AmountLostCasinoSunday)) * 100;
                custDf.NoData0 = custDf.PlaysCountMonday == 0;
                if (!breakoutSportsAndCasinos) {
                    dayFigure += (custDf.AmountWonCasinoSunday - custDf.AmountLostCasinoSunday);
                    custDf.NoData0 = custDf.AmountWonCasinoSunday == 0 && custDf.AmountLostCasinoSunday == 0;
                }
                custDf.NoData0 = custDf.NoData0 && dayFigure == 0;
                custDf.dFData0 = dayFigure * 100;
                break;
            case 1:
                dayFigure = (custDf.AmountWonMonday - custDf.AmountLostMonday);
                custDf.df1 = (dayFigure + (custDf.AmountWonCasinoMonday - custDf.AmountLostCasinoMonday)) * 100;
                custDf.NoData1 = custDf.PlaysCountMonday == 0;
                if (!breakoutSportsAndCasinos) {
                    dayFigure += (custDf.AmountWonCasinoMonday - custDf.AmountLostCasinoMonday);
                    custDf.NoData1 = custDf.AmountWonCasinoMonday == 0 && custDf.AmountLostCasinoMonday == 0;
                }
                custDf.NoData1 = custDf.NoData1 && dayFigure == 0;
                custDf.dFData1 = dayFigure * 100;
                break;
            case 2:
                dayFigure = (custDf.AmountWonTuesday - custDf.AmountLostTuesday);
                custDf.df2 = (dayFigure + (custDf.AmountWonCasinoTuesday - custDf.AmountLostCasinoTuesday)) * 100;
                custDf.NoData2 = custDf.PlaysCountMonday == 0;
                if (!breakoutSportsAndCasinos) {
                    dayFigure += (custDf.AmountWonCasinoTuesday - custDf.AmountLostCasinoTuesday);
                    custDf.NoData2 = custDf.AmountWonCasinoTuesday == 0 && custDf.AmountLostCasinoTuesday == 0;
                }
                custDf.NoData2 = custDf.NoData2 && dayFigure == 0;
                custDf.dFData2 = dayFigure * 100;
                break;
            case 3:
                dayFigure = (custDf.AmountWonWednesday - custDf.AmountLostWednesday);
                custDf.df3 = (dayFigure + (custDf.AmountWonCasinoWednesday - custDf.AmountLostCasinoWednesday)) * 100;
                custDf.NoData3 = custDf.PlaysCountMonday == 0;
                if (!breakoutSportsAndCasinos) {
                    dayFigure += (custDf.AmountWonCasinoWednesday - custDf.AmountLostCasinoWednesday);
                    custDf.NoData3 = custDf.AmountWonCasinoWednesday == 0 && custDf.AmountLostCasinoWednesday == 0;
                }
                custDf.NoData3 = custDf.NoData3 && dayFigure == 0;
                custDf.dFData3 = dayFigure * 100;
                break;
            case 4:
                dayFigure = (custDf.AmountWonThursday - custDf.AmountLostThursday);
                custDf.df4 = (dayFigure + (custDf.AmountWonCasinoThursday - custDf.AmountLostCasinoThursday)) * 100;
                custDf.NoData4 = custDf.PlaysCountMonday == 0;
                if (!breakoutSportsAndCasinos) {
                    dayFigure += (custDf.AmountWonCasinoThursday - custDf.AmountLostCasinoThursday);
                    custDf.NoData4 = custDf.AmountWonCasinoThursday == 0 && custDf.AmountLostCasinoThursday == 0;
                }
                custDf.NoData4 = custDf.NoData4 && dayFigure == 0;
                custDf.dFData4 = dayFigure * 100;
                break;
            case 5:
                dayFigure = (custDf.AmountWonFriday - custDf.AmountLostFriday);
                custDf.df5 = (dayFigure + (custDf.AmountWonCasinoFriday - custDf.AmountLostCasinoFriday)) * 100;
                custDf.NoData5 = custDf.PlaysCountMonday == 0;
                if (!breakoutSportsAndCasinos) {
                    dayFigure += (custDf.AmountWonCasinoFriday - custDf.AmountLostCasinoFriday);
                    custDf.NoData5 = custDf.AmountWonCasinoFriday == 0 && custDf.AmountLostCasinoFriday == 0;
                }
                custDf.NoData5 = custDf.NoData5 && dayFigure == 0;
                custDf.dFData5 = dayFigure * 100;
                break;
            case 6:
                dayFigure = (custDf.AmountWonSaturday - custDf.AmountLostSaturday);
                custDf.df6 = (dayFigure + (custDf.AmountWonCasinoSaturday - custDf.AmountLostCasinoSaturday)) * 100;
                custDf.NoData6 = custDf.PlaysCountMonday == 0;
                if (!breakoutSportsAndCasinos) {
                    dayFigure += (custDf.AmountWonCasinoSaturday - custDf.AmountLostCasinoSaturday);
                    custDf.NoData6 = custDf.AmountWonCasinoSaturday == 0 && custDf.AmountLostCasinoSaturday == 0;
                }
                custDf.NoData6 = custDf.NoData6 && dayFigure == 0;
                custDf.dFData6 = dayFigure * 100;
                break;
        }
        return dayFigure * 100; //CommonFunctions.FormatNumber(dayFigure, divideFig, true);
    };

    agentService.GetCasinoFigure = function (custDf, dayIdx) {
        var casinoDayFigure = 0;
        if (custDf == null)
            return casinoDayFigure;
        switch (dayIdx) {
            case 0:
                casinoDayFigure = (custDf.AmountWonCasinoSunday - custDf.AmountLostCasinoSunday);
                //custDf.NoData0 = custDf.AmountWonCasinoSunday == 0 && custDf.AmountLostCasinoSunday == 0;
                break;
            case 1:
                casinoDayFigure = (custDf.AmountWonCasinoMonday - custDf.AmountLostCasinoMonday);
                //custDf.NoData1 = custDf.AmountWonCasinoMonday == 0 && custDf.AmountLostCasinoMonday == 0;
                break;
            case 2:
                casinoDayFigure = (custDf.AmountWonCasinoTuesday - custDf.AmountLostCasinoTuesday);
                //custDf.NoData2 = custDf.AmountWonCasinoTuesday == 0 && custDf.AmountLostCasinoTuesday == 0;
                break;
            case 3:
                casinoDayFigure = (custDf.AmountWonCasinoWednesday - custDf.AmountLostCasinoWednesday);
                //custDf.NoData3 = custDf.AmountWonCasinoWednesday == 0 && custDf.AmountLostCasinoWednesday == 0;
                break;
            case 4:
                casinoDayFigure = (custDf.AmountWonCasinoThursday - custDf.AmountLostCasinoThursday);
                //custDf.NoData4 = custDf.AmountWonCasinoThursday == 0 && custDf.AmountLostCasinoThursday == 0;
                break;
            case 5:
                casinoDayFigure = (custDf.AmountWonCasinoFriday - custDf.AmountLostCasinoFriday);
                //custDf.NoData5 = custDf.AmountWonCasinoFriday == 0 && custDf.AmountLostCasinoFriday == 0;
                break;
            case 6:
                casinoDayFigure = (custDf.AmountWonCasinoSaturday - custDf.AmountLostCasinoSaturday);
                //custDf.NoData6 = custDf.AmountWonCasinoSaturday == 0 && custDf.AmountLostCasinoSaturday == 0;
                break;
        }
        return casinoDayFigure * 100; //CommonFunctions.FormatNumber(casinoDayFigure, divideFig, true);
    };

    agentService.GetDailyFigureDate = function (dayIdx, startingDate) {
        if (startingDate != null) {
            var date = new Date(parseInt(startingDate.substr(6)));
            var dateB = new Date(parseInt(startingDate.substr(6)));
            switch (dayIdx) {
                case 0:
                    return new Date(dateB.setDate(dateB.getDate() + 6)).getMonth() + 1 + '/' + new Date(date.setDate(date.getDate() + 6)).getDate();
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                    return new Date(dateB.setDate(dateB.getDate() + (dayIdx))).getMonth() + 1 + '/' + new Date(date.setDate(date.getDate() + (dayIdx - 1))).getDate();
            }
            return '';
        }
        return '';
    };

    function includeWager(wager, wagerTypeCode, wagerAmountCode) {

        var wagerType = wager.WagerType;
        var wagerAmount = wager.AmountWagered;

        var wagerTypes;
        if (wagerTypeCode == 1)
            wagerTypes = 'GSLME';
        else
            wagerTypes = 'GSLMEPTIC';

        if (wagerTypes.indexOf(wagerType) < 0) {
            return false;
        }

        switch (wagerAmountCode) {
            case 0:
                return true;
            case 1:
                if (wagerAmount / 100 > 0 && wagerAmount / 100 <= 1000)
                    return true;
                else
                    return false;
            case 2:
                if (wagerAmount / 100 > 1000 && wagerAmount / 100 <= 9999.99)
                    return true;
                else
                    return false;
        }
        return true;
    };

    function excludeWager(wagerInfoItem, deletedWagers) {
        if (deletedWagers == null || deletedWagers.length == 0)
            return false;
        for (var i = 0; i < deletedWagers.length; i++) {
            var ticketNumber = deletedWagers[i].TicketNumber;
            var wagerNumber = deletedWagers[i].WagerNumber;
            if (parseInt(ticketNumber) == parseInt(wagerInfoItem.TicketNumber) && parseInt(wagerNumber) == parseInt(wagerInfoItem.WagerNumber))
                return true;
        }
        return false;
    };

    agentService.GroupCustomersByAgent = function (rawData) {
        var returnedData = [];
        var holdAgentId = null;
        if (rawData) {
            for (var i = 0; i < rawData.length; i++) {
                for (var j = 0; j < rawData[i].length; j++) {
                    var agData = rawData[i][j];
                    if (holdAgentId != agData.AgentId) {
                        var CustomersData = new Array();
                        var AgentData = { AgentId: agData.AgentId };
                        agData.WageringActiveSwitch = agData.WageringActive ? true : false;
                        CustomersData.push(agData);
                        AgentData.CustomersList = CustomersData;
                        returnedData.push(AgentData);
                    }
                    else {
                        agData.WageringActiveSwitch = agData.WageringActive ? true : false;
                        returnedData[returnedData.length - 1].CustomersList.push(agData);
                    }
                    holdAgentId = agData.AgentId;
                }
            }
        }
        return returnedData;
    };

    return agentService;

}
]);