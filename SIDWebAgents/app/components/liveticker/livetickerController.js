﻿appModule.controller("livetickerController", [
    '$rootScope', '$scope', '$agentService', function ($rootScope, $scope, $agentService) {

      var REFRESHEVERY = 300000; // 5 min

      $scope.ReportFilters = {
        WagerType: {},
        WagerAmount: {},
      };

      $scope.WagerTypes = [];
      $scope.WagerAmounts = [];

      $scope.Init = function () {
        $scope.InitializeFilters();
        $scope.GetLiveTickerInfo();
        if (!$rootScope.IsMobile)
          setTimeout(function () {
            jQuery('table.tablesaw').fixedHeader({
              topOffset: 65
            });
          }, 1200);
        //$scope.DoCleanup();
      };

      $scope.DoCleanup = function () {
        setTimeout(function () {
          $scope.CleanOldWagers();
          $scope.DoCleanup();
        }, REFRESHEVERY);
      };


      $scope.$on('NewWagersFound', function () {
        if (!$scope.LiveTickerInfo) return;
        var newWagersCount = $agentService.LiveTickerInfo.length - $scope.LiveTickerInfo.length;
        $scope.LiveTickerInfo = $agentService.LiveTickerInfo;
        $rootScope.safeApply();
        for (var i = 0; i < newWagersCount; i++) {
          var id = $scope.LiveTickerInfo[i].TicketNumber + "_" + $scope.LiveTickerInfo[i].WagerNumber + "_liveTicket";
          document.getElementById(id).classList.add('log-new');
          setDelay(id);
        }
      });

      var setDelay = function (id) {
        setTimeout(function () {
          document.getElementById(id).classList.remove('log-new');
        }, 15000);
      };

      $scope.GetWagerItemDescription = function (fullWagerDescription, wagerItemIndex) {
        var descArray = fullWagerDescription.split('\r\n');
        if (descArray.length > 1 && descArray[wagerItemIndex])
          return descArray[wagerItemIndex].replace("Credit Adjustment", '').replace("Debit Adjustment", '');
        else return fullWagerDescription.replace("Credit Adjustment", '').replace("Debit Adjustment", '');
      };

      $scope.InitializeFilters = function () {
        var wagerTypeObj = { Index: 0, WagerType: "All Bet Types" };
        $scope.WagerTypes.push(wagerTypeObj);
        wagerTypeObj = { Index: 1, WagerType: "Straight Bets Only" };
        $scope.WagerTypes.push(wagerTypeObj);
        $scope.ReportFilters.WagerType = $scope.WagerTypes[0];

        var wagerAmountsObj = { Index: 0, WagerAmount: "Show All Wager Amounts" };
        $scope.WagerAmounts.push(wagerAmountsObj);
        wagerAmountsObj = { Index: 1, WagerAmount: "$0 - $1,000" };
        $scope.WagerAmounts.push(wagerAmountsObj);
        wagerAmountsObj = { Index: 2, WagerAmount: "$1,001 - $10,000" };
        $scope.WagerAmounts.push(wagerAmountsObj);
        wagerAmountsObj = { Index: 3, WagerAmount: "$10,000 and up" };
        $scope.WagerAmounts.push(wagerAmountsObj);
        $scope.ReportFilters.WagerAmount = $scope.WagerAmounts[0];
      };

      $scope.GetLiveTickerInfo = function () {
        $agentService.LiveTickerFilters.WagerType = $scope.ReportFilters.WagerType;
        $agentService.LiveTickerFilters.WagerAmount = $scope.ReportFilters.WagerAmount;
        if ($scope.ReportFilters.WagerType && $scope.ReportFilters.WagerType.Index >= 0 && $scope.ReportFilters.WagerAmount && $scope.ReportFilters.WagerAmount.Index >= 0) {
          $agentService.GetLiveTickerInfo($scope.ReportFilters.WagerType.Index, $scope.ReportFilters.WagerAmount.Index).then(function () {
            $scope.LiveTickerInfo = $agentService.LiveTickerInfo;
          });

        }
      };

      $scope.DisplayWagerTypeName = function (wager) {
        var twriter = wager.EnteredBy || wager.TicketWriter;
        if (twriter) {
            if (twriter.indexOf("COLCHIAN") != -1) {
                $scope.isXTransaction = true;
                return $scope.Translate("Horses");
            }
            if (twriter.indexOf("GSLIVE") != -1) {
                $scope.isXTransaction = true;
                return $scope.Translate("Live Betting");
            }
            if (twriter.indexOf("System") != -1) {
                $scope.isXTransaction = true;
                return $scope.Translate("Casino");
            }
        }
        return $agentService.DisplayWagerTypeName(wager);
      };

      $scope.CleanOldWagers = function () {
        if (!$scope.LiveTickerInfo || $scope.LiveTickerInfo.length == 0) {
          return;
        }
        var tNow = new Date();
        var tmpLiveTickerInfo = [];
        for (var i = 0; i < $scope.LiveTickerInfo.length; i++) {
          if (Math.abs(tNow.getTime() - $scope.LiveTickerInfo[i].ShowedDateTime.getTime()) < REFRESHEVERY) {
            tmpLiveTickerInfo.push($scope.LiveTickerInfo[i]);
          }
        }
        $scope.LiveTickerInfo = null;
        $scope.LiveTickerInfo = tmpLiveTickerInfo;
        $rootScope.safeApply();
      };

      $scope.Init();
    }
]);