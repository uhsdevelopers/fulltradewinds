﻿appModule.controller("dashboardController", [
    '$rootScope', '$scope', '$agentService', function ($rootScope, $scope, $agentService) {
        window.morrisObj = null;
        var amountWagered = 0;
        var WagerType = '';
        $scope.Week = {
            val: "0"
        };
        $scope.CurrentBalance = 0;
        $scope.Init = function () {
            setAgentPermissions();
            $scope.GetCustomersList();
            getDailyFigureDatesRange();
            document.getElementById("body_bg").classList.add('dashboard');
            document.getElementById("containerFluid").classList.remove('container-fluid', 'xyz');
            getLiveTickerInfo();
            getAgentPerformance();
            getHeaderInfo();
            getAgentWinLossByWeek(0);
      GetCustomersDailyFigures();
        };

        function setAgentPermissions() {
            if (!$agentService.AgentInfo)
                return;
            $scope.AgentInfo = $agentService.AgentInfo;
        }

        function getDailyFigureDatesRange() {
            $agentService.GetDailyFigureDatesRange().then(function (result) {
                $scope.DatesRange = result.data.d.Data;
                $scope.dailyFigureDate = $scope.DatesRange[0];
                formatDatesRangeForDisplay();
            });
        }

        function formatDatesRangeForDisplay() {
            if ($scope.DatesRange !== null) {
                for (var i = 0; i < $scope.DatesRange.length; i++) {
                    $scope.DatesRange[i].DayOfWeekStr = $scope.DisplayDate($scope.DatesRange[i].Date);
                }
            }
        };

    function GetCustomersDailyFigures() {
      $scope.AgentAsCustomerInfo = $agentService.AgentAsCustomerInfo;
      $agentService.GetCustomersDailyFigures(0).then(function (result) {
        var groupedData = $scope.GroupDailyFiguresByAgent(result.data.d.Data);
        var totalPlayers = 0;
        if (groupedData)
          groupedData.forEach(function (e) {
            e.CustomerDailyFigures.forEach(function (c) {
              if (c.PlaysCount > 0) {
                totalPlayers += 1;
              }
            });
          });
        $scope.TotalPlayers = totalPlayers;
      });
    };

        $scope.GetCustomersList = function () {
            if ($scope.ActionCustomers == null) $scope.GetUngroupedCustomers();
        };

        $scope.GetCustomerBalance = function (customerId) {
            $agentService.GetCustomerBalance(customerId).then(function (result) {
                $scope.CustomerBalance = result.data.d.Data;
            });
        };

        $scope.Filters = $scope.Filters || {};

        $scope.Filters.BetLog = function (LiveTickerInfo) {
            return function (lT) {
                if (lT.AmountWagered > amountWagered && (WagerType === 'All' || $scope.GetWagerType(lT, 0).indexOf(WagerType) >= 0)) return true;
                else return false;
            };
        };

        $scope.SetMaxBet = function (value) {
            var options = $('*[id^="betOption_"]');
            for (var i = 0; i < options.length; i++) {
                options[i].classList.remove('selected');
            };
            document.getElementById('betOption_' + value).classList.add('selected');
            amountWagered = value;
        };

        $scope.GetWagerType = function (wager, wagersCount) {
            var twriter = wager.EnteredBy || wager.TicketWriter;
            if (twriter) {
                switch (twriter) {
                    case "System":
                        $scope.isXTransaction = true;
                        return "Casino";
                    case "LIVE":
                        $scope.isXTransaction = true;
                        return "Live Betting";
                    default:
                        if (twriter.indexOf("COLCHIAN") != -1) {
                            $scope.isXTransaction = true;
                            return "Horses";
                        }
                }
            }
            switch (wager.WagerType) {
                case 'S':
                    return 'Spread';
                case 'M':
                    return 'Money Line';
                case 'E':
                    return 'Team Total';
                case 'L':
                    return 'Total';
                case 'P':
                    return wagersCount + ' Team Parlay';
                case 'T':
                    return 'Teaser';
                case 'I':
                    return wagersCount + ' If bet';
                case 'C':
                    return 'Future/Prop';
                case 'G':
                    return 'HORSES';
                case 'A':
                    return 'Maunal play';
                default:
                    return wager.WagerType;
            }
        };

        $scope.UpdateCustomerBalance = function (customerId, currentBalance) {
            $scope.ActionCustomers.forEach(function (e) {
                if (e.CustomerId == customerId) {
                    e.CurrentBalance = currentBalance;
                    $scope.GetCustomerBalance(e.CustomerId);
                    return;
                }
            });
        };

        $scope.UpdateCustomer = function (customer) {
            $scope.ActionCustomers.forEach(function (e) {
                if (e.CustomerId == customer.CustomerId) {
                    e = customer;
                    return;
                }
            });
        };


        $scope.ShowWagerType = function (wagerType) {
            return true;//wagerType === 'P' || wagerType === 'T' || wagerType === 'I' || wagerType === 'C' || wagerType === 'G';
        };

        $scope.SetWagerType = function (wagerType) {
            var options = $('*[id^="wagerOption_"]');
            for (var i = 0; i < options.length; i++) {
                options[i].classList.remove('selected');
            };
            document.getElementById('wagerOption_' + wagerType).classList.add('selected');
            WagerType = wagerType.replace('_', ' ');
        };

        $scope.GetPeriod = function (period) {
            switch (period) {
                case "0":
                    return $scope.Translate('This week');
                case "1":
                    return $scope.Translate('Last week');
                case "2":
                    return $scope.Translate('Last 2 weeks');
            }
            return null;
        };

        $scope.$on('NewWagersFound', function () {
            if (!$scope.LiveTickerInfo) return;
            var newWagers = $agentService.LiveTickerInfo.length - $scope.LiveTickerInfo.length;
            $scope.LiveTickerInfo = $agentService.LiveTickerInfo;
            $scope.TotalPlayers = $agentService.TotalPlayers;
            $rootScope.safeApply();
            for (var i = 0; i < newWagers; i++) {
                var id = $scope.LiveTickerInfo[i].TicketNumber + "_" + $scope.LiveTickerInfo[i].WagerNumber + "_liveTicket";
                document.getElementById(id).classList.add('log-new');
                setDelay(i);
            }
        });

        var setDelay = function (i) {
            setTimeout(function () {
                document.getElementById(i + "_liveTicket").classList.remove('log-new');
            }, 6000);
        };

        var getLiveTickerInfo = function () {
            $agentService.GetLiveTickerInfo(0, 0).then(function () {
                $scope.LiveTickerInfo = $agentService.LiveTickerInfo;
                $rootScope.safeApply();
            });
        };

        var getAgentPerformance = function () {
            $agentService.GetAgentPerformance('D', '', null).then(function (result) {
                $scope.AgentPerformance = result.data.d.Data;
            });
        };

        var getAgentWinLossByWeek = function (weekNum) {
            $agentService.GetWinLossByWeek(weekNum).then(function (result) {
                var winLoss = result.data.d.Data;
                var week = [];
                if (winLoss == null || winLoss.length == 0) {
                    week = [
                        { period: $scope.Translate('Monday'), winLoss: 0, casinoNet: 0 },
                        { period: $scope.Translate('Tuesday'), winLoss: 0, casinoNet: 0 },
                        { period: $scope.Translate('Wednesday'), winLoss: 0, casinoNet: 0 },
                        { period: $scope.Translate('Thursday'), winLoss: 0, casinoNet: 0 },
                        { period: $scope.Translate('Friday'), winLoss: 0, casinoNet: 0 },
                        { period: $scope.Translate('Saturday'), winLoss: 0, casinoNet: 0 },
                        { period: $scope.Translate('Sunday'), winLoss: 0, casinoNet: 0 }
                    ];
                } else {
                    week = [
                        { period: CommonFunctions.FormatDateTime(winLoss[0].DailyFigureDate, 4), winLoss: 0, casinoNet: 0 },
                        { period: winLoss.length > 1 ? CommonFunctions.FormatDateTime(winLoss[1].DailyFigureDate, 4) : $scope.Translate('Tuesday'), winLoss: 0, casinoNet: 0 },
                        { period: winLoss.length > 2 ? CommonFunctions.FormatDateTime(winLoss[2].DailyFigureDate, 4) : $scope.Translate('Wednesday'), winLoss: 0, casinoNet: 0 },
                        { period: winLoss.length > 3 ? CommonFunctions.FormatDateTime(winLoss[3].DailyFigureDate, 4) : $scope.Translate('Thurday'), winLoss: 0, casinoNet: 0 },
                        { period: winLoss.length > 4 ? CommonFunctions.FormatDateTime(winLoss[4].DailyFigureDate, 4) : $scope.Translate('Friday'), winLoss: 0, casinoNet: 0 },
                        { period: winLoss.length > 5 ? CommonFunctions.FormatDateTime(winLoss[5].DailyFigureDate, 4) : $scope.Translate('Saturday'), winLoss: 0, casinoNet: 0 },
                        { period: winLoss.length > 6 ? CommonFunctions.FormatDateTime(winLoss[6].DailyFigureDate, 4) : $scope.Translate('Sunday'), winLoss: 0, casinoNet: 0 }
                    ];
                    for (var i = 0; i < winLoss.length; i++) {
                        week[i].winLoss = CommonFunctions.RoundNumber(winLoss[i].AmountWon - winLoss[i].AmountLost);
                        week[i].casinoNet = CommonFunctions.RoundNumber(winLoss[i].CasinoAmountWon - winLoss[i].CasinoAmountLost);
                    }
                }
                if (morrisObj != null) {
                    morrisObj.destroy();
                }
                morrisObj = NaN;
                morrisObj = new Morris.Bar({
                    element: 'morris-bar-chart',
                    resize: true,
                    data: week,
                    parseTime: false,
                    barGap: 0.5,
                    xkey: 'period',
                    ykeys: ['winLoss', 'casinoNet'],
                    labels: [$scope.Translate('Win Loss'), $scope.Translate('Casino Net')],
                    barColors: ['#337ab7', '#99D199']
                });

            });
        };

        $scope.UpdateChart = function () {
            getAgentWinLossByWeek($scope.Week.val);
        };

        var getHeaderInfo = function () {
            $agentService.GetCustDailyFigureByAgentDashboard(0).then(function (result) {
                var df = result.data.d.Data;
                $scope.ThisWeekTotal = weekCalculation(df).weekTotal;
                $scope.VolumeAmount = weekCalculation(df).volume;
            });
            $agentService.GetCustDailyFigureByAgentDashboard(1).then(function (result) {
                var df = result.data.d.Data;
                $scope.LastWeekTotal = weekCalculation(df).weekTotal;;
            });
            $rootScope.safeApply();
        };

        var weekCalculation = function (df) {
            var calculation = {
                weekTotal: 0,
                volume: 0
            };
            var amountWon = 0;
            var amountLost = 0;
            df.forEach(function (e) {
                amountLost += e.AmountLost + e.CasinoAmountLost;
                amountWon += e.AmountWon + e.CasinoAmountWon;
                calculation.volume += e.VolumeAmount;
            });
            calculation.weekTotal = amountWon - amountLost;
            return calculation;
        };

        /*var getTotalPlayers = function () {
            if (!$scope.LiveTickerInfo || $scope.LiveTickerInfo.length == 0) return;
            var totalPlayers = 0;
            var holdCustomer = $scope.LiveTickerInfo[0].CustomerId;
            $scope.LiveTickerInfo.forEach(function (i) {
                if (holdCustomer !== i.CustomerId) {
                    totalPlayers++;
                }
                holdCustomer = i.CustomerId;
                if (i === $scope.LiveTickerInfo.length - 1) {
                    totalPlayers++;
                }
            });
            $scope.TotalPlayers = totalPlayers;
        };*/

        $scope.DisplayPeriod = function (period) {
            return $agentService.DisplayPeriod('D', period);
        };

        $scope.Init();

        $scope.$on('$destroy', function () {
            document.getElementById("body_bg").classList.remove('dashboard');
            document.getElementById("containerFluid").classList.add('container-fluid', 'xyz');

        });
    }
]);