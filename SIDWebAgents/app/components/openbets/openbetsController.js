﻿appModule.controller("openbetsController", [
  '$scope', '$agentService', '$window', '$rootScope', function ($scope, $agentService, $window, $rootScope) {

      $scope.totalDisplayed = 3;

      var hashData = /customerID=([^&]+)/.exec(window.location.hash);
      if (hashData) var customerHash = hashData[1];

      $scope.CustomersList = [{ CustomerID: '-All-', CustomerName: $scope.Translate('All customers') }];

      $scope.ReportFilters = {
          CustomerId: {},
          WagerType: "",
          ContinueOnPush: false,
          CheckForAR: false,
          SportType: ""
      };

      $scope.Init = function () {
          $scope.NoProps = false;
          $scope.GetSportTypesList();
          $scope.GetCustomerOpenBets();

      };

      $scope.PrintReport = function () {

          if ($scope.GroupedCustomerBets) $scope.totalDisplayed = $scope.GroupedCustomerBets.length;
          $scope.$on('ngRepeatFinished', function (ngRepeatFinishedEvent) {
              window.print();
          });
      };

      $scope.WriteWinner = function (openBetItem) {


          var team1Score = "";
          var team2Score = "";
          var winner = "";
          if (openBetItem.Team1Score != null)
              team1Score = openBetItem.Team1Score;
          if (openBetItem.Team2Score != null)
              team2Score = openBetItem.Team2Score;
          if (team1Score != team2Score) {
              var winnerId = "";
              if (openBetItem.WinnerID != null)
                  winnerId = openBetItem.WinnerID;
              winner += winnerId + " " + $scope.Translate("WON PERIOD BY") + " " + Math.abs(team1Score - team2Score);
          }

          return winner;
      };


      $scope.WriteTeamsScores = function (openBetItem) {

          if (isNaN(openBetItem.Team1Score) || isNaN(openBetItem.Team2Score)) return $scope.Translate("Pending");

          var teamsResults = "";
          var team1Id = "";
          var team2Id = "";
          var team1Score = "";
          var team2Score = "";
          if (!openBetItem) return "";
          if (openBetItem.Team1ID != null)
              team1Id = openBetItem.Team1ID;
          if (openBetItem.Team2ID != null)
              team2Id = openBetItem.Team2ID;
          if (openBetItem.Team1Score != null)
              team1Score = openBetItem.Team1Score;
          if (openBetItem.Team2Score != null)
              team2Score = openBetItem.Team2Score;
          teamsResults += team1Id + " - " + team1Score + " / " + team2Id + " - " + team2Score;
          if (openBetItem.WagerType == "E" || openBetItem.WagerType == "L" || openBetItem.WagerType == "T") {
              teamsResults += " " + $scope.Translate("TOTAL_POINTS") + ": " + (team1Score + team2Score);
          }
          return teamsResults;
      };

      $scope.ParlayText = function (totalPicks, totalItems) {
          var pendingBets = totalPicks - totalItems;
          return pendingBets > 0 ? ' (' + totalPicks + ' ' + 'Teams, ' + pendingBets + ' Open)' : ' (' + totalPicks + ' ' + 'Teams)';
      };


      $scope.GetSportTypesList = function () {
          $agentService.GetSportTypesList().then(function (result) {
              $scope.SportsList = $scope.GetDistinctSports(result.data.d.Data);
              $scope.ReportFilters.SportType = $scope.SportsList[0];
          });
      };

      $scope.GetDistinctSports = function (list) {
          var unique = {};
          var distinctSports = [];
          distinctSports.push({ SportType: "All", SportName: $scope.Translate("All sports") });
          for (var i in list) {
              if (typeof (unique[list[i].SportType]) == "undefined") {
                  var sportType = { SportType: list[i].SportType.trim(), SportName: $scope.Translate(list[i].SportType.trim()) };
                  distinctSports.push(sportType);
              }
              unique[list[i].SportType] = "";
          }

          return distinctSports;
      };

      $scope.GetGroupedCustomerBets = function (list) {

          var groupedCustomerBets = [];
          var holdCustomer = list[0].CustomerID;
          $scope.ReportFilters.CustomerId = $scope.CustomersList[0];
          var groupedList = new Array();
          var groupedCustomerInfo = {};
          list.forEach(function (i, index) {
              if (holdCustomer != i.CustomerID) {
                  $scope.CustomersList.push({ CustomerID: holdCustomer, CustomerName: holdCustomer });
                  if (customerHash == holdCustomer.trim()) $scope.ReportFilters.CustomerId = $scope.CustomersList[$scope.CustomersList.length - 1];
                  groupedCustomerInfo = { CustomerID: holdCustomer, ObjectList: grouperObjs(groupedList) };
                  groupedCustomerBets.push(groupedCustomerInfo);
                  groupedList = new Array();
                  groupedList.push(i);
              } else {
                  groupedList.push(i);
              }
              holdCustomer = i.CustomerID;
              if (index == list.length - 1) {
                  groupedCustomerInfo = { CustomerID: holdCustomer, ObjectList: grouperObjs(groupedList) };
                  groupedCustomerBets.push(groupedCustomerInfo);
              }
          });
          return groupedCustomerBets;
      };

      $scope.ResetSportType = function () {
          if (jQuery('#scrollBox').hasScrollBar()) $scope.totalDisplayed = 15;;
          if ($scope.ReportFilters.WagerType == 'C') $scope.ReportFilters.SportType = $scope.SportsList[0];
      };

      var grouperObjs = function (allOpenBets) {

          var allOpenBets = allOpenBets;
          var groupedOpenBets = new Array();
          if (allOpenBets != null && allOpenBets.length > 0) {

              groupedOpenBets = new Array();
              groupedItems = new Array();
              holdTicketNumber = allOpenBets[0].TicketNumber;
              holdWagerNumber = allOpenBets[0].WagerNumber;
              for (var i = 0; i < allOpenBets.length; i++) {
                  if (holdTicketNumber != allOpenBets[i].TicketNumber || holdWagerNumber != allOpenBets[i].WagerNumber) {
                      groupedOpenBets.push(allOpenBets[i - 1]);
                      if (allOpenBets[i - 1].WagerType == 'A') {
                          groupedOpenBets[groupedOpenBets.length - 1].Items = CreateManualItems(allOpenBets[i - 1].Description);
                      }
                      else {
                          groupedOpenBets[groupedOpenBets.length - 1].Items = CreateWagerItems(allOpenBets[i - 1].Description);
                      }
                  }
                  holdTicketNumber = allOpenBets[i].TicketNumber;
                  holdWagerNumber = allOpenBets[i].WagerNumber;

                  if (i == allOpenBets.length - 1) {
            allOpenBets[i].Items = CreateWagerItems(allOpenBets[i].Description);
                      groupedOpenBets.push(allOpenBets[i]);
                  }
              }
          }
          return groupedOpenBets;
      };

      $scope.GetWagerItemDescription = function (fullWagerDescription, wagerItemIndex) {
          if (!fullWagerDescription) return;
          var descArray = fullWagerDescription.split('\r\n');
          if (descArray.length > 0 && descArray[wagerItemIndex])
              return descArray[wagerItemIndex].replace("Credit Adjustment", '').replace("Debit Adjustment", '');
          else return fullWagerDescription.replace("Credit Adjustment", '').replace("Debit Adjustment", '');
      };

      $scope.GetCustomerOpenBets = function () {
          if ($scope.NoProps) {
              $scope.ReportFilters.WagerType = "C";
              $scope.ReportFilters.SportType = $scope.SportsList[0];
          } else {
              $scope.ReportFilters.WagerType = "";
          }
          $scope.NoProps = !$scope.NoProps;
          var wagerType = $scope.CheckForWagerType();
          var customerId = $scope.GetCustomerID();
          var sportType = $scope.GetSportType();
          $agentService.GetCustomerOpenBets(customerId, wagerType, true, true, sportType, $scope.NoProps).then(function (result) {
              if (result.data.d.Data && result.data.d.Data.length > 0)
                  $scope.GroupedCustomerBets = $scope.GetGroupedCustomerBets(result.data.d.Data);
              else $scope.GroupedCustomerBets = [];
          });
      };

      $scope.GetSportType = function () {
          if ($scope.ReportFilters.SportType.SportType == null) {
              return "";
          } else if ($scope.ReportFilters.SportType.SportType == "All") {
              return "";
          }
          else
              return $scope.ReportFilters.SportType.SportType;
      };

      $scope.GetCustomerID = function () {
          if ($scope.ReportFilters.CustomerId.CustomerID == null) {
              return "";
          } else if ($scope.ReportFilters.CustomerId.CustomerID == "-All-") {
              return "";
          }
          else
              return $scope.ReportFilters.CustomerId.CustomerID;
      };

      $scope.CheckForWagerType = function () {
          var wagerType = "";
          if ($scope.ReportFilters.WagerType != null) {
              switch ($scope.ReportFilters.WagerType) {
                  case "S":
                  case "P":
                  case "T":
                  case "C":
                  case "I":
                      wagerType = $scope.ReportFilters.WagerType;
                      $scope.ReportFilters.CheckForAR = false;
                      $scope.ReportFilters.ContinueOnPush = false;
                      break;
                  case "R":
                      wagerType = "I";
                      $scope.ReportFilters.CheckForAR = true;
                      $scope.ReportFilters.ContinueOnPush = false;
                      break;
                  case "IWP":
                      wagerType = "I";
                      $scope.ReportFilters.ContinueOnPush = true;
                      $scope.ReportFilters.CheckForAR = false;
                      break;
              }
          }
          return wagerType;
      };

      $scope.FormatTicketNumberDisplay = function (wi) {
          return wi.TicketNumber + "-" + wi.WagerNumber;
      };

      $scope.ShowingOpenBetsFor = function (customerId) {
          return $scope.Translate("Open Bets for ") + customerId.replace('TW', '');
          /*if ($scope.ReportFilters.CustomerId) {
              str = "Open Bets For ";
              if ($scope.ReportFilters.CustomerId.CustomerID == "All") {
                  str += " Everyone"
              }
              else
                  str += $scope.ReportFilters.CustomerId.CustomerID;
          }
          return str;
          */
      };

      $scope.Filters = $scope.Filters || {};

      $scope.Filters.Customer = function (GroupedCustomerBets) {
          return function (gC) {
              if ($scope.ReportFilters.CustomerId.CustomerID == "-All-" || $scope.ReportFilters.CustomerId.CustomerID.trim() == gC.CustomerID.trim()) return true;
              else return false;
          };
      };

      $scope.Filters.Tickets = function (ObjectList) {
          return function (oB) {
              if (($scope.ReportFilters.WagerType == "" || ($scope.ReportFilters.WagerType == 'R' && oB.ARLink == 1 || (oB.ARLink == 0 && $scope.ReportFilters.WagerType.indexOf(oB.WagerType.trim()) >= 0)))
                && ($scope.ReportFilters.SportType.SportType == "All" || oB.Description.indexOf($scope.ReportFilters.SportType.SportType) >= 0
                  || (oB.SportType != null ? oB.SportType.trim() == $scope.ReportFilters.SportType.SportType : false))) return true;
              else return false;
          };
      };

      $scope.DisplayWagerTypeName = function (wager) {
          var twriter = wager.EnteredBy || wager.TicketWriter;
          if (twriter) {
              if (twriter.indexOf("COLCHIAN") != -1) {
                  $scope.isXTransaction = true;
                  return $scope.Translate("Horses");
              }
              if (twriter.indexOf("GSLIVE") != -1) {
                  $scope.isXTransaction = true;
                  return $scope.Translate("Live Betting");
              }
              if (twriter.indexOf("System") != -1) {
                  $scope.isXTransaction = true;
                  return $scope.Translate("Casino");
              }
          }
          return $agentService.DisplayWagerTypeName(wager);
      };

      $scope.LoadMore = function () {
          $scope.totalDisplayed = +5;
          $rootScope.safeApply();

      };

      $scope.Init();

      angular.element('#scrollBox').bind("scroll", function () {
          var dash = document.getElementsByClassName('dashbwrapper')[0];
          var windowHeight = "offsetHeight" in document.getElementsByClassName('dashbwrapper')[0] ? document.getElementsByClassName('dashbwrapper')[0].offsetHeight : document.documentElement.offsetHeight;
          var body = document.getElementById('scrollBox'), html = dash;
          var docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
          windowBottom = windowHeight + body.scrollTop;
          if (windowBottom >= docHeight) {
              $scope.totalDisplayed += 3;
              $rootScope.safeApply();
          }
      });


      function CreateManualItems(fullDescription) {
          var arr = fullDescription.split("|");
          var items = [];
          for (var i = 0; i < arr.length; i++) {
              items.push({ Description: arr[i] });
          }
          return items;
      }

      function CreateWagerItems(fullDescription) {
          var arr = fullDescription.split('\r\n');
          var items = [];
          for (var i = 0; i < arr.length; i++) {
              items.push({ Description: arr[i] });
          }
          return items;
      }

      var _newWagerItem = function (wager) {
          return {
              Comments: (wager.Comments == null || wager.Comments == '' ? null : wager.Comments),
              ContinueOnPushFlag: wager.ContinueOnPushFlag,
              Description: wager.Description,
              FreePlayFlag: wager.FreePlayFlag,
              SportType: wager.SportType,
              PeriodDescription: wager.PeriodDescription,
              Team1Score: wager.Team1Score,
              Team2Score: wager.Team2Score,
              Team1ID: wager.Team1ID,
              Team2ID: wager.Team2ID,
              Outcome: (wager.OutCome == "W" ? 'WON' : wager.OutCome == "L" ? 'LOST' : wager.OutCome == "X" ? 'PUSH' : ''),
              Result: (wager.OutCome == "W" || wager.TranType == "W" ? 'WON' : wager.OutCome == "L" || wager.TranType == "L" ? 'LOST' : wager.OutCome == "X" ? 'PUSH' : ''),
              WagerType: wager.WagerType,
              WinnerID: wager.WinnerID,
              EventDateTime: wager.EventDateTime
          };
      };

  }

]);


changeIcon = function (el) {
    var st;
    if (el.className.indexOf('icon_viewresults_minus') != -1) {
        st = el.className.toString().replace("icon_viewresults_minus", "");
        el.className = "icon_viewresults " + st;
    }
    else {
        st = el.className.toString().replace("icon_viewresults", "");
        el.className = "icon_viewresults_minus " + st;
    }
};