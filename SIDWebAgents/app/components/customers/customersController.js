﻿appModule.controller("customersController", [
    '$scope', '$agentService', '$rootScope', function ($scope, $agentService, $rootScope) {

      $scope.ReportFilters = {
        CustomerStatus: '',
        WeekNumber: {}
      };
        
    

      $scope.Init = function () {
        $scope.LoadingReportData = false;
        $scope.AgentInfo = $agentService.AgentInfo;
        $scope.WeeksRange = $agentService.GetWeeksRange();
        $scope.ReportFilters.WeekNumber = $scope.WeeksRange[0];
        $scope.GetCustomersList();
        $agentService.GetActivePlayersCount().then(function (result) {
          $scope.Players = result.data.d.Data;
        });
      };

      $scope.WagerLimit = function (customer) {
        var expTempDate = new Date(customer.TempWagerLimitExpirationString);
        var today = new Date();
        if (expTempDate > today) {
          return customer.TempWagerLimit;
        }
        return customer.WagerLimit;
      };
     
      $scope.GetCustomersList = function () {
        $scope.LoadingReportData = true;
          $rootScope.AgentsList = $agentService.AgentsList;
          $scope.LoadingReportData = false;
      $(".tablesaw-bar").remove();
          setTimeout(function () {
            jQuery('[data-toggle="tooltip"]').tooltip();
            jQuery('[data-toggle="popover"]').popover({ trigger: "hover" });
            if (!$rootScope.IsMobile)
              jQuery('table.tablesaw').fixedHeader({
                topOffset: 65
              });
          }, 500);
      };

    $scope.OnFilterChanged = function () {
      $scope.LoadingReportData = true;
      setTimeout(function () {
        $scope.LoadingReportData = false;
        $(".tablesaw-bar").remove();
      }, 10);
    };

        $scope.RemoveIdString = function(stringValue, stringToRemove) {
            var newString = stringValue.replace(stringToRemove, '');
            return newString;
        };

        $scope.ReportFilter = function (customersList) {
        return function (cL) {
          switch ($scope.ReportFilters.CustomerStatus) {
            case "X":
              return true;
            case "A":
              return cL.WageringActiveSwitch === true;
            case "I":
              return cL.WageringActiveSwitch === false;
          }
          return false;
        };
      };
      $scope.UpdateCustomerAccess = function (customerId, e, element, parentIndex, object) {
        if ($scope.ReportFilters.CustomerStatus != 'X' || $scope.SuspendWagering == false) {
        for (var i = 0; i < $scope.AgentsList[parentIndex].CustomersList.length; i++) {
          var el = $scope.AgentsList[parentIndex].CustomersList[i];
            if (el.CustomerId === customerId) {
              el[object] = !el[object];
              break;
            }
          }
          if ($scope.SuspendWagering == false) return;
          jQuery.noConflict();
          (function ($) {
            $('#ModalCust_req').modal('toggle');
            $('#ModalCust_req').modal('show');
          })(jQuery);
          return;
        }
        if (object === "WageringActive") {
        for (var i = 0; i < $scope.AgentsList[parentIndex].CustomersList.length; i++) {
          var el = $scope.AgentsList[parentIndex].CustomersList[i];
            if (el.CustomerId === customerId) {
              el.WageringActiveSwitch = el[object];
              break;
            }
          }
        }
        var accessValue;
        if (element)
          accessValue = "Y";
        else
          accessValue = "N";
        var code = e.target.id.split("_")[0];
        $agentService.UpdateCustomerAccess(customerId, code, accessValue);
      };

      $scope.Status = 'disable';
      $scope.UpdateAllCustomersAccess = function (GetPreviusState) {
        if ($scope.ReportFilters.CustomerStatus != 'X' && !$scope.SuspendWagering) return;
        var code = iconId;
        var index = 0;
        customersList.forEach(function (e) {
          var element = document.getElementById(code + '_' + index);
          if (!GetPreviusState) {
            e.PreviusState = element.checked ? true : false;
            element.checked = $scope.Status === "disable" ? false : true;
          }
          else element.checked = e.PreviusState;
          index++;
          //$agentService.UpdateCustomerAccess(e.CustomerId, code, $scope.Status === 'enable' ? 'Y' : 'N');
        });
        $scope.Status = $scope.Status === "disable" ? "enable" : "disable";
      };

      var iconId = '';
      var customersList = [];
      $scope.SetIcon = function (icon, name, customers) {
        iconId = icon;
        customersList = customers;
        $scope.RestricionName = name;
      };
      $scope.SuspendWagering = $agentService.AgentInfo ? $agentService.AgentInfo.SuspendWageringFlag == 'Y' : false;
      $rootScope.$on('AgentAsCutomerInfoLoaded', function () { $scope.SuspendWagering = $agentService.AgentInfo.SuspendWageringFlag == 'Y'; });

      $scope.Init();

      $scope.$on('$destroy', function () {
        document.getElementById("page-content-wrapper").classList.remove('no-printable');

      });
    }
]);
