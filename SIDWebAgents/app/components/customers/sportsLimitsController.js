﻿
appModule.controller("sportsLimitsController", [
    '$scope', '$agentService', '$rootScope', function ($scope, $agentService, $rootScope) {

      var sportLimitsToUpdate = [];

      $scope.Init = function () {
          $scope.LoadingReportData = false;
      setTimeout(function () {
              $scope.Selection = {
                  Customer: $scope.ActionCustomers[0]
              };
              $scope.GetSportsLimits($scope.ActionCustomers[0].CustomerId);
      }, 500);
      };

      $scope.ChangeLimit = function (sportLimit) {
        var founded = false;
        sportLimit.Changed = true;
        sportLimitsToUpdate.forEach(function (e) {
          if (e.sportSubType == sportLimit.sportSubType) {
            e = sportLimit;
            founded = true;
            return;
          }
        });
        if (!founded) sportLimitsToUpdate.push(sportLimit);
      };

      $scope.MoveToTop = function (subSport) {
        if ($rootScope.IsMobile)
          jQuery('#div_sport').animate({
            scrollTop: jQuery("#sportRow_" + subSport).offset().top
          }, 'fast');
      };

      $scope.GetSportsLimits = function (customerId) {
        $scope.LoadingReportData = true;
        $agentService.GetDetailWagerLimits(customerId, $scope.SportType, $scope.PeriodNumber).then(function (result) {
          $scope.SportsLimits = result.data.d.Data;
          $scope.LoadingReportData = false;
        $(".tablesaw-bar").remove();
          setTimeout(function () {
            if (!$rootScope.IsMobile)
              jQuery('table.tablesaw').fixedHeader({
                topOffset: 65
              });
          }, 1000);
        });
      };

      $scope.UpdateCustomerSportLimits = function () {
        if (!$scope.Selection.Customer) return;
        sportLimitsToUpdate.forEach(function (e) {
          e.Changed = false;
          $agentService.UpdateCustomerSportsLimits(e.newCuSpreadLimit ? e.newCuSpreadLimit == 0 ? null : e.newCuSpreadLimit * 100 : e.cuSpreadLimit,
              e.newInetSpreadLimit ? e.newInetSpreadLimit == 0 ? null : e.newInetSpreadLimit * 100 : e.inetSpreadLimit,
              e.newCuMoneyLineLimit ? e.newCuMoneyLineLimit == 0 ? null : e.newCuMoneyLineLimit * 100 : e.cuMoneyLineLimit,
              e.newInetMoneyLineLimit ? e.newInetMoneyLineLimit == 0 ? null : e.newInetMoneyLineLimit * 100 : e.inetMoneyLineLimit,
              e.newCuTotalPointsLimit ? e.newCuTotalPointsLimit == 0 ? null : e.newCuTotalPointsLimit * 100 : e.cuTotalPointsLimit,
              e.newInetTotalPointsLimit ? e.newInetTotalPointsLimit == 0 ? null : e.newInetTotalPointsLimit * 100 : e.inetTotalPointsLimit,
              $scope.PeriodNumber, $scope.SportType, e.sportSubType, $scope.Selection.Customer.CustomerId).then(function (result) {
              });
        });
        UI.Alert("Sport limit had been updated");
        sportLimitsToUpdate = [];
      };

      $scope.Readkey = function (e) {
        if (event.keyCode == 13 || event.which == 13) {
          $scope.UpdateCustomerSportLimits();
        }
      };

      $scope.Init();

    }
]);