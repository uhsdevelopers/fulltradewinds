﻿appModule.controller("dailyFiguresController", [
  '$scope', '$agentService', '$routeParams', '$location', '$rootScope', function ($scope, $agentService, $routeParams, $location, $rootScope) {

    var agentsCurrentBalance = [];

    $scope.AgentAsCustomerInfo = $agentService.AgentAsCustomerInfo;
    $scope.AgentsToShow = [{ AgentId: 'All Agents' }];

    $rootScope.$on('AgentAsCutomerInfoLoaded', function () {
      $scope.AgentAsCustomerInfo = $agentService.AgentAsCustomerInfo;
    });


    $scope.ReportFilters = {
      WeekNumber: {},
      BreakoutSportsAndCasinos: false,
      ShowCashTrans: false
    };

    $scope.Report = {
      CasinoTransactions: [],
      CashTransactions: [],
      DailyFigures: null,
      OpenBets: null,
      AllTransactions: null,
      TransactionsByDate: null
    };

    $scope.Init = function () {
      $scope.SettleFigureMode = false;
      $scope.LoadingReportData = false;
      if ($routeParams.ShowSettleFigure != null) {
        $scope.SettleFigureMode = $routeParams.ShowSettleFigure;
      }
      $scope.WeeksRange = $agentService.GetWeeksRange();
      $scope.CustomersToShow = $scope.BuildCustomerTypesToShowObject();
      $scope.ReportFilters.WeekNumber = $scope.WeeksRange[0];
      $scope.CustomersDailyFiguresAry = new Array();
      if ($scope.SettleFigureMode) {
        $scope.WeeksRange.shift();
        $scope.ReportFilters.WeekNumber = $scope.WeeksRange[0];
        $scope.ReportFilters.CustomersToShow = $scope.CustomersToShow[2];
        $scope.GetCustomersDailyFigures();
        $scope.GetCustomersList();
      } else {
        $agentService.GetAgentsCurrentBalance().then(function (result) {
          agentsCurrentBalance = result.data.d.Data;
          $scope.GetCustomersDailyFigures();
          $scope.GetCustomersList();
        });
        $scope.ReportFilters.CustomersToShow = $scope.CustomersToShow[1];
      }
    };

    var getAgentsCurrentBalance = function (agent) {
      if (typeof agentsCurrentBalance === "undefined") return 0;
      var cb = 0;
      agentsCurrentBalance.forEach(function (ag) {
        if (ag.CustomerID.trim() == agent) {
          cb = ag.CurrentBalance;
          return;
        }
      });
      return cb;
    };

    $scope.GetBankTotals = function () {
      if (!agentsCurrentBalance || typeof agentsCurrentBalance === "undefined") return 0;
      var cbt = 0;
      agentsCurrentBalance.forEach(function (ag) {
        cbt += ag.CurrentBalance;
      });
      return cbt;

    };

    $scope.GetCustomersList = function () {
      $agentService.GetCustomersList($scope.ReportFilters.WeekNumber.Index).then(function (result) {
        $rootScope.AgentsList = $agentService.AgentsList;
      });
    };

    $scope.BuildCustomerTypesToShowObject = function () {
      var returnedList = new Array();
      var item = { Code: 'A', Description: $scope.Translate('All Players') };
      returnedList.push(item);
      item = { Code: 'C', Description: $scope.Translate('Active Players Only') };
      returnedList.push(item);
      if ($scope.SettleFigureMode) {
        item = { Code: 'S', Description: $scope.Translate('Players With Settle Only') };
        returnedList.push(item);
      }

      return returnedList;
    };

    $scope.GetTotalsByAgent = function () {
      $scope.LoadingReportData = true;
      setTimeout(function () {
        $scope.LoadingReportData = false;
        $(".tablesaw-bar").remove();
        $rootScope.safeApply();
      }, 10);
    };

    $scope.GetCustomersDailyFigures = function () {
      $scope.LoadingReportData = true;
      if ($scope.AddToList($scope.ReportFilters.WeekNumber.Index)) {
        $agentService.GetCustomersDailyFigures($scope.ReportFilters.WeekNumber.Index).then(function (result) {
          if (!$rootScope.IsMobile) {
            setTimeout(function () {
              jQuery('table.agents').fixedHeader({
                topOffset: 65
              });
            }, 500);
          }
          var groupedData = $scope.GroupDailyFiguresByAgent(result.data.d.Data);
          var customersDailyFiguresObj = { Data: groupedData, WeekNumber: $scope.ReportFilters.WeekNumber.Index };
          $scope.CustomersDailyFiguresAry.push(customersDailyFiguresObj);
          $scope.CustomersDailyFigures = groupedData;
          if (groupedData && groupedData.length > 0) {
            $scope.StartingDate = groupedData[0].StartingDate;
            $scope.EndingDate = groupedData[0].EndingDate;
          }
          $scope.GetTotals();
          $scope.GetTotalsForPreviousWeek();
          if (!$scope.SettleFigureMode)
            for (var i = 0; i < $scope.CustomersDailyFigures.length; i++) {
              $scope.CustomersDailyFigures[i].CurrentBalance = getAgentsCurrentBalance($scope.CustomersDailyFigures[i].AgentId);
              var found = false;
              for (var j = 0; j < agentsCurrentBalance.length; j++) {
                if (agentsCurrentBalance[j].CustomerID.trim() == $scope.CustomersDailyFigures[i].AgentId) {
                  found = true;
                  break;
                }
              };
              if (j < agentsCurrentBalance.length && !found && agentsCurrentBalance[j].CurrentBalance > 0)
                $scope.CustomersDailyFigures.push(new { AgentId: agentsCurrentBalance[j].CustomerID.trim() });
            };

          $scope.CustomersDailyFigures.forEach(function (e) {
            var agent = {
              AgentId: e.AgentId
            }
            $scope.AgentsToShow.push(agent);
          });
          $scope.ReportFilters.AgentToShow = $scope.AgentsToShow[0];
          $scope.LoadingReportData = false;
        });
      }
      else {
        $scope.CustomersDailyFigures = $scope.AssignProperObject($scope.ReportFilters.WeekNumber.Index);
        $scope.StartingDate = $scope.CustomersDailyFigures[0].StartingDate;
        $scope.EndingDate = $scope.CustomersDailyFigures[0].EndingDate;
        $scope.GetTotals();
        $scope.GetTotalsForPreviousWeek();
        $scope.LoadingReportData = false;
      }

    };

    $scope.GroupDailyFiguresByAgent = function (rawData) {
      var returnedData = [];
      var holdAgentId = null;
      if (rawData) {
        for (var i = 0; i < rawData.length; i++) {
          if (holdAgentId != rawData[i].AgentId) {
            //var CustomersData = new Array();
            var AgentData = { AgentId: rawData[i].AgentId, StartingDate: rawData[i].StartingDate, EndingDate: rawData[i].EndingDate, CustomerDailyFigures: rawData[i].CustomerDailyFigures };
            //CustomersData.push(rawData[i]);
            //AgentData.CustomerDailyFigures = CustomersData;
            returnedData.push(AgentData);
          }
          else {
            returnedData[returnedData.length - 1].CustomerDailyFigures.push(rawData[i]);
          }
          holdAgentId = rawData[i].AgentId;
        }
      }
      return returnedData;
    };

    $scope.AssignProperObject = function (weekNumIdx) {
      var data = null;
      if ($scope.CustomersDailyFiguresAry != null) {
        for (var i = 0; i < $scope.CustomersDailyFiguresAry.length; i++) {
          if (parseInt($scope.CustomersDailyFiguresAry[i].WeekNumber) == parseInt(weekNumIdx)) {
            data = $scope.CustomersDailyFiguresAry[i].Data;
            break;
          }
        }
      }
      return data;
    };

    $scope.AddToList = function (weekNumIdx) {
      if ($scope.CustomersDailyFiguresAry.length == 0)
        return true;
      for (var i = 0; i < $scope.CustomersDailyFiguresAry.length; i++) {
        if (parseInt($scope.CustomersDailyFiguresAry[i].WeekNumber) == parseInt(weekNumIdx))
          return false;
      }
      return true;
    };

    $scope.GetDate = function (dayIdx) {
      return $agentService.GetDailyFigureDate(dayIdx, $scope.StartingDate);
    };

    $scope.GetMonth = function () {
      return $agentService.GetMonthNameFromDate($scope.StartingDate, $scope.EndingDate);
    };

    $scope.GetWeekDayName = function (idx) {
      return $agentService.GetWeekDayName(idx);
    };

    $scope.GetDayFigure = function (custDf, dayIdx) {

      return $agentService.GetDayFigure(custDf, dayIdx, $scope.ReportFilters.BreakoutSportsAndCasinos);
    };

    $scope.GetCasinoFigure = function (custDf, dayIdx) {
      return $agentService.GetCasinoFigure(custDf, dayIdx);
    };

    $scope.GetWeekFigure = function (custDf) {
      if (custDf == null)
        return 0;
      custDf.weekFigure = (custDf.AmountWonSunday - custDf.AmountLostSunday);
      if (!$scope.ReportFilters.BreakoutSportsAndCasinos) {
        custDf.weekFigure += (custDf.AmountWonCasinoSunday - custDf.AmountLostCasinoSunday);
      }
      custDf.weekFigure += (custDf.AmountWonMonday - custDf.AmountLostMonday);
      if (!$scope.ReportFilters.BreakoutSportsAndCasinos) {
        custDf.weekFigure += (custDf.AmountWonCasinoMonday - custDf.AmountLostCasinoMonday);
      }
      custDf.weekFigure += (custDf.AmountWonTuesday - custDf.AmountLostTuesday);
      if (!$scope.ReportFilters.BreakoutSportsAndCasinos) {
        custDf.weekFigure += (custDf.AmountWonCasinoTuesday - custDf.AmountLostCasinoTuesday);
      }
      custDf.weekFigure += (custDf.AmountWonWednesday - custDf.AmountLostWednesday);
      if (!$scope.ReportFilters.BreakoutSportsAndCasinos) {
        custDf.weekFigure += (custDf.AmountWonCasinoWednesday - custDf.AmountLostCasinoWednesday);
      }
      custDf.weekFigure += (custDf.AmountWonThursday - custDf.AmountLostThursday);
      if (!$scope.ReportFilters.BreakoutSportsAndCasinos) {
        custDf.weekFigure += (custDf.AmountWonCasinoThursday - custDf.AmountLostCasinoThursday);
      }
      custDf.weekFigure += (custDf.AmountWonFriday - custDf.AmountLostFriday);
      if (!$scope.ReportFilters.BreakoutSportsAndCasinos) {
        custDf.weekFigure += (custDf.AmountWonCasinoFriday - custDf.AmountLostCasinoFriday);
      }
      custDf.weekFigure += (custDf.AmountWonSaturday - custDf.AmountLostSaturday);
      if (!$scope.ReportFilters.BreakoutSportsAndCasinos) {
        custDf.weekFigure += (custDf.AmountWonCasinoSaturday - custDf.AmountLostCasinoSaturday);
      }
      custDf.weekFigure *= 100;
      return custDf.weekFigure;
    };

    $scope.GetCasinoWeekFigure = function (custDf) {
      var casinoWeekFigure = 0;
      if (custDf == null)
        return casinoWeekFigure;
      casinoWeekFigure = (custDf.AmountWonCasinoSunday - custDf.AmountLostCasinoSunday) +
        (custDf.AmountWonCasinoMonday - custDf.AmountLostCasinoMonday) +
        (custDf.AmountWonCasinoTuesday - custDf.AmountLostCasinoTuesday) +
        (custDf.AmountWonCasinoWednesday - custDf.AmountLostCasinoWednesday) +
        (custDf.AmountWonCasinoThursday - custDf.AmountLostCasinoThursday) +
        (custDf.AmountWonCasinoFriday - custDf.AmountLostCasinoFriday) +
        (custDf.AmountWonCasinoSaturday - custDf.AmountLostCasinoSaturday);
      return casinoWeekFigure * 100;
    };

    $scope.GetOverallFigure = function (custDf, divideFig) {
      return custDf.CurrentBalance;
      /*var weekFigure = 0;
      if (custDf == null)
          return weekFigure;
      weekFigure = $scope.GetWeekFigure(custDf, divideFig);

      return custDf.PrevBal + weekFigure;*/
    };

    $scope.InitializeTotals = function () {
      $scope.ReportTotals = {
        PrevBalance: 0,
        Monday: 0,
        Tuesday: 0,
        Wednesday: 0,
        Thursday: 0,
        Friday: 0,
        Saturday: 0,
        Sunday: 0,
        Week: 0,
        OverAllFigure: 0,
        PlaysCount: 0,
        LastWeek: 0,
        OverAllFigureLastWeek: 0
      };
    };

    $scope.GetTotalsForPreviousWeek = function () {
      if ($scope.CustomersDailyFiguresAry == null)
        return;
      var runningLastWeek = 0;
      var runningOverAllFigureLastWeek = 0;
      var rawWeek = 0;
      $scope.CustomersDailyFiguresPrevWeek = $scope.AssignProperObject($scope.ReportFilters.WeekNumber.Index + 1);
      if ($scope.CustomersDailyFiguresPrevWeek == null) {
        $agentService.GetCustomersDailyFigures($scope.ReportFilters.WeekNumber.Index + 1).then(function (result) {
          var groupedData = $scope.GroupDailyFiguresByAgent(result.data.d.Data);
          $scope.CustomersDailyFiguresObj = { Data: groupedData, WeekNumber: $scope.ReportFilters.WeekNumber.Index + 1 };
          $scope.CustomersDailyFiguresAry.push($scope.CustomersDailyFiguresObj);
          $scope.CustomersDailyFiguresPrevWeek = groupedData;
          for (var i = 0; i < $scope.CustomersDailyFiguresPrevWeek.length; i++) {
            for (var j = 0; j < $scope.CustomersDailyFiguresPrevWeek[i].CustomerDailyFigures.length; j++) {
              rawWeek = $scope.GetWeekFigure($scope.CustomersDailyFiguresPrevWeek[i].CustomerDailyFigures[j], false);
              runningLastWeek += rawWeek;
              runningOverAllFigureLastWeek += $scope.CustomersDailyFigures[i].CustomerDailyFigures[j].PrevBal + rawWeek; //$scope.GetOverallFigure($scope.CustomersDailyFiguresPrevWeek[i].CustomerDailyFigures[j], false);
            }
          }
          $scope.ReportTotals.LastWeek = runningLastWeek;
          $scope.ReportTotals.OverAllFigureLastWeek = runningOverAllFigureLastWeek;
        });
      }
      else {
        for (var i = 0; i < $scope.CustomersDailyFiguresPrevWeek.length; i++) {
          for (var j = 0; j < $scope.CustomersDailyFiguresPrevWeek[i].CustomerDailyFigures.length; j++) {
            rawWeek = $scope.GetWeekFigure($scope.CustomersDailyFiguresPrevWeek[i].CustomerDailyFigures[j], false);
            runningLastWeek += rawWeek;
            runningOverAllFigureLastWeek += $scope.CustomersDailyFigures[i].CustomerDailyFigures[j].PrevBal + rawWeek; //$scope.GetOverallFigure($scope.CustomersDailyFiguresPrevWeek[i].CustomerDailyFigures[j], false);
          }
        }
        $scope.ReportTotals.LastWeek = runningLastWeek;
        $scope.ReportTotals.OverAllFigureLastWeek = runningOverAllFigureLastWeek;
      }
    };

    $scope.GetTotals = function () {
      if ($scope.CustomersDailyFigures == null)
        return;
      $scope.InitializeTotals();
      for (var i = 0; i < $scope.CustomersDailyFigures.length; i++) {
        for (var j = 0; j < $scope.CustomersDailyFigures[i].CustomerDailyFigures.length; j++) {
          var rawWeek = $scope.GetWeekFigure($scope.CustomersDailyFigures[i].CustomerDailyFigures[j], false);
          $scope.ReportTotals.Week += rawWeek;
          $scope.ReportTotals.PrevBalance += $scope.CustomersDailyFigures[i].CustomerDailyFigures[j].PrevBal;
          $scope.ReportTotals.Sunday += $scope.GetDayFigure($scope.CustomersDailyFigures[i].CustomerDailyFigures[j], 0, false);
          $scope.ReportTotals.Monday += $scope.GetDayFigure($scope.CustomersDailyFigures[i].CustomerDailyFigures[j], 1, false);
          $scope.ReportTotals.Tuesday += $scope.GetDayFigure($scope.CustomersDailyFigures[i].CustomerDailyFigures[j], 2, false);
          $scope.ReportTotals.Wednesday += $scope.GetDayFigure($scope.CustomersDailyFigures[i].CustomerDailyFigures[j], 3, false);
          $scope.ReportTotals.Thursday += $scope.GetDayFigure($scope.CustomersDailyFigures[i].CustomerDailyFigures[j], 4, false);
          $scope.ReportTotals.Friday += $scope.GetDayFigure($scope.CustomersDailyFigures[i].CustomerDailyFigures[j], 5, false);
          $scope.ReportTotals.Saturday += $scope.GetDayFigure($scope.CustomersDailyFigures[i].CustomerDailyFigures[j], 6, false);
          $scope.ReportTotals.OverAllFigure += $scope.CustomersDailyFigures[i].CustomerDailyFigures[j].PrevBal + rawWeek;//$scope.GetOverallFigure($scope.CustomersDailyFigures[i].CustomerDailyFigures[j], false);
          $scope.ReportTotals.PlaysCount += $scope.CustomersDailyFigures[i].CustomerDailyFigures[j].PlaysCount;
        }
      }
      if (!$rootScope.IsMobile) {
        setTimeout(function () {
          jQuery('table.summary').fixedHeader({
            topOffset: 65
          });
        }, 200);
      }
    };

    $scope.GetOverall = function (df) {
      var x = 0;
      var overall = 0;
      while (x < 7) {
        var dfData = 'df' + x;
        overall += df[dfData];
        x++;
      }
      return df.overall = (overall + df.PrevBal);
    };

    $scope.GetAgentTotals = function (totalToGet, sortedFigure, dF) {
      if (sortedFigure == null) return 0;
      var returnedValue = 0.0;
      var i;
      switch (totalToGet) {
      case 'PrevBalance':
        for (i = 0; i < sortedFigure.length; i++) {
          returnedValue += sortedFigure[i].PrevBal - sortedFigure[i].OtherTran;
        }
        break;
      case 'Monday':
        for (i = 0; i < sortedFigure.length; i++) {
          returnedValue += sortedFigure[i].dFData1;
        }
        dF.Monday = returnedValue;
        break;
      case 'Tuesday':
        for (i = 0; i < sortedFigure.length; i++) {
          returnedValue += sortedFigure[i].dFData2;
        }
        dF.Tuesday = returnedValue;
        break;
      case 'Wednesday':
        for (i = 0; i < sortedFigure.length; i++) {
          returnedValue += sortedFigure[i].dFData3;
        }
        dF.Wednesday = returnedValue;
        break;
      case 'Thursday':
        for (i = 0; i < sortedFigure.length; i++) {
          returnedValue += sortedFigure[i].dFData4;
        }
        dF.Thursday = returnedValue;
        break;
      case 'Friday':
        for (i = 0; i < sortedFigure.length; i++) {
          returnedValue += sortedFigure[i].dFData5;
        }
        dF.Friday = returnedValue;
        break;
      case 'Saturday':
        for (i = 0; i < sortedFigure.length; i++) {
          returnedValue += sortedFigure[i].dFData6;
        }
        dF.Saturday = returnedValue;
        break;
      case 'Sunday':
        for (i = 0; i < sortedFigure.length; i++) {
          returnedValue += sortedFigure[i].dFData0;
        }
        dF.Sunday = returnedValue;
        break;
      case 'Week':
        for (i = 0; i < sortedFigure.length; i++) {
          returnedValue += sortedFigure[i].weekFigure;
        }
        dF.Week = returnedValue;
        break;
      case 'OverAllFigure':
        for (i = 0; i < sortedFigure.length; i++) {
          returnedValue += sortedFigure[i].overall;
        }
        break;
      case 'PlaysCount':
        for (i = 0; i < sortedFigure.length; i++) {
          returnedValue += sortedFigure[i].PlaysCount;
        }
        break;
      case 'Settle':
        for (i = 0; i < sortedFigure.length; i++) {
          returnedValue += Math.trunc(sortedFigure[i].SettleFigure);
        }
        break;
      }
      return returnedValue;
    };

    $scope.Filters = $scope.Filters || {};

    $scope.Filters.CustomersToShow = function () {
      return function (df) {
        if (!$scope.ReportFilters.CustomersToShow) return true;
        switch ($scope.ReportFilters.CustomersToShow.Code) {
        case 'A':
          return true;
        case 'C':
          return !(df.NoData0 && df.NoData1 && df.NoData2 && df.NoData3 && df.NoData4 && df.NoData5 && df.NoData6);
        case 'S':
          return (Math.trunc(df.SettleFigure) != 0);
        default:
          return false;
        }
      };
    };

    $scope.Filters.ActiveAgent = function () {
      return function (df) {
        return !(df.Monday == 0 && df.Tuesday == 0 && df.Wednesday == 0 && df.Thursday == 0 && df.Friday == 0 && df.Saturday == 0 && df.Sunday == 0 && ($scope.ReportFilters.WeekNumber.Index == 0 ? df.CurrentBalance == 0 : true));

      };
    };

    $scope.Filters.AgentToShow = function () {

      return function (df) {
        if ($scope.ReportFilters.AgentToShow.AgentId == 'All Agents') return true;
        else {
          if (df.AgentId == $scope.ReportFilters.AgentToShow.AgentId)
            setTimeout(function () {
              jQuery('#dailyFiguresDiv').animate({
                scrollTop: 9999
              }, 'fast');
              alreadyIn = false;
            }, 200);
          return df.AgentId == $scope.ReportFilters.AgentToShow.AgentId;
        }
      }
    }

    $scope.ShowRow = function (row) {
      if (!$scope.ReportFilters.CustomersToShow) return true;
      var showItem = false;
      if ($scope.ReportFilters.CustomersToShow.Code == 'A') {
        showItem = true;
      } else {
        if ($scope.ReportFilters.CustomersToShow.Code == 'C' && row.Active == 'Y') {
          showItem = true;
        } else {
          if ($scope.ReportFilters.CustomersToShow.Code == 'S' && row.SettleFigure > 0) {
            showItem = true;
          }
        }
      }
      return showItem;
    };

    //DailyFigure Details

    $scope.CloseDailyFiguresDetails = function () {
      document.getElementById('monday').className = ('collapse');
      $scope.Arrow = -1;
    };

    var OpenDailyFiguresDetails = function () {
      var l = document.getElementById('monday');
      if (l) l.className = ('collapse in');
    };

    $scope.ShowMoreDetails = function (dfRow, dayOfWeek, includeCasino) {
      OpenDailyFiguresDetails();
      $scope.date = addDays(parseJsonDate(dfRow.StartingDate), dayOfWeek);
      $scope.SelectedCustomer = dfRow;
      $agentService.GetCustomerTransactionListByDate(dfRow.CustomerId, $scope.date, includeCasino).then(function () {
        $scope.TransactionsByDate = $agentService.TransactionList;
        $agentService.GetCashTransactionsByDate(dfRow.CustomerId, $scope.date).then(function (result) {
          var cashTransactions = result.data.d.Data;
          if (cashTransactions != null && cashTransactions.length > 0) {
            $scope.TransactionsByDate.push.apply($scope.TransactionsByDate, cashTransactions[i]);
            for (var i = cashTransactions.length - 1; i >= 0; i--) {
              var inserted = false;
              for (var j = 0; j < $scope.TransactionsByDate.length; j++) {
                if (CommonFunctions.FormatDateTime($scope.TransactionsByDate[j].TranDateTimeString, 10) <
                  CommonFunctions.FormatDateTime(cashTransactions.TranDateTimeString, 10)) {
                  $scope.TransactionsByDate.splice(1, 0, cashTransactions[i]);
                  inserted = true;
                  break;
                }
              }
              if (!inserted) {
                $scope.TransactionsByDate.push(cashTransactions[i]);
              }
            }
          }
        });
        $location.hash('monday');
      });
    };

    $scope.ScoredPtsHeader = function (openBetItem) {
      var resultsTitle = "";
      if (!openBetItem) return "";

      if (openBetItem.SportType != null && openBetItem.SportType == "Soccer") {
        resultsTitle += "GOALS "; //"Goals ";
      } else {
        resultsTitle += "POINTS "; //"Points ";
      }

      var periodDescripion = "";
      if (openBetItem.PeriodDescription != null)
        periodDescripion = openBetItem.PeriodDescription;

      if (openBetItem.PeriodDescription != null) {
        resultsTitle += "Scored in " + periodDescripion; //" period:";
      }

      return resultsTitle;
    };

    $scope.WriteTeamsScores = function (openBetItem) {

      if (isNaN(openBetItem.Team1Score) || isNaN(openBetItem.Team2Score)) return "Pending";

      var teamsResults = "";
      var team1Id = "";
      var team2Id = "";
      var team1Score = "";
      var team2Score = "";
      if (!openBetItem) return "";
      if (openBetItem.Team1ID != null)
        team1Id = openBetItem.Team1ID;
      if (openBetItem.Team2ID != null)
        team2Id = openBetItem.Team2ID;
      if (openBetItem.Team1Score != null)
        team1Score = openBetItem.Team1Score;
      if (openBetItem.Team2Score != null)
        team2Score = openBetItem.Team2Score;
      teamsResults += team1Id + " - " + team1Score + " / " + team2Id + " - " + team2Score;
      if (openBetItem.WagerType == "E" || openBetItem.WagerType == "L" || openBetItem.WagerType == "T") {
        teamsResults += " " + "TOTAL POINTS: " + (team1Score + team2Score);
      }
      return teamsResults;
    };

    $scope.WriteWinner = function (openBetItem) {
      var team1Score = "";
      var team2Score = "";
      var winner = "";
      if (openBetItem.Team1Score != null)
        team1Score = openBetItem.Team1Score;
      if (openBetItem.Team2Score != null)
        team2Score = openBetItem.Team2Score;
      if (team1Score != team2Score) {
        var winnerId = "";
        if (openBetItem.WinnerID != null)
          winnerId = openBetItem.WinnerID;
        winner += winnerId + " Won Period By " + Math.abs(team1Score - team2Score);
      }
      return winner;
    };

    $scope.GetWagerItemDescription = function (fullWagerDescription, wagerItemIndex) {
      var descArray = fullWagerDescription.split('\r\n');
      if (descArray.length > 0 && descArray[wagerItemIndex])
        return descArray[wagerItemIndex].replace("Credit Adjustment", '').replace("Debit Adjustment", '');
      else return fullWagerDescription.replace("Credit Adjustment", '').replace("Debit Adjustment", '');
    };

    $scope.isXTransaction = false;
    $scope.WagerType = function (t) {
      var twriter = t.EnteredBy || t.TicketWriter;
      if (twriter) {
        if (twriter.indexOf("COLCHIAN") != -1) {
          $scope.isXTransaction = true;
          return $scope.Translate("Horses");
        }
        if (twriter.indexOf("GSLIVE") != -1) {
          $scope.isXTransaction = true;
          return $scope.Translate("Live Betting");
        }
        if (twriter.indexOf("System") != -1) {
          $scope.isXTransaction = true;
          return $scope.Translate("Casino");
        }
      }
      $scope.isXTransaction = false;
      return LineOffering.WagerTypes.GetByCode(t.WagerType, t.TeaserName, t.ContinueOnPushFlag);
    };

    $scope.TotalPlayers = function () {
      var totalPlayers = 0;
      if ($scope.CustomersDailyFigures)
        $scope.CustomersDailyFigures.forEach(function (e) {
          e.CustomerDailyFigures.forEach(function (c) {
            if (c.PlaysCount > 0) {
              totalPlayers += 1;
            }
          });
        });
      return totalPlayers;
    };
    $scope.Init();
  }
]);

function parseJsonDate(jsonDateString) {
  return new Date(parseInt(jsonDateString.replace('/Date(', '')));
}

function addDays(date, days) {
  var result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
}

function changeIcon(el) {
  var st;
  if (el.className.indexOf('icon_viewresults_minus') != -1) {
    st = el.className.toString().replace("icon_viewresults_minus", "");
    el.className = "icon_viewresults " + st;
  }
  else {
    st = el.className.toString().replace("icon_viewresults", "");
    el.className = "icon_viewresults_minus " + st;
  }
  return false;
}