﻿appModule.controller("cashtransactionsController", [
    '$scope', '$agentService', '$routeParams', function ($scope, $agentService, $routeParams) {

        $scope.ReportFilters = {
            WeekNumber: {}
        };

        $scope.Init = function () {
            $scope.WeeksRange = $agentService.GetWeeksRange();
            $scope.ReportFilters.WeekNumber = $scope.WeeksRange[0];
            $scope.AgentId = null;
            if ($routeParams.AgentIdArg != null) {
                $scope.AgentId = $routeParams.AgentIdArg;
            }
            $scope.GetAgentCashTransactions();
        };

        $scope.GetAgentCashTransactions = function () {
            $agentService.GetAgentCashTransactions($scope.ReportFilters.WeekNumber.Index, $scope.AgentId).then(function (result) {
                $scope.AgentTransactions = result.data.d.Data;
            });
        };

        $scope.CalculateTotalForCashTransactions = function (factor) {
            if (!$scope.AgentTransactions)
                return 0;
            var total = 0.0;

            for (var i = 0; i < $scope.AgentTransactions.length; i++) {
                if ($scope.AgentTransactions[i].TranCode == "C")
                    total -= $scope.AgentTransactions[i].Amount;
                else
                    total += $scope.AgentTransactions[i].Amount;
            }
            return $scope.FormatMyNumber(factor * total, true, false);
        };

        $scope.Init();
    }
]);