﻿appModule.controller("customerController", ['$scope', '$agentService', '$rootScope', function ($scope, $agentService, $rootScope) {

  $scope.ReportFilters = {
    PeriodRange: 'D',
    WagerType: '',
    WeekNumber: {},
    BreakoutSportsAndCasinos: false,
    ShowCashTrans: false
  };

  $scope.Report = {
    CasinoTransactions: [],
    CashTransactions: [],
    DailyFigures: null,
    OpenBets: null,
    AllTransactions: null,
    TransactionsByDate: null
  };

  $scope.isXTransaction = false;

  $scope.Translate = function (str) {
    return str;
  };

  $scope.Init = function () {
    $scope.custCurrencyCode = 'USD.';
    $scope.dailyFigureTemplate = "/app/components/customer/dailyFigures.html";
    $scope.openBetsTemplate = "/app/components/customer/openBets.html";
    $scope.performanceTemplate = "/app/components/customer/performance.html";
    $scope.allTransactionsTemplate = "/app/components/customer/allTransactions.html";

    $scope.Templates = { reports: { selected: "" } };

    $scope.GetCustomersList();
    $scope.SelectedTab = 0;
    $scope.SetCustomerOption($scope.SelectedTab);
    $scope.CustomerDailyFiguresAry = new Array();
    $scope.WeeksRange = $agentService.GetWeeksRange();
    $scope.ReportFilters.WeekNumber = $scope.WeeksRange[0];
    $scope.AgentInfo = $agentService.AgentInfo;
    getCustomerBalance($scope.SelectedCustomer.CustomerId);
  };

  $scope.DDBox = {
    TranType: 'ALL'
  };

  $scope.Filters = $scope.Filters || {};

  $scope.Filters.TranType = function () {
    return function (ts) {
      if ($scope.DDBox.TranType == 'ALL') return true;
      return $scope.DDBox.TranType.toString().indexOf(ts.TranType) >= 0;
    };
  };

  $scope.ActionsVisible = function () {
    return $scope.AgentInfo.EnterTransactionFlag == 'Y' || $scope.AgentInfo.ChangeCreditLimitFlag == 'Y'
        || $scope.AgentInfo.ChangeTempCreditFlag == 'Y' || $scope.AgentInfo.SuspendWageringFlag == 'Y'
        || $scope.AgentInfo.UpdateCommentsFlag == 'Y' || $scope.AgentInfo.AddNewAccountFlag == 'Y';
  };

  $scope.UpdateCustomerBalance = function (customerId, currentBalance) {
    $scope.SelectedCustomer.CurrentBalance = currentBalance;
    getCustomerBalance($scope.SelectedCustomer.CustomerId);
    return;
  };

  $scope.UpdateCustomer = function (customer) {
    $scope.SelectedCustomer = customer;
    return;
  };


  var getCustomerBalance = function (customerId) {
    $agentService.GetCustomerBalance(customerId).then(function (result) {
      $scope.CustomerBalance = result.data.d.Data;
    });
  };

  $scope.SetSelectedCustomer = function (customer) {
    $scope.SelectedCustomer = customer;
    $scope.SetCustomerOption($scope.SelectedTab);
  };

  $scope.GetCustomersList = function () {
    if (!$rootScope.AgentsList || $rootScope.AgentsList.length == 0) {
      $agentService.GetCustomersList($scope.ReportFilters.WeekNumber.Index).then(function () {
        $rootScope.AgentsList = $agentService.AgentsList;
      });
    }
    if ($scope.LookupCustomer.CustomerId) {
      $scope.SelectedCustomer = $scope.LookupCustomer;
      setTimeout(function () {
        if ($scope.CustomerOption == 3) {
          $scope.SetCustomerOption(3);
          eventFire(document.getElementById('tbOpenBets'), 'click');
        }
        var element = document.getElementById('a_' + $scope.LookupCustomer.CustomerId);
        if (element) element.scrollIntoView();
      }, 900);
    } else
      $scope.SelectedCustomer = $rootScope.AgentsList[0].CustomersList[0];


  };

  $scope.SetCustomerOption = function (option) {
    if (option == null) option = 0;
    $scope.SelectedTab = option;
    switch (option) {
      case 0:
        $scope.GetCustomerPerformance();
        break;
      case 1:
        break;
      case 2:
        $scope.ShowDailyFigures(0);
        break;
      case 3:
        $scope.ShowOpentBets();
        break;
      case 4:
        $scope.ShowAllTransactions();
        break;
    };
  };

  //Performance Section Begin

  $scope.GetCustomerPerformance = function (range, customerId) {
    if (range) $scope.ReportFilters.PeriodRange = range;
    if (customerId) {
      $scope.Customer = {
        Info: { CustomerID: customerId }
      };
    }
    $scope.Templates.reports.selected = "/app/components/customer/performance.html";
  };

  $scope.DisplayPeriod = function (period) {
    return $agentService.DisplayPeriod($scope.ReportFilters.PeriodRange, period);
  };

  $scope.AddToTotal = function (e, ap, winloss) {
    if (winloss == "W") ap.WonSelected = !ap.WonSelected;
    else ap.LostSelected = !ap.LostSelected;
    $scope.UserTotal = $agentService.GetPerformanceTotal($scope.Report.CustomerPerformance);
  };

  $scope.TitleCustomer = function () {
    if ($rootScope.IsMobile) return "Customer " + $scope.SelectedCustomer.CustomerId;
    else return "Customer";
  };

  //Performance Section End

  //Daily Figures Section Begin

  var isCalendarPick = false;
  var endDate = "";
  $scope.DailyWinLossList = [];
  $scope.DailyCasinoWinLossList = [];
  $scope.DailyCashInOutList = [];
  var DailyWinLoss = function (hasValue, value) {
    return {
      hasValue: hasValue,
      value: value
    };
  };
  var CasinoWinLoss = function (hasValue, value) {
    return {
      hasValue: hasValue,
      value: value
    };
  };
  var CashInOut = function (hasValue, value) {
    return {
      hasValue: hasValue,
      value: value
    };
  };
  var Balance = function (hasValue, value) {
    return {
      hasValue: hasValue,
      value: value
    };
  };

  function eventFire(el, etype) {
    if (el.fireEvent) {
      el.fireEvent('on' + etype);
    } else {
      var evObj = document.createEvent('Events');
      evObj.initEvent(etype, true, false);
      el.dispatchEvent(evObj);
    }
  }

  $scope.currentDate = new Date();
  $scope.Arrow = -1;
  $scope.EndingBalance = 0;
  $scope.DailyFiguresDate = CommonFunctions.FormatDateTime(new Date(), 4);

  $rootScope.$on('dailyFiguresLoaded', function () {
    $scope.DailyWinLossList = [];
    $scope.DailyCasinoWinLossList = [];
    $scope.DailyCashInOutList = [];
    $scope.DailyBalanceList = [];
    $scope.Report.DailyFigures = $agentService.DailyFigures;
    $scope.DailyFiguresDate = (isCalendarPick ? endDate :
        (new Date() != $scope.Report.DailyFigures.ValuesPerDay[0].ThisDate ? CommonFunctions.FormatDateTime($scope.Report.DailyFigures.ValuesPerDay[0].ThisDate, 4) : CommonFunctions.FormatDateTime(new Date(), 4)));
    showDailyWinLoss();
    showDailyCashInOut();
    showDailyCasinoWinLoss();
    showDailyBalance();
  });

  $rootScope.$on('transactionListByDateLoaded', function () {
    $scope.Report.TransactionsByDate = $agentService.TransactionList;
    if ($scope.setCategory == 'bal' && $scope.Report.TransactionsByDate) {
      showCasinoTransactions($scope.Arrow);
    }
    $rootScope.safeApply();
    return true;
  });

  $rootScope.$on('cashTransactionsLoaded', function () {
    $scope.Report.CashTransactions = $agentService.CashTransactionList;
    if ($scope.setCategory == 'bal' && $scope.Report.CashTransactions) {
      for (var i = 0; i < $scope.Report.CashTransactions.length; i++) {
        var inserted = false;
        for (var j = 0; j < $scope.Report.TransactionsByDate.length; j++) {
          if (CommonFunctions.FormatDateTime($scope.Report.TransactionsByDate[j].TranDateTime, 10) <
               CommonFunctions.FormatDateTime($scope.Report.CashTransactions[i].TranDateTime, 10)) {
            $scope.Report.TransactionsByDate.splice(j, 0, $scope.Report.CashTransactions[i]);
            inserted = true;
            break;
          }
        }
        if (!inserted) {
          $scope.Report.TransactionsByDate.push($scope.Report.CashTransactions[i]);
        }
      }
    }
    $rootScope.safeApply();

    return true;
  });

  $rootScope.$on('casinoTransactionsLoaded', function () {
    $scope.Report.CasinoTransactions = $agentService.CasinoTransactionList;
    if ($scope.setCategory == 'bal' && $scope.Report.CasinoTransactions) {
      var inserted = false;
      for (var i = 0; i < $scope.Report.CasinoTransactions.length; i++) {
        for (var j = 0; j < $scope.Report.TransactionsByDate.length; j++) {
          if (CommonFunctions.FormatDateTime($scope.Report.TransactionsByDate[j].TranDateTime, 1) <
               CommonFunctions.FormatDateTime($scope.Report.CasinoTransactions[i], 1)) {
            $scope.Report.TransactionsByDate.splice(j, 0, $scope.Report.CasinoTransactions[i]);
            inserted = true;
            break;
          }

        }
        if (!inserted) {
          $scope.Report.TransactionsByDate.push($scope.Report.CasinoTransactions[i]);
        }
      }
      showCashTransactions($scope.Arrow);
    }
    else if ($scope.setCategory == 'bal') showCashTransactions($scope.Arrow);
    $rootScope.safeApply();

    return true;
  });

  $scope.ShowCasinoLine = function () {
    for (var i = 0; $scope.DailyCasinoWinLossList.length > i ; i++)
      if ($scope.DailyCasinoWinLossList[i].hasValue) return true;
    return false;
  };

  function showCasinoTransactions(index) {
    if ($scope.SelectedCustomer.CustomerId)
      $agentService.GetCasinoTransactionsByDate($scope.SelectedCustomer.CustomerId, $scope.Report.DailyFigures.ValuesPerDay[index].ThisDate);
  };

  function showDailyWinLoss() {
    if ($scope.Report.DailyFigures == null) return;
    for (var i = 0; i < 7; i++) {
      $scope.DailyWinLossList.push($scope.Report.DailyFigures.ValuesPerDay[i].WinLoss != null ? DailyWinLoss(true, $scope.Report.DailyFigures.ValuesPerDay[i].WinLoss) : DailyWinLoss(false, null));
    }
  };

  function showDailyCashInOut() {
    if ($scope.Report.DailyFigures == null) return;
    for (var i = 0; i < 7; i++) {
      $scope.DailyCashInOutList.push($scope.Report.DailyFigures.ValuesPerDay[i].CashInOut != null ? CashInOut(true, $scope.Report.DailyFigures.ValuesPerDay[i].CashInOut) : CashInOut(false, null));
    }
  };

  function showDailyCasinoWinLoss() {
    if ($scope.Report.DailyFigures == null) return;
    for (var i = 0; i < 7; i++) {
      $scope.DailyCasinoWinLossList.push($scope.Report.DailyFigures.ValuesPerDay[i].CasinoWinLoss != null ? CasinoWinLoss(true, $scope.Report.DailyFigures.ValuesPerDay[i].CasinoWinLoss) : CasinoWinLoss(false, null));
    }
  };

  function showDailyBalance() {
    if ($scope.Report.DailyFigures == null) return;
    var balance = $scope.Report.DailyFigures.StartingBalance != null ? $scope.Report.DailyFigures.StartingBalance : 0;
    for (var i = 0; i < 7; i++) {
      balance += ($scope.Report.DailyFigures.ValuesPerDay[i].WinLoss != null ? $scope.Report.DailyFigures.ValuesPerDay[i].WinLoss : 0)
          + ($scope.Report.DailyFigures.ValuesPerDay[i].CashInOut != null ? $scope.Report.DailyFigures.ValuesPerDay[i].CashInOut : 0)
          + ($scope.Report.DailyFigures.ValuesPerDay[i].CasinoWinLoss != null ? $scope.Report.DailyFigures.ValuesPerDay[i].CasinoWinLoss : 0);
      if ($scope.Report.DailyFigures.ValuesPerDay[i].WinLoss == null && $scope.Report.DailyFigures.ValuesPerDay[i].CashInOut == null && $scope.Report.DailyFigures.ValuesPerDay[i].CasinoWinLoss == null)
        $scope.DailyBalanceList.push(Balance(false, balance));
      else
        $scope.DailyBalanceList.push(Balance(true, balance));
    }
  };

  $scope.ShowNextWeek = function () {
    $scope.CloseDailyFiguresDetails();
    if ($scope.Report.DailyFigures == null || $scope.Report.DailyFigures.WeekOffset == 0)
      return false;
    isCalendarPick = false;
    $scope.ShowDailyFigures($scope.Report.DailyFigures.WeekOffset - 1);
    return true;
  };

  $scope.ShowPreviousWeek = function () {
    $scope.CloseDailyFiguresDetails();
    if ($scope.Report.DailyFigures == null)
      return false;
    isCalendarPick = false;
    $scope.ShowDailyFigures($scope.Report.DailyFigures.WeekOffset + 1);
    return true;
  };

  $scope.CloseDailyFiguresDetails = function () {
    document.getElementById('monday').className = ('collapse');
    $scope.Arrow = -1;
  };

  $scope.ShowCurrentMonths = function () {
    if ($scope.Report.DailyFigures == null)
      return "";
    var months = ["JAN", "FEB ", "MAR", "APR", "MAY ", "JUNE", "JULY", "AUG", "SEPT", "OCT ", "NOV", "DEC"];
    var output = [];

    for (var i = 0; i < $scope.Report.DailyFigures.ValuesPerDay.length; i++) {
      var monthNum = $scope.Report.DailyFigures.ValuesPerDay[i].ThisDate.substring(0, $scope.Report.DailyFigures.ValuesPerDay[i].ThisDate.indexOf("/")) - 1;

      if (output.indexOf(months[monthNum]) < 0)
        output.push(months[monthNum]);
    }
    return output.join(" / ");
  };

  $scope.ShowDailyFigures = function (weekOffset) {
    $scope.custCurrencyCode = GetCustomerCurrencyCode($scope.SelectedCustomer.CustomerId);
    $agentService.GetCustomerDailyFigures($scope.SelectedCustomer.CustomerId, weekOffset, $scope.custCurrencyCode);
    setTimeout(function () {

      jQuery('input[name="ReportDate"]').daterangepicker({
        locale: {
          firstDay: 1
        },
        singleDatePicker: true,
        showDropdowns: true,
        opens: 'left'
      },
      function (start, end, label) {
        isCalendarPick = true;
        $scope.ShowDailyFigures(moment().isoWeekday(1).week() - start.isoWeekday(1).week());
        $scope.currentDate = end.toDate();
        endDate = CommonFunctions.FormatDateTime(end.toDate(), 4);
        $rootScope.safeApply();
      });

    }, 1000); //end of date stuff
  };

  function GetCustomerCurrencyCode(customerId) {
      var retCurrencyCode = 'USD.';
     
    if ($rootScope.AgentsList) {
      for (var i = 0; i < $rootScope.AgentsList[0].CustomersList.length; i++) {
        if ($rootScope.AgentsList[0].CustomersList[i].CustomerId == customerId) {
          retCurrencyCode = $rootScope.AgentsList[0].CustomersList[i].Currency.substring(0, 3);
          break;
        }
      }
    }
    return retCurrencyCode;
  };

  $scope.ShowDateNumber = function (index) {
    if ($scope.Report.DailyFigures == null)
      return "";
    var date = $scope.Report.DailyFigures.ValuesPerDay[index].ThisDate.substring($scope.Report.DailyFigures.ValuesPerDay[index].ThisDate.indexOf("/") + 1);
    return date.substring(0, date.indexOf("/"));
  };

  $scope.HighlightedClass = function (day) {

    if ($scope.Report.DailyFigures)
      return ($scope.FormatDateTime($scope.currentDate, 1) == $scope.FormatDateTime($scope.Report.DailyFigures.ValuesPerDay[day].ThisDate, 1) ? 'day_selected' : 'day_unselected');
    return null;
  };

  $scope.FormatDateTime = function (acceptedDateTime, formatCode) {
    return CommonFunctions.FormatDateTime(acceptedDateTime, formatCode);
  };

  $scope.ShowMoreDetails = function (index, category) {
    if ($scope.Report.DailyFigures == null)
      return false;
    if ($scope.Arrow != index || category != $scope.setCategory) {
      $scope.Report.CasinoTransactions = [];
      $scope.Report.CashTransactions = [];
      $scope.Report.TransactionsByDate = null;

      openDailyFiguresDetails();
      $scope.Arrow = index;
      $scope.TransactionDate = $scope.Report.DailyFigures.ValuesPerDay[index].ThisDate;
      switch (category) {
        case "cashintout":
          $scope.setCategory = "cash";
          showCashTransactions(index);
          break;
        case "winloss":
          $scope.setCategory = "winloss";
          showTransactionsbyDate($scope.Report.DailyFigures.ValuesPerDay[index].ThisDate, false);
          break;
        case "casino":
          $scope.setCategory = "casino";
          showCasinoTransactions(index);
          break;
        case "bal":
          $scope.setCategory = "bal";
          showTransactionsbyDate($scope.Report.DailyFigures.ValuesPerDay[index].ThisDate, false);
          break;
      }
    }
    return false;
  };

  function showTransactionsbyDate(date, includeCasino) {
    if ($scope.SelectedCustomer.CustomerId)
      $agentService.GetCustomerTransactionListByDate($scope.SelectedCustomer.CustomerId, date, includeCasino);
  };

  function showCashTransactions(index) {
    if ($scope.SelectedCustomer.CustomerId)
      $agentService.GetCashTransactionsByDate($scope.SelectedCustomer.CustomerId, $scope.Report.DailyFigures.ValuesPerDay[index].ThisDate);
  };

  function openDailyFiguresDetails() {
    var l = document.getElementById('monday');
    l.className = ('collapse in');
  };

  $scope.ShowEndingBalance = function () {
    if ($scope.Report.DailyFigures == null)
      return null;
    var endingBalance = $scope.Report.DailyFigures.StartingBalance + $scope.Report.DailyFigures.WinLossTotal + $scope.Report.DailyFigures.CashInOutTotal + $scope.Report.DailyFigures.CasinoWinLossTotal;
    if ($scope.Report.DailyFigures.ZeroBalance != null)
      endingBalance += $scope.Report.DailyFigures.ZeroBalance;
    return endingBalance;

  };

  $scope.ShowStartingBalance = function () {
    if (!$scope.Report.DailyFigures || $scope.Report.DailyFigures.StartingBalance == null)
      return "";
    return $scope.Report.DailyFigures.StartingBalance != null ? $scope.Report.DailyFigures.StartingBalance : 0;
  };

  $scope.ShowWinLossTotal = function () {
    if ($scope.Report.DailyFigures == null)
      return "";
    return $scope.Report.DailyFigures.WinLossTotal != null ? $scope.Report.DailyFigures.WinLossTotal : 0;
  };

  $scope.ShowCashInOutTotal = function () {
    if ($scope.Report.DailyFigures == null)
      return "";
    return $scope.Report.DailyFigures.CashInOutTotal != null ? $scope.Report.DailyFigures.CashInOutTotal : 0;
  };

  $scope.ShowCasinoWinLossTotal = function () {
    if ($scope.Report.DailyFigures == null)
      return "";
    return $scope.Report.DailyFigures.CasinoWinLossTotal != null ? $scope.Report.DailyFigures.CasinoWinLossTotal : 0;
  };

  $scope.ShowZeroBalance = function () {
    if ($scope.Report.DailyFigures == null)
      return "";
    return $scope.Report.DailyFigures.ZeroBalance != null ? $scope.Report.DailyFigures.ZeroBalance : 0;
  };

  //Daily Figures Section End

  //Open Bets Section Begin

  $rootScope.$on('openBetsLoaded', function () {
    $scope.Report.OpenBets = $agentService.GroupedOpenBets;
    $rootScope.safeApply();
    return true;
  });

  $scope.ShowOpentBets = function () {
    if ($scope.SelectedCustomer.CustomerId)
      $agentService.GetCustomerPendingBets($scope.SelectedCustomer.CustomerId);
    $scope.Templates.reports.selected = $scope.openBetsTemplate;
  };

  $scope.GetWagerStatus = function (openBet) {
    var wagerStatus = "";

    if (openBet.WagerStatus === "O") {
      wagerStatus = " (" + $scope.Translate("OPEN_BET_LABEL") + ")";
    }
    return wagerStatus;
  };

  $scope.PointsBought = function (wi) {
    var pointsbought = 0;
    var lineStr = "";
    switch (wi.ItemWagerType) {
      case "S":
        var origSpread = wi.OrigSpread;
        var finalSpread = wi.AdjSpread;
        if (origSpread !== finalSpread) pointsbought = (origSpread > 0 ? origSpread - finalSpread : Math.abs(finalSpread) - Math.abs(origSpread));
        pointsbought = Math.abs(pointsbought);
        if (wi.PeriodNumber === 0) lineStr += LineOffering.PointsBought(pointsbought);
        break;
      case "L":
        var origtpoints = wi.OrigTotalPoints;
        var finaltpoints = wi.AdjTotalPoints;
        if (origtpoints !== finaltpoints) pointsbought = (origtpoints > 0 ? origtpoints - finaltpoints : finaltpoints + origtpoints);
        pointsbought = Math.abs(pointsbought);
        if (wi.PeriodNumber === 0) lineStr += LineOffering.PointsBought(pointsbought);
        break;
    }
    return lineStr;
  };

  $scope.GetWagerItemDescription = function (fullWagerDescription, wagerItemIndex) {
    var descArray = fullWagerDescription.split('\r\n');
    if (descArray.length > 0 && descArray[wagerItemIndex])
      return descArray[wagerItemIndex].replace("Credit Adjustment", '').replace("Debit Adjustment", '');
    else return fullWagerDescription.replace("Credit Adjustment", '').replace("Debit Adjustment", '');
  };

  $scope.WagerType = function (t) {
    var twriter = t.EnteredBy || t.TicketWriter;
    if (twriter) {
      switch (twriter) {
        case "System":
          $scope.isXTransaction = true;
          return $scope.Translate("Casino");
        case "LIVE":
          $scope.isXTransaction = true;
          return $scope.Translate("Live Betting");
        default:
          if (twriter.indexOf("COLCHIAN") != -1) {
            $scope.isXTransaction = true;
            return $scope.Translate("Horses");
          }
      }
    }
    $scope.isXTransaction = false;
    return LineOffering.WagerTypes.GetByCode(t.WagerType, t.TeaserName);
  };

  //Open Bets Section End

  //All Transactions Section Begin

  $rootScope.$on('transactionListLoaded', function () {
    $scope.Report.AllTransactions = $agentService.TransactionList;
    $rootScope.safeApply();
    return true;
  });

  $scope.ShowAllTransactions = function () {
    setTimeout(function () {
      function cb(start, end) {
        var diff = end.diff(start, 'days');
        jQuery('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        if ($scope.SelectedCustomer.CustomerId)
          $agentService.GetCustomerTransactionListByDays($scope.SelectedCustomer.CustomerId, diff + 1);
        if (!$rootScope.isMobile)
          CommonFunctions.PrepareTable('performanceTbl');
      }

      cb(moment().subtract(6, 'days'), moment());
      jQuery('#reportrange').daterangepicker({
        locale: {
          firstDay: 1
        },
        startDate: moment().subtract(6, 'days'),
        ranges: {
          'Today': [moment(), moment()],
          'Yesterday': [moment().subtract(1, 'days'), moment()],
          'Last 7 Days': [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'Last 60 Days': [moment().subtract(59, 'days'), moment()],
          'Last 90 Days': [moment().subtract(89, 'days'), moment()]
        }
      }, cb);
    }, 1000); //end of date stuff
  };

  $scope.ScoredPtsHeader = function (openBetItem) {
    var resultsTitle = "";
    if (!openBetItem) return "";

    if (openBetItem.SportType != null && openBetItem.SportType == "Soccer") {
      resultsTitle += $scope.Translate("GOALS") + " "; //"Goals ";
    } else {
      resultsTitle += $scope.Translate("POINTS") + " "; //"Points ";
    }

    var periodDescripion = "";
    if (openBetItem.PeriodDescription != null)
      periodDescripion = openBetItem.PeriodDescription;

    if (openBetItem.PeriodDescription != null) {
      resultsTitle += $scope.Translate("SCORED_IN") + " " + $scope.Translate(periodDescripion); //" period:";
    }

    return resultsTitle;
  };

  $scope.WriteTeamsScores = function (openBetItem) {

    if (isNaN(openBetItem.Team1Score) || isNaN(openBetItem.Team2Score)) return $scope.Translate("Pending");

    var teamsResults = "";
    var team1Id = "";
    var team2Id = "";
    var team1Score = "";
    var team2Score = "";
    if (!openBetItem) return "";
    if (openBetItem.Team1ID != null)
      team1Id = openBetItem.Team1ID;
    if (openBetItem.Team2ID != null)
      team2Id = openBetItem.Team2ID;
    if (openBetItem.Team1Score != null)
      team1Score = openBetItem.Team1Score;
    if (openBetItem.Team2Score != null)
      team2Score = openBetItem.Team2Score;
    teamsResults += team1Id + " - " + team1Score + " / " + team2Id + " - " + team2Score;
    if (openBetItem.WagerType == "E" || openBetItem.WagerType == "L" || openBetItem.WagerType == "T") {
      teamsResults += " " + $scope.Translate("TOTAL_POINTS") + ": " + (team1Score + team2Score);
    }
    return teamsResults;
  };

  $scope.WriteWinner = function (openBetItem) {


    var team1Score = "";
    var team2Score = "";
    var winner = "";
    if (openBetItem.Team1Score != null)
      team1Score = openBetItem.Team1Score;
    if (openBetItem.Team2Score != null)
      team2Score = openBetItem.Team2Score;
    if (team1Score != team2Score) {
      var winnerId = "";
      if (openBetItem.WinnerID != null)
        winnerId = openBetItem.WinnerID;
      winner += winnerId + " " + $scope.Translate("WON PERIOD BY") + " " + Math.abs(team1Score - team2Score);
    }

    return winner;
  };

  //All Transactions Section End

  $scope.Init();

}]);