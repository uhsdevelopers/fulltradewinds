﻿SETTINGS = {
    ConsoleActive: false,
    //SocketServers: ['http://192.168.100.176:8082/signalr'],
    //SocketServers: ['https://m1.trdwd.ec/signalr'],
    SocketServers: ['https://ws.trdwd.ec/signalr'],

    //SocketServers: ['127.0.0.1'],
    TextLearningModeOn: false,
    DefaultLanguage: 'ENG',
    TranlationFileSufix: '_ag',
    DecimalPrecision: 0,
    MaxDenominator: 20,
    //LoginSite: 'http://192.168.100.176:8081/',
    LoginSite: 'https://trdwd.ec/',
    //MainSite: 'http://192.168.100.176:8020/Autologin',
    MainSite: 'https://agent.trdwd.ec/Autologin',
    
    TrackTime: 10000
};