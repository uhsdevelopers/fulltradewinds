﻿appModule.directive('performance', ['$rootScope', '$agentService', function ($rootScope, $agentService) {
  return {
    restrict: 'AEC',
    scope: {
      customerId: '=',
      version: '='

    },
    controller: ['$scope', '$rootScope', '$translatorService', function ($scope, $rootScope, $translatorService) {

      $scope.Translate = $translatorService.Translate;
      $scope.LoadingReportData = false;

      $scope.AgentID = null;

      $scope.ReportFilters = {
        PeriodRange: 'W',
        WagerType: ''
      };

      $scope.DisplayMyNumber = function (n) {
        return $scope.$parent['DisplayMyNumber'].apply(null, [n]);
      };

      $scope.Init = function () {
        if (!$rootScope.IsMobile)
          setTimeout(function () {
            jQuery('table.tablesaw').fixedHeader({
              topOffset: 65
            });
          }, 1200);
        if ($scope.customerId) {
          $scope.GetAgentPerformance('W', $scope.customerId.trim());
        }
        else $scope.GetAgentPerformance(null, null);
        $scope.UserTotal = 0;
        if ($agentService.AgentAsCustomerInfo && $agentService.AgentAsCustomerInfo.AgentType != 'A') $scope.GetAgentsList();

      };


      $rootScope.$on('AgentAsCutomerInfoLoaded', function () {
        $scope.AgentAsCustomerInfo = $agentService.AgentAsCustomerInfo;
        if ($agentService.AgentAsCustomerInfo.AgentType != 'A') $scope.GetAgentsList();
      });

      $scope.GetAgentPerformance = function (range, customerId) {
        if ($scope.customerId) customerId = $scope.customerId;
        if (typeof customerId === "undefined") customerId = null;
        if (range) $scope.ReportFilters.PeriodRange = range;
        $scope.LoadingReportData = true;
        $(".tablesaw-bar").remove();
        $agentService.GetAgentPerformance($scope.ReportFilters.PeriodRange, $scope.ReportFilters.WagerType, customerId).then(function (result) {
          $scope.AgentPerformance = result.data.d.Data;
          setTimeout(function() {
            $scope.LoadingReportData = false; 
            if (!$rootScope.IsMobile)
              CommonFunctions.PrepareTable('performanceTbl');
          });
        });
      };

      $scope.DisplayPeriod = function (period) {
        return $agentService.DisplayPeriod($scope.ReportFilters.PeriodRange, period);
      };

      $scope.AddToTotal = function (e, ap, winloss) {
        if (winloss == "W") ap.WonSelected = !ap.WonSelected;
        else ap.LostSelected = !ap.LostSelected;
        $scope.UserTotal = $agentService.GetPerformanceTotal($scope.AgentPerformance);
      };

      $scope.GetAgentsList = function () {
        $agentService.GetAgentsList().then(function (result) {
          $scope.AgentsList = result.data.d.Data;
          if (!$scope.AgentAsCustomerInfo)
            $rootScope.$on('AgentAsCutomerInfoLoaded', function () {
              $scope.AgentsList.unshift($scope.AgentAsCustomerInfo.CustomerID.trim());
              $scope.AgentID = $scope.AgentsList[0];
            });
          else {
            $scope.AgentsList.unshift($scope.AgentAsCustomerInfo.CustomerID.trim());
            $scope.AgentID = $scope.AgentsList[0];
          }

        });
      };

      $scope.GetPeriod = function (period) {
        switch (period) {
          case 'D':
            return 'Daily';
          case 'W':
            return 'Weekly';
          case 'M':
            return 'Monthly';
          case 'Y':
            return 'Yearly';
        }
        return null;
      };
      if ($agentService.AgentAsCustomerInfo)
        $scope.AgentAsCustomerInfo = $agentService.AgentAsCustomerInfo;


      $scope.$watch('customerId', function (customerId) {
        $scope.customerId = customerId;
        $scope.Init();
      });

    }],
    templateUrl: '/app/directives/performance/performance.html'
  };


}]);