﻿appModule.directive('custActions', ['$agentService', function ($agentService) {
  return {
    restrict: 'AEC',
    scope: {
      customer: '=',
      agentInfo: '=',
      updateCustomerBalance: '&',
      updateCustomer: '&'
    },
    controller: ['$scope', '$rootScope', '$translatorService', function ($scope, $rootScope, $translatorService) {
      $scope.Translate = function (text) {
        return $translatorService.Translate(text);
      };
      $scope.SportsLimits = [];
      $scope.tranType = 'Deposit';
      $scope.paymentAmt = null;
      $scope.paymentDescr = null;
      $scope.creditLimitAmt = null;
      $scope.tmpCreditLimitAmt = null;
      $scope.quickLimitAmt = null;
      $scope.tmpQuickLimitAmt = null;
      $scope.freePlayTransaction = false;
      $scope.msgExpirationDate = null;
      $scope.msgPriority = 'M';
        jQuery('input[name="txtTempCreditAdjThru"]').daterangepicker({
          locale: {
            firstDay: 1
          },
          singleDatePicker: true,
          showDropdowns: true,
          opens: 'left'
        });
        jQuery('input[name="txtTempQuickAdjThru"]').daterangepicker({
          locale: {
            firstDay: 1
          },
          singleDatePicker: true,
          showDropdowns: true,
          opens: 'left'
        });
            //end of date stuff

      $scope.$watch('customer', function (newVal) {
        if (!newVal) return;
        $scope.CurrentBalance = $scope.customer ? CommonFunctions.FormatNumber($scope.customer.CurrentBalance / 100) : null;
        $scope.customer = newVal;
        getDailyFigureDatesRange();
        $scope.confirmationMsg = '';
        if (!$scope.customer) return;
        $scope.creditLimitAmt = null;
        $scope.tmpCreditLimitAmt = null;
        $scope.custPassword = null;
        $scope.tmpCreditAdjDateThru = null;
        $scope.tmpQuickAdjDateThru = null;
        $scope.quickLimitAmt = null;
        $scope.tmpQuickLimitAmt = null;
        $scope.states = CommonFunctions.GetStates();
        $scope.updateCustomerBalance(
            {
              customerId: $scope.customer.CustomerId,
              currentBalance: $scope.customer.CurrentBalance
            }
        );
      });

      $scope.QuickAmountPlaceholder = function () {
        if (!$scope.customer) return;
        return $scope.customer ? CommonFunctions.FormatNumber($scope.customer.WagerLimit, true) : 'amount';
      };

      $scope.QuckTempPlaceholder = function () {
        if (!$scope.customer) return;
        return $scope.customer ? CommonFunctions.FormatNumber($scope.customer.TempWagerLimit, true) : 'Tmp Credit Adj';
      };

      $scope.QuickDatePlaceholder = function () {
        if (!$scope.customer) return;
                return $scope.customer ? CommonFunctions.FormatDateTime($scope.customer.TempWagerLimitExpirationString, 4) : 'Tmp Credit Adj';
      };

      $scope.FormatDateTime = function (date) {
        return CommonFunctions.FormatDateTime(date, 4);
      };

      $scope.SettlePlaceholder = function () {
        if (!$scope.customer) return;
        return $scope.customer ? CommonFunctions.FormatNumber($scope.customer.SettleFigure, false) : 'settle amount';
      };

      $scope.AmountPlaceholder = function () {
        if (!$scope.customer) return;
        return $scope.customer ? CommonFunctions.FormatNumber($scope.customer.CreditLimit, true) : 'amount';
      };

      $scope.TempPlaceholder = function () {
        if (!$scope.customer) return;
        return $scope.customer ? CommonFunctions.FormatNumber($scope.customer.TempCreditAdj, true) : 'Tmp Credit Adj';
      };

      $scope.DatePlaceholder = function () {
        if (!$scope.customer) return;
                return CommonFunctions.FormatDateTime($scope.customer.TempCreditAdjExpDateString, 4);
      };

      $scope.IsDisabled = function () {
        return !$scope.customer || !$scope.AgentInfo;
      };

      $scope.AddCustomerMessage = function () {
        if (!$scope.customer) {
          alert($scope.Translate('Select customer from the list.'));
          return;
        }
        if (!$scope.msgSubject || $scope.msgSubject.trim() == "") {
          alert($scope.Translate('Please write down a subject'));
          return;
        }
        if (!$scope.msgBody || $scope.msgBody.trim() == "") {
          alert($scope.Translate('Please wite down a message'));
          return;
        }
        var date = null;
        try {
          date = new Date($scope.msgExpirationDate);
        } catch (err) {
        }

        if (typeof date.getMonth !== 'function') {
          alert($scope.Translate('Select a Valid Date.'));
          return;
        }

        $agentService.AddCustomerMessage($scope.customer.CustomerId, $scope.msgExpirationDate, $scope.msgPriority, $scope.msgSubject, $scope.msgBody).then(function (result) {
          if (result.Code == 1) {
            alert($scope.Translate('Error adding comment'));
          } else {
            $scope.msgSubject = null;
            $scope.msgBody = null;
            $scope.confirmationMsg = $scope.Translate("Message will expire (stop displaying) at 12:00:01 A.M on ") + CommonFunctions.FormatDateTime(date, 4);
          }
        });
      };

      $scope.PostPaymentTransaction = function () {
        if (!$scope.customer) {
          alert($scope.Translate('Select customer from the list.'));
          return;
        }
        if ($scope.tranType == null || $scope.tranType == "") {
          alert($scope.Translate('Select transaction type from the list.'));
          return;
        }

        if ($scope.paymentAmt == null || !isValidNumber($scope.paymentAmt)) {
          alert($scope.Translate('Type in a valid amount greater than zero.'));
          return;
        }

        if ($scope.paymentDescr == null || $scope.paymentDescr == "") {
          alert($scope.Translate('Write a description for the transaction.'));
          return;
        }

        if (($scope.tranType == "BAdjCredit" || $scope.tranType == "BAdjDebit") && $scope.dailyFigureDate == "") {
          alert($scope.Translate('Select a date for the Bet Adjustment.'));
          return;
        }

        var customerId = $scope.customer.CustomerId;
        var tranCode = getTransactionCode();
        var tranType = getTransactionType();
        var amount = Number($scope.paymentAmt);
        var description = buildDescription();
        var dailyFigureDate = null;

        if (($scope.tranType == "BAdjCredit" || $scope.tranType == "BAdjDebit")) {
          dailyFigureDate = new Date(parseInt($scope.dailyFigureDate.Date.substr(6)));
        }

        if ($scope.tranType == "FpDeposit" || $scope.tranType == "FpWithdrawal") {
          $scope.freePlayTransaction = true;
        }
        var freePlayTransaction = $scope.freePlayTransaction;

        $agentService.InsertCustomerTransaction(customerId, tranCode, tranType, amount, null, description, dailyFigureDate, freePlayTransaction).then(function (result) {
          if (result.Code == 1) {
            alert($scope.Translate('Error posting the transaction.'));
          }
          $scope.CurrentBalance = CommonFunctions.FormatNumber(Number($scope.customer.CurrentBalance / 100) + ($scope.tranType == "Withdrawal" || $scope.tranType == "FpWithdrawal" ? amount * -1 : amount));
          $scope.confirmationMsg = getTransactionName() + ' for ' + CommonFunctions.FormatNumber(amount, false, false, true) + ' Posted correctly.';
          $scope.updateCustomerBalance(
              {
                customerId: $scope.customer.CustomerId,
                currentBalance: $scope.customer.CurrentBalance + (($scope.tranType == "Withdrawal" || $scope.tranType == "FpWithdrawal" ? amount * -1 : amount) * 100)
              }
          );
          $scope.paymentAmt = "";
          $scope.paymentDescr = null;
        });

      };

      $scope.PostQuickLimitsTransaction = function () {

        if ($scope.IsDisabled()) return;

        if ($scope.creditLimitAmt != null && $scope.creditLimitAmt != "" && !isValidNumber($scope.creditLimitAmt, true)) {
          alert($scope.Translate('Type in a valid Credit Limit amount.'));
          return;
        }

        if ($scope.tmpCreditLimitAmt != null && $scope.tmpCreditLimitAmt != "") {
          if (!isValidNumber($scope.tmpCreditLimitAmt, true)) {
          alert($scope.Translate('Type in a valid Temp Credit Adjustment amount.'));
          return;
        }
          var date;
          try {
            date = new Date($scope.tmpCreditAdjDateThru);
          } catch (err) { }

          if (typeof date.getMonth !== 'function') {
            alert($scope.Translate('Select a Valid Date.'));
            return;
          }
          var d1 = new Date();
          if (date < d1) {
            alert($scope.Translate('Select a Valid Date.'));
            return;
          }
        }

        if ($scope.tmpCreditLimitAmt != null && $scope.tmpCreditLimitAmt != "" && ($scope.tmpCreditAdjDateThru == null || $scope.tmpCreditAdjDateThru == "")
            && Number(($scope.tmpCreditLimitAmt + "").replace(',', '')) > 0) {
          alert($scope.Translate('Select an Ending Date.'));
          return;
        }

        if ($scope.quickLimitAmt != null && $scope.quickLimitAmt != "" && !isValidNumber($scope.quickLimitAmt, true)) {
          alert($scope.Translate('Type in a valid Credit Limit amount.'));
          return;
        }

        if ($scope.tmpQuickLimitAmt != null && $scope.tmpQuickLimitAmt != "" && !isValidNumber($scope.tmpQuickLimitAmt, true)) {
          alert($scope.Translate('Type in a valid Temp Credit Adjustment amount.'));
          return;
        }

        if ($scope.tmpQuickLimitAmt != null && $scope.tmpQuickLimitAmt != "" && ($scope.tmpQuickAdjDateThru == null || $scope.tmpQuickAdjDateThru == "")
            && Number(($scope.tmpQuickLimitAmt + "").replace(',', '')) > 0) {
          alert($scope.Translate('Select an Ending Date.'));
          return;
        }

        var customerId = $scope.customer.CustomerId;
        var tranAmount = null;
        try {
          tranAmount = Math.round(parseFloat($scope.creditLimitAmt ? $scope.creditLimitAmt : $scope.customer.CreditLimit / 100));
        }
        catch (err) {
        }

        ///////////////////


        var settleLimitAmt = null;
        try {
          settleLimitAmt = Math.round(parseFloat($scope.settleLimitAmt ? $scope.settleLimitAmt : $scope.customer.TempCreditAdj / 100));
        }
        catch (err) {
        }

        if ($scope.settleLimitAmt != null && isValidNumber($scope.settleLimitAmt, true)) {
          $agentService.ChangeCustomerSettleFigure(customerId, (settleLimitAmt * 100), null).then(function (result) {
            if (result.data.d.Code == 1) {
              alert($scope.Translate('Error posting the transaction.'));
            }
            $scope.customer.SettleFigure = settleLimitAmt * 100;
            $scope.detailLimitTranPostedMsg = $scope.Translate('Settle figure Adjustment for ') + CommonFunctions.FormatNumber(settleLimitAmt, false, false) + $scope.Translate(' Posted correctly.');
          });
        };

        ////////////////

        var tmpCreditLimitAmt = null;
        try {
          tmpCreditLimitAmt = Math.round(parseFloat($scope.tmpCreditLimitAmt ? $scope.tmpCreditLimitAmt : $scope.customer.TempCreditAdj / 100));
        }
        catch (err) {
        }

        if ($scope.creditLimitAmt != null && isValidNumber($scope.creditLimitAmt, true)) {
          $agentService.ChangeCustomerCreditLimit(customerId, (tranAmount * 100), null).then(function (result) {
            if (result.data.d.Code == 1) {
              alert($scope.Translate('Error posting the transaction.'));
            }
            $scope.customer.CreditLimit = tranAmount * 100;
            $scope.updateCustomerBalance(
            {
              customerId: $scope.customer.CustomerId,
              currentBalance: $scope.customer.CurrentBalance
            });
            $scope.detailLimitTranPostedMsg = $scope.Translate('Credit Limit Adjustment for ') + CommonFunctions.FormatNumber(tranAmount, false, false) + $scope.Translate(' Posted correctly.');
          });
        };

                $agentService.ChangeCustomerTempCreditLimit(customerId, $scope.tmpCreditLimitAmt != null && isValidNumber($scope.tmpCreditLimitAmt, true) ? (tmpCreditLimitAmt * 100) : $scope.customer.TempCreditAdj, date).then(function (result) {
            if (result.data.d.Code == 1) {
              alert($scope.Translate('Error posting the transaction.'));
            }
                    $scope.customer.TempCreditAdj = $scope.tmpCreditLimitAmt != null && isValidNumber($scope.tmpCreditLimitAmt, true) ? tmpCreditLimitAmt * 100 : $scope.customer.TempCreditAdj;
            $scope.customer.TempCreditAdjExpDate = date;
                    $scope.customer.TempCreditAdjExpDateString = CommonFunctions.FormatDateTime(date, 4);
            $scope.updateCustomerBalance(
            {
              customerId: $scope.customer.CustomerId,
              currentBalance: $scope.customer.CurrentBalance
            });
            $scope.detailLimitTempPostedMsg = $scope.Translate('Temp Credit Limit Adjustment for ') + CommonFunctions.FormatNumber(tmpCreditLimitAmt, false, false) + $scope.Translate(' Posted correctly.');
          });

        //////////////////////////////

        var qDate;
        if (Number(($scope.tmpQuickLimitAmt + "").replace(',', '')) > 0) {
          try {
            qDate = new Date($scope.tmpQuickAdjDateThru);
            //Allan: Until
            //qDate.setDate(qDate.getDate() + 1);
          } catch (err) {
          }

          if (typeof qDate.getMonth !== 'function') {
            alert($scope.Translate('Select a Valid Date.'));
            return;
          }
          var qd1 = new Date();

          if (qDate < qd1) {
            alert($scope.Translate('Select a Valid Date.'));
            return;
          }
        } else qDate = null;

        var qtranAmount = null;
        try {
          qtranAmount = Math.round(parseFloat($scope.quickLimitAmt ? $scope.quickLimitAmt : $scope.customer.WagerLimit / 100));
        }
        catch (err) {
        }

        var tmpQuickLimitAmt = null;
        try {
          tmpQuickLimitAmt = Math.round(parseFloat($scope.tmpQuickLimitAmt ? $scope.tmpQuickLimitAmt : $scope.customer.TempWagerLimit / 100));
        }
        catch (err) {
        }


        if ($scope.quickLimitAmt != null && !$scope.customer.TempWagerLimit && isValidNumber($scope.quickLimitAmt, true)) {
          $agentService.ChangeCustomerQuickLimit(customerId, (qtranAmount * 100), null).then(function (result) {
            if (result.data.d.Code == 1) {
              alert($scope.Translate('Error posting the transaction.'));
            }
            $scope.customer.WagerLimit = qtranAmount * 100;
            $scope.updateCustomerBalance(
            {
              customerId: $scope.customer.CustomerId,
              currentBalance: $scope.customer.CurrentBalance
            });
            $scope.quickLimitTranPostedMsg = $scope.Translate('Quick Limit Adjustment for ') + CommonFunctions.FormatNumber(qtranAmount, false, false) + $scope.Translate(' Posted correctly.');
          });
        };

        if ($scope.tmpQuickLimitAmt != null && isValidNumber($scope.tmpQuickLimitAmt, true)) {
          $agentService.ChangeCustomerTempQuickLimit(customerId, (tmpQuickLimitAmt * 100), qDate).then(function (result) {
            if (result.data.d.Code == 1) {
              alert($scope.Translate('Error posting the transaction.'));
            }
            $scope.customer.TempWagerLimit = tmpQuickLimitAmt * 100;
            $scope.customer.TempWagerLimitExpiration = qDate;
            $scope.updateCustomerBalance(
            {
              customerId: $scope.customer.CustomerId,
              currentBalance: $scope.customer.CurrentBalance
            });
            $scope.quickLimitTempPostedMsg = $scope.Translate('Temp Quick Limit Adjustment for ') + CommonFunctions.FormatNumber(tmpQuickLimitAmt, false, false) + $scope.Translate(' Posted correctly.');
          });
        };
      };

      $scope.UpdateCustomer = function () {
        if ($scope.IsDisabled()) return;
        if (!$scope.customer.password || $scope.customer.password.toString().trim() == "") {
          alert($scope.Translate('Please set a valid password'));
          return;
        }
        $agentService.UpdateCustomer($scope.customer.CustomerId, $scope.customer.password,
            $scope.customer.NameFirst, $scope.customer.NameMI, $scope.customer.NameLast, $scope.customer.Address, $scope.customer.State, $scope.customer.City, $scope.customer.Zip).then(function (result) {
              if (result.data.d.Code != 0) {
                $scope.confirmationMsg = $scope.Translate("Error updating customer");

              } else {
                $scope.confirmationMsg = $scope.Translate("Customer updated");

              }


            });
        $scope.updateCustomer(
        {
          customer: $scope.customer
        });
        $scope.confirmationMsg = $scope.Translate("Customer updated");
      };


      $scope.UpdateCustomerAccess = function (customerId, object, e) {
        if ($scope.IsDisabled()) return;
        if ($scope.SuspendWagering == false) {
          return;
        }
        $scope.customer[object] = !$scope.customer[object];
        var accessValue = $scope.customer[object] ? "Y" : "N";
        $agentService.UpdateCustomerAccess($scope.customer.CustomerId, e.target.id, accessValue);
      };

      $scope.TabActive = function (tab) {
        if (!$agentService.AgentInfo) return;
        var updateCustomer = $agentService.AgentInfo.AddNewAccountFlag == 'Y';
        var paymentsActive = $agentService.AgentInfo.EnterTransactionFlag == 'Y';
        var quickLimitActive = $agentService.AgentInfo.ChangeCreditLimitFlag == 'Y' || $agentService.AgentInfo.ChangeTempCreditFlag == 'Y' || $agentService.AgentInfo.ChangeWagerLimitFlag == 'Y';
        var suspendWagering = $agentService.AgentInfo.SuspendWageringFlag == 'Y';
        var commentActive = $agentService.AgentInfo.UpdateCommentsFlag == 'Y';
        switch (tab) {
          case 'P':
            return paymentsActive;
          case 'Q':
            return !paymentsActive && quickLimitActive;;
          case 'D':
            return !paymentsActive && !quickLimitActive && suspendWagering;
          case 'C':
            return !paymentsActive && !quickLimitActive && !suspendWagering && commentActive;
          case 'A':
            return !paymentsActive && !quickLimitActive && !suspendWagering && !commentActive && updateCustomer;
        }
      };

      $scope.$watch('msgPriority', function (val) {
        switch (val) {
          case 'M':
            $scope.PriorityText = $scope.Translate("Message will be displayed on the customer's screen one time, and it will be kept in customer's Inbox until it expires.");
            break;
          case 'H':
            $scope.PriorityText = $scope.Translate("Message will be displayed on the customer's screen until it expires.");
            break;
          case 'L':
            $scope.PriorityText = $scope.Translate("Message will be sent to customer's Inbox and it will be visible until it expires.");
            break;
          case 'S':
            $scope.PriorityText = $scope.Translate("Message will be displayed on the customer's screen when the account is suspended.");
            break;
          default:
        }
      });


      $scope.AgentInfo = $agentService.AgentInfo;
      $scope.SuspendWagering = $agentService.AgentInfo ? $agentService.AgentInfo.SuspendWageringFlag == 'Y' : false;

      $rootScope.$on('AgentInfoLoaded', function () {
        $scope.AgentInfo = $agentService.AgentInfo;
        if ($agentService.AgentInfo) {
          $scope.SuspendWagering = $agentService.AgentInfo.SuspendWageringFlag == 'Y';
          $rootScope.safeApply();
        }
      });


      function getDailyFigureDatesRange() {
        $agentService.GetDailyFigureDatesRange().then(function (result) {
          $scope.DatesRange = result.data.d.Data;
          $scope.dailyFigureDate = $scope.DatesRange[0];
          formatDatesRangeForDisplay();
        });
      }

      function formatDatesRangeForDisplay() {
        if ($scope.DatesRange != null) {
          for (var i = 0; i < $scope.DatesRange.length; i++) {
            $scope.DatesRange[i].DayOfWeekStr = displayDate($scope.DatesRange[i].Date);
          }
        }
      }

      function displayDate(strDate) {
        if (strDate == null) return "";
        var date = new Date(parseInt(strDate.substr(6)));
        return firstLetterUpperCase(CommonFunctions.FormatDateTime(date, 6, 3, true).toString());
      }

      function firstLetterUpperCase(str) {
        return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase();
      }

      function buildDescription() {
        var baseDesc = "";
        switch ($scope.tranType) {
          case "Deposit":
            baseDesc = $scope.Translate("Customer Deposit");
            break;
          case "FpDeposit":
            baseDesc = $scope.Translate("Customer Deposit * Free Play * [Internet] *");
            break;
          case "BAdjCredit":
            baseDesc = $scope.Translate("Credit Betting Adjustment for ") + $scope.customer.CustomerId;
            break;
          case "Withdrawal":
            baseDesc = $scope.Translate("Customer Withdrawal");
            break;
          case "FpWithdrawal":
            baseDesc = $scope.Translate("Customer Withdrawal * Free Play * [Internet] *");
            break;
          case "BAdjDebit":
            baseDesc = $scope.Translate("Debit Betting Adjustment for ") + $scope.customer.CustomerId;
            break;
        }
        baseDesc += ' - ' + $scope.paymentDescr.substring(0, 200);
        return baseDesc;
      }

      function getTransactionCode() {
        switch ($scope.tranType) {
          case "Deposit":
          case "FpDeposit":
          case "BAdjCredit":
            return "C";
          case "Withdrawal":
          case "FpWithdrawal":
          case "BAdjDebit":
            return "D";
        }
        return "";
      }

      function getTransactionType() {
        switch ($scope.tranType) {
          case "Deposit":
          case "FpDeposit":
            return "E";
          case "Withdrawal":
          case "FpWithdrawal":
            return "I";
          case "BAdjCredit":
            return "C";
          case "BAdjDebit":
            return "D";
        }
        return "";
      }

      function getTransactionName() {
        switch ($scope.tranType) {
          case "Deposit":
          case "Withdrawal":
            return $scope.tranType;
          case "FpDeposit":
            return "Free Play Deposit";
          case "FpWithdrawal":
            return "Free Play Withdrawal";
          case "BAdjCredit":
            return "Bet Adj Credit";
          case "BAdjDebit":
            return "Bet Adj Debit";
        }
        return "";
      };

      function isValidNumber(targetAmount, allowZero) {
        if (targetAmount == null) return false;
        if (isNaN((targetAmount + "").replace(',', ''))) return false;
        if (allowZero) {
          if (Number((targetAmount + "").replace(',', '')) < 0) return false;
        } else if (Number((targetAmount + "").replace(',', '')) <= 0) return false;
        return true;
      };

    }],
    templateUrl: 'app/directives/custActions/custActions.html'
  };
}]);