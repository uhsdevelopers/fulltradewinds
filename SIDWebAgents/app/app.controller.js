﻿var appCtrl = appModule.controller("appController", ['$rootScope', '$scope', '$agentService', '$webSocketService', '$route', '$translatorService', function ($rootScope, $scope, $agentService, $webSocketService, $route, $translatorService) {

      $scope.Init = function () {
      $agentService.GetCustomersList(0).then(function () {
        $scope.AgentsList = $agentService.AgentsList;
        $agentService.GetAgentAsCustomer().then(function () {
          $agentService.GetAgent().then(function () {
            $scope.AgentInfo = $agentService.AgentInfo;
            $scope.AgentAsCustomerInfo = $agentService.AgentAsCustomerInfo;
            $webSocketService.SubscribeCustomer($scope.AgentAsCustomerInfo.CustomerID.trim());
          });
        });
        $scope.GetUngroupedCustomers();
      });
      };

    $(document).ready(function () {
        var onSizeChanges = [{
          element: "lineDiv",
          type: "id", //id / class
          threshold: 991.98,
          lowerClass: "page-content-wrapper-mob",
          upperClass: "page-content-wrapper"
        }];

        var xDown = null;
        var yDown = null;

        function handleTouchStart(evt) {
          if (!$rootScope.IsMobile) return;
          xDown = evt.touches[0].clientX;
          yDown = evt.touches[0].clientY;
        };

        function handleTouchMove(evt) {
          var hasSwipe = (evt.target.offsetParent.className.toString().indexOf("tablesaw-swipe") >= 0
              || evt.target.offsetParent.offsetParent.className.toString().indexOf("tablesaw-swipe") >= 0) && !jQuery("#wrapper").hasClass("toggled");
          if (!xDown || !yDown || !$rootScope.IsMobile || hasSwipe) {
            return;
          }
          var xUp = evt.touches[0].clientX;
          var yUp = evt.touches[0].clientY;

          var xDiff = xDown - xUp;
          var yDiff = yDown - yUp;

          if (Math.abs(xDiff) > Math.abs(yDiff)) {/*most significant*/
            if (xDiff > 0) {
              if (jQuery("#wrapper").hasClass("toggled")) $scope.ToggleMenu();
            } else {
              if (!jQuery("#wrapper").hasClass("toggled")) $scope.ToggleMenu();
            }
          }
          /* reset values */
          xDown = null;
          yDown = null;
        };

        function setTouchActions() {
          document.addEventListener('touchstart', handleTouchStart, false);
          document.addEventListener('touchmove', handleTouchMove, false);
        }

      function onSizeChange() {
        var w = jQuery(window).width();
        if (w > 991.98) {
          $rootScope.IsMobile = false;
        } else {
          $rootScope.IsMobile = true;
        }
        $rootScope.safeApply();
      }

      jQuery(window).resize(onSizeChange);
      onSizeChange();
      setTouchActions();
      });

      $scope.ToggleMenu = function () {
      //console.log(jQuery("#wrapper"));
        jQuery("#wrapper").toggleClass("toggled");
      };

      $scope.Translate = function (text) {
        CommonFunctions.TranslateFn = $translatorService.Translate;
        return $translatorService.Translate(text);
      };

      $scope.GetUngroupedCustomers = function () {
      if (!$scope.AgentsList || $scope.AgentsList.length === 0) return;
        var customers = [];
      for (var i = 0; i < $scope.AgentsList.length; i++) {
          for (var j = 0; j < $scope.AgentsList[i].CustomersList.length; j++) {
              $scope.AgentsList[i].CustomersList[j].DisplayCustomerId = $scope.AgentsList[i].CustomersList[j].CustomerId.replace("TW", "");
              customers.push($scope.AgentsList[i].CustomersList[j]);
          }
          
        }
        $scope.ActionCustomers = customers;
      };

      $scope.GetAgentRestriction = function (code) {
        if (!$agentService.Restrictions) return null;
        for (var i = 0; $agentService.Restrictions.length > i; i++) {
          if ($agentService.Restrictions[i].Code === code) return $agentService.Restrictions[i];
        }
        return null;
      };

      $scope.GroupDailyFiguresByAgent = function (rawData) {
        var returnedData = [];
        var holdAgentId = null;
        if (rawData) {
          for (var i = 0; i < rawData.length; i++) {
            if (holdAgentId !== rawData[i].AgentId) {
              var CustomersData = new Array();
              var AgentData = { AgentId: rawData[i].AgentId, StartingDate: rawData[i].StartingDate, EndingDate: rawData[i].EndingDate };
              CustomersData.push(rawData[i]);
              AgentData.CustomerDailyFigures = CustomersData;
              returnedData.push(AgentData);
            }
            else {
              returnedData[returnedData.length - 1].CustomerDailyFigures.push(rawData[i]);
            }
            holdAgentId = rawData[i].AgentId;
          }
        }
        return returnedData;
      };

      $scope.$on("$routeChangeSuccess", function (event, current, previous) {
        $scope.LeftMenuVisible = current.$$route && current.$$route.templateUrl && current.$$route.templateUrl.indexOf('livetickerView.html') < 0;
      });

      $scope.FormatDateTime = function (d, format, timeZone, showFourDigitsYear) {
        if (format == 6 && $rootScope.IsMobile) format = 16;
        return CommonFunctions.FormatDateTime(d, format, timeZone, showFourDigitsYear);
      };

      $scope.FormatMyNumber = function (num, divideByHundred, applyFloor) {
        return CommonFunctions.FormatNumber(num, divideByHundred, applyFloor);
      };

      $scope.DisplayDate = function (strDate) {
        if (strDate == null)
          return "";
        var date = new Date(parseInt(strDate.substr(6)));

        return $scope.FirstLetterUpperCase($scope.FormatDateTime(date, 6, 3, true).toString());
      };

      $scope.FirstLetterUpperCase = function (str) {
        return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase();
      };

      $scope.DisplayTimeFromDate = function (strDate) {
        return $scope.FormatDateTime(strDate, 8, 3, true) + $scope.FormatDateTime(strDate, 14, 3, true);
      };

      $scope.DisplayMyNumber = function (num) {
        return $scope.FormatMyNumber(num, false, false);
      };

      $scope.Logout = function () {
        $agentService.KillTheSession();
      };

      $scope.SaveLearnedText = function () {
        $translatorService.SaveLearnedTexts();
      }

      $scope.ShowCustomerDialog = function (customerObj, option, customer) {
        if (option === 3 && $rootScope.IsMobile) {
          window.location.href = '#/openbets?customerID=' + customer;
          return;
        }
        $scope.LookupCustomer = customerObj;
        $scope.CustomerOption = option;
        $scope.DialogTemplate = "/app/components/customer/customerView.html";
        document.getElementById("page-content-wrapper").classList.add('no-printable');
        jQuery.noConflict();
        (function ($) {
          $('#modalDialog').modal({
            backdrop: 'static',
            keyboard: false
          }).removeData("modal").modal({ backdrop: 'static', keyboard: false });
        })(jQuery);
      };

      $scope.CloseDialog = function () {
        (function ($) {
          $('#modalDialog').modal('hide');
          $scope.DialogTemplate = "";
          document.getElementById("page-content-wrapper").classList.remove('no-printable');
        })(jQuery);
      };

      $scope.GetChosenTeamId = function (wi, toUpper) {
        var chosenTeam = "";
        if (!wi || !wi.ItemWagerType) return '';
        switch (wi.ItemWagerType) {
          case "S":
          case "M":
          case "E":
            if (wi.ChosenTeamID === wi.Team1ID) {
              chosenTeam = wi.Team1RotNum + " " + wi.ChosenTeamID;
            } else {
              chosenTeam = wi.Team2RotNum + " " + wi.ChosenTeamID;
            }
            break;
          case "L":
          case "C":
            chosenTeam = wi.ChosenTeamID;
            break;
        }
        if (toUpper)
          return chosenTeam.toUpperCase();
        else
          return chosenTeam;
      };

      $scope.GetWagerDescription = function (wi) {
        if (!wi) return "";
        var chosenTeam = $scope.GetChosenTeamId(wi, false);
        var chosenSport = (wi.SportType).trim();
        var itemDescription = wi.Description;
        var ds = itemDescription.replace(chosenTeam, "").replace(chosenSport, "").replace("-", "").trim();
        return itemDescription.replace(chosenTeam, "").replace(chosenSport, "").replace("-", "").trim();
      };

      $scope.OpenLiveLines = function () {
        var form = document.createElement("form");
        var element1 = document.createElement("input");
        var element2 = document.createElement("input");
        var element3 = document.createElement("input");
        form.method = "POST";
        form.action = SETTINGS.MainSite;

        element1.value = $scope.AgentAsCustomerInfo.CustomerID.trim();
        element1.name = "agentID";
        element1.type = "hidden";
        form.appendChild(element1);

        element2.value = "DEMO";
        element2.name = "customerID";
        element2.type = "hidden";
        form.appendChild(element2);

        element3.value = "";
        element3.name = "password";
        element3.type = "hidden";
        form.appendChild(element3);

        document.body.appendChild(form);

        form.setAttribute("target", "_blank");
        form.submit();

      };

      $scope.IsActive = function (menu) {
        if (!$route.current) return false;
        switch (menu) {
          case 'Dashboard':
            return 'dashboardController' === $route.current.controller;
          case 'Customer':
            return 'customersController' === $route.current.controller;
          case 'Performance':
            return '/performance' === $route.current.originalPath;
          case 'Daily':
            return 'dailyFiguresController' === $route.current.controller && !$route.current.params.ShowSettleFigure;
          case 'Settle':
            return 'dailyFiguresController' === $route.current.controller && $route.current.params.ShowSettleFigure;
          case 'Open':
            return 'openbetsController' === $route.current.controller;
          case 'Game':
            return 'agentpositionController' === $route.current.controller;
          case 'Distribution':
            return 'agentdistributionController' === $route.current.controller && !$route.current.params.SummaryOnly;
          case 'Limits':
            return 'sportsLimitsController' === $route.current.controller;
        }
        return false;
      };

      $scope.SwitchToClassic = function () {
        var customerId = $scope.AgentInfo.AgentID.trim();
        var customerPwd = $scope.AgentInfo.Password;
        $agentService.LogWritter("Agent " + customerId + " switched to classic", "switch site");
        $agentService.KillTheSession().then(function () {
          $('#customerID').val(customerId);
          $('#password').val(customerPwd);
          document.forms["loginform"].submit();
        });
      };
    
      $scope.LineEntry = function () {
          var form1 = document.createElement("form");
          var customerId = $scope.AgentInfo.AgentID.trim();
          var customerPwd = $scope.AgentInfo.Password;
          var element1 = document.createElement("input");
          var element2 = document.createElement("input");
          var element3 = document.createElement("input");
          var element4 = document.createElement("input");
          var element5 = document.createElement("input");
          form1.setAttribute("target", "_blank");
          form1.method = "POST";
          form1.action = "https://engine.trdwd.ec/LoginVerify.asp";

          element1.value = customerId;
          element1.name = "customerID";
          element1.type = "hidden";
          form1.appendChild(element1);

          element2.value = customerPwd;
          element2.name = "password";
          element2.type = "hidden";
          form1.appendChild(element2);
          
          element3.value = "tradewinds";
          element3.name = "site";
          element3.type = "hidden";
          form1.appendChild(element3);
          
          element4.value = "trdwd.ec";
          element4.name = "DomainURL";
          element4.type = "hidden";
          form1.appendChild(element4);
          
          element5.value = "lineentry";
          element5.name = "aid";
          element5.type = "hidden";
          form1.appendChild(element5);

          document.body.appendChild(form1);

          form1.submit();
       
      };
    

      //$scope.LineEntry = function () {
      //    var customerId = $scope.AgentInfo.AgentID.trim();
      //    var customerPwd = $scope.AgentInfo.Password;
      //    $agentService.LogWritter("Agent " + customerId + " Line Entry", "switch site");
      //    $agentService.KillTheSession().then(function () {
      //        $('#customerID1').val(customerId);
      //        $('#password1').val(customerPwd);
      //        document.forms["LineEntryForm"].submit();
      //    });
      //};

      $scope.OpenLiveTicker = function () {
        var form = document.createElement("form");
        var element1 = document.createElement("input");
        var element2 = document.createElement("input");
        form.setAttribute("target", "_blank");
        form.method = "POST";
        form.action = "#/liveticker";

        element1.value = $scope.AgentInfo.AgentID.trim();
        element1.name = "customerID1";
        element1.type = "hidden";
        form.appendChild(element1);

        element2.value = $scope.AgentInfo.Password;
        element2.name = "password1";
        element2.type = "hidden";
        form.appendChild(element2);

        document.body.appendChild(form);

        form.submit();

      };

      $scope.CloseMenu = function () {
        if ($rootScope.IsMobile) {
          $("#wrapper").removeClass("toggled");
        }
      };

      var Scheduling = setTimeout(function tick() {
        $agentService.TrackStatus().then();
        Scheduling = setTimeout(tick, SETTINGS.TrackTime);
      }, SETTINGS.TrackTime);

      $scope.Init();
    }
]);