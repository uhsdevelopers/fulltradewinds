﻿using System.Web.Script.Services;
using System.Web.Services;
using System;
using System.IO;

namespace SIDWebAgents.Services
{
    /// <summary>
    /// Summary description for AppService
    /// </summary>
    [ScriptService]
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class TranslatorService : CoreService
    {


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string SaveTranslations(String lang, String json)
        {
            using (var wr = new StreamWriter(Server.MapPath("/Data/Lang/" + lang + ".json"), false))
            {
                wr.Write(json);
                wr.Close();
            }

            return "Ok";
        }
    }
}