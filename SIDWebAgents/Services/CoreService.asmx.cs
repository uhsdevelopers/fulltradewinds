﻿using System;
using System.ComponentModel;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.SessionState;
using InstanceManager.BusinessLayer;
using SIDLibraries.Utilities;
using SIDWebLibraries.HelperClasses;

namespace SIDWebAgents.Services {
  /// <summary>
  /// Summary description for CoreService
  /// </summary>
  [ScriptService]
  [WebService(Namespace = "http://tempuri.org/")]
  [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
  [ToolboxItem(false)]
  // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
  // [System.Web.Script.Services.ScriptService]
  public class CoreService : WebService, IRequiresSessionState {
    protected readonly ModuleInfo InstanceModuleInfo;


    public CoreService() {
        if (SessionVariablesCore.InstanceManager != null)
      InstanceModuleInfo = SessionVariablesCore.InstanceManager.InstanceModuleInfo;
    }

    protected void Log(Exception ex) {
      if (InstanceModuleInfo == null) return;
      using (var lg = new LogWriter(InstanceModuleInfo)) {
        lg.WriteToSystemLog(ex);
      }

    }

  }
}
