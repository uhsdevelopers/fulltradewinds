﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using SIDLibraries.BusinessLayer;
using SIDWebAgents.HelperClasses;
using SIDWebLibraries.HelperClasses;
using SystemLog = SIDWebLibraries.HelperClasses.SystemLog;

namespace SIDWebAgents.Services {
  /// <summary>
  /// Summary description for AgentService
  /// </summary>
  [ScriptService]
  [WebService(Namespace = "http://tempuri.org/")]
  [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
  [ToolboxItem(false)]
  public class AgentService : CoreService {

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetAgentInfo() {
      var rc = new ResultContainer();
      if (rc.SessionExpired) return rc;
      using (var ag = new Agents(InstanceModuleInfo)) {
        SessionVariablesCore.AgentInfo = ag.GetAgent(SessionVariables.AgentId, SessionVariables.Password, "");
      }
      return SessionVariablesCore.AgentInfo != null ? new ResultContainer { Data = SessionVariablesCore.AgentInfo } : new ResultContainer { Data = null };
      //throw new Exception("Agent Info is not present.");

      //}
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetAgentAsCustomerInfo() {
      var rc = new ResultContainer();
      if (rc.SessionExpired) return rc;
      using (var cust = new Customers(InstanceModuleInfo)) {
        var restrictions = cust.GetRestrictionsByCustomer(SessionVariables.AgentId);
        var settings = cust.GetWebCustomerSettings(SessionVariables.AgentId);
        return SessionVariables.AgentAsCustomerInfo != null ? new ResultContainer { Data = new { SessionVariables.AgentAsCustomerInfo, Restrictions = restrictions, Settings = settings } } : new ResultContainer { Data = null };
      }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetCustomersInfo(int weekNumber) {
      var rc = new ResultContainer();
      if (rc.SessionExpired) return rc;
      using (var ag = new Agents(InstanceModuleInfo)) {
        var data = ag.GetCustomersInfo(SessionVariables.AgentId, weekNumber).GroupBy(g => g.AgentId);
        return new ResultContainer { Data = data };
      }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetAgentsCurrentBalance() {
      var rc = new ResultContainer();
      if (rc.SessionExpired) return rc;
      using (var ag = new Agents(InstanceModuleInfo)) {
        return new ResultContainer { Data = ag.GetAgentsCurrentBalance(SessionVariables.AgentId) };
      }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetCustomerBalance(string customerId) {
      var rc = new ResultContainer();
      return rc.SessionExpired ? rc : new ResultContainer { Data = new CustomerBalanceInfo(customerId, SessionVariablesCore.InstanceManager.InstanceModuleInfo) };
    }



    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetAgentCashTransactions(int weekNumber, string agentId) {
      var rc = new ResultContainer();
      if (rc.SessionExpired) return rc;
      var agValue = agentId;
      if (string.IsNullOrEmpty(agentId)) {
        agValue = SessionVariables.AgentId;
      }

      using (var ag = new Agents(InstanceModuleInfo)) {
        return new ResultContainer { Data = ag.GetAgentCashTransactions(agValue, weekNumber) };
      }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetCustomerTransactions(string customerId, int weekNumber) {
      var rc = new ResultContainer();
      if (rc.SessionExpired) return rc;
      string tmpCustomerId;
      if (string.IsNullOrEmpty(customerId)) {
        if (SessionVariables.CustomerId != "") {
          tmpCustomerId = SessionVariables.CustomerId;
        }
        else {
          tmpCustomerId = customerId;
          SessionVariables.CustomerId = customerId;
        }
      }
      else {
        tmpCustomerId = customerId;
        SessionVariables.CustomerId = customerId;
      }

      if (string.IsNullOrEmpty(tmpCustomerId)) return new ResultContainer {Data = "No customer found."};
        using (var tran = new Transactions(InstanceModuleInfo)) {
          return new ResultContainer { Data = tran.GetCustomerTransactionListByWeek(tmpCustomerId, weekNumber) };
        }
      }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetCustomerTransactionListByDays(string customerId, int numDays) {
      var rc = new ResultContainer();
      if (rc.SessionExpired) return rc;
      try {
        using (var ta = new Transactions(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
          rc.Data = ta.GetCustomerTransactionListByDays(customerId, numDays);
          SystemLog.WriteAccessLog("Agent " + customerId.Trim() + " viewed report " + customerId.Trim(), "All Transactions");
        }
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetAgentTransactionListByDays(int numDays) {
      var rc = new ResultContainer();
      if (rc.SessionExpired) return rc;
      try {
        using (var ta = new Transactions(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
          rc.Data = ta.GetAgentTransactionListByDays(SessionVariables.AgentId, numDays);
          SystemLog.WriteAccessLog("Agent " + SessionVariables.AgentId.Trim() + " viewed report", "All Transactions");
        }
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetCustomerTransactionByGradeNumber(int gradeNumber) {
      var rc = new ResultContainer();
      if (rc.SessionExpired) return rc;
      using (var tran = new Transactions(InstanceModuleInfo)) {
        return new ResultContainer { Data = tran.GetCustomerTransactionByGradeNumber(gradeNumber) };
      }
    }

    /*[WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetCustomersDailyFigures(int weekNumber) {
      var rc = new ResultContainer();
      if (rc.SessionExpired) return rc;
      using (var ag = new Agents(InstanceModuleInfo)) {
        return new ResultContainer { Data = ag.GetCustomersDailyFigures(SessionVariables.AgentId, weekNumber) };
      }
    }*/

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetCustomersDailyFigures(int weekNumber) {
      var rc = new ResultContainer();
      if (rc.SessionExpired) return rc;
      using (var ag = new Agents(InstanceModuleInfo)) {
        var temp = new ArrayList();
        var dfs = ag.GetCustomersDailyFigures(SessionVariables.AgentId, weekNumber).GroupBy(d => d.AgentId).ToList();
        foreach (var df in dfs) {
          var adf = df.First();
          temp.Add(new { AgentId = adf.AgentId, StartingDate = adf.StartingDate, EndingDate = adf.EndingDate, CustomerDailyFigures = df });
        }
        return new ResultContainer { Data = temp };
      }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetCustomerDailyFigures(string customerId, int weekOffset, string currencyCode) {
      var rc = new ResultContainer();
      if (rc.SessionExpired) return rc;
      try {
        using (var df = new DailyFigures(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
          var res = df.GetByCustomer(customerId, weekOffset, currencyCode);

          if (res == null)
            return rc;
          var report = new {
            CashInOutTotal = (from r in res
                              select r.CashInOut ?? 0).Sum(),
            EndingBalance = 0.0,
            StartingBalance = res.First().StartingBalace,
            WeekOffset = weekOffset,
            WinLossTotal = (from r in res
                            select r.WinLoss ?? 0).Sum(),
            ZeroBalance = res.First().ZeroBal,
            ValuesPerDay = (from r in res
                            select new {
                              r.ThisDate,
                              r.CashInOut,
                              r.WinLoss,
                              r.CasinoWinLoss,
                              Wagers = new List<object>()
                            }).ToList(),
            CasinoWinLossTotal = (from r in res
                                  select r.CasinoWinLoss ?? 0).Sum()
          };
          rc.Data = report;

          SystemLog.WriteAccessLog("Agent " + customerId.Trim() + " viewed report " + customerId.Trim(), "Daily Figures");
        }
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetCashTransactionsByDate(string customerId, string date) {
      var rc = new ResultContainer();
      if (rc.SessionExpired) return rc;
      date = new string(date.Where(c => char.IsNumber(c) || c == '/').ToArray());
      try {
        using (var ta = new Transactions(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
          var res = ta.GetCashTransactionsByDate(customerId, DateTime.Parse(date));

          if (res == null) return rc;
          var report = (from r in res
                        select new {
                          r.DocumentNumber,
                          r.TranCode,
                          TranType = new {
                            code = r.TranType,
                            name = (r.TranType == "E" ? "Deposit" : r.TranType == "C" ? "Credit Adjustment" : r.TranType == "T" ? "Transfer Credit" :
                                r.TranType == "I" ? "Retired Cash" : r.TranType == "D" ? "Debit Adjustment" : r.TranType == "U" ? "Transfer Debit" : r.TranType == "Q" ? "Agent Distribution" :
                                "Refound")
                          },
                          r.Amount,
                          r.ShortDesc,
                          r.TranDateTime
                        }).ToList();

          rc.Data = report;
          SystemLog.WriteAccessLog("Agent " + customerId.Trim() + " viewed report " + customerId.Trim(), "Daily figures/Cash transactions");
        }
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetCustomerTransactionListByDate(string customerId, string date, bool includeCasino) {
      var rc = new ResultContainer();
      if (rc.SessionExpired) return rc;
      date = new string(date.Where(c => char.IsNumber(c) || c == '/').ToArray());
      try {
        using (var ta = new Transactions(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
          rc.Data = ta.GetTransactionListByDate(customerId, DateTime.Parse(date), includeCasino);
          SystemLog.WriteAccessLog("Agent " + customerId.Trim() + " viewed report " + customerId.Trim(), "Transactios by date");
        }
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetCasinoTransactionsByDate(string customerId, string date) {
      var rc = new ResultContainer();
      if (rc.SessionExpired) return rc;
      date = new string(date.Where(c => char.IsNumber(c) || c == '/').ToArray());
      try {
        using (var ta = new Transactions(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
          rc.Data = ta.GetCasinoTransactionsByDate(customerId, DateTime.Parse(date));
          SystemLog.WriteAccessLog("Agent " + customerId.Trim() + " viewed report " + customerId.Trim(), "Daily figures/Casino transactions");
        }
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer UpdateCustomerAccess(string customerId, string accessCode, string accessValue) {
      var rc = new ResultContainer(); 
      if (rc.SessionExpired) return rc;
      try {
        using (var ag = new Agents(InstanceModuleInfo)) {
          rc.Data = ag.UpdateCustomerAccess(customerId, accessCode, accessValue);
          SystemLog.WriteAccessLog(
              "Agent " + SessionVariables.AgentId.Trim() + " updated customer " + customerId.Trim(),
              "Customer Actions");

        }
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer UpdateCustomer(string customerId, string password, string firstName, string middleName, string lastName, string address, string city, string state, string zip) {
      var rc = new ResultContainer();
      if (rc.SessionExpired) return rc;
      try {
        using (var ag = new Agents(InstanceModuleInfo)) {
          rc.Data =
              ag.UpdateCustomer(customerId, password.ToUpper(), firstName, middleName, lastName, address, city,
                  state, zip);
          SystemLog.WriteAccessLog(
              "Agent " + SessionVariables.AgentId.Trim() + " updated customer " + customerId.Trim(),
              "Customer Actions");

        }
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetAgentPerformance(string period, string wagerType, string customerId) {
      var rc = new ResultContainer();
      if (rc.SessionExpired) return rc;
      using (var df = new DailyFigures(InstanceModuleInfo)) {
        var custId = customerId;
        if (string.IsNullOrEmpty(custId)) {
          custId = SessionVariables.AgentId;
        }
        rc.Data = df.GetWebCustomerPerformanceByPeriodAndWagerType(custId, period, wagerType);
        SystemLog.WriteAccessLog("Agent " + SessionVariables.AgentId.Trim() + " viewed report", "Agent performance");
        return rc;

      }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetAgentCustomersList() {
      var rc = new ResultContainer(); if (rc.Code == ResultContainer.ResultCode.SessionExpired) return rc;
      using (var ag = new Agents(InstanceModuleInfo)) {
        return new ResultContainer { Data = ag.GetAgentCustomersList(SessionVariables.AgentId) };
      }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetCustomerOpenBets(string customerId, string wagerType, bool continueOnPush, bool checkForAR, string sportType, bool noProps) {
      var rc = new ResultContainer(); 
      if (rc.SessionExpired) return rc;
      using (var ag = new Agents(InstanceModuleInfo)) {
        rc.Data = ag.GetCustomerOpenBets(SessionVariables.AgentId, customerId, wagerType, continueOnPush, checkForAR, sportType, noProps);
        SystemLog.WriteAccessLog("Agent " + customerId.Trim() + " viewed report", "Open Bets");
        return rc;
      }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetCustomerPendingBets(string customerId) {
      var rc = new ResultContainer(); 
      if (rc.SessionExpired) return rc;
      try {
        using (var ta = new TicketsAndWagers(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
          rc.Data = ta.GetCustomerPendingBets(customerId);
          SystemLog.WriteAccessLog("Agent " + customerId.Trim() + " viewed report", "Open Bets");
        }
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetSportTypesList() {
      var rc = new ResultContainer(); 
      if (rc.SessionExpired) return rc;
      using (var sp = new SportTypes(InstanceModuleInfo)) {
        return new ResultContainer { Data = sp.GetSportTypes() };
      }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetAgentDistribution() {
      var rc = new ResultContainer(); 
      if (rc.SessionExpired) return rc;
      using (var ag = new Agents(InstanceModuleInfo)) {
        rc.Data = ag.GetWebAgentDistributionInfo(SessionVariables.AgentId);
        SystemLog.WriteAccessLog("Agent " + SessionVariables.AgentId.Trim() + " viewed report", "Agent position by game");
        return rc;
      }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetActivePeriodsAndSports(int weekNumber) {
      var rc = new ResultContainer(); 
      if (rc.SessionExpired) return rc;
      using (var ag = new Agents(InstanceModuleInfo)) {
        return new ResultContainer { Data = ag.GetActivePeriodsAndSports(SessionVariables.AgentId, weekNumber) };
      }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetAgentPositionByGame(String sportType, String sportSubType, int periodNumber, int weekNum) {
      var rc = new ResultContainer(); 
      if (rc.SessionExpired) return rc;
      using (var ag = new Agents(InstanceModuleInfo)) {
        rc.Data = ag.GetWebAgentPosition(sportType, sportSubType, periodNumber, SessionVariables.AgentId, weekNum);
        SystemLog.WriteAccessLog("Agent " + SessionVariables.AgentId.Trim() + " viewed report", "Agent position by game");
        return rc;
      }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetDatesSinceCloseOfWeek() {
      var rc = new ResultContainer(); 
      if (rc.SessionExpired) return rc;
      using (var prn = new SystemParameters(InstanceModuleInfo)) {
        return new ResultContainer { Data = prn.GetDatesSinceCloseOfWeek() };
      }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetCustDailyFigureByAgentDashboard(int weekNumber) {
      var rc = new ResultContainer(); 
      if (rc.SessionExpired) return rc;
      using (var ag = new Agents(InstanceModuleInfo)) {
        return new ResultContainer { Data = ag.GetCustDailyFigureByAgentDashboard(SessionVariables.AgentId, weekNumber, SessionVariables.AgentAsCustomerInfo.AgentType) };
      }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetActivePlayersCount() {
      var rc = new ResultContainer(); 
      if (rc.SessionExpired) return rc;
      using (var ag = new Agents(InstanceModuleInfo)) {
        return new ResultContainer { Data = ag.GetActivePlayersCount(SessionVariables.AgentId, SessionVariables.AgentAsCustomerInfo.AgentType) };
      }
    }



    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer InsertCustomerTransaction(String customerId, String tranCode, string tranType, float amount, string paymentBy, string description, string dailyFigureDate, bool freePlayTransaction) {
      var rc = new ResultContainer(); 
      if (rc.SessionExpired) return rc;
      if (dailyFigureDate != null) dailyFigureDate = new string(dailyFigureDate.Where(c => char.IsNumber(c) || c == '/').ToArray());
      using (var tran = new Transactions(InstanceModuleInfo)) {
        try {
          rc.Data = tran.InsertCustomerWebTransacion(customerId, tranCode, tranType, amount, paymentBy, description,
              SessionVariables.AgentId, dailyFigureDate == null ? (DateTime?)null : DateTime.Parse(dailyFigureDate),
              freePlayTransaction);
          SystemLog.WriteAccessLog("Agent " + SessionVariables.AgentId.Trim() + " inserted transaction to " + customerId.Trim(), "Customer Actions");
          return rc;

        }
        catch (Exception e) {
          return new ResultContainer { Data = "Error inserting transaction: " + e.Message, Code = ResultContainer.ResultCode.Fail };
        }
      }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer ChangeCustomerCreditLimits(String customerId, float creditLimit) {
      var rc = new ResultContainer(); 
      if (rc.SessionExpired) return rc;
      using (var ag = new Agents(InstanceModuleInfo)) {
        try {
          rc.Data = ag.ChangeCustomerCreditLimits(customerId, creditLimit, null);
          SystemLog.WriteAccessLog("Agent " + SessionVariables.AgentId.Trim() + " changed credit limit to " + customerId.Trim(), "Customer Actions");
          return rc;
        }
        catch (Exception e) {
          return new ResultContainer { Data = "Error inserting transaction: " + e.Message, Code = ResultContainer.ResultCode.Fail };
        }
      }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer ChangeCustomerSettleFigure(String customerId, float settleFigure) {
      var rc = new ResultContainer(); 
      if (rc.SessionExpired) return rc;
      using (var ag = new Agents(InstanceModuleInfo)) {
        try {
          rc.Data = ag.ChangeCustomerSettleFigure(customerId, settleFigure);
          SystemLog.WriteAccessLog("Agent " + SessionVariables.AgentId.Trim() + " changed settle figure to " + customerId.Trim(), "Customer Actions");
        }
        catch (Exception e) {
          rc.Data = "Error inserting transaction: " + e.Message;
          rc.Code = ResultContainer.ResultCode.Fail;

        }
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer ChangeCustomerTempCreditLimits(String customerId, float creditLimit, string tempCreditAdjExpDate) {
      var rc = new ResultContainer(); 
      if (rc.SessionExpired) return rc;
      if (tempCreditAdjExpDate != null) tempCreditAdjExpDate = new string(tempCreditAdjExpDate.Where(c => char.IsNumber(c) || c == '/').ToArray());
      using (var ag = new Agents(InstanceModuleInfo)) {
        try {
          rc.Data = ag.ChangeCustomerTempCreditLimits(customerId, creditLimit, tempCreditAdjExpDate == null ? (DateTime?)null : DateTime.Parse(tempCreditAdjExpDate));
          SystemLog.WriteAccessLog("Agent " + SessionVariables.AgentId.Trim() + " changed temp credit limit to " + customerId.Trim(), "Customer Actions");
        }
        catch (Exception e) {
          rc.Data = "Error inserting transaction: " + e.Message;
          rc.Code = ResultContainer.ResultCode.Fail;

        }
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer ChangeCustomerQuickLimits(String customerId, float quickLimit) {
      var rc = new ResultContainer(); 
      if (rc.SessionExpired) return rc;
      using (var ag = new Agents(InstanceModuleInfo)) {
        try {
          rc.Data = ag.ChangeCustomerQuickLimits(customerId, quickLimit);
          SystemLog.WriteAccessLog("Agent " + SessionVariables.AgentId.Trim() + " changed quick limit to " + customerId.Trim(), "Customer Actions");
        }
        catch (Exception e) {
          rc.Data = "Error inserting transaction: " + e.Message;
          rc.Code = ResultContainer.ResultCode.Fail;
        }
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer ChangeCustomerTempQuickLimits(String customerId, float quickLimit, string tempQuickAdjExpDate) {
      var rc = new ResultContainer(); 
      if (rc.SessionExpired) return rc;
      if (tempQuickAdjExpDate != null) tempQuickAdjExpDate = new string(tempQuickAdjExpDate.Where(c => char.IsNumber(c) || c == '/').ToArray());
      using (var ag = new Agents(InstanceModuleInfo)) {
        try {
          rc.Data = ag.ChangeCustomerTempQuickLimits(customerId, quickLimit, tempQuickAdjExpDate == null ? (DateTime?)null : DateTime.Parse(tempQuickAdjExpDate));
          SystemLog.WriteAccessLog("Agent " + SessionVariables.AgentId.Trim() + " changed quick limit to " + customerId.Trim(), "Customer Actions");
          return rc;
        }
        catch (Exception e) {
          return new ResultContainer { Data = "Error inserting transaction: " + e.Message, Code = ResultContainer.ResultCode.Fail };
        }
      }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetLiveTickerInfo(String wagerTypeIdx, String wagerAmtsIdx) {
      var rc = new ResultContainer(); 
      if (rc.SessionExpired) return rc;
      using (var ag = new Agents(InstanceModuleInfo)) {
        rc.Data = ag.GetLiveTickerInfo(SessionVariables.AgentId, wagerTypeIdx, wagerAmtsIdx);
        SystemLog.WriteAccessLog("Agent " + SessionVariables.AgentId.Trim() + " viewed report", "Live ticket info");
        return rc;
      }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetWinLossByWeek(int weekNumber) {
      var rc = new ResultContainer(); 
      if (rc.SessionExpired) return rc;
      using (var ag = new Agents(InstanceModuleInfo)) {
        rc.Data = ag.GetWinLossByWeek(SessionVariables.AgentId, SessionVariables.AgentAsCustomerInfo.AgentType, weekNumber);
        return rc;
      }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void LogOut() {
      HttpContext.Current.Session.Abandon();
      HttpContext.Current.Session.Clear();
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetAgentsList() {
      var rc = new ResultContainer(); 
      if (rc.SessionExpired) return rc;
      using (var ag = new Agents(InstanceModuleInfo)) {
        rc.Data = ag.GetAgentsList(SessionVariables.AgentId);
        return rc;
      }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetGameActionByLine(int gameNum, int periodNum, string chosenTeamId, string wagerType) {
      var rc = new ResultContainer(); 
      if (rc.SessionExpired) return rc;
      using (var ag = new TicketsAndWagers(InstanceModuleInfo)) {
        rc.Data = ag.GetGameActionByLine(gameNum, periodNum, chosenTeamId, wagerType, SessionVariables.AgentId);
        SystemLog.WriteAccessLog("Agent " + SessionVariables.AgentId.Trim() + " viewed report", "Game action by line");

        return rc;
      }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer GetDetailWagerLimits(string userId, string sportType, int period) {
      var rc = new ResultContainer(); 
      if (rc.SessionExpired) return rc;
      using (var ag = new Agents(InstanceModuleInfo)) {
        rc.Data = ag.GetDetailWagerLimits(userId, sportType, period);
        SystemLog.WriteAccessLog("Agent " + SessionVariables.AgentId.Trim() + " viewed report", "detail wager limits");
        return rc;
      }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer UpdateCustomerSportsLimits(int? cuSpread, int? inetSpread, int? cuMoney, int? inetMoney, int? cuTotal, int? inetTotal, int period, string sportType, string sportSubType, string customerId) {
      var rc = new ResultContainer(); 
      if (rc.SessionExpired) return rc;
      using (var ag = new Agents(InstanceModuleInfo)) {
        try {
          rc.Data = ag.UpdateCustomerSportsLimits(cuSpread, inetSpread, cuMoney, inetMoney, cuTotal, inetTotal, period, sportType, sportSubType.Trim(), customerId);
          var log = "Agent " + SessionVariables.AgentId.Trim() + " updated " + (sportType??"").Trim() + "/" + (sportSubType??"").Trim() + " limits to " + customerId.Trim();
          SystemLog.WriteAccessLog(log, "Customer Actions");
          TelegramApi.SendTelegram(log);
        }
        catch (Exception ex) {
          Log(ex);
          rc.SetError(ex.Message);

        }
        return rc;
      }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer AddCustomerMessage(String customerId, DateTime msgExpirationDateTime, string priority, String msgSubject, String msgBody) {
      var rc = new ResultContainer(); 
      if (rc.SessionExpired) return rc;
      try {
        using (var ta = new Customers(SessionVariablesCore.InstanceManager.InstanceModuleInfo)) {
          rc.Data = ta.AddCustomerMessage(customerId, msgExpirationDateTime, "U", priority, msgSubject, msgBody, null);
          SystemLog.WriteAccessLog("Agent " + customerId.Trim() + " send message", "AddCustomerMessage/Ag");
        }
      }
      catch (Exception ex) {
        Log(ex);
        rc.SetError(ex.Message);
      }
      return rc;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void LogWritter(string operation, string data) {
      SystemLog.WriteAccessLog(operation, data);
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ResultContainer TrackStatus() {
      return new ResultContainer(false);
    }

  }
}
