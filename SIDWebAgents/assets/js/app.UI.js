﻿var UI = UI || {};

UI.confirmationShown = false;

UI.Alert = function (msg) {
    alert(msg);
};

UI.Alert = function (msg, title, fcnHide) {
    if (title == null) title = "Information";
    BootstrapDialog.show({
        title: title,
        message: msg,
        size: BootstrapDialog.SIZE_SMALL,
        onhide: fcnHide
    });

};

UI.ConfirmOkCancel = function (msg, fcnOk, fcnCancel, title) {
    if (!UI.confirmationShown) {
        if (title == null) title = "Information";
        BootstrapDialog.confirm(msg, function (result) {
            UI.confirmationShown = false;
            if (result && fcnOk) fcnOk();
            else {
                if (!result && fcnCancel) {
                    fcnCancel();
                }
            } return result;
        });
        UI.confirmationShown = true;
    }
};

UI.Confirm = function (msg, fcnOk, fcnCancel, title) {
    if (!UI.confirmationShown) {
        if (title == null) title = "Confirmation";
        new BootstrapDialog({
            title: title,
            message: msg,
            closable: false,
            buttons: [{
                label: 'No',
                action: function (dialog) {
                    if (fcnCancel) fcnCancel();
                    dialog.close();
                    UI.confirmationShown = false;
                }
            }, {
                label: 'Yes',
                cssClass: 'btn-primary',
                action: function (dialog) {
                    if (fcnOk) fcnOk();
                    dialog.close();
                    UI.confirmationShown = false;
                }
            }]
        }).open();
        UI.confirmationShown = true;
    }
};

/* UI.FullScreenModal = function (msg, title, fcnHide) {
    if (title == null) title = "Information";
    BootstrapDialog.show({
        cssClass: 'modal-wide',
        title: title,
        message: msg,
        size: BootstrapDialog.SIZE_SMALL,
        onhide: fcnHide
    });

}; */

UI.ScrollDown = function (sectionId) {
    setTimeout(function () {
        var pos = $('#aScroll').offset().top;
        $('#' + sectionId).animate({
            scrollTop: pos
        }, 'slow');
        //$("#" + sectionId).animate({ scrollTop: $(document).height() }, 1000);
    }, 100);
};

UI.SetFocus = function (inputId) {
    setTimeout(function () {
        $("#" + inputId).focus();
    }, 500);
};

UI.Type = {
    Success: "success",
    Info: "info",
    Warning: "warning",
    Danger: "danger"
};

UI.Position = {
    Center: "center",
    Top: "top",
    Bottom: "bottom",
    Left: "left",
    Right: "right"
}

UI.Notify = function (msg, from, align, offset, type) {
    if (!type) type = "info";
    $.notify({
        message: msg
    }, {
        placement: {
            from: from,
            align: align
        },
        offset: offset,
        type: type
    });
};