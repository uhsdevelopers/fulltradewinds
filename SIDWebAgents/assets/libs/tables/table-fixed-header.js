
(function ($) {

  $.fn.fixedHeader = function (options) {
    var config = {
      topOffset: 38
      //bgColor: 'white'
    };
    if (options) { $.extend(config, options); }

    return this.each(function () {
      var o = $(this);

      var $win = $(window)
        , $body = $('#containerFluid')
        , $head = $('thead.header', o)
        , isFixed = 0;
      var headTop = $head.length && $head.offset().top - config.topOffset;

      function processScroll() {
        if ($win.width() <= 768) {
          config.topOffset = 70;
          headTop = $head.length && $head.offset().top - config.topOffset;
          $('thead.header-copy', o).removeClass('header-copy header-fixed').addClass('header-copy header-fixed no-printable').css({ 'position': 'fixed', 'top': config['topOffset'] });
          o.find('thead.header-copy').width($head.width());

          headerCopyRectify();
          $head.css({
            margin: '0 auto',
            width: o.width(),
            'background-color': config.bgColor
          });
        }
        if (!o.is(':visible')) return;
        if ($('thead.header-copy').length) {
          $('thead.header-copy').width($head.width());
          var i, scrollTop = $body.scrollTop();
        }
        var t = $head.length && $head.offset().top - config.topOffset;
        if (!isFixed && headTop != t) { headTop = t; }
        var parent = o.parent()[0].id;
        var bodyInView = isElementInView($('tbody', o), false, parent),
          headerInView = isElementInView($head, true, parent);
        $('thead.header-copy', o).hide();

        if ((!headerInView && bodyInView)) {
          $('thead.header-copy', o).show().offset({ left: $head.offset().left });
        } else {
          $('thead.header-copy', o).hide();
        }
        // NG: dislocate while iframe page resized. fixed by jeffen@pactera 2015/7/8
        headerCopyRectify();
      }

      // set a broken bone when header copy dislocated
      function headerCopyRectify() {
        o.find('thead.header > tr > th').each(function (i, h) {
          var w = $(h).width();
          o.find('thead.header-copy> tr > th:eq(' + i + ')').width(w);
        });
      }
      function isElementInView(element, fullyInView, parent) {
        var pageTop = $body.scrollTop();
        var pageBottom = pageTop + $body[0].clientHeight;
        var elementTop = $(element).offset().top;
        var elementBottom = elementTop + $(element).height();

        if (fullyInView === true) {
          return ((pageTop < elementTop) && (pageBottom > elementBottom));
        } else {
          return ((elementTop <= pageBottom) && (elementBottom >= pageTop));
        }
      }

      $('.allscrll').on('scroll', processScroll);
      // NG: dislocate while body resized. fixed by jeffen@pactera 2015/7/9
      $win.on('resize', processScroll);

      // hack sad times - holdover until rewrite for 2.1
      $head.on('click', function () {
        if (!isFixed) setTimeout(function () { $win.scrollTop($win.scrollTop() - 47) }, 10);
      });

      $head.clone(true).removeClass('header').addClass('header-copy header-fixed no-printable').css({ 'position': 'fixed', 'top': config['topOffset'] }).appendTo(o);
      o.find('thead.header-copy').width($head.width());

      headerCopyRectify();
      $head.css({
        margin: '0 auto',
        width: o.width(),
        'background-color': config.bgColor
      });
      processScroll();


    });
  };

})(jQuery);