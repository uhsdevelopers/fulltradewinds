﻿window.paceOptions = {
    document: true, // disabled
    eventLag: true,
    restartOnPushState: false,
    restartOnRequestAfter: true,
    maxTime: 500,
    ajax: {
        trackMethods: ['POST', 'GET']
    }

};