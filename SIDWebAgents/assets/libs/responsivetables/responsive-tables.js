﻿
var responsiTable = {};

responsiTable.updateTables = function () {
    jQuery("table.responsive").each(function (i, element) {
        if (jQuery(element).width() > jQuery("#page-content-wrapper").width()) {
            console.log("setting table");
            responsiTable.splitTable(jQuery(element));
        } else {
            responsiTable.unsplitTable(jQuery(element));
        }
    });

    return true;
};

responsiTable.splitTable = function (original) {
    original.wrap("<div class='table-wrapper' />");

    var copy = original.clone();
    copy.find("td:not(:first-child), th:not(:first-child)").css("display", "none");
    copy.removeClass("responsive");

    original.closest(".table-wrapper").append(copy);
    copy.wrap("<div class='pinned' />");
    original.wrap("<div class='scrollable' />");

    responsiTable.setCellHeights(original, copy);
};

responsiTable.unsplitTable = function (original) {
    original.closest(".table-wrapper").find(".pinned").remove();
    original.unwrap();
    original.unwrap();
};

responsiTable.setCellHeights = function (original, copy) {
    var tr = original.find('tr'),
        trCopy = copy.find('tr'),
        heights = [];

    tr.each(function (index) {
        var self = jQuery(this),
          tx = self.find('th, td');

        tx.each(function () {
            var height = jQuery(this).outerHeight(true);
            heights[index] = heights[index] || 0;
            if (height > heights[index]) heights[index] = height;
        });

    });

    trCopy.each(function (index) {
        jQuery(this).height(heights[index]);
    });
}

jQuery(document).ready(function () {
    //var switched = false;
    //responsiTable.updateTables();
    jQuery(window).on("redraw", function () { responsiTable.updateTables(); }); // An event to listen for
    jQuery(window).on("resize", responsiTable.updateTables);
});
