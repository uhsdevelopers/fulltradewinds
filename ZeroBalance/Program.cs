﻿using System;
using System.Configuration;
using GUILibraries;
using ZeroBalance.UI;

namespace ZeroBalance {
    static class Program {
      /// <summary>
      /// The main entry point for the application.
      /// </summary>
      [STAThread]
      static void Main(string[] args) {

        using (var appLoader = new AppLoader(InstanceManager.BusinessLayer.InstanceManager.Applications.ZeroBalance, args, ConfigurationManager.AppSettings["SystemId"])) {
          if (appLoader.IsSafeToStart() && appLoader.UserHasPermission()) {
            using (var frmZeroBalance = new FrmZeroBalance(appLoader.InstanceManager.InstanceModuleInfo)) {
              appLoader.StartApp(frmZeroBalance);
            }
          }
          appLoader.KillApp();
        }
      }
  }
}
