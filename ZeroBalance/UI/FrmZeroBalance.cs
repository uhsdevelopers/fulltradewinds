﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;
using GUILibraries.Forms;
using SIDLibraries.Utilities;

namespace ZeroBalance.UI {
  public partial class FrmZeroBalance : SIDModuleForm {
    #region Public Properties

    #endregion

    #region Private Properties

    #endregion

    #region Private vars

    int _processedAccountsQty;

    ZeroBalanceMode _mode = ZeroBalanceMode.Full;
    DateTime _effectiveDate;
    DateTime _selectedTime;

    #endregion

    #region Constructors

    public FrmZeroBalance(ModuleInfo moduleInfo)
      : base(moduleInfo) {
      AppModuleInfo.MaintAction = SetModuleInMaintenance;
      InitializeComponent();
    }

    #endregion

    #region Private Methods

    private void AddToLog(string message) {
      gvLog.Rows.Add(DateTime.Now.ToString("yyyy-MM-dd hh:mm tt"), message);
      gvLog.ClearSelection();
    }

    private void DumpLogToFile() {
      using (StreamWriter w = File.AppendText("ZeroBalance.log")) {
        foreach (DataGridViewRow row in gvLog.Rows) {
          w.WriteLine(row.Cells[0].Value + "\t" + AppModuleInfo.InstanceInfo.DatabaseName + "\t" + row.Cells[1].Value);
        }
      }
    }

    private void ExecuteThreadedWork(BackgroundWorker sender) {
      using (var zeroBalanceProcess = new SIDLibraries.BusinessLayer.ZeroBalance(AppModuleInfo, _effectiveDate.AddMinutes(_selectedTime.Minute), _mode)) {
        sender.ReportProgress(1);
        zeroBalanceProcess.DeleteRedundantTransactions();

        if (zeroBalanceProcess.Mode == ZeroBalanceMode.BackoutOnly) {
          return;
        }
        if (zeroBalanceProcess.Mode != ZeroBalanceMode.AgentDistributionOnly) {
          _processedAccountsQty = zeroBalanceProcess.ProcessCustomerAccounts();
          sender.ReportProgress(2);
        }
        if (zeroBalanceProcess.Mode == ZeroBalanceMode.ZeroBalanceOnly) {
          return;
        }
        sender.ReportProgress(3);
        zeroBalanceProcess.ExecuteAgentDistribution();
        sender.ReportProgress(4);
      }
    }

    private void ExecuteZeroBalanceProcess() {
      btnGo.Enabled = false;
      gvLog.Rows.Clear();
      AddToLog("");

      var message = "";
      _selectedTime = DateTime.Parse(txtTime.Text);
      _effectiveDate = dtmEffectiveDate.Value.Date.AddHours(_selectedTime.Hour);

      if (message.Length > 0) {
        MessageBox.Show(message, AppModuleInfo.Name, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        return;
      }

      message = "";
      if (!chbBackoutOnly.Checked && CheckIfProcessWasExecutedForDate()) {
        message = "Process was previously executed for selected date.  Please run Backout Only process and then try again.";
      }
      if (message.Length > 0) {
        MessageBox.Show(message, AppModuleInfo.Name, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        btnGo.Enabled = true;
        return;
      }

      AddToLog("Starting Processing in Interactive Mode...");

      if (!chbBackoutOnly.Checked && !chbAgentDistOnly.Checked && !chbZBalOnly.Checked) {
        AddToLog("-- Full Run");
      }
      else if (chbAgentDistOnly.Checked) {
        _mode = ZeroBalanceMode.AgentDistributionOnly;
        AddToLog("-- Agent Distribution Only");
      }
      else if (chbZBalOnly.Checked) {
        _mode = ZeroBalanceMode.ZeroBalanceOnly;
        AddToLog("-- Zero Balance Only");
      }
      else if (chbBackoutOnly.Checked) {
        _mode = ZeroBalanceMode.BackoutOnly;
        AddToLog("-- Backout Only");
      }

      bgwrk.RunWorkerAsync();
    }

    private bool CheckIfProcessWasExecutedForDate() {
      bool recordsFound;
      using (var zeroBalanceProcess = new SIDLibraries.BusinessLayer.ZeroBalance(AppModuleInfo, _effectiveDate.AddMinutes(_selectedTime.Minute), _mode)) {
        int recordsCount = zeroBalanceProcess.CheckForPreviouslyExecutedJob(DateTime.Parse(_selectedTime.ToShortDateString()));
        recordsFound = recordsCount > 0;
      }
      return recordsFound;
    }

    private void FinishProcess() {
      AddToLog("Process Ended.");
      DumpLogToFile();
      btnGo.Enabled = true;
    }

    private static void HandleFormClosing() {
    }

    private void InitializeForm() {
      Text += @" - Week End"; // replace this harcoded text with a dynamic source *
      var now = DateTime.Now;
      dtmEffectiveDate.Value = now;
      txtTime.Text = now.ToString("hh:mm tt");
      AddToLog("");
    }

    private void SetModuleInMaintenance() {
      if (InvokeRequired) {
        Invoke(new MaintenanceAction(SetModuleInMaintenance));
      }
      else {
        Close();
      }
    }

    private void ShowProcessProgress(ProgressChangedEventArgs e) {
      var message = "";
      switch (e.ProgressPercentage) {
        case 1:
          message = "Checking for and deleting all redundant transactions.";
          break;
        case 2:
          AddToLog("Processed " + _processedAccountsQty + " customer accounts successfully.");
          break;
        case 3:
          AddToLog("Starting agent distribution...");
          break;
        case 4:
          AddToLog("Agent distribution ended.");
          break;
      }
      AddToLog(message);
    }

    private void ToggleCheckboxes(CheckBox sender) {
      if (!sender.Checked) return;
        switch (sender.Name) {
          case "chbBackoutOnly":
            chbAgentDistOnly.Checked = chbZBalOnly.Checked = false;
            break;
          case "chbAgentDistOnly":
            chbBackoutOnly.Checked = chbZBalOnly.Checked = false;
            break;
          case "chbZBalOnly":
          if (CheckForWeekDay()) {
                chbBackoutOnly.Checked = chbAgentDistOnly.Checked = false;
            }
          else {
                sender.Checked = false;
            }
            break;
        }
      }

    private bool CheckForWeekDay()
    {
        if(((int)ServerDateTime.DayOfWeek + 1) == AppModuleInfo.Parameters.CloseDayOfWeek ||
            MessageBox.Show(@"Process should be run on " + DateTimeF.GetDayOfWeekFromInt(AppModuleInfo.Parameters.CloseDayOfWeek) + "s. Proceed?", @"Zero Balance Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) == DialogResult.OK)
        {
            return true;
        }
        return false;
    }

    #endregion

    #region Events

    private void FrmZeroBalance_Load(object sender, EventArgs e) {
      InitializeForm();
    }

    private void bgwrk_DoWork(object sender, DoWorkEventArgs e) {
      ExecuteThreadedWork((BackgroundWorker)sender);
    }

    private void bgwrk_ProgressChanged(object sender, ProgressChangedEventArgs e) {
      ShowProcessProgress(e);
    }

    private void bgwrk_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
      FinishProcess();
    }

    private void btnGo_Click(object sender, EventArgs e) {
      ExecuteZeroBalanceProcess();
    }

    private void chbAgentDistOnly_CheckedChanged(object sender, EventArgs e) {
      ToggleCheckboxes((CheckBox)sender);
    }

    private void FrmZeroBalance_FormClosing(object sender, FormClosingEventArgs e) {
      HandleFormClosing();
    }

    #endregion
  }
}