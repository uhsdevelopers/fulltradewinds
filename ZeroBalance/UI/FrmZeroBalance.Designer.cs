﻿namespace ZeroBalance.UI
{
    partial class FrmZeroBalance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmZeroBalance));
            this.dtmEffectiveDate = new System.Windows.Forms.DateTimePicker();
            this.lblEffectiveDate = new System.Windows.Forms.Label();
            this.txtTime = new System.Windows.Forms.MaskedTextBox();
            this.btnGo = new System.Windows.Forms.Button();
            this.chbBackoutOnly = new System.Windows.Forms.CheckBox();
            this.chbAgentDistOnly = new System.Windows.Forms.CheckBox();
            this.chbZBalOnly = new System.Windows.Forms.CheckBox();
            this.gvLog = new System.Windows.Forms.DataGridView();
            this.DateAndTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnLog = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bgwrk = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.gvLog)).BeginInit();
            this.SuspendLayout();
            // 
            // dtmEffectiveDate
            // 
            this.dtmEffectiveDate.Location = new System.Drawing.Point(172, 12);
            this.dtmEffectiveDate.Name = "dtmEffectiveDate";
            this.dtmEffectiveDate.Size = new System.Drawing.Size(201, 20);
            this.dtmEffectiveDate.TabIndex = 0;
            // 
            // lblEffectiveDate
            // 
            this.lblEffectiveDate.AutoSize = true;
            this.lblEffectiveDate.Location = new System.Drawing.Point(87, 16);
            this.lblEffectiveDate.Name = "lblEffectiveDate";
            this.lblEffectiveDate.Size = new System.Drawing.Size(78, 13);
            this.lblEffectiveDate.TabIndex = 1;
            this.lblEffectiveDate.Text = "Effective Date:";
            // 
            // txtTime
            // 
            this.txtTime.InsertKeyMode = System.Windows.Forms.InsertKeyMode.Overwrite;
            this.txtTime.Location = new System.Drawing.Point(378, 13);
            this.txtTime.Mask = "90:00 LL";
            this.txtTime.Name = "txtTime";
            this.txtTime.Size = new System.Drawing.Size(55, 20);
            this.txtTime.TabIndex = 2;
            // 
            // btnGo
            // 
            this.btnGo.Location = new System.Drawing.Point(247, 266);
            this.btnGo.Name = "btnGo";
            this.btnGo.Size = new System.Drawing.Size(75, 23);
            this.btnGo.TabIndex = 3;
            this.btnGo.Text = "Go";
            this.btnGo.UseVisualStyleBackColor = true;
            this.btnGo.Click += new System.EventHandler(this.btnGo_Click);
            // 
            // chbBackoutOnly
            // 
            this.chbBackoutOnly.AutoSize = true;
            this.chbBackoutOnly.Location = new System.Drawing.Point(12, 49);
            this.chbBackoutOnly.Name = "chbBackoutOnly";
            this.chbBackoutOnly.Size = new System.Drawing.Size(112, 17);
            this.chbBackoutOnly.TabIndex = 5;
            this.chbBackoutOnly.Text = "Backout Only (/R)";
            this.chbBackoutOnly.UseVisualStyleBackColor = true;
            // 
            // chbAgentDistOnly
            // 
            this.chbAgentDistOnly.AutoSize = true;
            this.chbAgentDistOnly.Location = new System.Drawing.Point(192, 49);
            this.chbAgentDistOnly.Name = "chbAgentDistOnly";
            this.chbAgentDistOnly.Size = new System.Drawing.Size(154, 17);
            this.chbAgentDistOnly.TabIndex = 6;
            this.chbAgentDistOnly.Text = "Agent Distribution Only (/A)";
            this.chbAgentDistOnly.UseVisualStyleBackColor = true;
            this.chbAgentDistOnly.CheckedChanged += new System.EventHandler(this.chbAgentDistOnly_CheckedChanged);
            // 
            // chbZBalOnly
            // 
            this.chbZBalOnly.AutoSize = true;
            this.chbZBalOnly.Location = new System.Drawing.Point(414, 49);
            this.chbZBalOnly.Name = "chbZBalOnly";
            this.chbZBalOnly.Size = new System.Drawing.Size(135, 17);
            this.chbZBalOnly.TabIndex = 7;
            this.chbZBalOnly.Text = "Zero Balance Only (/Z)";
            this.chbZBalOnly.UseVisualStyleBackColor = true;
            this.chbZBalOnly.CheckedChanged += new System.EventHandler(this.chbAgentDistOnly_CheckedChanged);
            // 
            // gvLog
            // 
            this.gvLog.AllowUserToAddRows = false;
            this.gvLog.AllowUserToDeleteRows = false;
            this.gvLog.BackgroundColor = System.Drawing.Color.White;
            this.gvLog.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.gvLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.gvLog.ColumnHeadersVisible = false;
            this.gvLog.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DateAndTime,
            this.ColumnLog});
            this.gvLog.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.gvLog.Location = new System.Drawing.Point(12, 72);
            this.gvLog.MultiSelect = false;
            this.gvLog.Name = "gvLog";
            this.gvLog.ReadOnly = true;
            this.gvLog.RowHeadersVisible = false;
            this.gvLog.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.gvLog.RowTemplate.Height = 16;
            this.gvLog.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.gvLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.gvLog.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gvLog.Size = new System.Drawing.Size(545, 188);
            this.gvLog.TabIndex = 8;
            // 
            // DateAndTime
            // 
            this.DateAndTime.HeaderText = "DateAndTime";
            this.DateAndTime.Name = "DateAndTime";
            this.DateAndTime.ReadOnly = true;
            this.DateAndTime.Visible = false;
            // 
            // ColumnLog
            // 
            this.ColumnLog.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColumnLog.HeaderText = "Log";
            this.ColumnLog.Name = "ColumnLog";
            this.ColumnLog.ReadOnly = true;
            // 
            // bgwrk
            // 
            this.bgwrk.WorkerReportsProgress = true;
            this.bgwrk.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwrk_DoWork);
            this.bgwrk.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgwrk_ProgressChanged);
            this.bgwrk.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwrk_RunWorkerCompleted);
            // 
            // FrmZeroBalance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(569, 301);
            this.Controls.Add(this.gvLog);
            this.Controls.Add(this.chbZBalOnly);
            this.Controls.Add(this.chbAgentDistOnly);
            this.Controls.Add(this.chbBackoutOnly);
            this.Controls.Add(this.btnGo);
            this.Controls.Add(this.txtTime);
            this.Controls.Add(this.lblEffectiveDate);
            this.Controls.Add(this.dtmEffectiveDate);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmZeroBalance";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmZeroBalance_FormClosing);
            this.Load += new System.EventHandler(this.FrmZeroBalance_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gvLog)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtmEffectiveDate;
        private System.Windows.Forms.Label lblEffectiveDate;
        private System.Windows.Forms.MaskedTextBox txtTime;
        private System.Windows.Forms.Button btnGo;
        private System.Windows.Forms.CheckBox chbBackoutOnly;
        private System.Windows.Forms.CheckBox chbAgentDistOnly;
        private System.Windows.Forms.CheckBox chbZBalOnly;
        private System.Windows.Forms.DataGridView gvLog;
        private System.Windows.Forms.DataGridViewTextBoxColumn colLog;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateAndTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnLog;
        private System.ComponentModel.BackgroundWorker bgwrk;
    }
}