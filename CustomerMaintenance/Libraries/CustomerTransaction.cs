﻿using System;

namespace CustomerMaintenance.Libraries {
  class CustomerTransaction {
    public readonly DateTime TranDateTime;
    public readonly int DocumentNumber;
    public readonly String Description;
    public readonly double Credit;
    public readonly double Debit;
    public readonly double Balance;
    public readonly String TranCode;
    public readonly String TranType;
    public readonly DateTime DailyFigureDate;
    public readonly int GradeNumber;


    public CustomerTransaction(DateTime tranDateTime, int documentNumber, String description, double credit, double debit, double balance, String tranCode, String tranType, DateTime dailyFigureDate, int gradeNumber) {
      TranDateTime = tranDateTime;
      DocumentNumber = documentNumber;
      Description = description;
      Credit = credit;
      Debit = debit;
      Balance = balance;
      TranCode = tranCode;
      TranType = tranType;
      DailyFigureDate = dailyFigureDate;
      GradeNumber = gradeNumber;
    }
  }
}