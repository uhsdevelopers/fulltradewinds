﻿using System;
using System.Windows.Forms;

namespace CustomerMaintenance.Libraries {
  public static class GridViewCellCalendar {
    public class CalendarColumn : DataGridViewColumn {
      public CalendarColumn()
        : base(new CalendarCell()) {
      }

      public override DataGridViewCell CellTemplate {
        get {
          return base.CellTemplate;
        }
        set {
          // Ensure that the cell used for the template is a CalendarCell.
          if (value != null &&
              !value.GetType().IsAssignableFrom(typeof(CalendarCell))) {
            throw new InvalidCastException("Must be a CalendarCell");
          }
          base.CellTemplate = value;
        }
      }

      private class CalendarCell : DataGridViewTextBoxCell {

        public CalendarCell() {
          // Use the short date format.
          Style.Format = "d";
        }

        public override void InitializeEditingControl(int rowIndex, object
            initialFormattedValue, DataGridViewCellStyle dataGridViewCellStyle) {
          // Set the value of the editing control to the current cell value.
          base.InitializeEditingControl(rowIndex, initialFormattedValue,
              dataGridViewCellStyle);
          var ctl =
              DataGridView.EditingControl as CalendarEditingControl;
          if (ctl == null) return;
          if (Value != null && Value.ToString() != "")
            ctl.Value = (DateTime)Value;
          else
            ctl.Value = DateTime.Now;
        }

        public override Type EditType {
          get {
            // Return the type of the editing contol that CalendarCell uses.
            return typeof(CalendarEditingControl);
          }
        }

        public override Type ValueType {
          get {
            // Return the type of the value that CalendarCell contains.
            return typeof(DateTime);
          }
        }

        public override object DefaultNewRowValue {
          get {
            // Use the current date and time as the default value.
            return DateTime.Now;
          }
        }
      }

      class CalendarEditingControl : DateTimePicker, IDataGridViewEditingControl {
        DataGridView _dataGridView;
        private bool _valueChanged;
        int _rowIndex;

        public CalendarEditingControl() {
          Format = DateTimePickerFormat.Short;
        }

        // Implements the IDataGridViewEditingControl.EditingControlFormattedValue 
        // property.
        public object EditingControlFormattedValue {
          get {
            return Value.ToShortDateString();
          }
          set {
            if (value is String) {
              Value = DateTime.Parse((String)value);
            }
          }
        }

        // Implements the 
        // IDataGridViewEditingControl.GetEditingControlFormattedValue method.
        public object GetEditingControlFormattedValue(
            DataGridViewDataErrorContexts context) {
          return EditingControlFormattedValue;
        }

        // Implements the 
        // IDataGridViewEditingControl.ApplyCellStyleToEditingControl method.
        public void ApplyCellStyleToEditingControl(
            DataGridViewCellStyle dataGridViewCellStyle) {
          Font = dataGridViewCellStyle.Font;
          CalendarForeColor = dataGridViewCellStyle.ForeColor;
          CalendarMonthBackground = dataGridViewCellStyle.BackColor;
        }

        // Implements the IDataGridViewEditingControl.EditingControlRowIndex 
        // property.
        public int EditingControlRowIndex {
          get {
            return _rowIndex;
          }
          set {
            _rowIndex = value;
          }
        }

        // Implements the IDataGridViewEditingControl.EditingControlWantsInputKey 
        // method.
        public bool EditingControlWantsInputKey(
            Keys key, bool dataGridViewWantsInputKey) {
          // Let the DateTimePicker handle the keys listed.
          switch (key & Keys.KeyCode) {
            case Keys.Left:
            case Keys.Up:
            case Keys.Down:
            case Keys.Right:
            case Keys.Home:
            case Keys.End:
            case Keys.PageDown:
            case Keys.PageUp:
              return true;
            default:
              return false;
          }
        }

        // Implements the IDataGridViewEditingControl.PrepareEditingControlForEdit 
        // method.
        public void PrepareEditingControlForEdit(bool selectAll) {
          // No preparation needs to be done.
        }

        // Implements the IDataGridViewEditingControl
        // .RepositionEditingControlOnValueChange property.
        public bool RepositionEditingControlOnValueChange {
          get {
            return false;
          }
        }

        // Implements the IDataGridViewEditingControl
        // .EditingControlDataGridView property.
        public DataGridView EditingControlDataGridView {
          get {
            return _dataGridView;
          }
          set {
            _dataGridView = value;
          }
        }

        // Implements the IDataGridViewEditingControl
        // .EditingControlValueChanged property.
        public bool EditingControlValueChanged {
          get {
            return _valueChanged;
          }
          set {
            _valueChanged = value;
          }
        }

        // Implements the IDataGridViewEditingControl
        // .EditingPanelCursor property.
        public Cursor EditingPanelCursor {
          get {
            return Cursor;
          }
        }

        protected override void OnValueChanged(EventArgs eventargs) {
          // Notify the DataGridView that the contents of the cell
          // have changed.
          _valueChanged = true;
          EditingControlDataGridView.NotifyCurrentCellDirty(true);
          base.OnValueChanged(eventargs);
        }
      }
    }
  }
}
