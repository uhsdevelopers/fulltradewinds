﻿using System;
using System.Collections.Generic;

namespace CustomerMaintenance.Libraries {

  public class AuditTargetItem {
    public string Code { get; set; }
    public string Description { get; set; }
    public Boolean IsNumber { get; set; }
    public string SourceTable { get; set; }
    public List<PkField> PkFieldsSet { get; set; }
  }

  public class PkField {
    public string Name { get; set; }
    public string Value { get; set; }
  }

  public static class AuditTargetItems {
    public const String SETTLE_FIGURE = "SettleFigure";
    public const String PASSWORD = "Password";
    public const String PAYOUT_PASSWORD = "PayoutPassword";
    public const String CREDIT_LIMIT = "CreditLimit";
    public const String TEMP_CREDIT_ADJ = "TempCreditAdj";
    public const String QUICK_WAGER_LIMIT = "WagerLimit";
    public const String TEMP_QUICK_WAGER_LIMIT = "TempWagerLimit";
    public const String TEMP_QUICK_WAGER_LIMIT_EXPIRATION = "TempWagerLimitExpiration";
    public const String CU_MINIMUM_WAGER = "CUMinimumWager";
    public const String INET_MINIMUM_WAGER = "InetMinimumWager";
    public const String PARLAY_MAX_BET = "CustParlayMaxBet";
    public const String PARLAY_MAX_PAYOUT = "ParlayMaxPayout";
    public const String TEASER_MAX_BET = "TeaserMaxBet";
    public const String CONTEST_MAX_BET = "ContestMaxBet";
    public const String DET_WAGER_LIMITS = "DetailedWagerLimits";
    public const String DET_WAGER_LIMITS_CU = "CULimit";
    public const String DET_WAGER_LIMITS_INET = "InetLimit";
    public const String DET_PARLAY_WAGER_LIMITS = "DetailedParlayWagerLimits";
    public const String DET_PARLAY_WAGER_LIMITS_CU = "CULimit";
    public const String DET_PARLAY_WAGER_LIMITS_INET = "InetLimit";
    public const String DET_WAGER_LIMITS_TEMP_CU = "TempCULimit";
    public const String DET_WAGER_LIMITS_TEMP_INET = "TempInetLimit";

    public static readonly string[] SettleFigure = { SETTLE_FIGURE, "Settle Figure", "tbCustomer" };
    public static readonly string[] Password = { PASSWORD, "Password", "tbCustomer" };
    public static readonly string[] PayoutPassword = { PAYOUT_PASSWORD, "Payout Password", "tbCustomer" };
    public static readonly string[] CreditLimit = { CREDIT_LIMIT, "Credit Limit", "tbCustomer" };
    public static readonly string[] TempCreditAdj = { TEMP_CREDIT_ADJ, "Credit Increase", "tbCustomer" };
    public static readonly string[] QuickWagerLimit = { QUICK_WAGER_LIMIT, "Quick Limit", "tbCustomer" };
    public static readonly string[] CuMinimumWager = { CU_MINIMUM_WAGER, "Call Unit Minimum Wager", "tbCustomer" };
    public static readonly string[] InetMinimumWager = { INET_MINIMUM_WAGER, "Internet Unit Minimum Wager", "tbCustomer" };
    public static readonly string[] ParlayMaxBet = { PARLAY_MAX_BET, "Parlay Limit Maximum Wager", "tbCustomer" };
    public static readonly string[] ParlayMaxPayout = { PARLAY_MAX_PAYOUT, "Parlay Limit Maximum Payout", "tbCustomer" };
    public static readonly string[] TeaserMaxBet = { TEASER_MAX_BET, "Teaser Limit Maximum Wager", "tbCustomer" };
    public static readonly string[] ContestMaxBet = { CONTEST_MAX_BET, "Prop/Future Limit Maximum Wager", "tbCustomer" };
    public static readonly string[] DetWagerLimits = { DET_WAGER_LIMITS, "Detailed Wager Limits", "tbCustWagerLimit" };
    public static readonly string[] DetWagerLimitsCu = { DET_WAGER_LIMITS_CU, "Detailed Wager Limits (CU)", "tbCustWagerLimit" };
    public static readonly string[] DetWagerLimitsInet = { DET_WAGER_LIMITS_INET, "Detailed Wager Limits (Inet)", "tbCustWagerLimit" };
    public static readonly string[] DetParlayTeaserWagerLimits = { DET_PARLAY_WAGER_LIMITS, "Detailed Parlay/Teaser Wager Limits", "tbCustParlayTeaserWagerLimit" };
    public static readonly string[] DetParlayTeaserWagerLimitsCu = { DET_PARLAY_WAGER_LIMITS_CU, "Detailed Parlay/Teaser Wager Limits (CU)", "tbCustParlayTeaserWagerLimit" };
    public static readonly string[] DetParlayTeaserWagerLimitsInet = { DET_PARLAY_WAGER_LIMITS_INET, "Detailed Parlay/Teaser Wager Limits (Inet)", "tbCustParlayTeaserWagerLimit" };
    public static readonly string[] DetTempWagerLimitsCu = { DET_WAGER_LIMITS_CU, "Detailed Temp Wager Limits (CU)", "tbCustWagerLimit" };
    public static readonly string[] DetTempWagerLimitsInet = { DET_WAGER_LIMITS_INET, "Detailed Temp Wager Limits (Inet)", "tbCustWagerLimit" };
  }
}