﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using SIDLibraries.Utilities;

namespace CustomerMaintenance.Libraries {
  public class CustomerInfoUpdater : IDisposable {
    private List<CustomerInfo> _customerInfoFromDbList;
    private List<CustomerStoreShadeGroup> _storeShadeGroupsFromDbList;
    public List<CustomerAvailableTeaser> CustomerAvailableTeasersFromDbList;
    public List<CustomerAvailableBetService> CustomerAvailableBetSvcProfilesFromDbList;
    private List<CustomerCostToBuyPoints> _customerCostToBuyPointsFromDbList;
    private List<CustomerVigDiscountElem> _customerVigDiscountOptions;
    private List<AgentInfo> _customerAsAgentInfoFromDbList;
    private List<CustomerWagerLimit> _customerWagerLimitsOptions;
    private List<CustomerParlayTeaserWagerLimit> _customerParlayTeaserWagerLimitsOptions;
    private List<CustomerTransaction> _customerEnteredTransaction;

    public CustomerRebateInfo CustomerRebateInfoFromDb;

    List<LogWriter.CuAccessLogInfo> _itemsToCuLog;

    public class CustomerInfo {
      public string Active;
      public string Address;
      public string AgentId;
      public string AgentType;
      public string ApplyDetailWagerLimitsFlag;
      public string AutoAgentShadeGroupFlag;
      public string BaseballAction;
      public string BusinessPhone;
      public string CasinoActive;
      public string City;
      public string Comments;
      public string CommentsForCustomer;
      public string CommentsForTw;
      public string ConfirmationDelay;
      public string ContestMaxBet;
      public string Country;
      public string CreditAcctFlag;
      public string CreditLimit;
      public string CuMinimumWager;
      public string Currency;
      public string EasternLineFlag;
      public string EMail;
      public string EnableRifFlag;
      public string EnforceAccumWagerLimitsByLineFlag;
      public string EnforceAccumWagerLimitsFlag;
      public string EstWager101To500Flag;
      public string EstWager501To1000Flag;
      public string EstWager51To100Flag;
      public string EstWager5To50Flag;
      public string EstWagerOver1000Flag;
      public string Fax;
      public string HalfPointCuBasketballDow;
      public string HalfPointCuBasketballFlag;
      public string HalfPointCuFootballDow;
      public string HalfPointCuFootballFlag;
      public string HalfPointInetBasketballDow;
      public string HalfPointInetBasketballFlag;
      public string HalfPointInetFootballDow;
      public string HalfPointInetFootballFlag;
      public string HalfPointWagerLimitFlag;
      public string HalfPointMaxBet;
      public string HomePhone;
      public string InetMinimumWager;
      public string InetTarget;
      public string InstantActionFlag;
      public string IntBaseballFlag;
      public string IntBasketballFlag;
      public string IntFootballFlag;
      public string IntHockeyFlag;
      public string IntHorsesFlag;
      public string IntOtherFlag;
      public string LimitRifToRiskFlag;
      public string LossCap;
      public string NewCustMakeup;
      public string NameFirst;
      public string NameLast;
      public string NameMi;
      public string NoEmailFlag;
      public string NoMailFlag;
      public string NoPhoneFlag;
      public string ParlayMaxBet;
      public string ParlayMaxPayout;
      public string ParlayName;
      public string Password;
      public string PayoutPassword;
      public string PercentBook;
      public string PriceType;
      public string Promoter;
      public string ReferredBy;
      public string SettleFigure;
      public string Source;
      public string State;
      public string StaticLinesFlag;
      public string TeaserMaxBet;
      public string TempCreditAdj;
      public string TempCreditAdjExpDate;
      public string TimeZone;
      public string UsePuckLineFlag;
      public string WagerLimit;
      public string TempWagerLimit;
      public string TempWagerLimitExpiration;
      public string WeeklyLimitFlag;
      public string WiseActionFlag;
      public string ZeroBalanceFlag;
      public string ZeroBalPositiveOnlyFlag;
      public string Zip;
      public string EnforceParlayMaxBetFlag;
      public string EnforceTeaserMaxBetFlag;
      public string HalfPointFootballOn3Flag;
      public string HalfPointFootballOff3Flag; 
      public string HalfPointFootballOn7Flag;
      public string HalfPointFootballOff7Flag;
      public string HalfPointDenyTotalsFlag;

      public CustomerInfo() {
      }

      public CustomerInfo(string active, string address, string agentId, string agentType, string applyDetailWagerLimitsFlag, string autoAgentShadeGroupFlag, string baseballAction,
                                  string businessPhone, string casinoActive, string city, string comments, string commentsForCustomer, string commentsForTw, string confirmationDelay,
                                  string contestMaxBet, string country, string creditAcctFlag, string creditLimit, string cUMinimumWager, string currency, string easternLineFlag,
                                  string eMail, string enableRifFlag, string enforceAccumWagerLimitsByLineFlag, string enforceAccumWagerLimitsFlag, string estWager101To500Flag,
                                  string estWager501To1000Flag, string estWager51To100Flag, string estWager5To50Flag, string estWagerOver1000Flag, string fax, string halfPointCuBasketballDow,
                                  string halfPointCuBasketballFlag, string halfPointCuFootballDow, string halfPointCuFootballFlag, string halfPointInetBasketballDow, string halfPointInetBasketballFlag,
                                  string halfPointInetFootballDow, string halfPointInetFootballFlag, string halfPointWagerLimitFlag, string halfPointMaxBet, string homePhone, string inetMinimumWager,
                                  string inetTarget, string instantActionFlag, string intBaseballFlag, string intBasketballFlag, string intFootballFlag, string intHockeyFlag,
                                  string intHorsesFlag, string intOtherFlag, string limitRifToRiskFlag, string lossCap, string newCustMakeup, string nameFirst, string nameLast, string nameMi,
                                  string noEmailFlag, string noMailFlag, string noPhoneFlag, string parlayMaxBet, string parlayMaxPayout, string parlayName, string password,
                                  string payoutPassword, string percentBook, string priceType, string promoter, string referredBy, string settleFigure, string source,
                                  string state, string staticLinesFlag, string teaserMaxBet, string tempCreditAdj, string tempCreditAdjExpDate, string timeZone, string usePuckLineFlag,
                                  string wagerLimit, string tempWagerLimit, string tempWagerLimitExpiration, string weeklyLimitFlag, string wiseActionFlag, string zeroBalanceFlag, string zeroBalPositiveOnlyFlag, string zip, 
                                  string enforceParlayMaxBetFlag, string enforceTeaserMaxBetFlag,
                                  string halfPointFootballOn3Flag, string halfPointFootballOff3Flag, string halfPointFootballOn7Flag, string halfPointFootballOff7Flag,
                                  string halfPointDenyTotalsFlag
                                    ) {
        Active = active;
        Address = address;
        AgentId = agentId;
        AgentType = agentType;
        ApplyDetailWagerLimitsFlag = applyDetailWagerLimitsFlag;
        AutoAgentShadeGroupFlag = autoAgentShadeGroupFlag;
        BaseballAction = baseballAction;
        BusinessPhone = businessPhone;
        CasinoActive = casinoActive;
        City = city;
        Comments = comments;
        CommentsForCustomer = commentsForCustomer;
        CommentsForTw = commentsForTw;
        ConfirmationDelay = confirmationDelay;
        ContestMaxBet = contestMaxBet;
        Country = country;
        CreditAcctFlag = creditAcctFlag;
        CreditLimit = creditLimit;
        CuMinimumWager = cUMinimumWager;
        Currency = currency;
        EasternLineFlag = easternLineFlag;
        EMail = eMail;
        EnableRifFlag = enableRifFlag;
        EnforceAccumWagerLimitsByLineFlag = enforceAccumWagerLimitsByLineFlag;
        EnforceAccumWagerLimitsFlag = enforceAccumWagerLimitsFlag;
        EstWager101To500Flag = estWager101To500Flag;
        EstWager501To1000Flag = estWager501To1000Flag;
        EstWager51To100Flag = estWager51To100Flag;
        EstWager5To50Flag = estWager5To50Flag;
        EstWagerOver1000Flag = estWagerOver1000Flag;
        Fax = fax;
        HalfPointCuBasketballDow = halfPointCuBasketballDow;
        HalfPointCuBasketballFlag = halfPointCuBasketballFlag;
        HalfPointCuFootballDow = halfPointCuFootballDow;
        HalfPointCuFootballFlag = halfPointCuFootballFlag;
        HalfPointInetBasketballDow = halfPointInetBasketballDow;
        HalfPointInetBasketballFlag = halfPointInetBasketballFlag;
        HalfPointInetFootballDow = halfPointInetFootballDow;
        HalfPointInetFootballFlag = halfPointInetFootballFlag;
        HalfPointWagerLimitFlag = halfPointWagerLimitFlag;
        HalfPointMaxBet = halfPointMaxBet;
        HomePhone = homePhone;
        InetMinimumWager = inetMinimumWager;
        InetTarget = inetTarget;
        InstantActionFlag = instantActionFlag;
        IntBaseballFlag = intBaseballFlag;
        IntBasketballFlag = intBasketballFlag;
        IntFootballFlag = intFootballFlag;
        IntHockeyFlag = intHockeyFlag;
        IntHorsesFlag = intHorsesFlag;
        IntOtherFlag = intOtherFlag;
        LimitRifToRiskFlag = limitRifToRiskFlag;
        LossCap = lossCap;
        NewCustMakeup = newCustMakeup;
        NameFirst = nameFirst;
        NameLast = nameLast;
        NameMi = nameMi;
        NoEmailFlag = noEmailFlag;
        NoMailFlag = noMailFlag;
        NoPhoneFlag = noPhoneFlag;
        ParlayMaxBet = parlayMaxBet;
        ParlayMaxPayout = parlayMaxPayout;
        ParlayName = parlayName;
        Password = password;
        PayoutPassword = payoutPassword;
        PercentBook = percentBook;
        PriceType = priceType;
        Promoter = promoter;
        ReferredBy = referredBy;
        SettleFigure = settleFigure;
        Source = source;
        State = state;
        StaticLinesFlag = staticLinesFlag;
        TeaserMaxBet = teaserMaxBet;
        TempCreditAdj = tempCreditAdj;
        TempCreditAdjExpDate = tempCreditAdjExpDate;
        TimeZone = timeZone;
        UsePuckLineFlag = usePuckLineFlag;
        WagerLimit = wagerLimit;
        TempWagerLimit = tempWagerLimit;
        TempWagerLimitExpiration = tempWagerLimitExpiration;
        WeeklyLimitFlag = weeklyLimitFlag;
        WiseActionFlag = wiseActionFlag;
        ZeroBalanceFlag = zeroBalanceFlag;
        ZeroBalPositiveOnlyFlag = zeroBalPositiveOnlyFlag;
        Zip = zip;
        EnforceParlayMaxBetFlag = enforceParlayMaxBetFlag;
        EnforceTeaserMaxBetFlag = enforceTeaserMaxBetFlag;
        HalfPointFootballOn3Flag = halfPointFootballOn3Flag;
        HalfPointFootballOff3Flag = halfPointFootballOff3Flag;
        HalfPointFootballOn7Flag = halfPointFootballOn7Flag;
        HalfPointFootballOff7Flag = halfPointFootballOff7Flag;
        HalfPointDenyTotalsFlag = halfPointDenyTotalsFlag;
      }
    }

    public class CustomerRebateInfo {
      public int? PackageId { get; set; }
      public string CustomerId { private get; set; }
      public string PackageName { get; set; }
      public string ExecutionFrequency { get; set; }
      public string PaymentCriteria { get; set; }
      private double PreviousMakeup { get; set; }
      public double NewMakeup { get; set; }
      public bool OverrideMakeup { get; set; }
      public bool InsertRebateTransaction { get; set; }

    }

    private class CustomerStoreShadeGroup {
      public readonly String CustomerId;
      public readonly String Store;
      public readonly String CustProfile;


      public CustomerStoreShadeGroup(String currentCustomerId, String store, String custProfile) {
        CustomerId = currentCustomerId;
        Store = store;
        CustProfile = custProfile;
      }
    }

    public class CustomerAvailableTeaser {
      public readonly String CustomerId;
      public readonly String TeaserName;
      public readonly CheckState ItemStatus;

      public CustomerAvailableTeaser(String currentCustomerId, String teaserName, CheckState itemStatus) {
        CustomerId = currentCustomerId;
        TeaserName = teaserName;
        ItemStatus = itemStatus;
      }
    }

    public class CustomerAvailableBetService {
      public readonly String CustomerId;
      public readonly String SystemId;
      public readonly String Profile;
      public readonly Boolean Selected;

      public CustomerAvailableBetService(String currentCustomerId, String systemId, String profile, Boolean selected) {
        CustomerId = currentCustomerId;
        SystemId = systemId;
        Profile = profile;
        Selected = selected;
      }
    }

    public class CustomerCostToBuyPoints {
      public readonly int BasketballSpreadBuy;
      public readonly int BasketballSpreadBuyMax;
      public readonly int BasketballTotalBuy;
      public readonly int BasketballTotalBuyMax;

      public readonly int CollegeBasketballSpreadBuy;
      public readonly int CollegeBasketballSpreadBuyMax;
      public readonly int CollegeBasketballTotalBuy;
      public readonly int CollegeBasketballTotalBuyMax;

      public readonly int FootballSpreadBuy;
      public readonly int FootballSpreadBuyMax;
      public readonly int FootballSpreadBuyOn3;
      public readonly int FootballSpreadBuyOff3;
      public readonly int FootballSpreadBuyOn7;
      public readonly int FootballSpreadBuyOff7;
      public readonly int FootballTotalBuy;
      public readonly int FootballTotalBuyMax;
      public readonly int CollegeFootballSpreadBuy;
      public readonly int CollegeFootballSpreadBuyMax;
      public readonly int CollegeFootballSpreadBuyOn3;
      public readonly int CollegeFootballSpreadBuyOff3;
      public readonly int CollegeFootballSpreadBuyOn7;
      public readonly int CollegeFootballSpreadBuyOff7;
      public readonly int CollegeFootballTotalBuy;
      public readonly int CollegeFootballTotalBuyMax;
      public readonly String ProgressivePointBuyingFlag;
      public readonly String ProgressiveChartName;

      public CustomerCostToBuyPoints() {
        
      }

      public CustomerCostToBuyPoints(int basketballSpreadBuy, int basketballSpreadBuyMax, int basketballTotalBuy, int basketballTotalBuyMax,
                                      int collegeBasketballSpreadBuy, int collegeBasketballSpreadBuyMax, int collegeBasketballTotalBuy, int collegeBasketballTotalBuyMax,
                                      int footballSpreadBuy, int footballSpreadBuyMax, int footballSpreadBuyOn3,
                                      int footballSpreadBuyOff3, int footballSpreadBuyOn7, int footballSpreadBuyOff7, int footballTotalBuy,
                                      int footballTotalBuyMax, int collegeFootballSpreadBuy, int collegeFootballSpreadBuyMax, int collegeFootballSpreadBuyOn3,
                                      int collegeFootballSpreadBuyOff3, int collegeFootballSpreadBuyOn7, int collegeFootballSpreadBuyOff7,
                                      int collegeFootballTotalBuy, int collegeFootballTotalBuyMax, String progressivePointBuyingFlag, String progressiveChartName) {
        BasketballSpreadBuy = basketballSpreadBuy;
        BasketballSpreadBuyMax = basketballSpreadBuyMax;
        BasketballTotalBuy = basketballTotalBuy;
        BasketballTotalBuyMax = basketballTotalBuyMax;

        CollegeBasketballSpreadBuy = collegeBasketballSpreadBuy;
        CollegeBasketballSpreadBuyMax = collegeBasketballSpreadBuyMax;
        CollegeBasketballTotalBuy = collegeBasketballTotalBuy;
        CollegeBasketballTotalBuyMax = collegeBasketballTotalBuyMax;

        FootballSpreadBuy = footballSpreadBuy;
        FootballSpreadBuyMax = footballSpreadBuyMax;
        FootballSpreadBuyOn3 = footballSpreadBuyOn3;
        FootballSpreadBuyOff3 = footballSpreadBuyOff3;
        FootballSpreadBuyOn7 = footballSpreadBuyOn7;
        FootballSpreadBuyOff7 = footballSpreadBuyOff7;
        FootballTotalBuy = footballTotalBuy;
        FootballTotalBuyMax = footballTotalBuyMax;
        CollegeFootballSpreadBuy = collegeFootballSpreadBuy;
        CollegeFootballSpreadBuyMax = collegeFootballSpreadBuyMax;
        CollegeFootballSpreadBuyOn3 = collegeFootballSpreadBuyOn3;
        CollegeFootballSpreadBuyOff3 = collegeFootballSpreadBuyOff3;
        CollegeFootballSpreadBuyOn7 = collegeFootballSpreadBuyOn7;
        CollegeFootballSpreadBuyOff7 = collegeFootballSpreadBuyOff7;
        CollegeFootballTotalBuy = collegeFootballTotalBuy;
        CollegeFootballTotalBuyMax = collegeFootballTotalBuyMax;
        ProgressivePointBuyingFlag = progressivePointBuyingFlag;
        ProgressiveChartName = progressiveChartName;
      }

    }

    public class CustomerVigDiscountElem {
      public readonly String SportType;
      public readonly String SportSubType;
      public readonly String Period;
      public readonly String WagerType;
      public readonly String WagerTypeName;
      public readonly String CuVigDisc;
      public readonly String CuExpDate;
      public readonly String InetVigDisc;
      public readonly String InetExpDate;

      public CustomerVigDiscountElem(String sportType, String sportSubType, String period, String wagerType, String wagerTypeName, String cUVigDisc, String cUExpDate,
                                      String inetVigDisc, String inetExpDate) {
        SportType = sportType;
        SportSubType = sportSubType;
        Period = period;
        WagerType = wagerType;
        WagerTypeName = wagerTypeName;
        CuVigDisc = cUVigDisc;
        CuExpDate = cUExpDate;
        InetVigDisc = inetVigDisc;
        InetExpDate = inetExpDate;
      }
    }

    public class AgentInfo {
      public String AgentId;
      public String CommissionType;
      public String CommissionPercent;
      public String MasterAgentId;
      public String DistributeToMasterFlag;
      public String HeadCountRate;
      public String DistributeNoFundsFlag;
      public String ChangeTempCreditFlag;
      public String EnterTransactionFlag;
      public String SuspendWageringFlag;
      public String UpdateCommentsFlag;
      public String ManageLinesFlag;
      public String CasinoFeePercent;
      public String InetHeadCountRate;
      public String AddNewAccountFlag;
      public String ChangeCreditLimitFlag;
      public String ChangeSettleFigureFlag;
      public String ChangeWagerLimitFlag;
      public String CustomerIdPrefix;
      public String EnterBettingAdjustmentFlag;
      public String SetMinimumBetAmountFlag;
      public String CasinoHeadCountRate;
      public String IncludeFpLossesFlag;
      public String CurrentMakeUp;
      public DateTime DistributionDate;

      public AgentInfo() {
      }

      public AgentInfo(String agentId, String commissionType, String commissionPercent, String masterAgentId, String distributeToMasterFlag,
                       String headCountRate, String distributeNoFundsFlag, String changeTempCreditFlag, String enterTransactionFlag, String suspendWageringFlag,
                       String updateCommentsFlag, String manageLinesFlag, String casinoFeePercent, String inetHeadCountRate, String addNewAccountFlag, String changeCreditLimitFlag,
                       String changeSettleFigureFlag, String changeWagerLimitFlag, String customerIdPrefix, String enterBettingAdjustmentFlag, String setMinimumBetAmountFlag,
                       String casinoHeadCountRate, String includeFpLossesFlag, String currentMakeUp, DateTime distributionDate) {
        AgentId = agentId;
        CommissionType = commissionType;
        CommissionPercent = commissionPercent;
        MasterAgentId = masterAgentId;
        DistributeToMasterFlag = distributeToMasterFlag;
        HeadCountRate = headCountRate;
        DistributeNoFundsFlag = distributeNoFundsFlag;
        ChangeTempCreditFlag = changeTempCreditFlag;
        EnterTransactionFlag = enterTransactionFlag;
        SuspendWageringFlag = suspendWageringFlag;
        UpdateCommentsFlag = updateCommentsFlag;
        ManageLinesFlag = manageLinesFlag;
        CasinoFeePercent = casinoFeePercent;
        InetHeadCountRate = inetHeadCountRate;
        AddNewAccountFlag = addNewAccountFlag;
        ChangeCreditLimitFlag = changeCreditLimitFlag;
        ChangeSettleFigureFlag = changeSettleFigureFlag;
        ChangeWagerLimitFlag = changeWagerLimitFlag;
        CustomerIdPrefix = customerIdPrefix;
        EnterBettingAdjustmentFlag = enterBettingAdjustmentFlag;
        SetMinimumBetAmountFlag = setMinimumBetAmountFlag;
        CasinoHeadCountRate = casinoHeadCountRate;
        IncludeFpLossesFlag = includeFpLossesFlag;
        CurrentMakeUp = currentMakeUp;
        DistributionDate = distributionDate;
      }
    }

    public class CustomerWagerLimit {
      public readonly bool SportIsEnabled;
      public readonly bool OverrideCircle;
      public readonly String SportType;
      public readonly String SportSubType;
      public readonly String Period;
      public readonly String WagerType;
      public readonly String WagerTypeName;
      public readonly String CuLimit;
      public readonly String InetLimit;
      public readonly String TempCuLimit;
      public readonly String TempInetLimit;
      public readonly String TempLimitsExpiration;

      public DateTime? TempLimitsExpirationDate {
        get {
          DateTime date;
          if (DateTime.TryParse(TempLimitsExpiration, out date)) return date;
          return null;
        }
      }

      public CustomerWagerLimit(bool sportIsEnabled, bool overrideCircle, String sportType, String sportSubType, String period, String wagerType, String wagerTypeName, String cuLimit, String inetLimit, String tempCuLimit, String tempInetLimit, String tempLimitsExpiration) {
        SportIsEnabled = sportIsEnabled;
        OverrideCircle = overrideCircle;
        SportType = sportType;
        SportSubType = sportSubType;
        Period = period;
        WagerType = wagerType;
        WagerTypeName = wagerTypeName;
        CuLimit = cuLimit;
        InetLimit = inetLimit;
        TempCuLimit = tempCuLimit;
        TempInetLimit = tempInetLimit;
        TempLimitsExpiration = tempLimitsExpiration;
      }
    }

    public class CustomerParlayTeaserWagerLimit {
      public readonly String SportType;
      public readonly String SportSubType;
      public readonly String Period;
      public readonly String WagerType;
      public readonly String WagerTypeName;
      public readonly String CuLimit;
      public readonly String InetLimit;
      public readonly String WagerTypePT;

      public CustomerParlayTeaserWagerLimit(String sportType, String sportSubType, String period, String wagerType, String wagerTypeName, String cuLimit, String inetLimit, String wagerTypePT) {
        SportType = sportType;
        SportSubType = sportSubType;
        Period = period;
        WagerType = wagerType;
        WagerTypeName = wagerTypeName;
        CuLimit = cuLimit;
        InetLimit = inetLimit;
        WagerTypePT = wagerTypePT;
      }
    }

    public class CustomerTransaction {
      public readonly int DocumentNumber;
      public readonly String Amount;
      public readonly String EnteredAmount;
      public readonly String TranCode;
      public readonly String Description;
      public readonly String Reference;
      public readonly String HoldFundsPercent;
      public readonly String HoldFundsReleaseDate;
      public readonly String CcNumber;
      public readonly String CcExpires;
      public readonly String CcApprovalNumber;
      public readonly String DailyFigureDate;
      public readonly String CasinoAdjustment;
      public readonly String PaymentBy;

      public CustomerTransaction(int documentNumber, String tranCode, String amount, String enteredAmount, String description, String reference, String holdFundsPercent,
                                  String holdFundsReleaseDate, String ccNumber, String ccExpires, String ccApprovalNumber, String dailyFigureDate, String casinoAdjustment, String paymentBy) {
        DocumentNumber = documentNumber;
        TranCode = tranCode;
        Amount = amount;
        EnteredAmount = enteredAmount;
        Description = description;
        Reference = reference;
        HoldFundsPercent = holdFundsPercent;
        HoldFundsReleaseDate = holdFundsReleaseDate;
        CcNumber = ccNumber;
        CcExpires = ccExpires;
        CcApprovalNumber = ccApprovalNumber;
        DailyFigureDate = dailyFigureDate;
        CasinoAdjustment = casinoAdjustment;
        PaymentBy = paymentBy;
      }
    }

    private string _currentCustomerId;
    private readonly ModuleInfo _moduleInfo;

    public double AmountToAdjustBalance;
    public String TransacCode = "";

    private Panel _custPanShadeGroups;

    public CustomerInfoUpdater(string custId, ModuleInfo moduleInfo) {
      _moduleInfo = moduleInfo;
      _currentCustomerId = custId;
    }

    public void LoadCustomerInfoToStructFromDb(spCstGetCustomerInfoNoNulls_Result tbCust) {
      if (tbCust == null) return;
      _customerInfoFromDbList = new List<CustomerInfo>();

      var dbCustStruct = new CustomerInfo(
                                  tbCust.Active.Trim(),
                                  tbCust.Address.Trim(),
                                  tbCust.AgentID.Trim(),
                                  tbCust.AgentType.Trim(),
                                  tbCust.ApplyDetailWagerLimitsFlag,
                                  tbCust.AutoAgentShadeGroupFlag,
                                  tbCust.BaseballAction.Trim(),
                                  tbCust.BusinessPhone.Trim(),
                                  tbCust.CasinoActive.Trim(),
                                  tbCust.City.Trim(),
                                  tbCust.Comments.Trim(),
                                  tbCust.CommentsForCustomer.Trim(),
                                  tbCust.CommentsForTW.Trim(),
                                  tbCust.ConfirmationDelay.ToString(CultureInfo.InvariantCulture).Trim(),
                                  tbCust.ContestMaxBet.ToString(CultureInfo.InvariantCulture).Trim(),
                                  tbCust.Country.Trim(),
                                  tbCust.CreditAcctFlag,
                                  tbCust.CreditLimit.ToString(CultureInfo.InvariantCulture).Trim(),
                                  tbCust.CUMinimumWager.ToString(CultureInfo.InvariantCulture).Trim(),
                                  tbCust.Currency.Trim(),
                                  tbCust.EasternLineFlag,
                                  tbCust.EMail.Trim(),
                                  tbCust.EnableRifFlag,
                                  tbCust.EnforceAccumWagerLimitsByLineFlag,
                                  tbCust.EnforceAccumWagerLimitsFlag,
                                  tbCust.EstWager101to500Flag,
                                  tbCust.EstWager501to1000Flag,
                                  tbCust.EstWager51to100Flag,
                                  tbCust.EstWager5to50Flag,
                                  tbCust.EstWagerOver1000Flag,
                                  tbCust.Fax.Trim(),
                                  tbCust.HalfPointCuBasketballDow.ToString(CultureInfo.InvariantCulture),
                                  tbCust.HalfPointCuBasketballFlag,
                                  tbCust.HalfPointCuFootballDow.ToString(CultureInfo.InvariantCulture),
                                  tbCust.HalfPointCuFootballFlag,
                                  tbCust.HalfPointInetBasketballDow.ToString(CultureInfo.InvariantCulture),
                                  tbCust.HalfPointInetBasketballFlag,
                                  tbCust.HalfPointInetFootballDow.ToString(CultureInfo.InvariantCulture),
                                  tbCust.HalfPointInetFootballFlag,
                                  tbCust.HalfPointWagerLimitFlag,
                                  tbCust.HalfPointMaxBet.ToString(CultureInfo.InvariantCulture).Trim(),
                                  tbCust.HomePhone.Trim(),
                                  tbCust.InetMinimumWager.ToString(CultureInfo.InvariantCulture).Trim(),
                                  tbCust.InetTarget.Trim(),
                                  tbCust.InstantActionFlag,
                                  tbCust.IntBaseballFlag,
                                  tbCust.IntBasketballFlag,
                                  tbCust.IntFootballFlag,
                                  tbCust.IntHockeyFlag,
                                  tbCust.IntHorsesFlag,
                                  tbCust.IntOtherFlag,
                                  tbCust.LimitRifToRiskFlag,
                                  tbCust.LossCap.ToString(CultureInfo.InvariantCulture).Trim(),
                                  tbCust.NewCustomerMakeup.ToString(CultureInfo.InvariantCulture),
                                  tbCust.NameFirst.Trim(),
                                  tbCust.NameLast.Trim(),
                                  tbCust.NameMI.Trim(),
                                  tbCust.NoEmailFlag,
                                  tbCust.NoMailFlag,
                                  tbCust.NoPhoneFlag,
                                  tbCust.ParlayMaxBet.ToString(CultureInfo.InvariantCulture).Trim(),
                                  tbCust.ParlayMaxPayout.ToString(CultureInfo.InvariantCulture).Trim(),
                                  tbCust.ParlayName.Trim(),
                                  tbCust.Password.Trim(),
                                  tbCust.PayoutPassword.Trim(),
                                  tbCust.PercentBook.ToString(CultureInfo.InvariantCulture).Trim(),
                                  tbCust.PriceType.Trim(),
                                  tbCust.Promoter.Trim(),
                                  tbCust.ReferredBy.Trim(),
                                  tbCust.SettleFigure.ToString(CultureInfo.InvariantCulture).Trim(),
                                  tbCust.Source.Trim(),
                                  tbCust.State.Trim(),
                                  tbCust.StaticLinesFlag.Trim(),
                                  tbCust.TeaserMaxBet.ToString(CultureInfo.InvariantCulture).Trim(),
                                  tbCust.TempCreditAdj.ToString(CultureInfo.InvariantCulture).Trim(),
                                  tbCust.TempCreditAdjExpDate.ToShortDateString().Trim(),
                                  tbCust.TimeZone.ToString(CultureInfo.InvariantCulture).Trim(),
                                  tbCust.UsePuckLineFlag,
                                  tbCust.WagerLimit.ToString(CultureInfo.InvariantCulture).Trim(),
                                  tbCust.TempWagerLimit.ToString(CultureInfo.InvariantCulture),
                                  tbCust.TempWagerLimitExpiration.ToShortDateString(),
                                  tbCust.WeeklyLimitFlag,
                                  tbCust.WiseActionFlag,
                                  tbCust.ZeroBalanceFlag,
                                  tbCust.ZeroBalPositiveOnlyFlag,
                                  tbCust.Zip.Trim(),
                                  tbCust.EnforceParlayMaxBetFlag,
                                  tbCust.EnforceTeaserMaxBetFlag,
                                  tbCust.HalfPointFootballOn3Flag, tbCust.HalfPointFootballOff3Flag, tbCust.HalfPointFootballOn7Flag, tbCust.HalfPointFootballOff7Flag, tbCust.HalfPointDenyTotalsFlag
                                  );

      _customerInfoFromDbList.Add(dbCustStruct);
    }

    public void LoadCustomerStoreShadeGroupsToStructFromDb(Panel panShadeGroups, IEnumerable<spCstGetCustomerShadeGroups_Result> customerShadeGroups) {

      _storeShadeGroupsFromDbList = new List<CustomerStoreShadeGroup>();
      _custPanShadeGroups = new Panel();
      if (panShadeGroups != null) {
        _custPanShadeGroups = panShadeGroups;
      }

      foreach (var res in customerShadeGroups) {
        var custStoreShadeGroup = new CustomerStoreShadeGroup(res.CustomerID, res.Store, res.CustProfile);
        _storeShadeGroupsFromDbList.Add(custStoreShadeGroup);
      }
    }

    private string CastValue(string valueFromdb, string valueFromFrm, string logData, ref bool doUpdate) {
      return CastValue(valueFromdb, valueFromFrm, valueFromFrm, logData, ref doUpdate);
    }

    private string CastValue(string valueFromdb, string valueFromFrm, string logValue, string logTitle, ref bool doUpdate, bool logChange = true) {
      if (valueFromdb == valueFromFrm || valueFromFrm == "Obj_Not_Loaded") return string.IsNullOrEmpty(valueFromdb) ? null : valueFromdb;
      doUpdate = true;
      if (!logChange) return string.IsNullOrEmpty(valueFromFrm) ? null : valueFromFrm;
      var logInfo = new LogWriter.CuAccessLogInfo { ProgramName = _moduleInfo.Description, Operation = "Changed " + _currentCustomerId, Data = logTitle + " - " + logValue };
      _itemsToCuLog.Add(logInfo);
      return valueFromFrm;
    }

    private double? CastNumericValue(string valueFromdb, string valueFromFrm, string logTitle, ref bool doUpdate, bool logChange = true) {
      return CastNumericValue(valueFromdb, valueFromFrm, valueFromFrm, logTitle, ref doUpdate, logChange);
    }

    private double CastNumericValue(string valueFromdb, string valueFromFrm, string logValue, string logTitle, ref bool doUpdate, bool logChange = true) {
      if (valueFromdb == valueFromFrm || valueFromFrm == "Obj_Not_Loaded") return valueFromdb == null ? 0 : double.Parse(valueFromdb);
      double origValueToTest;
      double finalValueToTest;

      if ((valueFromdb == valueFromFrm) || (!double.TryParse(valueFromdb, out origValueToTest) && (valueFromdb != null)) || (!double.TryParse(valueFromFrm, out finalValueToTest) && (valueFromFrm != null)))
        return string.IsNullOrEmpty(valueFromFrm) ? 0 : Double.Parse(valueFromdb ?? "0");
      if (origValueToTest == finalValueToTest) return origValueToTest;
      doUpdate = true;
      if (!logChange) return finalValueToTest;
      var logInfo = new LogWriter.CuAccessLogInfo { ProgramName = _moduleInfo.Description, Operation = "Changed " + _currentCustomerId, Data = logTitle + " - " + (string.IsNullOrEmpty(logValue) ? DisplayNumber.Format(finalValueToTest, true, 100, true) : logValue) };
      _itemsToCuLog.Add(logInfo);
      return finalValueToTest;
    }

    private DateTime? CastDateValue(string valueFromdb, string valueFromFrm, string logValue, string logTitle, ref bool doUpdate, bool logChange = true) {
      if (valueFromdb == "1/1/1900") valueFromdb = "";
      if (valueFromFrm == "1/1/1900") valueFromFrm = "";

      if (valueFromdb != valueFromFrm && valueFromFrm != "Obj_Not_Loaded") {
        doUpdate = true;
        if (logChange) {
          var logInfo = new LogWriter.CuAccessLogInfo { ProgramName = _moduleInfo.Description, Operation = "Changed " + _currentCustomerId, Data = logTitle + " - " + (string.IsNullOrEmpty(logValue) ? valueFromFrm : logValue) };
          _itemsToCuLog.Add(logInfo);
        }

        if (valueFromFrm == "") return null;
        return DateTime.Parse(valueFromFrm);

      }
      if (valueFromdb == "") return null;
      return DateTime.Parse(valueFromdb);
    }

    private void CastBoolValue(string valueFromdb, string valueFromFrm, string logValue, string logTitle, ref bool doUpdate, bool logChange = true) {
      if (valueFromdb == null) valueFromdb = "False";
      if (valueFromFrm == null) valueFromFrm = "False";

      if (valueFromdb == valueFromFrm || valueFromFrm == "Obj_Not_Loaded") return;
      doUpdate = true;
      if (!logChange) return;
      var logInfo = new LogWriter.CuAccessLogInfo { ProgramName = _moduleInfo.Description, Operation = "Changed " + _currentCustomerId, Data = logTitle + " - " + (string.IsNullOrEmpty(logValue) ? valueFromFrm : logValue) };
      _itemsToCuLog.Add(logInfo);
    }

    public void CompareCustomerInfoToUpdate(IEnumerable<CustomerInfo> customerInfoFromFrmList, Boolean packageInherited) {
      if (_customerInfoFromDbList == null || customerInfoFromFrmList == null) return;
      var retrievedFromDb = _customerInfoFromDbList.FirstOrDefault();
      var retrievedFromFrm = customerInfoFromFrmList.FirstOrDefault();

      if (retrievedFromDb == null || retrievedFromFrm == null) return;

      _itemsToCuLog = new List<LogWriter.CuAccessLogInfo>();
      spCstGetCustomer_Result customer;
      using (var cust = new Customers(_moduleInfo)) {
        customer = cust.GetCustomer(_currentCustomerId);
      }
      if (customer == null) return;

      var doUpdate = false;
      var doMakeupUpdate = false;
      var agentChanged = false;

      customer.Active = CastValue(retrievedFromDb.Active, retrievedFromFrm.Active, retrievedFromFrm.Active == "Y" ? "Yes" : "No",
        "Customer Suspended", ref doUpdate);

      customer.Address = CastValue(retrievedFromDb.Address, retrievedFromFrm.Address, "Customer Address Changed", ref doUpdate);

      customer.AgentType = CastValue(retrievedFromDb.AgentType.Trim(), retrievedFromFrm.AgentType.Trim(),
        retrievedFromFrm.AgentType == Agents.AGENT_TYPE ? "Regular" : "Master", "Agent Type Changed", ref doUpdate);

      customer.AutoAgentShadeGroupFlag = CastValue(retrievedFromDb.AutoAgentShadeGroupFlag, retrievedFromFrm.AutoAgentShadeGroupFlag,
        retrievedFromFrm.AutoAgentShadeGroupFlag == "Y" ? "Yes" : "No", "Auto Agent Shade Group Flag Changed", ref doUpdate);

      customer.BaseballAction = CastValue(retrievedFromDb.BaseballAction, retrievedFromFrm.BaseballAction,
        "Baseball Wagers", ref doUpdate);

      customer.BusinessPhone = CastValue(retrievedFromDb.BusinessPhone, retrievedFromFrm.BusinessPhone,
        "Business Phone Changed", ref doUpdate);

      customer.CasinoActive = CastValue(retrievedFromDb.CasinoActive, retrievedFromFrm.CasinoActive,
        "Suspend Casino Flag Changed", retrievedFromFrm.CasinoActive == "Y" ? "Yes" : "No", ref doUpdate);

      customer.City = CastValue(retrievedFromDb.City, retrievedFromFrm.City, "City Changed", ref doUpdate);

      customer.Comments = CastValue(retrievedFromDb.Comments, retrievedFromFrm.Comments,
        "Comments Changed", ref doUpdate);

      customer.CommentsForCustomer = CastValue(retrievedFromDb.CommentsForCustomer, retrievedFromFrm.CommentsForCustomer,
        "Comments For Customer Changed", ref doUpdate);

      customer.CommentsForTW = CastValue(retrievedFromDb.CommentsForTw, retrievedFromFrm.CommentsForTw,
        "Comments For Ticket Writers Changed", ref doUpdate);

      customer.ConfirmationDelay = (int?)CastNumericValue(retrievedFromDb.ConfirmationDelay, retrievedFromFrm.ConfirmationDelay,
        "Internet Wager Confirmation Delay Changed", ref doUpdate);

      customer.ContestMaxBet = CastNumericValue(retrievedFromDb.ContestMaxBet, retrievedFromFrm.ContestMaxBet,
        "Contest Max Bet Changed", ref doUpdate);

      customer.Country = CastValue(retrievedFromDb.Country, retrievedFromFrm.Country,
        "Country Changed", ref doUpdate);

      customer.CreditAcctFlag = CastValue(retrievedFromDb.CreditAcctFlag, retrievedFromFrm.CreditAcctFlag,
        "Credit Account Flag Changed", retrievedFromFrm.CreditAcctFlag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.CreditLimit = CastNumericValue(retrievedFromDb.CreditLimit, retrievedFromFrm.CreditLimit,
        "Credit Limit Changed", ref doUpdate);

      customer.CUMinimumWager = (int?)CastNumericValue(retrievedFromDb.CuMinimumWager, retrievedFromFrm.CuMinimumWager,
        "CU Minimum Wager Limit Changed", ref doUpdate);

      customer.Currency = CastValue(retrievedFromDb.Currency, retrievedFromFrm.Currency,
        "Currency Changed", ref doUpdate);

      customer.EasternLineFlag = CastValue(retrievedFromDb.EasternLineFlag, retrievedFromFrm.EasternLineFlag,
        "Eastern Line Flag Changed", retrievedFromFrm.EasternLineFlag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.EMail = CastValue(retrievedFromDb.EMail, retrievedFromFrm.EMail,
        "EMail Changed", ref doUpdate);

      customer.EnableRifFlag = CastValue(retrievedFromDb.EnableRifFlag, retrievedFromFrm.EnableRifFlag,
        "Enable Rolling If Bets Flag Changed", retrievedFromFrm.EnableRifFlag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.EnforceAccumWagerLimitsByLineFlag = CastValue(retrievedFromDb.EnforceAccumWagerLimitsByLineFlag, retrievedFromFrm.EnforceAccumWagerLimitsByLineFlag,
        "Enforce Accum Wager Limits By Line Flag Changed", retrievedFromFrm.EnforceAccumWagerLimitsByLineFlag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.EnforceAccumWagerLimitsFlag = CastValue(retrievedFromDb.EnforceAccumWagerLimitsFlag, retrievedFromFrm.EnforceAccumWagerLimitsFlag,
        "Enforce Accum Wager Limits Flag Changed", retrievedFromFrm.EnforceAccumWagerLimitsFlag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.EstWager101to500Flag = CastValue(retrievedFromDb.EstWager101To500Flag, retrievedFromFrm.EstWager101To500Flag,
        "Estimated Wager 101 to 500 Flag Changed", retrievedFromFrm.EstWager101To500Flag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.EstWager501to1000Flag = CastValue(retrievedFromDb.EstWager501To1000Flag, retrievedFromFrm.EstWager501To1000Flag,
        "Estimated Wager 501 to 1000 Flag Changed", retrievedFromFrm.EstWager501To1000Flag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.EstWager51to100Flag = CastValue(retrievedFromDb.EstWager51To100Flag, retrievedFromFrm.EstWager51To100Flag,
        "Estimated Wager 51 to 100 Flag Changed", retrievedFromFrm.EstWager51To100Flag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.EstWager5to50Flag = CastValue(retrievedFromDb.EstWager5To50Flag, retrievedFromFrm.EstWager5To50Flag,
        "Estimated Wager 5 to 50 Flag Changed", retrievedFromFrm.EstWager5To50Flag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.EstWagerOver1000Flag = CastValue(retrievedFromDb.EstWagerOver1000Flag, retrievedFromFrm.EstWagerOver1000Flag,
        "Estimated Wager Over 1000 Flag Changed", retrievedFromFrm.EstWagerOver1000Flag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.Fax = CastValue(retrievedFromDb.Fax, retrievedFromFrm.Fax,
        "Fax Changed", ref doUpdate);

      customer.HalfPointCuBasketballDow = (int)CastNumericValue(retrievedFromDb.HalfPointCuBasketballDow == "-1" ? null : retrievedFromDb.HalfPointCuBasketballDow,
        retrievedFromFrm.HalfPointCuBasketballDow, retrievedFromFrm.HalfPointCuBasketballDow != null ? GetDayNameByNum(retrievedFromFrm.HalfPointCuBasketballDow) : "None",
        "Half Point CU Basketball Day Of Week Changed", ref doUpdate);

      customer.HalfPointCuBasketballFlag = CastValue(retrievedFromDb.HalfPointCuBasketballFlag, retrievedFromFrm.HalfPointCuBasketballFlag,
        "Half Point CU Basketball Changed", retrievedFromFrm.HalfPointCuBasketballFlag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.HalfPointCuFootballDow = (int)CastNumericValue(retrievedFromDb.HalfPointCuFootballDow == "-1" ? null : retrievedFromDb.HalfPointCuFootballDow,
        retrievedFromFrm.HalfPointCuFootballDow, retrievedFromFrm.HalfPointCuFootballDow != null ? GetDayNameByNum(retrievedFromFrm.HalfPointCuFootballDow) : "None",
        "Half Point CU Football Day Of Week Changed", ref doUpdate);

      customer.HalfPointCuFootballFlag = CastValue(retrievedFromDb.HalfPointCuFootballFlag, retrievedFromFrm.HalfPointCuFootballFlag,
        "Half Point CU Football Changed", retrievedFromFrm.HalfPointCuFootballFlag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.HalfPointInetBasketballDow = (int)CastNumericValue(retrievedFromDb.HalfPointInetBasketballDow == "-1" ? null : retrievedFromDb.HalfPointInetBasketballDow,
        retrievedFromFrm.HalfPointInetBasketballDow, retrievedFromFrm.HalfPointInetBasketballDow != null ? GetDayNameByNum(retrievedFromFrm.HalfPointInetBasketballDow) : "None",
        "Half Point Internet Basketball Day Of Week Changed", ref doUpdate);

      customer.HalfPointInetBasketballFlag = CastValue(retrievedFromDb.HalfPointInetBasketballFlag, retrievedFromFrm.HalfPointInetBasketballFlag,
        "Half Point Internet Basketball Changed", retrievedFromFrm.HalfPointInetBasketballFlag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.HalfPointInetFootballDow = (int)CastNumericValue(retrievedFromDb.HalfPointInetFootballDow == "-1" ? null : retrievedFromDb.HalfPointInetFootballDow,
        retrievedFromFrm.HalfPointInetFootballDow, retrievedFromFrm.HalfPointInetFootballDow != null ? GetDayNameByNum(retrievedFromFrm.HalfPointInetFootballDow) : "None",
        "Half Point Internet Basketball Day Of Week Changed", ref doUpdate);

      customer.HalfPointInetFootballFlag = CastValue(retrievedFromDb.HalfPointInetFootballFlag, retrievedFromFrm.HalfPointInetFootballFlag,
        "Half Point Internet Football Changed", retrievedFromFrm.HalfPointInetFootballFlag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.HalfPointWagerLimitFlag = CastValue(retrievedFromDb.HalfPointWagerLimitFlag, retrievedFromFrm.HalfPointWagerLimitFlag,
        "Enforce Free Half Point Wager Amount Changed", retrievedFromFrm.HalfPointWagerLimitFlag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.HalfPointFootballOn3Flag = CastValue(retrievedFromDb.HalfPointFootballOn3Flag, retrievedFromFrm.HalfPointFootballOn3Flag,
        "Half Point Football On3 Changed", retrievedFromFrm.HalfPointFootballOn3Flag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.HalfPointFootballOff3Flag = CastValue(retrievedFromDb.HalfPointFootballOff3Flag, retrievedFromFrm.HalfPointFootballOff3Flag,
        "Half Point Football Off3 Changed", retrievedFromFrm.HalfPointFootballOff3Flag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.HalfPointFootballOn7Flag = CastValue(retrievedFromDb.HalfPointFootballOn7Flag, retrievedFromFrm.HalfPointFootballOn7Flag,
        "Half Point Football On7 Changed", retrievedFromFrm.HalfPointFootballOn7Flag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.HalfPointFootballOff7Flag = CastValue(retrievedFromDb.HalfPointFootballOff7Flag, retrievedFromFrm.HalfPointFootballOff7Flag,
        "Half Point Football Off7 Changed", retrievedFromFrm.HalfPointFootballOff7Flag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.HalfPointDenyTotalsFlag = CastValue(retrievedFromDb.HalfPointDenyTotalsFlag, retrievedFromFrm.HalfPointDenyTotalsFlag,
        "Half Point Deny Totals Changed", retrievedFromFrm.HalfPointDenyTotalsFlag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.HalfPointMaxBet = CastNumericValue(retrievedFromDb.HalfPointMaxBet, retrievedFromFrm.HalfPointMaxBet,
        "Half Point Max Bet Changed", ref doUpdate);

      customer.HomePhone = CastValue(retrievedFromDb.HomePhone, retrievedFromFrm.HomePhone,
        "Home Phone Changed", ref doUpdate);

      customer.InetMinimumWager = (int?)CastNumericValue(retrievedFromDb.InetMinimumWager, retrievedFromFrm.InetMinimumWager,
        "Internet Minimum Wager Changed", ref doUpdate);

      customer.InetTarget = CastValue(retrievedFromDb.InetTarget, retrievedFromFrm.InetTarget,
        "InetTarget Changed", ref doUpdate);

      customer.InstantActionFlag = CastValue(retrievedFromDb.InstantActionFlag, retrievedFromFrm.InstantActionFlag,
        "Instant Action Flag Changed", retrievedFromFrm.InstantActionFlag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.IntBaseballFlag = CastValue(retrievedFromDb.IntBaseballFlag, retrievedFromFrm.IntBaseballFlag,
        "Interested in Baseball Flag Changed", retrievedFromFrm.IntBaseballFlag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.IntBasketballFlag = CastValue(retrievedFromDb.IntBasketballFlag, retrievedFromFrm.IntBasketballFlag,
        "Interested in Basketball Flag Changed", retrievedFromFrm.IntBasketballFlag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.IntFootballFlag = CastValue(retrievedFromDb.IntFootballFlag, retrievedFromFrm.IntFootballFlag,
        "Interested in Football Flag Changed", retrievedFromFrm.IntFootballFlag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.IntHockeyFlag = CastValue(retrievedFromDb.IntHockeyFlag, retrievedFromFrm.IntHockeyFlag,
        "Interested in Hockey Flag Changed", retrievedFromFrm.IntHockeyFlag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.IntHorsesFlag = CastValue(retrievedFromDb.IntHorsesFlag, retrievedFromFrm.IntHorsesFlag,
        "Interested in Horse Racing Flag Changed", retrievedFromFrm.IntHorsesFlag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.IntOtherFlag = CastValue(retrievedFromDb.IntOtherFlag, retrievedFromFrm.IntOtherFlag,
        "Interested in Other Sports Flag Changed", retrievedFromFrm.IntOtherFlag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.LimitRifToRiskFlag = CastValue(retrievedFromDb.LimitRifToRiskFlag, retrievedFromFrm.LimitRifToRiskFlag,
        "Limit Rolling if Bets To Risk Flag Changed", retrievedFromFrm.LimitRifToRiskFlag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.LossCap = CastNumericValue(retrievedFromDb.LossCap, retrievedFromFrm.LossCap,
        "Loss Cap Changed", ref doUpdate);

      var fieldUpdated = false;
      customer.NewCustomerMakeup = CastNumericValue(retrievedFromDb.NewCustMakeup, retrievedFromFrm.NewCustMakeup,
        "New Customer Makeup Changed", ref fieldUpdated, false) ?? 0;
      if (fieldUpdated) doMakeupUpdate = doUpdate = true;

      customer.NameFirst = CastValue(retrievedFromDb.NameFirst, retrievedFromFrm.NameFirst,
        "First Name Changed", ref doUpdate);

      customer.NameLast = CastValue(retrievedFromDb.NameLast, retrievedFromFrm.NameLast,
        "Last Name Changed", ref doUpdate);

      customer.NameMI = CastValue(retrievedFromDb.NameMi, retrievedFromFrm.NameMi,
        "Middle Name Changed", ref doUpdate);

      customer.NoEmailFlag = CastValue(retrievedFromDb.NoEmailFlag, retrievedFromFrm.NoEmailFlag,
        "No Email Flag Changed", retrievedFromFrm.NoEmailFlag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.NoMailFlag = CastValue(retrievedFromDb.NoMailFlag, retrievedFromFrm.NoMailFlag,
        "No mail Flag Changed", retrievedFromFrm.NoMailFlag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.NoPhoneFlag = CastValue(retrievedFromDb.NoPhoneFlag, retrievedFromFrm.NoPhoneFlag,
        "No Phone Flag Changed", retrievedFromFrm.NoPhoneFlag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.EnforceParlayMaxBetFlag = CastValue(retrievedFromDb.EnforceParlayMaxBetFlag, retrievedFromFrm.EnforceParlayMaxBetFlag,
      "Enforce Parlay Max Bet Flag Changed", retrievedFromFrm.EnforceParlayMaxBetFlag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.ParlayMaxBet = CastNumericValue(retrievedFromDb.ParlayMaxBet, retrievedFromFrm.ParlayMaxBet,
        "Parlay Max Bet Changed", ref doUpdate);

      customer.ParlayMaxPayout = CastNumericValue(retrievedFromDb.ParlayMaxPayout, retrievedFromFrm.ParlayMaxPayout,
        "Parlay Max Payout Changed", ref doUpdate);

      customer.ParlayName = CastValue(retrievedFromDb.ParlayName, retrievedFromFrm.ParlayName,
        "Parlay Name Changed", ref doUpdate);

      if (!packageInherited) {
        fieldUpdated = false;
        customer.AgentID = CastValue(retrievedFromDb.AgentId, retrievedFromFrm.AgentId,
          retrievedFromFrm.AgentId == "" ? "None" : retrievedFromFrm.AgentId, "Customer Agent Changed", ref fieldUpdated);
        if (fieldUpdated) agentChanged = doUpdate = true;
      }

      customer.Password = CastValue(retrievedFromDb.Password, retrievedFromFrm.Password,
        "Password Changed", ref doUpdate);

      customer.PayoutPassword = CastValue(retrievedFromDb.PayoutPassword, retrievedFromFrm.PayoutPassword,
        "Payout Password Changed", ref doUpdate);

      customer.PercentBook = (int?)CastNumericValue(retrievedFromDb.PercentBook, retrievedFromFrm.PercentBook,
        "Chart Percent Book Changed", ref doUpdate);

      customer.PriceType = CastValue(retrievedFromDb.PriceType, retrievedFromFrm.PriceType,
        GetPriceTypeName(retrievedFromFrm.PriceType), "Price Type Changed", ref doUpdate);

      customer.Promoter = CastValue(retrievedFromDb.Promoter, retrievedFromFrm.Promoter,
        "Promoter Changed", ref doUpdate);

      customer.ReferredBy = CastValue(retrievedFromDb.ReferredBy, retrievedFromFrm.ReferredBy,
        "Referred By Changed", ref doUpdate);

      customer.SettleFigure = CastNumericValue(retrievedFromDb.SettleFigure, retrievedFromFrm.SettleFigure,
        "Settle Figure Changed Changed", ref doUpdate);

      customer.Source = CastValue(retrievedFromDb.Source, retrievedFromFrm.Source,
        "Campaign Source Changed", ref doUpdate);

      customer.State = CastValue(retrievedFromDb.State, retrievedFromFrm.State,
        "State Changed", ref doUpdate);

      customer.StaticLinesFlag = CastValue(retrievedFromDb.StaticLinesFlag, retrievedFromFrm.StaticLinesFlag,
        "Static Lines During Call Flag Changed", retrievedFromFrm.StaticLinesFlag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.EnforceTeaserMaxBetFlag = CastValue(retrievedFromDb.EnforceTeaserMaxBetFlag, retrievedFromFrm.EnforceTeaserMaxBetFlag,
"Enforce Teaser Max Bet Flag Changed", retrievedFromFrm.EnforceTeaserMaxBetFlag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.TeaserMaxBet = CastNumericValue(retrievedFromDb.TeaserMaxBet, retrievedFromFrm.TeaserMaxBet,
        "Teaser Max Bet Changed", ref doUpdate);

      customer.TempCreditAdj = CastNumericValue(retrievedFromDb.TempCreditAdj, retrievedFromFrm.TempCreditAdj,
        "Temporary Credit Adjustment Changed", ref doUpdate);

      customer.TempCreditAdjExpDate = CastDateValue(retrievedFromDb.TempCreditAdjExpDate, retrievedFromFrm.TempCreditAdjExpDate, "",
        "Temporary Credit Adjustment Date Changed", ref doUpdate);

      customer.TimeZone = (int)CastNumericValue(retrievedFromDb.TimeZone, retrievedFromFrm.TimeZone,
        GetTimeZoneName(retrievedFromFrm.TimeZone), "Time Zone Changed", ref doUpdate);

      customer.UsePuckLineFlag = CastValue(retrievedFromDb.UsePuckLineFlag, retrievedFromFrm.UsePuckLineFlag,
        "Use Puck Line Flag Changed", retrievedFromFrm.UsePuckLineFlag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.WagerLimit = CastNumericValue(retrievedFromDb.WagerLimit, retrievedFromFrm.WagerLimit,
        "Wager Quick Limit Changed", ref doUpdate);

      customer.TempWagerLimit = CastNumericValue(retrievedFromDb.TempWagerLimit, retrievedFromFrm.TempWagerLimit,
        "Temp Wager Quick Limit Changed", ref doUpdate);

      customer.TempWagerLimitExpiration = CastDateValue(retrievedFromDb.TempWagerLimitExpiration, retrievedFromFrm.TempWagerLimitExpiration, "",
        "Temp Wager Quick Limit Expiration Changed", ref doUpdate);

      customer.WeeklyLimitFlag = CastValue(retrievedFromDb.WeeklyLimitFlag, retrievedFromFrm.WeeklyLimitFlag,
        "Weekly Limit Flag Changed", retrievedFromFrm.WeeklyLimitFlag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.WiseActionFlag = CastValue(retrievedFromDb.WiseActionFlag, retrievedFromFrm.WiseActionFlag,
        "Wise Action Flag Changed", retrievedFromFrm.WiseActionFlag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.ZeroBalanceFlag = CastValue(retrievedFromDb.ZeroBalanceFlag, retrievedFromFrm.ZeroBalanceFlag,
        "Zero Balance Flag Changed", retrievedFromFrm.ZeroBalanceFlag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.ZeroBalPositiveOnlyFlag = CastValue(retrievedFromDb.ZeroBalPositiveOnlyFlag, retrievedFromFrm.ZeroBalPositiveOnlyFlag,
        "Zero Balance Positive Figure Only Flag Changed", retrievedFromFrm.ZeroBalPositiveOnlyFlag == "Y" ? "Yes" : "No", ref doUpdate);

      customer.Zip = CastValue(retrievedFromDb.Zip, retrievedFromFrm.Zip,
        "Zip Code Changed", ref doUpdate);

      if (doUpdate) {
        using (var cust = new Customers(_moduleInfo)) {
          customer.LastChangedBy = Environment.UserName;
          cust.UpdateCustomerInfo(customer);
          if (agentChanged) {
            cust.UpdateCustomerAgent(customer.CustomerID, customer.AgentID, Environment.UserName);
          }

          using (var lw = new LogWriter(_moduleInfo)) {
            lw.WriteToCuAccessLog(_itemsToCuLog);
          }
          var tbCust = cust.GetCustomerInfo(customer.CustomerID).FirstOrDefault();
          LoadCustomerInfoToStructFromDb(tbCust);
        }
      }

      if (!doMakeupUpdate) return;
      using (var cust = new Customers(_moduleInfo)) {
        customer.LastChangedBy = Environment.UserName;
        cust.UpdateCustomerMakeup(customer);

        using (var lw = new LogWriter(_moduleInfo)) {
          var logInfo = new LogWriter.CuAccessLogInfo { ProgramName = _moduleInfo.Description, Operation = "Changed " + _currentCustomerId, Data = "New Makeup Changed - " + DisplayNumber.Format(customer.NewCustomerMakeup, true, 1, true) };
          lw.WriteToCuAccessLog(logInfo);
        }
        var tbCust = cust.GetCustomerInfo(customer.CustomerID).FirstOrDefault();
        LoadCustomerInfoToStructFromDb(tbCust);
      }
    }

    public void CompareCustomerRebatePackageToUpdate(ref CustomerRebateInfo customerRebateInfoFromFrm) {
      var retrievedFromDb = CustomerRebateInfoFromDb;
      var retrievedFromFrm = customerRebateInfoFromFrm;

      _itemsToCuLog = new List<LogWriter.CuAccessLogInfo>();
      spCstGetCustomer_Result customer;
      using (var cust = new Customers(_moduleInfo)) {
        customer = cust.GetCustomer(_currentCustomerId);
        customerRebateInfoFromFrm.PackageId = customer.PackageId;
      }

      var doUpdate = false;

      customer.PackageName = CastValue(retrievedFromDb.PackageName, retrievedFromFrm.PackageName,
        "Rebate Package Name Changed", ref doUpdate);

      customer.ExecutionFrequency = CastValue(retrievedFromDb.ExecutionFrequency, retrievedFromFrm.ExecutionFrequency,
        "Rebate Package Execution Frequency Changed", ref doUpdate);

      customer.PaymentCriteria = CastValue(retrievedFromDb.PaymentCriteria, retrievedFromFrm.PaymentCriteria,
        "Rebate Package Payment Criteria Changed", ref doUpdate);

      double origValueToTest = retrievedFromDb.NewMakeup;
      double finalValueToTest = retrievedFromFrm.NewMakeup;

      if (origValueToTest != finalValueToTest) {
        customer.PreviousMakeUp = customer.NewCustomerMakeup;
        customer.NewCustomerMakeup = finalValueToTest;
        doUpdate = true;

        var logInfo = new LogWriter.CuAccessLogInfo { ProgramName = _moduleInfo.Description, Operation = "Changed " + _currentCustomerId, Data = "Rebate Package New Makeup Changed - " + finalValueToTest };
        _itemsToCuLog.Add(logInfo);
      }


      var boolValueFromdb = retrievedFromDb.OverrideMakeup;
      var boolValueFromFrm = retrievedFromFrm.OverrideMakeup;

      if (boolValueFromdb != boolValueFromFrm) {
        customer.OverrideMakeUp = boolValueFromFrm;
        doUpdate = true;

        var logInfo = new LogWriter.CuAccessLogInfo { ProgramName = _moduleInfo.Description, Operation = "Changed " + _currentCustomerId, Data = "Rebate Package Override Makeup Changed - " + boolValueFromFrm };
        _itemsToCuLog.Add(logInfo);
      }

      boolValueFromdb = retrievedFromDb.InsertRebateTransaction;
      boolValueFromFrm = retrievedFromFrm.InsertRebateTransaction;

      if (boolValueFromdb != boolValueFromFrm) {
        customer.InsertRebateTransaction = boolValueFromFrm;
        doUpdate = true;

        var logInfo = new LogWriter.CuAccessLogInfo { ProgramName = _moduleInfo.Description, Operation = "Changed " + _currentCustomerId, Data = "Rebate Package Insert Rebate Transaction Changed - " + boolValueFromFrm };
        _itemsToCuLog.Add(logInfo);
      }

      if (!doUpdate) return;
      using (var cust = new Customers(_moduleInfo)) {
        customer.LastChangedBy = Environment.UserName;
        cust.UpdateCustomerRebatePackageInfo(customer);

        using (var lw = new LogWriter(_moduleInfo)) {
          lw.WriteToCuAccessLog(_itemsToCuLog);
        }
        var tbCust = cust.GetCustomerInfo(customer.CustomerID).FirstOrDefault();
        LoadCustomerInfoToStructFromDb(tbCust);
      }
    }

    public void CompareCustomerStoreShadeGroupsInfoToUpdate(Panel currentStoresList, out Boolean dbWasAffected) {
      dbWasAffected = false;
      if (_custPanShadeGroups == null || _custPanShadeGroups.Controls.Count <= 0) return;
      var retCBx = (CheckedListBox)_custPanShadeGroups.Controls[0];
      string newStore;

      var cbxStoresList = new ComboBox();

      if (currentStoresList.Controls.Count > 0) cbxStoresList = (ComboBox)currentStoresList.Controls[0];

      if (cbxStoresList.SelectedItem != null) newStore = cbxStoresList.SelectedItem.ToString();
      else return;

      using (var cust = new Customers(_moduleInfo)) {
        _itemsToCuLog = new List<LogWriter.CuAccessLogInfo>();

        var oldStore = (from s in _storeShadeGroupsFromDbList select s.Store).FirstOrDefault();

        string itemName;
        if (oldStore != null && oldStore.Trim() != newStore.Trim()) {
          cust.DeleteCustStoreProfile(_currentCustomerId, newStore);

          for (var i = 0; i < retCBx.Items.Count; i++) {
            itemName = retCBx.Items[i].ToString();
            if (retCBx.GetItemCheckState(i) != CheckState.Checked) continue;
            cust.InsertCustStoreProfile(_currentCustomerId, newStore, itemName);
            var logInfo = new LogWriter.CuAccessLogInfo { ProgramName = _moduleInfo.Description, Operation = "Changed " + _currentCustomerId, Data = "Profile Added - " + itemName };
            _itemsToCuLog.Add(logInfo);
          }
          dbWasAffected = true;
        }
        else {
          for (var i = 0; i < retCBx.Items.Count; i++) {
            itemName = retCBx.Items[i].ToString();
            var custProfileInDb = false;
            LogWriter.CuAccessLogInfo logInfo;
            foreach (var shadeGroupRow in _storeShadeGroupsFromDbList) {
              _currentCustomerId = shadeGroupRow.CustomerId;
              var custProfile = shadeGroupRow.CustProfile;

              if (itemName.Trim().ToLower() != custProfile.Trim().ToLower()) continue;
              if (retCBx.GetItemCheckState(i) == CheckState.Unchecked) {
                cust.DeleteCustStoreProfile(_currentCustomerId, newStore, itemName);
                logInfo = new LogWriter.CuAccessLogInfo { ProgramName = _moduleInfo.Description, Operation = "Changed " + _currentCustomerId, Data = "Profile Removed - " + custProfile };
                _itemsToCuLog.Add(logInfo);
                //remove from db
              }
              custProfileInDb = true;
              break;
            }

            if (retCBx.GetItemCheckState(i) != CheckState.Checked || custProfileInDb) continue;
            cust.InsertCustStoreProfile(_currentCustomerId, newStore, itemName);
            logInfo = new LogWriter.CuAccessLogInfo { ProgramName = _moduleInfo.Description, Operation = "Changed " + _currentCustomerId, Data = "Profile Added - " + itemName };
            _itemsToCuLog.Add(logInfo);
            //insert into tbCustStoreProfile
          }
        }

        if (_itemsToCuLog.Count <= 0) return;
        using (var lw = new LogWriter(_moduleInfo)) {
          lw.WriteToCuAccessLog(_itemsToCuLog);
        }
      }
    }

    public void LoadCustomerAvailableTeasersToStructFromDb(CheckedListBox teasersList) {
      if (teasersList == null) return;
      CustomerAvailableTeasersFromDbList = new List<CustomerAvailableTeaser>();

      for (var i = 0; i < teasersList.Items.Count; i++) {
        var teaser = new CustomerAvailableTeaser(_currentCustomerId, teasersList.Items[i].ToString(), teasersList.GetItemCheckState(i));
        CustomerAvailableTeasersFromDbList.Add(teaser);
      }
    }

    public void CompareCustomerAvailableTeasersInfoToUpdate(List<CustomerAvailableTeaser> originalTeasersList, CheckedListBox latestTeasersList) {
      if (originalTeasersList == null || latestTeasersList == null) return;
      using (var cust = new Customers(_moduleInfo)) {
        _itemsToCuLog = new List<LogWriter.CuAccessLogInfo>();


        for (var i = 0; i < latestTeasersList.Items.Count; i++) {
          var finalElem = new CustomerAvailableTeaser(
            _currentCustomerId,
            latestTeasersList.Items[i].ToString(),
            latestTeasersList.GetItemCheckState(i));

          var originalElem = new CustomerAvailableTeaser(
            originalTeasersList[i].CustomerId,
            originalTeasersList[i].TeaserName,
            originalTeasersList[i].ItemStatus);

          if (finalElem.TeaserName.Trim() != originalElem.TeaserName.Trim()) continue;
          if (finalElem.ItemStatus == originalElem.ItemStatus) continue;
          if (finalElem.ItemStatus == CheckState.Checked) {
            cust.AddCustTeaserInfo(finalElem.TeaserName.Trim(), _currentCustomerId);
            //write to log
            var logInfo = new LogWriter.CuAccessLogInfo { ProgramName = _moduleInfo.Description, Operation = "Changed " + _currentCustomerId, Data = "Teaser Option Added - " + finalElem.TeaserName.Trim() };
            _itemsToCuLog.Add(logInfo);
          }
          else {

            cust.DeleteCustTeaserInfo(finalElem.TeaserName.Trim(), _currentCustomerId);
            //write to log
            var logInfo = new LogWriter.CuAccessLogInfo { ProgramName = _moduleInfo.Description, Operation = "Changed " + _currentCustomerId, Data = "Teaser Option Removed - " + finalElem.TeaserName.Trim() };
            _itemsToCuLog.Add(logInfo);
          }
        }
      }

      if (_itemsToCuLog.Count <= 0) return;
      using (var lw = new LogWriter(_moduleInfo)) {
        lw.WriteToCuAccessLog(_itemsToCuLog);
      }
    }

    public void LoadCustomerAvailableBetServicesToStructFromDb(DataGridView betSvcsList) {
      if (betSvcsList == null) return;
      CustomerAvailableBetSvcProfilesFromDbList = new List<CustomerAvailableBetService>();

      for (var i = 0; i < betSvcsList.Rows.Count; i++) {
        var betService = new CustomerAvailableBetService(
          _currentCustomerId,
          betSvcsList.Rows[i].Cells[2].Value.ToString(),
          betSvcsList.Rows[i].Cells[3].Value.ToString(),
          Boolean.Parse(betSvcsList.Rows[i].Cells[0].Value.ToString())
          );
        CustomerAvailableBetSvcProfilesFromDbList.Add(betService);
      }
    }

    public void CompareCustomerAvailableBetServicesInfoToUpdate(List<CustomerAvailableBetService> originalBetServicesList, DataGridView latestBetServicesList) {
      if (originalBetServicesList == null || latestBetServicesList == null) return;
      using (var betSvc = new BetServices(_moduleInfo)) {
        _itemsToCuLog = new List<LogWriter.CuAccessLogInfo>();


        for (var i = 0; i < latestBetServicesList.Rows.Count; i++) {
          var finalElem = new CustomerAvailableBetService(
            _currentCustomerId,
            latestBetServicesList.Rows[i].Cells[2].Value.ToString(),
            latestBetServicesList.Rows[i].Cells[3].Value.ToString(),
            Boolean.Parse(latestBetServicesList.Rows[i].Cells[0].Value.ToString())
            );

          var originalElem = new CustomerAvailableBetService(
            originalBetServicesList[i].CustomerId,
            originalBetServicesList[i].SystemId,
            originalBetServicesList[i].Profile,
            originalBetServicesList[i].Selected);

          if ((finalElem.SystemId.Trim() != originalElem.SystemId.Trim()) || (finalElem.Profile.Trim() != originalElem.Profile.Trim())) continue;
          if (finalElem.Selected == originalElem.Selected) continue;
          if (finalElem.Selected) {
            betSvc.AddCustBetServiceInfo(finalElem.CustomerId.Trim(), finalElem.SystemId.Trim(), finalElem.Profile.Trim());
            //write to log
            var logInfo = new LogWriter.CuAccessLogInfo { ProgramName = _moduleInfo.Description, Operation = "Changed " + _currentCustomerId, Data = "Bet Service Profile Added - " + finalElem.Profile.Trim() };
            _itemsToCuLog.Add(logInfo);
          }
          else {

            betSvc.DeleteCustBetServiceInfo(finalElem.CustomerId.Trim(), finalElem.SystemId.Trim(), finalElem.Profile.Trim());
            //write to log
            var logInfo = new LogWriter.CuAccessLogInfo { ProgramName = _moduleInfo.Description, Operation = "Changed " + _currentCustomerId, Data = "Bet Service Profile Removed - " + finalElem.Profile.Trim() };
            _itemsToCuLog.Add(logInfo);
          }
        }
      }

      if (_itemsToCuLog.Count <= 0) return;
      using (var lw = new LogWriter(_moduleInfo)) {
        lw.WriteToCuAccessLog(_itemsToCuLog);
      }
    }

    public void LoadCustomerCostToBuyPointsToStructFromDb(spCstGetCostToBuyPointsByCustomer_Result costToBuyPointsItem) {
      _customerCostToBuyPointsFromDbList = new List<CustomerCostToBuyPoints>();
      CustomerCostToBuyPoints custCostToBuy;
      if (costToBuyPointsItem == null) {
        custCostToBuy = new CustomerCostToBuyPoints();
        _customerCostToBuyPointsFromDbList.Add(custCostToBuy);
        return;
      }
      custCostToBuy = new CustomerCostToBuyPoints(costToBuyPointsItem.BasketballSpreadBuy,
              costToBuyPointsItem.BasketballSpreadBuyMax,
              costToBuyPointsItem.BasketballTotalBuy,
              costToBuyPointsItem.BasketballTotalBuyMax,

              costToBuyPointsItem.CollegeBasketballSpreadBuy,
              costToBuyPointsItem.CollegeBasketballSpreadBuyMax,
              costToBuyPointsItem.CollegeBasketballTotalBuy,
              costToBuyPointsItem.CollegeBasketballTotalBuyMax,

              costToBuyPointsItem.FootballSpreadBuy,
              costToBuyPointsItem.FootballSpreadBuyMax,
              costToBuyPointsItem.FootballSpreadBuyOn3,
              costToBuyPointsItem.FootballSpreadBuyOff3,
              costToBuyPointsItem.FootballSpreadBuyOn7,
              costToBuyPointsItem.FootballSpreadBuyOff7,
              costToBuyPointsItem.FootballTotalBuy,
              costToBuyPointsItem.FootballTotalBuyMax,
              costToBuyPointsItem.CollegeFootballSpreadBuy,
              costToBuyPointsItem.CollegeFootballSpreadBuyMax,
              costToBuyPointsItem.CollegeFootballSpreadBuyOn3,
              costToBuyPointsItem.CollegeFootballSpreadBuyOff3,
              costToBuyPointsItem.CollegeFootballSpreadBuyOn7,
              costToBuyPointsItem.CollegeFootballSpreadBuyOff7,
              costToBuyPointsItem.CollegeFootballTotalBuy,
              costToBuyPointsItem.CollegeFootballTotalBuyMax,
              costToBuyPointsItem.ProgressivePointBuyingFlag ?? "N",
              costToBuyPointsItem.ProgressiveChartName ?? ""
      );
      _customerCostToBuyPointsFromDbList.Add(custCostToBuy);
    }

    public void CompareCustomerCostToBuyPointsInfoToUpdate(IEnumerable<CustomerCostToBuyPoints> customerCostToBuyInfoFromFrmList) {
      var retrievedFromDb = _customerCostToBuyPointsFromDbList.FirstOrDefault();
      var retrievedFromFrm = customerCostToBuyInfoFromFrmList.FirstOrDefault();

      if (retrievedFromDb == null || retrievedFromFrm == null) return;

      _itemsToCuLog = new List<LogWriter.CuAccessLogInfo>();
      var buyPoint = new spCstGetCostToBuyPointsByCustomer_Result();

      var doUpdate = false;

      buyPoint.BasketballSpreadBuy = (int)CastNumericValue(retrievedFromDb.BasketballSpreadBuy.ToString(CultureInfo.InvariantCulture), retrievedFromFrm.BasketballSpreadBuy.ToString(CultureInfo.InvariantCulture), "", "", ref doUpdate, false);
      buyPoint.BasketballSpreadBuyMax = (int)CastNumericValue(retrievedFromDb.BasketballSpreadBuyMax.ToString(CultureInfo.InvariantCulture), retrievedFromFrm.BasketballSpreadBuyMax.ToString(CultureInfo.InvariantCulture), "", "", ref doUpdate, false);
      buyPoint.BasketballTotalBuy = (int)CastNumericValue(retrievedFromDb.BasketballTotalBuy.ToString(CultureInfo.InvariantCulture), retrievedFromFrm.BasketballTotalBuy.ToString(CultureInfo.InvariantCulture), "", "", ref doUpdate, false);
      buyPoint.BasketballTotalBuyMax = (int)CastNumericValue(retrievedFromDb.BasketballTotalBuyMax.ToString(CultureInfo.InvariantCulture), retrievedFromFrm.BasketballTotalBuyMax.ToString(CultureInfo.InvariantCulture), "", "", ref doUpdate, false);

      buyPoint.CollegeBasketballSpreadBuy = (int)CastNumericValue(retrievedFromDb.CollegeBasketballSpreadBuy.ToString(CultureInfo.InvariantCulture), retrievedFromFrm.CollegeBasketballSpreadBuy.ToString(CultureInfo.InvariantCulture), "", "", ref doUpdate, false);
      buyPoint.CollegeBasketballSpreadBuyMax = (int)CastNumericValue(retrievedFromDb.CollegeBasketballSpreadBuyMax.ToString(CultureInfo.InvariantCulture), retrievedFromFrm.CollegeBasketballSpreadBuyMax.ToString(CultureInfo.InvariantCulture), "", "", ref doUpdate, false);
      buyPoint.CollegeBasketballTotalBuy = (int)CastNumericValue(retrievedFromDb.CollegeBasketballTotalBuy.ToString(CultureInfo.InvariantCulture), retrievedFromFrm.CollegeBasketballTotalBuy.ToString(CultureInfo.InvariantCulture), "", "", ref doUpdate, false);
      buyPoint.CollegeBasketballTotalBuyMax = (int)CastNumericValue(retrievedFromDb.CollegeBasketballTotalBuyMax.ToString(CultureInfo.InvariantCulture), retrievedFromFrm.CollegeBasketballTotalBuyMax.ToString(CultureInfo.InvariantCulture), "", "", ref doUpdate, false);

      buyPoint.CollegeFootballSpreadBuy = (int)CastNumericValue(retrievedFromDb.CollegeFootballSpreadBuy.ToString(CultureInfo.InvariantCulture), retrievedFromFrm.CollegeFootballSpreadBuy.ToString(CultureInfo.InvariantCulture), "", "", ref doUpdate, false);

      buyPoint.CollegeFootballSpreadBuyMax = (int)CastNumericValue(retrievedFromDb.CollegeFootballSpreadBuyMax.ToString(CultureInfo.InvariantCulture), retrievedFromFrm.CollegeFootballSpreadBuyMax.ToString(CultureInfo.InvariantCulture), "", "", ref doUpdate, false);

      buyPoint.CollegeFootballSpreadBuyOff3 = (int)CastNumericValue(retrievedFromDb.CollegeFootballSpreadBuyOff3.ToString(CultureInfo.InvariantCulture), retrievedFromFrm.CollegeFootballSpreadBuyOff3.ToString(CultureInfo.InvariantCulture), "", "", ref doUpdate, false);

      buyPoint.CollegeFootballSpreadBuyOff7 = (int)CastNumericValue(retrievedFromDb.CollegeFootballSpreadBuyOff7.ToString(CultureInfo.InvariantCulture), retrievedFromFrm.CollegeFootballSpreadBuyOff7.ToString(CultureInfo.InvariantCulture), "", "", ref doUpdate, false);

      buyPoint.CollegeFootballSpreadBuyOn3 = (int)CastNumericValue(retrievedFromDb.CollegeFootballSpreadBuyOn3.ToString(CultureInfo.InvariantCulture), retrievedFromFrm.CollegeFootballSpreadBuyOn3.ToString(CultureInfo.InvariantCulture), "", "", ref doUpdate, false);

      buyPoint.CollegeFootballSpreadBuyOn7 = (int)CastNumericValue(retrievedFromDb.CollegeFootballSpreadBuyOn7.ToString(CultureInfo.InvariantCulture), retrievedFromFrm.CollegeFootballSpreadBuyOn7.ToString(CultureInfo.InvariantCulture), "", "", ref doUpdate, false);

      buyPoint.CollegeFootballTotalBuy = (int)CastNumericValue(retrievedFromDb.CollegeFootballTotalBuy.ToString(CultureInfo.InvariantCulture), retrievedFromFrm.CollegeFootballTotalBuy.ToString(CultureInfo.InvariantCulture), "", "", ref doUpdate, false);

      buyPoint.CollegeFootballTotalBuyMax = (int)CastNumericValue(retrievedFromDb.CollegeFootballTotalBuyMax.ToString(CultureInfo.InvariantCulture), retrievedFromFrm.CollegeFootballTotalBuyMax.ToString(CultureInfo.InvariantCulture), "", "", ref doUpdate, false);

      buyPoint.CustomerID = _currentCustomerId;

      buyPoint.FootballSpreadBuy = (int)CastNumericValue(retrievedFromDb.FootballSpreadBuy.ToString(CultureInfo.InvariantCulture), retrievedFromFrm.FootballSpreadBuy.ToString(CultureInfo.InvariantCulture), "", "", ref doUpdate, false);

      buyPoint.FootballSpreadBuyMax = (int)CastNumericValue(retrievedFromDb.FootballSpreadBuyMax.ToString(CultureInfo.InvariantCulture), retrievedFromFrm.FootballSpreadBuyMax.ToString(CultureInfo.InvariantCulture), "", "", ref doUpdate, false);

      buyPoint.FootballSpreadBuyOff3 = (int)CastNumericValue(retrievedFromDb.FootballSpreadBuyOff3.ToString(CultureInfo.InvariantCulture), retrievedFromFrm.FootballSpreadBuyOff3.ToString(CultureInfo.InvariantCulture), "", "", ref doUpdate, false);

      buyPoint.FootballSpreadBuyOff7 = (int)CastNumericValue(retrievedFromDb.FootballSpreadBuyOff7.ToString(CultureInfo.InvariantCulture), retrievedFromFrm.FootballSpreadBuyOff7.ToString(CultureInfo.InvariantCulture), "", "", ref doUpdate, false);

      buyPoint.FootballSpreadBuyOn3 = (int)CastNumericValue(retrievedFromDb.FootballSpreadBuyOn3.ToString(CultureInfo.InvariantCulture), retrievedFromFrm.FootballSpreadBuyOn3.ToString(CultureInfo.InvariantCulture), "", "", ref doUpdate, false);

      buyPoint.FootballSpreadBuyOn7 = (int)CastNumericValue(retrievedFromDb.FootballSpreadBuyOn7.ToString(CultureInfo.InvariantCulture), retrievedFromFrm.FootballSpreadBuyOn7.ToString(CultureInfo.InvariantCulture), "", "", ref doUpdate, false);

      buyPoint.FootballTotalBuy = (int)CastNumericValue(retrievedFromDb.FootballTotalBuy.ToString(CultureInfo.InvariantCulture), retrievedFromFrm.FootballTotalBuy.ToString(CultureInfo.InvariantCulture), "", "", ref doUpdate, false);

      buyPoint.FootballTotalBuyMax = (int)CastNumericValue(retrievedFromDb.FootballTotalBuyMax.ToString(CultureInfo.InvariantCulture), retrievedFromFrm.FootballTotalBuyMax.ToString(CultureInfo.InvariantCulture), "", "", ref doUpdate, false);

      buyPoint.ProgressiveChartName = CastValue(retrievedFromDb.ProgressiveChartName, retrievedFromFrm.ProgressiveChartName, "", "", ref doUpdate, false) ?? string.Empty;

      buyPoint.ProgressivePointBuyingFlag = CastValue(retrievedFromDb.ProgressivePointBuyingFlag, retrievedFromFrm.ProgressivePointBuyingFlag == "True" ? "Y" : "N", "", "", ref doUpdate, false);

      if (!doUpdate) return;
      using (var buyPoints = new BuyPoints(_moduleInfo)) {
        buyPoints.DeleteCostToBuyPointsInfo(_currentCustomerId);
        buyPoints.AddCostToBuyPointsInfo(buyPoint);
      }

      var logInfo = new LogWriter.CuAccessLogInfo { ProgramName = _moduleInfo.Description, Operation = "Changed " + _currentCustomerId, Data = "Buying Points Info Changed" };
      _itemsToCuLog.Add(logInfo);

      using (var lw = new LogWriter(_moduleInfo)) {
        lw.WriteToCuAccessLog(_itemsToCuLog);
      }
    }

    public void LoadcustomerVigDiscountOptionsToStructFromDb(IEnumerable<spCstGetCustomerVigDiscountOptions_Result> dbList) {
      _customerVigDiscountOptions = new List<CustomerVigDiscountElem>();

      foreach (var list in dbList) {
        var result = new CustomerVigDiscountElem(
          list.Sport.Split('-')[0].Trim(),
          list.Sport.Split('-')[1].Trim(),
          list.Period ?? "",
          list.wagerType,
          list.Wager_Type,
          list.CU_Vig_Disc__ ?? "",
          list.CU_Exp_Date ?? "",
          list.Inet_Vig_Disc__ ?? "",
          list.Inet_Exp_Date ?? ""
          );

        _customerVigDiscountOptions.Add(result);
      }
    }

    public void CompareCustomerVigDiscountInfoToUpdate(IEnumerable<CustomerVigDiscountElem> modifiedVigDiscounts) {
      if (_customerVigDiscountOptions == null) return;

      var atLeastOneRowModified = false;

      using (var cust = new Customers(_moduleInfo)) {

        foreach (var item in modifiedVigDiscounts) {
          var origCuVigDisc = "0";
          var origInetVigDisc = "0";

          var finalSport = item.SportType;
          var finalSportSubType = item.SportSubType;
          var finalPeriod = item.Period;
          var finalWagerType = item.WagerType;
          var finalWagerTypeName = item.WagerTypeName;
          var finalCuVigDisc = item.CuVigDisc;
          var finalCuExpDate = item.CuExpDate;
          var finalInetVigDisc = item.InetVigDisc;
          var finalInetExpDate = item.InetExpDate;

          var vigDisc = (from li in _customerVigDiscountOptions
                         where li.SportType.Trim() == finalSport.Trim() && li.SportSubType.Trim() == finalSportSubType.Trim()
                               && li.Period.Trim() == finalPeriod.Trim() && li.WagerType.Trim() == finalWagerType.Trim()
                         select li.CuVigDisc).FirstOrDefault();

          if (!string.IsNullOrEmpty(vigDisc))
            origCuVigDisc = (double.Parse(vigDisc)).ToString(CultureInfo.InvariantCulture);

          var origCuExpDate = (from li in _customerVigDiscountOptions
                               where li.SportType.Trim() == finalSport.Trim() && li.SportSubType.Trim() == finalSportSubType.Trim()
                                     && li.Period.Trim() == finalPeriod.Trim() && li.WagerType.Trim() == finalWagerType.Trim()
                               select li.CuExpDate).FirstOrDefault();

          vigDisc = (from li in _customerVigDiscountOptions
                     where li.SportType.Trim() == finalSport.Trim() && li.SportSubType.Trim() == finalSportSubType.Trim()
                           && li.Period.Trim() == finalPeriod.Trim() && li.WagerType.Trim() == finalWagerType.Trim()
                     select li.InetVigDisc).FirstOrDefault();

          if (!string.IsNullOrEmpty(vigDisc))
            origInetVigDisc = (double.Parse(vigDisc)).ToString(CultureInfo.InvariantCulture);

          var origInetExpDate = (from li in _customerVigDiscountOptions
                                 where li.SportType.Trim() == finalSport.Trim() && li.SportSubType.Trim() == finalSportSubType.Trim()
                                       && li.Period.Trim() == finalPeriod.Trim() && li.WagerType.Trim() == finalWagerType.Trim()
                                 select li.InetExpDate).FirstOrDefault();

          _itemsToCuLog = new List<LogWriter.CuAccessLogInfo>();
          var rowModified = false;


          CastNumericValue(origCuVigDisc.Trim(), finalCuVigDisc.Trim(), finalSport + " - " + finalSportSubType + " / " + finalPeriod + " / " + finalWagerTypeName + " - " + finalCuVigDisc, "Vig Disc Call Unit Percent", ref rowModified);
          if (rowModified) atLeastOneRowModified = true;

          CastNumericValue(origInetVigDisc.Trim(), finalInetVigDisc.Trim(), finalSport + " - " + finalSportSubType + " / " + finalPeriod + " / " + finalWagerTypeName + " - " + finalInetVigDisc, "Vig Disc Inet Percent", ref rowModified);
          if (rowModified) atLeastOneRowModified = true;

          CastDateValue(origCuExpDate, finalCuExpDate, finalSport + " - " + finalSportSubType + "/" + finalPeriod + "/" + finalWagerTypeName + " - " + finalCuExpDate, "Vig Disc Call Unit Exp Date", ref rowModified);
          if (rowModified) atLeastOneRowModified = true;

          CastDateValue(origInetExpDate, finalInetExpDate, finalSport + " - " + finalSportSubType + "/" + finalPeriod + "/" + finalWagerTypeName + " - " + finalInetExpDate, "Vig Disc Inet Exp Date", ref rowModified);
          if (rowModified) atLeastOneRowModified = true;

          if (!rowModified) continue;
          cust.UpdateCustomerVigDiscountOption(_currentCustomerId.Trim(), finalSport.Trim(), finalSportSubType.Trim(), GetPeriodNumberFromName(finalPeriod.Trim()), finalWagerType.Trim(), finalCuVigDisc, finalCuExpDate, finalInetVigDisc, finalInetExpDate);

          using (var lw = new LogWriter(_moduleInfo)) {
            lw.WriteToCuAccessLog(_itemsToCuLog);
          }
        }

        if (!atLeastOneRowModified) return;
        var gvSet = cust.GetCustomerVigDiscountOptions(_currentCustomerId).ToList();
        LoadcustomerVigDiscountOptionsToStructFromDb(gvSet);
      }
    }

    public void LoadcustomerWagerLimitsToStructFromDb(IEnumerable<spCstGetCustomerWagerLimits_Result> dbList) {
      _customerWagerLimitsOptions = new List<CustomerWagerLimit>();

      foreach (var list in dbList) {
        var result = new CustomerWagerLimit(
          list.SportEnabled ?? false,
          list.OverrideCircle ?? false,
          list.sportType ?? "",
          list.SportSubType ?? "",
          list.Period ?? "",
          list.WagerType ?? "",
          list.WagerTypeDesc ?? "",
          list.CuLimit.ToString(CultureInfo.InvariantCulture),
          list.InetLimit.ToString(CultureInfo.InvariantCulture),
          list.TempCuLimit.ToString(CultureInfo.InvariantCulture),
          list.TempInetLimit.ToString(CultureInfo.InvariantCulture),
          list.TempLimitsExpiration == null ? "" : list.TempLimitsExpiration.Value.ToLongDateString()
          );

        _customerWagerLimitsOptions.Add(result);
      }
    }

    public void LoadcustomerParlayWagerLimitsToStructFromDb(IEnumerable<spCstGetCustomerParlayTeaserWagerLimits_Result> dbList) {
      _customerParlayTeaserWagerLimitsOptions = new List<CustomerParlayTeaserWagerLimit>();

      foreach (var list in dbList) {
        var result = new CustomerParlayTeaserWagerLimit(
          list.sportType ?? "",
          list.SportSubType ?? "",
          list.Period ?? "",
          list.WagerType ?? "",
          list.WagerTypeDesc ?? "",
          list.CuLimit.ToString(CultureInfo.InvariantCulture),
          list.InetLimit.ToString(CultureInfo.InvariantCulture),
          list.WagerTypePT ?? ""
          );

        _customerParlayTeaserWagerLimitsOptions.Add(result);
      }
    }

    public void CompareCustomerWagerLimitsInfoToUpdate(IEnumerable<CustomerWagerLimit> modifiedWagerLimits) {
      if (_customerWagerLimitsOptions == null) return;

      var atLeastOneRowModified = false;
      using (var cust = new Customers(_moduleInfo)) {
        foreach (var item in modifiedWagerLimits) {
          var finalSportIsEnabled = item.SportIsEnabled;
          var finalOverrideCircle = item.OverrideCircle;
          var finalSport = item.SportType;
          var finalSportSubType = item.SportSubType;
          var finalPeriod = item.Period;
          var finalWagerType = item.WagerType;
          var finalWagerTypeName = item.WagerTypeName;
          var finalCuLimit = item.CuLimit;
          var finalInetLimit = item.InetLimit;
          var finalTempCuLimit = item.TempCuLimit;
          var finalTempInetLimit = item.TempInetLimit;
          var finalTempLimitsExpiration = item.TempLimitsExpiration;


          var limits = (from li in _customerWagerLimitsOptions
                        where li.SportType.Trim() == finalSport.Trim() && li.SportSubType.Trim() == finalSportSubType.Trim()
                              && li.Period.Trim() == finalPeriod.Trim() && li.WagerType.Trim() == finalWagerType.Trim()
                        select li).FirstOrDefault();

          var origSportIsEnabled = limits == null || limits.SportIsEnabled;
          var origOverrideCircle = limits != null && limits.OverrideCircle;
          var origCuLimit = limits != null && !string.IsNullOrEmpty(limits.CuLimit) ? (double.Parse(limits.CuLimit) / 100).ToString(CultureInfo.InvariantCulture) : "0";
          var origInetLimit = limits != null && !string.IsNullOrEmpty(limits.InetLimit) ? (double.Parse(limits.InetLimit) / 100).ToString(CultureInfo.InvariantCulture) : "0";
          var origTempCuLimit = limits != null && !string.IsNullOrEmpty(limits.TempCuLimit) ? (double.Parse(limits.TempCuLimit) / 100).ToString(CultureInfo.InvariantCulture) : "0";
          var origTempInetLimit = limits != null && !string.IsNullOrEmpty(limits.TempInetLimit) ? (double.Parse(limits.TempInetLimit) / 100).ToString(CultureInfo.InvariantCulture) : "0";
          var origTempLimitsExpiration = limits != null ? limits.TempLimitsExpiration : "";

          _itemsToCuLog = new List<LogWriter.CuAccessLogInfo>();
          var rowModified = false;

          CastBoolValue(origSportIsEnabled.ToString(), finalSportIsEnabled.ToString(), finalSport + "/" + finalSportSubType + "/" + finalPeriod + "/" + finalWagerTypeName + " - " + finalSportIsEnabled.ToString(), "Dtl Call Unit Sport Enabled", ref rowModified);

          CastBoolValue(origOverrideCircle.ToString(), finalOverrideCircle.ToString(), finalSport + "/" + finalSportSubType + "/" + finalPeriod + "/" + finalWagerTypeName + " - " + finalSportIsEnabled.ToString(), "Dtl Call Unit Override Circle Limit", ref rowModified);

          CastNumericValue(origCuLimit.Trim(), finalCuLimit.Trim(), finalSport + "/" + finalSportSubType + "/" + finalPeriod + "/" + finalWagerTypeName + " - " + finalCuLimit, "Dtl Call Unit Max Bet", ref rowModified);
          if (rowModified) atLeastOneRowModified = true;

          CastNumericValue(origInetLimit.Trim(), finalInetLimit.Trim(), finalSport + "/" + finalSportSubType + "/" + finalPeriod + "/" + finalWagerTypeName + " - " + finalInetLimit, "Dtl Inet Max Bet", ref rowModified);
          if (rowModified) atLeastOneRowModified = true;

          var tempLimitsAreValid = true;
          CastNumericValue(origTempCuLimit.Trim(), finalTempCuLimit.Trim(), finalSport + "/" + finalSportSubType + "/" + finalPeriod + "/" + finalWagerTypeName + " - " + finalTempCuLimit, "Dtl Temp Call Unit Max Bet", ref rowModified);
          if (rowModified) {
            atLeastOneRowModified = true;
            tempLimitsAreValid = !string.IsNullOrEmpty(finalTempCuLimit);
          }

          CastNumericValue(origTempInetLimit.Trim(), finalTempInetLimit.Trim(), finalSport + "/" + finalSportSubType + "/" + finalPeriod + "/" + finalWagerTypeName + " - " + finalTempInetLimit, "Dtl Temp Inet Max Bet", ref rowModified);
          if (rowModified) {
            atLeastOneRowModified = true;
            tempLimitsAreValid = tempLimitsAreValid && !string.IsNullOrEmpty(finalTempInetLimit);
          }

          CastDateValue(origTempLimitsExpiration, finalTempLimitsExpiration, finalSport + "/" + finalSportSubType + "/" + finalPeriod + "/" + finalWagerTypeName + " - " + finalTempLimitsExpiration, "Dtl Temp Max Bet Date", ref rowModified);
          if (rowModified) {
            atLeastOneRowModified = true;
            tempLimitsAreValid = tempLimitsAreValid && !string.IsNullOrEmpty(finalTempLimitsExpiration);
          }

          if (!tempLimitsAreValid) {
            finalTempCuLimit = "0";
            finalTempInetLimit = "0";
            finalTempLimitsExpiration = "";
          }

          if (rowModified) {
            cust.UpdateCustomerWagerLimitsOption(_currentCustomerId.Trim(), finalSportIsEnabled, finalOverrideCircle, finalSport.Trim(), finalSportSubType.Trim(), GetPeriodNumberFromName(finalPeriod.Trim()), finalWagerType.Trim(), finalCuLimit, finalInetLimit, Environment.UserName, finalTempCuLimit, finalTempInetLimit, finalTempLimitsExpiration);

            using (var lw = new LogWriter(_moduleInfo)) {
              lw.WriteToCuAccessLog(_itemsToCuLog);
            }
          }
          _itemsToCuLog = null;
        }

        if (!atLeastOneRowModified) return;
        var wagerLimits = cust.GetCustomerWagerLimits(_currentCustomerId).ToList();
        LoadcustomerWagerLimitsToStructFromDb(wagerLimits);
      }
    }

    public void CompareCustomerParlayTeaserWagerLimitsInfoToUpdate(IEnumerable<CustomerParlayTeaserWagerLimit> modifiedWagerLimits) {
      if (_customerParlayTeaserWagerLimitsOptions == null || modifiedWagerLimits == null) return;

      using (var cust = new Customers(_moduleInfo)) {
        foreach (var item in modifiedWagerLimits) {
          var finalSport = item.SportType;
          var finalSportSubType = item.SportSubType;
          var finalPeriod = item.Period;
          var finalWagerType = item.WagerType;
          var finalWagerTypeName = item.WagerTypeName;
          var finalCuLimit = item.CuLimit;
          var finalInetLimit = item.InetLimit;
          var wagerTypePt = item.WagerTypePT;

          var limits = (from li in _customerParlayTeaserWagerLimitsOptions
                        where li.SportType.Trim() == finalSport.Trim() && li.SportSubType.Trim() == finalSportSubType.Trim()
                              && li.Period.Trim() == finalPeriod.Trim() && li.WagerType.Trim() == finalWagerType.Trim()
                        select li).FirstOrDefault();

          var origCuLimit = limits != null && !string.IsNullOrEmpty(limits.CuLimit) ? (double.Parse(limits.CuLimit) / 100).ToString(CultureInfo.InvariantCulture) : "0";
          var origInetLimit = limits != null && !string.IsNullOrEmpty(limits.InetLimit) ? (double.Parse(limits.InetLimit) / 100).ToString(CultureInfo.InvariantCulture) : "0";

          _itemsToCuLog = new List<LogWriter.CuAccessLogInfo>();
          var rowModified = false;

          CastNumericValue(origCuLimit.Trim(), finalCuLimit.Trim(), finalSport + "/" + finalSportSubType + "/" + finalPeriod + "/" + finalWagerTypeName + " - " + finalCuLimit, "Dtl Parlay Call Unit Max Bet", ref rowModified);

          CastNumericValue(origInetLimit.Trim(), finalInetLimit.Trim(), finalSport + "/" + finalSportSubType + "/" + finalPeriod + "/" + finalWagerTypeName + " - " + finalInetLimit, "Dtl Parlay Inet Max Bet", ref rowModified);

          if (rowModified) {
            cust.UpdateCustomerParlayWagerLimitsOption(_currentCustomerId.Trim(), finalSport.Trim(), finalSportSubType.Trim(), GetPeriodNumberFromName(finalPeriod.Trim()), finalWagerType.Trim(), finalCuLimit, finalInetLimit, wagerTypePt, Environment.UserName);

            using (var lw = new LogWriter(_moduleInfo)) {
              lw.WriteToCuAccessLog(_itemsToCuLog);
            }
          }
          _itemsToCuLog = null;
        }

        //if (!atLeastOneRowModified) return;
        //var wagerLimits = cust.GetCustomerParlayWagerLimits(_currentCustomerId).ToList();
        //LoadcustomerParlayWagerLimitsToStructFromDb(wagerLimits);
      }
    }

    public void LoadCustomerAsAgentInfoToStructFromDb(spAgGetCustomerAsAgentInfo_Result agent, String currentMakeUp, DateTime distributionDate) {

      _customerAsAgentInfoFromDbList = new List<AgentInfo>();

      var distributeToMasterFlag = agent.DistributeToMasterFlag.Trim();
      if (distributeToMasterFlag == "") distributeToMasterFlag = "N";

      var changeTempCreditFlag = agent.ChangeTempCreditFlag.Trim();
      if (changeTempCreditFlag == "") changeTempCreditFlag = "N";

      var enterTransactionFlag = agent.EnterTransactionFlag.Trim();
      if (enterTransactionFlag == "") enterTransactionFlag = "N";

      var suspendWageringFlag = agent.SuspendWageringFlag.Trim();
      if (suspendWageringFlag == "") suspendWageringFlag = "N";

      var updateCommentsFlag = agent.UpdateCommentsFlag.Trim();
      if (updateCommentsFlag == "") updateCommentsFlag = "N";

      var manageLinesFlag = agent.ManageLinesFlag.Trim();
      if (manageLinesFlag == "") manageLinesFlag = "N";

      var addNewAccountFlag = agent.AddNewAccountFlag.Trim();
      if (addNewAccountFlag == "") addNewAccountFlag = "N";

      var changeCreditLimitFlag = agent.ChangeCreditLimitFlag.Trim();
      if (changeCreditLimitFlag == "") changeCreditLimitFlag = "N";

      var changeSettleFigureFlag = agent.ChangeSettleFigureFlag.Trim();
      if (changeSettleFigureFlag == "") changeSettleFigureFlag = "N";

      var changeWagerLimitFlag = agent.ChangeWagerLimitFlag.Trim();
      if (changeWagerLimitFlag == "") changeWagerLimitFlag = "N";

      var enterBettingAdjustmentFlag = agent.EnterBettingAdjustmentFlag.Trim();
      if (enterBettingAdjustmentFlag == "") enterBettingAdjustmentFlag = "N";

      var setMinimumBetAmountFlag = agent.SetMinimumBetAmountFlag.Trim();
      if (setMinimumBetAmountFlag == "") setMinimumBetAmountFlag = "N";

      var includeFpLossesFlag = agent.IncludeFpLossesFlag.Trim();
      if (includeFpLossesFlag == "") includeFpLossesFlag = "N";

      var dbAgentStruct = new AgentInfo(
          agent.AgentID,
          agent.CommissionType,
          agent.CommissionPercent.ToString(CultureInfo.InvariantCulture),
          agent.MasterAgentID,
          distributeToMasterFlag,
          agent.HeadCountRate.ToString(CultureInfo.InvariantCulture),
          agent.DistributeNoFundsFlag.Trim(),
          changeTempCreditFlag,
          enterTransactionFlag,
          suspendWageringFlag,
          updateCommentsFlag,
          manageLinesFlag,
          agent.CasinoFeePercent.ToString(CultureInfo.InvariantCulture),
          agent.InetHeadCountRate.ToString(CultureInfo.InvariantCulture),
          addNewAccountFlag,
          changeCreditLimitFlag,
          changeSettleFigureFlag,
          changeWagerLimitFlag,
          agent.CustomerIDPrefix.Trim(),
          enterBettingAdjustmentFlag,
          setMinimumBetAmountFlag,
          agent.CasinoHeadCountRate.ToString(CultureInfo.InvariantCulture),
          includeFpLossesFlag,
          currentMakeUp,
          distributionDate
          );

      _customerAsAgentInfoFromDbList.Add(dbAgentStruct);
    }

    public void CompareCustomerAsAgentInfoToUpdate(IEnumerable<AgentInfo> customerAsAgentInfoFromFrmList) {
      if (customerAsAgentInfoFromFrmList == null || _customerAsAgentInfoFromDbList == null) return;
      var retrievedFromDb = _customerAsAgentInfoFromDbList.FirstOrDefault();
      var retrievedFromFrm = customerAsAgentInfoFromFrmList.FirstOrDefault();

      if (retrievedFromDb == null || retrievedFromFrm == null) return;

      _itemsToCuLog = new List<LogWriter.CuAccessLogInfo>();

      using (var agent = new Agents(_moduleInfo)) {
        var agCustomer = agent.GetAgent(_currentCustomerId);

        var doUpdate = false;

        agCustomer.AddNewAccountFlag = CastValue(retrievedFromDb.AddNewAccountFlag, retrievedFromFrm.AddNewAccountFlag,
          retrievedFromFrm.AddNewAccountFlag == "Y" ? "Yes" : "No", "Agent Add New Account", ref doUpdate);

        var updateMakeUp = false;
        var newMakeup = CastValue(retrievedFromDb.CurrentMakeUp, retrievedFromFrm.CurrentMakeUp ?? "0", "", ref updateMakeUp);
        if (!updateMakeUp) newMakeup = "";


        double? finalValueToTest = CastNumericValue(retrievedFromDb.CasinoFeePercent, retrievedFromFrm.CasinoFeePercent, "Casino Commission Percent", ref doUpdate);
        if (finalValueToTest == null || finalValueToTest == 0) agCustomer.CasinoFeePercent = null;
        else agCustomer.CasinoFeePercent = (int)finalValueToTest;

        finalValueToTest = CastNumericValue(retrievedFromDb.CasinoHeadCountRate, retrievedFromFrm.CasinoHeadCountRate, "Casino Head Count", ref doUpdate);
        if (finalValueToTest == null || finalValueToTest == 0) agCustomer.CasinoHeadCountRate = null;
        else agCustomer.CasinoHeadCountRate = (int)finalValueToTest;

        agCustomer.ChangeCreditLimitFlag = CastValue(retrievedFromDb.ChangeCreditLimitFlag, retrievedFromFrm.ChangeCreditLimitFlag,
          retrievedFromFrm.ChangeCreditLimitFlag == "Y" ? "Yes" : "No", "Agent Change Credit Limit", ref doUpdate);

        agCustomer.ChangeSettleFigureFlag = CastValue(retrievedFromDb.ChangeSettleFigureFlag, retrievedFromFrm.ChangeSettleFigureFlag,
          retrievedFromFrm.ChangeSettleFigureFlag == "Y" ? "Yes" : "No", "Agent Change Settle Figure", ref doUpdate);

        agCustomer.ChangeTempCreditFlag = CastValue(retrievedFromDb.ChangeTempCreditFlag, retrievedFromFrm.ChangeTempCreditFlag,
          retrievedFromFrm.ChangeTempCreditFlag == "Y" ? "Yes" : "No", "Agent Change Temp Credit", ref doUpdate);

        agCustomer.ChangeWagerLimitFlag = CastValue(retrievedFromDb.ChangeWagerLimitFlag, retrievedFromFrm.ChangeWagerLimitFlag,
          retrievedFromFrm.ChangeWagerLimitFlag == "Y" ? "Yes" : "No", "Agent Change Wager Limit", ref doUpdate);

        finalValueToTest = CastNumericValue(retrievedFromDb.CommissionPercent, retrievedFromFrm.CommissionPercent, "Sportsbook Commission Percent", ref doUpdate);
        if (finalValueToTest == null || finalValueToTest == 0) agCustomer.CommissionPercent = null;
        else agCustomer.CommissionPercent = (int)finalValueToTest;

        agCustomer.CommissionType = CastValue(retrievedFromDb.CommissionType, retrievedFromFrm.CommissionType,
          GetCommisionTypeName(retrievedFromFrm.CommissionType), "Commission Type", ref doUpdate);

        agCustomer.CustomerIDPrefix = CastValue(retrievedFromDb.CustomerIdPrefix, retrievedFromFrm.CustomerIdPrefix, "Commission Type", ref doUpdate);

        agCustomer.ChangeWagerLimitFlag = CastValue(retrievedFromDb.ChangeWagerLimitFlag, retrievedFromFrm.ChangeWagerLimitFlag,
          retrievedFromFrm.ChangeWagerLimitFlag == "Y" ? "Yes" : "No", "Agent Change Wager Limit", ref doUpdate);

        agCustomer.DistributeToMasterFlag = CastValue(retrievedFromDb.DistributeToMasterFlag, retrievedFromFrm.DistributeToMasterFlag,
          retrievedFromFrm.DistributeToMasterFlag == "Y" ? "Yes" : "No", "Distribute Funds to Master", ref doUpdate);

        agCustomer.EnterBettingAdjustmentFlag = CastValue(retrievedFromDb.EnterBettingAdjustmentFlag, retrievedFromFrm.EnterBettingAdjustmentFlag,
          retrievedFromFrm.EnterBettingAdjustmentFlag == "Y" ? "Yes" : "No", "Agent Enter Betting Adjustment", ref doUpdate);

        agCustomer.EnterTransactionFlag = CastValue(retrievedFromDb.EnterTransactionFlag, retrievedFromFrm.EnterTransactionFlag,
          retrievedFromFrm.EnterTransactionFlag == "Y" ? "Yes" : "No", "Agent Enter Transactions", ref doUpdate);

        finalValueToTest = CastNumericValue(retrievedFromDb.HeadCountRate, retrievedFromFrm.HeadCountRate, "Call Unit Head Count", ref doUpdate);
        if (finalValueToTest == null || finalValueToTest == 0) agCustomer.HeadCountRate = null;
        else agCustomer.HeadCountRate = (int)finalValueToTest;

        agCustomer.IncludeFpLossesFlag = CastValue(retrievedFromDb.IncludeFpLossesFlag, retrievedFromFrm.IncludeFpLossesFlag,
          retrievedFromFrm.IncludeFpLossesFlag == "Y" ? "Yes" : "No", "Include Free Play Losses", ref doUpdate);

        finalValueToTest = CastNumericValue(retrievedFromDb.InetHeadCountRate, retrievedFromFrm.InetHeadCountRate, "Inet Head Count", ref doUpdate);
        if (finalValueToTest == null || finalValueToTest == 0) agCustomer.InetHeadCountRate = null;
        else agCustomer.InetHeadCountRate = (int)finalValueToTest;

        agCustomer.ManageLinesFlag = CastValue(retrievedFromDb.ManageLinesFlag, retrievedFromFrm.ManageLinesFlag,
          retrievedFromFrm.ManageLinesFlag == "Y" ? "Yes" : "No", "Agent Manage Lines", ref doUpdate);

        agCustomer.MasterAgentID = CastValue(retrievedFromDb.MasterAgentId, retrievedFromFrm.MasterAgentId, "Master Agent", ref doUpdate);

        agCustomer.SetMinimumBetAmountFlag = CastValue(retrievedFromDb.SetMinimumBetAmountFlag, retrievedFromFrm.SetMinimumBetAmountFlag,
          retrievedFromFrm.SetMinimumBetAmountFlag == "Y" ? "Yes" : "No", "Agent Set Minimum Bet Amount", ref doUpdate);

        agCustomer.SuspendWageringFlag = CastValue(retrievedFromDb.SuspendWageringFlag, retrievedFromFrm.SuspendWageringFlag,
          retrievedFromFrm.SuspendWageringFlag == "Y" ? "Yes" : "No", "Agent Suspend Wagering", ref doUpdate);

        agCustomer.UpdateCommentsFlag = CastValue(retrievedFromDb.UpdateCommentsFlag, retrievedFromFrm.UpdateCommentsFlag,
          retrievedFromFrm.UpdateCommentsFlag == "Y" ? "Yes" : "No", "Agent Update Comments", ref doUpdate);

        if (doUpdate) {
          agent.UpdateAgentInfo(agCustomer);
          using (var lw = new LogWriter(_moduleInfo)) {
            lw.WriteToCuAccessLog(_itemsToCuLog);
          }
        }

        if (updateMakeUp) {

          agent.UpdateAgentMakeup(agCustomer.AgentID.Trim(), newMakeup, retrievedFromDb.DistributionDate);

          var logInfo = new LogWriter.CuAccessLogInfo { ProgramName = _moduleInfo.Description, Operation = "Changed " + _currentCustomerId, Data = "Change Makeup Figure - " + newMakeup };
          using (var lw = new LogWriter(_moduleInfo)) {
            lw.WriteToCuAccessLog(logInfo);
          }
        }

        if (!doUpdate) return;
        using (var agts = new Agents(_moduleInfo)) {
          var result = agts.GetAgentInfo(retrievedFromDb.AgentId).FirstOrDefault();
          var makeUp = retrievedFromDb.CurrentMakeUp;
          if (updateMakeUp) {
            makeUp = newMakeup;
          }

          LoadCustomerAsAgentInfoToStructFromDb(result, makeUp, retrievedFromDb.DistributionDate);
        }
      }
    }

    public void LoadCustomerTransactionInfoToStructFromDb(spTrnGetCustomerTransactionByDocNumber_Result customerTransaction) {
      _customerEnteredTransaction = new List<CustomerTransaction>();

      var custTransactionStruct = new CustomerTransaction(
          customerTransaction.DocumentNumber,
          customerTransaction.TranCode,
          customerTransaction.Amount.ToString(),
          customerTransaction.EnteredAmount.ToString(CultureInfo.InvariantCulture),
          customerTransaction.Description,
          customerTransaction.Reference ?? "",
          customerTransaction.HoldPercent.ToString(CultureInfo.InvariantCulture),
          customerTransaction.ReleaseDate.ToShortDateString(),
          customerTransaction.CCNumber,
          customerTransaction.CCExpires,
          customerTransaction.CCApproval,
          customerTransaction.DailyFigureDate.ToShortDateString(),
          customerTransaction.CasinoAdjustmentFlag ?? "N",
          customerTransaction.PaymentBy
          );

      _customerEnteredTransaction.Add(custTransactionStruct);
    }

    public void CompareCustomerTransactionInfoToUpdate(CustomerTransaction customerTransactionInfoFromFrm, DateTime editedTranDateTime) {
      _itemsToCuLog = new List<LogWriter.CuAccessLogInfo>();

      var retrievedFromDb = _customerEnteredTransaction.FirstOrDefault();

      if (retrievedFromDb == null) return;

      using (var trans = new Transactions(_moduleInfo)) {
        var tran = trans.GetTransaction(retrievedFromDb.DocumentNumber);
        tran.LastUpdateDateTime = editedTranDateTime;
        tran.LastUpdateLoginID = Environment.UserName;

        var doUpdate = false;


        var valueFromdb = retrievedFromDb.Amount;
        var valueFromFrm = (double.Parse(customerTransactionInfoFromFrm.Amount) * 100).ToString(CultureInfo.InvariantCulture);
        TransacCode = retrievedFromDb.TranCode;

        if (valueFromdb != valueFromFrm) {
          tran.Amount = double.Parse(valueFromFrm);
          doUpdate = true;
          var logInfo = new LogWriter.CuAccessLogInfo { ProgramName = _moduleInfo.Description, Operation = "Changed " + _currentCustomerId, Data = "Transaction Amount $" + String.Format("{0:###.###}", double.Parse(valueFromFrm) / 100) };

          AmountToAdjustBalance = double.Parse(valueFromdb) - double.Parse(valueFromFrm);

          if (TransacCode == "C") {
            AmountToAdjustBalance *= -1;
          }

          _itemsToCuLog.Add(logInfo);

        }

        valueFromdb = retrievedFromDb.EnteredAmount;
        valueFromFrm = (double.Parse(customerTransactionInfoFromFrm.EnteredAmount) * 100).ToString(CultureInfo.InvariantCulture);
        if (valueFromdb != valueFromFrm) {
          tran.EnteredAmount = double.Parse(valueFromFrm);
          var logInfo = new LogWriter.CuAccessLogInfo { ProgramName = _moduleInfo.Description, Operation = "Changed " + _currentCustomerId, Data = "Transaction Entered Amount $" + String.Format("{0:###.###}", double.Parse(valueFromFrm) / 100) };
          _itemsToCuLog.Add(logInfo);
        }

        tran.CasinoAdjustmentFlag = CastValue(retrievedFromDb.CasinoAdjustment, customerTransactionInfoFromFrm.CasinoAdjustment,
          customerTransactionInfoFromFrm.CasinoAdjustment == "Y" ? "Yes" : "No", "Casino Adjustment Flag", ref doUpdate);

        tran.CCApproval = CastValue(retrievedFromDb.CcApprovalNumber, customerTransactionInfoFromFrm.CcApprovalNumber,
          "Credit Card Approval Number", ref doUpdate);

        tran.CCExpires = CastValue(retrievedFromDb.CcExpires.Trim(), customerTransactionInfoFromFrm.CcExpires.Trim(),
          "Credit Card Expiration Date", ref doUpdate);

        tran.CCNumber = CastValue(retrievedFromDb.CcNumber.Trim(), customerTransactionInfoFromFrm.CcNumber.Trim(),
          "Credit Card Number", ref doUpdate);

        tran.DailyFigureDate = CastDateValue(DateTime.Parse(retrievedFromDb.DailyFigureDate).ToShortDateString(), DateTime.Parse(customerTransactionInfoFromFrm.DailyFigureDate).ToShortDateString(),
          DateTime.Parse(customerTransactionInfoFromFrm.DailyFigureDate).ToShortDateString(), "Transaction Daily Figure Date", ref doUpdate);

        tran.Description = CastValue(retrievedFromDb.Description.Trim(), customerTransactionInfoFromFrm.Description.Trim(),
          "Description", ref doUpdate);

        var fieldChanged = false;
        tran.HoldPercent = (int?)CastNumericValue(retrievedFromDb.HoldFundsPercent, customerTransactionInfoFromFrm.HoldFundsPercent, "Hold Percent", ref fieldChanged);
        if (fieldChanged) {
          doUpdate = true;
          tran.HoldAmount = double.Parse(customerTransactionInfoFromFrm.EnteredAmount) * 100 * int.Parse(customerTransactionInfoFromFrm.HoldFundsPercent) / 100;
        }

        tran.ReleaseDate = CastDateValue(DateTime.Parse(retrievedFromDb.HoldFundsReleaseDate).ToShortDateString(), DateTime.Parse(customerTransactionInfoFromFrm.HoldFundsReleaseDate).ToShortDateString(), "", "Hold Amount Expiration Date", ref doUpdate);

        tran.Reference = CastValue(retrievedFromDb.Reference.Trim(), customerTransactionInfoFromFrm.Reference.Trim(),
          "Transaction Reference", ref doUpdate);

        tran.PaymentBy = CastValue(retrievedFromDb.PaymentBy.Trim(), customerTransactionInfoFromFrm.PaymentBy.Trim(),
"Transaction PaymentBy", ref doUpdate);

        if (!doUpdate) return;
        trans.UpdateTransactionInfo(tran, AmountToAdjustBalance);

        using (var lw = new LogWriter(_moduleInfo)) {
          lw.WriteToCuAccessLog(_itemsToCuLog);
        }
      }

    }

    private static string GetCommisionTypeName(string valueFromFrm) {
      var commisionType = "";

      switch (valueFromFrm) {

        case "A":
          commisionType = "Affiliate Split";
          break;

        case "R":
          commisionType = "Agent Red Figure";
          break;

        case "Q":
          commisionType = "Affiliate Weekly Profit";
          break;

        case "P":
          commisionType = "Agent Weekly Profit";
          break;

        case "T":
          commisionType = "Affiliate Red Figure";
          break;

        case "S":
          commisionType = "Agent Split";
          break;


      }

      return commisionType;
    }

    private static int GetPeriodNumberFromName(string periodName) {

      var periodNumber = new int();

      switch (periodName) {
        case "1st 5 Innings":
        case "1st Half":
        case "1st Period":
          periodNumber = 1;
          break;

        case "1st Quarter":
        case "3rd Period":
        case "Quarters":
          periodNumber = 3;
          break;

        case "2nd Half":
        case "2nd Period":
        case "Last 4 Innings":
          periodNumber = 2;
          break;

        case "2nd Quarter":
          periodNumber = 4;
          break;

        case "3rd Quarter":
          periodNumber = 5;
          break;

        case "4th Quarter":
          periodNumber = 6;
          break;

        case "Game":
          periodNumber = 0;
          break;

      }

      return periodNumber;
    }

    private static string GetDayNameByNum(string p) {
      var dayOfWeek = "";

      string[] weekDays = { "Everyday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };

      for (var i = 0; i < weekDays.Count(); i++) {
        if (int.Parse(p) != i) continue;
        dayOfWeek = weekDays[i];
        break;
      }

      return dayOfWeek;
    }

    private static string GetPriceTypeName(string p) {
      var priceTypeName = "";
      switch (p) {
        case LineOffering.PRICE_AMERICAN:
          priceTypeName = "American";
          break;
        case LineOffering.PRICE_DECIMAL:
          priceTypeName = "Decimal";
          break;
        case LineOffering.PRICE_FRACTIONAL:
          priceTypeName = "Fractional";
          break;
      }

      return priceTypeName;
    }

    private static string GetTimeZoneName(string p) {
      var timeZoneName = "";

      switch (p) {
        case "1":
          timeZoneName = "Central";
          break;
        case "0":
          timeZoneName = "Eastern";
          break;
        case "2":
          timeZoneName = "Mountain";
          break;
        case "3":
          timeZoneName = "Pacific";
          break;
      }

      return timeZoneName;
    }

    public void Dispose() {
      _customerInfoFromDbList = null;
      _storeShadeGroupsFromDbList = null;
      CustomerAvailableTeasersFromDbList = null;
      CustomerAvailableBetSvcProfilesFromDbList = null;
      _customerCostToBuyPointsFromDbList = null;
      _customerVigDiscountOptions = null;
      _customerAsAgentInfoFromDbList = null;
      _customerWagerLimitsOptions = null;
      _customerEnteredTransaction = null;
    }

    ~CustomerInfoUpdater() {
      try {
        Dispose();
      }
      catch (Exception) {
        // ignored
      }
    }
  }
}