﻿using System;

namespace CustomerMaintenance.Libraries {
  class CustomerFreePlayTransaction {
    public readonly DateTime TranDateTime;
    public readonly int DocumentNumber;
    public readonly String Description;
    public readonly double Credit;
    public readonly double Debit;
    public readonly double Balance;
    public readonly String TranType;
    public readonly DateTime ValueDate;
    public readonly int GradeNumber;

    public CustomerFreePlayTransaction(DateTime tranDateTime, int documentNumber, String description, double credit, double debit, double balance, String tranCode, String tranType, DateTime valueDate, int gradeNumber) {
      TranDateTime = tranDateTime;
      DocumentNumber = documentNumber;
      Description = description;
      Credit = credit;
      Debit = debit;
      Balance = balance;
      TranType = tranType;
      ValueDate = valueDate;
      GradeNumber = gradeNumber;
    }
  }
}