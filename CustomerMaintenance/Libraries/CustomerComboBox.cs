﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using System.Collections;
using CustomerMaintenance.UI;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Utilities;

namespace CustomerMaintenance.Libraries {
  class CustomerComboBox : IDisposable {
    #region Private Vars

    private string _listBoxType = "states"; //Countries, PriceType, States, Currency, Agent, Inet, TimeZone
    private string _status = "edit"; //new, edit
    private Form _parentForm;
    private String _programName;
    private String _previousAgentId;
    private readonly ModuleInfo _moduleInfo;

    private ComboBox _cBx;

    #endregion

    #region Public Vars

    public Boolean PackageInhToCustomer;
    private String _customerId;

    private readonly List<String> _createdComboBoxes;

    #endregion

    #region Private Properties

    #endregion

    #region Public Properties

    public string ListBoxType {
      get {
        return _listBoxType;
      }
      set {
        _listBoxType = value;
      }
    }

    public string Status {
      get {
        return _status;
      }
      set {
        _status = value;
      }
    }

    #endregion

    #region Structures

    private class State {
      public readonly string Abbreviation;
      public readonly string Name;

      public State(string sAbbrev, string sName) {
        Abbreviation = sAbbrev;
        Name = sName;
      }
    }

    private class Country {
      public readonly string CountryName;

      public Country(string cName) {
        CountryName = cName;
      }
    }

    private class TimeZone {
      public string TzAbbreviation, TzName;
      public int TzCode;

      public TimeZone() {
      }
      public TimeZone(string tzAbbrev, string tzName, int tzCode) {
        TzAbbreviation = tzAbbrev;
        TzName = tzName;
        TzCode = tzCode;
      }
    }

    private class PriceType {
      public string PtAbbreviation, PtName;

      public PriceType() {
      }

      public PriceType(string ptAbbrev, string ptName) {
        PtAbbreviation = ptAbbrev;
        PtName = ptName;
      }
    }

    #endregion

    #region Constructors

    public CustomerComboBox(ModuleInfo moduleInfo) {
      _moduleInfo = moduleInfo;
      _createdComboBoxes = new List<string>();
    }

    ~CustomerComboBox() {
      try {
        Dispose();
      }
      catch (Exception ex) {
        SystemLog.Log(ex, _moduleInfo);
      }
    }

    #endregion

    #region Public Methods

    public void Dispose() {
      if (_cBx == null)
        return;
      _cBx.SelectedIndexChanged -= cBx_SelectedTranTypeIndexChanged;
      _cBx.SelectedIndexChanged -= cBx_SelectedIndexChanged;
      _cBx.SelectedIndexChanged -= cBx0_SelectedIndexChanged;
      _cBx.SelectedValueChanged -= cBx_SelectedValueChanged;
    }

    #endregion

    #region Private Methods

    private void ChangeTransactionsFormControlsDisplay(object sender) {
      using (var tran = new Transactions(_moduleInfo)) {
        var cBx = (ComboBox)sender;

        var panTranMethod = (Panel)_parentForm.Controls.Find("panTranMethod", true).FirstOrDefault();

        var methods = tran.GetTransactionMethodsByTranType(cBx.SelectedItem.ToString()).ToList();

        if (panTranMethod != null && panTranMethod.Controls.Count > 0) {
          panTranMethod.Controls.RemoveAt(0);
        }

        var newCbx = new ComboBox();

        foreach (var str in methods) {
          var method = str.Method;
          newCbx.Items.Add(method);
        }

        newCbx.DropDownStyle = ComboBoxStyle.DropDownList;

        if (panTranMethod != null) panTranMethod.Controls.Add(newCbx);

        var btnPayto = (Button)_parentForm.Controls.Find("btnPayto", true).FirstOrDefault();

        var transactionDescription = (TextBox)_parentForm.Controls.Find("txtTranDescription", true).FirstOrDefault();

        if (transactionDescription != null) {
          if (cBx.SelectedItem.ToString() == "Deposit" || cBx.SelectedItem.ToString() == "Withdrawal") {
            transactionDescription.Text = @"Customer " + cBx.SelectedItem + @" ";
          }
          else {
            transactionDescription.Text = cBx.SelectedItem + @" ";
          }
        }


        var txtFreePlay = (TextBox)_parentForm.Controls.Find("txtFreePlay", true).FirstOrDefault();
        var txtFees = (TextBox)_parentForm.Controls.Find("txtFees", true).FirstOrDefault();
        var txtPromo = (TextBox)_parentForm.Controls.Find("txtPromo", true).FirstOrDefault();
        var chbHold = (CheckBox)_parentForm.Controls.Find("chbHold", true).FirstOrDefault();

        var chbCasinoAdj = (CheckBox)_parentForm.Controls.Find("chbCasinoAdjustment", true).FirstOrDefault();

        var txtCardNo = (TextBox)_parentForm.Controls.Find("txtCardNo", true).FirstOrDefault();
        var txtCcExpDate = (TextBox)_parentForm.Controls.Find("txtCCExpDate", true).FirstOrDefault();
        var txtApprovalNo = (TextBox)_parentForm.Controls.Find("txtApprovalNo", true).FirstOrDefault();

        var btnCoverDeposit = (Button)_parentForm.Controls.Find("btnCoverDeposit", true).FirstOrDefault();

        var grpAdjustments = (GroupBox)_parentForm.Controls.Find("grpAdjustments", true).FirstOrDefault();

        var tranType = cBx.SelectedItem.ToString();

        switch (tranType) {
          case "Deposit":

            btnPayto.Visible = true;
            txtFreePlay.Enabled = true;
            txtFees.Enabled = true;
            txtPromo.Enabled = true;
            chbHold.Enabled = true;
            txtCardNo.Enabled = true;
            txtCcExpDate.Enabled = true;
            txtApprovalNo.Enabled = true;
            btnCoverDeposit.Enabled = false;
            grpAdjustments.Visible = false;
            break;

          case "Withdrawal":
            btnPayto.Visible = true;
            txtFreePlay.Enabled = false;
            txtFees.Enabled = false;
            txtPromo.Enabled = false;
            chbHold.Enabled = false;
            txtCardNo.Enabled = false;
            txtCcExpDate.Enabled = false;
            txtApprovalNo.Enabled = false;
            btnCoverDeposit.Enabled = true;
            grpAdjustments.Visible = false;
            break;

          case "Debit Adjustment":
            btnPayto.Visible = false;
            txtFreePlay.Enabled = false;
            txtFees.Enabled = false;
            txtPromo.Enabled = false;
            chbHold.Enabled = false;
            txtCardNo.Enabled = false;
            txtCcExpDate.Enabled = false;
            txtApprovalNo.Enabled = false;
            btnCoverDeposit.Enabled = true;
            grpAdjustments.Visible = true;
            chbCasinoAdj.Checked = false;
            break;

          case "Credit Adjustment":
            btnPayto.Visible = false;
            txtFreePlay.Enabled = false;
            txtFees.Enabled = false;
            txtPromo.Enabled = false;
            chbHold.Enabled = false;
            txtCardNo.Enabled = false;
            txtCcExpDate.Enabled = false;
            txtApprovalNo.Enabled = false;
            btnCoverDeposit.Enabled = false;
            grpAdjustments.Visible = true;
            chbCasinoAdj.Checked = false;
            break;

          default:
            btnPayto.Visible = false;
            txtFreePlay.Enabled = false;
            txtFees.Enabled = false;
            txtPromo.Enabled = false;
            chbHold.Enabled = false;
            txtCardNo.Enabled = false;
            txtCcExpDate.Enabled = false;
            txtApprovalNo.Enabled = false;
            btnCoverDeposit.Enabled = false;
            grpAdjustments.Visible = false;
            break;
        }
      }
    }

    private void HandleKeyPressing(KeyPressEventArgs e) {
      var str = e.KeyChar.ToString(CultureInfo.InvariantCulture).ToUpper();
      var ch = str.ToCharArray();
      e.KeyChar = ch[0];
    }

    private void LoadShadeGroupsInformation(object sender) {
      var newform = (FrmCustomerMaintenance)_parentForm;

      if (_parentForm == null) return;
      var shadeGroupsPanel = (Panel)newform.Controls.Find("panShadeGroups", true).FirstOrDefault();
      var cBx = (ComboBox)sender;

      var customerId = newform.CustomerId;

      newform.UpdateStoreShadeGroups(customerId, cBx.SelectedItem.ToString());
      using (var cust = new Customers(_moduleInfo)) {
        var custShadeGroups = cust.GetCustomerShadeGroups(customerId).ToList();
        var storeShadeGroups = cust.GetStoreShadeGroups(cBx.SelectedItem.ToString()).ToList();

        if (shadeGroupsPanel != null && shadeGroupsPanel.Controls.Count > 0) {
          shadeGroupsPanel.Controls.RemoveAt(0);
        }
        if (shadeGroupsPanel != null)
          shadeGroupsPanel.Controls.Add(newform.PopulateCustomerShadeGroupsObj(storeShadeGroups, custShadeGroups));
      }
    }

    private void PerformPackageIhneritanceLogic(object sender) {
      var cBx = (ComboBox)sender;
      var newAgent = "";
      if (cBx.Text.Length > 0)
        newAgent = cBx.Text.Trim().ToUpper();

      var agentExists = false;

      foreach (var item in cBx.Items) {
        var itemValue = item.ToString();
        if (itemValue.Trim().ToUpper() != newAgent) continue;
        agentExists = true;
        break;
      }

      if (!agentExists) {
        MessageBox.Show(@"Agent does not exist. Going back to " + _previousAgentId);
        cBx.Text = _previousAgentId;
        cBx.Focus();
      }
      else {
        var itemsToCuLog = new List<LogWriter.CuAccessLogInfo>();

        using (var cust = new Customers(_moduleInfo)) {
          if (((ComboBox)sender).Text != "") {
            if (MessageBox.Show(@"Should this customer inherit the selected agent's package?", @"Send package", MessageBoxButtons.OKCancel) == DialogResult.OK) {
              cust.UpdateCustomerAgent(_customerId.Trim(), newAgent, Environment.UserName);
              cust.InheritAgentsPackageToCustomer(_customerId.Trim(), newAgent, Environment.UserName);

              var logInfo = new LogWriter.CuAccessLogInfo {
                ProgramName = _programName,
                Operation = "Changed " + _customerId,
                Data = "Agent - " + newAgent
              };
              itemsToCuLog.Add(logInfo);

              logInfo.ProgramName = _programName;
              logInfo.Operation = "Changed " + _customerId;
              logInfo.Data = "Customer inherited the selected agent's package - " + newAgent;
              itemsToCuLog.Add(logInfo);
              PackageInhToCustomer = true;
              ((FrmCustomerMaintenance)_parentForm).ResetCustomer();
            }
            else {
              PackageInhToCustomer = false;
            }
          }
        }

        using (var lw = new LogWriter(_moduleInfo)) {
          lw.WriteToCuAccessLog(itemsToCuLog);
        }
        cBx.Text = newAgent;
      }
    }

    #endregion

    #region Protected Methods

    #endregion

    #region Controls

    public ComboBox States(string editModeArg) {
      var cBx = new ComboBox();

      var statesList = new ArrayList();
      var state = new State("AK", "Alaska");
      statesList.Add(state);
      state = new State("AL", "Alabama");
      statesList.Add(state);
      state = new State("AR", "Arkansas");
      statesList.Add(state);
      state = new State("AZ", "Arizona");
      statesList.Add(state);
      state = new State("CA", "California");
      statesList.Add(state);
      state = new State("CO", "Colorado");
      statesList.Add(state);
      state = new State("CT", "Connecticut");
      statesList.Add(state);
      state = new State("DE", "Delaware");
      statesList.Add(state);
      state = new State("FL", "Florida");
      statesList.Add(state);
      state = new State("GA", "Georgia");
      statesList.Add(state);
      state = new State("HI", "Hawaii");
      statesList.Add(state);
      state = new State("IA", "Iowa");
      statesList.Add(state);
      state = new State("ID", "Idaho");
      statesList.Add(state);
      state = new State("IL", "Illinois");
      statesList.Add(state);
      state = new State("IN", "Indiana");
      statesList.Add(state);
      state = new State("KS", "Kansas");
      statesList.Add(state);
      state = new State("KY", "Kentucky");
      statesList.Add(state);
      state = new State("LA", "Louisiana");
      statesList.Add(state);
      state = new State("MA", "Massachusetts");
      statesList.Add(state);
      state = new State("MD", "Maryland");
      statesList.Add(state);
      state = new State("ME", "Maine");
      statesList.Add(state);
      state = new State("MI", "Michigan");
      statesList.Add(state);
      state = new State("MN", "Minnesota");
      statesList.Add(state);
      state = new State("MO", "Missouri");
      statesList.Add(state);
      state = new State("MS", "Mississippi");
      statesList.Add(state);
      state = new State("MT", "Montana");
      statesList.Add(state);
      state = new State("NC", "North Carolina");
      statesList.Add(state);
      state = new State("ND", "North Dakota");
      statesList.Add(state);
      state = new State("NE", "Nebraska");
      statesList.Add(state);
      state = new State("NH", "New Hampshire");
      statesList.Add(state);
      state = new State("NJ", "New Jersey");
      statesList.Add(state);
      state = new State("NM", "New Mexico");
      statesList.Add(state);
      state = new State("NV", "Nevada");
      statesList.Add(state);
      state = new State("NY", "New York");
      statesList.Add(state);
      state = new State("OH", "Ohio");
      statesList.Add(state);
      state = new State("OK", "Oklahoma");
      statesList.Add(state);
      state = new State("OR", "Oregon");
      statesList.Add(state);
      state = new State("PA", "Pennsylvania");
      statesList.Add(state);
      state = new State("RI", "Rhode Island");
      statesList.Add(state);
      state = new State("SC", "South Carolina");
      statesList.Add(state);
      state = new State("SD", "South Dakota");
      statesList.Add(state);
      state = new State("TX", "Texas");
      statesList.Add(state);
      state = new State("UT", "Utah");
      statesList.Add(state);
      state = new State("VA", "Virginia");
      statesList.Add(state);
      state = new State("VT", "Vermont");
      statesList.Add(state);
      state = new State("WA", "Washington");
      statesList.Add(state);
      state = new State("WI", "Wisconsin");
      statesList.Add(state);
      state = new State("WV", "West Virginia");
      statesList.Add(state);
      state = new State("WY", "Wyoming");
      statesList.Add(state);
      state = new State("YK", "Yukon");
      statesList.Add(state);

      cBx.Items.Add("");

      foreach (State st in statesList) {
        var sAbb = st.Abbreviation;
        var sNam = st.Name;
        cBx.Items.Add(sAbb + " " + sNam);
        if (sAbb == editModeArg) {
          cBx.SelectedItem = sAbb + " " + sNam;
        }
      }

      cBx.DropDownStyle = ComboBoxStyle.DropDownList;
      cBx.Name = "cBxCCbStates";
      _createdComboBoxes.Add(cBx.Name);
      return cBx;
    }

    public ComboBox Agents(string editModeArg, String custId, String appName, FrmCustomerMaintenance callerForm) {
      _cBx = new ComboBox();
      using (var cust = new Customers(_moduleInfo)) {
        var li = cust.GetAgentsList().ToList();
        _parentForm = callerForm;
        _programName = appName;
        _customerId = custId;
        _previousAgentId = editModeArg;

        var sAgnt = "";
        _cBx.Items.Add(sAgnt);

        foreach (var str in li) {
          sAgnt = str.AgentID;
          _cBx.Items.Add(sAgnt);
          if (sAgnt != null && sAgnt.Trim() == editModeArg) {
            _cBx.SelectedItem = sAgnt;
          }
        }

        _cBx.SelectedValueChanged += cBx_SelectedValueChanged;
        //cBx.DropDownClosed += cBx_DropDownClosed;
        _cBx.KeyPress += cBx_KeyPress;
        _cBx.DropDownStyle = ComboBoxStyle.DropDown;
        _cBx.Name = "cBxCCbAgents";
        _createdComboBoxes.Add(_cBx.Name);
      }
      return _cBx;
    }

    public ComboBox MasterAgents(string editModeArg, FrmCustomerMaintenance callerForm) {
      _cBx = new ComboBox();
      using (var cust = new Customers(_moduleInfo)) {
        var li = cust.GetMasterAgentsList(callerForm.CustomerId).ToList();

        foreach (var str in li) {
          var sAgnt = str.CustomerID;
          _cBx.Items.Add(sAgnt);
          if (sAgnt == editModeArg) {
            _cBx.SelectedItem = sAgnt;
          }
        }

        _cBx.DropDownStyle = ComboBoxStyle.DropDownList;
        _cBx.SelectedIndexChanged += cBx0_SelectedIndexChanged;
        _cBx.Name = "cBxCCbMAgents";
        _createdComboBoxes.Add(_cBx.Name);
      }
      return _cBx;
    }

    public ComboBox Currencies(string editModeArg, int transCount) {
      _cBx = new ComboBox { Width = 170 };
      using (var cu = new Currencies(_moduleInfo)) {
        var li = cu.GetCurrencyCodes();

        foreach (var cCod in li) {
          var cCode = cCod.CurrencyCode;
          var cName = cCod.CurrencyName;

          _cBx.Items.Add(cCode + " " + cName);
          if (cCode + " " + cName == editModeArg) {
            _cBx.SelectedItem = cCode + " " + cName;
          }
        }

        _cBx.Enabled = transCount <= 0;

        _cBx.DropDownStyle = ComboBoxStyle.DropDownList;
        _cBx.Name = "cBxCCbCurrencies";
        _createdComboBoxes.Add(_cBx.Name);
      }
      return _cBx;
    }

    public ComboBox Countries(string editModeArg) {
      var cBx = new ComboBox();
      //cBx.Width = 170;

      var countriesList = new ArrayList();
      var country = new Country("Afghanistan");
      countriesList.Add(country);
      country = new Country("Albania");
      countriesList.Add(country);
      country = new Country("Algeria");
      countriesList.Add(country);
      country = new Country("American Samoa");
      countriesList.Add(country);
      country = new Country("Andorra");
      countriesList.Add(country);
      country = new Country("Angola");
      countriesList.Add(country);
      country = new Country("Anguilla");
      countriesList.Add(country);
      country = new Country("Antarctica");
      countriesList.Add(country);
      country = new Country("Antigua and Barbuda");
      countriesList.Add(country);
      country = new Country("Argentina");
      countriesList.Add(country);
      country = new Country("Armenia");
      countriesList.Add(country);
      country = new Country("Aruba");
      countriesList.Add(country);
      country = new Country("Australia");
      countriesList.Add(country);
      country = new Country("Austria");
      countriesList.Add(country);
      country = new Country("Azerbaijan");
      countriesList.Add(country);
      country = new Country("Bahamas");
      countriesList.Add(country);
      country = new Country("Bahrain");
      countriesList.Add(country);
      country = new Country("Bangladesh");
      countriesList.Add(country);
      country = new Country("Barbados");
      countriesList.Add(country);
      country = new Country("Belarus");
      countriesList.Add(country);
      country = new Country("Belgium");
      countriesList.Add(country);
      country = new Country("Belize");
      countriesList.Add(country);
      country = new Country("Benin");
      countriesList.Add(country);
      country = new Country("Bermuda");
      countriesList.Add(country);
      country = new Country("Bhutan");
      countriesList.Add(country);
      country = new Country("Bolivia");
      countriesList.Add(country);
      country = new Country("Bosnia and Herzegovina");
      countriesList.Add(country);
      country = new Country("Botswana");
      countriesList.Add(country);
      country = new Country("Brazil");
      countriesList.Add(country);
      country = new Country("British Indian Ocean Territory");
      countriesList.Add(country);
      country = new Country("British Virgin Islands");
      countriesList.Add(country);
      country = new Country("Brunei");
      countriesList.Add(country);
      country = new Country("Bulgaria");
      countriesList.Add(country);
      country = new Country("Burkina Faso");
      countriesList.Add(country);
      country = new Country("Burma (Myanmar)");
      countriesList.Add(country);
      country = new Country("Burundi");
      countriesList.Add(country);
      country = new Country("Cambodia");
      countriesList.Add(country);
      country = new Country("Cameroon");
      countriesList.Add(country);
      country = new Country("Canada");
      countriesList.Add(country);
      country = new Country("Cape Verde");
      countriesList.Add(country);
      country = new Country("Cayman Islands");
      countriesList.Add(country);
      country = new Country("Central African Republic");
      countriesList.Add(country);
      country = new Country("Chad");
      countriesList.Add(country);
      country = new Country("Chile");
      countriesList.Add(country);
      country = new Country("China");
      countriesList.Add(country);
      country = new Country("Christmas Island");
      countriesList.Add(country);
      country = new Country("Cocos (Keeling) Islands");
      countriesList.Add(country);
      country = new Country("Colombia");
      countriesList.Add(country);
      country = new Country("Comoros");
      countriesList.Add(country);
      country = new Country("Cook Islands");
      countriesList.Add(country);
      country = new Country("Costa Rica");
      countriesList.Add(country);
      country = new Country("Croatia");
      countriesList.Add(country);
      country = new Country("Cuba");
      countriesList.Add(country);
      country = new Country("Cyprus");
      countriesList.Add(country);
      country = new Country("Czech Republic");
      countriesList.Add(country);
      country = new Country("Democratic Republic of the Congo");
      countriesList.Add(country);
      country = new Country("Denmark");
      countriesList.Add(country);
      country = new Country("Djibouti");
      countriesList.Add(country);
      country = new Country("Dominica");
      countriesList.Add(country);
      country = new Country("Dominican Republic");
      countriesList.Add(country);
      country = new Country("Ecuador");
      countriesList.Add(country);
      country = new Country("Egypt");
      countriesList.Add(country);
      country = new Country("El Salvador");
      countriesList.Add(country);
      country = new Country("Equatorial Guinea");
      countriesList.Add(country);
      country = new Country("Eritrea");
      countriesList.Add(country);
      country = new Country("Estonia");
      countriesList.Add(country);
      country = new Country("Ethiopia");
      countriesList.Add(country);
      country = new Country("Falkland Islands");
      countriesList.Add(country);
      country = new Country("Faroe Islands");
      countriesList.Add(country);
      country = new Country("Fiji");
      countriesList.Add(country);
      country = new Country("Finland");
      countriesList.Add(country);
      country = new Country("France");
      countriesList.Add(country);
      country = new Country("French Polynesia");
      countriesList.Add(country);
      country = new Country("Gabon");
      countriesList.Add(country);
      country = new Country("Gambia");
      countriesList.Add(country);
      country = new Country("Gaza Strip");
      countriesList.Add(country);
      country = new Country("Georgia");
      countriesList.Add(country);
      country = new Country("Germany");
      countriesList.Add(country);
      country = new Country("Ghana");
      countriesList.Add(country);
      country = new Country("Gibraltar");
      countriesList.Add(country);
      country = new Country("Greece");
      countriesList.Add(country);
      country = new Country("Greenland");
      countriesList.Add(country);
      country = new Country("Grenada");
      countriesList.Add(country);
      country = new Country("Guam");
      countriesList.Add(country);
      country = new Country("Guatemala");
      countriesList.Add(country);
      country = new Country("Guinea");
      countriesList.Add(country);
      country = new Country("Guinea-Bissau");
      countriesList.Add(country);
      country = new Country("Guyana");
      countriesList.Add(country);
      country = new Country("Haiti");
      countriesList.Add(country);
      country = new Country("Holy See (Vatican City)");
      countriesList.Add(country);
      country = new Country("Honduras");
      countriesList.Add(country);
      country = new Country("Hong Kong");
      countriesList.Add(country);
      country = new Country("Hungary");
      countriesList.Add(country);
      country = new Country("Iceland");
      countriesList.Add(country);
      country = new Country("India");
      countriesList.Add(country);
      country = new Country("Indonesia");
      countriesList.Add(country);
      country = new Country("Iran");
      countriesList.Add(country);
      country = new Country("Iraq");
      countriesList.Add(country);
      country = new Country("Ireland");
      countriesList.Add(country);
      country = new Country("Isle of Man");
      countriesList.Add(country);
      country = new Country("Israel");
      countriesList.Add(country);
      country = new Country("Italy");
      countriesList.Add(country);
      country = new Country("Ivory Coast");
      countriesList.Add(country);
      country = new Country("Jamaica");
      countriesList.Add(country);
      country = new Country("Japan");
      countriesList.Add(country);
      country = new Country("Jersey");
      countriesList.Add(country);
      country = new Country("Jordan");
      countriesList.Add(country);
      country = new Country("Kazakhstan");
      countriesList.Add(country);
      country = new Country("Kenya");
      countriesList.Add(country);
      country = new Country("Kiribati");
      countriesList.Add(country);
      country = new Country("Kosovo");
      countriesList.Add(country);
      country = new Country("Kuwait");
      countriesList.Add(country);
      country = new Country("Kyrgyzstan");
      countriesList.Add(country);
      country = new Country("Laos");
      countriesList.Add(country);
      country = new Country("Latvia");
      countriesList.Add(country);
      country = new Country("Lebanon");
      countriesList.Add(country);
      country = new Country("Lesotho");
      countriesList.Add(country);
      country = new Country("Liberia");
      countriesList.Add(country);
      country = new Country("Libya");
      countriesList.Add(country);
      country = new Country("Liechtenstein");
      countriesList.Add(country);
      country = new Country("Lithuania");
      countriesList.Add(country);
      country = new Country("Luxembourg");
      countriesList.Add(country);
      country = new Country("Macau");
      countriesList.Add(country);
      country = new Country("Macedonia");
      countriesList.Add(country);
      country = new Country("Madagascar");
      countriesList.Add(country);
      country = new Country("Malawi");
      countriesList.Add(country);
      country = new Country("Malaysia");
      countriesList.Add(country);
      country = new Country("Maldives");
      countriesList.Add(country);
      country = new Country("Mali");
      countriesList.Add(country);
      country = new Country("Malta");
      countriesList.Add(country);
      country = new Country("Marshall Islands");
      countriesList.Add(country);
      country = new Country("Mauritania");
      countriesList.Add(country);
      country = new Country("Mauritius");
      countriesList.Add(country);
      country = new Country("Mayotte");
      countriesList.Add(country);
      country = new Country("Mexico");
      countriesList.Add(country);
      country = new Country("Micronesia");
      countriesList.Add(country);
      country = new Country("Moldova");
      countriesList.Add(country);
      country = new Country("Monaco");
      countriesList.Add(country);
      country = new Country("Mongolia");
      countriesList.Add(country);
      country = new Country("Montenegro");
      countriesList.Add(country);
      country = new Country("Montserrat");
      countriesList.Add(country);
      country = new Country("Morocco");
      countriesList.Add(country);
      country = new Country("Mozambique");
      countriesList.Add(country);
      country = new Country("Namibia");
      countriesList.Add(country);
      country = new Country("Nauru");
      countriesList.Add(country);
      country = new Country("Nepal");
      countriesList.Add(country);
      country = new Country("Netherlands");
      countriesList.Add(country);
      country = new Country("Netherlands Antilles");
      countriesList.Add(country);
      country = new Country("New Caledonia");
      countriesList.Add(country);
      country = new Country("New Zealand");
      countriesList.Add(country);
      country = new Country("Nicaragua");
      countriesList.Add(country);
      country = new Country("Niger");
      countriesList.Add(country);
      country = new Country("Nigeria");
      countriesList.Add(country);
      country = new Country("Niue");
      countriesList.Add(country);
      country = new Country("Norfolk Island");
      countriesList.Add(country);
      country = new Country("North Korea");
      countriesList.Add(country);
      country = new Country("Northern Mariana Islands");
      countriesList.Add(country);
      country = new Country("Norway");
      countriesList.Add(country);
      country = new Country("Oman");
      countriesList.Add(country);
      country = new Country("Pakistan");
      countriesList.Add(country);
      country = new Country("Palau");
      countriesList.Add(country);
      country = new Country("Panama");
      countriesList.Add(country);
      country = new Country("Papua New Guinea");
      countriesList.Add(country);
      country = new Country("Paraguay");
      countriesList.Add(country);
      country = new Country("Peru");
      countriesList.Add(country);
      country = new Country("Philippines");
      countriesList.Add(country);
      country = new Country("Pitcairn Islands");
      countriesList.Add(country);
      country = new Country("Poland");
      countriesList.Add(country);
      country = new Country("Portugal");
      countriesList.Add(country);
      country = new Country("Puerto Rico");
      countriesList.Add(country);
      country = new Country("Qatar");
      countriesList.Add(country);
      country = new Country("Republic of the Congo");
      countriesList.Add(country);
      country = new Country("Romania");
      countriesList.Add(country);
      country = new Country("Russia");
      countriesList.Add(country);
      country = new Country("Rwanda");
      countriesList.Add(country);
      country = new Country("Saint Barthelemy");
      countriesList.Add(country);
      country = new Country("Saint Helena");
      countriesList.Add(country);
      country = new Country("Saint Kitts and Nevis");
      countriesList.Add(country);
      country = new Country("Saint Lucia");
      countriesList.Add(country);
      country = new Country("Saint Martin");
      countriesList.Add(country);
      country = new Country("Saint Pierre and Miquelon");
      countriesList.Add(country);
      country = new Country("Saint Vincent and the Grenadines");
      countriesList.Add(country);
      country = new Country("Samoa");
      countriesList.Add(country);
      country = new Country("San Marino");
      countriesList.Add(country);
      country = new Country("Sao Tome and Principe");
      countriesList.Add(country);
      country = new Country("Saudi Arabia");
      countriesList.Add(country);
      country = new Country("Senegal");
      countriesList.Add(country);
      country = new Country("Serbia");
      countriesList.Add(country);
      country = new Country("Seychelles");
      countriesList.Add(country);
      country = new Country("Sierra Leone");
      countriesList.Add(country);
      country = new Country("Singapore");
      countriesList.Add(country);
      country = new Country("Slovakia");
      countriesList.Add(country);
      country = new Country("Slovenia");
      countriesList.Add(country);
      country = new Country("Solomon Islands");
      countriesList.Add(country);
      country = new Country("Somalia");
      countriesList.Add(country);
      country = new Country("South Africa");
      countriesList.Add(country);
      country = new Country("South Korea");
      countriesList.Add(country);
      country = new Country("Spain");
      countriesList.Add(country);
      country = new Country("Sri Lanka");
      countriesList.Add(country);
      country = new Country("Sudan");
      countriesList.Add(country);
      country = new Country("Suriname");
      countriesList.Add(country);
      country = new Country("Svalbard");
      countriesList.Add(country);
      country = new Country("Swaziland");
      countriesList.Add(country);
      country = new Country("Sweden");
      countriesList.Add(country);
      country = new Country("Switzerland");
      countriesList.Add(country);
      country = new Country("Syria");
      countriesList.Add(country);
      country = new Country("Taiwan");
      countriesList.Add(country);
      country = new Country("Tajikistan");
      countriesList.Add(country);
      country = new Country("Tanzania");
      countriesList.Add(country);
      country = new Country("Thailand");
      countriesList.Add(country);
      country = new Country("Timor-Leste");
      countriesList.Add(country);
      country = new Country("Togo");
      countriesList.Add(country);
      country = new Country("Tokelau");
      countriesList.Add(country);
      country = new Country("Tonga");
      countriesList.Add(country);
      country = new Country("Trinidad and Tobago");
      countriesList.Add(country);
      country = new Country("Tunisia");
      countriesList.Add(country);
      country = new Country("Turkey");
      countriesList.Add(country);
      country = new Country("Turkmenistan");
      countriesList.Add(country);
      country = new Country("Turks and Caicos Islands");
      countriesList.Add(country);
      country = new Country("Tuvalu");
      countriesList.Add(country);
      country = new Country("Uganda");
      countriesList.Add(country);
      country = new Country("Ukraine");
      countriesList.Add(country);
      country = new Country("United Arab Emirates");
      countriesList.Add(country);
      country = new Country("United Kingdom");
      countriesList.Add(country);
      country = new Country("United States");
      countriesList.Add(country);
      country = new Country("Uruguay");
      countriesList.Add(country);
      country = new Country("US Virgin Islands");
      countriesList.Add(country);
      country = new Country("Uzbekistan");
      countriesList.Add(country);
      country = new Country("Vanuatu");
      countriesList.Add(country);
      country = new Country("Venezuela");
      countriesList.Add(country);
      country = new Country("Vietnam");
      countriesList.Add(country);
      country = new Country("Wallis and Futuna");
      countriesList.Add(country);
      country = new Country("West Bank");
      countriesList.Add(country);
      country = new Country("Western Sahara");
      countriesList.Add(country);
      country = new Country("Yemen");
      countriesList.Add(country);
      country = new Country("Zambia");
      countriesList.Add(country);
      country = new Country("Zimbabwe");
      countriesList.Add(country);

      cBx.Items.Add("");

      foreach (Country st in countriesList) {
        var cNam = st.CountryName;
        cBx.Items.Add(cNam);
        if (cNam == editModeArg) {
          cBx.SelectedItem = cNam;
        }
      }

      cBx.DropDownStyle = ComboBoxStyle.DropDownList;
      cBx.Name = "cBxCCbCountries";
      _createdComboBoxes.Add(cBx.Name);
      return cBx;
    }

    public ComboBox InetTargets(string editModeArg) {
      _cBx = new ComboBox();
      using (var cust = new Customers(_moduleInfo)) {
        var li = cust.GetInetTargets();

        foreach (var str in li) {
          var sInet = str.Store;
          _cBx.Items.Add(sInet);
          if (sInet == editModeArg) {
            _cBx.SelectedItem = sInet;
          }
        }

        _cBx.DropDownStyle = ComboBoxStyle.DropDownList;
        _cBx.Name = "cBxCCbInetTargets";
        _createdComboBoxes.Add(_cBx.Name);
      }
      return _cBx;
    }

    public ComboBox TimeZones(int editModeArg) {
      _cBx = new ComboBox();

      var tZonesList = new ArrayList();
      var tz = new TimeZone { TzAbbreviation = "CST", TzName = "Central", TzCode = 1 };

      tZonesList.Add(tz);

      tz = new TimeZone { TzAbbreviation = "EST", TzName = "Eastern", TzCode = 0 };
      tZonesList.Add(tz);

      tz = new TimeZone { TzAbbreviation = "MST", TzName = "Mountain", TzCode = 2 };
      tZonesList.Add(tz);

      tz = new TimeZone { TzAbbreviation = "PST", TzName = "Pacific", TzCode = 3 };
      tZonesList.Add(tz);

      foreach (TimeZone tZone in tZonesList) {
        var sAbb = tZone.TzAbbreviation;
        var sNam = tZone.TzName;
        var iCode = tZone.TzCode;

        _cBx.Items.Add(sAbb + " - " + sNam);
        if (iCode == editModeArg) {
          _cBx.SelectedItem = sAbb + " - " + sNam;
        }
      }

      _cBx.DropDownStyle = ComboBoxStyle.DropDownList;
      _cBx.Name = "cBxCCbTimeZones";
      _createdComboBoxes.Add(_cBx.Name);
      return _cBx;
    }

    public ComboBox PriceTypes(string editModeArg) {
      _cBx = new ComboBox();

      var pTypesList = new ArrayList();
      var pt = new PriceType { PtAbbreviation = LineOffering.PRICE_AMERICAN, PtName = "American" };

      pTypesList.Add(pt);

      pt = new PriceType { PtAbbreviation = LineOffering.PRICE_DECIMAL, PtName = "Decimal" };

      pTypesList.Add(pt);

      pt = new PriceType { PtAbbreviation = LineOffering.PRICE_FRACTIONAL, PtName = "Fractional" };

      pTypesList.Add(pt);

      foreach (PriceType pType in pTypesList) {
        var sAbb = pType.PtAbbreviation;
        var sNam = pType.PtName;

        _cBx.Items.Add(sNam);
        if (sAbb == editModeArg) {
          _cBx.SelectedItem = sNam;
        }
      }

      _cBx.DropDownStyle = ComboBoxStyle.DropDownList;
      _cBx.Name = "cBxCCbPriceTypes";
      _createdComboBoxes.Add(_cBx.Name);
      return _cBx;
    }

    public ComboBox Stores(string editModeArg, Form containingForm) {
      _cBx = new ComboBox();
      using (var cu = new Customers(_moduleInfo)) {
        var li = cu.SpGetBookStores();

        _parentForm = containingForm;

        var distinctStores = (from p in li select p.Store).Distinct().ToList();

        foreach (var str in distinctStores) {
          if (str != null && str.Trim().ToUpper() != "MASTER") {
            var sStore = str.Trim();
            _cBx.Items.Add(sStore);
            if (sStore == editModeArg) {
              _cBx.SelectedItem = sStore;
            }
          }
        }

        _cBx.SelectedIndexChanged += cBx_SelectedIndexChanged;
        _cBx.DropDownStyle = ComboBoxStyle.DropDownList;
        _cBx.Name = "cBxCCbStores";
        _createdComboBoxes.Add(_cBx.Name);
      }
      return _cBx;
    }

    public ComboBox ParlayTypes(string editModeArg) {
      _cBx = new ComboBox();
      using (var tAw = new TicketsAndWagers(_moduleInfo)) {
        var li = tAw.GetParlaysSpecs();

        foreach (var parlayType in li) {
          var pName = parlayType.ParlayName;

          _cBx.Items.Add(pName);
          if (pName == editModeArg.Trim()) {
            _cBx.SelectedItem = pName;
          }
        }

        _cBx.DropDownStyle = ComboBoxStyle.DropDownList;
        _cBx.Name = "cBxCCbParlayTypes";
        _createdComboBoxes.Add(_cBx.Name);
      }

      return _cBx;

    }

    public ComboBox WeekDays(int editModeArg, bool evalSelectedItem) {
      _cBx = new ComboBox();
      var weekDays = new[] { "Everyday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };


      for (var i = 0; i < weekDays.Count(); i++) {
        _cBx.Items.Add(weekDays[i]);

        if (i == editModeArg && evalSelectedItem) {
          _cBx.SelectedItem = weekDays[i];
        }
      }

      _cBx.DropDownStyle = ComboBoxStyle.DropDownList;
      _cBx.Name = "cBxCCbWeekDays";
      _createdComboBoxes.Add(_cBx.Name);
      return _cBx;
    }

    public ComboBox BuyPointsChartNames(string editModeArg) {
      _cBx = new ComboBox();
      using (var bPoints = new BuyPoints(_moduleInfo)) {
        var li = bPoints.GetProgressiveChartNames().ToList();

        foreach (var str in li) {
          var sChartName = str.ChartName;
          _cBx.Items.Add(sChartName);
          if (sChartName == editModeArg) {
            _cBx.SelectedItem = sChartName;
          }
        }

        _cBx.DropDownStyle = ComboBoxStyle.DropDownList;
        _cBx.Name = "cBxCCbBuyPointChartNames";
        _createdComboBoxes.Add(_cBx.Name);
      }
      return _cBx;
    }

    public ComboBox TransactionTypes(FrmCustomerTransactionDetails callerForm) {
      _cBx = new ComboBox();
      using (var tran = new Transactions(_moduleInfo)) {
        var li = tran.GetTransactionTypes().ToList();

        _parentForm = callerForm;

        foreach (var str in li) {
          var tranTypeDesc = str.TranTypeDesc;
          _cBx.Items.Add(tranTypeDesc);
        }

        _cBx.SelectedIndexChanged += cBx_SelectedTranTypeIndexChanged;
        _cBx.DropDownStyle = ComboBoxStyle.DropDownList;
        _cBx.Name = "cBxCCbTransactionTypes";
        _createdComboBoxes.Add(_cBx.Name);
      }
      return _cBx;
    }

    public ComboBox SportTypes() {
      _cBx = new ComboBox();
      using (var sportTypes = new SportTypes(_moduleInfo)) {
        var li = sportTypes.GetSportAndSubSportTypes().ToList();

        foreach (var str in li) {
          var sportTypeDesc = str.SportType + " - " + str.SportSubType;
          _cBx.Items.Add(sportTypeDesc);
        }

        _cBx.DropDownStyle = ComboBoxStyle.DropDownList;
        _cBx.Width = 200;
        _cBx.Name = "cBxCCbSportTypes";
        _createdComboBoxes.Add(_cBx.Name);
      }
      return _cBx;
    }

    public ComboBox CustomerRestrictions(string customerId) {
      _cBx = new ComboBox();
      using (var cst = new Customers(_moduleInfo)) {
        var li = cst.GetActions(customerId);

        foreach (var str in li) {
          if (str.CustomerType != "C" || str.SystemRestricted)
            continue;
          _cBx.Items.Add(str);
        }

        _cBx.DropDownStyle = ComboBoxStyle.DropDownList;
        _cBx.Name = "cBxCCbRestrictions";
        _cBx.ValueMember = "ActionId";
        _cBx.DisplayMember = "Name";
        _createdComboBoxes.Add(_cBx.Name);
      }
      return _cBx;
    }

    #endregion

    #region Events

    private void cBx_DropDownClosed(object sender, EventArgs e) {
      PerformPackageIhneritanceLogic(sender);
    }

    private void cBx_SelectedValueChanged(object sender, EventArgs e) {
      PerformPackageIhneritanceLogic(sender);
    }

    private void cBx_KeyPress(object sender, KeyPressEventArgs e) {
      HandleKeyPressing(e);
    }

    private void cBx0_SelectedIndexChanged(object sender, EventArgs e) {
      var frm = _parentForm;

      var chB = (CheckBox)frm.Controls.Find("chbDistributeFunds", true).FirstOrDefault();

      if (chB != null) chB.Checked = false;
    }

    private void cBx_SelectedIndexChanged(object sender, EventArgs e) {
      LoadShadeGroupsInformation(sender);
    }

    private void cBx_SelectedTranTypeIndexChanged(object sender, EventArgs e) {
      ChangeTransactionsFormControlsDisplay(sender);
    }

    #endregion
  }
}
