﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using SIDLibraries.UI;
using SIDLibraries.Utilities;

namespace CustomerMaintenance.Extensions.SavePackageToCsv {
  public class SavePackageToCsv : ExtensionCore {

    public readonly List<string> Erros;

    public SavePackageToCsv(ModuleInfo moduleInfo)
      : base(moduleInfo, "SavePackageToCsv") {
      Erros = new List<string>();
    }

    public void SaveFile(string fileName, string agentId, DateTime serverDateTime) {
      try {
        List<spMngGetAgentPackageData_Result> package;
        if (File.Exists(fileName)) {
          package = CsvDeserialize(fileName);
          var dateTime = GetFileDateTime(fileName);
          UpdateCsvPackage(package, GetDbPackage(agentId, dateTime));
          BackUpCsvFile(fileName);
        }
        else {
          package = GetDbPackage(agentId, null);
        }
        var data = CsvConverter.Serialize(package);
        File.WriteAllText(NewFileName(fileName, serverDateTime), data);
      }
      catch (Exception ex) {
        Erros.Add(ex.Message);
      }
    }

    private void BackUpCsvFile(string fileName) {
      try {
        var fi = new FileInfo(fileName);
        var backupPath = fi.Directory + @"\Backups\";
        if (!Directory.Exists(backupPath))
          Directory.CreateDirectory(backupPath);
        File.Move(fileName, backupPath + fi.Name.Replace(".csv", " BK.csv"));
      }
      catch (Exception ex) {
        Erros.Add(ex.Message);
      }
    }

    private List<spMngGetAgentPackageData_Result> GetDbPackage(string agentId, DateTime? lastDateTime) {
      try {
        using (var mng = new Management(ModuleInfo)) {
          var package = mng.GetAgentPackageData(agentId, lastDateTime);

          if (package == null || !package.Any()) return null;

          foreach (var c in package) {
            if (c.Address != null) c.Address = c.Address.Replace("\n", " ").Replace("\r", "").Trim();
            if (c.BusinessPhone != null) c.BusinessPhone = c.BusinessPhone.Replace("\n", " ").Replace("\r", "").Trim();
            if (c.City != null) c.City = c.City.Replace("\n", " ").Replace("\r", "").Trim();
            if (c.EMail != null) c.EMail = c.EMail.Replace("\n", " ").Replace("\r", "").Trim();
            if (c.Fax != null) c.Fax = c.Fax.Replace("\n", " ").Replace("\r", "").Trim();
            if (c.HomePhone != null) c.HomePhone = c.HomePhone.Replace("\n", " ").Replace("\r", "").Trim();
            if (c.NameFirst != null) c.NameFirst = c.NameFirst.Replace("\n", " ").Replace("\r", "").Trim();
            if (c.NameLast != null) c.NameLast = c.NameLast.Replace("\n", " ").Replace("\r", "").Trim();
            if (c.State != null) c.State = c.State.Replace("\n", " ").Replace("\r", "").Trim();
            if (c.Type != null) c.Type = c.Type.Replace("\n", " ").Replace("\r", "").Trim();
            if (c.Zip != null) c.Zip = c.Zip.Replace("\n", " ").Replace("\r", "").Trim();
            if (c.Comments != null) c.Comments = c.Comments.Replace("\n", " ").Replace("\r", "").Trim();
          }
          return package;
        }
      }
      catch (Exception ex) {
        Erros.Add(ex.Message);
        return null;
      }
    }

    public void CleanAgentPackageInfo(string agentId, bool cleanCustomerNotes) {
      using (var mng = new Management(ModuleInfo)) {
        mng.CleanAgentPackageData(agentId, cleanCustomerNotes);
      }
    }

    private void UpdateCsvPackage(List<spMngGetAgentPackageData_Result> csvPackage, List<spMngGetAgentPackageData_Result> dbPackage) {
      if (dbPackage == null || csvPackage == null) return;
      try {
        foreach (var dbp in dbPackage) {
          var csvp = csvPackage.FirstOrDefault(c => c.CustomerId == dbp.CustomerId);
          if (csvp == null) {
            csvPackage.Add(dbp);
            continue;
          }
          if (!string.IsNullOrWhiteSpace(dbp.Type)) csvp.Type = dbp.Type;
          if (!string.IsNullOrWhiteSpace(dbp.AgentId)) csvp.AgentId = dbp.AgentId;
          if (!string.IsNullOrWhiteSpace(dbp.CustomerId)) csvp.CustomerId = dbp.CustomerId;
          if (!string.IsNullOrWhiteSpace(dbp.NameLast)) csvp.NameLast = dbp.NameLast;
          if (!string.IsNullOrWhiteSpace(dbp.NameFirst)) csvp.NameFirst = dbp.NameFirst;
          if (!string.IsNullOrWhiteSpace(dbp.Address)) csvp.Address = dbp.Address;
          if (!string.IsNullOrWhiteSpace(dbp.City)) csvp.City = dbp.City;
          if (!string.IsNullOrWhiteSpace(dbp.State)) csvp.State = dbp.State;
          if (!string.IsNullOrWhiteSpace(dbp.Zip)) csvp.Zip = dbp.Zip;
          if (!string.IsNullOrWhiteSpace(dbp.EMail)) csvp.EMail = dbp.EMail;
          if (!string.IsNullOrWhiteSpace(dbp.HomePhone)) csvp.HomePhone = dbp.HomePhone;
          if (!string.IsNullOrWhiteSpace(dbp.BusinessPhone)) csvp.BusinessPhone = dbp.BusinessPhone;
          if (!string.IsNullOrWhiteSpace(dbp.Fax)) csvp.Fax = dbp.Fax;
          if (dbp.LastChangedByDateTime != null) csvp.LastChangedByDateTime = dbp.LastChangedByDateTime;
          if (!string.IsNullOrWhiteSpace(dbp.Comments)) csvp.Comments = dbp.Comments;
        }
      }
      catch (Exception ex) {
        Erros.Add(ex.Message);
      }
    }

    private List<spMngGetAgentPackageData_Result> CsvDeserialize(string fileName) {
      try {
        return File.ReadAllLines(fileName)
          .Skip(1)
          .Select(CustomerFromCsv)
          .ToList();
      }
      catch (Exception ex) {
        Erros.Add(ex.Message);
        return null;
      }
    }

    private spMngGetAgentPackageData_Result CustomerFromCsv(string csvLine) {
      try {
        var regex = new Regex("(?<=^|,)(\"(?:[^\"]|\"\")*\"|[^,]*)");
        var values = regex.Matches(csvLine); //  csvLine.Split(',');
        return new spMngGetAgentPackageData_Result {
          Type = values[0].Value,
          AgentId = values[1].Value,
          CustomerId = values[2].Value,
          NameLast = values[3].Value,
          NameFirst = values[4].Value,
          Address = values[5].Value,
          City = values[6].Value,
          State = values[7].Value,
          Zip = values[8].Value,
          EMail = values[9].Value,
          HomePhone = values[10].Value,
          BusinessPhone = values[11].Value,
          Fax = values[12].Value,
          LastChangedByDateTime = string.IsNullOrEmpty(values[13].Value) ? (DateTime?)null : DateTime.Parse(values[13].Value),
          Comments = values[14].Value
        };
      }
      catch (Exception ex) {
        Erros.Add(ex.Message);
        return null;
      }
    }

    private DateTime? GetFileDateTime(string fileName) {
      try {
        var fileInfo = fileName.Replace(".csv", "").Split(' ');
        if (fileInfo.Length < 2) return null;
        var dateInfo = fileInfo[1].Split('-');
        if (dateInfo.Length < 4) return null;
        return new DateTime(int.Parse(dateInfo[0]), int.Parse(dateInfo[1]), int.Parse(dateInfo[2]), int.Parse(dateInfo[3]), int.Parse(dateInfo[4]), 0);
      }
      catch (Exception ex) {
        Erros.Add(ex.Message);
        return null;
      }
    }

    private String NewFileName(string fileName, DateTime serverDateTime) {
      try {
        var newFileName = fileName.Replace(".csv", "").Split(' ')[0];
        newFileName += " " + serverDateTime.Year + "-" + serverDateTime.Month + "-" + serverDateTime.Day + "-" + serverDateTime.Hour + "-" + serverDateTime.Minute + ".csv";
        return newFileName;
      }
      catch (Exception ex) {
        Erros.Add(ex.Message);
        return string.Empty;
      }
    }

  }
}