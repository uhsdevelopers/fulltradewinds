﻿using System.Collections.Generic;
using System.Drawing;
using CustomerMaintenance.UI;
using InstanceManager.BusinessLayer;
using SIDLibraries.Entities;
using SIDLibraries.BusinessLayer;
using SIDLibraries.UI;

// ReSharper disable once CheckNamespace
namespace CustomerMaintenance.Extensions {
  class PerformanceByWagerType : ExtensionCore {
    private readonly FrmCustomerMaintenance _customerMaintenance;
    private readonly ModuleInfo _moduleInfo;
    public PerformanceByWagerType(ModuleInfo moduleInfo, FrmCustomerMaintenance customerMaintenance) : base(moduleInfo, "PerformanceByWagerType") {
      _moduleInfo = moduleInfo;
      _customerMaintenance = customerMaintenance;
      if (IsActive()) {
        customerMaintenance.grpWagerTypes.Visible = true;
        _customerMaintenance.cmbWagerTypes.SelectedIndexChanged -= _customerMaintenance.cmbWagerTypes_SelectedIndexChanged;
        FillWagerTypeCmb();
        _customerMaintenance.cmbWagerTypes.SelectedIndexChanged += _customerMaintenance.cmbWagerTypes_SelectedIndexChanged;
        customerMaintenance.grpViewDailyFigs.Location = new Point(4, 80);
      }
      else {
        customerMaintenance.grpWagerTypes.Visible = false;
        customerMaintenance.grpViewDailyFigs.Location = new Point(4, 4);
      }
    }

    public List<spCstGetCustomerPerformanceByPeriod_Result> GetCustomerPerformance() {
      List<spCstGetCustomerPerformanceByPeriod_Result> gvDataSource;

      using (var df = new DailyFigures(_moduleInfo)) {
        if (_customerMaintenance.cmbWagerTypes.SelectedIndex > 0) {
          var wagerType = ((WagerType)_customerMaintenance.cmbWagerTypes.SelectedItem).Code;
          gvDataSource = df.GetCustomerPerformanceByPeriodAndWagerType(_customerMaintenance.txtCustomerId.Text, _customerMaintenance.PerformanceViewDateRange, wagerType);
        }
        else {
          gvDataSource = df.GetCustomerPerformanceByPeriod(_customerMaintenance.txtCustomerId.Text, _customerMaintenance.PerformanceViewDateRange);
        }
      }

      return gvDataSource;
    }

    public void FillWagerTypeCmb() {
      if (!IsActive()) return;
      var wagerTypes = WagerType.GetWagerTypes();

      var wt = new WagerType {Name = "All", Code = "", WebCode = 0};
      wagerTypes.Insert(0, wt);

      _customerMaintenance.cmbWagerTypes.DataSource = wagerTypes;
      _customerMaintenance.cmbWagerTypes.DisplayMember = "Name";
      _customerMaintenance.cmbWagerTypes.ValueMember = "Code";
    }

  }
}
