﻿using InstanceManager.BusinessLayer;
using CustomerMaintenance.UI;
using SIDLibraries.UI;

// ReSharper disable once CheckNamespace
namespace CustomerMaintenance.Extensions {
  public class UseDetailedParlayTeaserWagerLimits : ExtensionCore {
    readonly FrmCustomerMaintenance _customerMaintenanceFrm;
    public UseDetailedParlayTeaserWagerLimits(ModuleInfo moduleInfo, FrmCustomerMaintenance customerMaintenanceFrm)
      : base(moduleInfo, "UseDetailedParlayTeaserWagerLimits") {
      _customerMaintenanceFrm = customerMaintenanceFrm;
      HideNonRequiredObjects();
    }

    private void HideNonRequiredObjects() {
      _customerMaintenanceFrm.btnParlayLimits.Visible = false;
      _customerMaintenanceFrm.btnTeaserLimits.Visible = false;
      _customerMaintenanceFrm.chbEnforceParlayBetLimit.Visible = false;
      _customerMaintenanceFrm.chbEnforceTeaserBetLimit.Visible = false;

      if (!IsActive()) return;
      //CustomerMaintenanceFrm.txtMaxParlayBet.Visible = false;
      //CustomerMaintenanceFrm.btnShowAuditParlayMaxBet.Visible = false;
      _customerMaintenanceFrm.btnParlayLimits.Visible = true;
      _customerMaintenanceFrm.chbEnforceParlayBetLimit.Visible = true;
      //CustomerMaintenanceFrm.txtMaxTeaserBet.Visible = false;
      //CustomerMaintenanceFrm.btnShowAuditTeaserMaxBet.Visible = false;
      _customerMaintenanceFrm.btnTeaserLimits.Visible = true;
      _customerMaintenanceFrm.chbEnforceTeaserBetLimit.Visible = true;
    }
  }
}
