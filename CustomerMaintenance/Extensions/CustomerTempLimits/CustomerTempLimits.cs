﻿using System.Windows.Forms;
using CustomerMaintenance.UI;
using InstanceManager.BusinessLayer;
using SIDLibraries.UI;
using SIDLibraries.Utilities;

// ReSharper disable once CheckNamespace
namespace CustomerMaintenance.Extensions {
  class CustomerTempLimits : ExtensionCore {


    public CustomerTempLimits(ModuleInfo moduleInfo, FrmCustomerMaintenance custMaintenanceForm) : base(moduleInfo, "CustomerTempLimits") {
      custMaintenanceForm.SetCustomerTempLimitExtension(IsActive());
    }

    public void AddDetailLimitsColumns (DataGridView grid){
      grid.Columns.Add("TempCULimit", "Temp CU Limit");
      grid.Columns.Add("TempInetLimit", "Temp Inet Limit");
      grid.Columns.Add("TempLimitExpiration", "Temp Limit Expiration");
      grid.Columns[FrmCustomerMaintenance.TEMP_CU_LIMIT_INDEX].Width = 70;
      grid.Columns[FrmCustomerMaintenance.TEMP_INET_LIMIT_INDEX].Width = 70;
      grid.Columns[FrmCustomerMaintenance.TEMP_LIMIT_EXP_INDEX].Width = 90;
      if (!IsActive())
      {
          grid.Columns[FrmCustomerMaintenance.TEMP_CU_LIMIT_INDEX].Visible = false;
          grid.Columns[FrmCustomerMaintenance.TEMP_INET_LIMIT_INDEX].Visible = false;
          grid.Columns[FrmCustomerMaintenance.TEMP_LIMIT_EXP_INDEX].Visible = false;
      }
    }

    public void GetDetailLimits(DataGridViewRow row, out string tempCuLimit, out string tempInetLimit, out string tempLimitsExpiration) {
      tempCuLimit = "0";
      tempInetLimit = "0";
      tempLimitsExpiration = string.Empty;
      if (!IsActive()) return;
      tempCuLimit = StringF.ToStringOrEmpty(row.Cells[FrmCustomerMaintenance.TEMP_CU_LIMIT_INDEX].IsInEditMode ? row.Cells[FrmCustomerMaintenance.TEMP_CU_LIMIT_INDEX].EditedFormattedValue : row.Cells[FrmCustomerMaintenance.TEMP_CU_LIMIT_INDEX].Value);
      tempInetLimit = StringF.ToStringOrEmpty(row.Cells[FrmCustomerMaintenance.TEMP_INET_LIMIT_INDEX].IsInEditMode ? row.Cells[FrmCustomerMaintenance.TEMP_INET_LIMIT_INDEX].EditedFormattedValue : row.Cells[FrmCustomerMaintenance.TEMP_INET_LIMIT_INDEX].Value);
      tempLimitsExpiration = StringF.ToStringOrEmpty(row.Cells[FrmCustomerMaintenance.TEMP_LIMIT_EXP_INDEX].IsInEditMode ? row.Cells[FrmCustomerMaintenance.TEMP_LIMIT_EXP_INDEX].EditedFormattedValue : row.Cells[FrmCustomerMaintenance.TEMP_LIMIT_EXP_INDEX].Value);
    }

  }
}
