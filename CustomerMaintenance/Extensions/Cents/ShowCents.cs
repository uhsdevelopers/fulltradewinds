﻿using InstanceManager.BusinessLayer;
using SIDLibraries.UI;

// ReSharper disable once CheckNamespace
namespace CustomerMaintenance.Extensions {
  class ShowCents : ExtensionCore  {

    public ShowCents(ModuleInfo moduleInfo, SystemParams args) : base(moduleInfo, "ShowCents") { args.IncludeCents = IsActive(); }

  }
}
