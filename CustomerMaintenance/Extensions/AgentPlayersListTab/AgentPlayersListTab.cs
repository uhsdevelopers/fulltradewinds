﻿using System;
using CustomerMaintenance.UI;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;
using SIDLibraries.UI;
using SIDLibraries.Utilities;
using System.Windows.Forms;
using System.Linq;
using System.Drawing;

// ReSharper disable once CheckNamespace
namespace CustomerMaintenance.Extensions {
  class AgentPlayersListTab : ExtensionCore {

    #region private Classes

    private class AgentSummaryTotals {
      public double TotalBank { get; set; }
      public double TotalMon { get; set; }
      public double TotalTue { get; set; }
      public double TotalWed { get; set; }
      public double TotalThu { get; set; }
      public double TotalFri { get; set; }
      public double TotalSat { get; set; }
      public double TotalSun { get; set; }
      public double TotalWeek {
        get {
          return (TotalMon + TotalTue + TotalWed + TotalThu + TotalFri + TotalSat + TotalSun);
        }
      }
      public double TotalPendingBalance { get; set; }
      public double TotalWeeklyFigure { get; set; }
      public double TotalCurrentBalance { get; set; }

      public AgentSummaryTotals() {
        TotalBank = 0.0;
        TotalMon = 0.0;
        TotalTue = 0.0;
        TotalWed = 0.0;
        TotalThu = 0.0;
        TotalFri = 0.0;
        TotalSat = 0.0;
        TotalSun = 0.0;
        TotalPendingBalance = 0;
        TotalWeeklyFigure = 0;
        TotalCurrentBalance = 0;
      }
    }

    #endregion

    #region Public Properties

    public Boolean ResetDatesCmbIdx;

    #endregion

    #region Private Vars

    private readonly FrmCustomerMaintenance _customerMaintenance;
    private ComboBox _cmbWeeksRange;
    private bool _showAgentSummary;

    #endregion

    #region Structures

    #endregion

    #region Constructors

    public AgentPlayersListTab(ModuleInfo moduleInfo, FrmCustomerMaintenance customerMaintenance)
      : base(moduleInfo, "ShowAgentPlayersListTab") {
      _customerMaintenance = customerMaintenance;
    }

    #endregion

    #region Public Methods

    #endregion

    #region Private Methods

    private void AddTotalsRow(String agentType, DataGridView playersListGrid, AgentSummaryTotals totals, int weekNumber) {
      switch (agentType) {
        case Agents.MASTERAGENT_TYPE:
          playersListGrid.Rows.Add(
          "Totals ",
          FormatNumber(totals.TotalBank, false, 1, true),
          FormatNumber(totals.TotalMon, false, 1, true),
          FormatNumber(totals.TotalTue, false, 1, true),
          FormatNumber(totals.TotalWed, false, 1, true),
          FormatNumber(totals.TotalThu, false, 1, true),
          FormatNumber(totals.TotalFri, false, 1, true),
          FormatNumber(totals.TotalSat, false, 1, true),
          FormatNumber(totals.TotalSun, false, 1, true),
          FormatNumber(totals.TotalWeek, false, 1, true)
          );
          break;
        case Agents.AGENT_TYPE:
          playersListGrid.Rows.Add(
          "Totals ",
          FormatNumber(totals.TotalMon, false, 1, false),
          FormatNumber(totals.TotalPendingBalance, false, 1, true),
          FormatNumber(totals.TotalWeeklyFigure, false, 1, true),
          FormatNumber(totals.TotalCurrentBalance, false, 1, true),
          FormatNumber(totals.TotalThu, false, 1, false),
          FormatNumber(totals.TotalFri, false, 1, false),
          FormatNumber(totals.TotalSun, false, 1, false)
          );
          break;
      }

      DataGridViewRow lastRow = playersListGrid.Rows[playersListGrid.RowCount - 1];

      lastRow.Cells["CustAgentId"].Style.Alignment = DataGridViewContentAlignment.MiddleRight;
      lastRow.DefaultCellStyle.Font = new Font("Microsoft Sans Serif", 8.25F, FontStyle.Bold);

      if (weekNumber > 0 && agentType == Agents.AGENT_TYPE) {
        foreach (DataGridViewRow row in playersListGrid.Rows) {
          foreach (DataGridViewCell cell in row.Cells) {
            if (cell.ColumnIndex == 0 || cell.ColumnIndex == 1 || cell.ColumnIndex == 3) {
              continue;
            }
            cell.Style.ForeColor = Color.DarkGray;
          }
        }
      }
    }

    private void BuildAgentSummaryWeeklyFigures(ComboBox cmbWeeksRange, DataGridView agentListGrid, string agentId, string agentType, int? weekIdx) {
      CreateAgentPlayersListGridHeaders(cmbWeeksRange, agentListGrid, agentType, _showAgentSummary);
      FillAgentPlayersListGridView(agentListGrid, agentId, agentType, weekIdx, _showAgentSummary);
    }

    internal void BuildPlayersTab() {
      var tbPage = new TabPage { Name = "tbPlayers", Text = @"Players" };
      FillAgentPlayersListTab(tbPage, _customerMaintenance.TbCust.AgentType, _customerMaintenance.TbCust.CustomerID);
      var tabCustomer = (TabControl)_customerMaintenance.Controls.Find("tabCustomer", true).FirstOrDefault();
      if (tabCustomer == null)
        return;
      tabCustomer.TabPages.Insert(0, tbPage);
      tabCustomer.SelectedIndex = 0;
    }

    private void ChangeGridHeaderDates(DataGridView affectedGridView, int startingWeek) {
      var startWeekDateTime = DateTimeF.GetStartWeekDateTime(startingWeek);

      foreach (DataGridViewColumn column in affectedGridView.Columns) {
        switch (column.Name) {
          case "Mon":
            column.HeaderText = column.Name + Environment.NewLine + DateTimeF.AddLeadingZero(startWeekDateTime.Month) + @"/" + DateTimeF.AddLeadingZero(startWeekDateTime.Day);
            break;
          case "Tue":
            column.HeaderText = column.Name + Environment.NewLine + DateTimeF.AddLeadingZero(startWeekDateTime.AddDays(1).Month) + @"/" + DateTimeF.AddLeadingZero(startWeekDateTime.AddDays(1).Day);
            break;
          case "Wed":
            column.HeaderText = column.Name + Environment.NewLine + DateTimeF.AddLeadingZero(startWeekDateTime.AddDays(2).Month) + @"/" + DateTimeF.AddLeadingZero(startWeekDateTime.AddDays(2).Day);
            break;
          case "Thu":
            column.HeaderText = column.Name + Environment.NewLine + DateTimeF.AddLeadingZero(startWeekDateTime.AddDays(3).Month) + @"/" + DateTimeF.AddLeadingZero(startWeekDateTime.AddDays(3).Day);
            break;
          case "Fri":
            column.HeaderText = column.Name + Environment.NewLine + DateTimeF.AddLeadingZero(startWeekDateTime.AddDays(4).Month) + @"/" + DateTimeF.AddLeadingZero(startWeekDateTime.AddDays(4).Day);
            break;
          case "Sat":
            column.HeaderText = column.Name + Environment.NewLine + DateTimeF.AddLeadingZero(startWeekDateTime.AddDays(5).Month) + @"/" + DateTimeF.AddLeadingZero(startWeekDateTime.AddDays(5).Day);
            break;
          case "Sun":
            column.HeaderText = column.Name + Environment.NewLine + DateTimeF.AddLeadingZero(startWeekDateTime.AddDays(6).Month) + @"/" + DateTimeF.AddLeadingZero(startWeekDateTime.AddDays(6).Day);
            break;
        }
      }

    }

    private void CreateAgentPlayersListGridHeaders(ComboBox cmbWeeksRange, DataGridView playersListGrid, string agentType, bool setupForSummary = false) {
      DataGridViewTextBoxColumn titleColumn;
      if (setupForSummary)
        agentType = Agents.MASTERAGENT_TYPE;
      switch (agentType) {
        case Agents.AGENT_TYPE:
          titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"CustomerId", Width = 100 };
          playersListGrid.Columns.Add(titleColumn);

          titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Password", Width = 75 };
          playersListGrid.Columns.Add(titleColumn);

          titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Pending Bal", Width = 75 };
          playersListGrid.Columns.Add(titleColumn);

          titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Weekly Figure", Width = 75 };
          playersListGrid.Columns.Add(titleColumn);

          titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Current Balance", Width = 75 };
          playersListGrid.Columns.Add(titleColumn);

          titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Credit Limit", Width = 75 };
          playersListGrid.Columns.Add(titleColumn);

          titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Wager Limit", Width = 75 };
          playersListGrid.Columns.Add(titleColumn);

          titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Settle Figure", Width = 75 };
          playersListGrid.Columns.Add(titleColumn);

          titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Last Wager", Width = 125 };
          playersListGrid.Columns.Add(titleColumn);

          playersListGrid.Columns[0].Name = "CustAgentId";
          playersListGrid.Columns[1].Name = "Password";
          playersListGrid.Columns[2].Name = "PendingBal";
          //playersListGrid.Columns[3].Name = "Non_PostedCasino";
          playersListGrid.Columns[3].Name = "WeeklyFigure";
          playersListGrid.Columns[4].Name = "Current_Balance";
          playersListGrid.Columns[5].Name = "CreditLimit";
          playersListGrid.Columns[6].Name = "WagerLimit";
          playersListGrid.Columns[7].Name = "SettleFigure";
          playersListGrid.Columns[8].Name = "LastWager";
          if (playersListGrid.Columns["CustAgentId"] != null) playersListGrid.Columns["CustAgentId"].DefaultCellStyle.Font = new Font(Control.DefaultFont, FontStyle.Bold);
          if (playersListGrid.Columns["PendingBal"] != null) playersListGrid.Columns["PendingBal"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
          //if (playersListGrid.Columns["Non_PostedCasino"] != null) playersListGrid.Columns["Non_PostedCasino"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
          if (playersListGrid.Columns["WeeklyFigure"] != null) playersListGrid.Columns["WeeklyFigure"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
          if (playersListGrid.Columns["Current_Balance"] != null) playersListGrid.Columns["Current_Balance"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
          if (playersListGrid.Columns["CreditLimit"] != null) playersListGrid.Columns["CreditLimit"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
          if (playersListGrid.Columns["WagerLimit"] != null) playersListGrid.Columns["WagerLimit"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
          if (playersListGrid.Columns["SettleFigure"] != null) playersListGrid.Columns["SettleFigure"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
          break;

        case "M":
          titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"AgentId", Width = 75 };
          playersListGrid.Columns.Add(titleColumn);

          titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Bank", Width = 75 };
          playersListGrid.Columns.Add(titleColumn);

          titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Mon", Width = 70 };
          playersListGrid.Columns.Add(titleColumn);

          titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Tue", Width = 70 };
          playersListGrid.Columns.Add(titleColumn);

          titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Wed", Width = 70 };
          playersListGrid.Columns.Add(titleColumn);

          titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Thu", Width = 70 };
          playersListGrid.Columns.Add(titleColumn);

          titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Fri", Width = 70 };
          playersListGrid.Columns.Add(titleColumn);

          titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Sat", Width = 75 };
          playersListGrid.Columns.Add(titleColumn);

          titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Sun", Width = 75 };
          playersListGrid.Columns.Add(titleColumn);

          titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Total " + Environment.NewLine + @"Week", Width = 75 };
          playersListGrid.Columns.Add(titleColumn);

          playersListGrid.Columns[0].Name = "CustAgentId";
          playersListGrid.Columns[1].Name = "Bank";
          playersListGrid.Columns[2].Name = "Mon";
          playersListGrid.Columns[3].Name = "Tue";
          playersListGrid.Columns[4].Name = "Wed";
          playersListGrid.Columns[5].Name = "Thu";
          playersListGrid.Columns[6].Name = "Fri";
          playersListGrid.Columns[7].Name = "Sat";
          playersListGrid.Columns[8].Name = "Sun";
          playersListGrid.Columns[9].Name = "TotalWeek";
          if (playersListGrid.Columns["CustAgentId"] != null) playersListGrid.Columns["CustAgentId"].DefaultCellStyle.Font = new Font(Control.DefaultFont, FontStyle.Bold);
          if (playersListGrid.Columns["Bank"] != null) playersListGrid.Columns["Bank"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
          if (playersListGrid.Columns["Mon"] != null) playersListGrid.Columns["Mon"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
          if (playersListGrid.Columns["Tue"] != null) playersListGrid.Columns["Tue"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
          if (playersListGrid.Columns["Wed"] != null) playersListGrid.Columns["Wed"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
          if (playersListGrid.Columns["Thu"] != null) playersListGrid.Columns["Thu"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
          if (playersListGrid.Columns["Fri"] != null) playersListGrid.Columns["Fri"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
          if (playersListGrid.Columns["Sat"] != null) playersListGrid.Columns["Sat"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
          if (playersListGrid.Columns["Sun"] != null) playersListGrid.Columns["Sun"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
          if (playersListGrid.Columns["TotalWeek"] != null) playersListGrid.Columns["TotalWeek"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

          ChangeGridHeaderDates(playersListGrid, cmbWeeksRange.SelectedIndex);
          break;
      }

      foreach (DataGridViewColumn column in playersListGrid.Columns) {
        column.SortMode = DataGridViewColumnSortMode.NotSortable;
        column.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
      }
    }

    private ComboBox CreateWeeksRangeDropDown() {
      var weeksRange = new ComboBox { Name = "cmbWeeksRange", DropDownStyle = ComboBoxStyle.DropDownList };
      weeksRange.Items.AddRange(DateTimeF.AddDateRangeItemsList(8));
      weeksRange.SelectedIndex = 0;
      weeksRange.Size = new Size(125, 20);
      weeksRange.Location = new Point(8, 15);
      weeksRange.SelectedIndexChanged += weeksRange_SelectedIndexChanged;
      return weeksRange;
    }

    private void FillAgentPlayersListGridView(DataGridView playersListGrid, string agentId, string agentType, int? weekNumber, bool setupForSummary = false) {
      using (var agent = new Agents(_customerMaintenance.AppModuleInfo)) {
        var totals = new AgentSummaryTotals();
        playersListGrid.Rows.Clear();
        if (setupForSummary)
          agentType = Agents.MASTERAGENT_TYPE;
        switch (agentType) {
          case Agents.AGENT_TYPE:
            var customersList = agent.GetCustomerListWithLastWagerDate(agentId, weekNumber ?? 0);
            if (customersList != null && customersList.Count > 0) {
              //_totalCurrentBalance = (from c in customersList select c.CurrentBalance).Sum();
              foreach (var customer in customersList) {
                playersListGrid.Rows.Add(
                customer.CustomerId.Trim(),
                customer.password,
                FormatNumber(customer.PendingWagerBalance, true, 100, false),
                FormatNumber(customer.WeeklyFigure ?? 0, false, 1, false),
                FormatNumber(customer.CurrentBalance, true, 100, false),
                FormatNumber(customer.CreditLimit, 100, false, false),
                FormatNumber(customer.WagerLimit, 100, false, false),
                FormatNumber(customer.SettleFigure, 1, false, false),
                customer.Lastplay);
                totals.TotalPendingBalance += customer.PendingWagerBalance / 100;
                totals.TotalWeeklyFigure += customer.WeeklyFigure ?? 0;
                totals.TotalCurrentBalance += customer.CurrentBalance / 100;
              }
              AddTotalsRow(agentType, playersListGrid, totals, weekNumber ?? 0);
            }
            break;
          case "M":
            var agentsList = agent.GetAgentDailyFiguresByMasterByDw(agentId, weekNumber ?? 0);
            if (agentsList != null && agentsList.Count > 0) {
              string holdAgentId = null;
              double? holdBank = 0.0;

              var agentRowTotals = new AgentSummaryTotals();

              foreach (var varAgent in agentsList) {
                if (holdAgentId != null && holdAgentId != varAgent.agentid.Trim()) {
                  AddGridRow(playersListGrid, holdAgentId, agentRowTotals);
                  totals.TotalBank += (double)holdBank;
                  totals.TotalMon += agentRowTotals.TotalMon;
                  totals.TotalTue += agentRowTotals.TotalTue;
                  totals.TotalWed += agentRowTotals.TotalWed;
                  totals.TotalThu += agentRowTotals.TotalThu;
                  totals.TotalFri += agentRowTotals.TotalFri;
                  totals.TotalSat += agentRowTotals.TotalSat;
                  totals.TotalSun += agentRowTotals.TotalSun;

                  agentRowTotals = new AgentSummaryTotals();
                }
                switch (varAgent.dailyFigureDw) {
                  case 1:
                    agentRowTotals.TotalMon = varAgent.NetAmt ?? 0;
                    break;
                  case 2:
                    agentRowTotals.TotalTue = varAgent.NetAmt ?? 0;
                    break;
                  case 3:
                    agentRowTotals.TotalWed = varAgent.NetAmt ?? 0;
                    break;
                  case 4:
                    agentRowTotals.TotalThu = varAgent.NetAmt ?? 0;
                    break;
                  case 5:
                    agentRowTotals.TotalFri = varAgent.NetAmt ?? 0;
                    break;
                  case 6:
                    agentRowTotals.TotalSat = varAgent.NetAmt ?? 0;
                    break;
                  case 7:
                    agentRowTotals.TotalSun = varAgent.NetAmt ?? 0;
                    break;
                }
                holdAgentId = varAgent.agentid.Trim();
                agentRowTotals.TotalBank = varAgent.CurrentBalance ?? 0;
                holdBank = varAgent.CurrentBalance ?? 0;
              }
              // print last row begin
              AddGridRow(playersListGrid, holdAgentId, agentRowTotals);
              totals.TotalBank += (double)holdBank;
              totals.TotalMon += agentRowTotals.TotalMon;
              totals.TotalTue += agentRowTotals.TotalTue;
              totals.TotalWed += agentRowTotals.TotalWed;
              totals.TotalThu += agentRowTotals.TotalThu;
              totals.TotalFri += agentRowTotals.TotalFri;
              totals.TotalSat += agentRowTotals.TotalSat;
              totals.TotalSun += agentRowTotals.TotalSun;
              // print last row end
              if (!setupForSummary)
                AddTotalsRow(agentType, playersListGrid, totals, weekNumber ?? 0);
            }
            break;
        }
        playersListGrid.ClearSelection();
      }
    }

    private void AddGridRow(DataGridView playersListGrid, String agentId, AgentSummaryTotals agentRowTotals) {
      playersListGrid.Rows.Add(
                    agentId,
                    FormatNumber(agentRowTotals.TotalBank, false, 1, true),
                    FormatNumber(agentRowTotals.TotalMon, false, 1, true),
                    FormatNumber(agentRowTotals.TotalTue, false, 1, true),
                    FormatNumber(agentRowTotals.TotalWed, false, 1, true),
                    FormatNumber(agentRowTotals.TotalThu, false, 1, true),
                    FormatNumber(agentRowTotals.TotalFri, false, 1, true),
                    FormatNumber(agentRowTotals.TotalSat, false, 1, true),
                    FormatNumber(agentRowTotals.TotalSun, false, 1, true),
                    FormatNumber(agentRowTotals.TotalWeek, false, 1, true)
                    );
    }

    private void FillAgentPlayersListTab(TabPage tbPlayers, String agentType, String agentId) {
      var location = new Point {X = 4, Y = 50};
      var playersListGrid = new DataGridView {
        Name = "dgvwplayersList", Location = location, Size = new Size(750, 350), AllowUserToAddRows = false,
        AllowUserToDeleteRows = false, AutoGenerateColumns = false, RowHeadersVisible = false, ColumnHeadersHeight = 50,
        SelectionMode = DataGridViewSelectionMode.FullRowSelect, AllowUserToResizeRows = false, ReadOnly = true, MultiSelect = false,
        AlternatingRowsDefaultCellStyle = { BackColor = Color.LightGray }, ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing
      };

      if (_cmbWeeksRange == null) {
        _cmbWeeksRange = new ComboBox();
        _cmbWeeksRange = CreateWeeksRangeDropDown();
      }
      if (ResetDatesCmbIdx) {
        _cmbWeeksRange.SelectedIndex = 0;
      }

      var display = new GroupBox { Name = "grpDisplay", Text = @"Display", Location = new Point(4, 7), Size = new Size(150, 40) };
      display.Controls.Add(_cmbWeeksRange);

      tbPlayers.Controls.Add(display);
      int? weekIdx = _cmbWeeksRange.SelectedIndex;

      CreateAgentPlayersListGridHeaders(_cmbWeeksRange, playersListGrid, agentType);
      FillAgentPlayersListGridView(playersListGrid, agentId, agentType, weekIdx);
      playersListGrid.CellDoubleClick += playersListGrid_CellDoubleClick;
      tbPlayers.Controls.Add(playersListGrid);
      _showAgentSummary = false;
      if (agentType != Agents.AGENT_TYPE) return;
      _showAgentSummary = true;
      var footerLocation = new Point {X = playersListGrid.Location.X, Y = playersListGrid.Height + 50};
      var agentListGrid = new DataGridView {
        Name = "dgvwAgentSummary", Location = footerLocation, Size = new Size(750, 350), AllowUserToAddRows = false,
        AllowUserToDeleteRows = false, AutoGenerateColumns = false, RowHeadersVisible = false, ColumnHeadersHeight = 50,
        SelectionMode = DataGridViewSelectionMode.FullRowSelect, AllowUserToResizeRows = false, ReadOnly = true, MultiSelect = false,
        AlternatingRowsDefaultCellStyle = { BackColor = Color.LightGray },
        AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill,
        ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing
      };
      BuildAgentSummaryWeeklyFigures(_cmbWeeksRange, agentListGrid, agentId, agentType, weekIdx);
      tbPlayers.Controls.Add(agentListGrid);
    }

    private void HandleWeeksRangeChange(object sender) {
      if (((ComboBox)sender).SelectedIndex <= -1) return;
      var idx = ((ComboBox)sender).SelectedIndex;
      var affectedGridView = (DataGridView)_customerMaintenance.Controls.Find("dgvwplayersList", true).FirstOrDefault();
      var summaryGridView = (DataGridView)_customerMaintenance.Controls.Find("dgvwAgentSummary", true).FirstOrDefault();
      if (affectedGridView == null && summaryGridView == null)
        return;
      if (_customerMaintenance.AgentType == "M") {
        ChangeGridHeaderDates(affectedGridView, idx);
      }
      FillAgentPlayersListGridView(affectedGridView, _customerMaintenance.CustomerId.Trim(), _customerMaintenance.AgentType, idx);

      if (_showAgentSummary && summaryGridView != null) {
        ChangeGridHeaderDates(summaryGridView, idx);
        FillAgentPlayersListGridView(summaryGridView, _customerMaintenance.CustomerId.Trim(), _customerMaintenance.AgentType, idx, _showAgentSummary);
      }
    }

    private void HandleDoubleClickActionOnGrid(object sender, DataGridViewCellEventArgs e) {
      if (_customerMaintenance.AgentType == "M" && e.RowIndex == (((DataGridView)sender).RowCount - 1))
        return;
      var customerId = ((DataGridView)sender).Rows[e.RowIndex].Cells["CustAgentId"].Value.ToString().Trim();
      var callingAgent = _customerMaintenance.txtCustomerId.Text;
      ResetDatesCmbIdx = false;
      LoadCustomer(customerId);

      var callingAgentLinkBtn = (LinkLabel)_customerMaintenance.Controls.Find("lklReturnToAgent", true).FirstOrDefault();

      if (callingAgentLinkBtn != null) {
        callingAgentLinkBtn.Text = @"Return to " + callingAgent;
        callingAgentLinkBtn.Visible = true;
        callingAgentLinkBtn.AccessibleName = callingAgent;
      }
    }

    public void LoadCustomer(string customerId) {
      _customerMaintenance.txtCustomerId.Text = "";
      if (LoginsAndProfiles.ValidateUserFunctionalityAccess(_customerMaintenance.CurrentUserPermissions, LoginsAndProfiles.UPDATE_CUSTOMER_MAINTENANCE)) {
        _customerMaintenance.SaveCustomerInfo();
      }
      else {
        _customerMaintenance.CustomerInfoRetrieved = false;
        _customerMaintenance.ClearFormElementsContent();
      }
      _customerMaintenance.txtCustomerId.Text = customerId;
      _customerMaintenance.CustomerId = customerId;
      _customerMaintenance.RetrieveCustomerInfo(customerId);
    }

    #endregion

    #region Protected Methods

    #endregion

    #region Events

    private void playersListGrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e) {
      HandleDoubleClickActionOnGrid(sender, e);
    }

    private void weeksRange_SelectedIndexChanged(object sender, EventArgs e) {
      HandleWeeksRangeChange(sender);
    }

    #endregion

  }
}