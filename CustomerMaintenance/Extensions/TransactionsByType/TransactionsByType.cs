﻿using System;
using System.Collections.Generic;
using CustomerMaintenance.UI;
using InstanceManager.BusinessLayer;
using SIDLibraries.Entities;
using SIDLibraries.BusinessLayer;
using SIDLibraries.UI;
using SIDLibraries.Utilities;
using System.Windows.Forms;

// ReSharper disable once CheckNamespace
namespace CustomerMaintenance.Extensions {
  class TransactionsByType : ExtensionCore {
    private readonly FrmCustomerMaintenance _customerMaintenance;
    public TransactionsByType(ModuleInfo moduleInfo, FrmCustomerMaintenance customerMaintenance)
      : base(moduleInfo, "TransactionsByType") {
      _customerMaintenance = customerMaintenance;
      if (IsActive()) {
        customerMaintenance.grpTranTypes.Visible = true;
        _customerMaintenance.cmbTranTypes.SelectedIndexChanged -= _customerMaintenance.cmbTranTypes_SelectedIndexChanged;
        FillTransactionTypesCmb();
        _customerMaintenance.cmbTranTypes.SelectedIndexChanged += _customerMaintenance.cmbTranTypes_SelectedIndexChanged;
        _customerMaintenance.dgvwTransactions.SelectionMode = DataGridViewSelectionMode.CellSelect;
        _customerMaintenance.dgvwTransactions.MultiSelect = true;
        _customerMaintenance.btnLastVerified.Visible = false;
        _customerMaintenance.panTranTotalSelected.Visible = true;
      }
      else {
        customerMaintenance.grpTranTypes.Visible = false;
        _customerMaintenance.dgvwTransactions.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        _customerMaintenance.dgvwTransactions.MultiSelect = false;
        _customerMaintenance.btnLastVerified.Visible = true;
        _customerMaintenance.panTranTotalSelected.Visible = false;
      }
    }

    public List<spCstGetCustomerTransactionsByDate_Result> GetCustomerTransactions(int p) {
      List<spCstGetCustomerTransactionsByDate_Result> gvDataSource;

      using (var tran = new Transactions(ModuleInfo)) {
        if (_customerMaintenance.cmbTranTypes.SelectedIndex > 0) {
          var tranType = ((Transactions.TransactionItem)_customerMaintenance.cmbTranTypes.SelectedItem).Type;
          gvDataSource = tran.GetCustomerTransactionsByDateAndType(_customerMaintenance.txtCustomerId.Text, DateTimeF.StartTransactionDateTime(p), tranType);
        }
        else {
          gvDataSource = tran.GetCustomerTransactionsByDate(_customerMaintenance.txtCustomerId.Text, DateTimeF.StartTransactionDateTime(p));
        }
      }

      return gvDataSource;
    }

    private void FillTransactionTypesCmb() {
      if (!IsActive()) return;
      var combinedTranTypes = Transactions.GetMinCombinedTranTypesList();//Transactions.GetCombinedTranTypesList();

      var tt = new Transactions.TransactionItem { Description = "All", Type = "", Code = "" };
      combinedTranTypes.Insert(0, tt);

      _customerMaintenance.cmbTranTypes.DataSource = combinedTranTypes;
      _customerMaintenance.cmbTranTypes.DisplayMember = "Description";
      _customerMaintenance.cmbTranTypes.ValueMember = "Type";
    }


    internal void HandleCellSelectionChangeEvent(object sender) {
      double credit = 0;
      double debit = 0;
      var str = string.Empty;

      foreach (DataGridViewCell cell in ((DataGridView)sender).SelectedCells) {
        if (cell.FormattedValue != null && (cell.ColumnIndex == 3 || cell.ColumnIndex == 4)) str = cell.FormattedValue.ToString();
        double val;
        if (double.TryParse(str, out val)) {
          if (cell.ColumnIndex == 3)
            credit += val;
          else
            debit += val;
        }
      }
      var isCashTran = _customerMaintenance.cmbTranTypes.Text.Trim() == "Cash Transactions";
      var total = isCashTran ? debit - credit : credit - debit;
      _customerMaintenance.txtTotalsTransSelected.Text = FormatNumber(total, false, 0, false);
    }

    internal string GetTranTypeCode(object sender) {
      var tranType = IsActive() ? ((DataGridView)sender).Rows[((DataGridView)sender).SelectedCells[0].RowIndex].Cells[6].Value.ToString() : ((DataGridView)sender).SelectedRows[0].Cells[6].Value.ToString();
      return tranType;
    }

    internal string GetTransactionAmount(object sender, int idx) {
      string tranAmount = IsActive() ? ((DataGridView)sender).Rows[((DataGridView)sender).SelectedCells[0].RowIndex].Cells[idx].Value.ToString() : ((DataGridView)sender).SelectedRows[0].Cells[idx].Value.ToString();
      return tranAmount;
    }

    internal DateTime GetTranDailyFigureDate(object sender) {
      var dailyFigureDate = DateTime.Parse(IsActive() ? ((DataGridView)sender).Rows[((DataGridView)sender).SelectedCells[0].RowIndex].Cells[7].Value.ToString() : ((DataGridView)sender).SelectedRows[0].Cells[7].Value.ToString());
      return dailyFigureDate;
    }

    internal DateTime GetTranDateTime(object sender) {
      var tranDateTime = DateTime.Parse(IsActive() ? ((DataGridView)sender).Rows[((DataGridView)sender).SelectedCells[0].RowIndex].Cells[0].Value.ToString() : ((DataGridView)sender).SelectedRows[0].Cells[0].Value.ToString());
      return tranDateTime;
    }

    internal int GetTranDocumentNumber(object sender) {
      var documentNumber = int.Parse(IsActive() ? ((DataGridView)sender).Rows[((DataGridView)sender).SelectedCells[0].RowIndex].Cells[1].Value.ToString() : ((DataGridView)sender).SelectedRows[0].Cells[1].Value.ToString());
      return documentNumber;
    }

    internal int GetTranGradeNumber(object sender) {
      var gradeNumber = int.Parse(IsActive() ? ((DataGridView)sender).Rows[((DataGridView)sender).SelectedCells[0].RowIndex].Cells[8].Value.ToString() : ((DataGridView)sender).SelectedRows[0].Cells[8].Value.ToString());
      return gradeNumber;
    }
  }
}
