﻿using System;
using System.Configuration;
using CustomerMaintenance.UI;
using GUILibraries;
using SIDLibraries.BusinessLayer;


namespace CustomerMaintenance {
  static class Program {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    /// 
    [STAThread]
    static void Main(string[] args) {

      String customerToOpen = null;
      String tabToFocusOn = null;

      if (args != null && args.Length >= 4) {
        customerToOpen = args[2];
        tabToFocusOn = args[3];
      }

      using (var appLoader = new AppLoader(InstanceManager.BusinessLayer.InstanceManager.Applications.CustomerMaintenance, args, ConfigurationManager.AppSettings["SystemId"])) {
        using (var customerMaintenanceApp = new FrmCustomerMaintenance(appLoader.InstanceManager.InstanceModuleInfo, appLoader.UserPermissions, customerToOpen, tabToFocusOn)
        { ApplicationInUseRecordId = appLoader.InstanceManager.ApplicationInUseRecordId }) 
        {
          if (appLoader.IsSafeToStart() && appLoader.UserHasPermission(LoginsAndProfiles.SETUP_CUSTOMERS))
              appLoader.StartApp(customerMaintenanceApp);
        }
        appLoader.KillApp();
      }
    }
  }
}
