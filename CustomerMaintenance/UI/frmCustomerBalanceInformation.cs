﻿using System;
using InstanceManager.BusinessLayer;
using SIDLibraries.Entities;
using SIDLibraries.BusinessLayer;
using GUILibraries.Forms;
using System.Windows.Forms;

namespace CustomerMaintenance.UI {
  public partial class FrmCustomerBalanceInformation : SIDForm {
    readonly String _customerAccountType;
    readonly int _customerPendingWagerCount;
    readonly double _customerPendingWagerBalance;
    readonly int _customerFreePlayPendingCount;
    readonly double _customerFreePlayPendingBalance;
    readonly String _customerCurrency;
    readonly double _customerCurrentBalance;
    readonly double _customerCreditLimit;
    readonly double _customerCreditIncrease;
    readonly DateTime _customerCreditIncreaseThru;
    readonly double _customerPendingCredit;
    readonly double _customerNonPostedCasinoBalance;
    readonly double _customerFreePlayBalance;
    readonly Boolean _weeklyLimitsEnabled;
    readonly Form _callerFrm;

    public FrmCustomerBalanceInformation(Form caller, String accountType, int pendingWagerCount, double pendingWagerBalance, int freePlayPendingCount, double freePlayPendingBalance,
        String currency, double currentBalance, double creditLimit, double creditIncrease, DateTime creditIncreaseThru, double pendingCredit, double nonPostedCasinoBalance, double freePlayBalance, ModuleInfo moduleInfo, Boolean weeklyLimitsEnabled, double customerThisWeeksFigure)
      : base(moduleInfo) {
      _callerFrm = caller;
      _weeklyLimitsEnabled = weeklyLimitsEnabled;
      _customerAccountType = accountType;
      _customerPendingWagerCount = pendingWagerCount;
      _customerFreePlayPendingCount = freePlayPendingCount;
      _customerFreePlayPendingBalance = freePlayPendingBalance;
      _customerCurrency = currency;
      _customerCurrentBalance = !_weeklyLimitsEnabled ? currentBalance : customerThisWeeksFigure;
      _customerCreditLimit = creditLimit;
      _customerCreditIncrease = creditIncrease;
      _customerCreditIncreaseThru = creditIncreaseThru;
      _customerPendingWagerBalance = pendingWagerBalance;
      _customerPendingCredit = pendingCredit;
      _customerNonPostedCasinoBalance = nonPostedCasinoBalance;
      _customerFreePlayBalance = freePlayBalance;
      InitializeComponent();
      if (_weeklyLimitsEnabled)
        lblCurrentBalance.Text = @"Week to Date:";
    }

    private void btnClose_Click(object sender, EventArgs e) {
      Close();
    }

    private void frmCustomerBalanceInformation_Load(object sender, EventArgs e) {
      FillFormObjects();

    }

    private void FillFormObjects() {
      txtBalanceInfoHeader.Text = _customerAccountType.ToUpper();
      txtBalanceInfoHeader.Text += Environment.NewLine;
      txtBalanceInfoHeader.Text += Environment.NewLine;
      txtBalanceInfoHeader.Text += @"Pending Count/Risk: " + _customerPendingWagerCount + @" / " + FormatNumber(_customerPendingWagerBalance, true, true);
      txtBalanceInfoHeader.Text += Environment.NewLine;
      txtBalanceInfoHeader.Text += Environment.NewLine;
      txtBalanceInfoHeader.Text += @"Free Play Pending Count/Risk: " + _customerFreePlayPendingCount + @" / " + FormatNumber(_customerFreePlayPendingBalance, true, true);
      txtBalanceInfoHeader.Text += Environment.NewLine;
      txtBalanceInfoHeader.Text += Environment.NewLine;
      txtBalanceInfoHeader.Text += _customerCurrency;
      txtBalanceInfoHeader.Text += Environment.NewLine;
      txtBalanceInfoHeader.Text += Environment.NewLine;

      txtBalanceInfoHeader.Text += GetTodaysRate();

      lblCurrentBalanceFigure.Text = FormatNumber(_customerCurrentBalance, true, true);

      var actualCreditLimit = _customerCreditLimit;
      if (_customerCreditIncreaseThru.ToShortDateString() != "1/1/1900" && _customerCreditIncreaseThru >= ServerDateTime.Date)
        actualCreditLimit += _customerCreditIncrease;

      lblCreditLimitFigure.Text = FormatNumber(actualCreditLimit, true, true);

      lblPendingCreditFigure.Text = FormatNumber(_customerPendingCredit, true, true);
      lblNonPostedCasinoBalanceFigure.Text = FormatNumber(_customerNonPostedCasinoBalance, true, true);

      lblPostedPendingBetsFigure.Text = FormatNumber(_customerPendingWagerBalance * -1, true, true);

      lblPostedPendingBets.Visible = true;
      lblPostedPendingBetsFigure.Visible = true;
      if (_customerAccountType != "Postup") {
        lblAvailableBalanceFigure.Text = FormatNumber(_customerCurrentBalance + actualCreditLimit + _customerPendingCredit + _customerNonPostedCasinoBalance - _customerPendingWagerBalance, true, true);
      }
      else {
        if (!_weeklyLimitsEnabled) {
          lblPostedPendingBets.Visible = false;
          lblPostedPendingBetsFigure.Visible = false;
        }
        lblAvailableBalanceFigure.Text = FormatNumber(_customerCurrentBalance + actualCreditLimit + _customerPendingCredit + _customerNonPostedCasinoBalance, true, true);
      }

      lblFreePlayBalanceFigure.Text = FormatNumber(_customerFreePlayBalance, true, true);
    }

    private string GetTodaysRate() {
      var todaysRate = "Today's Rate: ";
      var currencyCode = _customerCurrency.Split(' ');
      var effectiveDate = ServerDateTime;
      effectiveDate = new DateTime(effectiveDate.Year, effectiveDate.Month, effectiveDate.Day, 0, 0, 0);
      spCurGetCurrencyRateByDate_Result todaysRecord;
      using (var cur = new Currencies(AppModuleInfo)) {
        todaysRecord = cur.GetCurrencyRateByDate(currencyCode[0], effectiveDate);
      }

      if (todaysRecord != null) {
        todaysRate += todaysRecord.Rate;
      }
      return todaysRate;
    }

    private void FrmCustomerBalanceInformation_KeyDown(object sender, KeyEventArgs e) {
      if (e.KeyCode == Keys.F5) {
        ((FrmCustomerMaintenance)_callerFrm).RefreshCustomerBalanceInformationFrm(this);
      }
    }


  }
}