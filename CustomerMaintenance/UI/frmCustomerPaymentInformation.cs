﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using GUILibraries.Utilities;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using CustomerMaintenance.Libraries;
using GUILibraries.Forms;

namespace CustomerMaintenance.UI {
  public partial class FrmCustomerPaymentInformation : SIDForm {
    private readonly String _currentCustomerId;
    private readonly int _currentDocumentNumber;
    private string _state;
    private readonly Boolean _isArchivedTran;
    private readonly FrmCustomerTransactionDetails _parentForm;

    public FrmCustomerPaymentInformation(String customerId, int documentNumber, Boolean archivedTran, SIDForm callerForm) : base(callerForm.AppModuleInfo) {
      _currentCustomerId = customerId;
      _currentDocumentNumber = documentNumber;
      _isArchivedTran = archivedTran;
      _parentForm = (FrmCustomerTransactionDetails)callerForm;
      InitializeComponent();
    }


    private void frmCustomerPaymentInformation_Load(object sender, EventArgs e) {
      if (_isArchivedTran) {
        btnOk.Visible = false;
      }

      LoadPreviousPaymentsInformation();
      GetPaymentInformationInfo();
    }

    private void LoadPreviousPaymentsInformation() {
      using (var tran = new Transactions(AppModuleInfo)) {
        var previousInfo = tran.GetPayToTransactionPreviousInfo(_currentCustomerId).ToList();

        if (previousInfo.Count <= 0) return;
        WritePreviousInfoGrvwHeaders();
        dgrvwPreviousPaymentInfo.Rows.Add("New...", "", "", "", "", "", "", "", "", "", "");
        foreach (spTrnGetPayToTransactionPreviousInfo_Result res in previousInfo) {
          dgrvwPreviousPaymentInfo.Rows.Add(res.TranDateTime, res.NameFirst + " " + res.NameMI + " " + res.NameLast, res.NameFirst, res.NameMI, res.NameLast, res.Address, res.City, res.State, res.Zip, res.Country, res.Phone);
        }

        FormF.PreventDataGridViewColumnsToBeSortedAndResized(dgrvwPreviousPaymentInfo);

        dgrvwPreviousPaymentInfo.ClearSelection();
      }
    }

    private void GetPaymentInformationInfo() {

      using (var tran = new Transactions(AppModuleInfo)) {
        List<spTrnGetPayToTransactionInfo_Result> paymentInfo = tran.GetPayToTransactionInfo(_currentDocumentNumber);

        var cmbState = new CustomerComboBox(AppModuleInfo);

        if (paymentInfo.Count > 0) {
          foreach (spTrnGetPayToTransactionInfo_Result str in paymentInfo) {
            txtName.Text = str.NameFirst.Trim();
            txtNameMi.Text = str.NameMI.Trim();
            txtNameLast.Text = str.NameLast.Trim();
            txtAddress.Text = str.Address.Trim();
            txtCity.Text = str.City.Trim();
            panStates.Controls.Add(cmbState.States(str.State.Trim()));
            _state = str.State.Trim();
            txtPostalCode.Text = str.Zip.Trim();
            txtCountry.Text = str.Country.Trim();
            txtPhone.Text = str.Phone.Trim();


          }
        }
        else {
          panStates.Controls.Add(cmbState.States(""));
        }

        var initialPaymentInfo = new Transactions.CustomerPaymentInformationRecord {
          PaymentInfoNameFirst = txtName.Text,
          PaymentInfoNameMi = txtNameMi.Text,
          PaymentInfoNameLast = txtNameLast.Text,
          PaymentInfoAddress = txtAddress.Text,
          PaymentInfoCity = txtCity.Text,
          PaymentInfoState = _state,
          PaymentInfoZip = txtPostalCode.Text,
          PaymentInfoCountry = txtCountry.Text,
          PaymentInfoPhone = txtPhone.Text
        };

        if (tran.CustomerPaymentInformationRecords.Count > 0) {
          tran.CustomerPaymentInformationRecords.Clear();
        }

        tran.CustomerPaymentInformationRecords.Add(initialPaymentInfo);
      }

    }

    private void UpdatePaymentInformationInfo() {
      var cmbState = new CustomerComboBox(AppModuleInfo);

      txtName.Text = dgrvwPreviousPaymentInfo.SelectedRows[0].Cells[2].Value.ToString().Trim();
      txtNameMi.Text = dgrvwPreviousPaymentInfo.SelectedRows[0].Cells[3].Value.ToString().Trim();
      txtNameLast.Text = dgrvwPreviousPaymentInfo.SelectedRows[0].Cells[4].Value.ToString().Trim();
      txtAddress.Text = dgrvwPreviousPaymentInfo.SelectedRows[0].Cells[5].Value.ToString().Trim();
      txtCity.Text = dgrvwPreviousPaymentInfo.SelectedRows[0].Cells[6].Value.ToString().Trim();
      panStates.Controls.RemoveAt(0);
      panStates.Controls.Add(cmbState.States(dgrvwPreviousPaymentInfo.SelectedRows[0].Cells[7].Value.ToString().Trim()));
      txtPostalCode.Text = dgrvwPreviousPaymentInfo.SelectedRows[0].Cells[8].Value.ToString().Trim();
      txtCountry.Text = dgrvwPreviousPaymentInfo.SelectedRows[0].Cells[9].Value.ToString().Trim();
      txtPhone.Text = dgrvwPreviousPaymentInfo.SelectedRows[0].Cells[10].Value.ToString().Trim();
    }

    private void WritePreviousInfoGrvwHeaders() {
      dgrvwPreviousPaymentInfo.DataSource = null;

      if (dgrvwPreviousPaymentInfo.Rows.Count > 0) {
        dgrvwPreviousPaymentInfo.Rows.Clear();
      }

      var titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Date", Width = 80 };
      dgrvwPreviousPaymentInfo.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Full Name", Width = 75 };
      dgrvwPreviousPaymentInfo.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"NameFirst", Width = 75, Visible = false };
      dgrvwPreviousPaymentInfo.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"NameMi", Width = 50, Visible = false };
      dgrvwPreviousPaymentInfo.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"NameLast", Width = 50, Visible = false };
      dgrvwPreviousPaymentInfo.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Address", Width = 150 };
      dgrvwPreviousPaymentInfo.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"City", Width = 75 };
      dgrvwPreviousPaymentInfo.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"State", Width = 50 };
      dgrvwPreviousPaymentInfo.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Postal Code", Width = 50 };
      dgrvwPreviousPaymentInfo.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Country", Width = 50 };
      dgrvwPreviousPaymentInfo.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Phone", Width = 75, Visible = false };
      dgrvwPreviousPaymentInfo.Columns.Add(titleColumn);

    }

    private void btnCancel_Click(object sender, EventArgs e) {
      Close();
    }

    private void dgrvwPreviousPaymentInfo_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e) {
      if (((DataGridView)sender).SelectedRows.Count > 0) {
        UpdatePaymentInformationInfo();
      }
    }

    private void btnOk_Click(object sender, EventArgs e) {

      GetFinalPaymentInfo();
      using (var tran = new Transactions(AppModuleInfo)) {
        _parentForm.PayToInfo = tran.CustomerPaymentInformationRecords;
      }

      Close();
    }

    private void GetFinalPaymentInfo() {
      try {
        var finalPaymentInfo = new Transactions.CustomerPaymentInformationRecord {
          PaymentInfoNameFirst = txtName.Text,
          PaymentInfoNameMi = txtNameMi.Text,
          PaymentInfoNameLast = txtNameLast.Text,
          PaymentInfoAddress = txtAddress.Text,
          PaymentInfoCity = txtCity.Text
        };

        var statesCmb = new ComboBox();

        if (panStates.Controls.Count > 0) {
          statesCmb = (ComboBox)panStates.Controls[0];
        }

        string[] stateAbb = null;
        if (statesCmb.SelectedItem != null)
          stateAbb = statesCmb.SelectedItem.ToString().Split(' ');

        if (stateAbb != null)
          finalPaymentInfo.PaymentInfoState = stateAbb[0].Trim();
        finalPaymentInfo.PaymentInfoZip = txtPostalCode.Text;
        finalPaymentInfo.PaymentInfoCountry = txtCountry.Text;
        finalPaymentInfo.PaymentInfoPhone = txtPhone.Text;

        using (var tran = new Transactions(AppModuleInfo)) {
          tran.CustomerPaymentInformationRecords.Add(finalPaymentInfo);
        }
      }
      catch (Exception ex) {
        Log(ex);
      }
    }

    private void dgrvwPreviousPaymentInfo_RowEnter(object sender, DataGridViewCellEventArgs e) {
      if (((DataGridView)sender).SelectedRows.Count > 0) {
        UpdatePaymentInformationInfo();
      }
    }

  }
}
