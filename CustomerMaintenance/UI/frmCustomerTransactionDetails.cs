﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using CustomerMaintenance.Libraries;
using SIDLibraries.Utilities;
using GUILibraries.Controls;
using GUILibraries.Forms;

namespace CustomerMaintenance.UI {
  public partial class FrmCustomerTransactionDetails : SIDForm {

    #region Private Vars

    readonly List<spULPGetCurrentUserPermissions_Result> _currentUserPermissions;

    private readonly FrmCustomerMaintenance _parentFrm;

    private readonly String _weeklyLimitsEnabled;

    private CustomerInfoUpdater _customerInfo;

    private Boolean _tranFinalSpacesAdded;

    private String _paymentBy;

    #endregion

    #region Public Vars

    private readonly String _loadFormType; //Edit or New (record)
    private readonly String _currentCustomerId;
    private readonly String _currentCustomerAgentId;
    private readonly String _currentCustomerCurrency;
    private readonly int _currentDocumentNumber;
    private readonly DateTime _currentDocumentTranDateTime;
    private Boolean _isArchivedTran;

    public List<Transactions.CustomerPaymentInformationRecord> PayToInfo;

    public List<DebitCoverer> DebitCoverers;

    #endregion

    #region Public Properties

    #endregion

    #region Private Properties

    #endregion

    #region Structures

    #endregion

    #region Constructors

    public FrmCustomerTransactionDetails(String customerId, String agentId, String customerCurrency, String loadType, int documentNumber, DateTime documentTranDateTime, SIDForm parentFrm)
      : base(parentFrm.AppModuleInfo) {
      _currentCustomerId = customerId;
      _currentCustomerAgentId = agentId;
      _currentCustomerCurrency = customerCurrency;
      _loadFormType = loadType;
      _currentDocumentNumber = documentNumber;
      _currentDocumentTranDateTime = documentTranDateTime;
      _parentFrm = (FrmCustomerMaintenance)parentFrm;
      _weeklyLimitsEnabled = _parentFrm.CustomerHasWeeklyLimits;
      _currentUserPermissions = _parentFrm.CurrentUserPermissions;
      InitializeComponent();
    }

    #endregion

    #region Private Methods

    private void CalculateHoldAmount(double holdPerc) {
      lblHoldAmount.Text = @"Hold: " + FormatNumber(double.Parse(txtEnteredAmount.Text) * holdPerc, false, 1, true);
      lblAmountToRelease.Text = @"Release: " + FormatNumber(double.Parse(txtEnteredAmount.Text) - double.Parse(txtEnteredAmount.Text) * holdPerc);
      lblTranAmount.Text = FormatNumber(double.Parse(txtEnteredAmount.Text) - double.Parse(txtEnteredAmount.Text) * holdPerc);
    }

    private void PrepareFormToShowInEditMode() {
      using (var tran = new Transactions(AppModuleInfo)) {
        var customerTransaction = tran.GetCustomerTransactionByDocNumber(_currentDocumentNumber, _currentDocumentTranDateTime).FirstOrDefault();

        if (customerTransaction == null) return;
        var archiveDataAfterDays = Params.ArchiveDataAfterDays;
        var diffResult = ServerDateTime.Subtract(_currentDocumentTranDateTime);

        if (diffResult.Days >= archiveDataAfterDays) {
          _isArchivedTran = true;
        }

        _customerInfo = new CustomerInfoUpdater(_currentCustomerId, AppModuleInfo);

        _customerInfo.LoadCustomerTransactionInfoToStructFromDb(customerTransaction);

        var tranDateTime = DateTime.Parse(customerTransaction.TranDateTime.ToString());
        txtTranDateTime.Text = tranDateTime.ToShortDateString() + @" " + tranDateTime.ToShortTimeString();
        txtTranDateTime.Enabled = false;

        var txtTranType = new TextBox {
          Width = 155,
          Text = GetTranTypeName(customerTransaction.TranType),
          Enabled = false
        };
        panTranType.Controls.Add(txtTranType);

        double amount;

        if (customerTransaction.TranType == "I" || customerTransaction.TranType == "Q") {
          amount = (customerTransaction.Amount ?? 0) / 100;
        }
        else {
          if (customerTransaction.EnteredAmount != 0)
            amount = customerTransaction.EnteredAmount / 100;
          else
            amount = (customerTransaction.Amount ?? 0) / 100;
        }

        txtEnteredAmount.Text = FormatNumber(amount, false, 1, true);
        _paymentBy = customerTransaction.PaymentBy;

        var methods = tran.GetTransactionMethodsByTranType(GetTranTypeName(customerTransaction.TranType)).ToList();
        if (methods.Any()) {
          var paymentByCbx = new ComboBox();

          foreach (var str in methods) {
            String method = str.Method;
            paymentByCbx.Items.Add(method);
          }
          paymentByCbx.DropDownStyle = ComboBoxStyle.DropDownList;
          paymentByCbx.Text = _paymentBy;
          paymentByCbx.SelectedIndexChanged += paymentByCbx_SelectedIndexChanged;
          panTranMethod.Controls.Add(paymentByCbx);
        }
        else {
          var txtTranMethod = new TextBox { Width = 155, Text = _paymentBy, Enabled = false };
          panTranMethod.Controls.Add(txtTranMethod);
        }

        /*var txtTranMethod = new TextBox { Width = 155, Text = customerTransaction.PaymentBy, Enabled = false };
        panTranMethod.Controls.Add(txtTranMethod);*/

        switch (customerTransaction.TranType) {
          case "E":
          case "I":
            btnPayto.Visible = true;
            grpAdjustments.Visible = false;
            break;
          case "C":
          case "D":
            grpAdjustments.Visible = true;
            SetTranAdjustmentsParams(DateTime.Parse(customerTransaction.DailyFigureDate.ToString(CultureInfo.InvariantCulture)), customerTransaction.CasinoAdjustmentFlag);
            break;
          default:
            btnPayto.Visible = false;
            grpAdjustments.Visible = false;
            break;
        }

        txtFreePlay.Enabled = false;
        txtFees.Enabled = false;
        txtPromo.Enabled = false;

        if (int.Parse(customerTransaction.HoldPercent.ToString(CultureInfo.InvariantCulture)) > 0) {
          chbHold.Checked = true;
          var releaseDate = DateTime.Parse(customerTransaction.ReleaseDate.ToString(CultureInfo.InvariantCulture));
          var today = new DateTime(ServerDateTime.Year, ServerDateTime.Month, ServerDateTime.Day, 0, 0, 0);
          var ts = releaseDate - today;

          var holdNumDays = ts.Days;
          txtHold.Text = customerTransaction.HoldPercent.ToString(CultureInfo.InvariantCulture);
          txtNumberOfDays.Text = holdNumDays.ToString(CultureInfo.InvariantCulture);
        }
        else {
          chbHold.Checked = false;
          chbHold.Enabled = customerTransaction.TranType == "E";
          txtHold.Text = "";
          txtHold.Enabled = false;
          txtNumberOfDays.Text = "";
          txtNumberOfDays.Enabled = false;
        }

        grpCreditCard.Visible = LoginsAndProfiles.ValidateUserFunctionalityAccess(_currentUserPermissions, LoginsAndProfiles.DISPLAY_CREDIT_CARD_NUMBERS);



        txtCardNo.Text = customerTransaction.CCNumber;

        var ccExpDate = customerTransaction.CCExpires;

        txtCCExpDate.Text = ccExpDate;

        txtApprovalNo.Text = customerTransaction.CCApproval;
        btnCoverDeposit.Enabled = false;

        txtTranDescription.Text = customerTransaction.Description;

        txtReference.Text = customerTransaction.Reference;

        txtDocumentNumber.Text = customerTransaction.DocumentNumber.ToString(CultureInfo.InvariantCulture);
        txtDocumentNumber.Enabled = false;
        txtGradeNumber.Text = customerTransaction.GradeNum.ToString(CultureInfo.InvariantCulture);
        txtGradeNumber.Enabled = false;
        txtEnteredBy.Text = customerTransaction.EnteredBy;
        txtEnteredBy.Enabled = false;
      }

    }

    private string GetTranTypeName(string p) {
      var tranTypeName = "";
      try {
        switch (p) {
          case "C":
            tranTypeName = "Credit Adjustment";
            break;
          case "D":
            tranTypeName = "Debit Adjustment";
            break;
          case "E":
            tranTypeName = "Customer Deposit";
            break;
          case "F":
            tranTypeName = "Fees Credit";
            break;
          case "H":
            tranTypeName = "Fees Debit";
            break;
          case "B":
            tranTypeName = "Promotional Credit";
            break;
          case "N":
            tranTypeName = "Promotional Debit";
            break;
          case "T":
            tranTypeName = "Transfer Credit";
            break;
          case "U":
            tranTypeName = "Transfer Debit";
            break;
          case "I":
            tranTypeName = "Customer Withdrawal";
            break;

        }
      }
      catch (Exception ex) {
        Log(ex);
      }

      return tranTypeName;
    }

    private void SetTranAdjustmentsParams(DateTime dateTime, String casinoFlag) {
      var comparisonDate = new DateTime(1900, 01, 01, 0, 0, 0);

      if (dateTime != comparisonDate) {
        chbAdjustCustomersFigure.Checked = true;
        txtDailyFigureDate.Text = dateTime.ToShortDateString();
        chbCasinoAdjustment.Checked = casinoFlag == "Y";
      }
      else {
        chbAdjustCustomersFigure.Checked = false;
        txtDailyFigureDate.Text = "";
        chbCasinoAdjustment.Checked = false;
        dtmChangeTranDailyFigure.Enabled = false;

      }
    }

    private void PrepareFormToShowInAddNewMode() {
      txtTranDateTime.Text = ServerDateTime.ToShortDateString() + @" " + ServerDateTime.ToShortTimeString();
      txtTranDateTime.Enabled = false;

      var cbx = new CustomerComboBox(AppModuleInfo);

      if (panTranType.Controls.Count > 0) {
        panTranType.Controls.RemoveAt(0);
      }

      panTranType.Controls.Add(cbx.TransactionTypes(this));

      btnPayto.Visible = false;

      txtFreePlay.Enabled = false;
      txtFees.Enabled = false;
      txtPromo.Enabled = false;
      chbHold.Enabled = false;
      txtHold.Enabled = false;
      txtNumberOfDays.Enabled = false;

      grpCreditCard.Visible = LoginsAndProfiles.ValidateUserFunctionalityAccess(_currentUserPermissions, LoginsAndProfiles.DISPLAY_CREDIT_CARD_NUMBERS);

      txtCardNo.Enabled = false;
      txtCCExpDate.Enabled = false;
      txtApprovalNo.Enabled = false;
      btnCoverDeposit.Enabled = false;

      int docNumber;

      using (var tran = new Transactions(AppModuleInfo)) {
        docNumber = tran.GetNextDocumentNumber();
      }

      txtDocumentNumber.Text = docNumber.ToString(CultureInfo.InvariantCulture);

      txtDocumentNumber.Enabled = false;
      txtGradeNumber.Text = "";
      txtGradeNumber.Enabled = false;
      txtEnteredBy.Text = Environment.UserName;
      txtEnteredBy.Enabled = false;

      grpAdjustments.Visible = false;
      txtDailyFigureDate.Text = ServerDateTime.ToShortDateString();
      chbAdjustCustomersFigure.Checked = true;
    }

    private static string BuildCcRefundTranDescription(String cCNumber, String coverageDocNumber) {
      var desc = "Credit Card Refund for #";
      desc += cCNumber.Substring(0, 4);
      desc += "********";
      var i = cCNumber.Length;
      desc += cCNumber.Substring(i - 4, 4);
      desc += " Doc #";
      desc += coverageDocNumber;

      desc += " Customer Withdrawal";

      return desc;
    }

    private void AddNewTransaction(int documentNumber, double amount, String enteredAmount, String description, String coverageDocumentNumber, String cCNumber, String cCExpires, Boolean getPaymentByType, String paymentBy) {
      String gradeNum = null;
      String cCApproval = null;
      String holdAmount = null;
      String holdPercent = null;
      String releaseDate = null;
      String reference = null;
      String dailyFigureDate = null;
      String freePlayRefund = null;
      String feesRefund = null;
      String promoRefund = null;


      var itemsToCuLog = new List<LogWriter.CuAccessLogInfo>();

      var tranDateTime = DateTime.Parse(txtTranDateTime.Text);
      var tranType = GetTranTypeCodeFromUserSelection();
      var tranCode = getTranCodeFromTranType(tranType);
      amount *= 100;

      if (getPaymentByType) {
        if (panTranMethod.Controls.Count > 0) {
          var cbx = (ComboBox)panTranMethod.Controls[0];
          if (cbx.SelectedIndex >= 0) {
            paymentBy = cbx.SelectedItem.ToString();
          }
        }
      }
      else {
        if (paymentBy.Length == 0 || paymentBy == "") {
          paymentBy = null;
        }
      }


      if (txtReference.Text.Length > 0) {
        reference = txtReference.Text;
      }

      var enteredBy = txtEnteredBy.Text;

      if (txtCardNo.Text.Length > 0) {
        cCNumber = txtCardNo.Text;
      }

      if (txtCCExpDate.Text.Length > 0) {
        cCExpires = txtCCExpDate.Text;
      }

      if (txtApprovalNo.Text.Length > 0) {
        cCApproval = txtApprovalNo.Text;
      }

      if (txtHold.Text.Length > 0) {
        holdPercent = txtHold.Text;
        holdAmount = (double.Parse(txtHold.Text) * double.Parse(enteredAmount)).ToString(CultureInfo.InvariantCulture);
        releaseDate = tranDateTime.AddDays(int.Parse(txtNumberOfDays.Text)).ToShortDateString();

      }

      if (enteredAmount != null)
        enteredAmount = (double.Parse(enteredAmount) * 100).ToString(CultureInfo.InvariantCulture);

      if ((tranType == "C" || tranType == "D") && chbAdjustCustomersFigure.Checked) {
        dailyFigureDate = DateTime.Parse(txtDailyFigureDate.Text).ToShortDateString();
      }

      var currencyCode = _currentCustomerCurrency.Split(' ')[0];
      var valueDate = tranDateTime.ToShortDateString();

      var casinoAdjustmentFlag = chbCasinoAdjustment.Checked ? "Y" : "N";

      var deletePayToRef = "N";

      if (tranType == "E" || tranType == "I")
        deletePayToRef = "Y";

      var logInfo = new LogWriter.CuAccessLogInfo {
        ProgramName = AppModuleInfo.Description,
        Operation = "Changed " + _currentCustomerId,
        Data = "Transaction Entered - "
      };
      logInfo.Data += tranCode == "C" ? "Credit for " : "Debit for ";
      logInfo.Data += FormatNumber(amount, true, 100, true);
      itemsToCuLog.Add(logInfo);

      if (txtFreePlay.Text.Length > 0 && double.Parse(txtFreePlay.Text) > 0) {
        freePlayRefund = (double.Parse(txtFreePlay.Text) * 100).ToString(CultureInfo.InvariantCulture);

        logInfo = new LogWriter.CuAccessLogInfo {
          ProgramName = AppModuleInfo.Description,
          Operation = "Changed " + _currentCustomerId,
          Data = "Free Play Entered - "
        };
        logInfo.Data += tranCode == "C" ? "Credit for " : "Debit for ";
        logInfo.Data += FormatNumber(double.Parse(txtFreePlay.Text), false, 1, true);
        itemsToCuLog.Add(logInfo);
      }

      if (txtFees.Text.Length > 0 && double.Parse(txtFees.Text) > 0) {
        feesRefund = (double.Parse(txtFees.Text) * 100).ToString(CultureInfo.InvariantCulture);

        logInfo = new LogWriter.CuAccessLogInfo {
          ProgramName = AppModuleInfo.Description,
          Operation = "Changed " + _currentCustomerId,
          Data = "Transaction Entered - "
        };
        logInfo.Data += tranCode == "C" ? "Fees Credit for " : "Fees Debit for ";
        logInfo.Data += FormatNumber(double.Parse(txtFees.Text), false, 1, true);
        itemsToCuLog.Add(logInfo);
      }

      if (txtPromo.Text.Length > 0 && double.Parse(txtPromo.Text) > 0) {
        promoRefund = (double.Parse(txtPromo.Text) * 100).ToString(CultureInfo.InvariantCulture);

        logInfo = new LogWriter.CuAccessLogInfo {
          ProgramName = AppModuleInfo.Description,
          Operation = "Changed " + _currentCustomerId,
          Data = "Transaction Entered - "
        };
        logInfo.Data += tranCode == "C" ? "Promo Credit for " : "Promo Debit for ";
        logInfo.Data += FormatNumber(double.Parse(txtPromo.Text), false, 1, true);
        itemsToCuLog.Add(logInfo);
      }

      try {

        using (var tran = new Transactions(AppModuleInfo)) {
          tran.InsertCustomerNewTransaction(documentNumber, _currentCustomerId, tranDateTime, tranCode, tranType, amount, paymentBy, description, enteredBy,
              gradeNum, cCNumber, cCExpires, cCApproval, enteredAmount, holdAmount, holdPercent, releaseDate, coverageDocumentNumber, reference, dailyFigureDate, _currentCustomerAgentId, currencyCode, valueDate, casinoAdjustmentFlag, deletePayToRef, freePlayRefund, feesRefund, promoRefund);
        }

        using (var lw = new LogWriter(AppModuleInfo)) {
          lw.WriteToCuAccessLog(itemsToCuLog);
        }

        RefreshCustomerBalances(amount / 100, tranCode, freePlayRefund, feesRefund, promoRefund);

      }
      catch (Exception ex) {
        Log(ex);
      }


    }

    private string getTranCodeFromTranType(string tranType) {
      var tranCode = "";

      switch (tranType) {
        case "B": //Promotional Credit
        case "C": //Credit Adjustment
        case "E": //Customer Deposit
        case "F": //Fees Credit
        case "T": //Transfer Credit
          tranCode = "C";
          break;

        case "D": //Debit Adjustment
        case "H": //Fees Debit
        case "I": //Customer Withdrawal
        case "N": //Promotional Debit
        case "U": //Transfer Debit
          tranCode = "D";
          break;

      }

      return tranCode;
    }

    private string GetTranTypeCodeFromUserSelection() {
      var tranType = "";

      if (panTranType.Controls.Count > 0) {
        var cbx = (ComboBox)panTranType.Controls[0];
        var tranName = cbx.SelectedItem.ToString();

        switch (tranName) {
          case "Credit Adjustment":
            tranType = "C";
            break;
          case "Debit Adjustment":
            tranType = "D";
            break;
          case "Deposit":
            tranType = "E";
            break;
          case "Fees Credit":
            tranType = "F";
            break;
          case "Fees Debit":
            tranType = "H";
            break;
          case "Promotional Credit":
            tranType = "B";
            break;
          case "Promotional Debit":
            tranType = "N";
            break;
          case "Transfer Credit":
            tranType = "T";
            break;
          case "Transfer Debit":
            tranType = "U";
            break;
          case "Withdrawal":
            tranType = "I";
            break;
        }
      }

      return tranType;
    }

    private CustomerInfoUpdater.CustomerTransaction LoadCustomerTransactionInfoToStructFromFrm() {

      var dfDate = txtDailyFigureDate.Text;
      if (txtDailyFigureDate.Text.Trim() == "") {
        dfDate = "01/01/1900";
      }

      var holdFundsReleaseDate = "";
      if (txtNumberOfDays.Text.Length > 0) {
        if (int.Parse(txtNumberOfDays.Text) > 0) {
          var tranDateTime = DateTime.Parse(txtTranDateTime.Text);
          holdFundsReleaseDate = tranDateTime.AddDays(int.Parse(txtNumberOfDays.Text)).ToShortDateString();
        }
      }
      else {
        holdFundsReleaseDate = "01/01/1900";
      }

      int documentNumber;
      int.TryParse(txtDocumentNumber.Text, out documentNumber);

      var transactionInfo = new CustomerInfoUpdater.CustomerTransaction(
          documentNumber,
          "",
          lblTranAmount.Text,
          txtEnteredAmount.Text,
          txtTranDescription.Text,
          txtReference.Text,
          txtHold.Text,
          holdFundsReleaseDate,
          txtCardNo.Text,
          txtCCExpDate.Text,
          txtApprovalNo.Text,
          dfDate,
          chbCasinoAdjustment.Checked ? "Y" : "N",
          _paymentBy
          );

      return transactionInfo;
    }

    private void RefreshCustomerBalances(double transacAmount, String transacCode, String freePlayRefund, String feesRefund, String promoRefund) {
      if (_weeklyLimitsEnabled == "N") {
        var txtcustAvailableBalance = (NumberTextBox)_parentFrm.Controls.Find("txtAvailableBalance", true).FirstOrDefault();

        if (txtcustAvailableBalance != null) {
          var custAvailableBalance = txtcustAvailableBalance.Text;

          var newAvailableBalanceToTextBox = double.Parse(custAvailableBalance);

          switch (transacCode) {
            case "C":
              newAvailableBalanceToTextBox += transacAmount;
              break;
            case "D":
              newAvailableBalanceToTextBox += (-1) * transacAmount;
              break;
          }


          if (feesRefund != null) {
            newAvailableBalanceToTextBox += double.Parse(feesRefund) / 100;
          }

          if (promoRefund != null) {
            newAvailableBalanceToTextBox += double.Parse(promoRefund) / 100;
          }

          txtcustAvailableBalance.Text = (newAvailableBalanceToTextBox * txtcustAvailableBalance.DividedBy).ToString(CultureInfo.InvariantCulture);
        }
      }

      if (freePlayRefund != null) {
        _parentFrm.CustomerFreePlayBalance += double.Parse(freePlayRefund);

        var txtcusFreePlaytAvailableBalance = (NumberTextBox)_parentFrm.Controls.Find("txtFreePlayBalance", true).FirstOrDefault();

        var custFreePlayAvailableBalance = txtcusFreePlaytAvailableBalance.Text;

        var newFreePlayAvailableBalance = double.Parse(custFreePlayAvailableBalance) + double.Parse(freePlayRefund) / 100;

        txtcusFreePlaytAvailableBalance.Text = (newFreePlayAvailableBalance * txtcusFreePlaytAvailableBalance.DividedBy).ToString(CultureInfo.InvariantCulture);

      }

      ReloadCustomerTransactionsGrid();
    }

    private void ReloadCustomerTransactionsGrid() {
      var cbx = (ComboBox)_parentFrm.Controls.Find("cmbDisplay", true).FirstOrDefault();
      if (cbx == null) return;
      var cbxIdx = cbx.SelectedIndex;
      if (cbxIdx > -1) {
        _parentFrm.PopulateCustomerTransactionsGv(cbxIdx);
      }

      cbx = (ComboBox)_parentFrm.Controls.Find("cmbFreePlayDisplay", true).FirstOrDefault();
      if (cbx != null) cbxIdx = cbx.SelectedIndex;
      if (cbxIdx > -1) {
        _parentFrm.PopulateCustomerFreePlaysTransactionsGv(cbxIdx);
      }
    }

    private bool CheckIfUpdateExistingPaymentInformation() {
      var updatePayToInfo = PayToInfo[0].PaymentInfoNameLast != PayToInfo[1].PaymentInfoNameLast ||
                             PayToInfo[0].PaymentInfoNameFirst != PayToInfo[1].PaymentInfoNameFirst ||
                             PayToInfo[0].PaymentInfoNameMi != PayToInfo[1].PaymentInfoNameMi ||
                             PayToInfo[0].PaymentInfoAddress != PayToInfo[1].PaymentInfoAddress ||
                             PayToInfo[0].PaymentInfoCity != PayToInfo[1].PaymentInfoCity ||
                             PayToInfo[0].PaymentInfoState != PayToInfo[1].PaymentInfoState ||
                             PayToInfo[0].PaymentInfoZip != PayToInfo[1].PaymentInfoZip ||
                             PayToInfo[0].PaymentInfoCountry != PayToInfo[1].PaymentInfoCountry ||
                             PayToInfo[0].PaymentInfoPhone != PayToInfo[1].PaymentInfoPhone;

      return updatePayToInfo;

    }

    private bool CheckIfInsertNewPaymentInformation() {
      var insertNewPayInfo = false;

      foreach (var pir in PayToInfo) {
        if (pir.PaymentInfoNameLast.Length > 0 || pir.PaymentInfoNameFirst.Length > 0 || pir.PaymentInfoNameMi.Length > 0 || pir.PaymentInfoAddress.Length > 0
        || pir.PaymentInfoCity.Length > 0
        || pir.PaymentInfoState.Length > 0
        || pir.PaymentInfoZip.Length > 0
        || pir.PaymentInfoCountry.Length > 0
        || pir.PaymentInfoPhone.Length > 0) {
          insertNewPayInfo = true;
        }
      }

      return insertNewPayInfo;
    }

    #endregion

    #region Public Methods

    #endregion

    #region Protected Methods

    #endregion

    #region Events

    private void btnCancel_Click(object sender, EventArgs e) {
      Close();
    }

    private void dtmChangeTranDailyFigure_ValueChanged(object sender, EventArgs e) {
      var dtp = (DateTimePicker)sender;

      txtDailyFigureDate.Text = dtp.Text;
    }

    private void frmCustomerTransactionDetails_Load(object sender, EventArgs e) {
      switch (_loadFormType) {
        case "New":
          PrepareFormToShowInAddNewMode();
          break;

        case "Edit":
          PrepareFormToShowInEditMode();
          if (_isArchivedTran) {
            lblArchivedTransaction.Visible = true;
            btnOk.Visible = false;
            btnCancel.Text = @"Ok";
            AcceptButton = btnCancel;
            txtEnteredAmount.ReadOnly = true;
            txtTranDescription.ReadOnly = true;
            txtReference.ReadOnly = true;
            chbHold.Enabled = false;
            txtCCExpDate.ReadOnly = true;
            txtCardNo.ReadOnly = true;
            txtApprovalNo.ReadOnly = true;
          }
          else {
            lblArchivedTransaction.Visible = false;
            btnOk.Visible = true;
          }
          break;
      }

      Text = _loadFormType + @" Transaction in " + _currentCustomerCurrency;
    }

    private void chbHold_CheckedChanged(object sender, EventArgs e) {
      if (((CheckBox)sender).Checked) {
        txtHold.Enabled = true;
        txtNumberOfDays.Enabled = true;
      }
      else {
        txtHold.Text = "";
        txtHold.Enabled = false;
        txtNumberOfDays.Text = "";
        txtNumberOfDays.Enabled = false;
        lblHoldAmount.Text = "";
        lblAmountToRelease.Text = "";
        lblTranAmount.Text = txtEnteredAmount.Text;

      }
    }

    private void btnPayto_Click(object sender, EventArgs e) {
      PayToInfo = new List<Transactions.CustomerPaymentInformationRecord>();

      var paymentInfoFrm = new FrmCustomerPaymentInformation(_currentCustomerId, _currentDocumentNumber, _isArchivedTran, this);

      paymentInfoFrm.ShowDialog();

    }

    private void btnCoverDeposit_Click(object sender, EventArgs e) {

      var methodCbx = new ComboBox();

      try {
        methodCbx = (ComboBox)panTranMethod.Controls[0];
      }

      catch (Exception ex) {
        MessageBox.Show(@"Unable to create Methods Combo Box: " + ex.Message);
      }

      if (methodCbx.SelectedIndex >= 0) {

        var coverDepositFrm = new FrmCustomerCoverCreditCardDeposit(_currentCustomerId, this, txtEnteredAmount.Text);
        coverDepositFrm.ShowDialog();
      }
      else {
        MessageBox.Show(@"Select a Payment Method before Covering Deposits");
      }
    }

    private void txtHold_TextChanged(object sender, EventArgs e) {
      lblHoldAmount.Text = "";
      lblAmountToRelease.Text = "";

      if (((TextBox)sender).Text.Length > 0) {
        if (int.Parse(((TextBox)sender).Text) > 0) {
          if (NumberF.IsValidNumber0To100(((TextBox)sender).Text, false)) {
            var holdPerc = double.Parse(((TextBox)sender).Text) / 100;
            CalculateHoldAmount(holdPerc);
          }
          else {
            MessageBox.Show(@"Invalid percentage number");
            txtHold.Focus();
          }
        }
        else {
          MessageBox.Show(@"Hold percent must fall between 1 and 100.");
          txtHold.Focus();
        }
      }

    }

    private void txtEnteredAmount_Leave(object sender, EventArgs e) {
      double amount;
      double.TryParse(((TextBox)sender).Text, out amount);
      ((TextBox)sender).Text = FormatNumber(amount, false, 1, true);
    }

    private void txtEnteredAmount_TextChanged(object sender, EventArgs e) {
      if (((TextBox)sender).Text.Length > 0) {
        if (!NumberF.IsValidAmount(((TextBox)sender).Text, false)) {
          MessageBox.Show(@"Invalid number");
          ((TextBox)sender).Focus();
          return;
        }
        lblTranAmount.Text = txtEnteredAmount.Text;

        if (chbHold.Checked && NumberF.IsValidNumber0To100(txtHold.Text, false)) {
          var holdPerc = double.Parse(txtHold.Text) / 100;
          CalculateHoldAmount(holdPerc);
        }
      }
    }

    private void tbnOk_Click(object sender, EventArgs e) {
      var addTran = true;
      var messageShown = false;

      if (txtEnteredAmount.Text.Length == 0) {
        MessageBox.Show(@"Please enter a transaction amount.");
        txtEnteredAmount.Focus();
        addTran = false;
        messageShown = true;
      }

      if (panTranType.Controls.Count > 0 && _loadFormType == "New") {
        var cbx = (ComboBox)panTranType.Controls[0];
        if (cbx.SelectedIndex < 0 && !messageShown) {
          MessageBox.Show(@"Please select a transaction type.");
          cbx.Focus();
          addTran = false;
          messageShown = true;

        }
      }

      if (txtFreePlay.Text.Length > 0 && !messageShown) {
        if (int.Parse(txtFreePlay.Text) == 0) {
          MessageBox.Show(@"Free Play refund amount must be greater than zero.");
          txtFreePlay.Focus();
          addTran = false;
          messageShown = true;
        }
      }

      if (txtFees.Text.Length > 0 && !messageShown) {
        if (int.Parse(txtFees.Text) == 0) {
          MessageBox.Show(@"Fee refund amount must be greater than zero.");
          txtFees.Focus();
          addTran = false;
          messageShown = true;
        }
      }

      if (txtPromo.Text.Length > 0 && !messageShown) {
        if (int.Parse(txtPromo.Text) == 0) {
          MessageBox.Show(@"Promo refund amount must be greater than zero.");
          txtPromo.Focus();
          addTran = false;
          messageShown = true;
        }
      }

      if (chbHold.Checked) {
        if ((txtHold.Text.Length == 0 || int.Parse(txtHold.Text) == 0) && !messageShown) {
          MessageBox.Show(@"Hold percent must fall between 1 and 100.");
          txtHold.Focus();
          addTran = false;
          messageShown = true;

        }

        if (txtNumberOfDays.Text.Length == 0 && !messageShown) {
          MessageBox.Show(@"Please enter the number of days to hold funds.");
          txtNumberOfDays.Focus();
          addTran = false;
          messageShown = true;

        }

      }

      if (chbAdjustCustomersFigure.Checked) {
        if (txtDailyFigureDate.Text.Length == 0 && !messageShown) {
          MessageBox.Show(@"Please enter date for Customer's Daily Figure Adjustment.");
          dtmChangeTranDailyFigure.Focus();
          addTran = false;
        }
      }

      var coverersCount = 0;
      if (DebitCoverers != null) {
        coverersCount = DebitCoverers.Count;
      }


      if (addTran) {
        if (_loadFormType == "New") {
          if (coverersCount > 0) {
            var messageToShow = "This process will generate the following transactions:";
            messageToShow += Environment.NewLine;

            if (DebitCoverers != null) {
              foreach (var dC in DebitCoverers) {
                messageToShow += Environment.NewLine;
                messageToShow += dC.TranMethod + " debit to " + dC.CcNumber + " for $" + dC.CoverAmount;
              }

              if (double.Parse(lblExcessAmt.Text) > 0) {
                messageToShow += Environment.NewLine;
                messageToShow += "Remaining debit for $" + lblExcessAmt.Text;
              }
              messageToShow += Environment.NewLine;
              messageToShow += Environment.NewLine;
              messageToShow += "Continue with this process and generate the transactions?";



              if (MessageBox.Show(messageToShow, @"Insert debit cover transactions", MessageBoxButtons.OKCancel) == DialogResult.OK) {

                foreach (var dC in DebitCoverers) {
                  int newDocNumber;
                  using (var tran = new Transactions(AppModuleInfo)) {
                    newDocNumber = tran.GetNextDocumentNumber();
                  }
                  AddNewTransaction(newDocNumber, double.Parse(dC.CoverAmount), null, BuildCcRefundTranDescription(dC.CcNumber, dC.DocumentNumber), dC.DocumentNumber, dC.CcNumber, dC.CcExpires, false, dC.TranMethod);
                }


                if (double.Parse(lblExcessAmt.Text) > 0) {
                  AddNewTransaction(int.Parse(txtDocumentNumber.Text), double.Parse(lblExcessAmt.Text), lblExcessAmt.Text, txtTranDescription.Text.Trim(), null, null, null, true, null);
                }

                Close();
              }
            }
          }
          else {
            AddNewTransaction(int.Parse(txtDocumentNumber.Text), double.Parse(lblTranAmount.Text), txtEnteredAmount.Text, txtTranDescription.Text.Trim(), null, null, null, true, null);
            Close();
          }
        }
        else {
          _customerInfo.CompareCustomerTransactionInfoToUpdate(LoadCustomerTransactionInfoToStructFromFrm(), _parentFrm.GetCurrentDateTime());
          if (_customerInfo.AmountToAdjustBalance != 0)
            RefreshCustomerBalances(_customerInfo.AmountToAdjustBalance / 100, _customerInfo.TransacCode, null, null, null);
          Close();
          Dispose();
        }

        if (PayToInfo != null) {

          if (PayToInfo.Count == 0) {
            MessageBox.Show(@"I made something wrong");
          }

          if (PayToInfo.Count == 1) {
            if (CheckIfInsertNewPaymentInformation()) {
              using (var tran = new Transactions(AppModuleInfo)) {
                tran.InsertNewPaymentInformation(PayToInfo, _currentDocumentNumber);
              }
              Close();
              Dispose();
            }
          }

          if (PayToInfo.Count == 2) {
            if (CheckIfUpdateExistingPaymentInformation()) {
              using (var tran = new Transactions(AppModuleInfo)) {
                tran.UpdatePaymentInformation(PayToInfo, _currentDocumentNumber);
              }
              Close();
              Dispose();
            }
          }
        }
        ReloadCustomerTransactionsGrid();
      }
    }

    private void chbAdjustCustomersFigure_CheckedChanged(object sender, EventArgs e) {
      var chB = (CheckBox)sender;

      txtDailyFigureDate.Text = chB.Checked ? ServerDateTime.ToShortDateString() : "";
    }

    private void chbCasinoAdjustment_CheckedChanged(object sender, EventArgs e) {
      var chB = (CheckBox)sender;

      var tranType = "";

      if (panTranType.Controls.Count > 0) {
        var cmB = (ComboBox)panTranType.Controls[0];
        tranType = cmB.SelectedItem.ToString();
      }

      if (chB.Checked) {
        txtTranDescription.Text = tranType + @" - Casino";
      }
      else {
        txtTranDescription.Text = tranType;
      }
    }

    private void numberAmount_KeyPress(object sender, KeyPressEventArgs e) {
      int ascii = Convert.ToInt16(e.KeyChar);
      if ((ascii >= 48 && ascii <= 57) || (ascii == 8) || ascii == 46) {
        e.Handled = false;
      }
      else {
        e.Handled = true;
      }
    }

    private void txtTranDescription_Enter(object sender, EventArgs e) {
      if (!_tranFinalSpacesAdded && _loadFormType == "New") {
        var description = ((TextBox)sender).Text;
        description += "  ";
        ((TextBox)sender).Text = description;
        _tranFinalSpacesAdded = true;
      }

      ((TextBox)sender).Select(((TextBox)sender).Text.Length, 0);
    }

    private void paymentByCbx_SelectedIndexChanged(object sender, EventArgs e) {
      _paymentBy = ((ComboBox)sender).SelectedItem.ToString();
    }

    #endregion

    #region Structs

    public struct DebitCoverer {
      public string DocumentNumber;
      public string TranMethod;
      public string CcNumber;
      public string CcExpires;
      public string OriginalAmount;
      public string CoverAmount;

      public DebitCoverer(string documentNumber, string tranMethod, string ccNumber, string ccExpires, string originalAmount, string coverAmount) {
        DocumentNumber = documentNumber;
        TranMethod = tranMethod;
        CcNumber = ccNumber;
        CcExpires = ccExpires;
        OriginalAmount = originalAmount;
        CoverAmount = coverAmount;
      }
    }

    #endregion
  }
}