﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using SIDLibraries.BusinessLayer;
using GUILibraries.Controls;
using SIDLibraries.Utilities;
using GUILibraries.Forms;

namespace CustomerMaintenance.UI {
  public partial class FrmCustomerNewFreePlay : SIDForm {
    private readonly String _currentCustomerId;
    private readonly String _customerCurrency;
    private readonly String _currentAgentId;

    private readonly FrmCustomerMaintenance _callerForm;

    public FrmCustomerNewFreePlay(SIDForm callerFrm, String customerId, String currency, String agentId)
      : base(callerFrm.AppModuleInfo) {
      _callerForm = (FrmCustomerMaintenance)callerFrm;
      _currentCustomerId = customerId;
      _customerCurrency = currency;
      _currentAgentId = agentId;
      InitializeComponent();
    }

    private void btnCancel_Click(object sender, EventArgs e) {
      Close();
    }

    private void frmCustomerNewFreePlay_Load(object sender, EventArgs e) {
      Text += @" in " + _customerCurrency;
      txtTranDateTime.Text = DisplayDate.Format(ServerDateTime, DisplayDateFormat.LongDateTime);
    }

    private void numberToText_Leave(object sender, EventArgs e) {
      var ntxtB = (NumberTextBox)sender;

      if (ntxtB.Text != "") {

        ((TextBox)sender).Text = FormatNumber(DisplayNumber.RemoveFormat(((TextBox)sender).Text) * ntxtB.DividedBy, ntxtB.DivideFlag, ntxtB.DividedBy, ntxtB.ShowIfZero);
      }
    }

    private void cmbTransactionType_SelectedIndexChanged(object sender, EventArgs e) {
      txtDescription.Text = @"Customer " + cmbTransactionType.SelectedItem + @" ** Free Play **";
    }

    private void btnOk_Click(object sender, EventArgs e) {
      var messageShown = false;

      if (cmbTransactionType.SelectedIndex == -1) {
        MessageBox.Show(@"Please select a free play type.", @"invalid transaction type");
        cmbTransactionType.Focus();
        messageShown = true;
      }

      if (txtAmount.Text.Length > 0 && cmbTransactionType.SelectedIndex > -1) {
        try {
          using (var fp = new FreePlays(AppModuleInfo)) {
            fp.InsertNewFpTransaction(_currentCustomerId, GetTranTypeCode(cmbTransactionType.SelectedItem.ToString()), double.Parse(txtAmount.Text) * 100, txtDescription.Text, _currentAgentId, _customerCurrency.Split(' ')[0]);

            using (var log = new LogWriter(AppModuleInfo)) {
              log.WriteToCuAccessLog(AppModuleInfo.Description, "Changed " + _currentCustomerId, "Free Play Transaction Entered - " + cmbTransactionType.SelectedItem + " " + FormatNumber(double.Parse(txtAmount.Text), false, 1, true));
            }

            var frm = _callerForm;

            var nTxb = (NumberTextBox)frm.Controls.Find("txtFreePlayBalance", true).FirstOrDefault();

            if (nTxb != null) {
              var availableBalance = double.Parse(nTxb.Text);

              if (GetTranTypeCode(cmbTransactionType.SelectedItem.ToString()) == "E") {
                nTxb.Text = ((availableBalance + double.Parse(txtAmount.Text)) * nTxb.DividedBy).ToString(CultureInfo.InvariantCulture);
                frm.CustomerFreePlayBalance += double.Parse(txtAmount.Text) * 100;
              }
              else {
                nTxb.Text = ((availableBalance - double.Parse(txtAmount.Text)) * nTxb.DividedBy).ToString(CultureInfo.InvariantCulture);
                frm.CustomerFreePlayBalance -= double.Parse(txtAmount.Text) * 100;
              }
            }

            var cBx = (ComboBox)frm.Controls.Find("cmbFreePlayDisplay", true).FirstOrDefault();

            if (cBx != null && cBx.SelectedIndex > -1) {
              frm.PopulateCustomerFreePlaysTransactionsGv(cBx.SelectedIndex);
            }
            Close();
          }
        }

        catch (Exception ex) {
          Log(ex);
        }
      }
      else {
        if (!messageShown) {
          MessageBox.Show(@"Please enter a transaction amount.", @"invalid amount");
          txtAmount.Focus();
        }
      }
    }

    private static string GetTranTypeCode(String p) {
      var tranType = "";

      switch (p) {
        case "Deposit":
          tranType = "E";
          break;
        case "Withdrawal":
          tranType = "I";
          break;
      }


      return tranType;
    }
  }
}