﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using SIDLibraries.BusinessLayer;
using CustomerMaintenance.Libraries;
using SIDLibraries.Utilities;
using GUILibraries.Controls;
using GUILibraries.Forms;
using SIDLibraries.Entities;

namespace CustomerMaintenance.UI {
  public partial class FrmSendPackageToCustomers : SIDForm {

    #region public Classes

    private class WriteToLogElement {
      public readonly String Item;
      public readonly String CuData;

      public WriteToLogElement(String item, String cUData) {
        Item = item;
        CuData = cUData;
      }

    }

    private class PackageOptions : IDisposable {
      public int AllTeasers { get; private set; }
      public int BaseballActionListed { get; private set; }
      public int BetServiceProfiles { get; private set; }
      public int CallUnitMinBet { get; private set; }
      public int CasinoSuspendWagering { get; private set; }
      public int ChartPercentBook { get; private set; }
      public int CostToBuyPoints { get; private set; }
      public int CreditAccountingFlag { get; private set; }
      public int Currency { get; private set; }
      public int DetailHorseLimits { get; private set; }
      public int DetailLimits { get; private set; }
      public int EnableRollingIfBets { get; private set; }
      public int EnforceAccumGameLimits { get; private set; }
      public int FreeHalfPoints { get; private set; }
      public int HorseMaxPayout { get; private set; }
      public int HorseTrackRestrictions { get; private set; }
      public int InetTarget { get; private set; }
      public int InternetMinBet { get; private set; }
      public int LimitRifToRisk { get; private set; }
      public int MaxContestBet { get; private set; }
      public int MaxTeaserBet { get; private set; }
      public int ParlayMaxBet { get; private set; }
      public int ParlayMaxPayout { get; private set; }
      public int ParlaySchedule { get; private set; }
      public int PriceType { get; private set; }
      public int PuckCanadianLineSetting { get; private set; }
      public int SourceInformation { get; private set; }
      public int SportsbookSuspendWagering { get; private set; }
      public int StaticLinesSetting { get; private set; }
      public int Store { get; private set; }
      public int TimeZone { get; private set; }
      public int VigDiscounts { get; private set; }
      public int WagerLimitOfferingPage { get; private set; }
      public int ZeroBalanceFlag { get; private set; }
      public int CreditLimit { get; private set; }
      public int RestrictionsFlag { get; private set; }
      public int DisabledSports { get; private set; }
      public int ShadeGroups { get; private set; }

      public List<LogWriter.CuAccessLogInfo> ItemsToCuLog { get; private set; }

      private List<String> TargetOptions { get; set; }
      private List<String> SportTypeAndSubType { get; set; }
      private List<spCstGetActions_Result> CustomerRestrictionItems { get; set; }
      private String ModuleDescription { get; set; }
      private List<WriteToLogElement> OptionsToCuData { get; set; }

      public PackageOptions(List<String> targetOptions, List<String> sportTypeAndSubType, String moduleDescription, List<WriteToLogElement> optionsToCuData, List<spCstGetActions_Result> customerRestrictionItems) {
        TargetOptions = targetOptions;
        SportTypeAndSubType = sportTypeAndSubType;
        CustomerRestrictionItems = customerRestrictionItems;
        ModuleDescription = moduleDescription;
        OptionsToCuData = optionsToCuData;
        FlagTargetOptions();
      }

      public void WriteCallUnitAccessLogData(String sourceCustomerId, String targetCustomerId) {
        if (TargetOptions.Count == 0) {
          return;
        }

        LogWriter.CuAccessLogInfo logInfo;

        ItemsToCuLog = new List<LogWriter.CuAccessLogInfo>();

        String option;

        foreach (var to in TargetOptions) {
          option = to;
          if (option != "chbDetailLimits" || SportTypeAndSubType == null || SportTypeAndSubType.Count <= 0)
            continue;
          foreach (var str in SportTypeAndSubType) {
            logInfo = new LogWriter.CuAccessLogInfo { ProgramName = ModuleDescription };
            if (targetCustomerId == "") {
              logInfo.Operation = "Changed " + sourceCustomerId;
            }
            else {
              logInfo.Operation = sourceCustomerId + " Changed " + targetCustomerId;
            }
            logInfo.Data = "Send Package - ";
            logInfo.Data += GetCuDataInfo(option);
            logInfo.Data += " / " + str;

            ItemsToCuLog.Add(logInfo);
          }
        }

        foreach (var to in TargetOptions) {
          logInfo = new LogWriter.CuAccessLogInfo();

          option = to;
          if (option == "chbDetailLimits") continue;
          logInfo.ProgramName = ModuleDescription;
          if (targetCustomerId == "") {
            logInfo.Operation = "Changed " + sourceCustomerId;
          }
          else {
            logInfo.Operation = sourceCustomerId + " Changed " + targetCustomerId;
          }
          logInfo.Data = "Send Package - ";
          logInfo.Data += GetCuDataInfo(option);

          ItemsToCuLog.Add(logInfo);
        }
      }

      private void FlagTargetOptions() {
        if (TargetOptions.Count == 0) {
          return;
        }

        foreach (var to in TargetOptions) {
          var option = to;

          if (option == "chbAllTeasers") {
            AllTeasers = 1;
          }

          if (option == "chbBaseballActionListed") {
            BaseballActionListed = 1;
          }

          if (option == "chbBetServiceProfiles") {
            BetServiceProfiles = 1;
          }

          if (option == "chbCallUnitMinBet") {
            CallUnitMinBet = 1;
          }

          if (option == "chbCasinoSuspendWagering") {
            CasinoSuspendWagering = 1;
          }

          if (option == "chbChartPercentBook") {
            ChartPercentBook = 1;
          }

          if (option == "chbCostToBuyPoints") {
            CostToBuyPoints = 1;
          }

          if (option == "chbCreditAccountingFlag") {
            CreditAccountingFlag = 1;
          }

          if (option == "chbCurrency") {
            Currency = 1;
          }

          if (option == "chbDetailHorseLimits") {
            DetailHorseLimits = 1;
          }

          if (option == "chbDetailLimits") {
            DetailLimits = 1;
          }

          if (option == "chbEnableRollingIfBets") {
            EnableRollingIfBets = 1;
          }

          if (option == "chbEnforceAccumGameLimits") {
            EnforceAccumGameLimits = 1;
          }

          if (option == "chbFreeHalfPoints") {
            FreeHalfPoints = 1;
          }

          if (option == "chbHorseMaxPayout") {
            HorseMaxPayout = 1;
          }

          if (option == "chbHorseTrackRestrictions") {
            HorseTrackRestrictions = 1;
          }

          if (option == "chbInetTarget") {
            InetTarget = 1;
          }

          if (option == "chbInternetMinBet") {
            InternetMinBet = 1;
          }

          if (option == "chbLimitRIFToRisk") {
            LimitRifToRisk = 1;
          }

          if (option == "chbMaxContestBet") {
            MaxContestBet = 1;
          }

          if (option == "chbMaxTeaserBet") {
            MaxTeaserBet = 1;
          }

          if (option == "chbParlayMaxBet") {
            ParlayMaxBet = 1;
          }

          if (option == "chbCreditLimit") {
            CreditLimit = 1;
          }

          if (option == "chbParlayMaxPayout") {
            ParlayMaxPayout = 1;
          }

          if (option == "chbParlaySchedule") {
            ParlaySchedule = 1;
          }

          if (option == "chbPriceType") {
            PriceType = 1;
          }

          if (option == "chbPuckCanadianLineSetting") {
            PuckCanadianLineSetting = 1;
          }

          if (option == "chbSourceInformation") {
            SourceInformation = 1;
          }

          if (option == "chbSportsbookSuspendWagering") {
            SportsbookSuspendWagering = 1;
          }

          if (option == "chbStaticLinesSetting") {
            StaticLinesSetting = 1;
          }

          if (option == "chbStore") {
            Store = 1;
          }

          if (option == "chbTimeZone") {
            TimeZone = 1;
          }


          if (option == "chbVigDiscounts") {
            VigDiscounts = 1;
          }

          if (option == "chbWagerLimitOfferingPage") {
            WagerLimitOfferingPage = 1;
          }

          if (option == "chbZeroBalanceFlag") {
            ZeroBalanceFlag = 1;
          }

          if (option == "chbRestrictions") {
            RestrictionsFlag = 1;
          }

          if (option == "chbDisabledSports") {
            DisabledSports = 1;
          }

          if (option == "chbShadeGroups") {
            ShadeGroups = 1;
          }
        }
      }

      private string GetCuDataInfo(String chbDetailLimits) {
        var retData = "";

        foreach (var elem in OptionsToCuData) {
          if (elem.Item.Trim() == chbDetailLimits.Trim()) {
            retData = elem.CuData;
            break;
          }
        }

        return retData;
      }

      public void Dispose() {
        ItemsToCuLog = null;
        TargetOptions = null;
        OptionsToCuData = null;
      }

      ~PackageOptions() {
        try {
          Dispose();
        }
        catch (Exception) {
        }
      }
    }

    #endregion

    #region Private Vars

    private List<String> _checkedOptions;
    private List<WriteToLogElement> _optionsToCuData;
    private List<String> _sportAndSubSportItems;
    private List<spCstGetActions_Result> _customerRestrictionItems;
    private readonly String _currentCustomerId;
    private readonly String _currentAgentType;
    private ComboBox _sportscBx;
    private ComboBox _restrictionscBx;

    #endregion

    #region Public Properties

    #endregion

    #region Private Properties

    #endregion

    #region Structures

    #endregion

    #region Constructors

    public FrmSendPackageToCustomers(SIDForm callerfrm)
      : base(callerfrm.AppModuleInfo) {
      _currentCustomerId = ((FrmCustomerMaintenance)callerfrm).CustomerId;
      _currentAgentType = ((FrmCustomerMaintenance)callerfrm).AgentTypeFromFrm;
      InitializeComponent();
    }

    #endregion

    #region Public Methods

    #endregion

    #region Private Methods

    private void BuildItemsDescriptionToCuData() {
      _optionsToCuData = new List<WriteToLogElement>();

      var cuElement = new WriteToLogElement("chbAllTeasers", "Teasers");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbBaseballActionListed", "Baseball Action Type");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbBetServiceProfiles", "BetService profiles");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbCallUnitMinBet", "Call Unit Minimum Bet");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbCasinoSuspendWagering", "Casino Suspend Wagering Setting");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbChartPercentBook", "Percent Book");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbCostToBuyPoints", "Cost to Buy Points");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbCreditAccountingFlag", "Credit Acct Flag");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbCurrency", "Currency");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbDetailHorseLimits", "Horse Detail Limits");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbDetailLimits", "Detail Wager Limits");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbEnableRollingIfBets", "Enable Rolling If Setting");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbEnforceAccumGameLimits", "Enforce Accum Wager Limits");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbFreeHalfPoints", "Free half point settings");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbHorseMaxPayout", "Horse Max Payout");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbHorseTrackRestrictions", "Horse Track Restrictions");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbInetTarget", "Inet Target");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbInternetMinBet", "Internet Minimum Bet");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbLimitRIFToRisk", "Limit Rif to Risk Setting");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbMaxContestBet", "Contest Max Bet");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbMaxTeaserBet", "Teaser Max Bet");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbParlayMaxBet", "Parlay Max Bet");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbCreditLimit", "Credit Limit");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbParlayMaxPayout", "Parlay Max Payout");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbParlaySchedule", "Parlay");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbPriceType", "Price Type");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbPuckCanadianLineSetting", "Puck Line Setting");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbSourceInformation", "Source Information");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbSportsbookSuspendWagering", "Suspend Wagering Setting");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbStaticLinesSetting", "Static Lines Setting");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbStore", "Store");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbTimeZone", "Time Zone Setting");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbVigDiscounts", "Vig Discounts");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbWagerLimitOfferingPage", "Wager Limit");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbZeroBalanceFlag", "Zero Balance Setting");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbRestrictionsFlag", "Restrictions");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbDisabledSports", "Disabled Sports");
      _optionsToCuData.Add(cuElement);

      cuElement = new WriteToLogElement("chbShadeGroups", "Shade Groups");
      _optionsToCuData.Add(cuElement);

    }

    private void GetCheckedOptions() {
      _checkedOptions = new List<string>();

      var cnt = panPackageOptions.Controls.Count;


      for (var i = 0; i < cnt; i++) {
        var ctrl = panPackageOptions.Controls[i];

        if (ctrl.GetType().ToString() != "System.Windows.Forms.CheckBox") continue;
        if (!((CheckBox)ctrl).Checked) continue;
        var checkedOption = ctrl.Name;
        _checkedOptions.Add(checkedOption);
      }
      if (chbDetailLimits.Checked)
        _checkedOptions.Add(chbDetailLimits.Name);
      if (chbRestrictions.Checked)
        _checkedOptions.Add(chbRestrictions.Name);
    }

    private List<String> GetSelectedSports() {
      var selectedSports = new List<String>();

      if (chlbSports.CheckedItems.Count > 0) {
        selectedSports.AddRange(from object it in chlbSports.CheckedItems select it.ToString());
      }
      else
        selectedSports = null;

      return selectedSports;
    }

    private List<spCstGetActions_Result> GetSelectedRestrictions() {
      var selectedRestrictions = new List<spCstGetActions_Result>();

      if (chlbRestrictions.CheckedItems.Count > 0) {
        selectedRestrictions.AddRange(from object it in chlbRestrictions.CheckedItems select (spCstGetActions_Result)it);
      }
      else
        selectedRestrictions = null;

      return selectedRestrictions;
    }

    private void GetRestrictions() {
      var cbObj = new CustomerComboBox(AppModuleInfo);
      if (_restrictionscBx == null) {
        _restrictionscBx = cbObj.CustomerRestrictions(_currentCustomerId);
      }

      foreach (var obj in _restrictionscBx.Items) {
        chlbRestrictions.Items.Add(obj);
      }
    }

    private void GetSportTypes() {
      var cbObj = new CustomerComboBox(AppModuleInfo);
      if (_sportscBx == null) {
        _sportscBx = cbObj.SportTypes();
      }

      foreach (var obj in _sportscBx.Items) {
        chlbSports.Items.Add(obj);
      }
    }

    private void ShowInSelectedSports(string item, CheckState n) {
      if (item == null || n == CheckState.Indeterminate)
        return;
      if (n == CheckState.Checked) {
        var list = new List<string>();
        foreach (string listItem in lstSelectedSports.Items) {
          list.Add(listItem);
        }
        list.Add(item);
        list.Sort();
        lstSelectedSports.Items.Clear();

        foreach (var l in list) {
          lstSelectedSports.Items.Add(l);
        }
      }
      else {
        var stageList = new List<string>();
        foreach (var r in lstSelectedSports.Items) {
          if (((string)r).Trim() == item.Trim())
            continue;
          stageList.Add((string)r);
        }
        lstSelectedSports.Items.Clear();

        foreach (var l in stageList) {
          lstSelectedSports.Items.Add(l);
        }
      }
    }

    private void ShowInSelectedRestrictions(spCstGetActions_Result item, CheckState n) {
      if (item == null || n == CheckState.Indeterminate)
        return;
      if (n == CheckState.Checked) {
        var list = new List<spCstGetActions_Result>();
        foreach (spCstGetActions_Result listItem in lstSelectedRestrictions.Items) {
          list.Add(listItem);
        }
        list.Add(item);
        list = list.OrderBy(i => i.Name).ToList();
        lstSelectedRestrictions.Items.Clear();

        foreach (var l in list) {
          lstSelectedRestrictions.Items.Add(l);
        }
      }
      else {
        var stageList = new List<spCstGetActions_Result>();
        foreach (var r in lstSelectedRestrictions.Items) {
          if (((spCstGetActions_Result)r).ActionId == item.ActionId)
            continue;
          stageList.Add((spCstGetActions_Result)r);
        }
        lstSelectedRestrictions.Items.Clear();

        foreach (var l in stageList) {
          lstSelectedRestrictions.Items.Add(l);
        }
      }
    }

    private void HideShowSendPackageToAgentsCheckBox() {
      grpSendToAgents.Visible = _currentAgentType == "M";
    }

    private void SendPackageToCustomers() {
      if (_checkedOptions.Count <= 0) return;
      BuildItemsDescriptionToCuData();
      if (chbDetailLimits.Checked)
        _sportAndSubSportItems = GetSelectedSports();

      if (chbRestrictions.Checked)
        _customerRestrictionItems = GetSelectedRestrictions();

      using (var packageOptions = new PackageOptions(_checkedOptions, _sportAndSubSportItems, AppModuleInfo.Description, _optionsToCuData, _customerRestrictionItems)) {
        using (var agents = new Agents(AppModuleInfo)) {
          agents.Connect();
          if (_currentAgentType == "M") {
            SearchMasterAgentsFromMAgent(_currentCustomerId, packageOptions, agents, 4);
          }
          else {
            var userName = Environment.UserName.Trim();
            if (packageOptions.DetailLimits == 1 && _sportAndSubSportItems != null && _sportAndSubSportItems.Count > 0) {
              foreach (var spt in _sportAndSubSportItems) {
                var sportType = spt.Split('-')[0].Trim();
                var sportSubType = spt.Split('-')[1].Trim();
                agents.SendPackageToCustomers(_currentCustomerId, sportType, sportSubType, 0, 0,
                  0, 0, 0, 0, 0, 0,
                  0, 0, packageOptions.DetailLimits, 0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0, 0, 0, 0, null, 0, 0, userName);
              }
            }

            if (packageOptions.RestrictionsFlag == 1 && _customerRestrictionItems != null && _customerRestrictionItems.Count > 0) {
              foreach (var rst in _customerRestrictionItems) {
                agents.SendPackageToCustomers(_currentCustomerId, "", "", 0, 0,
                  0, 0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0, 0, 0, 1, rst.ActionId, 0, 0, userName);
              }
            }

            agents.SendPackageToCustomers(_currentCustomerId, "", "", packageOptions.AllTeasers, packageOptions.BaseballActionListed,
            packageOptions.BetServiceProfiles, packageOptions.CallUnitMinBet, packageOptions.CasinoSuspendWagering, packageOptions.ChartPercentBook, packageOptions.CostToBuyPoints, packageOptions.CreditAccountingFlag,
            packageOptions.Currency, packageOptions.DetailHorseLimits, 0, packageOptions.CreditLimit, packageOptions.EnableRollingIfBets, packageOptions.EnforceAccumGameLimits, packageOptions.FreeHalfPoints, packageOptions.HorseMaxPayout,
            packageOptions.HorseTrackRestrictions, packageOptions.InetTarget, packageOptions.InternetMinBet, packageOptions.LimitRifToRisk, packageOptions.MaxContestBet, packageOptions.MaxTeaserBet, packageOptions.ParlayMaxBet,
            packageOptions.ParlayMaxPayout, packageOptions.ParlaySchedule, packageOptions.PriceType, packageOptions.PuckCanadianLineSetting, packageOptions.SourceInformation,
            packageOptions.SportsbookSuspendWagering, packageOptions.StaticLinesSetting, packageOptions.Store, packageOptions.TimeZone, packageOptions.VigDiscounts, packageOptions.WagerLimitOfferingPage, packageOptions.ZeroBalanceFlag, 0, null, packageOptions.DisabledSports, packageOptions.ShadeGroups, userName);

            using (var lw = new LogWriter(AppModuleInfo)) {
              packageOptions.WriteCallUnitAccessLogData(_currentCustomerId, "");
              lw.WriteToCuAccessLog(packageOptions.ItemsToCuLog);
            }
          }
          agents.Disconnect();
        }
      }
    }

    private void SearchMasterAgentsFromMAgent(String masterAgentId, PackageOptions packageOptions, Agents agents, int maxNodesCount) {
      var mAgentsList = agents.GetMasterAgentsList(masterAgentId);

      SendMAgentPackageToCustomers(masterAgentId, packageOptions);

      if (mAgentsList.Count <= 0 || maxNodesCount <= 0) return;
      foreach (var mAgentId in mAgentsList) {
        if (chbAffectAgents.Visible && chbAffectAgents.Checked) {
          SendPackageToAgent(masterAgentId, mAgentId, packageOptions, agents);
        }
        SearchMasterAgentsFromMAgent(mAgentId, packageOptions, agents, maxNodesCount--);
      }
    }

    private void SendMAgentPackageToCustomers(string masterAgentId, PackageOptions packageOptions) {
      using (var agents = new Agents(AppModuleInfo)) {
        var agentsList = agents.GetAgentsForMasterAgent(masterAgentId);

        if (agentsList == null || agentsList.Count <= 0) return;
        foreach (var agentId in agentsList) {
          packageOptions.WriteCallUnitAccessLogData(masterAgentId.Trim(), agentId.AgentID.Trim());
          if (chbAffectAgents.Visible && chbAffectAgents.Checked) {
            SendPackageToAgent(masterAgentId.Trim(), agentId.AgentID.Trim(), packageOptions, agents);
          }
          var userName = Environment.UserName.Trim();
          if (packageOptions.DetailLimits == 1 && _sportAndSubSportItems != null && _sportAndSubSportItems.Count > 0) {
            foreach (var spt in _sportAndSubSportItems) {
              var sportType = spt.Split('-')[0].Trim();
              var sportSubType = spt.Split('-')[1].Trim();
              agents.SendMasterAgentPackageToCustomers(_currentCustomerId, agentId.AgentID, sportType, sportSubType, 0, 0,
                0, 0, 0, 0, 0, 0,
                0, 0, packageOptions.DetailLimits, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, null, 0, 0, userName);
            }
          }

          if (packageOptions.RestrictionsFlag == 1 && _customerRestrictionItems != null && _customerRestrictionItems.Count > 0) {
            foreach (var rst in _customerRestrictionItems) {
              agents.SendMasterAgentPackageToCustomers(_currentCustomerId, agentId.AgentID, "", "", 0, 0,
                0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 1, rst.ActionId, 0, 0, userName);
            }
          }

          agents.SendMasterAgentPackageToCustomers(_currentCustomerId, agentId.AgentID, "", "", packageOptions.AllTeasers, packageOptions.BaseballActionListed,
            packageOptions.BetServiceProfiles, packageOptions.CallUnitMinBet, packageOptions.CasinoSuspendWagering, packageOptions.ChartPercentBook, packageOptions.CostToBuyPoints, packageOptions.CreditAccountingFlag,
            packageOptions.Currency, packageOptions.DetailHorseLimits, packageOptions.DetailLimits, packageOptions.CreditLimit, packageOptions.EnableRollingIfBets, packageOptions.EnforceAccumGameLimits, packageOptions.FreeHalfPoints, packageOptions.HorseMaxPayout,
            packageOptions.HorseTrackRestrictions, packageOptions.InetTarget, packageOptions.InternetMinBet, packageOptions.LimitRifToRisk, packageOptions.MaxContestBet, packageOptions.MaxTeaserBet, packageOptions.ParlayMaxBet,
            packageOptions.ParlayMaxPayout, packageOptions.ParlaySchedule, packageOptions.PriceType, packageOptions.PuckCanadianLineSetting, packageOptions.SourceInformation,
            packageOptions.SportsbookSuspendWagering, packageOptions.StaticLinesSetting, packageOptions.Store, packageOptions.TimeZone, packageOptions.VigDiscounts, packageOptions.WagerLimitOfferingPage, packageOptions.ZeroBalanceFlag, 0, null, packageOptions.DisabledSports, packageOptions.ShadeGroups, userName);

          using (var lw = new LogWriter(AppModuleInfo)) {
            lw.WriteToCuAccessLog(packageOptions.ItemsToCuLog);
          }
        }
      }
    }

    private void SendPackageToAgent(string sourceAgentId, string targetAgentId, PackageOptions packageOptions, Agents agents) {
      packageOptions.WriteCallUnitAccessLogData(sourceAgentId.Trim(), targetAgentId.Trim());
      var userName = Environment.UserName.Trim();
      if (packageOptions.DetailLimits == 1 && _sportAndSubSportItems != null && _sportAndSubSportItems.Count > 0) {
        foreach (var spt in _sportAndSubSportItems) {
          var sportType = spt.Split('-')[0].Trim();
          var sportSubType = spt.Split('-')[1].Trim();
          agents.SendPackageToAgent(sourceAgentId, targetAgentId, sportType, sportSubType, 0, 0,
                          0, 0, 0, 0, 0, 0,
                          0, 0, packageOptions.DetailLimits, 0, 0, 0, 0, 0,
                          0, 0, 0, 0, 0, 0, 0,
                          0, 0, 0, 0, 0,
                          0, 0, 0, 0, 0, 0, 0, 0, null, 0, 0, userName);
        }
      }

      if (packageOptions.RestrictionsFlag == 1 && _customerRestrictionItems != null && _customerRestrictionItems.Count > 0) {
        foreach (var rst in _customerRestrictionItems) {
          agents.SendPackageToAgent(sourceAgentId, targetAgentId, "", "", 0, 0,
            0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 1, rst.ActionId, 0, 0, userName);
        }
      }

      agents.SendPackageToAgent(sourceAgentId, targetAgentId, "", "", packageOptions.AllTeasers, packageOptions.BaseballActionListed,
                                                              packageOptions.BetServiceProfiles, packageOptions.CallUnitMinBet, packageOptions.CasinoSuspendWagering, packageOptions.ChartPercentBook, packageOptions.CostToBuyPoints, packageOptions.CreditAccountingFlag,
                                                              packageOptions.Currency, packageOptions.DetailHorseLimits, packageOptions.DetailLimits, packageOptions.CreditLimit, packageOptions.EnableRollingIfBets, packageOptions.EnforceAccumGameLimits, packageOptions.FreeHalfPoints, packageOptions.HorseMaxPayout,
                                                              packageOptions.HorseTrackRestrictions, packageOptions.InetTarget, packageOptions.InternetMinBet, packageOptions.LimitRifToRisk, packageOptions.MaxContestBet, packageOptions.MaxTeaserBet, packageOptions.ParlayMaxBet,
                                                              packageOptions.ParlayMaxPayout, packageOptions.ParlaySchedule, packageOptions.PriceType, packageOptions.PuckCanadianLineSetting, packageOptions.SourceInformation,
                                                              packageOptions.SportsbookSuspendWagering, packageOptions.StaticLinesSetting, packageOptions.Store, packageOptions.TimeZone, packageOptions.VigDiscounts, packageOptions.WagerLimitOfferingPage, packageOptions.ZeroBalanceFlag, 0, null, packageOptions.DisabledSports, packageOptions.ShadeGroups, userName);
      /*
    agents.SendMasterAgentPackageToCustomers(sourceAgentId, targetAgentId, "", "", packageOptions.AllTeasers, packageOptions.BaseballActionListed,
                            packageOptions.BetServiceProfiles, packageOptions.CallUnitMinBet, packageOptions.CasinoSuspendWagering, packageOptions.ChartPercentBook, packageOptions.CostToBuyPoints, packageOptions.CreditAccountingFlag,
                            packageOptions.Currency, packageOptions.DetailHorseLimits, 0, packageOptions.CreditLimit, packageOptions.EnableRollingIfBets, packageOptions.EnforceAccumGameLimits, packageOptions.FreeHalfPoints, packageOptions.HorseMaxPayout,
                            packageOptions.HorseTrackRestrictions, packageOptions.InetTarget, packageOptions.InternetMinBet, packageOptions.LimitRifToRisk, packageOptions.MaxContestBet, packageOptions.MaxTeaserBet, packageOptions.ParlayMaxBet,
                            packageOptions.ParlayMaxPayout, packageOptions.ParlaySchedule, packageOptions.PriceType, packageOptions.PuckCanadianLineSetting, packageOptions.SourceInformation,
                            packageOptions.SportsbookSuspendWagering, packageOptions.StaticLinesSetting, packageOptions.Store, packageOptions.TimeZone, packageOptions.VigDiscounts, packageOptions.WagerLimitOfferingPage, packageOptions.ZeroBalanceFlag, 0, null, packageOptions.DisabledSports, packageOptions.ShadeGroups, userName);
      */

      using (var lw = new LogWriter(AppModuleInfo)) {
        lw.WriteToCuAccessLog(packageOptions.ItemsToCuLog);
      }
    }

    private void SetDefaultsToSportsAndRestrictionsObjs() {
      chlbSports.Width = 200;
      chlbSports.CheckOnClick = true;
      chlbSports.ItemCheck += chlbSports_ItemCheck;
      panSportTypes.Enabled = false;

      chlbRestrictions.ValueMember = "ActionId";
      chlbRestrictions.DisplayMember = "Name";
      chlbRestrictions.CheckOnClick = true;
      chlbRestrictions.Width = 200;
      panRestrictions.Enabled = false;
      chlbRestrictions.ItemCheck += chlbRestrictions_ItemCheck;
      lstSelectedRestrictions.DisplayMember = "Name";
      lstSelectedRestrictions.ValueMember = "ActionId";
    }

    private void ToggleCheckBoxes(bool p) {
      try {
        var cnt = panPackageOptions.Controls.Count;

        for (var i = 0; i < cnt; i++) {
          var ctrl = panPackageOptions.Controls[i];

          if (ctrl.GetType().ToString() == "System.Windows.Forms.CheckBox") {
            ((CheckBox)ctrl).Checked = p;
          }
        }
        chbDetailLimits.Checked = p;
        chbRestrictions.Checked = p;
      }

      catch (Exception ex) {
        Log(ex);
      }
    }

    #endregion

    #region Protected Methods

    #endregion

    #region Events

    private void frmSendPackageToCustomers_Load(object sender, EventArgs e) {
      HideShowSendPackageToAgentsCheckBox();
      SetDefaultsToSportsAndRestrictionsObjs();
    }

    private void btnCancel_Click(object sender, EventArgs e) {
      Close();
    }

    private void btnCheckAll_Click(object sender, EventArgs e) {
      ToggleCheckBoxes(true);
    }

    private void btnSend_Click(object sender, EventArgs e) {
      var execProcess = true;

      GetCheckedOptions();

      if (_checkedOptions.Count == 0) {
        execProcess = false;
      }

      if (chbDetailLimits.Checked) {
        if (panSportTypes.Controls.Count > 0) {
          var cbx = (CheckedComboBox)panSportTypes.Controls[0];

          if (cbx.CheckedItems.Count == 0) {
            MessageBox.Show(@"At least one sport must be selected");
            if (cbx.Items.Count > 0) {
              cbx.Focus();
              return;
            }
          }
        }
      }

      if (chbRestrictions.Checked) {
        if (panRestrictions.Controls.Count > 0) {
          var cbx = (CheckedComboBox)panRestrictions.Controls[0];

          if (cbx.CheckedItems.Count == 0) {
            MessageBox.Show(@"At least one restriction must be selected");
            if (cbx.Items.Count > 0) {
              cbx.Focus();
              return;
            }
          }
        }
      }

      if (_checkedOptions.Count == 0) {
        MessageBox.Show(@"At least one option must be selected");
        return;
      }

      if (!execProcess) return;
      SendPackageToCustomers();
      Close();
    }

    private void btnUncheckAll_Click(object sender, EventArgs e) {
      ToggleCheckBoxes(false);
    }

    private void chbDetailLimits_CheckedChanged(object sender, EventArgs e) {

      chlbSports.Items.Clear();
      if (((CheckBox)sender).Checked) {
        GetSportTypes();
        panSportTypes.Enabled = true;
        grpChosenSports.Visible = true;
      }
      else {
        chlbSports.Text = "";
        lstSelectedSports.Items.Clear();
        grpChosenSports.Visible = false;
        panSportTypes.Enabled = false;
      }
    }

    private void chbRestrictions_CheckedChanged(object sender, EventArgs e) {
      chlbRestrictions.Items.Clear();
      if (((CheckBox)sender).Checked) {
        GetRestrictions();
        panRestrictions.Enabled = true;
        grpChosenRestrictions.Visible = true;
      }
      else {
        chlbRestrictions.Text = "";
        lstSelectedRestrictions.Items.Clear();
        grpChosenRestrictions.Visible = false;
        panRestrictions.Enabled = false;
      }
    }

    private void chlbSports_ItemCheck(object sender, ItemCheckEventArgs e) {
      ShowInSelectedSports(((CheckedListBox)sender).SelectedItem.ToString(), e.NewValue);
    }

    private void chlbRestrictions_ItemCheck(object sender, ItemCheckEventArgs e) {
      ShowInSelectedRestrictions((spCstGetActions_Result)((CheckedListBox)sender).SelectedItem, e.NewValue);
    }

    #endregion
  }
}
