﻿using CustomerMaintenance.Libraries;
using GUILibraries.Forms;
using GUILibraries.Utilities;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using SIDLibraries.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Common = CustomerMaintenance.Utilities.Common;

namespace CustomerMaintenance.UI {
  public partial class FrmCustomerParlayTeaserWagerLimits : SIDForm {
    #region Private Vars

    private readonly FrmCustomerMaintenance _parentForm;
    private readonly String _wagerType;
    private readonly String _currentCustomerId = "";
    private readonly CustomerInfoUpdater _customerInfo;
    private List<CustomerInfoUpdater.CustomerParlayTeaserWagerLimit> ModifiedWagerLimits { get; set; }

    #endregion

    #region Public Vars

    #endregion

    #region Public Properties

    #endregion

    #region Private Properties

    #endregion

    #region Structures

    #endregion

    #region Constructors

    public FrmCustomerParlayTeaserWagerLimits(SIDForm parent, String wagerType)
      : base(parent.AppModuleInfo) {
      _parentForm = (FrmCustomerMaintenance)parent;
      _currentCustomerId = _parentForm.CustomerId;
      _wagerType = wagerType;
      _customerInfo = new CustomerInfoUpdater(_currentCustomerId, AppModuleInfo);
      InitializeComponent();
    }

    #endregion

    #region Public Methods

    #endregion

    #region Private Methods

    private void GetWagerLimitsSports() {
      cmbWLSports.SelectedIndexChanged -= cmbWLSports_SelectedIndexChanged;
      Common.GetWagerLimitsSports(AppModuleInfo, cmbWLSports, null, false, _wagerType);
      cmbWLSports.SelectedIndexChanged += cmbWLSports_SelectedIndexChanged;
      if (cmbWLSports.Items.Count > 0) cmbWLSports.SelectedIndex = 0;

    }

    private string GetSelectedWlSport() {
      if (cmbWLSports.Items.Count > 0 && cmbWLSports.SelectedIndex >= 0) {
        return cmbWLSports.SelectedValue.ToString();
      }
      return string.Empty;
    }

    private void LoadCustomerParlayTeaserWagerLimits() {
      try {
        using (var cust = new Customers(AppModuleInfo)) {
          var wagerLimits = cust.GetCustomerParlayTeaserWagerLimits(_currentCustomerId, _wagerType).ToList();

          if (dgvwParlayTeaserWagerLimitsDetails.Columns.Count < 10) {
            var dgrvwColNames = new ArrayList();

            foreach (DataGridViewTextBoxColumn col in dgvwParlayTeaserWagerLimitsDetails.Columns) {
              dgrvwColNames.Add(col.Name);
            }
            //dgvwWagerLimitsDetails.CancelEdit();
            foreach (String str in dgrvwColNames) {
              dgvwParlayTeaserWagerLimitsDetails.Columns.Remove(str);
            }

            dgvwParlayTeaserWagerLimitsDetails.Columns.Add("SportEnabled", "Enabled");
            dgvwParlayTeaserWagerLimitsDetails.Columns.Add("SportSubSport", "Sport - SubSport");
            dgvwParlayTeaserWagerLimitsDetails.Columns.Add("Period", "Period");
            dgvwParlayTeaserWagerLimitsDetails.Columns.Add("Type", "Type");
            dgvwParlayTeaserWagerLimitsDetails.Columns.Add("CULimit", "CU Limit");
            dgvwParlayTeaserWagerLimitsDetails.Columns.Add("InetLimit", "Inet Limit");
            dgvwParlayTeaserWagerLimitsDetails.Columns.Add("WagerTypeCode", "Wager Type Code");
            dgvwParlayTeaserWagerLimitsDetails.Columns.Add("PeriodNumber", "PeriodNumber");
            dgvwParlayTeaserWagerLimitsDetails.Columns.Add("tmpCuLimit", "tmpCuLimit");
            dgvwParlayTeaserWagerLimitsDetails.Columns.Add("tmpInetLimit", "tmpInetLimit");
            dgvwParlayTeaserWagerLimitsDetails.Columns.Add("tmpLimitsExpiration", "tmpLimitsExpiration");

            dgvwParlayTeaserWagerLimitsDetails.Columns.Add("SportType", "Sport Type");
            dgvwParlayTeaserWagerLimitsDetails.Columns.Add("SportSubType", "Sport SubType");


            dgvwParlayTeaserWagerLimitsDetails.Columns[0].Visible = false;

            dgvwParlayTeaserWagerLimitsDetails.Columns[1].Width = 160;
            dgvwParlayTeaserWagerLimitsDetails.Columns[1].ReadOnly = true;
            dgvwParlayTeaserWagerLimitsDetails.Columns[2].Width = 74;
            dgvwParlayTeaserWagerLimitsDetails.Columns[2].ReadOnly = true;
            dgvwParlayTeaserWagerLimitsDetails.Columns[3].Width = 68;
            dgvwParlayTeaserWagerLimitsDetails.Columns[3].ReadOnly = true;

            dgvwParlayTeaserWagerLimitsDetails.Columns[4].Width = 70;
            dgvwParlayTeaserWagerLimitsDetails.Columns[5].Width = 70;
            dgvwParlayTeaserWagerLimitsDetails.Columns[6].Visible = false;
            dgvwParlayTeaserWagerLimitsDetails.Columns[7].Visible = false;
            dgvwParlayTeaserWagerLimitsDetails.Columns[8].Visible = false;
            dgvwParlayTeaserWagerLimitsDetails.Columns[9].Visible = false;
            dgvwParlayTeaserWagerLimitsDetails.Columns[10].Visible = false;
            dgvwParlayTeaserWagerLimitsDetails.Columns[11].Visible = false;
            dgvwParlayTeaserWagerLimitsDetails.Columns[12].Visible = false;
          }

          dgvwParlayTeaserWagerLimitsDetails.Rows.Clear();

          var selectedSport = GetSelectedWlSport();
          List<spCstGetCustomerParlayTeaserWagerLimits_Result> filteredLimits;
          if (cmbWLSports.SelectedIndex > 0) {
            var selectedSubSport = string.Empty;
            /*if (selectedSport == SportTypes.TENNIS || selectedSport == SportTypes.GOLF) {
              selectedSubSport = selectedSport;
              selectedSport = SportTypes.OTHER_SPORTS;
            }*/
            filteredLimits = wagerLimits.Where(a => a.sportType.Trim() == selectedSport && (selectedSubSport == string.Empty || a.SportSubType.Contains(selectedSubSport))).ToList();
          }
          else filteredLimits = wagerLimits;
          foreach (var res in filteredLimits) {

            var cuLimit = double.Parse(res.CuLimit.ToString(CultureInfo.InvariantCulture)) / 100;
            var inetLimit = double.Parse(res.InetLimit.ToString(CultureInfo.InvariantCulture)) / 100;
            if (ModifiedWagerLimits != null && ModifiedWagerLimits.Count > 0) {
              var mwl = (from wl in ModifiedWagerLimits
                         where wl.SportType.Trim() == res.sportType.Trim()
                               && wl.SportSubType.Trim() == res.SportSubType.Trim()
                               && wl.Period == res.Period
                               && wl.WagerType == res.WagerType
                         select wl).FirstOrDefault();
              if (mwl != null) {
                cuLimit = double.Parse(mwl.CuLimit);
                inetLimit = double.Parse(mwl.InetLimit);
              }

            }

            dgvwParlayTeaserWagerLimitsDetails.Rows.Add("", res.SportSubSport, res.Period, res.WagerTypeDesc, FormatNumber(cuLimit, false), FormatNumber(inetLimit, false), res.WagerType, res.PeriodNumber, "", "", "", res.sportType, res.SportSubType);
          }
          FormF.PreventDataGridViewColumnsToBeSortedAndResized(dgvwParlayTeaserWagerLimitsDetails);
          _customerInfo.LoadcustomerParlayWagerLimitsToStructFromDb(wagerLimits);
        }
      }
      catch (EntitySqlException ex) {
        Log(ex);
      }
    }

    private void AddWagerLimitEditedRowToControlList(int i) {

      if (dgvwParlayTeaserWagerLimitsDetails.Rows[i].Cells[0].Value == null)
        return;

      var sportType = dgvwParlayTeaserWagerLimitsDetails.Rows[i].Cells[1].Value.ToString().Split('-')[0];
      var sportSubType = dgvwParlayTeaserWagerLimitsDetails.Rows[i].Cells[1].Value.ToString().Split('-')[1];
      var period = dgvwParlayTeaserWagerLimitsDetails.Rows[i].Cells[2].Value.ToString();
      var wagerType = dgvwParlayTeaserWagerLimitsDetails.Rows[i].Cells[6].Value.ToString();
      var wagerTypeName = dgvwParlayTeaserWagerLimitsDetails.Rows[i].Cells[3].Value.ToString();
      var cuLimit = StringF.ToStringOrEmpty(dgvwParlayTeaserWagerLimitsDetails.Rows[i].Cells[4].IsInEditMode ? dgvwParlayTeaserWagerLimitsDetails.Rows[i].Cells[4].EditedFormattedValue : dgvwParlayTeaserWagerLimitsDetails.Rows[i].Cells[4].Value).Replace(".00", "");
      var inetLimit = StringF.ToStringOrEmpty(dgvwParlayTeaserWagerLimitsDetails.Rows[i].Cells[5].IsInEditMode ? dgvwParlayTeaserWagerLimitsDetails.Rows[i].Cells[5].EditedFormattedValue : dgvwParlayTeaserWagerLimitsDetails.Rows[i].Cells[5].Value).Replace(".00", "");

      var modifiedRow = new CustomerInfoUpdater.CustomerParlayTeaserWagerLimit(
            sportType,
            sportSubType,
            period,
            wagerType,
            wagerTypeName,
            cuLimit,
            inetLimit,
            _wagerType
            );

      if (ModifiedWagerLimits == null) {
        ModifiedWagerLimits = new List<CustomerInfoUpdater.CustomerParlayTeaserWagerLimit>();
      }
      else {
        var toDel = (from v in ModifiedWagerLimits
                     where v.SportType.Trim() == modifiedRow.SportType.Trim()
                         && v.SportSubType.Trim() == modifiedRow.SportSubType.Trim()
                         && v.Period.Trim() == modifiedRow.Period.Trim()
                         && v.WagerType.Trim() == modifiedRow.WagerType.Trim()
                     select v).FirstOrDefault();
        lock (ModifiedWagerLimits) {
          ModifiedWagerLimits.Remove(toDel);
        }
      }
      lock (ModifiedWagerLimits) {
        ModifiedWagerLimits.Add(modifiedRow);
      }
    }

    private void SaveCustomerParlayWagerLimits() {
      _customerInfo.CompareCustomerParlayTeaserWagerLimitsInfoToUpdate(ModifiedWagerLimits);
      ModifiedWagerLimits = null;
    }

    #endregion

    #region Protected Methods

    #endregion

    #region Events

    private void frmCustomerParlayWagerLimits_Load(object sender, EventArgs e) {
      GetWagerLimitsSports();
      LoadCustomerParlayTeaserWagerLimits();

    }

    private void cancelButton_Click(object sender, EventArgs e) {
      Close();
    }

    private void cmbWLSports_SelectedIndexChanged(object sender, EventArgs e) {
      LoadCustomerParlayTeaserWagerLimits();
    }

    private void OkButton_Click(object sender, EventArgs e) {
      SaveCustomerParlayWagerLimits();
      Close();
    }

    #endregion

    private void dgvwParlayTeaserWagerLimitsDetails_CellEndEdit(object sender, DataGridViewCellEventArgs e) {
      AddWagerLimitEditedRowToControlList(e.RowIndex);
    }

    private void dgvwParlayTeaserWagerLimitsDetails_KeyDown(object sender, KeyEventArgs e) {
      var r = dgvwParlayTeaserWagerLimitsDetails.SelectedCells[0].RowIndex;
      var c = dgvwParlayTeaserWagerLimitsDetails.SelectedCells[0].ColumnIndex;
      if (c != 4 && c != 5)
        return;
      if (e.KeyCode == Keys.Tab) {
        e.Handled = true;
        btnOk.Focus();
      }

      if (e.Control && e.KeyCode == Keys.V) {
        char[] rowSplitter = { '\r', '\n' };
        char[] columnSplitter = { '\t' };
        var dataInClipboard = Clipboard.GetDataObject();
        //cmd.exe /c "echo off | clip"
        if (dataInClipboard == null) return;
        var stringInClipboard = (string)dataInClipboard.GetData(DataFormats.Text);
        var rowsInClipboard = stringInClipboard.Split(rowSplitter, StringSplitOptions.RemoveEmptyEntries);

        if (dgvwParlayTeaserWagerLimitsDetails.Rows.Count < (r + rowsInClipboard.Length)) {
          dgvwParlayTeaserWagerLimitsDetails.Rows.Add(r + rowsInClipboard.Length - dgvwParlayTeaserWagerLimitsDetails.Rows.Count);
        }
        for (var iRow = 0; iRow < rowsInClipboard.Length; iRow++) {
          var valuesInRow = rowsInClipboard[iRow].Split(columnSplitter);

          for (var iCol = 0; iCol < valuesInRow.Length; iCol++) {
            if (dgvwParlayTeaserWagerLimitsDetails.ColumnCount - 1 >= c + iCol) {
              if (dgvwParlayTeaserWagerLimitsDetails.Rows[r + iRow].Cells[c + iCol].ColumnIndex > 2) {
                int targetValue;
                int.TryParse(valuesInRow[iCol].Replace(",", ""), out targetValue);
                dgvwParlayTeaserWagerLimitsDetails.Rows[r + iRow].Cells[c + iCol].Value = FormatNumber(targetValue, false, 1, true);
                AddWagerLimitEditedRowToControlList(r + iRow);
              }
            }
          }
        }
      }
    }

    private void dgvwParlayTeaserWagerLimitsDetails_CellValidating(object sender, DataGridViewCellValidatingEventArgs e) {
      var dgv = (DataGridView)sender;
      if (e.ColumnIndex != 4 && e.ColumnIndex != 5) return;
      var cellValue = e.FormattedValue.ToString();

      if (cellValue != string.Empty) {
        if (NumberF.IsValidNumber(cellValue, true)) return;
        MessageBox.Show(@"Not a valid number");
        e.Cancel = true;
      }
      else if (dgv.EditingControl != null) dgv.EditingControl.Text = @"0";
    }

    private void dgvwParlayTeaserWagerLimitsDetails_DataError(object sender, DataGridViewDataErrorEventArgs e) {
      switch (e.ColumnIndex) {
        case 3:
          MessageBox.Show(@"Please enter a valid Parlay CU Limit Amount.");
          break;

        case 4:
          MessageBox.Show(@"Please enter a valid Parlay Inet Limit Amount.");
          break;
      }

      e.ThrowException = false;
    }

    private void dgvwParlayTeaserWagerLimitsDetails_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e) {
      var gv = (DataGridView)sender;

      var i = gv.CurrentCell.ColumnIndex;

      if (i == 3 || i == 4)
        e.Control.KeyPress += _parentForm.numberAmount_KeyPress;
    }

    private void btnShowAuditWagerLimits_Click(object sender, EventArgs e) {
      var item = new AuditTargetItem {
        Code = AuditTargetItems.DetParlayTeaserWagerLimits[0],
        Description = AuditTargetItems.DetParlayTeaserWagerLimits[1],
        IsNumber = true,
        SourceTable = AuditTargetItems.DetParlayTeaserWagerLimits[2]
      };

      if (dgvwParlayTeaserWagerLimitsDetails.SelectedCells.Count > 0) {
        var rowIdx = dgvwParlayTeaserWagerLimitsDetails.SelectedCells[0].OwningRow.Index;
        var selectedrow = dgvwParlayTeaserWagerLimitsDetails.Rows[rowIdx];
        ShowAuditInfo(item, selectedrow);
      }
    }

    private void ShowAuditInfo(AuditTargetItem auditInfoToShow, DataGridViewRow wagerLimitDetails) {
      using (var auditForm = new FrmCustomerAuditDetails(auditInfoToShow, _currentCustomerId, AppModuleInfo, wagerLimitDetails, true)) {
        auditForm.Icon = Icon;
        auditForm.ShowDialog();
      }
    }
  }
}
