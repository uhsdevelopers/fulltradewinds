﻿namespace CustomerMaintenance.UI
{
    partial class FrmSendPackageToCustomers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chbDetailLimits = new System.Windows.Forms.CheckBox();
            this.chbParlayMaxBet = new System.Windows.Forms.CheckBox();
            this.chbDetailHorseLimits = new System.Windows.Forms.CheckBox();
            this.chbMaxTeaserBet = new System.Windows.Forms.CheckBox();
            this.chbAllTeasers = new System.Windows.Forms.CheckBox();
            this.chbVigDiscounts = new System.Windows.Forms.CheckBox();
            this.chbBaseballActionListed = new System.Windows.Forms.CheckBox();
            this.chbPuckCanadianLineSetting = new System.Windows.Forms.CheckBox();
            this.chbZeroBalanceFlag = new System.Windows.Forms.CheckBox();
            this.chbSportsbookSuspendWagering = new System.Windows.Forms.CheckBox();
            this.chbParlaySchedule = new System.Windows.Forms.CheckBox();
            this.chbCostToBuyPoints = new System.Windows.Forms.CheckBox();
            this.chbStore = new System.Windows.Forms.CheckBox();
            this.chbInternetMinBet = new System.Windows.Forms.CheckBox();
            this.chbEnableRollingIfBets = new System.Windows.Forms.CheckBox();
            this.chbInetTarget = new System.Windows.Forms.CheckBox();
            this.chbEnforceAccumGameLimits = new System.Windows.Forms.CheckBox();
            this.chbCurrency = new System.Windows.Forms.CheckBox();
            this.chbParlayMaxPayout = new System.Windows.Forms.CheckBox();
            this.chbHorseMaxPayout = new System.Windows.Forms.CheckBox();
            this.chbMaxContestBet = new System.Windows.Forms.CheckBox();
            this.chbWagerLimitOfferingPage = new System.Windows.Forms.CheckBox();
            this.chbFreeHalfPoints = new System.Windows.Forms.CheckBox();
            this.chbCreditAccountingFlag = new System.Windows.Forms.CheckBox();
            this.chbStaticLinesSetting = new System.Windows.Forms.CheckBox();
            this.chbChartPercentBook = new System.Windows.Forms.CheckBox();
            this.chbCasinoSuspendWagering = new System.Windows.Forms.CheckBox();
            this.chbSourceInformation = new System.Windows.Forms.CheckBox();
            this.chbHorseTrackRestrictions = new System.Windows.Forms.CheckBox();
            this.chbCallUnitMinBet = new System.Windows.Forms.CheckBox();
            this.chbTimeZone = new System.Windows.Forms.CheckBox();
            this.chbLimitRIFToRisk = new System.Windows.Forms.CheckBox();
            this.chbBetServiceProfiles = new System.Windows.Forms.CheckBox();
            this.chbPriceType = new System.Windows.Forms.CheckBox();
            this.btnCheckAll = new System.Windows.Forms.Button();
            this.btnUncheckAll = new System.Windows.Forms.Button();
            this.btnSend = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.panPackageOptions = new System.Windows.Forms.Panel();
            this.chbDisabledSports = new System.Windows.Forms.CheckBox();
            this.chbCreditLimit = new System.Windows.Forms.CheckBox();
            this.chbRestrictions = new System.Windows.Forms.CheckBox();
            this.panSportTypes = new System.Windows.Forms.Panel();
            this.chlbSports = new GUILibraries.Controls.CheckedComboBox();
            this.chbAffectAgents = new System.Windows.Forms.CheckBox();
            this.lstSelectedSports = new System.Windows.Forms.ListBox();
            this.grpChosenSports = new System.Windows.Forms.GroupBox();
            this.grpSendToAgents = new System.Windows.Forms.GroupBox();
            this.grpSendCancel = new System.Windows.Forms.GroupBox();
            this.panRestrictions = new System.Windows.Forms.Panel();
            this.chlbRestrictions = new GUILibraries.Controls.CheckedComboBox();
            this.grpChosenRestrictions = new System.Windows.Forms.GroupBox();
            this.lstSelectedRestrictions = new System.Windows.Forms.ListBox();
            this.chbShadeGroups = new System.Windows.Forms.CheckBox();
            this.panPackageOptions.SuspendLayout();
            this.panSportTypes.SuspendLayout();
            this.grpChosenSports.SuspendLayout();
            this.grpSendToAgents.SuspendLayout();
            this.grpSendCancel.SuspendLayout();
            this.panRestrictions.SuspendLayout();
            this.grpChosenRestrictions.SuspendLayout();
            this.SuspendLayout();
            // 
            // chbDetailLimits
            // 
            this.chbDetailLimits.AutoSize = true;
            this.chbDetailLimits.Location = new System.Drawing.Point(411, 99);
            this.chbDetailLimits.Name = "chbDetailLimits";
            this.chbDetailLimits.Size = new System.Drawing.Size(82, 17);
            this.chbDetailLimits.TabIndex = 40;
            this.chbDetailLimits.Text = "Detail Limits";
            this.chbDetailLimits.UseVisualStyleBackColor = true;
            this.chbDetailLimits.CheckedChanged += new System.EventHandler(this.chbDetailLimits_CheckedChanged);
            // 
            // chbParlayMaxBet
            // 
            this.chbParlayMaxBet.AutoSize = true;
            this.chbParlayMaxBet.Location = new System.Drawing.Point(34, 62);
            this.chbParlayMaxBet.Name = "chbParlayMaxBet";
            this.chbParlayMaxBet.Size = new System.Drawing.Size(97, 17);
            this.chbParlayMaxBet.TabIndex = 60;
            this.chbParlayMaxBet.Text = "Parlay Max Bet";
            this.chbParlayMaxBet.UseVisualStyleBackColor = true;
            // 
            // chbDetailHorseLimits
            // 
            this.chbDetailHorseLimits.AutoSize = true;
            this.chbDetailHorseLimits.Location = new System.Drawing.Point(34, 85);
            this.chbDetailHorseLimits.Name = "chbDetailHorseLimits";
            this.chbDetailHorseLimits.Size = new System.Drawing.Size(113, 17);
            this.chbDetailHorseLimits.TabIndex = 80;
            this.chbDetailHorseLimits.Text = "Detail Horse Limits";
            this.chbDetailHorseLimits.UseVisualStyleBackColor = true;
            // 
            // chbMaxTeaserBet
            // 
            this.chbMaxTeaserBet.AutoSize = true;
            this.chbMaxTeaserBet.Location = new System.Drawing.Point(34, 108);
            this.chbMaxTeaserBet.Name = "chbMaxTeaserBet";
            this.chbMaxTeaserBet.Size = new System.Drawing.Size(101, 17);
            this.chbMaxTeaserBet.TabIndex = 100;
            this.chbMaxTeaserBet.Text = "Max Teaser Bet";
            this.chbMaxTeaserBet.UseVisualStyleBackColor = true;
            // 
            // chbAllTeasers
            // 
            this.chbAllTeasers.AutoSize = true;
            this.chbAllTeasers.Location = new System.Drawing.Point(34, 131);
            this.chbAllTeasers.Name = "chbAllTeasers";
            this.chbAllTeasers.Size = new System.Drawing.Size(78, 17);
            this.chbAllTeasers.TabIndex = 120;
            this.chbAllTeasers.Text = "All Teasers";
            this.chbAllTeasers.UseVisualStyleBackColor = true;
            // 
            // chbVigDiscounts
            // 
            this.chbVigDiscounts.AutoSize = true;
            this.chbVigDiscounts.Location = new System.Drawing.Point(34, 154);
            this.chbVigDiscounts.Name = "chbVigDiscounts";
            this.chbVigDiscounts.Size = new System.Drawing.Size(91, 17);
            this.chbVigDiscounts.TabIndex = 140;
            this.chbVigDiscounts.Text = "Vig Discounts";
            this.chbVigDiscounts.UseVisualStyleBackColor = true;
            // 
            // chbBaseballActionListed
            // 
            this.chbBaseballActionListed.AutoSize = true;
            this.chbBaseballActionListed.Location = new System.Drawing.Point(34, 177);
            this.chbBaseballActionListed.Name = "chbBaseballActionListed";
            this.chbBaseballActionListed.Size = new System.Drawing.Size(132, 17);
            this.chbBaseballActionListed.TabIndex = 160;
            this.chbBaseballActionListed.Text = "Baseball Action/Listed";
            this.chbBaseballActionListed.UseVisualStyleBackColor = true;
            // 
            // chbPuckCanadianLineSetting
            // 
            this.chbPuckCanadianLineSetting.AutoSize = true;
            this.chbPuckCanadianLineSetting.Location = new System.Drawing.Point(34, 200);
            this.chbPuckCanadianLineSetting.Name = "chbPuckCanadianLineSetting";
            this.chbPuckCanadianLineSetting.Size = new System.Drawing.Size(160, 17);
            this.chbPuckCanadianLineSetting.TabIndex = 180;
            this.chbPuckCanadianLineSetting.Text = "Puck/Canadian Line Setting";
            this.chbPuckCanadianLineSetting.UseVisualStyleBackColor = true;
            // 
            // chbZeroBalanceFlag
            // 
            this.chbZeroBalanceFlag.AutoSize = true;
            this.chbZeroBalanceFlag.Location = new System.Drawing.Point(34, 223);
            this.chbZeroBalanceFlag.Name = "chbZeroBalanceFlag";
            this.chbZeroBalanceFlag.Size = new System.Drawing.Size(113, 17);
            this.chbZeroBalanceFlag.TabIndex = 200;
            this.chbZeroBalanceFlag.Text = "Zero Balance Flag";
            this.chbZeroBalanceFlag.UseVisualStyleBackColor = true;
            // 
            // chbSportsbookSuspendWagering
            // 
            this.chbSportsbookSuspendWagering.AutoSize = true;
            this.chbSportsbookSuspendWagering.Location = new System.Drawing.Point(34, 246);
            this.chbSportsbookSuspendWagering.Name = "chbSportsbookSuspendWagering";
            this.chbSportsbookSuspendWagering.Size = new System.Drawing.Size(174, 17);
            this.chbSportsbookSuspendWagering.TabIndex = 220;
            this.chbSportsbookSuspendWagering.Text = "Sportsbook Suspend Wagering";
            this.chbSportsbookSuspendWagering.UseVisualStyleBackColor = true;
            // 
            // chbParlaySchedule
            // 
            this.chbParlaySchedule.AutoSize = true;
            this.chbParlaySchedule.Location = new System.Drawing.Point(34, 269);
            this.chbParlaySchedule.Name = "chbParlaySchedule";
            this.chbParlaySchedule.Size = new System.Drawing.Size(103, 17);
            this.chbParlaySchedule.TabIndex = 240;
            this.chbParlaySchedule.Text = "Parlay Schedule";
            this.chbParlaySchedule.UseVisualStyleBackColor = true;
            // 
            // chbCostToBuyPoints
            // 
            this.chbCostToBuyPoints.AutoSize = true;
            this.chbCostToBuyPoints.Location = new System.Drawing.Point(34, 292);
            this.chbCostToBuyPoints.Name = "chbCostToBuyPoints";
            this.chbCostToBuyPoints.Size = new System.Drawing.Size(116, 17);
            this.chbCostToBuyPoints.TabIndex = 260;
            this.chbCostToBuyPoints.Text = "Cost To Buy Points";
            this.chbCostToBuyPoints.UseVisualStyleBackColor = true;
            // 
            // chbStore
            // 
            this.chbStore.AutoSize = true;
            this.chbStore.Location = new System.Drawing.Point(34, 315);
            this.chbStore.Name = "chbStore";
            this.chbStore.Size = new System.Drawing.Size(51, 17);
            this.chbStore.TabIndex = 280;
            this.chbStore.Text = "Store";
            this.chbStore.UseVisualStyleBackColor = true;
            // 
            // chbInternetMinBet
            // 
            this.chbInternetMinBet.AutoSize = true;
            this.chbInternetMinBet.Location = new System.Drawing.Point(34, 338);
            this.chbInternetMinBet.Name = "chbInternetMinBet";
            this.chbInternetMinBet.Size = new System.Drawing.Size(101, 17);
            this.chbInternetMinBet.TabIndex = 300;
            this.chbInternetMinBet.Text = "Internet Min Bet";
            this.chbInternetMinBet.UseVisualStyleBackColor = true;
            // 
            // chbEnableRollingIfBets
            // 
            this.chbEnableRollingIfBets.AutoSize = true;
            this.chbEnableRollingIfBets.Location = new System.Drawing.Point(34, 361);
            this.chbEnableRollingIfBets.Name = "chbEnableRollingIfBets";
            this.chbEnableRollingIfBets.Size = new System.Drawing.Size(127, 17);
            this.chbEnableRollingIfBets.TabIndex = 320;
            this.chbEnableRollingIfBets.Text = "Enable Rolling If Bets";
            this.chbEnableRollingIfBets.UseVisualStyleBackColor = true;
            // 
            // chbInetTarget
            // 
            this.chbInetTarget.AutoSize = true;
            this.chbInetTarget.Location = new System.Drawing.Point(34, 384);
            this.chbInetTarget.Name = "chbInetTarget";
            this.chbInetTarget.Size = new System.Drawing.Size(78, 17);
            this.chbInetTarget.TabIndex = 340;
            this.chbInetTarget.Text = "Inet Target";
            this.chbInetTarget.UseVisualStyleBackColor = true;
            // 
            // chbEnforceAccumGameLimits
            // 
            this.chbEnforceAccumGameLimits.AutoSize = true;
            this.chbEnforceAccumGameLimits.Location = new System.Drawing.Point(34, 407);
            this.chbEnforceAccumGameLimits.Name = "chbEnforceAccumGameLimits";
            this.chbEnforceAccumGameLimits.Size = new System.Drawing.Size(159, 17);
            this.chbEnforceAccumGameLimits.TabIndex = 360;
            this.chbEnforceAccumGameLimits.Text = "Enforce Accum Game Limits";
            this.chbEnforceAccumGameLimits.UseVisualStyleBackColor = true;
            // 
            // chbCurrency
            // 
            this.chbCurrency.AutoSize = true;
            this.chbCurrency.Location = new System.Drawing.Point(34, 430);
            this.chbCurrency.Name = "chbCurrency";
            this.chbCurrency.Size = new System.Drawing.Size(68, 17);
            this.chbCurrency.TabIndex = 380;
            this.chbCurrency.Text = "Currency";
            this.chbCurrency.UseVisualStyleBackColor = true;
            // 
            // chbParlayMaxPayout
            // 
            this.chbParlayMaxPayout.AutoSize = true;
            this.chbParlayMaxPayout.Location = new System.Drawing.Point(227, 39);
            this.chbParlayMaxPayout.Name = "chbParlayMaxPayout";
            this.chbParlayMaxPayout.Size = new System.Drawing.Size(114, 17);
            this.chbParlayMaxPayout.TabIndex = 70;
            this.chbParlayMaxPayout.Text = "Parlay Max Payout";
            this.chbParlayMaxPayout.UseVisualStyleBackColor = true;
            // 
            // chbHorseMaxPayout
            // 
            this.chbHorseMaxPayout.AutoSize = true;
            this.chbHorseMaxPayout.Location = new System.Drawing.Point(227, 62);
            this.chbHorseMaxPayout.Name = "chbHorseMaxPayout";
            this.chbHorseMaxPayout.Size = new System.Drawing.Size(113, 17);
            this.chbHorseMaxPayout.TabIndex = 90;
            this.chbHorseMaxPayout.Text = "Horse Max Payout";
            this.chbHorseMaxPayout.UseVisualStyleBackColor = true;
            // 
            // chbMaxContestBet
            // 
            this.chbMaxContestBet.AutoSize = true;
            this.chbMaxContestBet.Location = new System.Drawing.Point(227, 85);
            this.chbMaxContestBet.Name = "chbMaxContestBet";
            this.chbMaxContestBet.Size = new System.Drawing.Size(104, 17);
            this.chbMaxContestBet.TabIndex = 110;
            this.chbMaxContestBet.Text = "Max Contest Bet";
            this.chbMaxContestBet.UseVisualStyleBackColor = true;
            // 
            // chbWagerLimitOfferingPage
            // 
            this.chbWagerLimitOfferingPage.AutoSize = true;
            this.chbWagerLimitOfferingPage.Location = new System.Drawing.Point(227, 108);
            this.chbWagerLimitOfferingPage.Name = "chbWagerLimitOfferingPage";
            this.chbWagerLimitOfferingPage.Size = new System.Drawing.Size(153, 17);
            this.chbWagerLimitOfferingPage.TabIndex = 130;
            this.chbWagerLimitOfferingPage.Text = "Wager Limit (offering page)";
            this.chbWagerLimitOfferingPage.UseVisualStyleBackColor = true;
            // 
            // chbFreeHalfPoints
            // 
            this.chbFreeHalfPoints.AutoSize = true;
            this.chbFreeHalfPoints.Location = new System.Drawing.Point(227, 131);
            this.chbFreeHalfPoints.Name = "chbFreeHalfPoints";
            this.chbFreeHalfPoints.Size = new System.Drawing.Size(101, 17);
            this.chbFreeHalfPoints.TabIndex = 150;
            this.chbFreeHalfPoints.Text = "Free Half Points";
            this.chbFreeHalfPoints.UseVisualStyleBackColor = true;
            // 
            // chbCreditAccountingFlag
            // 
            this.chbCreditAccountingFlag.AutoSize = true;
            this.chbCreditAccountingFlag.Location = new System.Drawing.Point(227, 154);
            this.chbCreditAccountingFlag.Name = "chbCreditAccountingFlag";
            this.chbCreditAccountingFlag.Size = new System.Drawing.Size(133, 17);
            this.chbCreditAccountingFlag.TabIndex = 170;
            this.chbCreditAccountingFlag.Text = "Credit Accounting Flag";
            this.chbCreditAccountingFlag.UseVisualStyleBackColor = true;
            // 
            // chbStaticLinesSetting
            // 
            this.chbStaticLinesSetting.AutoSize = true;
            this.chbStaticLinesSetting.Location = new System.Drawing.Point(227, 177);
            this.chbStaticLinesSetting.Name = "chbStaticLinesSetting";
            this.chbStaticLinesSetting.Size = new System.Drawing.Size(117, 17);
            this.chbStaticLinesSetting.TabIndex = 190;
            this.chbStaticLinesSetting.Text = "Static Lines Setting";
            this.chbStaticLinesSetting.UseVisualStyleBackColor = true;
            // 
            // chbChartPercentBook
            // 
            this.chbChartPercentBook.AutoSize = true;
            this.chbChartPercentBook.Location = new System.Drawing.Point(227, 200);
            this.chbChartPercentBook.Name = "chbChartPercentBook";
            this.chbChartPercentBook.Size = new System.Drawing.Size(119, 17);
            this.chbChartPercentBook.TabIndex = 210;
            this.chbChartPercentBook.Text = "Chart Percent Book";
            this.chbChartPercentBook.UseVisualStyleBackColor = true;
            // 
            // chbCasinoSuspendWagering
            // 
            this.chbCasinoSuspendWagering.AutoSize = true;
            this.chbCasinoSuspendWagering.Location = new System.Drawing.Point(227, 223);
            this.chbCasinoSuspendWagering.Name = "chbCasinoSuspendWagering";
            this.chbCasinoSuspendWagering.Size = new System.Drawing.Size(152, 17);
            this.chbCasinoSuspendWagering.TabIndex = 230;
            this.chbCasinoSuspendWagering.Text = "Casino Suspend Wagering";
            this.chbCasinoSuspendWagering.UseVisualStyleBackColor = true;
            // 
            // chbSourceInformation
            // 
            this.chbSourceInformation.AutoSize = true;
            this.chbSourceInformation.Location = new System.Drawing.Point(227, 246);
            this.chbSourceInformation.Name = "chbSourceInformation";
            this.chbSourceInformation.Size = new System.Drawing.Size(115, 17);
            this.chbSourceInformation.TabIndex = 250;
            this.chbSourceInformation.Text = "Source Information";
            this.chbSourceInformation.UseVisualStyleBackColor = true;
            // 
            // chbHorseTrackRestrictions
            // 
            this.chbHorseTrackRestrictions.AutoSize = true;
            this.chbHorseTrackRestrictions.Location = new System.Drawing.Point(227, 269);
            this.chbHorseTrackRestrictions.Name = "chbHorseTrackRestrictions";
            this.chbHorseTrackRestrictions.Size = new System.Drawing.Size(143, 17);
            this.chbHorseTrackRestrictions.TabIndex = 270;
            this.chbHorseTrackRestrictions.Text = "Horse Track Restrictions";
            this.chbHorseTrackRestrictions.UseVisualStyleBackColor = true;
            // 
            // chbCallUnitMinBet
            // 
            this.chbCallUnitMinBet.AutoSize = true;
            this.chbCallUnitMinBet.Location = new System.Drawing.Point(227, 292);
            this.chbCallUnitMinBet.Name = "chbCallUnitMinBet";
            this.chbCallUnitMinBet.Size = new System.Drawing.Size(104, 17);
            this.chbCallUnitMinBet.TabIndex = 290;
            this.chbCallUnitMinBet.Text = "Call Unit Min Bet";
            this.chbCallUnitMinBet.UseVisualStyleBackColor = true;
            // 
            // chbTimeZone
            // 
            this.chbTimeZone.AutoSize = true;
            this.chbTimeZone.Location = new System.Drawing.Point(227, 315);
            this.chbTimeZone.Name = "chbTimeZone";
            this.chbTimeZone.Size = new System.Drawing.Size(77, 17);
            this.chbTimeZone.TabIndex = 310;
            this.chbTimeZone.Text = "Time Zone";
            this.chbTimeZone.UseVisualStyleBackColor = true;
            // 
            // chbLimitRIFToRisk
            // 
            this.chbLimitRIFToRisk.AutoSize = true;
            this.chbLimitRIFToRisk.Location = new System.Drawing.Point(227, 338);
            this.chbLimitRIFToRisk.Name = "chbLimitRIFToRisk";
            this.chbLimitRIFToRisk.Size = new System.Drawing.Size(107, 17);
            this.chbLimitRIFToRisk.TabIndex = 330;
            this.chbLimitRIFToRisk.Text = "Limit RIF To Risk";
            this.chbLimitRIFToRisk.UseVisualStyleBackColor = true;
            // 
            // chbBetServiceProfiles
            // 
            this.chbBetServiceProfiles.AutoSize = true;
            this.chbBetServiceProfiles.Location = new System.Drawing.Point(227, 361);
            this.chbBetServiceProfiles.Name = "chbBetServiceProfiles";
            this.chbBetServiceProfiles.Size = new System.Drawing.Size(118, 17);
            this.chbBetServiceProfiles.TabIndex = 350;
            this.chbBetServiceProfiles.Text = "Bet Service Profiles";
            this.chbBetServiceProfiles.UseVisualStyleBackColor = true;
            // 
            // chbPriceType
            // 
            this.chbPriceType.AutoSize = true;
            this.chbPriceType.Location = new System.Drawing.Point(227, 384);
            this.chbPriceType.Name = "chbPriceType";
            this.chbPriceType.Size = new System.Drawing.Size(77, 17);
            this.chbPriceType.TabIndex = 370;
            this.chbPriceType.Text = "Price Type";
            this.chbPriceType.UseVisualStyleBackColor = true;
            // 
            // btnCheckAll
            // 
            this.btnCheckAll.Location = new System.Drawing.Point(410, 35);
            this.btnCheckAll.Name = "btnCheckAll";
            this.btnCheckAll.Size = new System.Drawing.Size(75, 23);
            this.btnCheckAll.TabIndex = 10;
            this.btnCheckAll.Text = "Check All";
            this.btnCheckAll.UseVisualStyleBackColor = true;
            this.btnCheckAll.Click += new System.EventHandler(this.btnCheckAll_Click);
            // 
            // btnUncheckAll
            // 
            this.btnUncheckAll.Location = new System.Drawing.Point(410, 64);
            this.btnUncheckAll.Name = "btnUncheckAll";
            this.btnUncheckAll.Size = new System.Drawing.Size(75, 23);
            this.btnUncheckAll.TabIndex = 20;
            this.btnUncheckAll.Text = "Uncheck All";
            this.btnUncheckAll.UseVisualStyleBackColor = true;
            this.btnUncheckAll.Click += new System.EventHandler(this.btnUncheckAll_Click);
            // 
            // btnSend
            // 
            this.btnSend.Location = new System.Drawing.Point(41, 15);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(75, 23);
            this.btnSend.TabIndex = 390;
            this.btnSend.Text = "Send";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(136, 15);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 400;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // panPackageOptions
            // 
            this.panPackageOptions.Controls.Add(this.chbShadeGroups);
            this.panPackageOptions.Controls.Add(this.chbDisabledSports);
            this.panPackageOptions.Controls.Add(this.chbCreditLimit);
            this.panPackageOptions.Controls.Add(this.chbParlayMaxBet);
            this.panPackageOptions.Controls.Add(this.chbDetailHorseLimits);
            this.panPackageOptions.Controls.Add(this.chbMaxTeaserBet);
            this.panPackageOptions.Controls.Add(this.chbAllTeasers);
            this.panPackageOptions.Controls.Add(this.chbPriceType);
            this.panPackageOptions.Controls.Add(this.chbVigDiscounts);
            this.panPackageOptions.Controls.Add(this.chbBetServiceProfiles);
            this.panPackageOptions.Controls.Add(this.chbBaseballActionListed);
            this.panPackageOptions.Controls.Add(this.chbLimitRIFToRisk);
            this.panPackageOptions.Controls.Add(this.chbPuckCanadianLineSetting);
            this.panPackageOptions.Controls.Add(this.chbTimeZone);
            this.panPackageOptions.Controls.Add(this.chbZeroBalanceFlag);
            this.panPackageOptions.Controls.Add(this.chbCallUnitMinBet);
            this.panPackageOptions.Controls.Add(this.chbSportsbookSuspendWagering);
            this.panPackageOptions.Controls.Add(this.chbHorseTrackRestrictions);
            this.panPackageOptions.Controls.Add(this.chbParlaySchedule);
            this.panPackageOptions.Controls.Add(this.chbSourceInformation);
            this.panPackageOptions.Controls.Add(this.chbCostToBuyPoints);
            this.panPackageOptions.Controls.Add(this.chbCasinoSuspendWagering);
            this.panPackageOptions.Controls.Add(this.chbStore);
            this.panPackageOptions.Controls.Add(this.chbChartPercentBook);
            this.panPackageOptions.Controls.Add(this.chbInternetMinBet);
            this.panPackageOptions.Controls.Add(this.chbStaticLinesSetting);
            this.panPackageOptions.Controls.Add(this.chbEnableRollingIfBets);
            this.panPackageOptions.Controls.Add(this.chbCreditAccountingFlag);
            this.panPackageOptions.Controls.Add(this.chbInetTarget);
            this.panPackageOptions.Controls.Add(this.chbFreeHalfPoints);
            this.panPackageOptions.Controls.Add(this.chbEnforceAccumGameLimits);
            this.panPackageOptions.Controls.Add(this.chbWagerLimitOfferingPage);
            this.panPackageOptions.Controls.Add(this.chbCurrency);
            this.panPackageOptions.Controls.Add(this.chbMaxContestBet);
            this.panPackageOptions.Controls.Add(this.chbParlayMaxPayout);
            this.panPackageOptions.Controls.Add(this.chbHorseMaxPayout);
            this.panPackageOptions.Location = new System.Drawing.Point(8, 8);
            this.panPackageOptions.Name = "panPackageOptions";
            this.panPackageOptions.Size = new System.Drawing.Size(396, 448);
            this.panPackageOptions.TabIndex = 30;
            // 
            // chbDisabledSports
            // 
            this.chbDisabledSports.AutoSize = true;
            this.chbDisabledSports.Location = new System.Drawing.Point(227, 407);
            this.chbDisabledSports.Name = "chbDisabledSports";
            this.chbDisabledSports.Size = new System.Drawing.Size(100, 17);
            this.chbDisabledSports.TabIndex = 383;
            this.chbDisabledSports.Text = "Disabled Sports";
            this.chbDisabledSports.UseVisualStyleBackColor = true;
            // 
            // chbCreditLimit
            // 
            this.chbCreditLimit.AutoSize = true;
            this.chbCreditLimit.Location = new System.Drawing.Point(34, 39);
            this.chbCreditLimit.Name = "chbCreditLimit";
            this.chbCreditLimit.Size = new System.Drawing.Size(77, 17);
            this.chbCreditLimit.TabIndex = 382;
            this.chbCreditLimit.Text = "Credit Limit";
            this.chbCreditLimit.UseVisualStyleBackColor = true;
            // 
            // chbRestrictions
            // 
            this.chbRestrictions.AutoSize = true;
            this.chbRestrictions.Location = new System.Drawing.Point(410, 248);
            this.chbRestrictions.Name = "chbRestrictions";
            this.chbRestrictions.Size = new System.Drawing.Size(81, 17);
            this.chbRestrictions.TabIndex = 381;
            this.chbRestrictions.Text = "Restrictions";
            this.chbRestrictions.UseVisualStyleBackColor = true;
            this.chbRestrictions.CheckedChanged += new System.EventHandler(this.chbRestrictions_CheckedChanged);
            // 
            // panSportTypes
            // 
            this.panSportTypes.Controls.Add(this.chlbSports);
            this.panSportTypes.Enabled = false;
            this.panSportTypes.Location = new System.Drawing.Point(500, 87);
            this.panSportTypes.Name = "panSportTypes";
            this.panSportTypes.Size = new System.Drawing.Size(210, 29);
            this.panSportTypes.TabIndex = 50;
            // 
            // chlbSports
            // 
            this.chlbSports.CheckOnClick = true;
            this.chlbSports.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.chlbSports.DropDownHeight = 1;
            this.chlbSports.FormattingEnabled = true;
            this.chlbSports.IntegralHeight = false;
            this.chlbSports.Location = new System.Drawing.Point(3, 3);
            this.chlbSports.Name = "chlbSports";
            this.chlbSports.Size = new System.Drawing.Size(200, 21);
            this.chlbSports.TabIndex = 0;
            this.chlbSports.ValueSeparator = ", ";
            // 
            // chbAffectAgents
            // 
            this.chbAffectAgents.AutoSize = true;
            this.chbAffectAgents.Location = new System.Drawing.Point(6, 19);
            this.chbAffectAgents.Name = "chbAffectAgents";
            this.chbAffectAgents.Size = new System.Drawing.Size(145, 17);
            this.chbAffectAgents.TabIndex = 401;
            this.chbAffectAgents.Text = "Send Package to Agents";
            this.chbAffectAgents.UseVisualStyleBackColor = true;
            // 
            // lstSelectedSports
            // 
            this.lstSelectedSports.FormattingEnabled = true;
            this.lstSelectedSports.Location = new System.Drawing.Point(6, 19);
            this.lstSelectedSports.Name = "lstSelectedSports";
            this.lstSelectedSports.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lstSelectedSports.Size = new System.Drawing.Size(293, 82);
            this.lstSelectedSports.TabIndex = 402;
            // 
            // grpChosenSports
            // 
            this.grpChosenSports.Controls.Add(this.lstSelectedSports);
            this.grpChosenSports.Location = new System.Drawing.Point(404, 122);
            this.grpChosenSports.Name = "grpChosenSports";
            this.grpChosenSports.Size = new System.Drawing.Size(306, 108);
            this.grpChosenSports.TabIndex = 403;
            this.grpChosenSports.TabStop = false;
            this.grpChosenSports.Text = "Selected Sports For Sending Detail Limits";
            this.grpChosenSports.Visible = false;
            // 
            // grpSendToAgents
            // 
            this.grpSendToAgents.Controls.Add(this.chbAffectAgents);
            this.grpSendToAgents.Location = new System.Drawing.Point(8, 461);
            this.grpSendToAgents.Name = "grpSendToAgents";
            this.grpSendToAgents.Size = new System.Drawing.Size(163, 46);
            this.grpSendToAgents.TabIndex = 404;
            this.grpSendToAgents.TabStop = false;
            this.grpSendToAgents.Text = "Select to ...";
            // 
            // grpSendCancel
            // 
            this.grpSendCancel.Controls.Add(this.btnSend);
            this.grpSendCancel.Controls.Add(this.btnCancel);
            this.grpSendCancel.Location = new System.Drawing.Point(177, 461);
            this.grpSendCancel.Name = "grpSendCancel";
            this.grpSendCancel.Size = new System.Drawing.Size(276, 46);
            this.grpSendCancel.TabIndex = 405;
            this.grpSendCancel.TabStop = false;
            // 
            // panRestrictions
            // 
            this.panRestrictions.Controls.Add(this.chlbRestrictions);
            this.panRestrictions.Enabled = false;
            this.panRestrictions.Location = new System.Drawing.Point(500, 236);
            this.panRestrictions.Name = "panRestrictions";
            this.panRestrictions.Size = new System.Drawing.Size(210, 29);
            this.panRestrictions.TabIndex = 51;
            // 
            // chlbRestrictions
            // 
            this.chlbRestrictions.CheckOnClick = true;
            this.chlbRestrictions.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.chlbRestrictions.DropDownHeight = 1;
            this.chlbRestrictions.FormattingEnabled = true;
            this.chlbRestrictions.IntegralHeight = false;
            this.chlbRestrictions.Location = new System.Drawing.Point(3, 3);
            this.chlbRestrictions.Name = "chlbRestrictions";
            this.chlbRestrictions.Size = new System.Drawing.Size(200, 21);
            this.chlbRestrictions.TabIndex = 0;
            this.chlbRestrictions.ValueSeparator = ", ";
            // 
            // grpChosenRestrictions
            // 
            this.grpChosenRestrictions.Controls.Add(this.lstSelectedRestrictions);
            this.grpChosenRestrictions.Location = new System.Drawing.Point(404, 290);
            this.grpChosenRestrictions.Name = "grpChosenRestrictions";
            this.grpChosenRestrictions.Size = new System.Drawing.Size(306, 108);
            this.grpChosenRestrictions.TabIndex = 404;
            this.grpChosenRestrictions.TabStop = false;
            this.grpChosenRestrictions.Text = "Selected Restrictions For Sending To Customers";
            this.grpChosenRestrictions.Visible = false;
            // 
            // lstSelectedRestrictions
            // 
            this.lstSelectedRestrictions.FormattingEnabled = true;
            this.lstSelectedRestrictions.Location = new System.Drawing.Point(6, 19);
            this.lstSelectedRestrictions.Name = "lstSelectedRestrictions";
            this.lstSelectedRestrictions.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lstSelectedRestrictions.Size = new System.Drawing.Size(293, 82);
            this.lstSelectedRestrictions.TabIndex = 402;
            // 
            // chbShadeGroups
            // 
            this.chbShadeGroups.AutoSize = true;
            this.chbShadeGroups.Location = new System.Drawing.Point(227, 428);
            this.chbShadeGroups.Name = "chbShadeGroups";
            this.chbShadeGroups.Size = new System.Drawing.Size(94, 17);
            this.chbShadeGroups.TabIndex = 384;
            this.chbShadeGroups.Text = "Shade Groups";
            this.chbShadeGroups.UseVisualStyleBackColor = true;
            // 
            // FrmSendPackageToCustomers
            // 
            this.AcceptButton = this.btnSend;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(733, 524);
            this.Controls.Add(this.grpChosenRestrictions);
            this.Controls.Add(this.panRestrictions);
            this.Controls.Add(this.grpSendCancel);
            this.Controls.Add(this.panSportTypes);
            this.Controls.Add(this.chbDetailLimits);
            this.Controls.Add(this.grpSendToAgents);
            this.Controls.Add(this.chbRestrictions);
            this.Controls.Add(this.grpChosenSports);
            this.Controls.Add(this.panPackageOptions);
            this.Controls.Add(this.btnUncheckAll);
            this.Controls.Add(this.btnCheckAll);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSendPackageToCustomers";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Send Package To Customers";
            this.Load += new System.EventHandler(this.frmSendPackageToCustomers_Load);
            this.panPackageOptions.ResumeLayout(false);
            this.panPackageOptions.PerformLayout();
            this.panSportTypes.ResumeLayout(false);
            this.grpChosenSports.ResumeLayout(false);
            this.grpSendToAgents.ResumeLayout(false);
            this.grpSendToAgents.PerformLayout();
            this.grpSendCancel.ResumeLayout(false);
            this.panRestrictions.ResumeLayout(false);
            this.grpChosenRestrictions.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chbDetailLimits;
        private System.Windows.Forms.CheckBox chbParlayMaxBet;
        private System.Windows.Forms.CheckBox chbDetailHorseLimits;
        private System.Windows.Forms.CheckBox chbMaxTeaserBet;
        private System.Windows.Forms.CheckBox chbAllTeasers;
        private System.Windows.Forms.CheckBox chbVigDiscounts;
        private System.Windows.Forms.CheckBox chbBaseballActionListed;
        private System.Windows.Forms.CheckBox chbPuckCanadianLineSetting;
        private System.Windows.Forms.CheckBox chbZeroBalanceFlag;
        private System.Windows.Forms.CheckBox chbSportsbookSuspendWagering;
        private System.Windows.Forms.CheckBox chbParlaySchedule;
        private System.Windows.Forms.CheckBox chbCostToBuyPoints;
        private System.Windows.Forms.CheckBox chbStore;
        private System.Windows.Forms.CheckBox chbInternetMinBet;
        private System.Windows.Forms.CheckBox chbEnableRollingIfBets;
        private System.Windows.Forms.CheckBox chbInetTarget;
        private System.Windows.Forms.CheckBox chbEnforceAccumGameLimits;
        private System.Windows.Forms.CheckBox chbCurrency;
        private System.Windows.Forms.CheckBox chbParlayMaxPayout;
        private System.Windows.Forms.CheckBox chbHorseMaxPayout;
        private System.Windows.Forms.CheckBox chbMaxContestBet;
        private System.Windows.Forms.CheckBox chbWagerLimitOfferingPage;
        private System.Windows.Forms.CheckBox chbFreeHalfPoints;
        private System.Windows.Forms.CheckBox chbCreditAccountingFlag;
        private System.Windows.Forms.CheckBox chbStaticLinesSetting;
        private System.Windows.Forms.CheckBox chbChartPercentBook;
        private System.Windows.Forms.CheckBox chbCasinoSuspendWagering;
        private System.Windows.Forms.CheckBox chbSourceInformation;
        private System.Windows.Forms.CheckBox chbHorseTrackRestrictions;
        private System.Windows.Forms.CheckBox chbCallUnitMinBet;
        private System.Windows.Forms.CheckBox chbTimeZone;
        private System.Windows.Forms.CheckBox chbLimitRIFToRisk;
        private System.Windows.Forms.CheckBox chbBetServiceProfiles;
        private System.Windows.Forms.CheckBox chbPriceType;
        private System.Windows.Forms.Button btnCheckAll;
        private System.Windows.Forms.Button btnUncheckAll;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel panPackageOptions;
        private System.Windows.Forms.Panel panSportTypes;
        private System.Windows.Forms.CheckBox chbAffectAgents;
        private System.Windows.Forms.ListBox lstSelectedSports;
        private System.Windows.Forms.GroupBox grpChosenSports;
        private System.Windows.Forms.GroupBox grpSendToAgents;
        private System.Windows.Forms.GroupBox grpSendCancel;
        private GUILibraries.Controls.CheckedComboBox chlbSports;
        private System.Windows.Forms.CheckBox chbRestrictions;
        private System.Windows.Forms.CheckBox chbCreditLimit;
        private System.Windows.Forms.CheckBox chbDisabledSports;
        private System.Windows.Forms.Panel panRestrictions;
        private GUILibraries.Controls.CheckedComboBox chlbRestrictions;
        private System.Windows.Forms.GroupBox grpChosenRestrictions;
        private System.Windows.Forms.ListBox lstSelectedRestrictions;
        private System.Windows.Forms.CheckBox chbShadeGroups;
    }
}