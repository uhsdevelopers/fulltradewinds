﻿namespace CustomerMaintenance.UI
{
    partial class FrmSearchCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblInstructions = new System.Windows.Forms.Label();
            this.panButtons = new System.Windows.Forms.Panel();
            this.btnBusinessPhone = new System.Windows.Forms.Button();
            this.btnHomePhone = new System.Windows.Forms.Button();
            this.btnCustomerID = new System.Windows.Forms.Button();
            this.btnOpenedBy = new System.Windows.Forms.Button();
            this.btnPassword = new System.Windows.Forms.Button();
            this.btnEMail = new System.Windows.Forms.Button();
            this.btnAgentID = new System.Windows.Forms.Button();
            this.btnNameFirst = new System.Windows.Forms.Button();
            this.btnNameLast = new System.Windows.Forms.Button();
            this.txtSearchBy = new System.Windows.Forms.TextBox();
            this.lblFind = new System.Windows.Forms.Label();
            this.btnSelect = new System.Windows.Forms.Button();
            this.dgvwSearchResults = new System.Windows.Forms.DataGridView();
            this.panButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvwSearchResults)).BeginInit();
            this.SuspendLayout();
            // 
            // lblInstructions
            // 
            this.lblInstructions.AutoSize = true;
            this.lblInstructions.Location = new System.Drawing.Point(110, 24);
            this.lblInstructions.Name = "lblInstructions";
            this.lblInstructions.Size = new System.Drawing.Size(17, 13);
            this.lblInstructions.TabIndex = 0;
            this.lblInstructions.Text = "lbl";
            this.lblInstructions.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panButtons
            // 
            this.panButtons.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panButtons.Controls.Add(this.btnBusinessPhone);
            this.panButtons.Controls.Add(this.btnHomePhone);
            this.panButtons.Controls.Add(this.btnCustomerID);
            this.panButtons.Controls.Add(this.btnOpenedBy);
            this.panButtons.Controls.Add(this.btnPassword);
            this.panButtons.Controls.Add(this.btnEMail);
            this.panButtons.Controls.Add(this.btnAgentID);
            this.panButtons.Controls.Add(this.btnNameFirst);
            this.panButtons.Controls.Add(this.btnNameLast);
            this.panButtons.Controls.Add(this.txtSearchBy);
            this.panButtons.Controls.Add(this.lblFind);
            this.panButtons.Location = new System.Drawing.Point(43, 93);
            this.panButtons.Name = "panButtons";
            this.panButtons.Size = new System.Drawing.Size(511, 155);
            this.panButtons.TabIndex = 10;
            // 
            // btnBusinessPhone
            // 
            this.btnBusinessPhone.Location = new System.Drawing.Point(309, 111);
            this.btnBusinessPhone.Name = "btnBusinessPhone";
            this.btnBusinessPhone.Size = new System.Drawing.Size(99, 23);
            this.btnBusinessPhone.TabIndex = 110;
            this.btnBusinessPhone.Text = "Business Phone";
            this.btnBusinessPhone.UseVisualStyleBackColor = true;
            this.btnBusinessPhone.Click += new System.EventHandler(this.btnOption_Click);
            // 
            // btnHomePhone
            // 
            this.btnHomePhone.Location = new System.Drawing.Point(207, 111);
            this.btnHomePhone.Name = "btnHomePhone";
            this.btnHomePhone.Size = new System.Drawing.Size(99, 23);
            this.btnHomePhone.TabIndex = 100;
            this.btnHomePhone.Text = "Home Phone";
            this.btnHomePhone.UseVisualStyleBackColor = true;
            this.btnHomePhone.Click += new System.EventHandler(this.btnOption_Click);
            // 
            // btnCustomerID
            // 
            this.btnCustomerID.Location = new System.Drawing.Point(99, 111);
            this.btnCustomerID.Name = "btnCustomerID";
            this.btnCustomerID.Size = new System.Drawing.Size(99, 23);
            this.btnCustomerID.TabIndex = 90;
            this.btnCustomerID.Text = "CustomerID";
            this.btnCustomerID.UseVisualStyleBackColor = true;
            this.btnCustomerID.Click += new System.EventHandler(this.btnOption_Click);
            // 
            // btnOpenedBy
            // 
            this.btnOpenedBy.Location = new System.Drawing.Point(309, 82);
            this.btnOpenedBy.Name = "btnOpenedBy";
            this.btnOpenedBy.Size = new System.Drawing.Size(99, 23);
            this.btnOpenedBy.TabIndex = 80;
            this.btnOpenedBy.Text = "Opened By";
            this.btnOpenedBy.UseVisualStyleBackColor = true;
            this.btnOpenedBy.Click += new System.EventHandler(this.btnOption_Click);
            // 
            // btnPassword
            // 
            this.btnPassword.Location = new System.Drawing.Point(207, 82);
            this.btnPassword.Name = "btnPassword";
            this.btnPassword.Size = new System.Drawing.Size(99, 23);
            this.btnPassword.TabIndex = 70;
            this.btnPassword.Text = "Password";
            this.btnPassword.UseVisualStyleBackColor = true;
            this.btnPassword.Click += new System.EventHandler(this.btnOption_Click);
            // 
            // btnEMail
            // 
            this.btnEMail.Location = new System.Drawing.Point(99, 82);
            this.btnEMail.Name = "btnEMail";
            this.btnEMail.Size = new System.Drawing.Size(99, 23);
            this.btnEMail.TabIndex = 60;
            this.btnEMail.Text = "E-mail";
            this.btnEMail.UseVisualStyleBackColor = true;
            this.btnEMail.Click += new System.EventHandler(this.btnOption_Click);
            // 
            // btnAgentID
            // 
            this.btnAgentID.Location = new System.Drawing.Point(309, 53);
            this.btnAgentID.Name = "btnAgentID";
            this.btnAgentID.Size = new System.Drawing.Size(99, 23);
            this.btnAgentID.TabIndex = 50;
            this.btnAgentID.Text = "Agent";
            this.btnAgentID.UseVisualStyleBackColor = true;
            this.btnAgentID.Click += new System.EventHandler(this.btnOption_Click);
            // 
            // btnNameFirst
            // 
            this.btnNameFirst.Location = new System.Drawing.Point(207, 53);
            this.btnNameFirst.Name = "btnNameFirst";
            this.btnNameFirst.Size = new System.Drawing.Size(99, 23);
            this.btnNameFirst.TabIndex = 40;
            this.btnNameFirst.Text = "First Name";
            this.btnNameFirst.UseVisualStyleBackColor = true;
            this.btnNameFirst.Click += new System.EventHandler(this.btnOption_Click);
            // 
            // btnNameLast
            // 
            this.btnNameLast.Location = new System.Drawing.Point(99, 53);
            this.btnNameLast.Name = "btnNameLast";
            this.btnNameLast.Size = new System.Drawing.Size(99, 23);
            this.btnNameLast.TabIndex = 30;
            this.btnNameLast.Text = "Last Name";
            this.btnNameLast.UseVisualStyleBackColor = true;
            this.btnNameLast.Click += new System.EventHandler(this.btnOption_Click);
            // 
            // txtSearchBy
            // 
            this.txtSearchBy.Location = new System.Drawing.Point(168, 11);
            this.txtSearchBy.Name = "txtSearchBy";
            this.txtSearchBy.Size = new System.Drawing.Size(207, 20);
            this.txtSearchBy.TabIndex = 20;
            // 
            // lblFind
            // 
            this.lblFind.AutoSize = true;
            this.lblFind.Location = new System.Drawing.Point(132, 14);
            this.lblFind.Name = "lblFind";
            this.lblFind.Size = new System.Drawing.Size(30, 13);
            this.lblFind.TabIndex = 0;
            this.lblFind.Text = "Find:";
            // 
            // btnSelect
            // 
            this.btnSelect.Location = new System.Drawing.Point(249, 523);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(99, 23);
            this.btnSelect.TabIndex = 130;
            this.btnSelect.Text = "Select";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // dgvwSearchResults
            // 
            this.dgvwSearchResults.AllowUserToAddRows = false;
            this.dgvwSearchResults.AllowUserToDeleteRows = false;
            this.dgvwSearchResults.AllowUserToResizeColumns = false;
            this.dgvwSearchResults.AllowUserToResizeRows = false;
            this.dgvwSearchResults.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvwSearchResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvwSearchResults.Location = new System.Drawing.Point(43, 267);
            this.dgvwSearchResults.MultiSelect = false;
            this.dgvwSearchResults.Name = "dgvwSearchResults";
            this.dgvwSearchResults.ReadOnly = true;
            this.dgvwSearchResults.RowHeadersVisible = false;
            this.dgvwSearchResults.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvwSearchResults.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvwSearchResults.Size = new System.Drawing.Size(511, 240);
            this.dgvwSearchResults.TabIndex = 120;
            this.dgvwSearchResults.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvwSearchResults_CellDoubleClick);
            // 
            // frmSearchCustomer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(596, 562);
            this.Controls.Add(this.dgvwSearchResults);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.panButtons);
            this.Controls.Add(this.lblInstructions);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSearchCustomer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Search Customer";
            this.Load += new System.EventHandler(this.frmSearchCustomer_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmSearchCustomer_KeyDown);
            this.panButtons.ResumeLayout(false);
            this.panButtons.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvwSearchResults)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblInstructions;
        private System.Windows.Forms.Panel panButtons;
        private System.Windows.Forms.Button btnBusinessPhone;
        private System.Windows.Forms.Button btnHomePhone;
        private System.Windows.Forms.Button btnCustomerID;
        private System.Windows.Forms.Button btnOpenedBy;
        private System.Windows.Forms.Button btnPassword;
        private System.Windows.Forms.Button btnEMail;
        private System.Windows.Forms.Button btnAgentID;
        private System.Windows.Forms.Button btnNameFirst;
        private System.Windows.Forms.Button btnNameLast;
        private System.Windows.Forms.TextBox txtSearchBy;
        private System.Windows.Forms.Label lblFind;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.DataGridView dgvwSearchResults;
    }
}