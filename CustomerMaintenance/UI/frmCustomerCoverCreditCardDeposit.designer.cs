﻿namespace CustomerMaintenance.UI
{
    partial class FrmCustomerCoverCreditCardDeposit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCCInfoDateRangeSelection = new System.Windows.Forms.Label();
            this.cmbStartingCCDepositsDate = new System.Windows.Forms.ComboBox();
            this.lblDays = new System.Windows.Forms.Label();
            this.panCreditCardDeposits = new System.Windows.Forms.Panel();
            this.dgvwCreditCardDeposits = new System.Windows.Forms.DataGridView();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblMultipleDepositsInstructions1 = new System.Windows.Forms.Label();
            this.lblMultipleDepositsInstructions2 = new System.Windows.Forms.Label();
            this.panCreditCardDeposits.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvwCreditCardDeposits)).BeginInit();
            this.SuspendLayout();
            // 
            // lblCCInfoDateRangeSelection
            // 
            this.lblCCInfoDateRangeSelection.AutoSize = true;
            this.lblCCInfoDateRangeSelection.Location = new System.Drawing.Point(77, 13);
            this.lblCCInfoDateRangeSelection.Name = "lblCCInfoDateRangeSelection";
            this.lblCCInfoDateRangeSelection.Size = new System.Drawing.Size(184, 13);
            this.lblCCInfoDateRangeSelection.TabIndex = 0;
            this.lblCCInfoDateRangeSelection.Text = "Show credit card deposits for the last ";
            // 
            // cmbStartingCCDepositsDate
            // 
            this.cmbStartingCCDepositsDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStartingCCDepositsDate.FormattingEnabled = true;
            this.cmbStartingCCDepositsDate.Items.AddRange(new object[] {
            "30",
            "60",
            "90",
            "120",
            "150",
            "180",
            "210",
            "240",
            "270",
            "300",
            "330",
            "366"});
            this.cmbStartingCCDepositsDate.Location = new System.Drawing.Point(258, 10);
            this.cmbStartingCCDepositsDate.Name = "cmbStartingCCDepositsDate";
            this.cmbStartingCCDepositsDate.Size = new System.Drawing.Size(49, 21);
            this.cmbStartingCCDepositsDate.TabIndex = 10;
            this.cmbStartingCCDepositsDate.SelectedIndexChanged += new System.EventHandler(this.cmbStartingCCDepositsDate_SelectedIndexChanged);
            // 
            // lblDays
            // 
            this.lblDays.AutoSize = true;
            this.lblDays.Location = new System.Drawing.Point(309, 13);
            this.lblDays.Name = "lblDays";
            this.lblDays.Size = new System.Drawing.Size(29, 13);
            this.lblDays.TabIndex = 20;
            this.lblDays.Text = "days";
            // 
            // panCreditCardDeposits
            // 
            this.panCreditCardDeposits.Controls.Add(this.dgvwCreditCardDeposits);
            this.panCreditCardDeposits.Location = new System.Drawing.Point(12, 42);
            this.panCreditCardDeposits.Name = "panCreditCardDeposits";
            this.panCreditCardDeposits.Size = new System.Drawing.Size(437, 131);
            this.panCreditCardDeposits.TabIndex = 30;
            // 
            // dgvwCreditCardDeposits
            // 
            this.dgvwCreditCardDeposits.AllowUserToAddRows = false;
            this.dgvwCreditCardDeposits.AllowUserToDeleteRows = false;
            this.dgvwCreditCardDeposits.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvwCreditCardDeposits.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvwCreditCardDeposits.Location = new System.Drawing.Point(4, 4);
            this.dgvwCreditCardDeposits.Name = "dgvwCreditCardDeposits";
            this.dgvwCreditCardDeposits.ReadOnly = true;
            this.dgvwCreditCardDeposits.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvwCreditCardDeposits.Size = new System.Drawing.Size(430, 124);
            this.dgvwCreditCardDeposits.TabIndex = 40;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(128, 227);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 70;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(232, 227);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 80;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblMultipleDepositsInstructions1
            // 
            this.lblMultipleDepositsInstructions1.AutoSize = true;
            this.lblMultipleDepositsInstructions1.Location = new System.Drawing.Point(94, 186);
            this.lblMultipleDepositsInstructions1.Name = "lblMultipleDepositsInstructions1";
            this.lblMultipleDepositsInstructions1.Size = new System.Drawing.Size(291, 13);
            this.lblMultipleDepositsInstructions1.TabIndex = 50;
            this.lblMultipleDepositsInstructions1.Text = "To cover multiple deposits, hold the \"CTRL\" key down while";
            // 
            // lblMultipleDepositsInstructions2
            // 
            this.lblMultipleDepositsInstructions2.AutoSize = true;
            this.lblMultipleDepositsInstructions2.Location = new System.Drawing.Point(94, 202);
            this.lblMultipleDepositsInstructions2.Name = "lblMultipleDepositsInstructions2";
            this.lblMultipleDepositsInstructions2.Size = new System.Drawing.Size(181, 13);
            this.lblMultipleDepositsInstructions2.TabIndex = 60;
            this.lblMultipleDepositsInstructions2.Text = "clicking on the Items with the mouse.";
            // 
            // frmCustomerCoverCreditCardDeposit
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(461, 262);
            this.Controls.Add(this.lblMultipleDepositsInstructions2);
            this.Controls.Add(this.lblMultipleDepositsInstructions1);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.panCreditCardDeposits);
            this.Controls.Add(this.lblDays);
            this.Controls.Add(this.cmbStartingCCDepositsDate);
            this.Controls.Add(this.lblCCInfoDateRangeSelection);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCustomerCoverCreditCardDeposit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Cover Credit Card Deposit";
            this.panCreditCardDeposits.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvwCreditCardDeposits)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCCInfoDateRangeSelection;
        private System.Windows.Forms.ComboBox cmbStartingCCDepositsDate;
        private System.Windows.Forms.Label lblDays;
        private System.Windows.Forms.Panel panCreditCardDeposits;
        private System.Windows.Forms.DataGridView dgvwCreditCardDeposits;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblMultipleDepositsInstructions1;
        private System.Windows.Forms.Label lblMultipleDepositsInstructions2;
    }
}