﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using CustomerMaintenance.Libraries;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using GUILibraries.Forms;

namespace CustomerMaintenance.UI {
  public partial class FrmCustomerAuditDetails : SIDForm {

    #region Classes

    private class CustomerAuditInfo {
      public Double? PreviousDoubleValue { get; set; }
      public int? PreviousIntValue { private get; set; }
      public String PreviousStringValue { get; set; }
      public String LastChangedBy { get; set; }
      public DateTime LastChangedByDateTime { get; set; }
      public DateTime TempCreditAdjExpDate { get; set; }
      public Double? PreviousDoubleCuLimitValue { get; set; }
      public Double? PreviousDoubleInetLimitValue { get; set; }
      public Double? PreviousDoubleTempCuLimitValue { get; set; }
      public Double? PreviousDoubleTempInetLimitValue { get; set; }
    }

    #endregion

    #region Private Vars

    private readonly AuditTargetItem _infoToShow;
    private readonly String _currentCustomerId = "";
    private readonly DataGridViewRow _wagerDetailsInfo;

    string _sportType = "";
    string _sportSubType = "";
    string _periodDescription = "";
    int _periodNumber = -1;
    string _wagerType = "";
    string _wagerTypeName = "";

    #endregion

    #region Constructors

    public FrmCustomerAuditDetails(AuditTargetItem infoToShow, String customerId, ModuleInfo moduleInfo)
      : base(moduleInfo) {
      if (!string.IsNullOrEmpty(customerId)) {
        _infoToShow = infoToShow;
        _currentCustomerId = customerId;
        InitializeComponent();
        LoadAuditInfo(false);
      }
      else {
        MessageBox.Show(@"No customer was selected");
        Close();
      }
    }

    public FrmCustomerAuditDetails(AuditTargetItem infoToShow, String customerId, ModuleInfo moduleInfo, DataGridViewRow wagerDetailsInfo, Boolean showForParlayLimits = false)
      : base(moduleInfo) {
      if (!string.IsNullOrEmpty(customerId)) {
        _infoToShow = infoToShow;
        _currentCustomerId = customerId;
        _wagerDetailsInfo = wagerDetailsInfo;
        InitializeComponent();
        GetSportAdditionalInfo();
        if (showForParlayLimits) {
          LoadParlayWagerLimitsAuditInfo(false);
        }
        else {
          LoadWagerLimitsAuditInfo(false);
        }
      }
      else {
        MessageBox.Show(@"No customer was selected");
        Close();
      }
    }
    #endregion

    #region Private Methods

    private void AdjustFormTitle() {
      Text += _infoToShow.Description;
    }

    private void FillAuditGrid(IEnumerable<CustomerAuditInfo> auditList) {
      WriteGridHeaders();
      if (dgvwAuditInfo.Rows.Count > 0)
        dgvwAuditInfo.Rows.Clear();

      foreach (var item in auditList) {
        switch (_infoToShow.Code) {
          case AuditTargetItems.SETTLE_FIGURE:
            dgvwAuditInfo.Rows.Add(FormatNumber(item.PreviousDoubleValue ?? 0, false), item.LastChangedBy.Trim(), item.LastChangedByDateTime);
            break;
          case AuditTargetItems.TEMP_CREDIT_ADJ:
            dgvwAuditInfo.Rows.Add(FormatNumber(item.PreviousDoubleValue ?? 0, 100, false), item.TempCreditAdjExpDate, item.LastChangedBy.Trim(), item.LastChangedByDateTime);
            Size = new Size(500, 330);
            dgvwAuditInfo.Width = 500;
            break;
          case AuditTargetItems.CREDIT_LIMIT:
          case AuditTargetItems.QUICK_WAGER_LIMIT:
          case AuditTargetItems.TEMP_QUICK_WAGER_LIMIT:
          case AuditTargetItems.CU_MINIMUM_WAGER:
          case AuditTargetItems.INET_MINIMUM_WAGER:
          case AuditTargetItems.PARLAY_MAX_BET:
          case AuditTargetItems.PARLAY_MAX_PAYOUT:
          case AuditTargetItems.TEASER_MAX_BET:
          case AuditTargetItems.CONTEST_MAX_BET:
          case AuditTargetItems.DET_WAGER_LIMITS_CU:
          case AuditTargetItems.DET_WAGER_LIMITS_INET:
            dgvwAuditInfo.Rows.Add(FormatNumber(item.PreviousDoubleValue ?? 0, 100), item.LastChangedBy.Trim(), item.LastChangedByDateTime);
            break;
          case AuditTargetItems.PASSWORD:
          case AuditTargetItems.PAYOUT_PASSWORD:
            dgvwAuditInfo.Rows.Add(item.PreviousStringValue, item.LastChangedBy.Trim(), item.LastChangedByDateTime);
            break;
          case AuditTargetItems.DET_WAGER_LIMITS:
            dgvwAuditInfo.Rows.Add(FormatNumber(item.PreviousDoubleCuLimitValue ?? 0 / 100, false), FormatNumber(item.PreviousDoubleInetLimitValue ?? 0 / 100, false),
              FormatNumber(item.PreviousDoubleTempCuLimitValue ?? 0 / 100, false), FormatNumber(item.PreviousDoubleTempInetLimitValue ?? 0 / 100, false),
              item.LastChangedBy.Trim(), item.LastChangedByDateTime);
            Size = new Size(455, 330);
            dgvwAuditInfo.Width = 455;
            break;
          case AuditTargetItems.DET_PARLAY_WAGER_LIMITS:
            dgvwAuditInfo.Rows.Add(FormatNumber(item.PreviousDoubleCuLimitValue ?? 0 / 100, false), FormatNumber(item.PreviousDoubleInetLimitValue ?? 0 / 100, false),
            item.LastChangedBy.Trim(), item.LastChangedByDateTime);
            Size = new Size(455, 330);
            dgvwAuditInfo.Width = 455;
            break;
        }
      }

    }

    private void GetSportAdditionalInfo() {
      if (_wagerDetailsInfo == null) return;
      if (_wagerDetailsInfo.Cells[FrmCustomerMaintenance.SPORT_SUB_SPORT_INDEX].Value != null)
      {
          _sportType = _wagerDetailsInfo.Cells[FrmCustomerMaintenance.SPORT_INDEX].Value.ToString().Trim();
          _sportSubType = _wagerDetailsInfo.Cells[FrmCustomerMaintenance.SUB_SPORT_INDEX].Value.ToString().Trim();
      }
      if (_wagerDetailsInfo.Cells[FrmCustomerMaintenance.PERIOD_NUMBER_INDEX].Value != null)
      {
          _periodNumber = int.Parse(_wagerDetailsInfo.Cells[FrmCustomerMaintenance.PERIOD_NUMBER_INDEX].Value.ToString());
      }

      if (_wagerDetailsInfo.Cells[FrmCustomerMaintenance.WAGER_TYPE_CODE_INDEX].Value != null)
      {
          _wagerType = _wagerDetailsInfo.Cells[FrmCustomerMaintenance.WAGER_TYPE_CODE_INDEX].Value.ToString();
      }

      if (_wagerDetailsInfo.Cells[FrmCustomerMaintenance.PERIOD_INDEX].Value != null)
      {
          _periodDescription = _wagerDetailsInfo.Cells[FrmCustomerMaintenance.PERIOD_INDEX].Value.ToString();
      }

      if (_wagerDetailsInfo.Cells[FrmCustomerMaintenance.TYPE_INDEX].Value != null)
      {
          _wagerTypeName = _wagerDetailsInfo.Cells[FrmCustomerMaintenance.TYPE_INDEX].Value.ToString();
      }
    }

    private void LoadAuditInfo(bool showArchiveInfo) {
      List<spCstGetAuditCustomerInfo_Result> auditInfo;
      using (var cst = new Customers(AppModuleInfo)) {
        auditInfo = cst.GetAuditCustomerInfo(_currentCustomerId, AppModuleInfo.InstanceInfo.DatabaseName, showArchiveInfo).ToList();
      }

      if (auditInfo.Count <= 0) return;
      var auditList = new List<CustomerAuditInfo>();

      switch (_infoToShow.Code) {
        case AuditTargetItems.SETTLE_FIGURE:
          auditList = (from a in auditInfo
                       where a.ModifiedColumn != null && a.ModifiedColumn.Trim() == _infoToShow.Code
                       group a by new { a.SettleFigure, a.LastChangedBy, a.LastChangedByDateTime } into grp
                       select new
                         CustomerAuditInfo {
                           LastChangedBy = grp.Key.LastChangedBy,
                           PreviousDoubleValue = grp.Key.SettleFigure,
                           LastChangedByDateTime = grp.Key.LastChangedByDateTime ?? DateTime.Now
                         }).OrderByDescending(k => k.LastChangedByDateTime).ToList();
          break;
        case AuditTargetItems.PASSWORD:
          auditList = (from a in auditInfo
                       where a.ModifiedColumn != null && a.ModifiedColumn.Trim() == _infoToShow.Code
                       group a by new { a.Password, a.LastChangedBy, a.LastChangedByDateTime } into grp
                       select new
                         CustomerAuditInfo {
                           LastChangedBy = grp.Key.LastChangedBy,
                           PreviousStringValue = grp.Key.Password,
                           LastChangedByDateTime = grp.Key.LastChangedByDateTime ?? DateTime.Now
                         }).OrderByDescending(k => k.LastChangedByDateTime).ToList();
          break;
        case AuditTargetItems.PAYOUT_PASSWORD:
          auditList = (from a in auditInfo
                       where a.ModifiedColumn != null && a.ModifiedColumn.Trim() == _infoToShow.Code
                       group a by new { a.PayoutPassword, a.LastChangedBy, a.LastChangedByDateTime } into grp
                       select new
                         CustomerAuditInfo {
                           LastChangedBy = grp.Key.LastChangedBy,
                           PreviousStringValue = grp.Key.PayoutPassword,
                           LastChangedByDateTime = grp.Key.LastChangedByDateTime ?? DateTime.Now
                         }).OrderByDescending(k => k.LastChangedByDateTime).ToList();
          break;
        case AuditTargetItems.CREDIT_LIMIT:
          auditList = (from a in auditInfo
                       where a.ModifiedColumn != null && a.ModifiedColumn.Trim() == _infoToShow.Code
                       group a by new { a.CreditLimit, a.LastChangedBy, a.LastChangedByDateTime } into grp
                       select new
                         CustomerAuditInfo {
                           LastChangedBy = grp.Key.LastChangedBy,
                           PreviousDoubleValue = grp.Key.CreditLimit,
                           LastChangedByDateTime = grp.Key.LastChangedByDateTime ?? DateTime.Now
                         }).OrderByDescending(k => k.LastChangedByDateTime).ToList();
          break;
        case AuditTargetItems.TEMP_CREDIT_ADJ:
          auditList = (from a in auditInfo
                       where a.ModifiedColumn != null && a.ModifiedColumn.Trim() == _infoToShow.Code
                       group a by new { a.TempCreditAdj, a.TempCreditAdjExpDate, a.LastChangedBy, a.LastChangedByDateTime } into grp
                       select new
                         CustomerAuditInfo {
                           LastChangedBy = grp.Key.LastChangedBy,
                           PreviousDoubleValue = grp.Key.TempCreditAdj,
                           TempCreditAdjExpDate = grp.Key.TempCreditAdjExpDate ?? DateTime.Now,
                           LastChangedByDateTime = grp.Key.LastChangedByDateTime ?? DateTime.Now
                         }).OrderByDescending(k => k.LastChangedByDateTime).ToList();
          break;
        case AuditTargetItems.QUICK_WAGER_LIMIT:
          auditList = (from a in auditInfo
                       where a.ModifiedColumn != null && a.ModifiedColumn.Trim().ToLower() == _infoToShow.Code.ToLower()
                       group a by new { a.WagerLimit, a.LastChangedBy, a.LastChangedByDateTime } into grp
                       select new
                         CustomerAuditInfo {
                           LastChangedBy = grp.Key.LastChangedBy,
                           PreviousDoubleValue = grp.Key.WagerLimit,
                           LastChangedByDateTime = grp.Key.LastChangedByDateTime ?? DateTime.Now
                         }).OrderByDescending(k => k.LastChangedByDateTime).ToList();
          break;
        case AuditTargetItems.TEMP_QUICK_WAGER_LIMIT:
          auditList = (from a in auditInfo
                       where a.ModifiedColumn != null && a.ModifiedColumn.Trim().ToLower() == _infoToShow.Code.ToLower()
                       group a by new { a.TempWagerLimit, a.LastChangedBy, a.LastChangedByDateTime } into grp
                       select new
                         CustomerAuditInfo {
                           LastChangedBy = grp.Key.LastChangedBy,
                           PreviousDoubleValue = grp.Key.TempWagerLimit,
                           LastChangedByDateTime = grp.Key.LastChangedByDateTime ?? DateTime.Now
                         }).OrderByDescending(k => k.LastChangedByDateTime).ToList();
          break;
        case AuditTargetItems.CU_MINIMUM_WAGER:
          auditList = (from a in auditInfo
                       where a.ModifiedColumn != null && a.ModifiedColumn.Trim() == _infoToShow.Code
                       group a by new { a.CUMinimumWager, a.LastChangedBy, a.LastChangedByDateTime } into grp
                       select new
                         CustomerAuditInfo {
                           LastChangedBy = grp.Key.LastChangedBy,
                           PreviousIntValue = grp.Key.CUMinimumWager,
                           LastChangedByDateTime = grp.Key.LastChangedByDateTime ?? DateTime.Now
                         }).OrderByDescending(k => k.LastChangedByDateTime).ToList();
          break;
        case AuditTargetItems.INET_MINIMUM_WAGER:
          auditList = (from a in auditInfo
                       where a.ModifiedColumn != null && a.ModifiedColumn.Trim() == _infoToShow.Code
                       group a by new { a.InetMinimumWager, a.LastChangedBy, a.LastChangedByDateTime } into grp
                       select new
                         CustomerAuditInfo {
                           LastChangedBy = grp.Key.LastChangedBy,
                           PreviousIntValue = grp.Key.InetMinimumWager,
                           LastChangedByDateTime = grp.Key.LastChangedByDateTime ?? DateTime.Now
                         }).OrderByDescending(k => k.LastChangedByDateTime).ToList();
          break;
        case AuditTargetItems.PARLAY_MAX_BET:
          auditList = (from a in auditInfo
                       where a.ModifiedColumn != null && a.ModifiedColumn.Trim() == _infoToShow.Code
                       group a by new { a.ParlayMaxBet, a.LastChangedBy, a.LastChangedByDateTime } into grp
                       select new
                         CustomerAuditInfo {
                           LastChangedBy = grp.Key.LastChangedBy,
                           PreviousDoubleValue = grp.Key.ParlayMaxBet,
                           LastChangedByDateTime = grp.Key.LastChangedByDateTime ?? DateTime.Now
                         }).OrderByDescending(k => k.LastChangedByDateTime).ToList();
          break;
        case AuditTargetItems.PARLAY_MAX_PAYOUT:
          auditList = (from a in auditInfo
                       where a.ModifiedColumn != null && a.ModifiedColumn.Trim() == _infoToShow.Code
                       group a by new { a.ParlayMaxPayout, a.LastChangedBy, a.LastChangedByDateTime } into grp
                       select new
                         CustomerAuditInfo {
                           LastChangedBy = grp.Key.LastChangedBy,
                           PreviousDoubleValue = grp.Key.ParlayMaxPayout,
                           LastChangedByDateTime = grp.Key.LastChangedByDateTime ?? DateTime.Now
                         }).OrderByDescending(k => k.LastChangedByDateTime).ToList();
          break;
        case AuditTargetItems.TEASER_MAX_BET:
          auditList = (from a in auditInfo
                       where a.ModifiedColumn != null && a.ModifiedColumn.Trim() == _infoToShow.Code
                       group a by new { a.TeaserMaxBet, a.LastChangedBy, a.LastChangedByDateTime } into grp
                       select new
                         CustomerAuditInfo {
                           LastChangedBy = grp.Key.LastChangedBy,
                           PreviousDoubleValue = grp.Key.TeaserMaxBet,
                           LastChangedByDateTime = grp.Key.LastChangedByDateTime ?? DateTime.Now
                         }).OrderByDescending(k => k.LastChangedByDateTime).ToList();
          break;
        case AuditTargetItems.CONTEST_MAX_BET:
          auditList = (from a in auditInfo
                       where a.ModifiedColumn != null && a.ModifiedColumn.Trim() == _infoToShow.Code
                       group a by new { a.ContestMaxBet, a.LastChangedBy, a.LastChangedByDateTime } into grp
                       select new
                         CustomerAuditInfo {
                           LastChangedBy = grp.Key.LastChangedBy,
                           PreviousDoubleValue = grp.Key.ContestMaxBet,
                           LastChangedByDateTime = grp.Key.LastChangedByDateTime ?? DateTime.Now
                         }).OrderByDescending(k => k.LastChangedByDateTime).ToList();
          break;
      }

      if (auditList.Count > 0) {
        FillAuditGrid(auditList);
      }
    }

    private void LoadWagerLimitsAuditInfo(bool showArchiveInfo) {
      List<spCstGetAuditCustomerWagerLimitsInfo_Result> auditInfo;

      using (var cst = new Customers(AppModuleInfo)) {
        auditInfo = cst.GetAuditCustomerWagerLimitsInfo(_currentCustomerId, _sportType, _sportSubType, _periodNumber, _wagerType, AppModuleInfo.InstanceInfo.DatabaseName, showArchiveInfo).ToList();
      }

      if (auditInfo.Count <= 0) return;
      var auditList = (from a in auditInfo
                       group a by new { a.CULimit, a.InetLimit, a.TempCULimit, a.TempInetLimit, a.TempLimitsExpiration, a.LastChangedBy, a.LastChangedByDateTime } into grp
                       select new
                         CustomerAuditInfo {
                           LastChangedBy = grp.Key.LastChangedBy,
                           PreviousDoubleCuLimitValue = grp.Key.CULimit,
                           PreviousDoubleInetLimitValue = grp.Key.InetLimit,
                           PreviousDoubleTempCuLimitValue = grp.Key.TempCULimit,
                           PreviousDoubleTempInetLimitValue = grp.Key.TempInetLimit,
                           LastChangedByDateTime = grp.Key.LastChangedByDateTime ?? DateTime.Now
                         }).OrderByDescending(k => k.LastChangedByDateTime).ToList();
      if (auditList.Count > 0) {
        FillAuditGrid(auditList);
      }
    }

    private void LoadParlayWagerLimitsAuditInfo(bool showArchiveInfo) {
      List<spCstGetAuditCustomerParlayWagerLimitsInfo_Result> auditInfo;

      using (var cst = new Customers(AppModuleInfo)) {
        auditInfo = cst.GetAuditCustomerParlayWagerLimitsInfo(_currentCustomerId, _sportType, _sportSubType, _periodNumber, _wagerType, AppModuleInfo.InstanceInfo.DatabaseName, showArchiveInfo).ToList();
      }
      if (auditInfo.Count <= 0) return;

      var auditList = (from a in auditInfo
                       group a by new { a.CULimit, a.InetLimit, a.LastChangedBy, a.LastChangedByDateTime } into grp
                       select new
                         CustomerAuditInfo {
                           LastChangedBy = grp.Key.LastChangedBy,
                           PreviousDoubleCuLimitValue = grp.Key.CULimit,
                           PreviousDoubleInetLimitValue = grp.Key.InetLimit,
                           LastChangedByDateTime = grp.Key.LastChangedByDateTime ?? DateTime.Now
                         }).OrderByDescending(k => k.LastChangedByDateTime).ToList();

      if (auditList.Count > 0) {
        FillAuditGrid(auditList);
      }

    }

    private void WriteGridHeaders() {
      dgvwAuditInfo.DataSource = null;

      var dgrvwColNames = (from DataGridViewTextBoxColumn col in dgvwAuditInfo.Columns select col.Name).ToList();

      foreach (var str in dgrvwColNames) {
        dgvwAuditInfo.Columns.Remove(str);
      }

      DataGridViewTextBoxColumn titleColumn;

      if (_infoToShow.Code == "DetailedWagerLimits" || _infoToShow.Code == "DetailedParlayWagerLimits") {
        titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Last CU Value", Width = 100 };
        dgvwAuditInfo.Columns.Add(titleColumn);
        titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Last Inet Value", Width = 100 };
        dgvwAuditInfo.Columns.Add(titleColumn);
        if (_infoToShow.Code == "DetailedWagerLimits") {
          titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Last Temp CU Value", Width = 100 };
          dgvwAuditInfo.Columns.Add(titleColumn);
          titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Last Temp Inet Value", Width = 100 };
          dgvwAuditInfo.Columns.Add(titleColumn);
        }
      }
      else {
        titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Last Value", Width = 100 };
        dgvwAuditInfo.Columns.Add(titleColumn);
      }

      if (_infoToShow.Code == "TempCreditAdj") {
        titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Through", Width = 125 };
        dgvwAuditInfo.Columns.Add(titleColumn);
      }



      titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Changed By", Width = 100 };
      dgvwAuditInfo.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Changed On", Width = 125 };
      dgvwAuditInfo.Columns.Add(titleColumn);
    }

    private void WriteWagerLimitsAuditDetails() {
      if (_infoToShow.Code != "DetailedWagerLimits") return;
      if (_wagerDetailsInfo != null) {
        lblWagerDetails.Text = @"Showing: " + _sportType + @" - " + _sportSubType + @", for " + _periodDescription + @" and " + _wagerTypeName;
      }
    }

    #endregion

    #region Events

    private void frmCustomerAuditDetails_Load(object sender, EventArgs e) {
      AdjustFormTitle();
      WriteWagerLimitsAuditDetails();
    }

    private void btnClose_Click(object sender, EventArgs e) {
      Close();
    }

    private void chbShowArchiveData_CheckedChanged(object sender, EventArgs e) {
      var showArchiveData = ((CheckBox)sender).Checked;
      LoadAuditInfo(showArchiveData);
    }

    #endregion
  }
}