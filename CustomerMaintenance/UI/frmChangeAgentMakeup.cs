﻿using System;
using System.Linq;
using System.Windows.Forms;
using GUILibraries.Controls;
using GUILibraries.Forms;
using SIDLibraries.Utilities;

namespace CustomerMaintenance.UI {
  public partial class FrmChangeAgentMakeup : SIDForm {
    readonly FrmCustomerMaintenance _parent;
    readonly double _currentAgentMakeUp;

    public FrmChangeAgentMakeup(SIDForm parentFrm, double currentMakeUp) : base(parentFrm.AppModuleInfo) {
      _parent = (FrmCustomerMaintenance)parentFrm;
      _currentAgentMakeUp = currentMakeUp;
      InitializeComponent();
    }

    private void btnCancel_Click(object sender, EventArgs e) {
      Close();
    }

    private void btnOk_Click(object sender, EventArgs e) {
      try {
        double newMakeUp;
        double.TryParse(txtNewMakeup.Text, out newMakeUp);
        var txtB = (NumberTextBox)_parent.Controls.Find("txtCurrentMakeup", true).FirstOrDefault();
        if (txtB != null) txtB.Text = FormatNumber(newMakeUp * txtB.DividedBy, false, 1, true);
        _parent.CurrentMakeUp = newMakeUp * 100;
        Close();
      }
      catch (Exception ex) {
        Log(ex);
      }
    }

    private void frmChangeAgentMakeup_Load(object sender, EventArgs e) {
      txtNewMakeup.Text = FormatNumber(_currentAgentMakeUp, true, 100, true);
    }

    private void numberToText_Leave(object sender, EventArgs e) {
      var ntxtB = (NumberTextBox)sender;

      if (ntxtB.Text != "") {

        ((TextBox)sender).Text = FormatNumber(DisplayNumber.RemoveFormat(((TextBox)sender).Text) * ntxtB.DividedBy, ntxtB.DivideFlag, ntxtB.DividedBy, ntxtB.ShowIfZero);
      }
    }
  }
}
