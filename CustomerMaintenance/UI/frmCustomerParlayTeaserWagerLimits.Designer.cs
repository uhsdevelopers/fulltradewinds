﻿namespace CustomerMaintenance.UI
{
    partial class FrmCustomerParlayTeaserWagerLimits
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbWLSports = new System.Windows.Forms.ComboBox();
            this.dgvwParlayTeaserWagerLimitsDetails = new System.Windows.Forms.DataGridView();
            this.grpBSports = new System.Windows.Forms.GroupBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnShowAuditWagerLimits = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvwParlayTeaserWagerLimitsDetails)).BeginInit();
            this.grpBSports.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbWLSports
            // 
            this.cmbWLSports.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbWLSports.FormattingEnabled = true;
            this.cmbWLSports.Location = new System.Drawing.Point(6, 19);
            this.cmbWLSports.Name = "cmbWLSports";
            this.cmbWLSports.Size = new System.Drawing.Size(145, 21);
            this.cmbWLSports.TabIndex = 132;
            this.cmbWLSports.SelectedIndexChanged += new System.EventHandler(this.cmbWLSports_SelectedIndexChanged);
            // 
            // dgvwParlayTeaserWagerLimitsDetails
            // 
            this.dgvwParlayTeaserWagerLimitsDetails.AllowUserToAddRows = false;
            this.dgvwParlayTeaserWagerLimitsDetails.AllowUserToDeleteRows = false;
            this.dgvwParlayTeaserWagerLimitsDetails.AllowUserToResizeRows = false;
            this.dgvwParlayTeaserWagerLimitsDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvwParlayTeaserWagerLimitsDetails.Location = new System.Drawing.Point(9, 67);
            this.dgvwParlayTeaserWagerLimitsDetails.Name = "dgvwParlayTeaserWagerLimitsDetails";
            this.dgvwParlayTeaserWagerLimitsDetails.RowHeadersVisible = false;
            this.dgvwParlayTeaserWagerLimitsDetails.Size = new System.Drawing.Size(513, 276);
            this.dgvwParlayTeaserWagerLimitsDetails.TabIndex = 133;
            this.dgvwParlayTeaserWagerLimitsDetails.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvwParlayTeaserWagerLimitsDetails_CellEndEdit);
            this.dgvwParlayTeaserWagerLimitsDetails.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvwParlayTeaserWagerLimitsDetails_CellValidating);
            this.dgvwParlayTeaserWagerLimitsDetails.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvwParlayTeaserWagerLimitsDetails_DataError);
            this.dgvwParlayTeaserWagerLimitsDetails.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvwParlayTeaserWagerLimitsDetails_EditingControlShowing);
            this.dgvwParlayTeaserWagerLimitsDetails.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvwParlayTeaserWagerLimitsDetails_KeyDown);
            // 
            // grpBSports
            // 
            this.grpBSports.Controls.Add(this.cmbWLSports);
            this.grpBSports.Location = new System.Drawing.Point(9, 8);
            this.grpBSports.Name = "grpBSports";
            this.grpBSports.Size = new System.Drawing.Size(200, 55);
            this.grpBSports.TabIndex = 134;
            this.grpBSports.TabStop = false;
            this.grpBSports.Text = "Select Sport";
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(273, 370);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 136;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(148, 370);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 135;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // btnShowAuditWagerLimits
            // 
            this.btnShowAuditWagerLimits.Location = new System.Drawing.Point(232, 38);
            this.btnShowAuditWagerLimits.Name = "btnShowAuditWagerLimits";
            this.btnShowAuditWagerLimits.Size = new System.Drawing.Size(75, 23);
            this.btnShowAuditWagerLimits.TabIndex = 137;
            this.btnShowAuditWagerLimits.Text = "History...";
            this.btnShowAuditWagerLimits.UseVisualStyleBackColor = true;
            this.btnShowAuditWagerLimits.Visible = false;
            this.btnShowAuditWagerLimits.Click += new System.EventHandler(this.btnShowAuditWagerLimits_Click);
            // 
            // FrmCustomerParlayTeaserWagerLimits
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(530, 419);
            this.Controls.Add(this.btnShowAuditWagerLimits);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.grpBSports);
            this.Controls.Add(this.dgvwParlayTeaserWagerLimitsDetails);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCustomerParlayTeaserWagerLimits";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Parlay/Teaser Wager Limits";
            this.Load += new System.EventHandler(this.frmCustomerParlayWagerLimits_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvwParlayTeaserWagerLimitsDetails)).EndInit();
            this.grpBSports.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbWLSports;
        private System.Windows.Forms.DataGridView dgvwParlayTeaserWagerLimitsDetails;
        private System.Windows.Forms.GroupBox grpBSports;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnShowAuditWagerLimits;
    }
}