﻿namespace CustomerMaintenance.UI {
  partial class FrmCustomerMessage {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
            this.lblSubject = new System.Windows.Forms.Label();
            this.txtSubject = new System.Windows.Forms.TextBox();
            this.lblBody = new System.Windows.Forms.Label();
            this.txtBody = new System.Windows.Forms.TextBox();
            this.lblExpirationDate = new System.Windows.Forms.Label();
            this.dtmExpirationDate = new System.Windows.Forms.DateTimePicker();
            this.lblStatus = new System.Windows.Forms.Label();
            this.chbStatus = new System.Windows.Forms.ComboBox();
            this.lblPriority = new System.Windows.Forms.Label();
            this.cmbPriority = new System.Windows.Forms.ComboBox();
            this.btnAccept = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.panStatus = new System.Windows.Forms.Panel();
            this.chbFlagAsDeleted = new System.Windows.Forms.CheckBox();
            this.panSendToPackage = new System.Windows.Forms.Panel();
            this.chbSendToPackage = new System.Windows.Forms.CheckBox();
            this.lblActionDesc = new System.Windows.Forms.Label();
            this.txtActionDesc = new System.Windows.Forms.TextBox();
            this.panStatus.SuspendLayout();
            this.panSendToPackage.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblSubject
            // 
            this.lblSubject.AutoSize = true;
            this.lblSubject.Location = new System.Drawing.Point(13, 13);
            this.lblSubject.Name = "lblSubject";
            this.lblSubject.Size = new System.Drawing.Size(46, 13);
            this.lblSubject.TabIndex = 0;
            this.lblSubject.Text = "Subject:";
            // 
            // txtSubject
            // 
            this.txtSubject.Location = new System.Drawing.Point(66, 13);
            this.txtSubject.Name = "txtSubject";
            this.txtSubject.Size = new System.Drawing.Size(529, 20);
            this.txtSubject.TabIndex = 1;
            // 
            // lblBody
            // 
            this.lblBody.AutoSize = true;
            this.lblBody.Location = new System.Drawing.Point(16, 58);
            this.lblBody.Name = "lblBody";
            this.lblBody.Size = new System.Drawing.Size(34, 13);
            this.lblBody.TabIndex = 2;
            this.lblBody.Text = "Body:";
            // 
            // txtBody
            // 
            this.txtBody.Location = new System.Drawing.Point(66, 50);
            this.txtBody.Multiline = true;
            this.txtBody.Name = "txtBody";
            this.txtBody.Size = new System.Drawing.Size(529, 113);
            this.txtBody.TabIndex = 3;
            // 
            // lblExpirationDate
            // 
            this.lblExpirationDate.AutoSize = true;
            this.lblExpirationDate.Location = new System.Drawing.Point(19, 202);
            this.lblExpirationDate.Name = "lblExpirationDate";
            this.lblExpirationDate.Size = new System.Drawing.Size(82, 13);
            this.lblExpirationDate.TabIndex = 4;
            this.lblExpirationDate.Text = "Expiration Date:";
            // 
            // dtmExpirationDate
            // 
            this.dtmExpirationDate.Location = new System.Drawing.Point(107, 202);
            this.dtmExpirationDate.Name = "dtmExpirationDate";
            this.dtmExpirationDate.Size = new System.Drawing.Size(200, 20);
            this.dtmExpirationDate.TabIndex = 5;
            this.dtmExpirationDate.ValueChanged += new System.EventHandler(this.dtmExpirationDate_ValueChanged);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(3, 25);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(40, 13);
            this.lblStatus.TabIndex = 6;
            this.lblStatus.Text = "Status:";
            this.lblStatus.Visible = false;
            // 
            // chbStatus
            // 
            this.chbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.chbStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.chbStatus.FormattingEnabled = true;
            this.chbStatus.Items.AddRange(new object[] {
            "Deleted",
            "Read",
            "Unread"});
            this.chbStatus.Location = new System.Drawing.Point(88, 22);
            this.chbStatus.Name = "chbStatus";
            this.chbStatus.Size = new System.Drawing.Size(121, 21);
            this.chbStatus.TabIndex = 7;
            this.chbStatus.Visible = false;
            // 
            // lblPriority
            // 
            this.lblPriority.AutoSize = true;
            this.lblPriority.Location = new System.Drawing.Point(25, 247);
            this.lblPriority.Name = "lblPriority";
            this.lblPriority.Size = new System.Drawing.Size(86, 13);
            this.lblPriority.TabIndex = 8;
            this.lblPriority.Text = "Message Action:";
            // 
            // cmbPriority
            // 
            this.cmbPriority.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPriority.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbPriority.FormattingEnabled = true;
            this.cmbPriority.Items.AddRange(new object[] {
            "Always Alert",
            "Alert once",
            "Inbox Only",
            "Suspended"});
            this.cmbPriority.Location = new System.Drawing.Point(126, 243);
            this.cmbPriority.Name = "cmbPriority";
            this.cmbPriority.Size = new System.Drawing.Size(121, 21);
            this.cmbPriority.TabIndex = 9;
            this.cmbPriority.SelectedIndexChanged += new System.EventHandler(this.cmbPriority_SelectedIndexChanged);
            // 
            // btnAccept
            // 
            this.btnAccept.Location = new System.Drawing.Point(214, 340);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(75, 23);
            this.btnAccept.TabIndex = 10;
            this.btnAccept.Text = "Add";
            this.btnAccept.UseVisualStyleBackColor = true;
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(331, 340);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // panStatus
            // 
            this.panStatus.Controls.Add(this.chbFlagAsDeleted);
            this.panStatus.Controls.Add(this.lblStatus);
            this.panStatus.Controls.Add(this.chbStatus);
            this.panStatus.Location = new System.Drawing.Point(21, 277);
            this.panStatus.Name = "panStatus";
            this.panStatus.Size = new System.Drawing.Size(227, 46);
            this.panStatus.TabIndex = 12;
            // 
            // chbFlagAsDeleted
            // 
            this.chbFlagAsDeleted.AutoSize = true;
            this.chbFlagAsDeleted.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chbFlagAsDeleted.Location = new System.Drawing.Point(2, 4);
            this.chbFlagAsDeleted.Name = "chbFlagAsDeleted";
            this.chbFlagAsDeleted.Size = new System.Drawing.Size(145, 17);
            this.chbFlagAsDeleted.TabIndex = 8;
            this.chbFlagAsDeleted.Text = "Flag message as Deleted";
            this.chbFlagAsDeleted.UseVisualStyleBackColor = true;
            // 
            // panSendToPackage
            // 
            this.panSendToPackage.Controls.Add(this.chbSendToPackage);
            this.panSendToPackage.Location = new System.Drawing.Point(19, 329);
            this.panSendToPackage.Name = "panSendToPackage";
            this.panSendToPackage.Size = new System.Drawing.Size(189, 34);
            this.panSendToPackage.TabIndex = 13;
            // 
            // chbSendToPackage
            // 
            this.chbSendToPackage.AutoSize = true;
            this.chbSendToPackage.Location = new System.Drawing.Point(72, 14);
            this.chbSendToPackage.Name = "chbSendToPackage";
            this.chbSendToPackage.Size = new System.Drawing.Size(113, 17);
            this.chbSendToPackage.TabIndex = 0;
            this.chbSendToPackage.Text = "Send To Package";
            this.chbSendToPackage.UseVisualStyleBackColor = true;
            // 
            // lblActionDesc
            // 
            this.lblActionDesc.AutoSize = true;
            this.lblActionDesc.Location = new System.Drawing.Point(253, 247);
            this.lblActionDesc.Name = "lblActionDesc";
            this.lblActionDesc.Size = new System.Drawing.Size(0, 13);
            this.lblActionDesc.TabIndex = 14;
            // 
            // txtActionDesc
            // 
            this.txtActionDesc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtActionDesc.Location = new System.Drawing.Point(259, 247);
            this.txtActionDesc.Multiline = true;
            this.txtActionDesc.Name = "txtActionDesc";
            this.txtActionDesc.ReadOnly = true;
            this.txtActionDesc.Size = new System.Drawing.Size(336, 73);
            this.txtActionDesc.TabIndex = 15;
            // 
            // FrmCustomerMessage
            // 
            this.AcceptButton = this.btnAccept;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(618, 388);
            this.Controls.Add(this.txtActionDesc);
            this.Controls.Add(this.lblActionDesc);
            this.Controls.Add(this.panSendToPackage);
            this.Controls.Add(this.panStatus);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnAccept);
            this.Controls.Add(this.cmbPriority);
            this.Controls.Add(this.lblPriority);
            this.Controls.Add(this.dtmExpirationDate);
            this.Controls.Add(this.lblExpirationDate);
            this.Controls.Add(this.txtBody);
            this.Controls.Add(this.lblBody);
            this.Controls.Add(this.txtSubject);
            this.Controls.Add(this.lblSubject);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCustomerMessage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "New Message";
            this.Load += new System.EventHandler(this.frmCustomerMessage_Load);
            this.panStatus.ResumeLayout(false);
            this.panStatus.PerformLayout();
            this.panSendToPackage.ResumeLayout(false);
            this.panSendToPackage.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label lblSubject;
    private System.Windows.Forms.TextBox txtSubject;
    private System.Windows.Forms.Label lblBody;
    private System.Windows.Forms.TextBox txtBody;
    private System.Windows.Forms.Label lblExpirationDate;
    private System.Windows.Forms.DateTimePicker dtmExpirationDate;
    private System.Windows.Forms.Label lblStatus;
    private System.Windows.Forms.ComboBox chbStatus;
    private System.Windows.Forms.Label lblPriority;
    private System.Windows.Forms.ComboBox cmbPriority;
    private System.Windows.Forms.Button btnAccept;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.Panel panStatus;
    private System.Windows.Forms.Panel panSendToPackage;
    private System.Windows.Forms.CheckBox chbSendToPackage;
    private System.Windows.Forms.CheckBox chbFlagAsDeleted;
    private System.Windows.Forms.Label lblActionDesc;
    private System.Windows.Forms.TextBox txtActionDesc;
  }
}