﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using CustomerMaintenance.Libraries;
using GUILibraries.Forms;
using GUILibraries.Utilities;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;

namespace CustomerMaintenance.UI {
  public partial class FrmCostToBuyHalfPoints : SIDForm {
    private readonly String _currentcustomerId = "";

    private readonly CustomerInfoUpdater _custInfo;

    CustomerComboBox _cCb;

    public FrmCostToBuyHalfPoints(String customerId, ModuleInfo moduleInfo) : base (moduleInfo) {
      if (!string.IsNullOrEmpty(customerId)) {
        _currentcustomerId = customerId;
        InitializeComponent();
        panProgressivePointsBuying.Visible = false;
        _custInfo = new CustomerInfoUpdater(_currentcustomerId, AppModuleInfo);
      }
      else {
        MessageBox.Show(@"No customer was selected");
        Close();
      }


    }

    private void CostToBuyHalfPoints_Load(object sender, EventArgs e) {
      using (var bPoints = new BuyPoints(AppModuleInfo)) {
        var buyingPtsSettings = bPoints.GetCostToBuyPointsByCustomer(_currentcustomerId).SingleOrDefault();

        if (buyingPtsSettings != null) {
          var progBuyPntsEnabled = (buyingPtsSettings.ProgressivePointBuyingFlag == "Y");

          _cCb = new CustomerComboBox(AppModuleInfo);

          if (panProgressivePointsBuying.Controls.Count > 0) {
            panProgressivePointsBuying.Controls.RemoveAt(0);
          }
          var progressiveChartName = buyingPtsSettings.ProgressiveChartName.Trim();
          panProgressivePointsBuying.Controls.Add(_cCb.BuyPointsChartNames(progressiveChartName));

          if (progBuyPntsEnabled) {
            chbUseProgressive.Checked = true;
            panProgressivePointsBuying.Visible = true;
          }

          txtNFLFOOTBALLCostToBuyHalfSpread.Text = String.Format("{0:n0}", int.Parse(buyingPtsSettings.FootballSpreadBuy.ToString()));
          txtNFLFOOTBALLmaxNumberOfHalvesSpread.Text = String.Format("{0:n0}", int.Parse(buyingPtsSettings.FootballSpreadBuyMax.ToString()));
          txtNFLFOOTBALLCostToBuyON3Spread.Text = String.Format("{0:n0}", int.Parse(buyingPtsSettings.FootballSpreadBuyOn3.ToString()));
          txtNFLFOOTBALLCostToBuyON7Spread.Text = String.Format("{0:n0}", int.Parse(buyingPtsSettings.FootballSpreadBuyOn7.ToString()));
          txtNFLFOOTBALLCostToBuyOFF3Spread.Text = String.Format("{0:n0}", int.Parse(buyingPtsSettings.FootballSpreadBuyOff3.ToString()));
          txtNFLFOOTBALLCostToBuyOFF7Spread.Text = String.Format("{0:n0}", int.Parse(buyingPtsSettings.FootballSpreadBuyOff7.ToString()));
          txtNFLFOOTBALLCostToBuyHalfTotalPoints.Text = String.Format("{0:n0}", int.Parse(buyingPtsSettings.FootballTotalBuy.ToString()));
          txtNFLFOOTBALLmaxNumberOfHalvesTotalPoints.Text = String.Format("{0:n0}", int.Parse(buyingPtsSettings.FootballTotalBuyMax.ToString()));

          txtCollegeFOOTBALLCostToBuyHalfSpread.Text = String.Format("{0:n0}", int.Parse(buyingPtsSettings.CollegeFootballSpreadBuy.ToString()));
          txtCollegeFOOTBALLmaxNumberOfHalvesSpread.Text = String.Format("{0:n0}", int.Parse(buyingPtsSettings.CollegeFootballSpreadBuyMax.ToString()));
          txtCollegeFOOTBALLCostToBuyON3Spread.Text = String.Format("{0:n0}", int.Parse(buyingPtsSettings.CollegeFootballSpreadBuyOn3.ToString()));
          txtCollegeFOOTBALLCostToBuyON7Spread.Text = String.Format("{0:n0}", int.Parse(buyingPtsSettings.CollegeFootballSpreadBuyOn7.ToString()));
          txtCollegeFOOTBALLCostToBuyOFF3Spread.Text = String.Format("{0:n0}", int.Parse(buyingPtsSettings.CollegeFootballSpreadBuyOff3.ToString()));
          txtCollegeFOOTBALLCostToBuyOFF7Spread.Text = String.Format("{0:n0}", int.Parse(buyingPtsSettings.CollegeFootballSpreadBuyOff7.ToString()));
          txtCollegeFOOTBALLCostToBuyHalfTotalPoints.Text = String.Format("{0:n0}", int.Parse(buyingPtsSettings.CollegeFootballTotalBuy.ToString()));
          txtCollegeFOOTBALLmaxNumberOfHalvesTotalPoints.Text = String.Format("{0:n0}", int.Parse(buyingPtsSettings.CollegeFootballTotalBuyMax.ToString()));

          txtBASKETBALLCostToBuyHalfSpread.Text = String.Format("{0:n0}", int.Parse(buyingPtsSettings.BasketballSpreadBuy.ToString()));
          txtBASKETBALLmaxNumberOfHalvesSpread.Text = String.Format("{0:n0}", int.Parse(buyingPtsSettings.BasketballSpreadBuyMax.ToString()));
          txtBASKETBALLCostToBuyHalfTotalPoints.Text = String.Format("{0:n0}", int.Parse(buyingPtsSettings.BasketballTotalBuy.ToString()));
          txtBASKETBALLmaxNumberOfHalvesTotalPoints.Text = String.Format("{0:n0}", int.Parse(buyingPtsSettings.BasketballTotalBuyMax.ToString()));

          txtCollegeBASKETBALLCostToBuyHalfSpread.Text = String.Format("{0:n0}", int.Parse(buyingPtsSettings.CollegeBasketballSpreadBuy.ToString()));
          txtCollegeBASKETBALLmaxNumberOfHalvesSpread.Text = String.Format("{0:n0}", int.Parse(buyingPtsSettings.CollegeBasketballSpreadBuyMax.ToString()));
          txtCollegeBASKETBALLCostToBuyHalfTotalPoints.Text = String.Format("{0:n0}", int.Parse(buyingPtsSettings.CollegeBasketballTotalBuy.ToString()));
          txtCollegeBASKETBALLmaxNumberOfHalvesTotalPoints.Text = String.Format("{0:n0}", int.Parse(buyingPtsSettings.CollegeBasketballTotalBuyMax.ToString()));
        }
        _custInfo.LoadCustomerCostToBuyPointsToStructFromDb(buyingPtsSettings);
      }
    }

    private void buyingPointsCancelButton_Click(object sender, EventArgs e) {
      Close();
    }

    private void useProgressiveCheckBox_CheckedChanged(object sender, EventArgs e) {
      var chB = (CheckBox)sender;

      if (!chB.Checked) {
        ComboBox cbx = null;

        if (panProgressivePointsBuying.Controls.Count > 0) {
          cbx = (ComboBox)panProgressivePointsBuying.Controls[0];
        }

        if (cbx != null) cbx.SelectedIndex = -1;

        panProgressivePointsBuying.Visible = false;
      }
      else {
        panProgressivePointsBuying.Visible = true;
      }

      foreach (Control ctrl in Controls) {
        if (ctrl.GetType().ToString() == "System.Windows.Forms.GroupBox") {
          foreach (Control childCtrl in ctrl.Controls) {
            if (childCtrl.GetType().ToString() == "System.Windows.Forms.TextBox") {
              childCtrl.Enabled = !chB.Checked;
            }
          }
        }
      }

      if (!chB.Checked) return;
      txtNFLFOOTBALLmaxNumberOfHalvesSpread.Enabled = true;
      txtNFLFOOTBALLmaxNumberOfHalvesTotalPoints.Enabled = true;
      txtCollegeFOOTBALLmaxNumberOfHalvesSpread.Enabled = true;
      txtCollegeFOOTBALLmaxNumberOfHalvesTotalPoints.Enabled = true;
      txtBASKETBALLmaxNumberOfHalvesSpread.Enabled = true;
      txtBASKETBALLmaxNumberOfHalvesTotalPoints.Enabled = true;
    }

    private IEnumerable<CustomerInfoUpdater.CustomerCostToBuyPoints> LoadCustomerCostToBuyPointsToStructFromFrm() {
      var custCostToBuyPoints = new List<CustomerInfoUpdater.CustomerCostToBuyPoints>();

      int varTxtBasketballCostToBuyHalfSpread;
      int.TryParse(txtBASKETBALLCostToBuyHalfSpread.Text, out varTxtBasketballCostToBuyHalfSpread);

      int varTxtBasketbalLmaxNumberOfHalvesSpread;
      int.TryParse(txtBASKETBALLmaxNumberOfHalvesSpread.Text, out varTxtBasketbalLmaxNumberOfHalvesSpread);

      int varTxtBasketballCostToBuyHalfTotalPoints;
      int.TryParse(txtBASKETBALLCostToBuyHalfTotalPoints.Text, out varTxtBasketballCostToBuyHalfTotalPoints);

      int varTxtBasketbalLmaxNumberOfHalvesTotalPoints;
      int.TryParse(txtBASKETBALLmaxNumberOfHalvesTotalPoints.Text, out varTxtBasketbalLmaxNumberOfHalvesTotalPoints);


      int varTxtCollegeBasketballCostToBuyHalfSpread;
      int.TryParse(txtCollegeBASKETBALLCostToBuyHalfSpread.Text, out varTxtCollegeBasketballCostToBuyHalfSpread);

      int varTxtCollegeBasketbalLmaxNumberOfHalvesSpread;
      int.TryParse(txtCollegeBASKETBALLmaxNumberOfHalvesSpread.Text, out varTxtCollegeBasketbalLmaxNumberOfHalvesSpread);

      int varTxtCollegeBasketballCostToBuyHalfTotalPoints;
      int.TryParse(txtCollegeBASKETBALLCostToBuyHalfTotalPoints.Text, out varTxtCollegeBasketballCostToBuyHalfTotalPoints);

      int varTxtCollegeBasketbalLmaxNumberOfHalvesTotalPoints;
      int.TryParse(txtCollegeBASKETBALLmaxNumberOfHalvesTotalPoints.Text, out varTxtCollegeBasketbalLmaxNumberOfHalvesTotalPoints);


      int varTxtNflfootballCostToBuyHalfSpread;
      int.TryParse(txtNFLFOOTBALLCostToBuyHalfSpread.Text, out varTxtNflfootballCostToBuyHalfSpread);
      //custCostToBuyPointsElem.FootballSpreadBuy = buyValue;

      int varTxtNflfootbalLmaxNumberOfHalvesSpread;
      int.TryParse(txtNFLFOOTBALLmaxNumberOfHalvesSpread.Text, out varTxtNflfootbalLmaxNumberOfHalvesSpread);
      //custCostToBuyPointsElem.FootballSpreadBuyMax = buyValue;

      int varTxtNflfootballCostToBuyOn3Spread;
      int.TryParse(txtNFLFOOTBALLCostToBuyON3Spread.Text, out varTxtNflfootballCostToBuyOn3Spread);
      //custCostToBuyPointsElem.FootballSpreadBuyOn3 = buyValue;

      int varTxtNflfootballCostToBuyOff3Spread;
      int.TryParse(txtNFLFOOTBALLCostToBuyOFF3Spread.Text, out varTxtNflfootballCostToBuyOff3Spread);
      //custCostToBuyPointsElem.FootballSpreadBuyOff3 = buyValue;

      int varTxtNflfootballCostToBuyOn7Spread;
      int.TryParse(txtNFLFOOTBALLCostToBuyON7Spread.Text, out varTxtNflfootballCostToBuyOn7Spread);
      //custCostToBuyPointsElem.FootballSpreadBuyOn7 = buyValue;

      int varTxtNflfootballCostToBuyOff7Spread;
      int.TryParse(txtNFLFOOTBALLCostToBuyOFF7Spread.Text, out varTxtNflfootballCostToBuyOff7Spread);
      //custCostToBuyPointsElem.FootballSpreadBuyOff7 = buyValue;

      int varTxtNflfootballCostToBuyHalfTotalPoints;
      int.TryParse(txtNFLFOOTBALLCostToBuyHalfTotalPoints.Text, out varTxtNflfootballCostToBuyHalfTotalPoints);
      //custCostToBuyPointsElem.FootballTotalBuy = buyValue;

      int varTxtNflfootbalLmaxNumberOfHalvesTotalPoints;
      int.TryParse(txtNFLFOOTBALLmaxNumberOfHalvesTotalPoints.Text, out varTxtNflfootbalLmaxNumberOfHalvesTotalPoints);
      //custCostToBuyPointsElem.FootballTotalBuyMax = buyValue;

      int varTxtCollegeFootballCostToBuyHalfSpread;
      int.TryParse(txtCollegeFOOTBALLCostToBuyHalfSpread.Text, out varTxtCollegeFootballCostToBuyHalfSpread);
      //custCostToBuyPointsElem.CollegeFootballSpreadBuy = buyValue;

      int varTxtCollegeFootbalLmaxNumberOfHalvesSpread;
      int.TryParse(txtCollegeFOOTBALLmaxNumberOfHalvesSpread.Text, out varTxtCollegeFootbalLmaxNumberOfHalvesSpread);
      //custCostToBuyPointsElem.CollegeFootballSpreadBuyMax = buyValue;

      int varTxtCollegeFootballCostToBuyOn3Spread;
      int.TryParse(txtCollegeFOOTBALLCostToBuyON3Spread.Text, out varTxtCollegeFootballCostToBuyOn3Spread);
      //custCostToBuyPointsElem.CollegeFootballSpreadBuyOn3 = buyValue;

      int varTxtCollegeFootballCostToBuyOff3Spread;
      int.TryParse(txtCollegeFOOTBALLCostToBuyOFF3Spread.Text, out varTxtCollegeFootballCostToBuyOff3Spread);
      //custCostToBuyPointsElem.CollegeFootballSpreadBuyOff3 = buyValue;

      int varTxtCollegeFootballCostToBuyOn7Spread;
      int.TryParse(txtCollegeFOOTBALLCostToBuyON7Spread.Text, out varTxtCollegeFootballCostToBuyOn7Spread);
      //custCostToBuyPointsElem.CollegeFootballSpreadBuyOn7 = buyValue;

      int varTxtCollegeFootballCostToBuyOff7Spread;
      int.TryParse(txtCollegeFOOTBALLCostToBuyOFF7Spread.Text, out varTxtCollegeFootballCostToBuyOff7Spread);
      //custCostToBuyPointsElem.CollegeFootballSpreadBuyOff7 = buyValue;

      int varTxtCollegeFootballCostToBuyHalfTotalPoints;
      int.TryParse(txtCollegeFOOTBALLCostToBuyHalfTotalPoints.Text, out varTxtCollegeFootballCostToBuyHalfTotalPoints);
      //custCostToBuyPointsElem.CollegeFootballTotalBuy = buyValue;

      int varTxtCollegeFootbalLmaxNumberOfHalvesTotalPoints;
      int.TryParse(txtCollegeFOOTBALLmaxNumberOfHalvesTotalPoints.Text, out varTxtCollegeFootbalLmaxNumberOfHalvesTotalPoints);
      //custCostToBuyPointsElem.CollegeFootballTotalBuyMax = buyValue;

      //custCostToBuyPointsElem.CustomerID = ;
      //custCostToBuyPointsElem.ProgressivePointBuyingFlag = chbUseProgressive.Checked.ToString();

      //custCostToBuyPointsElem.ProgressiveChartName = FormF.GetSelectedItemInDropDownFromPanel(this, "panProgressivePointsBuying");

      var custCostToBuyPointsElem = new CustomerInfoUpdater.CustomerCostToBuyPoints(varTxtBasketballCostToBuyHalfSpread,
          varTxtBasketbalLmaxNumberOfHalvesSpread,
          varTxtBasketballCostToBuyHalfTotalPoints,
          varTxtBasketbalLmaxNumberOfHalvesTotalPoints,

          varTxtCollegeBasketballCostToBuyHalfSpread,
          varTxtCollegeBasketbalLmaxNumberOfHalvesSpread,
          varTxtCollegeBasketballCostToBuyHalfTotalPoints,
          varTxtCollegeBasketbalLmaxNumberOfHalvesTotalPoints,

          varTxtNflfootballCostToBuyHalfSpread,
          varTxtNflfootbalLmaxNumberOfHalvesSpread,
          varTxtNflfootballCostToBuyOn3Spread,
          varTxtNflfootballCostToBuyOff3Spread,
          varTxtNflfootballCostToBuyOn7Spread,
          varTxtNflfootballCostToBuyOff7Spread,
          varTxtNflfootballCostToBuyHalfTotalPoints,
          varTxtNflfootbalLmaxNumberOfHalvesTotalPoints,
          varTxtCollegeFootballCostToBuyHalfSpread,
          varTxtCollegeFootbalLmaxNumberOfHalvesSpread,
          varTxtCollegeFootballCostToBuyOn3Spread,
          varTxtCollegeFootballCostToBuyOff3Spread,
          varTxtCollegeFootballCostToBuyOn7Spread,
          varTxtCollegeFootballCostToBuyOff7Spread,
          varTxtCollegeFootballCostToBuyHalfTotalPoints,
          varTxtCollegeFootbalLmaxNumberOfHalvesTotalPoints,
          chbUseProgressive.Checked.ToString(),
          FormF.GetSelectedItemInDropDownFromPanel(this, "panProgressivePointsBuying")
          );

      custCostToBuyPoints.Add(custCostToBuyPointsElem);
      return custCostToBuyPoints;
    }

    private static bool ValidateBuyPointsSettings(CustomerInfoUpdater.CustomerCostToBuyPoints buyPointsInfo) {

      var msg = "";
      var validData = true;
      if ((buyPointsInfo.BasketballSpreadBuy > 0 && buyPointsInfo.BasketballSpreadBuyMax == 0) || (buyPointsInfo.BasketballSpreadBuyMax > 0 && buyPointsInfo.BasketballSpreadBuy == 0)) {
        msg = "Basketball Spread";
        validData = false;
      }
      if ((buyPointsInfo.BasketballTotalBuy > 0 && buyPointsInfo.BasketballTotalBuyMax == 0) || (buyPointsInfo.BasketballTotalBuyMax > 0 && buyPointsInfo.BasketballTotalBuy == 0)) {
        msg = "Basketball Total";
        validData = false;
      }
      if ((buyPointsInfo.CollegeBasketballTotalBuy > 0 && buyPointsInfo.CollegeBasketballTotalBuyMax == 0) || (buyPointsInfo.CollegeBasketballTotalBuyMax > 0 && buyPointsInfo.CollegeBasketballTotalBuy == 0)) {
        msg = "College Basketball Total";
        validData = false;
      }
      if ((buyPointsInfo.CollegeBasketballSpreadBuy > 0 && buyPointsInfo.CollegeBasketballSpreadBuyMax == 0) || (buyPointsInfo.CollegeBasketballSpreadBuyMax > 0 && buyPointsInfo.CollegeBasketballSpreadBuy == 0)) {
        msg = "College Basketball Spread";
        validData = false;
      }

      if (validData) return true;
      MessageBox.Show(@"Incorrect " + msg + @" settings.", @"Buy points settings", MessageBoxButtons.OK);
      return false;
    }

    private void btnBuyingPointsOK_Click(object sender, EventArgs e) {
      var buyPointsInfo = LoadCustomerCostToBuyPointsToStructFromFrm().ToList();
      if (!ValidateBuyPointsSettings(buyPointsInfo.FirstOrDefault())) return;
      _custInfo.CompareCustomerCostToBuyPointsInfoToUpdate(buyPointsInfo);
      Close();
    }
  }
}
