﻿namespace CustomerMaintenance.UI
{
    partial class FrmCustomerPaymentInformation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpPaymentInformation = new System.Windows.Forms.GroupBox();
            this.panStates = new System.Windows.Forms.Panel();
            this.txtCountry = new System.Windows.Forms.TextBox();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.txtPostalCode = new System.Windows.Forms.TextBox();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.txtNameLast = new System.Windows.Forms.TextBox();
            this.txtNameMi = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.lblCountry = new System.Windows.Forms.Label();
            this.lblState = new System.Windows.Forms.Label();
            this.lblPhone = new System.Windows.Forms.Label();
            this.lblPostalCode = new System.Windows.Forms.Label();
            this.lblCity = new System.Windows.Forms.Label();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.grpPreviousPaymentsInfo = new System.Windows.Forms.GroupBox();
            this.dgrvwPreviousPaymentInfo = new System.Windows.Forms.DataGridView();
            this.grpPaymentInformation.SuspendLayout();
            this.grpPreviousPaymentsInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgrvwPreviousPaymentInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // grpPaymentInformation
            // 
            this.grpPaymentInformation.Controls.Add(this.panStates);
            this.grpPaymentInformation.Controls.Add(this.txtCountry);
            this.grpPaymentInformation.Controls.Add(this.txtPhone);
            this.grpPaymentInformation.Controls.Add(this.txtPostalCode);
            this.grpPaymentInformation.Controls.Add(this.txtCity);
            this.grpPaymentInformation.Controls.Add(this.txtAddress);
            this.grpPaymentInformation.Controls.Add(this.txtNameLast);
            this.grpPaymentInformation.Controls.Add(this.txtNameMi);
            this.grpPaymentInformation.Controls.Add(this.txtName);
            this.grpPaymentInformation.Controls.Add(this.btnCancel);
            this.grpPaymentInformation.Controls.Add(this.btnOk);
            this.grpPaymentInformation.Controls.Add(this.lblCountry);
            this.grpPaymentInformation.Controls.Add(this.lblState);
            this.grpPaymentInformation.Controls.Add(this.lblPhone);
            this.grpPaymentInformation.Controls.Add(this.lblPostalCode);
            this.grpPaymentInformation.Controls.Add(this.lblCity);
            this.grpPaymentInformation.Controls.Add(this.lblAddress);
            this.grpPaymentInformation.Controls.Add(this.lblName);
            this.grpPaymentInformation.Location = new System.Drawing.Point(12, 173);
            this.grpPaymentInformation.Name = "grpPaymentInformation";
            this.grpPaymentInformation.Size = new System.Drawing.Size(534, 237);
            this.grpPaymentInformation.TabIndex = 30;
            this.grpPaymentInformation.TabStop = false;
            // 
            // panStates
            // 
            this.panStates.Location = new System.Drawing.Point(260, 128);
            this.panStates.Name = "panStates";
            this.panStates.Size = new System.Drawing.Size(174, 27);
            this.panStates.TabIndex = 130;
            // 
            // txtCountry
            // 
            this.txtCountry.Location = new System.Drawing.Point(285, 169);
            this.txtCountry.Name = "txtCountry";
            this.txtCountry.Size = new System.Drawing.Size(100, 20);
            this.txtCountry.TabIndex = 170;
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(93, 197);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(100, 20);
            this.txtPhone.TabIndex = 190;
            // 
            // txtPostalCode
            // 
            this.txtPostalCode.Location = new System.Drawing.Point(93, 171);
            this.txtPostalCode.Name = "txtPostalCode";
            this.txtPostalCode.Size = new System.Drawing.Size(100, 20);
            this.txtPostalCode.TabIndex = 150;
            // 
            // txtCity
            // 
            this.txtCity.Location = new System.Drawing.Point(93, 133);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(100, 20);
            this.txtCity.TabIndex = 110;
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(93, 55);
            this.txtAddress.Multiline = true;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(341, 58);
            this.txtAddress.TabIndex = 90;
            // 
            // txtNameLast
            // 
            this.txtNameLast.Location = new System.Drawing.Point(249, 16);
            this.txtNameLast.Name = "txtNameLast";
            this.txtNameLast.Size = new System.Drawing.Size(185, 20);
            this.txtNameLast.TabIndex = 70;
            // 
            // txtNameMi
            // 
            this.txtNameMi.Location = new System.Drawing.Point(204, 16);
            this.txtNameMi.Name = "txtNameMi";
            this.txtNameMi.Size = new System.Drawing.Size(24, 20);
            this.txtNameMi.TabIndex = 60;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(93, 16);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(100, 20);
            this.txtName.TabIndex = 50;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(428, 190);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 210;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(428, 161);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 200;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // lblCountry
            // 
            this.lblCountry.AutoSize = true;
            this.lblCountry.Location = new System.Drawing.Point(219, 171);
            this.lblCountry.Name = "lblCountry";
            this.lblCountry.Size = new System.Drawing.Size(46, 13);
            this.lblCountry.TabIndex = 160;
            this.lblCountry.Text = "Country:";
            this.lblCountry.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblState
            // 
            this.lblState.AutoSize = true;
            this.lblState.Location = new System.Drawing.Point(219, 136);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(35, 13);
            this.lblState.TabIndex = 120;
            this.lblState.Text = "State:";
            this.lblState.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Location = new System.Drawing.Point(33, 196);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(41, 13);
            this.lblPhone.TabIndex = 180;
            this.lblPhone.Text = "Phone:";
            this.lblPhone.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblPostalCode
            // 
            this.lblPostalCode.AutoSize = true;
            this.lblPostalCode.Location = new System.Drawing.Point(7, 169);
            this.lblPostalCode.Name = "lblPostalCode";
            this.lblPostalCode.Size = new System.Drawing.Size(67, 13);
            this.lblPostalCode.TabIndex = 140;
            this.lblPostalCode.Text = "Postal Code:";
            this.lblPostalCode.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCity
            // 
            this.lblCity.AutoSize = true;
            this.lblCity.Location = new System.Drawing.Point(47, 133);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(27, 13);
            this.lblCity.TabIndex = 100;
            this.lblCity.Text = "City:";
            this.lblCity.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(26, 55);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(48, 13);
            this.lblAddress.TabIndex = 80;
            this.lblAddress.Text = "Address:";
            this.lblAddress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(36, 16);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(38, 13);
            this.lblName.TabIndex = 40;
            this.lblName.Text = "Name:";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // grpPreviousPaymentsInfo
            // 
            this.grpPreviousPaymentsInfo.Controls.Add(this.dgrvwPreviousPaymentInfo);
            this.grpPreviousPaymentsInfo.Location = new System.Drawing.Point(12, 13);
            this.grpPreviousPaymentsInfo.Name = "grpPreviousPaymentsInfo";
            this.grpPreviousPaymentsInfo.Size = new System.Drawing.Size(534, 154);
            this.grpPreviousPaymentsInfo.TabIndex = 10;
            this.grpPreviousPaymentsInfo.TabStop = false;
            this.grpPreviousPaymentsInfo.Text = "Previous Payments Information";
            // 
            // dgrvwPreviousPaymentInfo
            // 
            this.dgrvwPreviousPaymentInfo.AllowUserToAddRows = false;
            this.dgrvwPreviousPaymentInfo.AllowUserToDeleteRows = false;
            this.dgrvwPreviousPaymentInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgrvwPreviousPaymentInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrvwPreviousPaymentInfo.Location = new System.Drawing.Point(6, 19);
            this.dgrvwPreviousPaymentInfo.MultiSelect = false;
            this.dgrvwPreviousPaymentInfo.Name = "dgrvwPreviousPaymentInfo";
            this.dgrvwPreviousPaymentInfo.ReadOnly = true;
            this.dgrvwPreviousPaymentInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgrvwPreviousPaymentInfo.Size = new System.Drawing.Size(522, 129);
            this.dgrvwPreviousPaymentInfo.TabIndex = 20;
            this.dgrvwPreviousPaymentInfo.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgrvwPreviousPaymentInfo_RowEnter);
            this.dgrvwPreviousPaymentInfo.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgrvwPreviousPaymentInfo_RowHeaderMouseClick);
            // 
            // frmCustomerPaymentInformation
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(559, 422);
            this.Controls.Add(this.grpPreviousPaymentsInfo);
            this.Controls.Add(this.grpPaymentInformation);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCustomerPaymentInformation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Payment Information";
            this.Load += new System.EventHandler(this.frmCustomerPaymentInformation_Load);
            this.grpPaymentInformation.ResumeLayout(false);
            this.grpPaymentInformation.PerformLayout();
            this.grpPreviousPaymentsInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgrvwPreviousPaymentInfo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpPaymentInformation;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Label lblCountry;
        private System.Windows.Forms.Label lblState;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.Label lblPostalCode;
        private System.Windows.Forms.Label lblCity;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Panel panStates;
        private System.Windows.Forms.TextBox txtCountry;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.TextBox txtPostalCode;
        private System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.TextBox txtNameLast;
        private System.Windows.Forms.TextBox txtNameMi;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.GroupBox grpPreviousPaymentsInfo;
        private System.Windows.Forms.DataGridView dgrvwPreviousPaymentInfo;
    }
}