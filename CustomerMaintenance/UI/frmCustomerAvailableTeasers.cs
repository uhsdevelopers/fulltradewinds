﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using CustomerMaintenance.Libraries;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using GUILibraries.Forms;

namespace CustomerMaintenance.UI {
  public partial class FrmCustomerAvailableTeasers : SIDForm {
    private readonly String _currentCustomerId = "";

    CustomerInfoUpdater _customerInfo;

    public FrmCustomerAvailableTeasers(String customerId, ModuleInfo moduleInfo) : base (moduleInfo) {
      if (!string.IsNullOrEmpty(customerId)) {
        _currentCustomerId = customerId;
        _customerInfo = new CustomerInfoUpdater(_currentCustomerId, AppModuleInfo);
        InitializeComponent();
      }
      else {
        MessageBox.Show(@"No customer was selected");
        Close();
      }

    }

    private void CustomerAvailableTeasersForm_Load(object sender, EventArgs e) {

      lblInstructions.TextAlign = ContentAlignment.MiddleCenter;
      lblInstructions.Text = @"Select the teasers available to customer '" + _currentCustomerId + @"'.";
      if (availableTeaserscheckedListBox.Items.Count > 0) {
        availableTeaserscheckedListBox.Items.Clear();
      }

      using (var cust = new Customers(AppModuleInfo)) {
        List<spCstGetCustomerTeaserSpecs_Result> custTeaserOptions = cust.GetCustomerTeaserSpecs(_currentCustomerId).ToList();
        IEnumerable<spCstGetTeaserSpecs_Result> bookTeaserOptions = cust.GetTeaserSpecs();

        int i = 0;

        foreach (spCstGetTeaserSpecs_Result teaserItem in bookTeaserOptions) {

          availableTeaserscheckedListBox.Items.Add(teaserItem.TeaserName);

          foreach (spCstGetCustomerTeaserSpecs_Result str in custTeaserOptions) {
            var custTeaserOption = str.TeaserName;
            if (custTeaserOption != teaserItem.TeaserName) continue;
            availableTeaserscheckedListBox.SetItemChecked(i, true);
            break;
          }
          i++;
        }
        _customerInfo.LoadCustomerAvailableTeasersToStructFromDb(availableTeaserscheckedListBox);
      }
    }

    private void btnCancel_Click(object sender, EventArgs e) {
      Close();
    }

    private void btnOk_Click(object sender, EventArgs e) {
      _customerInfo.CompareCustomerAvailableTeasersInfoToUpdate(_customerInfo.CustomerAvailableTeasersFromDbList, availableTeaserscheckedListBox);
      _customerInfo = null;
      Close();
    }
  }
}
