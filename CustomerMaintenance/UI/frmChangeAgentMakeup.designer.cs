﻿namespace CustomerMaintenance.UI
{
    partial class FrmChangeAgentMakeup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNewMakeup = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtNewMakeup = new GUILibraries.Controls.NumberTextBox();
            this.SuspendLayout();
            // 
            // lblNewMakeup
            // 
            this.lblNewMakeup.AutoSize = true;
            this.lblNewMakeup.Location = new System.Drawing.Point(12, 20);
            this.lblNewMakeup.Name = "lblNewMakeup";
            this.lblNewMakeup.Size = new System.Drawing.Size(106, 13);
            this.lblNewMakeup.TabIndex = 0;
            this.lblNewMakeup.Text = "New Makeup Figure:";
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(43, 50);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 30;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(149, 50);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 40;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtNewMakeup
            // 
            this.txtNewMakeup.DividedBy = 1;
            this.txtNewMakeup.DivideFlag = false;
            this.txtNewMakeup.Location = new System.Drawing.Point(124, 17);
            this.txtNewMakeup.Name = "txtNewMakeup";
            this.txtNewMakeup.ShowIfZero = true;
            this.txtNewMakeup.Size = new System.Drawing.Size(100, 20);
            this.txtNewMakeup.TabIndex = 20;
            this.txtNewMakeup.Leave += new System.EventHandler(this.numberToText_Leave);
            // 
            // frmChangeAgentMakeup
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(248, 91);
            this.Controls.Add(this.txtNewMakeup);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.lblNewMakeup);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmChangeAgentMakeup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Change Current Makeup";
            this.Load += new System.EventHandler(this.frmChangeAgentMakeup_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNewMakeup;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private GUILibraries.Controls.NumberTextBox txtNewMakeup;
    }
}