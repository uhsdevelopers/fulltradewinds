﻿namespace CustomerMaintenance.UI {
  partial class FrmCustomerRestrictedActionParams {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
      this.dgvParams = new System.Windows.Forms.DataGridView();
      this.btnSave = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.RestrictedActionParamId = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.ParamName = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.ParamValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.ActionID = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.ActionParamName = new System.Windows.Forms.DataGridViewTextBoxColumn();
      ((System.ComponentModel.ISupportInitialize)(this.dgvParams)).BeginInit();
      this.SuspendLayout();
      // 
      // dgvParams
      // 
      this.dgvParams.AllowUserToAddRows = false;
      this.dgvParams.AllowUserToDeleteRows = false;
      this.dgvParams.AllowUserToResizeColumns = false;
      this.dgvParams.AllowUserToResizeRows = false;
      this.dgvParams.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgvParams.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RestrictedActionParamId,
            this.ParamName,
            this.ParamValue,
            this.ActionID,
            this.ActionParamName});
      this.dgvParams.Location = new System.Drawing.Point(13, 13);
      this.dgvParams.Name = "dgvParams";
      this.dgvParams.RowHeadersVisible = false;
      this.dgvParams.Size = new System.Drawing.Size(448, 298);
      this.dgvParams.TabIndex = 0;
      this.dgvParams.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvParams_CellValidating);
      // 
      // btnSave
      // 
      this.btnSave.Location = new System.Drawing.Point(98, 317);
      this.btnSave.Name = "btnSave";
      this.btnSave.Size = new System.Drawing.Size(75, 23);
      this.btnSave.TabIndex = 1;
      this.btnSave.Text = "Save";
      this.btnSave.UseVisualStyleBackColor = true;
      this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.Location = new System.Drawing.Point(274, 317);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 2;
      this.btnCancel.Text = "Cancel";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // RestrictedActionParamId
      // 
      this.RestrictedActionParamId.DataPropertyName = "RestrictedActionParamId";
      this.RestrictedActionParamId.HeaderText = "RestrictedActionParamId";
      this.RestrictedActionParamId.Name = "RestrictedActionParamId";
      this.RestrictedActionParamId.ReadOnly = true;
      this.RestrictedActionParamId.Visible = false;
      // 
      // ParamName
      // 
      this.ParamName.DataPropertyName = "FriendlyName";
      this.ParamName.HeaderText = "Name";
      this.ParamName.Name = "ParamName";
      this.ParamName.ReadOnly = true;
      this.ParamName.Width = 220;
      // 
      // ParamValue
      // 
      this.ParamValue.DataPropertyName = "Value";
      this.ParamValue.HeaderText = "Value";
      this.ParamValue.Name = "ParamValue";
      this.ParamValue.Width = 220;
      // 
      // ActionID
      // 
      this.ActionID.HeaderText = "ActionID";
      this.ActionID.Name = "ActionID";
      this.ActionID.Visible = false;
      // 
      // ActionParamName
      // 
      this.ActionParamName.HeaderText = "ActionParamName";
      this.ActionParamName.Name = "ActionParamName";
      this.ActionParamName.Visible = false;
      // 
      // FrmCustomerRestrictedActionParams
      // 
      this.AcceptButton = this.btnSave;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(473, 353);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.btnSave);
      this.Controls.Add(this.dgvParams);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
      this.KeyPreview = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "FrmCustomerRestrictedActionParams";
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Restriction Parameters";
      this.Load += new System.EventHandler(this.frmCustomerRestrictedActionParams_Load);
      ((System.ComponentModel.ISupportInitialize)(this.dgvParams)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.DataGridView dgvParams;
    private System.Windows.Forms.Button btnSave;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.DataGridViewTextBoxColumn RestrictedActionParamId;
    private System.Windows.Forms.DataGridViewTextBoxColumn ParamName;
    private System.Windows.Forms.DataGridViewTextBoxColumn ParamValue;
    private System.Windows.Forms.DataGridViewTextBoxColumn ActionID;
    private System.Windows.Forms.DataGridViewTextBoxColumn ActionParamName;
  }
}