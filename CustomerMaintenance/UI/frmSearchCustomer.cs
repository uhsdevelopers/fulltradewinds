﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using System.Collections;

using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using GUILibraries.Forms;

namespace CustomerMaintenance.UI {
  public partial class FrmSearchCustomer : SIDForm {

    #region private Constants

    private const int GRID_VIEW_ROW_HEIGHT = 16;


    #endregion

    #region Public Properties

    #endregion

    #region Private Properties

    private IEnumerable<ColumnAlias> GvwColumAlias {
      get {
        if (_gvwColumAlias == null) {
          _gvwColumAlias = new List<ColumnAlias>();

          var colA = new ColumnAlias { ColumnId = "NameFirst", columnAlias = "First" };
          _gvwColumAlias.Add(colA);

          colA.ColumnId = "NameLast";
          colA.columnAlias = "Last";
          _gvwColumAlias.Add(colA);

          colA.ColumnId = "CustomerID";
          colA.columnAlias = "Customer ID";
          _gvwColumAlias.Add(colA);

          colA.ColumnId = "AgentID";
          colA.columnAlias = "Agent";
          _gvwColumAlias.Add(colA);

          colA.ColumnId = "EMail";
          colA.columnAlias = "E-Mail";
          _gvwColumAlias.Add(colA);

          colA.ColumnId = "HomePhone";
          colA.columnAlias = "Home Phone";
          _gvwColumAlias.Add(colA);

          colA.ColumnId = "BusinessPhone";
          colA.columnAlias = "Business Phone";
          _gvwColumAlias.Add(colA);

          colA.ColumnId = "Password";
          colA.columnAlias = "Password";
          _gvwColumAlias.Add(colA);

          colA.ColumnId = "OpenedBy";
          colA.columnAlias = "OpenedBy";
          _gvwColumAlias.Add(colA);

          return _gvwColumAlias;
        }
        return _gvwColumAlias;
      }
    }

    #endregion

    #region Public vars

    #endregion

    #region Private vars

    List<ColumnAlias> _gvwColumAlias;
    List<DisplayableColumns> _gvwAvailableColumns;
    readonly Form _parentFrm;
    readonly List<spULPGetCurrentUserPermissions_Result> _currentUserAccess;

    #endregion

    #region Structures

    private struct ColumnAlias {
      public String ColumnId;
      public String columnAlias;
    }

    private struct DisplayableColumns {
      public String FirstColumn;
      public String SecondColumn;
      public String ThirdColumn;
      public String FourthColumn;
      public String LoadInfoCriteria;
      public String SearchCriteria;
    }

    #endregion

    #region Constructors

    public FrmSearchCustomer(SIDForm parentFrm) : base(parentFrm.AppModuleInfo) {
      _parentFrm = parentFrm;
      _currentUserAccess = ((FrmCustomerMaintenance)parentFrm).CurrentUserPermissions;
      InitializeComponent();
    }

    #endregion

    #region Public Methods

    #endregion

    #region Private Methods

    private void WriteSearchResultsGrvwHeaders(DataGridView caller, String firstColumn) {
      caller.DataSource = null;

      var dgrvwColNames = new ArrayList();

      foreach (DataGridViewTextBoxColumn col in caller.Columns) {
        dgrvwColNames.Add(col.Name);
      }

      foreach (String str in dgrvwColNames) {
        caller.Columns.Remove(str);
      }

      var columns = (from li in _gvwAvailableColumns where li.SearchCriteria == firstColumn.Replace("btn", "") select li).FirstOrDefault();

      var gvwColumns = new List<String> {
              columns.FirstColumn,
              columns.SecondColumn,
              columns.ThirdColumn,
              columns.FourthColumn,
              columns.LoadInfoCriteria
            };

      var itemIdx = 0;

      foreach (var str in gvwColumns) {
        var titleColumn = new DataGridViewTextBoxColumn {
          HeaderText = str,
          Width = 124,
          DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleLeft }
        };
        if (itemIdx > 3)
          titleColumn.Visible = false;
        caller.Columns.Add(titleColumn);
        itemIdx++;
      }
    }

    private void LoadSelectedCustomer() {
      if (dgvwSearchResults.SelectedRows.Count > 0) {
        var custIdColIdx = 0;

        foreach (DataGridViewColumn col in dgvwSearchResults.Columns) {
          if (col.HeaderText != null && col.HeaderText.Trim().ToLower() == "customerid") {
            custIdColIdx = col.Index;
            break;
          }
        }

        var customerId = "";
        if (dgvwSearchResults.SelectedRows[0].Cells[custIdColIdx].Value != null) {
          customerId = dgvwSearchResults.SelectedRows[0].Cells[custIdColIdx].Value.ToString().Trim();
        }

        if (customerId.Length != 0) {
          var frm = (FrmCustomerMaintenance)_parentFrm;
          if (frm != null) {
            var custIdTextBox = (TextBox)frm.Controls.Find("txtCustomerId", true).FirstOrDefault();
            if (!frm.CustomerInfoRetrieved) {
              if (custIdTextBox != null) {
                custIdTextBox.Text = customerId;
                frm.CustomerId = customerId;
                frm.RetrieveCustomerInfo(customerId);
              }

            }
            else {
              if (custIdTextBox != null) {
                custIdTextBox.Text = "";
                if (LoginsAndProfiles.ValidateUserFunctionalityAccess(_currentUserAccess, LoginsAndProfiles.UPDATE_CUSTOMER_MAINTENANCE)) {
                  frm.SaveCustomerInfo();
                }
                else {
                  frm.CustomerInfoRetrieved = false;
                  frm.ClearFormElementsContent();
                }
                custIdTextBox.Text = customerId;
                frm.CustomerId = customerId;
                frm.RetrieveCustomerInfo(customerId);
              }
            }
          }
        }
        Close();
      }
      else {
        MessageBox.Show(@"Please select a customer from the list to load them", @"No selection");
      }
    }

    private void LoadDefaultGridViewColumns() {
      _gvwAvailableColumns = new List<DisplayableColumns>();
      var columnsList = new DisplayableColumns {
        FirstColumn = "Last",
        SecondColumn = "First",
        ThirdColumn = "Agent",
        FourthColumn = "Customer ID",
        LoadInfoCriteria = "CustomerId",
        SearchCriteria = "NameLast"
      };
      _gvwAvailableColumns.Add(columnsList);

      columnsList = new DisplayableColumns {
        FirstColumn = "First",
        SecondColumn = "Last",
        ThirdColumn = "Agent",
        FourthColumn = "Customer ID",
        LoadInfoCriteria = "CustomerId",
        SearchCriteria = "NameFirst"
      };
      _gvwAvailableColumns.Add(columnsList);

      columnsList = new DisplayableColumns {
        FirstColumn = "Agent",
        SecondColumn = "First",
        ThirdColumn = "Last",
        FourthColumn = "Customer ID",
        LoadInfoCriteria = "CustomerId",
        SearchCriteria = "AgentID"
      };
      _gvwAvailableColumns.Add(columnsList);

      columnsList = new DisplayableColumns {
        FirstColumn = "E-Mail",
        SecondColumn = "First",
        ThirdColumn = "Last",
        FourthColumn = "Customer ID",
        LoadInfoCriteria = "CustomerId",
        SearchCriteria = "EMail"
      };
      _gvwAvailableColumns.Add(columnsList);

      columnsList = new DisplayableColumns {
        FirstColumn = "Password",
        SecondColumn = "First",
        ThirdColumn = "Last",
        FourthColumn = "Customer ID",
        LoadInfoCriteria = "CustomerId",
        SearchCriteria = "Password"
      };
      _gvwAvailableColumns.Add(columnsList);

      columnsList = new DisplayableColumns {
        FirstColumn = "Opened By",
        SecondColumn = "First",
        ThirdColumn = "Last",
        FourthColumn = "Customer ID",
        LoadInfoCriteria = "CustomerId",
        SearchCriteria = "OpenedBy"
      };
      _gvwAvailableColumns.Add(columnsList);

      columnsList = new DisplayableColumns {
        FirstColumn = "CustomerID",
        SecondColumn = "First",
        ThirdColumn = "Last",
        FourthColumn = "Agent",
        LoadInfoCriteria = "CustomerId",
        SearchCriteria = "CustomerID"
      };
      _gvwAvailableColumns.Add(columnsList);

      columnsList = new DisplayableColumns {
        FirstColumn = "Home Phone",
        SecondColumn = "First",
        ThirdColumn = "Last",
        FourthColumn = "Customer ID",
        LoadInfoCriteria = "CustomerId",
        SearchCriteria = "HomePhone"
      };
      _gvwAvailableColumns.Add(columnsList);

      columnsList = new DisplayableColumns {
        FirstColumn = "Business Phone",
        SecondColumn = "First",
        ThirdColumn = "Last",
        FourthColumn = "Customer ID",
        LoadInfoCriteria = "CustomerId",
        SearchCriteria = "BusinessPhone"
      };
      _gvwAvailableColumns.Add(columnsList);
    }

    private void RetrieveResultsAndFillGrvw(string buttonId) {
      if (txtSearchBy.Text != "") {
        WriteSearchResultsGrvwHeaders(dgvwSearchResults, buttonId);

        List<spCstGetCustomer_Result> customersList;
        using (var cust = new Customers(AppModuleInfo)) {
          customersList = cust.GetCustomersListInfo(buttonId, txtSearchBy.Text);
        }
        if (customersList != null && customersList.Count > 0) {
          switch (buttonId) {
            case "NameLast":
              foreach (var c in customersList) {
                dgvwSearchResults.Rows.Add(
                    String.IsNullOrEmpty(c.NameLast) ? "" : c.NameLast.Trim(),
                    String.IsNullOrEmpty(c.NameFirst) ? "" : c.NameFirst.Trim(),
                    String.IsNullOrEmpty(c.AgentID) ? "" : c.AgentID.Trim(),
                    String.IsNullOrEmpty(c.CustomerID) ? "" : c.CustomerID.Trim(),
                    String.IsNullOrEmpty(c.CustomerID) ? "" : c.CustomerID.Trim());
                dgvwSearchResults.Rows[dgvwSearchResults.Rows.Count - 1].Height = GRID_VIEW_ROW_HEIGHT;
              }
              break;
            case "NameFirst":
              foreach (var c in customersList) {
                dgvwSearchResults.Rows.Add(
                    String.IsNullOrEmpty(c.NameFirst) ? "" : c.NameFirst.Trim(),
                    String.IsNullOrEmpty(c.NameLast) ? "" : c.NameLast.Trim(),
                    String.IsNullOrEmpty(c.AgentID) ? "" : c.AgentID.Trim(),
                    String.IsNullOrEmpty(c.CustomerID) ? "" : c.CustomerID.Trim(),
                    String.IsNullOrEmpty(c.CustomerID) ? "" : c.CustomerID.Trim());
                dgvwSearchResults.Rows[dgvwSearchResults.Rows.Count - 1].Height = GRID_VIEW_ROW_HEIGHT;
              }
              break;
            case "AgentID":
              foreach (var c in customersList) {
                dgvwSearchResults.Rows.Add(
                    String.IsNullOrEmpty(c.AgentID) ? "" : c.AgentID.Trim(),
                    String.IsNullOrEmpty(c.NameFirst) ? "" : c.NameFirst.Trim(),
                    String.IsNullOrEmpty(c.NameLast) ? "" : c.NameLast.Trim(),
                    String.IsNullOrEmpty(c.CustomerID) ? "" : c.CustomerID.Trim(),
                    String.IsNullOrEmpty(c.CustomerID) ? "" : c.CustomerID.Trim());
                dgvwSearchResults.Rows[dgvwSearchResults.Rows.Count - 1].Height = GRID_VIEW_ROW_HEIGHT;
              }
              break;
            case "EMail":
              foreach (var c in customersList) {
                dgvwSearchResults.Rows.Add(
                    String.IsNullOrEmpty(c.EMail) ? "" : c.EMail.Trim(),
                    String.IsNullOrEmpty(c.NameFirst) ? "" : c.NameFirst.Trim(),
                    String.IsNullOrEmpty(c.NameLast) ? "" : c.NameLast.Trim(),
                    String.IsNullOrEmpty(c.CustomerID) ? "" : c.CustomerID.Trim(),
                    String.IsNullOrEmpty(c.CustomerID) ? "" : c.CustomerID.Trim());
                dgvwSearchResults.Rows[dgvwSearchResults.Rows.Count - 1].Height = GRID_VIEW_ROW_HEIGHT;
              }
              break;
            case "Password":
              foreach (var c in customersList) {
                dgvwSearchResults.Rows.Add(
                    String.IsNullOrEmpty(c.Password) ? "" : c.Password.Trim(),
                    String.IsNullOrEmpty(c.NameFirst) ? "" : c.NameFirst.Trim(),
                    String.IsNullOrEmpty(c.NameLast) ? "" : c.NameLast.Trim(),
                    String.IsNullOrEmpty(c.CustomerID) ? "" : c.CustomerID.Trim(),
                    String.IsNullOrEmpty(c.CustomerID) ? "" : c.CustomerID.Trim());
                dgvwSearchResults.Rows[dgvwSearchResults.Rows.Count - 1].Height = GRID_VIEW_ROW_HEIGHT;
              }
              break;
            case "OpenedBy":
              foreach (var c in customersList) {
                dgvwSearchResults.Rows.Add(
                    String.IsNullOrEmpty(c.OpenedBy) ? "" : c.OpenedBy.Trim(),
                    String.IsNullOrEmpty(c.NameFirst) ? "" : c.NameFirst.Trim(),
                    String.IsNullOrEmpty(c.NameLast) ? "" : c.NameLast.Trim(),
                    String.IsNullOrEmpty(c.CustomerID) ? "" : c.CustomerID.Trim(),
                    String.IsNullOrEmpty(c.CustomerID) ? "" : c.CustomerID.Trim());
                dgvwSearchResults.Rows[dgvwSearchResults.Rows.Count - 1].Height = GRID_VIEW_ROW_HEIGHT;
              }
              break;
            case "CustomerID":
              foreach (var c in customersList) {
                dgvwSearchResults.Rows.Add(
                    String.IsNullOrEmpty(c.CustomerID) ? "" : c.CustomerID.Trim(),
                    String.IsNullOrEmpty(c.NameFirst) ? "" : c.NameFirst.Trim(),
                    String.IsNullOrEmpty(c.NameLast) ? "" : c.NameLast.Trim(),
                    String.IsNullOrEmpty(c.AgentID) ? "" : c.AgentID.Trim(),
                    String.IsNullOrEmpty(c.CustomerID) ? "" : c.CustomerID.Trim());
                dgvwSearchResults.Rows[dgvwSearchResults.Rows.Count - 1].Height = GRID_VIEW_ROW_HEIGHT;
              }
              break;
            case "HomePhone":
              foreach (var c in customersList) {
                dgvwSearchResults.Rows.Add(
                    String.IsNullOrEmpty(c.HomePhone) ? "" : c.HomePhone.Trim(),
                    String.IsNullOrEmpty(c.NameFirst) ? "" : c.NameFirst.Trim(),
                    String.IsNullOrEmpty(c.NameLast) ? "" : c.NameLast.Trim(),
                    String.IsNullOrEmpty(c.CustomerID) ? "" : c.CustomerID.Trim(),
                    String.IsNullOrEmpty(c.CustomerID) ? "" : c.CustomerID.Trim());
                dgvwSearchResults.Rows[dgvwSearchResults.Rows.Count - 1].Height = GRID_VIEW_ROW_HEIGHT;
              }
              break;
            case "BusinessPhone":
              foreach (var c in customersList) {
                dgvwSearchResults.Rows.Add(
                    String.IsNullOrEmpty(c.BusinessPhone) ? "" : c.BusinessPhone.Trim(),
                    String.IsNullOrEmpty(c.NameFirst) ? "" : c.NameFirst.Trim(),
                    String.IsNullOrEmpty(c.NameLast) ? "" : c.NameLast.Trim(),
                    String.IsNullOrEmpty(c.CustomerID) ? "" : c.CustomerID.Trim(),
                    String.IsNullOrEmpty(c.CustomerID) ? "" : c.CustomerID.Trim());
                dgvwSearchResults.Rows[dgvwSearchResults.Rows.Count - 1].Height = GRID_VIEW_ROW_HEIGHT;
              }
              break;
          }
          dgvwSearchResults.ClearSelection();
        }
      }
    }

    #endregion

    #region Events

    private void btnOption_Click(object sender, EventArgs e) {
      var buttonId = ((Button)sender).Name.Replace("btn", "");

      var dgrvwColNames = new List<String>();

      foreach (DataGridViewTextBoxColumn col in dgvwSearchResults.Columns) {
        dgrvwColNames.Add(col.Name);
      }

      foreach (var str in dgrvwColNames) {
        dgvwSearchResults.Columns.Remove(str);
      }

      if (dgvwSearchResults.Rows.Count > 0) {
        dgvwSearchResults.Rows.Clear();
      }

      RetrieveResultsAndFillGrvw(buttonId);
    }

    private void btnSelect_Click(object sender, EventArgs e) {
      LoadSelectedCustomer();
    }

    private void dgvwSearchResults_CellDoubleClick(object sender, DataGridViewCellEventArgs e) {
      LoadSelectedCustomer();
    }

    private void frmSearchCustomer_Load(object sender, EventArgs e) {
      lblInstructions.Text = @"Enter one or more characters to search by, then press the button";
      lblInstructions.Text += Environment.NewLine;
      lblInstructions.Text += @"corresponding to the search field";

      LoadDefaultGridViewColumns();
    }

    private void frmSearchCustomer_KeyDown(object sender, KeyEventArgs e) {
      if (e.KeyCode == Keys.Escape) {
        Close();
      }
    }

    #endregion


  }
}
