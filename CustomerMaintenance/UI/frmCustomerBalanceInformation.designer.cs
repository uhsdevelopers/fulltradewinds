﻿namespace CustomerMaintenance.UI {
  partial class FrmCustomerBalanceInformation {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
      this.txtBalanceInfoHeader = new System.Windows.Forms.TextBox();
      this.grpCurrentBalance = new System.Windows.Forms.GroupBox();
      this.lblLine = new System.Windows.Forms.Label();
      this.lblAvailableBalanceFigure = new System.Windows.Forms.Label();
      this.lblPostedPendingBetsFigure = new System.Windows.Forms.Label();
      this.lblNonPostedCasinoBalanceFigure = new System.Windows.Forms.Label();
      this.lblPendingCreditFigure = new System.Windows.Forms.Label();
      this.lblCreditLimitFigure = new System.Windows.Forms.Label();
      this.lblCurrentBalanceFigure = new System.Windows.Forms.Label();
      this.lblAvailableBalance = new System.Windows.Forms.Label();
      this.lblPostedPendingBets = new System.Windows.Forms.Label();
      this.lblNonPostedCasinoBalance = new System.Windows.Forms.Label();
      this.lblPendingCredit = new System.Windows.Forms.Label();
      this.lblCreditLimit = new System.Windows.Forms.Label();
      this.lblCurrentBalance = new System.Windows.Forms.Label();
      this.grpFreePlayBalance = new System.Windows.Forms.GroupBox();
      this.lblFreePlayBalanceFigure = new System.Windows.Forms.Label();
      this.lblFreePlayBalance = new System.Windows.Forms.Label();
      this.btnClose = new System.Windows.Forms.Button();
      this.grpCurrentBalance.SuspendLayout();
      this.grpFreePlayBalance.SuspendLayout();
      this.SuspendLayout();
      // 
      // txtBalanceInfoHeader
      // 
      this.txtBalanceInfoHeader.Enabled = false;
      this.txtBalanceInfoHeader.Location = new System.Drawing.Point(22, 15);
      this.txtBalanceInfoHeader.Multiline = true;
      this.txtBalanceInfoHeader.Name = "txtBalanceInfoHeader";
      this.txtBalanceInfoHeader.ReadOnly = true;
      this.txtBalanceInfoHeader.Size = new System.Drawing.Size(279, 123);
      this.txtBalanceInfoHeader.TabIndex = 0;
      this.txtBalanceInfoHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // grpCurrentBalance
      // 
      this.grpCurrentBalance.Controls.Add(this.lblLine);
      this.grpCurrentBalance.Controls.Add(this.lblAvailableBalanceFigure);
      this.grpCurrentBalance.Controls.Add(this.lblPostedPendingBetsFigure);
      this.grpCurrentBalance.Controls.Add(this.lblNonPostedCasinoBalanceFigure);
      this.grpCurrentBalance.Controls.Add(this.lblPendingCreditFigure);
      this.grpCurrentBalance.Controls.Add(this.lblCreditLimitFigure);
      this.grpCurrentBalance.Controls.Add(this.lblCurrentBalanceFigure);
      this.grpCurrentBalance.Controls.Add(this.lblAvailableBalance);
      this.grpCurrentBalance.Controls.Add(this.lblPostedPendingBets);
      this.grpCurrentBalance.Controls.Add(this.lblNonPostedCasinoBalance);
      this.grpCurrentBalance.Controls.Add(this.lblPendingCredit);
      this.grpCurrentBalance.Controls.Add(this.lblCreditLimit);
      this.grpCurrentBalance.Controls.Add(this.lblCurrentBalance);
      this.grpCurrentBalance.Location = new System.Drawing.Point(22, 154);
      this.grpCurrentBalance.Name = "grpCurrentBalance";
      this.grpCurrentBalance.Size = new System.Drawing.Size(279, 171);
      this.grpCurrentBalance.TabIndex = 1;
      this.grpCurrentBalance.TabStop = false;
      // 
      // lblLine
      // 
      this.lblLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblLine.Location = new System.Drawing.Point(-1, 127);
      this.lblLine.Name = "lblLine";
      this.lblLine.Size = new System.Drawing.Size(273, 21);
      this.lblLine.TabIndex = 12;
      this.lblLine.Text = "___________________________________________";
      this.lblLine.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lblAvailableBalanceFigure
      // 
      this.lblAvailableBalanceFigure.Location = new System.Drawing.Point(168, 148);
      this.lblAvailableBalanceFigure.Name = "lblAvailableBalanceFigure";
      this.lblAvailableBalanceFigure.Size = new System.Drawing.Size(96, 13);
      this.lblAvailableBalanceFigure.TabIndex = 11;
      this.lblAvailableBalanceFigure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lblPostedPendingBetsFigure
      // 
      this.lblPostedPendingBetsFigure.Location = new System.Drawing.Point(168, 114);
      this.lblPostedPendingBetsFigure.Name = "lblPostedPendingBetsFigure";
      this.lblPostedPendingBetsFigure.Size = new System.Drawing.Size(96, 13);
      this.lblPostedPendingBetsFigure.TabIndex = 10;
      this.lblPostedPendingBetsFigure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lblNonPostedCasinoBalanceFigure
      // 
      this.lblNonPostedCasinoBalanceFigure.Location = new System.Drawing.Point(168, 89);
      this.lblNonPostedCasinoBalanceFigure.Name = "lblNonPostedCasinoBalanceFigure";
      this.lblNonPostedCasinoBalanceFigure.Size = new System.Drawing.Size(96, 13);
      this.lblNonPostedCasinoBalanceFigure.TabIndex = 9;
      this.lblNonPostedCasinoBalanceFigure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lblPendingCreditFigure
      // 
      this.lblPendingCreditFigure.Location = new System.Drawing.Point(168, 64);
      this.lblPendingCreditFigure.Name = "lblPendingCreditFigure";
      this.lblPendingCreditFigure.Size = new System.Drawing.Size(96, 13);
      this.lblPendingCreditFigure.TabIndex = 8;
      this.lblPendingCreditFigure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lblCreditLimitFigure
      // 
      this.lblCreditLimitFigure.Location = new System.Drawing.Point(168, 38);
      this.lblCreditLimitFigure.Name = "lblCreditLimitFigure";
      this.lblCreditLimitFigure.Size = new System.Drawing.Size(96, 13);
      this.lblCreditLimitFigure.TabIndex = 7;
      this.lblCreditLimitFigure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lblCurrentBalanceFigure
      // 
      this.lblCurrentBalanceFigure.Location = new System.Drawing.Point(168, 16);
      this.lblCurrentBalanceFigure.Name = "lblCurrentBalanceFigure";
      this.lblCurrentBalanceFigure.Size = new System.Drawing.Size(96, 13);
      this.lblCurrentBalanceFigure.TabIndex = 6;
      this.lblCurrentBalanceFigure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lblAvailableBalance
      // 
      this.lblAvailableBalance.Location = new System.Drawing.Point(26, 149);
      this.lblAvailableBalance.Name = "lblAvailableBalance";
      this.lblAvailableBalance.Size = new System.Drawing.Size(134, 15);
      this.lblAvailableBalance.TabIndex = 5;
      this.lblAvailableBalance.Text = "Available Balance:";
      this.lblAvailableBalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lblPostedPendingBets
      // 
      this.lblPostedPendingBets.Location = new System.Drawing.Point(6, 111);
      this.lblPostedPendingBets.Name = "lblPostedPendingBets";
      this.lblPostedPendingBets.Size = new System.Drawing.Size(153, 18);
      this.lblPostedPendingBets.TabIndex = 4;
      this.lblPostedPendingBets.Text = "Posted Pending Bets:";
      this.lblPostedPendingBets.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lblNonPostedCasinoBalance
      // 
      this.lblNonPostedCasinoBalance.Location = new System.Drawing.Point(7, 86);
      this.lblNonPostedCasinoBalance.Name = "lblNonPostedCasinoBalance";
      this.lblNonPostedCasinoBalance.Size = new System.Drawing.Size(153, 16);
      this.lblNonPostedCasinoBalance.TabIndex = 3;
      this.lblNonPostedCasinoBalance.Text = "Non-Posted Casino Balance:";
      this.lblNonPostedCasinoBalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lblPendingCredit
      // 
      this.lblPendingCredit.Location = new System.Drawing.Point(32, 60);
      this.lblPendingCredit.Name = "lblPendingCredit";
      this.lblPendingCredit.Size = new System.Drawing.Size(128, 17);
      this.lblPendingCredit.TabIndex = 2;
      this.lblPendingCredit.Text = "Pending Credit (If-Bets):";
      this.lblPendingCredit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lblCreditLimit
      // 
      this.lblCreditLimit.AutoSize = true;
      this.lblCreditLimit.Location = new System.Drawing.Point(99, 38);
      this.lblCreditLimit.Name = "lblCreditLimit";
      this.lblCreditLimit.Size = new System.Drawing.Size(61, 13);
      this.lblCreditLimit.TabIndex = 1;
      this.lblCreditLimit.Text = "Credit Limit:";
      this.lblCreditLimit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lblCurrentBalance
      // 
      this.lblCurrentBalance.Location = new System.Drawing.Point(64, 16);
      this.lblCurrentBalance.Name = "lblCurrentBalance";
      this.lblCurrentBalance.Size = new System.Drawing.Size(96, 13);
      this.lblCurrentBalance.TabIndex = 0;
      this.lblCurrentBalance.Text = "Current Balance:";
      this.lblCurrentBalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // grpFreePlayBalance
      // 
      this.grpFreePlayBalance.Controls.Add(this.lblFreePlayBalanceFigure);
      this.grpFreePlayBalance.Controls.Add(this.lblFreePlayBalance);
      this.grpFreePlayBalance.Location = new System.Drawing.Point(22, 351);
      this.grpFreePlayBalance.Name = "grpFreePlayBalance";
      this.grpFreePlayBalance.Size = new System.Drawing.Size(279, 45);
      this.grpFreePlayBalance.TabIndex = 2;
      this.grpFreePlayBalance.TabStop = false;
      // 
      // lblFreePlayBalanceFigure
      // 
      this.lblFreePlayBalanceFigure.Location = new System.Drawing.Point(168, 16);
      this.lblFreePlayBalanceFigure.Name = "lblFreePlayBalanceFigure";
      this.lblFreePlayBalanceFigure.Size = new System.Drawing.Size(96, 13);
      this.lblFreePlayBalanceFigure.TabIndex = 13;
      this.lblFreePlayBalanceFigure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lblFreePlayBalance
      // 
      this.lblFreePlayBalance.AutoSize = true;
      this.lblFreePlayBalance.Location = new System.Drawing.Point(7, 20);
      this.lblFreePlayBalance.Name = "lblFreePlayBalance";
      this.lblFreePlayBalance.Size = new System.Drawing.Size(96, 13);
      this.lblFreePlayBalance.TabIndex = 0;
      this.lblFreePlayBalance.Text = "Free Play Balance:";
      // 
      // btnClose
      // 
      this.btnClose.Location = new System.Drawing.Point(129, 413);
      this.btnClose.Name = "btnClose";
      this.btnClose.Size = new System.Drawing.Size(75, 23);
      this.btnClose.TabIndex = 3;
      this.btnClose.Text = "Close";
      this.btnClose.UseVisualStyleBackColor = true;
      this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
      // 
      // FrmCustomerBalanceInformation
      // 
      this.AcceptButton = this.btnClose;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(326, 452);
      this.Controls.Add(this.btnClose);
      this.Controls.Add(this.grpFreePlayBalance);
      this.Controls.Add(this.grpCurrentBalance);
      this.Controls.Add(this.txtBalanceInfoHeader);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.KeyPreview = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "FrmCustomerBalanceInformation";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Balance Information (Press F5 To Refresh)";
      this.Load += new System.EventHandler(this.frmCustomerBalanceInformation_Load);
      this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmCustomerBalanceInformation_KeyDown);
      this.grpCurrentBalance.ResumeLayout(false);
      this.grpCurrentBalance.PerformLayout();
      this.grpFreePlayBalance.ResumeLayout(false);
      this.grpFreePlayBalance.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox txtBalanceInfoHeader;
    private System.Windows.Forms.GroupBox grpCurrentBalance;
    private System.Windows.Forms.GroupBox grpFreePlayBalance;
    private System.Windows.Forms.Button btnClose;
    private System.Windows.Forms.Label lblFreePlayBalance;
    private System.Windows.Forms.Label lblAvailableBalance;
    private System.Windows.Forms.Label lblPostedPendingBets;
    private System.Windows.Forms.Label lblNonPostedCasinoBalance;
    private System.Windows.Forms.Label lblPendingCredit;
    private System.Windows.Forms.Label lblCreditLimit;
    private System.Windows.Forms.Label lblCurrentBalance;
    private System.Windows.Forms.Label lblAvailableBalanceFigure;
    private System.Windows.Forms.Label lblPostedPendingBetsFigure;
    private System.Windows.Forms.Label lblNonPostedCasinoBalanceFigure;
    private System.Windows.Forms.Label lblPendingCreditFigure;
    private System.Windows.Forms.Label lblCreditLimitFigure;
    private System.Windows.Forms.Label lblCurrentBalanceFigure;
    private System.Windows.Forms.Label lblLine;
    private System.Windows.Forms.Label lblFreePlayBalanceFigure;
  }
}