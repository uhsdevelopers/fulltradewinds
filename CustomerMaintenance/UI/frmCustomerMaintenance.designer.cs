﻿using GUILibraries.Controls;

namespace CustomerMaintenance.UI {
  partial class FrmCustomerMaintenance {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCustomerMaintenance));
            this.txtCustomerId = new System.Windows.Forms.TextBox();
            this.lblCustomerName = new System.Windows.Forms.Label();
            this.lblCustNameLast = new System.Windows.Forms.Label();
            this.tabCustomer = new System.Windows.Forms.TabControl();
            this.tabpPersonal = new System.Windows.Forms.TabPage();
            this.panPersonal = new System.Windows.Forms.Panel();
            this.grpSuspendedActions = new System.Windows.Forms.GroupBox();
            this.chBrestrictionsBox = new System.Windows.Forms.CheckedListBox();
            this.panGeneralInfo = new System.Windows.Forms.Panel();
            this.chbZeroBalancePositiveFigures = new System.Windows.Forms.CheckBox();
            this.btnShowAuditSettleFigure = new System.Windows.Forms.Button();
            this.panPriceTypesList = new System.Windows.Forms.Panel();
            this.txtSettleFigure = new GUILibraries.Controls.NumberTextBox();
            this.panTimeZonesList = new System.Windows.Forms.Panel();
            this.txtChartPerc = new GUILibraries.Controls.NumberTextBox();
            this.panInetTargetsList = new System.Windows.Forms.Panel();
            this.panCountriesList = new System.Windows.Forms.Panel();
            this.txtCountry = new System.Windows.Forms.TextBox();
            this.panCurrencyCodesList = new System.Windows.Forms.Panel();
            this.panAgentsList = new System.Windows.Forms.Panel();
            this.panStatesProvList = new System.Windows.Forms.Panel();
            this.lblName = new System.Windows.Forms.Label();
            this.lblchartPerc = new System.Windows.Forms.Label();
            this.lblState = new System.Windows.Forms.Label();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.lblInet = new System.Windows.Forms.Label();
            this.lblZipCode = new System.Windows.Forms.Label();
            this.lblCity = new System.Windows.Forms.Label();
            this.chbWeeklyLimits = new System.Windows.Forms.CheckBox();
            this.txtNameFirst = new System.Windows.Forms.TextBox();
            this.lblTimeZone = new System.Windows.Forms.Label();
            this.txtZipCode = new System.Windows.Forms.TextBox();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblCurrency = new System.Windows.Forms.Label();
            this.txtNameMi = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.lblCountry = new System.Windows.Forms.Label();
            this.lblPriceType = new System.Windows.Forms.Label();
            this.txtAdress2 = new System.Windows.Forms.TextBox();
            this.lblAgent = new System.Windows.Forms.Label();
            this.txtNameLast = new System.Windows.Forms.TextBox();
            this.lblSettleFigure = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.chbZeroBalance = new System.Windows.Forms.CheckBox();
            this.panMisc = new System.Windows.Forms.Panel();
            this.txtSecondsToDelay = new GUILibraries.Controls.NumberTextBox();
            this.lblSecondsToDelay = new System.Windows.Forms.Label();
            this.chbShowOnInstantAction = new System.Windows.Forms.CheckBox();
            this.chbWiseGuy = new System.Windows.Forms.CheckBox();
            this.grpPasswords = new System.Windows.Forms.GroupBox();
            this.btnShowAuditPayoutPassword = new System.Windows.Forms.Button();
            this.btnShowAuditPassword = new System.Windows.Forms.Button();
            this.lblPayoutPassword = new System.Windows.Forms.Label();
            this.lblWageringPassword = new System.Windows.Forms.Label();
            this.txtWageringPassword = new System.Windows.Forms.TextBox();
            this.txtPayoutPassword = new System.Windows.Forms.TextBox();
            this.grpSuspendWagering = new System.Windows.Forms.GroupBox();
            this.chbCasino = new System.Windows.Forms.CheckBox();
            this.chbSuspendSports = new System.Windows.Forms.CheckBox();
            this.grpPhone = new System.Windows.Forms.GroupBox();
            this.lblFax = new System.Windows.Forms.Label();
            this.txtFax = new System.Windows.Forms.TextBox();
            this.lblBusinessPhone = new System.Windows.Forms.Label();
            this.lblHomePhone = new System.Windows.Forms.Label();
            this.txtHomePhone = new System.Windows.Forms.TextBox();
            this.txtBusinessPhone = new System.Windows.Forms.TextBox();
            this.tabpOffering = new System.Windows.Forms.TabPage();
            this.panOfferingTab = new System.Windows.Forms.Panel();
            this.btnBetServiceProfiles = new System.Windows.Forms.Button();
            this.grpFreeHalfPoint = new System.Windows.Forms.GroupBox();
            this.chbNoFreeHalfPointInTotals = new System.Windows.Forms.CheckBox();
            this.grpFreeHalfPntOnOff = new System.Windows.Forms.GroupBox();
            this.chbNFLFOOTBALLCostToBuyOFF7FreeHalfPont = new System.Windows.Forms.CheckBox();
            this.chbNFLFOOTBALLCostToBuyON7FreeHalfPont = new System.Windows.Forms.CheckBox();
            this.chbNFLFOOTBALLCostToBuyOFF3FreeHalfPont = new System.Windows.Forms.CheckBox();
            this.chbNFLFOOTBALLCostToBuyON3FreeHalfPont = new System.Windows.Forms.CheckBox();
            this.txtMaxFreeHalfPoint = new GUILibraries.Controls.NumberTextBox();
            this.lblMaxFreeHalfPoint = new System.Windows.Forms.Label();
            this.panInetFootball = new System.Windows.Forms.Panel();
            this.panCuFootball = new System.Windows.Forms.Panel();
            this.panInetBasketball = new System.Windows.Forms.Panel();
            this.panCuBasketball = new System.Windows.Forms.Panel();
            this.chbEnforceHalfPoint = new System.Windows.Forms.CheckBox();
            this.chbInetFootball = new System.Windows.Forms.CheckBox();
            this.chbCuFootball = new System.Windows.Forms.CheckBox();
            this.chbInetBasketball = new System.Windows.Forms.CheckBox();
            this.chbCuBasketball = new System.Windows.Forms.CheckBox();
            this.grpBaseball = new System.Windows.Forms.GroupBox();
            this.chbEasternLines = new System.Windows.Forms.CheckBox();
            this.rdoFixed = new System.Windows.Forms.RadioButton();
            this.rdoAction = new System.Windows.Forms.RadioButton();
            this.rdoListed = new System.Windows.Forms.RadioButton();
            this.grpStoreAndShadeGroups = new System.Windows.Forms.GroupBox();
            this.panShadeGroups = new System.Windows.Forms.Panel();
            this.panStoresList = new System.Windows.Forms.Panel();
            this.lblShadeGroups = new System.Windows.Forms.Label();
            this.lblStore = new System.Windows.Forms.Label();
            this.panVariousSettings = new System.Windows.Forms.Panel();
            this.btnCustBuyPoints = new System.Windows.Forms.Button();
            this.chbUsePuck = new System.Windows.Forms.CheckBox();
            this.chbLimitIfToRisk = new System.Windows.Forms.CheckBox();
            this.chbEnableRollingIfBets = new System.Windows.Forms.CheckBox();
            this.chbCreditCustomer = new System.Windows.Forms.CheckBox();
            this.chbStaticLines = new System.Windows.Forms.CheckBox();
            this.grpWagerLimits = new System.Windows.Forms.GroupBox();
            this.txtTempQuickLimit = new GUILibraries.Controls.NumberTextBox();
            this.lblTempQuickLimit = new System.Windows.Forms.Label();
            this.dtmTempQuickLimit = new System.Windows.Forms.DateTimePicker();
            this.btnShowAuditQuickLimit = new System.Windows.Forms.Button();
            this.txtThroughQuickLimit = new System.Windows.Forms.TextBox();
            this.lblThroughTempQuickLimit = new System.Windows.Forms.Label();
            this.btnShowAuditCUMinBet = new System.Windows.Forms.Button();
            this.btnShowAuditInetMinBet = new System.Windows.Forms.Button();
            this.txtInetMinBet = new GUILibraries.Controls.NumberTextBox();
            this.txtLossLimit = new GUILibraries.Controls.NumberTextBox();
            this.txtCuMinBet = new GUILibraries.Controls.NumberTextBox();
            this.lblInetMinBet = new System.Windows.Forms.Label();
            this.txtQuickLimit = new GUILibraries.Controls.NumberTextBox();
            this.lblLossLimit = new System.Windows.Forms.Label();
            this.lblQuickLimit = new System.Windows.Forms.Label();
            this.lblCuMinBet = new System.Windows.Forms.Label();
            this.grpCreditLimit = new System.Windows.Forms.GroupBox();
            this.btnShowAuditCreditIncrease = new System.Windows.Forms.Button();
            this.btnShowAuditCreditLimit = new System.Windows.Forms.Button();
            this.dtmTempCreditIncrease = new System.Windows.Forms.DateTimePicker();
            this.txtThroughCredIncrease = new System.Windows.Forms.TextBox();
            this.lblThroughCredIncrease = new System.Windows.Forms.Label();
            this.lblCreditIncrease = new System.Windows.Forms.Label();
            this.txtCreditIncrease = new GUILibraries.Controls.NumberTextBox();
            this.txtCreditLimit = new GUILibraries.Controls.NumberTextBox();
            this.lblNoCredit = new System.Windows.Forms.Label();
            this.lblCreditLimit = new System.Windows.Forms.Label();
            this.panParlayAndTeasers = new System.Windows.Forms.Panel();
            this.btnTeasers = new System.Windows.Forms.Button();
            this.lblParlay = new System.Windows.Forms.Label();
            this.panParlayTypes = new System.Windows.Forms.Panel();
            this.tabpVigDiscount = new System.Windows.Forms.TabPage();
            this.panVigDiscount = new System.Windows.Forms.Panel();
            this.lblVigDiscountNote = new System.Windows.Forms.Label();
            this.dgvwVigDiscount = new System.Windows.Forms.DataGridView();
            this.tabpPerformance = new System.Windows.Forms.TabPage();
            this.panPerformance = new System.Windows.Forms.Panel();
            this.grpWagerTypes = new System.Windows.Forms.GroupBox();
            this.cmbWagerTypes = new System.Windows.Forms.ComboBox();
            this.panGoToDFsOrWagers = new System.Windows.Forms.Panel();
            this.btnGoToWagers = new System.Windows.Forms.Button();
            this.btnGoToDFs = new System.Windows.Forms.Button();
            this.panPerformanceData = new System.Windows.Forms.Panel();
            this.txtSelectedPerformance = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPerfViewModeLegend = new System.Windows.Forms.TextBox();
            this.dgvwCustomerPerformance = new System.Windows.Forms.DataGridView();
            this.grpViewDailyFigs = new System.Windows.Forms.GroupBox();
            this.radYearlyPerformance = new System.Windows.Forms.RadioButton();
            this.radMonthlyPerformance = new System.Windows.Forms.RadioButton();
            this.radWeeklyPerformance = new System.Windows.Forms.RadioButton();
            this.radDailyPerformance = new System.Windows.Forms.RadioButton();
            this.tabpTransactions = new System.Windows.Forms.TabPage();
            this.panTransactions = new System.Windows.Forms.Panel();
            this.panTranTotalSelected = new System.Windows.Forms.Panel();
            this.txtTotalsTransSelected = new System.Windows.Forms.TextBox();
            this.lblTransTotalSelected = new System.Windows.Forms.Label();
            this.grpTranTypes = new System.Windows.Forms.GroupBox();
            this.cmbTranTypes = new System.Windows.Forms.ComboBox();
            this.btnTranGotoWagers = new System.Windows.Forms.Button();
            this.btnLastVerified = new System.Windows.Forms.Button();
            this.btnDeleteTran = new System.Windows.Forms.Button();
            this.btnTranBrowse = new System.Windows.Forms.Button();
            this.btnTranNew = new System.Windows.Forms.Button();
            this.panHeader = new System.Windows.Forms.Panel();
            this.btnPendingWagers = new System.Windows.Forms.Button();
            this.lblChangeMakeup = new System.Windows.Forms.Label();
            this.lblNewMakeup = new System.Windows.Forms.Label();
            this.txtNewMakeup = new GUILibraries.Controls.NumberTextBox();
            this.btnChangeCustMakeup = new System.Windows.Forms.Button();
            this.txtNonPostedCasino = new GUILibraries.Controls.NumberTextBox();
            this.txtLastWeekCO = new GUILibraries.Controls.NumberTextBox();
            this.lblDisplay = new System.Windows.Forms.Label();
            this.txtPendingBets = new GUILibraries.Controls.NumberTextBox();
            this.btnNonPostedCasino = new System.Windows.Forms.Button();
            this.txtAvailableBalance = new GUILibraries.Controls.NumberTextBox();
            this.lblAvailableBalance = new System.Windows.Forms.Label();
            this.lblPendingBets = new System.Windows.Forms.Label();
            this.btnChangeCO = new System.Windows.Forms.Button();
            this.lblLastWeekCO = new System.Windows.Forms.Label();
            this.lblNonPostedCasino = new System.Windows.Forms.Label();
            this.cmbDisplay = new System.Windows.Forms.ComboBox();
            this.btnBalance = new System.Windows.Forms.Button();
            this.panTransactionsDetails = new System.Windows.Forms.Panel();
            this.dgvwTransactions = new System.Windows.Forms.DataGridView();
            this.ctxMenuStripTransactions = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiNewTransaction = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiChangeTransaction = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDeleteTransaction = new System.Windows.Forms.ToolStripMenuItem();
            this.tabpFreePlays = new System.Windows.Forms.TabPage();
            this.panFreePlays = new System.Windows.Forms.Panel();
            this.btnFreePlayGoToWagers = new System.Windows.Forms.Button();
            this.btnFreePlayDelete = new System.Windows.Forms.Button();
            this.btnFreePlayNew = new System.Windows.Forms.Button();
            this.panFreePlaysDetails = new System.Windows.Forms.Panel();
            this.dgvwFreePlaysDetails = new System.Windows.Forms.DataGridView();
            this.ctxMenuStripFreePlays = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiNewFreePlay = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDeleteFreePlay = new System.Windows.Forms.ToolStripMenuItem();
            this.panFreePlayHeader = new System.Windows.Forms.Panel();
            this.txtFreePlayPendingCount = new GUILibraries.Controls.NumberTextBox();
            this.lblFreePlayDisplay = new System.Windows.Forms.Label();
            this.txtFreePlayPendingAmount = new GUILibraries.Controls.NumberTextBox();
            this.lblFreePlayAvailableBalance = new System.Windows.Forms.Label();
            this.txtFreePlayBalance = new GUILibraries.Controls.NumberTextBox();
            this.lblFreePlayPendingAmount = new System.Windows.Forms.Label();
            this.lblFreePlayPendingCount = new System.Windows.Forms.Label();
            this.cmbFreePlayDisplay = new System.Windows.Forms.ComboBox();
            this.tabpComments = new System.Windows.Forms.TabPage();
            this.panComments = new System.Windows.Forms.Panel();
            this.grpCommentsForCustomer = new System.Windows.Forms.GroupBox();
            this.dgvwCustomerMessages = new System.Windows.Forms.DataGridView();
            this.btnSendToPackage = new System.Windows.Forms.Button();
            this.chbShowDeleted = new System.Windows.Forms.CheckBox();
            this.txtCommentsForCustomer = new System.Windows.Forms.TextBox();
            this.btnNewMessage = new System.Windows.Forms.Button();
            this.grpCommentsForTW = new System.Windows.Forms.GroupBox();
            this.txtCommentsForTW = new System.Windows.Forms.TextBox();
            this.tabpMarketing = new System.Windows.Forms.TabPage();
            this.panMarketing = new System.Windows.Forms.Panel();
            this.grpGeneralComments = new System.Windows.Forms.GroupBox();
            this.txtGeneralComments = new System.Windows.Forms.TextBox();
            this.panGeneralMarketing = new System.Windows.Forms.Panel();
            this.lblSource = new System.Windows.Forms.Label();
            this.lblPromoter = new System.Windows.Forms.Label();
            this.lblReferredBy = new System.Windows.Forms.Label();
            this.txtSource = new System.Windows.Forms.TextBox();
            this.txtReferredBy = new System.Windows.Forms.TextBox();
            this.txtPromoter = new System.Windows.Forms.TextBox();
            this.grpEstimatedWagerAmount = new System.Windows.Forms.GroupBox();
            this.chbEstWagerAmount5To50 = new System.Windows.Forms.CheckBox();
            this.chbEstWagerAmountOver1000 = new System.Windows.Forms.CheckBox();
            this.chbEstWagerAmount51to100 = new System.Windows.Forms.CheckBox();
            this.chbEstWagerAmount501To1000 = new System.Windows.Forms.CheckBox();
            this.chbEstWagerAmount101to500 = new System.Windows.Forms.CheckBox();
            this.grpSportsOfInterest = new System.Windows.Forms.GroupBox();
            this.InterestedInOther = new System.Windows.Forms.CheckBox();
            this.InterestedInHorseRacing = new System.Windows.Forms.CheckBox();
            this.InterestedInBaseball = new System.Windows.Forms.CheckBox();
            this.InterestedInFootball = new System.Windows.Forms.CheckBox();
            this.InterestedInHockey = new System.Windows.Forms.CheckBox();
            this.chbInterestedInBasketBall = new System.Windows.Forms.CheckBox();
            this.grpContactRestrictions = new System.Windows.Forms.GroupBox();
            this.chbNoPhone = new System.Windows.Forms.CheckBox();
            this.chbNoEmail = new System.Windows.Forms.CheckBox();
            this.chbNoMail = new System.Windows.Forms.CheckBox();
            this.tabpAgent = new System.Windows.Forms.TabPage();
            this.panAgent = new System.Windows.Forms.Panel();
            this.panAutoCreateShadeGroups = new System.Windows.Forms.Panel();
            this.chbShadeGroupsAutoCreate = new System.Windows.Forms.CheckBox();
            this.panFirstAgentSettings = new System.Windows.Forms.Panel();
            this.chbCustomerIsMasterAgent = new System.Windows.Forms.CheckBox();
            this.chbCustomerIsAgent = new System.Windows.Forms.CheckBox();
            this.grpInternetCustomerAccess = new System.Windows.Forms.GroupBox();
            this.txtIdPrefix = new System.Windows.Forms.TextBox();
            this.lblIdPrefix = new System.Windows.Forms.Label();
            this.chbEnterBettingAdjs = new System.Windows.Forms.CheckBox();
            this.chbEnterDepositsWithdrawals = new System.Windows.Forms.CheckBox();
            this.chbSetMinimumBetAmt = new System.Windows.Forms.CheckBox();
            this.chbChangeSettleFigure = new System.Windows.Forms.CheckBox();
            this.chbUpdateComments = new System.Windows.Forms.CheckBox();
            this.chbChangeTempCredit = new System.Windows.Forms.CheckBox();
            this.chbAddNewAccounts = new System.Windows.Forms.CheckBox();
            this.chbChangeWagerLimits = new System.Windows.Forms.CheckBox();
            this.chbManageLines = new System.Windows.Forms.CheckBox();
            this.chbSuspendWagering = new System.Windows.Forms.CheckBox();
            this.chbChangeCreditLimit = new System.Windows.Forms.CheckBox();
            this.grpHeadCountRate = new System.Windows.Forms.GroupBox();
            this.txtInternetOnly = new GUILibraries.Controls.NumberTextBox();
            this.txtCasino = new GUILibraries.Controls.NumberTextBox();
            this.txtCallUnit = new GUILibraries.Controls.NumberTextBox();
            this.lblInternetOnly = new System.Windows.Forms.Label();
            this.lblCasino = new System.Windows.Forms.Label();
            this.lblCallUnit = new System.Windows.Forms.Label();
            this.grpCommissionType = new System.Windows.Forms.GroupBox();
            this.panAffiliate = new System.Windows.Forms.Panel();
            this.rdoAgentWeeklyProfit = new System.Windows.Forms.RadioButton();
            this.rdoAgentSplit = new System.Windows.Forms.RadioButton();
            this.rdoAffiliateWeeklyProfit = new System.Windows.Forms.RadioButton();
            this.rdoAgentRedFigure = new System.Windows.Forms.RadioButton();
            this.rdoAffiliateSplit = new System.Windows.Forms.RadioButton();
            this.rdoAffiliateRedFigure = new System.Windows.Forms.RadioButton();
            this.chbIncludeFreePlayLossesinDistribution = new System.Windows.Forms.CheckBox();
            this.lblAgentCommission = new System.Windows.Forms.Label();
            this.txtAgentCommision = new GUILibraries.Controls.NumberTextBox();
            this.panMakeUpAndCasinoFee = new System.Windows.Forms.Panel();
            this.lblPercentOfLosses = new System.Windows.Forms.Label();
            this.lblCasinoFee = new System.Windows.Forms.Label();
            this.btnChangeMakeup = new System.Windows.Forms.Button();
            this.lblCurrentMakeup = new System.Windows.Forms.Label();
            this.txtCasinoFee = new GUILibraries.Controls.NumberTextBox();
            this.txtCurrentMakeup = new GUILibraries.Controls.NumberTextBox();
            this.grpMasterAgent = new System.Windows.Forms.GroupBox();
            this.chbDistributeFunds = new System.Windows.Forms.CheckBox();
            this.panMasterAgentDropDown = new System.Windows.Forms.Panel();
            this.panSendPackage = new System.Windows.Forms.Panel();
            this.btnSavePackageToFile = new System.Windows.Forms.Button();
            this.btnSendPackage = new System.Windows.Forms.Button();
            this.tabpWagerLimits = new System.Windows.Forms.TabPage();
            this.panWagerLimits = new System.Windows.Forms.Panel();
            this.lblDisabledSportsCount = new System.Windows.Forms.Label();
            this.btnResetWagerLimits = new System.Windows.Forms.Button();
            this.btnShowAuditWagerLimitsN = new System.Windows.Forms.Button();
            this.btnShowAuditWagerLimits = new System.Windows.Forms.Button();
            this.cmbWLSports = new System.Windows.Forms.ComboBox();
            this.grpMiscellaneous = new System.Windows.Forms.GroupBox();
            this.chbEnforceTeaserBetLimit = new System.Windows.Forms.CheckBox();
            this.btnTeaserLimits = new System.Windows.Forms.Button();
            this.btnShowAuditTeaserMaxBet = new System.Windows.Forms.Button();
            this.txtMaxTeaserBet = new GUILibraries.Controls.NumberTextBox();
            this.btnShowAuditContestMaxBet = new System.Windows.Forms.Button();
            this.txtMaxPropBet = new GUILibraries.Controls.NumberTextBox();
            this.lblMaxTeaserBet = new System.Windows.Forms.Label();
            this.lblMaxPropBet = new System.Windows.Forms.Label();
            this.panImportant = new System.Windows.Forms.Panel();
            this.lblImportant = new System.Windows.Forms.Label();
            this.grpHorseLimits = new System.Windows.Forms.GroupBox();
            this.btnHorses = new System.Windows.Forms.Button();
            this.grpParlayLimits = new System.Windows.Forms.GroupBox();
            this.chbEnforceParlayBetLimit = new System.Windows.Forms.CheckBox();
            this.btnParlayLimits = new System.Windows.Forms.Button();
            this.btnShowAuditParlayMaxPayout = new System.Windows.Forms.Button();
            this.btnShowAuditParlayMaxBet = new System.Windows.Forms.Button();
            this.lblMaxParlayPayout = new System.Windows.Forms.Label();
            this.lblMaxParlayBet = new System.Windows.Forms.Label();
            this.txtMaxParlayPayout = new GUILibraries.Controls.NumberTextBox();
            this.txtMaxParlayBet = new GUILibraries.Controls.NumberTextBox();
            this.panWagerLimitsDetails = new System.Windows.Forms.Panel();
            this.dgvwWagerLimitsDetails = new System.Windows.Forms.DataGridView();
            this.grpEnforceWagerLimits = new System.Windows.Forms.GroupBox();
            this.chbByLine = new System.Windows.Forms.CheckBox();
            this.chbByGameSelection = new System.Windows.Forms.CheckBox();
            this.tabpRestrictions = new System.Windows.Forms.TabPage();
            this.txtActionDescription = new System.Windows.Forms.TextBox();
            this.clbActions = new System.Windows.Forms.CheckedListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbRestrictionsLog = new System.Windows.Forms.ListBox();
            this.btnRestrictionParams = new System.Windows.Forms.Button();
            this.lblCustomerID = new System.Windows.Forms.Label();
            this.txtCustomerSince = new System.Windows.Forms.TextBox();
            this.mnusMainMenu = new System.Windows.Forms.MenuStrip();
            this.tsmiAction = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiNew = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteAgentCascateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiFind = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiSave = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiReset = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiCalculator = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiTicketWriter = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiReports = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDecryptor = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiExit = new System.Windows.Forms.ToolStripMenuItem();
            this.panMainMenu = new System.Windows.Forms.Panel();
            this.btnSportsContestsLinks = new System.Windows.Forms.Button();
            this.dtmGeneralCalendar = new System.Windows.Forms.DateTimePicker();
            this.lblCurrentDateTime = new System.Windows.Forms.Label();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.lklReturnToAgent = new System.Windows.Forms.LinkLabel();
            this.lblShowingLimitsInUSDol = new System.Windows.Forms.Label();
            this.tabCustomer.SuspendLayout();
            this.tabpPersonal.SuspendLayout();
            this.panPersonal.SuspendLayout();
            this.grpSuspendedActions.SuspendLayout();
            this.panGeneralInfo.SuspendLayout();
            this.panMisc.SuspendLayout();
            this.grpPasswords.SuspendLayout();
            this.grpSuspendWagering.SuspendLayout();
            this.grpPhone.SuspendLayout();
            this.tabpOffering.SuspendLayout();
            this.panOfferingTab.SuspendLayout();
            this.grpFreeHalfPoint.SuspendLayout();
            this.grpFreeHalfPntOnOff.SuspendLayout();
            this.grpBaseball.SuspendLayout();
            this.grpStoreAndShadeGroups.SuspendLayout();
            this.panVariousSettings.SuspendLayout();
            this.grpWagerLimits.SuspendLayout();
            this.grpCreditLimit.SuspendLayout();
            this.panParlayAndTeasers.SuspendLayout();
            this.tabpVigDiscount.SuspendLayout();
            this.panVigDiscount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvwVigDiscount)).BeginInit();
            this.tabpPerformance.SuspendLayout();
            this.panPerformance.SuspendLayout();
            this.grpWagerTypes.SuspendLayout();
            this.panGoToDFsOrWagers.SuspendLayout();
            this.panPerformanceData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvwCustomerPerformance)).BeginInit();
            this.grpViewDailyFigs.SuspendLayout();
            this.tabpTransactions.SuspendLayout();
            this.panTransactions.SuspendLayout();
            this.panTranTotalSelected.SuspendLayout();
            this.grpTranTypes.SuspendLayout();
            this.panHeader.SuspendLayout();
            this.panTransactionsDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvwTransactions)).BeginInit();
            this.ctxMenuStripTransactions.SuspendLayout();
            this.tabpFreePlays.SuspendLayout();
            this.panFreePlays.SuspendLayout();
            this.panFreePlaysDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvwFreePlaysDetails)).BeginInit();
            this.ctxMenuStripFreePlays.SuspendLayout();
            this.panFreePlayHeader.SuspendLayout();
            this.tabpComments.SuspendLayout();
            this.panComments.SuspendLayout();
            this.grpCommentsForCustomer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvwCustomerMessages)).BeginInit();
            this.grpCommentsForTW.SuspendLayout();
            this.tabpMarketing.SuspendLayout();
            this.panMarketing.SuspendLayout();
            this.grpGeneralComments.SuspendLayout();
            this.panGeneralMarketing.SuspendLayout();
            this.grpEstimatedWagerAmount.SuspendLayout();
            this.grpSportsOfInterest.SuspendLayout();
            this.grpContactRestrictions.SuspendLayout();
            this.tabpAgent.SuspendLayout();
            this.panAgent.SuspendLayout();
            this.panAutoCreateShadeGroups.SuspendLayout();
            this.panFirstAgentSettings.SuspendLayout();
            this.grpInternetCustomerAccess.SuspendLayout();
            this.grpHeadCountRate.SuspendLayout();
            this.grpCommissionType.SuspendLayout();
            this.panAffiliate.SuspendLayout();
            this.panMakeUpAndCasinoFee.SuspendLayout();
            this.grpMasterAgent.SuspendLayout();
            this.panSendPackage.SuspendLayout();
            this.tabpWagerLimits.SuspendLayout();
            this.panWagerLimits.SuspendLayout();
            this.grpMiscellaneous.SuspendLayout();
            this.panImportant.SuspendLayout();
            this.grpHorseLimits.SuspendLayout();
            this.grpParlayLimits.SuspendLayout();
            this.panWagerLimitsDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvwWagerLimitsDetails)).BeginInit();
            this.grpEnforceWagerLimits.SuspendLayout();
            this.tabpRestrictions.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.mnusMainMenu.SuspendLayout();
            this.panMainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtCustomerId
            // 
            this.txtCustomerId.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCustomerId.Location = new System.Drawing.Point(99, 10);
            this.txtCustomerId.MaxLength = 10;
            this.txtCustomerId.Name = "txtCustomerId";
            this.txtCustomerId.Size = new System.Drawing.Size(100, 20);
            this.txtCustomerId.TabIndex = 0;
            this.txtCustomerId.Text = "TW";
            this.txtCustomerId.TextChanged += new System.EventHandler(this.custIdtextBox_TextChanged);
            this.txtCustomerId.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCustomerId_KeyPress);
            // 
            // lblCustomerName
            // 
            this.lblCustomerName.AutoSize = true;
            this.lblCustomerName.Location = new System.Drawing.Point(61, 51);
            this.lblCustomerName.Name = "lblCustomerName";
            this.lblCustomerName.Size = new System.Drawing.Size(0, 13);
            this.lblCustomerName.TabIndex = 2;
            // 
            // lblCustNameLast
            // 
            this.lblCustNameLast.AutoSize = true;
            this.lblCustNameLast.Location = new System.Drawing.Point(64, 80);
            this.lblCustNameLast.Name = "lblCustNameLast";
            this.lblCustNameLast.Size = new System.Drawing.Size(0, 13);
            this.lblCustNameLast.TabIndex = 3;
            // 
            // tabCustomer
            // 
            this.tabCustomer.Controls.Add(this.tabpPersonal);
            this.tabCustomer.Controls.Add(this.tabpOffering);
            this.tabCustomer.Controls.Add(this.tabpVigDiscount);
            this.tabCustomer.Controls.Add(this.tabpPerformance);
            this.tabCustomer.Controls.Add(this.tabpTransactions);
            this.tabCustomer.Controls.Add(this.tabpFreePlays);
            this.tabCustomer.Controls.Add(this.tabpComments);
            this.tabCustomer.Controls.Add(this.tabpMarketing);
            this.tabCustomer.Controls.Add(this.tabpAgent);
            this.tabCustomer.Controls.Add(this.tabpWagerLimits);
            this.tabCustomer.Controls.Add(this.tabpRestrictions);
            this.tabCustomer.Location = new System.Drawing.Point(9, 58);
            this.tabCustomer.Name = "tabCustomer";
            this.tabCustomer.SelectedIndex = 0;
            this.tabCustomer.Size = new System.Drawing.Size(801, 500);
            this.tabCustomer.TabIndex = 8;
            this.tabCustomer.Tag = "Personal";
            this.tabCustomer.SelectedIndexChanged += new System.EventHandler(this.tabCustomer_SelectedIndexChanged);
            this.tabCustomer.Deselecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabCustomer_Deselecting);
            // 
            // tabpPersonal
            // 
            this.tabpPersonal.Controls.Add(this.panPersonal);
            this.tabpPersonal.Location = new System.Drawing.Point(4, 22);
            this.tabpPersonal.Name = "tabpPersonal";
            this.tabpPersonal.Padding = new System.Windows.Forms.Padding(3);
            this.tabpPersonal.Size = new System.Drawing.Size(793, 474);
            this.tabpPersonal.TabIndex = 0;
            this.tabpPersonal.Text = "Personal";
            this.tabpPersonal.UseVisualStyleBackColor = true;
            // 
            // panPersonal
            // 
            this.panPersonal.Controls.Add(this.grpSuspendedActions);
            this.panPersonal.Controls.Add(this.panGeneralInfo);
            this.panPersonal.Controls.Add(this.panMisc);
            this.panPersonal.Controls.Add(this.grpPasswords);
            this.panPersonal.Controls.Add(this.grpSuspendWagering);
            this.panPersonal.Controls.Add(this.grpPhone);
            this.panPersonal.Location = new System.Drawing.Point(25, 8);
            this.panPersonal.Name = "panPersonal";
            this.panPersonal.Size = new System.Drawing.Size(713, 451);
            this.panPersonal.TabIndex = 51;
            // 
            // grpSuspendedActions
            // 
            this.grpSuspendedActions.Controls.Add(this.chBrestrictionsBox);
            this.grpSuspendedActions.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.grpSuspendedActions.ForeColor = System.Drawing.Color.Red;
            this.grpSuspendedActions.Location = new System.Drawing.Point(426, 110);
            this.grpSuspendedActions.Name = "grpSuspendedActions";
            this.grpSuspendedActions.Size = new System.Drawing.Size(263, 147);
            this.grpSuspendedActions.TabIndex = 75;
            this.grpSuspendedActions.TabStop = false;
            this.grpSuspendedActions.Text = "Restrictions In Place";
            this.grpSuspendedActions.Visible = false;
            // 
            // chBrestrictionsBox
            // 
            this.chBrestrictionsBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.chBrestrictionsBox.ForeColor = System.Drawing.Color.Black;
            this.chBrestrictionsBox.FormattingEnabled = true;
            this.chBrestrictionsBox.Location = new System.Drawing.Point(7, 20);
            this.chBrestrictionsBox.Name = "chBrestrictionsBox";
            this.chBrestrictionsBox.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.chBrestrictionsBox.Size = new System.Drawing.Size(250, 120);
            this.chBrestrictionsBox.TabIndex = 0;
            // 
            // panGeneralInfo
            // 
            this.panGeneralInfo.Controls.Add(this.chbZeroBalancePositiveFigures);
            this.panGeneralInfo.Controls.Add(this.btnShowAuditSettleFigure);
            this.panGeneralInfo.Controls.Add(this.panPriceTypesList);
            this.panGeneralInfo.Controls.Add(this.txtSettleFigure);
            this.panGeneralInfo.Controls.Add(this.panTimeZonesList);
            this.panGeneralInfo.Controls.Add(this.txtChartPerc);
            this.panGeneralInfo.Controls.Add(this.panInetTargetsList);
            this.panGeneralInfo.Controls.Add(this.panCountriesList);
            this.panGeneralInfo.Controls.Add(this.txtCountry);
            this.panGeneralInfo.Controls.Add(this.panCurrencyCodesList);
            this.panGeneralInfo.Controls.Add(this.panAgentsList);
            this.panGeneralInfo.Controls.Add(this.panStatesProvList);
            this.panGeneralInfo.Controls.Add(this.lblName);
            this.panGeneralInfo.Controls.Add(this.lblchartPerc);
            this.panGeneralInfo.Controls.Add(this.lblState);
            this.panGeneralInfo.Controls.Add(this.lblCustNameLast);
            this.panGeneralInfo.Controls.Add(this.txtCity);
            this.panGeneralInfo.Controls.Add(this.lblInet);
            this.panGeneralInfo.Controls.Add(this.lblZipCode);
            this.panGeneralInfo.Controls.Add(this.lblCustomerName);
            this.panGeneralInfo.Controls.Add(this.lblCity);
            this.panGeneralInfo.Controls.Add(this.chbWeeklyLimits);
            this.panGeneralInfo.Controls.Add(this.txtNameFirst);
            this.panGeneralInfo.Controls.Add(this.lblTimeZone);
            this.panGeneralInfo.Controls.Add(this.txtZipCode);
            this.panGeneralInfo.Controls.Add(this.lblAddress);
            this.panGeneralInfo.Controls.Add(this.lblCurrency);
            this.panGeneralInfo.Controls.Add(this.txtNameMi);
            this.panGeneralInfo.Controls.Add(this.txtEmail);
            this.panGeneralInfo.Controls.Add(this.lblCountry);
            this.panGeneralInfo.Controls.Add(this.lblPriceType);
            this.panGeneralInfo.Controls.Add(this.txtAdress2);
            this.panGeneralInfo.Controls.Add(this.lblAgent);
            this.panGeneralInfo.Controls.Add(this.txtNameLast);
            this.panGeneralInfo.Controls.Add(this.lblSettleFigure);
            this.panGeneralInfo.Controls.Add(this.lblEmail);
            this.panGeneralInfo.Controls.Add(this.txtAddress);
            this.panGeneralInfo.Controls.Add(this.chbZeroBalance);
            this.panGeneralInfo.Location = new System.Drawing.Point(8, 8);
            this.panGeneralInfo.Name = "panGeneralInfo";
            this.panGeneralInfo.Size = new System.Drawing.Size(383, 407);
            this.panGeneralInfo.TabIndex = 2;
            // 
            // chbZeroBalancePositiveFigures
            // 
            this.chbZeroBalancePositiveFigures.AutoSize = true;
            this.chbZeroBalancePositiveFigures.Location = new System.Drawing.Point(213, 242);
            this.chbZeroBalancePositiveFigures.Name = "chbZeroBalancePositiveFigures";
            this.chbZeroBalancePositiveFigures.Size = new System.Drawing.Size(167, 17);
            this.chbZeroBalancePositiveFigures.TabIndex = 202;
            this.chbZeroBalancePositiveFigures.Text = "Zero Bal Positive Figures Only";
            this.chbZeroBalancePositiveFigures.UseVisualStyleBackColor = true;
            // 
            // btnShowAuditSettleFigure
            // 
            this.btnShowAuditSettleFigure.Location = new System.Drawing.Point(365, 292);
            this.btnShowAuditSettleFigure.Name = "btnShowAuditSettleFigure";
            this.btnShowAuditSettleFigure.Size = new System.Drawing.Size(14, 23);
            this.btnShowAuditSettleFigure.TabIndex = 201;
            this.btnShowAuditSettleFigure.Text = "?";
            this.btnShowAuditSettleFigure.UseVisualStyleBackColor = true;
            this.btnShowAuditSettleFigure.Click += new System.EventHandler(this.btnShowAuditSettleFigure_Click);
            // 
            // panPriceTypesList
            // 
            this.panPriceTypesList.Location = new System.Drawing.Point(68, 327);
            this.panPriceTypesList.Name = "panPriceTypesList";
            this.panPriceTypesList.Size = new System.Drawing.Size(139, 30);
            this.panPriceTypesList.TabIndex = 190;
            // 
            // txtSettleFigure
            // 
            this.txtSettleFigure.AllowCents = false;
            this.txtSettleFigure.AllowNegatives = false;
            this.txtSettleFigure.DividedBy = 1;
            this.txtSettleFigure.DivideFlag = false;
            this.txtSettleFigure.Location = new System.Drawing.Point(293, 295);
            this.txtSettleFigure.Name = "txtSettleFigure";
            this.txtSettleFigure.ShowIfZero = true;
            this.txtSettleFigure.Size = new System.Drawing.Size(67, 20);
            this.txtSettleFigure.TabIndex = 180;
            this.txtSettleFigure.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSettleFigure.Leave += new System.EventHandler(this.numberToText_Leave);
            // 
            // panTimeZonesList
            // 
            this.panTimeZonesList.Location = new System.Drawing.Point(68, 291);
            this.panTimeZonesList.Name = "panTimeZonesList";
            this.panTimeZonesList.Size = new System.Drawing.Size(139, 30);
            this.panTimeZonesList.TabIndex = 170;
            // 
            // txtChartPerc
            // 
            this.txtChartPerc.AllowCents = false;
            this.txtChartPerc.AllowNegatives = false;
            this.txtChartPerc.DividedBy = 1;
            this.txtChartPerc.DivideFlag = false;
            this.txtChartPerc.Location = new System.Drawing.Point(313, 269);
            this.txtChartPerc.Name = "txtChartPerc";
            this.txtChartPerc.ShowIfZero = true;
            this.txtChartPerc.Size = new System.Drawing.Size(42, 20);
            this.txtChartPerc.TabIndex = 160;
            this.txtChartPerc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtChartPerc.Leave += new System.EventHandler(this.numberToText_Leave);
            // 
            // panInetTargetsList
            // 
            this.panInetTargetsList.Location = new System.Drawing.Point(68, 255);
            this.panInetTargetsList.Name = "panInetTargetsList";
            this.panInetTargetsList.Size = new System.Drawing.Size(139, 30);
            this.panInetTargetsList.TabIndex = 150;
            // 
            // panCountriesList
            // 
            this.panCountriesList.Location = new System.Drawing.Point(221, 326);
            this.panCountriesList.Name = "panCountriesList";
            this.panCountriesList.Size = new System.Drawing.Size(139, 30);
            this.panCountriesList.TabIndex = 200;
            // 
            // txtCountry
            // 
            this.txtCountry.Location = new System.Drawing.Point(68, 157);
            this.txtCountry.Name = "txtCountry";
            this.txtCountry.Size = new System.Drawing.Size(89, 20);
            this.txtCountry.TabIndex = 90;
            // 
            // panCurrencyCodesList
            // 
            this.panCurrencyCodesList.Location = new System.Drawing.Point(68, 183);
            this.panCurrencyCodesList.Name = "panCurrencyCodesList";
            this.panCurrencyCodesList.Size = new System.Drawing.Size(189, 30);
            this.panCurrencyCodesList.TabIndex = 110;
            // 
            // panAgentsList
            // 
            this.panAgentsList.Location = new System.Drawing.Point(68, 219);
            this.panAgentsList.Name = "panAgentsList";
            this.panAgentsList.Size = new System.Drawing.Size(139, 30);
            this.panAgentsList.TabIndex = 130;
            // 
            // panStatesProvList
            // 
            this.panStatesProvList.Location = new System.Drawing.Point(70, 121);
            this.panStatesProvList.Name = "panStatesProvList";
            this.panStatesProvList.Size = new System.Drawing.Size(139, 30);
            this.panStatesProvList.TabIndex = 70;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(26, 22);
            this.lblName.Name = "lblName";
            this.lblName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblName.Size = new System.Drawing.Size(38, 13);
            this.lblName.TabIndex = 8;
            this.lblName.Text = "Name:";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblchartPerc
            // 
            this.lblchartPerc.AutoSize = true;
            this.lblchartPerc.Location = new System.Drawing.Point(233, 272);
            this.lblchartPerc.Name = "lblchartPerc";
            this.lblchartPerc.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblchartPerc.Size = new System.Drawing.Size(74, 13);
            this.lblchartPerc.TabIndex = 33;
            this.lblchartPerc.Text = "Chart % Book:";
            this.lblchartPerc.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblState
            // 
            this.lblState.AutoSize = true;
            this.lblState.Location = new System.Drawing.Point(4, 129);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(62, 13);
            this.lblState.TabIndex = 18;
            this.lblState.Text = "State/Prov:";
            // 
            // txtCity
            // 
            this.txtCity.Location = new System.Drawing.Point(68, 95);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(290, 20);
            this.txtCity.TabIndex = 60;
            // 
            // lblInet
            // 
            this.lblInet.AutoSize = true;
            this.lblInet.Location = new System.Drawing.Point(33, 264);
            this.lblInet.Name = "lblInet";
            this.lblInet.Size = new System.Drawing.Size(28, 13);
            this.lblInet.TabIndex = 31;
            this.lblInet.Text = "Inet:";
            // 
            // lblZipCode
            // 
            this.lblZipCode.AutoSize = true;
            this.lblZipCode.Location = new System.Drawing.Point(215, 138);
            this.lblZipCode.Name = "lblZipCode";
            this.lblZipCode.Size = new System.Drawing.Size(55, 13);
            this.lblZipCode.TabIndex = 19;
            this.lblZipCode.Text = "Postal Cd:";
            // 
            // lblCity
            // 
            this.lblCity.AutoSize = true;
            this.lblCity.Location = new System.Drawing.Point(35, 98);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(27, 13);
            this.lblCity.TabIndex = 15;
            this.lblCity.Text = "City:";
            // 
            // chbWeeklyLimits
            // 
            this.chbWeeklyLimits.AutoSize = true;
            this.chbWeeklyLimits.Location = new System.Drawing.Point(263, 192);
            this.chbWeeklyLimits.Name = "chbWeeklyLimits";
            this.chbWeeklyLimits.Size = new System.Drawing.Size(91, 17);
            this.chbWeeklyLimits.TabIndex = 140;
            this.chbWeeklyLimits.Text = "Weekly Limits";
            this.chbWeeklyLimits.UseVisualStyleBackColor = true;
            this.chbWeeklyLimits.CheckedChanged += new System.EventHandler(this.chbWeeklyLimits_CheckedChanged);
            // 
            // txtNameFirst
            // 
            this.txtNameFirst.Location = new System.Drawing.Point(70, 19);
            this.txtNameFirst.Name = "txtNameFirst";
            this.txtNameFirst.Size = new System.Drawing.Size(100, 20);
            this.txtNameFirst.TabIndex = 10;
            // 
            // lblTimeZone
            // 
            this.lblTimeZone.AutoSize = true;
            this.lblTimeZone.Location = new System.Drawing.Point(1, 300);
            this.lblTimeZone.Name = "lblTimeZone";
            this.lblTimeZone.Size = new System.Drawing.Size(61, 13);
            this.lblTimeZone.TabIndex = 35;
            this.lblTimeZone.Text = "Time Zone:";
            // 
            // txtZipCode
            // 
            this.txtZipCode.Location = new System.Drawing.Point(279, 131);
            this.txtZipCode.Name = "txtZipCode";
            this.txtZipCode.Size = new System.Drawing.Size(79, 20);
            this.txtZipCode.TabIndex = 80;
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Location = new System.Drawing.Point(13, 46);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(48, 13);
            this.lblAddress.TabIndex = 14;
            this.lblAddress.Text = "Address:";
            // 
            // lblCurrency
            // 
            this.lblCurrency.AutoSize = true;
            this.lblCurrency.Location = new System.Drawing.Point(10, 193);
            this.lblCurrency.Name = "lblCurrency";
            this.lblCurrency.Size = new System.Drawing.Size(52, 13);
            this.lblCurrency.TabIndex = 25;
            this.lblCurrency.Text = "Currency:";
            // 
            // txtNameMi
            // 
            this.txtNameMi.Location = new System.Drawing.Point(176, 19);
            this.txtNameMi.Name = "txtNameMi";
            this.txtNameMi.Size = new System.Drawing.Size(27, 20);
            this.txtNameMi.TabIndex = 20;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(208, 157);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(150, 20);
            this.txtEmail.TabIndex = 100;
            // 
            // lblCountry
            // 
            this.lblCountry.AutoSize = true;
            this.lblCountry.Location = new System.Drawing.Point(16, 160);
            this.lblCountry.Name = "lblCountry";
            this.lblCountry.Size = new System.Drawing.Size(46, 13);
            this.lblCountry.TabIndex = 21;
            this.lblCountry.Text = "Country:";
            // 
            // lblPriceType
            // 
            this.lblPriceType.AutoSize = true;
            this.lblPriceType.Location = new System.Drawing.Point(1, 336);
            this.lblPriceType.Name = "lblPriceType";
            this.lblPriceType.Size = new System.Drawing.Size(61, 13);
            this.lblPriceType.TabIndex = 39;
            this.lblPriceType.Text = "Price Type:";
            // 
            // txtAdress2
            // 
            this.txtAdress2.Location = new System.Drawing.Point(70, 69);
            this.txtAdress2.MaxLength = 90;
            this.txtAdress2.Name = "txtAdress2";
            this.txtAdress2.Size = new System.Drawing.Size(288, 20);
            this.txtAdress2.TabIndex = 50;
            // 
            // lblAgent
            // 
            this.lblAgent.AutoSize = true;
            this.lblAgent.Location = new System.Drawing.Point(24, 228);
            this.lblAgent.Name = "lblAgent";
            this.lblAgent.Size = new System.Drawing.Size(38, 13);
            this.lblAgent.TabIndex = 28;
            this.lblAgent.Text = "Agent:";
            // 
            // txtNameLast
            // 
            this.txtNameLast.Location = new System.Drawing.Point(218, 19);
            this.txtNameLast.Name = "txtNameLast";
            this.txtNameLast.Size = new System.Drawing.Size(140, 20);
            this.txtNameLast.TabIndex = 30;
            // 
            // lblSettleFigure
            // 
            this.lblSettleFigure.AutoSize = true;
            this.lblSettleFigure.Location = new System.Drawing.Point(218, 298);
            this.lblSettleFigure.Name = "lblSettleFigure";
            this.lblSettleFigure.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblSettleFigure.Size = new System.Drawing.Size(69, 13);
            this.lblSettleFigure.TabIndex = 37;
            this.lblSettleFigure.Text = "Settle Figure:";
            this.lblSettleFigure.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Location = new System.Drawing.Point(163, 160);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(39, 13);
            this.lblEmail.TabIndex = 23;
            this.lblEmail.Text = "E-Mail:";
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(70, 43);
            this.txtAddress.MaxLength = 90;
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(288, 20);
            this.txtAddress.TabIndex = 40;
            // 
            // chbZeroBalance
            // 
            this.chbZeroBalance.AutoSize = true;
            this.chbZeroBalance.Location = new System.Drawing.Point(213, 219);
            this.chbZeroBalance.Name = "chbZeroBalance";
            this.chbZeroBalance.Size = new System.Drawing.Size(90, 17);
            this.chbZeroBalance.TabIndex = 120;
            this.chbZeroBalance.Text = "Zero Balance";
            this.chbZeroBalance.UseVisualStyleBackColor = true;
            this.chbZeroBalance.CheckedChanged += new System.EventHandler(this.chbZeroBalance_CheckedChanged);
            // 
            // panMisc
            // 
            this.panMisc.Controls.Add(this.txtSecondsToDelay);
            this.panMisc.Controls.Add(this.lblSecondsToDelay);
            this.panMisc.Controls.Add(this.chbShowOnInstantAction);
            this.panMisc.Controls.Add(this.chbWiseGuy);
            this.panMisc.Location = new System.Drawing.Point(426, 286);
            this.panMisc.Name = "panMisc";
            this.panMisc.Size = new System.Drawing.Size(263, 100);
            this.panMisc.TabIndex = 6;
            // 
            // txtSecondsToDelay
            // 
            this.txtSecondsToDelay.AllowCents = false;
            this.txtSecondsToDelay.AllowNegatives = false;
            this.txtSecondsToDelay.DividedBy = 1;
            this.txtSecondsToDelay.DivideFlag = false;
            this.txtSecondsToDelay.Location = new System.Drawing.Point(20, 62);
            this.txtSecondsToDelay.Name = "txtSecondsToDelay";
            this.txtSecondsToDelay.ShowIfZero = true;
            this.txtSecondsToDelay.Size = new System.Drawing.Size(33, 20);
            this.txtSecondsToDelay.TabIndex = 300;
            this.txtSecondsToDelay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtSecondsToDelay.Leave += new System.EventHandler(this.numberToText_Leave);
            // 
            // lblSecondsToDelay
            // 
            this.lblSecondsToDelay.AutoSize = true;
            this.lblSecondsToDelay.Location = new System.Drawing.Point(59, 65);
            this.lblSecondsToDelay.Name = "lblSecondsToDelay";
            this.lblSecondsToDelay.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblSecondsToDelay.Size = new System.Drawing.Size(195, 13);
            this.lblSecondsToDelay.TabIndex = 41;
            this.lblSecondsToDelay.Text = "Seconds To Delay Internet Confirmation";
            this.lblSecondsToDelay.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // chbShowOnInstantAction
            // 
            this.chbShowOnInstantAction.AutoSize = true;
            this.chbShowOnInstantAction.Location = new System.Drawing.Point(26, 13);
            this.chbShowOnInstantAction.Name = "chbShowOnInstantAction";
            this.chbShowOnInstantAction.Size = new System.Drawing.Size(204, 17);
            this.chbShowOnInstantAction.TabIndex = 280;
            this.chbShowOnInstantAction.Text = "Always Show On Instant Action Panel";
            this.chbShowOnInstantAction.UseVisualStyleBackColor = true;
            // 
            // chbWiseGuy
            // 
            this.chbWiseGuy.AutoSize = true;
            this.chbWiseGuy.Location = new System.Drawing.Point(26, 37);
            this.chbWiseGuy.Name = "chbWiseGuy";
            this.chbWiseGuy.Size = new System.Drawing.Size(83, 17);
            this.chbWiseGuy.TabIndex = 290;
            this.chbWiseGuy.Text = "Wise Action";
            this.chbWiseGuy.UseVisualStyleBackColor = true;
            // 
            // grpPasswords
            // 
            this.grpPasswords.Controls.Add(this.btnShowAuditPayoutPassword);
            this.grpPasswords.Controls.Add(this.btnShowAuditPassword);
            this.grpPasswords.Controls.Add(this.lblPayoutPassword);
            this.grpPasswords.Controls.Add(this.lblWageringPassword);
            this.grpPasswords.Controls.Add(this.txtWageringPassword);
            this.grpPasswords.Controls.Add(this.txtPayoutPassword);
            this.grpPasswords.Location = new System.Drawing.Point(426, 196);
            this.grpPasswords.Name = "grpPasswords";
            this.grpPasswords.Size = new System.Drawing.Size(263, 93);
            this.grpPasswords.TabIndex = 5;
            this.grpPasswords.TabStop = false;
            this.grpPasswords.Text = "Passwords";
            // 
            // btnShowAuditPayoutPassword
            // 
            this.btnShowAuditPayoutPassword.Location = new System.Drawing.Point(192, 56);
            this.btnShowAuditPayoutPassword.Name = "btnShowAuditPayoutPassword";
            this.btnShowAuditPayoutPassword.Size = new System.Drawing.Size(14, 23);
            this.btnShowAuditPayoutPassword.TabIndex = 271;
            this.btnShowAuditPayoutPassword.Text = "?";
            this.btnShowAuditPayoutPassword.UseVisualStyleBackColor = true;
            this.btnShowAuditPayoutPassword.Click += new System.EventHandler(this.btnShowAuditPayoutPassword_Click);
            // 
            // btnShowAuditPassword
            // 
            this.btnShowAuditPassword.Location = new System.Drawing.Point(192, 20);
            this.btnShowAuditPassword.Name = "btnShowAuditPassword";
            this.btnShowAuditPassword.Size = new System.Drawing.Size(14, 23);
            this.btnShowAuditPassword.TabIndex = 202;
            this.btnShowAuditPassword.Text = "?";
            this.btnShowAuditPassword.UseVisualStyleBackColor = true;
            this.btnShowAuditPassword.Click += new System.EventHandler(this.btnShowAuditPassword_Click);
            // 
            // lblPayoutPassword
            // 
            this.lblPayoutPassword.AutoSize = true;
            this.lblPayoutPassword.Location = new System.Drawing.Point(17, 61);
            this.lblPayoutPassword.Name = "lblPayoutPassword";
            this.lblPayoutPassword.Size = new System.Drawing.Size(43, 13);
            this.lblPayoutPassword.TabIndex = 46;
            this.lblPayoutPassword.Text = "Payout:";
            this.lblPayoutPassword.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblWageringPassword
            // 
            this.lblWageringPassword.AutoSize = true;
            this.lblWageringPassword.Location = new System.Drawing.Point(12, 25);
            this.lblWageringPassword.Name = "lblWageringPassword";
            this.lblWageringPassword.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblWageringPassword.Size = new System.Drawing.Size(56, 13);
            this.lblWageringPassword.TabIndex = 42;
            this.lblWageringPassword.Text = "Wagering:";
            this.lblWageringPassword.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtWageringPassword
            // 
            this.txtWageringPassword.Location = new System.Drawing.Point(81, 23);
            this.txtWageringPassword.Name = "txtWageringPassword";
            this.txtWageringPassword.Size = new System.Drawing.Size(105, 20);
            this.txtWageringPassword.TabIndex = 260;
            this.txtWageringPassword.Click += new System.EventHandler(this.txtWageringPassword_Click);
            // 
            // txtPayoutPassword
            // 
            this.txtPayoutPassword.Location = new System.Drawing.Point(83, 59);
            this.txtPayoutPassword.Name = "txtPayoutPassword";
            this.txtPayoutPassword.Size = new System.Drawing.Size(103, 20);
            this.txtPayoutPassword.TabIndex = 270;
            this.txtPayoutPassword.Click += new System.EventHandler(this.txtPayoutPassword_Click);
            // 
            // grpSuspendWagering
            // 
            this.grpSuspendWagering.Controls.Add(this.chbCasino);
            this.grpSuspendWagering.Controls.Add(this.chbSuspendSports);
            this.grpSuspendWagering.Enabled = false;
            this.grpSuspendWagering.Location = new System.Drawing.Point(426, 127);
            this.grpSuspendWagering.Name = "grpSuspendWagering";
            this.grpSuspendWagering.Size = new System.Drawing.Size(263, 54);
            this.grpSuspendWagering.TabIndex = 4;
            this.grpSuspendWagering.TabStop = false;
            this.grpSuspendWagering.Text = "Suspend Wagering";
            this.grpSuspendWagering.Visible = false;
            // 
            // chbCasino
            // 
            this.chbCasino.AutoSize = true;
            this.chbCasino.Location = new System.Drawing.Point(137, 25);
            this.chbCasino.Name = "chbCasino";
            this.chbCasino.Size = new System.Drawing.Size(58, 17);
            this.chbCasino.TabIndex = 250;
            this.chbCasino.Text = "Casino";
            this.chbCasino.UseVisualStyleBackColor = true;
            // 
            // chbSuspendSports
            // 
            this.chbSuspendSports.AutoSize = true;
            this.chbSuspendSports.Location = new System.Drawing.Point(15, 25);
            this.chbSuspendSports.Name = "chbSuspendSports";
            this.chbSuspendSports.Size = new System.Drawing.Size(81, 17);
            this.chbSuspendSports.TabIndex = 240;
            this.chbSuspendSports.Text = "SportsBook";
            this.chbSuspendSports.UseVisualStyleBackColor = true;
            // 
            // grpPhone
            // 
            this.grpPhone.Controls.Add(this.lblFax);
            this.grpPhone.Controls.Add(this.txtFax);
            this.grpPhone.Controls.Add(this.lblBusinessPhone);
            this.grpPhone.Controls.Add(this.lblHomePhone);
            this.grpPhone.Controls.Add(this.txtHomePhone);
            this.grpPhone.Controls.Add(this.txtBusinessPhone);
            this.grpPhone.Location = new System.Drawing.Point(426, 2);
            this.grpPhone.Name = "grpPhone";
            this.grpPhone.Size = new System.Drawing.Size(263, 102);
            this.grpPhone.TabIndex = 3;
            this.grpPhone.TabStop = false;
            this.grpPhone.Text = "Phone";
            // 
            // lblFax
            // 
            this.lblFax.AutoSize = true;
            this.lblFax.Location = new System.Drawing.Point(59, 76);
            this.lblFax.Name = "lblFax";
            this.lblFax.Size = new System.Drawing.Size(27, 13);
            this.lblFax.TabIndex = 47;
            this.lblFax.Text = "Fax:";
            this.lblFax.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtFax
            // 
            this.txtFax.Location = new System.Drawing.Point(98, 76);
            this.txtFax.Name = "txtFax";
            this.txtFax.Size = new System.Drawing.Size(103, 20);
            this.txtFax.TabIndex = 230;
            // 
            // lblBusinessPhone
            // 
            this.lblBusinessPhone.AutoSize = true;
            this.lblBusinessPhone.Location = new System.Drawing.Point(39, 50);
            this.lblBusinessPhone.Name = "lblBusinessPhone";
            this.lblBusinessPhone.Size = new System.Drawing.Size(52, 13);
            this.lblBusinessPhone.TabIndex = 46;
            this.lblBusinessPhone.Text = "Business:";
            this.lblBusinessPhone.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblHomePhone
            // 
            this.lblHomePhone.AutoSize = true;
            this.lblHomePhone.Location = new System.Drawing.Point(54, 19);
            this.lblHomePhone.Name = "lblHomePhone";
            this.lblHomePhone.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblHomePhone.Size = new System.Drawing.Size(38, 13);
            this.lblHomePhone.TabIndex = 42;
            this.lblHomePhone.Text = "Home:";
            this.lblHomePhone.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtHomePhone
            // 
            this.txtHomePhone.Location = new System.Drawing.Point(99, 15);
            this.txtHomePhone.Name = "txtHomePhone";
            this.txtHomePhone.Size = new System.Drawing.Size(100, 20);
            this.txtHomePhone.TabIndex = 210;
            // 
            // txtBusinessPhone
            // 
            this.txtBusinessPhone.Location = new System.Drawing.Point(97, 44);
            this.txtBusinessPhone.Name = "txtBusinessPhone";
            this.txtBusinessPhone.Size = new System.Drawing.Size(103, 20);
            this.txtBusinessPhone.TabIndex = 220;
            // 
            // tabpOffering
            // 
            this.tabpOffering.Controls.Add(this.panOfferingTab);
            this.tabpOffering.Location = new System.Drawing.Point(4, 22);
            this.tabpOffering.Name = "tabpOffering";
            this.tabpOffering.Padding = new System.Windows.Forms.Padding(3);
            this.tabpOffering.Size = new System.Drawing.Size(793, 474);
            this.tabpOffering.TabIndex = 1;
            this.tabpOffering.Text = "Offering";
            this.tabpOffering.UseVisualStyleBackColor = true;
            // 
            // panOfferingTab
            // 
            this.panOfferingTab.Controls.Add(this.btnBetServiceProfiles);
            this.panOfferingTab.Controls.Add(this.grpFreeHalfPoint);
            this.panOfferingTab.Controls.Add(this.grpBaseball);
            this.panOfferingTab.Controls.Add(this.grpStoreAndShadeGroups);
            this.panOfferingTab.Controls.Add(this.panVariousSettings);
            this.panOfferingTab.Controls.Add(this.grpWagerLimits);
            this.panOfferingTab.Controls.Add(this.grpCreditLimit);
            this.panOfferingTab.Controls.Add(this.panParlayAndTeasers);
            this.panOfferingTab.Location = new System.Drawing.Point(5, 6);
            this.panOfferingTab.Name = "panOfferingTab";
            this.panOfferingTab.Size = new System.Drawing.Size(773, 454);
            this.panOfferingTab.TabIndex = 52;
            // 
            // btnBetServiceProfiles
            // 
            this.btnBetServiceProfiles.Enabled = false;
            this.btnBetServiceProfiles.Location = new System.Drawing.Point(374, 411);
            this.btnBetServiceProfiles.Name = "btnBetServiceProfiles";
            this.btnBetServiceProfiles.Size = new System.Drawing.Size(119, 23);
            this.btnBetServiceProfiles.TabIndex = 390;
            this.btnBetServiceProfiles.Text = "BetService Profiles...";
            this.btnBetServiceProfiles.UseVisualStyleBackColor = true;
            this.btnBetServiceProfiles.Click += new System.EventHandler(this.betServiceProfilesCutton_Click);
            // 
            // grpFreeHalfPoint
            // 
            this.grpFreeHalfPoint.Controls.Add(this.chbNoFreeHalfPointInTotals);
            this.grpFreeHalfPoint.Controls.Add(this.grpFreeHalfPntOnOff);
            this.grpFreeHalfPoint.Controls.Add(this.txtMaxFreeHalfPoint);
            this.grpFreeHalfPoint.Controls.Add(this.lblMaxFreeHalfPoint);
            this.grpFreeHalfPoint.Controls.Add(this.panInetFootball);
            this.grpFreeHalfPoint.Controls.Add(this.panCuFootball);
            this.grpFreeHalfPoint.Controls.Add(this.panInetBasketball);
            this.grpFreeHalfPoint.Controls.Add(this.panCuBasketball);
            this.grpFreeHalfPoint.Controls.Add(this.chbEnforceHalfPoint);
            this.grpFreeHalfPoint.Controls.Add(this.chbInetFootball);
            this.grpFreeHalfPoint.Controls.Add(this.chbCuFootball);
            this.grpFreeHalfPoint.Controls.Add(this.chbInetBasketball);
            this.grpFreeHalfPoint.Controls.Add(this.chbCuBasketball);
            this.grpFreeHalfPoint.Location = new System.Drawing.Point(374, 186);
            this.grpFreeHalfPoint.Name = "grpFreeHalfPoint";
            this.grpFreeHalfPoint.Size = new System.Drawing.Size(387, 215);
            this.grpFreeHalfPoint.TabIndex = 290;
            this.grpFreeHalfPoint.TabStop = false;
            this.grpFreeHalfPoint.Text = "Free Half Point";
            // 
            // chbNoFreeHalfPointInTotals
            // 
            this.chbNoFreeHalfPointInTotals.AutoSize = true;
            this.chbNoFreeHalfPointInTotals.Location = new System.Drawing.Point(25, 158);
            this.chbNoFreeHalfPointInTotals.Name = "chbNoFreeHalfPointInTotals";
            this.chbNoFreeHalfPointInTotals.Size = new System.Drawing.Size(95, 17);
            this.chbNoFreeHalfPointInTotals.TabIndex = 382;
            this.chbNoFreeHalfPointInTotals.Text = "Deny In Totals";
            this.chbNoFreeHalfPointInTotals.UseVisualStyleBackColor = true;
            // 
            // grpFreeHalfPntOnOff
            // 
            this.grpFreeHalfPntOnOff.Controls.Add(this.chbNFLFOOTBALLCostToBuyOFF7FreeHalfPont);
            this.grpFreeHalfPntOnOff.Controls.Add(this.chbNFLFOOTBALLCostToBuyON7FreeHalfPont);
            this.grpFreeHalfPntOnOff.Controls.Add(this.chbNFLFOOTBALLCostToBuyOFF3FreeHalfPont);
            this.grpFreeHalfPntOnOff.Controls.Add(this.chbNFLFOOTBALLCostToBuyON3FreeHalfPont);
            this.grpFreeHalfPntOnOff.Location = new System.Drawing.Point(307, 89);
            this.grpFreeHalfPntOnOff.Name = "grpFreeHalfPntOnOff";
            this.grpFreeHalfPntOnOff.Size = new System.Drawing.Size(72, 114);
            this.grpFreeHalfPntOnOff.TabIndex = 381;
            this.grpFreeHalfPntOnOff.TabStop = false;
            this.grpFreeHalfPntOnOff.Text = "Football";
            // 
            // chbNFLFOOTBALLCostToBuyOFF7FreeHalfPont
            // 
            this.chbNFLFOOTBALLCostToBuyOFF7FreeHalfPont.AutoSize = true;
            this.chbNFLFOOTBALLCostToBuyOFF7FreeHalfPont.Location = new System.Drawing.Point(6, 94);
            this.chbNFLFOOTBALLCostToBuyOFF7FreeHalfPont.Name = "chbNFLFOOTBALLCostToBuyOFF7FreeHalfPont";
            this.chbNFLFOOTBALLCostToBuyOFF7FreeHalfPont.Size = new System.Drawing.Size(49, 17);
            this.chbNFLFOOTBALLCostToBuyOFF7FreeHalfPont.TabIndex = 3;
            this.chbNFLFOOTBALLCostToBuyOFF7FreeHalfPont.Text = "Off 7";
            this.chbNFLFOOTBALLCostToBuyOFF7FreeHalfPont.UseVisualStyleBackColor = true;
            // 
            // chbNFLFOOTBALLCostToBuyON7FreeHalfPont
            // 
            this.chbNFLFOOTBALLCostToBuyON7FreeHalfPont.AutoSize = true;
            this.chbNFLFOOTBALLCostToBuyON7FreeHalfPont.Location = new System.Drawing.Point(6, 69);
            this.chbNFLFOOTBALLCostToBuyON7FreeHalfPont.Name = "chbNFLFOOTBALLCostToBuyON7FreeHalfPont";
            this.chbNFLFOOTBALLCostToBuyON7FreeHalfPont.Size = new System.Drawing.Size(65, 17);
            this.chbNFLFOOTBALLCostToBuyON7FreeHalfPont.TabIndex = 2;
            this.chbNFLFOOTBALLCostToBuyON7FreeHalfPont.Text = "On To 7";
            this.chbNFLFOOTBALLCostToBuyON7FreeHalfPont.UseVisualStyleBackColor = true;
            // 
            // chbNFLFOOTBALLCostToBuyOFF3FreeHalfPont
            // 
            this.chbNFLFOOTBALLCostToBuyOFF3FreeHalfPont.AutoSize = true;
            this.chbNFLFOOTBALLCostToBuyOFF3FreeHalfPont.Location = new System.Drawing.Point(6, 44);
            this.chbNFLFOOTBALLCostToBuyOFF3FreeHalfPont.Name = "chbNFLFOOTBALLCostToBuyOFF3FreeHalfPont";
            this.chbNFLFOOTBALLCostToBuyOFF3FreeHalfPont.Size = new System.Drawing.Size(49, 17);
            this.chbNFLFOOTBALLCostToBuyOFF3FreeHalfPont.TabIndex = 1;
            this.chbNFLFOOTBALLCostToBuyOFF3FreeHalfPont.Text = "Off 3";
            this.chbNFLFOOTBALLCostToBuyOFF3FreeHalfPont.UseVisualStyleBackColor = true;
            // 
            // chbNFLFOOTBALLCostToBuyON3FreeHalfPont
            // 
            this.chbNFLFOOTBALLCostToBuyON3FreeHalfPont.AutoSize = true;
            this.chbNFLFOOTBALLCostToBuyON3FreeHalfPont.Location = new System.Drawing.Point(6, 19);
            this.chbNFLFOOTBALLCostToBuyON3FreeHalfPont.Name = "chbNFLFOOTBALLCostToBuyON3FreeHalfPont";
            this.chbNFLFOOTBALLCostToBuyON3FreeHalfPont.Size = new System.Drawing.Size(65, 17);
            this.chbNFLFOOTBALLCostToBuyON3FreeHalfPont.TabIndex = 0;
            this.chbNFLFOOTBALLCostToBuyON3FreeHalfPont.Text = "On To 3";
            this.chbNFLFOOTBALLCostToBuyON3FreeHalfPont.UseVisualStyleBackColor = true;
            // 
            // txtMaxFreeHalfPoint
            // 
            this.txtMaxFreeHalfPoint.AllowCents = false;
            this.txtMaxFreeHalfPoint.AllowNegatives = false;
            this.txtMaxFreeHalfPoint.DividedBy = 1;
            this.txtMaxFreeHalfPoint.DivideFlag = false;
            this.txtMaxFreeHalfPoint.Location = new System.Drawing.Point(162, 183);
            this.txtMaxFreeHalfPoint.Name = "txtMaxFreeHalfPoint";
            this.txtMaxFreeHalfPoint.ShowIfZero = true;
            this.txtMaxFreeHalfPoint.Size = new System.Drawing.Size(117, 20);
            this.txtMaxFreeHalfPoint.TabIndex = 380;
            this.txtMaxFreeHalfPoint.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMaxFreeHalfPoint.Leave += new System.EventHandler(this.numberToText_Leave);
            // 
            // lblMaxFreeHalfPoint
            // 
            this.lblMaxFreeHalfPoint.AutoSize = true;
            this.lblMaxFreeHalfPoint.Location = new System.Drawing.Point(116, 190);
            this.lblMaxFreeHalfPoint.Name = "lblMaxFreeHalfPoint";
            this.lblMaxFreeHalfPoint.Size = new System.Drawing.Size(43, 13);
            this.lblMaxFreeHalfPoint.TabIndex = 8;
            this.lblMaxFreeHalfPoint.Text = "up to $:";
            // 
            // panInetFootball
            // 
            this.panInetFootball.Location = new System.Drawing.Point(162, 124);
            this.panInetFootball.Name = "panInetFootball";
            this.panInetFootball.Size = new System.Drawing.Size(139, 30);
            this.panInetFootball.TabIndex = 370;
            // 
            // panCuFootball
            // 
            this.panCuFootball.Location = new System.Drawing.Point(162, 90);
            this.panCuFootball.Name = "panCuFootball";
            this.panCuFootball.Size = new System.Drawing.Size(139, 30);
            this.panCuFootball.TabIndex = 350;
            // 
            // panInetBasketball
            // 
            this.panInetBasketball.Location = new System.Drawing.Point(162, 56);
            this.panInetBasketball.Name = "panInetBasketball";
            this.panInetBasketball.Size = new System.Drawing.Size(139, 30);
            this.panInetBasketball.TabIndex = 330;
            // 
            // panCuBasketball
            // 
            this.panCuBasketball.Location = new System.Drawing.Point(163, 21);
            this.panCuBasketball.Name = "panCuBasketball";
            this.panCuBasketball.Size = new System.Drawing.Size(139, 30);
            this.panCuBasketball.TabIndex = 310;
            // 
            // chbEnforceHalfPoint
            // 
            this.chbEnforceHalfPoint.AutoSize = true;
            this.chbEnforceHalfPoint.Location = new System.Drawing.Point(28, 190);
            this.chbEnforceHalfPoint.Name = "chbEnforceHalfPoint";
            this.chbEnforceHalfPoint.Size = new System.Drawing.Size(87, 17);
            this.chbEnforceHalfPoint.TabIndex = 360;
            this.chbEnforceHalfPoint.Text = "Enforce Limit";
            this.chbEnforceHalfPoint.UseVisualStyleBackColor = true;
            this.chbEnforceHalfPoint.CheckedChanged += new System.EventHandler(this.chbFreeHalfPoints_CheckedChanged);
            // 
            // chbInetFootball
            // 
            this.chbInetFootball.AutoSize = true;
            this.chbInetFootball.Location = new System.Drawing.Point(25, 126);
            this.chbInetFootball.Name = "chbInetFootball";
            this.chbInetFootball.Size = new System.Drawing.Size(117, 17);
            this.chbInetFootball.TabIndex = 360;
            this.chbInetFootball.Text = "Internet Football on";
            this.chbInetFootball.UseVisualStyleBackColor = true;
            this.chbInetFootball.CheckedChanged += new System.EventHandler(this.chbFreeHalfPoints_CheckedChanged);
            // 
            // chbCuFootball
            // 
            this.chbCuFootball.AutoSize = true;
            this.chbCuFootball.Location = new System.Drawing.Point(25, 94);
            this.chbCuFootball.Name = "chbCuFootball";
            this.chbCuFootball.Size = new System.Drawing.Size(120, 17);
            this.chbCuFootball.TabIndex = 340;
            this.chbCuFootball.Text = "Call Unit Football on";
            this.chbCuFootball.UseVisualStyleBackColor = true;
            this.chbCuFootball.CheckedChanged += new System.EventHandler(this.chbFreeHalfPoints_CheckedChanged);
            // 
            // chbInetBasketball
            // 
            this.chbInetBasketball.AutoSize = true;
            this.chbInetBasketball.Location = new System.Drawing.Point(25, 60);
            this.chbInetBasketball.Name = "chbInetBasketball";
            this.chbInetBasketball.Size = new System.Drawing.Size(129, 17);
            this.chbInetBasketball.TabIndex = 320;
            this.chbInetBasketball.Text = "Internet Basketball on";
            this.chbInetBasketball.UseVisualStyleBackColor = true;
            this.chbInetBasketball.CheckedChanged += new System.EventHandler(this.chbFreeHalfPoints_CheckedChanged);
            // 
            // chbCuBasketball
            // 
            this.chbCuBasketball.AutoSize = true;
            this.chbCuBasketball.Location = new System.Drawing.Point(25, 28);
            this.chbCuBasketball.Name = "chbCuBasketball";
            this.chbCuBasketball.Size = new System.Drawing.Size(132, 17);
            this.chbCuBasketball.TabIndex = 300;
            this.chbCuBasketball.Text = "Call Unit Basketball on";
            this.chbCuBasketball.UseVisualStyleBackColor = true;
            this.chbCuBasketball.CheckedChanged += new System.EventHandler(this.chbFreeHalfPoints_CheckedChanged);
            // 
            // grpBaseball
            // 
            this.grpBaseball.Controls.Add(this.chbEasternLines);
            this.grpBaseball.Controls.Add(this.rdoFixed);
            this.grpBaseball.Controls.Add(this.rdoAction);
            this.grpBaseball.Controls.Add(this.rdoListed);
            this.grpBaseball.Location = new System.Drawing.Point(374, 117);
            this.grpBaseball.Name = "grpBaseball";
            this.grpBaseball.Size = new System.Drawing.Size(386, 63);
            this.grpBaseball.TabIndex = 240;
            this.grpBaseball.TabStop = false;
            this.grpBaseball.Text = "Baseball";
            // 
            // chbEasternLines
            // 
            this.chbEasternLines.AutoSize = true;
            this.chbEasternLines.Enabled = false;
            this.chbEasternLines.Location = new System.Drawing.Point(25, 43);
            this.chbEasternLines.Name = "chbEasternLines";
            this.chbEasternLines.Size = new System.Drawing.Size(90, 17);
            this.chbEasternLines.TabIndex = 280;
            this.chbEasternLines.Text = "Eastern Lines";
            this.chbEasternLines.UseVisualStyleBackColor = true;
            // 
            // rdoFixed
            // 
            this.rdoFixed.AutoSize = true;
            this.rdoFixed.Location = new System.Drawing.Point(223, 20);
            this.rdoFixed.Name = "rdoFixed";
            this.rdoFixed.Size = new System.Drawing.Size(78, 17);
            this.rdoFixed.TabIndex = 270;
            this.rdoFixed.TabStop = true;
            this.rdoFixed.Text = "Fixed Odds";
            this.rdoFixed.UseVisualStyleBackColor = true;
            // 
            // rdoAction
            // 
            this.rdoAction.AutoSize = true;
            this.rdoAction.Location = new System.Drawing.Point(142, 20);
            this.rdoAction.Name = "rdoAction";
            this.rdoAction.Size = new System.Drawing.Size(55, 17);
            this.rdoAction.TabIndex = 260;
            this.rdoAction.TabStop = true;
            this.rdoAction.Text = "Action";
            this.rdoAction.UseVisualStyleBackColor = true;
            // 
            // rdoListed
            // 
            this.rdoListed.AutoSize = true;
            this.rdoListed.Location = new System.Drawing.Point(25, 20);
            this.rdoListed.Name = "rdoListed";
            this.rdoListed.Size = new System.Drawing.Size(94, 17);
            this.rdoListed.TabIndex = 250;
            this.rdoListed.TabStop = true;
            this.rdoListed.Text = "Listed Pitchers";
            this.rdoListed.UseVisualStyleBackColor = true;
            // 
            // grpStoreAndShadeGroups
            // 
            this.grpStoreAndShadeGroups.Controls.Add(this.panShadeGroups);
            this.grpStoreAndShadeGroups.Controls.Add(this.panStoresList);
            this.grpStoreAndShadeGroups.Controls.Add(this.lblShadeGroups);
            this.grpStoreAndShadeGroups.Controls.Add(this.lblStore);
            this.grpStoreAndShadeGroups.Location = new System.Drawing.Point(12, 9);
            this.grpStoreAndShadeGroups.Name = "grpStoreAndShadeGroups";
            this.grpStoreAndShadeGroups.Size = new System.Drawing.Size(356, 168);
            this.grpStoreAndShadeGroups.TabIndex = 10;
            this.grpStoreAndShadeGroups.TabStop = false;
            this.grpStoreAndShadeGroups.Text = "Store and Shade Groups";
            // 
            // panShadeGroups
            // 
            this.panShadeGroups.Location = new System.Drawing.Point(88, 58);
            this.panShadeGroups.Name = "panShadeGroups";
            this.panShadeGroups.Size = new System.Drawing.Size(211, 97);
            this.panShadeGroups.TabIndex = 30;
            // 
            // panStoresList
            // 
            this.panStoresList.Location = new System.Drawing.Point(54, 19);
            this.panStoresList.Name = "panStoresList";
            this.panStoresList.Size = new System.Drawing.Size(139, 30);
            this.panStoresList.TabIndex = 20;
            // 
            // lblShadeGroups
            // 
            this.lblShadeGroups.AutoSize = true;
            this.lblShadeGroups.Location = new System.Drawing.Point(4, 58);
            this.lblShadeGroups.Name = "lblShadeGroups";
            this.lblShadeGroups.Size = new System.Drawing.Size(78, 13);
            this.lblShadeGroups.TabIndex = 14;
            this.lblShadeGroups.Text = "Shade Groups:";
            // 
            // lblStore
            // 
            this.lblStore.AutoSize = true;
            this.lblStore.Location = new System.Drawing.Point(12, 26);
            this.lblStore.Name = "lblStore";
            this.lblStore.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblStore.Size = new System.Drawing.Size(35, 13);
            this.lblStore.TabIndex = 8;
            this.lblStore.Text = "Store:";
            this.lblStore.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panVariousSettings
            // 
            this.panVariousSettings.Controls.Add(this.btnCustBuyPoints);
            this.panVariousSettings.Controls.Add(this.chbUsePuck);
            this.panVariousSettings.Controls.Add(this.chbLimitIfToRisk);
            this.panVariousSettings.Controls.Add(this.chbEnableRollingIfBets);
            this.panVariousSettings.Controls.Add(this.chbCreditCustomer);
            this.panVariousSettings.Controls.Add(this.chbStaticLines);
            this.panVariousSettings.Location = new System.Drawing.Point(374, 9);
            this.panVariousSettings.Name = "panVariousSettings";
            this.panVariousSettings.Size = new System.Drawing.Size(396, 102);
            this.panVariousSettings.TabIndex = 170;
            // 
            // btnCustBuyPoints
            // 
            this.btnCustBuyPoints.Location = new System.Drawing.Point(190, 67);
            this.btnCustBuyPoints.Name = "btnCustBuyPoints";
            this.btnCustBuyPoints.Size = new System.Drawing.Size(75, 23);
            this.btnCustBuyPoints.TabIndex = 230;
            this.btnCustBuyPoints.Text = "Buy Points...";
            this.btnCustBuyPoints.UseVisualStyleBackColor = true;
            this.btnCustBuyPoints.Click += new System.EventHandler(this.custBuyPointsButton_Click);
            // 
            // chbUsePuck
            // 
            this.chbUsePuck.AutoSize = true;
            this.chbUsePuck.Location = new System.Drawing.Point(25, 71);
            this.chbUsePuck.Name = "chbUsePuck";
            this.chbUsePuck.Size = new System.Drawing.Size(154, 17);
            this.chbUsePuck.TabIndex = 220;
            this.chbUsePuck.Text = "Use Puck Line For Hockey";
            this.chbUsePuck.UseVisualStyleBackColor = true;
            // 
            // chbLimitIfToRisk
            // 
            this.chbLimitIfToRisk.AutoSize = true;
            this.chbLimitIfToRisk.Location = new System.Drawing.Point(190, 41);
            this.chbLimitIfToRisk.Name = "chbLimitIfToRisk";
            this.chbLimitIfToRisk.Size = new System.Drawing.Size(120, 17);
            this.chbLimitIfToRisk.TabIndex = 210;
            this.chbLimitIfToRisk.Text = "Limit If-Bets To Risk";
            this.chbLimitIfToRisk.UseVisualStyleBackColor = true;
            // 
            // chbEnableRollingIfBets
            // 
            this.chbEnableRollingIfBets.AutoSize = true;
            this.chbEnableRollingIfBets.Location = new System.Drawing.Point(25, 41);
            this.chbEnableRollingIfBets.Name = "chbEnableRollingIfBets";
            this.chbEnableRollingIfBets.Size = new System.Drawing.Size(127, 17);
            this.chbEnableRollingIfBets.TabIndex = 200;
            this.chbEnableRollingIfBets.Text = "Enable Rolling If-Bets";
            this.chbEnableRollingIfBets.UseVisualStyleBackColor = true;
            // 
            // chbCreditCustomer
            // 
            this.chbCreditCustomer.AutoSize = true;
            this.chbCreditCustomer.Location = new System.Drawing.Point(190, 12);
            this.chbCreditCustomer.Name = "chbCreditCustomer";
            this.chbCreditCustomer.Size = new System.Drawing.Size(100, 17);
            this.chbCreditCustomer.TabIndex = 190;
            this.chbCreditCustomer.Text = "Credit Customer";
            this.chbCreditCustomer.UseVisualStyleBackColor = true;
            this.chbCreditCustomer.CheckedChanged += new System.EventHandler(this.chbCreditCustomer_CheckedChanged);
            // 
            // chbStaticLines
            // 
            this.chbStaticLines.AutoSize = true;
            this.chbStaticLines.Location = new System.Drawing.Point(25, 12);
            this.chbStaticLines.Name = "chbStaticLines";
            this.chbStaticLines.Size = new System.Drawing.Size(135, 17);
            this.chbStaticLines.TabIndex = 180;
            this.chbStaticLines.Text = "Static Lines During Call";
            this.chbStaticLines.UseVisualStyleBackColor = true;
            // 
            // grpWagerLimits
            // 
            this.grpWagerLimits.Controls.Add(this.txtTempQuickLimit);
            this.grpWagerLimits.Controls.Add(this.lblTempQuickLimit);
            this.grpWagerLimits.Controls.Add(this.dtmTempQuickLimit);
            this.grpWagerLimits.Controls.Add(this.btnShowAuditQuickLimit);
            this.grpWagerLimits.Controls.Add(this.txtThroughQuickLimit);
            this.grpWagerLimits.Controls.Add(this.lblThroughTempQuickLimit);
            this.grpWagerLimits.Controls.Add(this.btnShowAuditCUMinBet);
            this.grpWagerLimits.Controls.Add(this.btnShowAuditInetMinBet);
            this.grpWagerLimits.Controls.Add(this.txtInetMinBet);
            this.grpWagerLimits.Controls.Add(this.txtLossLimit);
            this.grpWagerLimits.Controls.Add(this.txtCuMinBet);
            this.grpWagerLimits.Controls.Add(this.lblInetMinBet);
            this.grpWagerLimits.Controls.Add(this.txtQuickLimit);
            this.grpWagerLimits.Controls.Add(this.lblLossLimit);
            this.grpWagerLimits.Controls.Add(this.lblQuickLimit);
            this.grpWagerLimits.Controls.Add(this.lblCuMinBet);
            this.grpWagerLimits.Location = new System.Drawing.Point(12, 338);
            this.grpWagerLimits.Name = "grpWagerLimits";
            this.grpWagerLimits.Size = new System.Drawing.Size(356, 105);
            this.grpWagerLimits.TabIndex = 120;
            this.grpWagerLimits.TabStop = false;
            this.grpWagerLimits.Text = "Wager Limit (Client Currency, 0 = Store Limit Applies)";
            // 
            // txtTempQuickLimit
            // 
            this.txtTempQuickLimit.AllowCents = false;
            this.txtTempQuickLimit.AllowNegatives = false;
            this.txtTempQuickLimit.DividedBy = 100;
            this.txtTempQuickLimit.DivideFlag = true;
            this.txtTempQuickLimit.Location = new System.Drawing.Point(85, 43);
            this.txtTempQuickLimit.MaxLength = 7;
            this.txtTempQuickLimit.Name = "txtTempQuickLimit";
            this.txtTempQuickLimit.ShowIfZero = true;
            this.txtTempQuickLimit.Size = new System.Drawing.Size(79, 20);
            this.txtTempQuickLimit.TabIndex = 208;
            this.txtTempQuickLimit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTempQuickLimit.TextChanged += new System.EventHandler(this.txtTempQuickLimit_TextChanged);
            // 
            // lblTempQuickLimit
            // 
            this.lblTempQuickLimit.AutoSize = true;
            this.lblTempQuickLimit.Location = new System.Drawing.Point(1, 49);
            this.lblTempQuickLimit.Name = "lblTempQuickLimit";
            this.lblTempQuickLimit.Size = new System.Drawing.Size(86, 13);
            this.lblTempQuickLimit.TabIndex = 207;
            this.lblTempQuickLimit.Text = "Tmp Quick Limit:";
            // 
            // dtmTempQuickLimit
            // 
            this.dtmTempQuickLimit.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtmTempQuickLimit.Location = new System.Drawing.Point(334, 43);
            this.dtmTempQuickLimit.Name = "dtmTempQuickLimit";
            this.dtmTempQuickLimit.Size = new System.Drawing.Size(16, 20);
            this.dtmTempQuickLimit.TabIndex = 206;
            this.dtmTempQuickLimit.ValueChanged += new System.EventHandler(this.dtmTempQuickLimit_ValueChanged);
            // 
            // btnShowAuditQuickLimit
            // 
            this.btnShowAuditQuickLimit.Location = new System.Drawing.Point(169, 15);
            this.btnShowAuditQuickLimit.Name = "btnShowAuditQuickLimit";
            this.btnShowAuditQuickLimit.Size = new System.Drawing.Size(14, 23);
            this.btnShowAuditQuickLimit.TabIndex = 204;
            this.btnShowAuditQuickLimit.Text = "?";
            this.btnShowAuditQuickLimit.UseVisualStyleBackColor = true;
            this.btnShowAuditQuickLimit.Click += new System.EventHandler(this.btnShowAuditQuickLimit_Click);
            // 
            // txtThroughQuickLimit
            // 
            this.txtThroughQuickLimit.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtThroughQuickLimit.Location = new System.Drawing.Point(251, 43);
            this.txtThroughQuickLimit.Name = "txtThroughQuickLimit";
            this.txtThroughQuickLimit.Size = new System.Drawing.Size(77, 20);
            this.txtThroughQuickLimit.TabIndex = 205;
            this.txtThroughQuickLimit.TextChanged += new System.EventHandler(this.txtThroughQuickLimit_TextChanged);
            // 
            // lblThroughTempQuickLimit
            // 
            this.lblThroughTempQuickLimit.AutoSize = true;
            this.lblThroughTempQuickLimit.Location = new System.Drawing.Point(181, 46);
            this.lblThroughTempQuickLimit.Name = "lblThroughTempQuickLimit";
            this.lblThroughTempQuickLimit.Size = new System.Drawing.Size(47, 13);
            this.lblThroughTempQuickLimit.TabIndex = 204;
            this.lblThroughTempQuickLimit.Text = "->   until:";
            // 
            // btnShowAuditCUMinBet
            // 
            this.btnShowAuditCUMinBet.Location = new System.Drawing.Point(169, 67);
            this.btnShowAuditCUMinBet.Name = "btnShowAuditCUMinBet";
            this.btnShowAuditCUMinBet.Size = new System.Drawing.Size(14, 23);
            this.btnShowAuditCUMinBet.TabIndex = 205;
            this.btnShowAuditCUMinBet.Text = "?";
            this.btnShowAuditCUMinBet.UseVisualStyleBackColor = true;
            this.btnShowAuditCUMinBet.Click += new System.EventHandler(this.btnShowAuditCUMinBet_Click);
            // 
            // btnShowAuditInetMinBet
            // 
            this.btnShowAuditInetMinBet.Location = new System.Drawing.Point(336, 67);
            this.btnShowAuditInetMinBet.Name = "btnShowAuditInetMinBet";
            this.btnShowAuditInetMinBet.Size = new System.Drawing.Size(14, 23);
            this.btnShowAuditInetMinBet.TabIndex = 204;
            this.btnShowAuditInetMinBet.Text = "?";
            this.btnShowAuditInetMinBet.UseVisualStyleBackColor = true;
            this.btnShowAuditInetMinBet.Click += new System.EventHandler(this.btnShowAuditInetMinBet_Click);
            // 
            // txtInetMinBet
            // 
            this.txtInetMinBet.AllowCents = false;
            this.txtInetMinBet.AllowNegatives = false;
            this.txtInetMinBet.DividedBy = 100;
            this.txtInetMinBet.DivideFlag = true;
            this.txtInetMinBet.Location = new System.Drawing.Point(251, 69);
            this.txtInetMinBet.MaxLength = 4;
            this.txtInetMinBet.Name = "txtInetMinBet";
            this.txtInetMinBet.ShowIfZero = true;
            this.txtInetMinBet.Size = new System.Drawing.Size(79, 20);
            this.txtInetMinBet.TabIndex = 160;
            this.txtInetMinBet.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtInetMinBet.Leave += new System.EventHandler(this.numberToText_Leave);
            // 
            // txtLossLimit
            // 
            this.txtLossLimit.AllowCents = false;
            this.txtLossLimit.AllowNegatives = false;
            this.txtLossLimit.DividedBy = 100;
            this.txtLossLimit.DivideFlag = true;
            this.txtLossLimit.Enabled = false;
            this.txtLossLimit.Location = new System.Drawing.Point(251, 17);
            this.txtLossLimit.MaxLength = 5;
            this.txtLossLimit.Name = "txtLossLimit";
            this.txtLossLimit.ShowIfZero = true;
            this.txtLossLimit.Size = new System.Drawing.Size(99, 20);
            this.txtLossLimit.TabIndex = 140;
            this.txtLossLimit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtLossLimit.Leave += new System.EventHandler(this.numberToText_Leave);
            // 
            // txtCuMinBet
            // 
            this.txtCuMinBet.AllowCents = false;
            this.txtCuMinBet.AllowNegatives = false;
            this.txtCuMinBet.DividedBy = 100;
            this.txtCuMinBet.DivideFlag = true;
            this.txtCuMinBet.Location = new System.Drawing.Point(85, 69);
            this.txtCuMinBet.MaxLength = 4;
            this.txtCuMinBet.Name = "txtCuMinBet";
            this.txtCuMinBet.ShowIfZero = true;
            this.txtCuMinBet.Size = new System.Drawing.Size(79, 20);
            this.txtCuMinBet.TabIndex = 150;
            this.txtCuMinBet.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCuMinBet.Leave += new System.EventHandler(this.numberToText_Leave);
            // 
            // lblInetMinBet
            // 
            this.lblInetMinBet.AutoSize = true;
            this.lblInetMinBet.Location = new System.Drawing.Point(184, 75);
            this.lblInetMinBet.Name = "lblInetMinBet";
            this.lblInetMinBet.Size = new System.Drawing.Size(67, 13);
            this.lblInetMinBet.TabIndex = 14;
            this.lblInetMinBet.Text = "Inet Min Bet:";
            // 
            // txtQuickLimit
            // 
            this.txtQuickLimit.AllowCents = false;
            this.txtQuickLimit.AllowNegatives = false;
            this.txtQuickLimit.DividedBy = 100;
            this.txtQuickLimit.DivideFlag = true;
            this.txtQuickLimit.Location = new System.Drawing.Point(84, 17);
            this.txtQuickLimit.MaxLength = 7;
            this.txtQuickLimit.Name = "txtQuickLimit";
            this.txtQuickLimit.ShowIfZero = true;
            this.txtQuickLimit.Size = new System.Drawing.Size(79, 20);
            this.txtQuickLimit.TabIndex = 130;
            this.txtQuickLimit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtQuickLimit.Leave += new System.EventHandler(this.numberToText_Leave);
            // 
            // lblLossLimit
            // 
            this.lblLossLimit.AutoSize = true;
            this.lblLossLimit.Location = new System.Drawing.Point(189, 24);
            this.lblLossLimit.Name = "lblLossLimit";
            this.lblLossLimit.Size = new System.Drawing.Size(56, 13);
            this.lblLossLimit.TabIndex = 12;
            this.lblLossLimit.Text = "Loss Limit:";
            // 
            // lblQuickLimit
            // 
            this.lblQuickLimit.AutoSize = true;
            this.lblQuickLimit.Location = new System.Drawing.Point(6, 24);
            this.lblQuickLimit.Name = "lblQuickLimit";
            this.lblQuickLimit.Size = new System.Drawing.Size(62, 13);
            this.lblQuickLimit.TabIndex = 8;
            this.lblQuickLimit.Text = "Quick Limit:";
            // 
            // lblCuMinBet
            // 
            this.lblCuMinBet.AutoSize = true;
            this.lblCuMinBet.Location = new System.Drawing.Point(6, 75);
            this.lblCuMinBet.Name = "lblCuMinBet";
            this.lblCuMinBet.Size = new System.Drawing.Size(64, 13);
            this.lblCuMinBet.TabIndex = 10;
            this.lblCuMinBet.Text = "CU Min Bet:";
            // 
            // grpCreditLimit
            // 
            this.grpCreditLimit.Controls.Add(this.btnShowAuditCreditIncrease);
            this.grpCreditLimit.Controls.Add(this.btnShowAuditCreditLimit);
            this.grpCreditLimit.Controls.Add(this.dtmTempCreditIncrease);
            this.grpCreditLimit.Controls.Add(this.txtThroughCredIncrease);
            this.grpCreditLimit.Controls.Add(this.lblThroughCredIncrease);
            this.grpCreditLimit.Controls.Add(this.lblCreditIncrease);
            this.grpCreditLimit.Controls.Add(this.txtCreditIncrease);
            this.grpCreditLimit.Controls.Add(this.txtCreditLimit);
            this.grpCreditLimit.Controls.Add(this.lblNoCredit);
            this.grpCreditLimit.Controls.Add(this.lblCreditLimit);
            this.grpCreditLimit.Location = new System.Drawing.Point(12, 252);
            this.grpCreditLimit.Name = "grpCreditLimit";
            this.grpCreditLimit.Size = new System.Drawing.Size(356, 80);
            this.grpCreditLimit.TabIndex = 70;
            this.grpCreditLimit.TabStop = false;
            this.grpCreditLimit.Text = "Credit Limit - Client Currency";
            // 
            // btnShowAuditCreditIncrease
            // 
            this.btnShowAuditCreditIncrease.Location = new System.Drawing.Point(164, 42);
            this.btnShowAuditCreditIncrease.Name = "btnShowAuditCreditIncrease";
            this.btnShowAuditCreditIncrease.Size = new System.Drawing.Size(14, 23);
            this.btnShowAuditCreditIncrease.TabIndex = 203;
            this.btnShowAuditCreditIncrease.Text = "?";
            this.btnShowAuditCreditIncrease.UseVisualStyleBackColor = true;
            this.btnShowAuditCreditIncrease.Click += new System.EventHandler(this.btnShowAuditCreditIncrease_Click);
            // 
            // btnShowAuditCreditLimit
            // 
            this.btnShowAuditCreditLimit.Location = new System.Drawing.Point(165, 14);
            this.btnShowAuditCreditLimit.Name = "btnShowAuditCreditLimit";
            this.btnShowAuditCreditLimit.Size = new System.Drawing.Size(14, 23);
            this.btnShowAuditCreditLimit.TabIndex = 202;
            this.btnShowAuditCreditLimit.Text = "?";
            this.btnShowAuditCreditLimit.UseVisualStyleBackColor = true;
            this.btnShowAuditCreditLimit.Click += new System.EventHandler(this.btnShowAuditCreditLimit_Click);
            // 
            // dtmTempCreditIncrease
            // 
            this.dtmTempCreditIncrease.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtmTempCreditIncrease.Location = new System.Drawing.Point(334, 44);
            this.dtmTempCreditIncrease.Name = "dtmTempCreditIncrease";
            this.dtmTempCreditIncrease.Size = new System.Drawing.Size(16, 20);
            this.dtmTempCreditIncrease.TabIndex = 110;
            this.dtmTempCreditIncrease.ValueChanged += new System.EventHandler(this.tempCreditIncDateTimePicker_ValueChanged);
            // 
            // txtThroughCredIncrease
            // 
            this.txtThroughCredIncrease.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtThroughCredIncrease.Location = new System.Drawing.Point(251, 44);
            this.txtThroughCredIncrease.Name = "txtThroughCredIncrease";
            this.txtThroughCredIncrease.Size = new System.Drawing.Size(77, 20);
            this.txtThroughCredIncrease.TabIndex = 100;
            this.txtThroughCredIncrease.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtThroughCredIncrease_KeyUp);
            // 
            // lblThroughCredIncrease
            // 
            this.lblThroughCredIncrease.AutoSize = true;
            this.lblThroughCredIncrease.Location = new System.Drawing.Point(184, 47);
            this.lblThroughCredIncrease.Name = "lblThroughCredIncrease";
            this.lblThroughCredIncrease.Size = new System.Drawing.Size(47, 13);
            this.lblThroughCredIncrease.TabIndex = 5;
            this.lblThroughCredIncrease.Text = "->   until:";
            // 
            // lblCreditIncrease
            // 
            this.lblCreditIncrease.AutoSize = true;
            this.lblCreditIncrease.Location = new System.Drawing.Point(1, 47);
            this.lblCreditIncrease.Name = "lblCreditIncrease";
            this.lblCreditIncrease.Size = new System.Drawing.Size(81, 13);
            this.lblCreditIncrease.TabIndex = 3;
            this.lblCreditIncrease.Text = "Credit Increase:";
            // 
            // txtCreditIncrease
            // 
            this.txtCreditIncrease.AllowCents = false;
            this.txtCreditIncrease.AllowNegatives = false;
            this.txtCreditIncrease.DividedBy = 100;
            this.txtCreditIncrease.DivideFlag = true;
            this.txtCreditIncrease.Location = new System.Drawing.Point(85, 44);
            this.txtCreditIncrease.Name = "txtCreditIncrease";
            this.txtCreditIncrease.ShowIfZero = true;
            this.txtCreditIncrease.Size = new System.Drawing.Size(75, 20);
            this.txtCreditIncrease.TabIndex = 90;
            this.txtCreditIncrease.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCreditIncrease.Leave += new System.EventHandler(this.numberToText_Leave);
            // 
            // txtCreditLimit
            // 
            this.txtCreditLimit.AllowCents = false;
            this.txtCreditLimit.AllowNegatives = false;
            this.txtCreditLimit.DividedBy = 100;
            this.txtCreditLimit.DivideFlag = true;
            this.txtCreditLimit.ForeColor = System.Drawing.SystemColors.WindowText;
            this.txtCreditLimit.Location = new System.Drawing.Point(84, 16);
            this.txtCreditLimit.Name = "txtCreditLimit";
            this.txtCreditLimit.ShowIfZero = true;
            this.txtCreditLimit.Size = new System.Drawing.Size(75, 20);
            this.txtCreditLimit.TabIndex = 80;
            this.txtCreditLimit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCreditLimit.Leave += new System.EventHandler(this.numberToText_Leave);
            // 
            // lblNoCredit
            // 
            this.lblNoCredit.AutoSize = true;
            this.lblNoCredit.Location = new System.Drawing.Point(189, 19);
            this.lblNoCredit.Name = "lblNoCredit";
            this.lblNoCredit.Size = new System.Drawing.Size(72, 13);
            this.lblNoCredit.TabIndex = 2;
            this.lblNoCredit.Text = "(0 = no credit)";
            // 
            // lblCreditLimit
            // 
            this.lblCreditLimit.AutoSize = true;
            this.lblCreditLimit.Location = new System.Drawing.Point(17, 19);
            this.lblCreditLimit.Name = "lblCreditLimit";
            this.lblCreditLimit.Size = new System.Drawing.Size(61, 13);
            this.lblCreditLimit.TabIndex = 0;
            this.lblCreditLimit.Text = "Credit Limit:";
            // 
            // panParlayAndTeasers
            // 
            this.panParlayAndTeasers.Controls.Add(this.btnTeasers);
            this.panParlayAndTeasers.Controls.Add(this.lblParlay);
            this.panParlayAndTeasers.Controls.Add(this.panParlayTypes);
            this.panParlayAndTeasers.Location = new System.Drawing.Point(12, 183);
            this.panParlayAndTeasers.Name = "panParlayAndTeasers";
            this.panParlayAndTeasers.Size = new System.Drawing.Size(356, 63);
            this.panParlayAndTeasers.TabIndex = 40;
            // 
            // btnTeasers
            // 
            this.btnTeasers.Location = new System.Drawing.Point(223, 15);
            this.btnTeasers.Name = "btnTeasers";
            this.btnTeasers.Size = new System.Drawing.Size(75, 23);
            this.btnTeasers.TabIndex = 60;
            this.btnTeasers.Text = "Teasers";
            this.btnTeasers.UseVisualStyleBackColor = true;
            this.btnTeasers.Click += new System.EventHandler(this.TeasersButton_Click);
            // 
            // lblParlay
            // 
            this.lblParlay.AutoSize = true;
            this.lblParlay.Location = new System.Drawing.Point(35, 25);
            this.lblParlay.Name = "lblParlay";
            this.lblParlay.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblParlay.Size = new System.Drawing.Size(39, 13);
            this.lblParlay.TabIndex = 15;
            this.lblParlay.Text = "Parlay:";
            this.lblParlay.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // panParlayTypes
            // 
            this.panParlayTypes.Location = new System.Drawing.Point(80, 17);
            this.panParlayTypes.Name = "panParlayTypes";
            this.panParlayTypes.Size = new System.Drawing.Size(139, 22);
            this.panParlayTypes.TabIndex = 50;
            // 
            // tabpVigDiscount
            // 
            this.tabpVigDiscount.Controls.Add(this.panVigDiscount);
            this.tabpVigDiscount.Location = new System.Drawing.Point(4, 22);
            this.tabpVigDiscount.Name = "tabpVigDiscount";
            this.tabpVigDiscount.Size = new System.Drawing.Size(793, 474);
            this.tabpVigDiscount.TabIndex = 2;
            this.tabpVigDiscount.Text = "Vig Discount";
            this.tabpVigDiscount.UseVisualStyleBackColor = true;
            // 
            // panVigDiscount
            // 
            this.panVigDiscount.Controls.Add(this.lblVigDiscountNote);
            this.panVigDiscount.Controls.Add(this.dgvwVigDiscount);
            this.panVigDiscount.Location = new System.Drawing.Point(15, 4);
            this.panVigDiscount.Name = "panVigDiscount";
            this.panVigDiscount.Size = new System.Drawing.Size(733, 472);
            this.panVigDiscount.TabIndex = 0;
            // 
            // lblVigDiscountNote
            // 
            this.lblVigDiscountNote.AutoSize = true;
            this.lblVigDiscountNote.Location = new System.Drawing.Point(465, 451);
            this.lblVigDiscountNote.Name = "lblVigDiscountNote";
            this.lblVigDiscountNote.Size = new System.Drawing.Size(259, 13);
            this.lblVigDiscountNote.TabIndex = 2;
            this.lblVigDiscountNote.Text = "NOTE:  Enter \"0\" in the % field to cancel vig discount";
            // 
            // dgvwVigDiscount
            // 
            this.dgvwVigDiscount.AllowUserToAddRows = false;
            this.dgvwVigDiscount.AllowUserToDeleteRows = false;
            this.dgvwVigDiscount.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvwVigDiscount.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvwVigDiscount.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvwVigDiscount.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvwVigDiscount.Location = new System.Drawing.Point(4, 4);
            this.dgvwVigDiscount.Name = "dgvwVigDiscount";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvwVigDiscount.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvwVigDiscount.RowHeadersVisible = false;
            this.dgvwVigDiscount.Size = new System.Drawing.Size(720, 444);
            this.dgvwVigDiscount.TabIndex = 0;
            this.dgvwVigDiscount.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvwVigDiscount_CellEndEdit);
            this.dgvwVigDiscount.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.vigDiscountdataGridView_CellValidating);
            this.dgvwVigDiscount.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvwVigDiscount_DataError);
            this.dgvwVigDiscount.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvwVigDiscount_EditingControlShowing);
            this.dgvwVigDiscount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvwVigDiscount_KeyDown);
            // 
            // tabpPerformance
            // 
            this.tabpPerformance.Controls.Add(this.panPerformance);
            this.tabpPerformance.Location = new System.Drawing.Point(4, 22);
            this.tabpPerformance.Name = "tabpPerformance";
            this.tabpPerformance.Size = new System.Drawing.Size(793, 474);
            this.tabpPerformance.TabIndex = 3;
            this.tabpPerformance.Text = "Performance";
            this.tabpPerformance.UseVisualStyleBackColor = true;
            // 
            // panPerformance
            // 
            this.panPerformance.Controls.Add(this.grpWagerTypes);
            this.panPerformance.Controls.Add(this.panGoToDFsOrWagers);
            this.panPerformance.Controls.Add(this.panPerformanceData);
            this.panPerformance.Controls.Add(this.grpViewDailyFigs);
            this.panPerformance.Location = new System.Drawing.Point(28, 4);
            this.panPerformance.Name = "panPerformance";
            this.panPerformance.Size = new System.Drawing.Size(707, 467);
            this.panPerformance.TabIndex = 0;
            // 
            // grpWagerTypes
            // 
            this.grpWagerTypes.Controls.Add(this.cmbWagerTypes);
            this.grpWagerTypes.Location = new System.Drawing.Point(4, 8);
            this.grpWagerTypes.Name = "grpWagerTypes";
            this.grpWagerTypes.Size = new System.Drawing.Size(140, 59);
            this.grpWagerTypes.TabIndex = 8;
            this.grpWagerTypes.TabStop = false;
            this.grpWagerTypes.Text = "Wager Types";
            // 
            // cmbWagerTypes
            // 
            this.cmbWagerTypes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbWagerTypes.FormattingEnabled = true;
            this.cmbWagerTypes.Location = new System.Drawing.Point(6, 19);
            this.cmbWagerTypes.Name = "cmbWagerTypes";
            this.cmbWagerTypes.Size = new System.Drawing.Size(128, 21);
            this.cmbWagerTypes.TabIndex = 0;
            this.cmbWagerTypes.SelectedIndexChanged += new System.EventHandler(this.cmbWagerTypes_SelectedIndexChanged);
            // 
            // panGoToDFsOrWagers
            // 
            this.panGoToDFsOrWagers.Controls.Add(this.btnGoToWagers);
            this.panGoToDFsOrWagers.Controls.Add(this.btnGoToDFs);
            this.panGoToDFsOrWagers.Location = new System.Drawing.Point(4, 307);
            this.panGoToDFsOrWagers.Name = "panGoToDFsOrWagers";
            this.panGoToDFsOrWagers.Size = new System.Drawing.Size(140, 108);
            this.panGoToDFsOrWagers.TabIndex = 2;
            // 
            // btnGoToWagers
            // 
            this.btnGoToWagers.Location = new System.Drawing.Point(27, 56);
            this.btnGoToWagers.Name = "btnGoToWagers";
            this.btnGoToWagers.Size = new System.Drawing.Size(86, 23);
            this.btnGoToWagers.TabIndex = 1;
            this.btnGoToWagers.Text = "Wagers...";
            this.btnGoToWagers.UseVisualStyleBackColor = true;
            this.btnGoToWagers.Click += new System.EventHandler(this.btnGoToWagers_Click);
            // 
            // btnGoToDFs
            // 
            this.btnGoToDFs.Location = new System.Drawing.Point(26, 19);
            this.btnGoToDFs.Name = "btnGoToDFs";
            this.btnGoToDFs.Size = new System.Drawing.Size(86, 23);
            this.btnGoToDFs.TabIndex = 0;
            this.btnGoToDFs.Text = "Daily Figures...";
            this.btnGoToDFs.UseVisualStyleBackColor = true;
            this.btnGoToDFs.Click += new System.EventHandler(this.btnGoToDFs_Click);
            // 
            // panPerformanceData
            // 
            this.panPerformanceData.Controls.Add(this.txtSelectedPerformance);
            this.panPerformanceData.Controls.Add(this.label1);
            this.panPerformanceData.Controls.Add(this.txtPerfViewModeLegend);
            this.panPerformanceData.Controls.Add(this.dgvwCustomerPerformance);
            this.panPerformanceData.Location = new System.Drawing.Point(199, 4);
            this.panPerformanceData.Name = "panPerformanceData";
            this.panPerformanceData.Size = new System.Drawing.Size(494, 460);
            this.panPerformanceData.TabIndex = 1;
            // 
            // txtSelectedPerformance
            // 
            this.txtSelectedPerformance.Location = new System.Drawing.Point(85, 429);
            this.txtSelectedPerformance.Name = "txtSelectedPerformance";
            this.txtSelectedPerformance.ReadOnly = true;
            this.txtSelectedPerformance.Size = new System.Drawing.Size(104, 20);
            this.txtSelectedPerformance.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 432);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Total Selected:";
            // 
            // txtPerfViewModeLegend
            // 
            this.txtPerfViewModeLegend.Location = new System.Drawing.Point(4, 403);
            this.txtPerfViewModeLegend.Name = "txtPerfViewModeLegend";
            this.txtPerfViewModeLegend.ReadOnly = true;
            this.txtPerfViewModeLegend.Size = new System.Drawing.Size(487, 20);
            this.txtPerfViewModeLegend.TabIndex = 0;
            this.txtPerfViewModeLegend.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // dgvwCustomerPerformance
            // 
            this.dgvwCustomerPerformance.AllowUserToAddRows = false;
            this.dgvwCustomerPerformance.AllowUserToDeleteRows = false;
            this.dgvwCustomerPerformance.AllowUserToOrderColumns = true;
            this.dgvwCustomerPerformance.AllowUserToResizeRows = false;
            this.dgvwCustomerPerformance.BackgroundColor = System.Drawing.Color.White;
            this.dgvwCustomerPerformance.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvwCustomerPerformance.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvwCustomerPerformance.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvwCustomerPerformance.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgvwCustomerPerformance.Location = new System.Drawing.Point(4, 4);
            this.dgvwCustomerPerformance.Name = "dgvwCustomerPerformance";
            this.dgvwCustomerPerformance.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvwCustomerPerformance.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvwCustomerPerformance.RowHeadersVisible = false;
            this.dgvwCustomerPerformance.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvwCustomerPerformance.ShowEditingIcon = false;
            this.dgvwCustomerPerformance.Size = new System.Drawing.Size(487, 390);
            this.dgvwCustomerPerformance.TabIndex = 0;
            this.dgvwCustomerPerformance.SelectionChanged += new System.EventHandler(this.dgvwCustomerPerformance_SelectionChanged);
            // 
            // grpViewDailyFigs
            // 
            this.grpViewDailyFigs.Controls.Add(this.radYearlyPerformance);
            this.grpViewDailyFigs.Controls.Add(this.radMonthlyPerformance);
            this.grpViewDailyFigs.Controls.Add(this.radWeeklyPerformance);
            this.grpViewDailyFigs.Controls.Add(this.radDailyPerformance);
            this.grpViewDailyFigs.Location = new System.Drawing.Point(4, 80);
            this.grpViewDailyFigs.Name = "grpViewDailyFigs";
            this.grpViewDailyFigs.Size = new System.Drawing.Size(140, 186);
            this.grpViewDailyFigs.TabIndex = 0;
            this.grpViewDailyFigs.TabStop = false;
            this.grpViewDailyFigs.Text = "View";
            // 
            // radYearlyPerformance
            // 
            this.radYearlyPerformance.Appearance = System.Windows.Forms.Appearance.Button;
            this.radYearlyPerformance.AutoSize = true;
            this.radYearlyPerformance.Location = new System.Drawing.Point(35, 141);
            this.radYearlyPerformance.Name = "radYearlyPerformance";
            this.radYearlyPerformance.Size = new System.Drawing.Size(52, 23);
            this.radYearlyPerformance.TabIndex = 7;
            this.radYearlyPerformance.TabStop = true;
            this.radYearlyPerformance.Text = " Yearly ";
            this.radYearlyPerformance.UseVisualStyleBackColor = true;
            this.radYearlyPerformance.CheckedChanged += new System.EventHandler(this.radYearlyPerformance_CheckedChanged);
            // 
            // radMonthlyPerformance
            // 
            this.radMonthlyPerformance.Appearance = System.Windows.Forms.Appearance.Button;
            this.radMonthlyPerformance.AutoSize = true;
            this.radMonthlyPerformance.Location = new System.Drawing.Point(33, 105);
            this.radMonthlyPerformance.Name = "radMonthlyPerformance";
            this.radMonthlyPerformance.Size = new System.Drawing.Size(54, 23);
            this.radMonthlyPerformance.TabIndex = 6;
            this.radMonthlyPerformance.TabStop = true;
            this.radMonthlyPerformance.Text = "Monthly";
            this.radMonthlyPerformance.UseVisualStyleBackColor = true;
            this.radMonthlyPerformance.CheckedChanged += new System.EventHandler(this.radMonthlyPerformance_CheckedChanged);
            // 
            // radWeeklyPerformance
            // 
            this.radWeeklyPerformance.Appearance = System.Windows.Forms.Appearance.Button;
            this.radWeeklyPerformance.AutoSize = true;
            this.radWeeklyPerformance.Location = new System.Drawing.Point(34, 69);
            this.radWeeklyPerformance.Name = "radWeeklyPerformance";
            this.radWeeklyPerformance.Size = new System.Drawing.Size(53, 23);
            this.radWeeklyPerformance.TabIndex = 5;
            this.radWeeklyPerformance.TabStop = true;
            this.radWeeklyPerformance.Text = "Weekly";
            this.radWeeklyPerformance.UseVisualStyleBackColor = true;
            this.radWeeklyPerformance.CheckedChanged += new System.EventHandler(this.radWeeklyPerformance_CheckedChanged);
            // 
            // radDailyPerformance
            // 
            this.radDailyPerformance.Appearance = System.Windows.Forms.Appearance.Button;
            this.radDailyPerformance.AutoSize = true;
            this.radDailyPerformance.Location = new System.Drawing.Point(35, 33);
            this.radDailyPerformance.Name = "radDailyPerformance";
            this.radDailyPerformance.Size = new System.Drawing.Size(52, 23);
            this.radDailyPerformance.TabIndex = 4;
            this.radDailyPerformance.TabStop = true;
            this.radDailyPerformance.Text = "  Daily  ";
            this.radDailyPerformance.UseVisualStyleBackColor = true;
            this.radDailyPerformance.CheckedChanged += new System.EventHandler(this.radDailyPerformance_CheckedChanged);
            // 
            // tabpTransactions
            // 
            this.tabpTransactions.Controls.Add(this.panTransactions);
            this.tabpTransactions.Location = new System.Drawing.Point(4, 22);
            this.tabpTransactions.Name = "tabpTransactions";
            this.tabpTransactions.Size = new System.Drawing.Size(793, 474);
            this.tabpTransactions.TabIndex = 4;
            this.tabpTransactions.Text = "Transactions";
            this.tabpTransactions.UseVisualStyleBackColor = true;
            // 
            // panTransactions
            // 
            this.panTransactions.Controls.Add(this.panTranTotalSelected);
            this.panTransactions.Controls.Add(this.grpTranTypes);
            this.panTransactions.Controls.Add(this.btnTranGotoWagers);
            this.panTransactions.Controls.Add(this.btnLastVerified);
            this.panTransactions.Controls.Add(this.btnDeleteTran);
            this.panTransactions.Controls.Add(this.btnTranBrowse);
            this.panTransactions.Controls.Add(this.btnTranNew);
            this.panTransactions.Controls.Add(this.panHeader);
            this.panTransactions.Controls.Add(this.panTransactionsDetails);
            this.panTransactions.Location = new System.Drawing.Point(18, 2);
            this.panTransactions.Name = "panTransactions";
            this.panTransactions.Size = new System.Drawing.Size(726, 468);
            this.panTransactions.TabIndex = 0;
            // 
            // panTranTotalSelected
            // 
            this.panTranTotalSelected.Controls.Add(this.txtTotalsTransSelected);
            this.panTranTotalSelected.Controls.Add(this.lblTransTotalSelected);
            this.panTranTotalSelected.Location = new System.Drawing.Point(441, 402);
            this.panTranTotalSelected.Name = "panTranTotalSelected";
            this.panTranTotalSelected.Size = new System.Drawing.Size(162, 43);
            this.panTranTotalSelected.TabIndex = 1;
            // 
            // txtTotalsTransSelected
            // 
            this.txtTotalsTransSelected.Location = new System.Drawing.Point(77, 21);
            this.txtTotalsTransSelected.Name = "txtTotalsTransSelected";
            this.txtTotalsTransSelected.ReadOnly = true;
            this.txtTotalsTransSelected.Size = new System.Drawing.Size(75, 20);
            this.txtTotalsTransSelected.TabIndex = 22;
            // 
            // lblTransTotalSelected
            // 
            this.lblTransTotalSelected.AutoSize = true;
            this.lblTransTotalSelected.Location = new System.Drawing.Point(0, 24);
            this.lblTransTotalSelected.Name = "lblTransTotalSelected";
            this.lblTransTotalSelected.Size = new System.Drawing.Size(79, 13);
            this.lblTransTotalSelected.TabIndex = 21;
            this.lblTransTotalSelected.Text = "Total Selected:";
            // 
            // grpTranTypes
            // 
            this.grpTranTypes.Controls.Add(this.cmbTranTypes);
            this.grpTranTypes.Location = new System.Drawing.Point(3, 405);
            this.grpTranTypes.Name = "grpTranTypes";
            this.grpTranTypes.Size = new System.Drawing.Size(176, 49);
            this.grpTranTypes.TabIndex = 20;
            this.grpTranTypes.TabStop = false;
            this.grpTranTypes.Text = "Filter by Tran Type";
            // 
            // cmbTranTypes
            // 
            this.cmbTranTypes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTranTypes.FormattingEnabled = true;
            this.cmbTranTypes.Location = new System.Drawing.Point(6, 19);
            this.cmbTranTypes.Name = "cmbTranTypes";
            this.cmbTranTypes.Size = new System.Drawing.Size(162, 21);
            this.cmbTranTypes.TabIndex = 0;
            this.cmbTranTypes.SelectedIndexChanged += new System.EventHandler(this.cmbTranTypes_SelectedIndexChanged);
            // 
            // btnTranGotoWagers
            // 
            this.btnTranGotoWagers.Location = new System.Drawing.Point(605, 422);
            this.btnTranGotoWagers.Name = "btnTranGotoWagers";
            this.btnTranGotoWagers.Size = new System.Drawing.Size(103, 23);
            this.btnTranGotoWagers.TabIndex = 19;
            this.btnTranGotoWagers.Text = "Go To Wagers...";
            this.btnTranGotoWagers.UseVisualStyleBackColor = true;
            this.btnTranGotoWagers.Click += new System.EventHandler(this.btnTranGotoWagers_Click);
            // 
            // btnLastVerified
            // 
            this.btnLastVerified.Location = new System.Drawing.Point(524, 422);
            this.btnLastVerified.Name = "btnLastVerified";
            this.btnLastVerified.Size = new System.Drawing.Size(75, 23);
            this.btnLastVerified.TabIndex = 18;
            this.btnLastVerified.Text = "Last Verified";
            this.btnLastVerified.UseVisualStyleBackColor = true;
            this.btnLastVerified.Click += new System.EventHandler(this.btnLastVerified_Click);
            // 
            // btnDeleteTran
            // 
            this.btnDeleteTran.Location = new System.Drawing.Point(365, 422);
            this.btnDeleteTran.Name = "btnDeleteTran";
            this.btnDeleteTran.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteTran.TabIndex = 17;
            this.btnDeleteTran.Text = "Delete";
            this.btnDeleteTran.UseVisualStyleBackColor = true;
            this.btnDeleteTran.Click += new System.EventHandler(this.btnDeleteTran_Click);
            // 
            // btnTranBrowse
            // 
            this.btnTranBrowse.Location = new System.Drawing.Point(284, 422);
            this.btnTranBrowse.Name = "btnTranBrowse";
            this.btnTranBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnTranBrowse.TabIndex = 16;
            this.btnTranBrowse.Text = "Details";
            this.btnTranBrowse.UseVisualStyleBackColor = true;
            this.btnTranBrowse.Click += new System.EventHandler(this.btnTranBrowse_Click);
            // 
            // btnTranNew
            // 
            this.btnTranNew.Location = new System.Drawing.Point(203, 422);
            this.btnTranNew.Name = "btnTranNew";
            this.btnTranNew.Size = new System.Drawing.Size(75, 23);
            this.btnTranNew.TabIndex = 15;
            this.btnTranNew.Text = "New";
            this.btnTranNew.UseVisualStyleBackColor = true;
            this.btnTranNew.Click += new System.EventHandler(this.btnTranNew_Click);
            // 
            // panHeader
            // 
            this.panHeader.Controls.Add(this.btnPendingWagers);
            this.panHeader.Controls.Add(this.lblChangeMakeup);
            this.panHeader.Controls.Add(this.lblNewMakeup);
            this.panHeader.Controls.Add(this.txtNewMakeup);
            this.panHeader.Controls.Add(this.btnChangeCustMakeup);
            this.panHeader.Controls.Add(this.txtNonPostedCasino);
            this.panHeader.Controls.Add(this.txtLastWeekCO);
            this.panHeader.Controls.Add(this.lblDisplay);
            this.panHeader.Controls.Add(this.txtPendingBets);
            this.panHeader.Controls.Add(this.btnNonPostedCasino);
            this.panHeader.Controls.Add(this.txtAvailableBalance);
            this.panHeader.Controls.Add(this.lblAvailableBalance);
            this.panHeader.Controls.Add(this.lblPendingBets);
            this.panHeader.Controls.Add(this.btnChangeCO);
            this.panHeader.Controls.Add(this.lblLastWeekCO);
            this.panHeader.Controls.Add(this.lblNonPostedCasino);
            this.panHeader.Controls.Add(this.cmbDisplay);
            this.panHeader.Controls.Add(this.btnBalance);
            this.panHeader.Location = new System.Drawing.Point(3, 3);
            this.panHeader.Name = "panHeader";
            this.panHeader.Size = new System.Drawing.Size(713, 67);
            this.panHeader.TabIndex = 14;
            // 
            // btnPendingWagers
            // 
            this.btnPendingWagers.Location = new System.Drawing.Point(339, 37);
            this.btnPendingWagers.Name = "btnPendingWagers";
            this.btnPendingWagers.Size = new System.Drawing.Size(24, 23);
            this.btnPendingWagers.TabIndex = 28;
            this.btnPendingWagers.Text = "...";
            this.btnPendingWagers.UseVisualStyleBackColor = true;
            this.btnPendingWagers.Click += new System.EventHandler(this.btnPendingWagers_Click);
            // 
            // lblChangeMakeup
            // 
            this.lblChangeMakeup.AutoSize = true;
            this.lblChangeMakeup.Location = new System.Drawing.Point(494, 22);
            this.lblChangeMakeup.Name = "lblChangeMakeup";
            this.lblChangeMakeup.Size = new System.Drawing.Size(86, 13);
            this.lblChangeMakeup.TabIndex = 27;
            this.lblChangeMakeup.Text = "D_Click/Change";
            this.lblChangeMakeup.DoubleClick += new System.EventHandler(this.lblChangeMakeup_DoubleClick);
            // 
            // lblNewMakeup
            // 
            this.lblNewMakeup.AutoSize = true;
            this.lblNewMakeup.Location = new System.Drawing.Point(493, 7);
            this.lblNewMakeup.Name = "lblNewMakeup";
            this.lblNewMakeup.Size = new System.Drawing.Size(68, 13);
            this.lblNewMakeup.TabIndex = 26;
            this.lblNewMakeup.Text = "Curr Makeup";
            // 
            // txtNewMakeup
            // 
            this.txtNewMakeup.AllowCents = false;
            this.txtNewMakeup.AllowNegatives = false;
            this.txtNewMakeup.DividedBy = 1;
            this.txtNewMakeup.DivideFlag = false;
            this.txtNewMakeup.Enabled = false;
            this.txtNewMakeup.Location = new System.Drawing.Point(490, 40);
            this.txtNewMakeup.Name = "txtNewMakeup";
            this.txtNewMakeup.ShowIfZero = true;
            this.txtNewMakeup.Size = new System.Drawing.Size(77, 20);
            this.txtNewMakeup.TabIndex = 25;
            this.txtNewMakeup.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnChangeCustMakeup
            // 
            this.btnChangeCustMakeup.Location = new System.Drawing.Point(566, 38);
            this.btnChangeCustMakeup.Name = "btnChangeCustMakeup";
            this.btnChangeCustMakeup.Size = new System.Drawing.Size(24, 23);
            this.btnChangeCustMakeup.TabIndex = 24;
            this.btnChangeCustMakeup.Text = "...";
            this.btnChangeCustMakeup.UseVisualStyleBackColor = true;
            this.btnChangeCustMakeup.Click += new System.EventHandler(this.btnChangeCustMakeup_Click);
            // 
            // txtNonPostedCasino
            // 
            this.txtNonPostedCasino.AllowCents = false;
            this.txtNonPostedCasino.AllowNegatives = false;
            this.txtNonPostedCasino.DividedBy = 100;
            this.txtNonPostedCasino.DivideFlag = true;
            this.txtNonPostedCasino.Enabled = false;
            this.txtNonPostedCasino.Location = new System.Drawing.Point(601, 40);
            this.txtNonPostedCasino.Name = "txtNonPostedCasino";
            this.txtNonPostedCasino.ShowIfZero = true;
            this.txtNonPostedCasino.Size = new System.Drawing.Size(89, 20);
            this.txtNonPostedCasino.TabIndex = 23;
            this.txtNonPostedCasino.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtNonPostedCasino.Leave += new System.EventHandler(this.numberToText_Leave);
            // 
            // txtLastWeekCO
            // 
            this.txtLastWeekCO.AllowCents = false;
            this.txtLastWeekCO.AllowNegatives = false;
            this.txtLastWeekCO.DividedBy = 1;
            this.txtLastWeekCO.DivideFlag = false;
            this.txtLastWeekCO.Enabled = false;
            this.txtLastWeekCO.Location = new System.Drawing.Point(369, 40);
            this.txtLastWeekCO.Name = "txtLastWeekCO";
            this.txtLastWeekCO.ShowIfZero = true;
            this.txtLastWeekCO.Size = new System.Drawing.Size(89, 20);
            this.txtLastWeekCO.TabIndex = 22;
            this.txtLastWeekCO.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtLastWeekCO.Leave += new System.EventHandler(this.numberToText_Leave);
            // 
            // lblDisplay
            // 
            this.lblDisplay.AutoSize = true;
            this.lblDisplay.Location = new System.Drawing.Point(4, 7);
            this.lblDisplay.Name = "lblDisplay";
            this.lblDisplay.Size = new System.Drawing.Size(41, 13);
            this.lblDisplay.TabIndex = 0;
            this.lblDisplay.Text = "Display";
            // 
            // txtPendingBets
            // 
            this.txtPendingBets.AllowCents = false;
            this.txtPendingBets.AllowNegatives = false;
            this.txtPendingBets.DividedBy = 1;
            this.txtPendingBets.DivideFlag = false;
            this.txtPendingBets.Enabled = false;
            this.txtPendingBets.Location = new System.Drawing.Point(249, 39);
            this.txtPendingBets.Name = "txtPendingBets";
            this.txtPendingBets.ShowIfZero = true;
            this.txtPendingBets.Size = new System.Drawing.Size(89, 20);
            this.txtPendingBets.TabIndex = 21;
            this.txtPendingBets.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPendingBets.Leave += new System.EventHandler(this.numberToText_Leave);
            // 
            // btnNonPostedCasino
            // 
            this.btnNonPostedCasino.Location = new System.Drawing.Point(689, 38);
            this.btnNonPostedCasino.Name = "btnNonPostedCasino";
            this.btnNonPostedCasino.Size = new System.Drawing.Size(24, 23);
            this.btnNonPostedCasino.TabIndex = 12;
            this.btnNonPostedCasino.Text = "?";
            this.btnNonPostedCasino.UseVisualStyleBackColor = true;
            this.btnNonPostedCasino.Click += new System.EventHandler(this.btnNonPostedCasino_Click);
            // 
            // txtAvailableBalance
            // 
            this.txtAvailableBalance.AllowCents = false;
            this.txtAvailableBalance.AllowNegatives = false;
            this.txtAvailableBalance.DividedBy = 100;
            this.txtAvailableBalance.DivideFlag = true;
            this.txtAvailableBalance.Enabled = false;
            this.txtAvailableBalance.Location = new System.Drawing.Point(122, 40);
            this.txtAvailableBalance.Name = "txtAvailableBalance";
            this.txtAvailableBalance.ShowIfZero = true;
            this.txtAvailableBalance.Size = new System.Drawing.Size(89, 20);
            this.txtAvailableBalance.TabIndex = 20;
            this.txtAvailableBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAvailableBalance.Leave += new System.EventHandler(this.numberToText_Leave);
            // 
            // lblAvailableBalance
            // 
            this.lblAvailableBalance.AutoSize = true;
            this.lblAvailableBalance.Location = new System.Drawing.Point(127, 7);
            this.lblAvailableBalance.Name = "lblAvailableBalance";
            this.lblAvailableBalance.Size = new System.Drawing.Size(92, 13);
            this.lblAvailableBalance.TabIndex = 1;
            this.lblAvailableBalance.Text = "Available Balance";
            // 
            // lblPendingBets
            // 
            this.lblPendingBets.AutoSize = true;
            this.lblPendingBets.Location = new System.Drawing.Point(272, 7);
            this.lblPendingBets.Name = "lblPendingBets";
            this.lblPendingBets.Size = new System.Drawing.Size(70, 13);
            this.lblPendingBets.TabIndex = 2;
            this.lblPendingBets.Text = "Pending Bets";
            this.lblPendingBets.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lblPendingBets_MouseDoubleClick);
            // 
            // btnChangeCO
            // 
            this.btnChangeCO.Location = new System.Drawing.Point(457, 38);
            this.btnChangeCO.Name = "btnChangeCO";
            this.btnChangeCO.Size = new System.Drawing.Size(24, 23);
            this.btnChangeCO.TabIndex = 10;
            this.btnChangeCO.Text = "...";
            this.btnChangeCO.UseVisualStyleBackColor = true;
            this.btnChangeCO.Click += new System.EventHandler(this.btnChangeCO_Click);
            // 
            // lblLastWeekCO
            // 
            this.lblLastWeekCO.AutoSize = true;
            this.lblLastWeekCO.Location = new System.Drawing.Point(366, 7);
            this.lblLastWeekCO.Name = "lblLastWeekCO";
            this.lblLastWeekCO.Size = new System.Drawing.Size(107, 13);
            this.lblLastWeekCO.TabIndex = 3;
            this.lblLastWeekCO.Text = "Last Week Carryover";
            // 
            // lblNonPostedCasino
            // 
            this.lblNonPostedCasino.AutoSize = true;
            this.lblNonPostedCasino.Location = new System.Drawing.Point(599, 7);
            this.lblNonPostedCasino.Name = "lblNonPostedCasino";
            this.lblNonPostedCasino.Size = new System.Drawing.Size(98, 13);
            this.lblNonPostedCasino.TabIndex = 4;
            this.lblNonPostedCasino.Text = "Non-Posted Casino";
            // 
            // cmbDisplay
            // 
            this.cmbDisplay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDisplay.FormattingEnabled = true;
            this.cmbDisplay.Items.AddRange(new object[] {
            "Last 7 Days",
            "Last 14 Days",
            "Last 30 Days",
            "Last 45 Days",
            "Last 90 Days",
            "All Transactions"});
            this.cmbDisplay.Location = new System.Drawing.Point(5, 39);
            this.cmbDisplay.Name = "cmbDisplay";
            this.cmbDisplay.Size = new System.Drawing.Size(101, 21);
            this.cmbDisplay.TabIndex = 5;
            this.cmbDisplay.SelectedIndexChanged += new System.EventHandler(this.cmbDisplay_SelectedIndexChanged);
            // 
            // btnBalance
            // 
            this.btnBalance.Location = new System.Drawing.Point(212, 38);
            this.btnBalance.Name = "btnBalance";
            this.btnBalance.Size = new System.Drawing.Size(24, 23);
            this.btnBalance.TabIndex = 7;
            this.btnBalance.Text = "?";
            this.btnBalance.UseVisualStyleBackColor = true;
            this.btnBalance.Click += new System.EventHandler(this.btnBalance_Click);
            // 
            // panTransactionsDetails
            // 
            this.panTransactionsDetails.Controls.Add(this.dgvwTransactions);
            this.panTransactionsDetails.Location = new System.Drawing.Point(3, 76);
            this.panTransactionsDetails.Name = "panTransactionsDetails";
            this.panTransactionsDetails.Size = new System.Drawing.Size(713, 323);
            this.panTransactionsDetails.TabIndex = 13;
            // 
            // dgvwTransactions
            // 
            this.dgvwTransactions.AllowUserToAddRows = false;
            this.dgvwTransactions.AllowUserToDeleteRows = false;
            this.dgvwTransactions.AllowUserToResizeRows = false;
            this.dgvwTransactions.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvwTransactions.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvwTransactions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvwTransactions.ColumnHeadersVisible = false;
            this.dgvwTransactions.ContextMenuStrip = this.ctxMenuStripTransactions;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvwTransactions.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgvwTransactions.GridColor = System.Drawing.Color.White;
            this.dgvwTransactions.Location = new System.Drawing.Point(0, 3);
            this.dgvwTransactions.MultiSelect = false;
            this.dgvwTransactions.Name = "dgvwTransactions";
            this.dgvwTransactions.ReadOnly = true;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvwTransactions.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvwTransactions.RowHeadersVisible = false;
            this.dgvwTransactions.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvwTransactions.Size = new System.Drawing.Size(713, 320);
            this.dgvwTransactions.TabIndex = 0;
            this.dgvwTransactions.SelectionChanged += new System.EventHandler(this.dgvwTransactions_SelectionChanged);
            this.dgvwTransactions.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgvwTransactions_MouseDown);
            // 
            // ctxMenuStripTransactions
            // 
            this.ctxMenuStripTransactions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiNewTransaction,
            this.tsmiChangeTransaction,
            this.tsmiDeleteTransaction});
            this.ctxMenuStripTransactions.Name = "ctxMenuStripTransactions";
            this.ctxMenuStripTransactions.Size = new System.Drawing.Size(125, 70);
            this.ctxMenuStripTransactions.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.ctxMenuStripTransactions_ItemClicked);
            // 
            // tsmiNewTransaction
            // 
            this.tsmiNewTransaction.AccessibleName = "0";
            this.tsmiNewTransaction.Name = "tsmiNewTransaction";
            this.tsmiNewTransaction.Size = new System.Drawing.Size(124, 22);
            this.tsmiNewTransaction.Text = "New...";
            // 
            // tsmiChangeTransaction
            // 
            this.tsmiChangeTransaction.AccessibleName = "1";
            this.tsmiChangeTransaction.Name = "tsmiChangeTransaction";
            this.tsmiChangeTransaction.Size = new System.Drawing.Size(124, 22);
            this.tsmiChangeTransaction.Text = "Change...";
            // 
            // tsmiDeleteTransaction
            // 
            this.tsmiDeleteTransaction.AccessibleName = "2";
            this.tsmiDeleteTransaction.Name = "tsmiDeleteTransaction";
            this.tsmiDeleteTransaction.Size = new System.Drawing.Size(124, 22);
            this.tsmiDeleteTransaction.Text = "Delete";
            // 
            // tabpFreePlays
            // 
            this.tabpFreePlays.Controls.Add(this.panFreePlays);
            this.tabpFreePlays.Location = new System.Drawing.Point(4, 22);
            this.tabpFreePlays.Name = "tabpFreePlays";
            this.tabpFreePlays.Size = new System.Drawing.Size(793, 474);
            this.tabpFreePlays.TabIndex = 5;
            this.tabpFreePlays.Text = "Free Play";
            this.tabpFreePlays.UseVisualStyleBackColor = true;
            // 
            // panFreePlays
            // 
            this.panFreePlays.Controls.Add(this.btnFreePlayGoToWagers);
            this.panFreePlays.Controls.Add(this.btnFreePlayDelete);
            this.panFreePlays.Controls.Add(this.btnFreePlayNew);
            this.panFreePlays.Controls.Add(this.panFreePlaysDetails);
            this.panFreePlays.Controls.Add(this.panFreePlayHeader);
            this.panFreePlays.Location = new System.Drawing.Point(15, 4);
            this.panFreePlays.Name = "panFreePlays";
            this.panFreePlays.Size = new System.Drawing.Size(732, 465);
            this.panFreePlays.TabIndex = 0;
            // 
            // btnFreePlayGoToWagers
            // 
            this.btnFreePlayGoToWagers.Location = new System.Drawing.Point(613, 408);
            this.btnFreePlayGoToWagers.Name = "btnFreePlayGoToWagers";
            this.btnFreePlayGoToWagers.Size = new System.Drawing.Size(103, 23);
            this.btnFreePlayGoToWagers.TabIndex = 22;
            this.btnFreePlayGoToWagers.Text = "Go To Wagers...";
            this.btnFreePlayGoToWagers.UseVisualStyleBackColor = true;
            this.btnFreePlayGoToWagers.Click += new System.EventHandler(this.btnFreePlayGoToWagers_Click);
            // 
            // btnFreePlayDelete
            // 
            this.btnFreePlayDelete.Location = new System.Drawing.Point(113, 408);
            this.btnFreePlayDelete.Name = "btnFreePlayDelete";
            this.btnFreePlayDelete.Size = new System.Drawing.Size(75, 23);
            this.btnFreePlayDelete.TabIndex = 21;
            this.btnFreePlayDelete.Text = "Delete";
            this.btnFreePlayDelete.UseVisualStyleBackColor = true;
            this.btnFreePlayDelete.Click += new System.EventHandler(this.btnFreePlayDelete_Click);
            // 
            // btnFreePlayNew
            // 
            this.btnFreePlayNew.Location = new System.Drawing.Point(13, 408);
            this.btnFreePlayNew.Name = "btnFreePlayNew";
            this.btnFreePlayNew.Size = new System.Drawing.Size(75, 23);
            this.btnFreePlayNew.TabIndex = 20;
            this.btnFreePlayNew.Text = "New";
            this.btnFreePlayNew.UseVisualStyleBackColor = true;
            this.btnFreePlayNew.Click += new System.EventHandler(this.btnFreePlayNew_Click);
            // 
            // panFreePlaysDetails
            // 
            this.panFreePlaysDetails.Controls.Add(this.dgvwFreePlaysDetails);
            this.panFreePlaysDetails.Location = new System.Drawing.Point(3, 76);
            this.panFreePlaysDetails.Name = "panFreePlaysDetails";
            this.panFreePlaysDetails.Size = new System.Drawing.Size(713, 326);
            this.panFreePlaysDetails.TabIndex = 16;
            // 
            // dgvwFreePlaysDetails
            // 
            this.dgvwFreePlaysDetails.AllowUserToAddRows = false;
            this.dgvwFreePlaysDetails.AllowUserToDeleteRows = false;
            this.dgvwFreePlaysDetails.AllowUserToResizeRows = false;
            this.dgvwFreePlaysDetails.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvwFreePlaysDetails.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgvwFreePlaysDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvwFreePlaysDetails.ColumnHeadersVisible = false;
            this.dgvwFreePlaysDetails.ContextMenuStrip = this.ctxMenuStripFreePlays;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvwFreePlaysDetails.DefaultCellStyle = dataGridViewCellStyle11;
            this.dgvwFreePlaysDetails.GridColor = System.Drawing.Color.White;
            this.dgvwFreePlaysDetails.Location = new System.Drawing.Point(0, 3);
            this.dgvwFreePlaysDetails.MultiSelect = false;
            this.dgvwFreePlaysDetails.Name = "dgvwFreePlaysDetails";
            this.dgvwFreePlaysDetails.ReadOnly = true;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvwFreePlaysDetails.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dgvwFreePlaysDetails.RowHeadersVisible = false;
            this.dgvwFreePlaysDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvwFreePlaysDetails.Size = new System.Drawing.Size(713, 320);
            this.dgvwFreePlaysDetails.TabIndex = 0;
            this.dgvwFreePlaysDetails.SelectionChanged += new System.EventHandler(this.dgvwFreePlaysDetails_SelectionChanged);
            this.dgvwFreePlaysDetails.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgvwFreePlaysDetails_MouseDown);
            // 
            // ctxMenuStripFreePlays
            // 
            this.ctxMenuStripFreePlays.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiNewFreePlay,
            this.tsmiDeleteFreePlay});
            this.ctxMenuStripFreePlays.Name = "ctxMenuStripFreePlays";
            this.ctxMenuStripFreePlays.Size = new System.Drawing.Size(108, 48);
            this.ctxMenuStripFreePlays.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.ctxMenuStripFreePlays_ItemClicked);
            // 
            // tsmiNewFreePlay
            // 
            this.tsmiNewFreePlay.AccessibleName = "0";
            this.tsmiNewFreePlay.Name = "tsmiNewFreePlay";
            this.tsmiNewFreePlay.Size = new System.Drawing.Size(107, 22);
            this.tsmiNewFreePlay.Text = "New...";
            // 
            // tsmiDeleteFreePlay
            // 
            this.tsmiDeleteFreePlay.AccessibleName = "1";
            this.tsmiDeleteFreePlay.Name = "tsmiDeleteFreePlay";
            this.tsmiDeleteFreePlay.Size = new System.Drawing.Size(107, 22);
            this.tsmiDeleteFreePlay.Text = "Delete";
            // 
            // panFreePlayHeader
            // 
            this.panFreePlayHeader.Controls.Add(this.txtFreePlayPendingCount);
            this.panFreePlayHeader.Controls.Add(this.lblFreePlayDisplay);
            this.panFreePlayHeader.Controls.Add(this.txtFreePlayPendingAmount);
            this.panFreePlayHeader.Controls.Add(this.lblFreePlayAvailableBalance);
            this.panFreePlayHeader.Controls.Add(this.txtFreePlayBalance);
            this.panFreePlayHeader.Controls.Add(this.lblFreePlayPendingAmount);
            this.panFreePlayHeader.Controls.Add(this.lblFreePlayPendingCount);
            this.panFreePlayHeader.Controls.Add(this.cmbFreePlayDisplay);
            this.panFreePlayHeader.Location = new System.Drawing.Point(3, 3);
            this.panFreePlayHeader.Name = "panFreePlayHeader";
            this.panFreePlayHeader.Size = new System.Drawing.Size(713, 67);
            this.panFreePlayHeader.TabIndex = 15;
            // 
            // txtFreePlayPendingCount
            // 
            this.txtFreePlayPendingCount.AllowCents = false;
            this.txtFreePlayPendingCount.AllowNegatives = false;
            this.txtFreePlayPendingCount.DividedBy = 1;
            this.txtFreePlayPendingCount.DivideFlag = false;
            this.txtFreePlayPendingCount.Enabled = false;
            this.txtFreePlayPendingCount.Location = new System.Drawing.Point(597, 32);
            this.txtFreePlayPendingCount.Name = "txtFreePlayPendingCount";
            this.txtFreePlayPendingCount.ShowIfZero = true;
            this.txtFreePlayPendingCount.Size = new System.Drawing.Size(100, 20);
            this.txtFreePlayPendingCount.TabIndex = 25;
            this.txtFreePlayPendingCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFreePlayPendingCount.Leave += new System.EventHandler(this.numberToText_Leave);
            // 
            // lblFreePlayDisplay
            // 
            this.lblFreePlayDisplay.AutoSize = true;
            this.lblFreePlayDisplay.Location = new System.Drawing.Point(7, 7);
            this.lblFreePlayDisplay.Name = "lblFreePlayDisplay";
            this.lblFreePlayDisplay.Size = new System.Drawing.Size(41, 13);
            this.lblFreePlayDisplay.TabIndex = 0;
            this.lblFreePlayDisplay.Text = "Display";
            // 
            // txtFreePlayPendingAmount
            // 
            this.txtFreePlayPendingAmount.AllowCents = false;
            this.txtFreePlayPendingAmount.AllowNegatives = false;
            this.txtFreePlayPendingAmount.DividedBy = 100;
            this.txtFreePlayPendingAmount.DivideFlag = true;
            this.txtFreePlayPendingAmount.Enabled = false;
            this.txtFreePlayPendingAmount.Location = new System.Drawing.Point(435, 32);
            this.txtFreePlayPendingAmount.Name = "txtFreePlayPendingAmount";
            this.txtFreePlayPendingAmount.ShowIfZero = true;
            this.txtFreePlayPendingAmount.Size = new System.Drawing.Size(100, 20);
            this.txtFreePlayPendingAmount.TabIndex = 24;
            this.txtFreePlayPendingAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFreePlayPendingAmount.Leave += new System.EventHandler(this.numberToText_Leave);
            // 
            // lblFreePlayAvailableBalance
            // 
            this.lblFreePlayAvailableBalance.AutoSize = true;
            this.lblFreePlayAvailableBalance.Location = new System.Drawing.Point(306, 7);
            this.lblFreePlayAvailableBalance.Name = "lblFreePlayAvailableBalance";
            this.lblFreePlayAvailableBalance.Size = new System.Drawing.Size(46, 13);
            this.lblFreePlayAvailableBalance.TabIndex = 1;
            this.lblFreePlayAvailableBalance.Text = "Balance";
            // 
            // txtFreePlayBalance
            // 
            this.txtFreePlayBalance.AllowCents = false;
            this.txtFreePlayBalance.AllowNegatives = false;
            this.txtFreePlayBalance.DividedBy = 100;
            this.txtFreePlayBalance.DivideFlag = true;
            this.txtFreePlayBalance.Enabled = false;
            this.txtFreePlayBalance.Location = new System.Drawing.Point(288, 32);
            this.txtFreePlayBalance.Name = "txtFreePlayBalance";
            this.txtFreePlayBalance.ShowIfZero = true;
            this.txtFreePlayBalance.Size = new System.Drawing.Size(100, 20);
            this.txtFreePlayBalance.TabIndex = 23;
            this.txtFreePlayBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFreePlayBalance.Leave += new System.EventHandler(this.numberToText_Leave);
            // 
            // lblFreePlayPendingAmount
            // 
            this.lblFreePlayPendingAmount.AutoSize = true;
            this.lblFreePlayPendingAmount.Location = new System.Drawing.Point(451, 7);
            this.lblFreePlayPendingAmount.Name = "lblFreePlayPendingAmount";
            this.lblFreePlayPendingAmount.Size = new System.Drawing.Size(67, 13);
            this.lblFreePlayPendingAmount.TabIndex = 2;
            this.lblFreePlayPendingAmount.Text = "Pending Amt";
            // 
            // lblFreePlayPendingCount
            // 
            this.lblFreePlayPendingCount.AutoSize = true;
            this.lblFreePlayPendingCount.Location = new System.Drawing.Point(624, 7);
            this.lblFreePlayPendingCount.Name = "lblFreePlayPendingCount";
            this.lblFreePlayPendingCount.Size = new System.Drawing.Size(77, 13);
            this.lblFreePlayPendingCount.TabIndex = 3;
            this.lblFreePlayPendingCount.Text = "Pending Count";
            // 
            // cmbFreePlayDisplay
            // 
            this.cmbFreePlayDisplay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbFreePlayDisplay.FormattingEnabled = true;
            this.cmbFreePlayDisplay.Items.AddRange(new object[] {
            "Last 7 Days",
            "Last 14 Days",
            "Last 30 Days",
            "Last 45 Days",
            "Last 90 Days",
            "All Transactions"});
            this.cmbFreePlayDisplay.Location = new System.Drawing.Point(10, 33);
            this.cmbFreePlayDisplay.Name = "cmbFreePlayDisplay";
            this.cmbFreePlayDisplay.Size = new System.Drawing.Size(121, 21);
            this.cmbFreePlayDisplay.TabIndex = 5;
            this.cmbFreePlayDisplay.SelectedIndexChanged += new System.EventHandler(this.cmbFreePlayDisplay_SelectedIndexChanged);
            // 
            // tabpComments
            // 
            this.tabpComments.Controls.Add(this.panComments);
            this.tabpComments.Location = new System.Drawing.Point(4, 22);
            this.tabpComments.Name = "tabpComments";
            this.tabpComments.Size = new System.Drawing.Size(793, 474);
            this.tabpComments.TabIndex = 6;
            this.tabpComments.Text = "Comments";
            this.tabpComments.UseVisualStyleBackColor = true;
            // 
            // panComments
            // 
            this.panComments.Controls.Add(this.grpCommentsForCustomer);
            this.panComments.Controls.Add(this.grpCommentsForTW);
            this.panComments.Location = new System.Drawing.Point(3, 4);
            this.panComments.Name = "panComments";
            this.panComments.Size = new System.Drawing.Size(752, 465);
            this.panComments.TabIndex = 0;
            // 
            // grpCommentsForCustomer
            // 
            this.grpCommentsForCustomer.Controls.Add(this.dgvwCustomerMessages);
            this.grpCommentsForCustomer.Controls.Add(this.btnSendToPackage);
            this.grpCommentsForCustomer.Controls.Add(this.chbShowDeleted);
            this.grpCommentsForCustomer.Controls.Add(this.txtCommentsForCustomer);
            this.grpCommentsForCustomer.Controls.Add(this.btnNewMessage);
            this.grpCommentsForCustomer.Location = new System.Drawing.Point(4, 141);
            this.grpCommentsForCustomer.Name = "grpCommentsForCustomer";
            this.grpCommentsForCustomer.Size = new System.Drawing.Size(732, 295);
            this.grpCommentsForCustomer.TabIndex = 2;
            this.grpCommentsForCustomer.TabStop = false;
            this.grpCommentsForCustomer.Text = "Comments For Customer";
            // 
            // dgvwCustomerMessages
            // 
            this.dgvwCustomerMessages.AllowUserToAddRows = false;
            this.dgvwCustomerMessages.AllowUserToDeleteRows = false;
            this.dgvwCustomerMessages.AllowUserToResizeRows = false;
            this.dgvwCustomerMessages.BackgroundColor = System.Drawing.Color.White;
            this.dgvwCustomerMessages.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvwCustomerMessages.Location = new System.Drawing.Point(7, 78);
            this.dgvwCustomerMessages.MultiSelect = false;
            this.dgvwCustomerMessages.Name = "dgvwCustomerMessages";
            this.dgvwCustomerMessages.ReadOnly = true;
            this.dgvwCustomerMessages.RowHeadersVisible = false;
            this.dgvwCustomerMessages.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvwCustomerMessages.Size = new System.Drawing.Size(722, 169);
            this.dgvwCustomerMessages.TabIndex = 0;
            this.dgvwCustomerMessages.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvwCustomerMessages_CellClick);
            // 
            // btnSendToPackage
            // 
            this.btnSendToPackage.Location = new System.Drawing.Point(612, 51);
            this.btnSendToPackage.Name = "btnSendToPackage";
            this.btnSendToPackage.Size = new System.Drawing.Size(117, 23);
            this.btnSendToPackage.TabIndex = 0;
            this.btnSendToPackage.Text = "Send To Package";
            this.btnSendToPackage.UseVisualStyleBackColor = true;
            this.btnSendToPackage.Click += new System.EventHandler(this.btnSendToPackage_Click);
            // 
            // chbShowDeleted
            // 
            this.chbShowDeleted.AutoSize = true;
            this.chbShowDeleted.Location = new System.Drawing.Point(7, 19);
            this.chbShowDeleted.Name = "chbShowDeleted";
            this.chbShowDeleted.Size = new System.Drawing.Size(144, 17);
            this.chbShowDeleted.TabIndex = 3;
            this.chbShowDeleted.Text = "Show Deleted Messages";
            this.chbShowDeleted.UseVisualStyleBackColor = true;
            this.chbShowDeleted.CheckedChanged += new System.EventHandler(this.chbShowDeleted_CheckedChanged);
            // 
            // txtCommentsForCustomer
            // 
            this.txtCommentsForCustomer.Location = new System.Drawing.Point(126, 19);
            this.txtCommentsForCustomer.Multiline = true;
            this.txtCommentsForCustomer.Name = "txtCommentsForCustomer";
            this.txtCommentsForCustomer.Size = new System.Drawing.Size(84, 18);
            this.txtCommentsForCustomer.TabIndex = 2;
            this.txtCommentsForCustomer.Visible = false;
            // 
            // btnNewMessage
            // 
            this.btnNewMessage.Location = new System.Drawing.Point(7, 50);
            this.btnNewMessage.Name = "btnNewMessage";
            this.btnNewMessage.Size = new System.Drawing.Size(123, 23);
            this.btnNewMessage.TabIndex = 2;
            this.btnNewMessage.Text = "New Message";
            this.btnNewMessage.UseVisualStyleBackColor = true;
            this.btnNewMessage.Click += new System.EventHandler(this.btnNewMessage_Click);
            // 
            // grpCommentsForTW
            // 
            this.grpCommentsForTW.Controls.Add(this.txtCommentsForTW);
            this.grpCommentsForTW.Location = new System.Drawing.Point(3, 3);
            this.grpCommentsForTW.Name = "grpCommentsForTW";
            this.grpCommentsForTW.Size = new System.Drawing.Size(739, 122);
            this.grpCommentsForTW.TabIndex = 1;
            this.grpCommentsForTW.TabStop = false;
            this.grpCommentsForTW.Text = "Comments for Ticket Writers";
            // 
            // txtCommentsForTW
            // 
            this.txtCommentsForTW.Location = new System.Drawing.Point(7, 19);
            this.txtCommentsForTW.Multiline = true;
            this.txtCommentsForTW.Name = "txtCommentsForTW";
            this.txtCommentsForTW.Size = new System.Drawing.Size(726, 89);
            this.txtCommentsForTW.TabIndex = 1;
            // 
            // tabpMarketing
            // 
            this.tabpMarketing.Controls.Add(this.panMarketing);
            this.tabpMarketing.Location = new System.Drawing.Point(4, 22);
            this.tabpMarketing.Name = "tabpMarketing";
            this.tabpMarketing.Size = new System.Drawing.Size(793, 474);
            this.tabpMarketing.TabIndex = 7;
            this.tabpMarketing.Text = "Notes";
            this.tabpMarketing.UseVisualStyleBackColor = true;
            // 
            // panMarketing
            // 
            this.panMarketing.Controls.Add(this.grpGeneralComments);
            this.panMarketing.Controls.Add(this.panGeneralMarketing);
            this.panMarketing.Controls.Add(this.grpEstimatedWagerAmount);
            this.panMarketing.Controls.Add(this.grpSportsOfInterest);
            this.panMarketing.Controls.Add(this.grpContactRestrictions);
            this.panMarketing.Location = new System.Drawing.Point(19, 4);
            this.panMarketing.Name = "panMarketing";
            this.panMarketing.Size = new System.Drawing.Size(721, 460);
            this.panMarketing.TabIndex = 0;
            // 
            // grpGeneralComments
            // 
            this.grpGeneralComments.Controls.Add(this.txtGeneralComments);
            this.grpGeneralComments.Location = new System.Drawing.Point(8, 8);
            this.grpGeneralComments.Name = "grpGeneralComments";
            this.grpGeneralComments.Size = new System.Drawing.Size(699, 200);
            this.grpGeneralComments.TabIndex = 161;
            this.grpGeneralComments.TabStop = false;
            this.grpGeneralComments.Text = "General Comments";
            // 
            // txtGeneralComments
            // 
            this.txtGeneralComments.Location = new System.Drawing.Point(7, 29);
            this.txtGeneralComments.Multiline = true;
            this.txtGeneralComments.Name = "txtGeneralComments";
            this.txtGeneralComments.Size = new System.Drawing.Size(686, 165);
            this.txtGeneralComments.TabIndex = 0;
            // 
            // panGeneralMarketing
            // 
            this.panGeneralMarketing.Controls.Add(this.lblSource);
            this.panGeneralMarketing.Controls.Add(this.lblPromoter);
            this.panGeneralMarketing.Controls.Add(this.lblReferredBy);
            this.panGeneralMarketing.Controls.Add(this.txtSource);
            this.panGeneralMarketing.Controls.Add(this.txtReferredBy);
            this.panGeneralMarketing.Controls.Add(this.txtPromoter);
            this.panGeneralMarketing.Location = new System.Drawing.Point(25, 102);
            this.panGeneralMarketing.Name = "panGeneralMarketing";
            this.panGeneralMarketing.Size = new System.Drawing.Size(682, 82);
            this.panGeneralMarketing.TabIndex = 10;
            this.panGeneralMarketing.Visible = false;
            // 
            // lblSource
            // 
            this.lblSource.AutoSize = true;
            this.lblSource.Location = new System.Drawing.Point(17, 15);
            this.lblSource.Name = "lblSource";
            this.lblSource.Size = new System.Drawing.Size(44, 13);
            this.lblSource.TabIndex = 0;
            this.lblSource.Text = "Source:";
            // 
            // lblPromoter
            // 
            this.lblPromoter.AutoSize = true;
            this.lblPromoter.Location = new System.Drawing.Point(9, 41);
            this.lblPromoter.Name = "lblPromoter";
            this.lblPromoter.Size = new System.Drawing.Size(52, 13);
            this.lblPromoter.TabIndex = 1;
            this.lblPromoter.Text = "Promoter:";
            // 
            // lblReferredBy
            // 
            this.lblReferredBy.AutoSize = true;
            this.lblReferredBy.Location = new System.Drawing.Point(344, 41);
            this.lblReferredBy.Name = "lblReferredBy";
            this.lblReferredBy.Size = new System.Drawing.Size(66, 13);
            this.lblReferredBy.TabIndex = 2;
            this.lblReferredBy.Text = "Referred By:";
            // 
            // txtSource
            // 
            this.txtSource.Location = new System.Drawing.Point(67, 12);
            this.txtSource.Name = "txtSource";
            this.txtSource.Size = new System.Drawing.Size(599, 20);
            this.txtSource.TabIndex = 20;
            // 
            // txtReferredBy
            // 
            this.txtReferredBy.Location = new System.Drawing.Point(416, 38);
            this.txtReferredBy.Name = "txtReferredBy";
            this.txtReferredBy.Size = new System.Drawing.Size(250, 20);
            this.txtReferredBy.TabIndex = 40;
            // 
            // txtPromoter
            // 
            this.txtPromoter.Location = new System.Drawing.Point(67, 38);
            this.txtPromoter.Name = "txtPromoter";
            this.txtPromoter.Size = new System.Drawing.Size(242, 20);
            this.txtPromoter.TabIndex = 30;
            // 
            // grpEstimatedWagerAmount
            // 
            this.grpEstimatedWagerAmount.Controls.Add(this.chbEstWagerAmount5To50);
            this.grpEstimatedWagerAmount.Controls.Add(this.chbEstWagerAmountOver1000);
            this.grpEstimatedWagerAmount.Controls.Add(this.chbEstWagerAmount51to100);
            this.grpEstimatedWagerAmount.Controls.Add(this.chbEstWagerAmount501To1000);
            this.grpEstimatedWagerAmount.Controls.Add(this.chbEstWagerAmount101to500);
            this.grpEstimatedWagerAmount.Location = new System.Drawing.Point(236, 214);
            this.grpEstimatedWagerAmount.Name = "grpEstimatedWagerAmount";
            this.grpEstimatedWagerAmount.Size = new System.Drawing.Size(177, 207);
            this.grpEstimatedWagerAmount.TabIndex = 160;
            this.grpEstimatedWagerAmount.TabStop = false;
            this.grpEstimatedWagerAmount.Text = "Estimated Wager Amount";
            this.grpEstimatedWagerAmount.Visible = false;
            // 
            // chbEstWagerAmount5To50
            // 
            this.chbEstWagerAmount5To50.AutoSize = true;
            this.chbEstWagerAmount5To50.Location = new System.Drawing.Point(6, 28);
            this.chbEstWagerAmount5To50.Name = "chbEstWagerAmount5To50";
            this.chbEstWagerAmount5To50.Size = new System.Drawing.Size(65, 17);
            this.chbEstWagerAmount5To50.TabIndex = 170;
            this.chbEstWagerAmount5To50.Text = "$5 - $50";
            this.chbEstWagerAmount5To50.UseVisualStyleBackColor = true;
            // 
            // chbEstWagerAmountOver1000
            // 
            this.chbEstWagerAmountOver1000.AutoSize = true;
            this.chbEstWagerAmountOver1000.Location = new System.Drawing.Point(6, 132);
            this.chbEstWagerAmountOver1000.Name = "chbEstWagerAmountOver1000";
            this.chbEstWagerAmountOver1000.Size = new System.Drawing.Size(82, 17);
            this.chbEstWagerAmountOver1000.TabIndex = 210;
            this.chbEstWagerAmountOver1000.Text = "Over $1000";
            this.chbEstWagerAmountOver1000.UseVisualStyleBackColor = true;
            // 
            // chbEstWagerAmount51to100
            // 
            this.chbEstWagerAmount51to100.AutoSize = true;
            this.chbEstWagerAmount51to100.Location = new System.Drawing.Point(6, 54);
            this.chbEstWagerAmount51to100.Name = "chbEstWagerAmount51to100";
            this.chbEstWagerAmount51to100.Size = new System.Drawing.Size(77, 17);
            this.chbEstWagerAmount51to100.TabIndex = 180;
            this.chbEstWagerAmount51to100.Text = "$51 - $100";
            this.chbEstWagerAmount51to100.UseVisualStyleBackColor = true;
            // 
            // chbEstWagerAmount501To1000
            // 
            this.chbEstWagerAmount501To1000.AutoSize = true;
            this.chbEstWagerAmount501To1000.Location = new System.Drawing.Point(6, 106);
            this.chbEstWagerAmount501To1000.Name = "chbEstWagerAmount501To1000";
            this.chbEstWagerAmount501To1000.Size = new System.Drawing.Size(89, 17);
            this.chbEstWagerAmount501To1000.TabIndex = 200;
            this.chbEstWagerAmount501To1000.Text = "$501 - $1000";
            this.chbEstWagerAmount501To1000.UseVisualStyleBackColor = true;
            // 
            // chbEstWagerAmount101to500
            // 
            this.chbEstWagerAmount101to500.AutoSize = true;
            this.chbEstWagerAmount101to500.Location = new System.Drawing.Point(6, 80);
            this.chbEstWagerAmount101to500.Name = "chbEstWagerAmount101to500";
            this.chbEstWagerAmount101to500.Size = new System.Drawing.Size(83, 17);
            this.chbEstWagerAmount101to500.TabIndex = 190;
            this.chbEstWagerAmount101to500.Text = "$101 - $500";
            this.chbEstWagerAmount101to500.UseVisualStyleBackColor = true;
            // 
            // grpSportsOfInterest
            // 
            this.grpSportsOfInterest.Controls.Add(this.InterestedInOther);
            this.grpSportsOfInterest.Controls.Add(this.InterestedInHorseRacing);
            this.grpSportsOfInterest.Controls.Add(this.InterestedInBaseball);
            this.grpSportsOfInterest.Controls.Add(this.InterestedInFootball);
            this.grpSportsOfInterest.Controls.Add(this.InterestedInHockey);
            this.grpSportsOfInterest.Controls.Add(this.chbInterestedInBasketBall);
            this.grpSportsOfInterest.Location = new System.Drawing.Point(25, 214);
            this.grpSportsOfInterest.Name = "grpSportsOfInterest";
            this.grpSportsOfInterest.Size = new System.Drawing.Size(177, 207);
            this.grpSportsOfInterest.TabIndex = 90;
            this.grpSportsOfInterest.TabStop = false;
            this.grpSportsOfInterest.Text = "Sports Of Interest";
            this.grpSportsOfInterest.Visible = false;
            // 
            // InterestedInOther
            // 
            this.InterestedInOther.AutoSize = true;
            this.InterestedInOther.Location = new System.Drawing.Point(6, 158);
            this.InterestedInOther.Name = "InterestedInOther";
            this.InterestedInOther.Size = new System.Drawing.Size(52, 17);
            this.InterestedInOther.TabIndex = 150;
            this.InterestedInOther.Text = "Other";
            this.InterestedInOther.UseVisualStyleBackColor = true;
            // 
            // InterestedInHorseRacing
            // 
            this.InterestedInHorseRacing.AutoSize = true;
            this.InterestedInHorseRacing.Location = new System.Drawing.Point(6, 132);
            this.InterestedInHorseRacing.Name = "InterestedInHorseRacing";
            this.InterestedInHorseRacing.Size = new System.Drawing.Size(91, 17);
            this.InterestedInHorseRacing.TabIndex = 140;
            this.InterestedInHorseRacing.Text = "Horse Racing";
            this.InterestedInHorseRacing.UseVisualStyleBackColor = true;
            // 
            // InterestedInBaseball
            // 
            this.InterestedInBaseball.AutoSize = true;
            this.InterestedInBaseball.Location = new System.Drawing.Point(6, 106);
            this.InterestedInBaseball.Name = "InterestedInBaseball";
            this.InterestedInBaseball.Size = new System.Drawing.Size(66, 17);
            this.InterestedInBaseball.TabIndex = 130;
            this.InterestedInBaseball.Text = "Baseball";
            this.InterestedInBaseball.UseVisualStyleBackColor = true;
            // 
            // InterestedInFootball
            // 
            this.InterestedInFootball.AutoSize = true;
            this.InterestedInFootball.Location = new System.Drawing.Point(6, 80);
            this.InterestedInFootball.Name = "InterestedInFootball";
            this.InterestedInFootball.Size = new System.Drawing.Size(63, 17);
            this.InterestedInFootball.TabIndex = 120;
            this.InterestedInFootball.Text = "Football";
            this.InterestedInFootball.UseVisualStyleBackColor = true;
            // 
            // InterestedInHockey
            // 
            this.InterestedInHockey.AutoSize = true;
            this.InterestedInHockey.Location = new System.Drawing.Point(6, 54);
            this.InterestedInHockey.Name = "InterestedInHockey";
            this.InterestedInHockey.Size = new System.Drawing.Size(63, 17);
            this.InterestedInHockey.TabIndex = 110;
            this.InterestedInHockey.Text = "Hockey";
            this.InterestedInHockey.UseVisualStyleBackColor = true;
            // 
            // chbInterestedInBasketBall
            // 
            this.chbInterestedInBasketBall.AutoSize = true;
            this.chbInterestedInBasketBall.Location = new System.Drawing.Point(6, 28);
            this.chbInterestedInBasketBall.Name = "chbInterestedInBasketBall";
            this.chbInterestedInBasketBall.Size = new System.Drawing.Size(75, 17);
            this.chbInterestedInBasketBall.TabIndex = 100;
            this.chbInterestedInBasketBall.Text = "Basketball";
            this.chbInterestedInBasketBall.UseVisualStyleBackColor = true;
            // 
            // grpContactRestrictions
            // 
            this.grpContactRestrictions.Controls.Add(this.chbNoPhone);
            this.grpContactRestrictions.Controls.Add(this.chbNoEmail);
            this.grpContactRestrictions.Controls.Add(this.chbNoMail);
            this.grpContactRestrictions.Location = new System.Drawing.Point(429, 262);
            this.grpContactRestrictions.Name = "grpContactRestrictions";
            this.grpContactRestrictions.Size = new System.Drawing.Size(272, 66);
            this.grpContactRestrictions.TabIndex = 50;
            this.grpContactRestrictions.TabStop = false;
            this.grpContactRestrictions.Text = "ContactRestrictions";
            this.grpContactRestrictions.Visible = false;
            // 
            // chbNoPhone
            // 
            this.chbNoPhone.AutoSize = true;
            this.chbNoPhone.Location = new System.Drawing.Point(179, 31);
            this.chbNoPhone.Name = "chbNoPhone";
            this.chbNoPhone.Size = new System.Drawing.Size(74, 17);
            this.chbNoPhone.TabIndex = 80;
            this.chbNoPhone.Text = "No Phone";
            this.chbNoPhone.UseVisualStyleBackColor = true;
            // 
            // chbNoEmail
            // 
            this.chbNoEmail.AutoSize = true;
            this.chbNoEmail.Location = new System.Drawing.Point(93, 31);
            this.chbNoEmail.Name = "chbNoEmail";
            this.chbNoEmail.Size = new System.Drawing.Size(71, 17);
            this.chbNoEmail.TabIndex = 70;
            this.chbNoEmail.Text = "No E-mail";
            this.chbNoEmail.UseVisualStyleBackColor = true;
            // 
            // chbNoMail
            // 
            this.chbNoMail.AutoSize = true;
            this.chbNoMail.Location = new System.Drawing.Point(7, 31);
            this.chbNoMail.Name = "chbNoMail";
            this.chbNoMail.Size = new System.Drawing.Size(62, 17);
            this.chbNoMail.TabIndex = 60;
            this.chbNoMail.Text = "No Mail";
            this.chbNoMail.UseVisualStyleBackColor = true;
            // 
            // tabpAgent
            // 
            this.tabpAgent.Controls.Add(this.panAgent);
            this.tabpAgent.Location = new System.Drawing.Point(4, 22);
            this.tabpAgent.Name = "tabpAgent";
            this.tabpAgent.Size = new System.Drawing.Size(793, 474);
            this.tabpAgent.TabIndex = 8;
            this.tabpAgent.Text = "Agent";
            this.tabpAgent.UseVisualStyleBackColor = true;
            // 
            // panAgent
            // 
            this.panAgent.Controls.Add(this.panAutoCreateShadeGroups);
            this.panAgent.Controls.Add(this.panFirstAgentSettings);
            this.panAgent.Controls.Add(this.grpInternetCustomerAccess);
            this.panAgent.Controls.Add(this.grpHeadCountRate);
            this.panAgent.Controls.Add(this.grpCommissionType);
            this.panAgent.Controls.Add(this.panMakeUpAndCasinoFee);
            this.panAgent.Controls.Add(this.grpMasterAgent);
            this.panAgent.Controls.Add(this.panSendPackage);
            this.panAgent.Location = new System.Drawing.Point(9, 4);
            this.panAgent.Name = "panAgent";
            this.panAgent.Size = new System.Drawing.Size(741, 461);
            this.panAgent.TabIndex = 0;
            // 
            // panAutoCreateShadeGroups
            // 
            this.panAutoCreateShadeGroups.Controls.Add(this.chbShadeGroupsAutoCreate);
            this.panAutoCreateShadeGroups.Location = new System.Drawing.Point(27, 86);
            this.panAutoCreateShadeGroups.Name = "panAutoCreateShadeGroups";
            this.panAutoCreateShadeGroups.Size = new System.Drawing.Size(200, 23);
            this.panAutoCreateShadeGroups.TabIndex = 40;
            // 
            // chbShadeGroupsAutoCreate
            // 
            this.chbShadeGroupsAutoCreate.AutoSize = true;
            this.chbShadeGroupsAutoCreate.Location = new System.Drawing.Point(6, 3);
            this.chbShadeGroupsAutoCreate.Name = "chbShadeGroupsAutoCreate";
            this.chbShadeGroupsAutoCreate.Size = new System.Drawing.Size(175, 17);
            this.chbShadeGroupsAutoCreate.TabIndex = 50;
            this.chbShadeGroupsAutoCreate.Text = "Auto create Line Shade Groups";
            this.chbShadeGroupsAutoCreate.UseVisualStyleBackColor = true;
            this.chbShadeGroupsAutoCreate.CheckedChanged += new System.EventHandler(this.chbShadeGroupsAutoCreate_CheckedChanged);
            // 
            // panFirstAgentSettings
            // 
            this.panFirstAgentSettings.Controls.Add(this.chbCustomerIsMasterAgent);
            this.panFirstAgentSettings.Controls.Add(this.chbCustomerIsAgent);
            this.panFirstAgentSettings.Location = new System.Drawing.Point(27, 19);
            this.panFirstAgentSettings.Name = "panFirstAgentSettings";
            this.panFirstAgentSettings.Size = new System.Drawing.Size(200, 53);
            this.panFirstAgentSettings.TabIndex = 10;
            // 
            // chbCustomerIsMasterAgent
            // 
            this.chbCustomerIsMasterAgent.AutoSize = true;
            this.chbCustomerIsMasterAgent.Location = new System.Drawing.Point(6, 28);
            this.chbCustomerIsMasterAgent.Name = "chbCustomerIsMasterAgent";
            this.chbCustomerIsMasterAgent.Size = new System.Drawing.Size(190, 17);
            this.chbCustomerIsMasterAgent.TabIndex = 30;
            this.chbCustomerIsMasterAgent.Text = "This customer is a MASTER Agent";
            this.chbCustomerIsMasterAgent.UseVisualStyleBackColor = true;
            this.chbCustomerIsMasterAgent.CheckedChanged += new System.EventHandler(this.chbCustomerIsMasterAgent_CheckedChanged);
            // 
            // chbCustomerIsAgent
            // 
            this.chbCustomerIsAgent.AutoSize = true;
            this.chbCustomerIsAgent.Location = new System.Drawing.Point(6, 4);
            this.chbCustomerIsAgent.Name = "chbCustomerIsAgent";
            this.chbCustomerIsAgent.Size = new System.Drawing.Size(147, 17);
            this.chbCustomerIsAgent.TabIndex = 20;
            this.chbCustomerIsAgent.Text = "This customer is an agent";
            this.chbCustomerIsAgent.UseVisualStyleBackColor = true;
            this.chbCustomerIsAgent.CheckedChanged += new System.EventHandler(this.chbCustomerIsAgent_CheckedChanged);
            // 
            // grpInternetCustomerAccess
            // 
            this.grpInternetCustomerAccess.Controls.Add(this.txtIdPrefix);
            this.grpInternetCustomerAccess.Controls.Add(this.lblIdPrefix);
            this.grpInternetCustomerAccess.Controls.Add(this.chbEnterBettingAdjs);
            this.grpInternetCustomerAccess.Controls.Add(this.chbEnterDepositsWithdrawals);
            this.grpInternetCustomerAccess.Controls.Add(this.chbSetMinimumBetAmt);
            this.grpInternetCustomerAccess.Controls.Add(this.chbChangeSettleFigure);
            this.grpInternetCustomerAccess.Controls.Add(this.chbUpdateComments);
            this.grpInternetCustomerAccess.Controls.Add(this.chbChangeTempCredit);
            this.grpInternetCustomerAccess.Controls.Add(this.chbAddNewAccounts);
            this.grpInternetCustomerAccess.Controls.Add(this.chbChangeWagerLimits);
            this.grpInternetCustomerAccess.Controls.Add(this.chbManageLines);
            this.grpInternetCustomerAccess.Controls.Add(this.chbSuspendWagering);
            this.grpInternetCustomerAccess.Controls.Add(this.chbChangeCreditLimit);
            this.grpInternetCustomerAccess.Location = new System.Drawing.Point(387, 123);
            this.grpInternetCustomerAccess.Name = "grpInternetCustomerAccess";
            this.grpInternetCustomerAccess.Size = new System.Drawing.Size(347, 265);
            this.grpInternetCustomerAccess.TabIndex = 290;
            this.grpInternetCustomerAccess.TabStop = false;
            this.grpInternetCustomerAccess.Text = "Internet Customer Access";
            // 
            // txtIdPrefix
            // 
            this.txtIdPrefix.Location = new System.Drawing.Point(198, 161);
            this.txtIdPrefix.Name = "txtIdPrefix";
            this.txtIdPrefix.Size = new System.Drawing.Size(50, 20);
            this.txtIdPrefix.TabIndex = 390;
            // 
            // lblIdPrefix
            // 
            this.lblIdPrefix.AutoSize = true;
            this.lblIdPrefix.Location = new System.Drawing.Point(152, 164);
            this.lblIdPrefix.Name = "lblIdPrefix";
            this.lblIdPrefix.Size = new System.Drawing.Size(48, 13);
            this.lblIdPrefix.TabIndex = 15;
            this.lblIdPrefix.Text = "Id Prefix:";
            // 
            // chbEnterBettingAdjs
            // 
            this.chbEnterBettingAdjs.AutoSize = true;
            this.chbEnterBettingAdjs.Location = new System.Drawing.Point(119, 229);
            this.chbEnterBettingAdjs.Name = "chbEnterBettingAdjs";
            this.chbEnterBettingAdjs.Size = new System.Drawing.Size(110, 17);
            this.chbEnterBettingAdjs.TabIndex = 410;
            this.chbEnterBettingAdjs.Text = "Enter Betting Adjs";
            this.chbEnterBettingAdjs.UseVisualStyleBackColor = true;
            // 
            // chbEnterDepositsWithdrawals
            // 
            this.chbEnterDepositsWithdrawals.AutoSize = true;
            this.chbEnterDepositsWithdrawals.Location = new System.Drawing.Point(119, 202);
            this.chbEnterDepositsWithdrawals.Name = "chbEnterDepositsWithdrawals";
            this.chbEnterDepositsWithdrawals.Size = new System.Drawing.Size(158, 17);
            this.chbEnterDepositsWithdrawals.TabIndex = 400;
            this.chbEnterDepositsWithdrawals.Text = "Enter Deposits/Withdrawals";
            this.chbEnterDepositsWithdrawals.UseVisualStyleBackColor = true;
            // 
            // chbSetMinimumBetAmt
            // 
            this.chbSetMinimumBetAmt.AutoSize = true;
            this.chbSetMinimumBetAmt.Location = new System.Drawing.Point(185, 128);
            this.chbSetMinimumBetAmt.Name = "chbSetMinimumBetAmt";
            this.chbSetMinimumBetAmt.Size = new System.Drawing.Size(126, 17);
            this.chbSetMinimumBetAmt.TabIndex = 370;
            this.chbSetMinimumBetAmt.Text = "Set Minimum Bet Amt";
            this.chbSetMinimumBetAmt.UseVisualStyleBackColor = true;
            // 
            // chbChangeSettleFigure
            // 
            this.chbChangeSettleFigure.AutoSize = true;
            this.chbChangeSettleFigure.Location = new System.Drawing.Point(185, 97);
            this.chbChangeSettleFigure.Name = "chbChangeSettleFigure";
            this.chbChangeSettleFigure.Size = new System.Drawing.Size(125, 17);
            this.chbChangeSettleFigure.TabIndex = 350;
            this.chbChangeSettleFigure.Text = "Change Settle Figure";
            this.chbChangeSettleFigure.UseVisualStyleBackColor = true;
            // 
            // chbUpdateComments
            // 
            this.chbUpdateComments.AutoSize = true;
            this.chbUpdateComments.Location = new System.Drawing.Point(185, 66);
            this.chbUpdateComments.Name = "chbUpdateComments";
            this.chbUpdateComments.Size = new System.Drawing.Size(113, 17);
            this.chbUpdateComments.TabIndex = 330;
            this.chbUpdateComments.Text = "Update Comments";
            this.chbUpdateComments.UseVisualStyleBackColor = true;
            // 
            // chbChangeTempCredit
            // 
            this.chbChangeTempCredit.AutoSize = true;
            this.chbChangeTempCredit.Location = new System.Drawing.Point(185, 35);
            this.chbChangeTempCredit.Name = "chbChangeTempCredit";
            this.chbChangeTempCredit.Size = new System.Drawing.Size(147, 17);
            this.chbChangeTempCredit.TabIndex = 310;
            this.chbChangeTempCredit.Text = "Change Temp Credit Limit";
            this.chbChangeTempCredit.UseVisualStyleBackColor = true;
            // 
            // chbAddNewAccounts
            // 
            this.chbAddNewAccounts.AutoSize = true;
            this.chbAddNewAccounts.Location = new System.Drawing.Point(9, 163);
            this.chbAddNewAccounts.Name = "chbAddNewAccounts";
            this.chbAddNewAccounts.Size = new System.Drawing.Size(118, 17);
            this.chbAddNewAccounts.TabIndex = 380;
            this.chbAddNewAccounts.Text = "Add New Accounts";
            this.chbAddNewAccounts.UseVisualStyleBackColor = true;
            // 
            // chbChangeWagerLimits
            // 
            this.chbChangeWagerLimits.AutoSize = true;
            this.chbChangeWagerLimits.Location = new System.Drawing.Point(9, 131);
            this.chbChangeWagerLimits.Name = "chbChangeWagerLimits";
            this.chbChangeWagerLimits.Size = new System.Drawing.Size(127, 17);
            this.chbChangeWagerLimits.TabIndex = 360;
            this.chbChangeWagerLimits.Text = "Change Wager Limits";
            this.chbChangeWagerLimits.UseVisualStyleBackColor = true;
            // 
            // chbManageLines
            // 
            this.chbManageLines.AutoSize = true;
            this.chbManageLines.Location = new System.Drawing.Point(9, 99);
            this.chbManageLines.Name = "chbManageLines";
            this.chbManageLines.Size = new System.Drawing.Size(93, 17);
            this.chbManageLines.TabIndex = 340;
            this.chbManageLines.Text = "Manage Lines";
            this.chbManageLines.UseVisualStyleBackColor = true;
            // 
            // chbSuspendWagering
            // 
            this.chbSuspendWagering.AutoSize = true;
            this.chbSuspendWagering.Location = new System.Drawing.Point(9, 67);
            this.chbSuspendWagering.Name = "chbSuspendWagering";
            this.chbSuspendWagering.Size = new System.Drawing.Size(117, 17);
            this.chbSuspendWagering.TabIndex = 320;
            this.chbSuspendWagering.Text = "Suspend Wagering";
            this.chbSuspendWagering.UseVisualStyleBackColor = true;
            // 
            // chbChangeCreditLimit
            // 
            this.chbChangeCreditLimit.AutoSize = true;
            this.chbChangeCreditLimit.Location = new System.Drawing.Point(9, 35);
            this.chbChangeCreditLimit.Name = "chbChangeCreditLimit";
            this.chbChangeCreditLimit.Size = new System.Drawing.Size(117, 17);
            this.chbChangeCreditLimit.TabIndex = 300;
            this.chbChangeCreditLimit.Text = "Change Credit Limit";
            this.chbChangeCreditLimit.UseVisualStyleBackColor = true;
            // 
            // grpHeadCountRate
            // 
            this.grpHeadCountRate.Controls.Add(this.txtInternetOnly);
            this.grpHeadCountRate.Controls.Add(this.txtCasino);
            this.grpHeadCountRate.Controls.Add(this.txtCallUnit);
            this.grpHeadCountRate.Controls.Add(this.lblInternetOnly);
            this.grpHeadCountRate.Controls.Add(this.lblCasino);
            this.grpHeadCountRate.Controls.Add(this.lblCallUnit);
            this.grpHeadCountRate.Location = new System.Drawing.Point(387, 32);
            this.grpHeadCountRate.Name = "grpHeadCountRate";
            this.grpHeadCountRate.Size = new System.Drawing.Size(347, 77);
            this.grpHeadCountRate.TabIndex = 250;
            this.grpHeadCountRate.TabStop = false;
            this.grpHeadCountRate.Text = "Head Count Rate";
            // 
            // txtInternetOnly
            // 
            this.txtInternetOnly.AllowCents = false;
            this.txtInternetOnly.AllowNegatives = false;
            this.txtInternetOnly.DividedBy = 1;
            this.txtInternetOnly.DivideFlag = false;
            this.txtInternetOnly.Location = new System.Drawing.Point(271, 20);
            this.txtInternetOnly.Name = "txtInternetOnly";
            this.txtInternetOnly.ShowIfZero = true;
            this.txtInternetOnly.Size = new System.Drawing.Size(61, 20);
            this.txtInternetOnly.TabIndex = 270;
            this.txtInternetOnly.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtInternetOnly.Leave += new System.EventHandler(this.numberToText_Leave);
            // 
            // txtCasino
            // 
            this.txtCasino.AllowCents = false;
            this.txtCasino.AllowNegatives = false;
            this.txtCasino.DividedBy = 1;
            this.txtCasino.DivideFlag = false;
            this.txtCasino.Location = new System.Drawing.Point(61, 45);
            this.txtCasino.Name = "txtCasino";
            this.txtCasino.ShowIfZero = true;
            this.txtCasino.Size = new System.Drawing.Size(61, 20);
            this.txtCasino.TabIndex = 280;
            this.txtCasino.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCasino.Leave += new System.EventHandler(this.numberToText_Leave);
            // 
            // txtCallUnit
            // 
            this.txtCallUnit.AllowCents = false;
            this.txtCallUnit.AllowNegatives = false;
            this.txtCallUnit.DividedBy = 1;
            this.txtCallUnit.DivideFlag = false;
            this.txtCallUnit.Location = new System.Drawing.Point(61, 19);
            this.txtCallUnit.Name = "txtCallUnit";
            this.txtCallUnit.ShowIfZero = true;
            this.txtCallUnit.Size = new System.Drawing.Size(61, 20);
            this.txtCallUnit.TabIndex = 260;
            this.txtCallUnit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCallUnit.Leave += new System.EventHandler(this.numberToText_Leave);
            // 
            // lblInternetOnly
            // 
            this.lblInternetOnly.AutoSize = true;
            this.lblInternetOnly.Location = new System.Drawing.Point(195, 27);
            this.lblInternetOnly.Name = "lblInternetOnly";
            this.lblInternetOnly.Size = new System.Drawing.Size(70, 13);
            this.lblInternetOnly.TabIndex = 17;
            this.lblInternetOnly.Text = "Internet Only:";
            // 
            // lblCasino
            // 
            this.lblCasino.AutoSize = true;
            this.lblCasino.Location = new System.Drawing.Point(6, 48);
            this.lblCasino.Name = "lblCasino";
            this.lblCasino.Size = new System.Drawing.Size(42, 13);
            this.lblCasino.TabIndex = 16;
            this.lblCasino.Text = "Casino:";
            // 
            // lblCallUnit
            // 
            this.lblCallUnit.AutoSize = true;
            this.lblCallUnit.Location = new System.Drawing.Point(6, 27);
            this.lblCallUnit.Name = "lblCallUnit";
            this.lblCallUnit.Size = new System.Drawing.Size(49, 13);
            this.lblCallUnit.TabIndex = 15;
            this.lblCallUnit.Text = "Call Unit:";
            // 
            // grpCommissionType
            // 
            this.grpCommissionType.Controls.Add(this.panAffiliate);
            this.grpCommissionType.Controls.Add(this.chbIncludeFreePlayLossesinDistribution);
            this.grpCommissionType.Controls.Add(this.lblAgentCommission);
            this.grpCommissionType.Controls.Add(this.txtAgentCommision);
            this.grpCommissionType.Location = new System.Drawing.Point(15, 266);
            this.grpCommissionType.Name = "grpCommissionType";
            this.grpCommissionType.Size = new System.Drawing.Size(356, 151);
            this.grpCommissionType.TabIndex = 150;
            this.grpCommissionType.TabStop = false;
            this.grpCommissionType.Text = "Commission Type";
            // 
            // panAffiliate
            // 
            this.panAffiliate.Controls.Add(this.rdoAgentWeeklyProfit);
            this.panAffiliate.Controls.Add(this.rdoAgentSplit);
            this.panAffiliate.Controls.Add(this.rdoAffiliateWeeklyProfit);
            this.panAffiliate.Controls.Add(this.rdoAgentRedFigure);
            this.panAffiliate.Controls.Add(this.rdoAffiliateSplit);
            this.panAffiliate.Controls.Add(this.rdoAffiliateRedFigure);
            this.panAffiliate.Location = new System.Drawing.Point(9, 19);
            this.panAffiliate.Name = "panAffiliate";
            this.panAffiliate.Size = new System.Drawing.Size(325, 77);
            this.panAffiliate.TabIndex = 160;
            // 
            // rdoAgentWeeklyProfit
            // 
            this.rdoAgentWeeklyProfit.AutoSize = true;
            this.rdoAgentWeeklyProfit.Location = new System.Drawing.Point(193, 3);
            this.rdoAgentWeeklyProfit.Name = "rdoAgentWeeklyProfit";
            this.rdoAgentWeeklyProfit.Size = new System.Drawing.Size(119, 17);
            this.rdoAgentWeeklyProfit.TabIndex = 200;
            this.rdoAgentWeeklyProfit.Text = "Agent Weekly Profit";
            this.rdoAgentWeeklyProfit.UseVisualStyleBackColor = true;
            // 
            // rdoAgentSplit
            // 
            this.rdoAgentSplit.AutoSize = true;
            this.rdoAgentSplit.Location = new System.Drawing.Point(193, 26);
            this.rdoAgentSplit.Name = "rdoAgentSplit";
            this.rdoAgentSplit.Size = new System.Drawing.Size(76, 17);
            this.rdoAgentSplit.TabIndex = 210;
            this.rdoAgentSplit.Text = "Agent Split";
            this.rdoAgentSplit.UseVisualStyleBackColor = true;
            // 
            // rdoAffiliateWeeklyProfit
            // 
            this.rdoAffiliateWeeklyProfit.AutoSize = true;
            this.rdoAffiliateWeeklyProfit.Location = new System.Drawing.Point(3, 3);
            this.rdoAffiliateWeeklyProfit.Name = "rdoAffiliateWeeklyProfit";
            this.rdoAffiliateWeeklyProfit.Size = new System.Drawing.Size(125, 17);
            this.rdoAffiliateWeeklyProfit.TabIndex = 170;
            this.rdoAffiliateWeeklyProfit.Text = "Affiliate Weekly Profit";
            this.rdoAffiliateWeeklyProfit.UseVisualStyleBackColor = true;
            // 
            // rdoAgentRedFigure
            // 
            this.rdoAgentRedFigure.AutoSize = true;
            this.rdoAgentRedFigure.Location = new System.Drawing.Point(193, 49);
            this.rdoAgentRedFigure.Name = "rdoAgentRedFigure";
            this.rdoAgentRedFigure.Size = new System.Drawing.Size(108, 17);
            this.rdoAgentRedFigure.TabIndex = 220;
            this.rdoAgentRedFigure.Text = "Agent Red Figure";
            this.rdoAgentRedFigure.UseVisualStyleBackColor = true;
            // 
            // rdoAffiliateSplit
            // 
            this.rdoAffiliateSplit.AutoSize = true;
            this.rdoAffiliateSplit.Location = new System.Drawing.Point(3, 26);
            this.rdoAffiliateSplit.Name = "rdoAffiliateSplit";
            this.rdoAffiliateSplit.Size = new System.Drawing.Size(82, 17);
            this.rdoAffiliateSplit.TabIndex = 180;
            this.rdoAffiliateSplit.Text = "Affiliate Split";
            this.rdoAffiliateSplit.UseVisualStyleBackColor = true;
            // 
            // rdoAffiliateRedFigure
            // 
            this.rdoAffiliateRedFigure.AutoSize = true;
            this.rdoAffiliateRedFigure.Location = new System.Drawing.Point(3, 49);
            this.rdoAffiliateRedFigure.Name = "rdoAffiliateRedFigure";
            this.rdoAffiliateRedFigure.Size = new System.Drawing.Size(114, 17);
            this.rdoAffiliateRedFigure.TabIndex = 190;
            this.rdoAffiliateRedFigure.Text = "Affiliate Red Figure";
            this.rdoAffiliateRedFigure.UseVisualStyleBackColor = true;
            // 
            // chbIncludeFreePlayLossesinDistribution
            // 
            this.chbIncludeFreePlayLossesinDistribution.AutoSize = true;
            this.chbIncludeFreePlayLossesinDistribution.Location = new System.Drawing.Point(9, 128);
            this.chbIncludeFreePlayLossesinDistribution.Name = "chbIncludeFreePlayLossesinDistribution";
            this.chbIncludeFreePlayLossesinDistribution.Size = new System.Drawing.Size(210, 17);
            this.chbIncludeFreePlayLossesinDistribution.TabIndex = 240;
            this.chbIncludeFreePlayLossesinDistribution.Text = "Include Free Play Losses in Distribution";
            this.chbIncludeFreePlayLossesinDistribution.UseVisualStyleBackColor = true;
            // 
            // lblAgentCommission
            // 
            this.lblAgentCommission.AutoSize = true;
            this.lblAgentCommission.Location = new System.Drawing.Point(19, 105);
            this.lblAgentCommission.Name = "lblAgentCommission";
            this.lblAgentCommission.Size = new System.Drawing.Size(107, 13);
            this.lblAgentCommission.TabIndex = 15;
            this.lblAgentCommission.Text = "Agent Commission %:";
            // 
            // txtAgentCommision
            // 
            this.txtAgentCommision.AllowCents = false;
            this.txtAgentCommision.AllowNegatives = false;
            this.txtAgentCommision.DividedBy = 1;
            this.txtAgentCommision.DivideFlag = false;
            this.txtAgentCommision.Location = new System.Drawing.Point(130, 102);
            this.txtAgentCommision.Name = "txtAgentCommision";
            this.txtAgentCommision.ShowIfZero = true;
            this.txtAgentCommision.Size = new System.Drawing.Size(39, 20);
            this.txtAgentCommision.TabIndex = 230;
            this.txtAgentCommision.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAgentCommision.Leave += new System.EventHandler(this.numberToText_Leave);
            // 
            // panMakeUpAndCasinoFee
            // 
            this.panMakeUpAndCasinoFee.Controls.Add(this.lblPercentOfLosses);
            this.panMakeUpAndCasinoFee.Controls.Add(this.lblCasinoFee);
            this.panMakeUpAndCasinoFee.Controls.Add(this.btnChangeMakeup);
            this.panMakeUpAndCasinoFee.Controls.Add(this.lblCurrentMakeup);
            this.panMakeUpAndCasinoFee.Controls.Add(this.txtCasinoFee);
            this.panMakeUpAndCasinoFee.Controls.Add(this.txtCurrentMakeup);
            this.panMakeUpAndCasinoFee.Location = new System.Drawing.Point(15, 189);
            this.panMakeUpAndCasinoFee.Name = "panMakeUpAndCasinoFee";
            this.panMakeUpAndCasinoFee.Size = new System.Drawing.Size(356, 71);
            this.panMakeUpAndCasinoFee.TabIndex = 110;
            // 
            // lblPercentOfLosses
            // 
            this.lblPercentOfLosses.AutoSize = true;
            this.lblPercentOfLosses.Location = new System.Drawing.Point(140, 38);
            this.lblPercentOfLosses.Name = "lblPercentOfLosses";
            this.lblPercentOfLosses.Size = new System.Drawing.Size(111, 13);
            this.lblPercentOfLosses.TabIndex = 14;
            this.lblPercentOfLosses.Text = "(percent of net losses)";
            // 
            // lblCasinoFee
            // 
            this.lblCasinoFee.AutoSize = true;
            this.lblCasinoFee.Location = new System.Drawing.Point(9, 38);
            this.lblCasinoFee.Name = "lblCasinoFee";
            this.lblCasinoFee.Size = new System.Drawing.Size(74, 13);
            this.lblCasinoFee.TabIndex = 12;
            this.lblCasinoFee.Text = "Casino Fee %:";
            // 
            // btnChangeMakeup
            // 
            this.btnChangeMakeup.Location = new System.Drawing.Point(202, 8);
            this.btnChangeMakeup.Name = "btnChangeMakeup";
            this.btnChangeMakeup.Size = new System.Drawing.Size(60, 24);
            this.btnChangeMakeup.TabIndex = 130;
            this.btnChangeMakeup.Text = "Change...";
            this.btnChangeMakeup.UseVisualStyleBackColor = true;
            this.btnChangeMakeup.Click += new System.EventHandler(this.btnChangeMakeup_Click);
            // 
            // lblCurrentMakeup
            // 
            this.lblCurrentMakeup.AutoSize = true;
            this.lblCurrentMakeup.Location = new System.Drawing.Point(6, 13);
            this.lblCurrentMakeup.Name = "lblCurrentMakeup";
            this.lblCurrentMakeup.Size = new System.Drawing.Size(83, 13);
            this.lblCurrentMakeup.TabIndex = 0;
            this.lblCurrentMakeup.Text = "Current Makeup";
            // 
            // txtCasinoFee
            // 
            this.txtCasinoFee.AllowCents = false;
            this.txtCasinoFee.AllowNegatives = false;
            this.txtCasinoFee.DividedBy = 1;
            this.txtCasinoFee.DivideFlag = false;
            this.txtCasinoFee.Location = new System.Drawing.Point(95, 35);
            this.txtCasinoFee.Name = "txtCasinoFee";
            this.txtCasinoFee.ShowIfZero = true;
            this.txtCasinoFee.Size = new System.Drawing.Size(39, 20);
            this.txtCasinoFee.TabIndex = 140;
            this.txtCasinoFee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCasinoFee.Leave += new System.EventHandler(this.numberToText_Leave);
            // 
            // txtCurrentMakeup
            // 
            this.txtCurrentMakeup.AllowCents = false;
            this.txtCurrentMakeup.AllowNegatives = false;
            this.txtCurrentMakeup.DividedBy = 100;
            this.txtCurrentMakeup.DivideFlag = true;
            this.txtCurrentMakeup.Enabled = false;
            this.txtCurrentMakeup.Location = new System.Drawing.Point(95, 9);
            this.txtCurrentMakeup.Name = "txtCurrentMakeup";
            this.txtCurrentMakeup.ShowIfZero = true;
            this.txtCurrentMakeup.Size = new System.Drawing.Size(97, 20);
            this.txtCurrentMakeup.TabIndex = 120;
            this.txtCurrentMakeup.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCurrentMakeup.Leave += new System.EventHandler(this.numberToText_Leave);
            // 
            // grpMasterAgent
            // 
            this.grpMasterAgent.Controls.Add(this.chbDistributeFunds);
            this.grpMasterAgent.Controls.Add(this.panMasterAgentDropDown);
            this.grpMasterAgent.Location = new System.Drawing.Point(15, 123);
            this.grpMasterAgent.Name = "grpMasterAgent";
            this.grpMasterAgent.Size = new System.Drawing.Size(356, 60);
            this.grpMasterAgent.TabIndex = 80;
            this.grpMasterAgent.TabStop = false;
            this.grpMasterAgent.Text = "Master Agent";
            // 
            // chbDistributeFunds
            // 
            this.chbDistributeFunds.AutoSize = true;
            this.chbDistributeFunds.Location = new System.Drawing.Point(185, 19);
            this.chbDistributeFunds.Name = "chbDistributeFunds";
            this.chbDistributeFunds.Size = new System.Drawing.Size(149, 17);
            this.chbDistributeFunds.TabIndex = 100;
            this.chbDistributeFunds.Text = "Distribute Funds to Master";
            this.chbDistributeFunds.UseVisualStyleBackColor = true;
            // 
            // panMasterAgentDropDown
            // 
            this.panMasterAgentDropDown.Location = new System.Drawing.Point(6, 19);
            this.panMasterAgentDropDown.Name = "panMasterAgentDropDown";
            this.panMasterAgentDropDown.Size = new System.Drawing.Size(173, 33);
            this.panMasterAgentDropDown.TabIndex = 90;
            // 
            // panSendPackage
            // 
            this.panSendPackage.Controls.Add(this.btnSavePackageToFile);
            this.panSendPackage.Controls.Add(this.btnSendPackage);
            this.panSendPackage.Location = new System.Drawing.Point(253, 19);
            this.panSendPackage.Name = "panSendPackage";
            this.panSendPackage.Size = new System.Drawing.Size(118, 98);
            this.panSendPackage.TabIndex = 60;
            // 
            // btnSavePackageToFile
            // 
            this.btnSavePackageToFile.Location = new System.Drawing.Point(3, 51);
            this.btnSavePackageToFile.Name = "btnSavePackageToFile";
            this.btnSavePackageToFile.Size = new System.Drawing.Size(112, 41);
            this.btnSavePackageToFile.TabIndex = 71;
            this.btnSavePackageToFile.Text = "Save package to file";
            this.btnSavePackageToFile.UseVisualStyleBackColor = true;
            this.btnSavePackageToFile.Click += new System.EventHandler(this.btnSavePackageToFile_Click);
            // 
            // btnSendPackage
            // 
            this.btnSendPackage.Location = new System.Drawing.Point(3, 4);
            this.btnSendPackage.Name = "btnSendPackage";
            this.btnSendPackage.Size = new System.Drawing.Size(112, 41);
            this.btnSendPackage.TabIndex = 70;
            this.btnSendPackage.Text = "Send Package To Customers...";
            this.btnSendPackage.UseVisualStyleBackColor = true;
            this.btnSendPackage.Click += new System.EventHandler(this.btnSendPackage_Click);
            // 
            // tabpWagerLimits
            // 
            this.tabpWagerLimits.Controls.Add(this.panWagerLimits);
            this.tabpWagerLimits.Location = new System.Drawing.Point(4, 22);
            this.tabpWagerLimits.Name = "tabpWagerLimits";
            this.tabpWagerLimits.Size = new System.Drawing.Size(793, 474);
            this.tabpWagerLimits.TabIndex = 9;
            this.tabpWagerLimits.Text = "Wager Limits";
            this.tabpWagerLimits.UseVisualStyleBackColor = true;
            // 
            // panWagerLimits
            // 
            this.panWagerLimits.Controls.Add(this.lblDisabledSportsCount);
            this.panWagerLimits.Controls.Add(this.btnResetWagerLimits);
            this.panWagerLimits.Controls.Add(this.btnShowAuditWagerLimitsN);
            this.panWagerLimits.Controls.Add(this.btnShowAuditWagerLimits);
            this.panWagerLimits.Controls.Add(this.cmbWLSports);
            this.panWagerLimits.Controls.Add(this.grpMiscellaneous);
            this.panWagerLimits.Controls.Add(this.panImportant);
            this.panWagerLimits.Controls.Add(this.grpHorseLimits);
            this.panWagerLimits.Controls.Add(this.grpParlayLimits);
            this.panWagerLimits.Controls.Add(this.panWagerLimitsDetails);
            this.panWagerLimits.Controls.Add(this.grpEnforceWagerLimits);
            this.panWagerLimits.Location = new System.Drawing.Point(4, 0);
            this.panWagerLimits.Name = "panWagerLimits";
            this.panWagerLimits.Size = new System.Drawing.Size(786, 473);
            this.panWagerLimits.TabIndex = 0;
            // 
            // lblDisabledSportsCount
            // 
            this.lblDisabledSportsCount.AutoSize = true;
            this.lblDisabledSportsCount.Location = new System.Drawing.Point(96, 15);
            this.lblDisabledSportsCount.Name = "lblDisabledSportsCount";
            this.lblDisabledSportsCount.Size = new System.Drawing.Size(118, 13);
            this.lblDisabledSportsCount.TabIndex = 135;
            this.lblDisabledSportsCount.Text = "Disabled Sports Cnt = 0";
            // 
            // btnResetWagerLimits
            // 
            this.btnResetWagerLimits.Location = new System.Drawing.Point(14, 6);
            this.btnResetWagerLimits.Name = "btnResetWagerLimits";
            this.btnResetWagerLimits.Size = new System.Drawing.Size(75, 23);
            this.btnResetWagerLimits.TabIndex = 134;
            this.btnResetWagerLimits.Text = "Reset Limits";
            this.btnResetWagerLimits.UseVisualStyleBackColor = true;
            this.btnResetWagerLimits.Click += new System.EventHandler(this.btnResetWagerLimits_Click);
            // 
            // btnShowAuditWagerLimitsN
            // 
            this.btnShowAuditWagerLimitsN.Location = new System.Drawing.Point(659, 35);
            this.btnShowAuditWagerLimitsN.Name = "btnShowAuditWagerLimitsN";
            this.btnShowAuditWagerLimitsN.Size = new System.Drawing.Size(75, 23);
            this.btnShowAuditWagerLimitsN.TabIndex = 133;
            this.btnShowAuditWagerLimitsN.Text = "History...";
            this.btnShowAuditWagerLimitsN.UseVisualStyleBackColor = true;
            this.btnShowAuditWagerLimitsN.Visible = false;
            this.btnShowAuditWagerLimitsN.Click += new System.EventHandler(this.btnShowAuditWagerLimitsN_Click);
            // 
            // btnShowAuditWagerLimits
            // 
            this.btnShowAuditWagerLimits.Location = new System.Drawing.Point(570, 35);
            this.btnShowAuditWagerLimits.Name = "btnShowAuditWagerLimits";
            this.btnShowAuditWagerLimits.Size = new System.Drawing.Size(75, 23);
            this.btnShowAuditWagerLimits.TabIndex = 132;
            this.btnShowAuditWagerLimits.Text = "History...";
            this.btnShowAuditWagerLimits.UseVisualStyleBackColor = true;
            this.btnShowAuditWagerLimits.Click += new System.EventHandler(this.btnShowAuditWagerLimits_Click);
            // 
            // cmbWLSports
            // 
            this.cmbWLSports.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbWLSports.FormattingEnabled = true;
            this.cmbWLSports.Location = new System.Drawing.Point(14, 35);
            this.cmbWLSports.Name = "cmbWLSports";
            this.cmbWLSports.Size = new System.Drawing.Size(145, 21);
            this.cmbWLSports.TabIndex = 131;
            this.cmbWLSports.SelectedIndexChanged += new System.EventHandler(this.cmbWLSports_SelectedIndexChanged);
            // 
            // grpMiscellaneous
            // 
            this.grpMiscellaneous.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.grpMiscellaneous.Controls.Add(this.chbEnforceTeaserBetLimit);
            this.grpMiscellaneous.Controls.Add(this.btnTeaserLimits);
            this.grpMiscellaneous.Controls.Add(this.btnShowAuditTeaserMaxBet);
            this.grpMiscellaneous.Controls.Add(this.txtMaxTeaserBet);
            this.grpMiscellaneous.Controls.Add(this.btnShowAuditContestMaxBet);
            this.grpMiscellaneous.Controls.Add(this.txtMaxPropBet);
            this.grpMiscellaneous.Controls.Add(this.lblMaxTeaserBet);
            this.grpMiscellaneous.Controls.Add(this.lblMaxPropBet);
            this.grpMiscellaneous.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.grpMiscellaneous.Location = new System.Drawing.Point(481, 372);
            this.grpMiscellaneous.Name = "grpMiscellaneous";
            this.grpMiscellaneous.Size = new System.Drawing.Size(253, 97);
            this.grpMiscellaneous.TabIndex = 130;
            this.grpMiscellaneous.TabStop = false;
            // 
            // chbEnforceTeaserBetLimit
            // 
            this.chbEnforceTeaserBetLimit.AccessibleDescription = "Teasers";
            this.chbEnforceTeaserBetLimit.AutoSize = true;
            this.chbEnforceTeaserBetLimit.Location = new System.Drawing.Point(26, 60);
            this.chbEnforceTeaserBetLimit.Name = "chbEnforceTeaserBetLimit";
            this.chbEnforceTeaserBetLimit.Size = new System.Drawing.Size(87, 17);
            this.chbEnforceTeaserBetLimit.TabIndex = 103;
            this.chbEnforceTeaserBetLimit.Text = "Enforce Limit";
            this.chbEnforceTeaserBetLimit.UseVisualStyleBackColor = true;
            this.chbEnforceTeaserBetLimit.CheckedChanged += new System.EventHandler(this.chbEnforceTeaserBetLimit_CheckedChanged);
            // 
            // btnTeaserLimits
            // 
            this.btnTeaserLimits.AccessibleName = "T";
            this.btnTeaserLimits.Location = new System.Drawing.Point(119, 56);
            this.btnTeaserLimits.Name = "btnTeaserLimits";
            this.btnTeaserLimits.Size = new System.Drawing.Size(100, 23);
            this.btnTeaserLimits.TabIndex = 102;
            this.btnTeaserLimits.Text = "Detailed Limits";
            this.btnTeaserLimits.UseVisualStyleBackColor = true;
            this.btnTeaserLimits.Click += new System.EventHandler(this.btnTeaserLimits_Click);
            // 
            // btnShowAuditTeaserMaxBet
            // 
            this.btnShowAuditTeaserMaxBet.Location = new System.Drawing.Point(224, 32);
            this.btnShowAuditTeaserMaxBet.Name = "btnShowAuditTeaserMaxBet";
            this.btnShowAuditTeaserMaxBet.Size = new System.Drawing.Size(14, 23);
            this.btnShowAuditTeaserMaxBet.TabIndex = 84;
            this.btnShowAuditTeaserMaxBet.Text = "?";
            this.btnShowAuditTeaserMaxBet.UseVisualStyleBackColor = true;
            this.btnShowAuditTeaserMaxBet.Click += new System.EventHandler(this.btnShowAuditTeaserMaxBet_Click);
            // 
            // txtMaxTeaserBet
            // 
            this.txtMaxTeaserBet.AllowCents = false;
            this.txtMaxTeaserBet.AllowNegatives = false;
            this.txtMaxTeaserBet.DividedBy = 1;
            this.txtMaxTeaserBet.DivideFlag = false;
            this.txtMaxTeaserBet.Location = new System.Drawing.Point(119, 34);
            this.txtMaxTeaserBet.Name = "txtMaxTeaserBet";
            this.txtMaxTeaserBet.ShowIfZero = true;
            this.txtMaxTeaserBet.Size = new System.Drawing.Size(100, 20);
            this.txtMaxTeaserBet.TabIndex = 170;
            this.txtMaxTeaserBet.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMaxTeaserBet.Leave += new System.EventHandler(this.numberToText_Leave);
            // 
            // btnShowAuditContestMaxBet
            // 
            this.btnShowAuditContestMaxBet.Location = new System.Drawing.Point(224, 9);
            this.btnShowAuditContestMaxBet.Name = "btnShowAuditContestMaxBet";
            this.btnShowAuditContestMaxBet.Size = new System.Drawing.Size(14, 23);
            this.btnShowAuditContestMaxBet.TabIndex = 83;
            this.btnShowAuditContestMaxBet.Text = "?";
            this.btnShowAuditContestMaxBet.UseVisualStyleBackColor = true;
            this.btnShowAuditContestMaxBet.Click += new System.EventHandler(this.btnShowAuditContestMaxBet_Click);
            // 
            // txtMaxPropBet
            // 
            this.txtMaxPropBet.AllowCents = false;
            this.txtMaxPropBet.AllowNegatives = false;
            this.txtMaxPropBet.DividedBy = 1;
            this.txtMaxPropBet.DivideFlag = false;
            this.txtMaxPropBet.Location = new System.Drawing.Point(119, 10);
            this.txtMaxPropBet.Name = "txtMaxPropBet";
            this.txtMaxPropBet.ShowIfZero = true;
            this.txtMaxPropBet.Size = new System.Drawing.Size(100, 20);
            this.txtMaxPropBet.TabIndex = 150;
            this.txtMaxPropBet.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMaxPropBet.Leave += new System.EventHandler(this.numberToText_Leave);
            // 
            // lblMaxTeaserBet
            // 
            this.lblMaxTeaserBet.AutoSize = true;
            this.lblMaxTeaserBet.Location = new System.Drawing.Point(31, 35);
            this.lblMaxTeaserBet.Name = "lblMaxTeaserBet";
            this.lblMaxTeaserBet.Size = new System.Drawing.Size(85, 13);
            this.lblMaxTeaserBet.TabIndex = 160;
            this.lblMaxTeaserBet.Text = "Max Teaser Bet:";
            this.lblMaxTeaserBet.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblMaxPropBet
            // 
            this.lblMaxPropBet.AutoSize = true;
            this.lblMaxPropBet.Location = new System.Drawing.Point(8, 14);
            this.lblMaxPropBet.Name = "lblMaxPropBet";
            this.lblMaxPropBet.Size = new System.Drawing.Size(109, 13);
            this.lblMaxPropBet.TabIndex = 140;
            this.lblMaxPropBet.Text = "Max Prop/Future Bet:";
            this.lblMaxPropBet.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panImportant
            // 
            this.panImportant.Controls.Add(this.lblImportant);
            this.panImportant.Location = new System.Drawing.Point(510, 344);
            this.panImportant.Name = "panImportant";
            this.panImportant.Size = new System.Drawing.Size(224, 26);
            this.panImportant.TabIndex = 110;
            // 
            // lblImportant
            // 
            this.lblImportant.AutoSize = true;
            this.lblImportant.Location = new System.Drawing.Point(3, 6);
            this.lblImportant.Name = "lblImportant";
            this.lblImportant.Size = new System.Drawing.Size(215, 13);
            this.lblImportant.TabIndex = 120;
            this.lblImportant.Text = "IMPORTANT: If \"0\" then Store Limit Applies";
            // 
            // grpHorseLimits
            // 
            this.grpHorseLimits.Controls.Add(this.btnHorses);
            this.grpHorseLimits.Location = new System.Drawing.Point(246, 345);
            this.grpHorseLimits.Name = "grpHorseLimits";
            this.grpHorseLimits.Size = new System.Drawing.Size(200, 102);
            this.grpHorseLimits.TabIndex = 90;
            this.grpHorseLimits.TabStop = false;
            this.grpHorseLimits.Text = "Horse Limits / Track Restrictions";
            this.grpHorseLimits.Visible = false;
            // 
            // btnHorses
            // 
            this.btnHorses.Location = new System.Drawing.Point(63, 40);
            this.btnHorses.Name = "btnHorses";
            this.btnHorses.Size = new System.Drawing.Size(75, 23);
            this.btnHorses.TabIndex = 100;
            this.btnHorses.Text = "Horses";
            this.btnHorses.UseVisualStyleBackColor = true;
            this.btnHorses.Click += new System.EventHandler(this.btnHorses_Click);
            // 
            // grpParlayLimits
            // 
            this.grpParlayLimits.Controls.Add(this.chbEnforceParlayBetLimit);
            this.grpParlayLimits.Controls.Add(this.btnParlayLimits);
            this.grpParlayLimits.Controls.Add(this.btnShowAuditParlayMaxPayout);
            this.grpParlayLimits.Controls.Add(this.btnShowAuditParlayMaxBet);
            this.grpParlayLimits.Controls.Add(this.lblMaxParlayPayout);
            this.grpParlayLimits.Controls.Add(this.lblMaxParlayBet);
            this.grpParlayLimits.Controls.Add(this.txtMaxParlayPayout);
            this.grpParlayLimits.Controls.Add(this.txtMaxParlayBet);
            this.grpParlayLimits.Location = new System.Drawing.Point(14, 345);
            this.grpParlayLimits.Name = "grpParlayLimits";
            this.grpParlayLimits.Size = new System.Drawing.Size(212, 110);
            this.grpParlayLimits.TabIndex = 40;
            this.grpParlayLimits.TabStop = false;
            this.grpParlayLimits.Text = "Parlay Limits";
            // 
            // chbEnforceParlayBetLimit
            // 
            this.chbEnforceParlayBetLimit.AccessibleDescription = "Parlays";
            this.chbEnforceParlayBetLimit.AutoSize = true;
            this.chbEnforceParlayBetLimit.Location = new System.Drawing.Point(10, 23);
            this.chbEnforceParlayBetLimit.Name = "chbEnforceParlayBetLimit";
            this.chbEnforceParlayBetLimit.Size = new System.Drawing.Size(87, 17);
            this.chbEnforceParlayBetLimit.TabIndex = 102;
            this.chbEnforceParlayBetLimit.Text = "Enforce Limit";
            this.chbEnforceParlayBetLimit.UseVisualStyleBackColor = true;
            this.chbEnforceParlayBetLimit.CheckedChanged += new System.EventHandler(this.chbEnforceParlayBetLimit_CheckedChanged);
            // 
            // btnParlayLimits
            // 
            this.btnParlayLimits.AccessibleName = "P";
            this.btnParlayLimits.Location = new System.Drawing.Point(99, 20);
            this.btnParlayLimits.Name = "btnParlayLimits";
            this.btnParlayLimits.Size = new System.Drawing.Size(100, 23);
            this.btnParlayLimits.TabIndex = 101;
            this.btnParlayLimits.Text = "Detailed Limits";
            this.btnParlayLimits.UseVisualStyleBackColor = true;
            this.btnParlayLimits.Click += new System.EventHandler(this.btnParlayLimits_Click);
            // 
            // btnShowAuditParlayMaxPayout
            // 
            this.btnShowAuditParlayMaxPayout.Location = new System.Drawing.Point(186, 76);
            this.btnShowAuditParlayMaxPayout.Name = "btnShowAuditParlayMaxPayout";
            this.btnShowAuditParlayMaxPayout.Size = new System.Drawing.Size(14, 23);
            this.btnShowAuditParlayMaxPayout.TabIndex = 82;
            this.btnShowAuditParlayMaxPayout.Text = "?";
            this.btnShowAuditParlayMaxPayout.UseVisualStyleBackColor = true;
            this.btnShowAuditParlayMaxPayout.Click += new System.EventHandler(this.btnShowAuditParlayMaxPayout_Click);
            // 
            // btnShowAuditParlayMaxBet
            // 
            this.btnShowAuditParlayMaxBet.Location = new System.Drawing.Point(186, 46);
            this.btnShowAuditParlayMaxBet.Name = "btnShowAuditParlayMaxBet";
            this.btnShowAuditParlayMaxBet.Size = new System.Drawing.Size(14, 23);
            this.btnShowAuditParlayMaxBet.TabIndex = 81;
            this.btnShowAuditParlayMaxBet.Text = "?";
            this.btnShowAuditParlayMaxBet.UseVisualStyleBackColor = true;
            this.btnShowAuditParlayMaxBet.Click += new System.EventHandler(this.btnShowAuditParlayMaxBet_Click);
            // 
            // lblMaxParlayPayout
            // 
            this.lblMaxParlayPayout.AutoSize = true;
            this.lblMaxParlayPayout.Location = new System.Drawing.Point(17, 79);
            this.lblMaxParlayPayout.Name = "lblMaxParlayPayout";
            this.lblMaxParlayPayout.Size = new System.Drawing.Size(43, 13);
            this.lblMaxParlayPayout.TabIndex = 70;
            this.lblMaxParlayPayout.Text = "Payout:";
            this.lblMaxParlayPayout.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblMaxParlayBet
            // 
            this.lblMaxParlayBet.AutoSize = true;
            this.lblMaxParlayBet.Location = new System.Drawing.Point(31, 49);
            this.lblMaxParlayBet.Name = "lblMaxParlayBet";
            this.lblMaxParlayBet.Size = new System.Drawing.Size(26, 13);
            this.lblMaxParlayBet.TabIndex = 50;
            this.lblMaxParlayBet.Text = "Bet:";
            this.lblMaxParlayBet.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMaxParlayPayout
            // 
            this.txtMaxParlayPayout.AllowCents = false;
            this.txtMaxParlayPayout.AllowNegatives = false;
            this.txtMaxParlayPayout.DividedBy = 1;
            this.txtMaxParlayPayout.DivideFlag = false;
            this.txtMaxParlayPayout.Location = new System.Drawing.Point(80, 76);
            this.txtMaxParlayPayout.Name = "txtMaxParlayPayout";
            this.txtMaxParlayPayout.ShowIfZero = true;
            this.txtMaxParlayPayout.Size = new System.Drawing.Size(100, 20);
            this.txtMaxParlayPayout.TabIndex = 80;
            this.txtMaxParlayPayout.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMaxParlayPayout.Leave += new System.EventHandler(this.numberToText_Leave);
            // 
            // txtMaxParlayBet
            // 
            this.txtMaxParlayBet.AllowCents = false;
            this.txtMaxParlayBet.AllowNegatives = false;
            this.txtMaxParlayBet.DividedBy = 1;
            this.txtMaxParlayBet.DivideFlag = false;
            this.txtMaxParlayBet.Location = new System.Drawing.Point(80, 46);
            this.txtMaxParlayBet.Name = "txtMaxParlayBet";
            this.txtMaxParlayBet.ShowIfZero = true;
            this.txtMaxParlayBet.Size = new System.Drawing.Size(100, 20);
            this.txtMaxParlayBet.TabIndex = 60;
            this.txtMaxParlayBet.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMaxParlayBet.Leave += new System.EventHandler(this.numberToText_Leave);
            // 
            // panWagerLimitsDetails
            // 
            this.panWagerLimitsDetails.Controls.Add(this.dgvwWagerLimitsDetails);
            this.panWagerLimitsDetails.Location = new System.Drawing.Point(4, 66);
            this.panWagerLimitsDetails.Name = "panWagerLimitsDetails";
            this.panWagerLimitsDetails.Size = new System.Drawing.Size(779, 271);
            this.panWagerLimitsDetails.TabIndex = 1;
            // 
            // dgvwWagerLimitsDetails
            // 
            this.dgvwWagerLimitsDetails.AllowUserToAddRows = false;
            this.dgvwWagerLimitsDetails.AllowUserToDeleteRows = false;
            this.dgvwWagerLimitsDetails.AllowUserToResizeRows = false;
            this.dgvwWagerLimitsDetails.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvwWagerLimitsDetails.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dgvwWagerLimitsDetails.ColumnHeadersHeight = 42;
            this.dgvwWagerLimitsDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvwWagerLimitsDetails.DefaultCellStyle = dataGridViewCellStyle14;
            this.dgvwWagerLimitsDetails.Location = new System.Drawing.Point(-1, 3);
            this.dgvwWagerLimitsDetails.Name = "dgvwWagerLimitsDetails";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvwWagerLimitsDetails.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.dgvwWagerLimitsDetails.RowHeadersVisible = false;
            this.dgvwWagerLimitsDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvwWagerLimitsDetails.Size = new System.Drawing.Size(777, 261);
            this.dgvwWagerLimitsDetails.TabIndex = 30;
            this.dgvwWagerLimitsDetails.TabStop = false;
            this.dgvwWagerLimitsDetails.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvwWagerLimitsDetails_CellClick);
            this.dgvwWagerLimitsDetails.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvwWagerLimitsDetails_CellContentClick);
            this.dgvwWagerLimitsDetails.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvwWagerLimitsDetails_CellEndEdit);
            this.dgvwWagerLimitsDetails.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvwWagerLimitsDetails_CellValidating);
            this.dgvwWagerLimitsDetails.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvwWagerLimitsDetails_DataError);
            this.dgvwWagerLimitsDetails.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvwWagerLimitsDetails_EditingControlShowing);
            this.dgvwWagerLimitsDetails.Scroll += new System.Windows.Forms.ScrollEventHandler(this.dgvwWagerLimitsDetails_Scroll);
            this.dgvwWagerLimitsDetails.SelectionChanged += new System.EventHandler(this.dgvwWagerLimitsDetails_SelectionChanged);
            this.dgvwWagerLimitsDetails.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvwWagerLimitsDetails_KeyDown);
            // 
            // grpEnforceWagerLimits
            // 
            this.grpEnforceWagerLimits.Controls.Add(this.chbByLine);
            this.grpEnforceWagerLimits.Controls.Add(this.chbByGameSelection);
            this.grpEnforceWagerLimits.Location = new System.Drawing.Point(246, 12);
            this.grpEnforceWagerLimits.Name = "grpEnforceWagerLimits";
            this.grpEnforceWagerLimits.Size = new System.Drawing.Size(318, 46);
            this.grpEnforceWagerLimits.TabIndex = 0;
            this.grpEnforceWagerLimits.TabStop = false;
            this.grpEnforceWagerLimits.Text = "Enforce Accumulated Wager Limits";
            // 
            // chbByLine
            // 
            this.chbByLine.AutoSize = true;
            this.chbByLine.Location = new System.Drawing.Point(208, 20);
            this.chbByLine.Name = "chbByLine";
            this.chbByLine.Size = new System.Drawing.Size(65, 17);
            this.chbByLine.TabIndex = 22;
            this.chbByLine.Text = "By LINE";
            this.chbByLine.UseVisualStyleBackColor = true;
            this.chbByLine.CheckedChanged += new System.EventHandler(this.chbAccumWagerLimits_CheckedChanged);
            // 
            // chbByGameSelection
            // 
            this.chbByGameSelection.AutoSize = true;
            this.chbByGameSelection.Location = new System.Drawing.Point(45, 20);
            this.chbByGameSelection.Name = "chbByGameSelection";
            this.chbByGameSelection.Size = new System.Drawing.Size(135, 17);
            this.chbByGameSelection.TabIndex = 21;
            this.chbByGameSelection.Text = "By GAME SELECTION";
            this.chbByGameSelection.UseVisualStyleBackColor = true;
            this.chbByGameSelection.CheckedChanged += new System.EventHandler(this.chbAccumWagerLimits_CheckedChanged);
            // 
            // tabpRestrictions
            // 
            this.tabpRestrictions.Controls.Add(this.txtActionDescription);
            this.tabpRestrictions.Controls.Add(this.clbActions);
            this.tabpRestrictions.Controls.Add(this.groupBox1);
            this.tabpRestrictions.Controls.Add(this.btnRestrictionParams);
            this.tabpRestrictions.Location = new System.Drawing.Point(4, 22);
            this.tabpRestrictions.Name = "tabpRestrictions";
            this.tabpRestrictions.Padding = new System.Windows.Forms.Padding(3);
            this.tabpRestrictions.Size = new System.Drawing.Size(793, 474);
            this.tabpRestrictions.TabIndex = 11;
            this.tabpRestrictions.Text = "Restrictions";
            this.tabpRestrictions.UseVisualStyleBackColor = true;
            // 
            // txtActionDescription
            // 
            this.txtActionDescription.Location = new System.Drawing.Point(411, 61);
            this.txtActionDescription.Multiline = true;
            this.txtActionDescription.Name = "txtActionDescription";
            this.txtActionDescription.ReadOnly = true;
            this.txtActionDescription.Size = new System.Drawing.Size(339, 136);
            this.txtActionDescription.TabIndex = 9;
            // 
            // clbActions
            // 
            this.clbActions.FormattingEnabled = true;
            this.clbActions.Location = new System.Drawing.Point(7, 31);
            this.clbActions.Name = "clbActions";
            this.clbActions.Size = new System.Drawing.Size(397, 304);
            this.clbActions.TabIndex = 8;
            this.clbActions.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.clbActions_ItemCheck);
            this.clbActions.SelectedIndexChanged += new System.EventHandler(this.clbActions_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbRestrictionsLog);
            this.groupBox1.Location = new System.Drawing.Point(7, 354);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(746, 114);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Last Changes:";
            // 
            // lbRestrictionsLog
            // 
            this.lbRestrictionsLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbRestrictionsLog.FormattingEnabled = true;
            this.lbRestrictionsLog.Location = new System.Drawing.Point(3, 16);
            this.lbRestrictionsLog.Name = "lbRestrictionsLog";
            this.lbRestrictionsLog.Size = new System.Drawing.Size(740, 95);
            this.lbRestrictionsLog.TabIndex = 0;
            // 
            // btnRestrictionParams
            // 
            this.btnRestrictionParams.Location = new System.Drawing.Point(410, 31);
            this.btnRestrictionParams.Name = "btnRestrictionParams";
            this.btnRestrictionParams.Size = new System.Drawing.Size(80, 23);
            this.btnRestrictionParams.TabIndex = 6;
            this.btnRestrictionParams.Text = "Parameters";
            this.btnRestrictionParams.UseVisualStyleBackColor = true;
            this.btnRestrictionParams.Click += new System.EventHandler(this.btnRestrictionParams_Click);
            // 
            // lblCustomerID
            // 
            this.lblCustomerID.AutoSize = true;
            this.lblCustomerID.Location = new System.Drawing.Point(10, 13);
            this.lblCustomerID.Name = "lblCustomerID";
            this.lblCustomerID.Size = new System.Drawing.Size(89, 13);
            this.lblCustomerID.TabIndex = 10;
            this.lblCustomerID.Text = "Customer ID (F5):";
            // 
            // txtCustomerSince
            // 
            this.txtCustomerSince.Enabled = false;
            this.txtCustomerSince.Location = new System.Drawing.Point(374, 6);
            this.txtCustomerSince.Name = "txtCustomerSince";
            this.txtCustomerSince.ReadOnly = true;
            this.txtCustomerSince.Size = new System.Drawing.Size(404, 20);
            this.txtCustomerSince.TabIndex = 1;
            this.txtCustomerSince.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // mnusMainMenu
            // 
            this.mnusMainMenu.AutoSize = false;
            this.mnusMainMenu.Dock = System.Windows.Forms.DockStyle.None;
            this.mnusMainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiAction});
            this.mnusMainMenu.Location = new System.Drawing.Point(6, 5);
            this.mnusMainMenu.Name = "mnusMainMenu";
            this.mnusMainMenu.Padding = new System.Windows.Forms.Padding(0, 2, 0, 2);
            this.mnusMainMenu.Size = new System.Drawing.Size(50, 24);
            this.mnusMainMenu.TabIndex = 13;
            this.mnusMainMenu.Text = "menuStrip1";
            // 
            // tsmiAction
            // 
            this.tsmiAction.AutoSize = false;
            this.tsmiAction.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiNew,
            this.tsmiEdit,
            this.tsmiDelete,
            this.deleteAgentCascateToolStripMenuItem,
            this.toolStripSeparator1,
            this.tsmiFind,
            this.toolStripSeparator2,
            this.tsmiSave,
            this.tsmiReset,
            this.toolStripSeparator3,
            this.tsmiCalculator,
            this.tsmiTicketWriter,
            this.tsmiReports,
            this.tsmiDecryptor,
            this.tsmiExit});
            this.tsmiAction.Name = "tsmiAction";
            this.tsmiAction.Padding = new System.Windows.Forms.Padding(1, 0, 0, 0);
            this.tsmiAction.Size = new System.Drawing.Size(40, 20);
            this.tsmiAction.Text = "Action";
            // 
            // tsmiNew
            // 
            this.tsmiNew.Name = "tsmiNew";
            this.tsmiNew.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.tsmiNew.Size = new System.Drawing.Size(227, 22);
            this.tsmiNew.Text = "New";
            this.tsmiNew.Click += new System.EventHandler(this.addToolStripMenuItem_Click);
            // 
            // tsmiEdit
            // 
            this.tsmiEdit.Name = "tsmiEdit";
            this.tsmiEdit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.tsmiEdit.Size = new System.Drawing.Size(227, 22);
            this.tsmiEdit.Text = "Edit";
            this.tsmiEdit.Click += new System.EventHandler(this.editCurrentToolStripMenuItem_Click);
            // 
            // tsmiDelete
            // 
            this.tsmiDelete.Name = "tsmiDelete";
            this.tsmiDelete.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.tsmiDelete.Size = new System.Drawing.Size(227, 22);
            this.tsmiDelete.Text = "Delete";
            this.tsmiDelete.Click += new System.EventHandler(this.deleteCurrentToolStripMenuItem_Click);
            // 
            // deleteAgentCascateToolStripMenuItem
            // 
            this.deleteAgentCascateToolStripMenuItem.Name = "deleteAgentCascateToolStripMenuItem";
            this.deleteAgentCascateToolStripMenuItem.Size = new System.Drawing.Size(227, 22);
            this.deleteAgentCascateToolStripMenuItem.Text = "Delete Agent And Customers";
            this.deleteAgentCascateToolStripMenuItem.Click += new System.EventHandler(this.deleteAgentCascateToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(224, 6);
            // 
            // tsmiFind
            // 
            this.tsmiFind.Name = "tsmiFind";
            this.tsmiFind.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.tsmiFind.Size = new System.Drawing.Size(227, 22);
            this.tsmiFind.Text = "Find";
            this.tsmiFind.Click += new System.EventHandler(this.tsmiFindCustomer_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(224, 6);
            // 
            // tsmiSave
            // 
            this.tsmiSave.Name = "tsmiSave";
            this.tsmiSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.tsmiSave.Size = new System.Drawing.Size(227, 22);
            this.tsmiSave.Text = "Save";
            this.tsmiSave.Click += new System.EventHandler(this.tsmiSave_Click);
            // 
            // tsmiReset
            // 
            this.tsmiReset.Name = "tsmiReset";
            this.tsmiReset.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this.tsmiReset.Size = new System.Drawing.Size(227, 22);
            this.tsmiReset.Text = "Reset";
            this.tsmiReset.Click += new System.EventHandler(this.tsmiReset_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(224, 6);
            // 
            // tsmiCalculator
            // 
            this.tsmiCalculator.Name = "tsmiCalculator";
            this.tsmiCalculator.ShortcutKeys = System.Windows.Forms.Keys.F11;
            this.tsmiCalculator.Size = new System.Drawing.Size(227, 22);
            this.tsmiCalculator.Text = "Calculator";
            this.tsmiCalculator.Click += new System.EventHandler(this.tsmiCalculator_Click);
            // 
            // tsmiTicketWriter
            // 
            this.tsmiTicketWriter.Name = "tsmiTicketWriter";
            this.tsmiTicketWriter.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this.tsmiTicketWriter.Size = new System.Drawing.Size(227, 22);
            this.tsmiTicketWriter.Text = "TicketWriter";
            this.tsmiTicketWriter.Click += new System.EventHandler(this.tsmiTicketWriter_Click);
            // 
            // tsmiReports
            // 
            this.tsmiReports.Name = "tsmiReports";
            this.tsmiReports.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.tsmiReports.Size = new System.Drawing.Size(227, 22);
            this.tsmiReports.Text = "Reports";
            this.tsmiReports.Click += new System.EventHandler(this.reportsToolStripMenuItem_Click);
            // 
            // tsmiDecryptor
            // 
            this.tsmiDecryptor.Name = "tsmiDecryptor";
            this.tsmiDecryptor.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.tsmiDecryptor.Size = new System.Drawing.Size(227, 22);
            this.tsmiDecryptor.Text = "Decryptor";
            this.tsmiDecryptor.Click += new System.EventHandler(this.decryptorToolStripMenuItem_Click);
            // 
            // tsmiExit
            // 
            this.tsmiExit.Name = "tsmiExit";
            this.tsmiExit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.tsmiExit.Size = new System.Drawing.Size(227, 22);
            this.tsmiExit.Text = "Exit";
            this.tsmiExit.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // panMainMenu
            // 
            this.panMainMenu.Controls.Add(this.mnusMainMenu);
            this.panMainMenu.Location = new System.Drawing.Point(199, 6);
            this.panMainMenu.Name = "panMainMenu";
            this.panMainMenu.Size = new System.Drawing.Size(79, 27);
            this.panMainMenu.TabIndex = 71;
            // 
            // btnSportsContestsLinks
            // 
            this.btnSportsContestsLinks.Location = new System.Drawing.Point(727, 29);
            this.btnSportsContestsLinks.Name = "btnSportsContestsLinks";
            this.btnSportsContestsLinks.Size = new System.Drawing.Size(52, 23);
            this.btnSportsContestsLinks.TabIndex = 52;
            this.btnSportsContestsLinks.Text = "Links";
            this.btnSportsContestsLinks.UseVisualStyleBackColor = true;
            this.btnSportsContestsLinks.Visible = false;
            this.btnSportsContestsLinks.Click += new System.EventHandler(this.btnSportsContestsLinks_Click);
            // 
            // dtmGeneralCalendar
            // 
            this.dtmGeneralCalendar.Location = new System.Drawing.Point(413, 31);
            this.dtmGeneralCalendar.Name = "dtmGeneralCalendar";
            this.dtmGeneralCalendar.Size = new System.Drawing.Size(188, 20);
            this.dtmGeneralCalendar.TabIndex = 73;
            // 
            // lblCurrentDateTime
            // 
            this.lblCurrentDateTime.AutoSize = true;
            this.lblCurrentDateTime.Location = new System.Drawing.Point(607, 34);
            this.lblCurrentDateTime.Name = "lblCurrentDateTime";
            this.lblCurrentDateTime.Size = new System.Drawing.Size(0, 13);
            this.lblCurrentDateTime.TabIndex = 74;
            // 
            // lklReturnToAgent
            // 
            this.lklReturnToAgent.AutoSize = true;
            this.lklReturnToAgent.Location = new System.Drawing.Point(16, 37);
            this.lklReturnToAgent.Name = "lklReturnToAgent";
            this.lklReturnToAgent.Size = new System.Drawing.Size(90, 13);
            this.lklReturnToAgent.TabIndex = 76;
            this.lklReturnToAgent.TabStop = true;
            this.lklReturnToAgent.Text = "lklReturnToAgent";
            this.lklReturnToAgent.Visible = false;
            this.lklReturnToAgent.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lklReturnToAgent_LinkClicked);
            // 
            // lblShowingLimitsInUSDol
            // 
            this.lblShowingLimitsInUSDol.AutoSize = true;
            this.lblShowingLimitsInUSDol.Location = new System.Drawing.Point(229, 36);
            this.lblShowingLimitsInUSDol.Name = "lblShowingLimitsInUSDol";
            this.lblShowingLimitsInUSDol.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblShowingLimitsInUSDol.Size = new System.Drawing.Size(176, 13);
            this.lblShowingLimitsInUSDol.TabIndex = 203;
            this.lblShowingLimitsInUSDol.Text = "Showing Wager Limits in US Dollars";
            this.lblShowingLimitsInUSDol.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblShowingLimitsInUSDol.Visible = false;
            // 
            // FrmCustomerMaintenance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(818, 560);
            this.Controls.Add(this.panMainMenu);
            this.Controls.Add(this.lblShowingLimitsInUSDol);
            this.Controls.Add(this.lklReturnToAgent);
            this.Controls.Add(this.lblCurrentDateTime);
            this.Controls.Add(this.dtmGeneralCalendar);
            this.Controls.Add(this.btnSportsContestsLinks);
            this.Controls.Add(this.txtCustomerSince);
            this.Controls.Add(this.lblCustomerID);
            this.Controls.Add(this.tabCustomer);
            this.Controls.Add(this.txtCustomerId);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "FrmCustomerMaintenance";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "";
            this.Activated += new System.EventHandler(this.frmCustomerMaintenance_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmCustomerMaintenance_FormClosing);
            this.Load += new System.EventHandler(this.customerMaintenanceForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmCustomerMaintenance_KeyDown);
            this.Resize += new System.EventHandler(this.frmCustomerMaintenance_Resize);
            this.tabCustomer.ResumeLayout(false);
            this.tabpPersonal.ResumeLayout(false);
            this.panPersonal.ResumeLayout(false);
            this.grpSuspendedActions.ResumeLayout(false);
            this.panGeneralInfo.ResumeLayout(false);
            this.panGeneralInfo.PerformLayout();
            this.panMisc.ResumeLayout(false);
            this.panMisc.PerformLayout();
            this.grpPasswords.ResumeLayout(false);
            this.grpPasswords.PerformLayout();
            this.grpSuspendWagering.ResumeLayout(false);
            this.grpSuspendWagering.PerformLayout();
            this.grpPhone.ResumeLayout(false);
            this.grpPhone.PerformLayout();
            this.tabpOffering.ResumeLayout(false);
            this.panOfferingTab.ResumeLayout(false);
            this.grpFreeHalfPoint.ResumeLayout(false);
            this.grpFreeHalfPoint.PerformLayout();
            this.grpFreeHalfPntOnOff.ResumeLayout(false);
            this.grpFreeHalfPntOnOff.PerformLayout();
            this.grpBaseball.ResumeLayout(false);
            this.grpBaseball.PerformLayout();
            this.grpStoreAndShadeGroups.ResumeLayout(false);
            this.grpStoreAndShadeGroups.PerformLayout();
            this.panVariousSettings.ResumeLayout(false);
            this.panVariousSettings.PerformLayout();
            this.grpWagerLimits.ResumeLayout(false);
            this.grpWagerLimits.PerformLayout();
            this.grpCreditLimit.ResumeLayout(false);
            this.grpCreditLimit.PerformLayout();
            this.panParlayAndTeasers.ResumeLayout(false);
            this.panParlayAndTeasers.PerformLayout();
            this.tabpVigDiscount.ResumeLayout(false);
            this.panVigDiscount.ResumeLayout(false);
            this.panVigDiscount.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvwVigDiscount)).EndInit();
            this.tabpPerformance.ResumeLayout(false);
            this.panPerformance.ResumeLayout(false);
            this.grpWagerTypes.ResumeLayout(false);
            this.panGoToDFsOrWagers.ResumeLayout(false);
            this.panPerformanceData.ResumeLayout(false);
            this.panPerformanceData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvwCustomerPerformance)).EndInit();
            this.grpViewDailyFigs.ResumeLayout(false);
            this.grpViewDailyFigs.PerformLayout();
            this.tabpTransactions.ResumeLayout(false);
            this.panTransactions.ResumeLayout(false);
            this.panTranTotalSelected.ResumeLayout(false);
            this.panTranTotalSelected.PerformLayout();
            this.grpTranTypes.ResumeLayout(false);
            this.panHeader.ResumeLayout(false);
            this.panHeader.PerformLayout();
            this.panTransactionsDetails.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvwTransactions)).EndInit();
            this.ctxMenuStripTransactions.ResumeLayout(false);
            this.tabpFreePlays.ResumeLayout(false);
            this.panFreePlays.ResumeLayout(false);
            this.panFreePlaysDetails.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvwFreePlaysDetails)).EndInit();
            this.ctxMenuStripFreePlays.ResumeLayout(false);
            this.panFreePlayHeader.ResumeLayout(false);
            this.panFreePlayHeader.PerformLayout();
            this.tabpComments.ResumeLayout(false);
            this.panComments.ResumeLayout(false);
            this.grpCommentsForCustomer.ResumeLayout(false);
            this.grpCommentsForCustomer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvwCustomerMessages)).EndInit();
            this.grpCommentsForTW.ResumeLayout(false);
            this.grpCommentsForTW.PerformLayout();
            this.tabpMarketing.ResumeLayout(false);
            this.panMarketing.ResumeLayout(false);
            this.grpGeneralComments.ResumeLayout(false);
            this.grpGeneralComments.PerformLayout();
            this.panGeneralMarketing.ResumeLayout(false);
            this.panGeneralMarketing.PerformLayout();
            this.grpEstimatedWagerAmount.ResumeLayout(false);
            this.grpEstimatedWagerAmount.PerformLayout();
            this.grpSportsOfInterest.ResumeLayout(false);
            this.grpSportsOfInterest.PerformLayout();
            this.grpContactRestrictions.ResumeLayout(false);
            this.grpContactRestrictions.PerformLayout();
            this.tabpAgent.ResumeLayout(false);
            this.panAgent.ResumeLayout(false);
            this.panAutoCreateShadeGroups.ResumeLayout(false);
            this.panAutoCreateShadeGroups.PerformLayout();
            this.panFirstAgentSettings.ResumeLayout(false);
            this.panFirstAgentSettings.PerformLayout();
            this.grpInternetCustomerAccess.ResumeLayout(false);
            this.grpInternetCustomerAccess.PerformLayout();
            this.grpHeadCountRate.ResumeLayout(false);
            this.grpHeadCountRate.PerformLayout();
            this.grpCommissionType.ResumeLayout(false);
            this.grpCommissionType.PerformLayout();
            this.panAffiliate.ResumeLayout(false);
            this.panAffiliate.PerformLayout();
            this.panMakeUpAndCasinoFee.ResumeLayout(false);
            this.panMakeUpAndCasinoFee.PerformLayout();
            this.grpMasterAgent.ResumeLayout(false);
            this.grpMasterAgent.PerformLayout();
            this.panSendPackage.ResumeLayout(false);
            this.tabpWagerLimits.ResumeLayout(false);
            this.panWagerLimits.ResumeLayout(false);
            this.panWagerLimits.PerformLayout();
            this.grpMiscellaneous.ResumeLayout(false);
            this.grpMiscellaneous.PerformLayout();
            this.panImportant.ResumeLayout(false);
            this.panImportant.PerformLayout();
            this.grpHorseLimits.ResumeLayout(false);
            this.grpParlayLimits.ResumeLayout(false);
            this.grpParlayLimits.PerformLayout();
            this.panWagerLimitsDetails.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvwWagerLimitsDetails)).EndInit();
            this.grpEnforceWagerLimits.ResumeLayout(false);
            this.grpEnforceWagerLimits.PerformLayout();
            this.tabpRestrictions.ResumeLayout(false);
            this.tabpRestrictions.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.mnusMainMenu.ResumeLayout(false);
            this.mnusMainMenu.PerformLayout();
            this.panMainMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

    }

    #endregion

    public System.Windows.Forms.TextBox txtCustomerId;
    private System.Windows.Forms.Label lblCustomerName;
    private System.Windows.Forms.Label lblCustNameLast;
    private System.Windows.Forms.TabControl tabCustomer;
    private System.Windows.Forms.TabPage tabpPersonal;
    private System.Windows.Forms.TabPage tabpOffering;
    private System.Windows.Forms.Label lblCustomerID;
    private System.Windows.Forms.TextBox txtNameFirst;
    private System.Windows.Forms.Label lblName;
    private System.Windows.Forms.TextBox txtAdress2;
    private System.Windows.Forms.TextBox txtAddress;
    private System.Windows.Forms.TextBox txtNameLast;
    private System.Windows.Forms.TextBox txtNameMi;
    private System.Windows.Forms.Label lblAddress;
    private System.Windows.Forms.TextBox txtCity;
    private System.Windows.Forms.Label lblCity;
    private System.Windows.Forms.TextBox txtZipCode;
    private System.Windows.Forms.Label lblZipCode;
    private System.Windows.Forms.Label lblState;
    private System.Windows.Forms.Label lblCountry;
    private System.Windows.Forms.Label lblEmail;
    private System.Windows.Forms.Label lblInet;
    private System.Windows.Forms.CheckBox chbWeeklyLimits;
    private System.Windows.Forms.Label lblAgent;
    private System.Windows.Forms.CheckBox chbZeroBalance;
    private System.Windows.Forms.Label lblCurrency;
    private System.Windows.Forms.TextBox txtEmail;
    private System.Windows.Forms.Label lblchartPerc;
    private System.Windows.Forms.Label lblPriceType;
    private System.Windows.Forms.Label lblSettleFigure;
    private System.Windows.Forms.Label lblTimeZone;
    private System.Windows.Forms.GroupBox grpPhone;
    private System.Windows.Forms.Label lblFax;
    private System.Windows.Forms.TextBox txtFax;
    private System.Windows.Forms.Label lblBusinessPhone;
    private System.Windows.Forms.Label lblHomePhone;
    private System.Windows.Forms.TextBox txtHomePhone;
    private System.Windows.Forms.TextBox txtBusinessPhone;
    private System.Windows.Forms.GroupBox grpSuspendWagering;
    private System.Windows.Forms.CheckBox chbCasino;
    private System.Windows.Forms.CheckBox chbSuspendSports;
    private System.Windows.Forms.GroupBox grpPasswords;
    private System.Windows.Forms.Label lblPayoutPassword;
    private System.Windows.Forms.Label lblWageringPassword;
    private System.Windows.Forms.TextBox txtWageringPassword;
    private System.Windows.Forms.TextBox txtPayoutPassword;
    private System.Windows.Forms.CheckBox chbWiseGuy;
    private System.Windows.Forms.CheckBox chbShowOnInstantAction;
    private System.Windows.Forms.Panel panMisc;
    private System.Windows.Forms.Label lblSecondsToDelay;
    private System.Windows.Forms.Panel panGeneralInfo;
    private System.Windows.Forms.Panel panPersonal;
    private System.Windows.Forms.Panel panStatesProvList;
    private System.Windows.Forms.Panel panAgentsList;
    private System.Windows.Forms.Panel panCurrencyCodesList;
    private System.Windows.Forms.Panel panCountriesList;
    private System.Windows.Forms.TextBox txtCountry;
    private System.Windows.Forms.Panel panInetTargetsList;
    private System.Windows.Forms.Panel panTimeZonesList;
    private System.Windows.Forms.Panel panPriceTypesList;
    private System.Windows.Forms.Panel panOfferingTab;
    private System.Windows.Forms.Label lblParlay;
    private System.Windows.Forms.Panel panParlayTypes;
    private System.Windows.Forms.Label lblStore;
    private System.Windows.Forms.Panel panStoresList;
    private System.Windows.Forms.Panel panShadeGroups;
    private System.Windows.Forms.Label lblShadeGroups;
    private System.Windows.Forms.Button btnTeasers;
    private System.Windows.Forms.Panel panParlayAndTeasers;
    private System.Windows.Forms.GroupBox grpCreditLimit;
    private System.Windows.Forms.DateTimePicker dtmTempCreditIncrease;
    private System.Windows.Forms.TextBox txtThroughCredIncrease;
    private System.Windows.Forms.Label lblThroughCredIncrease;
    private System.Windows.Forms.Label lblCreditIncrease;
    private System.Windows.Forms.Label lblNoCredit;
    private System.Windows.Forms.Label lblCreditLimit;
    private System.Windows.Forms.GroupBox grpWagerLimits;
    private System.Windows.Forms.Label lblQuickLimit;
    private System.Windows.Forms.Label lblInetMinBet;
    private System.Windows.Forms.Label lblLossLimit;
    private System.Windows.Forms.Label lblCuMinBet;
    private System.Windows.Forms.Panel panVariousSettings;
    private System.Windows.Forms.CheckBox chbCreditCustomer;
    private System.Windows.Forms.CheckBox chbStaticLines;
    private System.Windows.Forms.CheckBox chbUsePuck;
    private System.Windows.Forms.CheckBox chbLimitIfToRisk;
    private System.Windows.Forms.CheckBox chbEnableRollingIfBets;
    private System.Windows.Forms.Button btnCustBuyPoints;
    private System.Windows.Forms.GroupBox grpStoreAndShadeGroups;
    private System.Windows.Forms.TextBox txtCustomerSince;
    private System.Windows.Forms.GroupBox grpBaseball;
    private System.Windows.Forms.CheckBox chbEasternLines;
    private System.Windows.Forms.RadioButton rdoFixed;
    private System.Windows.Forms.RadioButton rdoAction;
    private System.Windows.Forms.RadioButton rdoListed;
    private System.Windows.Forms.GroupBox grpFreeHalfPoint;
    private System.Windows.Forms.Label lblMaxFreeHalfPoint;
    private System.Windows.Forms.Panel panInetFootball;
    private System.Windows.Forms.Panel panCuFootball;
    private System.Windows.Forms.Panel panInetBasketball;
    private System.Windows.Forms.Panel panCuBasketball;
    private System.Windows.Forms.CheckBox chbInetFootball;
    private System.Windows.Forms.CheckBox chbCuFootball;
    private System.Windows.Forms.CheckBox chbInetBasketball;
    private System.Windows.Forms.CheckBox chbCuBasketball;
    private System.Windows.Forms.Button btnBetServiceProfiles;
    private System.Windows.Forms.TabPage tabpVigDiscount;
    private System.Windows.Forms.Panel panVigDiscount;
    private System.Windows.Forms.Label lblVigDiscountNote;
    private System.Windows.Forms.TabPage tabpPerformance;
    private System.Windows.Forms.Panel panPerformance;
    private System.Windows.Forms.Panel panGoToDFsOrWagers;
    private System.Windows.Forms.Button btnGoToWagers;
    private System.Windows.Forms.Button btnGoToDFs;
    private System.Windows.Forms.Panel panPerformanceData;
    public System.Windows.Forms.GroupBox grpViewDailyFigs;
    private System.Windows.Forms.TextBox txtPerfViewModeLegend;
    private System.Windows.Forms.DataGridView dgvwCustomerPerformance;
    private System.Windows.Forms.RadioButton radYearlyPerformance;
    private System.Windows.Forms.RadioButton radWeeklyPerformance;
    private System.Windows.Forms.RadioButton radMonthlyPerformance;
    private System.Windows.Forms.RadioButton radDailyPerformance;
    private System.Windows.Forms.TabPage tabpTransactions;
    private System.Windows.Forms.Panel panTransactions;
    private System.Windows.Forms.Label lblNonPostedCasino;
    private System.Windows.Forms.Label lblLastWeekCO;
    private System.Windows.Forms.Label lblPendingBets;
    private System.Windows.Forms.Label lblAvailableBalance;
    private System.Windows.Forms.Label lblDisplay;
    private System.Windows.Forms.ComboBox cmbDisplay;
    private System.Windows.Forms.Panel panTransactionsDetails;
    public System.Windows.Forms.DataGridView dgvwTransactions;
    private System.Windows.Forms.Button btnNonPostedCasino;
    private System.Windows.Forms.Button btnChangeCO;
    private System.Windows.Forms.Button btnBalance;
    private System.Windows.Forms.Panel panHeader;
    private System.Windows.Forms.Button btnTranGotoWagers;
    public System.Windows.Forms.Button btnLastVerified;
    private System.Windows.Forms.Button btnDeleteTran;
    private System.Windows.Forms.Button btnTranBrowse;
    private System.Windows.Forms.Button btnTranNew;
    private System.Windows.Forms.TabPage tabpFreePlays;
    private System.Windows.Forms.Panel panFreePlays;
    private System.Windows.Forms.Panel panFreePlayHeader;
    private System.Windows.Forms.Label lblFreePlayDisplay;
    private System.Windows.Forms.Label lblFreePlayAvailableBalance;
    private System.Windows.Forms.Label lblFreePlayPendingAmount;
    private System.Windows.Forms.Label lblFreePlayPendingCount;
    private System.Windows.Forms.ComboBox cmbFreePlayDisplay;
    private System.Windows.Forms.Panel panFreePlaysDetails;
    private System.Windows.Forms.DataGridView dgvwFreePlaysDetails;
    private System.Windows.Forms.Button btnFreePlayGoToWagers;
    private System.Windows.Forms.Button btnFreePlayDelete;
    private System.Windows.Forms.Button btnFreePlayNew;
    private System.Windows.Forms.TabPage tabpComments;
    private System.Windows.Forms.Panel panComments;
    private System.Windows.Forms.GroupBox grpCommentsForCustomer;
    private System.Windows.Forms.GroupBox grpCommentsForTW;
    private System.Windows.Forms.TextBox txtCommentsForCustomer;
    private System.Windows.Forms.TextBox txtCommentsForTW;
    private System.Windows.Forms.TabPage tabpMarketing;
    private System.Windows.Forms.Panel panMarketing;
    private System.Windows.Forms.Label lblSource;
    private System.Windows.Forms.TextBox txtReferredBy;
    private System.Windows.Forms.TextBox txtPromoter;
    private System.Windows.Forms.TextBox txtSource;
    private System.Windows.Forms.Label lblReferredBy;
    private System.Windows.Forms.Label lblPromoter;
    private System.Windows.Forms.GroupBox grpContactRestrictions;
    private System.Windows.Forms.GroupBox grpEstimatedWagerAmount;
    private System.Windows.Forms.GroupBox grpSportsOfInterest;
    private System.Windows.Forms.CheckBox InterestedInOther;
    private System.Windows.Forms.CheckBox InterestedInHorseRacing;
    private System.Windows.Forms.CheckBox InterestedInBaseball;
    private System.Windows.Forms.CheckBox InterestedInFootball;
    private System.Windows.Forms.CheckBox InterestedInHockey;
    private System.Windows.Forms.CheckBox chbInterestedInBasketBall;
    private System.Windows.Forms.CheckBox chbNoPhone;
    private System.Windows.Forms.CheckBox chbNoEmail;
    private System.Windows.Forms.CheckBox chbNoMail;
    private System.Windows.Forms.CheckBox chbEstWagerAmount5To50;
    private System.Windows.Forms.CheckBox chbEstWagerAmountOver1000;
    private System.Windows.Forms.CheckBox chbEstWagerAmount51to100;
    private System.Windows.Forms.CheckBox chbEstWagerAmount501To1000;
    private System.Windows.Forms.CheckBox chbEstWagerAmount101to500;
    private System.Windows.Forms.Panel panGeneralMarketing;
    private System.Windows.Forms.TabPage tabpAgent;
    private System.Windows.Forms.Panel panAgent;
    private System.Windows.Forms.Panel panSendPackage;
    private System.Windows.Forms.Button btnSendPackage;
    private System.Windows.Forms.CheckBox chbShadeGroupsAutoCreate;
    private System.Windows.Forms.Panel panMakeUpAndCasinoFee;
    private System.Windows.Forms.Label lblCurrentMakeup;
    private System.Windows.Forms.GroupBox grpMasterAgent;
    private System.Windows.Forms.CheckBox chbDistributeFunds;
    private System.Windows.Forms.Panel panMasterAgentDropDown;
    private System.Windows.Forms.Label lblCasinoFee;
    private System.Windows.Forms.Button btnChangeMakeup;
    private System.Windows.Forms.Label lblPercentOfLosses;
    private System.Windows.Forms.GroupBox grpCommissionType;
    private System.Windows.Forms.RadioButton rdoAffiliateRedFigure;
    private System.Windows.Forms.RadioButton rdoAffiliateSplit;
    private System.Windows.Forms.RadioButton rdoAffiliateWeeklyProfit;
    private System.Windows.Forms.RadioButton rdoAgentRedFigure;
    private System.Windows.Forms.Label lblAgentCommission;
    private System.Windows.Forms.RadioButton rdoAgentSplit;
    private System.Windows.Forms.RadioButton rdoAgentWeeklyProfit;
    private System.Windows.Forms.CheckBox chbIncludeFreePlayLossesinDistribution;
    private System.Windows.Forms.Panel panAffiliate;
    private System.Windows.Forms.GroupBox grpHeadCountRate;
    private System.Windows.Forms.Label lblInternetOnly;
    private System.Windows.Forms.Label lblCasino;
    private System.Windows.Forms.Label lblCallUnit;
    private System.Windows.Forms.GroupBox grpInternetCustomerAccess;
    private System.Windows.Forms.CheckBox chbEnterBettingAdjs;
    private System.Windows.Forms.CheckBox chbEnterDepositsWithdrawals;
    private System.Windows.Forms.CheckBox chbSetMinimumBetAmt;
    private System.Windows.Forms.CheckBox chbChangeSettleFigure;
    private System.Windows.Forms.CheckBox chbUpdateComments;
    private System.Windows.Forms.CheckBox chbChangeTempCredit;
    private System.Windows.Forms.CheckBox chbAddNewAccounts;
    private System.Windows.Forms.CheckBox chbChangeWagerLimits;
    private System.Windows.Forms.CheckBox chbManageLines;
    private System.Windows.Forms.CheckBox chbSuspendWagering;
    private System.Windows.Forms.CheckBox chbChangeCreditLimit;
    private System.Windows.Forms.TextBox txtIdPrefix;
    private System.Windows.Forms.Label lblIdPrefix;
    private System.Windows.Forms.Panel panFirstAgentSettings;
    private System.Windows.Forms.Panel panAutoCreateShadeGroups;
    private System.Windows.Forms.TabPage tabpWagerLimits;
    private System.Windows.Forms.Panel panWagerLimits;
    private System.Windows.Forms.GroupBox grpEnforceWagerLimits;
    private System.Windows.Forms.Panel panWagerLimitsDetails;
    private System.Windows.Forms.DataGridView dgvwWagerLimitsDetails;
    private System.Windows.Forms.Panel panImportant;
    private System.Windows.Forms.Label lblImportant;
    private System.Windows.Forms.GroupBox grpHorseLimits;
    private System.Windows.Forms.GroupBox grpParlayLimits;
    private System.Windows.Forms.Button btnHorses;
    private System.Windows.Forms.Label lblMaxParlayPayout;
    private System.Windows.Forms.Label lblMaxParlayBet;
    private System.Windows.Forms.GroupBox grpMiscellaneous;
    private System.Windows.Forms.Label lblMaxTeaserBet;
    private System.Windows.Forms.Label lblMaxPropBet;
    private System.Windows.Forms.CheckBox chbCustomerIsMasterAgent;
    private System.Windows.Forms.CheckBox chbCustomerIsAgent;
    private NumberTextBox txtChartPerc;
    private NumberTextBox txtSettleFigure;
    private NumberTextBox txtSecondsToDelay;
    private NumberTextBox txtMaxFreeHalfPoint;
    private NumberTextBox txtInetMinBet;
    private NumberTextBox txtCuMinBet;
    private NumberTextBox txtLossLimit;
    private NumberTextBox txtQuickLimit;
    private NumberTextBox txtCreditIncrease;
    private NumberTextBox txtCreditLimit;
    private NumberTextBox txtNonPostedCasino;
    private NumberTextBox txtLastWeekCO;
    private NumberTextBox txtPendingBets;
    private NumberTextBox txtAvailableBalance;
    private NumberTextBox txtFreePlayPendingCount;
    private NumberTextBox txtFreePlayPendingAmount;
    private NumberTextBox txtFreePlayBalance;
    private NumberTextBox txtCurrentMakeup;
    private NumberTextBox txtCasinoFee;
    private NumberTextBox txtAgentCommision;
    private NumberTextBox txtCallUnit;
    private NumberTextBox txtInternetOnly;
    private NumberTextBox txtCasino;
    public NumberTextBox txtMaxParlayBet;
    private NumberTextBox txtMaxParlayPayout;
    private NumberTextBox txtMaxPropBet;
    public NumberTextBox txtMaxTeaserBet;
    private System.Windows.Forms.CheckBox chbByLine;
    private System.Windows.Forms.CheckBox chbByGameSelection;
    private System.Windows.Forms.MenuStrip mnusMainMenu;
    private System.Windows.Forms.ToolStripMenuItem tsmiAction;
    private System.Windows.Forms.ToolStripMenuItem tsmiNew;
    private System.Windows.Forms.ToolStripMenuItem tsmiEdit;
    private System.Windows.Forms.ToolStripMenuItem tsmiDelete;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    private System.Windows.Forms.ToolStripMenuItem tsmiFind;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    private System.Windows.Forms.ToolStripMenuItem tsmiSave;
    private System.Windows.Forms.ToolStripMenuItem tsmiReset;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
    private System.Windows.Forms.ToolStripMenuItem tsmiCalculator;
    private System.Windows.Forms.ToolStripMenuItem tsmiTicketWriter;
    private System.Windows.Forms.ToolStripMenuItem tsmiReports;
    private System.Windows.Forms.ToolStripMenuItem tsmiDecryptor;
    private System.Windows.Forms.ToolStripMenuItem tsmiExit;
    private System.Windows.Forms.Panel panMainMenu;
    private System.Windows.Forms.Button btnSportsContestsLinks;
    private System.Windows.Forms.ContextMenuStrip ctxMenuStripTransactions;
    private System.Windows.Forms.ToolStripMenuItem tsmiNewTransaction;
    private System.Windows.Forms.ToolStripMenuItem tsmiChangeTransaction;
    private System.Windows.Forms.ToolStripMenuItem tsmiDeleteTransaction;
    private System.Windows.Forms.ContextMenuStrip ctxMenuStripFreePlays;
    private System.Windows.Forms.ToolStripMenuItem tsmiNewFreePlay;
    private System.Windows.Forms.ToolStripMenuItem tsmiDeleteFreePlay;
    private System.Windows.Forms.DataGridView dgvwVigDiscount;
    private System.Windows.Forms.DateTimePicker dtmGeneralCalendar;
    private System.Windows.Forms.Label lblCurrentDateTime;
    private System.Windows.Forms.TextBox txtSelectedPerformance;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.ComboBox cmbWLSports;
    private System.Windows.Forms.TabPage tabpRestrictions;
    private System.Windows.Forms.Button btnRestrictionParams;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.ListBox lbRestrictionsLog;
    private System.Windows.Forms.CheckedListBox clbActions;
    private System.Windows.Forms.TextBox txtActionDescription;
    private System.Windows.Forms.GroupBox grpSuspendedActions;
    private System.Windows.Forms.CheckedListBox chBrestrictionsBox;
    private System.Windows.Forms.Button btnShowAuditSettleFigure;
    private System.Windows.Forms.Button btnShowAuditPayoutPassword;
    private System.Windows.Forms.Button btnShowAuditPassword;
    private System.Windows.Forms.Button btnShowAuditCreditLimit;
    private System.Windows.Forms.Button btnShowAuditCreditIncrease;
    private System.Windows.Forms.FontDialog fontDialog1;
    private System.Windows.Forms.Button btnShowAuditCUMinBet;
    private System.Windows.Forms.Button btnShowAuditInetMinBet;
    private System.Windows.Forms.Button btnShowAuditWagerLimits;
    private System.Windows.Forms.Button btnShowAuditQuickLimit;
    public System.Windows.Forms.Button btnShowAuditParlayMaxBet;
    private System.Windows.Forms.Button btnShowAuditParlayMaxPayout;
    public System.Windows.Forms.Button btnShowAuditTeaserMaxBet;
    private System.Windows.Forms.Button btnShowAuditContestMaxBet;
    private System.Windows.Forms.Button btnShowAuditWagerLimitsN;
    private NumberTextBox txtNewMakeup;
    private System.Windows.Forms.Button btnChangeCustMakeup;
    private System.Windows.Forms.Label lblNewMakeup;
    private System.Windows.Forms.Label lblChangeMakeup;
    private System.Windows.Forms.CheckBox chbZeroBalancePositiveFigures;
    private System.Windows.Forms.GroupBox grpGeneralComments;
    private System.Windows.Forms.TextBox txtGeneralComments;
    private System.Windows.Forms.CheckBox chbShowDeleted;
    private System.Windows.Forms.DataGridView dgvwCustomerMessages;
    private System.Windows.Forms.Button btnNewMessage;
    public System.Windows.Forms.ComboBox cmbWagerTypes;
    public System.Windows.Forms.GroupBox grpWagerTypes;
    public System.Windows.Forms.GroupBox grpTranTypes;
    public System.Windows.Forms.ComboBox cmbTranTypes;
    private System.Windows.Forms.ToolStripMenuItem deleteAgentCascateToolStripMenuItem;
    public System.Windows.Forms.TextBox txtTotalsTransSelected;
    public System.Windows.Forms.Label lblTransTotalSelected;
    public System.Windows.Forms.Panel panTranTotalSelected;
    private System.Windows.Forms.DateTimePicker dtmTempQuickLimit;
    private System.Windows.Forms.TextBox txtThroughQuickLimit;
    private System.Windows.Forms.Label lblThroughTempQuickLimit;
    private NumberTextBox txtTempQuickLimit;
    private System.Windows.Forms.Label lblTempQuickLimit;
    public System.Windows.Forms.LinkLabel lklReturnToAgent;
    public System.Windows.Forms.Button btnParlayLimits;
    public System.Windows.Forms.Button btnTeaserLimits;
    public System.Windows.Forms.CheckBox chbEnforceTeaserBetLimit;
    public System.Windows.Forms.CheckBox chbEnforceParlayBetLimit;
    private System.Windows.Forms.Button btnResetWagerLimits;
    private System.Windows.Forms.Label lblDisabledSportsCount;
    private System.Windows.Forms.Label lblShowingLimitsInUSDol;
    private System.Windows.Forms.CheckBox chbEnforceHalfPoint;
    private System.Windows.Forms.Button btnSendToPackage;
    private System.Windows.Forms.Button btnPendingWagers;
    private System.Windows.Forms.GroupBox grpFreeHalfPntOnOff;
    private System.Windows.Forms.CheckBox chbNFLFOOTBALLCostToBuyON3FreeHalfPont;
    private System.Windows.Forms.CheckBox chbNFLFOOTBALLCostToBuyOFF7FreeHalfPont;
    private System.Windows.Forms.CheckBox chbNFLFOOTBALLCostToBuyON7FreeHalfPont;
    private System.Windows.Forms.CheckBox chbNFLFOOTBALLCostToBuyOFF3FreeHalfPont;
    private System.Windows.Forms.CheckBox chbNoFreeHalfPointInTotals;
    private System.Windows.Forms.Button btnSavePackageToFile;
  }
}
