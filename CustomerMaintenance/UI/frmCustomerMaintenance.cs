﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using CustomerMaintenance.Extensions;
using CustomerMaintenance.Extensions.SavePackageToCsv;
using CustomerMaintenance.Libraries;
using GUILibraries.BusinessLayer;
using GUILibraries.Controls;
using GUILibraries.Forms;
using GUILibraries.Utilities;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using SIDLibraries.Utilities;
using Common = CustomerMaintenance.Utilities.Common;
using CustomerTransaction = CustomerMaintenance.Libraries.CustomerTransaction;

namespace CustomerMaintenance.UI {
  public partial class FrmCustomerMaintenance : SIDModuleForm {

    #region private Constants

    private const String PERFORMANCEVIEWDAILY = "D";
    private const String PERFORMANCEVIEWWEEKLY = "W";
    private const String PERFORMANCEVIEWMONTLY = "M";
    private const String PERFORMANCEVIEWYEARLY = "Y";

    public const int SPORT_ENABLED_INDEX = 0,
                     OVR_CIRCLE_INDEX = 1,
                        CU_LIMIT_INDEX = 5,
                        INET_LIMIT_INDEX = 6;

    #endregion

    #region public Constants

    public const String CUST_MSG_ALWAYS_ALERT = "Message will be displayed on the customer's screen until it expires.";
    public const String CUST_MSG_INBOX_ONLY = "Message will be sent to customer's Inbox and it will be visible until it expires.";
    public const String CUST_MSG_ALERT_ONCE = "Message will be displayed on the customer's screen one time, and it will be kept in customer's Inbox until it expires.";
    public const String CUST_MSG_SUSPENDED = "Message will be displayed on the customer's screen when the account is Suspended";
    public const int SPORT_SUB_SPORT_INDEX = 2,
                        PERIOD_INDEX = 3,
                        TYPE_INDEX = 4,
                        WAGER_TYPE_CODE_INDEX = 7,
                        PERIOD_NUMBER_INDEX = 8,
                        TEMP_CU_LIMIT_INDEX = 9,
                        TEMP_INET_LIMIT_INDEX = 10,
                        TEMP_LIMIT_EXP_INDEX = 11,
                        SPORT_INDEX = 12,
                        SUB_SPORT_INDEX = 13;

    #endregion

    #region Extensions

    private PerformanceByWagerType _performanceByWagerType;
    private TransactionsByType _transactionsByType { get; set; }
    private AgentPlayersListTab _showAgentPlayersListTab { get; set; }
    private CustomerTempLimits _customerTempLimits;
    private SavePackageToCsv _packageToCsv;
    //private UseDetailedParlayTeaserWagerLimits _useDetailedParlayTeaserWagerLimits;

    public void SetCustomerTempLimitExtension(bool extensionIsActive) {
      lblTempQuickLimit.Visible = extensionIsActive;
      lblThroughTempQuickLimit.Visible = extensionIsActive;
      txtThroughQuickLimit.Visible = extensionIsActive;
      txtTempQuickLimit.Visible = extensionIsActive;
      dtmTempQuickLimit.Visible = extensionIsActive;
    }

    #endregion

    #region Public Properties

    public decimal? ApplicationInUseRecordId { private get; set; }

    private static DateTime ClientDateTime {
      get {
        return DateTime.Now;
      }
    }

    private TimeSpan ClientServerTimeDiff {
      get {
        if (_clientServerTimeDiff != null) return (TimeSpan)_clientServerTimeDiff;
        _clientServerTimeDiff = ClientDateTime - ServerDateTime;
        return (TimeSpan)_clientServerTimeDiff;
      }
    }

    public double CurrentMakeUp { private get; set; }

    public String CustomerHasWeeklyLimits { get; private set; }

    public bool CustomerInfoRetrieved { get; set; }

    private Boolean SoundCardDetected {
      get {
        if (_soundCardDetected != null) return _soundCardDetected ?? false;
        _soundCardDetected = SoundCard.Detected();
        return _soundCardDetected ?? false;
      }
    }

    //private Boolean LoginIdHasFullCustAdministration { get; set; }

    private List<CustomerInfoUpdater.CustomerVigDiscountElem> ModifiedVigDiscounts { get; set; }

    private List<CustomerInfoUpdater.CustomerWagerLimit> ModifiedWagerLimits { get; set; }

    #endregion

    #region Private Properties

    ModuleInMaintenance ModuleInMaintenance { get; set; }
    List<spCstGetCustomerWagerLimits_Result> WagerLimits { get; set; }
    List<spCstGetRestrictionsByCustomer_Result> CustomerRestrictions { get; set; }

    private bool DenyCreditLimitChange {
      get {
        if (CustomerRestrictions == null) return false;
        try {
          return CustomerRestrictions.Any(r => r.Code == "DENYCRLIM");
        }
        catch (Exception e) {
          return false;
        }
      }
    }

    #endregion

    #region Public Properties

    public String PerformanceViewDateRange { get; private set; }
    private String PerformanceViewMode { get; set; }
    #endregion

    #region Public vars

    public String AgentTypeFromDb;
    public readonly List<spULPGetCurrentUserPermissions_Result> CurrentUserPermissions;
    //public List<spCmGetMasterAgentsList_Result> MAgents;
    private String _currentCustStore = "";
    private String _currentCustParlay = "";
    private String _currentCustomerCurrency = "";
    public String CustomerId = "";
    public String AgentType = "";
    private String _customerType = "";
    private String _agentId = "";
    private DateTime _distributionDate;
    private double _customerCarryoverAmount;
    private double _customerPendingWagerBalance;
    private double _customerCurrentBalance;
    private int _customerPendingWagerCount;
    private int _customerFreePlayPendingCount;
    private double _customerFreePlayPendingBalance;
    public double CustomerFreePlayBalance;
    private bool _packageInherited;
    public String HorseRacingMaxPayOut;

    #endregion

    #region Private vars

    private CustomerInfoUpdater _customerInfo;
    private CustomerBalanceInfo _balanceInfo;
    public spCstGetCustomerInfoNoNulls_Result TbCust;
    private CustomerComboBox _cB;
    public String AgentTypeFromFrm;
    private String _masterAgentId;
    private int _lastVerifiedDocNumber;
    private Boolean _lastVerifiedDocFound;
    private int _custTransCount;
    //private LoginsAndProfiles _logins;
    private List<CustomerInfoUpdater.CustomerInfo> _customerInfoFromFrmList;
    private List<CustomerInfoUpdater.AgentInfo> _customerAsAgentInfoFromFrmList;
    private double _customerCreditLimit;
    private double _customerCreditIncrease;
    private DateTime _customerCreditIncreaseThru;
    private double _customerPendingCredit;
    private double _customerNetCasinoAmount;
    private double _customerThisWeeksFigure;
    private bool _storesInfoRetrieved;
    private bool _vigDiscountInfoRetrieved;
    private bool _agentInfoRetrieved;
    private bool _default7DaysTransRetrieved;
    private bool _default7DaysFreePlaysRetrieved;
    private bool _wagerLimitsInfoRetrieved;
    private bool _customerRestrictionsRetrieved;

    private bool? _soundCardDetected;
    private readonly String _openCustomerId;
    readonly String _tabToLoad;

    private TimeSpan? _clientServerTimeDiff;

    private List<spCstGetCustomerMessages_Result> _customerMessages;

    private List<spCstGetActions_Result> _actions;
    List<spCstGetRestrictedActionParams_Result> _restrictedActionsParams;
    DateTimePicker _oDateTimePicker;
    bool _dateTimePickerExpanded;
    private double? _exchangeRate;

    #endregion

    #region Constructors

    public FrmCustomerMaintenance(ModuleInfo moduleInfo, List<spULPGetCurrentUserPermissions_Result> currentUserAccess, String openCustomerId = null, String tabToLoad = null)
      : base(moduleInfo) {
      CurrentUserPermissions = currentUserAccess;
      _openCustomerId = openCustomerId;
      _tabToLoad = tabToLoad;

      InitializeComponent();
      AppModuleInfo.MaintAction = SetModuleInMaintenance;
      InitializeExtensions();
      SetCentsNumericInputs();
      _soundCardDetected = null;
    }

    #endregion

    #region Public Methods

    public void ClearFormElementsContent() {
      cmbWLSports.SelectedIndexChanged -= cmbWLSports_SelectedIndexChanged;
      cmbTranTypes.SelectedIndexChanged -= cmbTranTypes_SelectedIndexChanged;
      chbZeroBalance.CheckedChanged -= chbZeroBalance_CheckedChanged;
      chbCreditCustomer.CheckedChanged -= chbCreditCustomer_CheckedChanged;

      try {
        foreach (TabPage tbP in tabCustomer.TabPages) {
          if (tbP.Text == @"Agent") continue;
          foreach (Control ctrl in tbP.Controls) {
            if (ctrl.GetType().ToString() != "System.Windows.Forms.Panel") continue;
            foreach (Control childCtrl in ctrl.Controls) {
              Common.ClearControlContent(childCtrl);
              if (childCtrl.GetType().ToString() != "System.Windows.Forms.Panel" &&
                  childCtrl.GetType().ToString() != "System.Windows.Forms.GroupBox") continue;
              foreach (Control child1Ctrl in childCtrl.Controls) {
                Common.ClearAdditionalControlContent(child1Ctrl);
                if (child1Ctrl.GetType().ToString() != "System.Windows.Forms.Panel") continue;
                if (child1Ctrl.Controls.Count > 0) child1Ctrl.Controls.RemoveAt(0);
              }
            }
          }
        }
      }
      catch (Exception e) {
        Log(e);
      }
      cmbWLSports.SelectedIndexChanged += cmbWLSports_SelectedIndexChanged;
      cmbTranTypes.SelectedIndexChanged += cmbTranTypes_SelectedIndexChanged;
      chbZeroBalance.CheckedChanged += chbZeroBalance_CheckedChanged;
      chbCreditCustomer.CheckedChanged += chbCreditCustomer_CheckedChanged;
      _storesInfoRetrieved = false;
      txtThroughCredIncrease.ReadOnly = false;
      txtThroughQuickLimit.ReadOnly = false;
      _vigDiscountInfoRetrieved = false;
      _agentInfoRetrieved = false;
      _default7DaysTransRetrieved = false;
      _default7DaysFreePlaysRetrieved = false;
      _wagerLimitsInfoRetrieved = false;
      _customerRestrictionsRetrieved = false;

      clbActions.Items.Clear();
      txtActionDescription.Text = "";
      lbRestrictionsLog.Items.Clear();

      ShowHideAllTabs(false);
      ResetPerformanceButtonsSelection();
      dtmTempCreditIncrease.Text = DateTime.Now.ToShortDateString();

      grpSuspendedActions.Visible = false;
      if (chBrestrictionsBox.Items.Count > 0)
        chBrestrictionsBox.Items.Clear();
      //this.Size = new System.Drawing.Size(804, 598);
      grpPhone.Location = new Point(426, 10);
      grpPasswords.Location = new Point(426, 110);
      panMisc.Location = new Point(426, 285);
      _actions = null;

      txtCustomerSince.Text = "";
      lblShowingLimitsInUSDol.Visible = false;

      if (_showAgentPlayersListTab.IsActive()) {
        var tabPage = (TabPage)Controls.Find("tbPlayers", true).FirstOrDefault();
        if (tabPage != null)
          tabCustomer.TabPages.Remove(tabPage);
        lklReturnToAgent.Text = "";
        lklReturnToAgent.AccessibleName = "";
        lklReturnToAgent.Visible = false;
      }
      RestoreWagerLimitsTabLabel();
      AgentType = "";
      CustomerId = "";
      AgentTypeFromDb = "";
      txtCustomerId.Focus();
    }

    public DateTime GetCurrentDateTime() {
      var newDateTime = DateTime.Now.AddDays(ClientServerTimeDiff.Days * -1);
      newDateTime = newDateTime.AddHours(ClientServerTimeDiff.Hours * -1);
      newDateTime = newDateTime.AddMinutes(ClientServerTimeDiff.Minutes * -1);
      newDateTime = newDateTime.AddSeconds(ClientServerTimeDiff.Seconds * -1);
      newDateTime = newDateTime.AddMilliseconds(ClientServerTimeDiff.Milliseconds * -1);
      return newDateTime;
    }

    public void GetCustomerMessages(Boolean queryDb, Boolean showDeleted = false) {
      using (var cust = new Customers(AppModuleInfo)) {
        if (_customerMessages == null || queryDb) {
          _customerMessages = cust.GetCustomerMessages(txtCustomerId.Text).ToList();
        }
      }

      if (dgvwCustomerMessages.Rows.Count > 0)
        dgvwCustomerMessages.Rows.Clear();

      if (_customerMessages == null || _customerMessages.Count <= 0) return;
      var status = "";
      var priority = "";

      foreach (var item in _customerMessages) {
        switch (item.MsgStatus) {
          case "D":
            status = "Deleted";
            break;
          case "R":
            status = "Read";
            break;
          case "U":
            status = "Unread";
            break;

        }

        switch (item.MsgPriority) {
          case "H":
            priority = "Always Alert";//"High";
            break;
          case "L":
            priority = "Inbox Only";//"Low";
            break;
          case "M":
            priority = "Alert once";//"Medium";
            break;
          case "S":
            priority = "Suspended";//"Medium";
            break;
        }
        var addRow = (item.MsgStatus == "D" && showDeleted) || item.MsgStatus != "D";
        if (!addRow) continue;
        dgvwCustomerMessages.Rows.Add(item.SentToPackage, item.CustomerMessageId.ToString(CultureInfo.InvariantCulture), item.MsgSentDateTime, item.MsgReadDateTime, item.MsgExpirationDateTime, priority, status, item.MsgSubject, item.MsgBody, item.SentToPackage);
        dgvwCustomerMessages.Rows[dgvwCustomerMessages.Rows.Count - 1].Cells[5].ToolTipText = GetPriorityDescription(priority);
        if (item.SentToPackage != null && (Boolean)item.SentToPackage) {
          dgvwCustomerMessages.Rows[dgvwCustomerMessages.Rows.Count - 1].DefaultCellStyle.BackColor = Color.LightYellow;
        }
      }

      dgvwCustomerMessages.ClearSelection();

      //FormF.PreventDataGridViewColumnsToBeSortedAndResized(dgvwCustomerMessages);
    }

    public CheckedListBox PopulateCustomerShadeGroupsObj(IEnumerable<spCmGetStoreShadeGroupsByStore_Result> storeShadeGroups, List<spCstGetCustomerShadeGroups_Result> customerShadeGroups) {

      var retCBx = new CheckedListBox { CheckOnClick = true, Name = "ShadeGroupsCheckedListBox" };
      var i = 0;

      foreach (var resObj in storeShadeGroups) {
        object item = resObj.CustProfile;
        retCBx.Name = "CustProfile";

        retCBx.Items.Add(item);

        foreach (var custsGrp in customerShadeGroups) {
          var custSGrp = custsGrp.CustProfile;
          if (!String.Equals(custSGrp, item.ToString(), StringComparison.CurrentCultureIgnoreCase)) continue;
          retCBx.SetItemChecked(i, true);
          break;
        }
        i++;
      }

      return retCBx;
    }

    public void UpdateStoreShadeGroups(string customerId, string store) {
      using (var cust = new Customers(AppModuleInfo)) {
        cust.DeleteCustStoreProfile(customerId, store);
      }
    }

    public void PopulateCustomerTransactionsGv(int p) {

      if (dgvwTransactions.Rows.Count > 0) {
        dgvwTransactions.DataSource = null;
        dgvwTransactions.Rows.Clear();
      }

      var transactions = _transactionsByType.GetCustomerTransactions(p);

      if (transactions != null && transactions.Count > 0) {
        var rCnt = transactions.Count;

        var customerTransactions = new List<CustomerTransaction>();

        for (var i = 0; i < rCnt; i++) {
          var description = transactions[i].Description ?? "";

          var amount = Convert.ToSingle(transactions[i].Amount ?? 0);
          double runningBalance;
          if (i == 0) {
            _customerCurrentBalance = transactions[i].CurrentBalance ?? 0;
            runningBalance = _customerCurrentBalance;
          }
          else {
            var parsedCredit = customerTransactions[i - 1].Credit;
            var parsedDebit = customerTransactions[i - 1].Debit;
            var runningAmount = parsedCredit > 0 ? parsedCredit : parsedDebit * (-1);
            var previousBalance = customerTransactions[i - 1].Balance;
            runningBalance = previousBalance - runningAmount;
          }
          var custTran = new CustomerTransaction(DateTime.Parse(transactions[i].TranDateTime.ToString()),
            transactions[i].DocumentNumber,
            description,
            transactions[i].TranCode == "C" ? amount : 0,
            transactions[i].TranCode == "D" ? amount : 0,
            runningBalance,
            transactions[i].TranCode,
            transactions[i].TranType,
            DateTime.Parse(transactions[i].DailyFigureDate.ToString(CultureInfo.InvariantCulture)),
            transactions[i].GradeNum);
          customerTransactions.Add(custTran);

        }


        WriteTransactionsGrvwHeaders(dgvwTransactions);
        dgvwTransactions.ColumnHeadersVisible = true;

        customerTransactions.Reverse();

        foreach (var ct in customerTransactions) {
          String creditAmt;
          String debitAmt;
          if (ct.TranCode == "D") {
            debitAmt = FormatNumber(ct.Debit, true, true);
            creditAmt = "";
          }
          else {
            debitAmt = "";
            creditAmt = FormatNumber(ct.Credit, true, true);
          }

          dgvwTransactions.Rows.Add(DisplayDate.Format(ct.TranDateTime, DisplayDateFormat.LongDateTime), ct.DocumentNumber, ct.Description, creditAmt, debitAmt, cmbTranTypes.SelectedIndex > 0 ? FormatNumber(0, false, true) : FormatNumber(ct.Balance, true, true), ct.TranType, ct.DailyFigureDate, ct.GradeNumber);

          if (ct.DocumentNumber == _lastVerifiedDocNumber) {
            _lastVerifiedDocFound = true;
          }

        }

        if (_lastVerifiedDocFound) {
          btnLastVerified.Enabled = true;
        }

        //FormF.PreventDataGridViewColumnsToBeSortedAndResized(dgvwTransactions);

        dgvwTransactions.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        dgvwTransactions.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
        dgvwTransactions.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;


        if (dgvwTransactions.SelectedRows.Count > 0)
          dgvwTransactions.ClearSelection();
        if (dgvwTransactions.Rows.Count > 0)
          dgvwTransactions.FirstDisplayedScrollingRowIndex = dgvwTransactions.Rows.Count - 1;
      }
    }

    public void PopulateCustomerFreePlaysTransactionsGv(int p) {
      if (dgvwFreePlaysDetails.Rows.Count > 0) {
        dgvwFreePlaysDetails.DataSource = null;
        dgvwFreePlaysDetails.Rows.Clear();
      }

      using (var fps = new FreePlays(AppModuleInfo)) {
        var fPTransactions = fps.GetCustomerFreePlayTransactionsByDate(CustomerId, DateTimeF.StartTransactionDateTime(p)).ToList();

        if (fPTransactions.Count > 0) {
          var rCnt = fPTransactions.Count;

          var customerFpTransactions = new List<CustomerFreePlayTransaction>();


          for (var i = 0; i < rCnt; i++) {
            CustomerFreePlayTransaction custFpTran;
            if (i == 0) {
              custFpTran = new CustomerFreePlayTransaction(DateTime.Parse(fPTransactions[i].TranDateTime.ToString(CultureInfo.InvariantCulture)), int.Parse(fPTransactions[i].DocumentNumber.ToString(CultureInfo.InvariantCulture)), fPTransactions[i].Description, fPTransactions[i].TranCode == "C" ? fPTransactions[i].Amount / 100 : 0, fPTransactions[i].TranCode == "D" ? fPTransactions[i].Amount / 100 : 0, CustomerFreePlayBalance / 100, fPTransactions[i].TranCode, fPTransactions[i].TranType, DateTime.Parse(fPTransactions[i].ValueDate.ToString()), int.Parse(fPTransactions[i].GradeNum.ToString(CultureInfo.InvariantCulture)));
              customerFpTransactions.Add(custFpTran);
            }
            else {
              var runningFpAmount = customerFpTransactions[i - 1].Credit > 0 ? customerFpTransactions[i - 1].Credit : customerFpTransactions[i - 1].Debit * (-1);
              var previousFpBalance = customerFpTransactions[i - 1].Balance;
              var runningFpBalance = previousFpBalance - runningFpAmount;
              custFpTran = new CustomerFreePlayTransaction(DateTime.Parse(fPTransactions[i].TranDateTime.ToString(CultureInfo.InvariantCulture)), int.Parse(fPTransactions[i].DocumentNumber.ToString(CultureInfo.InvariantCulture)), fPTransactions[i].Description, fPTransactions[i].TranCode == "C" ? fPTransactions[i].Amount / 100 : 0, fPTransactions[i].TranCode == "D" ? fPTransactions[i].Amount / 100 : 0, runningFpBalance, fPTransactions[i].TranCode, fPTransactions[i].TranType, DateTime.Parse(fPTransactions[i].ValueDate.ToString()), int.Parse(fPTransactions[i].GradeNum.ToString(CultureInfo.InvariantCulture)));
              customerFpTransactions.Add(custFpTran);
            }
          }

          WriteTransactionsGrvwHeaders(dgvwFreePlaysDetails);
          dgvwFreePlaysDetails.ColumnHeadersVisible = true;

          customerFpTransactions.Reverse();

          foreach (var ct in customerFpTransactions) {
            dgvwFreePlaysDetails.Rows.Add(DisplayDate.Format(ct.TranDateTime, DisplayDateFormat.LongDateTime), ct.DocumentNumber, ct.Description, FormatNumber(ct.Credit, false), FormatNumber(ct.Debit, false), FormatNumber(ct.Balance, false), ct.TranType, ct.ValueDate, ct.GradeNumber);
          }


          //FormF.PreventDataGridViewColumnsToBeSortedAndResized(dgvwFreePlaysDetails);

          dgvwFreePlaysDetails.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
          dgvwFreePlaysDetails.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
          dgvwFreePlaysDetails.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

          dgvwFreePlaysDetails.ClearSelection();
          dgvwFreePlaysDetails.FirstDisplayedScrollingRowIndex = rCnt - 1;

        }
      }
    }

    public void ResetCustomer() {
      if (!CustomerInfoRetrieved) return;
      CustomerInfoRetrieved = false;
      ClearFormElementsContent();
      RetrieveCustomerInfo(CustomerId);
    }

    public void RetrieveCustomerInfo(string customerId, spCstGetCustomerInfoNoNulls_Result custInfo = null) {

      _currentCustStore = "";
      _currentCustParlay = "";
      _currentCustomerCurrency = "";
      _customerType = "";

      _customerCarryoverAmount = 0;
      _customerPendingWagerBalance = 0;
      _exchangeRate = 1;

      _customerCurrentBalance = 0;
      _customerCreditLimit = 0;
      _customerPendingCredit = 0;
      _customerNetCasinoAmount = 0;
      _customerPendingWagerCount = 0;
      _customerFreePlayPendingCount = 0;
      _customerFreePlayPendingBalance = 0;
      CustomerFreePlayBalance = 0;
 
      using (var cust = new Customers(AppModuleInfo)) {
        TbCust = custInfo ?? cust.GetCustomerInfo(customerId).FirstOrDefault();
        _customerInfo = new CustomerInfoUpdater(customerId, AppModuleInfo);
        _customerInfo.LoadCustomerInfoToStructFromDb(TbCust);
        WagerLimits = cust.GetCustomerWagerLimits(CustomerId).ToList();
      }
      if (TbCust == null) return;
      if (LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.TEST_LOGIN)) {
        using (var ag = new Agents(AppModuleInfo)) {
          if (!ag.IsTestAgent(String.IsNullOrWhiteSpace(TbCust.AgentType) ? TbCust.AgentID : TbCust.CustomerID)) {
            MessageBox.Show(@"User only can view test Agents/Customers.");
            TbCust = null;
            return;
          }
        }
      }

      ChangeWagerLimitsTabLabel();
      //form Elements
      txtCustomerSince.Text = @"Modify Existing Customer (since " + TbCust.OpenDateTime.ToShortDateString() + @")";

      DisplayCustomerRestrictions();
      GetCustomerRestrictions(customerId);

      //Personal Tab Elements
      txtNameFirst.Text = TbCust.NameFirst.Trim();
      txtNameMi.Text = TbCust.NameMI.Trim();
      txtNameLast.Text = TbCust.NameLast.Trim();

      var addressAry = Regex.Split(TbCust.Address, "\r\n");
      if (addressAry.Length > 1) {
        txtAddress.Text = addressAry[0].Trim();
        txtAdress2.Text = addressAry[1].Trim();
      }
      else {
        txtAddress.Text = addressAry[0].Trim();
        txtAdress2.Text = "";
      }

      _cB = new CustomerComboBox(AppModuleInfo);

      _packageInherited = _cB.PackageInhToCustomer;

      if (panStatesProvList.Controls.Count > 0) {
        panStatesProvList.Controls.RemoveAt(0);
      }
      panStatesProvList.Controls.Add(_cB.States(TbCust.State.Trim()));

      if (panAgentsList.Controls.Count > 0) {
        panAgentsList.Controls.RemoveAt(0);
      }

      panAgentsList.Controls.Add(_cB.Agents(TbCust.AgentID.Trim(), customerId, AppModuleInfo.Description, this));
      _agentId = TbCust.AgentID.Trim();

      if (panCurrencyCodesList.Controls.Count > 0) {
        panCurrencyCodesList.Controls.RemoveAt(0);
      }

      _custTransCount = GetCustomerTransactionsCount(customerId);
      panCurrencyCodesList.Controls.Add(_cB.Currencies(TbCust.Currency.Trim(), _custTransCount));
      _currentCustomerCurrency = TbCust.Currency.Trim();
      lblShowingLimitsInUSDol.Visible = !_currentCustomerCurrency.StartsWith("USD");

      if (panCountriesList.Controls.Count > 0) {
        panCountriesList.Controls.RemoveAt(0);
      }
      panCountriesList.Controls.Add(_cB.Countries(TbCust.Country.Trim()));

      if (panInetTargetsList.Controls.Count > 0) {
        panInetTargetsList.Controls.RemoveAt(0);
      }
      panInetTargetsList.Controls.Add(_cB.InetTargets(TbCust.InetTarget.Trim()));

      if (panTimeZonesList.Controls.Count > 0) {
        panTimeZonesList.Controls.RemoveAt(0);
      }
      panTimeZonesList.Controls.Add(_cB.TimeZones(TbCust.TimeZone));

      if (panPriceTypesList.Controls.Count > 0) {
        panPriceTypesList.Controls.RemoveAt(0);
      }
      panPriceTypesList.Controls.Add(_cB.PriceTypes(TbCust.PriceType.Trim()));

      if (panStoresList.Controls.Count > 0) {
        panStoresList.Controls.RemoveAt(0);
      }

      txtCity.Text = TbCust.City.Trim();
      txtZipCode.Text = TbCust.Zip.Trim();
      txtCountry.Text = TbCust.Country.Trim();
      txtEmail.Text = TbCust.EMail.Trim();

      chbZeroBalance.CheckedChanged -= chbZeroBalance_CheckedChanged;
      chbZeroBalance.Checked = (TbCust.ZeroBalanceFlag == "Y");
      chbZeroBalance.CheckedChanged += chbZeroBalance_CheckedChanged;

      if (chbZeroBalance.Checked) {
        chbZeroBalancePositiveFigures.Enabled = true;
      }
      else {
        chbZeroBalancePositiveFigures.Checked = false;
        chbZeroBalancePositiveFigures.Enabled = false;
      }
      chbZeroBalancePositiveFigures.Checked = (TbCust.ZeroBalPositiveOnlyFlag == "Y");
      chbWeeklyLimits.Checked = (TbCust.WeeklyLimitFlag == "Y");
      txtChartPerc.Text = TbCust.PercentBook.ToString(CultureInfo.InvariantCulture);
      txtSettleFigure.Text = TbCust.SettleFigure.ToString(CultureInfo.InvariantCulture);

      txtHomePhone.Text = TbCust.HomePhone.Trim();
      txtBusinessPhone.Text = TbCust.BusinessPhone.Trim();
      txtFax.Text = TbCust.Fax.Trim();
      chbSuspendSports.Checked = (TbCust.Active == "N");
      chbCasino.Checked = (TbCust.CasinoActive == "N");
      txtWageringPassword.Text = TbCust.Password.Trim();
      txtPayoutPassword.Text = TbCust.PayoutPassword.Trim();
      chbShowOnInstantAction.Checked = (TbCust.InstantActionFlag == "Y");
      chbWiseGuy.Checked = (TbCust.WiseActionFlag == "Y");
      txtSecondsToDelay.Text = TbCust.ConfirmationDelay.ToString(CultureInfo.InvariantCulture);
      txtSecondsToDelay.Text = TbCust.ConfirmationDelay.ToString(CultureInfo.InvariantCulture);

      //Offering Tab Elements
      _currentCustStore = TbCust.Store.Trim();
      _currentCustParlay = TbCust.ParlayName.Trim();

      _customerCreditLimit = TbCust.CreditLimit;

      txtCreditLimit.Text = TbCust.CreditLimit.ToString(CultureInfo.InvariantCulture);
      txtCreditIncrease.Text = TbCust.TempCreditAdj.ToString(CultureInfo.InvariantCulture);
      _customerCreditIncrease = TbCust.TempCreditAdj;
      txtThroughCredIncrease.Text = (string.IsNullOrEmpty(TbCust.TempCreditAdjExpDate.ToShortDateString()) || TbCust.TempCreditAdjExpDate.ToShortDateString() == "1/1/1900" ? "" : TbCust.TempCreditAdjExpDate.ToShortDateString());

      txtThroughQuickLimit.Text = (string.IsNullOrEmpty(TbCust.TempWagerLimitExpiration.ToShortDateString()) || TbCust.TempWagerLimitExpiration.ToShortDateString() == "1/1/1900" ? "" : TbCust.TempWagerLimitExpiration.ToShortDateString());
      _customerCreditIncreaseThru = TbCust.TempCreditAdjExpDate;

      CheckIfTempCreditHasExpired(txtThroughCredIncrease.Text);

      txtTempQuickLimit.Text = TbCust.TempWagerLimit.ToString(CultureInfo.InvariantCulture);
      txtQuickLimit.Text = TbCust.WagerLimit.ToString(CultureInfo.InvariantCulture);
      txtLossLimit.Text = TbCust.LossCap.ToString(CultureInfo.InvariantCulture);
      txtCuMinBet.Text = TbCust.CUMinimumWager.ToString(CultureInfo.InvariantCulture);
      txtInetMinBet.Text = TbCust.InetMinimumWager.ToString(CultureInfo.InvariantCulture);

      chbStaticLines.Checked = (TbCust.StaticLinesFlag == "Y");
      chbEnableRollingIfBets.Checked = (TbCust.EnableRifFlag == "Y");
      chbUsePuck.Checked = (TbCust.UsePuckLineFlag == "Y");
      chbCreditCustomer.CheckedChanged -= chbCreditCustomer_CheckedChanged;
      chbCreditCustomer.Checked = (TbCust.CreditAcctFlag == "Y");
      chbCreditCustomer.CheckedChanged += chbCreditCustomer_CheckedChanged;
      _customerType = chbCreditCustomer.Checked ? "Credit" : "Postup";
      chbCreditCustomer.Enabled = TbCust.CurrentBalance == 0 && TbCust.PendingWagerCount == 0;
      chbLimitIfToRisk.Checked = (TbCust.LimitRifToRiskFlag == "Y");

      switch (TbCust.BaseballAction.Trim()) {
        case "Listed":
          rdoListed.Checked = true;
          break;

        case "Action":
          rdoAction.Checked = true;
          break;

        case "Fixed":
          rdoFixed.Checked = true;
          break;
      }

      chbEasternLines.Checked = (TbCust.EasternLineFlag == "Y");

      chbCuBasketball.Checked = (TbCust.HalfPointCuBasketballFlag == "Y");
      if (panCuBasketball.Controls.Count > 0) {
        panCuBasketball.Controls.RemoveAt(0);
      }
      panCuBasketball.Controls.Add(_cB.WeekDays(TbCust.HalfPointCuBasketballDow, chbCuBasketball.Checked));
      if (!chbCuBasketball.Checked)
        panCuBasketball.Enabled = false;

      chbInetBasketball.Checked = (TbCust.HalfPointInetBasketballFlag == "Y");
      if (panInetBasketball.Controls.Count > 0) {
        panInetBasketball.Controls.RemoveAt(0);
      }
      panInetBasketball.Controls.Add(_cB.WeekDays(TbCust.HalfPointInetBasketballDow, chbInetBasketball.Checked));
      if (!chbInetBasketball.Checked)
        panInetBasketball.Enabled = false;

      chbCuFootball.Checked = (TbCust.HalfPointCuFootballFlag == "Y");
      if (panCuFootball.Controls.Count > 0) {
        panCuFootball.Controls.RemoveAt(0);
      }
      panCuFootball.Controls.Add(_cB.WeekDays(TbCust.HalfPointCuFootballDow, chbCuFootball.Checked));
      if (!chbCuFootball.Checked)
        panCuFootball.Enabled = false;

      chbInetFootball.Checked = (TbCust.HalfPointInetFootballFlag == "Y");
      if (panInetFootball.Controls.Count > 0) {
        panInetFootball.Controls.RemoveAt(0);
      }
      panInetFootball.Controls.Add(_cB.WeekDays(TbCust.HalfPointInetFootballDow, chbInetFootball.Checked));
      if (!chbInetFootball.Checked)
        panInetFootball.Enabled = false;

      chbEnforceHalfPoint.Checked = (TbCust.HalfPointWagerLimitFlag == "Y");
      txtMaxFreeHalfPoint.Text = TbCust.HalfPointMaxBet.ToString(CultureInfo.InvariantCulture);

      chbNFLFOOTBALLCostToBuyON3FreeHalfPont.Checked = (TbCust.HalfPointFootballOn3Flag == "Y");
      chbNFLFOOTBALLCostToBuyOFF3FreeHalfPont.Checked = (TbCust.HalfPointFootballOff3Flag == "Y");
      chbNFLFOOTBALLCostToBuyON7FreeHalfPont.Checked = (TbCust.HalfPointFootballOn7Flag == "Y");
      chbNFLFOOTBALLCostToBuyOFF7FreeHalfPont.Checked = (TbCust.HalfPointFootballOff7Flag == "Y");
      chbNoFreeHalfPointInTotals.Checked = (TbCust.HalfPointDenyTotalsFlag == "Y");

      GetCustomerStoreOfferingInfo();
      txtThroughCredIncrease.ReadOnly = true;
      txtThroughQuickLimit.ReadOnly = true;
      _storesInfoRetrieved = true;

      //Transactions Tab Elements

      _balanceInfo = new CustomerBalanceInfo(customerId, AppModuleInfo);
      if (_balanceInfo.CasinoNetAmount != null) _customerNetCasinoAmount = _balanceInfo.CasinoNetAmount ?? 0;
      if (_balanceInfo.PendingCredit != null) _customerPendingCredit = _balanceInfo.PendingCredit ?? 0;

      if (TbCust.WeeklyLimitFlag.Trim() == "Y") {
        CustomerHasWeeklyLimits = "Y";
        _customerThisWeeksFigure = _balanceInfo.ThisWeeksFigure;
      }
      else {
        CustomerHasWeeklyLimits = "N";
      }

      _customerCurrentBalance = TbCust.CurrentBalance;
      _lastVerifiedDocNumber = int.Parse(TbCust.LastVerDocumentNumber.ToString(CultureInfo.InvariantCulture));

      txtAvailableBalance.Text = DisplayNumber.ToString(_balanceInfo.AvailableBalance ?? 0);
      _customerPendingWagerBalance += TbCust.PendingWagerBalance / 100;
      txtPendingBets.Text = _customerPendingWagerBalance.ToString(CultureInfo.InvariantCulture);
      _customerCarryoverAmount = TbCust.CarryOverAmount / 100;
      txtLastWeekCO.Text = _customerCarryoverAmount.ToString(CultureInfo.InvariantCulture);
      txtNewMakeup.Text = TbCust.NewCustomerMakeup.ToString(CultureInfo.InvariantCulture);
      txtNonPostedCasino.Text = _customerNetCasinoAmount.ToString(CultureInfo.InvariantCulture);

      _customerPendingWagerCount = int.Parse(TbCust.PendingWagerCount.ToString(CultureInfo.InvariantCulture));
      _customerPendingWagerBalance = TbCust.PendingWagerBalance;
      _customerFreePlayPendingCount = int.Parse(TbCust.FreePlayPendingCount.ToString(CultureInfo.InvariantCulture));
      _customerFreePlayPendingBalance = TbCust.FreePlayPendingBalance;
      CustomerFreePlayBalance = TbCust.FreePlayBalance;
      _exchangeRate = TbCust.ExchangeRate;

      // new tabs info begin

      //Free plays Tab Elements

      txtFreePlayBalance.Text = CustomerFreePlayBalance.ToString(CultureInfo.InvariantCulture);
      txtFreePlayPendingAmount.Text = _customerFreePlayPendingBalance.ToString(CultureInfo.InvariantCulture);
      txtFreePlayPendingCount.Text = _customerFreePlayPendingCount.ToString(CultureInfo.InvariantCulture);


      //Comments Tab elements

      txtGeneralComments.Text = TbCust.Comments.Trim();
      txtCommentsForTW.Text = TbCust.CommentsForTW.Trim();
      txtCommentsForCustomer.Text = TbCust.CommentsForCustomer.Trim();

      //Marketing Tab elements


      txtSource.Text = TbCust.Source.Trim();
      txtPromoter.Text = TbCust.Promoter.Trim();
      txtReferredBy.Text = TbCust.ReferredBy;
      chbNoMail.Checked = (TbCust.NoMailFlag == "Y");
      chbNoEmail.Checked = (TbCust.NoEmailFlag == "Y");
      chbNoPhone.Checked = (TbCust.NoPhoneFlag == "Y");

      chbInterestedInBasketBall.Checked = (TbCust.IntBasketballFlag == "Y");
      InterestedInHockey.Checked = (TbCust.IntHockeyFlag == "Y");
      InterestedInFootball.Checked = (TbCust.IntFootballFlag == "Y");
      InterestedInBaseball.Checked = (TbCust.IntBaseballFlag == "Y");
      InterestedInHorseRacing.Checked = (TbCust.IntHorsesFlag == "Y");
      InterestedInOther.Checked = (TbCust.IntOtherFlag == "Y");

      chbEstWagerAmount5To50.Checked = (TbCust.EstWager5to50Flag == "Y");
      chbEstWagerAmount51to100.Checked = (TbCust.EstWager51to100Flag == "Y");
      chbEstWagerAmount101to500.Checked = (TbCust.EstWager101to500Flag == "Y");
      chbEstWagerAmount501To1000.Checked = (TbCust.EstWager501to1000Flag == "Y");
      chbEstWagerAmountOver1000.Checked = (TbCust.EstWagerOver1000Flag == "Y");

      //Agent Tab Elements
      chbShadeGroupsAutoCreate.CheckedChanged -= chbShadeGroupsAutoCreate_CheckedChanged;
      chbCustomerIsAgent.Checked = (TbCust.AgentType == Agents.AGENT_TYPE);
      AgentTypeFromDb = TbCust.AgentType.Trim();
      chbCustomerIsMasterAgent.Checked = (TbCust.AgentType == Agents.MASTERAGENT_TYPE);
      chbShadeGroupsAutoCreate.Checked = (TbCust.AutoAgentShadeGroupFlag == "Y");
      chbShadeGroupsAutoCreate.CheckedChanged += chbShadeGroupsAutoCreate_CheckedChanged;

      if (chbCustomerIsAgent.Checked || chbCustomerIsMasterAgent.Checked) {
        ShowHideAgentTabElements(true);
      }

      deleteAgentCascateToolStripMenuItem.Visible = TbCust.AgentType == Agents.AGENT_TYPE;
      if (TbCust.AgentType == Agents.AGENT_TYPE || TbCust.AgentType == Agents.MASTERAGENT_TYPE) {
        chbCustomerIsAgent.Enabled = true;
        chbCustomerIsMasterAgent.Enabled = true;

        using (var agent = new Agents(AppModuleInfo)) {
          if (agent.AgentIsLinked(customerId)) {
            chbCustomerIsAgent.Enabled = false;
            chbCustomerIsMasterAgent.Enabled = false;
          }
          else {
            chbCustomerIsAgent.Enabled = true;
            chbCustomerIsMasterAgent.Enabled = true;
          }
        }
      }

      //Wager Limits Tab Elements

      //getCustomerLimitsEnforcement(tbCust.EnforceAccumWagerLimitsFlag);

      chbByGameSelection.Checked = (TbCust.EnforceAccumWagerLimitsFlag == "Y");
      chbByLine.Checked = (TbCust.EnforceAccumWagerLimitsByLineFlag == "Y");
      HorseRacingMaxPayOut = FormatNumber(TbCust.HorseMaxPayout, true, true);

      chbEnforceParlayBetLimit.CheckedChanged -= chbEnforceParlayBetLimit_CheckedChanged;
      chbEnforceParlayBetLimit.Checked = (TbCust.EnforceParlayMaxBetFlag == "Y");
      btnParlayLimits.Enabled = (TbCust.EnforceParlayMaxBetFlag == "N");
      chbEnforceParlayBetLimit.CheckedChanged += chbEnforceParlayBetLimit_CheckedChanged;
      txtMaxParlayBet.Text = (TbCust.ParlayMaxBet / 100).ToString(CultureInfo.InvariantCulture);

      txtMaxParlayPayout.Text = (TbCust.ParlayMaxPayout / 100).ToString(CultureInfo.InvariantCulture);

      txtMaxPropBet.Text = (TbCust.ContestMaxBet / 100).ToString(CultureInfo.InvariantCulture);

      txtMaxTeaserBet.Text = (TbCust.TeaserMaxBet / 100).ToString(CultureInfo.InvariantCulture);
      chbEnforceTeaserBetLimit.CheckedChanged -= chbEnforceTeaserBetLimit_CheckedChanged;
      chbEnforceTeaserBetLimit.Checked = (TbCust.EnforceTeaserMaxBetFlag == "Y");
      btnTeaserLimits.Enabled = (TbCust.EnforceTeaserMaxBetFlag == "N");
      chbEnforceTeaserBetLimit.CheckedChanged += chbEnforceTeaserBetLimit_CheckedChanged;

      ShowHideAllTabs(true);
      panFirstAgentSettings.Visible = true;
      CustomerInfoRetrieved = true;

      using (var lw = new LogWriter(AppModuleInfo)) {
        var logInfo = new LogWriter.CuAccessLogInfo {
          ProgramName = AppModuleInfo.Description,
          Operation = "Open Customer ",
          Data = customerId
        };

        lw.WriteToCuAccessLog(logInfo);
      }

      AgentType = TbCust.AgentType;
      if ((AgentType == Agents.AGENT_TYPE || AgentType == Agents.MASTERAGENT_TYPE) && _showAgentPlayersListTab.IsActive()) {
        _showAgentPlayersListTab.BuildPlayersTab();
      }
      TbCust = null;
    }

    private void ReloadCustomerBalanceInfo() {
      using (var cust = new Customers(AppModuleInfo)) {
        var custInfo = cust.GetCustomerInfo(CustomerId).FirstOrDefault();
        if (custInfo == null) return;
        _customerPendingWagerCount = int.Parse(custInfo.PendingWagerCount.ToString(CultureInfo.InvariantCulture));

        _customerPendingWagerBalance = custInfo.PendingWagerBalance;
        txtPendingBets.Text = (custInfo.PendingWagerBalance / 100).ToString(CultureInfo.InvariantCulture);

        _customerFreePlayPendingCount = int.Parse(custInfo.FreePlayPendingCount.ToString(CultureInfo.InvariantCulture));
        txtFreePlayPendingCount.Text = _customerFreePlayPendingCount.ToString(CultureInfo.InvariantCulture);

        _customerFreePlayPendingBalance = custInfo.FreePlayPendingBalance;
        txtFreePlayPendingAmount.Text = _customerFreePlayPendingBalance.ToString(CultureInfo.InvariantCulture);

        _customerCurrentBalance = custInfo.CurrentBalance;
        _customerCreditLimit = custInfo.CreditLimit;

        txtCreditIncrease.Text = custInfo.TempCreditAdj.ToString(CultureInfo.InvariantCulture);
        _customerCreditIncrease = custInfo.TempCreditAdj;

        _customerCreditIncreaseThru = custInfo.TempCreditAdjExpDate;
        txtThroughCredIncrease.Text = (string.IsNullOrEmpty(custInfo.TempCreditAdjExpDate.ToShortDateString()) || custInfo.TempCreditAdjExpDate.ToShortDateString() == "1/1/1900" ? "" : custInfo.TempCreditAdjExpDate.ToShortDateString());

        CustomerFreePlayBalance = custInfo.FreePlayBalance;
        txtFreePlayBalance.Text = CustomerFreePlayBalance.ToString(CultureInfo.InvariantCulture);
      }

      var balInfo = new CustomerBalanceInfo(CustomerId, AppModuleInfo);
      _customerPendingCredit = balInfo.PendingCredit ?? 0;
      _customerNetCasinoAmount = balInfo.CasinoNetAmount ?? 0;
      _customerThisWeeksFigure = balInfo.ThisWeeksFigure;
      txtAvailableBalance.Text = DisplayNumber.ToString(balInfo.AvailableBalance ?? 0);
    }

    public void RefreshCustomerBalanceInformationFrm(Form caller) {
      caller.Close();
      caller.Dispose();
      ReloadCustomerBalanceInfo();
      ShowCustomerBalance();
    }

    private void GetCustomerRestrictions(string customerId) {
      using (var customerService = new Customers(AppModuleInfo)) {
        CustomerRestrictions = customerService.GetRestrictionsByCustomer(customerId);
      }
    }

    public void SaveCustomerInfo(Boolean keepOpen = false) {
      if (!CustomerInfoRetrieved) return;
      _packageInherited = _cB.PackageInhToCustomer;
      if (chbEnforceParlayBetLimit.Checked) {
        if (txtMaxParlayBet.Text == "" || txtMaxParlayBet.Text == @"0") {
          txtMaxParlayBet.Text = txtCuMinBet.Text;
        }
      }

      if (chbEnforceTeaserBetLimit.Checked) {
        if (txtMaxTeaserBet.Text == "" || txtMaxTeaserBet.Text == @"0") {
          txtMaxTeaserBet.Text = txtCuMinBet.Text;
        }
      }

      _customerInfo.CompareCustomerInfoToUpdate(LoadCustomerInfoToStructFromFrm(), _packageInherited);

      if (_storesInfoRetrieved) {
        Boolean reloadStoreInfo;
        _customerInfo.CompareCustomerStoreShadeGroupsInfoToUpdate(panStoresList, out reloadStoreInfo);

        if (reloadStoreInfo) {
          using (var cu = new Customers(AppModuleInfo)) {
            var custShadeGroups = cu.GetCustomerShadeGroups(txtCustomerId.Text).ToList();
            _customerInfo.LoadCustomerStoreShadeGroupsToStructFromDb(panShadeGroups, custShadeGroups);
          }
        }
      }

      if (_vigDiscountInfoRetrieved && ModifiedVigDiscounts != null && ModifiedVigDiscounts.Count > 0) {
        _customerInfo.CompareCustomerVigDiscountInfoToUpdate(ModifiedVigDiscounts);
      }

      if (dgvwWagerLimitsDetails.IsCurrentCellInEditMode) {
        var cIdx = dgvwWagerLimitsDetails.CurrentCell.ColumnIndex;
        var rIdx = dgvwWagerLimitsDetails.CurrentCell.RowIndex;
        HandleGridViewCellEndEdit(cIdx, rIdx);
      }

      if (_wagerLimitsInfoRetrieved && ModifiedWagerLimits != null && ModifiedWagerLimits.Count > 0) {
        _customerInfo.CompareCustomerWagerLimitsInfoToUpdate(ModifiedWagerLimits);
        ChangeWagerLimitsTabLabel();
      }

      if (_agentInfoRetrieved && (AgentTypeFromDb == Agents.AGENT_TYPE || AgentTypeFromDb == Agents.MASTERAGENT_TYPE)) {
        _customerInfo.CompareCustomerAsAgentInfoToUpdate(LoadCustomerAsAgentInfoToStructFromFrm());
      }

      if (AgentTypeFromFrm != null && AgentTypeFromDb != AgentTypeFromFrm) {
        using (var lw = new LogWriter(AppModuleInfo)) {
          using (var cust = new Customers(AppModuleInfo)) {
            using (var agent = new Agents(AppModuleInfo)) {
              LogWriter.CuAccessLogInfo logInfo;
              switch (AgentTypeFromFrm) {
                case "":
                  cust.ChangeCustomerAgentType(CustomerId, "");
                  cust.RemoveAgentInfo(CustomerId);
                  logInfo = new LogWriter.CuAccessLogInfo {
                    Data = "Agent Type - Off",
                    Operation = "Changed " + CustomerId,
                    ProgramName = AppModuleInfo.Description
                  };
                  lw.WriteToCuAccessLog(logInfo);
                  break;
                case Agents.AGENT_TYPE:
                  cust.ChangeCustomerAgentType(CustomerId, Agents.AGENT_TYPE);
                  cust.RemoveAgentInfo(CustomerId);
                  agent.AddNewAgent(BuildAgentObject(CustomerId, Agents.AGENT_TYPE));
                  logInfo = new LogWriter.CuAccessLogInfo {
                    Data = "Agent Type - Agent",
                    Operation = "Changed " + CustomerId,
                    ProgramName = AppModuleInfo.Description
                  };
                  lw.WriteToCuAccessLog(logInfo);
                  break;
                case Agents.MASTERAGENT_TYPE:
                  cust.ChangeCustomerAgentType(CustomerId, Agents.MASTERAGENT_TYPE);
                  cust.RemoveAgentInfo(CustomerId);
                  agent.AddNewAgent(BuildAgentObject(CustomerId, Agents.MASTERAGENT_TYPE));
                  logInfo = new LogWriter.CuAccessLogInfo {
                    Data = "Agent Type - Master Agent",
                    Operation = "Changed " + CustomerId,
                    ProgramName = AppModuleInfo.Description
                  };
                  lw.WriteToCuAccessLog(logInfo);
                  break;
              }
            }
          }
        }
      }

      if (keepOpen) return;
      CustomerInfoRetrieved = false;
      ClearFormElementsContent();
      ModifiedWagerLimits = null;
      _customerInfo.Dispose();
      _customerInfo = null;
      _customerInfoFromFrmList = null;
      _customerAsAgentInfoFromFrmList = null;
      ModifiedVigDiscounts = null;
      _customerRestrictionsRetrieved = false;
      _restrictedActionsParams = null;
      _balanceInfo = null;
      _cB.Dispose();
      _cB = null;
      WagerLimits = null;
      TakeActionOnDisableSportsCheckBox("Check");
      CustomerRestrictions = null;
      GC.Collect();
      GC.WaitForPendingFinalizers();
    }

    private void TakeActionOnDisableSportsCheckBox(String action) {
      var chb = (CheckBox)dgvwWagerLimitsDetails.Controls.Find("chbDisableSports", true).FirstOrDefault();
      if (chb == null) return;
      switch (action) {
        case "Check":
          chb.Checked = true;
          break;
        case "Disable":
          chb.Enabled = false;
          break;
        case "Enable":
          chb.Enabled = true;
          break;
      }
    }

    public void SendMessageToCustomer(string customerId, DateTime expirationDt, string status, string priority, string subject, string message, decimal? originatingMessageId, Boolean sendToAll) {
      using (var cust = new Customers(AppModuleInfo)) {
        cust.AddCustomerMessage(customerId, expirationDt, status, priority, subject, message, originatingMessageId, sendToAll);
      }
    }

    #endregion

    #region Private Methods

    private void InitializeExtensions() {
      _performanceByWagerType = new PerformanceByWagerType(AppModuleInfo, this);
      _transactionsByType = new TransactionsByType(AppModuleInfo, this);
      _showAgentPlayersListTab = new AgentPlayersListTab(AppModuleInfo, this);
      _customerTempLimits = new CustomerTempLimits(AppModuleInfo, this);
      _packageToCsv = new SavePackageToCsv(AppModuleInfo);
      new ShowCents(AppModuleInfo, Params);
    }

    private void AddNewFreePlay() {
      using (var freePlayFrm = new FrmCustomerNewFreePlay(this, CustomerId, _currentCustomerCurrency, _agentId)) {
        freePlayFrm.Icon = Icon;
        freePlayFrm.ShowDialog();
      }
    }

    private void AddNewTransactionShow() {
      using (var newTranForm = new FrmCustomerTransactionDetails(CustomerId, _agentId, _currentCustomerCurrency, "New", 0, GetCurrentDateTime(), this)) {
        newTranForm.Icon = Icon;
        newTranForm.ShowDialog();
      }
    }

    private void AdjustAvailableBalanceDisplay(object sender) {
      using (var cust = new Customers(AppModuleInfo)) {
        TbCust = cust.GetCustomerInfo(CustomerId).FirstOrDefault();
        if (TbCust != null)
          _customerCurrentBalance = NumberF.ParseDouble(TbCust.CurrentBalance.ToString(CultureInfo.InvariantCulture));
      }

      _balanceInfo = new CustomerBalanceInfo(CustomerId, AppModuleInfo) {
        WeeklyLimitFlag = ((CheckBox)sender).Checked ? "Y" : "N"
      };

      var total = _balanceInfo.AvailableBalance ?? 0;
      if (total < 0) {
        txtAvailableBalance.Text = @"-" + FormatNumber(total, true, true);
      }
      else {
        txtAvailableBalance.Text = FormatNumber(total, true, true);
      }

      if (dgvwTransactions.Rows.Count > 0) {
        if (cmbDisplay.SelectedIndex > -1) {
          PopulateCustomerTransactionsGv(cmbDisplay.SelectedIndex);
        }
      }
    }

    private void AdjustCustomerCreditFlag(object sender) {
      _customerType = ((CheckBox)sender).Checked ? "Credit" : "Postup";
      if (_balanceInfo != null) {
        using (var cust = new Customers(AppModuleInfo)) {
          TbCust = cust.GetCustomerInfo(CustomerId).FirstOrDefault();
          if (TbCust != null) {
            _customerCurrentBalance = 0.0F;
            double.TryParse(TbCust.CurrentBalance.ToString(CultureInfo.InvariantCulture), out _customerCurrentBalance);
            AgentType = TbCust.AgentType;
          }
        }
        _balanceInfo = new CustomerBalanceInfo(CustomerId, AppModuleInfo) {
          AccountType = _customerType.ToUpper()
        };


        var total = _balanceInfo.AvailableBalance ?? 0.0;
        if (total < 0) {
          txtAvailableBalance.Text = @"-" + FormatNumber(total, true, true);
        }
        else {
          txtAvailableBalance.Text = FormatNumber(total, true, true);
        }

        if (dgvwTransactions.Rows.Count > 0) {
          if (cmbDisplay.SelectedIndex > -1) {
            PopulateCustomerTransactionsGv(cmbDisplay.SelectedIndex);
          }
        }
      }
    }

    private void AddVigDiscountEditedRowToControlList(int i) {
      var cUExpDate = StringF.ToStringOrEmpty(dgvwVigDiscount.Rows[i].Cells[5].IsInEditMode ? dgvwVigDiscount.Rows[i].Cells[5].EditedFormattedValue : dgvwVigDiscount.Rows[i].Cells[5].Value);

      var cUVigDisc = StringF.ToStringOrEmpty(dgvwVigDiscount.Rows[i].Cells[4].IsInEditMode ? dgvwVigDiscount.Rows[i].Cells[4].EditedFormattedValue : dgvwVigDiscount.Rows[i].Cells[4].Value);

      var inetExpDate = StringF.ToStringOrEmpty(dgvwVigDiscount.Rows[i].Cells[7].IsInEditMode ? dgvwVigDiscount.Rows[i].Cells[7].EditedFormattedValue : dgvwVigDiscount.Rows[i].Cells[7].Value);

      var inetVigDisc = StringF.ToStringOrEmpty(dgvwVigDiscount.Rows[i].Cells[6].IsInEditMode ? dgvwVigDiscount.Rows[i].Cells[6].EditedFormattedValue : dgvwVigDiscount.Rows[i].Cells[6].Value);

      var period = dgvwVigDiscount.Rows[i].Cells[1].Value.ToString();

      var sportSubType = dgvwVigDiscount.Rows[i].Cells[0].Value.ToString().Split('-')[1];

      var sportType = dgvwVigDiscount.Rows[i].Cells[0].Value.ToString().Split('-')[0];

      var wagerTypeName = dgvwVigDiscount.Rows[i].Cells[3].Value.ToString();

      var wagerType = dgvwVigDiscount.Rows[i].Cells[2].Value.ToString();

      var modifiedRow = new CustomerInfoUpdater.CustomerVigDiscountElem(
          sportType,
          sportSubType,
          period,
          wagerType,
          wagerTypeName,
          cUVigDisc,
          cUExpDate,
          inetVigDisc,
          inetExpDate
          );

      if (ModifiedVigDiscounts == null) {
        ModifiedVigDiscounts = new List<CustomerInfoUpdater.CustomerVigDiscountElem>();
      }
      else {
        var toDel = (from v in ModifiedVigDiscounts
                     where v.SportType.Trim() == modifiedRow.SportType.Trim()
                         && v.SportSubType.Trim() == modifiedRow.SportSubType.Trim()
                         && v.Period.Trim() == modifiedRow.Period.Trim()
                         && v.WagerType.Trim() == modifiedRow.WagerType.Trim()
                     select v).SingleOrDefault();
        ModifiedVigDiscounts.Remove(toDel);
      }
      ModifiedVigDiscounts.Add(modifiedRow);
    }

    private void AddWagerLimitEditedRowToControlList(spCstGetCustomerWagerLimits_Result wl) {
      if (wl == null || wl.SportSubSport == null || wl.sportType == null || wl.SportSubType == null || wl.Period == null)
        return;
      var sportIsEnabled = wl.SportEnabled ?? false;
      var overrideCircle = wl.OverrideCircle ?? false;
      var sportType = wl.sportType.Trim();
      var sportSubType = wl.SportSubType.Trim();
      var period = wl.Period.Trim();
      var wagerType = wl.WagerType;
      var wagerTypeName = wl.WagerTypeDesc;
      var cuLimit = wl.CuLimit.ToString(CultureInfo.InvariantCulture);
      var inetLimit = wl.InetLimit.ToString(CultureInfo.InvariantCulture);

      var tempCuLimit = wl.TempCuLimit.ToString(CultureInfo.InvariantCulture);
      var tempInetLimit = wl.TempInetLimit.ToString(CultureInfo.InvariantCulture);
      var tempLimitsExpiration = "";
      if (wl.TempLimitsExpiration != null)
        tempLimitsExpiration = wl.TempLimitsExpiration.ToString();

      var modifiedRow = new CustomerInfoUpdater.CustomerWagerLimit(
            sportIsEnabled,
            overrideCircle,
            sportType,
            sportSubType,
            period,
            wagerType,
            wagerTypeName,
            cuLimit,
            inetLimit,
            tempCuLimit,
            tempInetLimit,
            tempLimitsExpiration
            );

      if (ModifiedWagerLimits == null) {
        ModifiedWagerLimits = new List<CustomerInfoUpdater.CustomerWagerLimit>();
      }
      else {
        var toDel = (from v in ModifiedWagerLimits
                     where v.SportType.Trim() == modifiedRow.SportType.Trim()
                         && v.SportSubType.Trim() == modifiedRow.SportSubType.Trim()
                         && v.Period.Trim() == modifiedRow.Period.Trim()
                         && v.WagerType.Trim() == modifiedRow.WagerType.Trim()
                     select v).FirstOrDefault();
        lock (ModifiedWagerLimits) {
          ModifiedWagerLimits.Remove(toDel);
        }
      }
      lock (ModifiedWagerLimits) {
        ModifiedWagerLimits.Add(modifiedRow);
      }
    }

    private void BetServiceProfilesShow() {
      using (var betSrvPFrm = new FrmBetServiceProfiles(txtCustomerId.Text, AppModuleInfo)) {
        betSrvPFrm.Icon = Icon;
        betSrvPFrm.ShowDialog();
      }
    }

    private void BrowseTransaction() {
      if ((dgvwTransactions.SelectedRows.Count > 0 && !_transactionsByType.IsActive()) || (_transactionsByType.IsActive() && (dgvwTransactions.SelectedCells.Count == 1))) {
        var tranType = _transactionsByType.GetTranTypeCode(dgvwTransactions);
        switch (tranType) {
          case "X":
            var dailyFigureDate = _transactionsByType.GetTranDailyFigureDate(dgvwTransactions);
            using (var casinoForm = new FrmCustomerCasinoPlays("Posted", CustomerId, dailyFigureDate, AppModuleInfo)) {
              casinoForm.Icon = Icon;
              casinoForm.ShowDialog();
            }
            break;
          default:
            var selectedDocumentNumber = _transactionsByType.GetTranDocumentNumber(dgvwTransactions);
            var docTranDateTime = _transactionsByType.GetTranDateTime(dgvwTransactions);
            using (var tranDetailsForm = new FrmCustomerTransactionDetails(CustomerId, _agentId, _currentCustomerCurrency, "Edit", selectedDocumentNumber, docTranDateTime, this)) {
              tranDetailsForm.Icon = Icon;
              tranDetailsForm.ShowDialog();
            }
            break;
        }
      }
      else {
        MessageBox.Show(@"Please select one transaction.");
      }
    }

    private void ChangeCustomerMakeup() {
      if (LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.ADJUST_MAKEUP_FIGURES)) {
        using (var chAgMakeup = new FrmChangeAgentMakeup(this, CurrentMakeUp)) {
          chAgMakeup.Icon = Icon;
          chAgMakeup.ShowDialog();
        }
      }
      else {
        MessageBox.Show(Environment.UserName + @" does not have permissions to " + LoginsAndProfiles.ADJUST_MAKEUP_FIGURES);
      }
    }

    private void ChangeCarryOverShow() {
      if (LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.ADJUST_MAKEUP_FIGURES)) {
        using (var changeCarryoverFrm = new FrmAdjustCustomerCarryoverFigure(CustomerId, _customerCarryoverAmount, this)) {
          changeCarryoverFrm.Icon = Icon;
          changeCarryoverFrm.ShowDialog();
        }
      }
      else {
        MessageBox.Show(Environment.UserName + @" does not have permissions to " + LoginsAndProfiles.ADJUST_MAKEUP_FIGURES);
      }
    }

    private void ShowAuditInfo(AuditTargetItem auditInfoToShow) {
      using (var auditForm = new FrmCustomerAuditDetails(auditInfoToShow, txtCustomerId.Text, AppModuleInfo)) {
        auditForm.Icon = Icon;
        auditForm.ShowDialog();
      }
    }

    private void ShowAuditInfo(AuditTargetItem auditInfoToShow, DataGridViewRow wagerLimitDetails) {
      using (var auditForm = new FrmCustomerAuditDetails(auditInfoToShow, txtCustomerId.Text, AppModuleInfo, wagerLimitDetails)) {
        auditForm.Icon = Icon;
        auditForm.ShowDialog();
      }
    }

    private void CheckIfTempCreditHasExpired(string expTargetDate) {
      lblCreditIncrease.ForeColor = Color.Black;
      txtCreditIncrease.ForeColor = Color.Black;
      lblThroughCredIncrease.ForeColor = Color.Black;
      txtThroughCredIncrease.ForeColor = Color.Black;
      txtThroughCredIncrease.BackColor = Color.White;

      if (expTargetDate == "") return;
      DateTime dt;
      if (DateTime.TryParse(expTargetDate, out dt)) {
        if (dt.Date < GetCurrentDateTime().Date) {
          lblCreditIncrease.ForeColor = Color.Red;
          txtCreditIncrease.ForeColor = Color.Red;
          lblThroughCredIncrease.ForeColor = Color.Red;
          txtThroughCredIncrease.ForeColor = Color.Red;
        }
        else {
          lblCreditIncrease.ForeColor = Color.Black;
          txtCreditIncrease.ForeColor = Color.Black;
          lblThroughCredIncrease.ForeColor = Color.Black;
          txtThroughCredIncrease.ForeColor = Color.Black;
          txtThroughCredIncrease.BackColor = Color.White;
        }
      }
    }

    private void CostToBuyPointsShow() {
      using (var buyingPointsFrm = new FrmCostToBuyHalfPoints(txtCustomerId.Text, AppModuleInfo)) {
        buyingPointsFrm.Icon = Icon;
        buyingPointsFrm.ShowDialog();
      }
    }

    private void CreateNewMessage() {
      using (var msgfrm = new FrmCustomerMessage("New", null, this)) {
        msgfrm.Icon = Icon;
        msgfrm.ShowDialog();
      }
    }

    private void ShowHideAgentTabElements(bool p) {
      foreach (Control ctrl in tabpAgent.Controls) {
        if (ctrl.GetType().ToString() != "System.Windows.Forms.Panel") continue;
        var childPan = (Panel)ctrl;
        foreach (Control childCtrl in childPan.Controls) {
          if (childCtrl.GetType().ToString() == "System.Windows.Forms.Panel" || childCtrl.GetType().ToString() == "System.Windows.Forms.GroupBox") {
            childCtrl.Visible = p;
          }
        }
      }
    }

    private void DeleteFreePlay() {
      var rCnt = dgvwFreePlaysDetails.SelectedRows.Count;

      if (rCnt > 0) {
        if (MessageBox.Show(@"Are you sure you want to delete the selected free play?", @"delete free play transaction", MessageBoxButtons.OKCancel) == DialogResult.OK) {
          using (var trans = new FreePlays(AppModuleInfo)) {

            var docNumber = int.Parse(dgvwFreePlaysDetails.SelectedRows[0].Cells[1].Value.ToString());
            int debitAmt, creditAmt;
            int.TryParse(dgvwFreePlaysDetails.SelectedRows[0].Cells[4].Value.ToString().Replace(",", ""), out debitAmt);
            int.TryParse(dgvwFreePlaysDetails.SelectedRows[0].Cells[3].Value.ToString().Replace(",", ""), out creditAmt);

            var tranAmount = debitAmt > 0 ? debitAmt : creditAmt;

            var newFreePlayAvailableBalance = double.Parse(txtFreePlayBalance.Text);

            if (debitAmt > 0) {
              newFreePlayAvailableBalance += debitAmt;
              CustomerFreePlayBalance += debitAmt * 100;
              trans.DeleteFpTransaction(CustomerId, docNumber, tranAmount * 100);
            }
            else {
              newFreePlayAvailableBalance -= creditAmt;
              CustomerFreePlayBalance -= creditAmt * 100;
              trans.DeleteFpTransaction(CustomerId, docNumber, tranAmount * -100);
            }

            if (cmbFreePlayDisplay.SelectedIndex > -1) {
              PopulateCustomerFreePlaysTransactionsGv(cmbFreePlayDisplay.SelectedIndex);
            }
            txtFreePlayBalance.Text = (newFreePlayAvailableBalance * txtAvailableBalance.DividedBy).ToString(CultureInfo.InvariantCulture);

            using (var log = new LogWriter(AppModuleInfo)) {
              log.WriteToCuAccessLog(AppModuleInfo.Name, "Changed " + CustomerId, "Free Play Transaction Deleted - Document Number " + docNumber);
            }
          }
        }
      }
      else {
        MessageBox.Show(@"Select a transaction to delete");
      }
    }

    private void DeleteTransaction() {
      if ((dgvwTransactions.SelectedRows.Count > 0 && !_transactionsByType.IsActive()) || (_transactionsByType.IsActive() && (dgvwTransactions.SelectedCells.Count == 1))) {
        if (
      MessageBox.Show(@"Are you sure you want to delete the selected transaction?", @"delete transaction",
        MessageBoxButtons.OKCancel) != DialogResult.OK) return;

        var docNumber = _transactionsByType.GetTranDocumentNumber(dgvwTransactions);//int.Parse(dgvwTransactions.SelectedRows[0].Cells[1].Value.ToString());
        var creditAmt = NumberF.ParseDouble(_transactionsByType.GetTransactionAmount(dgvwTransactions, 3));//dgvwTransactions.SelectedRows[0].Cells[3].Value.ToString();
        var debitAmt = NumberF.ParseDouble(_transactionsByType.GetTransactionAmount(dgvwTransactions, 4));//dgvwTransactions.SelectedRows[0].Cells[4].Value.ToString();

        var tranAmount = (debitAmt > 0 ? debitAmt : creditAmt);

        var newAvailableBalance = NumberF.ParseDouble(txtAvailableBalance.Text);

        using (var trans = new Transactions(AppModuleInfo)) {
          using (var cst = new Customers(AppModuleInfo)) {
            if (debitAmt > 0) {
              if (!chbWeeklyLimits.Checked) {
                newAvailableBalance += debitAmt;
              }
              _customerCurrentBalance += debitAmt * 100;
              trans.DeleteTransaction(docNumber);
              cst.UpdateCustomerCurrentBalance(CustomerId, tranAmount * 100);

            }
            else {
              if (!chbWeeklyLimits.Checked) {
                newAvailableBalance -= creditAmt;
              }
              _customerCurrentBalance -= creditAmt * 100;
              trans.DeleteTransaction(docNumber);
              cst.UpdateCustomerCurrentBalance(CustomerId, tranAmount * -100);
            }
          }
        }

        if (cmbDisplay.SelectedIndex > -1) {
          PopulateCustomerTransactionsGv(cmbDisplay.SelectedIndex);
        }

        txtAvailableBalance.Text = (newAvailableBalance * txtAvailableBalance.DividedBy).ToString(CultureInfo.InvariantCulture);

        using (var log = new LogWriter(AppModuleInfo)) {
          log.WriteToCuAccessLog(AppModuleInfo.Name, "Changed " + CustomerId, "Transaction Deleted - Document Number " + docNumber);
        }
      }
      else {
        MessageBox.Show(@"Select a transaction to delete");
      }
    }

    private void DisableRestrictedFormFields() {
      txtNameFirst.Enabled = false;
      txtNameLast.Enabled = false;
      grpSuspendWagering.Enabled = false;
      panCurrencyCodesList.Enabled = false;
      panAgentsList.Enabled = false;
      panInetTargetsList.Enabled = false;
      txtChartPerc.Enabled = false;
      txtSettleFigure.Enabled = false;
      chbWiseGuy.Enabled = false;
      chbStaticLines.Enabled = false;
      chbCreditCustomer.Enabled = false;
      chbEnableRollingIfBets.Enabled = false;
      chbLimitIfToRisk.Enabled = false;
      grpCreditLimit.Enabled = false;
      grpWagerLimits.Enabled = false;
      grpFreeHalfPoint.Enabled = false;
      grpViewDailyFigs.Enabled = false;
    }

    private void DisplayCustomerRestrictions() {
      chBrestrictionsBox.Items.Clear();
      if (_actions == null) {
        CustomerId = txtCustomerId.Text;
        using (var cst = new Customers(AppModuleInfo)) {
          _actions = cst.GetActions(CustomerId);
        }
      }

      if (_actions.Count > 0) {
        foreach (var action in _actions) {
          if (action.RestrictedActionId != null && !action.SystemRestricted) {
            chBrestrictionsBox.Items.Insert(chBrestrictionsBox.Items.Count, action.Name.Replace("Suspend", "").Trim());
          }
        }
      }

      if (chBrestrictionsBox.Items.Count > 0) {
        //this.Size = new System.Drawing.Size(929, 598);
        grpPhone.Location = new Point(426, 2);
        grpPasswords.Location = new Point(426, 255);
        panMisc.Location = new Point(426, 348);

        grpSuspendedActions.Visible = true;
        for (var i = 0; i <= (chBrestrictionsBox.Items.Count - 1); i++) {
          chBrestrictionsBox.SetItemCheckState(i, CheckState.Checked);
        }
      }
      else {
        grpSuspendedActions.Visible = false;
        grpPhone.Location = new Point(426, 10);
        grpPasswords.Location = new Point(426, 196);
        panMisc.Location = new Point(426, 285);
      }
    }

    private void EnableDisableToolStripMenus(object sender, MouseEventArgs e, ContextMenuStrip ctxMenuStrip) {
      if (e.Button != MouseButtons.Right) return;
      if ((((DataGridView)sender).SelectedRows.Count == 0 && !_transactionsByType.IsActive()) || (_transactionsByType.IsActive() && ((DataGridView)sender).SelectedCells.Count == 0)) {
        Common.EnableDisableToolStripItem(CurrentUserPermissions, ctxMenuStrip);
      }
      else {
        if ((((DataGridView)sender).SelectedRows.Count < 0 && !_transactionsByType.IsActive()) || (_transactionsByType.IsActive() && ((DataGridView)sender).SelectedCells.Count < 0)) return;
        var tranType = _transactionsByType.GetTranTypeCode(sender);

        switch (tranType) {
          case "A":
          case "L":
          case "W":
          case "R":
            Common.EnableDisableToolStripItem(CurrentUserPermissions, ctxMenuStrip);
            break;
          default:
            foreach (ToolStripItem item in ctxMenuStrip.Items) {
              switch (item.AccessibleName) {
                case @"0":
                  item.Enabled = LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.ENTER_CUSTOMER_TRANSACTIONS);
                  break;
                case @"1":
                case @"2":
                  item.Enabled = LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.EDIT_DELETE_CUSTOMER_TRANSACTIONS);
                  break;
              }
            }
            break;
        }
      }
    }

    private spCstGetCustomerWagerLimits_Result GetWagerLimitObject(int i, bool? disableSport = null, bool? overrideCircle = null) {
      var wl = new spCstGetCustomerWagerLimits_Result();
      if (dgvwWagerLimitsDetails.Rows[i].Cells[SPORT_SUB_SPORT_INDEX].Value == null) return null;
      var isEnabled = true;
      if (dgvwWagerLimitsDetails.Rows[i].Cells[SPORT_ENABLED_INDEX].Value != null) {
        isEnabled = (bool)dgvwWagerLimitsDetails.Rows[i].Cells[SPORT_ENABLED_INDEX].Value;
      }
      if (disableSport != null)
        isEnabled = (bool)disableSport;

      wl.SportEnabled = isEnabled;

      var overrideC = false;
      if (dgvwWagerLimitsDetails.Rows[i].Cells[OVR_CIRCLE_INDEX].Value != null) {
        overrideC = (bool)dgvwWagerLimitsDetails.Rows[i].Cells[OVR_CIRCLE_INDEX].Value;
      }
      if (overrideCircle != null) {
        overrideC = (bool)overrideCircle;
      }

      wl.OverrideCircle = overrideC;
      if (dgvwWagerLimitsDetails.Rows[i].Cells[SPORT_SUB_SPORT_INDEX].Value != null) wl.SportSubSport = dgvwWagerLimitsDetails.Rows[i].Cells[SPORT_SUB_SPORT_INDEX].Value.ToString();
      if (dgvwWagerLimitsDetails.Rows[i].Cells[SPORT_INDEX].Value != null) wl.sportType = dgvwWagerLimitsDetails.Rows[i].Cells[SPORT_INDEX].Value.ToString();
      if (dgvwWagerLimitsDetails.Rows[i].Cells[SUB_SPORT_INDEX].Value != null) wl.SportSubType = dgvwWagerLimitsDetails.Rows[i].Cells[SUB_SPORT_INDEX].Value.ToString();
      if (dgvwWagerLimitsDetails.Rows[i].Cells[PERIOD_INDEX].Value != null) wl.Period = dgvwWagerLimitsDetails.Rows[i].Cells[PERIOD_INDEX].Value.ToString();
      if (dgvwWagerLimitsDetails.Rows[i].Cells[PERIOD_NUMBER_INDEX].Value != null) wl.PeriodNumber = int.Parse(dgvwWagerLimitsDetails.Rows[i].Cells[PERIOD_NUMBER_INDEX].Value.ToString());

      if (dgvwWagerLimitsDetails.Rows[i].Cells[WAGER_TYPE_CODE_INDEX].Value != null) wl.WagerType = dgvwWagerLimitsDetails.Rows[i].Cells[WAGER_TYPE_CODE_INDEX].Value.ToString();
      if (dgvwWagerLimitsDetails.Rows[i].Cells[TYPE_INDEX].Value != null) wl.WagerTypeDesc = dgvwWagerLimitsDetails.Rows[i].Cells[TYPE_INDEX].Value.ToString();
      wl.CuLimit = double.Parse(StringF.ToStringOrEmpty(dgvwWagerLimitsDetails.Rows[i].Cells[CU_LIMIT_INDEX].IsInEditMode ? dgvwWagerLimitsDetails.Rows[i].Cells[CU_LIMIT_INDEX].EditedFormattedValue : dgvwWagerLimitsDetails.Rows[i].Cells[CU_LIMIT_INDEX].Value));
      wl.InetLimit = double.Parse(StringF.ToStringOrEmpty(dgvwWagerLimitsDetails.Rows[i].Cells[INET_LIMIT_INDEX].IsInEditMode ? dgvwWagerLimitsDetails.Rows[i].Cells[INET_LIMIT_INDEX].EditedFormattedValue : dgvwWagerLimitsDetails.Rows[i].Cells[INET_LIMIT_INDEX].Value));

      string tempCuLimit, tempInetLimit, tempLimitsExpiration;
      _customerTempLimits.GetDetailLimits(dgvwWagerLimitsDetails.Rows[i], out tempCuLimit, out tempInetLimit, out tempLimitsExpiration);

      if (tempCuLimit != null) wl.TempCuLimit = double.Parse(tempCuLimit);
      if (tempInetLimit != null) wl.TempInetLimit = double.Parse(tempInetLimit);
      if (!String.IsNullOrEmpty(tempLimitsExpiration)) wl.TempLimitsExpiration = DateTime.Parse(tempLimitsExpiration);

      foreach (var wli in WagerLimits) {
        if (wli.sportType.Trim() == wl.sportType.Trim() && wli.SportSubType.Trim() == wl.SportSubType.Trim() && wli.PeriodNumber == wl.PeriodNumber && wli.WagerType.Trim() == wl.WagerType.Trim()) {
          wli.SportIsEnabledTmp = wl.SportEnabled;
          wli.OverrideCircleTmp = wl.OverrideCircle;
        }
      }

      return wl;
    }

    private void GoToWagersFromFreePlaysShow() {
      if (dgvwFreePlaysDetails.SelectedRows.Count > 0) {
        var tranDateTime = DateTime.Parse(dgvwFreePlaysDetails.SelectedRows[0].Cells[0].Value.ToString());
        var ticketNumber = int.Parse(dgvwFreePlaysDetails.SelectedRows[0].Cells[1].Value.ToString());

        using (var ticketInfoFrm = new FrmTicketInformation(this, 0, ticketNumber, tranDateTime, AppModuleInfo, CustomerId, CurrentUserPermissions, _currentCustomerCurrency, SoundCardDetected, null)) {
          ticketInfoFrm.Icon = Icon;
          ticketInfoFrm.OpenPlaysMode = false;
          ticketInfoFrm.EnableToEditPendingPlays = false;
          ticketInfoFrm.ShowDialog();

          var cbxIdx = cmbFreePlayDisplay.SelectedIndex;
          if (cbxIdx > -1)
            PopulateCustomerFreePlaysTransactionsGv(cbxIdx);
        }
      }
      else {
        MessageBox.Show(@"Select a wager related transaction to see additional details");
      }
    }

    private void GoToWagersFromTransactionsShow() {
      if (LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.REVIEW_WAGERS)) {
        if ((dgvwTransactions.SelectedRows.Count > 0 && !_transactionsByType.IsActive()) || (_transactionsByType.IsActive() && dgvwTransactions.SelectedCells.Count == 1)) {
          var tranDateTime = _transactionsByType.GetTranDateTime(dgvwTransactions); //DateTime.Parse(dgvwTransactions.SelectedRows[0].Cells[0].Value.ToString());
          var gradeNumber = _transactionsByType.GetTranGradeNumber(dgvwTransactions);//int.Parse(dgvwTransactions.SelectedRows[0].Cells[8].Value.ToString());
          var documentNumber = _transactionsByType.GetTranDocumentNumber(dgvwTransactions);//int.Parse(dgvwTransactions.SelectedRows[0].Cells[1].Value.ToString());
          using (var ticketInfoFrm = new FrmTicketInformation(this, documentNumber, gradeNumber, tranDateTime, AppModuleInfo, CustomerId, CurrentUserPermissions, _currentCustomerCurrency, SoundCardDetected, null)) {
            ticketInfoFrm.OpenPlaysMode = false;
            ticketInfoFrm.EnableToEditPendingPlays = false;
            ticketInfoFrm.Icon = Icon;
            ticketInfoFrm.ShowDialog();

            var cbxIdx = cmbDisplay.SelectedIndex;
            if (cbxIdx > -1 && ticketInfoFrm.ReloadTransactionsGrid)
              PopulateCustomerTransactionsGv(cbxIdx);
          }
        }
        else {
          MessageBox.Show(@"Select a wager related transaction to see additional details");
        }
      }
      else {
        MessageBox.Show(Environment.UserName + @" does not have permissions to " + LoginsAndProfiles.REVIEW_WAGERS);
      }
    }

    private void GoToWagersShow() {
      if (LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.REVIEW_WAGERS)) {
        using (var ticketInfoFrm = new FrmTicketInformation(this, 0, 0, GetCurrentDateTime(), AppModuleInfo, CustomerId, CurrentUserPermissions, _currentCustomerCurrency, SoundCardDetected, null)) {
          ticketInfoFrm.OpenPlaysMode = false;
          ticketInfoFrm.EnableToEditPendingPlays = false;
          ticketInfoFrm.Icon = Icon;
          ticketInfoFrm.ShowDialog();
        }
      }
      else {
        MessageBox.Show(Environment.UserName + @" does not have permissions to " + LoginsAndProfiles.REVIEW_WAGERS);
      }
    }

    private void HandleRowSelectionChangeEvent(object sender) {
      var tranCanBeDeleted = true;
      if ((((DataGridView)sender).SelectedRows.Count > 0 && !_transactionsByType.IsActive()) || (_transactionsByType.IsActive() && ((DataGridView)sender).SelectedCells.Count == 1)) {
        var tranType = _transactionsByType.GetTranTypeCode(sender);
        switch (tranType) {
          case "A":
          case "L":
          case "W":
          case "R":
            btnTranGotoWagers.Enabled = true;
            btnTranBrowse.Enabled = false;
            btnDeleteTran.Enabled = false;
            tranCanBeDeleted = false;
            break;
          default:
            btnTranGotoWagers.Enabled = false;
            btnTranBrowse.Enabled = true;
            btnDeleteTran.Enabled = true;
            break;
        }

      }

      btnTranNew.Enabled = LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.ENTER_CUSTOMER_TRANSACTIONS);

      if (LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.EDIT_DELETE_CUSTOMER_TRANSACTIONS) && tranCanBeDeleted) {
        btnTranBrowse.Enabled = true;
        btnDeleteTran.Enabled = true;
      }
      else {
        btnTranBrowse.Enabled = false;
        btnDeleteTran.Enabled = false;
      }
    }

    private bool HandleShadeGroupsAutoCreateClick() {
      return MessageBox.Show(@"Assigned Customers Will Be Removed From Shade Group. Are you sure?", @"Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK;
    }

    private void HandleTabCustommerSelectionChange(object sender) {
      if (!CustomerInfoRetrieved)
        return;

      var tab = (TabControl)sender;

      var selectedTabName = tab.SelectedTab.Name;

      if (!_vigDiscountInfoRetrieved && selectedTabName == "tabpVigDiscount") {
        dgvwVigDiscount.AutoGenerateColumns = false;
        GetCustomervigDiscountInfo();
        _vigDiscountInfoRetrieved = true;
      }

      if ((AgentTypeFromDb == Agents.AGENT_TYPE || AgentTypeFromDb == Agents.MASTERAGENT_TYPE) && !_agentInfoRetrieved && selectedTabName == "tabpAgent") {

        using (var agent = new Agents(AppModuleInfo)) {
          var makeup = agent.GetAgentDistributionInfo(CustomerId).FirstOrDefault();
          _distributionDate = new DateTime(2006, 07, 01);
          txtCurrentMakeup.Text = "";
          if (makeup != null) {
            txtCurrentMakeup.Text = makeup.NewMakeup.ToString(CultureInfo.InvariantCulture);
            CurrentMakeUp = double.Parse(makeup.NewMakeup.ToString(CultureInfo.InvariantCulture));
            _distributionDate = makeup.DistributionDate;
          }
        }
        RetrieveAgentInformation();
        _agentInfoRetrieved = true;
      }

      if (!_default7DaysTransRetrieved && selectedTabName == "tabpTransactions") {
        if (cmbDisplay != null && cmbDisplay.Items.Count > 0) cmbDisplay.SelectedIndex = 0;
        if (cmbTranTypes != null && cmbTranTypes.Items.Count > 0) cmbTranTypes.SelectedIndex = 0;
        _default7DaysTransRetrieved = true;
      }

      if (!_default7DaysFreePlaysRetrieved && selectedTabName == "tabpFreePlays") {
        if (cmbFreePlayDisplay != null && cmbFreePlayDisplay.Items.Count > 0) cmbFreePlayDisplay.SelectedIndex = 0;

        _default7DaysFreePlaysRetrieved = true;
      }

      if (!_wagerLimitsInfoRetrieved && selectedTabName == "tabpWagerLimits") {
        GetWagerLimitsSports();
        _wagerLimitsInfoRetrieved = true;
      }

      if (selectedTabName == "tabpComments") {
        btnSendToPackage.Visible = (AgentTypeFromDb == Agents.AGENT_TYPE);

        WriteCustomerMessagesGridHeaders(dgvwCustomerMessages);
        GetCustomerMessages(true);
      }

      if (!_customerRestrictionsRetrieved && selectedTabName == "tabpRestrictions") {
        GetCustomerActionsAndRestrictions();
        GetCustomerActionsAndRestrictionsLogs();
        _customerRestrictionsRetrieved = true;
      }

      if (selectedTabName == "tabpRestrictions" && clbActions != null) {
        clbActions.SelectedIndex = -1;
      }

      if (selectedTabName == "tabpOffering") {
        txtCreditLimit.Enabled = txtCreditIncrease.Enabled = txtThroughCredIncrease.Enabled = dtmTempCreditIncrease.Enabled = !DenyCreditLimitChange;
      }
      SaveCustomerInfo(true);
    }

    private void HandleFormClosingEvent() {
      using (var lw = new LogWriter(AppModuleInfo)) {
        lw.LogUserInOrOutToOrFromCuTb("Logout");
      }
      ModuleInMaintenance = null;
    }

    private void HandleFreeHalfPointsChange(object sender) {
      var chb = (CheckBox)sender;

      var panName = "pan" + chb.Name.Substring(3);
      var pan = (Panel)Controls.Find(panName, true).FirstOrDefault();

      if (pan == null) return;
      if (!chb.Checked) {
        if (pan.Controls.Count <= 0) return;
        var cbx = (ComboBox)pan.Controls[0];
        cbx.SelectedIndex = -1;

        pan.Enabled = false;
      }
      else {
        pan.Enabled = true;
      }
    }

    private void HandleTimerTick() {
      var currentDateTime = GetCurrentDateTime();

      var hours = currentDateTime.Hour.ToString(CultureInfo.InvariantCulture);
      if (currentDateTime.Hour < 10)
        hours = "0" + currentDateTime.Hour;

      var minutes = currentDateTime.Minute.ToString(CultureInfo.InvariantCulture);
      if (currentDateTime.Minute < 10)
        minutes = "0" + currentDateTime.Minute;

      var seconds = currentDateTime.Second.ToString(CultureInfo.InvariantCulture);
      if (currentDateTime.Second < 10)
        seconds = "0" + currentDateTime.Second;

      lblCurrentDateTime.Text = StringF.GetTimeZoneShortName(AppModuleInfo.Parameters.LocalTimeZone) + @": " + hours + @":" + minutes + @":" + seconds;
    }

    private void HandleKeyDown(KeyEventArgs e) {
      if (e.KeyCode == Keys.F5) {
        txtCustomerId.Focus();
        txtCustomerId.SelectAll();
      }

      if (!CustomerInfoRetrieved || ModifierKeys != Keys.Control || ((e.KeyCode <= Keys.D0 || e.KeyCode >= Keys.D9) && (e.KeyCode <= Keys.NumPad0 || e.KeyCode >= Keys.NumPad9))) return;
      switch (e.KeyCode) {
        case Keys.D1:
        case Keys.NumPad1:
          tabCustomer.SelectedIndex = 0;
          break;
        case Keys.D2:
        case Keys.NumPad2:
          tabCustomer.SelectedIndex = 1;
          break;
        case Keys.D3:
        case Keys.NumPad3:
          tabCustomer.SelectedIndex = 2;
          break;
        case Keys.D4:
        case Keys.NumPad4:
          tabCustomer.SelectedIndex = 3;
          break;
        case Keys.D5:
        case Keys.NumPad5:
          tabCustomer.SelectedIndex = 4;
          break;
        case Keys.D6:
        case Keys.NumPad6:
          tabCustomer.SelectedIndex = 5;
          break;
        case Keys.D7:
        case Keys.NumPad7:
          tabCustomer.SelectedIndex = 6;
          break;
        case Keys.D8:
        case Keys.NumPad8:
          tabCustomer.SelectedIndex = 7;
          break;
        case Keys.D9:
        case Keys.NumPad9:
          tabCustomer.SelectedIndex = 8;
          break;
      }
    }

    private void SetModuleInMaintenance() {
      if (InvokeRequired) {
        Invoke(new MaintenanceAction(SetModuleInMaintenance));
      }
      else {
        SaveCustomerInfo();
        Close();
      }
    }

    private void HandleTeasersShowOption() {
      using (var teasersForm = new FrmCustomerAvailableTeasers(txtCustomerId.Text, AppModuleInfo)) {
        teasersForm.Icon = Icon;
        teasersForm.ShowDialog();
      }
    }

    private void HorseRacingLimitsShow() {
      using (var horsePermissionsFrm = new FrmCustomerHorseLimitsAndTrackPermissions(this)) {
        horsePermissionsFrm.Icon = Icon;
        horsePermissionsFrm.ShowDialog();
      }
    }

    private void InsertNewCustomer(string customerId) {
      using (var cust = new Customers(AppModuleInfo)) {

        cust.InsertNewCustomer(customerId);
      }
    }

    private void RetrieveAgentInformation() {
      using (var agent = new Agents(AppModuleInfo)) {
        var result = agent.GetAgentInfo(CustomerId).FirstOrDefault();

        if (result != null) {
          chbDistributeFunds.Checked = result.DistributeToMasterFlag == "Y";
          txtCasinoFee.Text = result.CasinoFeePercent.ToString(CultureInfo.InvariantCulture);
          txtAgentCommision.Text = result.CommissionPercent.ToString(CultureInfo.InvariantCulture);
          chbIncludeFreePlayLossesinDistribution.Checked = result.IncludeFpLossesFlag == "Y";
          txtCallUnit.Text = result.HeadCountRate.ToString(CultureInfo.InvariantCulture);
          txtInternetOnly.Text = result.InetHeadCountRate.ToString(CultureInfo.InvariantCulture);
          txtCasino.Text = result.CasinoHeadCountRate.ToString(CultureInfo.InvariantCulture);

          //CustomerComboBox cB = new CustomerComboBox(moduleInfo);

          if (panMasterAgentDropDown.Controls.Count > 0) {
            panMasterAgentDropDown.Controls.RemoveAt(0);
          }

          panMasterAgentDropDown.Controls.Add(_cB.MasterAgents(result.MasterAgentID, this));

          _masterAgentId = result.MasterAgentID;

          chbChangeCreditLimit.Checked = result.ChangeCreditLimitFlag == "Y";
          chbSuspendWagering.Checked = result.SuspendWageringFlag == "Y";
          chbManageLines.Checked = result.ManageLinesFlag == "Y";
          chbChangeWagerLimits.Checked = result.ChangeWagerLimitFlag == "Y";
          chbAddNewAccounts.Checked = result.AddNewAccountFlag == "Y";
          chbChangeTempCredit.Checked = result.ChangeTempCreditFlag == "Y";
          chbUpdateComments.Checked = result.UpdateCommentsFlag == "Y";
          chbChangeSettleFigure.Checked = result.ChangeSettleFigureFlag == "Y";
          chbSetMinimumBetAmt.Checked = result.SetMinimumBetAmountFlag == "Y";
          txtIdPrefix.Text = result.CustomerIDPrefix;
          chbEnterDepositsWithdrawals.Checked = result.EnterTransactionFlag == "Y";
          chbEnterBettingAdjs.Checked = result.EnterBettingAdjustmentFlag == "Y";
          GetCommisionType(result.CommissionType);

          txtAgentCommision.Text = result.CommissionPercent.ToString(CultureInfo.InvariantCulture);
          chbIncludeFreePlayLossesinDistribution.Checked = result.IncludeFpLossesFlag == "Y";

          chbIncludeFreePlayLossesinDistribution.Visible = chbCustomerIsAgent.Checked;


          _customerInfo.LoadCustomerAsAgentInfoToStructFromDb(result, (CurrentMakeUp / 100).ToString(CultureInfo.InvariantCulture),
                                                             _distributionDate);
        }
      }
    }

    private void GetCommisionType(string p) {
      switch (p) {
        case "A":
          rdoAffiliateSplit.Checked = true;
          break;

        case "R":
          rdoAgentRedFigure.Checked = true;
          break;

        case "Q":
          rdoAffiliateWeeklyProfit.Checked = true;
          break;

        case "P":
          rdoAgentWeeklyProfit.Checked = true;
          break;

        case "T":
          rdoAffiliateRedFigure.Checked = true;
          break;

        case "S":
          rdoAgentSplit.Checked = true;
          break;
      }

    }

    private String GetCommisionTypeCodeFromSelected() {
      var commType = "";

      if (panAffiliate.Controls.Count <= 0) return commType;
      foreach (Control ctrl in panAffiliate.Controls) {
        if (ctrl.GetType().ToString() != "System.Windows.Forms.RadioButton") continue;
        var rad = (RadioButton)ctrl;
        var radName = rad.Name;
        var radChecked = rad.Checked;

        if (radChecked) {

          switch (radName) {
            case "rdoAffiliateSplit":
              commType = "A";
              break;

            case "rdoAgentRedFigure":
              commType = "R";
              break;

            case "rdoAffiliateWeeklyProfit":
              commType = "Q";
              break;

            case "rdoAgentWeeklyProfit":
              commType = "P";
              break;

            case "rdoAffiliateRedFigure":
              commType = "T";
              break;

            case "rdoAgentSplit":
              commType = "S";
              break;
          }
        }
      }

      return commType;
    }

    private void DeleteCustomerFromDataBase(string customerId) {
      using (var cust = new Customers(AppModuleInfo)) {
        if (AgentTypeFromDb != Agents.AGENT_TYPE && AgentTypeFromDb != Agents.MASTERAGENT_TYPE) {
          cust.DeleteCustomer(customerId);
          CustomerInfoRetrieved = false;
          txtCustomerId.Text = "";
          ShowHideAllTabs(false);
          panFirstAgentSettings.Visible = false;
        }
        else {
          if (MessageBox.Show(@"Delete Agent and all their customers?", @"Delete Agents Cascade", MessageBoxButtons.OKCancel) != DialogResult.OK) return;
          cust.DeleteAgentCascade(customerId);
          CustomerInfoRetrieved = false;
          txtCustomerId.Text = "";
          ShowHideAllTabs(false);
          if (_showAgentPlayersListTab.IsActive()) {
            var tabPage = (TabPage)Controls.Find("tbPlayers", true).FirstOrDefault();
            if (tabPage != null)
              tabCustomer.TabPages.Remove(tabPage);
            lklReturnToAgent.Text = "";
            lklReturnToAgent.AccessibleName = "";
            lklReturnToAgent.Visible = false;
          }

          panFirstAgentSettings.Visible = false;
        }
      }
    }

    private int GetCustomerTransactionsCount(string customerId) {
      int tranCount;
      using (var tran = new Transactions(AppModuleInfo)) {
        tranCount = tran.GetCustomerTransactionsCount(customerId);

      }
      return tranCount;
    }

    private void FindCustomerShow() {
      using (var frmSc = new FrmSearchCustomer(this)) {
        frmSc.Icon = Icon;
        frmSc.ShowDialog();
      }
    }

    private void ShowHideAllTabs(bool tabvisible) {
      foreach (TabPage tbP in tabCustomer.TabPages) {
        foreach (Control ctrl in tbP.Controls) {
          if (ctrl.GetType().ToString() != "System.Windows.Forms.Panel") continue;
          var pan = (Panel)ctrl;
          pan.Visible = tabvisible;
        }
      }

      var tbCtrl = tabCustomer;
      tbCtrl.SelectTab(0);
    }

    private void PopulateCustomerPerformanceGv(string performanceViewMode) {

      dgvwCustomerPerformance.DataSource = null;

      var gvDataSource = _performanceByWagerType.GetCustomerPerformance();

      if (gvDataSource != null) {

        WritePerformanceGrvwHeaders(dgvwCustomerPerformance);
        foreach (var res in gvDataSource) {
          dgvwCustomerPerformance.Rows.Add(res.period, FormatNumber(double.Parse(res.Lost), false, true), FormatNumber(double.Parse(res.Won), false, true), FormatNumber(double.Parse(res.Net), false, true));
        }


        txtPerfViewModeLegend.Text = performanceViewMode;

        dgvwCustomerPerformance.ClearSelection();
        dgvwCustomerPerformance.FirstDisplayedScrollingRowIndex = gvDataSource.Count - 1;
      }
      else {
        txtPerfViewModeLegend.Text = @"Problems reading data from Database";
      }
    }

    private static void WritePerformanceGrvwHeaders(DataGridView caller) {
      caller.DataSource = null;

      var dgrvwColNames = new ArrayList();

      foreach (DataGridViewTextBoxColumn col in caller.Columns) {
        dgrvwColNames.Add(col.Name);
      }

      foreach (String str in dgrvwColNames) {
        caller.Columns.Remove(str);
      }


      var titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = @"Period",
        Width = 150,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleRight }
      };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = @"Lost $",
        Width = 105,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleRight }
      };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = @"Won $",
        Width = 105,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleRight }
      };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = @"Net $",
        Width = 105,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleRight }
      };
      caller.Columns.Add(titleColumn);
    }

    private static void WriteTransactionsGrvwHeaders(DataGridView caller) {
      caller.DataSource = null;

      var dgrvwColNames = new ArrayList();

      foreach (DataGridViewTextBoxColumn col in caller.Columns) {
        dgrvwColNames.Add(col.Name);
      }

      foreach (String str in dgrvwColNames) {
        caller.Columns.Remove(str);
      }


      var titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Date", Width = 125 };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Doc #", Width = 75 };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = @"Description",
        Width = 225,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleLeft }
      };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = @"Credit",
        Width = 75,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleRight }
      };
      var style = new DataGridViewCellStyle { Format = "###,###" };
      titleColumn.DefaultCellStyle = style;
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = @"Debit",
        Width = 75,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleRight }
      };
      style = new DataGridViewCellStyle { Format = "###,###" };
      titleColumn.DefaultCellStyle = style;
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = @"Balance",
        Width = 100,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleRight }
      };
      style = new DataGridViewCellStyle { Format = "###,###" };
      titleColumn.DefaultCellStyle = style;
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"tranType", Width = 10, Visible = false };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"dailyFigureDate", Width = 100, Visible = false };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"GradeNumber", Width = 100, Visible = false };
      caller.Columns.Add(titleColumn);
      foreach (DataGridViewColumn column in caller.Columns) {
        column.SortMode = DataGridViewColumnSortMode.NotSortable;
      }
    }

    private void SelectLastVerifiedDocumentNumber() {
      var rCnt = dgvwTransactions.Rows.Count;

      for (var i = 0; i < rCnt; i++) {
        var documentNumber = int.Parse(dgvwTransactions.Rows[i].Cells[1].Value.ToString());
        if (documentNumber == _lastVerifiedDocNumber) {
          dgvwTransactions.Rows[i].Selected = true;

          dgvwTransactions.FirstDisplayedScrollingRowIndex = i;
          break;
        }
      }
    }

    private void ClearAgentTabElementContents() {
      try {
        foreach (TabPage tbP in tabCustomer.TabPages) {
          if (tbP.Text != @"Agent") continue;
          foreach (Control ctrl in tbP.Controls) {
            if (ctrl.GetType().ToString() != "System.Windows.Forms.Panel") continue;
            foreach (Control childCtrl in ctrl.Controls) {
              if ((childCtrl.GetType().ToString() == "System.Windows.Forms.Panel" || childCtrl.GetType().ToString() == "System.Windows.Forms.GroupBox") && childCtrl.Name != "panFirstAgentSettings") {
                foreach (Control child1Ctrl in childCtrl.Controls) {
                  Common.ClearAdditionalControlContent(child1Ctrl);
                  if (child1Ctrl.GetType().ToString() != "System.Windows.Forms.Panel") continue;
                  if (child1Ctrl.Controls.Count <= 0) continue;
                  foreach (var control in child1Ctrl.Controls) {
                    if (control.GetType().ToString() == "System.Windows.Forms.RadioButton") {
                      ((RadioButton)control).Checked = false;
                    }
                  }
                }
              }
              Common.ClearControlContent(childCtrl);
            }
          }
        }
      }
      catch (Exception e) {
        Log(e);
      }
    }

    private void GetWagerLimitsSports() {
      cmbWLSports.SelectedIndexChanged -= cmbWLSports_SelectedIndexChanged;
      Common.GetWagerLimitsSports(AppModuleInfo, cmbWLSports, WagerLimits, _customerTempLimits.IsActive());
      cmbWLSports.SelectedIndexChanged += cmbWLSports_SelectedIndexChanged;
      if (cmbWLSports.Items.Count > 0) cmbWLSports.SelectedIndex = 0;
    }

    private string GetSelectedWlSport() {
      if (cmbWLSports.Items.Count > 0 && cmbWLSports.SelectedIndex >= 0) {
        return cmbWLSports.SelectedValue.ToString();
      }
      return string.Empty;
    }

    private void GetCustomerWagerLimitsInfo() {
      if (WagerLimits == null) {
        using (var cust = new Customers(AppModuleInfo)) {
          WagerLimits = cust.GetCustomerWagerLimits(CustomerId).ToList();
        }
      }
      dgvwWagerLimitsDetails.CancelEdit();
      if (dgvwWagerLimitsDetails.Columns.Count < 15) {
        var dgrvwColNames = new ArrayList();

        for (var i = 0; i < dgvwWagerLimitsDetails.Columns.Count; i++) {
          dgrvwColNames.Add(dgvwWagerLimitsDetails.Columns[i].Name);
        }

        //dgvwWagerLimitsDetails.CancelEdit();
        foreach (String str in dgrvwColNames) {
          dgvwWagerLimitsDetails.Columns.Remove(str);
        }
        var dgvCmb = new DataGridViewCheckBoxColumn { ValueType = typeof(bool), Name = "SportEnabled", HeaderText = @"Enabled", Width = 50 };
        dgvCmb.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
        dgvwWagerLimitsDetails.Columns.Add(dgvCmb);

        dgvCmb = new DataGridViewCheckBoxColumn { ValueType = typeof(bool), Name = "OverrideCicleLimits", HeaderText = @"Ovr Circles", Width = 50 };
        dgvCmb.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

        GetCustomerActions();
        var shouldBeReadOnly = _actions.Any(a => a.Name.Trim().ToLower() == "mover" && a.RestrictedActionId != null);

        dgvCmb.ReadOnly = shouldBeReadOnly;
        dgvwWagerLimitsDetails.Columns.Add(dgvCmb);

        dgvwWagerLimitsDetails.Columns.Add("SportSubSport", "Sport - SubSport");
        dgvwWagerLimitsDetails.Columns.Add("Period", "Period");
        dgvwWagerLimitsDetails.Columns.Add("Type", "Type");
        dgvwWagerLimitsDetails.Columns.Add("CULimit", "CU Limit");
        dgvwWagerLimitsDetails.Columns.Add("InetLimit", "Inet Limit");
        dgvwWagerLimitsDetails.Columns.Add("WagerTypeCode", "Wager Type Code");
        dgvwWagerLimitsDetails.Columns.Add("PeriodNumber", "PeriodNumber");

        _customerTempLimits.AddDetailLimitsColumns(dgvwWagerLimitsDetails);
        dgvwWagerLimitsDetails.Columns.Add("SportType", "Sport Type");
        dgvwWagerLimitsDetails.Columns.Add("SportSubType", "Sport SubType");

        dgvwWagerLimitsDetails.Columns[SPORT_SUB_SPORT_INDEX].Width = GetColumnMaxLen(dgvwWagerLimitsDetails);
        dgvwWagerLimitsDetails.Columns[SPORT_SUB_SPORT_INDEX].ReadOnly = true;
        dgvwWagerLimitsDetails.Columns[PERIOD_INDEX].Width = 75;
        dgvwWagerLimitsDetails.Columns[PERIOD_INDEX].ReadOnly = true;
        dgvwWagerLimitsDetails.Columns[TYPE_INDEX].Width = 70;
        dgvwWagerLimitsDetails.Columns[TYPE_INDEX].ReadOnly = true;

        dgvwWagerLimitsDetails.Columns[CU_LIMIT_INDEX].Width = 70;
        dgvwWagerLimitsDetails.Columns[INET_LIMIT_INDEX].Width = 70;
        dgvwWagerLimitsDetails.Columns[WAGER_TYPE_CODE_INDEX].Visible = false;
        dgvwWagerLimitsDetails.Columns[PERIOD_NUMBER_INDEX].Visible = false;

        dgvwWagerLimitsDetails.Columns[SPORT_INDEX].Visible = false;
        dgvwWagerLimitsDetails.Columns[SUB_SPORT_INDEX].Visible = false;

        var chbDisableSports = new CheckBox { Size = new Size(15, 15), BackColor = Color.Transparent, Name = "chbDisableSports", Padding = new Padding(0), Margin = new Padding(0), Text = "", Checked = true };

        chbDisableSports.CheckedChanged += chbDisableSports_CheckedChanged;

        dgvwWagerLimitsDetails.Controls.Add(chbDisableSports);
        DataGridViewHeaderCell header = dgvwWagerLimitsDetails.Columns[SPORT_ENABLED_INDEX].HeaderCell;
        chbDisableSports.Location = new Point(
            header.ContentBounds.Left + 1 + (header.ContentBounds.Right - header.ContentBounds.Left - chbDisableSports.Size.Width) / 2,
            header.ContentBounds.Top + (header.ContentBounds.Bottom - header.ContentBounds.Top + chbDisableSports.Size.Height) / 2
        );
        TakeActionOnDisableSportsCheckBox(cmbWLSports.SelectedIndex == 0 ? "Disable" : "Enable");
      }

      PopulateWagerLimitsGrid();
      GetDisabledSportsCount();

      if (!_customerTempLimits.IsActive()) {
        dgvwWagerLimitsDetails.Width = panWagerLimitsDetails.Width = 600;
      }
      else {
        dgvwWagerLimitsDetails.Width = panWagerLimitsDetails.Width = 735;
      }
      _customerInfo.LoadcustomerWagerLimitsToStructFromDb(WagerLimits);
      dgvwWagerLimitsDetails.ClearSelection();

      foreach (DataGridViewColumn col in dgvwWagerLimitsDetails.Columns) {
        col.SortMode = DataGridViewColumnSortMode.NotSortable;
      }
    }

    private int GetColumnMaxLen(DataGridView gridView) {
      var ret = 0;
      var text = "";
      if (WagerLimits != null) {
        foreach (var wl in WagerLimits) {
          var len = wl.SportSubSport.Trim().Length;
          if (len > ret) {
            ret = len;
            text = wl.SportSubSport.Trim();
          }
        }
        Size textSize = TextRenderer.MeasureText(text, gridView.Font);
        return textSize.Width + (ret - (ret < 38 ? 0 : 38));
      }
      return 250;
    }

    private void PopulateWagerLimitsGrid() {
      dgvwWagerLimitsDetails.Rows.Clear();
      var selectedSport = GetSelectedWlSport();
      List<spCstGetCustomerWagerLimits_Result> filteredLimits;
      if (cmbWLSports.SelectedIndex > 0) {
        var selectedSubSport = string.Empty;
        filteredLimits = WagerLimits.Where(a => a.sportType.Trim() == selectedSport && (selectedSubSport == string.Empty || a.SportSubType.Contains(selectedSubSport))).ToList();
      }
      else filteredLimits = WagerLimits;

      foreach (var res in filteredLimits) {

        var cuLimit = double.Parse(res.CuLimit.ToString(CultureInfo.InvariantCulture)) / 100;
        var inetLimit = double.Parse(res.InetLimit.ToString(CultureInfo.InvariantCulture)) / 100;
        var tempCuLimit = double.Parse(res.TempCuLimit.ToString(CultureInfo.InvariantCulture)) / 100;
        var tempInetLimit = double.Parse(res.TempInetLimit.ToString(CultureInfo.InvariantCulture)) / 100;
        var tempLimitsExpiration = res.TempLimitsExpiration == null ? "" : res.TempLimitsExpiration.Value.ToShortDateString();
        if (ModifiedWagerLimits != null && ModifiedWagerLimits.Count > 0) {
          var mwl = (from wl in ModifiedWagerLimits
                     where wl.SportType.Trim() == res.sportType.Trim()
                           && wl.SportSubType.Trim() == res.SportSubType.Trim()
                           && wl.Period == res.Period
                           && wl.WagerType == res.WagerType
                     select wl).FirstOrDefault();
          if (mwl != null) {
            cuLimit = double.Parse(mwl.CuLimit);
            inetLimit = double.Parse(mwl.InetLimit);
            tempCuLimit = double.Parse(mwl.TempCuLimit);
            tempInetLimit = double.Parse(mwl.TempInetLimit);
            tempLimitsExpiration = mwl.TempLimitsExpiration;
          }

        }
        dgvwWagerLimitsDetails.Rows.Add(res.SportIsEnabledTmp, res.OverrideCircleTmp, res.SportSubSport, res.Period, res.WagerTypeDesc, FormatNumber(cuLimit, false), FormatNumber(inetLimit, false), res.WagerType, res.PeriodNumber, tempCuLimit, tempInetLimit, tempLimitsExpiration, res.sportType.Trim(), res.SportSubType.Trim());
      }
    }

    private void GetDisabledSportsCount() {
      if (WagerLimits == null)
        return;
      int disabledSportsCnt;
      var selectedSport = GetSelectedWlSport();
      var selectedSubSport = string.Empty;

      if (cmbWLSports.SelectedIndex == 0) {
        disabledSportsCnt = (from wl in WagerLimits where wl.SportIsEnabledTmp == false select wl.sportType).Count();
      }
      else {
        disabledSportsCnt = (from wl in WagerLimits
                             where wl.SportIsEnabledTmp == false && wl.sportType.Trim() == selectedSport && (selectedSubSport == string.Empty || wl.SportSubType.Trim() == selectedSubSport)
                             select wl.sportType).Count();
      }

      lblDisabledSportsCount.Visible = disabledSportsCnt > 0;
      lblDisabledSportsCount.Text = @"Disabled Sports Cnt = " + disabledSportsCnt;
    }

    private void GetCustomervigDiscountInfo() {
      using (var cust = new Customers(AppModuleInfo)) {
        var gvSet = cust.GetCustomerVigDiscountOptions(txtCustomerId.Text).ToList();
        if (gvSet.Any()) {
          _customerInfo.LoadcustomerVigDiscountOptionsToStructFromDb(gvSet);

          dgvwVigDiscount.Columns.Clear();

          var column = new DataGridViewColumn {
            Name = "Sport",
            HeaderText = @"Sport",
            ReadOnly = true,
            Width = 150
          };
          DataGridViewCell cell = new DataGridViewTextBoxCell();
          cell.Style.BackColor = Color.White;
          cell.Style.ForeColor = Color.Black;
          column.CellTemplate = cell;
          dgvwVigDiscount.Columns.Add(column);

          column = new DataGridViewColumn { Name = "Period", HeaderText = @"Period", ReadOnly = true };
          cell = new DataGridViewTextBoxCell { Style = { BackColor = Color.White, ForeColor = Color.Black } };
          column.CellTemplate = cell;
          dgvwVigDiscount.Columns.Add(column);

          column = new DataGridViewColumn {
            Name = "WagerTypeCode",
            HeaderText = @"Wager Type Code",
            ReadOnly = true,
            Visible = false
          };
          cell = new DataGridViewTextBoxCell { Style = { BackColor = Color.White, ForeColor = Color.Black } };
          column.CellTemplate = cell;
          dgvwVigDiscount.Columns.Add(column);

          column = new DataGridViewColumn {
            Name = "WagerType",
            HeaderText = @"Wager Type",
            ReadOnly = true,
            Visible = true
          };
          cell = new DataGridViewTextBoxCell { Style = { BackColor = Color.White, ForeColor = Color.Black } };
          column.CellTemplate = cell;
          dgvwVigDiscount.Columns.Add(column);

          column = new DataGridViewColumn {
            Name = "CuVigDisc",
            HeaderText = @"Cu Vig Disc %",
            ReadOnly = false,
            Visible = true,
            Width = 110
          };
          cell = new DataGridViewTextBoxCell { Style = { BackColor = Color.White, ForeColor = Color.Black } };
          column.CellTemplate = cell;
          dgvwVigDiscount.Columns.Add(column);

          var calendarCol = new GridViewCellCalendar.CalendarColumn {
            Name = "CuExpDate",
            HeaderText = @"Cu Exp Date",
            ReadOnly = false,
            Visible = true,
            Width = 110,
            DefaultCellStyle = { Format = "mm/dd/yyyy" }
          };

          dgvwVigDiscount.Columns.Add(calendarCol);

          column = new DataGridViewColumn {
            Name = "InetVigDisc",
            HeaderText = @"Inet Vig Disc %",
            ReadOnly = false,
            Visible = true,
            Width = 110
          };
          cell = new DataGridViewTextBoxCell { Style = { BackColor = Color.White, ForeColor = Color.Black } };
          column.CellTemplate = cell;

          dgvwVigDiscount.Columns.Add(column);

          calendarCol = new GridViewCellCalendar.CalendarColumn {
            Name = "InetExpDate",
            HeaderText = @"Inet Exp Date",
            ReadOnly = false,
            Visible = true,
            Width = 110,
            DefaultCellStyle = { Format = "mm/dd/yyyy" }
          };

          dgvwVigDiscount.Columns.Add(calendarCol);

          dgvwVigDiscount.Rows.Clear();

          foreach (var item in gvSet) {
            DateTime? cuExpDate = null;
            DateTime parsedDate;
            if (!string.IsNullOrEmpty(item.CU_Exp_Date)) {
              if (DateTime.TryParse(item.CU_Exp_Date, out parsedDate)) {
                cuExpDate = parsedDate;
              }
            }

            DateTime? inetExpDate = null;
            if (!string.IsNullOrEmpty(item.Inet_Exp_Date)) {
              if (DateTime.TryParse(item.Inet_Exp_Date, out parsedDate)) {
                inetExpDate = parsedDate;
              }
            }

            dgvwVigDiscount.Rows.Add(item.Sport, item.Period, item.wagerType, item.Wager_Type, item.CU_Vig_Disc__, cuExpDate, item.Inet_Vig_Disc__, inetExpDate);
          }

          FormF.PreventDataGridViewColumnsToBeSortedAndResized(dgvwVigDiscount);
        }

        lblVigDiscountNote.Visible = true;
      }
    }

    private void GetCustomerStoreOfferingInfo() {
      using (var cu = new Customers(AppModuleInfo)) {
        panStoresList.Controls.Add(_cB.Stores(_currentCustStore.Trim(), this));

        var custShadeGroups = cu.GetCustomerShadeGroups(txtCustomerId.Text).ToList();

        var storeShadeGroups = cu.GetStoreShadeGroups(_currentCustStore.Trim()).ToList();

        if (panShadeGroups.Controls.Count > 0) {
          panShadeGroups.Controls.RemoveAt(0);
        }

        panShadeGroups.Controls.Add(PopulateCustomerShadeGroupsObj(storeShadeGroups, custShadeGroups));
        _customerInfo.LoadCustomerStoreShadeGroupsToStructFromDb(panShadeGroups, custShadeGroups);

        if (panParlayTypes.Controls.Count > 0) {
          panParlayTypes.Controls.RemoveAt(0);
        }

        panParlayTypes.Controls.Add(_cB.ParlayTypes(_currentCustParlay));
      }

    }

    private spAgGetAgent_Result BuildAgentObject(String agentId, String agentType) {
      if (agentId == null) throw new ArgumentNullException("agentId");
      var agent = new spAgGetAgent_Result { AgentID = agentId, CommissionType = GetCommisionTypeCodeFromSelected() };
      int intVal;
      if (txtAgentCommision.Text != "") {
        int.TryParse(txtAgentCommision.Text.Replace(",", ""), out intVal);
        agent.CommissionPercent = intVal;
      }

      if (txtCasinoFee.Text != "") {
        int.TryParse(txtCasinoFee.Text.Replace(",", ""), out intVal);
        agent.CasinoFeePercent = intVal;
      }
      agent.MasterAgentID = FormF.GetSelectedItemInDropDownFromPanel(this, "panMasterAgentDropDown");
      agent.DistributeToMasterFlag = chbDistributeFunds.Checked ? "Y" : "N";
      agent.DistributeNoFundsFlag = "";
      if (agentType == Agents.AGENT_TYPE) {
        agent.IncludeFpLossesFlag = chbIncludeFreePlayLossesinDistribution.Checked ? "Y" : "N";
      }
      double doubleVal;
      if (txtCallUnit.Text != "") {
        double.TryParse(txtCallUnit.Text.Replace(",", ""), out doubleVal);
        agent.HeadCountRate = double.Parse(txtCallUnit.Text.Replace(",", ""));
      }

      if (txtInternetOnly.Text != "") {
        double.TryParse(txtInternetOnly.Text.Replace(",", ""), out doubleVal);
        agent.InetHeadCountRate = double.Parse(txtInternetOnly.Text.Replace(",", ""));
      }

      if (txtCasino.Text != "") {
        double.TryParse(txtCasino.Text.Replace(",", ""), out doubleVal);
        agent.CasinoHeadCountRate = doubleVal;
      }

      agent.ChangeTempCreditFlag = chbChangeTempCredit.Checked ? "Y" : "N";
      agent.SuspendWageringFlag = chbSuspendWagering.Checked ? "Y" : "N";
      agent.UpdateCommentsFlag = chbUpdateComments.Checked ? "Y" : "N";
      agent.EnterTransactionFlag = chbEnterDepositsWithdrawals.Checked ? "Y" : "N";
      agent.ManageLinesFlag = chbManageLines.Checked ? "Y" : "N";
      agent.ChangeSettleFigureFlag = chbChangeSettleFigure.Checked ? "Y" : "N";
      agent.ChangeWagerLimitFlag = chbChangeWagerLimits.Checked ? "Y" : "N";
      agent.EnterBettingAdjustmentFlag = chbEnterBettingAdjs.Checked ? "Y" : "N";
      agent.SetMinimumBetAmountFlag = chbSetMinimumBetAmt.Checked ? "Y" : "N";
      agent.ChangeCreditLimitFlag = chbChangeCreditLimit.Checked ? "Y" : "N";
      agent.AddNewAccountFlag = chbAddNewAccounts.Checked ? "Y" : "N";
      agent.CustomerIDPrefix = txtIdPrefix.Text.Trim();

      return agent;
    }

    private IEnumerable<CustomerInfoUpdater.AgentInfo> LoadCustomerAsAgentInfoToStructFromFrm() {
      _customerAsAgentInfoFromFrmList = new List<CustomerInfoUpdater.AgentInfo>();
      var dbAgentStruct = new CustomerInfoUpdater.AgentInfo {
        AddNewAccountFlag = chbAddNewAccounts.Checked ? "Y" : "N",
        AgentId = "",
        CasinoFeePercent = txtCasinoFee.Text,
        CasinoHeadCountRate = txtCasino.Text,
        ChangeCreditLimitFlag = chbChangeCreditLimit.Checked ? "Y" : "N",
        ChangeSettleFigureFlag = chbChangeSettleFigure.Checked ? "Y" : "N",
        ChangeTempCreditFlag = chbChangeTempCredit.Checked ? "Y" : "N",
        ChangeWagerLimitFlag = chbChangeWagerLimits.Checked ? "Y" : "N",
        CommissionPercent = txtAgentCommision.Text,
        CommissionType = GetCommisionTypeCodeFromSelected(),
        CustomerIdPrefix = txtIdPrefix.Text.Trim(),
        DistributeNoFundsFlag = "",
        DistributeToMasterFlag = chbDistributeFunds.Checked ? "Y" : "N",
        EnterBettingAdjustmentFlag = chbEnterBettingAdjs.Checked ? "Y" : "N",
        EnterTransactionFlag = chbEnterDepositsWithdrawals.Checked ? "Y" : "N",
        HeadCountRate = txtCallUnit.Text,
        IncludeFpLossesFlag = chbIncludeFreePlayLossesinDistribution.Checked ? "Y" : "N",
        InetHeadCountRate = txtInternetOnly.Text,
        ManageLinesFlag = chbManageLines.Checked ? "Y" : "N",
        MasterAgentId = FormF.GetSelectedItemInDropDownFromPanel(this, "panMasterAgentDropDown"),
        SetMinimumBetAmountFlag = chbSetMinimumBetAmt.Checked ? "Y" : "N",
        SuspendWageringFlag = chbSuspendWagering.Checked ? "Y" : "N",
        UpdateCommentsFlag = chbUpdateComments.Checked ? "Y" : "N"
      };

      if (txtCurrentMakeup.Text != "") {
        dbAgentStruct.CurrentMakeUp = txtCurrentMakeup.Text.Replace(",", "");
      }
      _customerAsAgentInfoFromFrmList.Add(dbAgentStruct);

      return _customerAsAgentInfoFromFrmList;
    }

    private IEnumerable<CustomerInfoUpdater.CustomerInfo> LoadCustomerInfoToStructFromFrm() {
      _customerInfoFromFrmList = new List<CustomerInfoUpdater.CustomerInfo>();
      var frmCustStruct = new CustomerInfoUpdater.CustomerInfo { Active = (chbSuspendSports.Checked ? "N" : "Y") };

      if (txtAdress2.Text.Trim().Length > 0) {
        frmCustStruct.Address = txtAddress.Text.Trim() + Environment.NewLine + txtAdress2.Text.Trim();
      }
      else {
        frmCustStruct.Address = txtAddress.Text.Trim();
      }
      frmCustStruct.AgentId = FormF.GetSelectedItemInDropDownFromPanel(this, "panAgentsList");
      frmCustStruct.AgentType = GetAgentType(chbCustomerIsAgent, chbCustomerIsMasterAgent);
      frmCustStruct.ApplyDetailWagerLimitsFlag = "";
      frmCustStruct.AutoAgentShadeGroupFlag = (chbShadeGroupsAutoCreate.Checked ? "Y" : "N");
      frmCustStruct.BaseballAction = GetSelectedItemInRadiosGroupFromGroupBox("grpBaseball");
      frmCustStruct.BusinessPhone = txtBusinessPhone.Text;
      frmCustStruct.CasinoActive = (chbCasino.Checked ? "N" : "Y");
      frmCustStruct.City = txtCity.Text;
      frmCustStruct.Comments = txtGeneralComments.Text;
      frmCustStruct.CommentsForCustomer = txtCommentsForCustomer.Text;
      frmCustStruct.CommentsForTw = txtCommentsForTW.Text;
      frmCustStruct.ConfirmationDelay = txtSecondsToDelay.Text;

      var fNum = NumberF.ParseDouble(txtMaxPropBet.Text);
      frmCustStruct.ContestMaxBet = (fNum * 100).ToString(CultureInfo.InvariantCulture);
      frmCustStruct.Country = txtCountry.Text;//should be GetSelectedItemInDropDownFromPanel("panCountriesList"));
      frmCustStruct.CreditAcctFlag = (chbCreditCustomer.Checked ? "Y" : "N");

      fNum = NumberF.ParseDouble(txtCreditLimit.Text);
      frmCustStruct.CreditLimit = (fNum * 100).ToString(CultureInfo.InvariantCulture);

      var iNum = NumberF.ParseDouble(txtCuMinBet.Text);
      frmCustStruct.CuMinimumWager = (iNum * 100).ToString(CultureInfo.InvariantCulture);
      frmCustStruct.Currency = FormF.GetSelectedItemInDropDownFromPanel(this, "panCurrencyCodesList");
      frmCustStruct.EasternLineFlag = (chbEasternLines.Checked ? "Y" : "N");
      frmCustStruct.EMail = txtEmail.Text;
      frmCustStruct.EnableRifFlag = (chbEnableRollingIfBets.Checked ? "Y" : "N");
      frmCustStruct.EnforceAccumWagerLimitsByLineFlag = (chbByLine.Checked ? "Y" : "N");
      frmCustStruct.EnforceAccumWagerLimitsFlag = (chbByGameSelection.Checked ? "Y" : "N");
      frmCustStruct.EstWager101To500Flag = (chbEstWagerAmount101to500.Checked ? "Y" : "N");
      frmCustStruct.EstWager501To1000Flag = (chbEstWagerAmount501To1000.Checked ? "Y" : "N");
      frmCustStruct.EstWager51To100Flag = (chbEstWagerAmount51to100.Checked ? "Y" : "N");
      frmCustStruct.EstWager5To50Flag = (chbEstWagerAmount5To50.Checked ? "Y" : "N");
      frmCustStruct.EstWagerOver1000Flag = (chbEstWagerAmountOver1000.Checked ? "Y" : "N");
      frmCustStruct.Fax = txtFax.Text;
      frmCustStruct.HalfPointCuBasketballDow = GetNumDayOfWeekByName(FormF.GetSelectedItemInDropDownFromPanel(this, "panCuBasketball"));
      frmCustStruct.HalfPointCuBasketballFlag = (chbCuBasketball.Checked ? "Y" : "N");
      frmCustStruct.HalfPointCuFootballDow = GetNumDayOfWeekByName(FormF.GetSelectedItemInDropDownFromPanel(this, "panCuFootball"));
      frmCustStruct.HalfPointCuFootballFlag = (chbCuFootball.Checked ? "Y" : "N");
      frmCustStruct.HalfPointInetBasketballDow = GetNumDayOfWeekByName(FormF.GetSelectedItemInDropDownFromPanel(this, "panInetBasketball"));
      frmCustStruct.HalfPointInetBasketballFlag = (chbInetBasketball.Checked ? "Y" : "N");
      frmCustStruct.HalfPointInetFootballDow = GetNumDayOfWeekByName(FormF.GetSelectedItemInDropDownFromPanel(this, "panInetFootball"));
      frmCustStruct.HalfPointInetFootballFlag = (chbInetFootball.Checked ? "Y" : "N");
      frmCustStruct.HalfPointWagerLimitFlag = (chbEnforceHalfPoint.Checked ? "Y" : "N");

      frmCustStruct.HalfPointFootballOn3Flag = (chbNFLFOOTBALLCostToBuyON3FreeHalfPont.Checked ? "Y" : "N");
      frmCustStruct.HalfPointFootballOff3Flag = (chbNFLFOOTBALLCostToBuyOFF3FreeHalfPont.Checked ? "Y" : "N");
      frmCustStruct.HalfPointFootballOn7Flag = (chbNFLFOOTBALLCostToBuyON7FreeHalfPont.Checked ? "Y" : "N");
      frmCustStruct.HalfPointFootballOff7Flag = (chbNFLFOOTBALLCostToBuyOFF7FreeHalfPont.Checked ? "Y" : "N");
      frmCustStruct.HalfPointDenyTotalsFlag = (chbNoFreeHalfPointInTotals.Checked ? "Y" : "N");

      fNum = NumberF.ParseDouble(txtMaxFreeHalfPoint.Text);
      frmCustStruct.HalfPointMaxBet = (fNum * 1).ToString(CultureInfo.InvariantCulture);
      frmCustStruct.HomePhone = txtHomePhone.Text;

      iNum = NumberF.ParseDouble(txtInetMinBet.Text);
      frmCustStruct.InetMinimumWager = (iNum * 100).ToString(CultureInfo.InvariantCulture);
      frmCustStruct.InetTarget = FormF.GetSelectedItemInDropDownFromPanel(this, "panInetTargetsList");
      frmCustStruct.InstantActionFlag = (chbShowOnInstantAction.Checked ? "Y" : "N");
      frmCustStruct.IntBaseballFlag = (InterestedInBaseball.Checked ? "Y" : "N");
      frmCustStruct.IntBasketballFlag = (chbInterestedInBasketBall.Checked ? "Y" : "N");
      frmCustStruct.IntFootballFlag = (InterestedInFootball.Checked ? "Y" : "N");
      frmCustStruct.IntHockeyFlag = (InterestedInHockey.Checked ? "Y" : "N");
      frmCustStruct.IntHorsesFlag = (InterestedInHorseRacing.Checked ? "Y" : "N");
      frmCustStruct.IntOtherFlag = (InterestedInOther.Checked ? "Y" : "N");
      frmCustStruct.LimitRifToRiskFlag = (chbLimitIfToRisk.Checked ? "Y" : "N");

      fNum = NumberF.ParseDouble(txtLossLimit.Text);
      frmCustStruct.LossCap = (fNum * 100).ToString(CultureInfo.InvariantCulture);

      fNum = NumberF.ParseDouble(txtNewMakeup.Text);
      frmCustStruct.NewCustMakeup = fNum.ToString(CultureInfo.InvariantCulture);

      frmCustStruct.NameFirst = txtNameFirst.Text;
      frmCustStruct.NameLast = txtNameLast.Text;
      frmCustStruct.NameMi = txtNameMi.Text;
      frmCustStruct.NoEmailFlag = (chbNoEmail.Checked ? "Y" : "N");
      frmCustStruct.NoMailFlag = (chbNoMail.Checked ? "Y" : "N");
      frmCustStruct.NoPhoneFlag = (chbNoPhone.Checked ? "Y" : "N");

      fNum = NumberF.ParseDouble(txtMaxParlayBet.Text);
      frmCustStruct.ParlayMaxBet = (fNum * 100).ToString(CultureInfo.InvariantCulture);
      frmCustStruct.EnforceParlayMaxBetFlag = (chbEnforceParlayBetLimit.Checked ? "Y" : "N");
      fNum = NumberF.ParseDouble(txtMaxParlayPayout.Text);
      frmCustStruct.ParlayMaxPayout = (fNum * 100).ToString(CultureInfo.InvariantCulture);
      frmCustStruct.ParlayName = FormF.GetSelectedItemInDropDownFromPanel(this, "panParlayTypes");
      frmCustStruct.Password = txtWageringPassword.Text;
      frmCustStruct.PayoutPassword = txtPayoutPassword.Text;
      frmCustStruct.PercentBook = txtChartPerc.Text;
      frmCustStruct.PriceType = GetPriceTypeCode(FormF.GetSelectedItemInDropDownFromPanel(this, "panPriceTypesList"));
      frmCustStruct.Promoter = txtPromoter.Text;
      frmCustStruct.ReferredBy = txtReferredBy.Text;

      fNum = NumberF.ParseDouble(txtSettleFigure.Text);
      frmCustStruct.SettleFigure = (fNum).ToString(CultureInfo.InvariantCulture);
      frmCustStruct.Source = txtSource.Text;
      frmCustStruct.State = GetStateCode(FormF.GetSelectedItemInDropDownFromPanel(this, "panStatesProvList"));
      frmCustStruct.StaticLinesFlag = (chbStaticLines.Checked ? "Y" : "N");

      fNum = NumberF.ParseDouble(txtMaxTeaserBet.Text);
      frmCustStruct.TeaserMaxBet = (fNum * 100).ToString(CultureInfo.InvariantCulture);
      frmCustStruct.EnforceTeaserMaxBetFlag = (chbEnforceTeaserBetLimit.Checked ? "Y" : "N");

      fNum = NumberF.ParseDouble(txtCreditIncrease.Text);
      frmCustStruct.TempCreditAdj = (fNum * 100).ToString(CultureInfo.InvariantCulture);
      frmCustStruct.TempCreditAdjExpDate = txtThroughCredIncrease.Text;

      fNum = NumberF.ParseDouble(txtTempQuickLimit.Text);
      frmCustStruct.TempWagerLimit = (fNum * 100).ToString(CultureInfo.InvariantCulture);
      txtThroughQuickLimit.Text = frmCustStruct.TempWagerLimitExpiration = fNum > 0 ? txtThroughQuickLimit.Text : "";
      if (string.IsNullOrEmpty(frmCustStruct.TempWagerLimitExpiration)) frmCustStruct.TempWagerLimit = txtTempQuickLimit.Text = @"0";

      frmCustStruct.TimeZone = GetTimeZoneCode(FormF.GetSelectedItemInDropDownFromPanel(this, "panTimeZonesList"));
      frmCustStruct.UsePuckLineFlag = (chbUsePuck.Checked ? "Y" : "N");

      fNum = NumberF.ParseDouble(txtQuickLimit.Text);
      frmCustStruct.WagerLimit = (fNum * 100).ToString(CultureInfo.InvariantCulture);
      frmCustStruct.WeeklyLimitFlag = (chbWeeklyLimits.Checked ? "Y" : "N");
      frmCustStruct.WiseActionFlag = (chbWiseGuy.Checked ? "Y" : "N");
      frmCustStruct.ZeroBalanceFlag = (chbZeroBalance.Checked ? "Y" : "N");
      frmCustStruct.ZeroBalPositiveOnlyFlag = (chbZeroBalancePositiveFigures.Checked ? "Y" : "N");
      frmCustStruct.Zip = txtZipCode.Text;

      _customerInfoFromFrmList.Add(frmCustStruct);
      return _customerInfoFromFrmList;

    }

    private string GetSelectedItemInRadiosGroupFromGroupBox(string p) {
      var selectedOption = "";
      var grp = (GroupBox)Controls.Find(p, true).FirstOrDefault();
      if (grp == null) return selectedOption;
      foreach (Control ctrl in grp.Controls) {
        if (ctrl.GetType().ToString() != "System.Windows.Forms.RadioButton") continue;
        var rad = (RadioButton)ctrl;
        if (!rad.Checked) continue;
        selectedOption = rad.Name.Substring(3);
        break;
      }

      return selectedOption;
    }

    private static string GetAgentType(CheckBox chbCustomerIsAgent, CheckBox chbCustomerIsMasterAgent) {
      var agentType = "";

      if (chbCustomerIsAgent.Checked) {
        agentType = Agents.AGENT_TYPE;
        return agentType;
      }

      if (!chbCustomerIsMasterAgent.Checked) return agentType;
      agentType = "M";
      return agentType;
    }

    private static string GetTimeZoneCode(string p) {
      var timeZoneCode = "";
      var tzAbbrev = p.Split(' ')[0];

      switch (tzAbbrev) {
        case "CST":
          timeZoneCode = "1";
          break;
        case "EST":
          timeZoneCode = "0";
          break;
        case "MST":
          timeZoneCode = "2";
          break;
        case "PST":
          timeZoneCode = "3";
          break;
      }

      return timeZoneCode;
    }

    private static string GetStateCode(string p) {
      var stateCode = p.Split(' ')[0];

      return stateCode;
    }

    private static string GetPriceTypeCode(string p) {
      var priceTypeCode = "";
      switch (p) {
        case "American":
          priceTypeCode = LineOffering.PRICE_AMERICAN;
          break;
        case "Decimal":
          priceTypeCode = LineOffering.PRICE_DECIMAL;
          break;
        case "Fractional":
          priceTypeCode = LineOffering.PRICE_FRACTIONAL;
          break;
      }

      return priceTypeCode;
    }

    private static string GetNumDayOfWeekByName(string p) {
      string dayOfWeek = null;

      string[] weekDays = { "Everyday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };

      for (var i = 0; i < weekDays.Count(); i++) {
        if (p != weekDays[i]) continue;
        dayOfWeek = i.ToString(CultureInfo.InvariantCulture);
        break;
      }

      return dayOfWeek;
    }

    private void GoToDfShow() {
      using (var dfiguresFrm = new FrmDailyFigures(txtCustomerId.Text, _currentCustomerCurrency, AppModuleInfo, CurrentUserPermissions, SoundCardDetected)) {
        dfiguresFrm.Icon = Icon;
        dfiguresFrm.ShowDialog();
      }

    }

    private void HandleFreePlaysShowOption(ToolStripItemClickedEventArgs e) {
      switch (e.ClickedItem.AccessibleName) {
        case "0":
          using (var freePlayFrm = new FrmCustomerNewFreePlay(this, CustomerId, _currentCustomerCurrency, _agentId)) {
            freePlayFrm.Icon = Icon;
            freePlayFrm.ShowDialog();
          }
          break;
        case "1":
          DeleteFreePlay();
          break;
      }
    }

    private void SaveCustomerWagerLimits() {
      _customerInfo.CompareCustomerWagerLimitsInfoToUpdate(ModifiedWagerLimits);
      ModifiedWagerLimits = null;
      ChangeWagerLimitsTabLabel();
    }

    private void SaveCustomerVigDiscounts() {
      _customerInfo.CompareCustomerVigDiscountInfoToUpdate(ModifiedVigDiscounts);
      ModifiedVigDiscounts = null;
    }

    private void SendMessagesPackage() {
      var sendToPackage = false;

      var checkedMgsCnt = (from DataGridViewRow r in dgvwCustomerMessages.Rows let toSendCell = r.Cells[0] let sentCell = r.Cells[9] where (toSendCell.Value != null && toSendCell.Value.ToString().ToLower() == "true") && (sentCell.Value != null && Boolean.Parse(sentCell.Value.ToString()) == false) select toSendCell).Count();

      if (dgvwCustomerMessages.Rows.Count == 0 || checkedMgsCnt == 0) {
        MessageBox.Show(@"No messages were selected for sending to customers or messages were already sent.", @"Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);
        return;
      }

      var dialogResult = MessageBox.Show(@"Send checked messages to Package?", @"Attention", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
      if (dialogResult == DialogResult.Yes) {
        sendToPackage = true;
      }

      if (!sendToPackage) return;
      foreach (DataGridViewRow r in dgvwCustomerMessages.Rows) {

        var cell = r.Cells[0];
        var sentCell = r.Cells[9];

        decimal messageId = 0;

        if (r.Cells[1].Value != null) {
          decimal.TryParse(r.Cells[1].Value.ToString(), out messageId);
        }

        var expirationDt = DateTime.Now;

        if (r.Cells[4].Value != null) {
          DateTime.TryParse(r.Cells[4].Value.ToString(), out expirationDt);
        }

        var priority = "";
        var priorityCode = "";

        if (r.Cells[5].Value != null) {
          priority = r.Cells[5].Value.ToString();
        }

        switch (priority) {
          case "Always Alert":
            priorityCode = "H";
            break;
          case "Inbox Only":
            priorityCode = "L";
            break;
          case "Alert once":
            priorityCode = "M";
            break;
        }


        var status = "";
        var statusCode = "";

        if (r.Cells[6].Value != null) {
          status = r.Cells[6].Value.ToString();
        }

        switch (status) {

          case "Deleted":
            statusCode = "D";
            break;
          case "Read":
            statusCode = "R";
            break;
          case "Unread":
            statusCode = "U";
            break;

        }

        var subject = "";

        if (r.Cells[7].Value != null) {
          subject = r.Cells[7].Value.ToString();
        }

        var message = "";

        if (r.Cells[8].Value != null) {
          message = r.Cells[8].Value.ToString();
        }

        if ((cell.Value != null && cell.Value.ToString().ToLower() == "true") && (sentCell.Value != null && Boolean.Parse(sentCell.Value.ToString()) == false)) {
          SendMessageToCustomer(CustomerId, expirationDt, statusCode, priorityCode, subject, message, messageId, true);
        }
      }
    }

    private void SetFocusOnTabIndex(String tabIdxToLoad) {
      var tabIdx = 0;
      foreach (TabPage tp in tabCustomer.TabPages) {
        if (tp.Text != null && tp.Text.ToUpper().Trim() == tabIdxToLoad.ToUpper().Trim()) {
          tabCustomer.SelectedIndex = tabIdx;
          break;
        }
        tabIdx++;
      }
    }

    private void WriteCustomerMessagesGridHeaders(DataGridView caller) {
      caller.DataSource = null;

      var dgrvwColNames = new List<string>();

      foreach (var col in caller.Columns) {
        var strType = col.GetType().Name;
        if (strType == "DataGridViewTextBoxColumn") {
          dgrvwColNames.Add(((DataGridViewTextBoxColumn)col).Name);
        }

        if (strType == "DataGridViewCheckBoxColumn") {
          dgrvwColNames.Add(((DataGridViewCheckBoxColumn)col).Name);
        }
      }

      foreach (var str in dgrvwColNames) {
        caller.Columns.Remove(str);
      }

      var chBxColumn = new DataGridViewCheckBoxColumn {
        HeaderText = @"To Send",
        Width = 60,
        Visible = (AgentTypeFromDb == Agents.AGENT_TYPE)
      };
      caller.Columns.Add(chBxColumn);



      var titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Id", Width = 60 };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Date Sent", Width = 100 };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Date Read", Width = 100 };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Exp Date", Width = 100 };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Priority", Width = 50 };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Status", Width = 50 };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Subject", Width = 100 };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Message", Width = 250 };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"SentToPackage", Width = 10, Visible = false };
      caller.Columns.Add(titleColumn);
    }

    private void SendPackageToCustomerShow() {
      using (var sendPackageFrm = new FrmSendPackageToCustomers(this)) {
        sendPackageFrm.Icon = Icon;
        sendPackageFrm.ShowDialog();
      }
    }

    private void ShowHideDeletedMessages(object sender) {
      if (((CheckBox)sender).Checked) {
        GetCustomerMessages(false, true);
      }
      else {
        GetCustomerMessages(false);
      }
    }

    private void ShowCustomerBalance() {
      var exchangeRate = 1.0;
      if (_exchangeRate != null)
        exchangeRate = (double)_exchangeRate;
      using (var balance = new FrmCustomerBalanceInformation(this, _customerType, _customerPendingWagerCount, _customerPendingWagerBalance,
                                                            _customerFreePlayPendingCount, _customerFreePlayPendingBalance, _currentCustomerCurrency, _customerCurrentBalance,
                                                            (_customerCreditLimit * exchangeRate),
                                                            (_customerCreditIncrease * exchangeRate), _customerCreditIncreaseThru, _customerPendingCredit, (_customerNetCasinoAmount * exchangeRate),
                                                            CustomerFreePlayBalance, AppModuleInfo, chbWeeklyLimits.Checked, _customerThisWeeksFigure)) {
        balance.Icon = Icon;
        balance.ShowDialog();
      }
    }

    private void ShowNonPostedCasinoPlays() {

      using (var nonPostedCasinoFrm = new FrmCustomerCasinoPlays("NonPosted", CustomerId, GetCurrentDateTime(), AppModuleInfo)) {
        nonPostedCasinoFrm.Icon = Icon;
        nonPostedCasinoFrm.ShowDialog();
      }
    }

    private void GetCustomerActionsAndRestrictions() {
      PopulateListWithActions(GetCustomerActions());
      PopulateListWithActions(_actions);
    }

    private IEnumerable<spCstGetActions_Result> GetCustomerActions() {
      if (_actions != null) return _actions;
      CustomerId = txtCustomerId.Text;
      using (var cst = new Customers(AppModuleInfo)) {
        _actions = cst.GetActions(CustomerId);
      }
      return _actions;
    }

    private void PopulateListWithActions(IEnumerable<spCstGetActions_Result> actions) {
      clbActions.Items.Clear();
      foreach (var a in actions) {
        var r = clbActions.Items.Add(a, a.RestrictedActionId != null || a.SystemRestricted);
        if (a.SystemRestricted) clbActions.SetItemCheckState(r, CheckState.Indeterminate);
      }
      clbActions.DisplayMember = "Name";

    }

    private void GetCustomerActionsAndRestrictionsLogs() {
      List<spCULGetLog_Result> logs;
      using (var lw = new LogWriter(AppModuleInfo)) {
        logs = lw.GetCallUnitLog(AppModuleInfo.Description, null, "-Restriction-", CustomerId);
      }
      lbRestrictionsLog.Items.Clear();
      if (logs != null) {
        foreach (var log in logs) {
          lbRestrictionsLog.Items.Add(log.AccessDateTime + ",  Login: " + log.LoginID.Trim() + ",  " + log.Operation.Trim());
        }
      }
    }

    private void ShowRebatesMaintenanceForm() {
      if (LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.SET_REBATES_TO_CUSTOMER)) {
        using (var cust = new Customers(AppModuleInfo)) {
          var custInfo = cust.GetCustomerInfo(CustomerId).FirstOrDefault();
          using (var rebatesFrm = new FrmCustomerRebates(custInfo, _customerInfo, this)) {
            rebatesFrm.Icon = Icon;
            rebatesFrm.ShowDialog();
          }
        }
      }
      else {
        MessageBox.Show(Environment.UserName + @" does not have permissions to " + LoginsAndProfiles.SET_REBATES_TO_CUSTOMER);
      }
    }

    private void SetQuickLimitInputState() {
      if (!_customerTempLimits.IsActive()) return;
      DateTime date;
      txtQuickLimit.Enabled = (txtTempQuickLimit.Text == string.Empty || txtTempQuickLimit.Text == @"0" || !DateTime.TryParse(txtThroughQuickLimit.Text, out date));
    }

    private void ResetCustomerWagerLimits() {
      if (dgvwWagerLimitsDetails.Rows.Count > 0) {
        foreach (DataGridViewRow row in dgvwWagerLimitsDetails.Rows) {
          row.Cells[SPORT_ENABLED_INDEX].Value = true;
          row.Cells[OVR_CIRCLE_INDEX].Value = false;
          row.Cells[CU_LIMIT_INDEX].Value = "0";
          row.Cells[INET_LIMIT_INDEX].Value = "0";
          row.Cells[TEMP_CU_LIMIT_INDEX].Value = "0";
          row.Cells[TEMP_INET_LIMIT_INDEX].Value = "0";
          row.Cells[TEMP_LIMIT_EXP_INDEX].Value = "";
        }
      }

      if (WagerLimits != null) {
        foreach (var wl in WagerLimits) {
          if (wl.CuLimit > 0 || wl.InetLimit > 0 || wl.TempCuLimit > 0 || wl.TempInetLimit > 0 || wl.SportEnabled == false || wl.OverrideCircle == true) {
            wl.SportEnabled = true;
            wl.OverrideCircle = false;
            wl.CuLimit = 0;
            wl.InetLimit = 0;
            wl.TempCuLimit = 0;
            wl.TempInetLimit = 0;
            wl.TempLimitsExpiration = null;
            AddWagerLimitEditedRowToControlList(wl);
          }
        }
      }

      if (_wagerLimitsInfoRetrieved && ModifiedWagerLimits != null && ModifiedWagerLimits.Count > 0) {
        _customerInfo.CompareCustomerWagerLimitsInfoToUpdate(ModifiedWagerLimits);
      }

      txtMaxParlayBet.Text = @"0";
      txtMaxParlayPayout.Text = @"0";
      txtMaxPropBet.Text = @"0";
      txtMaxTeaserBet.Text = @"0";

      ResetCustomerParlayTeaserWagerLimits();
    }

    private void ResetCustomerParlayTeaserWagerLimits() {
      using (var cust = new Customers(AppModuleInfo)) {
        cust.DeleteCustomerParlayTeaserWagerLimits(CustomerId);
      }

    }

    private void ResetPerformanceButtonsSelection() {
      var rad = new RadioButton();
      foreach (var ctrl in grpViewDailyFigs.Controls.Cast<Control>().Where(ctrl => rad.GetType() == ctrl.GetType())) {
        ((RadioButton)ctrl).Checked = false;
      }
      dgvwCustomerPerformance.Rows.Clear();
    }

    private string GetPriorityDescription(string priority) {
      switch (priority) {
        case "Always Alert":
          return CUST_MSG_ALWAYS_ALERT;
        case "Alert once":
          return CUST_MSG_ALERT_ONCE;
        case "Inbox Only":
          return CUST_MSG_INBOX_ONLY;
      }
      return "";
    }

    private void ChangeWagerLimitsTabLabel() {
      if (WagerLimits == null || WagerLimits.Count == 0)
        return;
      var rowsCnt = (from wl in WagerLimits where ((wl.CuLimit > 0 || wl.InetLimit > 0) || (_customerTempLimits.IsActive() && (wl.TempCuLimit > 0 || wl.TempInetLimit > 0))) select wl).Count();
      var originalText = tabCustomer.TabPages["tabpWagerLimits"].Text.Split(' ');
      if (rowsCnt > 0) {
        tabCustomer.TabPages["tabpWagerLimits"].Text = originalText[0] + @" " + originalText[1] + @" (*)";
      }
      else {
        tabCustomer.TabPages["tabpWagerLimits"].Text = originalText[0] + @" " + originalText[1];
      }
    }

    private void RestoreWagerLimitsTabLabel() {
      var originalText = tabCustomer.TabPages["tabpWagerLimits"].Text.Split(' ');
      tabCustomer.TabPages["tabpWagerLimits"].Text = originalText[0] + @" " + originalText[1];
    }

    private void HandleGridViewCellEndEdit(int columnIndex, int rowIndex) {
      if (columnIndex == SPORT_ENABLED_INDEX || columnIndex == OVR_CIRCLE_INDEX) {
        return;
      }
      AddWagerLimitEditedRowToControlList(GetWagerLimitObject(rowIndex));
    }

    private void ShowPendingBetsForm() {
      using (var pendingPlays = new frmCustomerPendingBets(CustomerId, AppModuleInfo)) {
        pendingPlays.Icon = Icon;
        pendingPlays.ShowDialog();
      }
    }

    #endregion

    #region Events

    private void customerMaintenanceForm_Load(object sender, EventArgs e) {
      panPersonal.Visible = false;
      panOfferingTab.Visible = false;
      panVigDiscount.Visible = false;
      panPerformance.Visible = false;
      panTransactions.Visible = false;
      panFreePlays.Visible = false;
      panComments.Visible = false;
      panMarketing.Visible = false;
      panAgent.Visible = false;
      ShowHideAgentTabElements(panAgent.Visible);
      panWagerLimits.Visible = false;
      btnSendToPackage.Visible = false;

      btnTranGotoWagers.Enabled = false;
      btnTranBrowse.Enabled = false;
      btnDeleteTran.Enabled = false;
      btnLastVerified.Enabled = false;

      btnFreePlayGoToWagers.Enabled = false;
      btnFreePlayDelete.Enabled = false;

      btnChangeCustMakeup.Enabled = LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.SET_REBATES_TO_CUSTOMER);
      btnResetWagerLimits.Enabled = LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.UPDATE_CUSTOMER_MAINTENANCE);

      if (LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.ENTER_CUSTOMER_TRANSACTIONS)) {
        btnTranNew.Enabled = true;
        btnFreePlayNew.Enabled = true;
      }
      else {
        btnTranNew.Enabled = false;
        btnFreePlayNew.Enabled = false;
      }

      if (_openCustomerId != null) {
        txtCustomerId.Text = _openCustomerId;
        RetrieveCustomerInfo(_openCustomerId);

        if (_tabToLoad != null) {
          SetFocusOnTabIndex(_tabToLoad);
        }
      }

      var tmr = new Timer { Interval = 1000 };
      tmr.Tick += tmr_Tick;
      tmr.Start();

      if (!LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.FULL_CUSTOMER_ADMINISTRATION)) {
        DisableRestrictedFormFields();
      }

      using (var lw = new LogWriter(AppModuleInfo)) {
        lw.LogUserInOrOutToOrFromCuTb("Login");
      }
      _performanceByWagerType.FillWagerTypeCmb();

      InitSavePackageToFile();

    }

    private void InitSavePackageToFile() {
        btnSavePackageToFile.Visible = _packageToCsv.IsActive() &&
          LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.FULL_CUSTOMER_ADMINISTRATION) &&
          LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.SETUP_CUSTOMERS)
        && AppModuleInfo.Parameters.SuperUsers.Contains(Environment.UserName);
    }

    private void SetCentsNumericInputs() {
      foreach (var pb in FindControlByType<NumberTextBox>(this, true)) {
        pb.AllowCents = Params.IncludeCents;
      }
      txtCreditLimit.AllowCents = false;
      txtCreditIncrease.AllowCents = false;
      txtQuickLimit.AllowCents = false;
      txtTempQuickLimit.AllowCents = false;
      txtCuMinBet.AllowCents = false;
      txtInetMinBet.AllowCents = false;
      txtMaxFreeHalfPoint.AllowCents = false;
      txtSettleFigure.AllowCents = false;
      txtSecondsToDelay.AllowCents = false;
      txtChartPerc.AllowCents = false;
      txtFreePlayPendingCount.AllowCents = false;
      txtCallUnit.AllowCents = false;
      txtCasino.AllowCents = false;
      txtInternetOnly.AllowCents = false;
      txtMaxParlayBet.AllowCents = false;
      txtMaxParlayPayout.AllowCents = false;
      txtMaxPropBet.AllowCents = false;
      txtMaxTeaserBet.AllowCents = false;
    }

    private void chbShowDeleted_CheckedChanged(object sender, EventArgs e) {
      ShowHideDeletedMessages(sender);
    }

    private void chbWeeklyLimits_CheckedChanged(object sender, EventArgs e) {
      AdjustAvailableBalanceDisplay(sender);
    }

    private void dgvwCustomerMessages_CellClick(object sender, DataGridViewCellEventArgs e) {

      if (e.ColumnIndex == 0 && e.RowIndex > -1) {
        if (((DataGridView)sender).Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null) {
          if (((DataGridView)sender).Rows[e.RowIndex].Cells[9].Value != null && Boolean.Parse(((DataGridView)sender).Rows[e.RowIndex].Cells[9].Value.ToString())) {
            ((DataGridView)sender).Rows[e.RowIndex].Cells[e.ColumnIndex].Value = true;
          }
          else {
            ((DataGridView)sender).Rows[e.RowIndex].Cells[e.ColumnIndex].Value = null;
          }
        }
        else {
          ((DataGridView)sender).Rows[e.RowIndex].Cells[e.ColumnIndex].Value = true;
        }
        ((DataGridView)sender).ClearSelection();
        return;
      }

      if (e.ColumnIndex > 0 && e.RowIndex > -1) {
        var msgId = "";
        if (((DataGridView)sender).Rows[e.RowIndex].Cells[1].Value != null) {
          msgId = ((DataGridView)sender).Rows[e.RowIndex].Cells[1].Value.ToString();
        }

        spCstGetCustomerMessages_Result msgObj = null;
        int messageId;
        int.TryParse(msgId, out messageId);

        if (msgId != "") {
          if (_customerMessages != null && _customerMessages.Count > 0) {
            msgObj = (from p in _customerMessages where p.CustomerMessageId == messageId select p).FirstOrDefault();
          }
          using (var msgfrm = new FrmCustomerMessage("Edit", msgObj, this)) {
            msgfrm.Icon = Icon;
            msgfrm.ShowDialog();
          }
        }
        else {
          using (var msgfrm = new FrmCustomerMessage("New", null, this)) {
            msgfrm.Icon = Icon;
            msgfrm.ShowDialog();
          }
        }

        ((DataGridView)sender).ClearSelection();
      }
    }

    private void dgvwFreePlaysDetails_MouseDown(object sender, MouseEventArgs e) {
      var hti = ((DataGridView)sender).HitTest(e.X, e.Y);
      if (hti != null && hti.Type != DataGridViewHitTestType.ColumnHeader && (hti.ColumnX > -1 || hti.RowY > -1)) {
        ((DataGridView)sender).ClearSelection();
        ((DataGridView)sender).Rows[hti.RowIndex].Selected = true;
      }

      EnableDisableToolStripMenus(sender, e, ctxMenuStripFreePlays);
    }

    private void dgvwTransactions_MouseDown(object sender, MouseEventArgs e) {
      if (e.Button == MouseButtons.Right) {
        var hti = ((DataGridView)sender).HitTest(e.X, e.Y);
        if (hti != null && hti.Type != DataGridViewHitTestType.ColumnHeader && (hti.ColumnX > -1 || hti.RowY > -1)) {
          ((DataGridView)sender).ClearSelection();
          if (_transactionsByType.IsActive()) {
            ((DataGridView)sender).Rows[hti.RowIndex].Cells[hti.ColumnIndex].Selected = true;
          }
          else {
            ((DataGridView)sender).Rows[hti.RowIndex].Selected = true;
          }
        }
      }

      EnableDisableToolStripMenus(sender, e, ctxMenuStripTransactions);
    }

    private void dgvwVigDiscount_CellEndEdit(object sender, DataGridViewCellEventArgs e) {
      AddVigDiscountEditedRowToControlList(e.RowIndex);
    }

    private void dgvwVigDiscount_KeyDown(object sender, KeyEventArgs e) {
      if (e.KeyCode.ToString() == "Delete") {
        if (((DataGridView)sender).SelectedCells.Count > 0) {
          ((DataGridView)sender).SelectedCells[0].Value = "";
        }
      }
    }

    private void ctxMenuStripFreePlays_ItemClicked(object sender, ToolStripItemClickedEventArgs e) {
      HandleFreePlaysShowOption(e);
    }

    private void ctxMenuStripTransactions_ItemClicked(object sender, ToolStripItemClickedEventArgs e) {
      switch (e.ClickedItem.AccessibleName) {
        case "0":
          using (var newTranForm = new FrmCustomerTransactionDetails(CustomerId, _agentId, _currentCustomerCurrency, "New", 0, GetCurrentDateTime(), this)) {
            newTranForm.Icon = Icon;
            newTranForm.ShowDialog();
          }

          break;
        case "1":
          BrowseTransaction();
          break;
        case "2":
          ((ContextMenuStrip)sender).Close();
          DeleteTransaction();
          break;
      }
    }

    private void tmr_Tick(Object myObject, EventArgs myEventArgs) {
      HandleTimerTick();
    }

    private void addToolStripMenuItem_Click(object sender, EventArgs e) {
      CustomerId = txtCustomerId.Text;


      if (CustomerId != "") {
        bool custExists;
        using (var cstms = new Customers(AppModuleInfo)) {
          spCstGetCustomerInfoNoNulls_Result custInfo;
          custExists = cstms.CustomerExists(CustomerId, out custInfo);
        }
        if (!custExists) {
          if (LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.ADD_CUSTOMERS)) {
            InsertNewCustomer(CustomerId);
            RetrieveCustomerInfo(CustomerId);
          }
          else {
            MessageBox.Show(Environment.UserName + @" does not have permissions to " + LoginsAndProfiles.ADD_CUSTOMERS);
          }
        }
        else {
          MessageBox.Show(@"Customer Id already exists", @"Customer already in Database");
        }
      }
      else {
        MessageBox.Show(@"No customer to Add", @"No customer typed in", MessageBoxButtons.OK);
        txtCustomerId.Focus();
      }
    }

    private void editCurrentToolStripMenuItem_Click(object sender, EventArgs e) {
      CustomerId = txtCustomerId.Text.Trim();
      if (_showAgentPlayersListTab != null)
        _showAgentPlayersListTab.ResetDatesCmbIdx = true;

      if (CustomerId.Length == 0) {
        MessageBox.Show(@"No customer to Edit");
      }

      else {
        using (var cust = new Customers(AppModuleInfo)) {
          spCstGetCustomerInfoNoNulls_Result custInfo;
          if (!cust.CustomerExists(CustomerId.Trim(), out custInfo)) {
            if (MessageBox.Show(@"Customer does not exist, create it?", @"Customer does not exist", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.OK) {
              if (LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.ADD_CUSTOMERS)) {
                InsertNewCustomer(CustomerId);
                RetrieveCustomerInfo(CustomerId);
                chbCustomerIsAgent.Enabled = true;
                chbCustomerIsMasterAgent.Enabled = true;
              }
              else {
                MessageBox.Show(Environment.UserName + @" does not have permissions to " + LoginsAndProfiles.ADD_CUSTOMERS);
                chbCustomerIsAgent.Enabled = false;
                chbCustomerIsMasterAgent.Enabled = false;
              }
            }
            else {
              txtCustomerId.Text = "";
              ShowHideAllTabs(false);
              panFirstAgentSettings.Visible = false;
            }
          }
          else {
            RetrieveCustomerInfo(CustomerId, custInfo);
          }
        }
      }
    }

    private void deleteCurrentToolStripMenuItem_Click(object sender, EventArgs e) {
      CustomerId = txtCustomerId.Text;

      if (MessageBox.Show(@"Are you sure you want to delete " + CustomerId + @"?", @"Delete CustomerId", MessageBoxButtons.OKCancel) != DialogResult.OK) return;
      if (LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.DELETE_CUSTOMERS)) {

        _custTransCount = GetCustomerTransactionsCount(CustomerId);

        if (_custTransCount > 0) {
          if (LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.DELETE_CUSTOMERS_WITH_TRANS)) {
            if (MessageBox.Show(@"Transactions are present for customer " + CustomerId + @", do you want to continue?", @"Delete CustomerId", MessageBoxButtons.OKCancel) != DialogResult.OK) return;
            DeleteCustomerFromDataBase(CustomerId);
            return;
          }
          MessageBox.Show(@"Cannot delete customer because transactions are present.");
        }
        else {
          DeleteCustomerFromDataBase(CustomerId);
        }
      }
      else {
        MessageBox.Show(Environment.UserName + @" does not have permissions to " + LoginsAndProfiles.DELETE_CUSTOMERS);
      }
    }

    private void deleteAgentCascateToolStripMenuItem_Click(object sender, EventArgs e) {
      if (LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.DELETE_AGENT_AND_CUSTOMERS)) {

        if (MessageBox.Show(@"Are you sure you want to delete " + CustomerId + @" and all his players?", @"Delete Agent", MessageBoxButtons.OKCancel) != DialogResult.OK) return;
        using (var agents = new Agents(AppModuleInfo)) {
          var agentId = txtCustomerId.Text;
          agents.DeleteAgentAndCustomers(agentId);
        }
      }
      else
        MessageBox.Show(Environment.UserName + @" does not have permissions to " + LoginsAndProfiles.DELETE_CUSTOMERS);
    }

    private void reportsToolStripMenuItem_Click(object sender, EventArgs e) {
      Process.Start("Reports.exe");
    }

    private void custIdtextBox_TextChanged(object sender, EventArgs e) {

      if (!CustomerInfoRetrieved || CustomerId.Trim() == ((TextBox)sender).Text) return;
      if (LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.UPDATE_CUSTOMER_MAINTENANCE)) {
        SaveCustomerInfo();
      }
      else {
        CustomerInfoRetrieved = false;
        ClearFormElementsContent();
      }
    }

    private void TeasersButton_Click(object sender, EventArgs e) {
      HandleTeasersShowOption();
    }

    private void tempCreditIncDateTimePicker_ValueChanged(object sender, EventArgs e) {
      var dtp = (DateTimePicker)sender;

      txtThroughCredIncrease.Text = dtp.Text;

      CheckIfTempCreditHasExpired(dtp.Text);
    }

    private void dtmTempQuickLimit_ValueChanged(object sender, EventArgs e) {
      var dtp = (DateTimePicker)sender;
      txtThroughQuickLimit.Text = dtp.Text;
      //CheckIfTempCreditHasExpired(dtp.Text);
    }

    private void custBuyPointsButton_Click(object sender, EventArgs e) {
      CostToBuyPointsShow();
    }

    private void betServiceProfilesCutton_Click(object sender, EventArgs e) {
      BetServiceProfilesShow();
    }

    private void vigDiscountdataGridView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e) {
      if (e.ColumnIndex == 4 || e.ColumnIndex == 6) {
        var cellValue = e.FormattedValue.ToString();

        if (cellValue != string.Empty) {

          if (!NumberF.IsValidNumber0To100(cellValue, true)) {
            MessageBox.Show(@"Not a valid number[0-100]");
            e.Cancel = true;
          }
        }
      }

      if (e.ColumnIndex == 5 || e.ColumnIndex == 7) {
        var cellValue = e.FormattedValue.ToString();

        if (cellValue != string.Empty) {

          if (!DateTimeF.IsValidDate(cellValue, true)) {
            MessageBox.Show(@"Not a valid Date");
            e.Cancel = true;
          }
        }
      }

    }

    private void radDailyPerformance_CheckedChanged(object sender, EventArgs e) {
      PerformanceViewMode = "Last 30 Days in " + _currentCustomerCurrency;
      PerformanceViewDateRange = PERFORMANCEVIEWDAILY;
      PopulateCustomerPerformanceGv(PerformanceViewMode);
    }

    private void radWeeklyPerformance_CheckedChanged(object sender, EventArgs e) {
      PerformanceViewMode = "Last 26 Weeks in " + _currentCustomerCurrency;
      PerformanceViewDateRange = PERFORMANCEVIEWWEEKLY;
      PopulateCustomerPerformanceGv(PerformanceViewMode);
    }

    private void radMonthlyPerformance_CheckedChanged(object sender, EventArgs e) {
      PerformanceViewMode = "Last 12 Months in " + _currentCustomerCurrency;
      PerformanceViewDateRange = PERFORMANCEVIEWMONTLY;
      PopulateCustomerPerformanceGv(PerformanceViewMode);
    }

    private void radYearlyPerformance_CheckedChanged(object sender, EventArgs e) {
      PerformanceViewMode = "All Years in " + _currentCustomerCurrency;
      PerformanceViewDateRange = PERFORMANCEVIEWYEARLY;
      PopulateCustomerPerformanceGv(PerformanceViewMode);
    }

    private void btnGoToDFs_Click(object sender, EventArgs e) {

      GoToDfShow();
    }

    private void btnGoToWagers_Click(object sender, EventArgs e) {
      GoToWagersShow();
    }

    private void btnSportsContestsLinks_Click(object sender, EventArgs e) {
      //frmSportsContestsLinks frm = new frmSportsContestsLinks(moduleInfo);
      //frm.ShowDialog();
    }

    private void cmbDisplay_SelectedIndexChanged(object sender, EventArgs e) {
      PopulateCustomerTransactionsGv(((ComboBox)sender).SelectedIndex);
      ChangeGridCreditDebitColumnsHeaderText(cmbTranTypes.Text);
      if (dgvwTransactions.Rows.Count != 0) return;
      btnTranGotoWagers.Enabled = false;
      btnTranBrowse.Enabled = false;
      btnDeleteTran.Enabled = false;
      btnLastVerified.Enabled = false;
    }

    private void btnShowAuditCreditIncrease_Click(object sender, EventArgs e) {
      var item = new AuditTargetItem {
        Code = AuditTargetItems.TempCreditAdj[0],
        Description = AuditTargetItems.TempCreditAdj[1],
        IsNumber = false,
        SourceTable = AuditTargetItems.TempCreditAdj[2],
        PkFieldsSet = new List<PkField> { new PkField { Name = "CustomerID", Value = txtCustomerId.Text } }
      };
      ShowAuditInfo(item);
    }

    private void btnShowAuditCreditLimit_Click(object sender, EventArgs e) {
      var item = new AuditTargetItem {
        Code = AuditTargetItems.CreditLimit[0],
        Description = AuditTargetItems.CreditLimit[1],
        IsNumber = true,
        SourceTable = AuditTargetItems.CreditLimit[2],
        PkFieldsSet = new List<PkField> { new PkField { Name = "CustomerID", Value = txtCustomerId.Text } }
      };
      ShowAuditInfo(item);
    }

    private void btnShowAuditCUMinBet_Click(object sender, EventArgs e) {
      var item = new AuditTargetItem {
        Code = AuditTargetItems.CuMinimumWager[0],
        Description = AuditTargetItems.CuMinimumWager[1],
        IsNumber = true,
        SourceTable = AuditTargetItems.CuMinimumWager[2],
        PkFieldsSet = new List<PkField> { new PkField { Name = "CustomerID", Value = txtCustomerId.Text } }
      };
      ShowAuditInfo(item);
    }

    private void btnShowAuditInetMinBet_Click(object sender, EventArgs e) {
      var item = new AuditTargetItem {
        Code = AuditTargetItems.InetMinimumWager[0],
        Description = AuditTargetItems.InetMinimumWager[1],
        IsNumber = true,
        SourceTable = AuditTargetItems.InetMinimumWager[2],
        PkFieldsSet = new List<PkField> { new PkField { Name = "CustomerID", Value = txtCustomerId.Text } }
      };
      ShowAuditInfo(item);
    }

    private void btnShowAuditPassword_Click(object sender, EventArgs e) {
      var item = new AuditTargetItem {
        Code = AuditTargetItems.Password[0],
        Description = AuditTargetItems.Password[1],
        IsNumber = false,
        SourceTable = AuditTargetItems.Password[2],
        PkFieldsSet = new List<PkField> { new PkField { Name = "CustomerID", Value = txtCustomerId.Text } }
      };
      ShowAuditInfo(item);
    }

    private void btnShowAuditPayoutPassword_Click(object sender, EventArgs e) {
      var item = new AuditTargetItem {
        Code = AuditTargetItems.PayoutPassword[0],
        Description = AuditTargetItems.PayoutPassword[1],
        IsNumber = false,
        SourceTable = AuditTargetItems.PayoutPassword[2],
        PkFieldsSet = new List<PkField> { new PkField { Name = "CustomerID", Value = txtCustomerId.Text } }
      };
      ShowAuditInfo(item);
    }

    private void btnShowAuditQuickLimit_Click(object sender, EventArgs e) {
      var item = new AuditTargetItem {
        Code = AuditTargetItems.QuickWagerLimit[0],
        Description = AuditTargetItems.QuickWagerLimit[1],
        IsNumber = true,
        SourceTable = AuditTargetItems.QuickWagerLimit[2],
        PkFieldsSet = new List<PkField> { new PkField { Name = "CustomerID", Value = txtCustomerId.Text } }
      };
      ShowAuditInfo(item);
    }

    private void btnShowAuditSettleFigure_Click(object sender, EventArgs e) {
      var item = new AuditTargetItem {
        Code = AuditTargetItems.SettleFigure[0],
        Description = AuditTargetItems.SettleFigure[1],
        SourceTable = AuditTargetItems.SettleFigure[2],
        IsNumber = true,
        PkFieldsSet = new List<PkField> { new PkField { Name = "CustomerID", Value = txtCustomerId.Text } }
      };
      ShowAuditInfo(item);
    }

    private void btnShowAuditParlayMaxBet_Click(object sender, EventArgs e) {
      var item = new AuditTargetItem {
        Code = AuditTargetItems.ParlayMaxBet[0],
        Description = AuditTargetItems.ParlayMaxBet[1],
        IsNumber = true,
        SourceTable = AuditTargetItems.ParlayMaxBet[2],
        PkFieldsSet = new List<PkField> { new PkField { Name = "CustomerID", Value = txtCustomerId.Text } }
      };
      ShowAuditInfo(item);
    }

    private void btnShowAuditParlayMaxPayout_Click(object sender, EventArgs e) {
      var item = new AuditTargetItem {
        Code = AuditTargetItems.ParlayMaxPayout[0],
        Description = AuditTargetItems.ParlayMaxPayout[1],
        IsNumber = true,
        SourceTable = AuditTargetItems.ParlayMaxPayout[2],
        PkFieldsSet = new List<PkField> { new PkField { Name = "CustomerID", Value = txtCustomerId.Text } }
      };
      ShowAuditInfo(item);
    }

    private void btnShowAuditContestMaxBet_Click(object sender, EventArgs e) {
      var item = new AuditTargetItem {
        Code = AuditTargetItems.ContestMaxBet[0],
        Description = AuditTargetItems.ContestMaxBet[1],
        IsNumber = true,
        SourceTable = AuditTargetItems.ContestMaxBet[2],
        PkFieldsSet = new List<PkField> { new PkField { Name = "CustomerID", Value = txtCustomerId.Text } }
      };
      ShowAuditInfo(item);
    }

    private void btnShowAuditTeaserMaxBet_Click(object sender, EventArgs e) {
      var item = new AuditTargetItem {
        Code = AuditTargetItems.TeaserMaxBet[0],
        Description = AuditTargetItems.TeaserMaxBet[1],
        IsNumber = true,
        SourceTable = AuditTargetItems.TeaserMaxBet[2],
        PkFieldsSet = new List<PkField> { new PkField { Name = "CustomerID", Value = txtCustomerId.Text } }
      };
      ShowAuditInfo(item);
    }

    private void btnShowAuditWagerLimits_Click(object sender, EventArgs e) {

      var item = new AuditTargetItem {
        Code = AuditTargetItems.DetWagerLimits[0],
        Description = AuditTargetItems.DetWagerLimits[1],
        IsNumber = true,
        SourceTable = AuditTargetItems.DetWagerLimits[2]
      };

      if (dgvwWagerLimitsDetails.SelectedCells.Count > 0) {
        var rowIdx = dgvwWagerLimitsDetails.SelectedCells[0].OwningRow.Index;
        var selectedrow = dgvwWagerLimitsDetails.Rows[rowIdx];
        ShowAuditInfo(item, selectedrow);
      }
    }

    private void btnShowAuditWagerLimitsN_Click(object sender, EventArgs e) {
      if (dgvwWagerLimitsDetails.SelectedCells.Count > 0) {
        if (dgvwWagerLimitsDetails.SelectedCells[0].ColumnIndex == 3 || dgvwWagerLimitsDetails.SelectedCells[0].ColumnIndex == 4) {
          var item = new AuditTargetItem { IsNumber = true };
          if (dgvwWagerLimitsDetails.SelectedCells[0].ColumnIndex == 3) {
            item.Code = AuditTargetItems.DetWagerLimitsCu[0];
            item.Description = AuditTargetItems.DetWagerLimitsCu[1];
            item.SourceTable = AuditTargetItems.DetWagerLimitsCu[2];
          }
          else {
            item.Code = AuditTargetItems.DetWagerLimitsInet[0];
            item.Description = AuditTargetItems.DetWagerLimitsInet[1];
            item.SourceTable = AuditTargetItems.DetWagerLimitsInet[2];
          }
          var rowIdx = dgvwWagerLimitsDetails.SelectedCells[0].OwningRow.Index;
          var selectedrow = dgvwWagerLimitsDetails.Rows[rowIdx];
          var sportType = "";
          var sportSubType = "";
          var periodNumber = -1;
          var wagerType = "";

          if (selectedrow.Cells[0].Value != null) {
            sportType = selectedrow.Cells[0].Value.ToString().Split('-')[0].Trim();
            sportSubType = selectedrow.Cells[0].Value.ToString().Split('-')[1].Trim();
          }
          if (selectedrow.Cells[6].Value != null) {
            periodNumber = int.Parse(selectedrow.Cells[6].Value.ToString());
          }

          if (selectedrow.Cells[5].Value != null) {
            wagerType = selectedrow.Cells[5].Value.ToString();
          }
          item.PkFieldsSet = new List<PkField> { new PkField { Name = "CustomerID", Value = txtCustomerId.Text }, new PkField { Name = "PeriodNumber", Value = periodNumber.ToString(CultureInfo.InvariantCulture) }, new PkField { Name = "SidesOrTotals", Value = wagerType }, new PkField { Name = "SportSubType", Value = sportSubType }, new PkField { Name = "SportType", Value = sportType } };
          ShowAuditInfo(item, selectedrow);
        }
        else {
          MessageBox.Show(@"Select either a CULimit or InetLimit cell", @"Select Grid Cell");
        }
      }
      else {
        MessageBox.Show(@"Select either a CULimit or InetLimit cell", @"Select Grid Cell");
      }
    }

    private void btnBalance_Click(object sender, EventArgs e) {
      ShowCustomerBalance();
    }

    private void btnChangeCO_Click(object sender, EventArgs e) {
      ChangeCarryOverShow();
    }

    private void btnNewMessage_Click(object sender, EventArgs e) {
      CreateNewMessage();
    }

    private void btnSendToPackage_Click(object sender, EventArgs e) {
      SendMessagesPackage();
    }

    private void btnNonPostedCasino_Click(object sender, EventArgs e) {
      ShowNonPostedCasinoPlays();
    }

    private void dgvwTransactions_SelectionChanged(object sender, EventArgs e) {
      if (_transactionsByType.IsActive()) {
        _transactionsByType.HandleCellSelectionChangeEvent(sender);
      }
      HandleRowSelectionChangeEvent(sender);
    }

    private void btnTranBrowse_Click(object sender, EventArgs e) {
      BrowseTransaction();
    }

    private void btnTranNew_Click(object sender, EventArgs e) {
      AddNewTransactionShow();
    }

    private void btnTranGotoWagers_Click(object sender, EventArgs e) {
      GoToWagersFromTransactionsShow();
    }

    private void btnLastVerified_Click(object sender, EventArgs e) {
      SelectLastVerifiedDocumentNumber();
    }

    private void btnFreePlayGoToWagers_Click(object sender, EventArgs e) {
      GoToWagersFromFreePlaysShow();
    }

    private void dgvwFreePlaysDetails_SelectionChanged(object sender, EventArgs e) {
      btnFreePlayGoToWagers.Enabled = false;
      btnFreePlayDelete.Enabled = false;

      if (((DataGridView)sender).SelectedRows.Count > 0) {
        switch (((DataGridView)sender).SelectedRows[0].Cells[6].Value.ToString()) {
          case "A":
          case "R":
            btnFreePlayGoToWagers.Enabled = true;
            btnFreePlayDelete.Enabled = false;
            break;
          default:
            btnFreePlayGoToWagers.Enabled = false;
            btnFreePlayDelete.Enabled = true;
            break;
        }
      }

      btnFreePlayNew.Enabled = LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.ENTER_CUSTOMER_TRANSACTIONS);

      //btnFreePlayDelete.Enabled = LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.EDIT_DELETE_CUSTOMER_TRANSACTIONS);

    }

    private void btnFreePlayNew_Click(object sender, EventArgs e) {
      AddNewFreePlay();
    }

    private void chbCustomerIsAgent_CheckedChanged(object sender, EventArgs e) {
      var chb = (CheckBox)sender;
      if (chb.Checked) {
        ShowHideAgentTabElements(true);
        chbCustomerIsMasterAgent.Checked = false;
        chbIncludeFreePlayLossesinDistribution.Visible = true;
        AgentTypeFromFrm = Agents.AGENT_TYPE;
      }
      else {
        chbIncludeFreePlayLossesinDistribution.Visible = false;
        if (chbCustomerIsMasterAgent.Checked == false) {
          ClearAgentTabElementContents();
          ShowHideAgentTabElements(false);
          var preserveVisibleStatusTo = (Panel)Controls.Find("panFirstAgentSettings", true)[0];
          preserveVisibleStatusTo.Visible = true;
        }
        AgentTypeFromFrm = "";
      }

      //CustomerComboBox cB = new CustomerComboBox(moduleInfo);

      if (panMasterAgentDropDown.Controls.Count > 0) {
        panMasterAgentDropDown.Controls.RemoveAt(0);
      }

      panMasterAgentDropDown.Controls.Add(_cB.MasterAgents(_masterAgentId, this));
    }

    private void chbCreditCustomer_CheckedChanged(object sender, EventArgs e) {
      if (MessageBox.Show(@"Changing this option might affect customer figures, do you want to continue? ", @"Warning", MessageBoxButtons.YesNo) == DialogResult.No) {
        chbCreditCustomer.CheckedChanged -= chbCreditCustomer_CheckedChanged;
        chbCreditCustomer.Checked = !chbCreditCustomer.Checked;
        chbCreditCustomer.CheckedChanged += chbCreditCustomer_CheckedChanged;
        return;
      }

      AdjustCustomerCreditFlag(sender);

    }

    private void chbCustomerIsMasterAgent_CheckedChanged(object sender, EventArgs e) {
      var chb = (CheckBox)sender;
      if (chb.Checked) {
        ShowHideAgentTabElements(true);
        chbCustomerIsAgent.Checked = false;
        AgentTypeFromFrm = "M";

      }
      else {
        if (chbCustomerIsAgent.Checked == false) {
          ClearAgentTabElementContents();
          ShowHideAgentTabElements(false);
          var preserveVisibleStatusTo = (Panel)Controls.Find("panFirstAgentSettings", true)[0];
          preserveVisibleStatusTo.Visible = true;
        }
        AgentTypeFromFrm = "";
      }
    }

    private void chbZeroBalance_CheckedChanged(object sender, EventArgs e) {
      if (MessageBox.Show(@"Changing this option might affect customer figures, do you want to continue? ", @"Warning", MessageBoxButtons.YesNo) == DialogResult.No) {
        chbZeroBalance.CheckedChanged -= chbZeroBalance_CheckedChanged;
        chbZeroBalance.Checked = !chbZeroBalance.Checked;
        chbZeroBalance.CheckedChanged += chbZeroBalance_CheckedChanged;
        return;
      }
      if (((CheckBox)sender).Checked) {
        chbZeroBalancePositiveFigures.Enabled = true;
      }
      else {
        chbZeroBalancePositiveFigures.Checked = false;
        chbZeroBalancePositiveFigures.Enabled = false;
      }
    }

    private void cmbFreePlayDisplay_SelectedIndexChanged(object sender, EventArgs e) {
      PopulateCustomerFreePlaysTransactionsGv(((ComboBox)sender).SelectedIndex);

      if (dgvwTransactions.Rows.Count == 0) {
        btnFreePlayGoToWagers.Enabled = false;
        btnFreePlayDelete.Enabled = false;
      }
    }

    private void decryptorToolStripMenuItem_Click(object sender, EventArgs e) {

      var encryptorFrm = new FrmEncryptor();
      encryptorFrm.ShowDialog();

    }

    private void btnSendPackage_Click(object sender, EventArgs e) {
      SendPackageToCustomerShow();
    }

    private void btnChangeMakeup_Click(object sender, EventArgs e) {
      if (txtCurrentMakeup.Text == "") {
        MessageBox.Show(@"No agent distribution record found." +
            Environment.NewLine + @"Enter the makeup figure after setting up the agent, then running zero balance on the regular schedule.");
        return;
      }
      ChangeCustomerMakeup();
    }

    private void tabCustomer_Deselecting(object sender, TabControlCancelEventArgs e) {
      if (!CustomerInfoRetrieved)
        return;

      if (txtNewMakeup.Enabled)
        txtNewMakeup.Enabled = false;

      var tab = (TabControl)sender;

      var deselectedTabName = tab.SelectedTab.Name;

      if (_vigDiscountInfoRetrieved && deselectedTabName == "tabpVigDiscount" && ModifiedVigDiscounts != null && ModifiedVigDiscounts.Count > 0) {
        SaveCustomerVigDiscounts();
      }

      if (_wagerLimitsInfoRetrieved && deselectedTabName == "tabpWagerLimits" && ModifiedWagerLimits != null && ModifiedWagerLimits.Count > 0) {
        SaveCustomerWagerLimits();
      }

    }

    private void tabCustomer_SelectedIndexChanged(object sender, EventArgs e) {
      HandleTabCustommerSelectionChange(sender);
    }

    private void dgvwWagerLimitsDetails_CellClick(object sender, DataGridViewCellEventArgs e) {
      if (e.ColumnIndex != TEMP_LIMIT_EXP_INDEX) return;
      _oDateTimePicker = new DateTimePicker();

      if (!string.IsNullOrEmpty(dgvwWagerLimitsDetails.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString()))
        _oDateTimePicker.Value = DateTime.Parse(dgvwWagerLimitsDetails.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());

      dgvwWagerLimitsDetails.Controls.Add(_oDateTimePicker);

      _oDateTimePicker.Format = DateTimePickerFormat.Short;

      var oRectangle = dgvwWagerLimitsDetails.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true);

      _oDateTimePicker.Size = new Size(oRectangle.Width, oRectangle.Height);

      _oDateTimePicker.Location = new Point(oRectangle.X, oRectangle.Y);

      _oDateTimePicker.CloseUp += oDateTimePicker_CloseUp;

      _oDateTimePicker.TextChanged += oDateTimePicker_TextChanged;

      _oDateTimePicker.Leave += oDateTimePicker_Leave;

      _oDateTimePicker.LostFocus += oDateTimePicker_Leave;

      _oDateTimePicker.MouseLeave += oDateTimePicker_Leave;

      _oDateTimePicker.DropDown += oDateTimePicker_DropDown;

      _oDateTimePicker.Visible = true;
    }

    private void dgvwWagerLimitsDetails_CellContentClick(object sender, DataGridViewCellEventArgs e) {
      if (((DataGridView)sender).CurrentCell.ColumnIndex != SPORT_ENABLED_INDEX && ((DataGridView)sender).CurrentCell.ColumnIndex != OVR_CIRCLE_INDEX) return;
      if (((DataGridView)sender).CurrentCell.ColumnIndex == SPORT_ENABLED_INDEX) {
        var disableSport = !(Boolean)((DataGridView)sender).CurrentCell.Value;
        AddWagerLimitEditedRowToControlList(GetWagerLimitObject(((DataGridView)sender).CurrentCell.RowIndex, disableSport));
      }
      else {
        var overrideCircle = !(Boolean)((DataGridView)sender).CurrentCell.Value;
        if (overrideCircle && ((DataGridView)sender).CurrentCell.ReadOnly == false) {
          if (MessageBox.Show(@"Are you sure you want to override Circle Limit?", @"Override Circle Limit", MessageBoxButtons.OKCancel) == DialogResult.OK) {
            AddWagerLimitEditedRowToControlList(GetWagerLimitObject(((DataGridView)sender).CurrentCell.RowIndex, null, true));
          }
          else {
            ((DataGridView)sender).CancelEdit();
          }
        }
        else {
          AddWagerLimitEditedRowToControlList(GetWagerLimitObject(((DataGridView)sender).CurrentCell.RowIndex, null, overrideCircle));
        }
      }
      ((DataGridView)sender).ClearSelection();
      GetDisabledSportsCount();
    }

    private void dgvwWagerLimitsDetails_CellEndEdit(object sender, DataGridViewCellEventArgs e) {
      HandleGridViewCellEndEdit(e.ColumnIndex, e.RowIndex);
    }

    private void dgvwWagerLimitsDetails_CellValidating(object sender, DataGridViewCellValidatingEventArgs e) {
      var dgv = (DataGridView)sender;
      if ((e.ColumnIndex != CU_LIMIT_INDEX && e.ColumnIndex != INET_LIMIT_INDEX && e.ColumnIndex != TEMP_CU_LIMIT_INDEX &&
          e.ColumnIndex != TEMP_INET_LIMIT_INDEX)) return;
      var cellValue = e.FormattedValue.ToString();

      if (e.ColumnIndex == TEMP_LIMIT_EXP_INDEX && cellValue != string.Empty) {
        if (!DateTimeF.IsValidDate(cellValue, true)) {
          MessageBox.Show(@"Not a valid date");
          e.Cancel = true;
          return;
        }
      }

      if (cellValue != string.Empty) {
        return;
      }
      MessageBox.Show(@"Not a valid date");
      e.Cancel = true;

      if (cellValue != string.Empty) {
        if (NumberF.IsValidNumber(cellValue, true)) return;
        MessageBox.Show(@"Not a valid number");
        e.Cancel = true;
      }
      else if (dgv.EditingControl != null) dgv.EditingControl.Text = @"0";

    }

    private void dgvwWagerLimitsDetails_DataError(object sender, DataGridViewDataErrorEventArgs e) {
      switch (e.ColumnIndex) {
        case CU_LIMIT_INDEX:
          MessageBox.Show(@"Please enter a valid CU Limit Amount.");
          break;

        case INET_LIMIT_INDEX:
          MessageBox.Show(@"Please enter a valid Inet Limit Amount.");
          break;

        case TEMP_CU_LIMIT_INDEX:
          MessageBox.Show(@"Please enter a valid Temp CU Limit Amount.");
          break;

        case TEMP_INET_LIMIT_INDEX:
          MessageBox.Show(@"Please enter a valid Temp Inet Limit Amount.");
          break;
      }

      e.ThrowException = false;
    }

    private void dgvwWagerLimitsDetails_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e) {
      var gv = (DataGridView)sender;
      var i = gv.CurrentCell.ColumnIndex;

      if (i == CU_LIMIT_INDEX || i == INET_LIMIT_INDEX || i == TEMP_CU_LIMIT_INDEX || i == TEMP_INET_LIMIT_INDEX)
        e.Control.KeyPress += numberAmount_KeyPress;

    }

    private void dgvwWagerLimitsDetails_KeyDown(object sender, KeyEventArgs e) {
      var r = dgvwWagerLimitsDetails.SelectedCells[0].RowIndex;
      var c = dgvwWagerLimitsDetails.SelectedCells[0].ColumnIndex;
      if ((c != CU_LIMIT_INDEX && c != INET_LIMIT_INDEX && c != TEMP_CU_LIMIT_INDEX &&
          c != TEMP_INET_LIMIT_INDEX)) return;
      if (e.KeyCode == Keys.Tab) {
        e.Handled = true;
        txtMaxParlayBet.Focus();
      }

      if (e.Control && e.KeyCode == Keys.V) {
        char[] rowSplitter = { '\r', '\n' };
        char[] columnSplitter = { '\t' };
        var dataInClipboard = Clipboard.GetDataObject();
        //cmd.exe /c "echo off | clip"
        if (dataInClipboard == null) return;
        var stringInClipboard = (string)dataInClipboard.GetData(DataFormats.Text);
        var rowsInClipboard = stringInClipboard.Split(rowSplitter, StringSplitOptions.RemoveEmptyEntries);

        for (var iRow = 0; iRow < rowsInClipboard.Length; iRow++) {
          var valuesInRow = rowsInClipboard[iRow].Split(columnSplitter);
          if ((r + iRow) >= dgvwWagerLimitsDetails.Rows.Count)
            continue;
          for (var iCol = 0; iCol < valuesInRow.Length; iCol++) {
            try {
              if (dgvwWagerLimitsDetails.ColumnCount - 1 >= c + iCol) {
                if (dgvwWagerLimitsDetails.Rows[r + iRow].Cells[c + iCol].ColumnIndex > 2) {
                  int targetValue;
                  int.TryParse(valuesInRow[iCol].Replace(",", ""), out targetValue);
                  dgvwWagerLimitsDetails.Rows[r + iRow].Cells[c + iCol].Value = FormatNumber(targetValue, false, 1, true);
                  AddWagerLimitEditedRowToControlList(GetWagerLimitObject(r + iRow));
                }
              }
            }
            catch (Exception) {
              // ignored
            }
          }
        }
      }
    }

    private void btnHorses_Click(object sender, EventArgs e) {
      HorseRacingLimitsShow();
    }

    private void frmCustomerMaintenance_FormClosing(object sender, FormClosingEventArgs e) {
      HandleFormClosingEvent();
    }

    private void frmCustomerMaintenance_Resize(object sender, EventArgs e) {
    }

    private void tsmiCalculator_Click(object sender, EventArgs e) {
      var p = Process.Start("calc.exe");
      if (p != null) p.WaitForInputIdle();
    }

    private void btnFreePlayDelete_Click(object sender, EventArgs e) {
      DeleteFreePlay();
    }

    private void tsmiSave_Click(object sender, EventArgs e) {
      if (LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.UPDATE_CUSTOMER_MAINTENANCE)) {
        SaveCustomerInfo();
        txtCustomerId.Text = "";
      }
      else {
        MessageBox.Show(Environment.UserName + @" does not have permissions to " + LoginsAndProfiles.UPDATE_CUSTOMER_MAINTENANCE);
      }

    }

    private void chbFreeHalfPoints_CheckedChanged(object sender, EventArgs e) {
      HandleFreeHalfPointsChange(sender);
    }

    private void dgvwVigDiscount_DataError(object sender, DataGridViewDataErrorEventArgs e) {
      switch (e.ColumnIndex) {
        case 4:
          MessageBox.Show(@"Please enter a valid CU Vig Discount Percentage [0-100].");
          break;

        case 5:
          MessageBox.Show(@"Please enter a valid CU Vig Discount Exp Date [mm/dd/yyyy].");
          break;

        case 6:
          MessageBox.Show(@"Please enter a valid Vig Discount Percentage [0-100].");
          break;

        case 7:
          MessageBox.Show(@"Please enter a valid Vig Discount Date [mm/dd/yyyy].");
          break;
      }

      e.ThrowException = false;
    }

    private void dgvwVigDiscount_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e) {
      var gv = (DataGridView)sender;

      var i = gv.CurrentCell.ColumnIndex;

      if (i == 4 || i == 6)
        e.Control.KeyPress += numberAmount_KeyPress;

      if (i == 5 || i == 7)
        e.Control.KeyPress += DateControl_KeyPress;
    }

    private void DateControl_KeyPress(object sender, KeyPressEventArgs e) {

      int ascii = Convert.ToInt16(e.KeyChar);
      if ((ascii >= 48 && ascii <= 57) || (ascii == 8) || (ascii == 47)) {
        e.Handled = false;
      }
      else {
        e.Handled = true;
      }
    }

    public void numberAmount_KeyPress(object sender, KeyPressEventArgs e) {
      int ascii = Convert.ToInt16(e.KeyChar);
      if ((ascii >= 48 && ascii <= 57) || (ascii == 8)) {
        e.Handled = false;
      }
      else {
        e.Handled = true;
      }
    }

    private void btnDeleteTran_Click(object sender, EventArgs e) {
      DeleteTransaction();
    }

    private void numberToText_Leave(object sender, EventArgs e) {

      var ntxtB = (NumberTextBox)sender;

      if (ntxtB.Text != "") {
        ((TextBox)sender).Text = FormatNumber(DisplayNumber.RemoveFormat(((TextBox)sender).Text) * ntxtB.DividedBy, false);
      }
    }

    private void txtThroughCredIncrease_KeyUp(object sender, KeyEventArgs e) {
      if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back) {
        ((TextBox)sender).Text = "";
      }
    }

    private void txtCustomerId_KeyPress(object sender, KeyPressEventArgs e) {
      if (e.KeyChar.ToString(CultureInfo.InvariantCulture) != "\r" || ((TextBox)sender).Text == "") return;
      CustomerInfoRetrieved = false;
      _storesInfoRetrieved = false;
      txtThroughCredIncrease.ReadOnly = false;
      txtThroughQuickLimit.ReadOnly = false;
      _vigDiscountInfoRetrieved = false;
      _agentInfoRetrieved = false;
      _default7DaysTransRetrieved = false;
      _default7DaysFreePlaysRetrieved = false;
      _wagerLimitsInfoRetrieved = false;
      _customerRestrictionsRetrieved = false;
      editCurrentToolStripMenuItem_Click(this, e);
    }

    private void frmCustomerMaintenance_KeyDown(object sender, KeyEventArgs e) {
      HandleKeyDown(e);
    }

    private void chbAccumWagerLimits_CheckedChanged(object sender, EventArgs e) {
      var chbName = ((CheckBox)sender).Name;

      if (!((CheckBox)sender).Checked) return;
      foreach (Control ctrl in grpEnforceWagerLimits.Controls) {
        if (ctrl.GetType().ToString() != "System.Windows.Forms.CheckBox") continue;
        if (ctrl.Name == chbName) continue;
        var chb = (CheckBox)ctrl;
        chb.Checked = false;
      }
    }

    private void tsmiFindCustomer_Click(object sender, EventArgs e) {
      FindCustomerShow();
    }

    private void tsmiReset_Click(object sender, EventArgs e) {
      ResetCustomer();
    }

    private void tsmiTicketWriter_Click(object sender, EventArgs e) {
      Process.Start("TicketWriter.exe");
    }

    private void exitToolStripMenuItem_Click(object sender, EventArgs e) {
      Close();
    }

    private void dgvwCustomerPerformance_SelectionChanged(object sender, EventArgs e) {
      double total = 0;
      var str = string.Empty;

      foreach (DataGridViewCell cell in dgvwCustomerPerformance.SelectedCells) {
        if (cell.FormattedValue != null) str = cell.FormattedValue.ToString();
        double val;
        if (double.TryParse(str, out val)) {
          total += val;
        }
      }
      txtSelectedPerformance.Text = FormatNumber(total, false);
    }

    private void cmbWLSports_SelectedIndexChanged(object sender, EventArgs e) {
      GetCustomerWagerLimitsInfo();
    }

    private void btnRestrictionParams_Click(object sender, EventArgs e) {
      if (clbActions.SelectedItems.Count <= 0) return;
      var action = (spCstGetActions_Result)clbActions.SelectedItem;
      List<spCstGetActionParams_Result> prms;
      using (var cst = new Customers(AppModuleInfo)) {
        prms = cst.GetActionParams(action.ActionId);
      }
      if (prms != null && prms.Count > 0) {
        if (action.CustomerId == null) action.CustomerId = CustomerId;
        var frm = new FrmCustomerRestrictedActionParams(action, AppModuleInfo, ref _restrictedActionsParams);
        frm.ShowDialog();
        WriteTxtActionDescription(action);
      }
      GetCustomerActionsAndRestrictionsLogs();
    }

    private void clbActions_SelectedIndexChanged(object sender, EventArgs e) {
      if (clbActions.SelectedIndex < 0) return;
      var action = (spCstGetActions_Result)clbActions.SelectedItem;
      RemoveActionParams(action);
      txtActionDescription.Text = action.Description;
      if (action.HasParameters != null && !(bool)action.HasParameters)
        btnRestrictionParams.Enabled = false;
      else {
        btnRestrictionParams.Enabled = CheckIfItemIsChecked(sender, action);
        if (_restrictedActionsParams == null) {
          _restrictedActionsParams = new List<spCstGetRestrictedActionParams_Result>();
        }

        using (var cst = new Customers(AppModuleInfo)) {
          _restrictedActionsParams.AddRange(cst.GetRestrictedActionParams(action.ActionId, action.CustomerId));
        }

        WriteTxtActionDescription(action);
      }
    }

    private bool CheckIfItemIsChecked(object sender, spCstGetActions_Result action) {
      return ((CheckedListBox)sender).CheckedItems.Cast<spCstGetActions_Result>().Any(checkedItem => checkedItem.ActionId == action.ActionId);
    }

    private void WriteTxtActionDescription(spCstGetActions_Result action) {

      using (var cst = new Customers(AppModuleInfo)) {
        _actions = cst.GetActions(CustomerId);
      }

      var restrictedActionId = (from a in _actions where a.ActionId == action.ActionId select a.RestrictedActionId).FirstOrDefault();

      txtActionDescription.Text = action.Description;
      if (_restrictedActionsParams == null || _restrictedActionsParams.Count <= 0 ||
          !(from rp in _restrictedActionsParams where rp.RestrictedActionId == restrictedActionId select rp).Any())
        return;
      foreach (var param in _restrictedActionsParams.Where(param => param.ActionID == action.ActionId)) {
        txtActionDescription.Text += Environment.NewLine;
        txtActionDescription.Text += param.FriendlyName + @": " + FormatNumber(double.Parse(param.Value), false);
      }
    }

    private void clbActions_ItemCheck(object sender, ItemCheckEventArgs e) {
      if (e.CurrentValue == CheckState.Indeterminate) {
        e.NewValue = CheckState.Indeterminate;
        return;
      }
      if (clbActions.SelectedIndex < 0) return;
      var action = (spCstGetActions_Result)clbActions.SelectedItem;
      var actionDesc = "Activated";
      if (e.NewValue == CheckState.Checked) {
        List<spCstGetActionParams_Result> prms;
        action.CustomerId = CustomerId;
        using (var cst = new Customers(AppModuleInfo)) {
          cst.RestrictAction(CustomerId, action.ActionId, Environment.UserName);
          prms = cst.GetActionParams(action.ActionId);
        }
        if (prms != null && prms.Count > 0) {
          var frm = new FrmCustomerRestrictedActionParams(action, AppModuleInfo, ref _restrictedActionsParams);
          frm.ShowDialog();
          WriteTxtActionDescription(action);
        }
      }
      else {
        action.CustomerId = null;
        actionDesc = "Deactivated";
        using (var cst = new Customers(AppModuleInfo)) {
          cst.AllowAction(CustomerId, action.ActionId, Environment.UserName);
          RemoveActionParams(action);
          WriteTxtActionDescription(action);
        }
      }

      using (var lw = new LogWriter(AppModuleInfo)) {
        var logInfo = new LogWriter.CuAccessLogInfo {
          ProgramName = AppModuleInfo.Description,
          Operation = "-Restriction- " + actionDesc + " [" + action.Name + "]",
          Data = CustomerId
        };
        lw.WriteToCuAccessLog(logInfo);
      }

      GetCustomerActionsAndRestrictionsLogs();
      _actions = null;
      DisplayCustomerRestrictions();
      GetCustomerRestrictions(CustomerId);
    }

    private void RemoveActionParams(spCstGetActions_Result action) {
      if (_restrictedActionsParams == null || !_restrictedActionsParams.Any()) return;
      var itemsToRemove = _restrictedActionsParams.Where(item => item.ActionID == action.ActionId).ToList();

      foreach (var i in itemsToRemove) {
        _restrictedActionsParams.Remove(i);
      }
    }

    private void frmCustomerMaintenance_Activated(object sender, EventArgs e) {
      txtCustomerId.Focus();
    }

    private void txtWageringPassword_Click(object sender, EventArgs e) {
      txtWageringPassword.SelectAll();
    }

    private void txtPayoutPassword_Click(object sender, EventArgs e) {
      txtPayoutPassword.SelectAll();
    }

    private void btnChangeCustMakeup_Click(object sender, EventArgs e) {
      ShowRebatesMaintenanceForm();
    }

    private void lblChangeMakeup_DoubleClick(object sender, EventArgs e) {
      if (LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.SET_REBATES_TO_CUSTOMER)) {
        txtNewMakeup.Enabled = txtNewMakeup.Enabled == false;
      }
      else {
        txtNewMakeup.Enabled = false;
      }
    }

    public void cmbWagerTypes_SelectedIndexChanged(object sender, EventArgs e) {
      if (PerformanceViewDateRange == null)
        return;
      PopulateCustomerPerformanceGv(PerformanceViewMode);
    }

    public void cmbTranTypes_SelectedIndexChanged(object sender, EventArgs e) {
      if (cmbDisplay != null && cmbDisplay.Items.Count > 0 && cmbDisplay.SelectedIndex > -1) {
        PopulateCustomerTransactionsGv(cmbDisplay.SelectedIndex);
        ChangeGridCreditDebitColumnsHeaderText(cmbTranTypes.Text);
        if (dgvwTransactions.Rows.Count != 0) return;
        btnTranGotoWagers.Enabled = false;
        btnTranBrowse.Enabled = false;
        btnDeleteTran.Enabled = false;
        btnLastVerified.Enabled = false;
      }
    }

    private void ChangeGridCreditDebitColumnsHeaderText(string ddText) {
      if (ddText == "" || dgvwTransactions.Columns.Count < 5) return;
      if (ddText == "Cash Transactions") {
        dgvwTransactions.Columns[3].HeaderText = @"Credit (-)";
        dgvwTransactions.Columns[4].HeaderText = @"Debit (+)";
      }
      else {
        dgvwTransactions.Columns[3].HeaderText = @"Credit";
        dgvwTransactions.Columns[4].HeaderText = @"Debit";
      }
    }

    private void txtTempQuickLimit_TextChanged(object sender, EventArgs e) {
      SetQuickLimitInputState();
    }

    private void txtThroughQuickLimit_TextChanged(object sender, EventArgs e) { SetQuickLimitInputState(); }

    void oDateTimePicker_DropDown(object sender, EventArgs e) {
      _dateTimePickerExpanded = true;
    }

    void oDateTimePicker_Leave(object sender, EventArgs e) {
      if (!_dateTimePickerExpanded)
        _dateTimePickerExpanded = _oDateTimePicker.Visible = false;
    }

    private void oDateTimePicker_TextChanged(object sender, EventArgs e) {
      dgvwWagerLimitsDetails.CurrentCell.Value = _oDateTimePicker.Text;
      AddWagerLimitEditedRowToControlList(GetWagerLimitObject(dgvwWagerLimitsDetails.CurrentCell.RowIndex));
    }

    void oDateTimePicker_CloseUp(object sender, EventArgs e) {
      // Hiding the control after use   
      _dateTimePickerExpanded = _oDateTimePicker.Visible = false;
    }

    private void lklReturnToAgent_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
      var customerId = ((LinkLabel)sender).AccessibleName;
      _showAgentPlayersListTab.LoadCustomer(customerId);
    }

    private void btnParlayLimits_Click(object sender, EventArgs e) {
      DetailedParlayLimitsShow();
    }

    private void DetailedParlayLimitsShow() {
      using (var detailedParlayLimits = new FrmCustomerParlayTeaserWagerLimits(this, WagerType.PARLAY)) {
        detailedParlayLimits.Icon = Icon;
        detailedParlayLimits.Text = WagerType.PARLAYNAME + @" Wager Limits";
        detailedParlayLimits.ShowDialog();
      }
    }

    private void btnTeaserLimits_Click(object sender, EventArgs e) {
      DetailedTeaserLimitsShow();
    }

    private void DetailedTeaserLimitsShow() {
      using (var detailedTeaserLimits = new FrmCustomerParlayTeaserWagerLimits(this, WagerType.TEASER)) {
        detailedTeaserLimits.Icon = Icon;
        detailedTeaserLimits.Text = WagerType.TEASERNAME + @" Wager Limits";
        detailedTeaserLimits.ShowDialog();
      }
    }

    private void chbEnforceParlayBetLimit_CheckedChanged(object sender, EventArgs e) {
      HandleEnforceParlayBetLimitCheck(sender);
    }

    private void HandleEnforceParlayBetLimitCheck(object sender) {
      btnParlayLimits.Enabled = true;
      if (!((CheckBox)sender).Checked) return;
      var dialogResult = MessageBox.Show(@"This will enforce Max Parlay Bet Value for all Parlays", @"Warning", MessageBoxButtons.YesNo);
      if (dialogResult == DialogResult.Yes) {
        btnParlayLimits.Enabled = false;
        if (txtMaxParlayBet.Text != "" && txtMaxParlayBet.Text != @"0") return;
        txtMaxParlayBet.Text = txtCuMinBet.Text;
        txtMaxParlayBet.Focus();
      }
      else {
        ((CheckBox)sender).Checked = false;
      }
    }

    private void chbEnforceTeaserBetLimit_CheckedChanged(object sender, EventArgs e) {
      HandleEnforceTeaserBetLimitCheck(sender);
    }

    private void HandleEnforceTeaserBetLimitCheck(object sender) {
      btnTeaserLimits.Enabled = true;
      if (!((CheckBox)sender).Checked) return;
      var dialogResult = MessageBox.Show(@"This will enforce Max Teaser Bet Value for all Teasers", @"Warning", MessageBoxButtons.YesNo);
      if (dialogResult == DialogResult.Yes) {
        btnTeaserLimits.Enabled = false;
        if (txtMaxTeaserBet.Text != "" && txtMaxTeaserBet.Text != @"0") return;
        txtMaxTeaserBet.Text = txtCuMinBet.Text;
        txtMaxTeaserBet.Focus();
      }
      else {
        ((CheckBox)sender).Checked = false;
      }
    }

    private void btnResetWagerLimits_Click(object sender, EventArgs e) {
      var dialogResult = MessageBox.Show(@"This Action will reset all Detailed Limits. Proceed?", @"Attention", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
      if (dialogResult != DialogResult.Yes) return;
      ResetCustomerWagerLimits();
      RestoreWagerLimitsTabLabel();
    }

    private void chbDisableSports_CheckedChanged(object sender, EventArgs e) {
      HandleDisableSportsCheckbox(sender);
    }

    private void HandleDisableSportsCheckbox(object sender) {
      var selectedSport = GetSelectedWlSport();
      var selectedSubSport = string.Empty;
      var checkAllSports = ((CheckBox)sender).Checked;

      foreach (DataGridViewRow row in dgvwWagerLimitsDetails.Rows) {
        var rowSportType = row.Cells[SPORT_INDEX].Value.ToString().Trim();
        var rowSportSubType = row.Cells[SUB_SPORT_INDEX].Value.ToString().Trim();
        if (cmbWLSports.SelectedIndex == 0) {
          AddWagerLimitEditedRowToControlList(GetWagerLimitObject(row.Index, checkAllSports));
        }
        else {
          if (rowSportType == selectedSport && (selectedSubSport == string.Empty || rowSportSubType == selectedSubSport)) {
            AddWagerLimitEditedRowToControlList(GetWagerLimitObject(row.Index, checkAllSports));
          }
        }
      }

      if (WagerLimits == null)
        return;
      dgvwWagerLimitsDetails.CellClick -= dgvwWagerLimitsDetails_CellClick;
      dgvwWagerLimitsDetails.CellContentClick -= dgvwWagerLimitsDetails_CellContentClick;
      dgvwWagerLimitsDetails.CellEndEdit -= dgvwWagerLimitsDetails_CellEndEdit;
      dgvwWagerLimitsDetails.CellValidating -= dgvwWagerLimitsDetails_CellValidating;
      dgvwWagerLimitsDetails.DataError -= dgvwWagerLimitsDetails_DataError;
      dgvwWagerLimitsDetails.EditingControlShowing -= dgvwWagerLimitsDetails_EditingControlShowing;
      dgvwWagerLimitsDetails.KeyDown -= dgvwWagerLimitsDetails_KeyDown;
      PopulateWagerLimitsGrid();
      dgvwWagerLimitsDetails.CellClick += dgvwWagerLimitsDetails_CellClick;
      dgvwWagerLimitsDetails.CellContentClick += dgvwWagerLimitsDetails_CellContentClick;
      dgvwWagerLimitsDetails.CellEndEdit += dgvwWagerLimitsDetails_CellEndEdit;
      dgvwWagerLimitsDetails.CellValidating += dgvwWagerLimitsDetails_CellValidating;
      dgvwWagerLimitsDetails.DataError += dgvwWagerLimitsDetails_DataError;
      dgvwWagerLimitsDetails.EditingControlShowing += dgvwWagerLimitsDetails_EditingControlShowing;
      dgvwWagerLimitsDetails.KeyDown += dgvwWagerLimitsDetails_KeyDown;

      GetDisabledSportsCount();
    }

    private void chbShadeGroupsAutoCreate_CheckedChanged(object sender, EventArgs e) {
      if (((CheckBox)sender).Checked) return;
      if (!HandleShadeGroupsAutoCreateClick()) {
        ((CheckBox)sender).Checked = true;
      }
    }

    private void dgvwWagerLimitsDetails_Scroll(object sender, ScrollEventArgs e) {
      HideCheckBoxes(sender);
    }

    private void HideCheckBoxes(object sender) {
      var chbDisableSports = (CheckBox)((DataGridView)sender).Controls.Find("chbDisableSports", true).FirstOrDefault();
      if (chbDisableSports == null) return;
      chbDisableSports.Visible = false;
      var visibleRowsCount = ((DataGridView)sender).DisplayedRowCount(true);
      if (((DataGridView)sender).FirstDisplayedCell == null)
        return;
      var firstDisplayedRowIndex = ((DataGridView)sender).FirstDisplayedCell.RowIndex;
      var lastvibileRowIndex = (firstDisplayedRowIndex + visibleRowsCount) - 1;
      for (var rowIndex = firstDisplayedRowIndex; rowIndex <= lastvibileRowIndex; rowIndex++) {
        var cells = ((DataGridView)sender).Rows[rowIndex].Cells;
        foreach (DataGridViewCell cell in cells) {
          if (cell.ColumnIndex != 0 || !cell.Displayed) continue;
          chbDisableSports.Visible = true;
          break;
        }
      }
    }

    private void dgvwWagerLimitsDetails_SelectionChanged(object sender, EventArgs e) {
      HideCheckBoxes(sender);
    }

    private void lblPendingBets_MouseDoubleClick(object sender, MouseEventArgs e) {
      ShowPendingBetsForm();

    }

    private void btnPendingWagers_Click(object sender, EventArgs e) {
      ShowPendingBetsForm();
    }

    private void btnSavePackageToFile_Click(object sender, EventArgs e) {
      if (!_packageToCsv.IsActive()) return;
      using (var fd = new SaveFileDialog()) {
        fd.Filter = @"CSV Files|*.csv";
        fd.FileName = CustomerId;
        if (fd.ShowDialog() != DialogResult.OK) return;
        _packageToCsv.SaveFile(fd.FileName, CustomerId, ServerDateTime);
        if (_packageToCsv.Erros.Any()) {
          MessageBox.Show(String.Join(", ", _packageToCsv.Erros), @"Error", MessageBoxButtons.OK);
        }
        else if (MessageBox.Show(@"File updated successfully! Do you want to remove customers info?", @"Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) == DialogResult.Yes) {
          var removeNotes = MessageBox.Show(@"Do you want to remove customers notes?", @"Information", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) == DialogResult.Yes;
          _packageToCsv.CleanAgentPackageInfo(CustomerId, removeNotes);
          MessageBox.Show(@"Info removed successfully", @"Information", MessageBoxButtons.OK);
        }
      }
    }

    #endregion

  }
}