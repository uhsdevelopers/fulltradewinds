﻿namespace CustomerMaintenance.UI
{
    partial class FrmCustomerTransactionDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panTranTypeSection = new System.Windows.Forms.Panel();
            this.btnPayto = new System.Windows.Forms.Button();
            this.panTranMethod = new System.Windows.Forms.Panel();
            this.panTranType = new System.Windows.Forms.Panel();
            this.txtEnteredAmount = new System.Windows.Forms.TextBox();
            this.txtTranDateTime = new System.Windows.Forms.TextBox();
            this.lblMethod = new System.Windows.Forms.Label();
            this.lblAmount = new System.Windows.Forms.Label();
            this.lblTransactionType = new System.Windows.Forms.Label();
            this.lblDateTime = new System.Windows.Forms.Label();
            this.grpRefunds = new System.Windows.Forms.GroupBox();
            this.txtPromo = new System.Windows.Forms.TextBox();
            this.txtFees = new System.Windows.Forms.TextBox();
            this.txtFreePlay = new System.Windows.Forms.TextBox();
            this.lblPromo = new System.Windows.Forms.Label();
            this.lblFees = new System.Windows.Forms.Label();
            this.lblFreePlay = new System.Windows.Forms.Label();
            this.grpUncollectedFunds = new System.Windows.Forms.GroupBox();
            this.lblTranAmount = new System.Windows.Forms.Label();
            this.lblAmountToRelease = new System.Windows.Forms.Label();
            this.lblHoldAmount = new System.Windows.Forms.Label();
            this.lblDays = new System.Windows.Forms.Label();
            this.txtNumberOfDays = new System.Windows.Forms.TextBox();
            this.lblPercOfTranAmount = new System.Windows.Forms.Label();
            this.txtHold = new System.Windows.Forms.TextBox();
            this.chbHold = new System.Windows.Forms.CheckBox();
            this.grpCreditCard = new System.Windows.Forms.GroupBox();
            this.lblExcessAmt = new System.Windows.Forms.Label();
            this.lblExcess = new System.Windows.Forms.Label();
            this.lblCoveredAmt = new System.Windows.Forms.Label();
            this.lblCovered = new System.Windows.Forms.Label();
            this.btnCoverDeposit = new System.Windows.Forms.Button();
            this.txtCCExpDate = new System.Windows.Forms.TextBox();
            this.lblCCExpDate = new System.Windows.Forms.Label();
            this.txtApprovalNo = new System.Windows.Forms.TextBox();
            this.txtCardNo = new System.Windows.Forms.TextBox();
            this.lblApprovalNo = new System.Windows.Forms.Label();
            this.lblCardNo = new System.Windows.Forms.Label();
            this.panDocNumber = new System.Windows.Forms.Panel();
            this.txtEnteredBy = new System.Windows.Forms.TextBox();
            this.lblEnteredBy = new System.Windows.Forms.Label();
            this.txtGradeNumber = new System.Windows.Forms.TextBox();
            this.lblGradeNumber = new System.Windows.Forms.Label();
            this.txtDocumentNumber = new System.Windows.Forms.TextBox();
            this.lblDocumentNumber = new System.Windows.Forms.Label();
            this.grpTranDescription = new System.Windows.Forms.GroupBox();
            this.txtReference = new System.Windows.Forms.TextBox();
            this.txtTranDescription = new System.Windows.Forms.TextBox();
            this.lblReference = new System.Windows.Forms.Label();
            this.grpAdjustments = new System.Windows.Forms.GroupBox();
            this.chbCasinoAdjustment = new System.Windows.Forms.CheckBox();
            this.dtmChangeTranDailyFigure = new System.Windows.Forms.DateTimePicker();
            this.txtDailyFigureDate = new System.Windows.Forms.TextBox();
            this.lblFigureDate = new System.Windows.Forms.Label();
            this.chbAdjustCustomersFigure = new System.Windows.Forms.CheckBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblArchivedTransaction = new System.Windows.Forms.Label();
            this.panTranTypeSection.SuspendLayout();
            this.grpRefunds.SuspendLayout();
            this.grpUncollectedFunds.SuspendLayout();
            this.grpCreditCard.SuspendLayout();
            this.panDocNumber.SuspendLayout();
            this.grpTranDescription.SuspendLayout();
            this.grpAdjustments.SuspendLayout();
            this.SuspendLayout();
            // 
            // panTranTypeSection
            // 
            this.panTranTypeSection.Controls.Add(this.btnPayto);
            this.panTranTypeSection.Controls.Add(this.panTranMethod);
            this.panTranTypeSection.Controls.Add(this.panTranType);
            this.panTranTypeSection.Controls.Add(this.txtEnteredAmount);
            this.panTranTypeSection.Controls.Add(this.txtTranDateTime);
            this.panTranTypeSection.Controls.Add(this.lblMethod);
            this.panTranTypeSection.Controls.Add(this.lblAmount);
            this.panTranTypeSection.Controls.Add(this.lblTransactionType);
            this.panTranTypeSection.Controls.Add(this.lblDateTime);
            this.panTranTypeSection.Location = new System.Drawing.Point(4, 13);
            this.panTranTypeSection.Name = "panTranTypeSection";
            this.panTranTypeSection.Size = new System.Drawing.Size(271, 155);
            this.panTranTypeSection.TabIndex = 10;
            // 
            // btnPayto
            // 
            this.btnPayto.Location = new System.Drawing.Point(183, 128);
            this.btnPayto.Name = "btnPayto";
            this.btnPayto.Size = new System.Drawing.Size(75, 23);
            this.btnPayto.TabIndex = 100;
            this.btnPayto.Text = "Pay To...";
            this.btnPayto.UseVisualStyleBackColor = true;
            this.btnPayto.Click += new System.EventHandler(this.btnPayto_Click);
            // 
            // panTranMethod
            // 
            this.panTranMethod.Location = new System.Drawing.Point(104, 96);
            this.panTranMethod.Name = "panTranMethod";
            this.panTranMethod.Size = new System.Drawing.Size(155, 28);
            this.panTranMethod.TabIndex = 90;
            // 
            // panTranType
            // 
            this.panTranType.Location = new System.Drawing.Point(104, 36);
            this.panTranType.Name = "panTranType";
            this.panTranType.Size = new System.Drawing.Size(155, 28);
            this.panTranType.TabIndex = 50;
            // 
            // txtEnteredAmount
            // 
            this.txtEnteredAmount.Location = new System.Drawing.Point(104, 70);
            this.txtEnteredAmount.Name = "txtEnteredAmount";
            this.txtEnteredAmount.Size = new System.Drawing.Size(155, 20);
            this.txtEnteredAmount.TabIndex = 70;
            this.txtEnteredAmount.TextChanged += new System.EventHandler(this.txtEnteredAmount_TextChanged);
            this.txtEnteredAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numberAmount_KeyPress);
            this.txtEnteredAmount.Leave += new System.EventHandler(this.txtEnteredAmount_Leave);
            // 
            // txtTranDateTime
            // 
            this.txtTranDateTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTranDateTime.Enabled = false;
            this.txtTranDateTime.Location = new System.Drawing.Point(104, 10);
            this.txtTranDateTime.Name = "txtTranDateTime";
            this.txtTranDateTime.ReadOnly = true;
            this.txtTranDateTime.Size = new System.Drawing.Size(155, 20);
            this.txtTranDateTime.TabIndex = 30;
            // 
            // lblMethod
            // 
            this.lblMethod.AutoSize = true;
            this.lblMethod.Location = new System.Drawing.Point(52, 103);
            this.lblMethod.Name = "lblMethod";
            this.lblMethod.Size = new System.Drawing.Size(46, 13);
            this.lblMethod.TabIndex = 80;
            this.lblMethod.Text = "Method:";
            this.lblMethod.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblAmount
            // 
            this.lblAmount.AutoSize = true;
            this.lblAmount.Location = new System.Drawing.Point(52, 74);
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Size = new System.Drawing.Size(46, 13);
            this.lblAmount.TabIndex = 60;
            this.lblAmount.Text = "Amount:";
            this.lblAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblTransactionType
            // 
            this.lblTransactionType.AutoSize = true;
            this.lblTransactionType.Location = new System.Drawing.Point(5, 45);
            this.lblTransactionType.Name = "lblTransactionType";
            this.lblTransactionType.Size = new System.Drawing.Size(93, 13);
            this.lblTransactionType.TabIndex = 40;
            this.lblTransactionType.Text = "Transaction Type:";
            this.lblTransactionType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDateTime
            // 
            this.lblDateTime.AutoSize = true;
            this.lblDateTime.Location = new System.Drawing.Point(37, 13);
            this.lblDateTime.Name = "lblDateTime";
            this.lblDateTime.Size = new System.Drawing.Size(61, 13);
            this.lblDateTime.TabIndex = 20;
            this.lblDateTime.Text = "Date/Time:";
            this.lblDateTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // grpRefunds
            // 
            this.grpRefunds.Controls.Add(this.txtPromo);
            this.grpRefunds.Controls.Add(this.txtFees);
            this.grpRefunds.Controls.Add(this.txtFreePlay);
            this.grpRefunds.Controls.Add(this.lblPromo);
            this.grpRefunds.Controls.Add(this.lblFees);
            this.grpRefunds.Controls.Add(this.lblFreePlay);
            this.grpRefunds.Location = new System.Drawing.Point(291, 13);
            this.grpRefunds.Name = "grpRefunds";
            this.grpRefunds.Size = new System.Drawing.Size(359, 71);
            this.grpRefunds.TabIndex = 210;
            this.grpRefunds.TabStop = false;
            this.grpRefunds.Text = "Refunds";
            // 
            // txtPromo
            // 
            this.txtPromo.Location = new System.Drawing.Point(291, 23);
            this.txtPromo.Name = "txtPromo";
            this.txtPromo.Size = new System.Drawing.Size(59, 20);
            this.txtPromo.TabIndex = 270;
            this.txtPromo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numberAmount_KeyPress);
            // 
            // txtFees
            // 
            this.txtFees.Location = new System.Drawing.Point(176, 23);
            this.txtFees.Name = "txtFees";
            this.txtFees.Size = new System.Drawing.Size(59, 20);
            this.txtFees.TabIndex = 250;
            this.txtFees.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numberAmount_KeyPress);
            // 
            // txtFreePlay
            // 
            this.txtFreePlay.Location = new System.Drawing.Point(68, 23);
            this.txtFreePlay.Name = "txtFreePlay";
            this.txtFreePlay.Size = new System.Drawing.Size(59, 20);
            this.txtFreePlay.TabIndex = 230;
            this.txtFreePlay.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numberAmount_KeyPress);
            // 
            // lblPromo
            // 
            this.lblPromo.AutoSize = true;
            this.lblPromo.Location = new System.Drawing.Point(243, 26);
            this.lblPromo.Name = "lblPromo";
            this.lblPromo.Size = new System.Drawing.Size(40, 13);
            this.lblPromo.TabIndex = 260;
            this.lblPromo.Text = "Promo:";
            // 
            // lblFees
            // 
            this.lblFees.AutoSize = true;
            this.lblFees.Location = new System.Drawing.Point(135, 26);
            this.lblFees.Name = "lblFees";
            this.lblFees.Size = new System.Drawing.Size(33, 13);
            this.lblFees.TabIndex = 240;
            this.lblFees.Text = "Fees:";
            // 
            // lblFreePlay
            // 
            this.lblFreePlay.AutoSize = true;
            this.lblFreePlay.Location = new System.Drawing.Point(6, 26);
            this.lblFreePlay.Name = "lblFreePlay";
            this.lblFreePlay.Size = new System.Drawing.Size(54, 13);
            this.lblFreePlay.TabIndex = 220;
            this.lblFreePlay.Text = "Free Play:";
            // 
            // grpUncollectedFunds
            // 
            this.grpUncollectedFunds.Controls.Add(this.lblTranAmount);
            this.grpUncollectedFunds.Controls.Add(this.lblAmountToRelease);
            this.grpUncollectedFunds.Controls.Add(this.lblHoldAmount);
            this.grpUncollectedFunds.Controls.Add(this.lblDays);
            this.grpUncollectedFunds.Controls.Add(this.txtNumberOfDays);
            this.grpUncollectedFunds.Controls.Add(this.lblPercOfTranAmount);
            this.grpUncollectedFunds.Controls.Add(this.txtHold);
            this.grpUncollectedFunds.Controls.Add(this.chbHold);
            this.grpUncollectedFunds.Location = new System.Drawing.Point(291, 90);
            this.grpUncollectedFunds.Name = "grpUncollectedFunds";
            this.grpUncollectedFunds.Size = new System.Drawing.Size(359, 93);
            this.grpUncollectedFunds.TabIndex = 280;
            this.grpUncollectedFunds.TabStop = false;
            this.grpUncollectedFunds.Text = "Uncollected Funds";
            // 
            // lblTranAmount
            // 
            this.lblTranAmount.AutoSize = true;
            this.lblTranAmount.Location = new System.Drawing.Point(306, 74);
            this.lblTranAmount.Name = "lblTranAmount";
            this.lblTranAmount.Size = new System.Drawing.Size(0, 13);
            this.lblTranAmount.TabIndex = 341;
            this.lblTranAmount.Visible = false;
            // 
            // lblAmountToRelease
            // 
            this.lblAmountToRelease.AutoSize = true;
            this.lblAmountToRelease.Location = new System.Drawing.Point(188, 60);
            this.lblAmountToRelease.Name = "lblAmountToRelease";
            this.lblAmountToRelease.Size = new System.Drawing.Size(0, 13);
            this.lblAmountToRelease.TabIndex = 14;
            // 
            // lblHoldAmount
            // 
            this.lblHoldAmount.AutoSize = true;
            this.lblHoldAmount.Location = new System.Drawing.Point(58, 60);
            this.lblHoldAmount.Name = "lblHoldAmount";
            this.lblHoldAmount.Size = new System.Drawing.Size(0, 13);
            this.lblHoldAmount.TabIndex = 13;
            // 
            // lblDays
            // 
            this.lblDays.AutoSize = true;
            this.lblDays.Location = new System.Drawing.Point(303, 27);
            this.lblDays.Name = "lblDays";
            this.lblDays.Size = new System.Drawing.Size(29, 13);
            this.lblDays.TabIndex = 340;
            this.lblDays.Text = "days";
            // 
            // txtNumberOfDays
            // 
            this.txtNumberOfDays.Location = new System.Drawing.Point(261, 23);
            this.txtNumberOfDays.Name = "txtNumberOfDays";
            this.txtNumberOfDays.Size = new System.Drawing.Size(36, 20);
            this.txtNumberOfDays.TabIndex = 320;
            this.txtNumberOfDays.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numberAmount_KeyPress);
            // 
            // lblPercOfTranAmount
            // 
            this.lblPercOfTranAmount.AutoSize = true;
            this.lblPercOfTranAmount.Location = new System.Drawing.Point(135, 26);
            this.lblPercOfTranAmount.Name = "lblPercOfTranAmount";
            this.lblPercOfTranAmount.Size = new System.Drawing.Size(120, 13);
            this.lblPercOfTranAmount.TabIndex = 310;
            this.lblPercOfTranAmount.Text = "% of transaction amount";
            // 
            // txtHold
            // 
            this.txtHold.Location = new System.Drawing.Point(61, 24);
            this.txtHold.Name = "txtHold";
            this.txtHold.Size = new System.Drawing.Size(59, 20);
            this.txtHold.TabIndex = 300;
            this.txtHold.TextChanged += new System.EventHandler(this.txtHold_TextChanged);
            this.txtHold.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numberAmount_KeyPress);
            // 
            // chbHold
            // 
            this.chbHold.AutoSize = true;
            this.chbHold.Location = new System.Drawing.Point(7, 26);
            this.chbHold.Name = "chbHold";
            this.chbHold.Size = new System.Drawing.Size(48, 17);
            this.chbHold.TabIndex = 290;
            this.chbHold.Text = "Hold";
            this.chbHold.UseVisualStyleBackColor = true;
            this.chbHold.CheckedChanged += new System.EventHandler(this.chbHold_CheckedChanged);
            // 
            // grpCreditCard
            // 
            this.grpCreditCard.Controls.Add(this.lblExcessAmt);
            this.grpCreditCard.Controls.Add(this.lblExcess);
            this.grpCreditCard.Controls.Add(this.lblCoveredAmt);
            this.grpCreditCard.Controls.Add(this.lblCovered);
            this.grpCreditCard.Controls.Add(this.btnCoverDeposit);
            this.grpCreditCard.Controls.Add(this.txtCCExpDate);
            this.grpCreditCard.Controls.Add(this.lblCCExpDate);
            this.grpCreditCard.Controls.Add(this.txtApprovalNo);
            this.grpCreditCard.Controls.Add(this.txtCardNo);
            this.grpCreditCard.Controls.Add(this.lblApprovalNo);
            this.grpCreditCard.Controls.Add(this.lblCardNo);
            this.grpCreditCard.Location = new System.Drawing.Point(291, 201);
            this.grpCreditCard.Name = "grpCreditCard";
            this.grpCreditCard.Size = new System.Drawing.Size(359, 122);
            this.grpCreditCard.TabIndex = 350;
            this.grpCreditCard.TabStop = false;
            this.grpCreditCard.Text = "Credit Card";
            // 
            // lblExcessAmt
            // 
            this.lblExcessAmt.AutoSize = true;
            this.lblExcessAmt.Location = new System.Drawing.Point(289, 96);
            this.lblExcessAmt.Name = "lblExcessAmt";
            this.lblExcessAmt.Size = new System.Drawing.Size(13, 13);
            this.lblExcessAmt.TabIndex = 460;
            this.lblExcessAmt.Text = "0";
            this.lblExcessAmt.Visible = false;
            // 
            // lblExcess
            // 
            this.lblExcess.AutoSize = true;
            this.lblExcess.Location = new System.Drawing.Point(233, 96);
            this.lblExcess.Name = "lblExcess";
            this.lblExcess.Size = new System.Drawing.Size(44, 13);
            this.lblExcess.TabIndex = 450;
            this.lblExcess.Text = "Excess:";
            this.lblExcess.Visible = false;
            // 
            // lblCoveredAmt
            // 
            this.lblCoveredAmt.AutoSize = true;
            this.lblCoveredAmt.Location = new System.Drawing.Point(107, 96);
            this.lblCoveredAmt.Name = "lblCoveredAmt";
            this.lblCoveredAmt.Size = new System.Drawing.Size(13, 13);
            this.lblCoveredAmt.TabIndex = 440;
            this.lblCoveredAmt.Text = "0";
            this.lblCoveredAmt.Visible = false;
            // 
            // lblCovered
            // 
            this.lblCovered.AutoSize = true;
            this.lblCovered.Location = new System.Drawing.Point(51, 96);
            this.lblCovered.Name = "lblCovered";
            this.lblCovered.Size = new System.Drawing.Size(50, 13);
            this.lblCovered.TabIndex = 430;
            this.lblCovered.Text = "Covered:";
            this.lblCovered.Visible = false;
            // 
            // btnCoverDeposit
            // 
            this.btnCoverDeposit.Location = new System.Drawing.Point(257, 55);
            this.btnCoverDeposit.Name = "btnCoverDeposit";
            this.btnCoverDeposit.Size = new System.Drawing.Size(93, 23);
            this.btnCoverDeposit.TabIndex = 420;
            this.btnCoverDeposit.Text = "Cover Deposit";
            this.btnCoverDeposit.UseVisualStyleBackColor = true;
            this.btnCoverDeposit.Click += new System.EventHandler(this.btnCoverDeposit_Click);
            // 
            // txtCCExpDate
            // 
            this.txtCCExpDate.Location = new System.Drawing.Point(277, 25);
            this.txtCCExpDate.Name = "txtCCExpDate";
            this.txtCCExpDate.Size = new System.Drawing.Size(73, 20);
            this.txtCCExpDate.TabIndex = 390;
            // 
            // lblCCExpDate
            // 
            this.lblCCExpDate.AutoSize = true;
            this.lblCCExpDate.Location = new System.Drawing.Point(243, 28);
            this.lblCCExpDate.Name = "lblCCExpDate";
            this.lblCCExpDate.Size = new System.Drawing.Size(28, 13);
            this.lblCCExpDate.TabIndex = 380;
            this.lblCCExpDate.Text = "Exp:";
            // 
            // txtApprovalNo
            // 
            this.txtApprovalNo.Location = new System.Drawing.Point(66, 58);
            this.txtApprovalNo.Name = "txtApprovalNo";
            this.txtApprovalNo.Size = new System.Drawing.Size(167, 20);
            this.txtApprovalNo.TabIndex = 410;
            // 
            // txtCardNo
            // 
            this.txtCardNo.Location = new System.Drawing.Point(68, 25);
            this.txtCardNo.Name = "txtCardNo";
            this.txtCardNo.Size = new System.Drawing.Size(167, 20);
            this.txtCardNo.TabIndex = 370;
            // 
            // lblApprovalNo
            // 
            this.lblApprovalNo.AutoSize = true;
            this.lblApprovalNo.Location = new System.Drawing.Point(6, 65);
            this.lblApprovalNo.Name = "lblApprovalNo";
            this.lblApprovalNo.Size = new System.Drawing.Size(62, 13);
            this.lblApprovalNo.TabIndex = 400;
            this.lblApprovalNo.Text = "Approval #:";
            // 
            // lblCardNo
            // 
            this.lblCardNo.AutoSize = true;
            this.lblCardNo.Location = new System.Drawing.Point(6, 28);
            this.lblCardNo.Name = "lblCardNo";
            this.lblCardNo.Size = new System.Drawing.Size(42, 13);
            this.lblCardNo.TabIndex = 360;
            this.lblCardNo.Text = "Card #:";
            // 
            // panDocNumber
            // 
            this.panDocNumber.Controls.Add(this.txtEnteredBy);
            this.panDocNumber.Controls.Add(this.lblEnteredBy);
            this.panDocNumber.Controls.Add(this.txtGradeNumber);
            this.panDocNumber.Controls.Add(this.lblGradeNumber);
            this.panDocNumber.Controls.Add(this.txtDocumentNumber);
            this.panDocNumber.Controls.Add(this.lblDocumentNumber);
            this.panDocNumber.Location = new System.Drawing.Point(291, 357);
            this.panDocNumber.Name = "panDocNumber";
            this.panDocNumber.Size = new System.Drawing.Size(359, 71);
            this.panDocNumber.TabIndex = 470;
            // 
            // txtEnteredBy
            // 
            this.txtEnteredBy.Location = new System.Drawing.Point(88, 42);
            this.txtEnteredBy.Name = "txtEnteredBy";
            this.txtEnteredBy.Size = new System.Drawing.Size(261, 20);
            this.txtEnteredBy.TabIndex = 530;
            // 
            // lblEnteredBy
            // 
            this.lblEnteredBy.AutoSize = true;
            this.lblEnteredBy.Location = new System.Drawing.Point(13, 45);
            this.lblEnteredBy.Name = "lblEnteredBy";
            this.lblEnteredBy.Size = new System.Drawing.Size(62, 13);
            this.lblEnteredBy.TabIndex = 520;
            this.lblEnteredBy.Text = "Entered By:";
            // 
            // txtGradeNumber
            // 
            this.txtGradeNumber.Location = new System.Drawing.Point(249, 11);
            this.txtGradeNumber.Name = "txtGradeNumber";
            this.txtGradeNumber.Size = new System.Drawing.Size(100, 20);
            this.txtGradeNumber.TabIndex = 510;
            // 
            // lblGradeNumber
            // 
            this.lblGradeNumber.AutoSize = true;
            this.lblGradeNumber.Location = new System.Drawing.Point(194, 14);
            this.lblGradeNumber.Name = "lblGradeNumber";
            this.lblGradeNumber.Size = new System.Drawing.Size(49, 13);
            this.lblGradeNumber.TabIndex = 500;
            this.lblGradeNumber.Text = "Grade #:";
            // 
            // txtDocumentNumber
            // 
            this.txtDocumentNumber.Location = new System.Drawing.Point(88, 11);
            this.txtDocumentNumber.Name = "txtDocumentNumber";
            this.txtDocumentNumber.Size = new System.Drawing.Size(100, 20);
            this.txtDocumentNumber.TabIndex = 490;
            // 
            // lblDocumentNumber
            // 
            this.lblDocumentNumber.AutoSize = true;
            this.lblDocumentNumber.Location = new System.Drawing.Point(13, 14);
            this.lblDocumentNumber.Name = "lblDocumentNumber";
            this.lblDocumentNumber.Size = new System.Drawing.Size(69, 13);
            this.lblDocumentNumber.TabIndex = 480;
            this.lblDocumentNumber.Text = "Document #:";
            // 
            // grpTranDescription
            // 
            this.grpTranDescription.Controls.Add(this.txtReference);
            this.grpTranDescription.Controls.Add(this.txtTranDescription);
            this.grpTranDescription.Controls.Add(this.lblReference);
            this.grpTranDescription.Location = new System.Drawing.Point(4, 176);
            this.grpTranDescription.Name = "grpTranDescription";
            this.grpTranDescription.Size = new System.Drawing.Size(271, 191);
            this.grpTranDescription.TabIndex = 110;
            this.grpTranDescription.TabStop = false;
            this.grpTranDescription.Text = "Description";
            // 
            // txtReference
            // 
            this.txtReference.Location = new System.Drawing.Point(74, 158);
            this.txtReference.Name = "txtReference";
            this.txtReference.Size = new System.Drawing.Size(178, 20);
            this.txtReference.TabIndex = 140;
            // 
            // txtTranDescription
            // 
            this.txtTranDescription.Location = new System.Drawing.Point(8, 19);
            this.txtTranDescription.Multiline = true;
            this.txtTranDescription.Name = "txtTranDescription";
            this.txtTranDescription.Size = new System.Drawing.Size(251, 128);
            this.txtTranDescription.TabIndex = 120;
            this.txtTranDescription.Enter += new System.EventHandler(this.txtTranDescription_Enter);
            // 
            // lblReference
            // 
            this.lblReference.AutoSize = true;
            this.lblReference.Location = new System.Drawing.Point(8, 164);
            this.lblReference.Name = "lblReference";
            this.lblReference.Size = new System.Drawing.Size(60, 13);
            this.lblReference.TabIndex = 130;
            this.lblReference.Text = "Reference:";
            // 
            // grpAdjustments
            // 
            this.grpAdjustments.Controls.Add(this.chbCasinoAdjustment);
            this.grpAdjustments.Controls.Add(this.dtmChangeTranDailyFigure);
            this.grpAdjustments.Controls.Add(this.txtDailyFigureDate);
            this.grpAdjustments.Controls.Add(this.lblFigureDate);
            this.grpAdjustments.Controls.Add(this.chbAdjustCustomersFigure);
            this.grpAdjustments.Location = new System.Drawing.Point(4, 372);
            this.grpAdjustments.Name = "grpAdjustments";
            this.grpAdjustments.Size = new System.Drawing.Size(258, 100);
            this.grpAdjustments.TabIndex = 150;
            this.grpAdjustments.TabStop = false;
            // 
            // chbCasinoAdjustment
            // 
            this.chbCasinoAdjustment.AutoSize = true;
            this.chbCasinoAdjustment.Location = new System.Drawing.Point(8, 76);
            this.chbCasinoAdjustment.Name = "chbCasinoAdjustment";
            this.chbCasinoAdjustment.Size = new System.Drawing.Size(113, 17);
            this.chbCasinoAdjustment.TabIndex = 200;
            this.chbCasinoAdjustment.Text = "Casino Adjustment";
            this.chbCasinoAdjustment.UseVisualStyleBackColor = true;
            this.chbCasinoAdjustment.CheckedChanged += new System.EventHandler(this.chbCasinoAdjustment_CheckedChanged);
            // 
            // dtmChangeTranDailyFigure
            // 
            this.dtmChangeTranDailyFigure.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtmChangeTranDailyFigure.Location = new System.Drawing.Point(183, 46);
            this.dtmChangeTranDailyFigure.Name = "dtmChangeTranDailyFigure";
            this.dtmChangeTranDailyFigure.Size = new System.Drawing.Size(16, 20);
            this.dtmChangeTranDailyFigure.TabIndex = 190;
            this.dtmChangeTranDailyFigure.ValueChanged += new System.EventHandler(this.dtmChangeTranDailyFigure_ValueChanged);
            // 
            // txtDailyFigureDate
            // 
            this.txtDailyFigureDate.Location = new System.Drawing.Point(80, 46);
            this.txtDailyFigureDate.Name = "txtDailyFigureDate";
            this.txtDailyFigureDate.ReadOnly = true;
            this.txtDailyFigureDate.Size = new System.Drawing.Size(87, 20);
            this.txtDailyFigureDate.TabIndex = 180;
            // 
            // lblFigureDate
            // 
            this.lblFigureDate.AutoSize = true;
            this.lblFigureDate.Location = new System.Drawing.Point(10, 49);
            this.lblFigureDate.Name = "lblFigureDate";
            this.lblFigureDate.Size = new System.Drawing.Size(65, 13);
            this.lblFigureDate.TabIndex = 170;
            this.lblFigureDate.Text = "Figure Date:";
            // 
            // chbAdjustCustomersFigure
            // 
            this.chbAdjustCustomersFigure.AutoSize = true;
            this.chbAdjustCustomersFigure.Location = new System.Drawing.Point(9, 20);
            this.chbAdjustCustomersFigure.Name = "chbAdjustCustomersFigure";
            this.chbAdjustCustomersFigure.Size = new System.Drawing.Size(141, 17);
            this.chbAdjustCustomersFigure.TabIndex = 160;
            this.chbAdjustCustomersFigure.Text = "Adjust Customer\'s Figure";
            this.chbAdjustCustomersFigure.UseVisualStyleBackColor = true;
            this.chbAdjustCustomersFigure.CheckedChanged += new System.EventHandler(this.chbAdjustCustomersFigure_CheckedChanged);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(458, 441);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 550;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.tbnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(552, 441);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 560;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblArchivedTransaction
            // 
            this.lblArchivedTransaction.AutoSize = true;
            this.lblArchivedTransaction.Location = new System.Drawing.Point(288, 446);
            this.lblArchivedTransaction.Name = "lblArchivedTransaction";
            this.lblArchivedTransaction.Size = new System.Drawing.Size(252, 13);
            this.lblArchivedTransaction.TabIndex = 540;
            this.lblArchivedTransaction.Text = "ARCHIVED TRANSACTION- no changes accepted";
            this.lblArchivedTransaction.Visible = false;
            // 
            // frmCustomerTransactionDetails
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(662, 487);
            this.Controls.Add(this.lblArchivedTransaction);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.grpAdjustments);
            this.Controls.Add(this.grpTranDescription);
            this.Controls.Add(this.panDocNumber);
            this.Controls.Add(this.grpCreditCard);
            this.Controls.Add(this.grpUncollectedFunds);
            this.Controls.Add(this.grpRefunds);
            this.Controls.Add(this.panTranTypeSection);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCustomerTransactionDetails";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.frmCustomerTransactionDetails_Load);
            this.panTranTypeSection.ResumeLayout(false);
            this.panTranTypeSection.PerformLayout();
            this.grpRefunds.ResumeLayout(false);
            this.grpRefunds.PerformLayout();
            this.grpUncollectedFunds.ResumeLayout(false);
            this.grpUncollectedFunds.PerformLayout();
            this.grpCreditCard.ResumeLayout(false);
            this.grpCreditCard.PerformLayout();
            this.panDocNumber.ResumeLayout(false);
            this.panDocNumber.PerformLayout();
            this.grpTranDescription.ResumeLayout(false);
            this.grpTranDescription.PerformLayout();
            this.grpAdjustments.ResumeLayout(false);
            this.grpAdjustments.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panTranTypeSection;
        private System.Windows.Forms.Label lblMethod;
        private System.Windows.Forms.Label lblAmount;
        private System.Windows.Forms.Label lblTransactionType;
        private System.Windows.Forms.Label lblDateTime;
        private System.Windows.Forms.GroupBox grpRefunds;
        private System.Windows.Forms.TextBox txtPromo;
        private System.Windows.Forms.TextBox txtFees;
        private System.Windows.Forms.TextBox txtFreePlay;
        private System.Windows.Forms.Label lblPromo;
        private System.Windows.Forms.Label lblFees;
        private System.Windows.Forms.Label lblFreePlay;
        private System.Windows.Forms.GroupBox grpUncollectedFunds;
        private System.Windows.Forms.GroupBox grpCreditCard;
        private System.Windows.Forms.Label lblDays;
        private System.Windows.Forms.TextBox txtNumberOfDays;
        private System.Windows.Forms.Label lblPercOfTranAmount;
        private System.Windows.Forms.TextBox txtHold;
        private System.Windows.Forms.CheckBox chbHold;
        private System.Windows.Forms.Button btnCoverDeposit;
        private System.Windows.Forms.TextBox txtCCExpDate;
        private System.Windows.Forms.Label lblCCExpDate;
        private System.Windows.Forms.TextBox txtApprovalNo;
        private System.Windows.Forms.TextBox txtCardNo;
        private System.Windows.Forms.Label lblApprovalNo;
        private System.Windows.Forms.Label lblCardNo;
        private System.Windows.Forms.Panel panDocNumber;
        private System.Windows.Forms.TextBox txtEnteredBy;
        private System.Windows.Forms.Label lblEnteredBy;
        private System.Windows.Forms.TextBox txtGradeNumber;
        private System.Windows.Forms.Label lblGradeNumber;
        private System.Windows.Forms.TextBox txtDocumentNumber;
        private System.Windows.Forms.Label lblDocumentNumber;
        private System.Windows.Forms.GroupBox grpTranDescription;
        private System.Windows.Forms.TextBox txtReference;
        private System.Windows.Forms.TextBox txtTranDescription;
        private System.Windows.Forms.Label lblReference;
        private System.Windows.Forms.GroupBox grpAdjustments;
        private System.Windows.Forms.TextBox txtDailyFigureDate;
        private System.Windows.Forms.Label lblFigureDate;
        private System.Windows.Forms.CheckBox chbAdjustCustomersFigure;
        private System.Windows.Forms.DateTimePicker dtmChangeTranDailyFigure;
        private System.Windows.Forms.CheckBox chbCasinoAdjustment;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox txtEnteredAmount;
        private System.Windows.Forms.TextBox txtTranDateTime;
        private System.Windows.Forms.Panel panTranType;
        private System.Windows.Forms.Panel panTranMethod;
        private System.Windows.Forms.Button btnPayto;
        private System.Windows.Forms.Label lblAmountToRelease;
        private System.Windows.Forms.Label lblHoldAmount;
        private System.Windows.Forms.Label lblExcessAmt;
        private System.Windows.Forms.Label lblExcess;
        private System.Windows.Forms.Label lblCoveredAmt;
        private System.Windows.Forms.Label lblCovered;
        private System.Windows.Forms.Label lblArchivedTransaction;
        private System.Windows.Forms.Label lblTranAmount;
    }
}