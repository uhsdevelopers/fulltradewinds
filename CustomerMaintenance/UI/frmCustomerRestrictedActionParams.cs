﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using SIDLibraries.Utilities;
using GUILibraries.Forms;

namespace CustomerMaintenance.UI {
  public partial class FrmCustomerRestrictedActionParams : SIDForm {

    private readonly spCstGetActions_Result _action;
    private List<spCstGetRestrictedActionParams_Result> _alreadyRestrictedActionsParams;
    private List<spCstGetRestrictedActionParams_Result> _prms;
    private string _startingValue = "";

    public bool Saved { get; set; }

    public FrmCustomerRestrictedActionParams(spCstGetActions_Result action, ModuleInfo moduleInfo, ref List<spCstGetRestrictedActionParams_Result> alreadyRestrictedActionsParams)
      : base(moduleInfo) {
      _action = action;
      _alreadyRestrictedActionsParams = alreadyRestrictedActionsParams;
      Saved = false;
      InitializeComponent();
    }

    private void frmCustomerRestrictedActionParams_Load(object sender, EventArgs e) {
      using (var cst = new Customers(AppModuleInfo)) {
        if (_alreadyRestrictedActionsParams != null && _alreadyRestrictedActionsParams.Count > 0 && (from t in _alreadyRestrictedActionsParams where t.RestrictedActionId == _action.RestrictedActionId select t).Any()) {
          _prms = (from t in _alreadyRestrictedActionsParams where t.RestrictedActionId == _action.RestrictedActionId select t).ToList();
        }
        else {
          _prms = cst.GetRestrictedActionParams(_action.ActionId, _action.CustomerId);
        }
        dgvParams.AutoGenerateColumns = false;
        if (_prms.Count == 0 && _action.SystemRestricted) {
          List<spCstGetActionParams_Result> aprms = cst.GetActionParams(_action.ActionId);
          dgvParams.Columns[2].DataPropertyName = "DefaultValue";
          dgvParams.Columns[3].DataPropertyName = "ActionId";
          dgvParams.Columns[4].DataPropertyName = "Name";
          dgvParams.DataSource = aprms;
          _startingValue = aprms[0].DefaultValue;
        }
        else {
          dgvParams.Columns[3].DataPropertyName = "ActionID";
          dgvParams.DataSource = _prms;
          _startingValue = _prms[0].Value;
        }
      }
      btnSave.Enabled = false;
    }

    private void btnSave_Click(object sender, EventArgs e) {
      using (var lw = new LogWriter(AppModuleInfo)) {
        using (var cst = new Customers(AppModuleInfo)) {
          List<spCstGetRestrictedActionParams_Result> prms;
          if (_action.SystemRestricted) {
            cst.RestrictAction(_action.CustomerId, _action.ActionId, Environment.UserName);
            prms = cst.GetRestrictedActionParams(_action.ActionId, _action.CustomerId);
          }
          else prms = null;

          foreach (DataGridViewRow row in dgvParams.Rows) {
            int restrictedActionParamId = 0;
            if (row.Cells["RestrictedActionParamId"].Value != null)
              restrictedActionParamId = int.Parse(row.Cells["RestrictedActionParamId"].Value.ToString());
            else if (prms != null) {
              restrictedActionParamId =
                (from p in prms where p.Name == (string)row.Cells["ActionParamName"].Value select p.RestrictedActionParamId)
                  .FirstOrDefault();
            }
            cst.UpdateRestrictedActionParam(restrictedActionParamId, row.Cells["ParamValue"].Value.ToString());

            if (_alreadyRestrictedActionsParams == null)
              _alreadyRestrictedActionsParams = new List<spCstGetRestrictedActionParams_Result>();

            RemoveActionParams();

            var logInfo = new LogWriter.CuAccessLogInfo {
              ProgramName = AppModuleInfo.Description,
              Operation = "-Restriction- Param Changed [" + row.Cells["ParamName"].Value + ": " + row.Cells["ParamValue"].Value + "]",
              Data = _action.CustomerId
            };
            lw.WriteToCuAccessLog(logInfo);
          }
          using (var cst1 = new Customers(AppModuleInfo)) {
            _alreadyRestrictedActionsParams.AddRange(cst1.GetRestrictedActionParams(_action.ActionId, _action.CustomerId));
          }
        }
      }
      Saved = true;
      Close();
    }

    private void RemoveActionParams() {
      if (_alreadyRestrictedActionsParams == null || !_alreadyRestrictedActionsParams.Any()) return;
      var itemsToRemove = _alreadyRestrictedActionsParams.Where(item => item.ActionID == _action.ActionId).ToList();

      foreach (var i in itemsToRemove) {
        _alreadyRestrictedActionsParams.Remove(i);
      }
    }

    private void ValidateNewCellValue(object sender, DataGridViewCellValidatingEventArgs e) {
      var actionId = -1;
      if (dgvParams.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
        actionId = int.Parse(dgvParams.Rows[e.RowIndex].Cells[3].Value.ToString());

      //if (_prms != null && _prms.Count > 0) {
      bool validateMinValue = (from a in _prms where a.ActionID == actionId select a.ValidateMinValue).FirstOrDefault();

      if (validateMinValue) {
        var minValue = (from a in _prms where a.ActionID == actionId select a.DefaultValue).FirstOrDefault();
        var targetValue = e.FormattedValue.ToString();
        var oldValue = ((DataGridView)sender)[e.ColumnIndex, e.RowIndex].Value.ToString();

        int minValueInt;
        int targetValueInt;
        int.TryParse(minValue, out minValueInt);
        int.TryParse(targetValue, out targetValueInt);

        if (minValueInt == -1 || targetValueInt == -1 || targetValueInt < minValueInt) {
          MessageBox.Show(@"Error updating cell. Min value should be " + minValueInt, @"Error");
          btnSave.Enabled = false;
          ((DataGridView)sender).CurrentCell = ((DataGridView)sender).Rows[e.RowIndex].Cells[e.ColumnIndex];
          ((DataGridView)sender).CurrentCell.Value = oldValue;
          //e.Cancel = true;
          ((DataGridView)sender).BeginEdit(true);
        }
        else {
          btnSave.Enabled = true;
        }
      }
      else {
        btnSave.Enabled = true;
      }
      //}
    }

    private void dgvParams_CellValidating(object sender, DataGridViewCellValidatingEventArgs e) {
      if (e.RowIndex > -1 && e.ColumnIndex == 2)
        ValidateNewCellValue(sender, e);
    }

    private void btnCancel_Click(object sender, EventArgs e) {
      if (dgvParams.Rows.Count > 0)
        dgvParams.Rows[0].Cells[2].Value = _startingValue;
    }

  }
}