﻿using GUILibraries.Controls;

namespace CustomerMaintenance.UI
{
    partial class FrmCustomerHorseLimitsAndTrackPermissions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpGrantTrackPermissions = new System.Windows.Forms.GroupBox();
            this.btnUnCheckAll = new System.Windows.Forms.Button();
            this.btnCheckAll = new System.Windows.Forms.Button();
            this.dgvwTrackPermissions = new System.Windows.Forms.DataGridView();
            this.panImportant = new System.Windows.Forms.Panel();
            this.lblImportant = new System.Windows.Forms.Label();
            this.grpHorseWagerLimits = new System.Windows.Forms.GroupBox();
            this.txtMinPick3To8 = new GUILibraries.Controls.NumberTextBox();
            this.lblMin = new System.Windows.Forms.Label();
            this.txtMaxPick3To8 = new GUILibraries.Controls.NumberTextBox();
            this.lblMax = new System.Windows.Forms.Label();
            this.txtMinWin = new GUILibraries.Controls.NumberTextBox();
            this.lblPick3To8 = new System.Windows.Forms.Label();
            this.txtMaxPlace = new GUILibraries.Controls.NumberTextBox();
            this.lblDailyDbl = new System.Windows.Forms.Label();
            this.txtMinPlace = new GUILibraries.Controls.NumberTextBox();
            this.lblQuinella = new System.Windows.Forms.Label();
            this.txtMinShow = new GUILibraries.Controls.NumberTextBox();
            this.lblTrifecta = new System.Windows.Forms.Label();
            this.txtMaxShow = new GUILibraries.Controls.NumberTextBox();
            this.lblExacta = new System.Windows.Forms.Label();
            this.txtMinExacta = new GUILibraries.Controls.NumberTextBox();
            this.lblShow = new System.Windows.Forms.Label();
            this.txtMaxExacta = new GUILibraries.Controls.NumberTextBox();
            this.lblPlace = new System.Windows.Forms.Label();
            this.txtMinTrifecta = new GUILibraries.Controls.NumberTextBox();
            this.lblWin = new System.Windows.Forms.Label();
            this.txtMaxTrifecta = new GUILibraries.Controls.NumberTextBox();
            this.txtMaxWin = new GUILibraries.Controls.NumberTextBox();
            this.txtMinQuinella = new GUILibraries.Controls.NumberTextBox();
            this.txtMaxDailyDbl = new GUILibraries.Controls.NumberTextBox();
            this.txtMaxQuinella = new GUILibraries.Controls.NumberTextBox();
            this.txtMinDailyDbl = new GUILibraries.Controls.NumberTextBox();
            this.lblMaxPayout = new System.Windows.Forms.Label();
            this.lblAllWagerTypes = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnApply = new System.Windows.Forms.Button();
            this.txtMaxPayout = new GUILibraries.Controls.NumberTextBox();
            this.grpGrantTrackPermissions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvwTrackPermissions)).BeginInit();
            this.panImportant.SuspendLayout();
            this.grpHorseWagerLimits.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpGrantTrackPermissions
            // 
            this.grpGrantTrackPermissions.Controls.Add(this.btnUnCheckAll);
            this.grpGrantTrackPermissions.Controls.Add(this.btnCheckAll);
            this.grpGrantTrackPermissions.Controls.Add(this.dgvwTrackPermissions);
            this.grpGrantTrackPermissions.Location = new System.Drawing.Point(13, 13);
            this.grpGrantTrackPermissions.Name = "grpGrantTrackPermissions";
            this.grpGrantTrackPermissions.Size = new System.Drawing.Size(540, 255);
            this.grpGrantTrackPermissions.TabIndex = 10;
            this.grpGrantTrackPermissions.TabStop = false;
            this.grpGrantTrackPermissions.Text = "Grant Track Permissions";
            // 
            // btnUnCheckAll
            // 
            this.btnUnCheckAll.Location = new System.Drawing.Point(451, 59);
            this.btnUnCheckAll.Name = "btnUnCheckAll";
            this.btnUnCheckAll.Size = new System.Drawing.Size(75, 23);
            this.btnUnCheckAll.TabIndex = 40;
            this.btnUnCheckAll.Text = "UnCheck All";
            this.btnUnCheckAll.UseVisualStyleBackColor = true;
            this.btnUnCheckAll.Click += new System.EventHandler(this.btnUnCheckAll_Click);
            // 
            // btnCheckAll
            // 
            this.btnCheckAll.Location = new System.Drawing.Point(451, 20);
            this.btnCheckAll.Name = "btnCheckAll";
            this.btnCheckAll.Size = new System.Drawing.Size(75, 23);
            this.btnCheckAll.TabIndex = 30;
            this.btnCheckAll.Text = "Check All";
            this.btnCheckAll.UseVisualStyleBackColor = true;
            this.btnCheckAll.Click += new System.EventHandler(this.btnCheckAll_Click);
            // 
            // dgvwTrackPermissions
            // 
            this.dgvwTrackPermissions.AllowUserToAddRows = false;
            this.dgvwTrackPermissions.AllowUserToDeleteRows = false;
            this.dgvwTrackPermissions.AllowUserToResizeRows = false;
            this.dgvwTrackPermissions.BackgroundColor = System.Drawing.Color.White;
            this.dgvwTrackPermissions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvwTrackPermissions.Location = new System.Drawing.Point(7, 20);
            this.dgvwTrackPermissions.Name = "dgvwTrackPermissions";
            this.dgvwTrackPermissions.RowHeadersVisible = false;
            this.dgvwTrackPermissions.Size = new System.Drawing.Size(272, 229);
            this.dgvwTrackPermissions.TabIndex = 20;
            // 
            // panImportant
            // 
            this.panImportant.Controls.Add(this.lblImportant);
            this.panImportant.Location = new System.Drawing.Point(172, 274);
            this.panImportant.Name = "panImportant";
            this.panImportant.Size = new System.Drawing.Size(224, 26);
            this.panImportant.TabIndex = 5;
            // 
            // lblImportant
            // 
            this.lblImportant.AutoSize = true;
            this.lblImportant.Location = new System.Drawing.Point(5, 6);
            this.lblImportant.Name = "lblImportant";
            this.lblImportant.Size = new System.Drawing.Size(215, 13);
            this.lblImportant.TabIndex = 50;
            this.lblImportant.Text = "IMPORTANT: If \"0\" then Store Limit Applies";
            // 
            // grpHorseWagerLimits
            // 
            this.grpHorseWagerLimits.Controls.Add(this.txtMinPick3To8);
            this.grpHorseWagerLimits.Controls.Add(this.lblMin);
            this.grpHorseWagerLimits.Controls.Add(this.txtMaxPick3To8);
            this.grpHorseWagerLimits.Controls.Add(this.lblMax);
            this.grpHorseWagerLimits.Controls.Add(this.txtMinWin);
            this.grpHorseWagerLimits.Controls.Add(this.lblPick3To8);
            this.grpHorseWagerLimits.Controls.Add(this.txtMaxPlace);
            this.grpHorseWagerLimits.Controls.Add(this.lblDailyDbl);
            this.grpHorseWagerLimits.Controls.Add(this.txtMinPlace);
            this.grpHorseWagerLimits.Controls.Add(this.lblQuinella);
            this.grpHorseWagerLimits.Controls.Add(this.txtMinShow);
            this.grpHorseWagerLimits.Controls.Add(this.lblTrifecta);
            this.grpHorseWagerLimits.Controls.Add(this.txtMaxShow);
            this.grpHorseWagerLimits.Controls.Add(this.lblExacta);
            this.grpHorseWagerLimits.Controls.Add(this.txtMinExacta);
            this.grpHorseWagerLimits.Controls.Add(this.lblShow);
            this.grpHorseWagerLimits.Controls.Add(this.txtMaxExacta);
            this.grpHorseWagerLimits.Controls.Add(this.lblPlace);
            this.grpHorseWagerLimits.Controls.Add(this.txtMinTrifecta);
            this.grpHorseWagerLimits.Controls.Add(this.lblWin);
            this.grpHorseWagerLimits.Controls.Add(this.txtMaxTrifecta);
            this.grpHorseWagerLimits.Controls.Add(this.txtMaxWin);
            this.grpHorseWagerLimits.Controls.Add(this.txtMinQuinella);
            this.grpHorseWagerLimits.Controls.Add(this.txtMaxDailyDbl);
            this.grpHorseWagerLimits.Controls.Add(this.txtMaxQuinella);
            this.grpHorseWagerLimits.Controls.Add(this.txtMinDailyDbl);
            this.grpHorseWagerLimits.Location = new System.Drawing.Point(13, 312);
            this.grpHorseWagerLimits.Name = "grpHorseWagerLimits";
            this.grpHorseWagerLimits.Size = new System.Drawing.Size(540, 92);
            this.grpHorseWagerLimits.TabIndex = 60;
            this.grpHorseWagerLimits.TabStop = false;
            this.grpHorseWagerLimits.Text = "Horse Wager Limits";
            // 
            // txtMinPick3To8
            // 
            this.txtMinPick3To8.DividedBy = 100;
            this.txtMinPick3To8.DivideFlag = true;
            this.txtMinPick3To8.Location = new System.Drawing.Point(480, 62);
            this.txtMinPick3To8.Name = "txtMinPick3To8";
            this.txtMinPick3To8.ShowIfZero = true;
            this.txtMinPick3To8.Size = new System.Drawing.Size(51, 20);
            this.txtMinPick3To8.TabIndex = 250;
            // 
            // lblMin
            // 
            this.lblMin.AutoSize = true;
            this.lblMin.Location = new System.Drawing.Point(9, 64);
            this.lblMin.Name = "lblMin";
            this.lblMin.Size = new System.Drawing.Size(24, 13);
            this.lblMin.TabIndex = 160;
            this.lblMin.Text = "Min";
            this.lblMin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMaxPick3To8
            // 
            this.txtMaxPick3To8.DividedBy = 100;
            this.txtMaxPick3To8.DivideFlag = true;
            this.txtMaxPick3To8.Location = new System.Drawing.Point(480, 36);
            this.txtMaxPick3To8.Name = "txtMaxPick3To8";
            this.txtMaxPick3To8.ShowIfZero = true;
            this.txtMaxPick3To8.Size = new System.Drawing.Size(51, 20);
            this.txtMaxPick3To8.TabIndex = 150;
            // 
            // lblMax
            // 
            this.lblMax.AutoSize = true;
            this.lblMax.Location = new System.Drawing.Point(6, 41);
            this.lblMax.Name = "lblMax";
            this.lblMax.Size = new System.Drawing.Size(27, 13);
            this.lblMax.TabIndex = 70;
            this.lblMax.Text = "Max";
            this.lblMax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMinWin
            // 
            this.txtMinWin.DividedBy = 100;
            this.txtMinWin.DivideFlag = true;
            this.txtMinWin.Location = new System.Drawing.Point(39, 62);
            this.txtMinWin.Name = "txtMinWin";
            this.txtMinWin.ShowIfZero = true;
            this.txtMinWin.Size = new System.Drawing.Size(51, 20);
            this.txtMinWin.TabIndex = 170;
            // 
            // lblPick3To8
            // 
            this.lblPick3To8.AutoSize = true;
            this.lblPick3To8.Location = new System.Drawing.Point(480, 20);
            this.lblPick3To8.Name = "lblPick3To8";
            this.lblPick3To8.Size = new System.Drawing.Size(46, 13);
            this.lblPick3To8.TabIndex = 7;
            this.lblPick3To8.Text = "Pick 3-8";
            // 
            // txtMaxPlace
            // 
            this.txtMaxPlace.DividedBy = 100;
            this.txtMaxPlace.DivideFlag = true;
            this.txtMaxPlace.Location = new System.Drawing.Point(102, 36);
            this.txtMaxPlace.Name = "txtMaxPlace";
            this.txtMaxPlace.ShowIfZero = true;
            this.txtMaxPlace.Size = new System.Drawing.Size(51, 20);
            this.txtMaxPlace.TabIndex = 90;
            // 
            // lblDailyDbl
            // 
            this.lblDailyDbl.AutoSize = true;
            this.lblDailyDbl.Location = new System.Drawing.Point(414, 20);
            this.lblDailyDbl.Name = "lblDailyDbl";
            this.lblDailyDbl.Size = new System.Drawing.Size(49, 13);
            this.lblDailyDbl.TabIndex = 6;
            this.lblDailyDbl.Text = "Daily Dbl";
            // 
            // txtMinPlace
            // 
            this.txtMinPlace.DividedBy = 100;
            this.txtMinPlace.DivideFlag = true;
            this.txtMinPlace.Location = new System.Drawing.Point(102, 62);
            this.txtMinPlace.Name = "txtMinPlace";
            this.txtMinPlace.ShowIfZero = true;
            this.txtMinPlace.Size = new System.Drawing.Size(51, 20);
            this.txtMinPlace.TabIndex = 180;
            // 
            // lblQuinella
            // 
            this.lblQuinella.AutoSize = true;
            this.lblQuinella.Location = new System.Drawing.Point(355, 20);
            this.lblQuinella.Name = "lblQuinella";
            this.lblQuinella.Size = new System.Drawing.Size(45, 13);
            this.lblQuinella.TabIndex = 5;
            this.lblQuinella.Text = "Quinella";
            // 
            // txtMinShow
            // 
            this.txtMinShow.DividedBy = 100;
            this.txtMinShow.DivideFlag = true;
            this.txtMinShow.Location = new System.Drawing.Point(165, 62);
            this.txtMinShow.Name = "txtMinShow";
            this.txtMinShow.ShowIfZero = true;
            this.txtMinShow.Size = new System.Drawing.Size(51, 20);
            this.txtMinShow.TabIndex = 190;
            // 
            // lblTrifecta
            // 
            this.lblTrifecta.AutoSize = true;
            this.lblTrifecta.Location = new System.Drawing.Point(294, 20);
            this.lblTrifecta.Name = "lblTrifecta";
            this.lblTrifecta.Size = new System.Drawing.Size(43, 13);
            this.lblTrifecta.TabIndex = 4;
            this.lblTrifecta.Text = "Trifecta";
            // 
            // txtMaxShow
            // 
            this.txtMaxShow.DividedBy = 100;
            this.txtMaxShow.DivideFlag = true;
            this.txtMaxShow.Location = new System.Drawing.Point(165, 36);
            this.txtMaxShow.Name = "txtMaxShow";
            this.txtMaxShow.ShowIfZero = true;
            this.txtMaxShow.Size = new System.Drawing.Size(51, 20);
            this.txtMaxShow.TabIndex = 100;
            // 
            // lblExacta
            // 
            this.lblExacta.AutoSize = true;
            this.lblExacta.Location = new System.Drawing.Point(234, 20);
            this.lblExacta.Name = "lblExacta";
            this.lblExacta.Size = new System.Drawing.Size(40, 13);
            this.lblExacta.TabIndex = 3;
            this.lblExacta.Text = "Exacta";
            // 
            // txtMinExacta
            // 
            this.txtMinExacta.DividedBy = 100;
            this.txtMinExacta.DivideFlag = true;
            this.txtMinExacta.Location = new System.Drawing.Point(228, 62);
            this.txtMinExacta.Name = "txtMinExacta";
            this.txtMinExacta.ShowIfZero = true;
            this.txtMinExacta.Size = new System.Drawing.Size(51, 20);
            this.txtMinExacta.TabIndex = 200;
            // 
            // lblShow
            // 
            this.lblShow.AutoSize = true;
            this.lblShow.Location = new System.Drawing.Point(177, 20);
            this.lblShow.Name = "lblShow";
            this.lblShow.Size = new System.Drawing.Size(34, 13);
            this.lblShow.TabIndex = 2;
            this.lblShow.Text = "Show";
            this.lblShow.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMaxExacta
            // 
            this.txtMaxExacta.DividedBy = 100;
            this.txtMaxExacta.DivideFlag = true;
            this.txtMaxExacta.Location = new System.Drawing.Point(228, 36);
            this.txtMaxExacta.Name = "txtMaxExacta";
            this.txtMaxExacta.ShowIfZero = true;
            this.txtMaxExacta.Size = new System.Drawing.Size(51, 20);
            this.txtMaxExacta.TabIndex = 110;
            // 
            // lblPlace
            // 
            this.lblPlace.AutoSize = true;
            this.lblPlace.Location = new System.Drawing.Point(112, 20);
            this.lblPlace.Name = "lblPlace";
            this.lblPlace.Size = new System.Drawing.Size(34, 13);
            this.lblPlace.TabIndex = 1;
            this.lblPlace.Text = "Place";
            // 
            // txtMinTrifecta
            // 
            this.txtMinTrifecta.DividedBy = 100;
            this.txtMinTrifecta.DivideFlag = true;
            this.txtMinTrifecta.Location = new System.Drawing.Point(288, 62);
            this.txtMinTrifecta.Name = "txtMinTrifecta";
            this.txtMinTrifecta.ShowIfZero = true;
            this.txtMinTrifecta.Size = new System.Drawing.Size(51, 20);
            this.txtMinTrifecta.TabIndex = 220;
            // 
            // lblWin
            // 
            this.lblWin.AutoSize = true;
            this.lblWin.Location = new System.Drawing.Point(58, 20);
            this.lblWin.Name = "lblWin";
            this.lblWin.Size = new System.Drawing.Size(26, 13);
            this.lblWin.TabIndex = 0;
            this.lblWin.Text = "Win";
            // 
            // txtMaxTrifecta
            // 
            this.txtMaxTrifecta.DividedBy = 100;
            this.txtMaxTrifecta.DivideFlag = true;
            this.txtMaxTrifecta.Location = new System.Drawing.Point(291, 36);
            this.txtMaxTrifecta.Name = "txtMaxTrifecta";
            this.txtMaxTrifecta.ShowIfZero = true;
            this.txtMaxTrifecta.Size = new System.Drawing.Size(51, 20);
            this.txtMaxTrifecta.TabIndex = 120;
            // 
            // txtMaxWin
            // 
            this.txtMaxWin.DividedBy = 100;
            this.txtMaxWin.DivideFlag = true;
            this.txtMaxWin.Location = new System.Drawing.Point(39, 36);
            this.txtMaxWin.Name = "txtMaxWin";
            this.txtMaxWin.ShowIfZero = true;
            this.txtMaxWin.Size = new System.Drawing.Size(51, 20);
            this.txtMaxWin.TabIndex = 80;
            // 
            // txtMinQuinella
            // 
            this.txtMinQuinella.DividedBy = 100;
            this.txtMinQuinella.DivideFlag = true;
            this.txtMinQuinella.Location = new System.Drawing.Point(354, 62);
            this.txtMinQuinella.Name = "txtMinQuinella";
            this.txtMinQuinella.ShowIfZero = true;
            this.txtMinQuinella.Size = new System.Drawing.Size(51, 20);
            this.txtMinQuinella.TabIndex = 230;
            // 
            // txtMaxDailyDbl
            // 
            this.txtMaxDailyDbl.DividedBy = 100;
            this.txtMaxDailyDbl.DivideFlag = true;
            this.txtMaxDailyDbl.Location = new System.Drawing.Point(417, 36);
            this.txtMaxDailyDbl.Name = "txtMaxDailyDbl";
            this.txtMaxDailyDbl.ShowIfZero = true;
            this.txtMaxDailyDbl.Size = new System.Drawing.Size(51, 20);
            this.txtMaxDailyDbl.TabIndex = 140;
            // 
            // txtMaxQuinella
            // 
            this.txtMaxQuinella.DividedBy = 100;
            this.txtMaxQuinella.DivideFlag = true;
            this.txtMaxQuinella.Location = new System.Drawing.Point(354, 36);
            this.txtMaxQuinella.Name = "txtMaxQuinella";
            this.txtMaxQuinella.ShowIfZero = true;
            this.txtMaxQuinella.Size = new System.Drawing.Size(51, 20);
            this.txtMaxQuinella.TabIndex = 130;
            // 
            // txtMinDailyDbl
            // 
            this.txtMinDailyDbl.DividedBy = 100;
            this.txtMinDailyDbl.DivideFlag = true;
            this.txtMinDailyDbl.Location = new System.Drawing.Point(417, 62);
            this.txtMinDailyDbl.Name = "txtMinDailyDbl";
            this.txtMinDailyDbl.ShowIfZero = true;
            this.txtMinDailyDbl.Size = new System.Drawing.Size(51, 20);
            this.txtMinDailyDbl.TabIndex = 240;
            // 
            // lblMaxPayout
            // 
            this.lblMaxPayout.AutoSize = true;
            this.lblMaxPayout.Location = new System.Drawing.Point(17, 414);
            this.lblMaxPayout.Name = "lblMaxPayout";
            this.lblMaxPayout.Size = new System.Drawing.Size(63, 13);
            this.lblMaxPayout.TabIndex = 260;
            this.lblMaxPayout.Text = "Max Payout";
            // 
            // lblAllWagerTypes
            // 
            this.lblAllWagerTypes.AutoSize = true;
            this.lblAllWagerTypes.Location = new System.Drawing.Point(151, 414);
            this.lblAllWagerTypes.Name = "lblAllWagerTypes";
            this.lblAllWagerTypes.Size = new System.Drawing.Size(83, 13);
            this.lblAllWagerTypes.TabIndex = 280;
            this.lblAllWagerTypes.Text = "(all wager types)";
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(467, 414);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 300;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnApply
            // 
            this.btnApply.Location = new System.Drawing.Point(374, 414);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(75, 23);
            this.btnApply.TabIndex = 290;
            this.btnApply.Text = "Apply";
            this.btnApply.UseVisualStyleBackColor = true;
            // 
            // txtMaxPayout
            // 
            this.txtMaxPayout.DividedBy = 100;
            this.txtMaxPayout.DivideFlag = true;
            this.txtMaxPayout.Location = new System.Drawing.Point(86, 411);
            this.txtMaxPayout.Name = "txtMaxPayout";
            this.txtMaxPayout.ShowIfZero = true;
            this.txtMaxPayout.Size = new System.Drawing.Size(51, 20);
            this.txtMaxPayout.TabIndex = 270;
            // 
            // frmCustomerHorseLimitsAndTrackPermissions
            // 
            this.AcceptButton = this.btnApply;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(568, 536);
            this.Controls.Add(this.txtMaxPayout);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblAllWagerTypes);
            this.Controls.Add(this.lblMaxPayout);
            this.Controls.Add(this.grpHorseWagerLimits);
            this.Controls.Add(this.panImportant);
            this.Controls.Add(this.grpGrantTrackPermissions);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCustomerHorseLimitsAndTrackPermissions";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Horse Limits And Track Permissions";
            this.Load += new System.EventHandler(this.frmCustomerHorseLimitsAndTrackPermissions_Load);
            this.grpGrantTrackPermissions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvwTrackPermissions)).EndInit();
            this.panImportant.ResumeLayout(false);
            this.panImportant.PerformLayout();
            this.grpHorseWagerLimits.ResumeLayout(false);
            this.grpHorseWagerLimits.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpGrantTrackPermissions;
        private System.Windows.Forms.DataGridView dgvwTrackPermissions;
        private System.Windows.Forms.Button btnUnCheckAll;
        private System.Windows.Forms.Button btnCheckAll;
        private System.Windows.Forms.Panel panImportant;
        private System.Windows.Forms.Label lblImportant;
        private System.Windows.Forms.GroupBox grpHorseWagerLimits;
        private System.Windows.Forms.Label lblMin;
        private System.Windows.Forms.Label lblMax;
        private System.Windows.Forms.Label lblPick3To8;
        private System.Windows.Forms.Label lblDailyDbl;
        private System.Windows.Forms.Label lblQuinella;
        private System.Windows.Forms.Label lblTrifecta;
        private System.Windows.Forms.Label lblExacta;
        private System.Windows.Forms.Label lblShow;
        private System.Windows.Forms.Label lblPlace;
        private System.Windows.Forms.Label lblWin;
        private System.Windows.Forms.Label lblMaxPayout;
        private System.Windows.Forms.Label lblAllWagerTypes;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnApply;
        private NumberTextBox txtMinPick3To8;
        private NumberTextBox txtMaxPick3To8;
        private NumberTextBox txtMinWin;
        private NumberTextBox txtMaxPlace;
        private NumberTextBox txtMinPlace;
        private NumberTextBox txtMinShow;
        private NumberTextBox txtMaxShow;
        private NumberTextBox txtMinExacta;
        private NumberTextBox txtMaxExacta;
        private NumberTextBox txtMinTrifecta;
        private NumberTextBox txtMaxTrifecta;
        private NumberTextBox txtMaxWin;
        private NumberTextBox txtMinQuinella;
        private NumberTextBox txtMaxDailyDbl;
        private NumberTextBox txtMaxQuinella;
        private NumberTextBox txtMinDailyDbl;
        private NumberTextBox txtMaxPayout;
    }
}