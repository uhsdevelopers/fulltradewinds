﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using CustomerMaintenance.Extensions;
using CustomerMaintenance.Libraries;
using GUILibraries.BusinessLayer;
using GUILibraries.Controls;
using GUILibraries.Forms;
using GUILibraries.Utilities;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using SIDLibraries.Utilities;
using System.Data.SqlClient;
using System.Data;
using System.Security.Permissions;

namespace CustomerMaintenance.UI
{
    public partial class FrmSqlDependencyTest : SIDForm
    {
        FrmCustomerMaintenance CallerFrm {get; set;}
        public string ConnectionString { get; set; }
        SqlConnection con { get; set; }

        SIDSQLDependency sqlD { get; set; }

        delegate void SetLabelValueCallback();

        double CreditLimit { get; set; }

        public FrmSqlDependencyTest(FrmCustomerMaintenance callerFrm, ModuleInfo AppModuleInfo)
            : base(AppModuleInfo)

        {
            CallerFrm = callerFrm;
            ConnectionString = ConnectionManager.GetDatabaseConnectionString(AppModuleInfo);
            InitializeComponent();
        }

        private bool CanRequestNotifications()
        {
            SqlClientPermission permission =
                new SqlClientPermission(
                PermissionState.Unrestricted);
            try
            {
                permission.Demand();
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        private void FrmSqlDependencyTest_Load(object sender, EventArgs e)
        {
            //con = new SqlConnection(ConnectionString);
            //bool t = CanRequestNotifications();
            //SetupWatcher();
            SetCreditLimitValue();
        }

        private void SetupWatcher()
        {
            if (con == null)
                return;
            SqlDependency.Stop(con.ConnectionString);
            SqlDependency.Start(con.ConnectionString);
        }

        private double LoadCreditLimitDataBasicSQLCmd()
        {

            double creditLimit = 0;
            if (con == null)
                return creditLimit;
            var connection = new SqlConnection(con.ConnectionString);
            connection.Open();

            try
            {
                using (var command = new SqlCommand("spSqlDependencyGetTest", connection))
                {
                    command.Notification = null;
                    SqlParameter prm = new SqlParameter("@CustomerID", SqlDbType.VarChar);
                    prm.Direction = ParameterDirection.Input;
                    prm.DbType = DbType.String;
                    prm.Value = ((FrmCustomerMaintenance)CallerFrm).CustomerId;
                    command.Parameters.Add(prm);

                    command.CommandType = CommandType.StoredProcedure;

                    var dependency = new SqlDependency(command);
                    //dependency.OnChange += new OnChangeEventHandler(OnDependencyChange);

                    using (SqlDataReader rdr = command.ExecuteReader())
                    {
                        try
                        {
                            if (rdr.HasRows)
                            {
                                while (rdr.Read())
                                {
                                    try
                                    {
                                        creditLimit = double.Parse(rdr["CreditLimit"].ToString());
                                    }
                                    catch (Exception ex)
                                    {
                                        Log(ex);
                                    }
                                }
                                rdr.NextResult();
                            }
                        }
                        finally
                        {
                            rdr.Close();
                        }
                    }
                }
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
            }
            return creditLimit;
        }

        private double LoadCreditLimitDataEntity()
        {
            double creditLimit = 0;

            using (var cust = new Customers(AppModuleInfo))
            {
                if (sqlD == null)
                {
                    sqlD = new SIDSQLDependency(AppModuleInfo, new SqlConnection(ConnectionManager.GetDatabaseConnectionString(AppModuleInfo)));
                    sqlD.DependencyChanged += sqlD_DependencyChanged;
                }
                List<SqlParameter> paramsList = new List<SqlParameter>();
                var prm = new SqlParameter("@CustomerID", SqlDbType.VarChar);
                prm.Direction = ParameterDirection.Input;
                prm.DbType = DbType.String;
                prm.Value = ((FrmCustomerMaintenance)CallerFrm).CustomerId;
                paramsList.Add(prm);
                var r = sqlD.ExecuteSQLCommand("spSqlDependencyGetTest", paramsList);
                try
                {
                    if (r.HasRows)
                    {
                        while (r.Read())
                        {
                            try
                            {
                                creditLimit = double.Parse(r["CreditLimit"].ToString());
                            }
                            catch (Exception ex)
                            {
                                Log(ex);
                            }
                        }
                        r.NextResult();
                    }
                }
                finally
                {
                    if (sqlD.Connection != null && sqlD.Connection.State == ConnectionState.Open)
                    {
                        sqlD.Connection.Close();
                    }
                }

            }
            return creditLimit;
        }

        private void sqlD_DependencyChanged(object sender, EventArgs e)
        {
            if (((SqlNotificationEventArgs)e).Type != SqlNotificationType.Subscribe)
            {
                SetCreditLimitValue();
            }
        }

        private void SetCreditLimitValue() {
            if (InvokeRequired)
            {
                var d = new SetLabelValueCallback(SetCreditLimitValue);
                Invoke(d);
            }
            else lblCurrentCreditLimit.Text = DisplayNumber.Format(LoadCreditLimitDataEntity(), true, 100, true, false);
        }

        private void FrmSqlDependencyTest_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (sqlD != null)
                SqlDependency.Stop(sqlD.Connection.ConnectionString);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            UpdateCreditLimit();
        }

        private void UpdateCreditLimit()
        {
            if (sqlD.Connection == null)
                return;
            if (sqlD.Connection.State == ConnectionState.Closed)
                sqlD.Connection.Open();
            try
            {
                using (var command = new SqlCommand("spSqlDependencyUpdateTest", sqlD.Connection))
                {
                    SqlParameter prm = new SqlParameter("@CustomerID", SqlDbType.VarChar);
                    prm.Direction = ParameterDirection.Input;
                    prm.DbType = DbType.String;
                    prm.Value = ((FrmCustomerMaintenance)CallerFrm).CustomerId;
                    command.Parameters.Add(prm);

                    prm = new SqlParameter("@CreditLimit", SqlDbType.Float);
                    prm.Direction = ParameterDirection.Input;
                    prm.DbType = DbType.Double;

                    double creditL = 0;
                    double.TryParse(txtCreditLimit.Text, out creditL);
                    prm.Value = creditL * 100;
                    command.Parameters.Add(prm);
                    command.CommandType = CommandType.StoredProcedure;
                    command.ExecuteNonQuery();
                }
            }
            catch (SqlException ex)
            {
            }
            finally
            {
                if (sqlD.Connection.State == ConnectionState.Open)
                    sqlD.Connection.Close();
            }
        }
    }
}
