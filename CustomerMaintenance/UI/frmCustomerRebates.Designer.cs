﻿namespace CustomerMaintenance.UI
{
    partial class FrmCustomerRebates
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPackageName = new System.Windows.Forms.Label();
            this.txtPackageName = new System.Windows.Forms.TextBox();
            this.grpFrequency = new System.Windows.Forms.GroupBox();
            this.radRunFrequencyDaily = new System.Windows.Forms.RadioButton();
            this.radRunFrequencyWeekly = new System.Windows.Forms.RadioButton();
            this.grpRebatePaymentType = new System.Windows.Forms.GroupBox();
            this.txtMakeupValue = new GUILibraries.Controls.NumberTextBox();
            this.lblLineSeparator = new System.Windows.Forms.Label();
            this.lblMakeup = new System.Windows.Forms.Label();
            this.radPaymentTypeVolume = new System.Windows.Forms.RadioButton();
            this.radPaymentTypeLostVolume = new System.Windows.Forms.RadioButton();
            this.chbOverrideMakeup = new System.Windows.Forms.CheckBox();
            this.radPaymentTypeNetLoss = new System.Windows.Forms.RadioButton();
            this.btnAccept = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.chbCreateRebateTran = new System.Windows.Forms.CheckBox();
            this.dgvRebateWageringSettings = new System.Windows.Forms.DataGridView();
            this.SportType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.SportSubType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.BetType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.WagerType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.RebatePerc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PackageDetailsId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grpFrequency.SuspendLayout();
            this.grpRebatePaymentType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRebateWageringSettings)).BeginInit();
            this.SuspendLayout();
            // 
            // lblPackageName
            // 
            this.lblPackageName.AutoSize = true;
            this.lblPackageName.Location = new System.Drawing.Point(13, 20);
            this.lblPackageName.Name = "lblPackageName";
            this.lblPackageName.Size = new System.Drawing.Size(84, 13);
            this.lblPackageName.TabIndex = 0;
            this.lblPackageName.Text = "Package Name:";
            // 
            // txtPackageName
            // 
            this.txtPackageName.Location = new System.Drawing.Point(104, 13);
            this.txtPackageName.MaxLength = 50;
            this.txtPackageName.Name = "txtPackageName";
            this.txtPackageName.Size = new System.Drawing.Size(112, 20);
            this.txtPackageName.TabIndex = 1;
            // 
            // grpFrequency
            // 
            this.grpFrequency.Controls.Add(this.radRunFrequencyDaily);
            this.grpFrequency.Controls.Add(this.radRunFrequencyWeekly);
            this.grpFrequency.Location = new System.Drawing.Point(16, 68);
            this.grpFrequency.Name = "grpFrequency";
            this.grpFrequency.Size = new System.Drawing.Size(200, 77);
            this.grpFrequency.TabIndex = 2;
            this.grpFrequency.TabStop = false;
            this.grpFrequency.Text = "Run Frequency";
            // 
            // radRunFrequencyDaily
            // 
            this.radRunFrequencyDaily.AccessibleDescription = "Daily";
            this.radRunFrequencyDaily.AutoSize = true;
            this.radRunFrequencyDaily.Location = new System.Drawing.Point(6, 19);
            this.radRunFrequencyDaily.Name = "radRunFrequencyDaily";
            this.radRunFrequencyDaily.Size = new System.Drawing.Size(48, 17);
            this.radRunFrequencyDaily.TabIndex = 1;
            this.radRunFrequencyDaily.TabStop = true;
            this.radRunFrequencyDaily.Text = "Daily";
            this.radRunFrequencyDaily.UseVisualStyleBackColor = true;
            // 
            // radRunFrequencyWeekly
            // 
            this.radRunFrequencyWeekly.AccessibleDescription = "Weekly";
            this.radRunFrequencyWeekly.AutoSize = true;
            this.radRunFrequencyWeekly.Location = new System.Drawing.Point(6, 42);
            this.radRunFrequencyWeekly.Name = "radRunFrequencyWeekly";
            this.radRunFrequencyWeekly.Size = new System.Drawing.Size(61, 17);
            this.radRunFrequencyWeekly.TabIndex = 0;
            this.radRunFrequencyWeekly.TabStop = true;
            this.radRunFrequencyWeekly.Text = "Weekly";
            this.radRunFrequencyWeekly.UseVisualStyleBackColor = true;
            // 
            // grpRebatePaymentType
            // 
            this.grpRebatePaymentType.Controls.Add(this.txtMakeupValue);
            this.grpRebatePaymentType.Controls.Add(this.lblLineSeparator);
            this.grpRebatePaymentType.Controls.Add(this.lblMakeup);
            this.grpRebatePaymentType.Controls.Add(this.radPaymentTypeVolume);
            this.grpRebatePaymentType.Controls.Add(this.radPaymentTypeLostVolume);
            this.grpRebatePaymentType.Controls.Add(this.chbOverrideMakeup);
            this.grpRebatePaymentType.Controls.Add(this.radPaymentTypeNetLoss);
            this.grpRebatePaymentType.Location = new System.Drawing.Point(16, 151);
            this.grpRebatePaymentType.Name = "grpRebatePaymentType";
            this.grpRebatePaymentType.Size = new System.Drawing.Size(200, 125);
            this.grpRebatePaymentType.TabIndex = 3;
            this.grpRebatePaymentType.TabStop = false;
            this.grpRebatePaymentType.Text = "Payment Type";
            // 
            // txtMakeupValue
            // 
            this.txtMakeupValue.DividedBy = 1;
            this.txtMakeupValue.DivideFlag = false;
            this.txtMakeupValue.Location = new System.Drawing.Point(80, 98);
            this.txtMakeupValue.Name = "txtMakeupValue";
            this.txtMakeupValue.ShowIfZero = true;
            this.txtMakeupValue.Size = new System.Drawing.Size(90, 20);
            this.txtMakeupValue.TabIndex = 7;
            // 
            // lblLineSeparator
            // 
            this.lblLineSeparator.AutoSize = true;
            this.lblLineSeparator.Location = new System.Drawing.Point(6, 62);
            this.lblLineSeparator.Name = "lblLineSeparator";
            this.lblLineSeparator.Size = new System.Drawing.Size(193, 13);
            this.lblLineSeparator.TabIndex = 6;
            this.lblLineSeparator.Text = "--------------------------------------------------------------";
            // 
            // lblMakeup
            // 
            this.lblMakeup.AutoSize = true;
            this.lblMakeup.Location = new System.Drawing.Point(6, 101);
            this.lblMakeup.Name = "lblMakeup";
            this.lblMakeup.Size = new System.Drawing.Size(74, 13);
            this.lblMakeup.TabIndex = 5;
            this.lblMakeup.Text = "New Makeup:";
            // 
            // radPaymentTypeVolume
            // 
            this.radPaymentTypeVolume.AccessibleDescription = "Volume";
            this.radPaymentTypeVolume.AutoSize = true;
            this.radPaymentTypeVolume.Location = new System.Drawing.Point(6, 19);
            this.radPaymentTypeVolume.Name = "radPaymentTypeVolume";
            this.radPaymentTypeVolume.Size = new System.Drawing.Size(60, 17);
            this.radPaymentTypeVolume.TabIndex = 1;
            this.radPaymentTypeVolume.TabStop = true;
            this.radPaymentTypeVolume.Text = "Volume";
            this.radPaymentTypeVolume.UseVisualStyleBackColor = true;
            // 
            // radPaymentTypeLostVolume
            // 
            this.radPaymentTypeLostVolume.AccessibleDescription = "Lost Volume";
            this.radPaymentTypeLostVolume.AutoSize = true;
            this.radPaymentTypeLostVolume.Location = new System.Drawing.Point(6, 42);
            this.radPaymentTypeLostVolume.Name = "radPaymentTypeLostVolume";
            this.radPaymentTypeLostVolume.Size = new System.Drawing.Size(83, 17);
            this.radPaymentTypeLostVolume.TabIndex = 0;
            this.radPaymentTypeLostVolume.TabStop = true;
            this.radPaymentTypeLostVolume.Text = "Lost Volume";
            this.radPaymentTypeLostVolume.UseVisualStyleBackColor = true;
            // 
            // chbOverrideMakeup
            // 
            this.chbOverrideMakeup.AutoSize = true;
            this.chbOverrideMakeup.Location = new System.Drawing.Point(80, 77);
            this.chbOverrideMakeup.Name = "chbOverrideMakeup";
            this.chbOverrideMakeup.Size = new System.Drawing.Size(108, 17);
            this.chbOverrideMakeup.TabIndex = 3;
            this.chbOverrideMakeup.Text = "Override Makeup";
            this.chbOverrideMakeup.UseVisualStyleBackColor = true;
            // 
            // radPaymentTypeNetLoss
            // 
            this.radPaymentTypeNetLoss.AccessibleDescription = "Net Loss";
            this.radPaymentTypeNetLoss.AutoSize = true;
            this.radPaymentTypeNetLoss.Location = new System.Drawing.Point(6, 76);
            this.radPaymentTypeNetLoss.Name = "radPaymentTypeNetLoss";
            this.radPaymentTypeNetLoss.Size = new System.Drawing.Size(67, 17);
            this.radPaymentTypeNetLoss.TabIndex = 2;
            this.radPaymentTypeNetLoss.TabStop = true;
            this.radPaymentTypeNetLoss.Text = "Net Loss";
            this.radPaymentTypeNetLoss.UseVisualStyleBackColor = true;
            this.radPaymentTypeNetLoss.CheckedChanged += new System.EventHandler(this.radPaymentTypeNetLoss_CheckedChanged);
            // 
            // btnAccept
            // 
            this.btnAccept.Location = new System.Drawing.Point(251, 217);
            this.btnAccept.Name = "btnAccept";
            this.btnAccept.Size = new System.Drawing.Size(75, 23);
            this.btnAccept.TabIndex = 4;
            this.btnAccept.Text = "Accept";
            this.btnAccept.UseVisualStyleBackColor = true;
            this.btnAccept.Click += new System.EventHandler(this.btnAccept_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(369, 217);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // chbCreateRebateTran
            // 
            this.chbCreateRebateTran.AutoSize = true;
            this.chbCreateRebateTran.Location = new System.Drawing.Point(16, 45);
            this.chbCreateRebateTran.Name = "chbCreateRebateTran";
            this.chbCreateRebateTran.Size = new System.Drawing.Size(154, 17);
            this.chbCreateRebateTran.TabIndex = 7;
            this.chbCreateRebateTran.Text = "Create Rebate Transaction";
            this.chbCreateRebateTran.UseVisualStyleBackColor = true;
            // 
            // dgvRebateWageringSettings
            // 
            this.dgvRebateWageringSettings.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRebateWageringSettings.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SportType,
            this.SportSubType,
            this.BetType,
            this.WagerType,
            this.RebatePerc,
            this.PackageDetailsId});
            this.dgvRebateWageringSettings.Location = new System.Drawing.Point(222, 12);
            this.dgvRebateWageringSettings.MultiSelect = false;
            this.dgvRebateWageringSettings.Name = "dgvRebateWageringSettings";
            this.dgvRebateWageringSettings.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvRebateWageringSettings.Size = new System.Drawing.Size(554, 95);
            this.dgvRebateWageringSettings.TabIndex = 8;
            this.dgvRebateWageringSettings.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvRebateWageringSettings_CellEnter);
            this.dgvRebateWageringSettings.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvRebateWageringSettings_CellValidating);
            this.dgvRebateWageringSettings.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgvRebateWageringSettings_EditingControlShowing);
            this.dgvRebateWageringSettings.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dgvRebateWageringSettings_UserDeletingRow);
            // 
            // SportType
            // 
            this.SportType.HeaderText = "SportType";
            this.SportType.Name = "SportType";
            this.SportType.ReadOnly = true;
            // 
            // SportSubType
            // 
            this.SportSubType.HeaderText = "Sport SubType";
            this.SportSubType.Name = "SportSubType";
            this.SportSubType.ReadOnly = true;
            // 
            // BetType
            // 
            this.BetType.HeaderText = "Bet Type";
            this.BetType.Name = "BetType";
            this.BetType.ReadOnly = true;
            // 
            // WagerType
            // 
            this.WagerType.HeaderText = "Wager Type";
            this.WagerType.Name = "WagerType";
            this.WagerType.ReadOnly = true;
            // 
            // RebatePerc
            // 
            this.RebatePerc.HeaderText = "Rebate Perc";
            this.RebatePerc.Name = "RebatePerc";
            this.RebatePerc.ReadOnly = true;
            // 
            // PackageDetailsId
            // 
            this.PackageDetailsId.HeaderText = "Package Details";
            this.PackageDetailsId.Name = "PackageDetailsId";
            this.PackageDetailsId.Visible = false;
            // 
            // frmCustomerRebates
            // 
            this.AcceptButton = this.btnAccept;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(788, 297);
            this.Controls.Add(this.dgvRebateWageringSettings);
            this.Controls.Add(this.chbCreateRebateTran);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnAccept);
            this.Controls.Add(this.grpRebatePaymentType);
            this.Controls.Add(this.grpFrequency);
            this.Controls.Add(this.txtPackageName);
            this.Controls.Add(this.lblPackageName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCustomerRebates";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Customer Rebates Program";
            this.Load += new System.EventHandler(this.frmCustomerRebates_Load);
            this.grpFrequency.ResumeLayout(false);
            this.grpFrequency.PerformLayout();
            this.grpRebatePaymentType.ResumeLayout(false);
            this.grpRebatePaymentType.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRebateWageringSettings)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPackageName;
        private System.Windows.Forms.TextBox txtPackageName;
        private System.Windows.Forms.GroupBox grpFrequency;
        private System.Windows.Forms.RadioButton radRunFrequencyDaily;
        private System.Windows.Forms.RadioButton radRunFrequencyWeekly;
        private System.Windows.Forms.GroupBox grpRebatePaymentType;
        private System.Windows.Forms.Label lblMakeup;
        private System.Windows.Forms.CheckBox chbOverrideMakeup;
        private System.Windows.Forms.RadioButton radPaymentTypeNetLoss;
        private System.Windows.Forms.RadioButton radPaymentTypeVolume;
        private System.Windows.Forms.RadioButton radPaymentTypeLostVolume;
        private System.Windows.Forms.Label lblLineSeparator;
        private System.Windows.Forms.Button btnAccept;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.CheckBox chbCreateRebateTran;
        private GUILibraries.Controls.NumberTextBox txtMakeupValue;
        private System.Windows.Forms.DataGridView dgvRebateWageringSettings;
        private System.Windows.Forms.DataGridViewComboBoxColumn SportType;
        private System.Windows.Forms.DataGridViewComboBoxColumn SportSubType;
        private System.Windows.Forms.DataGridViewComboBoxColumn BetType;
        private System.Windows.Forms.DataGridViewComboBoxColumn WagerType;
        private System.Windows.Forms.DataGridViewTextBoxColumn RebatePerc;
        private System.Windows.Forms.DataGridViewTextBoxColumn PackageDetailsId;
    }
}