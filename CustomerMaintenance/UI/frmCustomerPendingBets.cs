﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using GUILibraries.Forms;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using SIDLibraries.Utilities;

namespace CustomerMaintenance.UI
{
    public partial class frmCustomerPendingBets : SIDForm
    {
       
        #region Public Properties

        public int A {get; set;}

        #endregion

        #region Private Properties

        private string CustomerId { get; set; }

        private List<spTkWGetCustomerPendingBets_Result> PendingBets { get; set; }

        #endregion

        #region Structures

        #endregion

        #region Constructors

        public frmCustomerPendingBets(String customerId, ModuleInfo moduleInfo)
            : base(moduleInfo)
        {
            if (!string.IsNullOrEmpty(customerId))
            {
                CustomerId = customerId;
                InitializeComponent();
            }
            else
            {
                MessageBox.Show(@"No customer was selected");
                Close();
            }
        }

        #endregion

        #region Public Methods

        #endregion 

        #region Private Methods

        private void AdjustGridColumnHeaders()
        {
            foreach (DataGridViewColumn col in dgvwPendingBets.Columns)
            {
                switch (col.Index)
                {
                    case 0:
                        col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
                        break;
                    case 1:
                        col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                        break;
                    case 2:
                        col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
                        break;
                }
            }
        }

        private void FillCustomerPendingBetsGrid()
        {
            if (PendingBets != null && PendingBets.Count > 0)
            {
                foreach (spTkWGetCustomerPendingBets_Result pw in PendingBets)
                {
                    dgvwPendingBets.Rows.Add((pw.Description ?? "").Trim(), DisplayNumber.Format(pw.AmountWagered ?? 0, false, 1, false), (pw.Count ?? 0).ToString());
                }
            }
            AdjustGridColumnHeaders();
        }

        private void LoadCustomerPendingBets()
        {
            using (var tkw = new TicketsAndWagers(AppModuleInfo))
            {
                PendingBets = tkw.GetCustomerPendingWagers(CustomerId).ToList();
            }
        }

        #endregion

        #region Events

        private void frmCustomerPendingBets_Load(object sender, EventArgs e)
        {
            LoadCustomerPendingBets();
            FillCustomerPendingBetsGrid();
        }

        #endregion
    }
}
