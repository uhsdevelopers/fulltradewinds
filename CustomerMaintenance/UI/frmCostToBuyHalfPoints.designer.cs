﻿namespace CustomerMaintenance.UI
{
    partial class FrmCostToBuyHalfPoints
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.panProgressivePointBuying = new System.Windows.Forms.GroupBox();
      this.panProgressivePointsBuying = new System.Windows.Forms.Panel();
      this.chbUseProgressive = new System.Windows.Forms.CheckBox();
      this.grpBuyPointsNFL = new System.Windows.Forms.GroupBox();
      this.txtNFLFOOTBALLmaxNumberOfHalvesTotalPoints = new System.Windows.Forms.TextBox();
      this.txtNFLFOOTBALLCostToBuyHalfTotalPoints = new System.Windows.Forms.TextBox();
      this.lblTotalPointsNFLFOOTBALL = new System.Windows.Forms.Label();
      this.lblSpreadNFLFOOTBALL = new System.Windows.Forms.Label();
      this.txtNFLFOOTBALLCostToBuyOFF7Spread = new System.Windows.Forms.TextBox();
      this.txtNFLFOOTBALLCostToBuyOFF3Spread = new System.Windows.Forms.TextBox();
      this.lblNFLFOOTBALLCostToBuyOFF7 = new System.Windows.Forms.Label();
      this.lblNFLFOOTBALLCostToBuyOFF3 = new System.Windows.Forms.Label();
      this.txtNFLFOOTBALLCostToBuyON7Spread = new System.Windows.Forms.TextBox();
      this.txtNFLFOOTBALLCostToBuyON3Spread = new System.Windows.Forms.TextBox();
      this.txtNFLFOOTBALLmaxNumberOfHalvesSpread = new System.Windows.Forms.TextBox();
      this.txtNFLFOOTBALLCostToBuyHalfSpread = new System.Windows.Forms.TextBox();
      this.lblNFLFOOTBALLCostToBuyON7 = new System.Windows.Forms.Label();
      this.lblNFLFOOTBALLCostToBuyON3 = new System.Windows.Forms.Label();
      this.lblNFLFOOTBALLmaxNumberOfHalves = new System.Windows.Forms.Label();
      this.lblNFLFOOTBALLCostToBuyHalf = new System.Windows.Forms.Label();
      this.grpBuyPointsCOLLEGEFOOTBALL = new System.Windows.Forms.GroupBox();
      this.txtCollegeFOOTBALLmaxNumberOfHalvesTotalPoints = new System.Windows.Forms.TextBox();
      this.lblSpreadCollegeFOOTBALL = new System.Windows.Forms.Label();
      this.txtCollegeFOOTBALLCostToBuyHalfTotalPoints = new System.Windows.Forms.TextBox();
      this.lblCollegeFOOTBALLCostToBuyHalf = new System.Windows.Forms.Label();
      this.lblTotalPointsCollegeFOOTBALL = new System.Windows.Forms.Label();
      this.lblCollegeFOOTBALLmaxNumberOfHalves = new System.Windows.Forms.Label();
      this.lblCollegeFOOTBALLCostToBuyON3 = new System.Windows.Forms.Label();
      this.txtCollegeFOOTBALLCostToBuyOFF7Spread = new System.Windows.Forms.TextBox();
      this.lblCollegeFOOTBALLCostToBuyON7 = new System.Windows.Forms.Label();
      this.txtCollegeFOOTBALLCostToBuyOFF3Spread = new System.Windows.Forms.TextBox();
      this.txtCollegeFOOTBALLCostToBuyHalfSpread = new System.Windows.Forms.TextBox();
      this.lblCollegeFOOTBALLCostToBuyOFF7 = new System.Windows.Forms.Label();
      this.txtCollegeFOOTBALLmaxNumberOfHalvesSpread = new System.Windows.Forms.TextBox();
      this.lblCollegeFOOTBALLCostToBuyOFF3 = new System.Windows.Forms.Label();
      this.txtCollegeFOOTBALLCostToBuyON3Spread = new System.Windows.Forms.TextBox();
      this.txtCollegeFOOTBALLCostToBuyON7Spread = new System.Windows.Forms.TextBox();
      this.grpBuyPointsBASKETBALL = new System.Windows.Forms.GroupBox();
      this.txtBASKETBALLmaxNumberOfHalvesTotalPoints = new System.Windows.Forms.TextBox();
      this.lblBASKETBALLCostToBuyHalf = new System.Windows.Forms.Label();
      this.lblSpreadBASKETBALL = new System.Windows.Forms.Label();
      this.txtBASKETBALLCostToBuyHalfTotalPoints = new System.Windows.Forms.TextBox();
      this.lblTotalPointsBASKETBALL = new System.Windows.Forms.Label();
      this.txtBASKETBALLmaxNumberOfHalvesSpread = new System.Windows.Forms.TextBox();
      this.lblBASKETBALLmaxNumberOfHalves = new System.Windows.Forms.Label();
      this.txtBASKETBALLCostToBuyHalfSpread = new System.Windows.Forms.TextBox();
      this.btnBuyingPointsOK = new System.Windows.Forms.Button();
      this.btnBuyingPointsCancel = new System.Windows.Forms.Button();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.txtCollegeBASKETBALLmaxNumberOfHalvesTotalPoints = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.txtCollegeBASKETBALLCostToBuyHalfTotalPoints = new System.Windows.Forms.TextBox();
      this.label3 = new System.Windows.Forms.Label();
      this.txtCollegeBASKETBALLmaxNumberOfHalvesSpread = new System.Windows.Forms.TextBox();
      this.label4 = new System.Windows.Forms.Label();
      this.txtCollegeBASKETBALLCostToBuyHalfSpread = new System.Windows.Forms.TextBox();
      this.panProgressivePointBuying.SuspendLayout();
      this.grpBuyPointsNFL.SuspendLayout();
      this.grpBuyPointsCOLLEGEFOOTBALL.SuspendLayout();
      this.grpBuyPointsBASKETBALL.SuspendLayout();
      this.groupBox1.SuspendLayout();
      this.SuspendLayout();
      // 
      // panProgressivePointBuying
      // 
      this.panProgressivePointBuying.Controls.Add(this.panProgressivePointsBuying);
      this.panProgressivePointBuying.Controls.Add(this.chbUseProgressive);
      this.panProgressivePointBuying.Location = new System.Drawing.Point(13, 13);
      this.panProgressivePointBuying.Name = "panProgressivePointBuying";
      this.panProgressivePointBuying.Size = new System.Drawing.Size(408, 60);
      this.panProgressivePointBuying.TabIndex = 10;
      this.panProgressivePointBuying.TabStop = false;
      this.panProgressivePointBuying.Text = "Progressive Buy Points";
      // 
      // panProgressivePointsBuying
      // 
      this.panProgressivePointsBuying.Location = new System.Drawing.Point(172, 17);
      this.panProgressivePointsBuying.Name = "panProgressivePointsBuying";
      this.panProgressivePointsBuying.Size = new System.Drawing.Size(200, 25);
      this.panProgressivePointsBuying.TabIndex = 30;
      // 
      // chbUseProgressive
      // 
      this.chbUseProgressive.AutoSize = true;
      this.chbUseProgressive.Location = new System.Drawing.Point(7, 20);
      this.chbUseProgressive.Name = "chbUseProgressive";
      this.chbUseProgressive.Size = new System.Drawing.Size(165, 17);
      this.chbUseProgressive.TabIndex = 20;
      this.chbUseProgressive.Text = "Use Progressive Point Buying";
      this.chbUseProgressive.UseVisualStyleBackColor = true;
      this.chbUseProgressive.CheckedChanged += new System.EventHandler(this.useProgressiveCheckBox_CheckedChanged);
      // 
      // grpBuyPointsNFL
      // 
      this.grpBuyPointsNFL.Controls.Add(this.txtNFLFOOTBALLmaxNumberOfHalvesTotalPoints);
      this.grpBuyPointsNFL.Controls.Add(this.txtNFLFOOTBALLCostToBuyHalfTotalPoints);
      this.grpBuyPointsNFL.Controls.Add(this.lblTotalPointsNFLFOOTBALL);
      this.grpBuyPointsNFL.Controls.Add(this.lblSpreadNFLFOOTBALL);
      this.grpBuyPointsNFL.Controls.Add(this.txtNFLFOOTBALLCostToBuyOFF7Spread);
      this.grpBuyPointsNFL.Controls.Add(this.txtNFLFOOTBALLCostToBuyOFF3Spread);
      this.grpBuyPointsNFL.Controls.Add(this.lblNFLFOOTBALLCostToBuyOFF7);
      this.grpBuyPointsNFL.Controls.Add(this.lblNFLFOOTBALLCostToBuyOFF3);
      this.grpBuyPointsNFL.Controls.Add(this.txtNFLFOOTBALLCostToBuyON7Spread);
      this.grpBuyPointsNFL.Controls.Add(this.txtNFLFOOTBALLCostToBuyON3Spread);
      this.grpBuyPointsNFL.Controls.Add(this.txtNFLFOOTBALLmaxNumberOfHalvesSpread);
      this.grpBuyPointsNFL.Controls.Add(this.txtNFLFOOTBALLCostToBuyHalfSpread);
      this.grpBuyPointsNFL.Controls.Add(this.lblNFLFOOTBALLCostToBuyON7);
      this.grpBuyPointsNFL.Controls.Add(this.lblNFLFOOTBALLCostToBuyON3);
      this.grpBuyPointsNFL.Controls.Add(this.lblNFLFOOTBALLmaxNumberOfHalves);
      this.grpBuyPointsNFL.Controls.Add(this.lblNFLFOOTBALLCostToBuyHalf);
      this.grpBuyPointsNFL.Location = new System.Drawing.Point(13, 102);
      this.grpBuyPointsNFL.Name = "grpBuyPointsNFL";
      this.grpBuyPointsNFL.Size = new System.Drawing.Size(408, 149);
      this.grpBuyPointsNFL.TabIndex = 40;
      this.grpBuyPointsNFL.TabStop = false;
      this.grpBuyPointsNFL.Text = "Buy Points on NFL FOOTBALL";
      // 
      // txtNFLFOOTBALLmaxNumberOfHalvesTotalPoints
      // 
      this.txtNFLFOOTBALLmaxNumberOfHalvesTotalPoints.Location = new System.Drawing.Point(336, 59);
      this.txtNFLFOOTBALLmaxNumberOfHalvesTotalPoints.Name = "txtNFLFOOTBALLmaxNumberOfHalvesTotalPoints";
      this.txtNFLFOOTBALLmaxNumberOfHalvesTotalPoints.Size = new System.Drawing.Size(34, 20);
      this.txtNFLFOOTBALLmaxNumberOfHalvesTotalPoints.TabIndex = 100;
      // 
      // txtNFLFOOTBALLCostToBuyHalfTotalPoints
      // 
      this.txtNFLFOOTBALLCostToBuyHalfTotalPoints.Location = new System.Drawing.Point(336, 31);
      this.txtNFLFOOTBALLCostToBuyHalfTotalPoints.Name = "txtNFLFOOTBALLCostToBuyHalfTotalPoints";
      this.txtNFLFOOTBALLCostToBuyHalfTotalPoints.Size = new System.Drawing.Size(34, 20);
      this.txtNFLFOOTBALLCostToBuyHalfTotalPoints.TabIndex = 70;
      // 
      // lblTotalPointsNFLFOOTBALL
      // 
      this.lblTotalPointsNFLFOOTBALL.AutoSize = true;
      this.lblTotalPointsNFLFOOTBALL.Location = new System.Drawing.Point(307, 16);
      this.lblTotalPointsNFLFOOTBALL.Name = "lblTotalPointsNFLFOOTBALL";
      this.lblTotalPointsNFLFOOTBALL.Size = new System.Drawing.Size(85, 13);
      this.lblTotalPointsNFLFOOTBALL.TabIndex = 13;
      this.lblTotalPointsNFLFOOTBALL.Text = "TOTAL POINTS";
      // 
      // lblSpreadNFLFOOTBALL
      // 
      this.lblSpreadNFLFOOTBALL.AutoSize = true;
      this.lblSpreadNFLFOOTBALL.Location = new System.Drawing.Point(196, 16);
      this.lblSpreadNFLFOOTBALL.Name = "lblSpreadNFLFOOTBALL";
      this.lblSpreadNFLFOOTBALL.Size = new System.Drawing.Size(51, 13);
      this.lblSpreadNFLFOOTBALL.TabIndex = 12;
      this.lblSpreadNFLFOOTBALL.Text = "SPREAD";
      // 
      // txtNFLFOOTBALLCostToBuyOFF7Spread
      // 
      this.txtNFLFOOTBALLCostToBuyOFF7Spread.Location = new System.Drawing.Point(244, 111);
      this.txtNFLFOOTBALLCostToBuyOFF7Spread.Name = "txtNFLFOOTBALLCostToBuyOFF7Spread";
      this.txtNFLFOOTBALLCostToBuyOFF7Spread.Size = new System.Drawing.Size(34, 20);
      this.txtNFLFOOTBALLCostToBuyOFF7Spread.TabIndex = 180;
      // 
      // txtNFLFOOTBALLCostToBuyOFF3Spread
      // 
      this.txtNFLFOOTBALLCostToBuyOFF3Spread.Location = new System.Drawing.Point(244, 85);
      this.txtNFLFOOTBALLCostToBuyOFF3Spread.Name = "txtNFLFOOTBALLCostToBuyOFF3Spread";
      this.txtNFLFOOTBALLCostToBuyOFF3Spread.Size = new System.Drawing.Size(34, 20);
      this.txtNFLFOOTBALLCostToBuyOFF3Spread.TabIndex = 140;
      // 
      // lblNFLFOOTBALLCostToBuyOFF7
      // 
      this.lblNFLFOOTBALLCostToBuyOFF7.AutoSize = true;
      this.lblNFLFOOTBALLCostToBuyOFF7.Location = new System.Drawing.Point(199, 114);
      this.lblNFLFOOTBALLCostToBuyOFF7.Name = "lblNFLFOOTBALLCostToBuyOFF7";
      this.lblNFLFOOTBALLCostToBuyOFF7.Size = new System.Drawing.Size(39, 13);
      this.lblNFLFOOTBALLCostToBuyOFF7.TabIndex = 170;
      this.lblNFLFOOTBALLCostToBuyOFF7.Text = "OFF 7:";
      // 
      // lblNFLFOOTBALLCostToBuyOFF3
      // 
      this.lblNFLFOOTBALLCostToBuyOFF3.AutoSize = true;
      this.lblNFLFOOTBALLCostToBuyOFF3.Location = new System.Drawing.Point(199, 88);
      this.lblNFLFOOTBALLCostToBuyOFF3.Name = "lblNFLFOOTBALLCostToBuyOFF3";
      this.lblNFLFOOTBALLCostToBuyOFF3.Size = new System.Drawing.Size(39, 13);
      this.lblNFLFOOTBALLCostToBuyOFF3.TabIndex = 130;
      this.lblNFLFOOTBALLCostToBuyOFF3.Text = "OFF 3:";
      // 
      // txtNFLFOOTBALLCostToBuyON7Spread
      // 
      this.txtNFLFOOTBALLCostToBuyON7Spread.Location = new System.Drawing.Point(159, 111);
      this.txtNFLFOOTBALLCostToBuyON7Spread.Name = "txtNFLFOOTBALLCostToBuyON7Spread";
      this.txtNFLFOOTBALLCostToBuyON7Spread.Size = new System.Drawing.Size(34, 20);
      this.txtNFLFOOTBALLCostToBuyON7Spread.TabIndex = 160;
      // 
      // txtNFLFOOTBALLCostToBuyON3Spread
      // 
      this.txtNFLFOOTBALLCostToBuyON3Spread.Location = new System.Drawing.Point(159, 85);
      this.txtNFLFOOTBALLCostToBuyON3Spread.Name = "txtNFLFOOTBALLCostToBuyON3Spread";
      this.txtNFLFOOTBALLCostToBuyON3Spread.Size = new System.Drawing.Size(34, 20);
      this.txtNFLFOOTBALLCostToBuyON3Spread.TabIndex = 120;
      // 
      // txtNFLFOOTBALLmaxNumberOfHalvesSpread
      // 
      this.txtNFLFOOTBALLmaxNumberOfHalvesSpread.Location = new System.Drawing.Point(159, 58);
      this.txtNFLFOOTBALLmaxNumberOfHalvesSpread.Name = "txtNFLFOOTBALLmaxNumberOfHalvesSpread";
      this.txtNFLFOOTBALLmaxNumberOfHalvesSpread.Size = new System.Drawing.Size(34, 20);
      this.txtNFLFOOTBALLmaxNumberOfHalvesSpread.TabIndex = 90;
      // 
      // txtNFLFOOTBALLCostToBuyHalfSpread
      // 
      this.txtNFLFOOTBALLCostToBuyHalfSpread.Location = new System.Drawing.Point(159, 32);
      this.txtNFLFOOTBALLCostToBuyHalfSpread.Name = "txtNFLFOOTBALLCostToBuyHalfSpread";
      this.txtNFLFOOTBALLCostToBuyHalfSpread.Size = new System.Drawing.Size(34, 20);
      this.txtNFLFOOTBALLCostToBuyHalfSpread.TabIndex = 60;
      // 
      // lblNFLFOOTBALLCostToBuyON7
      // 
      this.lblNFLFOOTBALLCostToBuyON7.AutoSize = true;
      this.lblNFLFOOTBALLCostToBuyON7.Location = new System.Drawing.Point(57, 114);
      this.lblNFLFOOTBALLCostToBuyON7.Name = "lblNFLFOOTBALLCostToBuyON7";
      this.lblNFLFOOTBALLCostToBuyON7.Size = new System.Drawing.Size(96, 13);
      this.lblNFLFOOTBALLCostToBuyON7.TabIndex = 150;
      this.lblNFLFOOTBALLCostToBuyON7.Text = "Cost To Buy ON 7:";
      // 
      // lblNFLFOOTBALLCostToBuyON3
      // 
      this.lblNFLFOOTBALLCostToBuyON3.AutoSize = true;
      this.lblNFLFOOTBALLCostToBuyON3.Location = new System.Drawing.Point(57, 88);
      this.lblNFLFOOTBALLCostToBuyON3.Name = "lblNFLFOOTBALLCostToBuyON3";
      this.lblNFLFOOTBALLCostToBuyON3.Size = new System.Drawing.Size(96, 13);
      this.lblNFLFOOTBALLCostToBuyON3.TabIndex = 110;
      this.lblNFLFOOTBALLCostToBuyON3.Text = "Cost To Buy ON 3:";
      // 
      // lblNFLFOOTBALLmaxNumberOfHalves
      // 
      this.lblNFLFOOTBALLmaxNumberOfHalves.AutoSize = true;
      this.lblNFLFOOTBALLmaxNumberOfHalves.Location = new System.Drawing.Point(-1, 62);
      this.lblNFLFOOTBALLmaxNumberOfHalves.Name = "lblNFLFOOTBALLmaxNumberOfHalves";
      this.lblNFLFOOTBALLmaxNumberOfHalves.Size = new System.Drawing.Size(154, 13);
      this.lblNFLFOOTBALLmaxNumberOfHalves.TabIndex = 80;
      this.lblNFLFOOTBALLmaxNumberOfHalves.Text = "Maximum Number Of  ½ Points:";
      // 
      // lblNFLFOOTBALLCostToBuyHalf
      // 
      this.lblNFLFOOTBALLCostToBuyHalf.AutoSize = true;
      this.lblNFLFOOTBALLCostToBuyHalf.Location = new System.Drawing.Point(51, 38);
      this.lblNFLFOOTBALLCostToBuyHalf.Name = "lblNFLFOOTBALLCostToBuyHalf";
      this.lblNFLFOOTBALLCostToBuyHalf.Size = new System.Drawing.Size(102, 13);
      this.lblNFLFOOTBALLCostToBuyHalf.TabIndex = 50;
      this.lblNFLFOOTBALLCostToBuyHalf.Text = "Cost to Buy ½ Point:";
      // 
      // grpBuyPointsCOLLEGEFOOTBALL
      // 
      this.grpBuyPointsCOLLEGEFOOTBALL.Controls.Add(this.txtCollegeFOOTBALLmaxNumberOfHalvesTotalPoints);
      this.grpBuyPointsCOLLEGEFOOTBALL.Controls.Add(this.lblSpreadCollegeFOOTBALL);
      this.grpBuyPointsCOLLEGEFOOTBALL.Controls.Add(this.txtCollegeFOOTBALLCostToBuyHalfTotalPoints);
      this.grpBuyPointsCOLLEGEFOOTBALL.Controls.Add(this.lblCollegeFOOTBALLCostToBuyHalf);
      this.grpBuyPointsCOLLEGEFOOTBALL.Controls.Add(this.lblTotalPointsCollegeFOOTBALL);
      this.grpBuyPointsCOLLEGEFOOTBALL.Controls.Add(this.lblCollegeFOOTBALLmaxNumberOfHalves);
      this.grpBuyPointsCOLLEGEFOOTBALL.Controls.Add(this.lblCollegeFOOTBALLCostToBuyON3);
      this.grpBuyPointsCOLLEGEFOOTBALL.Controls.Add(this.txtCollegeFOOTBALLCostToBuyOFF7Spread);
      this.grpBuyPointsCOLLEGEFOOTBALL.Controls.Add(this.lblCollegeFOOTBALLCostToBuyON7);
      this.grpBuyPointsCOLLEGEFOOTBALL.Controls.Add(this.txtCollegeFOOTBALLCostToBuyOFF3Spread);
      this.grpBuyPointsCOLLEGEFOOTBALL.Controls.Add(this.txtCollegeFOOTBALLCostToBuyHalfSpread);
      this.grpBuyPointsCOLLEGEFOOTBALL.Controls.Add(this.lblCollegeFOOTBALLCostToBuyOFF7);
      this.grpBuyPointsCOLLEGEFOOTBALL.Controls.Add(this.txtCollegeFOOTBALLmaxNumberOfHalvesSpread);
      this.grpBuyPointsCOLLEGEFOOTBALL.Controls.Add(this.lblCollegeFOOTBALLCostToBuyOFF3);
      this.grpBuyPointsCOLLEGEFOOTBALL.Controls.Add(this.txtCollegeFOOTBALLCostToBuyON3Spread);
      this.grpBuyPointsCOLLEGEFOOTBALL.Controls.Add(this.txtCollegeFOOTBALLCostToBuyON7Spread);
      this.grpBuyPointsCOLLEGEFOOTBALL.Location = new System.Drawing.Point(13, 281);
      this.grpBuyPointsCOLLEGEFOOTBALL.Name = "grpBuyPointsCOLLEGEFOOTBALL";
      this.grpBuyPointsCOLLEGEFOOTBALL.Size = new System.Drawing.Size(408, 149);
      this.grpBuyPointsCOLLEGEFOOTBALL.TabIndex = 190;
      this.grpBuyPointsCOLLEGEFOOTBALL.TabStop = false;
      this.grpBuyPointsCOLLEGEFOOTBALL.Text = "Buy Points on College FOOTBALL";
      // 
      // txtCollegeFOOTBALLmaxNumberOfHalvesTotalPoints
      // 
      this.txtCollegeFOOTBALLmaxNumberOfHalvesTotalPoints.Location = new System.Drawing.Point(336, 59);
      this.txtCollegeFOOTBALLmaxNumberOfHalvesTotalPoints.Name = "txtCollegeFOOTBALLmaxNumberOfHalvesTotalPoints";
      this.txtCollegeFOOTBALLmaxNumberOfHalvesTotalPoints.Size = new System.Drawing.Size(34, 20);
      this.txtCollegeFOOTBALLmaxNumberOfHalvesTotalPoints.TabIndex = 250;
      // 
      // lblSpreadCollegeFOOTBALL
      // 
      this.lblSpreadCollegeFOOTBALL.AutoSize = true;
      this.lblSpreadCollegeFOOTBALL.Location = new System.Drawing.Point(196, 16);
      this.lblSpreadCollegeFOOTBALL.Name = "lblSpreadCollegeFOOTBALL";
      this.lblSpreadCollegeFOOTBALL.Size = new System.Drawing.Size(51, 13);
      this.lblSpreadCollegeFOOTBALL.TabIndex = 28;
      this.lblSpreadCollegeFOOTBALL.Text = "SPREAD";
      // 
      // txtCollegeFOOTBALLCostToBuyHalfTotalPoints
      // 
      this.txtCollegeFOOTBALLCostToBuyHalfTotalPoints.Location = new System.Drawing.Point(336, 31);
      this.txtCollegeFOOTBALLCostToBuyHalfTotalPoints.Name = "txtCollegeFOOTBALLCostToBuyHalfTotalPoints";
      this.txtCollegeFOOTBALLCostToBuyHalfTotalPoints.Size = new System.Drawing.Size(34, 20);
      this.txtCollegeFOOTBALLCostToBuyHalfTotalPoints.TabIndex = 220;
      // 
      // lblCollegeFOOTBALLCostToBuyHalf
      // 
      this.lblCollegeFOOTBALLCostToBuyHalf.AutoSize = true;
      this.lblCollegeFOOTBALLCostToBuyHalf.Location = new System.Drawing.Point(51, 38);
      this.lblCollegeFOOTBALLCostToBuyHalf.Name = "lblCollegeFOOTBALLCostToBuyHalf";
      this.lblCollegeFOOTBALLCostToBuyHalf.Size = new System.Drawing.Size(102, 13);
      this.lblCollegeFOOTBALLCostToBuyHalf.TabIndex = 200;
      this.lblCollegeFOOTBALLCostToBuyHalf.Text = "Cost to Buy ½ Point:";
      // 
      // lblTotalPointsCollegeFOOTBALL
      // 
      this.lblTotalPointsCollegeFOOTBALL.AutoSize = true;
      this.lblTotalPointsCollegeFOOTBALL.Location = new System.Drawing.Point(307, 16);
      this.lblTotalPointsCollegeFOOTBALL.Name = "lblTotalPointsCollegeFOOTBALL";
      this.lblTotalPointsCollegeFOOTBALL.Size = new System.Drawing.Size(85, 13);
      this.lblTotalPointsCollegeFOOTBALL.TabIndex = 29;
      this.lblTotalPointsCollegeFOOTBALL.Text = "TOTAL POINTS";
      // 
      // lblCollegeFOOTBALLmaxNumberOfHalves
      // 
      this.lblCollegeFOOTBALLmaxNumberOfHalves.AutoSize = true;
      this.lblCollegeFOOTBALLmaxNumberOfHalves.Location = new System.Drawing.Point(-1, 62);
      this.lblCollegeFOOTBALLmaxNumberOfHalves.Name = "lblCollegeFOOTBALLmaxNumberOfHalves";
      this.lblCollegeFOOTBALLmaxNumberOfHalves.Size = new System.Drawing.Size(154, 13);
      this.lblCollegeFOOTBALLmaxNumberOfHalves.TabIndex = 230;
      this.lblCollegeFOOTBALLmaxNumberOfHalves.Text = "Maximum Number Of  ½ Points:";
      // 
      // lblCollegeFOOTBALLCostToBuyON3
      // 
      this.lblCollegeFOOTBALLCostToBuyON3.AutoSize = true;
      this.lblCollegeFOOTBALLCostToBuyON3.Location = new System.Drawing.Point(57, 88);
      this.lblCollegeFOOTBALLCostToBuyON3.Name = "lblCollegeFOOTBALLCostToBuyON3";
      this.lblCollegeFOOTBALLCostToBuyON3.Size = new System.Drawing.Size(96, 13);
      this.lblCollegeFOOTBALLCostToBuyON3.TabIndex = 260;
      this.lblCollegeFOOTBALLCostToBuyON3.Text = "Cost To Buy ON 3:";
      // 
      // txtCollegeFOOTBALLCostToBuyOFF7Spread
      // 
      this.txtCollegeFOOTBALLCostToBuyOFF7Spread.Location = new System.Drawing.Point(244, 111);
      this.txtCollegeFOOTBALLCostToBuyOFF7Spread.Name = "txtCollegeFOOTBALLCostToBuyOFF7Spread";
      this.txtCollegeFOOTBALLCostToBuyOFF7Spread.Size = new System.Drawing.Size(34, 20);
      this.txtCollegeFOOTBALLCostToBuyOFF7Spread.TabIndex = 330;
      // 
      // lblCollegeFOOTBALLCostToBuyON7
      // 
      this.lblCollegeFOOTBALLCostToBuyON7.AutoSize = true;
      this.lblCollegeFOOTBALLCostToBuyON7.Location = new System.Drawing.Point(57, 114);
      this.lblCollegeFOOTBALLCostToBuyON7.Name = "lblCollegeFOOTBALLCostToBuyON7";
      this.lblCollegeFOOTBALLCostToBuyON7.Size = new System.Drawing.Size(96, 13);
      this.lblCollegeFOOTBALLCostToBuyON7.TabIndex = 300;
      this.lblCollegeFOOTBALLCostToBuyON7.Text = "Cost To Buy ON 7:";
      // 
      // txtCollegeFOOTBALLCostToBuyOFF3Spread
      // 
      this.txtCollegeFOOTBALLCostToBuyOFF3Spread.Location = new System.Drawing.Point(244, 85);
      this.txtCollegeFOOTBALLCostToBuyOFF3Spread.Name = "txtCollegeFOOTBALLCostToBuyOFF3Spread";
      this.txtCollegeFOOTBALLCostToBuyOFF3Spread.Size = new System.Drawing.Size(34, 20);
      this.txtCollegeFOOTBALLCostToBuyOFF3Spread.TabIndex = 290;
      // 
      // txtCollegeFOOTBALLCostToBuyHalfSpread
      // 
      this.txtCollegeFOOTBALLCostToBuyHalfSpread.Location = new System.Drawing.Point(159, 32);
      this.txtCollegeFOOTBALLCostToBuyHalfSpread.Name = "txtCollegeFOOTBALLCostToBuyHalfSpread";
      this.txtCollegeFOOTBALLCostToBuyHalfSpread.Size = new System.Drawing.Size(34, 20);
      this.txtCollegeFOOTBALLCostToBuyHalfSpread.TabIndex = 210;
      // 
      // lblCollegeFOOTBALLCostToBuyOFF7
      // 
      this.lblCollegeFOOTBALLCostToBuyOFF7.AutoSize = true;
      this.lblCollegeFOOTBALLCostToBuyOFF7.Location = new System.Drawing.Point(199, 114);
      this.lblCollegeFOOTBALLCostToBuyOFF7.Name = "lblCollegeFOOTBALLCostToBuyOFF7";
      this.lblCollegeFOOTBALLCostToBuyOFF7.Size = new System.Drawing.Size(39, 13);
      this.lblCollegeFOOTBALLCostToBuyOFF7.TabIndex = 320;
      this.lblCollegeFOOTBALLCostToBuyOFF7.Text = "OFF 7:";
      // 
      // txtCollegeFOOTBALLmaxNumberOfHalvesSpread
      // 
      this.txtCollegeFOOTBALLmaxNumberOfHalvesSpread.Location = new System.Drawing.Point(159, 58);
      this.txtCollegeFOOTBALLmaxNumberOfHalvesSpread.Name = "txtCollegeFOOTBALLmaxNumberOfHalvesSpread";
      this.txtCollegeFOOTBALLmaxNumberOfHalvesSpread.Size = new System.Drawing.Size(34, 20);
      this.txtCollegeFOOTBALLmaxNumberOfHalvesSpread.TabIndex = 240;
      // 
      // lblCollegeFOOTBALLCostToBuyOFF3
      // 
      this.lblCollegeFOOTBALLCostToBuyOFF3.AutoSize = true;
      this.lblCollegeFOOTBALLCostToBuyOFF3.Location = new System.Drawing.Point(199, 88);
      this.lblCollegeFOOTBALLCostToBuyOFF3.Name = "lblCollegeFOOTBALLCostToBuyOFF3";
      this.lblCollegeFOOTBALLCostToBuyOFF3.Size = new System.Drawing.Size(39, 13);
      this.lblCollegeFOOTBALLCostToBuyOFF3.TabIndex = 280;
      this.lblCollegeFOOTBALLCostToBuyOFF3.Text = "OFF 3:";
      // 
      // txtCollegeFOOTBALLCostToBuyON3Spread
      // 
      this.txtCollegeFOOTBALLCostToBuyON3Spread.Location = new System.Drawing.Point(159, 85);
      this.txtCollegeFOOTBALLCostToBuyON3Spread.Name = "txtCollegeFOOTBALLCostToBuyON3Spread";
      this.txtCollegeFOOTBALLCostToBuyON3Spread.Size = new System.Drawing.Size(34, 20);
      this.txtCollegeFOOTBALLCostToBuyON3Spread.TabIndex = 270;
      // 
      // txtCollegeFOOTBALLCostToBuyON7Spread
      // 
      this.txtCollegeFOOTBALLCostToBuyON7Spread.Location = new System.Drawing.Point(159, 111);
      this.txtCollegeFOOTBALLCostToBuyON7Spread.Name = "txtCollegeFOOTBALLCostToBuyON7Spread";
      this.txtCollegeFOOTBALLCostToBuyON7Spread.Size = new System.Drawing.Size(34, 20);
      this.txtCollegeFOOTBALLCostToBuyON7Spread.TabIndex = 310;
      // 
      // grpBuyPointsBASKETBALL
      // 
      this.grpBuyPointsBASKETBALL.Controls.Add(this.txtBASKETBALLmaxNumberOfHalvesTotalPoints);
      this.grpBuyPointsBASKETBALL.Controls.Add(this.lblBASKETBALLCostToBuyHalf);
      this.grpBuyPointsBASKETBALL.Controls.Add(this.lblSpreadBASKETBALL);
      this.grpBuyPointsBASKETBALL.Controls.Add(this.txtBASKETBALLCostToBuyHalfTotalPoints);
      this.grpBuyPointsBASKETBALL.Controls.Add(this.lblTotalPointsBASKETBALL);
      this.grpBuyPointsBASKETBALL.Controls.Add(this.txtBASKETBALLmaxNumberOfHalvesSpread);
      this.grpBuyPointsBASKETBALL.Controls.Add(this.lblBASKETBALLmaxNumberOfHalves);
      this.grpBuyPointsBASKETBALL.Controls.Add(this.txtBASKETBALLCostToBuyHalfSpread);
      this.grpBuyPointsBASKETBALL.Location = new System.Drawing.Point(427, 102);
      this.grpBuyPointsBASKETBALL.Name = "grpBuyPointsBASKETBALL";
      this.grpBuyPointsBASKETBALL.Size = new System.Drawing.Size(408, 149);
      this.grpBuyPointsBASKETBALL.TabIndex = 340;
      this.grpBuyPointsBASKETBALL.TabStop = false;
      this.grpBuyPointsBASKETBALL.Text = "Buy Points on BASKETBALL";
      // 
      // txtBASKETBALLmaxNumberOfHalvesTotalPoints
      // 
      this.txtBASKETBALLmaxNumberOfHalvesTotalPoints.Location = new System.Drawing.Point(336, 61);
      this.txtBASKETBALLmaxNumberOfHalvesTotalPoints.Name = "txtBASKETBALLmaxNumberOfHalvesTotalPoints";
      this.txtBASKETBALLmaxNumberOfHalvesTotalPoints.Size = new System.Drawing.Size(34, 20);
      this.txtBASKETBALLmaxNumberOfHalvesTotalPoints.TabIndex = 400;
      // 
      // lblBASKETBALLCostToBuyHalf
      // 
      this.lblBASKETBALLCostToBuyHalf.AutoSize = true;
      this.lblBASKETBALLCostToBuyHalf.Location = new System.Drawing.Point(51, 40);
      this.lblBASKETBALLCostToBuyHalf.Name = "lblBASKETBALLCostToBuyHalf";
      this.lblBASKETBALLCostToBuyHalf.Size = new System.Drawing.Size(102, 13);
      this.lblBASKETBALLCostToBuyHalf.TabIndex = 350;
      this.lblBASKETBALLCostToBuyHalf.Text = "Cost to Buy ½ Point:";
      // 
      // lblSpreadBASKETBALL
      // 
      this.lblSpreadBASKETBALL.AutoSize = true;
      this.lblSpreadBASKETBALL.Location = new System.Drawing.Point(196, 18);
      this.lblSpreadBASKETBALL.Name = "lblSpreadBASKETBALL";
      this.lblSpreadBASKETBALL.Size = new System.Drawing.Size(51, 13);
      this.lblSpreadBASKETBALL.TabIndex = 44;
      this.lblSpreadBASKETBALL.Text = "SPREAD";
      // 
      // txtBASKETBALLCostToBuyHalfTotalPoints
      // 
      this.txtBASKETBALLCostToBuyHalfTotalPoints.Location = new System.Drawing.Point(336, 33);
      this.txtBASKETBALLCostToBuyHalfTotalPoints.Name = "txtBASKETBALLCostToBuyHalfTotalPoints";
      this.txtBASKETBALLCostToBuyHalfTotalPoints.Size = new System.Drawing.Size(34, 20);
      this.txtBASKETBALLCostToBuyHalfTotalPoints.TabIndex = 370;
      // 
      // lblTotalPointsBASKETBALL
      // 
      this.lblTotalPointsBASKETBALL.AutoSize = true;
      this.lblTotalPointsBASKETBALL.Location = new System.Drawing.Point(307, 18);
      this.lblTotalPointsBASKETBALL.Name = "lblTotalPointsBASKETBALL";
      this.lblTotalPointsBASKETBALL.Size = new System.Drawing.Size(85, 13);
      this.lblTotalPointsBASKETBALL.TabIndex = 45;
      this.lblTotalPointsBASKETBALL.Text = "TOTAL POINTS";
      // 
      // txtBASKETBALLmaxNumberOfHalvesSpread
      // 
      this.txtBASKETBALLmaxNumberOfHalvesSpread.Location = new System.Drawing.Point(159, 60);
      this.txtBASKETBALLmaxNumberOfHalvesSpread.Name = "txtBASKETBALLmaxNumberOfHalvesSpread";
      this.txtBASKETBALLmaxNumberOfHalvesSpread.Size = new System.Drawing.Size(34, 20);
      this.txtBASKETBALLmaxNumberOfHalvesSpread.TabIndex = 390;
      // 
      // lblBASKETBALLmaxNumberOfHalves
      // 
      this.lblBASKETBALLmaxNumberOfHalves.AutoSize = true;
      this.lblBASKETBALLmaxNumberOfHalves.Location = new System.Drawing.Point(-1, 64);
      this.lblBASKETBALLmaxNumberOfHalves.Name = "lblBASKETBALLmaxNumberOfHalves";
      this.lblBASKETBALLmaxNumberOfHalves.Size = new System.Drawing.Size(154, 13);
      this.lblBASKETBALLmaxNumberOfHalves.TabIndex = 380;
      this.lblBASKETBALLmaxNumberOfHalves.Text = "Maximum Number Of  ½ Points:";
      // 
      // txtBASKETBALLCostToBuyHalfSpread
      // 
      this.txtBASKETBALLCostToBuyHalfSpread.Location = new System.Drawing.Point(159, 34);
      this.txtBASKETBALLCostToBuyHalfSpread.Name = "txtBASKETBALLCostToBuyHalfSpread";
      this.txtBASKETBALLCostToBuyHalfSpread.Size = new System.Drawing.Size(34, 20);
      this.txtBASKETBALLCostToBuyHalfSpread.TabIndex = 360;
      // 
      // btnBuyingPointsOK
      // 
      this.btnBuyingPointsOK.Location = new System.Drawing.Point(323, 436);
      this.btnBuyingPointsOK.Name = "btnBuyingPointsOK";
      this.btnBuyingPointsOK.Size = new System.Drawing.Size(75, 23);
      this.btnBuyingPointsOK.TabIndex = 410;
      this.btnBuyingPointsOK.Text = "Ok";
      this.btnBuyingPointsOK.UseVisualStyleBackColor = true;
      this.btnBuyingPointsOK.Click += new System.EventHandler(this.btnBuyingPointsOK_Click);
      // 
      // btnBuyingPointsCancel
      // 
      this.btnBuyingPointsCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnBuyingPointsCancel.Location = new System.Drawing.Point(468, 436);
      this.btnBuyingPointsCancel.Name = "btnBuyingPointsCancel";
      this.btnBuyingPointsCancel.Size = new System.Drawing.Size(75, 23);
      this.btnBuyingPointsCancel.TabIndex = 420;
      this.btnBuyingPointsCancel.Text = "Cancel";
      this.btnBuyingPointsCancel.UseVisualStyleBackColor = true;
      this.btnBuyingPointsCancel.Click += new System.EventHandler(this.buyingPointsCancelButton_Click);
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.txtCollegeBASKETBALLmaxNumberOfHalvesTotalPoints);
      this.groupBox1.Controls.Add(this.label1);
      this.groupBox1.Controls.Add(this.label2);
      this.groupBox1.Controls.Add(this.txtCollegeBASKETBALLCostToBuyHalfTotalPoints);
      this.groupBox1.Controls.Add(this.label3);
      this.groupBox1.Controls.Add(this.txtCollegeBASKETBALLmaxNumberOfHalvesSpread);
      this.groupBox1.Controls.Add(this.label4);
      this.groupBox1.Controls.Add(this.txtCollegeBASKETBALLCostToBuyHalfSpread);
      this.groupBox1.Location = new System.Drawing.Point(427, 281);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(408, 149);
      this.groupBox1.TabIndex = 401;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Buy Points on College BASKETBALL";
      // 
      // txtCollegeBASKETBALLmaxNumberOfHalvesTotalPoints
      // 
      this.txtCollegeBASKETBALLmaxNumberOfHalvesTotalPoints.Location = new System.Drawing.Point(336, 61);
      this.txtCollegeBASKETBALLmaxNumberOfHalvesTotalPoints.Name = "txtCollegeBASKETBALLmaxNumberOfHalvesTotalPoints";
      this.txtCollegeBASKETBALLmaxNumberOfHalvesTotalPoints.Size = new System.Drawing.Size(34, 20);
      this.txtCollegeBASKETBALLmaxNumberOfHalvesTotalPoints.TabIndex = 400;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(51, 40);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(102, 13);
      this.label1.TabIndex = 350;
      this.label1.Text = "Cost to Buy ½ Point:";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(196, 18);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(51, 13);
      this.label2.TabIndex = 44;
      this.label2.Text = "SPREAD";
      // 
      // txtCollegeBASKETBALLCostToBuyHalfTotalPoints
      // 
      this.txtCollegeBASKETBALLCostToBuyHalfTotalPoints.Location = new System.Drawing.Point(336, 33);
      this.txtCollegeBASKETBALLCostToBuyHalfTotalPoints.Name = "txtCollegeBASKETBALLCostToBuyHalfTotalPoints";
      this.txtCollegeBASKETBALLCostToBuyHalfTotalPoints.Size = new System.Drawing.Size(34, 20);
      this.txtCollegeBASKETBALLCostToBuyHalfTotalPoints.TabIndex = 370;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(307, 18);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(85, 13);
      this.label3.TabIndex = 45;
      this.label3.Text = "TOTAL POINTS";
      // 
      // txtCollegeBASKETBALLmaxNumberOfHalvesSpread
      // 
      this.txtCollegeBASKETBALLmaxNumberOfHalvesSpread.Location = new System.Drawing.Point(159, 60);
      this.txtCollegeBASKETBALLmaxNumberOfHalvesSpread.Name = "txtCollegeBASKETBALLmaxNumberOfHalvesSpread";
      this.txtCollegeBASKETBALLmaxNumberOfHalvesSpread.Size = new System.Drawing.Size(34, 20);
      this.txtCollegeBASKETBALLmaxNumberOfHalvesSpread.TabIndex = 390;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(-1, 64);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(154, 13);
      this.label4.TabIndex = 380;
      this.label4.Text = "Maximum Number Of  ½ Points:";
      // 
      // txtCollegeBASKETBALLCostToBuyHalfSpread
      // 
      this.txtCollegeBASKETBALLCostToBuyHalfSpread.Location = new System.Drawing.Point(159, 34);
      this.txtCollegeBASKETBALLCostToBuyHalfSpread.Name = "txtCollegeBASKETBALLCostToBuyHalfSpread";
      this.txtCollegeBASKETBALLCostToBuyHalfSpread.Size = new System.Drawing.Size(34, 20);
      this.txtCollegeBASKETBALLCostToBuyHalfSpread.TabIndex = 360;
      // 
      // FrmCostToBuyHalfPoints
      // 
      this.AcceptButton = this.btnBuyingPointsOK;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnBuyingPointsCancel;
      this.ClientSize = new System.Drawing.Size(851, 475);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.btnBuyingPointsCancel);
      this.Controls.Add(this.btnBuyingPointsOK);
      this.Controls.Add(this.grpBuyPointsBASKETBALL);
      this.Controls.Add(this.grpBuyPointsCOLLEGEFOOTBALL);
      this.Controls.Add(this.grpBuyPointsNFL);
      this.Controls.Add(this.panProgressivePointBuying);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.MaximizeBox = false;
      this.Name = "FrmCostToBuyHalfPoints";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Buying ½ Points";
      this.Load += new System.EventHandler(this.CostToBuyHalfPoints_Load);
      this.panProgressivePointBuying.ResumeLayout(false);
      this.panProgressivePointBuying.PerformLayout();
      this.grpBuyPointsNFL.ResumeLayout(false);
      this.grpBuyPointsNFL.PerformLayout();
      this.grpBuyPointsCOLLEGEFOOTBALL.ResumeLayout(false);
      this.grpBuyPointsCOLLEGEFOOTBALL.PerformLayout();
      this.grpBuyPointsBASKETBALL.ResumeLayout(false);
      this.grpBuyPointsBASKETBALL.PerformLayout();
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox panProgressivePointBuying;
        private System.Windows.Forms.CheckBox chbUseProgressive;
        private System.Windows.Forms.GroupBox grpBuyPointsNFL;
        private System.Windows.Forms.GroupBox grpBuyPointsCOLLEGEFOOTBALL;
        private System.Windows.Forms.GroupBox grpBuyPointsBASKETBALL;
        private System.Windows.Forms.Button btnBuyingPointsOK;
        private System.Windows.Forms.Button btnBuyingPointsCancel;
        private System.Windows.Forms.TextBox txtNFLFOOTBALLCostToBuyON7Spread;
        private System.Windows.Forms.TextBox txtNFLFOOTBALLCostToBuyON3Spread;
        private System.Windows.Forms.TextBox txtNFLFOOTBALLmaxNumberOfHalvesSpread;
        private System.Windows.Forms.TextBox txtNFLFOOTBALLCostToBuyHalfSpread;
        private System.Windows.Forms.Label lblNFLFOOTBALLCostToBuyON7;
        private System.Windows.Forms.Label lblNFLFOOTBALLCostToBuyON3;
        private System.Windows.Forms.Label lblNFLFOOTBALLmaxNumberOfHalves;
        private System.Windows.Forms.Label lblNFLFOOTBALLCostToBuyHalf;
        private System.Windows.Forms.Label lblNFLFOOTBALLCostToBuyOFF7;
        private System.Windows.Forms.Label lblNFLFOOTBALLCostToBuyOFF3;
        private System.Windows.Forms.Label lblSpreadNFLFOOTBALL;
        private System.Windows.Forms.TextBox txtNFLFOOTBALLCostToBuyOFF7Spread;
        private System.Windows.Forms.TextBox txtNFLFOOTBALLCostToBuyOFF3Spread;
        private System.Windows.Forms.Label lblTotalPointsNFLFOOTBALL;
        private System.Windows.Forms.TextBox txtNFLFOOTBALLmaxNumberOfHalvesTotalPoints;
        private System.Windows.Forms.TextBox txtNFLFOOTBALLCostToBuyHalfTotalPoints;
        private System.Windows.Forms.TextBox txtCollegeFOOTBALLmaxNumberOfHalvesTotalPoints;
        private System.Windows.Forms.Label lblSpreadCollegeFOOTBALL;
        private System.Windows.Forms.TextBox txtCollegeFOOTBALLCostToBuyHalfTotalPoints;
        private System.Windows.Forms.Label lblCollegeFOOTBALLCostToBuyHalf;
        private System.Windows.Forms.Label lblTotalPointsCollegeFOOTBALL;
        private System.Windows.Forms.Label lblCollegeFOOTBALLmaxNumberOfHalves;
        private System.Windows.Forms.Label lblCollegeFOOTBALLCostToBuyON3;
        private System.Windows.Forms.TextBox txtCollegeFOOTBALLCostToBuyOFF7Spread;
        private System.Windows.Forms.Label lblCollegeFOOTBALLCostToBuyON7;
        private System.Windows.Forms.TextBox txtCollegeFOOTBALLCostToBuyOFF3Spread;
        private System.Windows.Forms.TextBox txtCollegeFOOTBALLCostToBuyHalfSpread;
        private System.Windows.Forms.Label lblCollegeFOOTBALLCostToBuyOFF7;
        private System.Windows.Forms.TextBox txtCollegeFOOTBALLmaxNumberOfHalvesSpread;
        private System.Windows.Forms.Label lblCollegeFOOTBALLCostToBuyOFF3;
        private System.Windows.Forms.TextBox txtCollegeFOOTBALLCostToBuyON3Spread;
        private System.Windows.Forms.TextBox txtCollegeFOOTBALLCostToBuyON7Spread;
        private System.Windows.Forms.TextBox txtBASKETBALLmaxNumberOfHalvesTotalPoints;
        private System.Windows.Forms.Label lblBASKETBALLCostToBuyHalf;
        private System.Windows.Forms.Label lblSpreadBASKETBALL;
        private System.Windows.Forms.TextBox txtBASKETBALLCostToBuyHalfTotalPoints;
        private System.Windows.Forms.Label lblTotalPointsBASKETBALL;
        private System.Windows.Forms.TextBox txtBASKETBALLmaxNumberOfHalvesSpread;
        private System.Windows.Forms.Label lblBASKETBALLmaxNumberOfHalves;
        private System.Windows.Forms.TextBox txtBASKETBALLCostToBuyHalfSpread;
        private System.Windows.Forms.Panel panProgressivePointsBuying;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtCollegeBASKETBALLmaxNumberOfHalvesTotalPoints;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCollegeBASKETBALLCostToBuyHalfTotalPoints;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtCollegeBASKETBALLmaxNumberOfHalvesSpread;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCollegeBASKETBALLCostToBuyHalfSpread;
    }
}