﻿namespace CustomerMaintenance.UI
{
    partial class FrmAdjustCustomerCarryoverFigure
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNewCarryoverFigure = new System.Windows.Forms.Label();
            this.txtNewCarryoverFigure = new System.Windows.Forms.TextBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblNewCarryoverFigure
            // 
            this.lblNewCarryoverFigure.AutoSize = true;
            this.lblNewCarryoverFigure.Location = new System.Drawing.Point(24, 23);
            this.lblNewCarryoverFigure.Name = "lblNewCarryoverFigure";
            this.lblNewCarryoverFigure.Size = new System.Drawing.Size(112, 13);
            this.lblNewCarryoverFigure.TabIndex = 10;
            this.lblNewCarryoverFigure.Text = "New Carryover Figure:";
            // 
            // txtNewCarryoverFigure
            // 
            this.txtNewCarryoverFigure.Location = new System.Drawing.Point(142, 20);
            this.txtNewCarryoverFigure.Name = "txtNewCarryoverFigure";
            this.txtNewCarryoverFigure.Size = new System.Drawing.Size(100, 20);
            this.txtNewCarryoverFigure.TabIndex = 20;
            this.txtNewCarryoverFigure.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numberAmount_KeyPress);
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(55, 54);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 30;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(142, 54);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 40;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // frmAdjustCustomerCarryoverFigure
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(284, 88);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.txtNewCarryoverFigure);
            this.Controls.Add(this.lblNewCarryoverFigure);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmAdjustCustomerCarryoverFigure";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Adjust Customer Carryover Figure";
            this.Load += new System.EventHandler(this.frmAdjustCustomerCarryoverFigure_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNewCarryoverFigure;
        private System.Windows.Forms.TextBox txtNewCarryoverFigure;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
    }
}