﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using CustomerMaintenance.Libraries;
using GUILibraries.Controls;
using GUILibraries.Forms;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using System.Data;

namespace CustomerMaintenance.UI {
  public partial class FrmCustomerRebates : SIDForm {
    #region private Classes
    private class RebateBetType {
      public String Description { get; set; }
      public String Code { get; set; }
    }

    private class RebateWagerType {
      public String Description { get; set; }
      public String Code { get; set; }
    }

    #endregion

    #region Private Vars

    CustomerInfoUpdater.CustomerRebateInfo _rebateFrmInfo;

    #endregion

    #region Public Properties

    #endregion

    #region Private Properties

    spCstGetCustomerInfoNoNulls_Result CustomerInfo { get; set; }
    Form ParentFrm { get; set; }
    CustomerInfoUpdater CustomerInfoUpdater { get; set; }
    private List<spSptGetSportAndSubSportTypes_Result> SportAndSubSportList { get; set; }
    private List<spCstGetCustomerRebateSportAndWageringSettings_Result> WagerRebateSettings { get; set; }
    private List<RebateBetType> BetTypes { get; set; }
    private List<RebateWagerType> WagerTypes { get; set; }

    #endregion

    #region Structures

    #endregion

    #region Constructors

    public FrmCustomerRebates(spCstGetCustomerInfoNoNulls_Result customerInfo, CustomerInfoUpdater customerInfoUpdater, SIDForm callerFrm)
      : base(callerFrm.AppModuleInfo) {
      CustomerInfo = customerInfo;
      ParentFrm = callerFrm;
      CustomerInfoUpdater = customerInfoUpdater;
      InitializeComponent();
    }

    #endregion

    #region Public Methods

    #endregion

    #region Private Methods

    private void DeletePackageDetailsFromDb(int packageDetailsId) {
      using (var rebateSettings = new Customers(AppModuleInfo)) {
        rebateSettings.DeleteCustomerRebateSettingsDetailsInfo(packageDetailsId);
      }
    }

    private void FillSportAndWageringSettingsGrid() {
      var col = new DataGridViewComboBoxColumn { Name = "SportType" };

      var dataTable = new DataTable();
      dataTable.Columns.Add("SportType");

      var sportTypes = (from st in SportAndSubSportList
                        orderby st.SportType
                        select st.SportType.Trim()).Distinct().ToList();

      foreach (var str in sportTypes) {
        var dr = dataTable.NewRow();
        dr["SportType"] = str;
        dataTable.Rows.Add(dr);
      }

      col.DataSource = dataTable;
      col.DisplayMember = "SportType";
      col.ValueMember = "SportType";

      if (dgvRebateWageringSettings.Columns["SportType"] != null) {
        var lineTypeIdx = dgvRebateWageringSettings.Columns["SportType"].Index;
        dgvRebateWageringSettings.Columns.Remove("SportType");
        dgvRebateWageringSettings.Columns.Insert(lineTypeIdx, col);
      }

      col = new DataGridViewComboBoxColumn { Name = "SportSubType" };

      dataTable = new DataTable();
      dataTable.Columns.Add("SportSubType");

      var sportSubTypes = (from st in SportAndSubSportList
                           orderby st.SportType
                           select st.SportSubType.Trim()).Distinct().ToList();

      foreach (var str in sportSubTypes) {
        var dr = dataTable.NewRow();
        dr["SportSubType"] = str;
        dataTable.Rows.Add(dr);
      }

      col.DataSource = dataTable;
      col.DisplayMember = "SportSubType";
      col.ValueMember = "SportSubType";

      if (dgvRebateWageringSettings.Columns["SportSubType"] != null) {
        var lineTypeIdx = dgvRebateWageringSettings.Columns["SportSubType"].Index;
        dgvRebateWageringSettings.Columns.Remove("SportSubType");
        dgvRebateWageringSettings.Columns.Insert(lineTypeIdx, col);
      }

      col = new DataGridViewComboBoxColumn { Name = "BetType" };

      dataTable = new DataTable();
      dataTable.Columns.Add("BetType");
      dataTable.Columns.Add("Code");

      foreach (var str in BetTypes) {
        var dr = dataTable.NewRow();
        dr["BetType"] = str.Description;
        dr["Code"] = str.Code;
        dataTable.Rows.Add(dr);
      }

      col.DataSource = dataTable;
      col.DisplayMember = "BetType";
      col.ValueMember = "Code";

      if (dgvRebateWageringSettings.Columns["BetType"] != null) {
        var lineTypeIdx = dgvRebateWageringSettings.Columns["BetType"].Index;
        dgvRebateWageringSettings.Columns.Remove("BetType");
        dgvRebateWageringSettings.Columns.Insert(lineTypeIdx, col);
      }

      col = new DataGridViewComboBoxColumn { Name = "WagerType" };

      dataTable = new DataTable();
      dataTable.Columns.Add("WagerType");
      dataTable.Columns.Add("Code");

      foreach (var str in WagerTypes) {
        var dr = dataTable.NewRow();
        dr["WagerType"] = str.Description;
        dr["Code"] = str.Code;
        dataTable.Rows.Add(dr);
      }

      col.DataSource = dataTable;
      col.DisplayMember = "WagerType";
      col.ValueMember = "Code";

      if (dgvRebateWageringSettings.Columns["WagerType"] != null) {
        var lineTypeIdx = dgvRebateWageringSettings.Columns["WagerType"].Index;
        dgvRebateWageringSettings.Columns.Remove("WagerType");
        dgvRebateWageringSettings.Columns.Insert(lineTypeIdx, col);
      }

      foreach (var rebateDetail in WagerRebateSettings) {
        var rIndex = dgvRebateWageringSettings.Rows.Add();
        dgvRebateWageringSettings["SportType", rIndex].Value = rebateDetail.SportType.Trim();
        dgvRebateWageringSettings["SportSubType", rIndex].Value = rebateDetail.SportSubType.Trim();
        dgvRebateWageringSettings["BetType", rIndex].Value = rebateDetail.BetType.Trim();//CastToBetTypeDescription(rebateDetail.BetType.Trim());
        dgvRebateWageringSettings["WagerType", rIndex].Value = rebateDetail.WagerType.Trim();//CastToWagerTypeDescription(rebateDetail.WagerType.Trim());
        dgvRebateWageringSettings["RebatePerc", rIndex].Value = rebateDetail.RebatePerc ?? 0;
        dgvRebateWageringSettings["PackageDetailsId", rIndex].Value = rebateDetail.Id;
      }

      if (WagerRebateSettings.Count == 0) //Insert default record, as customer was never added before.
            {
        var rIndex = dgvRebateWageringSettings.Rows.Add();
        dgvRebateWageringSettings["SportType", rIndex].Value = "";
        dgvRebateWageringSettings["SportSubType", rIndex].Value = "";
        dgvRebateWageringSettings["BetType", rIndex].Value = "";
        dgvRebateWageringSettings["WagerType", rIndex].Value = "";
        dgvRebateWageringSettings["RebatePerc", rIndex].Value = 0;
      }


    }

    private string GetSelectedPaymentCriteria() {
      const string radName = "";
      foreach (Control ctrl in grpRebatePaymentType.Controls) {
        if (ctrl.GetType().ToString() != "System.Windows.Forms.RadioButton") continue;
        if (((RadioButton)ctrl).Checked)
          return ctrl.AccessibleDescription;
      }
      return radName;
    }

    private string GetSelectedFrequency() {
      const string radName = "";
      foreach (Control ctrl in grpFrequency.Controls) {
        if (ctrl.GetType().ToString() == "System.Windows.Forms.RadioButton") {
          if (((RadioButton)ctrl).Checked)
            return ctrl.AccessibleDescription;
        }
      }
      return radName;
    }

    private void LoadBetAndWagerTypes() {
      BetTypes = new List<RebateBetType>();
      var betType = new RebateBetType { Description = "All Types", Code = "A, C, I, P, T, E, L, M, S" };
      BetTypes.Add(betType);

      betType = new RebateBetType { Description = "Futures", Code = SIDLibraries.BusinessLayer.WagerType.CONTEST };
      BetTypes.Add(betType);

      betType = new RebateBetType { Description = "If-Bet", Code = SIDLibraries.BusinessLayer.WagerType.IFBET };
      BetTypes.Add(betType);

      betType = new RebateBetType { Description = "Manual Play", Code = "A" };
      BetTypes.Add(betType);

      betType = new RebateBetType { Description = "Parlay", Code = SIDLibraries.BusinessLayer.WagerType.PARLAY };
      BetTypes.Add(betType);

      betType = new RebateBetType { Description = "Straight Bet", Code = "E, L, M, S" };
      BetTypes.Add(betType);

      betType = new RebateBetType { Description = "Teaser", Code = SIDLibraries.BusinessLayer.WagerType.TEASER };
      BetTypes.Add(betType);

      WagerTypes = new List<RebateWagerType>();
      var wagerType = new RebateWagerType { Description = "Team Totals", Code = SIDLibraries.BusinessLayer.WagerType.TEAMTOTALPOINTS };
      WagerTypes.Add(wagerType);

      wagerType = new RebateWagerType { Description = "Total Points", Code = SIDLibraries.BusinessLayer.WagerType.TOTALPOINTS };
      WagerTypes.Add(wagerType);

      wagerType = new RebateWagerType { Description = "Money Line", Code = SIDLibraries.BusinessLayer.WagerType.MONEYLINE };
      WagerTypes.Add(wagerType);

      wagerType = new RebateWagerType { Description = "Spread", Code = SIDLibraries.BusinessLayer.WagerType.SPREAD };
      WagerTypes.Add(wagerType);

    }

    private void LoadCustomerRebateProgramSettings() {
      txtPackageName.Text = CustomerInfo.PackageName.Trim();
      if (txtPackageName.Text == "") {
        txtPackageName.Text = @"D - " + CustomerInfo.CustomerID.Trim();
      }
      chbCreateRebateTran.Checked = CustomerInfo.InsertRebateTransaction;

      switch (CustomerInfo.ExecutionFrequency.Trim()) {
        case "Daily":
          radRunFrequencyDaily.Checked = true;
          break;
        case "Weekly":
          radRunFrequencyWeekly.Checked = true;
          break;
      }

      chbOverrideMakeup.Enabled = false;
      chbOverrideMakeup.Checked = false;
      switch (CustomerInfo.PaymentCriteria.Trim()) {
        case "Volume":
          radPaymentTypeVolume.Checked = true;

          break;
        case "Lost Volume":
          radPaymentTypeLostVolume.Checked = true;
          break;
        case "Net Loss":
          radPaymentTypeNetLoss.Checked = true;
          chbOverrideMakeup.Enabled = true;
          chbOverrideMakeup.Checked = CustomerInfo.OverrideMakeUp;
          break;
      }

      txtMakeupValue.Text = FormatNumber(CustomerInfo.NewCustomerMakeup, false, 1, true);
    }

    private void LoadRebateSportAndWageringSettings() {
      using (var sports = new SportTypes(AppModuleInfo)) {
        SportAndSubSportList = sports.GetSportAndSubSportTypes().ToList();
      }

      using (var rebateSettings = new Customers(AppModuleInfo)) {
        int packageId;
        int.TryParse((CustomerInfo.PackageId ?? -1).ToString(CultureInfo.InvariantCulture), out packageId);
        WagerRebateSettings = rebateSettings.GetCustomerRebateSettingsDetailsInfo(packageId);
      }

    }

    private void SaveChanges() {
      var frmPackageName = txtPackageName.Text.Trim();
      if (frmPackageName.Length == 0) {
        MessageBox.Show(@"Package Name Cannot Be Empty", @"Error");
        txtPackageName.Focus();
        return;
      }

      var frmExecutionFrequency = GetSelectedFrequency();
      var frmPaymentCriteria = GetSelectedPaymentCriteria();
      var frmNewMakeup = txtMakeupValue.Text.Replace(",", "");
      var frmOverrideMakeup = chbOverrideMakeup.Checked;
      var frmInsertRebateTransaction = chbCreateRebateTran.Checked;

      _rebateFrmInfo = new CustomerInfoUpdater.CustomerRebateInfo { CustomerId = CustomerInfo.CustomerID.Trim(), PackageName = frmPackageName, ExecutionFrequency = frmExecutionFrequency, PaymentCriteria = frmPaymentCriteria };
      double newMakeup;
      double.TryParse(frmNewMakeup, out newMakeup);
      _rebateFrmInfo.NewMakeup = newMakeup;
      _rebateFrmInfo.OverrideMakeup = frmOverrideMakeup;
      _rebateFrmInfo.InsertRebateTransaction = frmInsertRebateTransaction;


      var rebateDbInfo = new CustomerInfoUpdater.CustomerRebateInfo {
        CustomerId = CustomerInfo.CustomerID.Trim(),
        PackageName = CustomerInfo.PackageName.Trim(),
        ExecutionFrequency = CustomerInfo.ExecutionFrequency.Trim(),
        PaymentCriteria = CustomerInfo.PaymentCriteria.Trim(),
        NewMakeup = CustomerInfo.NewCustomerMakeup,
        OverrideMakeup = CustomerInfo.OverrideMakeUp,
        InsertRebateTransaction = CustomerInfo.InsertRebateTransaction
      };

      CustomerInfoUpdater.CustomerRebateInfoFromDb = rebateDbInfo;
      CustomerInfoUpdater.CompareCustomerRebatePackageToUpdate(ref _rebateFrmInfo);

      if (_rebateFrmInfo.NewMakeup != rebateDbInfo.NewMakeup) {
        UpdateMakeupValueInParentForm(_rebateFrmInfo.NewMakeup);
      }
      SaveGridViewInformation();
    }

    private void SaveGridViewInformation() {
      foreach (DataGridViewRow row in dgvRebateWageringSettings.Rows) {
        int? packageDetailsId = null;
        if (row.Cells["PackageDetailsId"].Value != null)
          packageDetailsId = int.Parse(row.Cells["PackageDetailsId"].Value.ToString());
        string sportType = null;
        if (row.Cells["SportType"].Value != null)
          sportType = row.Cells["SportType"].Value.ToString();

        String sportSubType = null;
        if (row.Cells["SportSubType"].Value != null)
          sportSubType = row.Cells["SportSubType"].Value.ToString();
        String betType = null;
        if (row.Cells["BetType"].Value != null)
          betType = row.Cells["BetType"].Value.ToString();
        String wagerType = null;
        if (row.Cells["WagerType"].Value != null)
          wagerType = row.Cells["WagerType"].Value.ToString();

        String rebatePerc = null;
        if (row.Cells["RebatePerc"].Value != null)
          rebatePerc = row.Cells["RebatePerc"].Value.ToString();

        if (sportType == null && sportSubType == null && betType == null &&
            wagerType == null && rebatePerc == null && packageDetailsId == null)
          continue;

        double perc = 0;

        if (rebatePerc != null)
          double.TryParse(rebatePerc, out perc);

        UpdateDataBaseValues(packageDetailsId, sportType, sportSubType, betType, wagerType, perc);

      }
    }

    private void UpdateDataBaseValues(int? packageDetailsId, string sportType, string sportSubType, string betType, string wagerType, double rebatePerc) {
      using (var rebateSettings = new Customers(AppModuleInfo)) {
        rebateSettings.UpdateCustomerRebateSettingsDetailsInfo(_rebateFrmInfo.PackageId, packageDetailsId, sportType, sportSubType, betType, wagerType, rebatePerc);
      }
    }

    private void UpdateMakeupValueInParentForm(double newMakeup) {
      var parentTextBox = (NumberTextBox)ParentFrm.Controls.Find("txtNewMakeup", true).FirstOrDefault();
      if (parentTextBox != null) {
        parentTextBox.Text = FormatNumber(newMakeup, false, 1, true);
      }
    }

    #endregion

    #region Protected Methods

    #endregion

    #region Events

    private void frmCustomerRebates_Load(object sender, EventArgs e) {
      Text += @" - " + CustomerInfo.CustomerID.Trim();
      LoadCustomerRebateProgramSettings();
      LoadBetAndWagerTypes();
      LoadRebateSportAndWageringSettings();
      FillSportAndWageringSettingsGrid();
    }

    private void btnAccept_Click(object sender, EventArgs e) {
      SaveChanges();
      Close();
    }

    private void btnCancel_Click(object sender, EventArgs e) {
      Close();
    }

    private void dgvRebateWageringSettings_CellEnter(object sender, DataGridViewCellEventArgs e) {
      if (dgvRebateWageringSettings.CurrentCell.ColumnIndex == 4) {
        dgvRebateWageringSettings.CurrentCell.ReadOnly = false;
      }
    }

    private void dgvRebateWageringSettings_CellValidating(object sender, DataGridViewCellValidatingEventArgs e) {
      if (e.ColumnIndex != 4) return;
      var rebatePerc = "";
      if (dgvRebateWageringSettings.Rows[e.RowIndex].Cells["RebatePerc"].EditedFormattedValue != null)
        rebatePerc = dgvRebateWageringSettings.Rows[e.RowIndex].Cells["RebatePerc"].EditedFormattedValue.ToString();
      if (rebatePerc == "") return;
      int targetValue;
      int.TryParse(rebatePerc, out targetValue);
      if (targetValue >= 0 && targetValue <= 100) return;
      e.Cancel = true;
      MessageBox.Show(@"Invalid Rebate Percent Value", @"Invalid Number");
    }

    private void dgvRebateWageringSettings_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e) {
      if (dgvRebateWageringSettings.CurrentCell.ColumnIndex == 0 || dgvRebateWageringSettings.CurrentCell.ColumnIndex == 1) {
        if (dgvRebateWageringSettings.CurrentCell.ColumnIndex == 0) {
          var sportsComboBox = (ComboBox)e.Control;
          sportsComboBox.SelectedIndexChanged += SportsComboBox_SelectedIndexChanged;
        }
        else {
          var subSportsComboBox = (ComboBox)e.Control;
          subSportsComboBox.DropDown += SubSportsComboBox_DropDown;
        }
      }
    }

    private void dgvRebateWageringSettings_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e) {
      if (e.Row.Index == 0) {
        e.Cancel = true;
      }
      else {
        int? packageDetailsId = null;
        var row = e.Row;
        if (row.Cells["PackageDetailsId"].Value != null)
          packageDetailsId = int.Parse(row.Cells["PackageDetailsId"].Value.ToString());
        if (packageDetailsId != null) {
          DeletePackageDetailsFromDb((int)packageDetailsId);
        }
      }

    }

    private void radPaymentTypeNetLoss_CheckedChanged(object sender, EventArgs e) {
      if (((RadioButton)sender).Checked) {
        chbOverrideMakeup.Enabled = true;
      }
      else {
        chbOverrideMakeup.Checked = false;
        chbOverrideMakeup.Enabled = false;
      }
    }

    private void SportsComboBox_SelectedIndexChanged(object sender, EventArgs e) {
      if (dgvRebateWageringSettings.CurrentRow == null) return;
      var selectedSport = ((ComboBox)sender).Text;
      var selectedRow = dgvRebateWageringSettings.CurrentRow.Index;

      var cbCell = (DataGridViewComboBoxCell)dgvRebateWageringSettings.Rows[selectedRow].Cells["SportSubType"];

      var dataTable = new DataTable();
      dataTable.Columns.Add("SportSubType");
      var sportSubTypes = (from st in SportAndSubSportList
                           where st.SportType.Trim() == selectedSport.Trim()
                           orderby st.SportType
                           select st.SportSubType.Trim()).Distinct().ToList();

      foreach (var str in sportSubTypes) {
        var dr = dataTable.NewRow();
        dr["SportSubType"] = str;
        dataTable.Rows.Add(dr);
      }

      if (dataTable.Rows.Count > 0) {
        cbCell.DataSource = dataTable;
        cbCell.Value = dataTable.Rows[0].ItemArray[0];
      }
    }

    private void SubSportsComboBox_DropDown(object sender, EventArgs e) {
      if (dgvRebateWageringSettings.CurrentRow == null) return;
      var selectedRow = dgvRebateWageringSettings.CurrentRow.Index;
      var selectedSport = "";
      if (dgvRebateWageringSettings.Rows[selectedRow].Cells["SportType"].Value != null) {
        selectedSport = dgvRebateWageringSettings.Rows[selectedRow].Cells["SportType"].Value.ToString();
      }
      var cbCell = (DataGridViewComboBoxCell)dgvRebateWageringSettings.Rows[selectedRow].Cells["SportSubType"];

      var dataTable = new DataTable();
      dataTable.Columns.Add("SportSubType");
      var sportSubTypes = (from st in SportAndSubSportList
                           where st.SportType.Trim() == selectedSport
                           orderby st.SportType
                           select st.SportSubType.Trim()).Distinct().ToList();

      foreach (var str in sportSubTypes) {
        var dr = dataTable.NewRow();
        dr["SportSubType"] = str;
        dataTable.Rows.Add(dr);
      }

      if (dataTable.Rows.Count > 0) {
        cbCell.DataSource = dataTable;
        cbCell.Value = dataTable.Rows[0].ItemArray[0];
      }
    }

    #endregion
  }
}