﻿using System;
using System.Linq;
using System.Windows.Forms;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using GUILibraries.Forms;

namespace CustomerMaintenance.UI {
  public partial class FrmCustomerMessage : SIDForm {
    #region Private Vars

    readonly String _frmMode = "";
    readonly FrmCustomerMaintenance _parent;
    readonly spCstGetCustomerMessages_Result _messageObj;
    private Boolean _expDateWasChanged;

    #endregion

    #region Public Properties

    #endregion

    #region Private Properties

    #endregion

    #region Structures

    #endregion

    #region Constructors

    public FrmCustomerMessage(String frmMode, spCstGetCustomerMessages_Result messageObj, FrmCustomerMaintenance parent)
      : base(parent.AppModuleInfo) {
      _frmMode = frmMode;
      _messageObj = messageObj;
      _parent = parent;
      InitializeComponent();
    }

    #endregion

    #region Public Methods

    #endregion

    #region Private Methods

    private static string GetMessagePriorityNameFromCode(string priorityCode) {
      var priority = "";
      switch (priorityCode) {
        case "H":
          priority = "Always Alert";//"High";
          break;
        case "L":
          priority = "Inbox Only";//"Low";
          break;
        case "M":
          priority = "Alert once";//"Medium";
          break;
        case "S":
          priority = "Suspended";//"Medium";
          break;
      }
      return priority;
    }

    private string GetMessageStatusCode() {
      var status = "U";

      if (_frmMode != "Edit" || chbStatus.SelectedItem == null) return status;
      switch (chbStatus.SelectedItem.ToString()) {

        case "Deleted":
          status = "D";
          break;
        case "Read":
          status = "R";
          break;
        case "Unread":
          status = "U";
          break;

      }
      return status;
    }

    private static string GetMessageStatusNameFromCode(string statusCode) {
      var status = "";
      switch (statusCode) {
        case "D":
          status = "Deleted";
          break;
        case "R":
          status = "Read";
          break;
        case "U":
          status = "Unread";
          break;
      }
      return status;
    }

    private string GetMessagePriorityCode() {
      var priority = "L";

      if (cmbPriority.SelectedItem == null)
        return priority;

      switch (cmbPriority.SelectedItem.ToString()) {
        case "Always Alert"://"High":
          priority = "H";
          break;
        case "Inbox Only"://"Low":
          priority = "L";
          break;
        case "Alert once"://"Medium":
          priority = "M";
          break;
        case "Suspended"://"Medium":
          priority = "S";
          break;
      }
      return priority;
    }

    private void HandleExpirationDateValueChange() {
      if (_frmMode == "Edit") {
        if (_messageObj.MsgExpirationDateTime != null) {
          var newExpirationDate = dtmExpirationDate.Value.Date;
          var previousExpirationDate = ((DateTime)_messageObj.MsgExpirationDateTime).Date;
          _expDateWasChanged = newExpirationDate != previousExpirationDate;
        }
        else
          _expDateWasChanged = false;
      }
    }

    private void LoadForm() {
      if (_frmMode == "Edit") {
        Text = @"Change Message";
        btnAccept.Text = @"Modify";
        txtSubject.ReadOnly = false;
        txtSubject.Enabled = true;
        txtBody.ReadOnly = false;
        txtBody.Enabled = true;
        if (_messageObj != null) {
          txtSubject.Text = _messageObj.MsgSubject;
          txtBody.Text = _messageObj.MsgBody;
          DateTime eDt;
          if (_messageObj.MsgExpirationDateTime != null)
            eDt = (DateTime)_messageObj.MsgExpirationDateTime;
          else
            eDt = DateTime.Now;
          dtmExpirationDate.Value = eDt;

          chbStatus.Text = GetMessageStatusNameFromCode(_messageObj.MsgStatus);

          cmbPriority.Text = GetMessagePriorityNameFromCode(_messageObj.MsgPriority);

          if (_messageObj.MsgStatus == "D") {
            chbFlagAsDeleted.Visible = false;
          }

        }
      }
      else {
        Text = @"New Message";
        cmbPriority.SelectedIndex = 1;
        panStatus.Visible = false;
      }

      panSendToPackage.Visible = _parent.AgentTypeFromDb == Agents.AGENT_TYPE;
    }

    private decimal? SaveMessage() {
      var status = GetMessageStatusCode();
      if (chbFlagAsDeleted.Checked)
        status = "D";
      var priority = GetMessagePriorityCode();
      decimal? returnedId = null;

      using (var cust = new Customers(AppModuleInfo)) {
        if (_frmMode == "Edit" && _messageObj != null) {
          cust.UpdateCustomerMessage(_messageObj.CustomerMessageId, _messageObj.CustomerID, txtSubject.Text.Trim(), txtBody.Text.Trim(), dtmExpirationDate.Value, _expDateWasChanged, status, priority, AppModuleInfo.Description);
        }
        else {
          returnedId = cust.AddCustomerMessage(_parent.CustomerId, dtmExpirationDate.Value, status, priority, txtSubject.Text, txtBody.Text, null);
        }
      }
      var showDeleted = (CheckBox)_parent.Controls.Find("chbShowDeleted", true).FirstOrDefault();
      _parent.GetCustomerMessages(true, showDeleted != null && showDeleted.Checked);
      Close();
      return returnedId;
    }

    private void ValidateAndSaveMessage() {
      DialogResult dialogResult;
      var sendMessage = true;
      var msgExpirationDate = dtmExpirationDate.Value;
      var msgSubject = txtSubject.Text.Trim();
      var msgBody = txtBody.Text.Trim();

      if (msgSubject == "" || msgSubject.Length == 0) {
        dialogResult = MessageBox.Show(@"Leave Subject Field empty?", @"Attention", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        if (dialogResult == DialogResult.Yes) { }
        else {
          txtSubject.Focus();
          return;
        }
      }

      if (MessageBodyIsEmpty(msgBody)) return;
      var retvalue = SaveMessage();
      if (!chbSendToPackage.Checked) return;
      var status = GetMessageStatusCode();
      if (chbFlagAsDeleted.Checked)
        status = "D";
      var priority = GetMessagePriorityCode();
      if (_frmMode == "Edit" && _messageObj != null) {
        if (_messageObj.SentToPackage != null && (Boolean)_messageObj.SentToPackage) {
          dialogResult = MessageBox.Show(@"Message already Sent to package.  Send again?", @"Attention", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
          if (dialogResult == DialogResult.No) {
            sendMessage = false;
          }
        }

        if (!sendMessage) return;
        using (var cust = new Customers(AppModuleInfo)) {
          cust.UpdateMessageToCustomers(_messageObj.CustomerMessageId, _parent.CustomerId, msgExpirationDate, _expDateWasChanged, status, priority, msgSubject, msgBody, AppModuleInfo.Description);
        }
      }
      else {
        _parent.SendMessageToCustomer(_parent.CustomerId, msgExpirationDate, status, priority, msgSubject, msgBody, retvalue, true);
      }
    }

    private bool MessageBodyIsEmpty(string msgBody) {
      if (msgBody == "" || msgBody.Length == 0) {
        MessageBox.Show(@"Please type in some text as message body", @"Warning", MessageBoxButtons.OK, MessageBoxIcon.Stop);
        txtBody.Focus();
        return true;
      }
      return false;
    }

    private void WriteActionDescription(object sender) {
      txtActionDesc.Text = "";
      switch (((ComboBox)sender).SelectedIndex) {
        case 0:
          txtActionDesc.Text = FrmCustomerMaintenance.CUST_MSG_ALWAYS_ALERT;
          break;
        case 1:
          txtActionDesc.Text = FrmCustomerMaintenance.CUST_MSG_ALERT_ONCE;
          break;
        case 2:
          txtActionDesc.Text = FrmCustomerMaintenance.CUST_MSG_INBOX_ONLY;
          break;
        case 3:
          txtActionDesc.Text = FrmCustomerMaintenance.CUST_MSG_SUSPENDED;
          break;
      }
    }

    #endregion

    #region Protected Methods

    #endregion

    #region Events

    private void frmCustomerMessage_Load(object sender, EventArgs e) {
      LoadForm();
    }

    private void btnAccept_Click(object sender, EventArgs e) {
      ValidateAndSaveMessage();
    }

    private void btnCancel_Click(object sender, EventArgs e) {
      Close();
    }

    private void dtmExpirationDate_ValueChanged(object sender, EventArgs e) {
      HandleExpirationDateValueChange();
    }

    private void cmbPriority_SelectedIndexChanged(object sender, EventArgs e) {
      WriteActionDescription(sender);
    }

    #endregion

  }
}