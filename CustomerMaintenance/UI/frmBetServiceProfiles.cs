﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using CustomerMaintenance.Libraries;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using GUILibraries.Forms;

namespace CustomerMaintenance.UI {
  public partial class FrmBetServiceProfiles : SIDForm {
    private readonly String _currentCustomerId = "";
    private readonly CustomerInfoUpdater _customerInfo;


    public FrmBetServiceProfiles(String customerId, ModuleInfo moduleInfo) : base (moduleInfo) {
      if (!string.IsNullOrEmpty(customerId)) {
        _currentCustomerId = customerId;
        _customerInfo = new CustomerInfoUpdater(_currentCustomerId, AppModuleInfo);
        InitializeComponent();
      }
      else {
        MessageBox.Show(@"No customer was selected");
        Close();
      }


    }

    private void BetServiceProfilesForm_Load(object sender, EventArgs e) {
      List<spCstGetCustomerAvailableBetServices_Result> custBetSrvs;
      using (var bServices = new BetServices(AppModuleInfo)) {

        custBetSrvs = bServices.GetCustomerAvailableBetServices(_currentCustomerId).ToList();
      }

      betServiceProfGV.DataSource = custBetSrvs;

      _customerInfo.LoadCustomerAvailableBetServicesToStructFromDb(betServiceProfGV);
    }

    private void cancelButton_Click(object sender, EventArgs e) {
      Close();
    }

    private void OkButton_Click(object sender, EventArgs e) {
      _customerInfo.CompareCustomerAvailableBetServicesInfoToUpdate(_customerInfo.CustomerAvailableBetSvcProfilesFromDbList, betServiceProfGV);
      Close();
    }
  }
}
