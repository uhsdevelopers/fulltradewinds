﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using GUILibraries.Forms;
using GUILibraries.Utilities;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;

namespace CustomerMaintenance.UI {
  public partial class FrmCustomerHorseLimitsAndTrackPermissions : SIDForm {

    private readonly FrmCustomerMaintenance _parentForm;
    private String _currentCustomerId = "";

    HorseRacing _hRacing;

    public FrmCustomerHorseLimitsAndTrackPermissions(SIDForm parent) : base(parent.AppModuleInfo) {
      _parentForm = (FrmCustomerMaintenance)parent;
      InitializeComponent();
    }

    private void frmCustomerHorseLimitsAndTrackPermissions_Load(object sender, EventArgs e) {
      _currentCustomerId = _parentForm.CustomerId;
      GetCustomerTrackPermissions();
      GetCustomerHorseWagerLimits();
      txtMaxPayout.Text = _parentForm.HorseRacingMaxPayOut;


    }

    private void GetCustomerTrackPermissions() {
      try {
        using (_hRacing = new HorseRacing(AppModuleInfo)) {
          var trackPermissions = _hRacing.GetCustomerHorseRacingTrackPermissions(_currentCustomerId).ToList();

          {
            dgvwTrackPermissions.Columns.Add("TrackName", "Track Name");

            var checkColumn = new DataGridViewCheckBoxColumn {
              Name = "Allowed",
              HeaderText = @"Allowed",
              ReadOnly = false,
              Width = 50
            };
            dgvwTrackPermissions.Columns.Add(checkColumn);

            dgvwTrackPermissions.Columns[0].Width = 200;
            dgvwTrackPermissions.Columns[0].ReadOnly = true;


            foreach (spHrGetCustomerHorseRacingTrackPermissions_Result res in trackPermissions) {
              dgvwTrackPermissions.Rows.Add(res.TrackName, res.Enabled);
            }

            FormF.PreventDataGridViewColumnsToBeSortedAndResized(dgvwTrackPermissions);
          }
        }
      }
      catch (EntitySqlException ex) {
        Log(ex);
      }
    }

    private void GetCustomerHorseWagerLimits() {
      try {
        using (_hRacing = new HorseRacing(AppModuleInfo)) {
          var horseWagerLimits = _hRacing.GetCustomerHorseWagerLimits(_currentCustomerId).FirstOrDefault();
          if (horseWagerLimits != null) {
            txtMaxWin.Text = horseWagerLimits.WinMax.ToString();
            txtMaxPlace.Text = horseWagerLimits.PlaceMax.ToString();
            txtMaxShow.Text = horseWagerLimits.ShowMax.ToString();
            txtMaxExacta.Text = horseWagerLimits.ExactaMax.ToString();
            txtMaxTrifecta.Text = horseWagerLimits.TrifectaMax.ToString();
            txtMaxQuinella.Text = horseWagerLimits.QuinellaMax.ToString();
            txtMaxDailyDbl.Text = horseWagerLimits.DailyDoubleMax.ToString();
            txtMaxPick3To8.Text = horseWagerLimits.PickMax.ToString();

            txtMinWin.Text = horseWagerLimits.WinMin.ToString();
            txtMinPlace.Text = horseWagerLimits.PlaceMin.ToString();
            txtMinShow.Text = horseWagerLimits.ShowMin.ToString();
            txtMinExacta.Text = horseWagerLimits.ExactaMin.ToString();
            txtMinTrifecta.Text = horseWagerLimits.TrifectaMin.ToString();
            txtMinQuinella.Text = horseWagerLimits.QuinellaMin.ToString();
            txtMinDailyDbl.Text = horseWagerLimits.DailyDoubleMin.ToString();
            txtMinPick3To8.Text = horseWagerLimits.PickMin.ToString();
          }
        }

      }
      catch (EntitySqlException ex) {
        Log(ex);
      }
    }

    private void btnCancel_Click(object sender, EventArgs e) {
      Close();
    }

    private void btnCheckAll_Click(object sender, EventArgs e) {
      ToggleGridViewCheckBoxes(true);
    }

    private void btnUnCheckAll_Click(object sender, EventArgs e) {
      ToggleGridViewCheckBoxes(false);
    }

    private void ToggleGridViewCheckBoxes(bool p) {
      try {
        int cnt = dgvwTrackPermissions.Rows.Count;

        for (int i = 0; i < cnt; i++) {
          dgvwTrackPermissions.Rows[i].Cells[1].Value = p;
        }
      }

      catch (Exception ex) {
        Log(ex);
      }
    }
  }
}
