﻿using System;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using System.Collections;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Utilities;
using GUILibraries.Forms;

namespace CustomerMaintenance.UI {
  public partial class FrmAdjustCustomerCarryoverFigure : SIDForm {
    private readonly double _customerLastWeekCarryover;
    private readonly String _currentCustomerId;
    private readonly Form _customerMaintenanceFrm;

    private readonly ArrayList _dbUpdatableObjects = new ArrayList();

    private struct DbUpdatableObject {
      public String ObjName;
      public String OriginalValue;
    }


    public FrmAdjustCustomerCarryoverFigure(String customerId, double lastWeekCarryover, SIDForm callingForm)
      : base(callingForm.AppModuleInfo) {
      _customerLastWeekCarryover = lastWeekCarryover;
      _currentCustomerId = customerId;
      _customerMaintenanceFrm = callingForm;
      InitializeComponent();
    }

    private void btnCancel_Click(object sender, EventArgs e) {
      Close();
    }

    private void btnOk_Click(object sender, EventArgs e) {

      if (NumberF.IsValidAmount(txtNewCarryoverFigure.Text, true)) {
        var txtB = (TextBox)Controls.Find("txtNewCarryoverFigure", true).FirstOrDefault();

        foreach (DbUpdatableObject obj in _dbUpdatableObjects) {
          if (txtB != null && obj.ObjName == txtB.Name) {
            if (obj.OriginalValue != txtNewCarryoverFigure.Text) {
              try {
                DoDbUpdate(txtNewCarryoverFigure.Text);

              }
              catch (Exception ex) {
                MessageBox.Show(@"Problems updating customer Info: " + ex);
              }
            }
            break;
          }
        }

        Close();

      }
      else {
        MessageBox.Show(@"Is not valid Number");
        txtNewCarryoverFigure.Focus();
      }
    }

    private void DoDbUpdate(String newCarryoverFigure) {

      try {
        using (var cust = new Customers(AppModuleInfo)) {
          cust.UpdateCustomerCarryOver(_currentCustomerId, double.Parse(newCarryoverFigure));
        }

        var txtLastWeekCo = (TextBox)_customerMaintenanceFrm.Controls.Find("txtLastWeekCO", true).FirstOrDefault();
        if (txtLastWeekCo != null)
          txtLastWeekCo.Text = String.Format("{0:###,###}", double.Parse(newCarryoverFigure));
        using (var lw = new LogWriter(AppModuleInfo)) {
          lw.WriteToCuAccessLog("Customer Maintenance", "Changed " + _currentCustomerId, "Carry Over Amount Changed - " + String.Format("{0:n0}", (double.Parse(newCarryoverFigure))));
        }
      }
      catch (EntityException ex) {
        Log(ex);
      }

    }

    private void frmAdjustCustomerCarryoverFigure_Load(object sender, EventArgs e) {
      txtNewCarryoverFigure.Text = _customerLastWeekCarryover.ToString("N2");

      var dbUpdatableObjectVar = new DbUpdatableObject {
        ObjName = txtNewCarryoverFigure.Name,
        OriginalValue = _customerLastWeekCarryover.ToString(CultureInfo.InvariantCulture)
      };
      _dbUpdatableObjects.Add(dbUpdatableObjectVar);
      txtNewCarryoverFigure.Focus();
    }

    private void numberAmount_KeyPress(object sender, KeyPressEventArgs e) {
      int ascii = Convert.ToInt16(e.KeyChar);
      if ((ascii >= 48 && ascii <= 57) || (ascii == 8)) {
        e.Handled = false;
      }
      else {
        e.Handled = true;
      }
    }
  }
}