﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using GUILibraries.Utilities;
using SIDLibraries.BusinessLayer;
using GUILibraries.Forms;

namespace CustomerMaintenance.UI {
  public partial class FrmCustomerCoverCreditCardDeposit : SIDForm {
    private readonly String _currentCustomerId;
    private readonly String _typedDebitAmount;

    private readonly FrmCustomerTransactionDetails _parentFrm;

    public FrmCustomerCoverCreditCardDeposit(String customerId, SIDForm callerFrm, String debitAmount)
      : base(callerFrm.AppModuleInfo) {
      _currentCustomerId = customerId;
      _parentFrm = (FrmCustomerTransactionDetails)callerFrm;
      _typedDebitAmount = debitAmount;
      InitializeComponent();
    }

    private void btnCancel_Click(object sender, EventArgs e) {
      ShowHideCoverLabels(false, 0, 0);
      Close();
    }

    private void cmbStartingCCDepositsDate_SelectedIndexChanged(object sender, EventArgs e) {
      var idx = int.Parse(((ComboBox)sender).SelectedItem.ToString());
      GetCustomerCreditCardDepositsByDate(idx);
    }

    private void GetCustomerCreditCardDepositsByDate(int selectedItem) {

      var startingTranDateTime = new DateTime(ServerDateTime.Year, ServerDateTime.Month, ServerDateTime.Day, 0, 0, 0);

      startingTranDateTime = startingTranDateTime.AddDays((-1) * selectedItem);

      using (var tran = new Transactions(AppModuleInfo)) {
        var ccDeposits = tran.GetCustomerCreditCardDepositsByDate(_currentCustomerId, startingTranDateTime).ToList();

        if (ccDeposits.Count <= 0) return;
        WriteCreditCardDepositsDgvwHeaders();

        foreach (var res in ccDeposits) {
          var amount = double.Parse(res.Amount.ToString()) / 100;
          dgvwCreditCardDeposits.Rows.Add(res.PaymentBy ?? "".Trim(), res.CCNumber ?? "".Trim(), amount.ToString("N0"), res.DocumentNumber, res.CCExpires);
        }

        FormF.PreventDataGridViewColumnsToBeSortedAndResized(dgvwCreditCardDeposits);

        dgvwCreditCardDeposits.ClearSelection();
        dgvwCreditCardDeposits.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
      }

    }

    private void WriteCreditCardDepositsDgvwHeaders() {
      dgvwCreditCardDeposits.DataSource = null;

      if (dgvwCreditCardDeposits.Rows.Count > 0) {
        dgvwCreditCardDeposits.Rows.Clear();
      }

      var titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Type", Width = 80 };
      dgvwCreditCardDeposits.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Number", Width = 150 };
      dgvwCreditCardDeposits.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Amount", Width = 75 };
      dgvwCreditCardDeposits.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"DocNumber", Width = 75, Visible = false };
      dgvwCreditCardDeposits.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"CCExpires", Width = 75, Visible = false };
      dgvwCreditCardDeposits.Columns.Add(titleColumn);
    }

    private void btnOk_Click(object sender, EventArgs e) {
      var frm = _parentFrm;

      frm.DebitCoverers = new List<FrmCustomerTransactionDetails.DebitCoverer>();

      var holdAmount = double.Parse(_typedDebitAmount);
      double coverAmountSum = 0;
      var showCoverLabels = false;

      foreach (DataGridViewRow r in dgvwCreditCardDeposits.SelectedRows) {
        if (holdAmount > 0) {
          var coverer = new FrmCustomerTransactionDetails.DebitCoverer {
            TranMethod = r.Cells[0].Value.ToString(),
            CcNumber = r.Cells[1].Value.ToString(),
            OriginalAmount = r.Cells[2].Value.ToString(),
            DocumentNumber = r.Cells[3].Value.ToString(),
            CcExpires = r.Cells[4].Value.ToString()
          };

          var coverAmount = holdAmount - double.Parse(r.Cells[2].Value.ToString());

          if (coverAmount > 0) {
            coverer.CoverAmount = r.Cells[2].Value.ToString();
            coverAmountSum += double.Parse(r.Cells[2].Value.ToString());
          }
          else {
            coverer.CoverAmount = holdAmount.ToString(CultureInfo.InvariantCulture);
            coverAmountSum += holdAmount;
          }

          holdAmount -= double.Parse(r.Cells[2].Value.ToString());


          frm.DebitCoverers.Add(coverer);
        }
        showCoverLabels = true;

      }

      ShowHideCoverLabels(showCoverLabels, coverAmountSum, holdAmount);

      Close();
    }

    private void ShowHideCoverLabels(Boolean showCoverLabels, double coverAmount, double nonCoveredAmount) {

      var frm = _parentFrm;

      var lbl = (Label)frm.Controls.Find("lblCovered", true).FirstOrDefault();
      if (lbl != null) lbl.Visible = showCoverLabels;

      lbl = (Label)frm.Controls.Find("lblCoveredAmt", true).FirstOrDefault();
      if (lbl != null) {
        if (showCoverLabels) {
          lbl.Visible = true;
          lbl.Text = String.Format("{0:###,###}", coverAmount);
        }
        else {
          lbl.Visible = false;
        }
      }


      lbl = (Label)frm.Controls.Find("lblExcess", true).FirstOrDefault();
      if (lbl != null) lbl.Visible = showCoverLabels;

      lbl = (Label)frm.Controls.Find("lblExcessAmt", true).FirstOrDefault();
      if (lbl != null) {
        if (showCoverLabels) {
          lbl.Visible = true;
          lbl.Text = nonCoveredAmount > 0 ? String.Format("{0:###,###}", nonCoveredAmount) : "0";
        }
        else {
          lbl.Visible = false;
        }
      }
    }


  }
}