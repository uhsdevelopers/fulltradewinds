﻿namespace CustomerMaintenance.UI
{
    partial class FrmCustomerNewFreePlay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.btnOk = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.lblDateTime = new System.Windows.Forms.Label();
      this.txtTranDateTime = new System.Windows.Forms.TextBox();
      this.lblTransactionType = new System.Windows.Forms.Label();
      this.cmbTransactionType = new System.Windows.Forms.ComboBox();
      this.lblAmount = new System.Windows.Forms.Label();
      this.txtDescription = new System.Windows.Forms.TextBox();
      this.lblDescription = new System.Windows.Forms.Label();
      this.txtAmount = new GUILibraries.Controls.NumberTextBox();
      this.SuspendLayout();
      // 
      // btnOk
      // 
      this.btnOk.Location = new System.Drawing.Point(28, 309);
      this.btnOk.Name = "btnOk";
      this.btnOk.Size = new System.Drawing.Size(75, 23);
      this.btnOk.TabIndex = 90;
      this.btnOk.Text = "Ok";
      this.btnOk.UseVisualStyleBackColor = true;
      this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.Location = new System.Drawing.Point(136, 309);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 100;
      this.btnCancel.Text = "Cancel";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // lblDateTime
      // 
      this.lblDateTime.AutoSize = true;
      this.lblDateTime.Location = new System.Drawing.Point(41, 25);
      this.lblDateTime.Name = "lblDateTime";
      this.lblDateTime.Size = new System.Drawing.Size(61, 13);
      this.lblDateTime.TabIndex = 10;
      this.lblDateTime.Text = "Date/Time:";
      // 
      // txtTranDateTime
      // 
      this.txtTranDateTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.txtTranDateTime.Enabled = false;
      this.txtTranDateTime.Location = new System.Drawing.Point(119, 23);
      this.txtTranDateTime.Name = "txtTranDateTime";
      this.txtTranDateTime.ReadOnly = true;
      this.txtTranDateTime.Size = new System.Drawing.Size(124, 20);
      this.txtTranDateTime.TabIndex = 20;
      // 
      // lblTransactionType
      // 
      this.lblTransactionType.AutoSize = true;
      this.lblTransactionType.Location = new System.Drawing.Point(9, 66);
      this.lblTransactionType.Name = "lblTransactionType";
      this.lblTransactionType.Size = new System.Drawing.Size(93, 13);
      this.lblTransactionType.TabIndex = 30;
      this.lblTransactionType.Text = "Transaction Type:";
      this.lblTransactionType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // cmbTransactionType
      // 
      this.cmbTransactionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbTransactionType.FormattingEnabled = true;
      this.cmbTransactionType.Items.AddRange(new object[] {
            "Deposit",
            "Withdrawal"});
      this.cmbTransactionType.Location = new System.Drawing.Point(119, 63);
      this.cmbTransactionType.Name = "cmbTransactionType";
      this.cmbTransactionType.Size = new System.Drawing.Size(124, 21);
      this.cmbTransactionType.TabIndex = 40;
      this.cmbTransactionType.SelectedIndexChanged += new System.EventHandler(this.cmbTransactionType_SelectedIndexChanged);
      // 
      // lblAmount
      // 
      this.lblAmount.AutoSize = true;
      this.lblAmount.Location = new System.Drawing.Point(54, 108);
      this.lblAmount.Name = "lblAmount";
      this.lblAmount.Size = new System.Drawing.Size(46, 13);
      this.lblAmount.TabIndex = 50;
      this.lblAmount.Text = "Amount:";
      this.lblAmount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txtDescription
      // 
      this.txtDescription.Location = new System.Drawing.Point(28, 160);
      this.txtDescription.Multiline = true;
      this.txtDescription.Name = "txtDescription";
      this.txtDescription.Size = new System.Drawing.Size(215, 123);
      this.txtDescription.TabIndex = 80;
      // 
      // lblDescription
      // 
      this.lblDescription.AutoSize = true;
      this.lblDescription.Location = new System.Drawing.Point(25, 134);
      this.lblDescription.Name = "lblDescription";
      this.lblDescription.Size = new System.Drawing.Size(63, 13);
      this.lblDescription.TabIndex = 70;
      this.lblDescription.Text = "Description:";
      this.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txtAmount
      // 
      this.txtAmount.AllowCents = true;
      this.txtAmount.DividedBy = 1;
      this.txtAmount.DivideFlag = false;
      this.txtAmount.Location = new System.Drawing.Point(117, 103);
      this.txtAmount.Name = "txtAmount";
      this.txtAmount.ShowIfZero = true;
      this.txtAmount.Size = new System.Drawing.Size(124, 20);
      this.txtAmount.TabIndex = 60;
      this.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.txtAmount.Leave += new System.EventHandler(this.numberToText_Leave);
      // 
      // FrmCustomerNewFreePlay
      // 
      this.AcceptButton = this.btnOk;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(261, 368);
      this.Controls.Add(this.txtAmount);
      this.Controls.Add(this.lblDescription);
      this.Controls.Add(this.txtDescription);
      this.Controls.Add(this.lblAmount);
      this.Controls.Add(this.cmbTransactionType);
      this.Controls.Add(this.lblTransactionType);
      this.Controls.Add(this.txtTranDateTime);
      this.Controls.Add(this.lblDateTime);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.btnOk);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "FrmCustomerNewFreePlay";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Free Play";
      this.Load += new System.EventHandler(this.frmCustomerNewFreePlay_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblDateTime;
        private System.Windows.Forms.TextBox txtTranDateTime;
        private System.Windows.Forms.Label lblTransactionType;
        private System.Windows.Forms.ComboBox cmbTransactionType;
        private System.Windows.Forms.Label lblAmount;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label lblDescription;
        private GUILibraries.Controls.NumberTextBox txtAmount;
    }
}