﻿namespace CustomerMaintenance.UI
{
    partial class FrmCustomerAuditDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chbShowArchiveData = new System.Windows.Forms.CheckBox();
            this.dgvwAuditInfo = new System.Windows.Forms.DataGridView();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblWagerDetails = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvwAuditInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // chbShowArchiveData
            // 
            this.chbShowArchiveData.AutoSize = true;
            this.chbShowArchiveData.Location = new System.Drawing.Point(13, 7);
            this.chbShowArchiveData.Name = "chbShowArchiveData";
            this.chbShowArchiveData.Size = new System.Drawing.Size(124, 17);
            this.chbShowArchiveData.TabIndex = 0;
            this.chbShowArchiveData.Text = "Show Archived Data";
            this.chbShowArchiveData.UseVisualStyleBackColor = true;
            this.chbShowArchiveData.CheckedChanged += new System.EventHandler(this.chbShowArchiveData_CheckedChanged);
            // 
            // dgvwAuditInfo
            // 
            this.dgvwAuditInfo.AllowUserToAddRows = false;
            this.dgvwAuditInfo.AllowUserToDeleteRows = false;
            this.dgvwAuditInfo.AllowUserToResizeRows = false;
            this.dgvwAuditInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvwAuditInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvwAuditInfo.Location = new System.Drawing.Point(13, 46);
            this.dgvwAuditInfo.Name = "dgvwAuditInfo";
            this.dgvwAuditInfo.ReadOnly = true;
            this.dgvwAuditInfo.RowHeadersVisible = false;
            this.dgvwAuditInfo.Size = new System.Drawing.Size(348, 204);
            this.dgvwAuditInfo.TabIndex = 1;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(153, 257);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblWagerDetails
            // 
            this.lblWagerDetails.AutoSize = true;
            this.lblWagerDetails.Location = new System.Drawing.Point(17, 27);
            this.lblWagerDetails.Name = "lblWagerDetails";
            this.lblWagerDetails.Size = new System.Drawing.Size(0, 13);
            this.lblWagerDetails.TabIndex = 3;
            // 
            // FrmCustomerAuditDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(373, 292);
            this.Controls.Add(this.lblWagerDetails);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.dgvwAuditInfo);
            this.Controls.Add(this.chbShowArchiveData);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCustomerAuditDetails";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Customer Audit Details - ";
            this.Load += new System.EventHandler(this.frmCustomerAuditDetails_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvwAuditInfo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chbShowArchiveData;
        private System.Windows.Forms.DataGridView dgvwAuditInfo;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblWagerDetails;
    }
}