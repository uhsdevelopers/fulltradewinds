﻿namespace CustomerMaintenance.UI
{
    partial class FrmSqlDependencyTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpUpdate = new System.Windows.Forms.GroupBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.txtCreditLimit = new GUILibraries.Controls.NumberTextBox();
            this.lblCreditLimit = new System.Windows.Forms.Label();
            this.grpCurrent = new System.Windows.Forms.GroupBox();
            this.lblUpToDateCreditLimit = new System.Windows.Forms.Label();
            this.lblCurrentCreditLimit = new System.Windows.Forms.Label();
            this.grpUpdate.SuspendLayout();
            this.grpCurrent.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpUpdate
            // 
            this.grpUpdate.Controls.Add(this.btnUpdate);
            this.grpUpdate.Controls.Add(this.txtCreditLimit);
            this.grpUpdate.Controls.Add(this.lblCreditLimit);
            this.grpUpdate.Location = new System.Drawing.Point(25, 8);
            this.grpUpdate.Name = "grpUpdate";
            this.grpUpdate.Size = new System.Drawing.Size(265, 100);
            this.grpUpdate.TabIndex = 0;
            this.grpUpdate.TabStop = false;
            this.grpUpdate.Text = "Update";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(182, 66);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 2;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // txtCreditLimit
            // 
            this.txtCreditLimit.AllowCents = false;
            this.txtCreditLimit.AllowNegatives = false;
            this.txtCreditLimit.DividedBy = 100;
            this.txtCreditLimit.DivideFlag = true;
            this.txtCreditLimit.Location = new System.Drawing.Point(90, 28);
            this.txtCreditLimit.Name = "txtCreditLimit";
            this.txtCreditLimit.ShowIfZero = true;
            this.txtCreditLimit.Size = new System.Drawing.Size(100, 20);
            this.txtCreditLimit.TabIndex = 1;
            // 
            // lblCreditLimit
            // 
            this.lblCreditLimit.AutoSize = true;
            this.lblCreditLimit.Location = new System.Drawing.Point(23, 31);
            this.lblCreditLimit.Name = "lblCreditLimit";
            this.lblCreditLimit.Size = new System.Drawing.Size(61, 13);
            this.lblCreditLimit.TabIndex = 0;
            this.lblCreditLimit.Text = "Credit Limit:";
            // 
            // grpCurrent
            // 
            this.grpCurrent.Controls.Add(this.lblUpToDateCreditLimit);
            this.grpCurrent.Controls.Add(this.lblCurrentCreditLimit);
            this.grpCurrent.Location = new System.Drawing.Point(25, 143);
            this.grpCurrent.Name = "grpCurrent";
            this.grpCurrent.Size = new System.Drawing.Size(265, 100);
            this.grpCurrent.TabIndex = 1;
            this.grpCurrent.TabStop = false;
            this.grpCurrent.Text = "Up To Date";
            // 
            // lblUpToDateCreditLimit
            // 
            this.lblUpToDateCreditLimit.AutoSize = true;
            this.lblUpToDateCreditLimit.Location = new System.Drawing.Point(23, 40);
            this.lblUpToDateCreditLimit.Name = "lblUpToDateCreditLimit";
            this.lblUpToDateCreditLimit.Size = new System.Drawing.Size(61, 13);
            this.lblUpToDateCreditLimit.TabIndex = 2;
            this.lblUpToDateCreditLimit.Text = "Credit Limit:";
            // 
            // lblCurrentCreditLimit
            // 
            this.lblCurrentCreditLimit.AutoSize = true;
            this.lblCurrentCreditLimit.Location = new System.Drawing.Point(112, 40);
            this.lblCurrentCreditLimit.Name = "lblCurrentCreditLimit";
            this.lblCurrentCreditLimit.Size = new System.Drawing.Size(55, 13);
            this.lblCurrentCreditLimit.TabIndex = 1;
            this.lblCurrentCreditLimit.Text = "CreditLimit";
            // 
            // FrmSqlDependencyTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(506, 314);
            this.Controls.Add(this.grpCurrent);
            this.Controls.Add(this.grpUpdate);
            this.Name = "FrmSqlDependencyTest";
            this.Text = "Sql Dependency Test";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmSqlDependencyTest_FormClosed);
            this.Load += new System.EventHandler(this.FrmSqlDependencyTest_Load);
            this.grpUpdate.ResumeLayout(false);
            this.grpUpdate.PerformLayout();
            this.grpCurrent.ResumeLayout(false);
            this.grpCurrent.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpUpdate;
        private System.Windows.Forms.Button btnUpdate;
        private GUILibraries.Controls.NumberTextBox txtCreditLimit;
        private System.Windows.Forms.Label lblCreditLimit;
        private System.Windows.Forms.GroupBox grpCurrent;
        private System.Windows.Forms.Label lblUpToDateCreditLimit;
        private System.Windows.Forms.Label lblCurrentCreditLimit;
    }
}