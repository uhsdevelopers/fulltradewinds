﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;

namespace CustomerMaintenance.Utilities {
  public static class Common {
    public static void ClearControlContent(Control childCtrl) {
      if (childCtrl.GetType().ToString() == "System.Windows.Forms.DataGridView") {
        var gv = (DataGridView)childCtrl;
        gv.DataSource = null;
        gv.Rows.Clear();
        gv.RowHeadersVisible = false;
      }

      if (childCtrl.GetType().ToString() == "System.Windows.Forms.Label") {
        var lbl = (Label)childCtrl;
        lbl.Text = "";
        lbl.Visible = false;
      }

      if (childCtrl.GetType().ToString() == "System.Windows.Forms.TextBox") {
        var txt = (TextBox)childCtrl;
        txt.Text = "";
        txt.Visible = false;
      }

      if (childCtrl.GetType().ToString() == "System.Windows.Forms.ComboBox") {
        var cmb = (ComboBox)childCtrl;
        cmb.SelectedIndex = -1;
      }
    }

    public static void ClearAdditionalControlContent(Control child1Ctrl) {
      if (child1Ctrl.GetType().ToString() == "System.Windows.Forms.DataGridView") {
        var gv = (DataGridView)child1Ctrl;
        gv.CancelEdit();
        gv.DataSource = null;
        if (gv.CurrentRow != null) gv.Rows.Remove(gv.CurrentRow);
        gv.Rows.Clear();
        gv.RowHeadersVisible = false;
      }

      if (child1Ctrl.GetType().ToString() == "System.Windows.Forms.TextBox") {
        var txB = (TextBox)child1Ctrl;
        txB.Text = "";
      }

      if (child1Ctrl.GetType().ToString() == "System.Windows.Forms.CheckBox") {
        var chB = (CheckBox)child1Ctrl;
        chB.Checked = false;
      }

      if (child1Ctrl.GetType().ToString() != "System.Windows.Forms.ComboBox") return;
      var cmb = (ComboBox)child1Ctrl;
      cmb.SelectedIndex = -1;
    }

    public static void EnableDisableToolStripItem(List<spULPGetCurrentUserPermissions_Result> currentUserPermissions, ContextMenuStrip ctxMenuStrip) {
      foreach (ToolStripItem item in ctxMenuStrip.Items) {
        switch (item.AccessibleName) {
          case @"0":
            item.Enabled = LoginsAndProfiles.ValidateUserFunctionalityAccess(currentUserPermissions, LoginsAndProfiles.ENTER_CUSTOMER_TRANSACTIONS);
            break;
          case @"1":
            item.Enabled = false;
            break;
          case @"2":
            item.Enabled = false;
            break;
        }
      }
    }

    public static void GetWagerLimitsSports(ModuleInfo appModuleInfo, ComboBox cmbWLSports, List<spCstGetCustomerWagerLimits_Result> wagerLimits, bool customerTempLimitsExtIsActive, string wagerType = null) {
      List<spSptGetSportTypes_Result> spts;
      using (var spt = new SportTypes(appModuleInfo))
      {
          spts = spt.Get();

          if (wagerType == "T")
          {
              List<spSptGetSportTypes_Result> spts0;
              List<spSptGetTeaserSpecs_Result> tSpecs;
              spts = new List<spSptGetSportTypes_Result>();
              spts0 = spt.Get();
              tSpecs = spt.GetTeaserSpecs();
              foreach (spSptGetSportTypes_Result item in spts0)
              {
                  if (!tSpecs.Any(i => i.SportType.Trim() == item.SportType.Trim() && i.SportSubType.Trim() == item.SportSubType.Trim()))
                      continue;
                  spts.Add(item);
              }
          }
      }

      var sptArr = (from s in spts
                    orderby s.SequenceNumber ascending
                    select s.SportType.Trim()).Distinct().ToList();
      cmbWLSports.DrawMode = DrawMode.OwnerDrawVariable;
      cmbWLSports.DrawItem += (sender, e) => cmbWLSports_DrawItem(sender, e, wagerLimits, customerTempLimitsExtIsActive);
      sptArr.Insert(0, "Select One Sport");
      //sptArr.Insert(sptArr.Count, SportTypes.TENNIS);
      //sptArr.Insert(sptArr.Count, SportTypes.GOLF);

      cmbWLSports.DataSource = sptArr;
      cmbWLSports.SelectedIndex = -1;
    }

    private static void cmbWLSports_DrawItem(object sender, DrawItemEventArgs e, List<spCstGetCustomerWagerLimits_Result> wagerLimits, bool customerTempLimitsExtIsActive) {
      if (e.Index == -1) return;
      var combo = ((ComboBox)sender);
      using (var brush = new SolidBrush(e.ForeColor)) {
        var font = e.Font;

        if (e.Index > 0 && CheckIfItemIsBold(combo.Items[e.Index].ToString(), wagerLimits, customerTempLimitsExtIsActive)) {
          font = new Font(font, FontStyle.Bold);
        }
        else {
          font = new Font(font, FontStyle.Regular);
        }
        e.DrawBackground();
        e.Graphics.DrawString(combo.Items[e.Index].ToString(), font, brush, e.Bounds);
        e.DrawFocusRectangle();
      }
    }

    private static bool CheckIfItemIsBold(string itemDesc, List<spCstGetCustomerWagerLimits_Result> wagerLimits, bool customerTempLimitsExtIsActive) {

      var isBold = false;

      if (wagerLimits == null) return false;
      foreach (spCstGetCustomerWagerLimits_Result wl in wagerLimits) {
        if (wl.sportType.Trim() != itemDesc.Trim()) continue;
        if ((!(wl.CuLimit > 0) && !(wl.InetLimit > 0)) && (!customerTempLimitsExtIsActive || (!(wl.TempCuLimit > 0) && !(wl.TempInetLimit > 0)))) continue;
        isBold = true;
        break;
      }

      return isBold;
    }
  }
}
