﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using SIDLibraries.Utilities;

namespace GUILibraries {

  public class AppLoader : IDisposable {

    private const int DEFAULT_INSTACE = 1;
    private int ApplicationId { get; set; }
    private DateTime UserAccessDateTime { get; set; }
    private int SystemId { get; set; }
    private Boolean TimerStopped { get; set; }
    private System.Windows.Forms.Timer AppsInMaintenanceChecker { get; set; }
    private int _instanceId;
    private bool _setApplicationInUse = true;
    private bool _updateMessageSent;

    public InstanceManager.BusinessLayer.InstanceManager InstanceManager { get; private set; }
    public List<spULPGetCurrentUserPermissions_Result> UserPermissions { get; private set; }
    public spULPGetLoginInUse_Result LoginInUse { get; private set; }

    public AppLoader(InstanceManager.BusinessLayer.InstanceManager.Applications appId, string[] args, String systemId) {
      try {
      PrepareGuiEnvironment();

      ApplicationId = (int)appId;
      _instanceId = DEFAULT_INSTACE;

      int sysId;
      int.TryParse(systemId, out sysId);
      SystemId = sysId;

      SetInstanceId(args);
      InstanceManager = new InstanceManager.BusinessLayer.InstanceManager(ApplicationId, _instanceId, _setApplicationInUse);
      ValidateProgramVersion(InstanceManager.InstanceModuleInfo);

      PrepareUserInfo();

      SetAppExceptionHandler();

      CheckForAppInMaintenanceMode();
    }
      catch (Exception ex) {
        SystemLog.Log(ex);
      }
    }

    private static void ValidateProgramVersion(ModuleInfo moduleInfo) {
        var baseDirectory = AppDomain.CurrentDomain.BaseDirectory.Substring(0, AppDomain.CurrentDomain.BaseDirectory.Length - 1);
        if (Debugger.IsAttached || String.Equals(baseDirectory, moduleInfo.ActiveSharedFolder, StringComparison.CurrentCultureIgnoreCase)) return;
        Process.Start(moduleInfo.ActiveSharedFolder + @"\\" + moduleInfo.ExeName.Replace(".exe", "") + ".exe");
        Process.GetCurrentProcess().Kill();
    }

    private void SetAppExceptionHandler() {
      Application.ThreadException += UnhandledThreadExceptionHandler;
      Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
      AppDomain.CurrentDomain.UnhandledException += UnhandledException;
    }

    private void UnhandledThreadExceptionHandler(object sender, ThreadExceptionEventArgs e) {
      Log(e.Exception);
    }

    private void UnhandledException(object sender, UnhandledExceptionEventArgs e) {
      Log((Exception)e.ExceptionObject);
    }

    private void Log(Exception ex) {
      if (InstanceManager.InstanceModuleInfo == null) {
        SystemLog.Log(ex);
        return;
      }
      using (var lg = new LogWriter(InstanceManager.InstanceModuleInfo)) {
        lg.WriteToSystemLog(ex);
      }
    }

    private void CheckForAppInMaintenanceMode() {
      AppsInMaintenanceChecker = new System.Windows.Forms.Timer { Interval = 1000 * 20 };
      AppsInMaintenanceChecker.Tick += AppsInMaintenanceChecker_Tick;
      AppsInMaintenanceChecker.Start();
    }

    private void AppsInMaintenanceChecker_Tick(object sender, EventArgs e) {
      if (!TimerStopped) {
        HandleTimerTick();
      }
    }

    private void HandleTimerTick() {
      try {
        if (_updateMessageSent) {
          AppsInMaintenanceChecker.Enabled = false;
          return;
        }
        using (var im = new InstanceManager.BusinessLayer.InstanceManager()) {
          var instances = im.GetInstancesList(SystemId);
          if (instances == null) return;
          var instance = instances.FirstOrDefault(i => i.InstanceId == _instanceId);
          if (instance == null || String.IsNullOrEmpty(instance.AppsPath) ||
              InstanceManager == null || InstanceManager.InstanceModuleInfo == null ||
              String.IsNullOrEmpty(InstanceManager.InstanceModuleInfo.ActiveSharedFolder)) return;
          if (instance.AppsPath == InstanceManager.InstanceModuleInfo.ActiveSharedFolder) return;
          _updateMessageSent = true;
          MessageBox.Show(@"There is a new version available, please restart the ALL SID modules to get the update.");
        }
      }
      catch (Exception ex) {
        Log(ex);
      }
    }

    public void Dispose() {
    }

    private static void PrepareGuiEnvironment() {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
    }

    private void PrepareUserInfo() {
      try {
        UserAccessDateTime = InstanceManager.InstanceModuleInfo.ServerDateTime;
        using (var logins = new LoginsAndProfiles(InstanceManager.InstanceModuleInfo)) {
          UserPermissions = logins.GetCurrentUserPermissions();
        }
      }
      catch (Exception ex) {
        Log(ex);
      }
    }

    private void SetInstanceId(string[] args) {
      try {
        var instanceToUse = ConfigurationManager.AppSettings["DefaultInstance"];
        if (args != null && args.Length > 0) {
          var setAppStr = args[0];
          if (setAppStr != "") {
            _setApplicationInUse = (setAppStr != "0");
          }
          instanceToUse = args[1];
        }
        int.TryParse(instanceToUse, out _instanceId);
      }
      catch (Exception ex) {
        Log(ex);
      }
    }

    public bool UserHasPermission(string permissionName = "") {
      if (string.IsNullOrEmpty(permissionName)) return true;

      using (var logins = new LoginsAndProfiles(InstanceManager.InstanceModuleInfo)) {
        if (UserPermissions.Any()) {
          LoginInUse = logins.InitializeCurrentLoginInUseInfo(UserPermissions, permissionName, InstanceManager.InstanceModuleInfo.ShortName, UserAccessDateTime);
        }

        LoginsAndProfiles.ValidateUserFunctionalityAccess(UserPermissions, permissionName);
        if (logins.ValidateCurrentUserAccess(InstanceManager.InstanceModuleInfo.ShortName, permissionName)) return true;
        MessageBox.Show(Environment.UserName + @" does not have permissions to " + permissionName);
        return false;
      }
    }

    public bool IsSafeToStart() {
      try {
        LicenseManager.Validate(typeof(AppLoader));

        bool createdNew;
        if (!InstanceManager.InstanceModuleInfo.InstanceInfo.IsProduction) return true;
        using (var mutex = new Mutex(false, InstanceManager.InstanceModuleInfo.ExeName, out createdNew)) {
          if (mutex.WaitOne(0, false) && Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName).Length <= 1) return true;
          MessageBox.Show(@"Only one instance of the " + InstanceManager.InstanceModuleInfo.Description + @" Module may be connected to " + InstanceManager.InstanceModuleInfo.InstanceInfo.DatabaseName + @" at a time.");
          return false;
        }
      }
      catch (Exception ex) {
        Log(ex);
        return false;
      }
    }

    public void StartApp(Form formToStart) {
      try {
        if (formToStart == null || formToStart.IsDisposed) return;
        CheckForAppInMaintenanceMode();
        Application.Run(formToStart);
      }
      catch (Exception ex) {
        Log(ex);
      }
    }

    public void KillApp() {
      try {
        InstanceManager.DeleteAppInUse(InstanceManager.ApplicationInUseRecordId);
      }
      catch (Exception ex) {
        Log(ex);
      }
    }

  }
}