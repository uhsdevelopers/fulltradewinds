﻿using System;
using System.Collections;
using System.Windows.Forms;

namespace GUILibraries.Forms {
  public partial class FrmLogViewer : SIDForm {

    private readonly Form _frmParent;

    private ArrayList _logs;

    public ArrayList Logs {
      private get { return _logs; }
      set {
        _logs = value;
        ShowLogs();
      }
    }

    public FrmLogViewer(Form frmParent) {
      InitializeComponent();
      _frmParent = frmParent;
    }

    private void btnExit_Click(object sender, EventArgs e) {
      Close();
    }

    private void frmLogViewer_Load(object sender, EventArgs e) {
      Text = @"LogViewer - " + _frmParent.Name;
      ShowLogs();
    }

    private void btnReload_Click(object sender, EventArgs e) {
      ReloadAndShowLogs();
    }

    private void ReloadAndShowLogs() {
      ((SIDForm)_frmParent).AssignLogToViewer();
      ShowLogs();
    }

    delegate void ShowLogsCallback();
    private void ShowLogs() {
      if (InvokeRequired) {
        var d = new ShowLogsCallback(ShowLogs);
        Invoke(d);
        return;
      }
      try {
        lbLog.Items.Clear();
        if (Logs != null && Logs.Count > 0) {
          foreach (string log in Logs) {
            lbLog.Items.Add(log);
          }
        }
        lbLog.SelectedIndex = lbLog.Items.Count - 1;
      }
      catch { }
    }

    private void frmLogViewer_KeyDown(object sender, KeyEventArgs e) {
      switch (e.KeyCode) {
        case Keys.Enter:
          ReloadAndShowLogs();
          break;
        case Keys.Escape:
          Close();
          break;
      }
    }

    private void button1_Click(object sender, EventArgs e) {
      Logs.Clear();
      lbLog.Items.Clear();
    }
  }
}
