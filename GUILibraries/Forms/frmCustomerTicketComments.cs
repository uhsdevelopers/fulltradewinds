﻿using System;
using System.Linq;
using System.Windows.Forms;
using InstanceManager.BusinessLayer;
using SIDLibraries.Entities;
using SIDLibraries.BusinessLayer;

namespace GUILibraries.Forms {
  public partial class FrmCustomerTicketComments : SIDForm {
    private readonly int _currentTicketNumber;

    private String _startingWagerDiscrepancy = "";
    private String _startingWagerComments = "";

    public FrmCustomerTicketComments(int ticketNumber, ModuleInfo moduleInfo) : base (moduleInfo) {
      if (ticketNumber > 0) {
        _currentTicketNumber = ticketNumber;
        InitializeComponent();
      }
      else {
        MessageBox.Show("TicketNumber was not provided");
        Close();
      }
    }

    private void frmCustomerTicketComments_Load(object sender, EventArgs e) {
      GetCustomerTicketComments(out _startingWagerDiscrepancy, out _startingWagerComments);
    }

    private void GetCustomerTicketComments(out String startingWagerDiscrepancy, out String startingWagerComments) {
      startingWagerDiscrepancy = "N";
      startingWagerComments = "";
      spTkWGetCustomerTicketComments_Result ticketComments;

      using (var tAw = new TicketsAndWagers(AppModuleInfo))
        ticketComments = tAw.GetCustomerTicketComments(_currentTicketNumber).FirstOrDefault();

      if (ticketComments == null) return;
      chbTicketDiscrepancy.Checked = (ticketComments.DiscrepancyFlag == "Y");
      startingWagerDiscrepancy = ticketComments.DiscrepancyFlag;

      txtTicketComments.Text = ticketComments.Comments;
      startingWagerComments = ticketComments.Comments;
    }

    private void tbnTicketCommentsCancel_Click(object sender, EventArgs e) {
      Close();
    }

    private void tbnTicketCommentsOk_Click(object sender, EventArgs e) {
      var finalWagerDiscrepancy = chbTicketDiscrepancy.Checked ? "Y" : "N";
      var finalWagerComments = txtTicketComments.Text.Trim();

      if (_startingWagerDiscrepancy != finalWagerDiscrepancy || _startingWagerComments.Trim() != finalWagerComments) {
        using (var tAw = new TicketsAndWagers(AppModuleInfo)) {
          tAw.UpdateTicketComments(_currentTicketNumber, finalWagerComments, finalWagerDiscrepancy);
        }
      }
      Close();
    }

  }
}