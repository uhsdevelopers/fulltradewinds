﻿namespace GUILibraries.Forms
{
    partial class FrmCustomerWagerComments
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbnWagerCommentsCancel = new System.Windows.Forms.Button();
            this.tbnWagerCommentsOk = new System.Windows.Forms.Button();
            this.grpWagerComments = new System.Windows.Forms.GroupBox();
            this.txtWagerComments = new System.Windows.Forms.TextBox();
            this.chbWagerDiscrepancy = new System.Windows.Forms.CheckBox();
            this.grpWagerComments.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbnWagerCommentsCancel
            // 
            this.tbnWagerCommentsCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.tbnWagerCommentsCancel.Location = new System.Drawing.Point(255, 269);
            this.tbnWagerCommentsCancel.Name = "tbnWagerCommentsCancel";
            this.tbnWagerCommentsCancel.Size = new System.Drawing.Size(75, 23);
            this.tbnWagerCommentsCancel.TabIndex = 50;
            this.tbnWagerCommentsCancel.Text = "Cancel";
            this.tbnWagerCommentsCancel.UseVisualStyleBackColor = true;
            this.tbnWagerCommentsCancel.Click += new System.EventHandler(this.tbnWagerCommentsCancel_Click);
            // 
            // tbnWagerCommentsOk
            // 
            this.tbnWagerCommentsOk.Location = new System.Drawing.Point(157, 269);
            this.tbnWagerCommentsOk.Name = "tbnWagerCommentsOk";
            this.tbnWagerCommentsOk.Size = new System.Drawing.Size(75, 23);
            this.tbnWagerCommentsOk.TabIndex = 40;
            this.tbnWagerCommentsOk.Text = "Ok";
            this.tbnWagerCommentsOk.UseVisualStyleBackColor = true;
            this.tbnWagerCommentsOk.Click += new System.EventHandler(this.tbnWagerCommentsOk_Click);
            // 
            // grpWagerComments
            // 
            this.grpWagerComments.Controls.Add(this.txtWagerComments);
            this.grpWagerComments.Location = new System.Drawing.Point(12, 47);
            this.grpWagerComments.Name = "grpWagerComments";
            this.grpWagerComments.Size = new System.Drawing.Size(469, 194);
            this.grpWagerComments.TabIndex = 20;
            this.grpWagerComments.TabStop = false;
            this.grpWagerComments.Text = "Comments";
            // 
            // txtWagerComments
            // 
            this.txtWagerComments.Location = new System.Drawing.Point(7, 20);
            this.txtWagerComments.Multiline = true;
            this.txtWagerComments.Name = "txtWagerComments";
            this.txtWagerComments.Size = new System.Drawing.Size(456, 168);
            this.txtWagerComments.TabIndex = 30;
            // 
            // chbWagerDiscrepancy
            // 
            this.chbWagerDiscrepancy.AutoSize = true;
            this.chbWagerDiscrepancy.Location = new System.Drawing.Point(12, 12);
            this.chbWagerDiscrepancy.Name = "chbWagerDiscrepancy";
            this.chbWagerDiscrepancy.Size = new System.Drawing.Size(85, 17);
            this.chbWagerDiscrepancy.TabIndex = 10;
            this.chbWagerDiscrepancy.Text = "Discrepancy";
            this.chbWagerDiscrepancy.UseVisualStyleBackColor = true;
            // 
            // frmCustomerWagerComments
            // 
            this.AcceptButton = this.tbnWagerCommentsOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.tbnWagerCommentsCancel;
            this.ClientSize = new System.Drawing.Size(513, 368);
            this.Controls.Add(this.tbnWagerCommentsCancel);
            this.Controls.Add(this.tbnWagerCommentsOk);
            this.Controls.Add(this.grpWagerComments);
            this.Controls.Add(this.chbWagerDiscrepancy);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FrmCustomerWagerComments";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Customer Wager Comments";
            this.Load += new System.EventHandler(this.frmCustomerWagerComments_Load);
            this.grpWagerComments.ResumeLayout(false);
            this.grpWagerComments.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button tbnWagerCommentsCancel;
        private System.Windows.Forms.Button tbnWagerCommentsOk;
        private System.Windows.Forms.GroupBox grpWagerComments;
        private System.Windows.Forms.TextBox txtWagerComments;
        private System.Windows.Forms.CheckBox chbWagerDiscrepancy;
    }
}