﻿namespace GUILibraries.Forms
{
    partial class FrmGameScores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtGameScoresHeader = new System.Windows.Forms.TextBox();
            this.dgrvwGameScores = new System.Windows.Forms.DataGridView();
            this.panGameScores = new System.Windows.Forms.Panel();
            this.btnExit = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgrvwGameScores)).BeginInit();
            this.panGameScores.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtGameScoresHeader
            // 
            this.txtGameScoresHeader.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtGameScoresHeader.Enabled = false;
            this.txtGameScoresHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGameScoresHeader.Location = new System.Drawing.Point(-1, 12);
            this.txtGameScoresHeader.Multiline = true;
            this.txtGameScoresHeader.Name = "txtGameScoresHeader";
            this.txtGameScoresHeader.ReadOnly = true;
            this.txtGameScoresHeader.Size = new System.Drawing.Size(432, 69);
            this.txtGameScoresHeader.TabIndex = 0;
            this.txtGameScoresHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // dgrvwGameScores
            // 
            this.dgrvwGameScores.AllowUserToAddRows = false;
            this.dgrvwGameScores.AllowUserToDeleteRows = false;
            this.dgrvwGameScores.AllowUserToResizeColumns = false;
            this.dgrvwGameScores.AllowUserToResizeRows = false;
            this.dgrvwGameScores.BackgroundColor = System.Drawing.Color.White;
            this.dgrvwGameScores.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgrvwGameScores.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgrvwGameScores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgrvwGameScores.Location = new System.Drawing.Point(3, 3);
            this.dgrvwGameScores.MultiSelect = false;
            this.dgrvwGameScores.Name = "dgrvwGameScores";
            this.dgrvwGameScores.ReadOnly = true;
            this.dgrvwGameScores.RowHeadersVisible = false;
            this.dgrvwGameScores.Size = new System.Drawing.Size(426, 174);
            this.dgrvwGameScores.TabIndex = 1;
            this.dgrvwGameScores.SelectionChanged += new System.EventHandler(this.dgrvwGameScores_SelectionChanged);
            // 
            // panGameScores
            // 
            this.panGameScores.Controls.Add(this.dgrvwGameScores);
            this.panGameScores.Location = new System.Drawing.Point(-1, 87);
            this.panGameScores.Name = "panGameScores";
            this.panGameScores.Size = new System.Drawing.Size(432, 180);
            this.panGameScores.TabIndex = 2;
            // 
            // btnExit
            // 
            this.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnExit.Location = new System.Drawing.Point(356, 29);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 3;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Visible = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // frmGameScores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(443, 293);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.panGameScores);
            this.Controls.Add(this.txtGameScoresHeader);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmGameScores";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Game Scores at End Of Period";
            this.Load += new System.EventHandler(this.GameScores_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmGameScores_KeyPress);
            ((System.ComponentModel.ISupportInitialize)(this.dgrvwGameScores)).EndInit();
            this.panGameScores.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtGameScoresHeader;
        private System.Windows.Forms.DataGridView dgrvwGameScores;
        private System.Windows.Forms.Panel panGameScores;
        private System.Windows.Forms.Button btnExit;
    }
}