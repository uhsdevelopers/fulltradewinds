﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;

namespace GUILibraries.Forms {
  public partial class FrmCustomerCasinoPlays : SIDForm {
    private readonly String _currentPlaysType; //Posted or NonPosted
    private readonly String _currentCustomerId = "";
    private readonly DateTime _docDailyFigureDate;

    public FrmCustomerCasinoPlays(String playsType, String customerId, DateTime dailyFigureDate, ModuleInfo moduleInfo)
      : base(moduleInfo) {
      _currentCustomerId = customerId;
      _currentPlaysType = playsType;
      _docDailyFigureDate = new DateTime(dailyFigureDate.Year, dailyFigureDate.Month, dailyFigureDate.Day, 0, 0, 0);
      InitializeComponent();
    }

    private void btnClose_Click(object sender, EventArgs e) {
      Close();
    }

    private void frmCustomerNonPostedCasinoPlays_Load(object sender, EventArgs e) {
      switch (_currentPlaysType) {
        case "NonPosted":
          Text = @"Non-Posted Casino Plays";
          FillNonPostedCasinoPlaysDgvw();
          break;
        case "Posted":
          Text = @"Posted Casino Plays";
          FillPostedCasinoPlaysDgvw();
          break;
      }

    }

    private void FillNonPostedCasinoPlaysDgvw() {
      using (var casinos = new Casinos(AppModuleInfo)) {
        var casinoPlaysResult = casinos.GetCustomerNonPostedCasinoPlays(_currentCustomerId);

        if (!casinoPlaysResult.Any()) return;

        WriteCasinoPlaysGrvwHeaders();

        foreach (var res in casinoPlaysResult) {
          var date = DateTime.Parse(res.DailyFigureDate.ToString(CultureInfo.InvariantCulture));
          var winAmount = double.Parse(res.WinAmount.ToString()) / 100;
          var lostAmount = double.Parse(res.RiskAmount.ToString()) / 100;

          dgvwCasinoPlays.Rows.Add(date.ToShortDateString(), res.PlayDescription, FormatNumber(winAmount, true), FormatNumber(lostAmount, true));
        }
      }
    }

    private void FillPostedCasinoPlaysDgvw() {
      using (var casinos = new Casinos(AppModuleInfo)) {
        var casinoPlaysResult = casinos.GetCustomerPostedCasinoPlays(_currentCustomerId, _docDailyFigureDate);

        if (!casinoPlaysResult.Any()) return;
        WriteCasinoPlaysGrvwHeaders();

        foreach (var res in casinoPlaysResult) {
          var date = DateTime.Parse(res.DailyFigureDate.ToString(CultureInfo.InvariantCulture));
          var winAmount = double.Parse(res.WinAmount.ToString()) / 100;
          var lostAmount = double.Parse(res.RiskAmount.ToString()) / 100;

          dgvwCasinoPlays.Rows.Add(date.ToShortDateString(), res.PlayDescription, FormatNumber(winAmount, true), FormatNumber(lostAmount, true));
        }
      }
    }

    private void WriteCasinoPlaysGrvwHeaders() {
      var titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Date", Width = 100 };
      dgvwCasinoPlays.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Bet Type", Width = 100 };
      dgvwCasinoPlays.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Total Win", Width = 100 };
      dgvwCasinoPlays.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn { HeaderText = @"Total Loss", Width = 100 };
      dgvwCasinoPlays.Columns.Add(titleColumn);
    }
  }
}