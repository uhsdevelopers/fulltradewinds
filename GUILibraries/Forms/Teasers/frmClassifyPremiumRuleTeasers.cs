﻿using System;
using System.Globalization;
using System.Windows.Forms;
using GUILibraries.Utilities;
using SIDLibraries.BusinessLayer;

namespace GUILibraries.Forms.Teasers
{
  public partial class FrmClassifyPremiumRuleTeasers : Form
  {
    private readonly InstanceManager.ModuleInfo _moduleInfo;

    public FrmClassifyPremiumRuleTeasers(InstanceManager.ModuleInfo moduleInfo)
    {
      InitializeComponent();
      _moduleInfo = moduleInfo;
    }

    private void frmClassifyPremiumRuleTeasers_Load(object sender, EventArgs e)
    {
      LoadTeaserSpecs();
      LoadPremiumTeaserCents();
    }

    private void LoadPremiumTeaserCents()
    {
      using (var prm = new SystemParameters(_moduleInfo))
        txtPremiumCents.Text = prm.GetIntParameter(SystemParameters.PARAM_PremiumTeaserCents.ParamName).ToString(CultureInfo.InvariantCulture);
    }

    private void LoadTeaserSpecs()
    {
      dataGridView1.CellValueChanged -= dataGridView1_CellValueChanged;

      using (var tkw = new TicketsAndWagers(_moduleInfo))
      {
        var teasers = tkw.GetTeaserOffering();

        dataGridView1.DataSource = teasers;
        dataGridView1.Columns[ColumnNames.TeaserName].Width = 280;
        dataGridView1.Columns[ColumnNames.TeaserName].ReadOnly = true;
        dataGridView1.Columns[ColumnNames.TeaserTies].Visible = false;
        dataGridView1.Columns[ColumnNames.MaxPicks].Visible = false;
        dataGridView1.Columns[ColumnNames.MinPicks].Visible = false;
        dataGridView1.Columns[ColumnNames.SuspendFlag].Visible = false;
        dataGridView1.Columns[ColumnNames.CallUnitOnlyFlag].Visible = false;
        dataGridView1.Columns[ColumnNames.InternetOnlyFlag].Visible = false;
        dataGridView1.Columns[ColumnNames.Description].Visible = false;
        dataGridView1.Columns[ColumnNames.BaseOfferingFlag].Visible = false;

                // I thinks this column does not exist in the datasource. I am commenting this
                // and chaging the index of the new added column DMN 20141215
                // dataGridView1.Columns[ColumnNames.PremiumFlag].Visible = false;
        dataGridView1.Columns.Add(new DataGridViewCheckBoxColumn { Name = ColumnNames.ChkPremiumFlag, HeaderText = "Premium Active" });
        /* was:
        foreach (DataGridViewRow row in dataGridView1.Rows) {
          if (row.Cells[ColumnNames.PremiumFlag].Value != null && row.Cells[ColumnNames.PremiumFlag].Value.ToString().ToUpper() == "Y") {
            row.Cells[10].Value = true;
          }
        } */

                /* PremiumFlag column does not exist, this could lead to a problem, so I am comenting this DMN 20141215
                foreach (var row in dataGridView1.Rows.Cast<DataGridViewRow>().Where(row => row.Cells[ColumnNames.PremiumFlag].Value != null
                                                                                     && row.Cells[ColumnNames.PremiumFlag].Value.ToString().ToUpper() == "Y"))
                    row.Cells[ColumnNames.ChkPremiumFlag].Value = true; // Index was 10 MN 20141215 */
      }
      //dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
      dataGridView1.CellValueChanged += dataGridView1_CellValueChanged;
    }

    private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
    {
      using (var tkw = new TicketsAndWagers(_moduleInfo))
        tkw.UpdateTeaserSpecs(dataGridView1[0, e.RowIndex].Value.ToString(), dataGridView1[3, e.RowIndex].Value.ToString(), dataGridView1[2, e.RowIndex].Value.ToString(), int.Parse(dataGridView1[1, e.RowIndex].Value.ToString()), dataGridView1[4, e.RowIndex].Value.ToString(), dataGridView1[6, e.RowIndex].Value.ToString(), dataGridView1[5, e.RowIndex].Value.ToString(), dataGridView1[8, e.RowIndex].Value.ToString(), (bool)dataGridView1[10, e.RowIndex].Value, dataGridView1[7, e.RowIndex].Value.ToString());
    }

    private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
    {
      dataGridView1.CommitEdit(DataGridViewDataErrorContexts.Commit);
    }

    private void btnUpdate_Click(object sender, EventArgs e)
    {
      int value;

      if (int.TryParse(txtPremiumCents.Text, out value))
      {
        using (var prm = new SystemParameters(_moduleInfo))
        {
          prm.SaveParameter(SystemParameters.PARAM_PremiumTeaserCents, txtPremiumCents.Text);
          MessageBox.Show("Cents were updated!", _moduleInfo.Name, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
      }
      else MessageBox.Show("Invalid cents!", _moduleInfo.Name, MessageBoxButtons.OK, MessageBoxIcon.Warning);
    }
  }
}