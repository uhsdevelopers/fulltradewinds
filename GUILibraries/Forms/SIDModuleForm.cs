﻿using System.Configuration;
using System.Diagnostics;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;
using System;

namespace GUILibraries.Forms {
  // ReSharper disable once InconsistentNaming
  public partial class SIDModuleForm : SIDForm {
    protected SIDModuleForm(ModuleInfo moduleInfo)
      : base(moduleInfo) {
      if (AppModuleInfo != null)
        SystemParameters.LoadModuleParameters(AppModuleInfo);
      else {
        SystemLog.Log(new Exception("Module info not loaded properly. Module closed."));
        Environment.Exit(1);
      }
      InitializeComponent();
      InitializeComponentCustom();
    }

    protected SIDModuleForm() {
    }

    private static bool IsDesignMode() {
      return (Process.GetCurrentProcess().ProcessName.ToLower().Contains("devenv"));
    }

    private void ModuleForm_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e) {

      using (var im = new InstanceManager.BusinessLayer.InstanceManager()) {
        im.DeleteAppInUse(im.ApplicationInUseRecordId);
      }

      using (var logins = new LoginsAndProfiles(AppModuleInfo)) {
        logins.DeleteCurrentUserLoginInUseInfo(AppModuleInfo.ShortName);
      }
    }

    private void InitializeComponentCustom() {
      SetFormTitle();
    }

    private void SetFormTitle() {
      if (AppModuleInfo != null && AppModuleInfo.InstanceInfo != null)
        Text = @"*** Connected to " + AppModuleInfo.InstanceInfo.DatabaseName + @" ( " + AppModuleInfo.InstanceInfo.InstanceName + @") ***    " + ConfigurationManager.AppSettings["softwareOwner"] + @" " + AppModuleInfo.ShortName;
    }

  }
}