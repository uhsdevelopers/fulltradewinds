﻿namespace GUILibraries.Forms {
  partial class FrmLogViewer {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
      this.lbLog = new System.Windows.Forms.ListBox();
      this.btnExit = new System.Windows.Forms.Button();
      this.btnReload = new System.Windows.Forms.Button();
      this.panel1 = new System.Windows.Forms.Panel();
      this.button1 = new System.Windows.Forms.Button();
      this.panel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // lbLog
      // 
      this.lbLog.Dock = System.Windows.Forms.DockStyle.Fill;
      this.lbLog.FormattingEnabled = true;
      this.lbLog.Location = new System.Drawing.Point(0, 44);
      this.lbLog.Name = "lbLog";
      this.lbLog.Size = new System.Drawing.Size(813, 361);
      this.lbLog.TabIndex = 0;
      // 
      // btnExit
      // 
      this.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnExit.Location = new System.Drawing.Point(286, 11);
      this.btnExit.Name = "btnExit";
      this.btnExit.Size = new System.Drawing.Size(75, 23);
      this.btnExit.TabIndex = 1;
      this.btnExit.Text = "Exit";
      this.btnExit.UseVisualStyleBackColor = true;
      this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
      // 
      // btnReload
      // 
      this.btnReload.Location = new System.Drawing.Point(12, 11);
      this.btnReload.Name = "btnReload";
      this.btnReload.Size = new System.Drawing.Size(75, 23);
      this.btnReload.TabIndex = 2;
      this.btnReload.Text = "Reload";
      this.btnReload.UseVisualStyleBackColor = true;
      this.btnReload.Click += new System.EventHandler(this.btnReload_Click);
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.button1);
      this.panel1.Controls.Add(this.btnExit);
      this.panel1.Controls.Add(this.btnReload);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel1.Location = new System.Drawing.Point(0, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(813, 44);
      this.panel1.TabIndex = 3;
      // 
      // button1
      // 
      this.button1.Location = new System.Drawing.Point(93, 11);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(75, 23);
      this.button1.TabIndex = 3;
      this.button1.Text = "Empty";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new System.EventHandler(this.button1_Click);
      // 
      // FrmLogViewer
      // 
      this.AcceptButton = this.btnReload;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnExit;
      this.ClientSize = new System.Drawing.Size(813, 405);
      this.Controls.Add(this.lbLog);
      this.Controls.Add(this.panel1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.KeyPreview = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "FrmLogViewer";
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "LogViewer";
      this.Load += new System.EventHandler(this.frmLogViewer_Load);
      this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmLogViewer_KeyDown);
      this.panel1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.ListBox lbLog;
    private System.Windows.Forms.Button btnExit;
    private System.Windows.Forms.Button btnReload;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Button button1;
  }
}