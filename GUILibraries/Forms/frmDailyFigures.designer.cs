﻿namespace GUILibraries.Forms
{
    partial class FrmDailyFigures
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panDailyFigures = new System.Windows.Forms.Panel();
            this.dgvwDailyFigures = new System.Windows.Forms.DataGridView();
            this.panDailyFiguresDetails = new System.Windows.Forms.Panel();
            this.dgvwCustWagerDetails = new System.Windows.Forms.DataGridView();
            this.grpDailyFiguresByWeek = new System.Windows.Forms.GroupBox();
            this.txtWeeklyFigure4 = new GUILibraries.Controls.NumberTextBox();
            this.txtWeeklyFigure3 = new GUILibraries.Controls.NumberTextBox();
            this.txtWeeklyFigure2 = new GUILibraries.Controls.NumberTextBox();
            this.txtWeeklyFigure1 = new GUILibraries.Controls.NumberTextBox();
            this.lblWeeklyFigure4 = new System.Windows.Forms.Label();
            this.lblWeeklyFigure3 = new System.Windows.Forms.Label();
            this.lblWeeklyFigure2 = new System.Windows.Forms.Label();
            this.lblWeeklyFigure1 = new System.Windows.Forms.Label();
            this.btnWagerDetails = new System.Windows.Forms.Button();
            this.btnWagerComments = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.txtDailyFiguresCurrencyLegend = new System.Windows.Forms.TextBox();
            this.panDailyFigures.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvwDailyFigures)).BeginInit();
            this.panDailyFiguresDetails.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvwCustWagerDetails)).BeginInit();
            this.grpDailyFiguresByWeek.SuspendLayout();
            this.SuspendLayout();
            // 
            // panDailyFigures
            // 
            this.panDailyFigures.Controls.Add(this.dgvwDailyFigures);
            this.panDailyFigures.Location = new System.Drawing.Point(13, 13);
            this.panDailyFigures.Name = "panDailyFigures";
            this.panDailyFigures.Size = new System.Drawing.Size(251, 194);
            this.panDailyFigures.TabIndex = 0;
            // 
            // dgvwDailyFigures
            // 
            this.dgvwDailyFigures.AllowUserToAddRows = false;
            this.dgvwDailyFigures.AllowUserToDeleteRows = false;
            this.dgvwDailyFigures.BackgroundColor = System.Drawing.Color.White;
            this.dgvwDailyFigures.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvwDailyFigures.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvwDailyFigures.Location = new System.Drawing.Point(3, 4);
            this.dgvwDailyFigures.MultiSelect = false;
            this.dgvwDailyFigures.Name = "dgvwDailyFigures";
            this.dgvwDailyFigures.ReadOnly = true;
            this.dgvwDailyFigures.RowHeadersVisible = false;
            this.dgvwDailyFigures.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvwDailyFigures.Size = new System.Drawing.Size(246, 187);
            this.dgvwDailyFigures.StandardTab = true;
            this.dgvwDailyFigures.TabIndex = 10;
            this.dgvwDailyFigures.SelectionChanged += new System.EventHandler(this.dgvwDailyFigures_SelectionChanged);
            // 
            // panDailyFiguresDetails
            // 
            this.panDailyFiguresDetails.Controls.Add(this.dgvwCustWagerDetails);
            this.panDailyFiguresDetails.Location = new System.Drawing.Point(13, 228);
            this.panDailyFiguresDetails.Name = "panDailyFiguresDetails";
            this.panDailyFiguresDetails.Size = new System.Drawing.Size(612, 185);
            this.panDailyFiguresDetails.TabIndex = 1;
            // 
            // dgvwCustWagerDetails
            // 
            this.dgvwCustWagerDetails.AllowUserToAddRows = false;
            this.dgvwCustWagerDetails.AllowUserToDeleteRows = false;
            this.dgvwCustWagerDetails.BackgroundColor = System.Drawing.Color.White;
            this.dgvwCustWagerDetails.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvwCustWagerDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvwCustWagerDetails.Location = new System.Drawing.Point(0, 3);
            this.dgvwCustWagerDetails.MultiSelect = false;
            this.dgvwCustWagerDetails.Name = "dgvwCustWagerDetails";
            this.dgvwCustWagerDetails.ReadOnly = true;
            this.dgvwCustWagerDetails.RowHeadersVisible = false;
            this.dgvwCustWagerDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvwCustWagerDetails.Size = new System.Drawing.Size(609, 182);
            this.dgvwCustWagerDetails.StandardTab = true;
            this.dgvwCustWagerDetails.TabIndex = 20;
            this.dgvwCustWagerDetails.SelectionChanged += new System.EventHandler(this.dgvwCustWagerDetails_SelectionChanged);
            this.dgvwCustWagerDetails.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvwCustWagerDetails_KeyDown);
            // 
            // grpDailyFiguresByWeek
            // 
            this.grpDailyFiguresByWeek.Controls.Add(this.txtWeeklyFigure4);
            this.grpDailyFiguresByWeek.Controls.Add(this.txtWeeklyFigure3);
            this.grpDailyFiguresByWeek.Controls.Add(this.txtWeeklyFigure2);
            this.grpDailyFiguresByWeek.Controls.Add(this.txtWeeklyFigure1);
            this.grpDailyFiguresByWeek.Controls.Add(this.lblWeeklyFigure4);
            this.grpDailyFiguresByWeek.Controls.Add(this.lblWeeklyFigure3);
            this.grpDailyFiguresByWeek.Controls.Add(this.lblWeeklyFigure2);
            this.grpDailyFiguresByWeek.Controls.Add(this.lblWeeklyFigure1);
            this.grpDailyFiguresByWeek.Location = new System.Drawing.Point(330, 13);
            this.grpDailyFiguresByWeek.Name = "grpDailyFiguresByWeek";
            this.grpDailyFiguresByWeek.Size = new System.Drawing.Size(295, 194);
            this.grpDailyFiguresByWeek.TabIndex = 2;
            this.grpDailyFiguresByWeek.TabStop = false;
            this.grpDailyFiguresByWeek.Text = "Weekly Figures";
            // 
            // txtWeeklyFigure4
            // 
            this.txtWeeklyFigure4.AllowCents = false;
            this.txtWeeklyFigure4.DividedBy = 1;
            this.txtWeeklyFigure4.DivideFlag = false;
            this.txtWeeklyFigure4.Enabled = false;
            this.txtWeeklyFigure4.Location = new System.Drawing.Point(116, 116);
            this.txtWeeklyFigure4.Name = "txtWeeklyFigure4";
            this.txtWeeklyFigure4.ShowIfZero = true;
            this.txtWeeklyFigure4.Size = new System.Drawing.Size(100, 20);
            this.txtWeeklyFigure4.TabIndex = 11;
            this.txtWeeklyFigure4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtWeeklyFigure3
            // 
            this.txtWeeklyFigure3.AllowCents = false;
            this.txtWeeklyFigure3.DividedBy = 1;
            this.txtWeeklyFigure3.DivideFlag = false;
            this.txtWeeklyFigure3.Enabled = false;
            this.txtWeeklyFigure3.Location = new System.Drawing.Point(116, 88);
            this.txtWeeklyFigure3.Name = "txtWeeklyFigure3";
            this.txtWeeklyFigure3.ShowIfZero = true;
            this.txtWeeklyFigure3.Size = new System.Drawing.Size(100, 20);
            this.txtWeeklyFigure3.TabIndex = 10;
            this.txtWeeklyFigure3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtWeeklyFigure2
            // 
            this.txtWeeklyFigure2.AllowCents = false;
            this.txtWeeklyFigure2.DividedBy = 1;
            this.txtWeeklyFigure2.DivideFlag = false;
            this.txtWeeklyFigure2.Enabled = false;
            this.txtWeeklyFigure2.Location = new System.Drawing.Point(116, 56);
            this.txtWeeklyFigure2.Name = "txtWeeklyFigure2";
            this.txtWeeklyFigure2.ShowIfZero = true;
            this.txtWeeklyFigure2.Size = new System.Drawing.Size(100, 20);
            this.txtWeeklyFigure2.TabIndex = 9;
            this.txtWeeklyFigure2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtWeeklyFigure1
            // 
            this.txtWeeklyFigure1.AllowCents = false;
            this.txtWeeklyFigure1.DividedBy = 1;
            this.txtWeeklyFigure1.DivideFlag = false;
            this.txtWeeklyFigure1.Enabled = false;
            this.txtWeeklyFigure1.Location = new System.Drawing.Point(116, 23);
            this.txtWeeklyFigure1.Name = "txtWeeklyFigure1";
            this.txtWeeklyFigure1.ShowIfZero = true;
            this.txtWeeklyFigure1.Size = new System.Drawing.Size(100, 20);
            this.txtWeeklyFigure1.TabIndex = 8;
            this.txtWeeklyFigure1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblWeeklyFigure4
            // 
            this.lblWeeklyFigure4.AutoSize = true;
            this.lblWeeklyFigure4.Location = new System.Drawing.Point(6, 120);
            this.lblWeeklyFigure4.Name = "lblWeeklyFigure4";
            this.lblWeeklyFigure4.Size = new System.Drawing.Size(39, 13);
            this.lblWeeklyFigure4.TabIndex = 3;
            this.lblWeeklyFigure4.Text = "Label4";
            this.lblWeeklyFigure4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblWeeklyFigure3
            // 
            this.lblWeeklyFigure3.AutoSize = true;
            this.lblWeeklyFigure3.Location = new System.Drawing.Point(6, 90);
            this.lblWeeklyFigure3.Name = "lblWeeklyFigure3";
            this.lblWeeklyFigure3.Size = new System.Drawing.Size(39, 13);
            this.lblWeeklyFigure3.TabIndex = 2;
            this.lblWeeklyFigure3.Text = "Label3";
            this.lblWeeklyFigure3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblWeeklyFigure2
            // 
            this.lblWeeklyFigure2.AutoSize = true;
            this.lblWeeklyFigure2.Location = new System.Drawing.Point(7, 60);
            this.lblWeeklyFigure2.Name = "lblWeeklyFigure2";
            this.lblWeeklyFigure2.Size = new System.Drawing.Size(39, 13);
            this.lblWeeklyFigure2.TabIndex = 1;
            this.lblWeeklyFigure2.Text = "Label2";
            this.lblWeeklyFigure2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblWeeklyFigure1
            // 
            this.lblWeeklyFigure1.AutoSize = true;
            this.lblWeeklyFigure1.Location = new System.Drawing.Point(7, 30);
            this.lblWeeklyFigure1.Name = "lblWeeklyFigure1";
            this.lblWeeklyFigure1.Size = new System.Drawing.Size(39, 13);
            this.lblWeeklyFigure1.TabIndex = 0;
            this.lblWeeklyFigure1.Text = "Label1";
            this.lblWeeklyFigure1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnWagerDetails
            // 
            this.btnWagerDetails.Location = new System.Drawing.Point(268, 422);
            this.btnWagerDetails.Name = "btnWagerDetails";
            this.btnWagerDetails.Size = new System.Drawing.Size(89, 23);
            this.btnWagerDetails.TabIndex = 30;
            this.btnWagerDetails.Text = "Wager Details";
            this.btnWagerDetails.UseVisualStyleBackColor = true;
            this.btnWagerDetails.Click += new System.EventHandler(this.btnWagerDetails_Click);
            // 
            // btnWagerComments
            // 
            this.btnWagerComments.Location = new System.Drawing.Point(370, 422);
            this.btnWagerComments.Name = "btnWagerComments";
            this.btnWagerComments.Size = new System.Drawing.Size(103, 23);
            this.btnWagerComments.TabIndex = 40;
            this.btnWagerComments.Text = "Wager Comments";
            this.btnWagerComments.UseVisualStyleBackColor = true;
            this.btnWagerComments.Click += new System.EventHandler(this.btnWagerComments_Click);
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(486, 422);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(89, 23);
            this.btnClose.TabIndex = 50;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // txtDailyFiguresCurrencyLegend
            // 
            this.txtDailyFiguresCurrencyLegend.Enabled = false;
            this.txtDailyFiguresCurrencyLegend.Location = new System.Drawing.Point(12, 425);
            this.txtDailyFiguresCurrencyLegend.Name = "txtDailyFiguresCurrencyLegend";
            this.txtDailyFiguresCurrencyLegend.ReadOnly = true;
            this.txtDailyFiguresCurrencyLegend.Size = new System.Drawing.Size(228, 20);
            this.txtDailyFiguresCurrencyLegend.TabIndex = 6;
            this.txtDailyFiguresCurrencyLegend.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // FrmDailyFigures
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(637, 457);
            this.Controls.Add(this.txtDailyFiguresCurrencyLegend);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnWagerComments);
            this.Controls.Add(this.btnWagerDetails);
            this.Controls.Add(this.grpDailyFiguresByWeek);
            this.Controls.Add(this.panDailyFiguresDetails);
            this.Controls.Add(this.panDailyFigures);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "FrmDailyFigures";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Customer Daily Figures";
            this.Load += new System.EventHandler(this.DailyFigures_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmDailyFigures_KeyPress);
            this.panDailyFigures.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvwDailyFigures)).EndInit();
            this.panDailyFiguresDetails.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvwCustWagerDetails)).EndInit();
            this.grpDailyFiguresByWeek.ResumeLayout(false);
            this.grpDailyFiguresByWeek.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panDailyFigures;
        private System.Windows.Forms.DataGridView dgvwDailyFigures;
        private System.Windows.Forms.Panel panDailyFiguresDetails;
        private System.Windows.Forms.GroupBox grpDailyFiguresByWeek;
        private System.Windows.Forms.Button btnWagerDetails;
        private System.Windows.Forms.Button btnWagerComments;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TextBox txtDailyFiguresCurrencyLegend;
        private System.Windows.Forms.DataGridView dgvwCustWagerDetails;
        private System.Windows.Forms.Label lblWeeklyFigure2;
        private System.Windows.Forms.Label lblWeeklyFigure1;
        private System.Windows.Forms.Label lblWeeklyFigure4;
        private System.Windows.Forms.Label lblWeeklyFigure3;
        private GUILibraries.Controls.NumberTextBox txtWeeklyFigure4;
        private GUILibraries.Controls.NumberTextBox txtWeeklyFigure3;
        private GUILibraries.Controls.NumberTextBox txtWeeklyFigure2;
        private GUILibraries.Controls.NumberTextBox txtWeeklyFigure1;
    }
}