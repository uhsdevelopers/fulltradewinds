﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using GUILibraries.Controls;
using GUILibraries.Utilities;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using SIDLibraries.Utilities;

namespace GUILibraries.Forms {
  public partial class FrmTicketInformation : SIDForm {

    #region private Constants

    private const int GRIDVIEWROWHEIGHT = 16;

    #endregion

    #region Public Properties

    public Boolean RollingIfBetMode { private get; set; }
    public Boolean OpenPlaysMode { private get; set; }
    public Boolean EnableToEditPendingPlays { get; set; }
    public Boolean ReloadTransactionsGrid { get; private set; }

    #endregion

    #region Private Properties

    FrmCustomerWagerDetails WagerDetailsFrm { get; set; }

    #endregion

    #region Private vars

    private readonly List<spULPGetCurrentUserPermissions_Result> _currentUserPermissions;
    private readonly Form _parentForm;
    private readonly String _currentCustomerId = "";
    private readonly String _customerCurrentCurrency = "";
    private readonly int _currentDocumentNumber;
    private readonly int _currentGradeNumber;
    private readonly DateTime _currentTranDateTime;
    private int _currentTicketNumber;
    private int _currentWagerNumber;
    private DateTime _currentTicketPlacedDateTime;
    private readonly Boolean _machineHasSoundCard;
    List<spTkWGetCustomerHistoryWagerDetails_Result> _resultSet;
    private int? _ticketNumber;
    private int _currentDateRangeIdx;
    private bool _keyDownWasUsed;

    #endregion

    #region Constructors

    public FrmTicketInformation(Form parentFrm, int documentNumber, int gradeNumber, DateTime tranDateTime, ModuleInfo moduleInfo, String customerId, List<spULPGetCurrentUserPermissions_Result> userPermissions, String customerCurrency, Boolean soundCardDetected, int? ticketNumber)
      : base(moduleInfo) {
      if (parentFrm != null) {
        _parentForm = parentFrm;
        _currentDocumentNumber = documentNumber;
        _currentGradeNumber = gradeNumber;
        _currentTranDateTime = tranDateTime;

        _currentCustomerId = customerId;
        _customerCurrentCurrency = customerCurrency;
        _currentUserPermissions = userPermissions;
        _machineHasSoundCard = soundCardDetected;
        _ticketNumber = ticketNumber;
        InitializeComponent();
      }
      else {
        MessageBox.Show(@"No customerId was selected");
        Close();
      }
    }

    #endregion

    #region Private Methods

    private static double GetComplexBetPaidAmount(IEnumerable<spTkWGetCustomerHistoryWagerDetails_Result> resultSet) {
      double paidAmount = 0;
      foreach (var res in resultSet) {
        switch (res.WagerStatus.Trim()) {
          case "Loss":
            if (res.CreditAcctFlag == "N") {
              paidAmount += 0;
            }
            else {
              if (res.AmountLost != null)
                paidAmount += double.Parse(res.AmountLost.ToString()) * -1;
            }
            break;

          case "Win":
            if (res.CreditAcctFlag == "N") {
              if (res.AmountWagered != null && res.AmountWon != null)
                paidAmount += double.Parse(res.AmountWagered.ToString()) + double.Parse(res.AmountWon.ToString());
            }
            else {
              if (res.AmountWon != null)
                paidAmount += double.Parse(res.AmountWon.ToString());
            }
            break;

          case "Cancel":
            if (res.CreditAcctFlag == "N") {
              if (res.AmountWagered != null)
                paidAmount += double.Parse(res.AmountWagered.ToString());
            }
            else {
              paidAmount += 0;
            }
            break;

          case "Open":
          case "Pending":
            break;
        }
      }
      return paidAmount;
    }

    private static double GetPaidAMount(spTkWGetCustomerHistoryWagerDetails_Result res) {
      double paidAmount = 0;

      switch (res.WagerStatus.Trim()) {
        case "Loss":
          if (res.CreditAcctFlag == "N" || res.FreePlayFlag == "Y")
            paidAmount = 0;
          else if (res.AmountLost != null)
            paidAmount = double.Parse(res.AmountLost.ToString()) * -1;
          break;
        case "Win":
          if (res.CreditAcctFlag == "N") {
            if (res.AmountWagered != null && res.AmountWon != null)
              paidAmount = double.Parse(res.AmountWagered.ToString()) + double.Parse(res.AmountWon.ToString());
          }
          else if (res.AmountWon != null)
            paidAmount = double.Parse(res.AmountWon.ToString());
          break;
        case "Cancel":
          if (res.CreditAcctFlag == "N") {
            if (res.AmountWagered != null)
              paidAmount = double.Parse(res.AmountWagered.ToString());
          }
          else
            paidAmount = 0;
          break;
        case "Open":
        case "Pending":
          break;
      }
      return paidAmount;
    }

    private void PopulateWagersDetailsDgvw(int ticketNumber, DateTime ticketPostedDateTime) {
      using (var tAw = new TicketsAndWagers(AppModuleInfo))
        _resultSet = tAw.GetCustomerHistoryWagerDetails(ticketNumber, ticketPostedDateTime).ToList();

      var selectedRow = 0;

      if (_resultSet == null)
        return;
      if (_resultSet.Count > 0) {
        WriteWagerDetailsGrvwHeaders();

        int? holdPlayNumber = null;

        if (cbxTicketsDateRange.SelectedItem != null && cbxTicketsDateRange.SelectedItem.ToString() == "Pending/Open Wagers Only")
          _resultSet = (from h in _resultSet
                        where h.WagerStatus == "Pending" || h.WagerStatus == "Open"
                        select h).ToList();
        if (cbxTicketsDateRange.SelectedItem != null && cbxTicketsDateRange.SelectedItem.ToString() == "Open Parlays/Teasers Only")
          _resultSet = (from h in _resultSet
                        where h.WagerStatus == "Open" && (h.WagerType == WagerType.PARLAY || h.WagerType == WagerType.TEASER)
                        select h).ToList();
        if (cbxTicketsDateRange.SelectedItem != null && cbxTicketsDateRange.SelectedItem.ToString() == "Pending Only")
          _resultSet = (from h in _resultSet
                        where h.WagerStatus == "Pending"
                        select h).ToList();
        if (cbxTicketsDateRange.SelectedItem != null && cbxTicketsDateRange.SelectedItem.ToString() == "Available for Rolling Ifs") {
          _resultSet = (from h in _resultSet
                        where (h.WagerType == WagerType.SPREAD || h.WagerType == WagerType.TOTALPOINTS || h.WagerType == WagerType.TEAMTOTALPOINTS || h.WagerType == WagerType.MONEYLINE || h.WagerType == WagerType.PARLAY || h.WagerType == WagerType.TEASER)
                              && h.WagerStatus == "Pending"
                        select h).ToList();
        }
        if (_resultSet.Count <= 0) {
          btnWagerDetails.Enabled = false;
          btnDeleteWager.Enabled = false;
          btnWagerComments.Enabled = false;
          return;
        }
        foreach (var res in _resultSet) {
          var firstRowWagerType = res.WagerTypeDesc ?? "";
          var wagerDescription = (res.FreePlayFlag == "Y" ? "** Free Play ** " : "") + res.Description;
          wagerDescription = wagerDescription.Replace("\r\n", "\u2590\u258C");

          double paidAmount;
          int rifBackwardTicketNumber;
          int rifForwardTicketNumber;

          string detailsWagerType;
          if (holdPlayNumber != res.PlayNumber) {
            if (firstRowWagerType.Contains("Robin") || firstRowWagerType.Contains("Reverse") || firstRowWagerType.Contains("Birdcage")) {
              string complexBetLink;
              if (firstRowWagerType.IndexOf("Robin", StringComparison.Ordinal) > 0) {
                firstRowWagerType = "Round Robin";
                detailsWagerType = res.WagerType == WagerType.PARLAY ? "(RR) Parlay" : "(RR) Teaser";
                complexBetLink = res.RoundRobinLink.ToString();
              }
              else if (firstRowWagerType.IndexOf("Reverse", StringComparison.Ordinal) > 0) {
                firstRowWagerType = "Action Reverse";
                detailsWagerType = "(AR) If-Bet";
                complexBetLink = res.ARLink.ToString();
              }
              else {
                firstRowWagerType = "AR Birdcage";
                detailsWagerType = "(ARBC) If-Bet";
                complexBetLink = res.ARBirdcageLink.ToString();
              }
              var riskAmount = double.Parse((from r in _resultSet where r.PlayNumber == res.PlayNumber select r.AmountWagered).Sum().ToString());
              var toWinAmount = double.Parse((from r in _resultSet where r.PlayNumber == res.PlayNumber select r.ToWinAmount).Sum().ToString(CultureInfo.InvariantCulture));
              paidAmount = GetComplexBetPaidAmount(_resultSet.Where(w => w.PlayNumber == res.PlayNumber).ToList());
              var itemsCnt = int.Parse((from r in _resultSet where r.PlayNumber == res.PlayNumber select r).Count().ToString(CultureInfo.InvariantCulture));

              dgrvwWagersDetails.Rows.Add(res.PlayNumber.ToString(), firstRowWagerType, FormatNumber(riskAmount, true), FormatNumber(toWinAmount, true), "", FormatNumber(paidAmount, true), itemsCnt + " " + detailsWagerType.Split(' ')[1] + "s", ticketNumber, 0, res.WagerType, "", "", complexBetLink, res.InetSessionId);
              dgrvwWagersDetails.Rows[dgrvwWagersDetails.Rows.Count - 1].Height = GRIDVIEWROWHEIGHT;
              paidAmount = GetPaidAMount(res);
              dgrvwWagersDetails.Rows.Add("", detailsWagerType, FormatNumber((double)res.AmountWagered, true), FormatNumber(res.ToWinAmount, true), res.WagerStatus, FormatNumber(paidAmount, true).ToString(CultureInfo.InvariantCulture) == "0.00" ? "" : FormatNumber(paidAmount, true).ToString(CultureInfo.InvariantCulture), wagerDescription, res.TicketNumber, res.WagerNumber, res.WagerType, res.FreePlayFlag, res.TicketPostedDateTime, null, res.InetSessionId);
              dgrvwWagersDetails.Rows[dgrvwWagersDetails.Rows.Count - 1].Height = GRIDVIEWROWHEIGHT;
            }
            else {
              detailsWagerType = GetDetailsWagerType(res);
              rifBackwardTicketNumber = res.RifBackwardTicketNumber ?? 0;
              rifForwardTicketNumber = res.RifForwardTicketNumber ?? 0;

              if (rifBackwardTicketNumber != 0)
                detailsWagerType = "RIF-> " + detailsWagerType;
              if (rifForwardTicketNumber != 0)
                detailsWagerType = detailsWagerType + " ->RIF";
              paidAmount = GetPaidAMount(res);
              dgrvwWagersDetails.Rows.Add(res.PlayNumber.ToString(), detailsWagerType, FormatNumber((double)res.AmountWagered, true), FormatNumber(res.ToWinAmount, true), res.WagerStatus,
                                          FormatNumber(paidAmount, true).ToString(CultureInfo.InvariantCulture) == "0.00" ? "" : FormatNumber(paidAmount, true).ToString(CultureInfo.InvariantCulture), wagerDescription,
                                          res.TicketNumber, res.WagerNumber, res.WagerType, res.FreePlayFlag, res.TicketPostedDateTime, null, res.InetSessionId);
              dgrvwWagersDetails.Rows[dgrvwWagersDetails.Rows.Count - 1].Height = GRIDVIEWROWHEIGHT;
            }
          }
          else {
            var playNumber = "";

            if (firstRowWagerType.Contains("Robin") || firstRowWagerType.Contains("Reverse") || firstRowWagerType.Contains("Birdcage")) {
              if (firstRowWagerType.IndexOf("Robin", StringComparison.Ordinal) > 0) {
                detailsWagerType = res.WagerType == WagerType.PARLAY ? "(RR) Parlay" : "(RR) Teaser";
              }
              else if (firstRowWagerType.IndexOf("Reverse", StringComparison.Ordinal) > 0)
                detailsWagerType = "(AR) If-Bet";
              else
                detailsWagerType = "(ARBC) If-Bet";
            }
            else {
              detailsWagerType = GetDetailsWagerType(res);
              playNumber = res.PlayNumber.ToString();
            }
            rifBackwardTicketNumber = res.RifBackwardTicketNumber ?? 0;
            rifForwardTicketNumber = res.RifForwardTicketNumber ?? 0;

            if (rifBackwardTicketNumber != 0)
              detailsWagerType = "RIF-> " + detailsWagerType;
            if (rifForwardTicketNumber != 0)
              detailsWagerType = detailsWagerType + " ->RIF";
            paidAmount = GetPaidAMount(res);
            dgrvwWagersDetails.Rows.Add(playNumber, detailsWagerType, FormatNumber((double)res.AmountWagered, true), FormatNumber(res.ToWinAmount, true), res.WagerStatus,
                                        FormatNumber(paidAmount, true).ToString(CultureInfo.InvariantCulture) == "0.00" ? "" : FormatNumber(paidAmount, true).ToString(CultureInfo.InvariantCulture), wagerDescription,
                                        res.TicketNumber, res.WagerNumber, res.WagerType, res.FreePlayFlag, res.TicketPostedDateTime, null, res.InetSessionId);
            dgrvwWagersDetails.Rows[dgrvwWagersDetails.Rows.Count - 1].Height = GRIDVIEWROWHEIGHT;
          }
          holdPlayNumber = res.PlayNumber;
          if (int.Parse(res.WagerNumber.ToString(CultureInfo.InvariantCulture)) == _currentWagerNumber) {
            selectedRow = dgrvwWagersDetails.Rows.Count - 1;
          }
        }
        FormF.PreventDataGridViewColumnsToBeSortedAndResized(dgrvwWagersDetails);
        btnWagerDetails.Enabled = true;
        dgrvwWagersDetails.Rows[selectedRow].Selected = true;
      }
      else {
        btnWagerDetails.Enabled = false;
        btnDeleteWager.Enabled = false;
        btnWagerComments.Enabled = false;
      }

    }

    private string GetDetailsWagerType(spTkWGetCustomerHistoryWagerDetails_Result res) {
      string detailsWagerType = "";
      switch (res.WagerType) {
        case WagerType.MANUALPLAY:
          detailsWagerType = "Manual Play";

          if (res.LayoffFlag != null && res.LayoffFlag.Trim() == "Y")
            detailsWagerType = "Layoff Manual Play";
          break;
        case WagerType.CONTEST:
        case WagerType.TOTALPOINTS:
        case WagerType.TEAMTOTALPOINTS:
        case WagerType.MONEYLINE:
        case WagerType.SPREAD:
          detailsWagerType = "Straight";

          if (res.LayoffFlag != null && res.LayoffFlag.Trim() == "Y")
            detailsWagerType = "Layoff Straight";
          break;
        case "G":
          detailsWagerType = "X";
          break;
        case WagerType.IFBET:
          detailsWagerType = "If Bet";
          break;
        case WagerType.PARLAY:
          detailsWagerType = "Parlay";
          break;
        case WagerType.TEASER:
          detailsWagerType = "Teaser";
          break;
      }
      return detailsWagerType;
    }

    private void WriteWagerDetailsGrvwHeaders() {

      dgrvwWagersDetails.Columns.Clear();
      dgrvwWagersDetails.Columns.AddRange(new DataGridViewTextBoxColumn {
        HeaderText = @"#",
        Name = ColumnNames.Number,
        Width = 25
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"Type",
        Name = ColumnNames.Type,
        Width = 100
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"Risk",
        Name = ColumnNames.Risk,
        Width = 100,
        DefaultCellStyle = new DataGridViewCellStyle { Alignment = DataGridViewContentAlignment.MiddleRight }
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"To Win",
        Name = ColumnNames.ToWin,
        Width = 100,
        DefaultCellStyle = new DataGridViewCellStyle { Alignment = DataGridViewContentAlignment.MiddleRight }
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"Status",
        Name = ColumnNames.Status,
        Width = 50
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"Paid",
        Name = ColumnNames.Paid,
        Width = 100,
        DefaultCellStyle = new DataGridViewCellStyle { Alignment = DataGridViewContentAlignment.MiddleRight }
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"Description",
        Name = ColumnNames.Description,
        Width = 2400
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"TicketNumber",
        Name = ColumnNames.TicketNumber,
        Width = 50,
        Visible = false
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"WagerNumber",
        Name = ColumnNames.WagerNumber,
        Width = 10,
        Visible = false
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"WagerType",
        Name = ColumnNames.WagerType2,
        Width = 10,
        Visible = false
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"IsFreePlay",
        Name = ColumnNames.IsFreePlay,
        Width = 10,
        Visible = false
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"ticketPostedDateTime",
        Name = ColumnNames.TicketPostedDateTime,
        Width = 10,
        Visible = false
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"complexBetLink",
        Name = ColumnNames.ComplexBetLink,
        Width = 10,
        Visible = false
      }, new DataGridViewTextBoxColumn {
        HeaderText = ColumnNames.InetSessionId,
        Name = ColumnNames.InetSessionId,
        Width = 10,
        Visible = false
      });
    }

    private void AdjustCustomerBalancesDisplay(string outcome, string freePlayFlag, double amountWagered, double amountWon) {
      outcome = outcome.Trim().ToUpper();
      double amountToAdjust = 0;

      switch (outcome) {
        case "PENDING":
        case "OPEN":
        case "LOSS":
        case "CANCEL":
          amountToAdjust = amountWagered;
          break;
        case "WIN":
          amountToAdjust = amountWon;
          break;
      }
      PropertyInfo parentProperty;
      int parentCnt;
      double parentAmt = 0;
      double totalParentAmt = 0;

      NumberTextBox parentTxtBox;

      if (freePlayFlag == "Y") {
        if (outcome == "PENDING" || outcome == "OPEN") {
          parentProperty = _parentForm.GetType().GetProperty(PropertyNames.CustomerFreePlayPendingCount);

          if (parentProperty != null) {
            parentCnt = (int)parentProperty.GetValue(_parentForm, null);
            parentProperty.SetValue(_parentForm, parentCnt - 1, null);
          }
          parentProperty = _parentForm.GetType().GetProperty(PropertyNames.CustomerFreePlayPendingBalance);

          if (parentProperty != null) {
            parentAmt = (double)parentProperty.GetValue(_parentForm, null);
            parentProperty.SetValue(_parentForm, parentAmt - amountToAdjust * 100, null);
          }
          parentTxtBox = (NumberTextBox)_parentForm.Controls.Find(ControlNames.TxtFreePlayPendingAmount, true).FirstOrDefault();

          if (parentTxtBox != null) {
            parentAmt = double.Parse(parentTxtBox.Text);
            totalParentAmt = parentAmt - amountToAdjust;

            parentTxtBox.Text = (totalParentAmt * parentTxtBox.DividedBy).ToString(CultureInfo.InvariantCulture);
          }
          parentTxtBox = (NumberTextBox)_parentForm.Controls.Find(ControlNames.TxtFreePlayPendingCount, true).FirstOrDefault();

          if (parentTxtBox == null)
            return;
          parentCnt = int.Parse(parentTxtBox.Text);
          parentTxtBox.Text = ((parentCnt - 1) * parentTxtBox.DividedBy).ToString(CultureInfo.InvariantCulture);
        }
        else {
          parentProperty = _parentForm.GetType().GetProperty(PropertyNames.CustomerFreePlayBalance);

          if (parentProperty != null) {
            parentAmt = (double)parentProperty.GetValue(_parentForm, null);

            switch (outcome) {
              case "LOSS":
              case "CANCEL":
                parentProperty.SetValue(_parentForm, parentAmt + amountToAdjust * 100, null);
                break;
              case "WIN":
                parentProperty.SetValue(_parentForm, parentAmt - amountToAdjust * 100, null);
                break;
            }
          }
          parentTxtBox = (NumberTextBox)_parentForm.Controls.Find(ControlNames.TxtFreePlayBalance, true).FirstOrDefault();

          if (parentTxtBox == null)
            return;
          parentAmt = double.Parse(parentTxtBox.Text);

          switch (outcome) {
            case "LOSS":
            case "CANCEL":
              totalParentAmt = parentAmt + amountToAdjust;
              break;
            case "WIN":
              totalParentAmt = parentAmt - amountToAdjust;
              break;
          }
          parentTxtBox.Text = (totalParentAmt * parentTxtBox.DividedBy).ToString(CultureInfo.InvariantCulture);
        }
        // update FreePlayPendingBalance, FreePlayPendingCount and FreePlayBalance
      }
      else {
        if (outcome == "PENDING" || outcome == "OPEN") {
          parentProperty = _parentForm.GetType().GetProperty(PropertyNames.CustomerPendingWagerCount);

          if (parentProperty != null) {
            parentCnt = (int)parentProperty.GetValue(_parentForm, null);
            parentProperty.SetValue(_parentForm, parentCnt - 1, null);
          }
          parentProperty = _parentForm.GetType().GetProperty(PropertyNames.CustomerPendingWagerBalance);

          if (parentProperty != null) {
            parentAmt = (double)parentProperty.GetValue(_parentForm, null);
            parentProperty.SetValue(_parentForm, parentAmt - amountToAdjust * 100, null);
          }
          parentTxtBox = (NumberTextBox)_parentForm.Controls.Find(ControlNames.TxtAvailableBalance, true).FirstOrDefault();

          if (parentTxtBox != null)
            parentAmt = double.Parse(parentTxtBox.Text);
          totalParentAmt = parentAmt + amountToAdjust;

          if (parentTxtBox != null)
            parentTxtBox.Text = (totalParentAmt * parentTxtBox.DividedBy).ToString(CultureInfo.InvariantCulture);
          parentTxtBox = (NumberTextBox)_parentForm.Controls.Find(ControlNames.TxtPendingBets, true).FirstOrDefault();

          if (parentTxtBox != null)
            parentAmt = double.Parse(parentTxtBox.Text);
          totalParentAmt = parentAmt - amountToAdjust;

          if (parentTxtBox != null)
            parentTxtBox.Text = (totalParentAmt * parentTxtBox.DividedBy).ToString(CultureInfo.InvariantCulture);
          // Update PendingWagerBalance and PendingWagerCount
        }
        else {
          parentProperty = _parentForm.GetType().GetProperty(PropertyNames.CustomerCurrentBalance);

          if (parentProperty != null) {
            parentAmt = (double)parentProperty.GetValue(_parentForm, null);

            if (outcome == "WIN")
              parentProperty.SetValue(_parentForm, parentAmt - amountToAdjust * 100, null);
            else
              parentProperty.SetValue(_parentForm, parentAmt + amountToAdjust * 100, null);
          }
          parentTxtBox = (NumberTextBox)_parentForm.Controls.Find(ControlNames.TxtAvailableBalance, true).FirstOrDefault();

          if (parentTxtBox != null) {
            parentAmt = double.Parse(parentTxtBox.Text);

            if (outcome == "WIN")
              totalParentAmt = parentAmt - amountToAdjust;
            else
              totalParentAmt = parentAmt + amountToAdjust;
            parentTxtBox.Text = (totalParentAmt * parentTxtBox.DividedBy).ToString(CultureInfo.InvariantCulture);
          }
          ReloadCustomerTransactionsGrid();
        }
      }
    }

    private void FillTicketDetailsDgrvw(String selectedText) {
      WriteTicketDetailsGrvwHeaders();
      List<spTkWGetCustomerTicketInformation_Result> objResult;

      using (var tAw = new TicketsAndWagers(AppModuleInfo)) {
        switch (selectedText) {
          case TranFilterOption.WithPendingOpenWagers:
            objResult = tAw.GetCustomerTicketInformation(_currentCustomerId, 0, "pendingOpenWagers", _ticketNumber);
            break;
          case TranFilterOption.WithOpenParlaysTeasers:
            objResult = tAw.GetCustomerTicketInformation(_currentCustomerId, 0, "OpenParlayTeasers", _ticketNumber);
            break;
          case TranFilterOption.OpenParlaysTeasersOnly:
            objResult = tAw.GetCustomerTicketInformation(_currentCustomerId, 0, "OpenParlayTeasersOnly", _ticketNumber);
            break;
          case TranFilterOption.PendingOnly:
            objResult = tAw.GetCustomerTicketInformation(_currentCustomerId, 0, "pendingOnly", _ticketNumber);
            break;
          case TranFilterOption.AvailableForRollingIfs:
            objResult = tAw.GetCustomerTicketInformation(_currentCustomerId, 0, "AvailableRollingIfs", _ticketNumber);
            break;
          case TranFilterOption.PendingOpenWagersOnly:
            objResult = tAw.GetCustomerTicketInformation(_currentCustomerId, 0, "pendingOpenWagersOnly", _ticketNumber);
            break;
          case TranFilterOption.Last2Days:
            objResult = tAw.GetCustomerTicketInformation(_currentCustomerId, 2, null, _ticketNumber);
            break;
          case TranFilterOption.Last7Days:
            objResult = tAw.GetCustomerTicketInformation(_currentCustomerId, 7, null, _ticketNumber);
            break;
          case TranFilterOption.Last14Days:
            objResult = tAw.GetCustomerTicketInformation(_currentCustomerId, 14, null, _ticketNumber);
            break;
          case TranFilterOption.Last30Days:
            objResult = tAw.GetCustomerTicketInformation(_currentCustomerId, 30, null, _ticketNumber);
            break;
          case TranFilterOption.SpecificWager:
          case TranFilterOption.SpecificTicket:
            var daysAgo = GetStartingTicketsDate();
            objResult = tAw.GetCustomerTicketInformation(_currentCustomerId, daysAgo + 1, "specificTicket", _ticketNumber);
            _ticketNumber = null;
            break;
          default:
            objResult = tAw.GetCustomerTicketInformation(_currentCustomerId, 9999999, null, _ticketNumber);
            break;
        }
      }
      var rCnt = 0;
      var selectedRow = 0;
      var rowSelected = false;

      if (objResult != null && objResult.Count != 0) {

        foreach (var res in objResult) {
          if (res.TicketNumber == 0)
            continue;
          string ticketDescription;
          if (int.Parse(res.PlayCount.ToString()) == 0) {
            ticketDescription = "No Plays Accepted";
          }

          else {
            if (int.Parse(res.PlayCount.ToString()) == 1) {
              ticketDescription = res.PlayCount + " Play";
            }
            else {
              ticketDescription = res.PlayCount + " Plays";
            }

            if (int.Parse(res.PendingPlaysCount.ToString()) > 0 && int.Parse(res.OpenPlaysCount.ToString()) > 0) {
              ticketDescription += " (open and pending items)";
            }
            else {
              if (int.Parse(res.PendingPlaysCount.ToString()) > 0) {
                if (res.PendingPlaysCount == 1) {
                  ticketDescription += " (pending item)";
                }
                else
                  ticketDescription += " (pending items)";
              }
              else {
                if (res.OpenPlaysCount == 1) {
                  ticketDescription += " (open item)";
                }
                else if (res.OpenPlaysCount > 1)
                  ticketDescription += " (open items)";
              }
            }
          }

          var ticketPlacedDateTime = DateTime.Parse(res.PostedDateTime.ToString());

          string amount;

          if (res.FreePlayFlag == "Y") {
            amount = "(free play)";
            if (selectedText == "Pending/Open Wagers Only") {
              if (res.OpenPlaysCount == 0 && res.PendingPlaysCount == 0)
                amount = "";
            }
            dgrvwTicketsDetails.Rows.Add(res.TicketNumber.ToString(CultureInfo.InvariantCulture), DisplayDate.Format(ticketPlacedDateTime, DisplayDateFormat.LongDateTime) + " - " + ticketPlacedDateTime.DayOfWeek, ticketDescription, amount, res.TicketWriter, ticketPlacedDateTime.ToString(CultureInfo.InvariantCulture));
          }

          else {
            amount = res.Amount.ToString();
            if (selectedText == "Pending/Open Wagers Only") {
              if (res.OpenPlaysCount == 0 && res.PendingPlaysCount == 0)
                amount = "";
              dgrvwTicketsDetails.Rows.Add(res.TicketNumber.ToString(CultureInfo.InvariantCulture), DisplayDate.Format(ticketPlacedDateTime, DisplayDateFormat.LongDateTime) + " - " + ticketPlacedDateTime.DayOfWeek, ticketDescription, amount, res.TicketWriter, ticketPlacedDateTime.ToString(CultureInfo.InvariantCulture));
            }
            else {
              dgrvwTicketsDetails.Rows.Add(res.TicketNumber.ToString(CultureInfo.InvariantCulture), DisplayDate.Format(ticketPlacedDateTime, DisplayDateFormat.LongDateTime) + " - " + ticketPlacedDateTime.DayOfWeek, ticketDescription, FormatNumber(double.Parse(amount == "" ? "0" : amount), false, 1, false), res.TicketWriter, ticketPlacedDateTime.ToString(CultureInfo.InvariantCulture));
            }
          }
          dgrvwTicketsDetails.Rows[dgrvwTicketsDetails.Rows.Count - 1].Height = GRIDVIEWROWHEIGHT;


          if (int.Parse(res.TicketNumber.ToString(CultureInfo.InvariantCulture)) == _currentTicketNumber) {
            //dgrvwTicketsDetails.Rows[rCnt].Selected = true;
            //dgrvwTicketsDetails.FirstDisplayedScrollingRowIndex = rCnt;
            selectedRow = rCnt;
            rowSelected = true;
          }

          if (res.DiscrepancyFlag == "Y") {
            dgrvwTicketsDetails.Rows[rCnt].DefaultCellStyle.BackColor = Color.Beige;
          }

          rCnt++;
        }

        if (!rowSelected) {
          dgrvwTicketsDetails.Rows[rCnt - 1].Selected = true;
          dgrvwTicketsDetails.FirstDisplayedScrollingRowIndex = rCnt - 1;
        }
        else {
          dgrvwTicketsDetails.Rows[selectedRow].Selected = true;
          dgrvwTicketsDetails.FirstDisplayedScrollingRowIndex = selectedRow;
        }

      }
      dgrvwTicketsDetails.Select();

      FormF.PreventDataGridViewColumnsToBeSortedAndResized(dgrvwTicketsDetails);

      if (dgrvwTicketsDetails.Rows.Count == 1) {
        LoadWagerDetailsInfo(dgrvwTicketsDetails);
      }
    }

    private int GetStartingTicketsDate() {
      var numOfDays = 0;
      var refNumber = _currentGradeNumber == 0 ? _currentDocumentNumber : _currentGradeNumber;

      using (var tAw = new TicketsAndWagers(AppModuleInfo)) {
        var ticketNumAndDate = tAw.GetCustomerTicketNumberByIdRefNum(_currentTranDateTime, refNumber).FirstOrDefault();

        if (ticketNumAndDate == null)
          return numOfDays;
        _currentTicketNumber = int.Parse(ticketNumAndDate.TicketNumber.ToString(CultureInfo.InvariantCulture));
        _currentTicketPlacedDateTime = DateTime.Parse(ticketNumAndDate.PostedDateTime.ToString());
        _currentWagerNumber = int.Parse(ticketNumAndDate.WagerNumber.ToString(CultureInfo.InvariantCulture));
        var ts = DateTime.Now - _currentTicketPlacedDateTime;
        numOfDays = ts.Days;
      }
      return numOfDays;
    }

    private void WriteTicketDetailsGrvwHeaders() {
      dgrvwTicketsDetails.Columns.Clear();
      dgrvwTicketsDetails.Columns.AddRange(new DataGridViewTextBoxColumn {
        HeaderText = @"ticket #",
        Name = ColumnNames.TicketNumber2,
        Width = 100
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"Posted Date/Time",
        Name = ColumnNames.PostedDateTime,
        Width = 200
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"Description",
        Name = ColumnNames.Description,
        Width = 150
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"Amount",
        Name = ColumnNames.Amount,
        Width = 100,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleCenter }
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"written By",
        Name = ColumnNames.WrittenBy,
        Width = 275
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"Posted Date/Time",
        Name = ColumnNames.PostedDateTime2,
        Width = 100,
        Visible = false
      });
    }

    private void FillListedTicketsDropDown(out String selectedDefaultItem) {
      if (cbxTicketsDateRange.Items.Count > 0)
        cbxTicketsDateRange.Items.Clear();
      var ddItems = new List<object>();

      switch (_parentForm.Name) {
        default:
          ddItems.AddRange(new[] { TranFilterOption.All, TranFilterOption.WithPendingOpenWagers, TranFilterOption.Last2Days, TranFilterOption.Last7Days,
                                             TranFilterOption.Last14Days, TranFilterOption.Last30Days, TranFilterOption.WithOpenParlaysTeasers, TranFilterOption.SpecificTicket });
          selectedDefaultItem = TranFilterOption.Last2Days;
          break;
        case FormNames.MdiTicketWriter:
          ddItems.AddRange(new[] { TranFilterOption.Last2Days, TranFilterOption.Last7Days, TranFilterOption.Last14Days, TranFilterOption.Last30Days,
                                             TranFilterOption.OpenParlaysTeasersOnly, TranFilterOption.PendingOnly, TranFilterOption.AvailableForRollingIfs, TranFilterOption.SpecificTicket });
          selectedDefaultItem = TranFilterOption.Last2Days;
          break;
        case FormNames.FrmCustomerTransactions:
          ddItems.AddRange(new[] { TranFilterOption.PendingOpenWagersOnly, TranFilterOption.Last2Days, TranFilterOption.Last7Days, TranFilterOption.Last14Days,
                                             TranFilterOption.Last30Days, TranFilterOption.OpenParlaysTeasersOnly, TranFilterOption.PendingOnly, TranFilterOption.SpecificTicket });
          selectedDefaultItem = TranFilterOption.SpecificWager; // this selection is not present as option in the dd. Is it OK? DMN 20141216
          break;
      }
      foreach (var item in ddItems)
        cbxTicketsDateRange.Items.Add(item);
    }

    private void GoToWagerDetails() {
      if (dgrvwWagersDetails.SelectedRows.Count == 0)
        return;
      if (dgrvwTicketsDetails.SelectedRows.Count <= 0) {
        MessageBox.Show(@"Select wager to display details");
        return;
      }
      var wagerType = dgrvwWagersDetails.SelectedRows[0].Cells[ColumnNames.Type].Value.ToString();

      if (wagerType == "X")
        MessageBox.Show(@"No additional details available");
      else {
        int? rrItemsCnt = null;

        if (dgrvwWagersDetails.SelectedRows[0].Cells[ColumnNames.Type].Value.ToString() == "Round Robin") {
          rrItemsCnt = (from r in _resultSet where r.PlayNumber == int.Parse(dgrvwWagersDetails.SelectedRows[0].Cells[ColumnNames.Number].Value.ToString()) select r).Count();
        }

        WagerDetailsFrm = new FrmCustomerWagerDetails(this, _parentForm, int.Parse(dgrvwWagersDetails.SelectedRows[0].Cells[ColumnNames.TicketNumber].Value.ToString()),
                                                      DateTime.Parse(dgrvwTicketsDetails.SelectedRows[0].Cells[ColumnNames.PostedDateTime2].Value.ToString()),
                                                      int.Parse(dgrvwWagersDetails.SelectedRows[0].Cells[ColumnNames.WagerNumber].Value.ToString()),
                                                      dgrvwWagersDetails.SelectedRows[0].Cells[ColumnNames.Number].Value.ToString(), wagerType, _customerCurrentCurrency,
                                                      _currentUserPermissions, _machineHasSoundCard, rrItemsCnt, OpenPlaysMode, EnableToEditPendingPlays) {
                                                        RollingIfBetMode = RollingIfBetMode,
                                                        Icon = Icon
                                                      };

        WagerDetailsFrm.ShowDialog();
      }
    }

    private void ReloadCustomerTransactionsGrid() {
      var cbxIdx = -1;
      var cbx = (ComboBox)_parentForm.Controls.Find(ControlNames.CmbDisplay, true).FirstOrDefault();

      if (cbx != null)
        cbxIdx = cbx.SelectedIndex;
      MethodInfo method;
      object[] parametersArray;

      if (cbxIdx > -1) {
        method = _parentForm.GetType().GetMethod(MethodNames.PopulateCustomerTransactionsGv, BindingFlags.Public |
        BindingFlags.Instance);
        parametersArray = new object[] { cbxIdx };
        if (method != null)
          method.Invoke(_parentForm, parametersArray);
      }
      cbx = (ComboBox)_parentForm.Controls.Find(ControlNames.CmbFreePlayDisplay, true).FirstOrDefault();
      cbxIdx = -1;

      if (cbx != null)
        cbxIdx = cbx.SelectedIndex;
      if (cbxIdx <= -1)
        return;
      method = _parentForm.GetType().GetMethod(MethodNames.PopulateCustomerFreePlaysTransactionsGv, BindingFlags.Public | BindingFlags.Instance);
      parametersArray = new object[] { cbxIdx };
      if (method != null)
        method.Invoke(_parentForm, parametersArray);
    }

    #endregion

    #region Events

    private void frmTicketInformation_Load(object sender, EventArgs e) {
      txtDailyFiguresCurrencyLegend.Text = _customerCurrentCurrency;

      if (_ticketNumber == null) {
        string defaultItem;
        FillListedTicketsDropDown(out defaultItem);
        //cbxTicketsDateRange.SelectedIndex = defaultIdx;

        if (_currentDocumentNumber != 0 || _currentGradeNumber != 0) {
          cbxTicketsDateRange.SelectedIndex = GetSelectedIndex("");
          FillTicketDetailsDgrvw(TranFilterOption.SpecificWager);
        }
        else {
          if (RollingIfBetMode) {
            cbxTicketsDateRange.SelectedIndex = GetSelectedIndex(TranFilterOption.AvailableForRollingIfs);
            btnPlaybackConversation.Enabled = false;
          }
          else
            if (OpenPlaysMode) {
              cbxTicketsDateRange.SelectedIndex = GetSelectedIndex(TranFilterOption.OpenParlaysTeasersOnly);
            }
            else {
              cbxTicketsDateRange.SelectedIndex = GetSelectedIndex(defaultItem); // what if default item does not exist in selection? DMN 20141216
            }
        }
        _currentDateRangeIdx = cbxTicketsDateRange.SelectedIndex;
        ChangeButtonsToEnabled();
      }
      else {
        cbxTicketsDateRange.Enabled = panSearchTicket.Visible = false;
        if (!LoginsAndProfiles.ValidateUserFunctionalityAccess(_currentUserPermissions, LoginsAndProfiles.PLAYBACK_AUDIO) || _machineHasSoundCard == false)
          btnPlaybackConversation.Enabled = false;
        FillTicketDetailsDgrvw(TranFilterOption.SpecificTicket);
      }
    }

    private int GetSelectedIndex(string itemText) {
      var idx = -1;

      for (var i = 0; i < cbxTicketsDateRange.Items.Count; ++i) {
        var text = cbxTicketsDateRange.Items[i].ToString();

        if (text != itemText)
          continue;
        idx = i;
        break;
      }
      return idx;
    }

    private void ChangeButtonsToEnabled() {
      if (!LoginsAndProfiles.ValidateUserFunctionalityAccess(_currentUserPermissions, LoginsAndProfiles.DELETE_WAGERS) || RollingIfBetMode)
        btnDeleteWager.Enabled = false;
      if (_parentForm.Name == FormNames.MdiTicketWriter || _parentForm.Name == FormNames.FrmCustomerTransactions)
        btnDeleteWager.Visible = false;
      if (!LoginsAndProfiles.ValidateUserFunctionalityAccess(_currentUserPermissions, LoginsAndProfiles.PLAYBACK_AUDIO) || _machineHasSoundCard == false)
        btnPlaybackConversation.Enabled = false;
    }

    private void cbxTicketsDateRange_SelectedValueChanged(object sender, EventArgs e) {

      var selectedText = ((ComboBox)sender).SelectedItem.ToString();
      /*  selectedIdx Values
             
      0 = All
      1 = With Pending/Open Wagers
      2 = Last 2 Days
      3 = Last 7 Days
      4 = Last 14 Days
      5 = Last 30 Days
      6 = With Open Parlays/Teasers
            7 = Available for Rolling Ifs 
      8 = SpecificTicket
       */

      if (selectedText == "Specific Ticket" && (ntxtTicketNumber.TextLength == 0 || (ntxtTicketNumber.TextLength > 0 && (!NumberF.IsValidNumber(ntxtTicketNumber.Text, false))))) {
        if (!_keyDownWasUsed) {
        MessageBox.Show(@"Type In A Valid TicketNumber First And Then Select this Option Again", @"Suggestion", MessageBoxButtons.OK, MessageBoxIcon.Information);
        ntxtTicketNumber.Focus();
         } 
        ((ComboBox)sender).SelectedValueChanged -= cbxTicketsDateRange_SelectedValueChanged;
        ((ComboBox)sender).SelectedIndex = _currentDateRangeIdx;
        ((ComboBox)sender).SelectedValueChanged += cbxTicketsDateRange_SelectedValueChanged;
        return;
         
      }
      if (selectedText == "Specific Ticket" && !_keyDownWasUsed) {
        _ticketNumber = int.Parse(ntxtTicketNumber.Text);
      }

      FillTicketDetailsDgrvw(selectedText);

      if (dgrvwTicketsDetails.Rows.Count == 0) {
        btnPlaybackConversation.Enabled = false;
        btnTicketComments.Enabled = false;
      }
      else {
        btnPlaybackConversation.Enabled = true;
        btnTicketComments.Enabled = true;

        if (selectedText == TranFilterOption.OpenParlaysTeasersOnly) {
          OpenPlaysMode = true;
          EnableToEditPendingPlays = false;
        }
        else {
          OpenPlaysMode = false;
          EnableToEditPendingPlays = true;
        }
      }
      ntxtTicketNumber.Text = "";
      _keyDownWasUsed = false;
    }

    private void btnPlaybackConversation_Click(object sender, EventArgs e) {
      if (dgrvwTicketsDetails.SelectedRows.Count == 0)
        return;
      var playTicketAudio = new TicketDr(AppModuleInfo, int.Parse(dgrvwTicketsDetails.SelectedRows[0].Cells[ColumnNames.TicketNumber2].Value.ToString())) {
        TicketPostedDateTime = DateTime.Parse(dgrvwTicketsDetails.SelectedRows[0].Cells[ColumnNames.PostedDateTime2].Value.ToString())
      };
      if (playTicketAudio.TicketNumber > 0 && playTicketAudio.TicketPostedDateTime.ToString(CultureInfo.InvariantCulture).Length > 0)
        playTicketAudio.PrepareFileforPlayback();
    }

    private void btnTicketComments_Click(object sender, EventArgs e) {
      if (dgrvwTicketsDetails.SelectedRows.Count <= 0)
        return;
      var ticketNumber = int.Parse(dgrvwTicketsDetails.SelectedRows[0].Cells[ColumnNames.TicketNumber2].Value.ToString()); // was selectedcells. Check if it keeps working DMN 20141216
      var ticketComments = new FrmCustomerTicketComments(ticketNumber, AppModuleInfo) { Icon = Icon };
      ticketComments.ShowDialog();
    }

    private void btnViewInetLog_Click(object sender, EventArgs e) {
      if (dgrvwWagersDetails.SelectedRows.Count == 0)
        return;
      var inetSessionId = (dgrvwWagersDetails.SelectedRows[0].Cells[13].Value ?? "").ToString();

      using (var frm = new FrmInetLogReport(AppModuleInfo, inetSessionId)) {
        frm.ShowDialog();
      }

    }

    private void dgrvwTicketsDetails_SelectionChanged(object sender, EventArgs e) {
      LoadWagerDetailsInfo(sender);
    }

    private void LoadWagerDetailsInfo(object sender) {
      dgrvwWagersDetails.DataSource = null;
      dgrvwWagersDetails.Rows.Clear();

      if (((DataGridView)sender).SelectedRows.Count > 0) {
        var ticketNumber = int.Parse((((DataGridView)sender).SelectedCells[0].Value.ToString()));
        var ticketPostedDateTime = DateTime.Parse((((DataGridView)sender).SelectedCells[5].Value.ToString()));
        PopulateWagersDetailsDgvw(ticketNumber, ticketPostedDateTime);
      }
      ChangeButtonsToEnabled();
    }

    private void btnWagerDetails_Click(object sender, EventArgs e) {
      GoToWagerDetails();
    }

    private void dgrvwWagersDetails_KeyDown(object sender, KeyEventArgs e) {
      if (e.KeyCode != Keys.Enter)
        return;
      e.SuppressKeyPress = true;

      if (((DataGridView)sender).SelectedRows.Count > 0)
        GoToWagerDetails();
    }

    private void dgrvwWagersDetails_CellDoubleClick(object sender, DataGridViewCellEventArgs e) {
      GoToWagerDetails();
    }

    private void dgrvwWagersDetails_RowEnter(object sender, DataGridViewCellEventArgs e) {
      btnWagerDetails.Enabled = ((DataGridView)sender).Rows[e.RowIndex].Cells[9].Value.ToString() != "G";
      OpenPlaysMode = ((DataGridView)sender).Rows[e.RowIndex].Cells[4].Value != null && ((DataGridView)sender).Rows[e.RowIndex].Cells[4].Value.ToString() == "Open";
      btnViewInetLog.Enabled = ((DataGridView)sender).Rows[e.RowIndex].Cells[13].Value != null;

      if (!LoginsAndProfiles.ValidateUserFunctionalityAccess(_currentUserPermissions, LoginsAndProfiles.DELETE_WAGERS)
          && !LoginsAndProfiles.ValidateUserFunctionalityAccess(_currentUserPermissions, LoginsAndProfiles.DELETE_WAGERS_AFTER_POST))
        btnDeleteWager.Enabled = false;
      else
        btnDeleteWager.Enabled = true;
      btnWagerComments.Enabled = true;
    }

    private void btnWagerComments_Click(object sender, EventArgs e) {
      if (dgrvwWagersDetails.SelectedRows.Count == 0)
        return;
      if (dgrvwTicketsDetails.SelectedRows.Count <= 0) {
        MessageBox.Show(@"Select wager to display details");
        return;
      }
      var ticketNumber = int.Parse(dgrvwWagersDetails.SelectedRows[0].Cells[ColumnNames.TicketNumber].Value.ToString());
      var wagerNumber = int.Parse(dgrvwWagersDetails.SelectedRows[0].Cells[ColumnNames.WagerNumber].Value.ToString());

      if (wagerNumber == 0)
        wagerNumber = 1;
      var wagerCommentsFrm = new FrmCustomerWagerComments(ticketNumber, wagerNumber, AppModuleInfo) { Icon = Icon };

      wagerCommentsFrm.ShowDialog();

    }

    private void btnDeleteWager_Click(object sender, EventArgs e) {
      HandleWagersDeletion();
    }

    private void HandleWagersDeletion() {
      if (dgrvwWagersDetails.SelectedRows.Count == 0)
        return;

      var userCanDeleteWagers = LoginsAndProfiles.ValidateUserFunctionalityAccess(_currentUserPermissions, LoginsAndProfiles.DELETE_WAGERS);
      var userCanDeleteWagersAfterPost = LoginsAndProfiles.ValidateUserFunctionalityAccess(_currentUserPermissions, LoginsAndProfiles.DELETE_WAGERS_AFTER_POST);
      if (userCanDeleteWagers || userCanDeleteWagersAfterPost) {
        if (dgrvwWagersDetails.SelectedRows.Count <= 0 || MessageBox.Show(@"Are you sure you want to delete this wager?", @"Delete wager", MessageBoxButtons.YesNo) != DialogResult.Yes)
          return;
        double amountWagered = 0;
        double amountWon = 0;
        var ticketNumber = 0;
        var wagerNumber = 0;
        List<spTkWGetCustomerHistoryWagerDetails_Result> subResultSet = null;
        var refreshGrid = true;

        if (dgrvwWagersDetails.SelectedRows[0].Cells[ColumnNames.Risk].Value != null && dgrvwWagersDetails.SelectedRows[0].Cells[ColumnNames.Risk].Value.ToString() != "")
          amountWagered = double.Parse(dgrvwWagersDetails.SelectedRows[0].Cells[ColumnNames.Risk].Value.ToString());
        if (dgrvwWagersDetails.SelectedRows[0].Cells[ColumnNames.Paid].Value != null && dgrvwWagersDetails.SelectedRows[0].Cells[ColumnNames.Paid].Value.ToString() != "")
          amountWon = double.Parse(dgrvwWagersDetails.SelectedRows[0].Cells[ColumnNames.Paid].Value.ToString());
        if (dgrvwWagersDetails.SelectedRows[0].Cells[ColumnNames.TicketNumber].Value != null && dgrvwWagersDetails.SelectedRows[0].Cells[ColumnNames.TicketNumber].Value.ToString() != "")
          ticketNumber = int.Parse(dgrvwWagersDetails.SelectedRows[0].Cells[ColumnNames.TicketNumber].Value.ToString());
        if (dgrvwWagersDetails.SelectedRows[0].Cells[ColumnNames.WagerNumber].Value != null && dgrvwWagersDetails.SelectedRows[0].Cells[ColumnNames.WagerNumber].Value.ToString() != "")
          wagerNumber = int.Parse(dgrvwWagersDetails.SelectedRows[0].Cells[ColumnNames.WagerNumber].Value.ToString());
        var outcome = dgrvwWagersDetails.SelectedRows[0].Cells[ColumnNames.Status].Value == null ? "" : dgrvwWagersDetails.SelectedRows[0].Cells[ColumnNames.Status].Value.ToString();
        var wagerType = dgrvwWagersDetails.SelectedRows[0].Cells[ColumnNames.WagerType2].Value == null ? "" : dgrvwWagersDetails.SelectedRows[0].Cells[ColumnNames.WagerType2].Value.ToString();
        var wagerTypeName = dgrvwWagersDetails.SelectedRows[0].Cells[ColumnNames.Type].Value == null ? "" : dgrvwWagersDetails.SelectedRows[0].Cells[ColumnNames.Type].Value.ToString();
        var freePlayFlag = dgrvwWagersDetails.SelectedRows[0].Cells[ColumnNames.IsFreePlay].Value == null ? "" : dgrvwWagersDetails.SelectedRows[0].Cells[ColumnNames.IsFreePlay].Value.ToString();
        int? complexBetLink = dgrvwWagersDetails.SelectedRows[0].Cells[ColumnNames.ComplexBetLink].Value != null ? int.Parse(dgrvwWagersDetails.SelectedRows[0].Cells[ColumnNames.ComplexBetLink].Value.ToString()) : 0;

        if (wagerType == WagerType.PARLAY || wagerType == WagerType.TEASER || wagerType == WagerType.IFBET) {
          if (wagerType == WagerType.PARLAY) {
            if (wagerNumber > 0)
              wagerTypeName = "Parlay";
            var roundRobinLink = (from r in _resultSet
                                  where r.WagerNumber == wagerNumber
                                  select r.RoundRobinLink).FirstOrDefault();
            if (roundRobinLink != null && roundRobinLink > 0) {
              wagerTypeName = "Round Robin";
              complexBetLink = roundRobinLink;
            }
          }
          if (wagerType == WagerType.IFBET) {
            if (wagerNumber > 0)
              wagerTypeName = "If Bet";
            var aRLink = (from r in _resultSet
                          where r.WagerNumber == wagerNumber
                          select r.ARLink).FirstOrDefault();
            if (aRLink != null && aRLink > 0) {
              wagerTypeName = "Action Reverse";
              complexBetLink = aRLink;
            }
            var aRBirdcageLink = (from r in _resultSet
                                  where r.WagerNumber == wagerNumber
                                  select r.ARBirdcageLink).FirstOrDefault();
            if (aRBirdcageLink != null && aRBirdcageLink > 0) {
              wagerTypeName = "AR Birdcage";
              complexBetLink = aRBirdcageLink;
            }
          }
        }
        var atLeastOneIsPost = false;

        using (var tAw = new TicketsAndWagers(AppModuleInfo)) {
          switch (wagerTypeName) {
            case "Straight":
            case "Parlay":
            case "Teaser":
            case "If Bet":
              atLeastOneIsPost = tAw.WagerIsPost(ticketNumber, wagerNumber, wagerType == "C" ? "Y" : "N");
              break;
            case "Round Robin":
            case "Action Reverse":
            case "AR Birdcage":
              subResultSet = new List<spTkWGetCustomerHistoryWagerDetails_Result>();

              switch (wagerTypeName) {
                case "Round Robin":
                  subResultSet = (from i in _resultSet where i.RoundRobinLink == complexBetLink select i).ToList();
                  break;
                case "Action Reverse":
                  subResultSet = (from i in _resultSet where i.ARLink == complexBetLink select i).ToList();
                  break;
                case "AR Birdcage":
                  subResultSet = (from i in _resultSet where i.ARBirdcageLink == complexBetLink select i).ToList();
                  break;
              }
              foreach (var resultItem in subResultSet) {
                atLeastOneIsPost = tAw.WagerIsPost(resultItem.TicketNumber, resultItem.WagerNumber, resultItem.WagerType == WagerType.CONTEST ? "Y" : "N");

                if (atLeastOneIsPost)
                  break;
              }
              break;
          }
          if (atLeastOneIsPost) {
            if (userCanDeleteWagersAfterPost) {
              if (MessageBox.Show(@"At least one event has already started. Are you sure you want to delete the wager?", @"Delete wager after post", MessageBoxButtons.YesNo) == DialogResult.Yes) {
                DeleteComplexBet(wagerTypeName, subResultSet, tAw, amountWagered, amountWon, ticketNumber, wagerNumber, wagerType, outcome, freePlayFlag);
              }
            }
            else {
              MessageBox.Show(Environment.UserName + @" does not have permissions to " + LoginsAndProfiles.DELETE_WAGERS_AFTER_POST);
              refreshGrid = false;
            }
          }
          else {
            DeleteComplexBet(wagerTypeName, subResultSet, tAw, amountWagered, amountWon, ticketNumber, wagerNumber, wagerType, outcome, freePlayFlag);
          }
        }
        if (!refreshGrid)
          return;
        dgrvwWagersDetails.Refresh();
        var selectedText = "";
        if (cbxTicketsDateRange.SelectedItem != null)
          selectedText = cbxTicketsDateRange.SelectedItem.ToString();
        FillTicketDetailsDgrvw(selectedText);
      }
      else
        MessageBox.Show(Environment.UserName + @" does not have permissions to " + LoginsAndProfiles.DELETE_WAGERS);
    }

    private void DeleteComplexBet(string wagerTypeName, List<spTkWGetCustomerHistoryWagerDetails_Result> subResultSet, TicketsAndWagers tAw, double amountWagered, double amountWon, int ticketNumber, int wagerNumber, string wagerType, string outcome, string freePlayFlag) {
      if ((wagerTypeName == "Round Robin" || wagerTypeName == "Action Reverse" || wagerTypeName == "AR Birdcage") && wagerNumber == 0) {
        //delete the entire complexBet
        foreach (var resultItem in subResultSet) {
          tAw.DeleteWager(_currentCustomerId, resultItem.TicketNumber, resultItem.WagerNumber, resultItem.WagerType, resultItem.WagerStatus);
          amountWagered = (resultItem.AmountWagered ?? 0);
          amountWon = (resultItem.AmountWon ?? 0);
          AdjustCustomerBalancesDisplay(resultItem.WagerStatus, resultItem.FreePlayFlag, amountWagered, amountWon);
        }
      }
      else {
        tAw.DeleteWager(_currentCustomerId, ticketNumber, wagerNumber, wagerType, outcome);
        //dgrvwWagersDetails.Rows[rowIndex].Visible = false;
        AdjustCustomerBalancesDisplay(outcome, freePlayFlag, amountWagered, amountWon);
      }
      ReloadTransactionsGrid = true;
    }

    private void btnClose_Click(object sender, EventArgs e) {
      Close();
    }

    private void frmTicketInformation_FormClosing(object sender, FormClosingEventArgs e) {
      if (!RollingIfBetMode || WagerDetailsFrm != null)
        return;
      var parentRollingIfChb = (CheckBox)_parentForm.Controls.Find(ControlNames.ChbRollingIf, true).FirstOrDefault();

      if (parentRollingIfChb != null)
        parentRollingIfChb.Checked = false;
    }

    private void cbxTicketsDateRange_Click(object sender, EventArgs e) {
      _currentDateRangeIdx = ((ComboBox)sender).SelectedIndex;
    }

    private void ntxtTicketNumber_KeyUp(object sender, KeyEventArgs e) {
      if (e.KeyData != (Keys.Control | Keys.V)) return;
      var textBox = sender as TextBox;
      if (textBox != null) textBox.Paste();
    }

    #endregion

    private void HandleFormKeyDownEvent(KeyEventArgs e)
    {
        try
        {
            if (e == null) return;

            switch (e.KeyCode)
            {
                case Keys.F5:
                    if (cbxTicketsDateRange == null) return;
                    _keyDownWasUsed = true;
                    if (cbxTicketsDateRange.SelectedIndex - 1 >= 0) cbxTicketsDateRange.SelectedIndex = cbxTicketsDateRange.SelectedIndex - 1;
                    else cbxTicketsDateRange.SelectedIndex = cbxTicketsDateRange.Items.Count - 1;
                    break;

                case Keys.F6:
                    _keyDownWasUsed = true;
                    if (cbxTicketsDateRange == null || cbxTicketsDateRange.SelectedIndex < 0) return;
                    if (cbxTicketsDateRange.SelectedIndex + 1 < cbxTicketsDateRange.Items.Count)
                        cbxTicketsDateRange.SelectedIndex = cbxTicketsDateRange.SelectedIndex + 1;
                    else cbxTicketsDateRange.SelectedIndex = 0;
                    break;
            }
        }
        catch (Exception ex)
        {
            Log(ex);
        }
    }

    private void FrmTicketInformation_KeyDown(object sender, KeyEventArgs e)
    {
    HandleFormKeyDownEvent(e);
    }
  }
}