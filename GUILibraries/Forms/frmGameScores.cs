﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using GUILibraries.Utilities;

namespace GUILibraries.Forms {

  public partial class FrmGameScores : SIDForm {

    private readonly int _currentGameNumber;
    private DateTime _currentGameDateTime;
    private readonly String _currentTeam1Id = "";
    private readonly String _currentTeam2Id = "";

    public FrmGameScores(int gameNumber, DateTime gameDateTime, String team1Id, String team2Id, ModuleInfo moduleInfo)
      : base(moduleInfo) {
      if (gameNumber > 0 && team1Id.Length > 0 && team2Id.Length > 0) {
        _currentGameNumber = gameNumber;
        _currentGameDateTime = gameDateTime;
        _currentTeam1Id = team1Id;
        _currentTeam2Id = team2Id;
        InitializeComponent();
      }
      else {
        MessageBox.Show(@"No Game or gameDateTime or team1ID or team2ID was selected");
        Close();
      }
    }

    private void GameScores_Load(object sender, EventArgs e) {
      FormatGameScoresHeader();
      ShowGameScores();
    }

    private void FormatGameScoresHeader() {
      var team1Id = "";

      if (_currentTeam1Id != null)
        team1Id = _currentTeam1Id.Trim();
      var team2Id = "";

      if (_currentTeam2Id != null)
        team2Id = _currentTeam2Id.Trim();
      txtGameScoresHeader.Text = team1Id + @" vs. " + team2Id + Environment.NewLine + @"Game Number: " + _currentGameNumber + Environment.NewLine
                               + @"Game Time: " + _currentGameDateTime.DayOfWeek + @" " + String.Format("{0:g}", _currentGameDateTime);
    }

    private void ShowGameScores() {
      List<spGLGetGameScores_Result> gameResults;

      using (var gAl = new GamesAndLines(AppModuleInfo))
        gameResults = gAl.GetGameScores(_currentGameNumber);
      if (gameResults == null)
        return;
      dgrvwGameScores.DataSource = null;
      dgrvwGameScores.Columns.AddRange(new DataGridViewTextBoxColumn {
        HeaderText = @"Period",
        AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill,
        Name = ColumnNames.Period,
        Width = 85
      }, new DataGridViewTextBoxColumn {
        HeaderText = _currentTeam1Id,
        AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill,
        Name = ColumnNames.Team1Id,
        Width = 145
      }, new DataGridViewTextBoxColumn {
        HeaderText = _currentTeam2Id,
        AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill,
        Name = ColumnNames.Team2Id,
        Width = 145
      });
      foreach (var res in gameResults)
        dgrvwGameScores.Rows.Add(res.PeriodDescription, res.Team1Score, res.Team2Score);
      FormF.PreventDataGridViewColumnsToBeSortedAndResized(dgrvwGameScores);
    }

    private void dgrvwGameScores_SelectionChanged(object sender, EventArgs e) {
      dgrvwGameScores.ClearSelection();
    }

    private void frmGameScores_KeyPress(object sender, KeyPressEventArgs e) {
      if (e.KeyChar == Convert.ToChar(Keys.Escape) || e.KeyChar == Convert.ToChar(Keys.Enter))
        Close();
    }

    private void btnExit_Click(object sender, EventArgs e) {
      Close();
    }
  }
}
