﻿namespace GUILibraries.Forms {
  partial class FrmCustomerWagerDetails {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
      this.grpTicket = new System.Windows.Forms.GroupBox();
      this.lblPostedText = new System.Windows.Forms.Label();
      this.panWagerRollingIf = new System.Windows.Forms.Panel();
      this.btnIfWinOrPush = new System.Windows.Forms.Button();
      this.btnIfWinOnly = new System.Windows.Forms.Button();
      this.lblAgent = new System.Windows.Forms.Label();
      this.lblTotalTicket = new System.Windows.Forms.Label();
      this.lblTicketNumber = new System.Windows.Forms.Label();
      this.lblWrittenBy = new System.Windows.Forms.Label();
      this.lblPosted = new System.Windows.Forms.Label();
      this.lblWrittenByText = new System.Windows.Forms.Label();
      this.lblTicketNumberText = new System.Windows.Forms.Label();
      this.lblTotalTicketText = new System.Windows.Forms.Label();
      this.lblAgentText = new System.Windows.Forms.Label();
      this.grpWager = new System.Windows.Forms.GroupBox();
      this.lblWagerLastEdited = new System.Windows.Forms.Label();
      this.dgvwWagerOutcome = new System.Windows.Forms.DataGridView();
      this.lblWonText = new System.Windows.Forms.Label();
      this.panRollingIf = new System.Windows.Forms.Panel();
      this.lblRolledForwardAmtText = new System.Windows.Forms.Label();
      this.btnRollingIf = new System.Windows.Forms.Button();
      this.lblRolledForward = new System.Windows.Forms.Label();
      this.lblLostText = new System.Windows.Forms.Label();
      this.panOpenPlay = new System.Windows.Forms.Panel();
      this.btnOpenPlay = new System.Windows.Forms.Button();
      this.lblStatusText = new System.Windows.Forms.Label();
      this.lblPaidText = new System.Windows.Forms.Label();
      this.lblWinLossRatioText = new System.Windows.Forms.Label();
      this.lblToWinText = new System.Windows.Forms.Label();
      this.lblRiskText = new System.Windows.Forms.Label();
      this.lblAcctText = new System.Windows.Forms.Label();
      this.lblTiesText = new System.Windows.Forms.Label();
      this.lblDocNumText = new System.Windows.Forms.Label();
      this.lblTypeText = new System.Windows.Forms.Label();
      this.lblAcct = new System.Windows.Forms.Label();
      this.lblTies = new System.Windows.Forms.Label();
      this.lblStatus = new System.Windows.Forms.Label();
      this.lblPaid = new System.Windows.Forms.Label();
      this.lblDocNum = new System.Windows.Forms.Label();
      this.lblWinLossRatio = new System.Windows.Forms.Label();
      this.lblWon = new System.Windows.Forms.Label();
      this.lblToWin = new System.Windows.Forms.Label();
      this.lblLost = new System.Windows.Forms.Label();
      this.lblRisk = new System.Windows.Forms.Label();
      this.lblType = new System.Windows.Forms.Label();
      this.lblGameContestOutcome = new System.Windows.Forms.Label();
      this.txtGameContestOutcome = new System.Windows.Forms.TextBox();
      this.btnGameScores = new System.Windows.Forms.Button();
      this.btnPlaybackConversation = new System.Windows.Forms.Button();
      this.panBotomButtons = new System.Windows.Forms.Panel();
      this.lblCustomerCurrencyLegendText = new System.Windows.Forms.Label();
      this.btnExit = new System.Windows.Forms.Button();
      this.grpTicket.SuspendLayout();
      this.panWagerRollingIf.SuspendLayout();
      this.grpWager.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgvwWagerOutcome)).BeginInit();
      this.panRollingIf.SuspendLayout();
      this.panOpenPlay.SuspendLayout();
      this.panBotomButtons.SuspendLayout();
      this.SuspendLayout();
      // 
      // grpTicket
      // 
      this.grpTicket.Controls.Add(this.lblPostedText);
      this.grpTicket.Controls.Add(this.panWagerRollingIf);
      this.grpTicket.Controls.Add(this.lblAgent);
      this.grpTicket.Controls.Add(this.lblTotalTicket);
      this.grpTicket.Controls.Add(this.lblTicketNumber);
      this.grpTicket.Controls.Add(this.lblWrittenBy);
      this.grpTicket.Controls.Add(this.lblPosted);
      this.grpTicket.Controls.Add(this.lblWrittenByText);
      this.grpTicket.Controls.Add(this.lblTicketNumberText);
      this.grpTicket.Controls.Add(this.lblTotalTicketText);
      this.grpTicket.Controls.Add(this.lblAgentText);
      this.grpTicket.Location = new System.Drawing.Point(13, 13);
      this.grpTicket.Name = "grpTicket";
      this.grpTicket.Size = new System.Drawing.Size(893, 100);
      this.grpTicket.TabIndex = 0;
      this.grpTicket.TabStop = false;
      this.grpTicket.Text = "Ticket";
      // 
      // lblPostedText
      // 
      this.lblPostedText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.lblPostedText.Location = new System.Drawing.Point(80, 13);
      this.lblPostedText.Name = "lblPostedText";
      this.lblPostedText.Size = new System.Drawing.Size(154, 20);
      this.lblPostedText.TabIndex = 12;
      this.lblPostedText.Text = "Posted:";
      this.lblPostedText.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
      // 
      // panWagerRollingIf
      // 
      this.panWagerRollingIf.Controls.Add(this.btnIfWinOrPush);
      this.panWagerRollingIf.Controls.Add(this.btnIfWinOnly);
      this.panWagerRollingIf.Location = new System.Drawing.Point(525, 53);
      this.panWagerRollingIf.Name = "panWagerRollingIf";
      this.panWagerRollingIf.Size = new System.Drawing.Size(219, 28);
      this.panWagerRollingIf.TabIndex = 11;
      this.panWagerRollingIf.Visible = false;
      // 
      // btnIfWinOrPush
      // 
      this.btnIfWinOrPush.Location = new System.Drawing.Point(116, 3);
      this.btnIfWinOrPush.Name = "btnIfWinOrPush";
      this.btnIfWinOrPush.Size = new System.Drawing.Size(101, 23);
      this.btnIfWinOrPush.TabIndex = 13;
      this.btnIfWinOrPush.Text = "If WIN Or PUSH";
      this.btnIfWinOrPush.UseVisualStyleBackColor = true;
      this.btnIfWinOrPush.Click += new System.EventHandler(this.btnIfWinOrPush_Click);
      // 
      // btnIfWinOnly
      // 
      this.btnIfWinOnly.Location = new System.Drawing.Point(3, 3);
      this.btnIfWinOnly.Name = "btnIfWinOnly";
      this.btnIfWinOnly.Size = new System.Drawing.Size(88, 23);
      this.btnIfWinOnly.TabIndex = 12;
      this.btnIfWinOnly.Text = "If WIN Only";
      this.btnIfWinOnly.UseVisualStyleBackColor = true;
      this.btnIfWinOnly.Click += new System.EventHandler(this.btnIfWinOnly_Click);
      // 
      // lblAgent
      // 
      this.lblAgent.AutoSize = true;
      this.lblAgent.Location = new System.Drawing.Point(489, 20);
      this.lblAgent.Name = "lblAgent";
      this.lblAgent.Size = new System.Drawing.Size(38, 13);
      this.lblAgent.TabIndex = 5;
      this.lblAgent.Text = "Agent:";
      // 
      // lblTotalTicket
      // 
      this.lblTotalTicket.AutoSize = true;
      this.lblTotalTicket.Location = new System.Drawing.Point(276, 56);
      this.lblTotalTicket.Name = "lblTotalTicket";
      this.lblTotalTicket.Size = new System.Drawing.Size(67, 13);
      this.lblTotalTicket.TabIndex = 9;
      this.lblTotalTicket.Text = "Total Ticket:";
      // 
      // lblTicketNumber
      // 
      this.lblTicketNumber.AutoSize = true;
      this.lblTicketNumber.Location = new System.Drawing.Point(276, 20);
      this.lblTicketNumber.Name = "lblTicketNumber";
      this.lblTicketNumber.Size = new System.Drawing.Size(50, 13);
      this.lblTicketNumber.TabIndex = 3;
      this.lblTicketNumber.Text = "Ticket #:";
      // 
      // lblWrittenBy
      // 
      this.lblWrittenBy.AutoSize = true;
      this.lblWrittenBy.Location = new System.Drawing.Point(16, 56);
      this.lblWrittenBy.Name = "lblWrittenBy";
      this.lblWrittenBy.Size = new System.Drawing.Size(59, 13);
      this.lblWrittenBy.TabIndex = 7;
      this.lblWrittenBy.Text = "Written By:";
      // 
      // lblPosted
      // 
      this.lblPosted.AutoSize = true;
      this.lblPosted.Location = new System.Drawing.Point(16, 20);
      this.lblPosted.Name = "lblPosted";
      this.lblPosted.Size = new System.Drawing.Size(43, 13);
      this.lblPosted.TabIndex = 1;
      this.lblPosted.Text = "Posted:";
      // 
      // lblWrittenByText
      // 
      this.lblWrittenByText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.lblWrittenByText.Location = new System.Drawing.Point(80, 49);
      this.lblWrittenByText.Name = "lblWrittenByText";
      this.lblWrittenByText.Size = new System.Drawing.Size(154, 20);
      this.lblWrittenByText.TabIndex = 12;
      this.lblWrittenByText.Text = "Written By:";
      this.lblWrittenByText.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
      // 
      // lblTicketNumberText
      // 
      this.lblTicketNumberText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.lblTicketNumberText.Location = new System.Drawing.Point(336, 13);
      this.lblTicketNumberText.Name = "lblTicketNumberText";
      this.lblTicketNumberText.Size = new System.Drawing.Size(105, 20);
      this.lblTicketNumberText.TabIndex = 12;
      this.lblTicketNumberText.Text = "Ticket #:";
      this.lblTicketNumberText.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
      // 
      // lblTotalTicketText
      // 
      this.lblTotalTicketText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.lblTotalTicketText.Location = new System.Drawing.Point(349, 49);
      this.lblTotalTicketText.Name = "lblTotalTicketText";
      this.lblTotalTicketText.Size = new System.Drawing.Size(105, 20);
      this.lblTotalTicketText.TabIndex = 12;
      this.lblTotalTicketText.Text = "Total Ticket:";
      this.lblTotalTicketText.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
      // 
      // lblAgentText
      // 
      this.lblAgentText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.lblAgentText.Location = new System.Drawing.Point(556, 13);
      this.lblAgentText.Name = "lblAgentText";
      this.lblAgentText.Size = new System.Drawing.Size(105, 20);
      this.lblAgentText.TabIndex = 12;
      this.lblAgentText.Text = "Agent:";
      this.lblAgentText.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
      // 
      // grpWager
      // 
      this.grpWager.Controls.Add(this.lblWagerLastEdited);
      this.grpWager.Controls.Add(this.dgvwWagerOutcome);
      this.grpWager.Controls.Add(this.lblWonText);
      this.grpWager.Controls.Add(this.panRollingIf);
      this.grpWager.Controls.Add(this.lblLostText);
      this.grpWager.Controls.Add(this.panOpenPlay);
      this.grpWager.Controls.Add(this.lblStatusText);
      this.grpWager.Controls.Add(this.lblPaidText);
      this.grpWager.Controls.Add(this.lblWinLossRatioText);
      this.grpWager.Controls.Add(this.lblToWinText);
      this.grpWager.Controls.Add(this.lblRiskText);
      this.grpWager.Controls.Add(this.lblAcctText);
      this.grpWager.Controls.Add(this.lblTiesText);
      this.grpWager.Controls.Add(this.lblDocNumText);
      this.grpWager.Controls.Add(this.lblTypeText);
      this.grpWager.Controls.Add(this.lblAcct);
      this.grpWager.Controls.Add(this.lblTies);
      this.grpWager.Controls.Add(this.lblStatus);
      this.grpWager.Controls.Add(this.lblPaid);
      this.grpWager.Controls.Add(this.lblDocNum);
      this.grpWager.Controls.Add(this.lblWinLossRatio);
      this.grpWager.Controls.Add(this.lblWon);
      this.grpWager.Controls.Add(this.lblToWin);
      this.grpWager.Controls.Add(this.lblLost);
      this.grpWager.Controls.Add(this.lblRisk);
      this.grpWager.Controls.Add(this.lblType);
      this.grpWager.Location = new System.Drawing.Point(13, 125);
      this.grpWager.Name = "grpWager";
      this.grpWager.Size = new System.Drawing.Size(893, 273);
      this.grpWager.TabIndex = 20;
      this.grpWager.TabStop = false;
      this.grpWager.Text = "Wager";
      // 
      // lblWagerLastEdited
      // 
      this.lblWagerLastEdited.AutoSize = true;
      this.lblWagerLastEdited.Location = new System.Drawing.Point(22, 108);
      this.lblWagerLastEdited.Name = "lblWagerLastEdited";
      this.lblWagerLastEdited.Size = new System.Drawing.Size(37, 13);
      this.lblWagerLastEdited.TabIndex = 72;
      this.lblWagerLastEdited.Text = "Doc#:";
      // 
      // dgvwWagerOutcome
      // 
      this.dgvwWagerOutcome.AllowUserToAddRows = false;
      this.dgvwWagerOutcome.AllowUserToDeleteRows = false;
      this.dgvwWagerOutcome.AllowUserToResizeRows = false;
      this.dgvwWagerOutcome.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgvwWagerOutcome.Location = new System.Drawing.Point(7, 142);
      this.dgvwWagerOutcome.MultiSelect = false;
      this.dgvwWagerOutcome.Name = "dgvwWagerOutcome";
      this.dgvwWagerOutcome.ReadOnly = true;
      this.dgvwWagerOutcome.RowHeadersVisible = false;
      this.dgvwWagerOutcome.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgvwWagerOutcome.Size = new System.Drawing.Size(880, 125);
      this.dgvwWagerOutcome.StandardTab = true;
      this.dgvwWagerOutcome.TabIndex = 70;
      this.dgvwWagerOutcome.SelectionChanged += new System.EventHandler(this.dgvwWagerOutcome_SelectionChanged);
      this.dgvwWagerOutcome.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvwWagerOutcome_KeyDown);
      // 
      // lblWonText
      // 
      this.lblWonText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.lblWonText.Location = new System.Drawing.Point(178, 79);
      this.lblWonText.Name = "lblWonText";
      this.lblWonText.Size = new System.Drawing.Size(64, 20);
      this.lblWonText.TabIndex = 71;
      this.lblWonText.Text = "Won:";
      this.lblWonText.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
      // 
      // panRollingIf
      // 
      this.panRollingIf.Controls.Add(this.lblRolledForwardAmtText);
      this.panRollingIf.Controls.Add(this.btnRollingIf);
      this.panRollingIf.Controls.Add(this.lblRolledForward);
      this.panRollingIf.Location = new System.Drawing.Point(449, 92);
      this.panRollingIf.Name = "panRollingIf";
      this.panRollingIf.Size = new System.Drawing.Size(415, 29);
      this.panRollingIf.TabIndex = 50;
      // 
      // lblRolledForwardAmtText
      // 
      this.lblRolledForwardAmtText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.lblRolledForwardAmtText.Location = new System.Drawing.Point(116, 5);
      this.lblRolledForwardAmtText.Name = "lblRolledForwardAmtText";
      this.lblRolledForwardAmtText.Size = new System.Drawing.Size(94, 20);
      this.lblRolledForwardAmtText.TabIndex = 56;
      this.lblRolledForwardAmtText.Text = "Rolled Forward:";
      this.lblRolledForwardAmtText.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
      // 
      // btnRollingIf
      // 
      this.btnRollingIf.Location = new System.Drawing.Point(230, 3);
      this.btnRollingIf.Name = "btnRollingIf";
      this.btnRollingIf.Size = new System.Drawing.Size(170, 23);
      this.btnRollingIf.TabIndex = 55;
      this.btnRollingIf.Text = "Rolling If Hookup";
      this.btnRollingIf.UseVisualStyleBackColor = true;
      this.btnRollingIf.Click += new System.EventHandler(this.btnRollingIf_Click);
      // 
      // lblRolledForward
      // 
      this.lblRolledForward.AutoSize = true;
      this.lblRolledForward.Location = new System.Drawing.Point(34, 7);
      this.lblRolledForward.Name = "lblRolledForward";
      this.lblRolledForward.Size = new System.Drawing.Size(81, 13);
      this.lblRolledForward.TabIndex = 51;
      this.lblRolledForward.Text = "Rolled Forward:";
      // 
      // lblLostText
      // 
      this.lblLostText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.lblLostText.Location = new System.Drawing.Point(57, 79);
      this.lblLostText.Name = "lblLostText";
      this.lblLostText.Size = new System.Drawing.Size(64, 20);
      this.lblLostText.TabIndex = 71;
      this.lblLostText.Text = "Lost";
      this.lblLostText.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
      // 
      // panOpenPlay
      // 
      this.panOpenPlay.Controls.Add(this.btnOpenPlay);
      this.panOpenPlay.Location = new System.Drawing.Point(655, 91);
      this.panOpenPlay.Name = "panOpenPlay";
      this.panOpenPlay.Size = new System.Drawing.Size(209, 28);
      this.panOpenPlay.TabIndex = 60;
      this.panOpenPlay.Visible = false;
      // 
      // btnOpenPlay
      // 
      this.btnOpenPlay.Location = new System.Drawing.Point(23, 3);
      this.btnOpenPlay.Name = "btnOpenPlay";
      this.btnOpenPlay.Size = new System.Drawing.Size(170, 23);
      this.btnOpenPlay.TabIndex = 61;
      this.btnOpenPlay.Text = "Open ";
      this.btnOpenPlay.UseVisualStyleBackColor = true;
      this.btnOpenPlay.Click += new System.EventHandler(this.btnOpenPlay_Click);
      // 
      // lblStatusText
      // 
      this.lblStatusText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.lblStatusText.Location = new System.Drawing.Point(731, 52);
      this.lblStatusText.Name = "lblStatusText";
      this.lblStatusText.Size = new System.Drawing.Size(133, 20);
      this.lblStatusText.TabIndex = 71;
      this.lblStatusText.Text = "Status:";
      this.lblStatusText.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
      // 
      // lblPaidText
      // 
      this.lblPaidText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.lblPaidText.Location = new System.Drawing.Point(544, 52);
      this.lblPaidText.Name = "lblPaidText";
      this.lblPaidText.Size = new System.Drawing.Size(94, 20);
      this.lblPaidText.TabIndex = 71;
      this.lblPaidText.Text = "Paid:";
      this.lblPaidText.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
      // 
      // lblWinLossRatioText
      // 
      this.lblWinLossRatioText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.lblWinLossRatioText.Location = new System.Drawing.Point(349, 52);
      this.lblWinLossRatioText.Name = "lblWinLossRatioText";
      this.lblWinLossRatioText.Size = new System.Drawing.Size(47, 20);
      this.lblWinLossRatioText.TabIndex = 71;
      this.lblWinLossRatioText.Text = "Win/Loss Ratio:";
      this.lblWinLossRatioText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // lblToWinText
      // 
      this.lblToWinText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.lblToWinText.Location = new System.Drawing.Point(178, 52);
      this.lblToWinText.Name = "lblToWinText";
      this.lblToWinText.Size = new System.Drawing.Size(64, 20);
      this.lblToWinText.TabIndex = 71;
      this.lblToWinText.Text = "To Win:";
      this.lblToWinText.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
      // 
      // lblRiskText
      // 
      this.lblRiskText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.lblRiskText.Location = new System.Drawing.Point(59, 52);
      this.lblRiskText.Name = "lblRiskText";
      this.lblRiskText.Size = new System.Drawing.Size(64, 20);
      this.lblRiskText.TabIndex = 71;
      this.lblRiskText.Text = "Risk:";
      this.lblRiskText.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
      // 
      // lblAcctText
      // 
      this.lblAcctText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.lblAcctText.Location = new System.Drawing.Point(785, 22);
      this.lblAcctText.Name = "lblAcctText";
      this.lblAcctText.Size = new System.Drawing.Size(79, 20);
      this.lblAcctText.TabIndex = 71;
      this.lblAcctText.Text = "Acct:";
      this.lblAcctText.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
      // 
      // lblTiesText
      // 
      this.lblTiesText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.lblTiesText.Location = new System.Drawing.Point(679, 22);
      this.lblTiesText.Name = "lblTiesText";
      this.lblTiesText.Size = new System.Drawing.Size(47, 20);
      this.lblTiesText.TabIndex = 71;
      this.lblTiesText.Text = "Ties:";
      this.lblTiesText.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
      // 
      // lblDocNumText
      // 
      this.lblDocNumText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.lblDocNumText.Location = new System.Drawing.Point(544, 25);
      this.lblDocNumText.Name = "lblDocNumText";
      this.lblDocNumText.Size = new System.Drawing.Size(94, 20);
      this.lblDocNumText.TabIndex = 71;
      this.lblDocNumText.Text = "Doc#:";
      this.lblDocNumText.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
      // 
      // lblTypeText
      // 
      this.lblTypeText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.lblTypeText.Location = new System.Drawing.Point(59, 25);
      this.lblTypeText.Name = "lblTypeText";
      this.lblTypeText.Size = new System.Drawing.Size(264, 20);
      this.lblTypeText.TabIndex = 71;
      this.lblTypeText.Text = "Type:";
      this.lblTypeText.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
      // 
      // lblAcct
      // 
      this.lblAcct.AutoSize = true;
      this.lblAcct.Location = new System.Drawing.Point(754, 29);
      this.lblAcct.Name = "lblAcct";
      this.lblAcct.Size = new System.Drawing.Size(32, 13);
      this.lblAcct.TabIndex = 27;
      this.lblAcct.Text = "Acct:";
      // 
      // lblTies
      // 
      this.lblTies.AutoSize = true;
      this.lblTies.Location = new System.Drawing.Point(643, 29);
      this.lblTies.Name = "lblTies";
      this.lblTies.Size = new System.Drawing.Size(30, 13);
      this.lblTies.TabIndex = 25;
      this.lblTies.Text = "Ties:";
      // 
      // lblStatus
      // 
      this.lblStatus.AutoSize = true;
      this.lblStatus.Location = new System.Drawing.Point(685, 59);
      this.lblStatus.Name = "lblStatus";
      this.lblStatus.Size = new System.Drawing.Size(40, 13);
      this.lblStatus.TabIndex = 37;
      this.lblStatus.Text = "Status:";
      // 
      // lblPaid
      // 
      this.lblPaid.AutoSize = true;
      this.lblPaid.Location = new System.Drawing.Point(507, 59);
      this.lblPaid.Name = "lblPaid";
      this.lblPaid.Size = new System.Drawing.Size(31, 13);
      this.lblPaid.TabIndex = 35;
      this.lblPaid.Text = "Paid:";
      // 
      // lblDocNum
      // 
      this.lblDocNum.AutoSize = true;
      this.lblDocNum.Location = new System.Drawing.Point(501, 29);
      this.lblDocNum.Name = "lblDocNum";
      this.lblDocNum.Size = new System.Drawing.Size(37, 13);
      this.lblDocNum.TabIndex = 23;
      this.lblDocNum.Text = "Doc#:";
      // 
      // lblWinLossRatio
      // 
      this.lblWinLossRatio.AutoSize = true;
      this.lblWinLossRatio.Location = new System.Drawing.Point(259, 59);
      this.lblWinLossRatio.Name = "lblWinLossRatio";
      this.lblWinLossRatio.Size = new System.Drawing.Size(84, 13);
      this.lblWinLossRatio.TabIndex = 33;
      this.lblWinLossRatio.Text = "Win/Loss Ratio:";
      // 
      // lblWon
      // 
      this.lblWon.AutoSize = true;
      this.lblWon.Location = new System.Drawing.Point(127, 86);
      this.lblWon.Name = "lblWon";
      this.lblWon.Size = new System.Drawing.Size(33, 13);
      this.lblWon.TabIndex = 41;
      this.lblWon.Text = "Won:";
      // 
      // lblToWin
      // 
      this.lblToWin.AutoSize = true;
      this.lblToWin.Location = new System.Drawing.Point(127, 59);
      this.lblToWin.Name = "lblToWin";
      this.lblToWin.Size = new System.Drawing.Size(45, 13);
      this.lblToWin.TabIndex = 31;
      this.lblToWin.Text = "To Win:";
      // 
      // lblLost
      // 
      this.lblLost.AutoSize = true;
      this.lblLost.Location = new System.Drawing.Point(19, 86);
      this.lblLost.Name = "lblLost";
      this.lblLost.Size = new System.Drawing.Size(27, 13);
      this.lblLost.TabIndex = 39;
      this.lblLost.Text = "Lost";
      // 
      // lblRisk
      // 
      this.lblRisk.AutoSize = true;
      this.lblRisk.Location = new System.Drawing.Point(19, 59);
      this.lblRisk.Name = "lblRisk";
      this.lblRisk.Size = new System.Drawing.Size(31, 13);
      this.lblRisk.TabIndex = 29;
      this.lblRisk.Text = "Risk:";
      // 
      // lblType
      // 
      this.lblType.AutoSize = true;
      this.lblType.Location = new System.Drawing.Point(19, 29);
      this.lblType.Name = "lblType";
      this.lblType.Size = new System.Drawing.Size(34, 13);
      this.lblType.TabIndex = 21;
      this.lblType.Text = "Type:";
      // 
      // lblGameContestOutcome
      // 
      this.lblGameContestOutcome.AutoSize = true;
      this.lblGameContestOutcome.Location = new System.Drawing.Point(13, 410);
      this.lblGameContestOutcome.Name = "lblGameContestOutcome";
      this.lblGameContestOutcome.Size = new System.Drawing.Size(123, 13);
      this.lblGameContestOutcome.TabIndex = 80;
      this.lblGameContestOutcome.Text = "Game Contest Outcome:";
      // 
      // txtGameContestOutcome
      // 
      this.txtGameContestOutcome.Location = new System.Drawing.Point(158, 410);
      this.txtGameContestOutcome.Multiline = true;
      this.txtGameContestOutcome.Name = "txtGameContestOutcome";
      this.txtGameContestOutcome.ReadOnly = true;
      this.txtGameContestOutcome.Size = new System.Drawing.Size(357, 102);
      this.txtGameContestOutcome.TabIndex = 90;
      this.txtGameContestOutcome.TabStop = false;
      // 
      // btnGameScores
      // 
      this.btnGameScores.Location = new System.Drawing.Point(10, 38);
      this.btnGameScores.Name = "btnGameScores";
      this.btnGameScores.Size = new System.Drawing.Size(223, 23);
      this.btnGameScores.TabIndex = 102;
      this.btnGameScores.Text = "Game Scores";
      this.btnGameScores.UseVisualStyleBackColor = true;
      this.btnGameScores.Click += new System.EventHandler(this.btnGameScores_Click);
      // 
      // btnPlaybackConversation
      // 
      this.btnPlaybackConversation.Location = new System.Drawing.Point(10, 67);
      this.btnPlaybackConversation.Name = "btnPlaybackConversation";
      this.btnPlaybackConversation.Size = new System.Drawing.Size(223, 23);
      this.btnPlaybackConversation.TabIndex = 103;
      this.btnPlaybackConversation.Text = "Playback Conversation";
      this.btnPlaybackConversation.UseVisualStyleBackColor = true;
      this.btnPlaybackConversation.Click += new System.EventHandler(this.btnPlaybackConversation_Click);
      // 
      // panBotomButtons
      // 
      this.panBotomButtons.Controls.Add(this.lblCustomerCurrencyLegendText);
      this.panBotomButtons.Controls.Add(this.btnGameScores);
      this.panBotomButtons.Controls.Add(this.btnPlaybackConversation);
      this.panBotomButtons.Location = new System.Drawing.Point(523, 412);
      this.panBotomButtons.Name = "panBotomButtons";
      this.panBotomButtons.Size = new System.Drawing.Size(276, 100);
      this.panBotomButtons.TabIndex = 100;
      // 
      // lblCustomerCurrencyLegendText
      // 
      this.lblCustomerCurrencyLegendText.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.lblCustomerCurrencyLegendText.Location = new System.Drawing.Point(10, 12);
      this.lblCustomerCurrencyLegendText.Name = "lblCustomerCurrencyLegendText";
      this.lblCustomerCurrencyLegendText.Size = new System.Drawing.Size(223, 20);
      this.lblCustomerCurrencyLegendText.TabIndex = 12;
      this.lblCustomerCurrencyLegendText.Text = "Currency:";
      // 
      // btnExit
      // 
      this.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnExit.Location = new System.Drawing.Point(831, 489);
      this.btnExit.Name = "btnExit";
      this.btnExit.Size = new System.Drawing.Size(75, 23);
      this.btnExit.TabIndex = 101;
      this.btnExit.Text = "Exit";
      this.btnExit.UseVisualStyleBackColor = true;
      this.btnExit.Visible = false;
      this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
      // 
      // FrmCustomerWagerDetails
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnExit;
      this.ClientSize = new System.Drawing.Size(949, 528);
      this.Controls.Add(this.btnExit);
      this.Controls.Add(this.panBotomButtons);
      this.Controls.Add(this.txtGameContestOutcome);
      this.Controls.Add(this.lblGameContestOutcome);
      this.Controls.Add(this.grpWager);
      this.Controls.Add(this.grpTicket);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.KeyPreview = true;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "FrmCustomerWagerDetails";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Customer Wager Details";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmCustomerWagerDetails_FormClosing);
      this.Load += new System.EventHandler(this.CustomerWagerDetails_Load);
      this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.frmCustomerWagerDetails_KeyPress);
      this.grpTicket.ResumeLayout(false);
      this.grpTicket.PerformLayout();
      this.panWagerRollingIf.ResumeLayout(false);
      this.grpWager.ResumeLayout(false);
      this.grpWager.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgvwWagerOutcome)).EndInit();
      this.panRollingIf.ResumeLayout(false);
      this.panRollingIf.PerformLayout();
      this.panOpenPlay.ResumeLayout(false);
      this.panBotomButtons.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.GroupBox grpTicket;
    private System.Windows.Forms.GroupBox grpWager;
    private System.Windows.Forms.Label lblAgent;
    private System.Windows.Forms.Label lblTotalTicket;
    private System.Windows.Forms.Label lblTicketNumber;
    private System.Windows.Forms.Label lblWrittenBy;
    private System.Windows.Forms.Label lblPosted;
    private System.Windows.Forms.Label lblAcct;
    private System.Windows.Forms.Label lblTies;
    private System.Windows.Forms.Label lblStatus;
    private System.Windows.Forms.Label lblPaid;
    private System.Windows.Forms.Label lblDocNum;
    private System.Windows.Forms.Label lblWinLossRatio;
    private System.Windows.Forms.Label lblWon;
    private System.Windows.Forms.Label lblToWin;
    private System.Windows.Forms.Label lblLost;
    private System.Windows.Forms.Label lblRisk;
    private System.Windows.Forms.Label lblType;
    private System.Windows.Forms.DataGridView dgvwWagerOutcome;
    private System.Windows.Forms.Label lblGameContestOutcome;
    private System.Windows.Forms.TextBox txtGameContestOutcome;
    private System.Windows.Forms.Button btnGameScores;
    private System.Windows.Forms.Button btnPlaybackConversation;
    private System.Windows.Forms.Panel panRollingIf;
    private System.Windows.Forms.Button btnRollingIf;
    private System.Windows.Forms.Label lblRolledForward;
    private System.Windows.Forms.Panel panWagerRollingIf;
    private System.Windows.Forms.Button btnIfWinOrPush;
    private System.Windows.Forms.Button btnIfWinOnly;
    private System.Windows.Forms.Panel panOpenPlay;
    private System.Windows.Forms.Button btnOpenPlay;
    private System.Windows.Forms.Panel panBotomButtons;
    private System.Windows.Forms.Label lblPostedText;
    private System.Windows.Forms.Label lblTicketNumberText;
    private System.Windows.Forms.Label lblAgentText;
    private System.Windows.Forms.Label lblWrittenByText;
    private System.Windows.Forms.Label lblTotalTicketText;
    private System.Windows.Forms.Label lblTypeText;
    private System.Windows.Forms.Label lblDocNumText;
    private System.Windows.Forms.Label lblTiesText;
    private System.Windows.Forms.Label lblAcctText;
    private System.Windows.Forms.Label lblRiskText;
    private System.Windows.Forms.Label lblToWinText;
    private System.Windows.Forms.Label lblWinLossRatioText;
    private System.Windows.Forms.Label lblPaidText;
    private System.Windows.Forms.Label lblStatusText;
    private System.Windows.Forms.Label lblLostText;
    private System.Windows.Forms.Label lblWonText;
    private System.Windows.Forms.Label lblRolledForwardAmtText;
    private System.Windows.Forms.Label lblCustomerCurrencyLegendText;
    private System.Windows.Forms.Button btnExit;
    private System.Windows.Forms.Label lblWagerLastEdited;
  }
}