﻿namespace GUILibraries.Forms
{
    partial class FrmCustomerCasinoPlays
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panNonPostedCasinoPlays = new System.Windows.Forms.Panel();
            this.dgvwCasinoPlays = new System.Windows.Forms.DataGridView();
            this.btnClose = new System.Windows.Forms.Button();
            this.panNonPostedCasinoPlays.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvwCasinoPlays)).BeginInit();
            this.SuspendLayout();
            // 
            // panNonPostedCasinoPlays
            // 
            this.panNonPostedCasinoPlays.Controls.Add(this.dgvwCasinoPlays);
            this.panNonPostedCasinoPlays.Location = new System.Drawing.Point(13, 13);
            this.panNonPostedCasinoPlays.Name = "panNonPostedCasinoPlays";
            this.panNonPostedCasinoPlays.Size = new System.Drawing.Size(427, 237);
            this.panNonPostedCasinoPlays.TabIndex = 0;
            // 
            // dgvwCasinoPlays
            // 
            this.dgvwCasinoPlays.AllowUserToAddRows = false;
            this.dgvwCasinoPlays.AllowUserToDeleteRows = false;
            this.dgvwCasinoPlays.AllowUserToResizeRows = false;
            this.dgvwCasinoPlays.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvwCasinoPlays.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvwCasinoPlays.Location = new System.Drawing.Point(4, 3);
            this.dgvwCasinoPlays.MultiSelect = false;
            this.dgvwCasinoPlays.Name = "dgvwCasinoPlays";
            this.dgvwCasinoPlays.ReadOnly = true;
            this.dgvwCasinoPlays.RowHeadersVisible = false;
            this.dgvwCasinoPlays.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvwCasinoPlays.Size = new System.Drawing.Size(420, 231);
            this.dgvwCasinoPlays.StandardTab = true;
            this.dgvwCasinoPlays.TabIndex = 0;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(189, 257);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // frmCustomerCasinoPlays
            // 
            this.AcceptButton = this.btnClose;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(452, 299);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.panNonPostedCasinoPlays);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCustomerCasinoPlays";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.frmCustomerNonPostedCasinoPlays_Load);
            this.panNonPostedCasinoPlays.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvwCasinoPlays)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panNonPostedCasinoPlays;
        private System.Windows.Forms.DataGridView dgvwCasinoPlays;
        private System.Windows.Forms.Button btnClose;
    }
}