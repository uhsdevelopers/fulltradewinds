﻿namespace GUILibraries.Forms
{
    partial class FrmSystemInMaintenance
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblMessage = new System.Windows.Forms.Label();
            this.lblFrequency = new System.Windows.Forms.Label();
            this.lblMessageMaxCnt = new System.Windows.Forms.Label();
            this.txtMessage = new System.Windows.Forms.TextBox();
            this.lblFrequencyExplain = new System.Windows.Forms.Label();
            this.lblMaxCntExplained = new System.Windows.Forms.Label();
            this.grpPriority = new System.Windows.Forms.GroupBox();
            this.radLow = new System.Windows.Forms.RadioButton();
            this.radMedium = new System.Windows.Forms.RadioButton();
            this.radHigh = new System.Windows.Forms.RadioButton();
            this.btnUnlockApps = new System.Windows.Forms.Button();
            this.cmbTargetSystem = new System.Windows.Forms.ComboBox();
            this.txtMaxCount = new GUILibraries.Controls.NumberTextBox();
            this.txtFrequency = new GUILibraries.Controls.NumberTextBox();
            this.lblSystem = new System.Windows.Forms.Label();
            this.grpPriority.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(196, 227);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(102, 23);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "Broadcast to Apps";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(371, 227);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(50, 37);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(53, 13);
            this.lblMessage.TabIndex = 2;
            this.lblMessage.Text = "Message:";
            // 
            // lblFrequency
            // 
            this.lblFrequency.AutoSize = true;
            this.lblFrequency.Location = new System.Drawing.Point(13, 106);
            this.lblFrequency.Name = "lblFrequency";
            this.lblFrequency.Size = new System.Drawing.Size(106, 13);
            this.lblFrequency.TabIndex = 3;
            this.lblFrequency.Text = "Message Frequency:";
            // 
            // lblMessageMaxCnt
            // 
            this.lblMessageMaxCnt.AutoSize = true;
            this.lblMessageMaxCnt.Location = new System.Drawing.Point(13, 139);
            this.lblMessageMaxCnt.Name = "lblMessageMaxCnt";
            this.lblMessageMaxCnt.Size = new System.Drawing.Size(107, 13);
            this.lblMessageMaxCnt.TabIndex = 4;
            this.lblMessageMaxCnt.Text = "Message Max Count:";
            // 
            // txtMessage
            // 
            this.txtMessage.Location = new System.Drawing.Point(126, 37);
            this.txtMessage.Multiline = true;
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.Size = new System.Drawing.Size(306, 49);
            this.txtMessage.TabIndex = 5;
            this.txtMessage.Text = "App needs to be closed for Scheduled Maintenance. Close it?";
            // 
            // lblFrequencyExplain
            // 
            this.lblFrequencyExplain.AutoSize = true;
            this.lblFrequencyExplain.Location = new System.Drawing.Point(183, 112);
            this.lblFrequencyExplain.Name = "lblFrequencyExplain";
            this.lblFrequencyExplain.Size = new System.Drawing.Size(236, 13);
            this.lblFrequencyExplain.TabIndex = 8;
            this.lblFrequencyExplain.Text = "How often (in minutes) message will be displayed";
            // 
            // lblMaxCntExplained
            // 
            this.lblMaxCntExplained.AutoSize = true;
            this.lblMaxCntExplained.Location = new System.Drawing.Point(183, 136);
            this.lblMaxCntExplained.Name = "lblMaxCntExplained";
            this.lblMaxCntExplained.Size = new System.Drawing.Size(324, 13);
            this.lblMaxCntExplained.TabIndex = 9;
            this.lblMaxCntExplained.Text = "How many times message will be shown before forcing app to close";
            // 
            // grpPriority
            // 
            this.grpPriority.Controls.Add(this.radLow);
            this.grpPriority.Controls.Add(this.radMedium);
            this.grpPriority.Controls.Add(this.radHigh);
            this.grpPriority.Location = new System.Drawing.Point(16, 168);
            this.grpPriority.Name = "grpPriority";
            this.grpPriority.Size = new System.Drawing.Size(278, 43);
            this.grpPriority.TabIndex = 12;
            this.grpPriority.TabStop = false;
            this.grpPriority.Text = "Message Priority";
            // 
            // radLow
            // 
            this.radLow.AutoSize = true;
            this.radLow.Location = new System.Drawing.Point(206, 19);
            this.radLow.Name = "radLow";
            this.radLow.Size = new System.Drawing.Size(45, 17);
            this.radLow.TabIndex = 2;
            this.radLow.Text = "Low";
            this.radLow.UseVisualStyleBackColor = true;
            this.radLow.CheckedChanged += new System.EventHandler(this.radLow_CheckedChanged);
            // 
            // radMedium
            // 
            this.radMedium.AutoSize = true;
            this.radMedium.Location = new System.Drawing.Point(105, 19);
            this.radMedium.Name = "radMedium";
            this.radMedium.Size = new System.Drawing.Size(62, 17);
            this.radMedium.TabIndex = 1;
            this.radMedium.Text = "Medium";
            this.radMedium.UseVisualStyleBackColor = true;
            this.radMedium.CheckedChanged += new System.EventHandler(this.radMedium_CheckedChanged);
            // 
            // radHigh
            // 
            this.radHigh.AutoSize = true;
            this.radHigh.Checked = true;
            this.radHigh.Location = new System.Drawing.Point(19, 19);
            this.radHigh.Name = "radHigh";
            this.radHigh.Size = new System.Drawing.Size(47, 17);
            this.radHigh.TabIndex = 0;
            this.radHigh.TabStop = true;
            this.radHigh.Text = "High";
            this.radHigh.UseVisualStyleBackColor = true;
            this.radHigh.CheckedChanged += new System.EventHandler(this.radHigh_CheckedChanged);
            // 
            // btnUnlockApps
            // 
            this.btnUnlockApps.Location = new System.Drawing.Point(35, 227);
            this.btnUnlockApps.Name = "btnUnlockApps";
            this.btnUnlockApps.Size = new System.Drawing.Size(125, 23);
            this.btnUnlockApps.TabIndex = 13;
            this.btnUnlockApps.Text = "Restore Apps Usability";
            this.btnUnlockApps.UseVisualStyleBackColor = true;
            this.btnUnlockApps.Click += new System.EventHandler(this.btnUnlockApps_Click);
            // 
            // cmbTargetSystem
            // 
            this.cmbTargetSystem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTargetSystem.FormattingEnabled = true;
            this.cmbTargetSystem.Location = new System.Drawing.Point(400, 0);
            this.cmbTargetSystem.Name = "cmbTargetSystem";
            this.cmbTargetSystem.Size = new System.Drawing.Size(121, 21);
            this.cmbTargetSystem.TabIndex = 14;
            this.cmbTargetSystem.SelectedIndexChanged += new System.EventHandler(this.cmbTargetSystem_SelectedIndexChanged);
            // 
            // txtMaxCount
            // 
            this.txtMaxCount.DividedBy = 1;
            this.txtMaxCount.DivideFlag = false;
            this.txtMaxCount.Location = new System.Drawing.Point(128, 133);
            this.txtMaxCount.Name = "txtMaxCount";
            this.txtMaxCount.ShowIfZero = true;
            this.txtMaxCount.Size = new System.Drawing.Size(46, 20);
            this.txtMaxCount.TabIndex = 11;
            this.txtMaxCount.Text = "3";
            // 
            // txtFrequency
            // 
            this.txtFrequency.DividedBy = 1;
            this.txtFrequency.DivideFlag = false;
            this.txtFrequency.Location = new System.Drawing.Point(127, 106);
            this.txtFrequency.Name = "txtFrequency";
            this.txtFrequency.ShowIfZero = true;
            this.txtFrequency.Size = new System.Drawing.Size(46, 20);
            this.txtFrequency.TabIndex = 10;
            this.txtFrequency.Text = "1";
            // 
            // lblSystem
            // 
            this.lblSystem.AutoSize = true;
            this.lblSystem.Location = new System.Drawing.Point(318, 5);
            this.lblSystem.Name = "lblSystem";
            this.lblSystem.Size = new System.Drawing.Size(80, 13);
            this.lblSystem.TabIndex = 15;
            this.lblSystem.Text = "Systems List =>";
            // 
            // frmSystemInMaintenance
            // 
            this.AcceptButton = this.btnOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(533, 267);
            this.Controls.Add(this.lblSystem);
            this.Controls.Add(this.cmbTargetSystem);
            this.Controls.Add(this.btnUnlockApps);
            this.Controls.Add(this.grpPriority);
            this.Controls.Add(this.txtMaxCount);
            this.Controls.Add(this.txtFrequency);
            this.Controls.Add(this.lblMaxCntExplained);
            this.Controls.Add(this.lblFrequencyExplain);
            this.Controls.Add(this.txtMessage);
            this.Controls.Add(this.lblMessageMaxCnt);
            this.Controls.Add(this.lblFrequency);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmSystemInMaintenance";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "System In Maintenance";
            this.Load += new System.EventHandler(this.frmSystemInMaintenance_Load);
            this.grpPriority.ResumeLayout(false);
            this.grpPriority.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Label lblFrequency;
        private System.Windows.Forms.Label lblMessageMaxCnt;
        private System.Windows.Forms.TextBox txtMessage;
        private System.Windows.Forms.Label lblFrequencyExplain;
        private System.Windows.Forms.Label lblMaxCntExplained;
        private GUILibraries.Controls.NumberTextBox txtFrequency;
        private GUILibraries.Controls.NumberTextBox txtMaxCount;
        private System.Windows.Forms.GroupBox grpPriority;
        private System.Windows.Forms.RadioButton radLow;
        private System.Windows.Forms.RadioButton radMedium;
        private System.Windows.Forms.RadioButton radHigh;
        private System.Windows.Forms.Button btnUnlockApps;
        private System.Windows.Forms.ComboBox cmbTargetSystem;
        private System.Windows.Forms.Label lblSystem;
    }
}