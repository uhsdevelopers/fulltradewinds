﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using System.Collections;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using GUILibraries.Controls;
using GUILibraries.Utilities;

namespace GUILibraries.Forms {
  public partial class FrmDailyFigures : SIDForm {

    #region Private Vars

    private readonly String _currentCustomerId = "";
    private readonly String _customerCurrentCurrency = "";
    private readonly ArrayList _weeklyDailyFiguresDates = new ArrayList();
    private readonly ArrayList _selectedWagers = new ArrayList();
    private readonly List<spULPGetCurrentUserPermissions_Result> _currentCustomerPermissions;
    private readonly Boolean _machineHasSoundCard;

    #endregion

    #region Public Properties

    #endregion

    #region Private Properties

    #endregion

    #region Structures

    private struct WeeklyDailyFigureDateRange {
      public DateTime StartDate, EndDate;
    }

    private struct SelectedWager {
      public String WagerType;
      public int TicketNumber;
      public DateTime TicketPostedDateTime;
      public int WagerNumber;
      public String PlayNumber;
    }

    #endregion

    #region Constructors

    public FrmDailyFigures(String customerId, String customerCurrency, ModuleInfo moduleInfo, List<spULPGetCurrentUserPermissions_Result> currentCustomerPermissions, Boolean machineHasSoundCard)
      : base(moduleInfo) {
      if (!string.IsNullOrEmpty(customerId)) {
        _currentCustomerId = customerId;
        _customerCurrentCurrency = customerCurrency;
        _currentCustomerPermissions = currentCustomerPermissions;
        _machineHasSoundCard = machineHasSoundCard;
        InitializeComponent();
      }
      else {
        MessageBox.Show(@"No customerId was selected");
        Close();
      }
    }

    #endregion

    #region Private Methods

    private void GetCustomerDailyFigures(String customerId, DateTime dailyFigureBegDate) {
      List<spCstGetCustomerDailyFiguresbyDate_Result> gvDataSource;

      using (var df = new DailyFigures(AppModuleInfo))
        gvDataSource = df.GetCustomerDailyFiguresbyDate(customerId, dailyFigureBegDate);
      dgvwDailyFigures.DataSource = null;

      if (gvDataSource == null)
        return;
      dgvwDailyFigures.Columns.AddRange(new DataGridViewTextBoxColumn {
        HeaderText = @"Daily Figure Date ",
        AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader,
        Name = ColumnNames.DailyFigureDate,
        Width = 150
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"Amount",
        DefaultCellStyle = new DataGridViewCellStyle { Alignment = DataGridViewContentAlignment.MiddleRight },
        Name = ColumnNames.Amount,
        Width = 100,
        HeaderCell = new DataGridViewColumnHeaderCell { Style = new DataGridViewCellStyle { Alignment = DataGridViewContentAlignment.MiddleRight } }
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"AmountWon",
        AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader,
        Name = ColumnNames.AmountWon,
        Visible = false
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"AmountLost",
        AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader,
        Name = ColumnNames.AmountLost,
        Visible = false
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"DailyFigureDate",
        AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader,
        Name = ColumnNames.DailyFigureDate,
        Visible = false
      });
      foreach (var res in gvDataSource) {
        dgvwDailyFigures.Rows.Add(res.DailyFigureDateStr, FormatNumber(res.NetAmount ?? 0), FormatNumber(res.AmountWon ?? 0), FormatNumber(res.AmountLost ?? 0), res.DailyFigureDate);

        var dfDate = new DateTime(((DateTime)res.DailyFigureDate).Year, ((DateTime)res.DailyFigureDate).Month, ((DateTime)res.DailyFigureDate).Day);
        var netAmt = res.NetAmount;

        for (var j = 0; j < _weeklyDailyFiguresDates.Count; j++) {
          var wDf = (WeeklyDailyFigureDateRange)_weeklyDailyFiguresDates[j];
          var wDfSDate = new DateTime(wDf.StartDate.Year, wDf.StartDate.Month, wDf.StartDate.Day);
          var wDfEDate = new DateTime(wDf.EndDate.Year, wDf.EndDate.Month, wDf.EndDate.Day);

          var startDateCompareResult = DateTime.Compare(dfDate, wDfSDate);
          var endDateCompareResult = DateTime.Compare(dfDate, wDfEDate);

          if ((startDateCompareResult != 0 && startDateCompareResult != 1) ||
              (endDateCompareResult != 0 && endDateCompareResult != -1)) continue;
          var nTxt = (NumberTextBox)Controls.Find("txtWeeklyFigure" + (j + 1), true).FirstOrDefault();
          var holdNetAmout = nTxt == null || nTxt.Text == "" ? 0 : double.Parse(nTxt.Text);

          if (nTxt != null)
            nTxt.Text = FormatNumber((double)(holdNetAmout + netAmt));
          break;
        }
      }
      dgvwDailyFigures.Rows[gvDataSource.Count - 1].Selected = true;
      dgvwDailyFigures.FirstDisplayedScrollingRowIndex = gvDataSource.Count - 1;
      FormF.PreventDataGridViewColumnsToBeSortedAndResized(dgvwDailyFigures);
      dgvwDailyFigures.SelectionChanged += dgvwDailyFigures_SelectionChanged;
    }

    private static DateTime GetStartingDailyFigureDate() {
      var dailyFigureBegDate = DateTime.Now;
      var dayOfWeek = dailyFigureBegDate.DayOfWeek;

      double daysToAdd = SIDLibraries.Utilities.StringF.GetDaysToAdd(dayOfWeek);
      dailyFigureBegDate = dailyFigureBegDate.AddDays(-1 * daysToAdd);
      dailyFigureBegDate = dailyFigureBegDate.AddDays(-7 * 3);

      double hours = dailyFigureBegDate.Hour;
      double mins = dailyFigureBegDate.Minute;
      double secs = dailyFigureBegDate.Second;
      double millisecs = dailyFigureBegDate.Millisecond;

      dailyFigureBegDate = dailyFigureBegDate.AddHours(-1 * hours);
      dailyFigureBegDate = dailyFigureBegDate.AddMinutes(-1 * mins);
      dailyFigureBegDate = dailyFigureBegDate.AddSeconds(-1 * secs);
      dailyFigureBegDate = dailyFigureBegDate.AddMilliseconds(-1 * millisecs);

      return dailyFigureBegDate;
    }

    private void GoToWagerDetails() {
      var ticketNumber = ((SelectedWager)(_selectedWagers[0])).TicketNumber;
      var ticketPostedDateTime = ((SelectedWager)(_selectedWagers[0])).TicketPostedDateTime;
      var wagerNumber = ((SelectedWager)(_selectedWagers[0])).WagerNumber;
      var playNumber = ((SelectedWager)(_selectedWagers[0])).PlayNumber;
      var wagerType = ((SelectedWager)(_selectedWagers[0])).WagerType;

      if (wagerType.Contains("Casino")) {
        var casinoForm = new FrmCustomerCasinoPlays("Posted", _currentCustomerId, ticketPostedDateTime, AppModuleInfo) {
          Icon = Icon
        };
        casinoForm.ShowDialog();
      }
      else {
        int? rrItemsCnt = null;
        if (wagerNumber == 0 && wagerType == "Round Robin") {
          using (var tAw = new TicketsAndWagers(AppModuleInfo)) {
            var resultSet = tAw.GetCustomerHistoryWagerDetails(ticketNumber, ticketPostedDateTime).ToList();
            rrItemsCnt = (from r in resultSet where r.PlayNumber == int.Parse(playNumber) select r).Count();
          }
        }
        var wagerDetailsFrm = new FrmCustomerWagerDetails(ticketNumber, ticketPostedDateTime, wagerNumber, playNumber, wagerType, _customerCurrentCurrency, AppModuleInfo, _currentCustomerPermissions, _machineHasSoundCard, rrItemsCnt, false, false);
        try {
          wagerDetailsFrm.Icon = Icon;
          wagerDetailsFrm.ShowDialog();
        }
        catch (Exception ex) {
          Log(ex);
        }
      }
    }

    private void LoadCustWagerDetailsGv() {
      if (dgvwDailyFigures.SelectedRows.Count <= 0)
        return;
      btnWagerDetails.Enabled = true;

      var dailyFigureDate = DateTime.Parse((dgvwDailyFigures.SelectedRows[0].Cells[4].Value.ToString()));
      var amount = (dgvwDailyFigures.SelectedRows[0].Cells[1].Value).ToString();
      double selectedAmount;
      var isDouble = double.TryParse(amount, out selectedAmount);

      if (isDouble && selectedAmount == 0)
        btnWagerDetails.Enabled = false;
      List<spTkWGetCustomerWagerDetailsByDailyFigureDate_Result> detailsgrvwDataSource;

      using (var tAndW = new TicketsAndWagers(AppModuleInfo))
        detailsgrvwDataSource = tAndW.GetCustomerWagerDetailsByDailyFigureDate(_currentCustomerId, dailyFigureDate).ToList();
      if (dgvwCustWagerDetails.ColumnCount == 0)
        WriteCustWagerDetailsDgvwHeaders(dgvwCustWagerDetails);
      dgvwCustWagerDetails.Rows.Clear();

      foreach (var res in detailsgrvwDataSource)
        dgvwCustWagerDetails.Rows.Add(res.Type, FormatNumber(res.Amount ?? 0), res.Description, res.docNumber, res.ticketPostedDateTime, res.WagerNumber, res.playNumber);
      dgvwCustWagerDetails.ReadOnly = true;
      //dgvwCustWagerDetails.ClearSelection();
      FormF.PreventDataGridViewColumnsToBeSortedAndResized(dgvwCustWagerDetails);
    }

    private void SplitDailyFiguresByWeek(DateTime startDailyFigureDate) {
      var startDate = startDailyFigureDate;
      var endDate = startDate.AddDays(6);
      var df = new WeeklyDailyFigureDateRange { StartDate = startDate, EndDate = endDate };

      _weeklyDailyFiguresDates.Add(df);

      const string labelName = "lblWeeklyFigure";

      for (var i = 1; i < 5; i++) {
        var lbl = (Label)Controls.Find(labelName + i, true).FirstOrDefault();

        if (i == 4) {
          if (lbl != null)
            lbl.Text = startDate.Month + @"/" + startDate.Day + @" - current:";
        }
        else {
          if (lbl != null)
            lbl.Text = startDate.Month + @"/" + startDate.Day + @" - " + endDate.Month + @"/" + endDate.Day + @":";
        }
        startDate = endDate.AddDays(1);
        endDate = startDate.AddDays(6);
        df = new WeeklyDailyFigureDateRange { StartDate = startDate, EndDate = endDate };
        _weeklyDailyFiguresDates.Add(df);
      }
    }

    private static void WriteCustWagerDetailsDgvwHeaders(DataGridView caller) {
      var dgrvwColNames = new ArrayList();

      foreach (DataGridViewTextBoxColumn col in caller.Columns)
        dgrvwColNames.Add(col.Name);
      foreach (String str in dgrvwColNames)
        caller.Columns.Remove(str);
      caller.Columns.AddRange(new DataGridViewTextBoxColumn {
        HeaderText = @"Type",
        Name = ColumnNames.Type,
        Width = 125
      }, new DataGridViewTextBoxColumn {
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleRight },
        HeaderCell = new DataGridViewColumnHeaderCell { Style = { Alignment = DataGridViewContentAlignment.MiddleRight } },
        HeaderText = @"Won/-Lost",
        Name = ColumnNames.WonLost,
        Width = 75
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"Description",
        Width = 800,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleLeft },
        Name = ColumnNames.Description
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"docNumber",
        Name = ColumnNames.DocNumber,
        Width = 75,
        Visible = false
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"ticketPostedDateTime",
        Name = ColumnNames.TicketPostedDateTime,
        Width = 75,
        Visible = false
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"WagerNumber",
        Name = ColumnNames.WagerNumber,
        Width = 75,
        Visible = false
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"PlayNumber",
        Name = ColumnNames.PlayNumber,
        Width = 75,
        Visible = false
      });
    }

    private void SetCentsNumericInputs() {
      txtWeeklyFigure1.AllowCents = Params.IncludeCents;
      txtWeeklyFigure2.AllowCents = Params.IncludeCents;
      txtWeeklyFigure3.AllowCents = Params.IncludeCents;
      txtWeeklyFigure4.AllowCents = Params.IncludeCents;
    }

    #endregion

    #region Events

    private void btnClose_Click(object sender, EventArgs e) {
      Close();
    }

    private void btnWagerComments_Click(object sender, EventArgs e) {
      var ticketNumber = ((SelectedWager)(_selectedWagers[0])).TicketNumber;
      var wagerNumber = ((SelectedWager)(_selectedWagers[0])).WagerNumber;

      var wagerCommentsFrm = new FrmCustomerWagerComments(ticketNumber, wagerNumber, AppModuleInfo);
      try {
        wagerCommentsFrm.Icon = Icon;
        wagerCommentsFrm.ShowDialog();
      }
      catch (Exception ex) {
        Log(ex);
      }
    }

    private void btnWagerDetails_Click(object sender, EventArgs e) {
      GoToWagerDetails();
    }

    private void DailyFigures_Load(object sender, EventArgs e) {
      var startDailyFigureDate = GetStartingDailyFigureDate();
      SplitDailyFiguresByWeek(startDailyFigureDate);
      SetCentsNumericInputs();
      GetCustomerDailyFigures(_currentCustomerId, startDailyFigureDate);
      txtDailyFiguresCurrencyLegend.Text = _customerCurrentCurrency;
      btnWagerDetails.Enabled = false;
      btnWagerComments.Enabled = false;
    }

    private void dgvwCustWagerDetails_KeyDown(object sender, KeyEventArgs e) {
      var pressedKey = e.KeyValue.ToString(CultureInfo.InvariantCulture);

      if (pressedKey == "13")
        e.Handled = true;
    }

    private void dgvwCustWagerDetails_SelectionChanged(object sender, EventArgs e) {
      var selectedWager = new SelectedWager();

      if (dgvwCustWagerDetails.SelectedRows.Count != 1)
        return;
      selectedWager.WagerType = dgvwCustWagerDetails.SelectedRows[0].Cells[ColumnNames.Type].Value.ToString();
      selectedWager.TicketNumber = int.Parse(dgvwCustWagerDetails.SelectedRows[0].Cells[ColumnNames.DocNumber].Value.ToString());
      selectedWager.TicketPostedDateTime = DateTime.Parse(dgvwCustWagerDetails.SelectedRows[0].Cells[ColumnNames.TicketPostedDateTime].Value.ToString());
      selectedWager.WagerNumber = int.Parse(dgvwCustWagerDetails.SelectedRows[0].Cells[ColumnNames.WagerNumber].Value.ToString());
      selectedWager.PlayNumber = dgvwCustWagerDetails.SelectedRows[0].Cells[ColumnNames.PlayNumber].Value.ToString();

      if (_selectedWagers.Count > 0)
        _selectedWagers.Clear();
      _selectedWagers.Add(selectedWager);

      if (selectedWager.WagerNumber != 999999 || selectedWager.WagerType.Contains("Casino")) {
        if (!selectedWager.WagerType.Contains("Casino"))
          btnWagerComments.Enabled = true;
        btnWagerDetails.Enabled = true;
      }
      else {
        btnWagerComments.Enabled = false;
        btnWagerDetails.Enabled = false;
      }
    }

    private void dgvwDailyFigures_SelectionChanged(object sender, EventArgs e) {
      LoadCustWagerDetailsGv();
    }

    private void frmDailyFigures_KeyPress(object sender, KeyPressEventArgs e) {
      var pressedKey = e.KeyChar.ToString(CultureInfo.InvariantCulture);
      var rowCnt = dgvwCustWagerDetails.SelectedRows.Count;

      if ((pressedKey == "\r" || pressedKey.ToLower() == "e") && rowCnt > 0)
        GoToWagerDetails();
    }

    #endregion

  }
}