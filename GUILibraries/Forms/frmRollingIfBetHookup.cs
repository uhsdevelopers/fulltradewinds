﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using GUILibraries.Utilities;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using SIDLibraries.Utilities;

namespace GUILibraries.Forms {
  public partial class FrmRollingIfBetHookup : SIDForm {

    #region private Constants

    private const int GRID_VIEW_ROW_HEIGHT = 16;

    #endregion

    #region Private vars

    private readonly string _currentRifBackwardTicketNumber;
    private readonly string _currentRifBackwardWagerNumber;
    private readonly int _currentTicketNumber;
    private readonly int _currentWagerNumber;
    private readonly List<spTkWGetCustomerWagerDetails_Result> _currentWagerDetails;
    private readonly List<ReadbackItem> _currentReadbackItems;
    private readonly DataGridView _currentwagerOutcomeDetails;
    private Boolean _currentIsBaseHookup;
    private readonly Boolean _isFromReadback;
    private String _rifType;
    private int _ticketNumberToRetrieve;
    private int _wagerNumberToRetrieve;

    #endregion

    #region Constructors

    public FrmRollingIfBetHookup(int ticketNumber, List<spTkWGetCustomerWagerDetails_Result> wagerDetails, DataGridView wagerOutcomeDetails, ModuleInfo moduleInfo)
      : base(moduleInfo) {
      _currentTicketNumber = ticketNumber;
      _currentWagerDetails = wagerDetails;
      _currentwagerOutcomeDetails = wagerOutcomeDetails;

      InitializeComponent();
    }

    public FrmRollingIfBetHookup(int ticketNumber, int wagerNumber, string rifBackwardTicketNumber, string rifBackwardWagerNumber, List<ReadbackItem> readbackItems, ModuleInfo moduleInfo, Boolean fromReadback)
      : base(moduleInfo) {
      _currentTicketNumber = ticketNumber;
      _currentWagerNumber = wagerNumber;
      _currentRifBackwardTicketNumber = rifBackwardTicketNumber;
      if (String.IsNullOrEmpty(_currentRifBackwardTicketNumber)) {
        _currentRifBackwardTicketNumber = "0";
      }
      _currentRifBackwardWagerNumber = rifBackwardWagerNumber;
      if (String.IsNullOrEmpty(_currentRifBackwardWagerNumber)) {
        _currentRifBackwardWagerNumber = "0";
      }
      _currentReadbackItems = readbackItems;
      _isFromReadback = fromReadback;
      InitializeComponent();
    }

    #endregion

    #region Private Methods

    private void PopulateRollingIfBetFromReadbackGridView() {
      if (dgvwRollingIfBet.Rows.Count > 0)
        dgvwRollingIfBet.Rows.Clear();

      dgvwRollingIfBet.Rows.Add("", "", "");
      var rCnt = dgvwRollingIfBet.Rows.Count - 1;
      dgvwRollingIfBet.Rows[rCnt].Height = GRID_VIEW_ROW_HEIGHT;

      if (_currentTicketNumber != int.Parse(_currentRifBackwardTicketNumber) && int.Parse(_currentRifBackwardTicketNumber) != 0) {
        using (var taW = new TicketsAndWagers(AppModuleInfo)) {
          var rifHookups = taW.GetRifHookups(int.Parse(_currentRifBackwardTicketNumber), int.Parse(_currentRifBackwardWagerNumber)).ToList();

          foreach (var hookup in rifHookups) {
            var rifWagerInfo = taW.GetRifInfo(hookup.TicketNumber, hookup.WagerNumber);
            var holdItemNumber = 0;
            var accountType = "";
            double? amountWagered = 0;
            double? toWinAmount = 0;

            foreach (var rifInfo in rifWagerInfo) {
              if (holdItemNumber == 0) {
                var wagerTypeName = "";
                var wagerStatus = "";

                switch (rifInfo.wWagerType) {
                  case WagerType.PARLAY:
                    wagerTypeName = "Parlay";
                    break;
                  case WagerType.TEASER:
                    wagerTypeName = "If-Bet";
                    break;
                  case WagerType.IFBET:
                    wagerTypeName = "Teaser";
                    break;
                }
                switch (rifInfo.WagerStatus) {
                  case "P":
                    wagerStatus = "Pending";
                    break;
                  case "L":
                    wagerStatus = "Loss";
                    break;
                  case "W":
                    wagerStatus = "Win";
                    break;
                  case "O":
                    wagerStatus = "Open";
                    break;
                  case "X":
                    wagerStatus = "Cancel";
                    break;
                }
                dgvwRollingIfBet.Rows.Add("", rifInfo.TicketNumber + " - " + rifInfo.WagerNumber, wagerTypeName + " (" + wagerStatus + ")");
                rCnt = dgvwRollingIfBet.Rows.Count - 1;
                dgvwRollingIfBet.Rows[rCnt].Height = GRID_VIEW_ROW_HEIGHT;
              }
              dgvwRollingIfBet.Rows.Add("", "", DisplayDate.Format(DateTime.Parse(rifInfo.GameDateTime.ToString()), DisplayDateFormat.ShortDateNoYear) + " " + TicketsAndWagers.WriteWagerDescription(rifInfo));
              rCnt = dgvwRollingIfBet.Rows.Count - 1;
              dgvwRollingIfBet.Rows[rCnt].Height = GRID_VIEW_ROW_HEIGHT;
              holdItemNumber = rifInfo.ItemNumber;
              accountType = rifInfo.CreditAcctFlag;
              amountWagered = rifInfo.AmountWagered / 100;
              toWinAmount = rifInfo.ToWinAmount / 100;
            }
            if (accountType == null) continue;
            var string3 = accountType.Trim() == "Y" ? "CREDIT" : "POSTUP";
            string3 += " Risk: ";
            string3 += FormatNumber((double)amountWagered, false, 1, true);
            string3 += " To Win: ";
            string3 += FormatNumber((double)toWinAmount, false, 1, true);

            dgvwRollingIfBet.Rows.Add("", "", string3);
            rCnt = dgvwRollingIfBet.Rows.Count - 1;
            dgvwRollingIfBet.Rows[rCnt].Height = GRID_VIEW_ROW_HEIGHT;
          }
          dgvwRollingIfBet.Rows.Add("", "", "");
          rCnt = dgvwRollingIfBet.Rows.Count - 1;
          dgvwRollingIfBet.Rows[rCnt].Height = GRID_VIEW_ROW_HEIGHT;

          foreach (var ri in _currentReadbackItems.Where(ri => ri.WagerNumber == _currentWagerNumber)) {
            if (ri.RifWinOnlyFlag != null) {
              dgvwRollingIfBet.Rows.Add("", "", ri.RifWinOnlyFlag.Trim() == "Y" ? "IF WIN ONLY" : "IF WIN or PUSH");
              rCnt = dgvwRollingIfBet.Rows.Count - 1;
              dgvwRollingIfBet.Rows[rCnt].Height = GRID_VIEW_ROW_HEIGHT;
              dgvwRollingIfBet.Rows.Add("", "", "");
              rCnt = dgvwRollingIfBet.Rows.Count - 1;
              dgvwRollingIfBet.Rows[rCnt].Height = GRID_VIEW_ROW_HEIGHT;
            }
            dgvwRollingIfBet.Rows.Add("----->", "Play #" + ri.WagerNumber, ri.WagerTypeName);
            rCnt = dgvwRollingIfBet.Rows.Count - 1;
            dgvwRollingIfBet.Rows[rCnt].Height = GRID_VIEW_ROW_HEIGHT;
            dgvwRollingIfBet.Rows.Add("----->", "", ri.String3 + " (On Readback)");
            rCnt = dgvwRollingIfBet.Rows.Count - 1;
            dgvwRollingIfBet.Rows[rCnt].Height = GRID_VIEW_ROW_HEIGHT;
          }
          dgvwRollingIfBet.ClearSelection();
          return;
        }
        var holdWagerNumber = 0;
        var correlatedRifs = GetCorrelatedRifWagerNumbers(_currentReadbackItems, _currentWagerNumber);

        foreach (var rbI in _currentReadbackItems) {
          var hasCorrelation = correlatedRifs.Any(item => item == rbI.WagerNumber);

          if (!hasCorrelation) continue;
          if (holdWagerNumber != rbI.WagerNumber) {
            if (!String.IsNullOrEmpty(rbI.RifWinOnlyFlag)) {
              dgvwRollingIfBet.Rows.Add("", "", "");
              rCnt = dgvwRollingIfBet.Rows.Count - 1;
              dgvwRollingIfBet.Rows[rCnt].Height = GRID_VIEW_ROW_HEIGHT;
              dgvwRollingIfBet.Rows.Add("", "", rbI.RifWinOnlyFlag.Trim() == "Y" ? "IF WIN ONLY" : "IF WIN or PUSH");
              rCnt = dgvwRollingIfBet.Rows.Count - 1;
              dgvwRollingIfBet.Rows[rCnt].Height = GRID_VIEW_ROW_HEIGHT;
            }
            dgvwRollingIfBet.Rows.Add("", "Play #" + rbI.WagerNumber, rbI.WagerTypeName);
          }
          var string1 = "----->";
          if (rbI.WagerNumber != _currentWagerNumber)
            string1 = "";
          if (rbI.GameDateTime != null) {
            dgvwRollingIfBet.Rows.Add(string1, "", DisplayDate.Format(DateTime.Parse(rbI.GameDateTime.ToString()), DisplayDateFormat.ShortDateNoYear) + " " + rbI.String3 + " (On Readback)");
          }
          else {
            dgvwRollingIfBet.Rows.Add(string1, "", rbI.String3 + " (On Readback)");
          }
          rCnt = dgvwRollingIfBet.Rows.Count - 1;
          dgvwRollingIfBet.Rows[rCnt].Height = GRID_VIEW_ROW_HEIGHT;
          holdWagerNumber = rbI.WagerNumber;
        }
      }
      dgvwRollingIfBet.ClearSelection();
    }

    private static IEnumerable<int> GetCorrelatedRifWagerNumbers(List<ReadbackItem> currentReadbackItems, int wagerNumber) {
      var wagersNumber = new List<int>();

      int? targetWagerNumber = wagerNumber;

      wagersNumber.Add((int)targetWagerNumber);

      //forward Search
      Boolean keepSearching = true;
      while (keepSearching) {
        int? stageWagerNumber = (from p in currentReadbackItems where p.RifBackwardWagerNumber == targetWagerNumber.ToString() select p.WagerNumber).FirstOrDefault();
        if (stageWagerNumber > 0) {
          wagersNumber.Add((int)stageWagerNumber);
          targetWagerNumber = stageWagerNumber;

        }
        else {
          keepSearching = false;
        }
      }

      //backward Search
      keepSearching = true;
      targetWagerNumber = wagerNumber;

      string targetRifWagerNumber = (from p in currentReadbackItems where p.WagerNumber == (int)targetWagerNumber select p.RifBackwardWagerNumber).FirstOrDefault();

      if (!string.IsNullOrEmpty(targetRifWagerNumber)) {
        wagersNumber.Add(int.Parse(targetRifWagerNumber));
      }

      while (keepSearching) {
        int intOutput;
        var isInt = int.TryParse(targetRifWagerNumber, out intOutput);
        if (isInt) {
          String stageRifWagerNumber = (from p in currentReadbackItems where p.WagerNumber == intOutput select p.RifBackwardWagerNumber).FirstOrDefault();

          if (!string.IsNullOrEmpty(stageRifWagerNumber)) {
            wagersNumber.Add(int.Parse(stageRifWagerNumber));
            targetRifWagerNumber = stageRifWagerNumber;
          }
          else {
            keepSearching = false;
          }
        }
        else {
          keepSearching = false;
        }
      }

      wagersNumber.Sort();
      return wagersNumber;
    }

    private void PopulateRollingIfBetGridView() {
      _rifType = "";

      if (_currentWagerDetails != null) {
        WriteRollingIfBetsGrvwHeaders();
        _currentIsBaseHookup = _currentWagerDetails[0].RifForwardAmountWagered > 0;

        int rCnt;
        String selectionMark;

        if (_currentIsBaseHookup) {
          selectionMark = "----->";
          dgvwRollingIfBet.Rows.Add("", "", "");
          rCnt = dgvwRollingIfBet.Rows.Count - 1;
          dgvwRollingIfBet.Rows[rCnt].Height = GRID_VIEW_ROW_HEIGHT;
          AddHookupWagerInfo(selectionMark);
          dgvwRollingIfBet.Rows.Add("", "", "");
          rCnt = dgvwRollingIfBet.Rows.Count - 1;
          dgvwRollingIfBet.Rows[rCnt].Height = GRID_VIEW_ROW_HEIGHT;
          selectionMark = "";
          _ticketNumberToRetrieve = _currentWagerDetails[0].RifForwardTicketNumber ?? 0;
          _wagerNumberToRetrieve = _currentWagerDetails[0].RifForwardWagerNumber ?? 0;
          GetAndAddHookupWagerInfo(selectionMark, _ticketNumberToRetrieve, _wagerNumberToRetrieve);
        }
        else {
          selectionMark = "";
          dgvwRollingIfBet.Rows.Add("", "", "");
          rCnt = dgvwRollingIfBet.Rows.Count - 1;
          dgvwRollingIfBet.Rows[rCnt].Height = GRID_VIEW_ROW_HEIGHT;
          _ticketNumberToRetrieve = _currentWagerDetails[0].RifBackwardTicketNumber ?? 0;
          _wagerNumberToRetrieve = _currentWagerDetails[0].RifBackwardWagerNumber ?? 0;
          GetAndAddHookupWagerInfo(selectionMark, _ticketNumberToRetrieve, _wagerNumberToRetrieve);
          dgvwRollingIfBet.Rows.Add("", "", "");
          rCnt = dgvwRollingIfBet.Rows.Count - 1;
          dgvwRollingIfBet.Rows[rCnt].Height = GRID_VIEW_ROW_HEIGHT;
          selectionMark = "----->";
          AddHookupWagerInfo(selectionMark);
        }
      }
      dgvwRollingIfBet.ClearSelection();
      FormF.PreventDataGridViewColumnsToBeSortedAndResized(dgvwRollingIfBet);
    }

    private void GetAndAddHookupWagerInfo(string selectionMark, int ticketNumberToRetrieve, int wagerNumberToRetrieve) {
      using (var tAw = new TicketsAndWagers(AppModuleInfo)) {
        var ticketToRetrievePostedDateTime = tAw.GetTicketPostedDateTime(ticketNumberToRetrieve);
        var newWagerDetails = tAw.GetCustomerWagerDetails(ticketNumberToRetrieve, wagerNumberToRetrieve, ticketToRetrievePostedDateTime).ToList();
        var newWagerOutcomeDetails = tAw.GetCustomerWagerOutcome(ticketNumberToRetrieve, wagerNumberToRetrieve, 0, ticketToRetrievePostedDateTime).ToList();

        if (newWagerDetails.Count <= 0)
          return;
        if (_currentIsBaseHookup) {
          _rifType = newWagerDetails[0].RifWinOnlyFlag != null ? "IF WIN ONLY" : "IF WIN OR PUSH";
          dgvwRollingIfBet.Rows.Add("", "", _rifType);
          dgvwRollingIfBet.Rows[dgvwRollingIfBet.Rows.Count - 1].Height = GRID_VIEW_ROW_HEIGHT;
        }
        dgvwRollingIfBet.Rows.Add(selectionMark, newWagerDetails[0].TicketNumber.ToString(CultureInfo.InvariantCulture)
                                                 + " - " + newWagerDetails[0].WagerNumber.ToString(CultureInfo.InvariantCulture), newWagerDetails[0].WagerType
                                                 + " (" + newWagerDetails[0].WagerStatus + ")");
        dgvwRollingIfBet.Rows[dgvwRollingIfBet.Rows.Count - 1].Height = GRID_VIEW_ROW_HEIGHT;

        if (newWagerOutcomeDetails.Count > 0) {

          foreach (spTkWGetCustomerWagerOutcome_Result res in newWagerOutcomeDetails) {
            dgvwRollingIfBet.Rows.Add(selectionMark, "", DisplayDate.Format(DateTime.Parse(res.GameDateTime.ToString()), DisplayDateFormat.ShortDateNoYear)
                                                         + " " + TicketsAndWagers.WriteWagerDescription(res));
            dgvwRollingIfBet.Rows[dgvwRollingIfBet.Rows.Count - 1].Height = GRID_VIEW_ROW_HEIGHT;
          }
        }
        dgvwRollingIfBet.Rows.Add(selectionMark, "", newWagerDetails[0].CreditAcctFlag == "Y" ? "CREDIT" : "POST-UP"
                                                     + " Risk: " + FormatNumber(newWagerDetails[0].AmountWagered, false, 1, true)
                                                     + " To Win: " + FormatNumber(newWagerDetails[0].ToWinAmount, false, 1, true));
        dgvwRollingIfBet.Rows[dgvwRollingIfBet.Rows.Count - 1].Height = GRID_VIEW_ROW_HEIGHT;
      }
    }

    private void AddHookupWagerInfo(string selectionMark) {
      if (!_currentIsBaseHookup) {
        _rifType = _currentWagerDetails[0].RifWinOnlyFlag != null ? "IF WIN ONLY" : "IF WIN OR PUSH";

        dgvwRollingIfBet.Rows.Add("", "", _rifType);
        dgvwRollingIfBet.Rows[dgvwRollingIfBet.Rows.Count - 1].Height = GRID_VIEW_ROW_HEIGHT;
      }
      dgvwRollingIfBet.Rows.Add(selectionMark, _currentWagerDetails[0].TicketNumber.ToString(CultureInfo.InvariantCulture) + " - " + _currentWagerDetails[0].WagerNumber.ToString(CultureInfo.InvariantCulture), _currentWagerDetails[0].WagerType + " (" + _currentWagerDetails[0].WagerStatus + ")");
      dgvwRollingIfBet.Rows[dgvwRollingIfBet.Rows.Count - 1].Height = GRID_VIEW_ROW_HEIGHT;

      if (_currentwagerOutcomeDetails != null) {
        for (var i = 0; i < _currentwagerOutcomeDetails.Rows.Count; i++) {
          dgvwRollingIfBet.Rows.Add(selectionMark, "", DisplayDate.Format(DateTime.Parse(_currentwagerOutcomeDetails.Rows[i].Cells[2].Value.ToString()), DisplayDateFormat.ShortDateNoYear) + " " + _currentwagerOutcomeDetails.Rows[i].Cells[3].Value);
          dgvwRollingIfBet.Rows[dgvwRollingIfBet.Rows.Count - 1].Height = GRID_VIEW_ROW_HEIGHT;
        }
      }
      dgvwRollingIfBet.Rows.Add(selectionMark, "", _currentWagerDetails[0].CreditAcctFlag == "Y" ? "CREDIT" : "POST-UP" + " Risk: " + FormatNumber(_currentWagerDetails[0].AmountWagered, false, 1, true) + " To Win: " + FormatNumber(_currentWagerDetails[0].ToWinAmount, false, 1, true));
      dgvwRollingIfBet.Rows[dgvwRollingIfBet.Rows.Count - 1].Height = GRID_VIEW_ROW_HEIGHT;
    }

    private void WriteRollingIfBetsGrvwHeaders() {
      dgvwRollingIfBet.DataSource = null;
      dgvwRollingIfBet.Columns.AddRange(new DataGridViewTextBoxColumn {
        HeaderText = @"Target",
        Name = ColumnNames.Target,
        Width = 50, Visible = true
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"Ticket Wager",
        Name = ColumnNames.TicketWager,
        Width = 100, Visible = true
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"Description",
        Name = ColumnNames.Description,
        Width = 500, Visible = true
      });
    }

    #endregion

    #region Events

    private void btnOk_Click(object sender, EventArgs e) {
      Close();
    }

    private void frmRollingIfBetHookup_Load(object sender, EventArgs e) {
      if (_isFromReadback) {
        WriteRollingIfBetsGrvwHeaders();
        PopulateRollingIfBetFromReadbackGridView();
      }
      else
        PopulateRollingIfBetGridView();
    }

    private void btnExit_Click(object sender, EventArgs e) {
      Close();
    }

    #endregion

  }
}
