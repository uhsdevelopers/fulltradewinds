﻿namespace GUILibraries.Forms {
  partial class FrmInetLogReport {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
      this.dgvInetLogReport = new System.Windows.Forms.DataGridView();
      ((System.ComponentModel.ISupportInitialize)(this.dgvInetLogReport)).BeginInit();
      this.SuspendLayout();
      // 
      // dgvInetLogReport
      // 
      this.dgvInetLogReport.AllowUserToAddRows = false;
      this.dgvInetLogReport.AllowUserToDeleteRows = false;
      this.dgvInetLogReport.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
      this.dgvInetLogReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgvInetLogReport.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dgvInetLogReport.Location = new System.Drawing.Point(5, 5);
      this.dgvInetLogReport.Name = "dgvInetLogReport";
      this.dgvInetLogReport.ReadOnly = true;
      this.dgvInetLogReport.Size = new System.Drawing.Size(660, 398);
      this.dgvInetLogReport.TabIndex = 0;
      // 
      // FrmInetLogReport
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(670, 408);
      this.Controls.Add(this.dgvInetLogReport);
      this.Name = "FrmInetLogReport";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Inet Ticket Log Report";
      this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
      this.Load += new System.EventHandler(this.FrmInetLogReport_Load);
      ((System.ComponentModel.ISupportInitialize)(this.dgvInetLogReport)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.DataGridView dgvInetLogReport;
  }
}