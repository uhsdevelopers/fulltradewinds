﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using InstanceManager.BusinessLayer;
using SIDLibraries.Utilities;
using GUILibraries.BusinessLayer;

namespace GUILibraries.Forms {

  // ReSharper disable once InconsistentNaming
  public class SIDForm : Form {

    #region Private Properties

    private bool ShowErrorMessages { get; set; }

    #endregion

    #region Public Properties

    public ModuleInfo AppModuleInfo {
      get { return _moduleInfo; }
    }

    public bool UseSqlDependency {
      get {
        if (AppModuleInfo == null) {
          return false;
        }
        _useSqlDependency = AppModuleInfo.Parameters.UseSqlDependency;
        return _useSqlDependency;
      }
    }

    #endregion

    #region Protected Properties

    protected SystemParams Params {
      get { return _moduleInfo.Parameters; }
    }

    protected DateTime ServerDateTime {
      get { return _moduleInfo.ServerDateTime; }
    }

    #endregion

    #region Private Variables

    private ArrayList _debugLog;
    private readonly bool _debugActive;
    private FrmLogViewer _frmLogViewer;
    private readonly ModuleInfo _moduleInfo;
    private bool _useSqlDependency;

    #endregion

    #region Structures

    #endregion

    #region Constructors

    protected SIDForm(ModuleInfo moduleInfo) {
      _moduleInfo = moduleInfo;
#if DEBUG
      _debugActive = true;
#else
      _debugActive = false;
#endif
      _debugLog = new ArrayList();
      ShowErrorMessages = _debugActive;
      InitializeComponent();
      InitializeComponentCustom();
    }

    protected SIDForm() {
      _moduleInfo = null;
#if DEBUG
      _debugActive = true;
#else
      _debugActive = false;
#endif
      _debugLog = new ArrayList();
      ShowErrorMessages = _debugActive;
      InitializeComponent();
      InitializeComponentCustom();
    }

    #endregion

    #region Public Methods

    public ArrayList LogData {
      get { return _debugLog; }
      set { _debugLog = value; }
    }

    public object FindControl(string controlName, bool logIfNoFound = false) {
      var control = Controls.Find(controlName, true);
      if (control.Any()) return control.FirstOrDefault();
      if (logIfNoFound) Log(new Exception(Name + " could not find control: " + controlName));
      return null;
    }

    public static List<T> FindControlByType<T>(Control mainControl, bool getAllChild = false) where T : Control {
      var lt = new List<T>();
      for (var i = 0; i < mainControl.Controls.Count; i++) {
        if (mainControl.Controls[i] is T) lt.Add((T)mainControl.Controls[i]);
        if (getAllChild) lt.AddRange(FindControlByType<T>(mainControl.Controls[i], true));
      }
      return lt;
    }

    public static void Log(Exception ex, ModuleInfo moduleInfo, bool isCrititcal = false) {
      using (var lg = new LogWriter(moduleInfo)) {
        lg.WriteToSystemLog(ex);
      }
      if (isCrititcal) Application.Exit();
    }

    public void AssignLogToViewer() {
      if (_frmLogViewer != null && _frmLogViewer.Visible && _debugActive) _frmLogViewer.Logs = _debugLog;
    }

    #endregion

    #region Protected Methods

    protected void AppLog(string operation, string data = null) {
      using (var lg = new LogWriter(_moduleInfo)) {
        lg.WriteToSystemLog("Info", _moduleInfo.Name, operation, _moduleInfo.WindowsUserId, data);
      }
    }

    protected void Log(string text) {
      if (!_debugActive) return;
      _debugLog.Add(ServerDateTime + " || " + GetClassAndMethod() + " || " + text);
      AssignLogToViewer();
    }

    protected void Log(Exception ex, bool isCritical = false) {
      if (_debugActive) {
        _debugLog.Add(ServerDateTime + " || " + GetClassAndMethod() + " || " + ex.Message + " - " + ex.Data);
        AssignLogToViewer();
      }

      using (var lg = new LogWriter(_moduleInfo)) {
        lg.WriteToSystemLog(ex);
      }
      if (isCritical) Application.Exit();
    }

    protected void Log(ArrayList childLog) {
      if (!_debugActive) return;
      _debugLog.AddRange(childLog);
      AssignLogToViewer();
    }

    protected string FormatNumber(double? amount, bool divide = true, bool showZero = false) {
      var ret = DisplayNumber.Format(amount ?? 0, divide, 100, showZero, Params.IncludeCents);
      return ret;
    }

    protected string FormatNumber(double number, bool divide, int divideBy, bool showZero) {
      return DisplayNumber.Format(number, divide, divideBy, showZero, Params.IncludeCents);

    }

    protected string FormatNumber(double number, int divideBy, bool showZero = true, bool showCents = false) {
      return DisplayNumber.Format(number, true, divideBy, showZero, showCents);

    }

    protected string FormatNumber(double number, bool divide, int divideBy, bool showZero, bool showCents) {
      return DisplayNumber.Format(number, divide, divideBy, showZero, showCents);

    }

    protected string FormatNumber(double number, bool showCents) {
      return DisplayNumber.Format(number, false, 1, true, showCents);

    }

    protected string FormatNumber(double number) {
      return DisplayNumber.Format(number, false, 1, true, Params.IncludeCents);

    }

    #endregion

    #region Private Methods

    [MethodImpl(MethodImplOptions.NoInlining)]
    private static string GetClassAndMethod() {
      try {
        var st = new StackTrace();
        var sf = st.GetFrame(2);
        return sf.GetFileName() + "::" + sf.GetMethod().Name;
      }
      catch (Exception) {
        return "";
      }
    }

    private void LogFormUsage(String frmStatus, Form form) {
      using (var lg = new LogWriter(_moduleInfo)) {
        var item = new LogWriter.CuAccessLogInfo() { ProgramName = _moduleInfo.Name, Data = (form.Tag ?? "").ToString(), Operation = frmStatus + " " + form.Name };
        lg.WriteToCuFormsUsageAccessLog(item);
      }
    }

    private void ShowLog() {
      _frmLogViewer = new FrmLogViewer(this);
      _frmLogViewer.Show();
      _frmLogViewer.Logs = _debugLog;
    }

    private bool IsProduction() {
      return (_moduleInfo != null && _moduleInfo.InstanceInfo != null && _moduleInfo.InstanceInfo.IsProduction);
    }

    private void InitializeComponent() {
      SuspendLayout();
      // 
      // SIDForm
      // 
      ClientSize = new Size(284, 262);
      Name = "SIDForm";
      FormClosed += SIDForm_FormClosed;
      ResumeLayout(false);

    }

    private void InitializeComponentCustom() {
      SuspendLayout();
      ClientSize = new Size(284, 262);
      Name = "SIDForm";
      ResumeLayout(false);

      if (!IsProduction())
        Padding = new Padding(5);
      InitializeExtensions();
    }

    private void InitializeExtensions() {
      if (AppModuleInfo == null)
        return;
      FormUsageLogger = new FormUsageLogger(AppModuleInfo);
    }

    #endregion

    #region Events

    private void SIDForm_FormClosed(object sender, FormClosedEventArgs e) {
      if (FormUsageLogger == null || !FormUsageLogger.IsActive())
        return;
      LogFormUsage("User Went To", (Form)sender);
    }

    protected override void OnKeyDown(KeyEventArgs e) {
      base.OnKeyDown(e);

      if (ModifierKeys == Keys.Control && e.KeyCode == Keys.L)
        ShowLog();
    }

    protected override void OnPaint(PaintEventArgs e) {
      base.OnPaint(e);

      if (IsProduction()) return;
      using (var p = new Pen(Color.FromArgb(227, 95, 95))) {
        p.Width = 5;
        e.Graphics.DrawRectangle(p, ClientRectangle);
      }
    }

    #endregion

    #region Extensions

    public FormUsageLogger FormUsageLogger;

    #endregion

    /*
        class ClassTemplate {

        #region Public Properties

        public int a {get; set;}

        #endregion

        #region Private Properties

        private int b {get; set;}

        #endregion

        #region Structures

        struct s {
        string s1;
        }

        #endregion

        #region Constructors

        public ClassTemplate() { 

        }

        #endregion

        #region Public Methods

        public int Sum(int a, int b) { 
        return a + b;
        }

        #endregion 

        #region Private Methods

        private string ClassName() {
        return "ClassTemplate";
        }

        #endregion

        #region Events

        #endregion

        }
      */
  }
}