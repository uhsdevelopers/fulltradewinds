﻿namespace GUILibraries.Forms
{
    partial class FrmRollingIfBetHookup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.dgvwRollingIfBet = new System.Windows.Forms.DataGridView();
      this.panRollingIfBet = new System.Windows.Forms.Panel();
      this.btnOk = new System.Windows.Forms.Button();
      this.btnExit = new System.Windows.Forms.Button();
      ((System.ComponentModel.ISupportInitialize)(this.dgvwRollingIfBet)).BeginInit();
      this.panRollingIfBet.SuspendLayout();
      this.SuspendLayout();
      // 
      // dgvwRollingIfBet
      // 
      this.dgvwRollingIfBet.AllowUserToAddRows = false;
      this.dgvwRollingIfBet.AllowUserToDeleteRows = false;
      this.dgvwRollingIfBet.AllowUserToResizeColumns = false;
      this.dgvwRollingIfBet.AllowUserToResizeRows = false;
      this.dgvwRollingIfBet.BackgroundColor = System.Drawing.Color.White;
      this.dgvwRollingIfBet.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgvwRollingIfBet.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
      this.dgvwRollingIfBet.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
      this.dgvwRollingIfBet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgvwRollingIfBet.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
      this.dgvwRollingIfBet.Location = new System.Drawing.Point(3, 3);
      this.dgvwRollingIfBet.MultiSelect = false;
      this.dgvwRollingIfBet.Name = "dgvwRollingIfBet";
      this.dgvwRollingIfBet.ReadOnly = true;
      this.dgvwRollingIfBet.RowHeadersVisible = false;
      this.dgvwRollingIfBet.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgvwRollingIfBet.ShowEditingIcon = false;
      this.dgvwRollingIfBet.Size = new System.Drawing.Size(589, 457);
      this.dgvwRollingIfBet.TabIndex = 0;
      // 
      // panRollingIfBet
      // 
      this.panRollingIfBet.BackColor = System.Drawing.Color.White;
      this.panRollingIfBet.Controls.Add(this.btnExit);
      this.panRollingIfBet.Controls.Add(this.btnOk);
      this.panRollingIfBet.Controls.Add(this.dgvwRollingIfBet);
      this.panRollingIfBet.Enabled = false;
      this.panRollingIfBet.Location = new System.Drawing.Point(13, 13);
      this.panRollingIfBet.Name = "panRollingIfBet";
      this.panRollingIfBet.Size = new System.Drawing.Size(595, 498);
      this.panRollingIfBet.TabIndex = 1;
      // 
      // btnOk
      // 
      this.btnOk.Location = new System.Drawing.Point(260, 466);
      this.btnOk.Name = "btnOk";
      this.btnOk.Size = new System.Drawing.Size(75, 23);
      this.btnOk.TabIndex = 1;
      this.btnOk.Text = "Ok";
      this.btnOk.UseVisualStyleBackColor = true;
      this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
      // 
      // btnExit
      // 
      this.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnExit.Location = new System.Drawing.Point(477, 465);
      this.btnExit.Name = "btnExit";
      this.btnExit.Size = new System.Drawing.Size(75, 23);
      this.btnExit.TabIndex = 2;
      this.btnExit.Text = "Exit";
      this.btnExit.UseVisualStyleBackColor = true;
      this.btnExit.Visible = false;
      this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
      // 
      // FrmRollingIfBetHookup
      // 
      this.AcceptButton = this.btnOk;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnExit;
      this.ClientSize = new System.Drawing.Size(620, 523);
      this.Controls.Add(this.panRollingIfBet);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "FrmRollingIfBetHookup";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Rolling If-Bet Hookup";
      this.Load += new System.EventHandler(this.frmRollingIfBetHookup_Load);
      ((System.ComponentModel.ISupportInitialize)(this.dgvwRollingIfBet)).EndInit();
      this.panRollingIfBet.ResumeLayout(false);
      this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvwRollingIfBet;
        private System.Windows.Forms.Panel panRollingIfBet;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnExit;
    }
}