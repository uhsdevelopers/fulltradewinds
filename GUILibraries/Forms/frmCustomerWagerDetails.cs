﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using InstanceManager.BusinessLayer;
using SIDLibraries.Entities;
using SIDLibraries.BusinessLayer;
using System.Reflection;
using GUILibraries.Utilities;
using SIDLibraries.Utilities;

namespace GUILibraries.Forms {
  public partial class FrmCustomerWagerDetails : SIDForm {

    #region private Classes

    private class WagerItemDescription {
      public int ItemNumber { get; set; }
      public String Description { get; set; }
      public Boolean AlreadyDisplayed { get; set; }
    }

    #endregion

    #region Private Vars

    private Boolean _rifProcessFinished;
    private readonly Boolean _openPlaysMode;
    private readonly Boolean _enableToEditPendingPlays;
    private readonly FrmTicketInformation _parentFrm;
    private readonly Form _rootForm;

    private int _currentTicketNumber;
    private DateTime _currentTicketPostedDateTime;
    private int _currentWagerNumber;
    private String _currentPlayNumber = "";
    private String _currentWagerType = "";
    private String _currentCustomerCurrency = Currencies.DEFAULT_CURRENCY;
    private Boolean _showRrarTotals;

    private List<spTkWGetCustomerWagerOutcome_Result> _wagerOutcomeDetails;
    private List<spTkWGetCustomerWagerDetails_Result> _wagerDetails;
    private List<spULPGetCurrentUserPermissions_Result> _currentUserPermissions;
    private Boolean _machineHasSoundCard;

    private List<WagerItem.ItemDescription> _itemsDescription;

    private String _wagerDescription;
    private String _wagerTypeName;
    private int? _rrPlaysCount;
    private spTkWGetCustomerTicketDetails_Result ticketDetails;

    #endregion

    #region Public Vars

    #endregion

    #region Public Properties

    // ReSharper disable once MemberCanBePrivate.Global
    public Boolean RollingIfBetMode { get; set; }

    #endregion

    #region Constructors

    public FrmCustomerWagerDetails(int ticketNumber, DateTime ticketPostedDateTime, int wagerNumber, String playNumber, String wagerType, String customerCurrency, ModuleInfo moduleInfo, List<spULPGetCurrentUserPermissions_Result> customerPermissions, Boolean soundCardDetected, int? rRPlaysCount, Boolean openPlaysMode, Boolean enableToEditPendingPlays)
      : base(moduleInfo) {
      LoadMainVariables(ticketNumber, ticketPostedDateTime, wagerNumber, playNumber, wagerType, customerCurrency, customerPermissions, soundCardDetected, rRPlaysCount);
      _openPlaysMode = openPlaysMode;
      _enableToEditPendingPlays = enableToEditPendingPlays;
    }

    public FrmCustomerWagerDetails(FrmTicketInformation parentForm, Form refForm, int ticketNumber, DateTime ticketPostedDateTime, int wagerNumber, String playNumber, String wagerType, String customerCurrency, List<spULPGetCurrentUserPermissions_Result> customerPermissions, Boolean soundCardDetected, int? rRPlaysCount, Boolean openPlaysMode, Boolean enableToEditPendingPlays)
      : base(parentForm.AppModuleInfo) {
      LoadMainVariables(ticketNumber, ticketPostedDateTime, wagerNumber, playNumber, wagerType, customerCurrency, customerPermissions, soundCardDetected, rRPlaysCount);
      _parentFrm = parentForm;
      _rootForm = refForm;
      _openPlaysMode = openPlaysMode;
      _enableToEditPendingPlays = enableToEditPendingPlays;
    }

    private void LoadMainVariables(int ticketNumber, DateTime ticketPostedDateTime, int wagerNumber, string playNumber, string wagerType, string customerCurrency, List<spULPGetCurrentUserPermissions_Result> customerPermissions, bool soundCardDetected, int? rRPlaysCount) {
      if (ticketNumber > 0) {
        _currentTicketNumber = ticketNumber;
        _currentTicketPostedDateTime = ticketPostedDateTime;
        _currentWagerNumber = wagerNumber;
        _currentPlayNumber = playNumber;
        _currentWagerType = wagerType;
        _currentCustomerCurrency = customerCurrency;
        _currentUserPermissions = customerPermissions;
        _machineHasSoundCard = soundCardDetected;
        _rrPlaysCount = rRPlaysCount;
        InitializeComponent();
      }
      else {
        MessageBox.Show(@"No ticketnumber was selected");
        Close();
      }
    }

    #endregion

    #region Private Methods

    private void AddRollingIfBetTicketInfo(string ifBetType) {
      var parentProperty = _rootForm.GetType().GetProperty("RollingIfBetMode");
      parentProperty.SetValue(_rootForm, true, null);

      var cust = (spCstGetCustomer_Result)_rootForm.GetType().GetProperty("Customer").GetValue(_rootForm, null);
      var info = new RifBackwardTicketInfo.RifbTicketInfo {
        TicketNumber = _currentTicketNumber,
        WagerNumber = _currentWagerNumber,
        RifCurrentPlayFlag = "N",
        RifRiskAmount = double.Parse(lblRiskText.Text),
        RifToWinAmount = double.Parse(lblToWinText.Text),
        LimitRifToRisk = cust.LimitRifToRiskFlag == null ? "N" : cust.LimitRifToRiskFlag.Trim()
      };
      switch (ifBetType) {
        case "If WIN Only":
          info.WinOnlyFlag = "Y";
          break;
        case "If WIN Or PUSH":
          info.WinOnlyFlag = "N";
          break;
      }
      parentProperty = _rootForm.GetType().GetProperty("RifBTicketInfo");
      parentProperty.SetValue(_rootForm, info, null);

      var parentAvailAmtTxt = (TextBox)_rootForm.Controls.Find("txtAvailableAmount", true).FirstOrDefault();

      if (parentAvailAmtTxt != null) {
        if (info.LimitRifToRisk == "Y")
          parentAvailAmtTxt.Text = FormatNumber(info.RifRiskAmount, false, 1, true) + @" *" + ifBetType + @"*";
        else
          parentAvailAmtTxt.Text = FormatNumber(info.RifRiskAmount + info.RifToWinAmount, false, 1, true) + @" *" + ifBetType + @"*";
      }
      var chbRollingIf = (CheckBox)_rootForm.Controls.Find("chbRollingIF", true).FirstOrDefault();

      if (chbRollingIf != null && !chbRollingIf.Checked)
        chbRollingIf.Checked = true;
      _rifProcessFinished = true;
    }

    private string GetContestOutcome(int gameContestNum) {
      spCnGetContestOutcome_Result contestOutcome;
      using (var cn = new Contests(AppModuleInfo))
        contestOutcome = cn.GetContestOutcome(gameContestNum).FirstOrDefault();

      if (contestOutcome == null)
        return string.Empty;
      var contestOutcomeDetails = contestOutcome.ContestType + " - " + contestOutcome.ContestDesc;

      if (contestOutcome.contestWinner.Length > 0)
        contestOutcomeDetails += Environment.NewLine + "Winner: " + contestOutcome.contestWinner;
      if (contestOutcome.Comments.Length <= 0)
        return contestOutcomeDetails;
      contestOutcomeDetails += Environment.NewLine + Environment.NewLine + contestOutcome.Comments;
      return contestOutcomeDetails;
    }

    private void GetCustomerContestOutcome() {
      List<spCnGetCustomerContestOutcome_Result> contestDetails;

      using (var cn = new Contests(AppModuleInfo))
        contestDetails = cn.GetCustomerContestOutcome(_currentTicketNumber, _currentWagerNumber).ToList();
      if (!contestDetails.Any())
        return;
      WriteWagerOutcomeGrvwHeaders();

      foreach (var res in contestDetails) {
        var wagerDescription = WriteContestDescription(res);
        var gameDateTime = DateTime.Parse(res.ContestDateTime.ToString());
        dgvwWagerOutcome.Rows.Add(res.ContestNum.ToString(CultureInfo.InvariantCulture), res.Outcome, DisplayDate.Format(gameDateTime, DisplayDateFormat.LongDateTime), wagerDescription, null, null, null, null, "C", null, "Y");
      }
      dgvwWagerOutcome.Columns[0].Visible = false;
      dgvwWagerOutcome.ClearSelection();
    }

    private void GetCustomerTicketDetails() {
      using (var tAw = new TicketsAndWagers(AppModuleInfo)) {
        ticketDetails = tAw.GetCustomerTicketDetails(_currentTicketNumber, _currentTicketPostedDateTime).ToList().FirstOrDefault();

        if (ticketDetails == null)
          return;
        lblPostedText.Text = DisplayDate.Format(DateTime.Parse(ticketDetails.PostedDateTime.ToString()), DisplayDateFormat.LongDateTime);
        lblWrittenByText.Text = ticketDetails.TicketWriter;
        lblTicketNumberText.Text = ticketDetails.TicketNumber.ToString(CultureInfo.InvariantCulture);
        lblAgentText.Text = ticketDetails.AgentID ?? "";

        if (ticketDetails.TotalTicket != null)
          lblTotalTicketText.Text = FormatNumber((double)ticketDetails.TotalTicket, true, 1, false);
      }
    }

    private void GetCustomerWagerDetails(Boolean showRrarTotal) {
      if (!showRrarTotal) {
        using (var tAw = new TicketsAndWagers(AppModuleInfo)) {
          _wagerDetails = tAw.GetCustomerWagerDetails(_currentTicketNumber, _currentWagerNumber, _currentTicketPostedDateTime).ToList();

          if (_wagerDetails == null || !_wagerDetails.Any()) return;
          _wagerDescription = _wagerDetails[0].Description;
          _wagerTypeName = _wagerDetails[0].WagerType;

          string compositeWagerStatus;

          switch (_wagerDetails[0].WagerType) {
            case "Teaser":
              compositeWagerStatus = _wagerDetails[0].WagerStatus + " - " + _wagerDetails[0].TotalPicks + " teams";
              break;
            case "Parlay":
              compositeWagerStatus = _wagerDetails[0].WagerStatus + " - " + _wagerDetails[0].TotalPicks + " teams";
              break;
            default:
              compositeWagerStatus = _wagerDetails[0].WagerStatus;
              break;
          }
          lblTypeText.Text = TicketsAndWagers.WriteWagerTypeName(_wagerDetails[0]);
          lblDocNumText.Text = "";

          if (_wagerDetails[0].DocumentNumber != 0)
            lblDocNumText.Text = _wagerDetails[0].DocumentNumber.ToString(CultureInfo.InvariantCulture);
          lblTiesText.Text = _wagerDetails[0].Ties;
          lblAcctText.Text = _wagerDetails[0].CreditAcctFlag == "Y" ? "Credit" : "Postup";
          if (_wagerDetails[0].FreePlayFlag == "Y")
            lblAcctText.Text = @"FREE PLAY";
          lblRiskText.Text = FormatNumber(_wagerDetails[0].AmountWagered, true, 1, false);
          lblToWinText.Text = FormatNumber(_wagerDetails[0].ToWinAmount, true, 1, false);

          lblWinLossRatioText.Text = "";

          if (_currentWagerNumber == 0 && (_wagerDetails[0].WagerType == "Round Robin" || (_wagerDetails[0].WagerType != null && _wagerDetails[0].WagerType.Contains("Action Reverse"))))
            lblPaidText.Text = FormatNumber(double.Parse(_wagerDetails[0].PaidAmount.ToString()), true, 100, false);
          else
            lblPaidText.Text = FormatNumber(GetPaidAmount(_wagerDetails[0].CreditAcctFlag, _wagerDetails[0].AmountWagered, _wagerDetails[0].AmountWon, _wagerDetails[0].AmountLost, _wagerDetails[0].WagerStatus, _wagerDetails[0].FreePlayFlag, _wagerDetails[0].WagerType), false, 1, false);
          lblStatusText.Text = compositeWagerStatus;
          lblLostText.Text = FormatNumber(double.Parse(_wagerDetails[0].AmountLost.ToString(CultureInfo.InvariantCulture)), true, 1, false);
          lblWonText.Text = FormatNumber(double.Parse(_wagerDetails[0].AmountWon.ToString(CultureInfo.InvariantCulture)), true, 1, false);

          if (!(_wagerDetails[0].RifForwardTicketNumber == null && _wagerDetails[0].RifBackwardTicketNumber == null)) {
            lblRolledForwardAmtText.Text = _wagerDetails[0].RifForwardAmountWagered.ToString(CultureInfo.InvariantCulture);
            panRollingIf.Visible = true;
          }
          else
            panRollingIf.Visible = false;
          switch (_wagerDetails[0].WagerType) {
            case "Contest":
              GetCustomerContestOutcome();
              break;
            case "Manual Play":
              GetCustomerManualWagerOutcome();
              break;
            default:
              GetCustomerWagerOutcome();
              break;
          }
          if (!String.IsNullOrEmpty(_wagerDetails[0].LastEditedBy)) {
            lblWagerLastEdited.Text = @"Last Changed By: " + _wagerDetails[0].LastEditedBy.Trim() + @" On: " + (DateTimeF.FormatWagerDateTime((DateTime)_wagerDetails[0].LastEditedByOn));
          }
          else {
            lblWagerLastEdited.Visible = false;
          }

        }
      }
      else {
        int playNumber;
        int.TryParse(_currentPlayNumber, out playNumber);
        using (var tAw = new TicketsAndWagers(AppModuleInfo)) {
          var wagerRrarDetails = tAw.GetCustomerRR_ARWagerDetails(_currentTicketNumber, playNumber, _currentTicketPostedDateTime).ToList();

          _wagerDescription = wagerRrarDetails[0].Description;
          _wagerTypeName = wagerRrarDetails[0].WagerType;
          string compositeWagerType;
          switch (wagerRrarDetails[0].WagerType) {
            case "Parlay":
              compositeWagerType = wagerRrarDetails[0].WagerType; //+ " (" + wagerDetails.TotalPicks.ToString() + " teams)";
              break;
            default:
              compositeWagerType = wagerRrarDetails[0].WagerType;
              break;
          }
          lblTypeText.Text = compositeWagerType;
          if (_rrPlaysCount != null && _rrPlaysCount > 0) {
            var awayAmount = (from d in wagerRrarDetails select d.AmountWagered).Sum() / (int)_rrPlaysCount;
            lblTypeText.Text += @" (" + _rrPlaysCount + @" parlays - " + FormatNumber(awayAmount, false, 1, true) + @" " + _currentCustomerCurrency.Substring(0, 3) + @" away)";
          }
          lblDocNumText.Text = string.Empty;
          lblTiesText.Text = string.Empty;
          lblAcctText.Text = wagerRrarDetails[0].CreditAcctFlag == "Y" ? "Credit" : "Postup";
          lblRiskText.Text = FormatNumber((from d in wagerRrarDetails select d.AmountWagered).Sum(), false, 1, false);
          lblToWinText.Text = FormatNumber((from d in wagerRrarDetails select d.ToWinAmount).Sum(), false, 1, false);
          lblWinLossRatioText.Text = string.Empty;
          lblPaidText.Text = FormatNumber(GetPaidAmount(wagerRrarDetails), false, 1, false); //  GetPaidAmount(wagerRRARDetails);
          lblStatusText.Text = string.Empty;
          lblLostText.Text = FormatNumber((from d in wagerRrarDetails select d.AmountLost).Sum(), false, 1, false);
          lblWonText.Text = FormatNumber((from d in wagerRrarDetails select d.AmountWon).Sum(), false, 1, false);

          panRollingIf.Visible = false;

          GetCustomerWagerOutcome();
        }
      }
    }

    private void GetCustomerManualWagerOutcome() {
      int playNumber;
      int.TryParse(_currentPlayNumber, out playNumber);

      using (var tAw = new TicketsAndWagers(AppModuleInfo)) {
        var manualWagerOutcomeDetails = tAw.GetCustomerManualWagerOutcome(_currentTicketNumber, _currentWagerNumber, 1, _currentTicketPostedDateTime).ToList();
        if (!manualWagerOutcomeDetails.Any()) return;
        WriteWagerOutcomeGrvwHeaders();
        foreach (var res in manualWagerOutcomeDetails.OrderBy(i => i.ItemNumber)) {
          var itemDescription = res.Description.Trim().Replace("|", "\r\n"); //+ "Risking " + riskAmt + " to win " + toWinAmt;
          var numberOfLines = itemDescription.Split(new[] { Environment.NewLine }, StringSplitOptions.None).Count();
          var gameDateTime = DateTime.Parse(res.GradeDateTime.ToString());
          dgvwWagerOutcome.Rows.Add(res.TicketNumber.ToString(CultureInfo.InvariantCulture),
                                      res.Outcome,
                                      DisplayDate.Format(gameDateTime, DisplayDateFormat.LongDateTime),
                                      itemDescription,
                                      "", "", "", "", "A", "", "");
          if (numberOfLines <= 0) continue;
          if (dgvwWagerOutcome.Columns[ColumnNames.Wager] != null) dgvwWagerOutcome.Columns[ColumnNames.Wager].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
          dgvwWagerOutcome.Rows[dgvwWagerOutcome.Rows.Count - 1].Height = 15 * numberOfLines;
        }
      }
    }

    private void GetCustomerWagerOutcome() {
      int playNumber;
      int.TryParse(_currentPlayNumber, out playNumber);

      using (var tAw = new TicketsAndWagers(AppModuleInfo)) {
        _wagerOutcomeDetails = tAw.GetCustomerWagerOutcome(_currentTicketNumber, _currentWagerNumber, playNumber, _currentTicketPostedDateTime).ToList();

        if (_wagerOutcomeDetails == null || !_wagerOutcomeDetails.Any())
          return;
        WriteWagerOutcomeGrvwHeaders();

        _itemsDescription = new List<WagerItem.ItemDescription>();
        var distinctItemsDescriptionObj = new List<WagerItemDescription>();

        if (!string.IsNullOrEmpty(_wagerDescription)) {
          var allItemsDescription = _wagerTypeName.Contains("Action Reverse") ? _wagerDescription.Split(new[] { "|", "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries).ToList() : _wagerDescription.Split(new[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();
          if (_wagerTypeName.Contains("Action Reverse")) {
            CheckOutcomeDetailsItemsSort();
          }
          var distinctItemsDescription = (from d in allItemsDescription select d.Trim()).Distinct().ToList();
          if (distinctItemsDescription.Count > 0) {
            var i = 0;
            foreach (var str in distinctItemsDescription) {
              distinctItemsDescriptionObj.Add(new WagerItemDescription { Description = str, AlreadyDisplayed = false, ItemNumber = i + 1 });
              i++;
            }
          }
        }

        foreach (var res in _wagerOutcomeDetails.OrderBy(i => i.ItemNumber)) {
          var itemWagerDescription = !string.IsNullOrEmpty(_wagerDescription) ? GetItemDescription(res, distinctItemsDescriptionObj) : TicketsAndWagers.WriteWagerDescription(res);

          if (!string.IsNullOrEmpty(_wagerDescription)) {
            var distinctItemToUpdate = distinctItemsDescriptionObj.SingleOrDefault(x => x.Description == itemWagerDescription);
            if (distinctItemToUpdate != null)
              distinctItemToUpdate.AlreadyDisplayed = true;
          }

          var isContest = "N";

          if (res.WagerType.Trim() == "Contest" || res.WagerType.Trim() == "C")
            isContest = "Y";
          // wagerDescription: add risk and to win if currentWagerNumber > 0 DMN 20130921 20131003
          if ((_currentWagerType == "If Bet" || _currentWagerType == "(AR) If-Bet" || _currentWagerType == "(ARBC) If-Bet") && _currentWagerNumber > 0)
            itemWagerDescription += "  Risk: " + (res.AmountWagered.HasValue ? Math.Floor(res.AmountWagered.Value / 100) : 0)
            + " To Win: " + (res.ToWinAmount.HasValue ? Math.Floor(res.ToWinAmount.Value / 100) : 0);
          if (res.OriginatingTicketNumber.HasValue && res.OriginatingTicketNumber != 0 && !itemWagerDescription.Contains("Ref Ticket#"))
            itemWagerDescription += " (Ref Ticket# " + res.OriginatingTicketNumber.Value + ")";
          var gameDateTime = DateTime.Parse(res.GameDateTime.ToString());
          dgvwWagerOutcome.Rows.Add((res.GameNum ?? 0).ToString(CultureInfo.InvariantCulture), res.Outcome, DisplayDate.Format(gameDateTime, DisplayDateFormat.LongDateTime), itemWagerDescription, res.PeriodDescription, res.Team1ID, res.Team2ID, res.ChosenTeamID, res.WagerType, res.PeriodNumber, isContest);
          _itemsDescription.Add(new WagerItem.ItemDescription {
            TicketNumber = (res.TicketNumber ?? 0).ToString(CultureInfo.InvariantCulture),
            WagerNumber = (res.WagerNumber ?? 0).ToString(CultureInfo.InvariantCulture),
            ItemNumber = (res.ItemNumber ?? 0).ToString(CultureInfo.InvariantCulture),
            ItemBuiltDescription = itemWagerDescription
          });
        }
        var dataGridViewColumn = dgvwWagerOutcome.Columns[ColumnNames.GameContestNum];
        if (dataGridViewColumn != null)
          dataGridViewColumn.Visible = false;
        dgvwWagerOutcome.ClearSelection();
        //FormF.PreventDataGridViewColumnsToBeSortedAndResized(dgvwWagerOutcome);

      }
    }

    private void CheckOutcomeDetailsItemsSort() {
      if (_wagerOutcomeDetails == null || !_wagerOutcomeDetails.Any())
        return;
      List<spTkWGetWagerItems_Result> items;
      int wagerNumber = _currentWagerNumber == 0 ? 1 : _currentWagerNumber;

      using (var tAw = new TicketsAndWagers(AppModuleInfo)) {
        items = tAw.GetWagerItems(_currentTicketNumber, wagerNumber);
      }

      foreach (var res in _wagerOutcomeDetails) {
        int? itemNumber = (from wi in items
                           where wi.WagerType == res.WagerType && wi.AdjSpread == res.AdjSpread
                                 && wi.FinalMoney == res.FinalMoney
                                 && wi.AdjTotalPoints == res.AdjTotalPoints
                                 && wi.ChosenTeamID.Trim() == res.ChosenTeamID.Trim()
                                 && wi.PeriodNumber == res.PeriodNumber
                                 && wi.GameNum == res.GameNum
                                 && wi.TotalPointsOU.Trim() == res.TotalPointsOU.Trim()
                           select wi.ItemNumber).FirstOrDefault();

        res.ItemNumber = (int)itemNumber;
      }
    }

    private string GetItemDescription(spTkWGetCustomerWagerOutcome_Result res, List<WagerItemDescription> distinctItemsDescription) {
      var itemDescription = string.Empty;

      if (distinctItemsDescription.Count == 0 || distinctItemsDescription.Count < res.ItemNumber)
        return "";
      if (_currentWagerType == "Straight" || _currentWagerType == "Layoff Straight" || _currentWagerType == "If Bet" || _currentWagerType == "Parlay" || _currentWagerType == "Teaser") {
        var desc = distinctItemsDescription[(res.ItemNumber ?? 0) - 1].Description;

        if (res.SportType.Trim() == SportTypes.BASEBALL && !desc.Contains("Pitchers")) {
          var pitcher1Required = res.Pitcher1ReqFlag == "Y";
          var pitcher2Required = res.Pitcher2ReqFlag == "Y";


          if (res.AdjustableOddsFlag == "N") {
            desc += " Price Is Fixed ";
          }

          desc += " Pitchers: " + res.ListedPitcher1.Trim() + " (" + (pitcher1Required ? "must start" : "action") + ")";

          if (pitcher2Required)
            desc += (res.ListedPitcher1.Trim().Length > 0 ? ", " : " ")
                             + res.ListedPitcher2.Trim() + " (must start)";
          else
            desc += (res.ListedPitcher1.Trim().Length > 0 ? ", " : " ")
                             + res.ListedPitcher2.Trim() + " (action)";
        }

        return desc;
      }
      switch (res.WagerType) {
        case WagerType.SPREAD:
        case WagerType.MONEYLINE:
        case WagerType.TEAMTOTALPOINTS:
          var chosenRotationNumber = res.ChosenTeamID.Trim() == res.Team1ID.Trim() ? res.Team1RotNum : res.Team2RotNum;

          if (res.WagerType == WagerType.TEAMTOTALPOINTS) {
            if (res.TotalPointsOU.ToLower() == "o") {
              itemDescription = (from i in distinctItemsDescription
                                 where i.Description.Contains(res.ChosenTeamID.Trim()) && i.Description.Contains((chosenRotationNumber ?? 0).ToString(CultureInfo.InvariantCulture))
                                     && i.Description.Contains(res.SportType.Trim()) && i.Description.Contains(res.PeriodDescription.Trim())
                                     && i.Description.ToLower().Contains("team total over") && !i.AlreadyDisplayed
                                 select i.Description).FirstOrDefault();
            }
            else {
              itemDescription = (from i in distinctItemsDescription
                                 where i.Description.Contains(res.ChosenTeamID.Trim()) && i.Description.Contains((chosenRotationNumber ?? 0).ToString(CultureInfo.InvariantCulture))
                                     && i.Description.Contains(res.SportType.Trim()) && i.Description.Contains(res.PeriodDescription.Trim())
                                     && i.Description.ToLower().Contains("team total under") && !i.AlreadyDisplayed
                                 select i.Description).FirstOrDefault();
            }
          }
          else {
            if (res.SportType.Trim() == SportTypes.SOCCER && res.ChosenTeamID.Contains("Draw")) {
              itemDescription = (from i in distinctItemsDescription
                                 where i.Description.Contains(res.ChosenTeamID.Trim())
                                     && i.Description.Contains(res.SportType.Trim()) && i.Description.Contains(res.PeriodDescription.Trim())
                                     && (!i.Description.ToLower().Contains("team total over") && !i.Description.ToLower().Contains("team total under")) && !i.AlreadyDisplayed
                                 select i.Description).FirstOrDefault();
              itemDescription = res.Team2RotNum + " " + itemDescription;
            }
            else {
              itemDescription = (from i in distinctItemsDescription
                                 where i.Description.Contains(res.ChosenTeamID.Trim()) && i.Description.Contains((chosenRotationNumber ?? 0).ToString(CultureInfo.InvariantCulture))
                                     && i.Description.Contains(res.SportType.Trim()) && i.Description.Contains(res.PeriodDescription.Trim())
                                     && (!i.Description.ToLower().Contains("team total over") && !i.Description.ToLower().Contains("team total under")) && !i.AlreadyDisplayed
                                 select i.Description).FirstOrDefault();
            }
          }
          break;
        case WagerType.TOTALPOINTS:
          itemDescription = (from i in distinctItemsDescription
                             where i.Description.Contains(res.Team1ID.Trim() + "/" + res.Team2ID.Trim())
                                   && (i.Description.Contains((res.Team1RotNum ?? 0).ToString(CultureInfo.InvariantCulture))
                                       || i.Description.Contains((res.Team2RotNum ?? 0).ToString(CultureInfo.InvariantCulture)))
                                   && i.Description.Contains(res.SportType.Trim())
                                   && i.Description.Contains(res.PeriodDescription.Trim()) && !i.AlreadyDisplayed
                             select i.Description).FirstOrDefault();
          break;
      }

      if (res.SportType != null && res.SportType.Trim() == SportTypes.BASEBALL && itemDescription != null && !itemDescription.Contains("Pitchers")) {
        var pitcher1Required = res.Pitcher1ReqFlag == "Y";
        var pitcher2Required = res.Pitcher2ReqFlag == "Y";

        if (res.AdjustableOddsFlag == "N") {
          itemDescription += " Price Is Fixed ";
        }

        itemDescription += " Pitchers: " + res.ListedPitcher1.Trim() + " (" + (pitcher1Required ? "must start" : "action") + ")";

        if (pitcher2Required)
          itemDescription += (res.ListedPitcher1.Trim().Length > 0 ? ", " : " ")
                           + res.ListedPitcher2.Trim() + " (must start)";
        else
          itemDescription += (res.ListedPitcher1.Trim().Length > 0 ? ", " : " ")
                           + res.ListedPitcher2.Trim() + " (action)";
      }

      return itemDescription;
    }

    private static double GetPaidAmount(string isCreditAcct, double amountWagered, double amountWon, double amountLost, string wagerStatus, string freePlayFlag, string wagerType) // was non-static, string
    {
      double paidAmount = 0;

      if (isCreditAcct == "Y") {
        switch (wagerStatus) {
          case "Win":
            paidAmount += amountWon;
            break;
          case "Loss":
            if (wagerType.Contains("AR Birdcage"))
              paidAmount += amountLost * -1;
            else
              paidAmount += amountWagered * -1;
            break;
        }
        return paidAmount;
      }
      switch (wagerStatus) {
        case "Win":
          if (freePlayFlag == "N")
            paidAmount += amountWagered + amountWon;
          else
            paidAmount += amountWon;
          break;
        case "Cancel":
          if (freePlayFlag == "N")
            paidAmount += amountWagered;
          break;
      }
      return paidAmount;
    }

    private static double GetPaidAmount(IList<spTkWGetCustomerRR_ARWagerDetails_Result> result) // was non-static, string
    {
      return result.Sum(item => GetPaidAmount(result[0].CreditAcctFlag, item.AmountWagered, item.AmountWon, item.AmountLost, item.WagerStatus, item.FreePlayFlag, item.WagerType)); // FormatNumber(paidAmount, false, 1, false)
    }

    private void HandleKeyDown(object sender, KeyEventArgs e) {
      switch (e.KeyValue) {
        case (int)Keys.Right:
          ((DataGridView)sender).HorizontalScrollingOffset += 50;
          break;
        case (int)Keys.Left:
          if (((DataGridView)sender).HorizontalScrollingOffset - 50 < 0)
            ((DataGridView)sender).HorizontalScrollingOffset = 0;
          else ((DataGridView)sender).HorizontalScrollingOffset -= 50;
          break;
      }
    }

      private bool LoadOpenPlayWagerForm()
          // string wagerType (not used), out Boolean closeForms (changed method to bool)
    {
      var cont = true;
      var parentProperty = _rootForm.GetType().GetProperty("Ticket");
      if (parentProperty == null)
        return true;
          var ticketInfo = (Ticket) parentProperty.GetValue(_rootForm, null);

          if (ticketInfo != null && ticketInfo.Wagers != null && ticketInfo.Wagers.Count > 0)
          {
        var wagers = ticketInfo.Wagers;

        if ((from w in wagers
             let ticketNumber = w.FromPreviousTicket
             let wagerNumber = w.FromPreviousWagerNumber
             where !string.IsNullOrEmpty(ticketNumber)
                   && ticketNumber == _wagerDetails[0].TicketNumber.ToString(CultureInfo.InvariantCulture)
                   && !string.IsNullOrEmpty(wagerNumber)
                   && wagerNumber == _wagerDetails[0].WagerNumber.ToString(CultureInfo.InvariantCulture)
             select ticketNumber).Any())
          cont = false;
      }
      if (!cont)
        return false;

      parentProperty = _rootForm.GetType().GetProperty("OpenPlayWagerInfo");
      parentProperty.SetValue(_rootForm, _wagerDetails[0], null);

      if (_itemsDescription != null && _itemsDescription.Count > 0) {
        parentProperty = _rootForm.GetType().GetProperty("OpenPlayItemsDescription");
        parentProperty.SetValue(_rootForm, _itemsDescription, null);
      }
      var method = _rootForm.GetType().GetMethod("DisplayOpenBetWageringForm", BindingFlags.Public | BindingFlags.Instance);
      if (_wagerDetails[0].WagerStatus == "Pending") {
        List<spTkWGetWagerItems_Result> wItems;
        spTkWGetWagerManualPlayItem_Result mItem = null;
        spTkWGetContestWagerItem_Result cItem = null;
        spTkWGetCustomerWager_Result wager;

        using (var tAw = new TicketsAndWagers(AppModuleInfo)) {
          wager = tAw.GetWager(_currentTicketNumber, _currentWagerNumber);
          wItems = tAw.GetWagerItems(_currentTicketNumber, _currentWagerNumber);

          if (_currentWagerType == "Manual Play")
            mItem = tAw.GetManualWagerItem(_currentTicketNumber, _currentWagerNumber);
          if (_currentWagerType == "Straight") {
            cItem = tAw.GetContestWagerItem(_currentTicketNumber, _currentWagerNumber);
          }

          if (ticketInfo != null && ticketDetails != null) {
            using (var gl = new GamesAndLines(AppModuleInfo)) {
              cont = true;
              var activeGames = gl.GetActiveGamesByCustomer(ticketInfo.CustomerId, ticketDetails.Store);
              if (activeGames == null || activeGames.Count == 0)
                return false;
              if (wItems != null && wItems.Count > 0) {
                foreach (var item in wItems) {
                  var gameNum = item.GameNum;
                  var periodNum = item.PeriodNumber;
                  if (activeGames.Any(g => g.GameNum == gameNum && g.PeriodNumber == periodNum)) continue;
                  cont = false;
                  break;
                }
              }
            }

            if (cItem != null) {
              cont = true;
              var contestNum = cItem.ContestNum;
              using (var con = new Contests(AppModuleInfo)) {
                var activeContests = con.GetActiveContestsByCustomer(ticketInfo.CustomerId.Trim());
                if (activeContests == null || activeContests.Count == 0)
                  return false;
                if (activeContests.All(c => c.ContestNum != contestNum)) {
                  cont = false;
                }
              }
            }
          }

          if (!cont) {
            MessageBox.Show(@"At least one of the items did not pass the Wager cutoffTime Validation", @"Validation");
            return true;
          }
        }

        if (wager != null) {
          if (wItems != null && wItems.Any()) {
            parentProperty = _rootForm.GetType().GetProperty("PendingPlayItemsInDb");
            parentProperty.SetValue(_rootForm, wItems, null);
          }

          if (mItem != null) {
            parentProperty = _rootForm.GetType().GetProperty("PendingManualPlayItemInDb");
            parentProperty.SetValue(_rootForm, mItem, null);
          }

          if (cItem != null) {
            parentProperty = _rootForm.GetType().GetProperty("PendingContestItemInDb");
            parentProperty.SetValue(_rootForm, cItem, null);
          }

          parentProperty = _rootForm.GetType().GetProperty("PendingWagerInDb");
          parentProperty.SetValue(_rootForm, wager, null);
          method = _rootForm.GetType().GetMethod("DisplayWageringFormInEditMode", BindingFlags.Public | BindingFlags.Instance);
        }
      }

      method.Invoke(_rootForm, null);
      return true;
    }

    private void LoadCustomerWagerDetails() {
      if (_currentWagerNumber == 0 && (_currentWagerType == "Round Robin" || _currentWagerType == "Action Reverse" || _currentWagerType == "AR Birdcage"))
        _showRrarTotals = true;

      GetCustomerTicketDetails();
      GetCustomerWagerDetails(_showRrarTotals);

      lblCustomerCurrencyLegendText.Text = _currentCustomerCurrency;
      lblCustomerCurrencyLegendText.TextAlign = ContentAlignment.MiddleCenter;

      if (dgvwWagerOutcome.Rows.Count == 0) {
        btnGameScores.Enabled = false;
        btnPlaybackConversation.Enabled = false;
      }
      else {
        btnGameScores.Enabled = true;
        btnPlaybackConversation.Enabled = true;
      }
      panWagerRollingIf.Visible = RollingIfBetMode;

      if ((_openPlaysMode && (_currentWagerType == "Teaser" || _currentWagerType == "Parlay")) || EditingExistingWagerIsAllowed()) {
        //if (dgvwWagerOutcome.Rows.Count > 0) {
        panOpenPlay.Visible = true;
        btnOpenPlay.Text += _currentWagerType;
      }
      else
        panOpenPlay.Visible = false;


      if (!LoginsAndProfiles.ValidateUserFunctionalityAccess(_currentUserPermissions, LoginsAndProfiles.PLAYBACK_AUDIO) || _machineHasSoundCard == false)
        btnPlaybackConversation.Enabled = false;
    }

    private bool EditingExistingWagerIsAllowed() {
      return (_enableToEditPendingPlays && (_rootForm.AccessibleDescription ?? "").ToLower() == "isbtw" &&
          LoginsAndProfiles.ValidateUserFunctionalityAccess(_currentUserPermissions, LoginsAndProfiles.MODIFY_EXISTING_PENDING_WAGER) &&
          (_currentWagerType == "Straight" ||
          _currentWagerType == "Money Line" ||
          _currentWagerType == "Total Points" ||
          _currentWagerType == "Team Total" ||
          _currentWagerType == "Manual Play" ||
          _currentWagerType == "Contest" ||
          (_currentWagerType == "Teaser" && _wagerDetails[0].RoundRobinLink == null) ||
          (_currentWagerType == "Parlay" && _wagerDetails[0].RoundRobinLink == null) ||
          _currentWagerType.StartsWith("If Bet")) &&
          _wagerDetails != null &&
          _wagerDetails[0].WagerStatus == "Pending" &&
          (_wagerDetails[0].RifBackwardTicketNumber == null || _wagerDetails[0].RifBackwardTicketNumber == 0) &&
          (_wagerDetails[0].RifForwardTicketNumber == null || _wagerDetails[0].RifForwardTicketNumber == 0)
          );
    }

    private void WriteWagerOutcomeGrvwHeaders() {
      dgvwWagerOutcome.DataSource = null;
      dgvwWagerOutcome.Columns.AddRange(new DataGridViewTextBoxColumn {
        HeaderText = @"GameContestNum",
        AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader,
        Name = ColumnNames.GameContestNum,
        Width = 100,
        Visible = false
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"Outcome",
        AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells,
        Name = ColumnNames.Outcome,
        Width = 100
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"Game Time",
        AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader,
        Name = ColumnNames.GameTime,
        Width = 150
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"Wager",
        Name = ColumnNames.Wager, Width = 750,
        AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"PeriodDescription",
        AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader,
        Name = ColumnNames.PeriodDescription,
        Width = 100,
        Visible = false
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"Team1ID",
        Name = ColumnNames.Team1Id,
        Width = 100,
        Visible = false
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"Team2ID",
        Name = ColumnNames.Team2Id,
        Width = 100,
        Visible = false
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"ChosenTeamId",
        Name = ColumnNames.ChosenTeamId,
        Visible = false
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"ItemWagerType",
        Name = ColumnNames.ItemWagerType,
        Visible = false
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"PeriodNumber",
        Name = ColumnNames.PeriodNumber,
        Visible = false
      }, new DataGridViewTextBoxColumn {
        HeaderText = @"ItemIsContest",
        AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCellsExceptHeader,
        Name = ColumnNames.ItemIsContest,
        Width = 50,
        Visible = false
      });
    }

    private static String WriteContestDescription(spCnGetCustomerContestOutcome_Result res) {

      var contestDescription = (res.RotNum ?? 0).ToString() + " - ";

      if (res.ContestantName != null)
        contestDescription += res.ContestantName.Trim() + " "; //contestDescription += " - ";
      var thresholdLine = string.Empty;

      if (res.ThresholdLine != null)
        thresholdLine = res.ThresholdLine.ToString();

      if (res.ThresholdType == "P") {
        contestDescription += thresholdLine.Replace("+", "").Replace("-", "").Replace(".5", "½") + " ";

        if (res.ThresholdUnits != null)
          contestDescription += res.ThresholdUnits.Trim();
      }
      if (res.ThresholdType == "S") {
        if (res.ThresholdLine > 0)
          contestDescription += "+";
        contestDescription += thresholdLine.Replace(".5", "½") + " ";

        if (res.ThresholdUnits != null)
          contestDescription += res.ThresholdUnits;
      }
      contestDescription += " Final odds: ";

      switch (res.PriceType) {
        case LineOffering.PRICE_AMERICAN:
        case "X":
          if (res.FinalToBase != null && res.FinalToBase > 0)
            contestDescription += res.FinalMoney + " to " + res.FinalToBase;
          else {
            if (res.FinalMoney > 0)
              contestDescription += "+";
            contestDescription += res.FinalMoney.ToString();
          }
          break;
        case LineOffering.PRICE_DECIMAL:
          var finalDecimal = double.Parse(res.FinalMoney.ToString());
          contestDescription += finalDecimal;

          if (finalDecimal == Math.Floor(finalDecimal))
            contestDescription += ".0";
          break;
        case LineOffering.PRICE_FRACTIONAL:
          contestDescription += res.FinalMoney + "/" + res.FinalToBase;
          break;
      }
      return contestDescription;
    }

    #endregion

    #region Events

    private void CustomerWagerDetails_Load(object sender, EventArgs e) {
      LoadCustomerWagerDetails();
    }

    private void btnGameScores_Click(object sender, EventArgs e) {
      if (dgvwWagerOutcome.CurrentCell.RowIndex <= -1) return;
      var rIdx = dgvwWagerOutcome.CurrentCell.RowIndex;
      var gameNumber = int.Parse(dgvwWagerOutcome.Rows[rIdx].Cells[0].Value.ToString());
      var gameDateTime = DateTime.Parse(dgvwWagerOutcome.Rows[rIdx].Cells[2].Value.ToString());
      var team1Id = dgvwWagerOutcome.Rows[rIdx].Cells[5].Value.ToString();
      var team2Id = dgvwWagerOutcome.Rows[rIdx].Cells[6].Value.ToString();

      var gameScoresFrm = new FrmGameScores(gameNumber, gameDateTime, team1Id, team2Id, AppModuleInfo);
      try {
        gameScoresFrm.Icon = Icon;
        gameScoresFrm.ShowDialog();
      }
      catch (Exception ex) {
        Log(ex);
      }
    }

    private void btnIfWinOnly_Click(object sender, EventArgs e) {
      AddRollingIfBetTicketInfo(((Button)sender).Text);

      if (_parentFrm.WindowState == FormWindowState.Normal)
        _parentFrm.Close();
      Close();
    }

    private void btnIfWinOrPush_Click(object sender, EventArgs e) {
      AddRollingIfBetTicketInfo(((Button)sender).Text);

      if (_parentFrm.WindowState == FormWindowState.Normal)
        _parentFrm.Close();
      Close();
    }

    private void btnOpenPlay_Click(object sender, EventArgs e) {
      if (_wagerDetails[0].WagerType.ToUpper().Trim() == "TEASER") {
        // Check if customer available teaser is no longer offered to the customer DMN 20130831
        // asumming there is no overriding, but this need to be researched!
        var customer = (spCstGetCustomer_Result)_rootForm.GetType().GetProperty("Customer").GetValue(_rootForm, null);

        using (var cust = new Customers(AppModuleInfo)) {
          var teaserSpecsList = ((List<spCstGetCustomerTeaserSpecs_Result>)Is.NullValue(cust.GetCustomerTeaserSpecs(customer.CustomerID.Trim()),
                                                                                        new List<spCstGetCustomerTeaserSpecs_Result>())).FirstOrDefault(ts => ts.TeaserName.ToUpper().Trim() == _wagerDetails[0].TeaserName.ToString(CultureInfo.InvariantCulture).ToUpper().Trim());
          if (teaserSpecsList == null) {
            MessageBox.Show(@"This teaser is no longer offered to the customer", @"Teaser availability");
            return;
          }
        }
      }
      var closeForms = LoadOpenPlayWagerForm();

      if (closeForms) {
        if (_parentFrm.WindowState == FormWindowState.Normal)
          _parentFrm.Close();
        Close();
      }
      else
        MessageBox.Show(@"The open item is already on the readback screen.  It cannot be opened again.", @"Item on readback screen");
    }

    private void btnPlaybackConversation_Click(object sender, EventArgs e) {
      var playTicketAudio = new TicketDr(AppModuleInfo, _currentTicketNumber) {
        TicketPostedDateTime = _currentTicketPostedDateTime
      };
      playTicketAudio.PrepareFileforPlayback();
    }

    private void btnRollingIf_Click(object sender, EventArgs e) {
      var rollingIfBetFrm = new FrmRollingIfBetHookup(_currentTicketNumber, _wagerDetails, dgvwWagerOutcome, AppModuleInfo) {
        Icon = Icon
      };
      rollingIfBetFrm.ShowDialog();
    }

    private void dgvwWagerOutcome_KeyDown(object sender, KeyEventArgs e) {
      HandleKeyDown(sender, e);
    }

    private void dgvwWagerOutcome_SelectionChanged(object sender, EventArgs e) {
      if (dgvwWagerOutcome.SelectedRows.Count != 1) return;
      var iDx = dgvwWagerOutcome.CurrentCell.RowIndex;
      var gameContestNum = int.Parse(dgvwWagerOutcome.Rows[iDx].Cells[0].Value.ToString());
      var wagerOutcomeStatus = dgvwWagerOutcome.Rows[iDx].Cells[1].Value.ToString();
      var itemWagerType = "";
      if (dgvwWagerOutcome.Rows[iDx].Cells[8].Value != null)
        itemWagerType = dgvwWagerOutcome.Rows[iDx].Cells[8].Value.ToString();

      if (itemWagerType == WagerType.MANUALPLAY)
        return;

      if (wagerOutcomeStatus == "Pending" || wagerOutcomeStatus == "No Bet" || itemWagerType == WagerType.CONTEST)
        btnGameScores.Enabled = false;
      else
        btnGameScores.Enabled = true;

      if (itemWagerType == WagerType.CONTEST)
        txtGameContestOutcome.Text = GetContestOutcome(gameContestNum);
      else {
        var wagerPeriodDescription = "";

        if (dgvwWagerOutcome.Rows[iDx].Cells[4].Value != null)
          wagerPeriodDescription = dgvwWagerOutcome.Rows[iDx].Cells[4].Value.ToString();

        var wagerPeriodNumber = 0;

        if (dgvwWagerOutcome.Rows[iDx].Cells[9].Value != null)
          int.TryParse(dgvwWagerOutcome.Rows[iDx].Cells[9].Value.ToString(), out wagerPeriodNumber);

        txtGameContestOutcome.Text = "";
        using (var gAl = new GamesAndLines(AppModuleInfo)) {
          txtGameContestOutcome.Text = gAl.GetGameOutcome(gAl, gameContestNum, wagerPeriodDescription, wagerPeriodNumber, itemWagerType);
        }
      }
    }

    private void frmCustomerWagerDetails_FormClosing(object sender, FormClosingEventArgs e) {
      Tag = "Reviewed Wager " + _currentWagerType + " " + _currentTicketNumber.ToString()
      + "." + _currentWagerNumber.ToString();
      if (_rifProcessFinished || !RollingIfBetMode) return;
      if (_parentFrm.WindowState == FormWindowState.Normal)
        _parentFrm.Close();

      var parentRollingIfChb = (CheckBox)_rootForm.Controls.Find("chbRollingIF", true).FirstOrDefault();

      if (parentRollingIfChb != null)
        parentRollingIfChb.Checked = false;
    }

    private void frmCustomerWagerDetails_KeyPress(object sender, KeyPressEventArgs e) {
      var pressedKey = e.KeyChar.ToString(CultureInfo.InvariantCulture);

      if ((pressedKey == "\r" || pressedKey.ToLower() == "e"))
        Close();
    }

    private void btnExit_Click(object sender, EventArgs e) {
      Close();
    }

    #endregion

    #region Protected Methods

    protected override bool ProcessCmdKey(ref Message msg, Keys keyData) {
      var baseResult = base.ProcessCmdKey(ref msg, keyData);

      if (keyData != Keys.Escape) return baseResult;
      Close();
      return true;
    }

    #endregion

  }
}