﻿namespace GUILibraries.Forms {
  partial class FrmTicketInformation {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
      this.grpTicketHistory = new System.Windows.Forms.GroupBox();
      this.panSearchTicket = new System.Windows.Forms.Panel();
      this.lblTicketNumber = new System.Windows.Forms.Label();
      this.ntxtTicketNumber = new GUILibraries.Controls.NumberTextBox();
      this.btnTicketComments = new System.Windows.Forms.Button();
      this.btnPlaybackConversation = new System.Windows.Forms.Button();
      this.panTicketsDetails = new System.Windows.Forms.Panel();
      this.dgrvwTicketsDetails = new System.Windows.Forms.DataGridView();
      this.btnClose = new System.Windows.Forms.Button();
      this.txtDailyFiguresCurrencyLegend = new System.Windows.Forms.TextBox();
      this.cbxTicketsDateRange = new System.Windows.Forms.ComboBox();
      this.lblListedTickets = new System.Windows.Forms.Label();
      this.grpWagers = new System.Windows.Forms.GroupBox();
      this.btnWagerDetails = new System.Windows.Forms.Button();
      this.btnDeleteWager = new System.Windows.Forms.Button();
      this.panWagersDetails = new System.Windows.Forms.Panel();
      this.dgrvwWagersDetails = new System.Windows.Forms.DataGridView();
      this.btnWagerComments = new System.Windows.Forms.Button();
      this.btnViewInetLog = new System.Windows.Forms.Button();
      this.grpTicketHistory.SuspendLayout();
      this.panSearchTicket.SuspendLayout();
      this.panTicketsDetails.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgrvwTicketsDetails)).BeginInit();
      this.grpWagers.SuspendLayout();
      this.panWagersDetails.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dgrvwWagersDetails)).BeginInit();
      this.SuspendLayout();
      // 
      // grpTicketHistory
      // 
      this.grpTicketHistory.Controls.Add(this.btnViewInetLog);
      this.grpTicketHistory.Controls.Add(this.panSearchTicket);
      this.grpTicketHistory.Controls.Add(this.btnTicketComments);
      this.grpTicketHistory.Controls.Add(this.btnPlaybackConversation);
      this.grpTicketHistory.Controls.Add(this.panTicketsDetails);
      this.grpTicketHistory.Controls.Add(this.btnClose);
      this.grpTicketHistory.Controls.Add(this.txtDailyFiguresCurrencyLegend);
      this.grpTicketHistory.Controls.Add(this.cbxTicketsDateRange);
      this.grpTicketHistory.Controls.Add(this.lblListedTickets);
      this.grpTicketHistory.Location = new System.Drawing.Point(13, 13);
      this.grpTicketHistory.Name = "grpTicketHistory";
      this.grpTicketHistory.Size = new System.Drawing.Size(924, 246);
      this.grpTicketHistory.TabIndex = 0;
      this.grpTicketHistory.TabStop = false;
      this.grpTicketHistory.Text = "Ticket History";
      // 
      // panSearchTicket
      // 
      this.panSearchTicket.Controls.Add(this.lblTicketNumber);
      this.panSearchTicket.Controls.Add(this.ntxtTicketNumber);
      this.panSearchTicket.Location = new System.Drawing.Point(714, 19);
      this.panSearchTicket.Name = "panSearchTicket";
      this.panSearchTicket.Size = new System.Drawing.Size(200, 27);
      this.panSearchTicket.TabIndex = 63;
      // 
      // lblTicketNumber
      // 
      this.lblTicketNumber.AutoSize = true;
      this.lblTicketNumber.Location = new System.Drawing.Point(3, 6);
      this.lblTicketNumber.Name = "lblTicketNumber";
      this.lblTicketNumber.Size = new System.Drawing.Size(40, 13);
      this.lblTicketNumber.TabIndex = 62;
      this.lblTicketNumber.Text = "Ticket:";
      // 
      // ntxtTicketNumber
      // 
      this.ntxtTicketNumber.AllowCents = false;
      this.ntxtTicketNumber.AllowNegatives = false;
      this.ntxtTicketNumber.DividedBy = 1;
      this.ntxtTicketNumber.DivideFlag = false;
      this.ntxtTicketNumber.Location = new System.Drawing.Point(44, 3);
      this.ntxtTicketNumber.Name = "ntxtTicketNumber";
      this.ntxtTicketNumber.ShowIfZero = true;
      this.ntxtTicketNumber.Size = new System.Drawing.Size(153, 20);
      this.ntxtTicketNumber.TabIndex = 61;
      this.ntxtTicketNumber.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ntxtTicketNumber_KeyUp);
      // 
      // btnTicketComments
      // 
      this.btnTicketComments.Location = new System.Drawing.Point(401, 210);
      this.btnTicketComments.Name = "btnTicketComments";
      this.btnTicketComments.Size = new System.Drawing.Size(127, 23);
      this.btnTicketComments.TabIndex = 60;
      this.btnTicketComments.Text = "Ticket Comments";
      this.btnTicketComments.UseVisualStyleBackColor = true;
      this.btnTicketComments.Click += new System.EventHandler(this.btnTicketComments_Click);
      // 
      // btnPlaybackConversation
      // 
      this.btnPlaybackConversation.Location = new System.Drawing.Point(268, 210);
      this.btnPlaybackConversation.Name = "btnPlaybackConversation";
      this.btnPlaybackConversation.Size = new System.Drawing.Size(127, 23);
      this.btnPlaybackConversation.TabIndex = 50;
      this.btnPlaybackConversation.Text = "Playback Conversation";
      this.btnPlaybackConversation.UseVisualStyleBackColor = true;
      this.btnPlaybackConversation.Click += new System.EventHandler(this.btnPlaybackConversation_Click);
      // 
      // panTicketsDetails
      // 
      this.panTicketsDetails.Controls.Add(this.dgrvwTicketsDetails);
      this.panTicketsDetails.Location = new System.Drawing.Point(17, 56);
      this.panTicketsDetails.Name = "panTicketsDetails";
      this.panTicketsDetails.Size = new System.Drawing.Size(901, 145);
      this.panTicketsDetails.TabIndex = 30;
      // 
      // dgrvwTicketsDetails
      // 
      this.dgrvwTicketsDetails.AllowUserToAddRows = false;
      this.dgrvwTicketsDetails.AllowUserToDeleteRows = false;
      this.dgrvwTicketsDetails.AllowUserToOrderColumns = true;
      this.dgrvwTicketsDetails.AllowUserToResizeColumns = false;
      this.dgrvwTicketsDetails.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgrvwTicketsDetails.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
      this.dgrvwTicketsDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgrvwTicketsDetails.Location = new System.Drawing.Point(4, 4);
      this.dgrvwTicketsDetails.MultiSelect = false;
      this.dgrvwTicketsDetails.Name = "dgrvwTicketsDetails";
      this.dgrvwTicketsDetails.ReadOnly = true;
      this.dgrvwTicketsDetails.RowHeadersVisible = false;
      this.dgrvwTicketsDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgrvwTicketsDetails.Size = new System.Drawing.Size(894, 138);
      this.dgrvwTicketsDetails.StandardTab = true;
      this.dgrvwTicketsDetails.TabIndex = 1;
      this.dgrvwTicketsDetails.SelectionChanged += new System.EventHandler(this.dgrvwTicketsDetails_SelectionChanged);
      // 
      // btnClose
      // 
      this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnClose.Location = new System.Drawing.Point(612, 21);
      this.btnClose.Name = "btnClose";
      this.btnClose.Size = new System.Drawing.Size(75, 23);
      this.btnClose.TabIndex = 20;
      this.btnClose.Text = "Close";
      this.btnClose.UseVisualStyleBackColor = true;
      this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
      // 
      // txtDailyFiguresCurrencyLegend
      // 
      this.txtDailyFiguresCurrencyLegend.Enabled = false;
      this.txtDailyFiguresCurrencyLegend.Location = new System.Drawing.Point(349, 21);
      this.txtDailyFiguresCurrencyLegend.Name = "txtDailyFiguresCurrencyLegend";
      this.txtDailyFiguresCurrencyLegend.ReadOnly = true;
      this.txtDailyFiguresCurrencyLegend.Size = new System.Drawing.Size(228, 20);
      this.txtDailyFiguresCurrencyLegend.TabIndex = 7;
      this.txtDailyFiguresCurrencyLegend.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // cbxTicketsDateRange
      // 
      this.cbxTicketsDateRange.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxTicketsDateRange.FormattingEnabled = true;
      this.cbxTicketsDateRange.Location = new System.Drawing.Point(155, 20);
      this.cbxTicketsDateRange.Name = "cbxTicketsDateRange";
      this.cbxTicketsDateRange.Size = new System.Drawing.Size(171, 21);
      this.cbxTicketsDateRange.TabIndex = 10;
      this.cbxTicketsDateRange.SelectedValueChanged += new System.EventHandler(this.cbxTicketsDateRange_SelectedValueChanged);
      this.cbxTicketsDateRange.Click += new System.EventHandler(this.cbxTicketsDateRange_Click);
      // 
      // lblListedTickets
      // 
      this.lblListedTickets.AutoSize = true;
      this.lblListedTickets.Location = new System.Drawing.Point(37, 24);
      this.lblListedTickets.Name = "lblListedTickets";
      this.lblListedTickets.Size = new System.Drawing.Size(112, 13);
      this.lblListedTickets.TabIndex = 0;
      this.lblListedTickets.Text = "Listed Tickets (F5-F6):";
      // 
      // grpWagers
      // 
      this.grpWagers.Controls.Add(this.btnWagerDetails);
      this.grpWagers.Controls.Add(this.btnDeleteWager);
      this.grpWagers.Controls.Add(this.panWagersDetails);
      this.grpWagers.Controls.Add(this.btnWagerComments);
      this.grpWagers.Location = new System.Drawing.Point(13, 271);
      this.grpWagers.Name = "grpWagers";
      this.grpWagers.Size = new System.Drawing.Size(924, 246);
      this.grpWagers.TabIndex = 1;
      this.grpWagers.TabStop = false;
      this.grpWagers.Text = "Wagers";
      // 
      // btnWagerDetails
      // 
      this.btnWagerDetails.Location = new System.Drawing.Point(306, 213);
      this.btnWagerDetails.Name = "btnWagerDetails";
      this.btnWagerDetails.Size = new System.Drawing.Size(89, 23);
      this.btnWagerDetails.TabIndex = 90;
      this.btnWagerDetails.Text = "Wager Details";
      this.btnWagerDetails.UseVisualStyleBackColor = true;
      this.btnWagerDetails.Click += new System.EventHandler(this.btnWagerDetails_Click);
      // 
      // btnDeleteWager
      // 
      this.btnDeleteWager.Location = new System.Drawing.Point(422, 213);
      this.btnDeleteWager.Name = "btnDeleteWager";
      this.btnDeleteWager.Size = new System.Drawing.Size(103, 23);
      this.btnDeleteWager.TabIndex = 100;
      this.btnDeleteWager.Text = "Delete Wager";
      this.btnDeleteWager.UseVisualStyleBackColor = true;
      this.btnDeleteWager.Click += new System.EventHandler(this.btnDeleteWager_Click);
      // 
      // panWagersDetails
      // 
      this.panWagersDetails.Controls.Add(this.dgrvwWagersDetails);
      this.panWagersDetails.Location = new System.Drawing.Point(17, 20);
      this.panWagersDetails.Name = "panWagersDetails";
      this.panWagersDetails.Size = new System.Drawing.Size(898, 187);
      this.panWagersDetails.TabIndex = 70;
      // 
      // dgrvwWagersDetails
      // 
      this.dgrvwWagersDetails.AllowUserToAddRows = false;
      this.dgrvwWagersDetails.AllowUserToDeleteRows = false;
      this.dgrvwWagersDetails.AllowUserToOrderColumns = true;
      this.dgrvwWagersDetails.AllowUserToResizeColumns = false;
      this.dgrvwWagersDetails.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgrvwWagersDetails.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
      this.dgrvwWagersDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgrvwWagersDetails.Location = new System.Drawing.Point(3, 3);
      this.dgrvwWagersDetails.MultiSelect = false;
      this.dgrvwWagersDetails.Name = "dgrvwWagersDetails";
      this.dgrvwWagersDetails.ReadOnly = true;
      this.dgrvwWagersDetails.RowHeadersVisible = false;
      this.dgrvwWagersDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgrvwWagersDetails.Size = new System.Drawing.Size(894, 181);
      this.dgrvwWagersDetails.StandardTab = true;
      this.dgrvwWagersDetails.TabIndex = 80;
      this.dgrvwWagersDetails.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgrvwWagersDetails_CellDoubleClick);
      this.dgrvwWagersDetails.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgrvwWagersDetails_RowEnter);
      this.dgrvwWagersDetails.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgrvwWagersDetails_KeyDown);
      // 
      // btnWagerComments
      // 
      this.btnWagerComments.Location = new System.Drawing.Point(547, 213);
      this.btnWagerComments.Name = "btnWagerComments";
      this.btnWagerComments.Size = new System.Drawing.Size(103, 23);
      this.btnWagerComments.TabIndex = 110;
      this.btnWagerComments.Text = "Wager Comments";
      this.btnWagerComments.UseVisualStyleBackColor = true;
      this.btnWagerComments.Click += new System.EventHandler(this.btnWagerComments_Click);
      // 
      // btnViewInetLog
      // 
      this.btnViewInetLog.Location = new System.Drawing.Point(534, 210);
      this.btnViewInetLog.Name = "btnViewInetLog";
      this.btnViewInetLog.Size = new System.Drawing.Size(127, 23);
      this.btnViewInetLog.TabIndex = 64;
      this.btnViewInetLog.Text = "View Inet Log";
      this.btnViewInetLog.UseVisualStyleBackColor = true;
      this.btnViewInetLog.Click += new System.EventHandler(this.btnViewInetLog_Click);
      // 
      // FrmTicketInformation
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnClose;
      this.ClientSize = new System.Drawing.Size(949, 531);
      this.Controls.Add(this.grpWagers);
      this.Controls.Add(this.grpTicketHistory);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.KeyPreview = true;
      this.MaximizeBox = false;
      this.Name = "FrmTicketInformation";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Ticket Information";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmTicketInformation_FormClosing);
      this.Load += new System.EventHandler(this.frmTicketInformation_Load);
      this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmTicketInformation_KeyDown);
      this.grpTicketHistory.ResumeLayout(false);
      this.grpTicketHistory.PerformLayout();
      this.panSearchTicket.ResumeLayout(false);
      this.panSearchTicket.PerformLayout();
      this.panTicketsDetails.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgrvwTicketsDetails)).EndInit();
      this.grpWagers.ResumeLayout(false);
      this.panWagersDetails.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.dgrvwWagersDetails)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox grpTicketHistory;
    private System.Windows.Forms.ComboBox cbxTicketsDateRange;
    private System.Windows.Forms.Label lblListedTickets;
    private System.Windows.Forms.GroupBox grpWagers;
    private System.Windows.Forms.Button btnClose;
    private System.Windows.Forms.TextBox txtDailyFiguresCurrencyLegend;
    private System.Windows.Forms.Panel panTicketsDetails;
    private System.Windows.Forms.Button btnPlaybackConversation;
    private System.Windows.Forms.Button btnTicketComments;
    private System.Windows.Forms.DataGridView dgrvwTicketsDetails;
    private System.Windows.Forms.Panel panWagersDetails;
    private System.Windows.Forms.Button btnWagerComments;
    private System.Windows.Forms.Button btnDeleteWager;
    private System.Windows.Forms.Button btnWagerDetails;
    private System.Windows.Forms.DataGridView dgrvwWagersDetails;
    private Controls.NumberTextBox ntxtTicketNumber;
    private System.Windows.Forms.Label lblTicketNumber;
    private System.Windows.Forms.Panel panSearchTicket;
    private System.Windows.Forms.Button btnViewInetLog;
  }
}
