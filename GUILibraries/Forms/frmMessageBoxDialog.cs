﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using InstanceManager.BusinessLayer;

namespace GUILibraries.Forms {
  public partial class frmMessageBoxDialog : SIDForm {
    public String MessageBody { get; private set; }
    public Boolean OkClicked { get; private set; }
    public int WagersCount { private get; set; }

    public frmMessageBoxDialog(ModuleInfo moduleInfo)
      : base(moduleInfo) {
      InitializeComponent();
    }

    private void btnOk_Click(object sender, EventArgs e) {
      OkClicked = true;
      Close();
    }

    private void frmMessageBoxDialog_Load(object sender, EventArgs e) {
      string txt0 = WagersCount == 0 ? "Delete Checked Wager(s)" : (WagersCount == 1 ? "Delete Checked Wager" : "Delete Checked Wagers");
      lblWarningMessage.Text = @"Are you sure you want to " + txt0 + @"." + Environment.NewLine +
      @"If yes, affected customers will be notified as well." + Environment.NewLine +
      @"Write the message customers will see.";

      Icon image = image = File.Exists(@"Resources\clear.ico") ? new Icon(@"Resources\clear.ico") : null;
    }

    private void frmMessageBoxDialog_FormClosing(object sender, FormClosingEventArgs e) {
      if (OkClicked && (txtMessageBody.Text.Trim().Length == 0 || String.IsNullOrEmpty(txtMessageBody.Text.Trim()))) {
        MessageBox.Show(@"Write the message customers will see.", @"Incomplete", MessageBoxButtons.OK);
        e.Cancel = true;
        OkClicked = false;
      }
      MessageBody = txtMessageBody.Text;
    }

    private void btnCancel_Click(object sender, EventArgs e) {
      OkClicked = false;
    }
  }
}
