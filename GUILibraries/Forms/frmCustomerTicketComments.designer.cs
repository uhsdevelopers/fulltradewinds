﻿namespace GUILibraries.Forms
{
    partial class FrmCustomerTicketComments
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbnTicketCommentsCancel = new System.Windows.Forms.Button();
            this.tbnTicketCommentsOk = new System.Windows.Forms.Button();
            this.grpTicketComments = new System.Windows.Forms.GroupBox();
            this.txtTicketComments = new System.Windows.Forms.TextBox();
            this.chbTicketDiscrepancy = new System.Windows.Forms.CheckBox();
            this.grpTicketComments.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbnTicketCommentsCancel
            // 
            this.tbnTicketCommentsCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.tbnTicketCommentsCancel.Location = new System.Drawing.Point(255, 269);
            this.tbnTicketCommentsCancel.Name = "tbnTicketCommentsCancel";
            this.tbnTicketCommentsCancel.Size = new System.Drawing.Size(75, 23);
            this.tbnTicketCommentsCancel.TabIndex = 50;
            this.tbnTicketCommentsCancel.Text = "Cancel";
            this.tbnTicketCommentsCancel.UseVisualStyleBackColor = true;
            this.tbnTicketCommentsCancel.Click += new System.EventHandler(this.tbnTicketCommentsCancel_Click);
            // 
            // tbnTicketCommentsOk
            // 
            this.tbnTicketCommentsOk.Location = new System.Drawing.Point(157, 269);
            this.tbnTicketCommentsOk.Name = "tbnTicketCommentsOk";
            this.tbnTicketCommentsOk.Size = new System.Drawing.Size(75, 23);
            this.tbnTicketCommentsOk.TabIndex = 40;
            this.tbnTicketCommentsOk.Text = "Ok";
            this.tbnTicketCommentsOk.UseVisualStyleBackColor = true;
            this.tbnTicketCommentsOk.Click += new System.EventHandler(this.tbnTicketCommentsOk_Click);
            // 
            // grpTicketComments
            // 
            this.grpTicketComments.Controls.Add(this.txtTicketComments);
            this.grpTicketComments.Location = new System.Drawing.Point(12, 47);
            this.grpTicketComments.Name = "grpTicketComments";
            this.grpTicketComments.Size = new System.Drawing.Size(469, 194);
            this.grpTicketComments.TabIndex = 20;
            this.grpTicketComments.TabStop = false;
            this.grpTicketComments.Text = "Comments";
            // 
            // txtTicketComments
            // 
            this.txtTicketComments.Location = new System.Drawing.Point(7, 20);
            this.txtTicketComments.Multiline = true;
            this.txtTicketComments.Name = "txtTicketComments";
            this.txtTicketComments.Size = new System.Drawing.Size(456, 168);
            this.txtTicketComments.TabIndex = 30;
            // 
            // chbTicketDiscrepancy
            // 
            this.chbTicketDiscrepancy.AutoSize = true;
            this.chbTicketDiscrepancy.Location = new System.Drawing.Point(12, 12);
            this.chbTicketDiscrepancy.Name = "chbTicketDiscrepancy";
            this.chbTicketDiscrepancy.Size = new System.Drawing.Size(85, 17);
            this.chbTicketDiscrepancy.TabIndex = 10;
            this.chbTicketDiscrepancy.Text = "Discrepancy";
            this.chbTicketDiscrepancy.UseVisualStyleBackColor = true;
            // 
            // frmCustomerTicketComments
            // 
            this.AcceptButton = this.tbnTicketCommentsOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.tbnTicketCommentsCancel;
            this.ClientSize = new System.Drawing.Size(499, 310);
            this.Controls.Add(this.tbnTicketCommentsCancel);
            this.Controls.Add(this.tbnTicketCommentsOk);
            this.Controls.Add(this.grpTicketComments);
            this.Controls.Add(this.chbTicketDiscrepancy);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FrmCustomerTicketComments";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Customer Ticket Comments";
            this.Load += new System.EventHandler(this.frmCustomerTicketComments_Load);
            this.grpTicketComments.ResumeLayout(false);
            this.grpTicketComments.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button tbnTicketCommentsCancel;
        private System.Windows.Forms.Button tbnTicketCommentsOk;
        private System.Windows.Forms.GroupBox grpTicketComments;
        private System.Windows.Forms.TextBox txtTicketComments;
        private System.Windows.Forms.CheckBox chbTicketDiscrepancy;
    }
}