﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using InstanceManager.Entities;
using System.Text;

namespace GUILibraries.Forms {
  public partial class FrmSystemInMaintenance : SIDForm {
    private class ComboBoxItem {
      public string Text { private get; set; }
      public object Value { get; set; }

      public override string ToString() {
        return Text;
      }
    }

    #region Private Vars

    int _messageFrequency = 1;
    int _messageMaxCount = 3;
    String _messageText = "";
    List<spIMGetSystems_Result> _systems;

    #endregion

    #region Constructors

    public FrmSystemInMaintenance() {
      InitializeComponent();
    }

    #endregion

    #region Private Methods

    private void HandleAppsUnlocking() {
      using (var im = new InstanceManager.BusinessLayer.InstanceManager()) {
        if (cmbTargetSystem.SelectedItem != null) {
          int systemId;
          var selectedItem = (ComboBoxItem)cmbTargetSystem.SelectedItem;
          int.TryParse(selectedItem.Value.ToString(), out systemId);
          im.UnlockSystemFromMaintenance(systemId);
          btnOk.Text = "Broadcast to Apps";
          ToggleControlsUsability(true);
        }
        else {
          MessageBox.Show("Please select a target System to Unlock");
          cmbTargetSystem.Focus();
        }
      }
    }

    private void HandleAppsLocking() {
      using (var sm = new Utilities.SocketManager(Utilities.SocketManager.Mode.Sender, null)) {
        if (cmbTargetSystem.SelectedItem == null) {
          MessageBox.Show("Please select a target System");
          cmbTargetSystem.Focus();
          return;
        }
        sm.MessagePriority = GetMessagePriority();

        if (txtFrequency.Text.Trim() != "")
          int.TryParse(txtFrequency.Text, out _messageFrequency);
        sm.MessageShownFrequency = _messageFrequency;

        if (txtMaxCount.Text.Trim() != "")
          int.TryParse(txtMaxCount.Text, out _messageMaxCount);
        sm.MessageShownMaxCount = _messageMaxCount;

        if (txtMessage.Text.Trim() != "") {
          sm.MessageText = _messageText = txtMessage.Text;

          using (var im = new InstanceManager.BusinessLayer.InstanceManager()) {
            if (cmbTargetSystem.SelectedItem != null) {
              int systemId;
              var selectedItem = (ComboBoxItem)cmbTargetSystem.SelectedItem;
              int.TryParse(selectedItem.Value.ToString(), out systemId);
              var applications = im.GetModulesList(systemId);

              foreach (var app in applications.Where(app => app.Enabled))
                sm.SendMaintenanceMessage(Utilities.SocketManager.BASEMAINTPORT + app.ApplicationId);
            }
          }
          LockApplicationsFromStartup();
          btnOk.Text = "Broadcast sent";
          ToggleControlsUsability(false);
        }
        else {
          MessageBox.Show("Please type in any text for the message");
          txtMessage.Focus();
        }
      }
    }

    private void HandleSystemIndexChange(object sender) {
      var combo = (ComboBox)sender;

      if (combo == null || combo.SelectedItem == null)
        return;
      var selectedItem = (ComboBoxItem)combo.SelectedItem;
      int systemId;
      int.TryParse(selectedItem.Value.ToString(), out systemId);

      var isInMaintenance = (from s in _systems
                             where s.SystemId == systemId
                             select s.FlagForMaintenance).FirstOrDefault();
      if (isInMaintenance) {
        ToggleControlsUsability(false);
        combo.Enabled = true;
      }
      else
        ToggleControlsUsability(true);
    }

    private void LoadFormInfo() {
      LoadSystemsList();
    }

    private void LoadSystemsList() {
      using (var im = new InstanceManager.BusinessLayer.InstanceManager()) {
        _systems = im.GetSystemsList();

        foreach (var it in _systems.Select(item => new ComboBoxItem { Text = item.Name.Trim(), Value = item.SystemId }))
          cmbTargetSystem.Items.Add(it);
      }
    }

    private void LockApplicationsFromStartup() {
      using (var im = new InstanceManager.BusinessLayer.InstanceManager()) {
        if (cmbTargetSystem.SelectedItem == null)
          return;
        int systemId;
        var selectedItem = (ComboBoxItem)cmbTargetSystem.SelectedItem;
        int.TryParse(selectedItem.Value.ToString(), out systemId);
        var data = Encoding.ASCII.GetBytes("{\"Header\":\"KillApp\", \"DisplayMessageFreqMin\":\"" + _messageFrequency
            + "\",\"ShowMessageMaxCount\":\"" + _messageMaxCount
            + "\",\"MaintenanceMesssage\":\"" + _messageText
            + "\" , \"MessagePriority\":\""
            + GetMessagePriority() + "\"}");
        im.LockSystemForMaintenance(systemId, data);
      }
    }

    private int GetMessagePriority() {
      if (radHigh.Checked) {
        _messageFrequency = 1;
        _messageMaxCount = 3;
        return 1;
      }
      if (radMedium.Checked) {
        _messageFrequency = 2;
        _messageMaxCount = 5;
        return 2;
      }
      if (!radLow.Checked)
        return 1;
      _messageFrequency = 5;
      _messageMaxCount = 10;
      return 3;
    }

    private void ToggleControlsUsability(bool setTo) {
      btnOk.Enabled = setTo;
      txtMessage.Enabled = setTo;
      txtFrequency.Enabled = setTo;
      txtMaxCount.Enabled = setTo;
      grpPriority.Enabled = setTo;
      cmbTargetSystem.Enabled = setTo;
      btnUnlockApps.Enabled = !setTo;

    }

    #endregion

    #region Events

    private void frmSystemInMaintenance_Load(object sender, EventArgs e) {
      LoadFormInfo();
    }

    private void btnOk_Click(object sender, EventArgs e) {
      HandleAppsLocking();
    }

    private void btnUnlockApps_Click(object sender, EventArgs e) {
      HandleAppsUnlocking();
    }

    private void cmbTargetSystem_SelectedIndexChanged(object sender, EventArgs e) {
      HandleSystemIndexChange(sender);
    }

    private void radHigh_CheckedChanged(object sender, EventArgs e) {
      var rad = (RadioButton)sender;

      if (!rad.Checked)
        return;
      txtFrequency.Text = "1";
      txtMaxCount.Text = "3";
    }

    private void radMedium_CheckedChanged(object sender, EventArgs e) {
      var rad = (RadioButton)sender;

      if (!rad.Checked)
        return;
      txtFrequency.Text = "2";
      txtMaxCount.Text = "5";
    }

    private void radLow_CheckedChanged(object sender, EventArgs e) {
      var rad = (RadioButton)sender;

      if (!rad.Checked)
        return;
      txtFrequency.Text = "5";
      txtMaxCount.Text = "10";
    }

    #endregion
  }
}