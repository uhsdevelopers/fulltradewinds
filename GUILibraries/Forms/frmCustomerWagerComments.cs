﻿using System;
using System.Linq;
using System.Windows.Forms;
using InstanceManager.BusinessLayer;
using SIDLibraries.Entities;
using SIDLibraries.BusinessLayer;

namespace GUILibraries.Forms {
  public partial class FrmCustomerWagerComments : SIDForm {

    private readonly int _currentTicketNumber;
    private readonly int _currentWagerNumber;

    private String _startingWagerDiscrepancy = "";
    private String _startingWagerComments = "";

    public FrmCustomerWagerComments(int ticketNumber, int wagerNumber, ModuleInfo moduleInfo) : base (moduleInfo) {
      if (ticketNumber > 0 && wagerNumber > 0) {
        _currentTicketNumber = ticketNumber;
        _currentWagerNumber = wagerNumber;
        InitializeComponent();
      }
      else {
        MessageBox.Show("No ticketnumber and or WagerNumber was selected");
        Close();
      }
    }

    private void frmCustomerWagerComments_Load(object sender, EventArgs e) {
      GetCustomerWagerComments(out _startingWagerDiscrepancy, out _startingWagerComments);
    }

    private void GetCustomerWagerComments(out String startingWagerDiscrepancy, out String startingWagerComments) {
      startingWagerDiscrepancy = "N";
      startingWagerComments = "";
      spTkWGetCustomerWagerComments_Result wagerComments;

      using (var tAw = new TicketsAndWagers(AppModuleInfo))
        wagerComments = tAw.GetCustomerWagerComments(_currentTicketNumber, _currentWagerNumber).FirstOrDefault();

      if (wagerComments == null) return;
      chbWagerDiscrepancy.Checked = wagerComments.DiscrepancyFlag == "Y";
      startingWagerDiscrepancy = wagerComments.DiscrepancyFlag;

      txtWagerComments.Text = wagerComments.Comments;
      startingWagerComments = wagerComments.Comments;
    }

    private void tbnWagerCommentsOk_Click(object sender, EventArgs e) {
      var finalWagerDiscrepancy = chbWagerDiscrepancy.Checked ? "Y" : "N";
      var finalWagerComments = txtWagerComments.Text.Trim();

      if (_startingWagerDiscrepancy != finalWagerDiscrepancy || _startingWagerComments.Trim() != finalWagerComments) {
        using (var tAw = new TicketsAndWagers(AppModuleInfo))
          tAw.UpdateWagerComments(_currentTicketNumber, _currentWagerNumber, finalWagerComments, finalWagerDiscrepancy);
      }
      Close();
    }

    private void tbnWagerCommentsCancel_Click(object sender, EventArgs e) {
      Close();
    }

  }
}
