﻿using System;
using InstanceManager.BusinessLayer;
using SIDLibraries.Entities;
using SIDLibraries.BusinessLayer;

namespace GUILibraries.Forms {
  public partial class FrmCustomerBalanceInformation : SIDForm {
    readonly String _customerAccountType;
    readonly int _customerPendingWagerCount;
    readonly double _customerPendingWagerBalance;
    readonly int _customerFreePlayPendingCount;
    readonly double _customerFreePlayPendingBalance;
    readonly String _customerCurrency;
    readonly double _customerCurrentBalance;
    readonly double _customerCreditLimit;
    readonly double _customerPendingCredit;
    readonly double _customerNonPostedCasinoBalance;
    readonly double _customerFreePlayBalance;

    public FrmCustomerBalanceInformation(String accountType, int pendingWagerCount, double pendingWagerBalance, int freePlayPendingCount, double freePlayPendingBalance,
        String currency, double currentBalance, double creditLimit, double pendingCredit, double nonPostedCasinoBalance, double freePlayBalance, ModuleInfo moduleInfo)
      : base(moduleInfo) {
      _customerAccountType = accountType;
      _customerPendingWagerCount = pendingWagerCount;
      _customerFreePlayPendingCount = freePlayPendingCount;
      _customerFreePlayPendingBalance = freePlayPendingBalance;
      _customerCurrency = currency;
      _customerCurrentBalance = currentBalance;
      _customerCreditLimit = creditLimit;
      _customerPendingWagerBalance = pendingWagerBalance;
      _customerPendingCredit = pendingCredit;
      _customerNonPostedCasinoBalance = nonPostedCasinoBalance;
      _customerFreePlayBalance = freePlayBalance;

      InitializeComponent();
    }

    private void btnClose_Click(object sender, EventArgs e) {
      Close();
    }

    private void frmCustomerBalanceInformation_Load(object sender, EventArgs e) {
      FillFormObjects();
    }

    private void FillFormObjects() {
      txtBalanceInfoHeader.Text = _customerAccountType.ToUpper();
      txtBalanceInfoHeader.Text += Environment.NewLine;
      txtBalanceInfoHeader.Text += Environment.NewLine;
      txtBalanceInfoHeader.Text += "Pending Count/Risk: " + _customerPendingWagerCount + " / " + (_customerPendingWagerBalance / 100).ToString("N0");
      txtBalanceInfoHeader.Text += Environment.NewLine;
      txtBalanceInfoHeader.Text += Environment.NewLine;
      txtBalanceInfoHeader.Text += "Free Play Pending Count/Risk: " + _customerFreePlayPendingCount + " / " + (_customerFreePlayPendingBalance / 100).ToString("N0");
      txtBalanceInfoHeader.Text += Environment.NewLine;
      txtBalanceInfoHeader.Text += Environment.NewLine;
      txtBalanceInfoHeader.Text += _customerCurrency;
      txtBalanceInfoHeader.Text += Environment.NewLine;
      txtBalanceInfoHeader.Text += Environment.NewLine;

      txtBalanceInfoHeader.Text += GetTodaysRate();

      lblCurrentBalanceFigure.Text = (_customerCurrentBalance / 100).ToString("N0");
      lblCreditLimitFigure.Text = (_customerCreditLimit / 100).ToString("N0");
      lblPendingCreditFigure.Text = (_customerPendingCredit / 100).ToString("N0");
      lblNonPostedCasinoBalanceFigure.Text = (_customerNonPostedCasinoBalance / 100).ToString("N0");

      if (_customerAccountType != "Postup") {
        lblPostedPendingBets.Visible = true;
        lblPostedPendingBetsFigure.Visible = true;
        lblPostedPendingBetsFigure.Text = (_customerPendingWagerBalance / -100).ToString("N0");
        lblAvailableBalanceFigure.Text = ((_customerCurrentBalance + _customerCreditLimit + _customerPendingCredit + _customerNonPostedCasinoBalance - _customerPendingWagerBalance) / 100).ToString("N0");
      }
      else {
        lblPostedPendingBets.Visible = false;
        lblPostedPendingBetsFigure.Visible = false;
        lblAvailableBalanceFigure.Text = ((_customerCurrentBalance + _customerCreditLimit + _customerPendingCredit + _customerNonPostedCasinoBalance) / 100).ToString("N0");
      }

      lblFreePlayBalanceFigure.Text = (_customerFreePlayBalance / 100).ToString("N0");
    }

    private string GetTodaysRate() {
      var todaysRate = "Today's Rate: ";
      var currencyCode = _customerCurrency.Split(' ');
      var effectiveDate = DateTime.Now;
      effectiveDate = new DateTime(effectiveDate.Year, effectiveDate.Month, effectiveDate.Day, 0, 0, 0);

      spCurGetCurrencyRateByDate_Result todaysRecord;
      using (var cur = new Currencies(AppModuleInfo))
        todaysRecord = cur.GetCurrencyRateByDate(currencyCode[0], effectiveDate);
      if (todaysRecord != null)
        todaysRate += todaysRecord.Rate;
      return todaysRate;
    }
  }
}