﻿using System;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;

namespace GUILibraries.Forms {
  public partial class FrmInetLogReport : SIDForm {

    private readonly string _sessionId;

    public FrmInetLogReport(ModuleInfo moduleInfo, string sessionId) : base(moduleInfo) {
      _sessionId = sessionId;
      InitializeComponent();
    }

    private void FrmInetLogReport_Load(object sender, EventArgs e) {
      GetInetSessionLog();
    }

    private void GetInetSessionLog() {
      using (var tkw =  new TicketsAndWagers(AppModuleInfo)) {
        dgvInetLogReport.DataSource = tkw.GetInetSessionLog(_sessionId);
      }
      dgvInetLogReport.Columns[0].Visible = false;
      dgvInetLogReport.Columns[8].Visible = false;
      dgvInetLogReport.Columns[9].Visible = false;

      dgvInetLogReport.Columns[2].Width = dgvInetLogReport.Columns[3].Width = 80;
    }
  }
}
