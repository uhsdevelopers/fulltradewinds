﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using System.Threading;
using SIDLibraries.Utilities;
using GUILibraries.Forms;
using System.Windows.Forms;

namespace GUILibraries {
  public static class SIDProgram {

    public static SocketManager GetSocketListener(InstanceManager.InstanceManager im) {
      return new SocketManager(SocketManager.Mode.Listener, im.InstanceModuleInfo);
    }

    public static InstanceManager.InstanceManager GetInstanceObject(int instanceId, int applicationId, bool setAppInUse) {
      return new InstanceManager.InstanceManager(applicationId, instanceId, setAppInUse);
    }

    public static void StartForm(FrmLogClient form, InstanceManager.InstanceManager im, string permissionRequired) { 
   
      if (im.InstanceModuleInfo.FlagForMaintenance){
        MessageBox.Show(im.InstanceModuleInfo.Description + " is in Scheduled Maintenance. Try again in a few minutes or Contact TSG for additional details.");
        return;
      }

      using (AppErrorHandler appEH = new AppErrorHandler(im.InstanceModuleInfo.Id, im)) {
        Application.ThreadException += new ThreadExceptionEventHandler(appEH.UnhandledThreadExceptionHandler);

        using (Mutex mutex = new Mutex(false, "Global\\" + im.InstanceModuleInfo.ExeName)) {
          if (!mutex.WaitOne(0, false)) {
            MessageBox.Show("Only one instance of the " + im.InstanceModuleInfo.Description + " Module may be connected to " + im.InstanceModuleInfo.InstanceInfo.DatabaseName + " at a time.");
            return;
          }
          DateTime currentUserAccessDateTime;
          using (SystemParameters sp = new SystemParameters(im.InstanceModuleInfo)) {
            currentUserAccessDateTime = sp.GetServerDateTime();
          }

          Application.EnableVisualStyles();
          Application.SetCompatibleTextRenderingDefault(false);

          using (LoginsAndProfiles logins = new LoginsAndProfiles(im.InstanceModuleInfo)) {
            List<spULPGetCurrentUserPermissions_Result> currentUserPermissions = logins.getCurrentUserPermissions(permissionRequired, currentUserAccessDateTime);

            if (currentUserPermissions.Count > 0) {
              logins.initializeCurrentLoginInUseInfo(currentUserPermissions, permissionRequired, im.InstanceModuleInfo.ShortName, currentUserAccessDateTime);
            }

            if (logins.validateCurrentUserAccess(im.InstanceModuleInfo.ShortName, permissionRequired)) {
              Application.Run(form);
            }
            else {
              MessageBox.Show(Environment.UserName.ToString() + " does not have permissions to " + permissionRequired);
              im.DeleteAppInUse(im.InstanceModuleInfo.Id, im.ApplicationInUseRecordId);
            }
            currentUserPermissions = null;
          }
        }
      }

    }

  }
}
