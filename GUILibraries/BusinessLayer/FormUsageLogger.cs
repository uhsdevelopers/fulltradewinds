﻿using InstanceManager.BusinessLayer;
using SIDLibraries.UI;

namespace GUILibraries.BusinessLayer
{
    public class FormUsageLogger : ExtensionCore
    {
        public FormUsageLogger(ModuleInfo moduleInfo)
            : base(moduleInfo, "LogFormUsage")
        {
        }
    }
}
