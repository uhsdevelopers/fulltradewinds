﻿using System.Windows.Forms;

namespace GUILibraries.BusinessLayer
{
    public static class KeyValidator
    {
        public static bool IsValidNumericKey(KeyEventArgs e)
        {
            return (e.KeyCode >= Keys.D0 && e.KeyCode <= Keys.D9) ||
                   (e.KeyCode >= Keys.NumPad0 && e.KeyCode <= Keys.NumPad9) || e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back;
        }

        public static bool IsValidNumericKey(KeyPressEventArgs e)
        {
            return (char.IsDigit(e.KeyChar) || e.KeyChar == 8);
        }
    }
}
