﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SIDLibraries.BusinessLayer;
using System.Text.RegularExpressions;

namespace GUILibraries.BusinessLayer
{
    public class NumbersFormat
    {
        public static bool IsValidNumericKey(KeyEventArgs e)
        {
            if ((e.KeyCode >= Keys.D0 && e.KeyCode <= Keys.D9) ||
            (e.KeyCode >= Keys.NumPad0 && e.KeyCode <= Keys.NumPad9) || e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
            {
                return true;
            }
            return false;
        }

        public static bool IsValidNumericKey(KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
                return true;
            return false;
        }

    }
}
