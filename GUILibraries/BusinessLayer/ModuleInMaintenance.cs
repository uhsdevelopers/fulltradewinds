﻿using System;
using System.Windows.Forms;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;
using System.Globalization;

namespace GUILibraries.BusinessLayer {
  public class ModuleInMaintenance : IDisposable {

    #region Private Vars
    int _messageShownCount;
    private TimeSpan? _clientServerTimeDiff;
    private readonly ModuleInfo _moduleInfo;
    private DateTime _messageLastShownDateTime = DateTime.Now;
    readonly String _maintenanceMesssage = "";
    readonly String _messagePriority = "";
    Boolean _keepShowing;
    Timer _globalTmr;
    Timer _tmr;
    #endregion

    #region Public Properties

    public Boolean CloseApplication { get; set; }

    #endregion

    #region Private Properties

    int DisplayMessageFreqMin { get; set; }
    int ShowMessageMaxCount { get; set; }

    private static DateTime ClientDateTime {
      get {
        return DateTime.Now;
      }
    }

    private TimeSpan ClientServerTimeDiff {
      get {
        if (_clientServerTimeDiff != null) return (TimeSpan)_clientServerTimeDiff;
        _clientServerTimeDiff = ClientDateTime - ServerDateTime;
        if (_clientServerTimeDiff != null) return (TimeSpan)_clientServerTimeDiff;
        return TimeSpan.Zero;
      }
    }

    private DateTime? ServerDateTime {
      get {
        using (var prm = new SystemParameters(_moduleInfo))
          return prm.GetServerDateTime();
      }
    }

    #endregion

    #region Constructors

    public ModuleInMaintenance(ModuleInfo moduleInfo) {

      _moduleInfo = moduleInfo;
      DisplayMessageFreqMin = moduleInfo.MaintMessage.DisplayMessageFreqMin;
      ShowMessageMaxCount = moduleInfo.MaintMessage.ShowMessageMaxCount;
      _maintenanceMesssage = moduleInfo.Description + " " + moduleInfo.MaintMessage.MaintenanceMesssage;
      _messagePriority = moduleInfo.MaintMessage.MessagePriority.ToString(CultureInfo.InvariantCulture);
      _keepShowing = false;
      CloseApplication = false;
    }

    public void ShowModuleInMaintenanceMsg() {
      LoadOverallTimerControl();
      ShowModuleInMaintenanceMsg(_maintenanceMesssage, _messagePriority, out _keepShowing);
      if (_keepShowing)
        StartTimer();
    }

    private void LoadOverallTimerControl() {
      _messageLastShownDateTime = GetCurrentDateTime();

      _globalTmr = new Timer { Interval = DisplayMessageFreqMin * ShowMessageMaxCount * 60 * 1000 * 2 };
      _globalTmr.Tick += _globalTmr_Tick;
      _globalTmr.Start();
    }

    private void ShowModuleInMaintenanceMsg(String message, String priority, out Boolean keepShowing) {
      var icon = new MessageBoxIcon();
      switch (priority) {
        case "High":
        case "1":
          icon = MessageBoxIcon.Stop;
          break;
        case "Medium":
        case "2":
          icon = MessageBoxIcon.Warning;
          break;
        case "Low":
        case "3":
          icon = MessageBoxIcon.Information;
          break;
      }
      keepShowing = false;
      if (MessageBox.Show(message, "Important Message: " + (ShowMessageMaxCount - (_messageShownCount + 1)) + " more time(s) will be shown.", MessageBoxButtons.OKCancel, icon) == DialogResult.Cancel) {
        keepShowing = true;
        _messageShownCount++;
      }
      else {
        CloseApplication = true;
        _moduleInfo.MaintAction();
      }
    }

    ~ModuleInMaintenance() {
      try {
        Dispose();
      }
      catch (ObjectDisposedException) {
      }
    }

    #endregion

    #region Private Methods

    public void Dispose() {
      if (_tmr != null) {
        _tmr.Stop();
        _tmr = null;
      }
      if (_globalTmr == null) return;
      _globalTmr.Stop();
      _globalTmr = null;
    }

    private DateTime GetCurrentDateTime() {
      var newDateTime = DateTime.Now.AddDays(ClientServerTimeDiff.Days * -1);
      newDateTime = newDateTime.AddHours(ClientServerTimeDiff.Hours * -1);
      newDateTime = newDateTime.AddMinutes(ClientServerTimeDiff.Minutes * -1);
      newDateTime = newDateTime.AddSeconds(ClientServerTimeDiff.Seconds * -1);
      newDateTime = newDateTime.AddMilliseconds(ClientServerTimeDiff.Milliseconds * -1);
      return newDateTime;
    }

    private void HandleGlobalTimerTick() {
      var dtNow = GetCurrentDateTime();
      var span = (dtNow - _messageLastShownDateTime);
      if (span.Minutes < DisplayMessageFreqMin * ShowMessageMaxCount * 2) return;
      _moduleInfo.MaintAction();
      CloseApplication = true;
    }

    private void HandleTimerTick() {
      if (_messageShownCount < ShowMessageMaxCount)
        ShowModuleInMaintenanceMsg(_maintenanceMesssage, _messagePriority, out _keepShowing);
      else {
        _moduleInfo.MaintAction();
        CloseApplication = true;
      }
    }

    private void StartTimer() {
      if (DisplayMessageFreqMin <= 0) return;
      var intervalMilliSeconds = DisplayMessageFreqMin * 60 * 1000;
      _tmr = new Timer { Interval = intervalMilliSeconds };
      _tmr.Tick += tmr_Tick;
      _tmr.Start();
    }

    #endregion

    #region Protected Methods

    #endregion

    #region Events

    private void _globalTmr_Tick(Object myObject, EventArgs myEventArgs) {
      HandleGlobalTimerTick();
    }

    private void tmr_Tick(Object myObject, EventArgs myEventArgs) {
      HandleTimerTick();
    }

    #endregion
  }
}