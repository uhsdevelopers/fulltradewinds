﻿using System.Windows.Forms;

namespace GUILibraries.BusinessLayer
{
    public class ManagedControl
    {
        public Control Control { get; set; }

        public string Name
        {
            get
            {
                return Control.Name ?? "";
            }
        }

        public string OwnerFormName { get; set; }
    }
}
