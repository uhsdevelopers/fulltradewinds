﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace GUILibraries.BusinessLayer
{
    public class LayoutManager
    {
        private List<ManagedControl> _controls;

        public List<ManagedControl> Controls
        {
            get
            {
                return _controls;
            }
        }

        public void AddControl(ManagedControl control)
        {
            var foundControl = GetControl(control.Name);

            if (foundControl != null)
                _controls.Remove(foundControl);
            _controls.Add(control);
        }

        public void AddControls(List<ManagedControl> controls)
        {
            foreach (var control in controls)
                AddControl(control);
        }

        public void ApplyLayout(Form targetForm)
        {
            foreach (var control in Controls)
            {
                var foundControl = targetForm.Controls.Find(control.Name, true);

                if (control.OwnerFormName != targetForm.Name)
                    continue;

                switch (foundControl[0].GetType().Name)
                {
                    case "NumberTextBox":
                        if (control.Control.Text != "~TextUnchanged" && control.Control.Text != "")
                            foundControl[0].Text = control.Control.Text; // assuming there is only one control with this name
                        break;
                    case "TextBox":
                        if (control.Control.Text != "~TextUnchanged" && control.Control.Text != "")
                            foundControl[0].Text = control.Control.Text; // assuming there is only one control with this name
                        break;
                }
                foundControl[0].Visible = control.Control.Visible;
            }
        }

        public ManagedControl GetControl(string controlName)
        {
            return _controls.FirstOrDefault(c => c.Name == controlName);
        }

        public void RemoveControl(ManagedControl control)
        {
            var foundControl = GetControl(control.Name);

            if (foundControl != null)
            {
                _controls.Remove(foundControl);
            }
        }

        public void RemoveAllControls()
        {
            _controls = new List<ManagedControl>();
        }

        public LayoutManager()
        {
            _controls = new List<ManagedControl>();
        }
    }
}
