﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;

namespace GUILibraries.BusinessLayer
{
    public class LinesFormat
    {
        private const int EMPTY_POINTS = -1000000;

        public static void TextPointsKeyPress(TextBox textField, TextBox otherPointsField, KeyPressEventArgs e, String wagerType, Boolean allowNonFavored = false)
        {
            /* string negVal = "";
           int txtLen = textField.Text.Length;
            String txtContent = textField.Text;

            if (!string.IsNullOrEmpty(textField.SelectedText)) textField.Text = textField.Text.Replace(textField.SelectedText, "");

            string posVal = textField.Text;
            if (wagerType == WagerType.SPREAD)
            {
                if (allowNonFavored)
                {
                    if (txtLen == 0 && e.KeyChar == (char)43)
                    {
                        negVal = "+";
                        textField.Text = negVal;
                    }
                    else
                        if (txtContent.Contains("+"))
                        {
                            negVal = "+";
                        }
                        else
                            negVal = "-";

                }
                else
                {
                    negVal = "-";
                }
                posVal = posVal.Replace(negVal, "");
            }

            if (e.KeyChar == '/')
            {
                e.Handled = true;
                if (!textField.Text.Contains("½"))
                {
                    textField.Text = negVal + posVal + "½";
                    textField.Select(textField.Text.Length, 0);
                }
            }
            else if (e.KeyChar == ',')
            {
                e.Handled = true;
                if (!textField.Text.Contains(","))
                {
                    if (!posVal.Contains("½"))
                    {
                        textField.Text = negVal + posVal + ", " + negVal + (posVal == "0" ? "" : posVal) + "½";
                        textField.Select(textField.Text.Length, 0);
                    }
                    else
                    {
                        int intVal = 1;
                        if (!posVal.StartsWith("½")) intVal = int.Parse(Regex.Match(posVal, @"\d+").Value) + 1;

                        textField.Text = negVal + posVal + ", " + negVal + intVal.ToString();
                        textField.Select(textField.Text.Length, 0);
                    }
                }
            }
            else if (!char.IsNumber(e.KeyChar) && e.KeyChar != (char)8 || (allowNonFavored && e.KeyChar == (char)43))
            {
                    e.Handled = true;
            }
            else
            {
                textField.Text = negVal + posVal;
                textField.Select(textField.Text.Length, 0);
            }

            if (otherPointsField != null && !string.IsNullOrEmpty(otherPointsField.Text)) otherPointsField.Text = string.Empty; */
          }

        public static String ManualModePointsKeyPress(TextBox textField, KeyPressEventArgs e)
        {
            var txtContent = textField.Text;
            var selectedTextLen = textField.SelectedText.Length;

            if (selectedTextLen >= 1)
            {
                if (e.KeyChar != (char)8)
                        txtContent = e.KeyChar.ToString(CultureInfo.InvariantCulture) == "/" ? "½" : e.KeyChar.ToString(CultureInfo.InvariantCulture);
                else
                    txtContent = string.Empty;

                return txtContent;
            }
            var allowedChars = new [] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ".", "+", "-", "/", "\b" };

            if (CharIsAllowed(allowedChars, e.KeyChar.ToString(CultureInfo.InvariantCulture)))
            {
                var firstChar = "";
                string lastChar;

                if (e.KeyChar == (char)43 || e.KeyChar == (char)45 || e.KeyChar == (char)47) // + or - or /
                {
                    if (e.KeyChar == (char)43 || e.KeyChar == (char)45)
                    {
                        if (txtContent.Length > 0)
                        {
                            firstChar = txtContent.Substring(0, 1);

                            if (firstChar == "+" || firstChar == "-")
                                e.Handled = true;
                        }
                        else
                            txtContent = e.KeyChar.ToString(CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        if (txtContent.Length > 0)
                        {
                            lastChar = txtContent[txtContent.Length - 1].ToString(CultureInfo.InvariantCulture);
                            if (lastChar == "½")
                                e.Handled = true;
                            else
                                txtContent += "½";
                        }
                        else
                            txtContent = "½";
                    }
                }
                else
                {
                    if (e.KeyChar == (char)8) return txtContent;
                        if (txtContent.Length > 0)
                        {
                            lastChar = txtContent[txtContent.Length - 1].ToString(CultureInfo.InvariantCulture);
                            firstChar = txtContent.Substring(0, 1);

                            if (lastChar == "½")
                                e.Handled = true;
                            else
                            {
                                if (firstChar != "+" && firstChar != "-")
                                    txtContent = "+" + txtContent;
                                txtContent += e.KeyChar.ToString(CultureInfo.InvariantCulture);
                            }

                        }
                        else
                        {
                            if (firstChar != "+" && firstChar != "-")
                                txtContent = "+" + txtContent;
                            txtContent += e.KeyChar.ToString(CultureInfo.InvariantCulture);
                        }
                    }
                }
            else
                e.Handled = true;            
            return txtContent;
        }

        public static String ManualModePriceKeyPress(TextBox textField, KeyPressEventArgs e, String priceType, Boolean allowXtoYRep = false)
        {
            var txtLen = textField.Text.Length;
            var txtContent = textField.Text;
            var selectedTextLen = textField.SelectedText.Length;

            string[] allowedChars; // new string[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "+", "-", ".", "/", "to"};

            switch (priceType)
            {
                case "American":
                    allowedChars = new [] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "+", "-", "\b" };

                    if (selectedTextLen > 0)
                    {
                        switch (e.KeyChar)
                        {
                            case (char)45:
                            case (char)43:
                            txtContent = e.KeyChar.ToString(CultureInfo.InvariantCulture);
                                break;
                            case (char)8:
                                txtContent = string.Empty;
                                break;
                            default:
                                txtContent = e.KeyChar.ToString(CultureInfo.InvariantCulture);
                                break;
                        }
                        return txtContent;
                    }

                    if (CharIsAllowed(allowedChars, e.KeyChar.ToString(CultureInfo.InvariantCulture)))
                    {
                        var firstChar = "";

                        if (e.KeyChar == (char)43 || e.KeyChar == (char)45) // + or -
                        {
                            if (e.KeyChar == (char)43 || e.KeyChar == (char)45)
                            {
                                if (txtContent.Length > 0)
                                {
                                    firstChar = txtContent.Substring(0, 1);

                                    if (firstChar == "+" || firstChar == "-")
                                        e.Handled = true;
                                }
                                else
                                    txtContent = e.KeyChar.ToString(CultureInfo.InvariantCulture);
                            }
                        }
                        else
                        {
                            if (e.KeyChar != (char)8)
                            {
                                if (txtContent.Length > 0)
                                {
                                    firstChar = txtContent.Substring(0, 1);
                                    if (firstChar != "+" && firstChar != "-")
                                        txtContent = "+" + txtContent;
                                    txtContent += e.KeyChar.ToString(CultureInfo.InvariantCulture);

                                }
                                else
                                {
                                    if (firstChar != "+" && firstChar != "-")
                                        txtContent = "+" + txtContent;
                                    txtContent += e.KeyChar.ToString(CultureInfo.InvariantCulture);
                                }
                            }
                        }
                    }
                    else
                        e.Handled = true;
                    break;
                case "Decimal":
                    allowedChars = new [] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ".", "\b" };

                    if (selectedTextLen > 0)
                    {
                        if (e.KeyChar == (char)8 || e.KeyChar.ToString(CultureInfo.InvariantCulture) == ".")
                            txtContent = string.Empty;
                        else
                            txtContent = e.KeyChar.ToString(CultureInfo.InvariantCulture);
                        return txtContent;
                    }
                    if (CharIsAllowed(allowedChars, e.KeyChar.ToString(CultureInfo.InvariantCulture)))
                    {
                        if (e.KeyChar != (char)8)
                        {
                            var newDecimalValue = txtContent + e.KeyChar;

                        decimal tryDec;
                            if (!Decimal.TryParse(newDecimalValue, out tryDec))
                                e.Handled = true;
                            else
                                txtContent = newDecimalValue;
                        }
                    }
                    else
                        e.Handled = true;
                    break;
                case "Fractional":
                    allowedChars = new [] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "/", "\b" };

                    if (selectedTextLen > 0)
                    {
                        if (e.KeyChar == (char)8 || e.KeyChar.ToString(CultureInfo.InvariantCulture) == "/")
                            txtContent = string.Empty;
                        else
                            txtContent = e.KeyChar.ToString(CultureInfo.InvariantCulture);
                        return txtContent;
                    }
                    if (CharIsAllowed(allowedChars, e.KeyChar.ToString(CultureInfo.InvariantCulture)))
                    {
                        if (e.KeyChar.ToString(CultureInfo.InvariantCulture) == "/")
                        {
                            if (txtLen > 0)
                            {
                                if (txtContent.Contains("/"))
                                    e.Handled = true;
                                else
                                    txtContent += "/";
                            }
                            else
                                e.Handled = true;
                        }
                        else if (e.KeyChar != (char)8)
                                txtContent += e.KeyChar.ToString(CultureInfo.InvariantCulture);
                    }
                    else
                        e.Handled = true;
                    break;
                case "Xtrange":
                    allowedChars = new [] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "\b", "t", "T", "o", "O" };

                    if (selectedTextLen > 0)
                    {
                        if (e.KeyChar == (char)8 || e.KeyChar.ToString(CultureInfo.InvariantCulture).ToLower() == "t" || e.KeyChar.ToString(CultureInfo.InvariantCulture).ToLower() == "o")
                            txtContent = string.Empty;
                        else
                            txtContent = e.KeyChar.ToString(CultureInfo.InvariantCulture);
                        return txtContent;
                    }
                    if (!CharIsAllowed(allowedChars, e.KeyChar.ToString(CultureInfo.InvariantCulture)))
                        e.Handled = true;
                    else
                        txtContent += e.KeyChar.ToString(CultureInfo.InvariantCulture);
                    break;
            }

            return txtContent;
        }

        private static bool CharIsAllowed(IEnumerable<string> allowedChars, string typedChar)
        {
            return allowedChars.Any(str => typedChar == str);
        }

        public static void TextPriceKeyPress(TextBox textField, KeyPressEventArgs e, String xtoYRepCheck = null, Boolean allowOtherThanAmerican = false)
        {
            if (char.IsNumber(e.KeyChar) || e.KeyChar == (char)8) return;
                if (xtoYRepCheck == null)
                {
                    if (e.KeyChar.ToString(CultureInfo.InvariantCulture) != "-" && e.KeyChar.ToString(CultureInfo.InvariantCulture) != "+")
                        e.Handled = true;
                }
                else
                {
                    if (allowOtherThanAmerican)
                    {
                        if (e.KeyChar.ToString(CultureInfo.InvariantCulture) != "/" && e.KeyChar.ToString(CultureInfo.InvariantCulture) != "." && e.KeyChar.ToString(CultureInfo.InvariantCulture) != "-" && e.KeyChar.ToString(CultureInfo.InvariantCulture) != "+" && e.KeyChar.ToString(CultureInfo.InvariantCulture).ToLower() != "t" && e.KeyChar.ToString(CultureInfo.InvariantCulture) != "o" && e.KeyChar.ToString(CultureInfo.InvariantCulture) != " ")
                            e.Handled = true;
                    }
                    else if (e.KeyChar.ToString(CultureInfo.InvariantCulture) != "-" && e.KeyChar.ToString(CultureInfo.InvariantCulture) != "+" && e.KeyChar.ToString(CultureInfo.InvariantCulture).ToLower() != "t" && e.KeyChar.ToString(CultureInfo.InvariantCulture) != "o" && e.KeyChar.ToString(CultureInfo.InvariantCulture) != " ")
                        e.Handled = true;
                }
        }

        public static double GetNumericPoints(string points)
        {
            double numPoints = EMPTY_POINTS;
            if (string.IsNullOrEmpty(points)) return numPoints;
                var sNumPoints = points.Replace("½", ".5");
                if (sNumPoints.Contains(","))
                {
                    var numArr = sNumPoints.Split(',');
                    if (numArr.Length != 2) sNumPoints = sNumPoints.Replace(",", "");
                    else
                    {
                        if (numArr[0].Contains(".5")) sNumPoints = numArr[0].Replace(".5", ".75");
                        else if (numArr[1].Contains(".5")) sNumPoints = numArr[1].Replace(".5", ".25");
                    }
                }
                double.TryParse(sNumPoints, out numPoints);
            return numPoints;
        }

        public static string FormatStringPoints(double? value, string thresholdType)
        {
            var retValue = "";

            if (value != null)
            {
                var decPart = (Math.Ceiling(Math.Abs((decimal)value)) - Math.Abs((decimal)value)).ToString(CultureInfo.InvariantCulture);
                var sign = value > 0 ? "+" : "-";

                switch (decPart)
                {
                    case "0":
                            retValue = (value > 0 ? "+" : "") + value;
                        break;
                    case "0.25":
                            retValue = sign + Math.Floor(Math.Abs((decimal)value)) + "½, " + sign + Math.Ceiling(Math.Abs((decimal)value));
                        break;
                    case "0.5":
                            retValue = (value > 0 ? "+" : "") + value.ToString().Replace(".5", "½");
                        break;
                    case "0.75":
                            retValue = sign + Math.Floor(Math.Abs((decimal)value)) + ", " + sign + Math.Floor(Math.Abs((decimal)value)) + "½";
                        break;
                }
            }
            if (thresholdType == "P")
                retValue = retValue.Replace("+", "").Replace("-", "");
            return retValue;
        }

        public static bool IsValidLine(string points, bool allowAsian)
        {
            var doublePnts = GetNumericPoints(points);

            if (doublePnts == EMPTY_POINTS)
                return false;

            var res = Math.Abs(Math.Floor(doublePnts) - doublePnts);

            if (allowAsian)
            {
                if (res != 0 && res != 0.25 && res != 0.5 && res != 0.75)
                    return false;
            }
            else
            {
                if (res != 0 && res != 0.5)
                    return false;
            }
            return true;
        }
    }
}
