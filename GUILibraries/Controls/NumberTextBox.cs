﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using SIDLibraries.Utilities;

namespace GUILibraries.Controls {
  public partial class NumberTextBox : TextBox {

    Boolean _divideFlag = true;
    int _dividedBy = 100;
    Boolean _showIfZero = true;
    Boolean _allowDecimalSeparator;
    Boolean _allowNegatives;

    public NumberTextBox() {
      InitializeComponent();
    }

    public override string Text {
      get {
        return base.Text;
      }
      set {
        double res;
        base.Text = double.TryParse(value, out res) ? DisplayNumber.Format(res, _divideFlag, _dividedBy, _showIfZero, _allowDecimalSeparator) : value;
      }
    }

    [Description("Tells if the number will be divided by another figure")]
    [Category("Formatting")]
    [DefaultValue(0)]

    public Boolean DivideFlag {
      get {
        return _divideFlag;
      }
      set {
        _divideFlag = value;
        if (!value) {
          _dividedBy = 1;
        }
      }
    }

    [Description("Gets or Sets the figure number will be divided By")]
    [Category("Formatting")]
    [DefaultValue(0)]

    public int DividedBy {
      get {
        return _dividedBy;
      }
      set {
        if (value == 0 || !_divideFlag)
          _dividedBy = 1;
        else
          _dividedBy = value;
      }
    }

    [Description("Tells if textBox will show zero")]
    [Category("Formatting")]
    [DefaultValue(0)]

    public Boolean ShowIfZero {
      get {
        return _showIfZero;
      }
      set {
        _showIfZero = value;
      }
    }

    [Description("Tells if the number will accept cents")]
    [Category("Formatting")]
    [DefaultValue(0)]

    public Boolean AllowCents {
      get {
        return _allowDecimalSeparator;
      }
      set {
        _allowDecimalSeparator = value;
      }
    }

    public Boolean AllowNegatives {
      get {
        return _allowNegatives;
      }
      set {
        _allowNegatives = value;
      }
    }

    private void HandleKeyPressEvent(object sender, KeyPressEventArgs e) {
      int ascii = Convert.ToInt16(e.KeyChar);

      if ((ascii >= 48 && ascii <= 57) || (ascii == 8) || (_allowDecimalSeparator && ascii == 46) || (_allowDecimalSeparator && ascii == 47) || (_allowNegatives && ascii == 45))
        e.Handled = false;
      else
        e.Handled = true;

      if (!_allowDecimalSeparator || ascii != 47) return;
      var txtB = (NumberTextBox)sender;
      e.Handled = true;
      if (!string.IsNullOrEmpty(txtB.SelectedText))
        txtB.Text = txtB.Text.Replace(txtB.SelectedText, "");
      var value = txtB.Text;
      if (txtB.Text.Contains("½")) return;
      txtB.Text = value + @"½";
      txtB.Select(txtB.Text.Length, 0);
    }

    private void NumberTextBox_KeyPress(object sender, KeyPressEventArgs e) {
      HandleKeyPressEvent(sender, e);
    }

    private void NumberTextBox_Leave(object sender, EventArgs e) {
      if (!_allowDecimalSeparator) return;
      var txtB = (NumberTextBox)sender;
      txtB.Text = txtB.Text.Replace(".5", "½");
    }
  }
}