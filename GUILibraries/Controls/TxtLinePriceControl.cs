﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SIDLibraries.BusinessLayer;

namespace GUILibraries.Controls
{
    public partial class TxtLinePriceControl : TextBox
    {
        private bool _validKeyEntered;
        private String SelectedPrice;
        private Form CallerFrm;

        public delegate void ControlLeavingHandler(object sender, EventArgs e);
        public event EventHandler ControlLeft;
        public delegate void ControlTextChangedHandler(object sender, EventArgs e);
        public event EventHandler NewTextChanged;

        public TxtLinePriceControl(String selectedPrice, Form callerFrm)
        {
            SelectedPrice = selectedPrice;
            CallerFrm = callerFrm;
            InitializeComponent();
            this.Size = new Size(68, 25);
        }

        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                base.Text = value;
            }
        }

        private void txtPrice_KeyDown(object sender, KeyEventArgs e)
        {
           _validKeyEntered = false;
            SelectedPrice = "American";
            if (e.KeyCode == Keys.Decimal)
                SelectedPrice = "Decimal";

            switch (SelectedPrice)
            {
                case "American":
                case LineOffering.PRICE_AMERICAN:
                    if (e.KeyCode == Keys.D0 || e.KeyCode == Keys.D1 || e.KeyCode == Keys.D2 || e.KeyCode == Keys.D3 || e.KeyCode == Keys.D4
                        || e.KeyCode == Keys.D5 || e.KeyCode == Keys.D6 || e.KeyCode == Keys.D7 || e.KeyCode == Keys.D8 || e.KeyCode == Keys.D9
                        || e.KeyCode == Keys.NumPad0 || e.KeyCode == Keys.NumPad1 || e.KeyCode == Keys.NumPad2 || e.KeyCode == Keys.NumPad3 || e.KeyCode == Keys.NumPad4 || e.KeyCode == Keys.NumPad5
                        || e.KeyCode == Keys.NumPad6 || e.KeyCode == Keys.NumPad7 || e.KeyCode == Keys.NumPad8 || e.KeyCode == Keys.NumPad9
                        || e.KeyCode == Keys.Add || e.KeyCode == Keys.Subtract || e.KeyCode == Keys.Divide || e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete || e.KeyCode == Keys.OemMinus)
                    {
                        _validKeyEntered = true;
                    }
                    break;
                case "Decimal":
                case LineOffering.PRICE_DECIMAL:
                    if (e.KeyCode == Keys.D0 || e.KeyCode == Keys.D1 || e.KeyCode == Keys.D2 || e.KeyCode == Keys.D3 || e.KeyCode == Keys.D4
                        || e.KeyCode == Keys.D5 || e.KeyCode == Keys.D6 || e.KeyCode == Keys.D7 || e.KeyCode == Keys.D8 || e.KeyCode == Keys.D9
                        || e.KeyCode == Keys.NumPad0 || e.KeyCode == Keys.NumPad1 || e.KeyCode == Keys.NumPad2 || e.KeyCode == Keys.NumPad3 || e.KeyCode == Keys.NumPad4 || e.KeyCode == Keys.NumPad5
                        || e.KeyCode == Keys.NumPad6 || e.KeyCode == Keys.NumPad7 || e.KeyCode == Keys.NumPad8 || e.KeyCode == Keys.NumPad9
                        || e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete || e.KeyCode == Keys.Decimal || e.KeyCode == Keys.OemPeriod)
                    {
                        _validKeyEntered = true;
                    }
                    break;
                case "Fractional":
                case LineOffering.PRICE_FRACTIONAL:
                    if (e.KeyCode == Keys.D0 || e.KeyCode == Keys.D1 || e.KeyCode == Keys.D2 || e.KeyCode == Keys.D3 || e.KeyCode == Keys.D4
                        || e.KeyCode == Keys.D5 || e.KeyCode == Keys.D6 || e.KeyCode == Keys.D7 || e.KeyCode == Keys.D8 || e.KeyCode == Keys.D9
                        || e.KeyCode == Keys.NumPad0 || e.KeyCode == Keys.NumPad1 || e.KeyCode == Keys.NumPad2 || e.KeyCode == Keys.NumPad3 || e.KeyCode == Keys.NumPad4 || e.KeyCode == Keys.NumPad5
                        || e.KeyCode == Keys.NumPad6 || e.KeyCode == Keys.NumPad7 || e.KeyCode == Keys.NumPad8 || e.KeyCode == Keys.NumPad9
                        || e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete || e.KeyCode == Keys.Divide)
                    {
                        _validKeyEntered = true;
                    }
                    break;
            }
        }

        private void txtPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (_validKeyEntered)
            {
                if (e.KeyChar == (char)8) return;
                if (((TextBox)sender).SelectionStart == ((TextBox)sender).Text.Length)
                {
                    bool handled;
                    ((TextBox)sender).Text = LinesFormat.ManualModePointsKeyPress(((TextBox)sender).Text, ((TextBox)sender).SelectedText.Length, e.KeyChar, false, out handled);
                    e.Handled = handled;
                    ((TextBox)sender).HideSelection = true;
                    ((TextBox)sender).Select(((TextBox)sender).Text.Length, 0);
                }
                else
                {
                    if (e.KeyChar != (char)43 && e.KeyChar != (char)45 && e.KeyChar != (char)47) return;
                    if ((e.KeyChar == (char)43 || e.KeyChar == (char)45) && ((TextBox)sender).SelectionStart == 0)
                    {
                    }
                    else
                    {
                        ((TextBox)sender).HideSelection = true;
                        ((TextBox)sender).Select(((TextBox)sender).Text.Length, 0);
                    }
                }
            }
            else
                e.Handled = true;
        }

        private void txtPrice_Leave(object sender, EventArgs e)
        {
            if (((TextBox)sender).Text == "" || (CallerFrm != null && CallerFrm.ActiveControl.Name == "btnCancel")) return;
            if (((TextBox)sender).Text.Contains("to"))
            {
                MessageBox.Show(@"Odds must be in American, Decimal or Fractional format", @"Invalid Odds");
                ((TextBox)sender).Focus();
                return;
            }

            SelectedPrice = "American";
            if (((TextBox)sender).Text.Contains("."))
                SelectedPrice = "Decimal";
            if (((TextBox)sender).Text.Contains("/"))
                SelectedPrice = "Fractional";

            switch (SelectedPrice)
            {
                case "American":
                    int tryInt;
                    if (!Int32.TryParse(((TextBox)sender).Text, out tryInt) || tryInt == 0 || Math.Abs(double.Parse(tryInt.ToString())) < 100)
                    {
                        MessageBox.Show(@"Odds must be in American format", @"Invalid Odds");
                        ((TextBox)sender).Focus();
                    }
                    break;
                case "Decimal":
                    decimal tryDec;
                    if (!Decimal.TryParse(((TextBox)sender).Text, out tryDec) || tryDec == 0)
                    {
                        MessageBox.Show(@"Odds must be in Decimal format", @"Invalid Odds");
                        ((TextBox)sender).Focus();
                    }
                    break;
                case "Fractional":
                    if (((TextBox)sender).Text.Contains("/"))
                    {
                        int tryNumerator;
                        int tryDenominator;

                        var numDenom = ((TextBox)sender).Text.Split('/');

                        int.TryParse(numDenom[0], out tryNumerator);
                        int.TryParse(numDenom[1], out tryDenominator);

                        if (tryNumerator <= 0 || tryNumerator > 10000)
                        {
                            MessageBox.Show(@"Fractional prices must be in the format X/Y where both are greater than 0, X is less than 10,000 and Y is less than 20", @"Invalid Odds");
                            ((TextBox)sender).Focus();
                            return;
                        }

                        if (tryDenominator <= 0 || tryDenominator > 20)
                        {
                            MessageBox.Show(@"Fractional prices must be in the format X/Y where both are greater than 0, X is less than 10,000 and Y is less than 20", @"Invalid Odds");
                            ((TextBox)sender).Focus();
                        }
                    }
                    else
                    {
                        MessageBox.Show(@"Fractional prices must be in the format X/Y where both are greater than 0, X is less than 10,000 and Y is less than 20", @"Invalid Odds");
                        ((TextBox)sender).Focus();
                    }
                    break;
            }
            OnControlLeaving(new ControlLeavingEventArgs() { });
        }

        private void txtPrice_TextChanged(object sender, EventArgs e)
        {
            if (CallerFrm != null)
            {
                var txtRisk = CallerFrm.Controls.Find("txtRisk", true).FirstOrDefault();
                var txtToWin = CallerFrm.Controls.Find("txtToWin", true).FirstOrDefault();

                if (txtRisk != null)
                    ((TextBox)txtRisk).Text = String.Empty;
                if (txtToWin != null)
                    ((TextBox)txtToWin).Text = String.Empty;
                OnTextChanged(new ControlTextChangedEventArgs() { NewText = ((TextBox)sender).Text });
            }
        }

        public virtual void OnControlLeaving(ControlLeavingEventArgs e)
        {
            if (ControlLeft != null)
            {
                ControlLeft(this, e);
            }
        }

        public virtual void OnTextChanged(ControlTextChangedEventArgs e)
        {
            if (NewTextChanged != null)
            {
                NewTextChanged(this, e);
            }
        }
    }
}
