﻿namespace GUILibraries
{
    partial class TxtLineAdjControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // txtLineAdj
            // 
            this.TextChanged += new System.EventHandler(this.txtLineAdj_TextChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtLineAdj_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLineAdj_KeyPress);
            this.Leave += new System.EventHandler(this.txtLineAdj_Leave);
            this.ResumeLayout(false);
        }

        #endregion
    }
}
