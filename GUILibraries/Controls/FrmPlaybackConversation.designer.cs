﻿namespace GUILibraries.Controls
{
    partial class FrmPlaybackConversation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPlaybackConversation));
            this.axWindowsMediaPlayerPlayback = new AxWMPLib.AxWindowsMediaPlayer();
            ((System.ComponentModel.ISupportInitialize)(this.axWindowsMediaPlayerPlayback)).BeginInit();
            this.SuspendLayout();
            // 
            // axWindowsMediaPlayerPlayback
            // 
            this.axWindowsMediaPlayerPlayback.Enabled = true;
            this.axWindowsMediaPlayerPlayback.Location = new System.Drawing.Point(68, 27);
            this.axWindowsMediaPlayerPlayback.Name = "axWindowsMediaPlayerPlayback";
            this.axWindowsMediaPlayerPlayback.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axWindowsMediaPlayerPlayback.OcxState")));
            this.axWindowsMediaPlayerPlayback.Size = new System.Drawing.Size(539, 397);
            this.axWindowsMediaPlayerPlayback.TabIndex = 0;
            this.axWindowsMediaPlayerPlayback.PlayStateChange += new AxWMPLib._WMPOCXEvents_PlayStateChangeEventHandler(this.axWindowsMediaPlayerPlayback_PlayStateChange);
            // 
            // frmPlaybackConversation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(664, 458);
            this.Controls.Add(this.axWindowsMediaPlayerPlayback);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FrmPlaybackConversation";
            this.Text = "Playback Conversation";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmPlaybackConversation_FormClosed);
            this.Load += new System.EventHandler(this.frmPlaybackConversation_Load);
            ((System.ComponentModel.ISupportInitialize)(this.axWindowsMediaPlayerPlayback)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private AxWMPLib.AxWindowsMediaPlayer axWindowsMediaPlayerPlayback;
    }
}