﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using SIDLibraries.Utilities;

namespace GUILibraries.Controls {

  public class BetOptionNode : TreeNode {
    public readonly string ContestLevel;
    public readonly string NodeType;
    public DateTime? ScheduleDate;

    public BetOptionNode(string text, string nodeType, string contestLevel = "ContestType", DateTime? scheduleDate = null)
      : base(text) {
      ContestLevel = contestLevel;
      NodeType = nodeType;
      ScheduleDate = scheduleDate;
    }
  }

  public class GamesAndPropsTreeView : IDisposable {

    #region Private Constants
    private const string SPORT_DESCRIPTION = "";
    #endregion

    #region Private Properties
    private List<TreeRootNode> _treeParentNodes;
    private readonly TreeView _treeViewObject;

    #endregion

    #region Public Properties

    #endregion

    #region Public vars

    #endregion

    #region Private vars

    private List<spGLGetActiveGamesByCustomer_Result> _activeGames;
    private List<spCnGetActiveContests_Result> _activeContests;

    #endregion

    #region Structures

    #endregion

    #region Constructors

    public GamesAndPropsTreeView(TreeView callerObj, List<spGLGetActiveGamesByCustomer_Result> activeGames, List<spCnGetActiveContests_Result> activeContests = null) {
      _activeGames = activeGames;
      _activeContests = activeContests;
      _treeViewObject = callerObj;
    }

    #endregion

    #region Public Methods

    public void PopulateGamesAndContestsTree(String caller = null) {
      BetOptionNode level1 = null, level2 = null, level3 = null;
      BetOptionNode contestParentNode = null;

      int cGames = 0, cContest = 0;
      string slevel1 = string.Empty;
      string slevel3 = string.Empty;
      string holdLevel1 = string.Empty, holdLevel2 = string.Empty, holdLevel3 = string.Empty, holdLevel4 = string.Empty;
      string nodeType = string.Empty;

      if (_activeGames == null)
        _activeGames = new List<spGLGetActiveGamesByCustomer_Result>();
      _activeGames = _activeGames.OrderBy(x => x.SportType).ThenBy(x => x.ScheduleDate).ThenBy(x => x.SportSequenceNumber).
                                      ThenBy(x => x.Team1RotNum).ThenBy(x => x.SportSubType).ThenBy(x => x.GameDateTime).ThenBy(x => x.GameNum).
                                      ThenBy(x => x.PeriodNumber).
                                      ToList();

      if (_activeContests == null)
        _activeContests = new List<spCnGetActiveContests_Result>();
      _activeContests = _activeContests.OrderBy(x => x.ContestType).ThenBy(x => x.ContestType2).ThenBy(x => x.ContestType3).
                                          ThenBy(x => x.FirstRotNum).ThenBy(x => x.ContestNum).ThenBy(x => x.RotNum).ThenBy(x => x.ContestantName).
                                          ThenBy(x => x.ContestantSeq).
                                          ToList();

      if (_treeParentNodes == null) _treeParentNodes = new List<TreeRootNode>();
      _treeParentNodes.Clear();

      while (cGames < _activeGames.Count || cContest < _activeContests.Count) {
        if (cGames < _activeGames.Count) {
          if (cContest < _activeContests.Count) {
            if (String.Compare(_activeGames[cGames].SportType.Trim(), _activeContests[cContest].ContestType.Trim(), StringComparison.Ordinal) <= 0) {
              slevel1 = _activeGames[cGames].SportType.Trim();
              nodeType = "G";
            }
            else {
              slevel1 = _activeContests[cContest].ContestType.Trim();
              nodeType = "C";
            }
          }
          else {
            slevel1 = _activeGames[cGames].SportType.Trim();
            nodeType = "G";
          }
        }
        else {
          if (cContest < _activeContests.Count) {
            slevel1 = _activeContests[cContest].ContestType.Trim();
            nodeType = "C";
          }
        }
        if (slevel1 != holdLevel1) {
            _treeViewObject.Nodes.Add(new BetOptionNode(slevel1 + (nodeType == "G" ? " " + SPORT_DESCRIPTION : ""), nodeType, null));
          holdLevel2 = string.Empty;
          level1 = (BetOptionNode)_treeViewObject.Nodes[_treeViewObject.Nodes.Count - 1];
          if (nodeType == "G") _treeParentNodes.Add(new TreeRootNode(_activeGames[cGames], nodeType));
          else {
            contestParentNode = level1;
            _treeParentNodes.Add(new TreeRootNode(_activeContests[cContest], nodeType));
          }
        }

        string slevel4;
        string slevel2;
        if (nodeType == "G") {
          var toDate = (DateTime)_activeGames[cGames].ScheduleDate;
          toDate = toDate.AddDays(slevel1.Trim() == SportTypes.FOOTBALL ? 6 : 0);

          if (slevel1.Trim() == SportTypes.FOOTBALL)
            slevel2 = DisplayDate.Format((DateTime)_activeGames[cGames].ScheduleDate, DisplayDateFormat.MonthDate)
                    + " - " + DisplayDate.Format(toDate, DisplayDateFormat.MonthDate);
          else
            slevel2 = DisplayDate.Format((DateTime)_activeGames[cGames].ScheduleDate, DisplayDateFormat.MonthDate);

          if (holdLevel2 != slevel2 && level1 != null) {
            level1.Nodes.Add(new BetOptionNode(slevel2, "G", "", _activeGames[cGames].ScheduleDate));
            level2 = (BetOptionNode)level1.Nodes[level1.Nodes.Count - 1];
            holdLevel3 = string.Empty;
          }
          slevel3 = _activeGames[cGames].SportSubType.Trim();
          BetOptionNode auxNode;

          if (holdLevel3 != slevel3 && level2 != null) {
            auxNode = new BetOptionNode(slevel3, "G", "", _activeGames[cGames].ScheduleDate);
            auxNode.ImageIndex = auxNode.SelectedImageIndex = GetSportImageIndex(_activeGames[cGames].SportType);
            level2.Nodes.Add(auxNode);
            level3 = (BetOptionNode)level2.Nodes[level2.Nodes.Count - 1];
            holdLevel4 = string.Empty;
          }

          slevel4 = _activeGames[cGames].Team1RotNum.ToString();
          if (level3 != null && slevel4 != holdLevel4 && caller == null) {
            auxNode = new BetOptionNode(_activeGames[cGames].Team1RotNum + " " +
                                   _activeGames[cGames].Team1ID.Trim() + " / " +
                                   _activeGames[cGames].Team2RotNum + " " +
                                   _activeGames[cGames].Team2ID.Trim() + "    " +
                                   DateTimeF.FormatGameDate(_activeGames[cGames].GameDateTime ?? DateTime.MinValue), "G", "", _activeGames[cGames].ScheduleDate);
            auxNode.ImageIndex = auxNode.SelectedImageIndex = GetSportImageIndex(_activeGames[cGames].SportType);
            level3.Nodes.Add(auxNode);
          }
          cGames++;
        }
        else {
          slevel2 = _activeContests[cContest].ContestType2.Trim();

          if (slevel2 != ".") {
            if (slevel2 != holdLevel2 && level1 != null) {
              level1.Nodes.Add(new BetOptionNode(slevel2, "C", "ContestType2"));
              level2 = (BetOptionNode)level1.Nodes[level1.Nodes.Count - 1];
              contestParentNode = level2;
              holdLevel3 = string.Empty;
              holdLevel4 = string.Empty;
            }

            slevel3 = _activeContests[cContest].ContestType3.Trim();
            if (slevel3 != ".") {
              if (slevel3 != holdLevel3 && level2 != null) {
                level2.Nodes.Add(new BetOptionNode(slevel3, "C", "ContestType3"));
                level3 = (BetOptionNode)level2.Nodes[level2.Nodes.Count - 1];
                contestParentNode = level3;
                holdLevel4 = string.Empty;
              }
            }


          }
          slevel4 = _activeContests[cContest].RotNum.ToString();
          if (holdLevel4 != slevel4 && contestParentNode != null && caller == null) {
            contestParentNode.Nodes.Add(new BetOptionNode(_activeContests[cContest].ContestDesc.Trim(), "C", "ContestDesc"));
            cContest = cContest + _activeContests[cContest].NumberOfContestants ?? 1;
          }
          else
            cContest++;
        }

        holdLevel1 = slevel1;
        holdLevel2 = slevel2;
        holdLevel3 = slevel3;
        holdLevel4 = slevel4;

      }
      _treeViewObject.SelectedNode = null;
    }

    #endregion

    #region Private Methods

    public TreeRootNode GetSelectedRootNodeObject() {
      TreeRootNode node = null;
      if (_treeViewObject.SelectedNode != null) {
        switch (_treeViewObject.SelectedNode.Level) {
          case 0:
            node = _treeParentNodes[_treeViewObject.SelectedNode.Index]; break;
          case 1:
            node = _treeParentNodes[_treeViewObject.SelectedNode.Parent.Index]; break;
          case 2:
            node = _treeParentNodes[_treeViewObject.SelectedNode.Parent.Parent.Index]; break;
          case 3:
            node = _treeParentNodes[_treeViewObject.SelectedNode.Parent.Parent.Parent.Index]; break;
          default:
            node = _treeParentNodes[_treeViewObject.SelectedNode.Parent.Parent.Index]; break;
        }
      }
      return node;
    }

    public TreeNode GetSelectedNodeObject() {
      return _treeViewObject.SelectedNode;
    }
   
    //Create a table or xml file for this mapping
    private static int GetSportImageIndex(string sportType) {
      int ret;
      switch (sportType.Trim()) {
        case SportTypes.SOCCER: ret = 2; break;
        case SportTypes.BASKETBALL: ret = 3; break;
        case SportTypes.FOOTBALL: ret = 4; break;
        case SportTypes.HOCKEY: ret = 5; break;
        case SportTypes.BASEBALL : ret = 6; break;
        case "Contest": ret = 8; break;
        default: ret = 7; break;
      }
      return ret;
    }

    public void Dispose() {
      _activeGames = null;
      _activeContests = null;
    }

    ~GamesAndPropsTreeView() {
      Dispose();
    }

    #endregion

    #region Events

    #endregion

    #region Helper Classes

    public class TreeRootNode {
      public object NodeObject;
      public string Type;
      public ArrayList ChildNodes;

      public TreeRootNode(object nodeObject, string type) {
        NodeObject = nodeObject;
        Type = type;
      }
    }

    #endregion
  }
}

