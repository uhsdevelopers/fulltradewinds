﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using GUILibraries.Forms;
using InstanceManager.BusinessLayer;

namespace GUILibraries.Controls {
  public partial class FrmPlaybackConversation : SIDForm {

    private readonly String _currentFileToPlayback = "";
    private readonly String _currentFolderToDelete = "";
    static Boolean _appStop;

    public FrmPlaybackConversation(String folderToDelete, String fileToPlayback, ModuleInfo moduleInfo)
      : base(moduleInfo) {
      if (!string.IsNullOrEmpty(folderToDelete) && !string.IsNullOrEmpty(fileToPlayback)) {
        _currentFolderToDelete = folderToDelete;
        _currentFileToPlayback = fileToPlayback;
        InitializeComponent();
      }
      else {
        MessageBox.Show("Unable to playback. Target folder or TicketNumber was not provided");
        Close();
      }
    }

    private void frmPlaybackConversation_Load(object sender, EventArgs e) {
      if (File.Exists(_currentFileToPlayback)) {
        var url = _currentFileToPlayback;
        axWindowsMediaPlayerPlayback.settings.autoStart = true;
        axWindowsMediaPlayerPlayback.URL = url;
      }
      else {
        MessageBox.Show("Unable to playback. File not found in required local folder");
        Close();
      }
    }

    private void frmPlaybackConversation_FormClosed(object sender, FormClosedEventArgs e) {
      axWindowsMediaPlayerPlayback.URL = null;

      try {
        var tmr = new System.Windows.Forms.Timer();
        tmr.Tick += tmr_Tick;
        tmr.Interval = 500;
        tmr.Start();

        while (!_appStop)
          Application.DoEvents();

        var c = 3;
        var deleteOk = false;

        while (!deleteOk && c >= 0) {
          try {
            Utilities.TicketDr.DeleteDirectoryAndContents(_currentFolderToDelete);
            deleteOk = true;
          }
          catch {
            Thread.Sleep(500);
          }
          c--;
        }
      }
      catch (IOException ex) {
        MessageBox.Show("Problems deleting temp Playback folder: " + ex.Message);
      }
    }

    private static void tmr_Tick(Object myObject, EventArgs myEventArgs) {
      ((System.Windows.Forms.Timer)myObject).Stop();
      ((System.Windows.Forms.Timer)myObject).Dispose();
      _appStop = true;
    }

    private void axWindowsMediaPlayerPlayback_PlayStateChange(object sender, AxWMPLib._WMPOCXEvents_PlayStateChangeEvent e) {
      if (e.newState == 8)
        Close();
    }
  }
}
