﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SIDLibraries.BusinessLayer;

namespace GUILibraries
{
    public partial class TxtLineAdjControl : TextBox
    {
        private bool _validKeyEntered;

        private String SelectedSportType;
        public String SelectedWagerType;
        private Form CallerFrm;

        public delegate void ControlTextChangedHandler(object sender, EventArgs e);
        public event EventHandler NewTextChanged;

        public TxtLineAdjControl(string selectedSportType, Form callerFrm)
        {
            SelectedSportType = selectedSportType;
            CallerFrm = callerFrm;
            InitializeComponent();
            this.Size = new Size(50, 25);
        }

        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                base.Text = value;
            }
        }

        private void txtLineAdj_KeyDown(object sender, KeyEventArgs e)
        {
            _validKeyEntered = (e.KeyCode == Keys.D0 || e.KeyCode == Keys.D1 || e.KeyCode == Keys.D2 || e.KeyCode == Keys.D3 || e.KeyCode == Keys.D4 || e.KeyCode == Keys.OemMinus || e.KeyCode == Keys.Oemplus
                                || e.KeyCode == Keys.D5 || e.KeyCode == Keys.D6 || e.KeyCode == Keys.D7 || e.KeyCode == Keys.D8 || e.KeyCode == Keys.D9 || e.KeyCode == Keys.OemPipe
                                || e.KeyCode == Keys.NumPad0 || e.KeyCode == Keys.NumPad1 || e.KeyCode == Keys.NumPad2 || e.KeyCode == Keys.NumPad3 || e.KeyCode == Keys.NumPad4 || e.KeyCode == Keys.NumPad5
                                || e.KeyCode == Keys.NumPad6 || e.KeyCode == Keys.NumPad7 || e.KeyCode == Keys.NumPad8 || e.KeyCode == Keys.NumPad9
                                || e.KeyCode == Keys.Add || e.KeyCode == Keys.Subtract || e.KeyCode == Keys.Divide || e.KeyCode == Keys.Decimal || e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete
                                || (SelectedSportType == SportTypes.SOCCER && e.KeyCode == Keys.Oemcomma)
                                );
        }

        private void txtLineAdj_KeyPress(object sender, KeyPressEventArgs e)
        {
            GUILibraries.Utilities.Common.HandleKeyPressEvent(sender, e, _validKeyEntered);
        }

        private void txtLineAdj_TextChanged(object sender, EventArgs e)
        {
            OnTextChanged(new ControlTextChangedEventArgs() { NewText = ((TextBox)sender).Text});
        }

        private void txtLineAdj_Leave(object sender, EventArgs e)
        {
            if (CallerFrm != null && CallerFrm.ActiveControl.Name == "btnCancel")
                return;
            var points = ((TextBox)sender).Text;
            var itemWagerType = "";
            var ie = "";
            var messageText = "";
            var allowAsianLine = SelectedSportType == SportTypes.SOCCER;

            switch (SelectedWagerType)
            {
                case "Totals":
                    itemWagerType = "Total Points";
                    if (allowAsianLine)
                    {
                        messageText = "must be a number with an optional half point or Asian line ";
                        ie = "2, 2½ or 2.25";
                    }
                    else
                    {
                        messageText = "must be a number with an optional half point ";
                        ie = "66½";
                    }

                    break;
                case "Spread":
                    itemWagerType = "Spread";
                    if (allowAsianLine)
                    {
                        messageText = "must be a signed number with an optional half point or Asian line ";
                        ie = "2, 2½ or 2.25";
                    }
                    else
                    {
                        messageText = "must be a signed number with an optional half point ";
                        ie = "66½";
                    }
                    ie = "-7½";
                    break;
                case "TeamTotalsOver":
                case "TeamTotalsUnder":
                    itemWagerType = "Team Total Points";
                    if (allowAsianLine)
                    {
                        messageText = "must be a number with an optional half point or Asian line ";
                        ie = "2, 2½ or 2.25";
                    }
                    else
                    {
                        messageText = "must be a number with an optional half point ";
                        ie = "66½";
                    }
                    ie = "33½";
                    break;
            }
            if (LinesFormat.IsValidLine(points, allowAsianLine)) return;
            MessageBox.Show(itemWagerType + @" " + messageText + "(i.e., \"" + ie + "\")");
            ((TextBox)sender).Focus();
        }

        public virtual void OnTextChanged(ControlTextChangedEventArgs e)
        {
            if (NewTextChanged != null)
            {
                NewTextChanged(this, e);
            }
        }
    }
}
