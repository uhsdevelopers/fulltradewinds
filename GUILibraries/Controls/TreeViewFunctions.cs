﻿using System.Windows.Forms;
using System.Runtime.InteropServices;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace GUILibraries.Controls {

  public static class TreeViewFunctions {

    [DllImport("user32.dll", CharSet = CharSet.Auto)]
    private static extern int GetScrollPos(int hWnd, int nBar);

    [DllImport("user32.dll")]
    private static extern int SetScrollPos(IntPtr hWnd, int nBar, int nPos, bool bRedraw);

    private const int SB_HORZ = 0x0;
    private const int SB_VERT = 0x1;

    public static TreeNode GetNodeFromPath(TreeNodeCollection nodes, string path, List<string> ignoreValues, string removeValue = "") {
      TreeNode foundNode = null;
      foreach (TreeNode tn in nodes) {
        var tPath = tn.FullPath;
        if (ignoreValues != null) {
          foreach (string str in ignoreValues) {
            tPath = tPath.Replace(string.IsNullOrEmpty(str) ? "[-NOVALUE-]" : str, "").Trim();
          }
        }
        if (!string.IsNullOrEmpty(removeValue)) {
          tPath = tPath.Replace(removeValue, "");
          path = path.Replace(removeValue, "");
        }
        if (tPath == path) {
          return tn;
        }
        if (tn.Nodes.Count > 0) {
          foundNode = GetNodeFromPath(tn.Nodes, path, ignoreValues, removeValue);
        }
        if (foundNode != null)
          return foundNode;
      }
      return null;
    }

    public static Point GetTreeViewScrollPos(TreeView treeView) {
      return new Point(
          GetScrollPos((int)treeView.Handle, SB_HORZ),
          GetScrollPos((int)treeView.Handle, SB_VERT));
    }

    public static void SetTreeViewScrollPos(TreeView treeView, Point scrollPosition) {
      if (treeView == null) return;
      SetScrollPos(treeView.Handle, SB_HORZ, scrollPosition.X, true);
      SetScrollPos(treeView.Handle, SB_VERT, scrollPosition.Y, true);
    }

  }
}
