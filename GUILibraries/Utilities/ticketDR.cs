﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Media;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using GUILibraries.Controls;
using InstanceManager.BusinessLayer;

namespace GUILibraries.Utilities {

  public class TicketDr : IDisposable {

    #region Public Properties

    public int TicketNumber {
      get;
      set;
    }

    public DateTime TicketPostedDateTime {
      get {
        return _ticketPostedDateTime;
      }
      set {
        _ticketPostedDateTime = value;
      }
    }

    public DateTime? ServerDateTime { get; set; }

    public String UtilitiesFolder { get; set; }

    #endregion

    #region Private Properties

    private static String LocalTmpFolder {
      get {
        if (String.IsNullOrEmpty(_localTmpFolder))
          return _localTmpFolder = Path.GetTempPath();
        return _localTmpFolder;
      }
    }

    #endregion

    #region Private vars

    private DateTime _ticketPostedDateTime;
    private static String _localTmpFolder;

    //private NAudio.Wave.WaveIn sourceStream = null;
    //private NAudio.Wave.WaveFileWriter waveWriter = null;

    private const String DECOMPRESSION_TOOL_FILE_NAME = "PKZIP25.EXE";

    private readonly ModuleInfo _moduleInfo;

    #endregion

    #region Constructors

    public TicketDr(ModuleInfo moduleInfo, int ticketNumber) {
      _moduleInfo = moduleInfo;
      TicketNumber = ticketNumber;
    }

    #endregion

    #region Public Methods

    private bool CompressionToolExists() {
      try {
        return File.Exists(UtilitiesFolder + DECOMPRESSION_TOOL_FILE_NAME);
      }
      catch (IOException) {
        return false;
      }
    }

    public void PrepareFileforPlayback() {
      var remotePath = GetAudioRecordingFilesPath();
      string audioRecordingFilesPath;
      var audioFileTargetFolder = "";
      const string fileDecompressionToolPath = "Utilities";
      var localTmpFolder = LocalTmpFolder;
      var fileName = "DR" + TicketNumber + ".zip";
      var fileFound = true;

      if (_ticketPostedDateTime.ToShortDateString() == DateTime.Now.ToShortDateString()) {
        audioFileTargetFolder = "DigitRecToday";
        audioRecordingFilesPath = remotePath + "\\" + audioFileTargetFolder;

        FileExists(audioRecordingFilesPath, fileName, localTmpFolder, audioFileTargetFolder, fileDecompressionToolPath, DECOMPRESSION_TOOL_FILE_NAME, 0, 0, out fileFound);
      }
      else {
        var datesToCheck = _ticketPostedDateTime.AddDays(-1);
        const int numberMaxOfDays = 3;

        for (var i = 0; i < numberMaxOfDays; i++) {
          var year = datesToCheck.Year.ToString(CultureInfo.InvariantCulture);
          var month = datesToCheck.Month.ToString(CultureInfo.InvariantCulture);
          var day = datesToCheck.Day.ToString(CultureInfo.InvariantCulture);

          if (datesToCheck.Month < 10)
            month = "0" + datesToCheck.Month;
          if (datesToCheck.Day < 10)
            day = "0" + datesToCheck.Day;
          audioFileTargetFolder = "DigitRec" + year + "-" + month + "-" + day;
          audioRecordingFilesPath = remotePath + "\\" + audioFileTargetFolder;

          if (FileExists(audioRecordingFilesPath, fileName, localTmpFolder, audioFileTargetFolder, fileDecompressionToolPath, DECOMPRESSION_TOOL_FILE_NAME, i, numberMaxOfDays - 1, out fileFound))
            break;
          datesToCheck = datesToCheck.AddDays(1);
        }
      }
      if (!fileFound)
        return;
      if (!DecompressAudioFileForPlayback(localTmpFolder + "\\" + audioFileTargetFolder + "\\" + DECOMPRESSION_TOOL_FILE_NAME,
                                          localTmpFolder + "\\" + audioFileTargetFolder + "\\" + fileName,
                                          localTmpFolder + "\\" + audioFileTargetFolder))
        return;
      var localFolder = localTmpFolder + "\\" + audioFileTargetFolder;
      var ticketToPlayback = localTmpFolder + "\\" + audioFileTargetFolder + "\\" + "DR" + TicketNumber + ".wav";
      var playBack = new FrmPlaybackConversation(localFolder, ticketToPlayback, _moduleInfo);
      playBack.ShowDialog();
      // MessageBox.Show("Recording File was not found.  Place it in " + GetStandardAudioTargetFolder() + " folder and try again.");
    }

    public void RecordAudio() {
      SoundRecorder.Wave.Mci.Close();
      SoundRecorder.Wave.Mci.Open();
      SoundRecorder.Wave.Mci.Record();
    }

    public void StopRecording() {
      SoundRecorder.Wave.Mci.Stop();

      var fileName = LocalTmpFolder + "DR" + TicketNumber + ".wav";

      SoundRecorder.Wave.Mci.SaveRecording(fileName);
      SoundRecorder.Wave.Mci.Close();
    }

    public void TransferAudioToRepository() {
      var fileName = "DR" + TicketNumber + ".wav";
      //LogWriter lg = new LogWriter("TicketWriter");

      if (!File.Exists(LocalTmpFolder + fileName))
        return;
      //lg.writeToCUAccessLog("TicketWriter", "Recording Audio", "Wav File exists locally: " + LocalTmpFolder + fileName);
      Boolean fileCompressed;
      var fileCopiedToRemLoc = false;
      CompressFile(LocalTmpFolder, fileName, TicketNumber.ToString(CultureInfo.InvariantCulture), out fileCompressed);

      //lg.writeToCUAccessLog("TicketWriter", "Recording Audio", "Was File compressed?: " + fileCompressed);

      var targeZipFName = LocalTmpFolder + "\\DR" + TicketNumber + ".zip";

      if (File.Exists(targeZipFName)) {
        //lg.writeToCUAccessLog("TicketWriter", "Recording Audio", "Was File compressed?: " + targeZipFName);
        CopyFiletoRemoteLocation(targeZipFName, TicketNumber.ToString(CultureInfo.InvariantCulture), out fileCopiedToRemLoc);
        //lg.writeToCUAccessLog("TicketWriter", "Recording Audio", "Was File Copied?: " + fileCopiedToRemLoc);
      }
      if (fileCopiedToRemLoc)
        DeleteLocalFiles();
      //lg.writeToCUAccessLog("TicketWriter", "Recording Audio", "File does not exist: " + LocalTmpFolder + fileName);
    }

    #endregion

    #region Private Methods

    private static bool FileExists(String audioRecordingFilesPath, String fileName, String localTmpFolder, String audioFileTargetFolder, String fileDecompressionToolPath, String decompressionToolFileName, int iterationNum, int maxIterations, out bool fileFound) {
      fileFound = true;
      var showMessage = !(iterationNum < maxIterations);

      if (Directory.Exists(audioRecordingFilesPath)) {
        if (File.Exists(audioRecordingFilesPath + "\\" + fileName)) {
          if (Directory.Exists(localTmpFolder + "\\" + audioFileTargetFolder)) {
            try {
              DeleteDirectoryAndContents(localTmpFolder + "\\" + audioFileTargetFolder);
              Directory.CreateDirectory(localTmpFolder + "\\" + audioFileTargetFolder);
            }
            catch (IOException ex) {
              if (showMessage)
                MessageBox.Show(@"Unable to prepare temp local folder: " + ex.Message);
              return fileFound = false;
            }
            if (!File.Exists(localTmpFolder + "\\" + audioFileTargetFolder + "\\" + fileName)) {
              try {
                File.Copy(audioRecordingFilesPath + "\\" + fileName, localTmpFolder + "\\" + audioFileTargetFolder + "\\" + fileName);
              }
              catch (IOException ex) {
                if (showMessage)
                  MessageBox.Show(@"Unable to copy zip file to temp local folder: " + ex.Message);
                return fileFound = false;
              }
            }
            if (File.Exists(fileDecompressionToolPath + "\\" + decompressionToolFileName)) {
              try {
                if (!File.Exists(localTmpFolder + "\\" + audioFileTargetFolder + "\\" + decompressionToolFileName))
                  File.Copy(fileDecompressionToolPath + "\\" + decompressionToolFileName, localTmpFolder + "\\" + audioFileTargetFolder + "\\" + decompressionToolFileName);
              }
              catch (IOException ex) {
                if (showMessage)
                  MessageBox.Show(@"Unable to copy decompression files application to temp local folder: " + ex.Message);
                return fileFound = false;
              }
            }
            else {
              if (showMessage)
                MessageBox.Show(@"Decompression file application was not found. ");
              return fileFound = false;
            }
          }
          else {
            try {
              Directory.CreateDirectory(localTmpFolder + "\\" + audioFileTargetFolder);

              try {
                File.Copy(audioRecordingFilesPath + "\\" + fileName, localTmpFolder + "\\" + audioFileTargetFolder + "\\" + fileName);

                if (File.Exists(fileDecompressionToolPath + "\\" + decompressionToolFileName)) {
                  try {
                    File.Copy(fileDecompressionToolPath + "\\" + decompressionToolFileName, localTmpFolder + "\\" + audioFileTargetFolder + "\\" + decompressionToolFileName);
                  }
                  catch (IOException ex) {
                    if (showMessage)
                      MessageBox.Show(@"Unable to copy decompression files application to temp folder: " + ex.Message);
                    return fileFound = false;
                  }
                }
                else if (showMessage) {
                  MessageBox.Show(@"Decompression file application was not found. ");
                  return fileFound = false;
                }

              }
              catch (IOException ex) {
                if (showMessage)
                  MessageBox.Show(@"Unable to copy file to temp folder: " + ex.Message);
                return fileFound = false;
              }
            }
            catch (IOException ex) {
              if (showMessage)
                MessageBox.Show(@"Unable to create temp folder: " + ex.Message);
              return fileFound = false;
            }
          }
        }
        else {
          if (showMessage)
            MessageBox.Show(@"Unable to Find Audio Recordings File in folder.");
          return fileFound = false;
        }
      }
      else {
        if (showMessage)
          MessageBox.Show(@"Unable to Find Audio Recordings Folder.");
        return fileFound = false;
      }
      return true;
    }

    public static void DeleteDirectoryAndContents(string targetDir) {
      var files = Directory.GetFiles(targetDir);
      var dirs = Directory.GetDirectories(targetDir);

      foreach (var file in files) {
        File.SetAttributes(file, FileAttributes.Normal);
        File.Delete(file);
      }
      foreach (var dir in dirs)
        DeleteDirectoryAndContents(dir);
      Directory.Delete(targetDir, true);
    }

    private static Boolean DecompressAudioFileForPlayback(String decompressionAppName, String fileToDecompress, String decompressedFilePath) {
      var fileDecompressed = true;

      try {
        if (File.Exists(decompressedFilePath + "\\decompressFile.bat"))
          File.Delete(decompressedFilePath + "\\decompressFile.bat");
        try {
          var dFile = File.Create(decompressedFilePath + "\\decompressFile.bat");
          dFile.Close();
        }
        catch (IOException ex) {
          MessageBox.Show(@"Unable to create temp batch File: " + ex.Message);
          fileDecompressed = false;
        }
        try {
          TextWriter tw = new StreamWriter(decompressedFilePath + "\\decompressFile.bat");
          tw.WriteLine("\"" + decompressionAppName + "\" -extract \"" + fileToDecompress + "\" \"" + decompressedFilePath + "\"");
          tw.Close();
        }
        catch (IOException ex) {
          MessageBox.Show(@"Unable to write to temp batch File: " + ex.Message);
          fileDecompressed = false;
        }
        var startInfo = new ProcessStartInfo {
          FileName = decompressedFilePath + "\\decompressFile.bat",
          WindowStyle = ProcessWindowStyle.Hidden
        };
        var process = new Process { StartInfo = startInfo };
        process.Start();

        var c = 100;

        while (!process.HasExited && c > 0) {
          Thread.Sleep(500);
          c--;
        }
      }
      catch (FileNotFoundException ex) {
        MessageBox.Show(@"Unable to start Decompression Application tool: " + ex.Message);
        fileDecompressed = false;
      }
      catch (InvalidOperationException ex) {
        MessageBox.Show(@"Unable to start Decompression Application tool: " + ex.Message);
        fileDecompressed = false;
      }
      return fileDecompressed;
    }

    private String GetAudioRecordingFilesPath() {
      return _moduleInfo.Parameters.AudioDirectory;
    }

    protected virtual bool IsFileLocked(FileInfo file) {
      FileStream stream = null;

      try {
        stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
      }
      catch (IOException) {
        //the file is unavailable because it is:
        //still being written to
        //or being processed by another thread
        //or does not exist (has already been processed)
        return true;
      }
      finally {
        if (stream != null)
          stream.Close();
      }

      //file is not locked
      return false;
    }

    private void DeleteLocalFiles() {
      var wavFileFullPath = LocalTmpFolder + "\\DR" + TicketNumber + ".wav";
      var zipFileFullPath = LocalTmpFolder + "\\DR" + TicketNumber + ".zip";
      var file = new FileInfo(wavFileFullPath);
      if (!IsFileLocked(file)) {
        File.Delete(wavFileFullPath);
      }

      file = new FileInfo(zipFileFullPath);
      if (!IsFileLocked(file)) {
        File.Delete(zipFileFullPath);
      }
    }

    public void CheckForOldLocalDrFiles() {
      try {
        var files = Directory.GetFiles(LocalTmpFolder);
        var remotePath = GetAudioRecordingFilesPath();

        foreach (var mFile in files) {
          var fExtension = Path.GetExtension(mFile);
          var fName = Path.GetFileNameWithoutExtension(mFile);
          if (fName == null || (!fName.StartsWith("DR") || fExtension != ".zip")) continue;
          var fLastModified = File.GetLastWriteTime(mFile);
          var targetDateTime = DateTime.Now.Date;
          if (ServerDateTime != null)
            targetDateTime = (DateTime)ServerDateTime;
          string audioFileTargetFolder;
          if (fLastModified.Date == targetDateTime.Date) {
            audioFileTargetFolder = "DigitRecToday";
          }
          else {
            audioFileTargetFolder = "DigitRec" + fLastModified.Year + "-" + (fLastModified.Month < 10 ? "0" + fLastModified.Month : fLastModified.Month.ToString(CultureInfo.InvariantCulture)) + "-" + (fLastModified.Day < 10 ? "0" + fLastModified.Day : fLastModified.Day.ToString(CultureInfo.InvariantCulture));
          }

          if (!Directory.Exists(remotePath + "\\" + audioFileTargetFolder)) {
            try {
              Directory.CreateDirectory(remotePath + "\\" + audioFileTargetFolder);
            }
            catch (IOException) {
              audioFileTargetFolder = "DigitRecToday";
              //throw ex;
            }
          }

          var wavFileFullPath = LocalTmpFolder + "\\" + fName + ".wav";
          var zipFileFullPath = LocalTmpFolder + "\\" + fName + ".zip";
          if (!File.Exists(remotePath + "\\" + audioFileTargetFolder + "\\" + fName + ".zip")) {
            File.Move(zipFileFullPath, remotePath + "\\" + audioFileTargetFolder + "\\" + fName + ".zip");
          }

          if (File.Exists(wavFileFullPath))
            File.Delete(wavFileFullPath);
          if (File.Exists(zipFileFullPath))
            File.Delete(zipFileFullPath);
        }
      }
      catch (IOException) {
        //throw ex;
      }
    }

    private void CompressFile(string localTmpFolder, string fileName, string ticketNumber, out Boolean fileWasCompressed) {
      fileWasCompressed = false;
      var fileTocompress = localTmpFolder + "\\" + fileName;
      var targeZipFName = localTmpFolder + "\\DR" + ticketNumber + ".zip";
      // LogWriter lg = new LogWriter("TicketWriter");

      if (!File.Exists(localTmpFolder + "\\" + DECOMPRESSION_TOOL_FILE_NAME)) {
        if (CompressionToolExists()) {
          try {
            File.Copy(UtilitiesFolder + DECOMPRESSION_TOOL_FILE_NAME, localTmpFolder + "\\" + DECOMPRESSION_TOOL_FILE_NAME);
          }
          catch (IOException) {
          }
        }
        else {
          MessageBox.Show(@"No compression tool found");
          return;
        }
      }
      if (File.Exists(localTmpFolder + "\\compressFile.bat")) {
        try {
          File.Delete(localTmpFolder + "\\compressFile.bat");
        }
        catch (IOException) {
        }
      }
      try {
        var dFile = File.Create(localTmpFolder + "\\compressFile.bat");
        dFile.Close();
      }
      catch (IOException ex) {
        MessageBox.Show(@"Unable to create temp batch File: " + ex.Message);
      }
      try {
        TextWriter tw = new StreamWriter(localTmpFolder + "\\compressFile.bat");
        tw.WriteLine("\"" + localTmpFolder + "\\" + DECOMPRESSION_TOOL_FILE_NAME + "\" -add \"" + targeZipFName + "\" \"" + fileTocompress + "\"");
        tw.Close();
      }
      catch (IOException ex) {
        MessageBox.Show(@"Unable to write to temp batch File: " + ex.Message);
      }
      var startInfo = new ProcessStartInfo {
        FileName = localTmpFolder + "\\compressFile.bat",
        WindowStyle = ProcessWindowStyle.Hidden
      };
      var process = new Process { StartInfo = startInfo };
      process.Start();
      var c = 100;

      while (!process.HasExited && c > 0) {
        Thread.Sleep(500);
        c--;
      }
      if (File.Exists(targeZipFName))
        fileWasCompressed = true;
    }

    private void CopyFiletoRemoteLocation(String targeZipFName, String fileName, out Boolean fileWasCopied) {
      var remotePath = GetAudioRecordingFilesPath();
      const string audioFileTargetFolder = "DigitRecToday";
      fileWasCopied = false;

      if (!Directory.Exists(remotePath + "\\" + audioFileTargetFolder))
        return;
      try {
        File.Copy(targeZipFName, remotePath + "\\" + audioFileTargetFolder + "\\DR" + fileName + ".zip");
      }
      catch (IOException ex) {
        MessageBox.Show(@"Unable to copy zip file to temp local folder: " + ex.Message);
      }
      fileWasCopied = true;
    }

    #endregion

    public class Ar {
      public enum Status { Recording = 1, Stopped = 2 }
      [DllImport("winmm.dll", EntryPoint = "mciSendStringA", CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
      private static extern int mciSendString(string lpstrCommand, string lpstrReturnString, int uReturnLength, int hwndCallback);

      #region Public Properties

      public Status StatusCode { set; get; }

      public string FileName { set; get; }

      #endregion

      #region Constructors

      public Ar(string fileName) {
        FileName = LocalTmpFolder + "DR" + fileName + ".wav";
        StatusCode = Status.Stopped;
      }

      #endregion

      #region Public Methods

      public void RecordAudio() {
        mciSendString("Open new Type waveaudio Alias recsound", "", 0, 0);
        mciSendString("Record recsound", "", 0, 0);
        StatusCode = Status.Recording;
      }

      public void StopRecording() {
        //LogWriter lg = new LogWriter("TicketWriter");
        //lg.writeToCUAccessLog("TicketWriter", "StopRecord", "Start: " + FileName);
        var adjustedPath = "\"" + FileName + "\"";
        mciSendString("Save recsound " + adjustedPath, "", 0, 0);
        mciSendString("Close recsound ", "", 0, 0);
        StatusCode = Status.Stopped;
        //lg.writeToCUAccessLog("TicketWriter", "StopRecord", "Stop: " + FileName);
      }

      public void PlayRecording() {
        if (!File.Exists(FileName))
          return;
        var player = new SoundPlayer { SoundLocation = FileName };
        player.Play();
      }

      #endregion

      #region Events

      #endregion

    }


    public void Dispose() {
    }
  }
}
