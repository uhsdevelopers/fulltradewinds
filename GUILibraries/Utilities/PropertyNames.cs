﻿namespace GUILibraries.Utilities
{
    public static class PropertyNames
    {
        public const string CustomerCurrentBalance = "CustomerCurrentBalance";
        public const string CustomerFreePlayBalance = "CustomerFreePlayBalance";
        public const string CustomerFreePlayPendingCount = "CustomerFreePlayPendingCount";
        public const string CustomerFreePlayPendingBalance = "CustomerFreePlayPendingBalance";
        public const string CustomerPendingWagerCount = "CustomerPendingWagerCount";
        public const string CustomerPendingWagerBalance = "CustomerPendingWagerBalance";
    }
}
