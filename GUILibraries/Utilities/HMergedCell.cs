﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace GUILibraries.Utilities {
  public class HMergedCell : DataGridViewTextBoxCell {
    private int _mNLeftColumn;
    private int _mNRightColumn;

    /// <summary>
    /// Column Index of the left-most cell to be merged.
    /// This cell controls the merged text.
    /// </summary>
    public int LeftColumn {
      get {
        return _mNLeftColumn;
      }
      set {
        _mNLeftColumn = value;
      }
    }

    /// <summary>
    /// Column Index of the right-most cell to be merged
    /// </summary>
    public int RightColumn {
      get {
        return _mNRightColumn;
      }
      set {
        _mNRightColumn = value;
      }
    }

    protected override void Paint(Graphics graphics, Rectangle clipBounds, Rectangle cellBounds, int rowIndex, DataGridViewElementStates cellState, object value, object formattedValue, string errorText, DataGridViewCellStyle cellStyle, DataGridViewAdvancedBorderStyle advancedBorderStyle, DataGridViewPaintParts paintParts) {
      try {
        int i;

        // Draw the background
        graphics.FillRectangle(new SolidBrush(SystemColors.Control), cellBounds);

        // Draw the separator for rows
        graphics.DrawLine(new Pen(new SolidBrush(SystemColors.ControlDark)), cellBounds.Left, cellBounds.Bottom - 1, cellBounds.Right, cellBounds.Bottom - 1);

        // Draw the right vertical line for the cell
        if (ColumnIndex == _mNRightColumn)
          graphics.DrawLine(new Pen(new SolidBrush(SystemColors.ControlDark)), cellBounds.Right - 1, cellBounds.Top, cellBounds.Right - 1, cellBounds.Bottom);

        // Draw the text
        var sf = new StringFormat { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center, Trimming = StringTrimming.EllipsisCharacter };

        // Determine the total width of the merged cell
        int nWidth = 0;
        for (i = _mNLeftColumn; i <= _mNRightColumn; i++)
          nWidth += OwningRow.Cells[i].Size.Width;

        // Determine the width before the current cell.
        int nWidthLeft = 0;
        for (i = _mNLeftColumn; i < ColumnIndex; i++)
          nWidthLeft += OwningRow.Cells[i].Size.Width;

        // Retrieve the text to be displayed
        string strText = OwningRow.Cells[_mNLeftColumn].Value.ToString();

        var rectDest = new RectangleF(cellBounds.Left - nWidthLeft, cellBounds.Top, nWidth, cellBounds.Height);
        graphics.DrawString(strText, new Font("Arial", 10, FontStyle.Regular), Brushes.Black, rectDest, sf);
      }
      catch (Exception ex) {
        Trace.WriteLine(ex.ToString());
      }
    }

  }// class

}
