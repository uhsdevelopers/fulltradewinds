﻿using System.Windows.Forms;
using SIDLibraries.BusinessLayer;

namespace GUILibraries.Utilities {
  public static class Common {

      public static void HandleKeyPressEvent(object sender, KeyPressEventArgs e, bool _validKeyEntered)
      {
          if (_validKeyEntered)
          {
              if (e.KeyChar != (char)8)
              {
                  if (((TextBox)sender).SelectionStart == ((TextBox)sender).Text.Length)
                  {
                      bool handled;
                      ((TextBox)sender).Text = LinesFormat.ManualModePointsKeyPress(((TextBox)sender).Text, ((TextBox)sender).SelectedText.Length, e.KeyChar, true, out handled);
                      e.Handled = handled;
                      ((TextBox)sender).HideSelection = true;
                      ((TextBox)sender).Select(((TextBox)sender).Text.Length, 0);
                  }
                  else
                  {
                      if (e.KeyChar == (char)43 || e.KeyChar == (char)45 || e.KeyChar == (char)47)
                      {
                          if ((e.KeyChar == (char)43 || e.KeyChar == (char)45) && ((TextBox)sender).SelectionStart == 0)
                          {
                          }
                          else
                          {
                              ((TextBox)sender).HideSelection = true;
                              ((TextBox)sender).Select(((TextBox)sender).Text.Length, 0);
                          }
                      }
                  }
              }
          }
          else
          {
              e.Handled = true;
          }
      }
  }
}
