﻿namespace GUILibraries.Utilities
{
    public static class MethodNames
    {
        public const string PopulateCustomerFreePlaysTransactionsGv = "populateCustomerFreePlaysTransactionsGV";
        public const string PopulateCustomerTransactionsGv = "populateCustomerTransactionsGV";
    }
}
