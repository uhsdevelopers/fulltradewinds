﻿using System;
using System.Data;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Collections.Generic;
using System.Windows.Forms;

namespace GUILibraries.Utilities {
  public static class FormF {

    #region Public Methods

    public static Icon GetExecutableIcon() {
      IntPtr large;
      IntPtr small;
      ExtractIconEx(Application.ExecutablePath, 0, out large, out small, 1);
      return Icon.FromHandle(small);
    }

    [DllImport("Shell32")]
    public static extern int ExtractIconEx(
        string sFile,
        int iIndex,
        out IntPtr piLargeVersion,
        out IntPtr piSmallVersion,
        int amountIcons);

    public static void CenterNonResizable(Form frm)
    {
        frm.StartPosition = FormStartPosition.CenterScreen;
        frm.MaximumSize = frm.Size;
        frm.MinimumSize = frm.Size;
        frm.MaximizeBox = false;
        frm.Icon = GetExecutableIcon();
    }

    public static void ValidateNumericKey(ref KeyPressEventArgs e)
    {
        if (Char.IsLetter(e.KeyChar))
            e.Handled = true;
        else if (Char.IsSymbol(e.KeyChar))
            e.Handled = true;
        else if (Char.IsSeparator(e.KeyChar))
            e.Handled = true;
        else if (char.IsSymbol(e.KeyChar))
            e.Handled = true;
        else
            e.Handled = false;
    }

    public static void ValidateNonNumericKey(ref KeyPressEventArgs e)
    {
        if (Char.IsLetter(e.KeyChar))
            e.Handled = false;
        else if (Char.IsControl(e.KeyChar))
            e.Handled = false;
        else if (Char.IsSeparator(e.KeyChar))
            e.Handled = false;
        else
            e.Handled = true;
    }

    public static void DataGridViewDisableSort(DataGridView dgv) {
      foreach (DataGridViewColumn dc in dgv.Columns)
        dc.SortMode = DataGridViewColumnSortMode.NotSortable;
    }

    public static void ClearDataGridView(DataGridView dgv, bool clearDataSource, bool removeColumns){
      var allowUserToAddRows = dgv.AllowUserToAddRows;
      dgv.AllowUserToAddRows = false;

      if (clearDataSource) {
        dgv.DataSource = null;
        dgv.Rows.Clear();
      }
      else ((DataTable)dgv.DataSource).Rows.Clear();

      for (int i = dgv.Rows.Count - 1; i >= 0; i-- ) {
        dgv.Rows.RemoveAt(i);
      }
      if (removeColumns) {
        for (var i = dgv.Columns.Count; i > 0; i--) {
          dgv.Columns.Remove(dgv.Columns[i - 1]);
        }
      }
      dgv.AllowUserToAddRows = allowUserToAddRows;
    }

    public static bool IsFormActive(string formName) {
      FormCollection fc = Application.OpenForms;
      foreach (Form frm in fc) {
        if (frm.Name == formName) {
          frm.Focus();
          return true;
        }
        //iterate through
      }
      return false;
    }

    public static void PreventDataGridViewColumnsToBeSortedAndResized(DataGridView dgvw)
    {
        dgvw.AllowUserToResizeColumns = false;
        dgvw.AllowUserToResizeRows = false;

        foreach (DataGridViewColumn column in dgvw.Columns)
        {
            column.SortMode = DataGridViewColumnSortMode.NotSortable;
        }
    }

    public static string GetSelectedItemInDropDownFromPanel(Form containingFrm, string objName)
    {
        var selectedItem = "";
        var frm = containingFrm;

        try
        {
            Control[] cbxs = frm.Controls.Find(objName, true);
            var pan = (Panel)cbxs[0];
            ComboBox cbx = null;
            if (pan.Controls.Count > 0)
                cbx = (ComboBox)pan.Controls[0];
            if (cbx != null)
            {
                var selectedIdx = cbx.SelectedIndex;
                selectedItem = selectedIdx > -1 ? cbx.SelectedItem.ToString() : cbx.Text;
            }
        }
        catch (Exception)
        {
            selectedItem = "Obj_Not_Loaded";
        }
        return selectedItem;
    }

    public static Form GetFormByName(string formName, Form startingForm)
    {
        return GetFormByName(formName, startingForm, false);
    }

    public static Form GetFormByName(string formName, Form startingForm, bool isRootForm)
    {
        if (!isRootForm)
        {
            Form parentForm = startingForm.MdiParent ?? startingForm.Owner;

            if (parentForm != null)
            {
                return GetFormByName(formName, parentForm);
            }
            return GetFormByName(formName, startingForm, true);
        }
        if (startingForm.Name.ToLower() == formName.ToLower())
        {
            return startingForm;
        }
        var childForms = new List<Form>();
        childForms.AddRange(startingForm.MdiChildren);
        childForms.AddRange(startingForm.OwnedForms);

        foreach (Form childForm in childForms)
        {
            if (childForm.Name.ToLower() == formName.ToLower())
            {
                return childForm;
            }
            Form targetForm = GetFormByName(formName, childForm, true);

            if (targetForm != null)
            {
                return targetForm;
            }
        }
        return null;
    }

    public static bool IsLowResolution() {
      var screenWidth = Screen.PrimaryScreen.Bounds.Width;
      return screenWidth < 1024;
    }

    #endregion

  }
}
