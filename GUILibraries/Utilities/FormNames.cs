﻿namespace GUILibraries.Utilities
{
    public static class FormNames
    {
        public const string FrmCustomerTransactions = "FrmCustomerTransactions";
        public const string MdiTicketWriter = "MdiTicketWriter";
    }
}
