﻿namespace GUILibraries.Utilities
{
    public static class ControlNames
    {
        public const string ChbRollingIf = "chbRollingIF";
        public const string CmbDisplay = "cmbDisplay";
        public const string CmbFreePlayDisplay = "cmbFreePlayDisplay";
        public const string TxtAvailableBalance = "txtAvailableBalance";
        public const string TxtFreePlayBalance = "txtFreePlayBalance";
        public const string TxtFreePlayPendingAmount = "txtFreePlayPendingAmount";
        public const string TxtFreePlayPendingCount = "txtFreePlayPendingCount";
        public const string TxtPendingBets = "txtPendingBets";
    }
}
