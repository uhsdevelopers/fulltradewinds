﻿namespace GUILibraries.Utilities
{
    public static class ColumnNames
    {
        public const string Allow = "Allow";
        public const string Amount = "Amount";
        public const string AmountLost = "AmountLost";
        public const string AmountWon = "AmountWon";

        public const string BaseOfferingFlag = "BaseOfferingFlag";

        public const string CallUnitOnlyFlag = "CallUnitOnlyFlag";
        public const string ChosenTeamId = "ChosenTeamId";
        public const string ChkPremiumFlag = "chkPremiumFlag";
        public const string ComplexBetLink = "complexBetLink";
        public const string ContestType2 = "ContestType2";
        public const string CuMaxBet = "cuMaxBet";

        public const string DailyFigureDate = "Daily Figure Date";
        public const string Description = "Description";
        public const string DfltCircled = "dfltCircled";
        public const string DiffCents = "DiffCents";
        public const string DocNumber = "docNumber";

        public const string EndingPrice = "EndingPrice";

        public const string GameContestNum = "GameContestNum";
        public const string GamesPicked = "GamesPicked";
        public const string GamesWon = "GamesWon";
        public const string GameTime = "Game Time";

        public const string HiddenContestType = "HiddenContestType";

        public const string InetMaxBet = "inetMaxBet";
        public const string InternetOnlyFlag = "InternetOnlyFlag";
        public const string IsFreePlay = "IsFreePlay";
        public const string IsRoot = "IsRoot";
        public const string ItemIsContest = "ItemIsContest";
        public const string ItemWagerType = "ItemWagerType";

        public const string LineType = "LineType";

        public const string MaxPayout = "maxPayout";
        public const string MaxPicks = "MaxPicks";
        public const string MinPicks = "MinPicks";
        public const string MoneyLine = "MoneyLine";

        public const string Number = "Number";

        public const string Outcome = "Outcome";

        public const string Paid = "Paid";
        public const string Pays = "pays";
        public const string Pays2 = "Pays";
        public const string Period = "Period";
        public const string PeriodDesc = "periodDesc";
        public const string PeriodDescription = "PeriodDescription";
        public const string PeriodNumber = "PeriodNumber";
        public const string PeriodNumber2 = "periodNumber";
        public const string Picks = "picks";
        public const string PlayNumber = "PlayNumber";
        public const string PostedDateTime = "Posted Date/Time";
        public const string PostedDateTime2 = "Posted Date/Time2";
        public const string PremiumFlag = "PremiumFlag";

        public const string Risk = "Risk";

        public const string SportSubType = "sportSubType";
        public const string SportSubTypeId = "SportSubTypeId";
        public const string SportType = "sportType";
        public const string StartingPrice = "StartingPrice";
        public const string Status = "Status";
        public const string SuspendFlag = "SuspendFlag";

        public const string Target = "Target";
        public const string Team1Id = "Team1ID";
        public const string Team2Id = "Team2ID";
        public const string TeaserName = "TeaserName";
        public const string TeaserTies = "TeaserTies";
        public const string TicketNumber = "TicketNumber";
        public const string TicketNumber2 = "ticket #";
        public const string TicketPostedDateTime = "ticketPostedDateTime";
        public const string TicketWager = "Ticket Wager";
        public const string ToBase = "ToBase";
        public const string ToWin = "To Win";
        public const string Type = "Type";

        public const string Wager = "Wager";
        public const string WagerNumber = "WagerNumber";
        public const string WagerType = "wagerType";
        public const string WagerType2 = "WagerType";
        public const string WonLost = "Won/-Lost";
        public const string WrittenBy = "written By";
        public const string InetSessionId = "InetSessionId";
    }
}
