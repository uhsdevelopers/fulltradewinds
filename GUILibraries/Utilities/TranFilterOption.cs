﻿namespace GUILibraries.Utilities
{
    public static class TranFilterOption
    {
        public const string All = "All";
        public const string AvailableForRollingIfs = "Available for Rolling Ifs";        
        public const string Last2Days = "Last 2 Days";
        public const string Last7Days = "Last 7 Days";
        public const string Last14Days = "Last 14 Days";
        public const string Last30Days = "Last 30 Days";
        public const string OpenParlaysTeasersOnly = "Open Parlays/Teasers Only";
        public const string PendingOnly = "Pending Only";
        public const string PendingOpenWagersOnly = "Pending/Open Wagers Only";
        public const string SpecificWager = "Specific Wager";
        public const string SpecificTicket = "Specific Ticket";
        public const string WithOpenParlaysTeasers = "With Open Parlays/Teasers";
        public const string WithPendingOpenWagers = "With Pending/Open Wagers";
        
    }
}
