﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;
using GUILibraries.BusinessLayer;
using InstanceManager;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;

namespace GUILibraries.Utilities {

  // State object for reading client data asynchronously
  public class StateObject {
    // Client  socket.
    public Socket WorkSocket = null;
    // Size of receive buffer.
    public const int BufferSize = 1024;
    // Receive buffer.
    public byte[] Buffer = new byte[BufferSize];
    // Received data string.
    public StringBuilder Sb = new StringBuilder();
  }

  public class SocketManager : IDisposable {

    #region Public Contants

    public const int BASEMAINTPORT = 11000;

    #endregion

    #region Public Enums

    public enum Mode { Listener, Sender }

    #endregion

    #region Private Vars

    private Socket _socket;
    private readonly ModuleInfo _mi;

    #endregion

    #region Public Properties

    public string MessageText { get; set; }
    public int MessagePriority { get; set; }
    public int MessageShownFrequency { get; set; }
    public int MessageShownMaxCount { get; set; }

    #endregion

    #region Constructors

    public SocketManager(Mode type, ModuleInfo mi) {
      _mi = mi;

      if (type != Mode.Listener || _mi.FlagForMaintenance)
        return;
      var t = new Thread(() => StartListening(_mi.Id)) { IsBackground = true };
      t.Start();
    }

    #endregion

    #region Public Methods

    public void Dispose() {
      if (_socket != null)
        _socket.Close();
    }

    public void SendMaintenanceMessage(int listeningPort) {
      try {
        _socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        var iep1 = new IPEndPoint(IPAddress.Broadcast, listeningPort);
        var data = Encoding.ASCII.GetBytes("{\"Header\":\"KillApp\", \"DisplayMessageFreqMin\":\"" + MessageShownFrequency + "\",\"ShowMessageMaxCount\":\"" + MessageShownMaxCount + "\",\"MaintenanceMesssage\":\"" + MessageText + "\" , \"MessagePriority\":\"" + MessagePriority + "\"}");
        _socket.EnableBroadcast = true;
        _socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
        _socket.SendTo(data, iep1);
      }
      catch (SocketException e) {
        CConsole.WriteLine(e);
      }
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// JSON Deserialization
    /// </summary>
    public static T JsonDeserialize<T>(string jsonString) {
      var ser = new DataContractJsonSerializer(typeof(T));
      var ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
      var obj = (T)ser.ReadObject(ms);
      return obj;
    }

    private void StartListening(int id) {
      try {
        var listener = new UdpClient(BASEMAINTPORT + id);
        var groupEp = new IPEndPoint(IPAddress.Any, BASEMAINTPORT + id);
        var done = false;

        while (!done) {
          //allDone.Reset();
          var receiveByteArray = listener.Receive(ref groupEp);
          var receivedData = Encoding.ASCII.GetString(receiveByteArray, 0, receiveByteArray.Length);
          var retObj = JsonDeserialize<MaintenanceMessage>(receivedData);

          if (retObj == null || retObj.Header != "KillApp")
            continue;
          _mi.FlagForMaintenance = true;
          _mi.MaintMessage.DisplayMessageFreqMin = retObj.DisplayMessageFreqMin;
          _mi.MaintMessage.MaintenanceMesssage = retObj.MaintenanceMesssage;
          _mi.MaintMessage.MessagePriority = retObj.MessagePriority;
          _mi.MaintMessage.ShowMessageMaxCount = retObj.ShowMessageMaxCount;
          var mm = new ModuleInMaintenance(_mi);
          mm.ShowModuleInMaintenanceMsg();

          done = true;
          //allDone.WaitOne();
        }

      }
      catch (Exception ex) {
        throw ex;
      }
    }

    #endregion

  }
}