﻿using System;
using System.Linq;
using System.Windows.Forms;
using GUILibraries.Controls;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using SIDLibraries.Utilities;
using System.Globalization;
using TicketWriter.Properties;
using TicketWriter.UI;

namespace TicketWriter.Utilities {
  public static class Common {

    internal static void WriteCaptionOnAcceptButton(string currentWagerTypeName, String bntText, Button btnPlaceBet, Button btnAddToParlay, Button btnAddToTeaser, Button btnAddToIfBet) {
      switch (currentWagerTypeName) {
        case "Straight Bet":
          btnPlaceBet.Text = bntText;
          break;
        case "Parlay":
          btnAddToParlay.Text = bntText;
          break;
        case "Teaser":
          btnAddToTeaser.Text = bntText;
          break;
        case "If-Bet":
          btnAddToIfBet.Text = bntText;
          break;
      }
    }

    internal static string CalculateTotalSpec(string selectedPrice, int adjTotalOver, double newAdjTotal, int? decimalPrecision, int? fractionalMaxDenominator) {
      var newTotalSpec = "";
      switch (selectedPrice) {
        case "American":
        case LineOffering.PRICE_AMERICAN:
          if (adjTotalOver > 0)
            newTotalSpec += "+" + newAdjTotal.ToString(CultureInfo.InvariantCulture);
          else
            newTotalSpec += newAdjTotal.ToString(CultureInfo.InvariantCulture);
          break;
        case "Decimal":
        case LineOffering.PRICE_DECIMAL:
          newTotalSpec += OddsTypes.UStoDecimal(newAdjTotal, decimalPrecision);
          break;
        case "Fractional":
        case LineOffering.PRICE_FRACTIONAL:
          var newSpreadDec = OddsTypes.UStoDecimal(newAdjTotal, decimalPrecision);
          if (fractionalMaxDenominator != null) {
            var fractionAry = OddsTypes.DecToFraction(double.Parse(newSpreadDec.ToString(CultureInfo.InvariantCulture)),
              (int)fractionalMaxDenominator);
            newTotalSpec += fractionAry.Numerator + "/" + fractionAry.Denominator;
          }
          break;
      }
      return newTotalSpec;
    }

    internal static bool IsValidWager(NumberTextBox txtRisk, NumberTextBox txtToWin, GroupBox grpRiskAmount, GroupBox grpWinAmount, MdiTicketWriter mdi, int ticketNumberToUpd, int wagerNumberToUpd, string frmMode, string currentCustomerCurrency) {
      var isValidWager = true;
      var msgShown = false;

      if (grpRiskAmount.Visible && grpWinAmount.Visible && (txtRisk.Text.Length == 0 || txtToWin.Text.Length == 0 || txtRisk.Text == @"0" || txtToWin.Text == @"0")) {
        MessageBox.Show(Resources.FrmMakeAWager_IsValidWager_A_wager_amount_must_be_entered_, Resources.FrmMakeAWager_IsValidWager_Invalid_Amount);
        isValidWager = false;
        msgShown = true;
      }

      if (!mdi.RollingIfBetMode || msgShown) return isValidWager;
      double? rifMaxRiskAmount = null;
      if (frmMode == "Edit") {
        var wager = (from p in mdi.Ticket.Wagers where p.TicketNumber == ticketNumberToUpd && p.WagerNumber == wagerNumberToUpd select p).FirstOrDefault();
        if (wager != null) rifMaxRiskAmount = wager.RifMaxReinvestAmt;
      }
      else {
        rifMaxRiskAmount = mdi.RifBTicketInfo.RifRiskAmount + mdi.RifBTicketInfo.RifToWinAmount;
      }

      if (!(double.Parse(txtRisk.Text) > rifMaxRiskAmount)) return true;
      MessageBox.Show(Resources.FrmMakeAWager_IsValidWager_Maximum_rolling_if_bet_available_to_reinvest__ + rifMaxRiskAmount + Environment.NewLine + Resources.FrmMakeAWager_IsValidWager_Insufficient_funds_available_for_this_if_bet_risk_of_ + txtRisk.Text + Resources.FrmMakeAWager_IsValidWager__ + currentCustomerCurrency, Resources.FrmMakeAWager_IsValidWager_Invalid_rif_Amount);
      return false;
    }

    internal static void ShowFormInEditMode(DataGridViewRow dgvwR, FrmSportAndGameSelection frmSportAndGameSelection, string wagerTypeMode, string boxType,
        bool isFreePlayFlag, int amntWageredIdx, int cellidx, int fixedpriceIdx, int gamenumIdx, int groupnumIdx, int layoffIdx, int periodnumIdx,
        int pitcher1MstartIdx, int pitcher2MstartIdx, int priceTypeIdx, int selAmericanPriceIdx, int selBpointsIdx,
        int selGamePeriodDescription, int selOddsIdx, int totalsOuFlagIdx, int towinIdx, int wagercutoffIdx, int wageringmodeIdx, int tnumIdx, int winumIdx,
        int wnumIdx, int wtypeModeIdx) {
      var gameIsAvailable = true;
      var gameNumber = int.Parse(dgvwR.Cells[gamenumIdx].Value.ToString());
      String gameStatus;
      var periodNumber = int.Parse(dgvwR.Cells[periodnumIdx].Value.ToString());
      var periodDescription = dgvwR.Cells[selGamePeriodDescription].Value.ToString();

      if (!frmSportAndGameSelection.GameIsAvailable(gameNumber, periodNumber, out gameStatus)) {
        MessageBox.Show(Resources.FrmTicketConfirmation_ShowRequiredFormInEditMode_Game_no_longer_available_, Resources.FrmTicketConfirmation_ShowRequiredFormInEditMode_Game_not_Available);
        gameIsAvailable = false;
      }
      if (gameIsAvailable) {
        //var rowIdx = int.Parse(dgvwR.Cells[rowidx].Value.ToString());
        var cellIdx = int.Parse(dgvwR.Cells[cellidx].Value.ToString());
        var ticketNumber = int.Parse(dgvwR.Cells[tnumIdx].Value.ToString());
        var wagerNumber = int.Parse(dgvwR.Cells[wnumIdx].Value.ToString());
        var itemNumber = int.Parse(dgvwR.Cells[winumIdx].Value.ToString());

        var selectedOdds = dgvwR.Cells[selOddsIdx].Value.ToString();
        var selectedAmericanPrice = int.Parse(dgvwR.Cells[selAmericanPriceIdx].Value.ToString());
        var selectedBPointsOption = dgvwR.Cells[selBpointsIdx].Value.ToString();
        var amtWagered = dgvwR.Cells[amntWageredIdx].Value.ToString();
        var toWinAmt = dgvwR.Cells[towinIdx].Value.ToString();

        if (dgvwR.Cells[wtypeModeIdx].Value != null && wagerTypeMode == "ToWin")
          wagerTypeMode = dgvwR.Cells[wtypeModeIdx].Value.ToString();
        var priceType = dgvwR.Cells[priceTypeIdx].Value.ToString();
        var groupNum = int.Parse(dgvwR.Cells[groupnumIdx].Value.ToString());
        var wagerCutoff = DateTime.Parse(dgvwR.Cells[wagercutoffIdx].Value.ToString());

        var wageringMode = dgvwR.Cells[wageringmodeIdx].Value.ToString();
        var layoffWager = dgvwR.Cells[layoffIdx].Value.ToString();
        var fixedPrice = dgvwR.Cells[fixedpriceIdx].Value.ToString();

        var pitcher1MStart = dgvwR.Cells[pitcher1MstartIdx].Value != null ? dgvwR.Cells[pitcher1MstartIdx].Value.ToString() : "";
        var pitcher2MStart = dgvwR.Cells[pitcher2MstartIdx].Value != null ? dgvwR.Cells[pitcher2MstartIdx].Value.ToString() : "";

        var totalsOuFlag = dgvwR.Cells[totalsOuFlagIdx].Value != null ? dgvwR.Cells[totalsOuFlagIdx].Value.ToString() : null;

        frmSportAndGameSelection.ShowFormInEditMode(gameNumber, periodNumber, periodDescription, groupNum, wagerCutoff, /*rowIdx,*/ cellIdx, ticketNumber, wagerNumber, itemNumber, selectedOdds, selectedAmericanPrice,
            selectedBPointsOption, amtWagered, toWinAmt, wagerTypeMode, isFreePlayFlag, priceType, wageringMode, layoffWager, fixedPrice, pitcher1MStart, pitcher2MStart, boxType, totalsOuFlag);
      }
    }

    internal static void PopulateRounRobinTypeCmb(BindingSource bndsrcRoundRobinType) {
      for (var i = 1; i < 10; i++) {
        var typeName = "";

        for (var j = 0; j < (i < 6 ? i : i - 5); j++) {
          typeName += (j + (i < 6 ? 2 : 3)).ToString(CultureInfo.InvariantCulture) + "'s, ";
        }
        bndsrcRoundRobinType.Add(new RoundRobinType());
        ((RoundRobinType)bndsrcRoundRobinType.List[bndsrcRoundRobinType.List.Count - 1]).Index = i;
        ((RoundRobinType)bndsrcRoundRobinType.List[bndsrcRoundRobinType.List.Count - 1]).Name = typeName.Substring(0, typeName.Length - 2);
      }
      AppendCustomRrCombinations(bndsrcRoundRobinType);
      bndsrcRoundRobinType.ResetBindings(false);
    }

    private static void AppendCustomRrCombinations(BindingSource bndsrcRoundRobinType) {
      bndsrcRoundRobinType.Add(new RoundRobinType());
      ((RoundRobinType)bndsrcRoundRobinType.List[bndsrcRoundRobinType.List.Count - 1]).Index = bndsrcRoundRobinType.Count;
      var typeName = "4's";
      ((RoundRobinType)bndsrcRoundRobinType.List[bndsrcRoundRobinType.List.Count - 1]).Name = typeName;
      bndsrcRoundRobinType.Add(new RoundRobinType());
      ((RoundRobinType)bndsrcRoundRobinType.List[bndsrcRoundRobinType.List.Count - 1]).Index = bndsrcRoundRobinType.Count;
      typeName = "4's, 5's";
      ((RoundRobinType)bndsrcRoundRobinType.List[bndsrcRoundRobinType.List.Count - 1]).Name = typeName;
      bndsrcRoundRobinType.Add(new RoundRobinType());
      ((RoundRobinType)bndsrcRoundRobinType.List[bndsrcRoundRobinType.List.Count - 1]).Index = bndsrcRoundRobinType.Count;
      typeName = "5's";
      ((RoundRobinType)bndsrcRoundRobinType.List[bndsrcRoundRobinType.List.Count - 1]).Name = typeName;
      /*
        General Formula for the combinations (C):
       * C= n!/[(n - r)!]* (r!) where n is the total of items, r is the desired number of combinations
       * Order of the items is not important as long as the do not repeat 1, 2, 3 is equal to 3, 2, 1, so this combination is not allowed.
       * Since the order does not matter, it is called combination, not permutation.
       */
    }

    internal static void SetFinalMoney(Ticket.WagerItem stageWagerItem, string price, string wageringMode, int? oddsFractionMaxDenominator, int? oddsDecimalPrecision) {
      if (stageWagerItem.OrigMoney != stageWagerItem.FinalMoney || wageringMode == "M") {
        double doubleParsed;
        double.TryParse(price, out doubleParsed);
        stageWagerItem.FinalDecimal = OddsTypes.UStoDecimal(doubleParsed, oddsDecimalPrecision);
        double.TryParse(stageWagerItem.FinalDecimal.ToString(), out doubleParsed);
        var fractionAry = OddsTypes.DecToFraction(doubleParsed, oddsFractionMaxDenominator.GetValueOrDefault());
        stageWagerItem.FinalNumerator = fractionAry.Numerator;
        stageWagerItem.FinalDenominator = fractionAry.Denominator;
      }
      else {
        stageWagerItem.FinalDecimal = stageWagerItem.OrigDecimal;
        stageWagerItem.FinalNumerator = stageWagerItem.OrigNumerator;
        stageWagerItem.FinalDenominator = stageWagerItem.OrigDenominator;
      }
    }

    internal static void SetCustomerAndCurrency(spCstGetCustomer_Result customer, spCstGetCustomer_Result lastCustomer, out string customerId, out string currency) {
      customerId = "";
      currency = "";
      if (customer != null) {
        customerId = customer.CustomerID.Trim();
        currency = (String.IsNullOrEmpty(customer.Currency) ? Currencies.DEFAULT_CURRENCY : customer.Currency).Trim();
      }
      else {
        if (lastCustomer == null) return;
        customerId = lastCustomer.CustomerID.Trim();
        currency = (String.IsNullOrEmpty(lastCustomer.Currency) ? Currencies.DEFAULT_CURRENCY : lastCustomer.Currency).Trim();
      }
    }

  }
}