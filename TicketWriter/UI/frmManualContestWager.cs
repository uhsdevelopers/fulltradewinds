﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using GUILibraries.Forms;
using GUILibraries.Utilities;
using SIDLibraries.Utilities;
using SIDLibraries.Entities;
using SIDLibraries.BusinessLayer;
using GUILibraries.Controls;
using TicketWriter.Properties;

namespace TicketWriter.UI {
  public sealed partial class FrmManualContestWager : SIDForm {
    #region Public Properties

    public String FrmMode { private get; set; }

    private FrmSportAndGameSelection FrmSportAndGameSelection {
      get {
        return _frmSportAndGameSelection ??
               (_frmSportAndGameSelection =
                (FrmSportAndGameSelection)FormF.GetFormByName("FrmSportAndGameSelection", this));
      }
    }

    public Boolean IsFreePlay { private get; set; }

    private MdiTicketWriter Mdi {
      get {
        return _mdiTicketWriter ??
               (_mdiTicketWriter = (MdiTicketWriter)FormF.GetFormByName("MdiTicketWriter", this));
      }
    }

    public int WagerItemNumberToUpd { private get; set; }

    public int WagerNumberToUpd { private get; set; }

    public int TicketNumberToUpd { private get; set; }

    public String PreviouslySelectedAmtWagered { private get; set; }

    public String PreviouslySelectedSpread { get; set; }

    public String PreviouslySelectedOdds { private get; set; }

    public String PreviouslySelectedToWinAmt { private get; set; }

    public String PreviouslySelectedDescription { get; set; }

    public Boolean PreviouslySelectedLayOffWager { private get; set; }

    public String PreviouslySelectedThresholdLine { private get; set; }

    public String PreviouslySelectedXtoY { private get; set; }

    public String PreviouslySelectedToBase { private get; set; }

    #endregion

    #region Private Properties

    private string SelectedAdjustment {
      get {
        if (!string.IsNullOrEmpty(_selectedAdjustment)) return _selectedAdjustment;
        if (txtPrice.Text.Length <= 0) return _selectedAdjustment;
        if (txtPrice.Text.Contains("to")) {
          _selectedAdjustment = txtPrice.Text.ToLower().Split(new[] { "to" }, StringSplitOptions.RemoveEmptyEntries)[0].Trim();
          _selectedToBase = txtPrice.Text.ToLower().Split(new[] { "to" }, StringSplitOptions.RemoveEmptyEntries)[1].Trim();
        }
        else {
          _selectedAdjustment = txtPrice.Text;
        }
        return _selectedAdjustment;
      }
    }

    private Boolean IsXtoY { get; set; }

    public String WagerTypeMode { get; private set; }

    #endregion

    #region Public vars

    private List<Ticket.SelectedContestAndLine> OriginalContestsAndPeriodsInfo {
      get {
        if (Mdi.Ticket == null) return null;
        var list = Mdi.Ticket.OriginalContestOptions;

        return list;
      }
    }

    #endregion

    #region Private vars
    private readonly List<spCnGetActiveContests_Result> _activeContests;
    private String _contestShortDescription;
    private readonly int _contestNumber;
    private readonly int _contestantNumber;
    private String _contestant;
    private String _currentCustomerAgentId;
    private readonly String _currentWagerTypeName;
    private readonly String _customerHasStaticLines;
    private String _formattedAdjustment;
    public FrmSportAndGameSelection _frmSportAndGameSelection;
    private readonly Boolean _layoffWagersAccepted;
    private MdiTicketWriter _mdiTicketWriter;
    private string _selectedAdjustment;
    private string _selectedToBase = "";
    public spCnGetActiveContests_Result SelectedContest;
    private String _selectedPrice;
    private String _selectedWagerType = "";
    private String _thresholdLine = "";
    private bool _validKeyEntered;

    #endregion

    #region Structures

    #endregion

    #region Constructors

    public FrmManualContestWager(String wagerTypeName, FrmSportAndGameSelection parentForm, IEnumerable<spCnGetActiveContests_Result> activeContests, int contestNumber, int contestantNumber, String contestant, String priceType, Boolean layoffWagersAccepted)
      : base(parentForm.AppModuleInfo) {
      _currentWagerTypeName = wagerTypeName;
      _activeContests = activeContests.OrderBy(x => x.ContestType).ThenBy(x => x.RotNum).ToList();
      _contestNumber = contestNumber;
      _contestantNumber = contestantNumber;
      _contestant = contestant;
      _selectedPrice = priceType;
      _customerHasStaticLines = parentForm.StaticLinesDuringCall;
      _layoffWagersAccepted = layoffWagersAccepted;

      InitializeComponent();
    }

    #endregion

    #region Public Methods

    public double GetToBaseValue() {
      var selectedPrice = "American";
      if (txtPrice.Text.Contains("."))
        selectedPrice = "Decimal";
      if (txtPrice.Text.Contains("/"))
        selectedPrice = "Fractional";
      var selectedToBase = 0.0D;
      if (selectedPrice != "American" || !txtPrice.Text.Contains("to")) return selectedToBase;
      double tmpAdj;
      double.TryParse(txtPrice.Text.ToLower().Split(new[] { "to" }, StringSplitOptions.RemoveEmptyEntries)[1].Trim(), out tmpAdj);
      selectedToBase = tmpAdj;
      return selectedToBase;
    }

    public double GetAdjustment() {
      double adj = 0;
      var selectedPrice = "American";
      if (txtPrice.Text.Contains("."))
        selectedPrice = "Decimal";
      if (txtPrice.Text.Contains("/"))
        selectedPrice = "Fractional";
      double tmpAdj;
      switch (selectedPrice) {
        case "American":
        case LineOffering.PRICE_AMERICAN:
          if (txtPrice.Text.Contains("to")) {
            double.TryParse(txtPrice.Text.ToLower().Split(new[] { "to" }, StringSplitOptions.RemoveEmptyEntries)[0].Trim(), out tmpAdj);
            adj = tmpAdj;
          }
          else {
            double.TryParse(txtPrice.Text.ToLower().Trim(), out tmpAdj);
            adj = tmpAdj;
          }
          break;
        case "Decimal":
        case LineOffering.PRICE_DECIMAL:
          double.TryParse(txtPrice.Text, out tmpAdj);
          adj = OddsTypes.DecimalToUs(double.Parse(tmpAdj.ToString(CultureInfo.InvariantCulture)));
          break;
        case "Fractional":
        case LineOffering.PRICE_FRACTIONAL:
          int numerator;
          int denominator;
          int.TryParse(txtPrice.Text.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries)[0].Trim(), out numerator);
          int.TryParse(txtPrice.Text.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries)[1].Trim(), out denominator);
          var convertedToDecimal = OddsTypes.FractionalToDecimal(numerator, denominator);
          adj = OddsTypes.DecimalToUs(double.Parse(convertedToDecimal.ToString(CultureInfo.InvariantCulture)));
          break;
      }

      return adj;
    }

    #endregion

    #region Private Methods

    private void ClearAmountTextBoxes() {
      txtRisk.Text = "";
      txtToWin.Text = "";
    }

    private void FillWageringForm() {
      if (_activeContests != null) {
        SelectedContest = (from p in _activeContests where p.ContestantNum == _contestantNumber select p).FirstOrDefault();
      }

      if (SelectedContest != null && (SelectedContest.XtoYLineRep != null && SelectedContest.XtoYLineRep == "Y")) {
        IsXtoY = true;
        _selectedPrice = "Xtrange";
      }

      if (SelectedContest != null) {
        var formattedMoneyLine = FormatNumber(SelectedContest.MoneyLine.GetValueOrDefault(), false);

        if (SelectedContest.ContestDesc != null)
          txtContestOdds.Text = SelectedContest.ContestDesc.Trim();
        txtContestOdds.Text += Environment.NewLine;
        if (SelectedContest.ContestDateTime != null)
          txtContestOdds.Text += DisplayDate.Format((DateTime)SelectedContest.ContestDateTime, DisplayDateFormat.DayNameAndLongDate);

        if (SelectedContest.ContestantName != null)
          txtChosenTeam.Text = SelectedContest.ContestantName.Trim();


        if (SelectedContest.ThresholdType == null) {
          txtLineAdj.Visible = false;
          lblWagerType.Visible = false;
        }
        else {
          if (SelectedContest.ThresholdLine != null)
            txtLineAdj.Text = LinesFormat.FormatStringPoints(SelectedContest.ThresholdLine, SelectedContest.ThresholdType);
          lblWagerType.Text = SelectedContest.ThresholdUnits;
          _selectedWagerType = SelectedContest.ThresholdType.Trim();
          if (SelectedContest.ThresholdType == "P")
            txtLineAdj.Text = txtLineAdj.Text.Replace("+", "").Replace("-", "");
        }

        if (SelectedContest.XtoYLineRep == "N") {
          if (SelectedContest.MoneyLine == null)
            return;

          switch (_selectedPrice) {
            case "American":
            case LineOffering.PRICE_AMERICAN:
              if (SelectedContest.MoneyLine > 0) {
                txtPrice.Text += @"+" + formattedMoneyLine;
              }
              else {
                txtPrice.Text += formattedMoneyLine;
              }
              break;
            case "Decimal":
            case LineOffering.PRICE_DECIMAL:
              if (SelectedContest.DecimalOdds != null) {
                txtPrice.Text += SelectedContest.DecimalOdds;
                if (!txtPrice.Text.Contains("."))
                  txtPrice.Text += @".";
              }
              break;
            case "Fractional":
            case LineOffering.PRICE_FRACTIONAL:
              if (SelectedContest.Numerator != null && SelectedContest.Denominator != null) {
                txtPrice.Text += SelectedContest.Numerator + @"/" + SelectedContest.Denominator;
              }
              break;
          }
        }
        else {
          if (SelectedContest.MoneyLine != null) {
            if (SelectedContest.ToBase != null)
              txtPrice.Text += formattedMoneyLine + @" to " + SelectedContest.ToBase;
            formattedMoneyLine += " to " + SelectedContest.ToBase;
          }
        }
        _formattedAdjustment = formattedMoneyLine;

        if (SelectedContest.XtoYLineRep == "N") {

          if (double.Parse(_formattedAdjustment) > 0) { }
        }
      }

      chbLayoffWager.Visible = _layoffWagersAccepted;

      panWagerAmounts.Visible = _currentWagerTypeName == "Straight Bet";

      if (_currentWagerTypeName == "Parlay") {
        btnPlaceBet.Text = "Add To Parlay";
      }

    }

    private void FillWageringFormInEditMode() {
      if (_customerHasStaticLines == "N" || Mdi.TicketNumber != TicketNumberToUpd) {
        if (_activeContests != null) {
          SelectedContest = (from p in _activeContests where p.ContestantNum == _contestantNumber select p).FirstOrDefault();
        }
      }
      else {
        Ticket.SelectedContestAndLine selectedContestInfo = null;

        if (OriginalContestsAndPeriodsInfo != null)
          selectedContestInfo = (from gO in OriginalContestsAndPeriodsInfo where gO.ContestNum == _contestNumber && gO.TicketNumber == TicketNumberToUpd && gO.WagerNumber == WagerNumberToUpd && gO.ItemNumber == WagerItemNumberToUpd select gO).FirstOrDefault();

        if (selectedContestInfo != null) {
          SelectedContest = TwUtilities.ConvertToContestInfoObject(selectedContestInfo);
        }
      }

      if (SelectedContest != null && (SelectedContest.XtoYLineRep != null && SelectedContest.XtoYLineRep == "Y")) {
        IsXtoY = true;
        _selectedPrice = "Xtrange";
      }

      var formattedMoneyLine = "";

      if (SelectedContest == null) return;
      if (PreviouslySelectedOdds != null)
        formattedMoneyLine = FormatNumber(double.Parse(PreviouslySelectedOdds), false);

      if (SelectedContest.ContestDesc != null)
        txtContestOdds.Text = SelectedContest.ContestDesc.Trim();
      txtContestOdds.Text += Environment.NewLine;
      if (SelectedContest.ContestDateTime != null)
        txtContestOdds.Text += DisplayDate.Format((DateTime)SelectedContest.ContestDateTime, DisplayDateFormat.DayNameAndLongDate);

      if (SelectedContest.ContestantName != null)
        txtChosenTeam.Text = SelectedContest.ContestantName.Trim();


      if (SelectedContest.ThresholdType == null) {
        txtLineAdj.Visible = false;
        lblWagerType.Visible = false;
      }
      else {
        if (PreviouslySelectedThresholdLine != null)
          txtLineAdj.Text = LinesFormat.FormatStringPoints(double.Parse(PreviouslySelectedThresholdLine), SelectedContest.ThresholdType);
        lblWagerType.Text = SelectedContest.ThresholdUnits;
        _selectedWagerType = SelectedContest.ThresholdType.Trim();

        if (SelectedContest.ThresholdType == "P")
          txtLineAdj.Text = txtLineAdj.Text.Replace("+", "").Replace("-", "");
      }

      if (PreviouslySelectedXtoY == "N" || String.IsNullOrEmpty(PreviouslySelectedXtoY)) {
        if (PreviouslySelectedOdds == null)
          return;

        switch (_selectedPrice) {
          case "American":
          case LineOffering.PRICE_AMERICAN:
            if (double.Parse(PreviouslySelectedOdds) > 0) {
              txtPrice.Text += @"+" + formattedMoneyLine;
            }
            else {
              txtPrice.Text += formattedMoneyLine;
            }
            break;
          case "Decimal":
          case LineOffering.PRICE_DECIMAL:
            if (PreviouslySelectedOdds != null) {
              txtPrice.Text += OddsTypes.UStoDecimal(double.Parse(PreviouslySelectedOdds), 2);
              if (!txtPrice.Text.Contains("."))
                txtPrice.Text += @".";
            }
            break;
          case "Fractional":
          case LineOffering.PRICE_FRACTIONAL:
            if (PreviouslySelectedOdds != null) {
              var americanToDec = OddsTypes.UStoDecimal(double.Parse(PreviouslySelectedOdds), 2);
              var f = OddsTypes.DecToFraction(americanToDec);
              txtPrice.Text += f.Numerator + @"/" + f.Denominator;
            }
            break;
        }
      }
      else {
        if (PreviouslySelectedOdds != null) {
          if (PreviouslySelectedToBase != null)
            txtPrice.Text += formattedMoneyLine + @" to " + PreviouslySelectedToBase;
          formattedMoneyLine += " to " + PreviouslySelectedToBase;
        }
      }
      _formattedAdjustment = formattedMoneyLine;

      if (PreviouslySelectedXtoY != null) {
        if (PreviouslySelectedXtoY == "N") {
          if (double.Parse(_formattedAdjustment) > 0) { }
        }
      }

      if (PreviouslySelectedAmtWagered != null)
        txtRisk.Text = PreviouslySelectedAmtWagered;

      if (PreviouslySelectedToWinAmt != null)
        txtToWin.Text = PreviouslySelectedToWinAmt;

      chbLayoffWager.Checked = PreviouslySelectedLayOffWager;

      chbLayoffWager.Visible = _layoffWagersAccepted;

      panWagerAmounts.Visible = _currentWagerTypeName == "Straight Bet";

      if (_currentWagerTypeName == "Parlay") {
        btnPlaceBet.Text = "Add To Parlay";
      }
    }

    private void GoToPlaceBet() {
      var layoffFlag = chbLayoffWager.Checked ? "Y" : "N";

      if (IsValidPrice(txtPrice, _selectedPrice) && IsValidLine(txtLineAdj, txtLineAdj.Visible)) {
        var ticketNumberForWi = FrmMode == "Edit" ? TicketNumberToUpd : Mdi.TicketNumber;
        var wagerNumberForWi = FrmMode == "Edit" ? WagerNumberToUpd : 1;
        var itemNumberForcWi = 1;
        var frmPlaceParlay = (FrmPlaceParlay)FormF.GetFormByName("frmPlaceParlay", this);
        if (frmPlaceParlay != null)
          frmPlaceParlay.ItemFromMakeAWagerForm = true;

        if (_currentWagerTypeName == "Straight Bet") {
          FrmSportAndGameSelection.CreateStageWager(FrmMode, ticketNumberForWi, WagerNumberToUpd, out wagerNumberForWi, txtRisk, txtToWin, _currentWagerTypeName, WagerType.CONTEST, null, null, 1, 0, null, null, null, null, null, "", IsFreePlay, _contestShortDescription, layoffFlag);
        }

        if (_currentWagerTypeName == "Parlay") {
          if (frmPlaceParlay == null)
            throw new Exception("Unable to find Parlay Form");
          var parlay = frmPlaceParlay.Parlay;
          if (parlay == null) {
            parlay = FrmSportAndGameSelection.CreateParlayWager(frmPlaceParlay, FrmMode, ticketNumberForWi, WagerNumberToUpd, txtRisk, txtToWin, _currentWagerTypeName, WagerType.MONEYLINE, IsFreePlay, "N");

            if (parlay == null) {
              return;
            }
          }

          ticketNumberForWi = parlay.TicketNumber;
          wagerNumberForWi = parlay.Number ?? 0;
          itemNumberForcWi = parlay.Items.Count;

        }
        Ticket.ContestWagerItem cWi;
        string itemShortDescription;
        if (FrmMode == "Edit") {
          cWi = FrmSportAndGameSelection.CreateStageContestWagerItem(FrmMode, ticketNumberForWi, WagerNumberToUpd, WagerItemNumberToUpd, double.Parse(String.IsNullOrEmpty(txtRisk.Text) ? "0" : txtRisk.Text), SelectedContest, _selectedPrice, _contestant, "M", layoffFlag, double.Parse(_selectedAdjustment), _selectedToBase, _thresholdLine);
          if (Mdi.Ticket.ContestWagerItems != null && Mdi.Ticket.ContestWagerItems.Any(w => w.TicketNumber == cWi.TicketNumber && w.WagerNumber == cWi.WagerNumber)) {
            FrmSportAndGameSelection.UpdateContestWagerItem(cWi);
            if (_currentWagerTypeName == "Parlay") {
              var item = new ParlayItem();
              FrmSportAndGameSelection.FillParlayContestItem(item, cWi, _selectedPrice, txtRisk, txtToWin, _contestShortDescription, WagerTypeMode, "A", "N");
              if (frmPlaceParlay.Parlay.Items.Any(pwi => pwi.TicketNumber == cWi.TicketNumber && pwi.WagerNumber == cWi.WagerNumber && pwi.Number == cWi.ItemNumber)) {
                frmPlaceParlay.UpdateItem(item);
              }
              else {
                frmPlaceParlay.AddItem(item);
              }
              var wagerItem = FrmSportAndGameSelection.CreateStageWagerItemFromContestItem(FrmMode, _currentWagerTypeName, "M", WagerTypeMode,
                                                  _selectedPrice, cWi.TicketNumber, cWi.WagerNumber,
                                                  cWi.ItemNumber, SelectedAdjustment,
                                                  0, "", _contestShortDescription,
                                                  cWi,
                                                  out itemShortDescription, IsFreePlay, cWi.WageringMode, cWi.LayoffFlag);
              if (Mdi.Ticket.WagerItems != null && Mdi.Ticket.WagerItems.Any(w => w.TicketNumber == cWi.TicketNumber && w.WagerNumber == cWi.WagerNumber && w.IsContest)) {
                Mdi.Ticket.UpdateWagerItem(wagerItem);
              }
              else {
                Mdi.Ticket.AddWagerItem(wagerItem);
              }
            }
          }
          else {
            FrmSportAndGameSelection.AddStageContestWagerItem(cWi, SelectedContest);
            if (_currentWagerTypeName == "Parlay") {
              var item = new ParlayItem();
              FrmSportAndGameSelection.FillParlayContestItem(item, cWi, _selectedPrice, txtRisk, txtToWin, _contestShortDescription, WagerTypeMode, "A", "N");
              FrmSportAndGameSelection.ParlayOrTeaserItemFromSb = false;
              frmPlaceParlay.AddItem(item);
              var wagerItem = FrmSportAndGameSelection.CreateStageWagerItemFromContestItem(FrmMode, _currentWagerTypeName, "M", WagerTypeMode,
                                  _selectedPrice, ticketNumberForWi, wagerNumberForWi,
                                  itemNumberForcWi, SelectedAdjustment,
                                  0, "", _contestShortDescription,
                                  cWi,
                                  out itemShortDescription, IsFreePlay, "A", "N");
              Mdi.Ticket.AddWagerItem(wagerItem);
            }
          }
        }
        else {
          if (txtLineAdj.Visible) {
            var numLine = LinesFormat.GetNumericPoints(txtLineAdj.Text);

            if (SelectedContest.ThresholdLine != numLine) {
              _contestant = (SelectedContest.ContestantName == null ? "" : SelectedContest.ContestantName.Trim()) + " " + LinesFormat.FormatStringPoints(numLine, SelectedContest.ThresholdType) + " " + (SelectedContest.ThresholdUnits == null ? "" : SelectedContest.ThresholdUnits.Trim());
            }

          }
          cWi = FrmSportAndGameSelection.CreateStageContestWagerItem(FrmMode, ticketNumberForWi, wagerNumberForWi, null, double.Parse(String.IsNullOrEmpty(txtRisk.Text) ? "0" : txtRisk.Text), SelectedContest,
              _selectedPrice, _contestant, "M", layoffFlag, double.Parse(_selectedAdjustment), _selectedToBase, _thresholdLine);
          if (_currentWagerTypeName == "Parlay") {
            cWi.ItemNumber = itemNumberForcWi + 1;
          }
          FrmSportAndGameSelection.AddStageContestWagerItem(cWi, SelectedContest);
          if (_currentWagerTypeName == "Parlay") {
            var item = new ParlayItem();
            FrmSportAndGameSelection.FillParlayContestItem(item, cWi, _selectedPrice, txtRisk, txtToWin, _contestShortDescription, WagerTypeMode, "A", "N");
            FrmSportAndGameSelection.ParlayOrTeaserItemFromSb = false;
            frmPlaceParlay.AddItem(item);
            var wagerItem = FrmSportAndGameSelection.CreateStageWagerItemFromContestItem(FrmMode, _currentWagerTypeName, "M", WagerTypeMode,
                                _selectedPrice, ticketNumberForWi, wagerNumberForWi,
                                itemNumberForcWi, SelectedAdjustment,
                                0, "", _contestShortDescription,
                                cWi,
                                out itemShortDescription, IsFreePlay, "M", "N");

            Mdi.Ticket.AddWagerItem(wagerItem);
          }

        }
        if (_currentWagerTypeName == "Straight Bet") {
          FrmSportAndGameSelection.BuildReadbackLogInfoData(true);
          FrmSportAndGameSelection.PopulateReadbackLogInfoDgvw();
        }
        Close();
      }
      else {
        if (txtRisk.Text.Length == 0 && _currentWagerTypeName == "Straight Bet") {
          MessageBox.Show(@"Enter a valid Risk Amount", @"Invalid Risk");
          txtRisk.Focus();
          return;
        }

        if (txtToWin.Text.Length == 0 && _currentWagerTypeName == "Straight Bet") {
          MessageBox.Show(@"Enter a valid Win Amount", @"Invalid Win");
          txtToWin.Focus();
          return;
        }
      }
      Tag = "Added Manual " + _selectedWagerType + " item to " + _currentWagerTypeName + " in " + (String.IsNullOrEmpty(FrmMode) ? "New" : FrmMode) + " mode. " + _frmSportAndGameSelection.Customer.CustomerID.Trim() + " " + TicketNumberToUpd.ToString() + "." + WagerNumberToUpd.ToString() + "." + WagerItemNumberToUpd.ToString() + " (" + _contestShortDescription + ")" +
          (_currentWagerTypeName == "Straight Bet" ? " Risking: " + txtRisk.Text + ", to Win:" + txtToWin.Text : "");
    }

    private void HandleKeyDownEvent(KeyEventArgs e) {
      _validKeyEntered = (e.KeyCode == Keys.D0 || e.KeyCode == Keys.D1 || e.KeyCode == Keys.D2 || e.KeyCode == Keys.D3 || e.KeyCode == Keys.D4
                                   || e.KeyCode == Keys.D5 || e.KeyCode == Keys.D6 || e.KeyCode == Keys.D7 || e.KeyCode == Keys.D8 || e.KeyCode == Keys.D9
                                   || e.KeyCode == Keys.NumPad0 || e.KeyCode == Keys.NumPad1 || e.KeyCode == Keys.NumPad2 || e.KeyCode == Keys.NumPad3 || e.KeyCode == Keys.NumPad4 || e.KeyCode == Keys.NumPad5
                                   || e.KeyCode == Keys.NumPad6 || e.KeyCode == Keys.NumPad7 || e.KeyCode == Keys.NumPad8 || e.KeyCode == Keys.NumPad9
                                   || e.KeyCode == Keys.Add || e.KeyCode == Keys.Subtract || e.KeyCode == Keys.Divide || e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete);
    }

    private bool IsValidLine(TextBox targetLine, Boolean lineIsVisible) {
      if (lineIsVisible == false)
        return true;

      bool isValid;
      if (targetLine.Text.Contains(",") || targetLine.Text.Contains(GamesAndLines.HALFSYMBOL)) {
        double convertedLine;
        isValid = (double.TryParse(LinesFormat.GetNumericPoints(targetLine.Text).ToString(CultureInfo.InvariantCulture), out convertedLine));
      }
      else {
        double line;
        isValid = (double.TryParse(targetLine.Text, out line));
      }
      if (!isValid) {
        MessageBox.Show(@"Invalid Line", @"Invalid Line");
        targetLine.Focus();
      }
      return isValid;
    }

    private bool IsValidPrice(TextBox priceToTest, string selectedPrice) {

      if (priceToTest == null || priceToTest.Text.Length == 0 || ((txtRisk.Text == "" && txtToWin.Text == "") && _currentWagerTypeName == "Straight Bet"))
        return false;

      double price;

      switch (selectedPrice) {
        case "American":
        case "Xtrange":
        case LineOffering.PRICE_AMERICAN:
          if (priceToTest.Text.Contains("to")) {

            double toBase;
            if (double.TryParse(priceToTest.Text.Split(new[] { "to" }, StringSplitOptions.RemoveEmptyEntries)[0].Trim(), out price) && double.TryParse(priceToTest.Text.Split(new[] { "to" }, StringSplitOptions.RemoveEmptyEntries)[1].Trim(), out toBase)) {
              return true;
            }
          }
          else {
            if (double.TryParse(priceToTest.Text, out price))
              return true;

          }
          break;
        case "Decimal":
        case LineOffering.PRICE_DECIMAL:
          if (double.TryParse(priceToTest.Text.Replace(".", ""), out price))
            return true;
          break;
        case "Fractional":
        case LineOffering.PRICE_FRACTIONAL:
          int numerator;
          int denominator;
          if (int.TryParse(priceToTest.Text.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries)[0].Trim(), out numerator) && int.TryParse(priceToTest.Text.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries)[1].Trim(), out denominator))
            return true;
          break;
      }

      MessageBox.Show(@"Invalid Price", @"Invalid Price");
      priceToTest.Focus();
      return false;
    }

    private void ValidateContestWagerItem() {
      _contestShortDescription = BuildContestShortDescription();

      if (IsValidWager()) {
        var gameStatus = SelectedContest.Status;
        var cutoffTime = SelectedContest.WagerCutoff.GetValueOrDefault();

        if (gameStatus == null) return;
        switch (gameStatus) {
          case GamesAndLines.EVENT_COMPLETED: //Complete
          case GamesAndLines.EVENT_OFFLINE: //Offline
          case GamesAndLines.EVENT_CANCELLED: //Cancelled?
            MessageBox.Show(@"The contest has already started or is not currently available", @"Not available");
            txtToWin.Focus();
            return;
          case GamesAndLines.EVENT_CIRCLED: //Circled
          case GamesAndLines.EVENT_OPEN: //Open
            Boolean continueValidating;
            Boolean cutoffTimeBypassed;
            FrmSportAndGameSelection.CheckIfContestWagerCutoffHasBeenReached(this, LoginsAndProfiles.ACCEPT_ANY_WAGER, SelectedContest, "Contest: " + _contestShortDescription, txtRisk.Text, txtToWin.Text, cutoffTime, out continueValidating, out cutoffTimeBypassed);
            if (!continueValidating)
              return;
            var minWagerBypassed = true;
            if (_currentWagerTypeName == "Straight Bet") {
              FrmSportAndGameSelection.CheckIfBelowMinimumWager(this, LoginsAndProfiles.ACCEPT_ANY_WAGER, txtRisk.Text, out continueValidating, out minWagerBypassed);
              if (!continueValidating)
                return;
            }
            var availableFundsOk = true;
            if (_currentWagerTypeName == "Straight Bet") {
              FrmSportAndGameSelection.CheckForAvailableFunds(this, FrmMode, TicketNumberToUpd, WagerNumberToUpd, WagerItemNumberToUpd, LoginsAndProfiles.ACCEPT_ANY_WAGER, null, SelectedContest, _contestShortDescription, txtRisk.Text, txtToWin.Text, IsFreePlay, out continueValidating, out availableFundsOk); //Validate if user has sufficientFunds for wager
              if (!continueValidating)
                return;
            }
            var wagerLimitOk = true;
            if (_currentWagerTypeName == "Straight Bet") {
              FrmSportAndGameSelection.CheckIfExceedsContestWagerLimit(this, FrmMode, TicketNumberToUpd, WagerNumberToUpd, WagerItemNumberToUpd, LoginsAndProfiles.ACCEPT_ANY_WAGER, SelectedContest, _contestShortDescription, txtRisk.Text, txtToWin.Text, out continueValidating, out wagerLimitOk);
              if (!continueValidating)
                return;
            }
            if (cutoffTimeBypassed && minWagerBypassed && availableFundsOk && wagerLimitOk) {
              GoToPlaceBet();
              Close();
            }
            break;
        }
      }
      else {
        txtToWin.Focus();
      }
    }

    private string BuildContestShortDescription() {
      _selectedPrice = "American";
      _selectedAdjustment = txtPrice.Text;

      if (txtPrice.Text.Contains("to")) {
        double adj;
        int toBase;
        if (double.TryParse(txtPrice.Text.Split(new[] { "to" }, StringSplitOptions.RemoveEmptyEntries)[0].Trim(), out adj) && int.TryParse(txtPrice.Text.Split(new[] { "to" }, StringSplitOptions.RemoveEmptyEntries)[1].Trim(), out toBase)) {
          _selectedAdjustment = adj.ToString(CultureInfo.InvariantCulture);
          _selectedToBase = toBase.ToString(CultureInfo.InvariantCulture);
        }
        _selectedPrice = "Xtrange";
      }


      if (txtPrice.Text.Contains(".")) {
        _selectedPrice = "Decimal";
      }

      if (txtPrice.Text.Contains("/")) {
        _selectedPrice = "Fractional";
        _selectedAdjustment = "";
        _selectedToBase = "";

        int numerator;
        int denominator;
        if (int.TryParse(txtPrice.Text.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries)[0].Trim(), out numerator) && int.TryParse(txtPrice.Text.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries)[1].Trim(), out denominator)) {
          _selectedAdjustment = numerator.ToString(CultureInfo.InvariantCulture);
          _selectedToBase = denominator.ToString(CultureInfo.InvariantCulture);
        }

      }

      if (_selectedWagerType != null) {
        _thresholdLine = txtLineAdj.Text;
      }

      return TwUtilities.FormatShortContestDescription(SelectedContest, _selectedPrice, _selectedAdjustment, SelectedContest.ThresholdLine.ToString(), SelectedContest.ToBase.ToString());
    }

    private bool IsValidWager() {
      var isValidWager = true;

      if ((txtRisk.Text.Length == 0 || txtToWin.Text.Length == 0) && _currentWagerTypeName == "Straight Bet") {
        MessageBox.Show(Resources.FrmMakeAWager_IsValidWager_A_wager_amount_must_be_entered_, Resources.FrmMakeAWager_IsValidWager_Invalid_Amount);
        isValidWager = false;
      }
      return isValidWager;
    }

    #endregion

    #region Events

    private void btnPlaceBet_Click(object sender, EventArgs e) {
      ValidateContestWagerItem();
    }

    private void frmManualContestWager_Load(object sender, EventArgs e) {

      if (FrmMode == "Edit") {
        FillWageringFormInEditMode();
      }
      else {
        FillWageringForm();
      }
    }

    private void frmManualContestWager_Shown(object sender, EventArgs e) {
      double moneyLine;
      switch (_selectedPrice) {
        case "American":
        case LineOffering.PRICE_AMERICAN:
          moneyLine = SelectedContest.MoneyLine.HasValue ? (double)SelectedContest.MoneyLine : 0;
          if (moneyLine > 0) {
            txtRisk.Focus();
            WagerTypeMode = "Risk";
          }
          else {
            txtToWin.Focus();
            WagerTypeMode = "ToWin";
          }
          break;
        case "Decimal":
        case "Fractional":
        case LineOffering.PRICE_DECIMAL:
        case LineOffering.PRICE_FRACTIONAL:
          moneyLine = SelectedContest.DecimalOdds.HasValue ? (double)SelectedContest.DecimalOdds : 0;

          if (moneyLine > 2) {
            txtRisk.Focus();
            WagerTypeMode = "Risk";
          }
          else {
            txtToWin.Focus();
            WagerTypeMode = "ToWin";
          }
          break;
      }
      if (FrmSportAndGameSelection.ShowWagerLimitsInFrm.IsActive()) {
        FrmSportAndGameSelection.ShowWagerLimitsInFrm.FrmManualContestWager = this;
        FrmSportAndGameSelection.ShowWagerLimitsInFrm.DisplayMaxManualContestWagerLimits();
      }
    }

    private void txtLineAdj_KeyDown(object sender, KeyEventArgs e) {
      HandleKeyDownEvent(e);
    }

    private void txtLineAdj_KeyPress(object sender, KeyPressEventArgs e) {
      if (_validKeyEntered) {
        bool handled;
        ((TextBox)sender).Text = LinesFormat.ManualModePointsKeyPress(((TextBox)sender).Text, ((TextBox)sender).SelectedText.Length, e.KeyChar, true, out handled);
        e.Handled = handled;
        if (SelectedContest.ThresholdType == "P")
          txtLineAdj.Text = txtLineAdj.Text.Replace("+", "").Replace("-", "");
        ((TextBox)sender).HideSelection = true;
      }

      if (e.KeyChar != (char)8)
        e.Handled = true;
      ((TextBox)sender).Select(((TextBox)sender).Text.Length, 0);
    }

    private void txtPrice_KeyDown(object sender, KeyEventArgs e) {
      _validKeyEntered = false;
      if (e.KeyCode == Keys.Decimal)
        _selectedPrice = "Decimal";
      if (e.KeyCode == Keys.Divide)
        _selectedPrice = "Fractional";
      if (e.KeyCode == Keys.T || e.KeyCode == Keys.O)
        _selectedPrice = "Xtrange";

      switch (_selectedPrice) {
        case "American":
        case LineOffering.PRICE_AMERICAN:
        case "Xtrange":
          if (e.KeyCode == Keys.D0 || e.KeyCode == Keys.D1 || e.KeyCode == Keys.D2 || e.KeyCode == Keys.D3 || e.KeyCode == Keys.D4
              || e.KeyCode == Keys.D5 || e.KeyCode == Keys.D6 || e.KeyCode == Keys.D7 || e.KeyCode == Keys.D8 || e.KeyCode == Keys.D9
              || e.KeyCode == Keys.NumPad0 || e.KeyCode == Keys.NumPad1 || e.KeyCode == Keys.NumPad2 || e.KeyCode == Keys.NumPad3 || e.KeyCode == Keys.NumPad4 || e.KeyCode == Keys.NumPad5
              || e.KeyCode == Keys.NumPad6 || e.KeyCode == Keys.NumPad7 || e.KeyCode == Keys.NumPad8 || e.KeyCode == Keys.NumPad9
              || e.KeyCode == Keys.Add || e.KeyCode == Keys.Subtract || e.KeyCode == Keys.Divide || e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete
            //|| e.KeyCode == Keys.T || e.KeyCode == Keys.O
              ) {
            _validKeyEntered = true;
          }
          break;
        case "Decimal":
        case LineOffering.PRICE_DECIMAL:
          if (e.KeyCode == Keys.D0 || e.KeyCode == Keys.D1 || e.KeyCode == Keys.D2 || e.KeyCode == Keys.D3 || e.KeyCode == Keys.D4
              || e.KeyCode == Keys.D5 || e.KeyCode == Keys.D6 || e.KeyCode == Keys.D7 || e.KeyCode == Keys.D8 || e.KeyCode == Keys.D9
              || e.KeyCode == Keys.NumPad0 || e.KeyCode == Keys.NumPad1 || e.KeyCode == Keys.NumPad2 || e.KeyCode == Keys.NumPad3 || e.KeyCode == Keys.NumPad4 || e.KeyCode == Keys.NumPad5
              || e.KeyCode == Keys.NumPad6 || e.KeyCode == Keys.NumPad7 || e.KeyCode == Keys.NumPad8 || e.KeyCode == Keys.NumPad9
              || e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete || e.KeyCode == Keys.Decimal || e.KeyCode == Keys.OemPeriod) {
            _validKeyEntered = true;
          }
          break;
        case "Fractional":
        case LineOffering.PRICE_FRACTIONAL:
          if (e.KeyCode == Keys.D0 || e.KeyCode == Keys.D1 || e.KeyCode == Keys.D2 || e.KeyCode == Keys.D3 || e.KeyCode == Keys.D4
              || e.KeyCode == Keys.D5 || e.KeyCode == Keys.D6 || e.KeyCode == Keys.D7 || e.KeyCode == Keys.D8 || e.KeyCode == Keys.D9
              || e.KeyCode == Keys.NumPad0 || e.KeyCode == Keys.NumPad1 || e.KeyCode == Keys.NumPad2 || e.KeyCode == Keys.NumPad3 || e.KeyCode == Keys.NumPad4 || e.KeyCode == Keys.NumPad5
              || e.KeyCode == Keys.NumPad6 || e.KeyCode == Keys.NumPad7 || e.KeyCode == Keys.NumPad8 || e.KeyCode == Keys.NumPad9
              || e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete || e.KeyCode == Keys.Divide) {
            _validKeyEntered = true;
          }
          break;
      }
    }

    private void txtPrice_KeyPress(object sender, KeyPressEventArgs e) {

      if (_validKeyEntered) {
        if (e.KeyChar != (char)8) {
          if (((TextBox)sender).SelectionStart == ((TextBox)sender).Text.Length) {
            bool handled;
            ((TextBox)sender).Text = LinesFormat.ManualModePointsKeyPress(((TextBox)sender).Text, ((TextBox)sender).SelectedText.Length, e.KeyChar, false, out handled);
            e.Handled = handled;
            ((TextBox)sender).HideSelection = true;
            ((TextBox)sender).Select(((TextBox)sender).Text.Length, 0);
          }
          else {
            if (e.KeyChar == (char)43 || e.KeyChar == (char)45 || e.KeyChar == (char)47) {
              if ((e.KeyChar == (char)43 || e.KeyChar == (char)45) && ((TextBox)sender).SelectionStart == 0) {
              }
              else {
                ((TextBox)sender).HideSelection = true;
                ((TextBox)sender).Select(((TextBox)sender).Text.Length, 0);
              }
            }
          }
        }
      }
      else
        e.Handled = true;
    }

    private void txtPrice_Leave(object sender, EventArgs e) {
      if (((TextBox)sender).Text == "") return;
      var xtoYLineRep = SelectedContest.XtoYLineRep;
      if (xtoYLineRep == "Y") {
        if (!((TextBox)sender).Text.Contains("to")) {
          MessageBox.Show(@"Odds must be in X to Y format", @"Invalid Odds");
          ((TextBox)sender).Focus();
        }
      }
      else {
        if (((TextBox)sender).Text.Contains("to")) {
          MessageBox.Show(@"Odds must be in American, Decimal or Fractional format", @"Invalid Odds");
          ((TextBox)sender).Focus();
        }
      }

      if (_selectedPrice == "Fractional" || _selectedPrice == "F") {
        int numerator;
        int denominator;
        if (int.TryParse(txtPrice.Text.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries)[0].Trim(), out numerator) && int.TryParse(txtPrice.Text.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries)[1].Trim(), out denominator)) {
          return;
        }
        MessageBox.Show(@"Odds must be in Fractional format", @"Invalid Odds");
        ((TextBox)sender).Focus();
        return;
      }

      var tryNumTxt = ((TextBox)sender).Text;
      int tryNum;
      int.TryParse(tryNumTxt.Replace(",", ""), out tryNum);
      if (tryNum == 0) {
        MessageBox.Show(@"Odds must be in American, Decimal or Fractional format", @"Invalid Odds");
        ((TextBox)sender).Focus();
      }

      if (FrmSportAndGameSelection.ShowWagerLimitsInFrm.IsActive()) {
        FrmSportAndGameSelection.ShowWagerLimitsInFrm.FrmManualContestWager = this;
        FrmSportAndGameSelection.ShowWagerLimitsInFrm.DisplayMaxManualContestWagerLimits();
      }
    }

    private void txtRisk_KeyUp(object sender, KeyEventArgs e) {
      if (((NumberTextBox)sender).Text != "") {
        if (SelectedAdjustment.Length > 0) {
          _selectedPrice = "American";
          if (txtPrice.Text.Contains("."))
            _selectedPrice = "Decimal";
          if (txtPrice.Text.Contains("/"))
            _selectedPrice = "Fractional";

          double adj;
          double convertedToAmerican;

          switch (_selectedPrice) {
            case "American":
              if (IsXtoY) {
                double toBase;
                if (double.TryParse(_selectedAdjustment, out adj) && double.TryParse(_selectedToBase, out toBase)) {
                  txtToWin.Text = TwUtilities.DetermineXtoYToWinAmount(double.Parse(((NumberTextBox)sender).Text), adj, toBase).ToString(CultureInfo.InvariantCulture);
                }
              }
              else {
                if (double.TryParse(txtPrice.Text, out adj)) {
                  txtToWin.Text = LineOffering.DetermineToWinAmount(double.Parse(((NumberTextBox)sender).Text), adj, Params.IncludeCents).ToString(CultureInfo.InvariantCulture);
                }
              }
              break;
            case "Decimal":
              if (double.TryParse(txtPrice.Text, out adj)) {
                convertedToAmerican = OddsTypes.DecimalToUs(adj);
                txtToWin.Text = LineOffering.DetermineToWinAmount(double.Parse(((NumberTextBox)sender).Text), convertedToAmerican, Params.IncludeCents).ToString(CultureInfo.InvariantCulture);
              }

              break;
            case "Fractional":
              int numerator;
              int denominator;
              if (int.TryParse(txtPrice.Text.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries)[0].Trim(), out numerator) && int.TryParse(txtPrice.Text.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries)[1].Trim(), out denominator)) {
                var convertedToDecimal = OddsTypes.FractionalToDecimal(numerator, denominator);
                convertedToAmerican = OddsTypes.DecimalToUs(convertedToDecimal);
                txtToWin.Text = LineOffering.DetermineToWinAmount(double.Parse(((NumberTextBox)sender).Text), convertedToAmerican, Params.IncludeCents).ToString(CultureInfo.InvariantCulture);
              }
              break;

          }
        }
      }
      else {
        txtToWin.Text = "";
      }
    }

    private void txtToWin_KeyUp(object sender, KeyEventArgs e) {
      if (((NumberTextBox)sender).Text != "") {
        if (SelectedAdjustment.Length > 0) {
          _selectedPrice = "American";
          if (txtPrice.Text.Contains("."))
            _selectedPrice = "Decimal";
          if (txtPrice.Text.Contains("/"))
            _selectedPrice = "Fractional";

          double adj;
          double convertedToAmerican;

          switch (_selectedPrice) {
            case "American":
              if (IsXtoY) {
                double toBase;
                if (double.TryParse(_selectedAdjustment, out adj) && double.TryParse(_selectedToBase, out toBase)) {
                  txtRisk.Text = TwUtilities.DetermineXtoYRiskAmount(double.Parse(((NumberTextBox)sender).Text), adj, toBase).ToString(CultureInfo.InvariantCulture);
                }
              }
              else {
                if (double.TryParse(txtPrice.Text, out adj)) {
                  txtRisk.Text = LineOffering.DetermineRiskAmount(double.Parse(((NumberTextBox)sender).Text), adj, Params.IncludeCents).ToString(CultureInfo.InvariantCulture);
                }
              }
              break;
            case "Decimal":
              if (double.TryParse(txtPrice.Text, out adj)) {
                convertedToAmerican = OddsTypes.DecimalToUs(adj);
                txtRisk.Text = LineOffering.DetermineToWinAmount(double.Parse(((NumberTextBox)sender).Text), convertedToAmerican, Params.IncludeCents).ToString(CultureInfo.InvariantCulture);
              }

              break;
            case "Fractional":
              int numerator;
              int denominator;
              if (int.TryParse(txtPrice.Text.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries)[0].Trim(), out numerator) && int.TryParse(txtPrice.Text.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries)[1].Trim(), out denominator)) {
                var convertedToDecimal = OddsTypes.FractionalToDecimal(numerator, denominator);
                convertedToAmerican = OddsTypes.DecimalToUs(convertedToDecimal);
                txtRisk.Text = LineOffering.DetermineToWinAmount(double.Parse(((NumberTextBox)sender).Text), convertedToAmerican, Params.IncludeCents).ToString(CultureInfo.InvariantCulture);
              }
              break;

          }
        }
      }
      else {
        txtRisk.Text = "";
      }
    }

    private void txtPrice_TextChanged(object sender, EventArgs e) {
      ClearAmountTextBoxes();
    }

    #endregion

    private void FrmManualContestWager_FormClosing(object sender, FormClosingEventArgs e) {

    }

  }
}