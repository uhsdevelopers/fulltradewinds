﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using GUILibraries.Utilities;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Utilities;
using TicketWriter.Properties;
using SIDLibraries.Entities;
using GUILibraries.Controls;
using GUILibraries.BusinessLayer;
using GUILibraries.Forms;
using Common = TicketWriter.Utilities.Common;

namespace TicketWriter.UI {
  public sealed partial class FrmPlaceIfBet : SIDForm {
    #region Public Properties

    public ActionReverse ActionReverse { get; private set; }

    // ReSharper disable once MemberCanBePrivate.Global
    public string ActiveMdiChildName { get; set; }

    public BirdCage BirdCage { get; private set; }

    public Boolean ContinueOnPush { private get; set; }

    private FrmSportAndGameSelection FrmSportAndGameSelection {
      get {
        return _frmSportAndGameSelection ??
               (_frmSportAndGameSelection =
                (FrmSportAndGameSelection)FormF.GetFormByName("FrmSportAndGameSelection", this));
      }
    }

    public IfBet IfBet { get; set; }

    private Boolean IsFreePlay { get; set; }

    private FormParameterList ParameterList {
      get {
        return _parameterList ??
               (_parameterList =
                ((MdiTicketWriter)(FormF.GetFormByName("MdiTicketWriter", this))).ParameterList);
      }
    }

    public String FrmMode { private get; set; }

    public int TicketNumberToUpd { private get; set; }
    public int WagerNumberToUpd { private get; set; }

    private int WagerItemNumberToUpd { get; set; }

    private MdiTicketWriter Mdi {
      get {
        return _mdiTicketWriter ??
               (_mdiTicketWriter = (MdiTicketWriter)FormF.GetFormByName("MdiTicketWriter", this));
      }
    }

    public int RelativeXPos { private get; set; }

    public int RelativeYPos { private get; set; }

    public String SpecialIfBetAmount { private get; set; }

    public int? CurrentPicks {
      get {
        if (IfBet == null || Mdi == null || Mdi.Ticket == null || Mdi.Ticket.WagerItems == null) return null;
        var itemsCount = (from a in Mdi.Ticket.WagerItems where a.TicketNumber == IfBet.TicketNumber && a.WagerNumber == IfBet.Number select a).Count();
        if (itemsCount == 0)
          return null;
        return itemsCount;
      }
    }

    public const int MaxPicks = 6;
    public bool IsSpecialIfBet { get; set; }

    public Boolean ItemFromMakeAWagerForm { private get; set; }

    #endregion

    #region Private vars

    private bool _deleteWagersOnExit = true;
    private FrmSportAndGameSelection _frmSportAndGameSelection;
    private FormParameterList _parameterList;

    private Boolean _editActionReverseFlag;
    private Boolean _editArbcFlag;
    private String _editRiskAmt;
    private String _editToWinAmt;

    private MdiTicketWriter _mdiTicketWriter;
    private Boolean _isFreePlayFlag;

    private Ticket.Wager _wagerEditBackup;
    private List<Ticket.WagerItem> _itemsBackupCopy;
    private List<Ticket.WagerAndItemNumber> _itemsToRemove;
    private bool _cancelActionPerformed;

    private const int AMNT_WAGERED_IDX = 1;
    private const int CELLIDX = 5;
    private const int FIXEDPRICE_IDX = 18;
    private const int GAMENUM_IDX = 10;

    private const int GROUPNUM_IDX = 14;
    private const int LAYOFF_IDX = 17;
    private const int PERIODNUM_IDX = 13;
    private const int PITCHER1_MSTART_IDX = 19;

    private const int PITCHER2_MSTART_IDX = 20;
    private const int PRICE_TYPE_IDX = 12;
    private const int SEL_BPOINTS_IDX = 9;

    private const int SEL_ODDS_IDX = 8;
    private const int TOWIN_IDX = 2;
    private const int WINUM_IDX = 7;
    private const int WAGERCUTOFF_IDX = 15;

    private const int WAGERINGMODE_IDX = 16;
    private const int WNUM_IDX = 6;
    private const int TNUM_IDX = 24;

    private const int WTYPE_MODE_IDX = 11;

    private const int TOTALS_OU_FLAG_IDX = 21;

    private const int SEL_AMERICAN_PRICE_IDX = 22;
    private const int SEL_GAME_PERIOD_DESCRIPTION = 23;

    #endregion

    #region Constructors

    public FrmPlaceIfBet(ModuleInfo moduleInfo)
      : base(moduleInfo) {
      InitializeComponent();
    }

    #endregion

    #region Public Methods

    public void AddItem(IfBetItem item) {
      bndsrc.Add(item);
      if (bndsrc.Count == 1) {
        if (FrmSportAndGameSelection.ShowWagerLimitsInFrm.IsActive()) {
          lblMaxRiskDisplay.Text = @"Max $:" + FormatNumber(FrmSportAndGameSelection.GetLimitForCurrentWager(GetLoo(item.TicketNumber, item.WagerNumber, item.Number), item.TypeCode, item.TypeCode), true, 100, false); //+FrmSportAndGameSelection.GetMaxWagerLimits(WagerType.PARLAY);
        }
      }
      if (FrmMode != "Edit") {
        dgvwIfBet.Refresh();
      }
      FillItemsNumbers();
    }

    private spGLGetActiveGamesByCustomer_Result GetLoo(int ticketNumber, int wagerNumber, int itemNumber) {
      spGLGetActiveGamesByCustomer_Result selectedGamePeriodInfo = null;
      if (Mdi.Ticket.WagerItems != null) {
        selectedGamePeriodInfo = (from wi in Mdi.Ticket.WagerItems where wi.TicketNumber == ticketNumber && wi.WagerNumber == wagerNumber && wi.ItemNumber == itemNumber select wi.Loo).FirstOrDefault();
      }
      return selectedGamePeriodInfo;
    }

    public void AdjustFrmPosition() {
      var xw = RelativeXPos - Width;
      var yw = RelativeYPos - Height - 10;


      StartPosition = FormStartPosition.Manual;
      Location = new Point(xw, yw);
    }

    private void RemoveItem(int ticketNumber, int wagerNumber, int itemNumber) {
      for (var i = 0; i < bndsrc.Count; i++) {
        var localItem = (IfBetItem)bndsrc[i];

        if (localItem.TicketNumber == ticketNumber && localItem.WagerNumber == wagerNumber && localItem.Number == itemNumber) {
          bndsrc.RemoveCurrent();
          break;
        }
      }
      dgvwIfBet.Refresh();
    }

    public void SetCursorInActionReverseCheckBox() {
      chbActionReverse.Select();
    }

    public void UpdateItem(IfBetItem item) {
      for (var i = 0; i < bndsrc.Count; i++) {
        var localItem = (IfBetItem)bndsrc[i];

        if (localItem.TicketNumber == item.TicketNumber && localItem.WagerNumber == item.WagerNumber && localItem.Number == item.Number) {
          bndsrc[i] = item;
          break;
        }
      }
      dgvwIfBet.Refresh();
      // CalculateToRiskAmount(); is this needed? ?
    }

    public void FillItemsNumbers() {
      if (ActionReverse == null && BirdCage == null) {
        var count = 0;
        foreach (IfBetItem item in bndsrc.List) {
          if (FrmMode != null && FrmMode.ToUpper() == "EDIT") {
            dgvwIfBet.Rows[count].Cells[1].Value = item.AmountWagered;
            dgvwIfBet.Rows[count].Cells[2].Value = item.ToWinAmount;

            // Refresh amounts in parallel structure in Mdi
            var tItem = Mdi.Ticket.WagerItems.FirstOrDefault(wi => wi.TicketNumber == item.TicketNumber && wi.WagerNumber == item.WagerNumber && wi.ItemNumber == item.Number);

            if (tItem != null) {
              tItem.AmountWagered = item.AmountWagered;
              tItem.ToWinAmount = item.ToWinAmount;
            }
          }
          else {
            dgvwIfBet.Rows[count].Cells[1].Value = item.FrmAmountWagered;
            dgvwIfBet.Rows[count].Cells[2].Value = item.FrmToWinAmount;

            // Refresh amounts in parallel structure in Mdi
            var tItem = Mdi.Ticket.WagerItems.FirstOrDefault(wi => wi.TicketNumber == item.TicketNumber && wi.WagerNumber == item.WagerNumber && wi.ItemNumber == item.Number);

            if (tItem != null) {
              tItem.AmountWagered = item.FrmAmountWagered;
              tItem.ToWinAmount = item.FrmToWinAmount;
            }
          }
          count++;
        }
        if (IfBet != null) {
          var tWager = (from w in Mdi.Ticket.Wagers where w.TicketNumber == IfBet.TicketNumber && w.WagerNumber == IfBet.WagerNumber select w).FirstOrDefault();
          if (tWager != null) {
            tWager.AmountWagered = 0;
            tWager.ToWinAmount = 0;
          }
        }
      }


      if (txtAmount.Text.Length == 0) {
        return;
      }
      double amount;

      if (!double.TryParse(txtAmount.Text, out amount)) {
        return;
      }

      var idx = 0;
      foreach (IfBetItem item in bndsrc.List) {
        double? riskAmount;
        double? toWinAmount;
        if (item.FinalMoney >= 0) {
          item.AmountWagered = amount;
          riskAmount = amount;
          toWinAmount = LineOffering.DetermineToWinAmount(amount, item.SelectedAmericanPrice, Params.IncludeCents);
          dgvwIfBet.Rows[idx].Cells[2].Value = Convert.ChangeType(toWinAmount, typeof(Single));

        }
        else {
          item.ToWinAmount = amount;
          toWinAmount = amount;
          riskAmount = LineOffering.DetermineRiskAmount(amount, item.SelectedAmericanPrice, Params.IncludeCents);
          dgvwIfBet.Rows[idx].Cells[1].Value = Convert.ChangeType(riskAmount, typeof(Single));
        }
        // Refresh amounts in parallel structure in Mdi
        var tItem = Mdi.Ticket.WagerItems.FirstOrDefault(wi => wi.TicketNumber == item.TicketNumber && wi.WagerNumber == item.WagerNumber && wi.ItemNumber == item.Number);

        if (tItem != null) {
          tItem.AmountWagered = (double)riskAmount;
          tItem.ToWinAmount = toWinAmount;
          var volAmount = new[] { tItem.AmountWagered, tItem.ToWinAmount };
          tItem.VolumeAmount = volAmount.Min();
        }
        idx++;
      }
      if (IfBet != null) {
        var tWager = (from w in Mdi.Ticket.Wagers where w.TicketNumber == IfBet.TicketNumber && w.WagerNumber == IfBet.WagerNumber select w).FirstOrDefault();
        if (tWager != null) {
          tWager.AmountWagered = 0;
          tWager.ToWinAmount = 0;
        }
      }
      dgvwIfBet.Refresh();
      SetMakeWagerValues();

      if (ActionReverse != null || BirdCage != null) {
        SetSpecialIfBetMode();
      }
    }

    #endregion

    #region Private Methods

    private void CancelWager(bool closeForm = true) {
      try {
        var mode = (FrmMode ?? "").Trim().ToUpper();
        if (IfBet != null && (FrmMode == null || mode != "EDIT" || (mode == "EDIT" && IfBet.TicketNumber != Mdi.TicketNumber)) && _deleteWagersOnExit) {
          Mdi.Ticket.DeleteWager(IfBet.TicketNumber, IfBet.Number);
        }

        Mdi.Ticket.RemoveCancelledWagerAndItems(FrmMode, _itemsToRemove, _wagerEditBackup, _itemsBackupCopy, null, _cancelActionPerformed);
        Mdi.WageringPanelName = null;

        if (mode != "EDIT" && IfBet != null) {
          WageringForm.Close(this, FrmMode, IfBet.TicketNumber, IfBet.Number, closeForm);
        }
        else {
          WageringForm.Close(this, FrmMode, null, null, closeForm);
        }
      }
      catch (Exception ex) {
        Log(ex);
      }
    }

    private Boolean CheckIfItIsCheckable(object sender) {

      if (FrmMode == "Edit" && (_editActionReverseFlag || _editArbcFlag))
        return true;

      var rowsCnt = dgvwIfBet.Rows.Count;

      if (rowsCnt > 1) {
        var chb = (CheckBox)sender;

        if (!chb.Checked) {
          return true;
        }
        if (Mdi.BalanceInfo.AvailableBalance != null) {
          var availableBalance = (double)Mdi.BalanceInfo.AvailableBalance;

          for (var j = 1; j < rowsCnt; j++) {
            double amtWagered = double.Parse(dgvwIfBet.Rows[j].Cells[1].Value.ToString());

            if (amtWagered > availableBalance / 100) {
              return false;
            }
          }
        }
      }
      return true;
    }

    private void EnableActionReverse() {
      ActionReverse = new ActionReverse(0);
      BirdCage = null;
      SetSpecialIfBetMode();
    }

    private void EnableBirdCage() {
      BirdCage = new BirdCage(0);
      ActionReverse = null;
      SetSpecialIfBetMode();
    }

    private List<WagerItem> GenerateItems(bool freezeItems = true) {
      return IfBet.Items.Select(item => new WagerItem {
        AmountWagered = item.AmountWagered,
        Description = item.Description,
        FinalMoney = item.FinalMoney,
        FixedPrice = item.FixedPrice,
        Frozen = freezeItems,
        GameNumber = item.GameNumber,
        LayoffWager = item.LayoffWager,
        Number = item.Number,
        PeriodNumber = item.PeriodNumber,
        PeriodWagerCutoff = item.PeriodWagerCutoff,
        Pitcher1MStart = item.Pitcher1MStart,
        Pitcher2MStart = item.Pitcher2MStart,
        PriceType = item.PriceType,
        RowGroupNum = item.RowGroupNum,
        SelectedAmountWagered = item.SelectedAmountWagered,
        SelectedBPointsOption = item.SelectedBPointsOption,
        SelectedCell = item.SelectedCell,
        SelectedOdds = item.SelectedOdds,
        //SelectedRow = item.SelectedRow,
        SelectedToWinAmount = item.SelectedToWinAmount,
        SportSubType = item.SportSubType,
        SportType = item.SportType,
        ToWinAmount = item.ToWinAmount,
        Type = item.Type,
        VolumeAmount = item.VolumeAmount,
        WageringMode = item.WageringMode,
        TicketNumber = item.TicketNumber,
        WagerNumber = item.WagerNumber,
        WagerTypeMode = item.WagerTypeMode
      }).ToList();
    }

    private void InitializeForm() {
      if (FrmMode == null || FrmMode.ToUpper() != "EDIT") {
        return;
      }
      _itemsToRemove = new List<Ticket.WagerAndItemNumber>();
      ItemFromMakeAWagerForm = false;
      _wagerEditBackup = Mdi.Ticket.CreateWagerSafeCopy(TicketNumberToUpd, WagerNumberToUpd);
      if ((FrmSportAndGameSelection.ReadbackItems != null && (FrmSportAndGameSelection.ReadbackItems.Any(rb => rb.TicketNumber == TicketNumberToUpd && rb.WagerNumber == WagerNumberToUpd)))) {
        _itemsBackupCopy = Mdi.Ticket.CreateWagerItemsListSafeCopy(TicketNumberToUpd, WagerNumberToUpd);
      }

      GetEditIfBetInfo();
      FillPlaceIfBetGvInEditMode();

      ActionReverse = null;
      BirdCage = null;

      chbContinueOnPush.CheckedChanged -= chbContinueOnPush_CheckedChanged;
      chbContinueOnPush.Checked = ContinueOnPush;
      chbContinueOnPush.CheckedChanged += chbContinueOnPush_CheckedChanged;

      if (_editActionReverseFlag) {
        if (IfBet.Number != null) {
          ActionReverse = new ActionReverse(IfBet.Number.Value);
        }
        chbActionReverse.Checked = _editActionReverseFlag;
        chbActionReverse.Enabled = false;
        chbARBC.Checked = _editArbcFlag;
        lblArDesc.Text = Resources.FrmPlaceIfBet_frmPlaceIfBet_Load__2_if_bets;

        chbContinueOnPush.Checked = true;
        chbContinueOnPush.Enabled = false;
        SetActionReverseMode(true);
      }
      else if (_editArbcFlag) {
        if (IfBet.Number != null) {
          BirdCage = new BirdCage(IfBet.Number.Value);
        }
        chbARBC.Checked = _editArbcFlag;
        chbARBC.Enabled = false;
        lblArDesc.Text = Resources.FrmPlaceIfBet_frmPlaceIfBet_Load__6_if_bets;

        chbContinueOnPush.Checked = true;
        chbContinueOnPush.Enabled = false;
        SetBirdCageMode(true);
      }
      SetSpecialIfBetMode();
      btnPlaceBet.Text = Resources.FrmPlaceTeaser_frmPlaceTeaser_Load_Modify_ + FrmSportAndGameSelection.SelectedWagerType;

      if (!string.IsNullOrEmpty(SpecialIfBetAmount)) {
        txtAmount.Text = SpecialIfBetAmount;
      }
      SetMakeWagerValues();
      if (FrmMode == "Edit" && TicketNumberToUpd != Mdi.TicketNumber) {
        chbActionReverse.Enabled = chbARBC.Enabled = false;
      }
    }

    private string ManageSpecialModes() {
      if (IfBet.ActionReverse == null && IfBet.BirdCage == null) {
        return "";
      }
      var message = "";

      if (Mdi.ActionReverseIfBets != null && Mdi.ActionReverseIfBets.Count > 0)
        Mdi.ActionReverseIfBets.RemoveAll(arif => arif.ArLink == IfBet.Number);
      if (Mdi.BirdCageIfBets != null && Mdi.BirdCageIfBets.Count > 0)
        Mdi.BirdCageIfBets.RemoveAll(bcif => bcif.ArBirdcageLink == IfBet.Number);

      var itemCount = 1;

      if (IfBet.ActionReverse != null) {
        if (bndsrc.List.Count > 2) {
          message = "Only two wagers are allowed for a valid action reverse bet.";
        }
        else {
          foreach (var ifBet in IfBet.ActionReverse.Combination.IfBets) {
            ifBet.RRARBCBaseWagerItemNumber = itemCount;
            if (Mdi.ActionReverseIfBets != null) Mdi.ActionReverseIfBets.Add(ifBet);
            itemCount++;
          }
        }
        if (Mdi.ActionReverseIfBets != null) Mdi.ActionReverseIfBets = Mdi.ActionReverseIfBets.OrderBy(x => x.Number).ThenBy(x => x.RRARBCBaseWagerItemNumber).ToList();
      }
      else if (IfBet.BirdCage != null) {
        if (bndsrc.List.Count < 3) {
          message = "Three or more wagers must be entered for a valid birdcage bet";
        }
        else {
          if (Mdi.BirdCageIfBets == null) {
            Mdi.BirdCageIfBets = new List<IfBet>();
          }
          foreach (var ifBet in IfBet.BirdCage.Combination.IfBets) {
            ifBet.RRARBCBaseWagerItemNumber = itemCount;
            Mdi.BirdCageIfBets.Add(ifBet);
            itemCount++;
          }
        }
        if (Mdi != null && Mdi.BirdCageIfBets != null)
          Mdi.BirdCageIfBets = Mdi.BirdCageIfBets.OrderBy(x => x.Number).ThenBy(x => x.RRARBCBaseWagerItemNumber).ToList();
      }
      return message;
    }

    private void PlaceBet() {
      var message = "";

      if (IfBet == null || !IfBet.Number.HasValue) {
        message = bndsrc.List.Count < 2 ? "At least two wagers must be entered for a valid if bet." : "";

        if (message.Length > 0) {
          MessageBox.Show(message, ParameterList.GetItem("AppName").Value, MessageBoxButtons.OK, MessageBoxIcon.Warning);
          _deleteWagersOnExit = true;
          return;
        }
        _deleteWagersOnExit = true;
        return;
      }
      IfBet.ContinueOnPush = chbContinueOnPush.Checked;

      if (message.Length == 0) {
        message = bndsrc.List.Count < 2 ? "At least two wagers must be entered for a valid if bet." : ManageSpecialModes();
      }
      if (message.Length > 0) {
        MessageBox.Show(message, ParameterList.GetItem("AppName").Value, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        _deleteWagersOnExit = true;
        return;
      }
      var wager = Mdi.Ticket.GetWager(IfBet.TicketNumber, IfBet.Number.Value);
      wager.ArLink = null;
      wager.ArBirdcageLink = null;
      double? amountWagered = null;
      double? toWinAmount = null;

      if (IfBet.AmountWagered != null) {
        //amountWagered = (double?)Math.Round((decimal)IfBet.AmountWagered / 100, 1) * 100;
        amountWagered = IfBet.AmountWagered;
        toWinAmount = IfBet.ToWinAmount;//(double?)Math.Round((decimal)IfBet.ToWinAmount / 100) * 100;

        if (IfBet.ActionReverse != null) {
          var wagered = IfBet.ActionReverse.Combination.IfBets.First().AmountWagered;

          if (wagered != null) {
            amountWagered = wagered.Value * 2;
          }
          toWinAmount = IfBet.ActionReverse.Combination.IfBets.First().ToWinAmount * 2;
          wager.ArLink = IfBet.Number;
          wager.ArItemsCount = 2;
          wager.SpecialIfBetAmt = txtAmount.Text;
        }
        else if (IfBet.BirdCage != null) {
          amountWagered = 0;
          toWinAmount = 0;
          wager.ArBirdcageLink = IfBet.Number;

          foreach (var ifbet in IfBet.BirdCage.Combination.IfBets) {
            if (ifbet.AmountWagered != null) {
              amountWagered += ifbet.AmountWagered.Value;
            }
            toWinAmount += ifbet.ToWinAmount;
          }
        }
        else {
          var ifBetDescription = IfBet.Items.Aggregate("", (current, item) => current + item.Description + Environment.NewLine);
          wager.Description = ifBetDescription.Trim();
        }
        wager.AmountWagered = amountWagered;
        wager.ToWinAmount = toWinAmount;
      }
      wager.TotalPicks = IfBet.Items.Count;
      wager.ContinueOnPushFlag = chbContinueOnPush.Checked ? "Y" : "N";
      wager.FreePlayFlag = FrmSportAndGameSelection.IsFreePlay ? "Y" : "N";
      wager.LayoffFlag = "N";
      wager.RifCurrentPlayFlag = null;
      wager.RifWinOnlyFlag = null;

      var minWagerBypassed = true;
      var availableFundsOk = true;
      var wagerLimitOk = true;

      if (IfBet.ActionReverse != null || IfBet.BirdCage != null) {
        bool continueValidating;

        string ifBetType = chbContinueOnPush.Checked ? "winOrPush" : "winOnly";

        var wagerDescription = IfBet.ActionReverse != null ? "ACTION REVERSE" : "BIRD CAGE";
        wagerDescription += " " + IfBet.Description;

        FrmSportAndGameSelection.CheckIfBelowMinimumWager(this, LoginsAndProfiles.ACCEPT_ANY_WAGER, amountWagered.ToString(), out continueValidating, out minWagerBypassed, true, IfBet.ActionReverse != null);
        if (!continueValidating) {
          _deleteWagersOnExit = true;
          return;
        }

        FrmSportAndGameSelection.CheckForAvailableFunds(this, FrmMode, TicketNumberToUpd, IfBet.Number, WagerItemNumberToUpd, LoginsAndProfiles.ACCEPT_ANY_WAGER, null, null, wagerDescription, amountWagered.ToString(), toWinAmount.ToString(), IsFreePlay, out continueValidating, out availableFundsOk, ifBetType, IfBet.Number, dgvwIfBet, true, true); //Validate if customer has sufficient Funds for wager

        if (!continueValidating) {
          _deleteWagersOnExit = true;
          return;
        }

        var chosenTeamId = "";
        var totalsOu = "";

        var checkNextItem = true;
        if (Mdi.Ticket.WagerItems.Count > 0) {
          foreach (var item in Mdi.Ticket.WagerItems.Where(wi => wi.TicketNumber == IfBet.TicketNumber)) {
            if (item.WagerNumber != IfBet.Number)
              continue;
            if (!checkNextItem) continue;
            int itemNumber = item.ItemNumber;
            string wagerItemDescription = item.Description;
            string wageredAmt = item.AmountWagered.ToString();
            string toWinAmt = item.ToWinAmount.ToString();
            string itemWagerType = WagerType.GetFromCode(item.WagerType).Code;
            if (item.ChosenTeamId != null) {
              chosenTeamId = item.ChosenTeamId.Trim();
            }

            if (item.TotalPointsOu != null) {
              totalsOu = item.TotalPointsOu.Trim();
            }

            spGLGetActiveGamesByCustomer_Result selectedGamePeriodInfo = (from a in FrmSportAndGameSelection.ActiveGames where a.GameNum == item.GameNum && a.PeriodNumber == item.PeriodNumber select a).FirstOrDefault();

            if (selectedGamePeriodInfo != null) {

              FrmSportAndGameSelection.CheckIfExceedsWagerLimit(this, FrmMode, IfBet.TicketNumber, IfBet.Number, itemNumber, LoginsAndProfiles.ACCEPT_ANY_WAGER, selectedGamePeriodInfo, wagerItemDescription, wageredAmt, toWinAmt, itemWagerType, chosenTeamId, totalsOu, out continueValidating, out wagerLimitOk, ifBetType, IfBet.Number, dgvwIfBet, true, true);
              if (!continueValidating)
                checkNextItem = false;
            }
          }
        }
      }

      if (!wagerLimitOk) {
        _deleteWagersOnExit = true;
        return;
      }

      if (minWagerBypassed && availableFundsOk && wagerLimitOk) {

        Mdi.Ticket.UpdateWager(wager);

        var wagerItems = Mdi.Ticket.GetWagerItems(IfBet.TicketNumber, IfBet.Number.Value);

        foreach (var wi in wagerItems) {
          var ifBetItem = IfBet.Items.FirstOrDefault(ibi => ibi.Number == wi.ItemNumber);

          if (ifBetItem == null) {
            throw new Exception("Unable to find a wager item with the item number #" + wi.ItemNumber.ToString(CultureInfo.InvariantCulture));
          }

          wi.AmountWagered = ifBetItem.AmountWagered;
          wi.ToWinAmount = ifBetItem.ToWinAmount;
          wi.FreePlayFlag = FrmSportAndGameSelection.IsFreePlay ? "Y" : "N";
          if (IfBet.ActionReverse != null)
            wi.ComplexIfBetAmount = txtAmount.Text;
        }
        Mdi.Ticket.UpdateWagerItems(wagerItems);

        FrmSportAndGameSelection.BuildReadbackLogInfoData(true);
        FrmSportAndGameSelection.PopulateReadbackLogInfoDgvw();
        Mdi.WageringPanelName = null;
        Mdi.LayoutManager.RemoveAllControls();

        WageringForm.Close(this);
      }
    }

    private void SetActionReverseMode(bool modeOn) {
      EnableActionReverse();
      var invisibleControls = new List<Control> { lblBcDesc };
      var visibleControls = new List<Control> { lblAmount, txtAmount, lblArDesc, lblMaxRiskDisplay };
      SetLayout(visibleControls, invisibleControls);
      SetLayout(modeOn);
      txtAmount.Select();
      txtAmount.Select(0, txtAmount.Text.Length);
      IsSpecialIfBet = modeOn;
    }

    private void SetBirdCageMode(bool modeOn) {
      EnableBirdCage();
      IsSpecialIfBet = modeOn;
      if (!modeOn) {
        return;
      }
      var visibleControls = new List<Control> { lblAmount, lblBcDesc, txtAmount, lblMaxRiskDisplay };
      var invisibleControls = new List<Control> { lblArDesc };
      SetLayout(visibleControls, invisibleControls);
      SetLayout(false);
      txtAmount.Select();
      txtAmount.Select(0, txtAmount.Text.Length);
    }

    private void SetLayout(IEnumerable<Control> visibleControls, IEnumerable<Control> invisibleControls) {
      if (invisibleControls != null) {
        foreach (var control in invisibleControls) {
          control.Visible = false;
        }
      }
      if (visibleControls != null) {
        foreach (var control in visibleControls) {
          control.Visible = true;
        }
      }
    }

    private void SetLayout(bool checkedControl) {
      chbActionReverse.Checked = checkedControl;
      chbActionReverse.Enabled = !checkedControl;
      chbARBC.Checked = !chbActionReverse.Checked;
      chbARBC.Enabled = !chbActionReverse.Enabled;
      chbContinueOnPush.Checked = true;
      chbContinueOnPush.Enabled = false;
      SetLayout();
    }

    private void SetLayout() {
      Mdi.LayoutManager.AddControls(new List<ManagedControl>
                {  new ManagedControl
                    {  Control = new GroupBox
                        { Name = "grpRiskAmount",
                          Visible = false },
                       OwnerFormName = "FrmMakeAWager" },
                   new ManagedControl
                    {  Control = new GroupBox
                        { Name = "grpWinAmount",
                          Visible = false },
                       OwnerFormName = "FrmMakeAWager" }});
    }

    private void SetMakeWagerValues() {
      Mdi.LayoutManager.AddControls(new List<ManagedControl>
                {  new ManagedControl
                    {  Control = new NumberTextBox
                        { DivideFlag = false,
                          Name = "txtToWin",
                          Text = txtAmount.Text },
                       OwnerFormName   = "FrmMakeAWager" }});
    }

    private void SetSpecialIfBetMode() {
      if (IfBet == null) return;

      IfBet.ActionReverse = ActionReverse;
      IfBet.BirdCage = BirdCage;

      if (IfBet.ActionReverse != null) {
        if (Mdi.ActionReverseIfBets == null) {
          Mdi.ActionReverseIfBets = new List<IfBet>();
        }

        IfBet.ActionReverse.TicketNumber = IfBet.TicketNumber;

        if (IfBet.Number != null) {
          IfBet.ActionReverse.InitialNumber = IfBet.Number.Value;
        }
        IfBet.ActionReverse.SeedItems = GenerateItems();
        IfBet.ActionReverse.ResetCombinations();
      }
      else if (IfBet.BirdCage != null) {
        IfBet.BirdCage.TicketNumber = IfBet.TicketNumber;
        if (IfBet.Number != null) {
          IfBet.BirdCage.InitialNumber = IfBet.Number.Value;
        }
        IfBet.BirdCage.SeedItems = GenerateItems();
        IfBet.BirdCage.ResetCombinations();

        if (IfBet.BirdCage.SeedItems.Count >= 3) {
          lblBcDesc.Text = IfBet.BirdCage.TotalIfBetsCount.ToString(CultureInfo.InvariantCulture) + Resources.FrmPlaceIfBet_UpdateIfBet__if_bets;
        }
      }
    }

    private void FillPlaceIfBetGvInEditMode() {
      if (bndsrc != null) {
        bndsrc.Clear();
      }
      else {
        bndsrc = new BindingSource();
      }
      RebuildIfBetWager(TicketNumberToUpd, WagerNumberToUpd);
      var existingItems = (from p in Mdi.Ticket.WagerItems where p.TicketNumber == TicketNumberToUpd && p.WagerNumber == WagerNumberToUpd select p).ToList();

      foreach (var wi in existingItems) {
        var item = new IfBetItem {
          Frozen = true,
          AmountWagered = wi.AmountWagered,
          ToWinAmount = wi.ToWinAmount,
          Description = wi.ShortDescription,
          FinalMoney = wi.FinalMoney,
          Number = wi.ItemNumber,
          PriceType = wi.PriceType,
          SportSubType = wi.SportSubType,
          SportType = wi.SportType
        };

        if (wi.GameNum != null) {
          item.GameNumber = (int)wi.GameNum;
        }
        if (wi.PeriodNumber != null) {
          item.PeriodNumber = (int)wi.PeriodNumber;
        }
        item.PeriodDescription = wi.PeriodDescription;

        item.TicketNumber = wi.TicketNumber;
        item.WagerNumber = wi.WagerNumber;
        item.Type = WagerType.GetFromCode(wi.WagerType);
        item.PriceType = wi.PriceType;

        item.SelectedCell = wi.SelectedCell;
        item.SelectedOdds = wi.SelectedOdds;
        item.SelectedAmericanPrice = wi.SelectedAmericanPrice;
        item.SelectedBPointsOption = wi.SelectedBPointsOption;
        item.SelectedAmountWagered = (wi.AmountWagered ?? 0).ToString(CultureInfo.InvariantCulture);
        item.SelectedToWinAmount = (wi.ToWinAmount ?? 0).ToString(CultureInfo.InvariantCulture);
        item.WagerTypeMode = wi.WagerTypeMode;
        item.RowGroupNum = wi.RowGroupNum;
        item.PeriodWagerCutoff = wi.PeriodWagerCutoff;
        item.WageringMode = wi.WageringMode;
        item.LayoffWager = wi.LayoffFlag;
        item.FixedPrice = (wi.AdjustableOddsFlag == "N" || string.IsNullOrEmpty(wi.AdjustableOddsFlag) ? "Y" : "N");
        item.Pitcher1MStart = wi.Pitcher1ReqFlag;
        item.Pitcher2MStart = wi.Pitcher2ReqFlag;
        item.TotalPointsOu = wi.TotalPointsOu;

        _isFreePlayFlag = wi.FreePlayFlag == "Y";

        if (FrmSportAndGameSelection.ReadbackItems != null) {
          var existingReadbackItem = (from p in FrmSportAndGameSelection.ReadbackItems where p.TicketNumber == TicketNumberToUpd && p.WagerNumber == WagerNumberToUpd && p.ItemNumber == wi.ItemNumber && p.RowContainsEditItemInfo select p).FirstOrDefault();

          if (existingReadbackItem != null) {
            /*item.SelectedRow = existingReadbackItem.WiEditInfo.SelectedRow;*/
            item.SelectedCell = existingReadbackItem.WiEditInfo.SelectedCell;
            item.SelectedOdds = existingReadbackItem.WiEditInfo.SelectedOdds;
            item.SelectedAmericanPrice = existingReadbackItem.WiEditInfo.SelectedAmericanPrice;
            item.SelectedBPointsOption = existingReadbackItem.WiEditInfo.SelectedBPointsOption;
            item.SelectedAmountWagered = existingReadbackItem.WiEditInfo.SelectedAmountWagered;
            item.SelectedToWinAmount = existingReadbackItem.WiEditInfo.SelectedToWinAmount;
            item.WagerTypeMode = existingReadbackItem.WiEditInfo.WagerTypeMode;
            item.RowGroupNum = existingReadbackItem.WiEditInfo.RowGroupNum;
            item.PeriodWagerCutoff = item.PeriodWagerCutoff;
            item.WageringMode = existingReadbackItem.WiEditInfo.WageringMode;
            item.LayoffWager = existingReadbackItem.WiEditInfo.LayoffWager;
            item.FixedPrice = existingReadbackItem.WiEditInfo.FixedPrice;
            item.Pitcher1MStart = existingReadbackItem.WiEditInfo.Pitcher1MStart;
            item.Pitcher2MStart = existingReadbackItem.WiEditInfo.Pitcher2MStart;
            item.TotalPointsOu = existingReadbackItem.WiEditInfo.TotalsOuFlag;
            _isFreePlayFlag = existingReadbackItem.FreePlayFlag == "Y";
          }
        }

        AddItem(item);
      }
      dgvwIfBet.Refresh();
    }

    private void GetEditIfBetInfo() {
      var wagerToEdit = (from p in Mdi.Ticket.Wagers where p.TicketNumber == TicketNumberToUpd && p.WagerNumber == WagerNumberToUpd select p).FirstOrDefault();

      if (wagerToEdit == null) return;
      _editRiskAmt = wagerToEdit.AmountWagered.ToString();
      _editToWinAmt = wagerToEdit.ToWinAmount.ToString();

      if (wagerToEdit.ArLink != null && (int)wagerToEdit.ArLink > 0) {
        _editActionReverseFlag = true;
        _editArbcFlag = false;
      }
      else if (wagerToEdit.ArBirdcageLink != null && (int)wagerToEdit.ArBirdcageLink > 0) {
        _editActionReverseFlag = false;
        _editArbcFlag = true;
      }
      if (IfBet == null) {
        FrmSportAndGameSelection.CreateIfBetWager(this, "Edit", TicketNumberToUpd, WagerNumberToUpd, new TextBox { Name = "txtRiskFromMakeWager", Text = _editRiskAmt },
          new TextBox { Name = "txtToWinMakeWager", Text = _editToWinAmt }, wagerToEdit.WagerTypeName, wagerToEdit.WagerType, wagerToEdit.FreePlayFlag == "Y",
          wagerToEdit.LayoffFlag);
      }
    }

    private void ModifyWagerItem() {
      if (!chbContinueOnPush.Checked && dgvwIfBet.Rows.Count > 1) {
        var currIdx = dgvwIfBet.SelectedRows[0].Index;
        if (currIdx < (dgvwIfBet.Rows.Count - 1)) {
          var amtWagered = double.Parse(dgvwIfBet.Rows[currIdx + 1].Cells[1].Value.ToString());
          double? availableBalance = null;

          if (Mdi.BalanceInfo != null && Mdi.BalanceInfo.AvailableBalance != null) {
            availableBalance = Mdi.BalanceInfo.AvailableBalance;
          }

          if (amtWagered > availableBalance / 100) {
            MessageBox.Show(Resources.FrmPlaceIfBet_ModifyWagerItem_Item_cannot_be_edited_because_its_win_amount_is_being_reinvested_in_a_subsequent_wager_, Resources.FrmPlaceIfBet_ModifyWagerItem_Unable_to_edit);
            return;
          }
        }
      }
      var dgvwR = dgvwIfBet.SelectedRows[0];

      const string wagerTypeMode = "ToWin";
      const string boxType = "IfBetBox";
      Common.ShowFormInEditMode(dgvwR, FrmSportAndGameSelection, wagerTypeMode, boxType, _isFreePlayFlag,
            AMNT_WAGERED_IDX, CELLIDX, FIXEDPRICE_IDX, GAMENUM_IDX, GROUPNUM_IDX, LAYOFF_IDX, PERIODNUM_IDX, PITCHER1_MSTART_IDX, PITCHER2_MSTART_IDX,
            PRICE_TYPE_IDX, SEL_AMERICAN_PRICE_IDX, SEL_BPOINTS_IDX, SEL_GAME_PERIOD_DESCRIPTION, SEL_ODDS_IDX, TOTALS_OU_FLAG_IDX, TOWIN_IDX,
            WAGERCUTOFF_IDX, WAGERINGMODE_IDX, TNUM_IDX, WINUM_IDX, WNUM_IDX, WTYPE_MODE_IDX);
    }

    private void DeleteWagerItem() {

      if (dgvwIfBet.SelectedRows.Count <= 0) return;

      var row = dgvwIfBet.SelectedRows[0];
      var wagerNumber = int.Parse(row.Cells[6].Value.ToString());
      var itemNumber = int.Parse(row.Cells[7].Value.ToString());
      var ticketNumber = int.Parse(row.Cells[24].Value.ToString());

      if (IfBet.TicketNumber != Mdi.TicketNumber) {
        var item = (from wi in Mdi.Ticket.WagerItems where wi.TicketNumber == ticketNumber && wi.WagerNumber == wagerNumber && wi.ItemNumber == itemNumber select wi).FirstOrDefault();
        if (item != null && item.Outcome != "P") {
          MessageBox.Show(@"Item cannot be removed from the database", @"Item not pending", MessageBoxButtons.OK);
          return;
        }
      }

      if (!chbContinueOnPush.Checked && dgvwIfBet.Rows.Count > 1) {
        var currIdx = dgvwIfBet.SelectedRows[0].Index;

        if (currIdx < (dgvwIfBet.Rows.Count - 1)) {
          var amtWagered = double.Parse(dgvwIfBet.Rows[currIdx + 1].Cells[1].Value.ToString());
          double? availableBalance = null;

          if (Mdi.BalanceInfo != null && Mdi.BalanceInfo.AvailableBalance != null) {
            availableBalance = Mdi.BalanceInfo.AvailableBalance;
          }
          if (amtWagered > availableBalance / 100) {
            MessageBox.Show(Resources.FrmPlaceIfBet_DeleteWagerItem_Item_cannot_be_deleted_because_its_win_amount_is_being_reinvested_in_a_subsequent_wager_, Resources.FrmPlaceIfBet_ModifyWagerItem_Unable_to_edit);
            return;
          }
        }
      }
      if (MessageBox.Show(Resources.FrmPlaceIfBet_deleteWagerItem_This_action_will_remove_the_bet_from_the_If_Bet_, Resources.FrmPlaceTeaser_DeleteWagerItem_remove_Wager, MessageBoxButtons.OKCancel) == DialogResult.OK) {
        //delete wi and fix itemnumbers;

        RemoveItem(ticketNumber, wagerNumber, itemNumber);
        Mdi.Ticket.DeleteWagerItem(ticketNumber, wagerNumber, itemNumber);
        Mdi.Ticket.ReIndexWagerItems(ticketNumber, wagerNumber);
      }
    }

    private IfBet RebuildIfBetWager(int ticketNumberRef, int wagerNumberRef) {
      var wager = (from p in Mdi.Ticket.Wagers where p.TicketNumber == ticketNumberRef && p.WagerNumber == wagerNumberRef select p).FirstOrDefault();

      if (wager == null) return null;
      var ifBet = new IfBet {
        ModuleName = ParameterList.GetItem("ModuleName").Value,
        Number = wager.WagerNumber,
        TicketNumber = wager.TicketNumber
      };

      IfBet = ifBet;
      return ifBet;
    }

    private void ResetBetType() {
      IfBet = null;

      Mdi.LayoutManager.AddControls(new List<ManagedControl>
                {  new ManagedControl
                    {  Control = new GroupBox { Name = "grpRiskAmount", Visible = true },
                       OwnerFormName = "FrmMakeAWager" },
                   new ManagedControl
                    {  Control = new GroupBox { Name = "grpWinAmount", Visible = true },
                       OwnerFormName = "FrmMakeAWager" },
                   new ManagedControl
                    {  Control = new NumberTextBox { DivideFlag = false, Name = "txtToWin", Text = "" },
                       OwnerFormName   = "FrmMakeAWager" }});

      Mdi.LayoutManager.RemoveAllControls();

      if (FrmSportAndGameSelection.SelectedWagerType != null) {
        FrmSportAndGameSelection.SelectedWagerType = "Straight Bet";
      }
      Mdi.ToggleFpAndRifChecks(true);
    }

    private void UpdateIfBet(object sender, ListChangedEventArgs e) {
      if (IfBet == null) {
        return;
      }
      switch (e.ListChangedType) {
        case ListChangedType.ItemAdded:
          IfBet.Items.Add((IfBetItem)((BindingSource)sender).List[e.NewIndex]);
          if (FrmMode == "Edit" && ItemFromMakeAWagerForm) {
            _itemsToRemove.Add(new Ticket.WagerAndItemNumber {
              GameNumber = ((IfBetItem)((BindingSource)sender).List[e.NewIndex]).GameNumber,
              PeriodNumber = ((IfBetItem)((BindingSource)sender).List[e.NewIndex]).PeriodNumber,
              TicketNumber = ((IfBetItem)((BindingSource)sender).List[e.NewIndex]).TicketNumber,
              WagerNumber = IfBet.Number ?? 0,
              ItemNumber = ((IfBetItem)((BindingSource)sender).List[e.NewIndex]).Number,
              ItemDescription = ((IfBetItem)((BindingSource)sender).List[e.NewIndex]).Description
            });
          }
          break;
        case ListChangedType.ItemChanged:
          IfBet.Items[e.NewIndex] = (IfBetItem)((BindingSource)sender).List[e.NewIndex];
          break;
        case ListChangedType.ItemDeleted:
          if (Mdi.TicketNumber != IfBet.TicketNumber) {
            Ticket.WagerItem wi = null;
            if (Mdi.Ticket.WagerItems != null)
              wi = (from wi2 in Mdi.Ticket.WagerItems where wi2.TicketNumber == IfBet.TicketNumber && wi2.WagerNumber == IfBet.Number && wi2.ItemNumber == (e.NewIndex + 1) select wi2).FirstOrDefault();
            Ticket.ContestWagerItem cwi = null;
            if (Mdi.Ticket.ContestWagerItems != null)
              cwi = (from wi3 in Mdi.Ticket.ContestWagerItems where wi3.TicketNumber == IfBet.TicketNumber && wi3.WagerNumber == IfBet.Number && wi3.ItemNumber == (e.NewIndex + 1) select wi3).FirstOrDefault();
            Mdi.Ticket.AddToItemsToRemoveFromDb("I", wi, cwi);
          }
          IfBet.Items.RemoveAt(e.NewIndex);
          break;
      }
      lblBcDesc.Text = "";

      SetSpecialIfBetMode();
    }

    #endregion

    #region Protected Methods

    protected override bool ProcessCmdKey(ref Message msg, Keys keyData) {
      var result = Mdi.ActivateMdiChildForm(keyData, base.ProcessCmdKey(ref msg, keyData), ActiveMdiChildName);
      return result;
    }

    #endregion

    #region Events

    private void frmPlaceIfBet_Load(object sender, EventArgs e) {
      InitializeForm();
    }

    private void bndsrc_ListChanged(object sender, ListChangedEventArgs e) {
      UpdateIfBet(sender, e);
    }

    private void btnCancel_Click(object sender, EventArgs e) {
      _cancelActionPerformed = true;

      if (FrmSportAndGameSelection.ReadbackItems != null &&
          FrmSportAndGameSelection.ReadbackItems.Any(p => p.TicketNumber == TicketNumberToUpd && p.WagerNumber == WagerNumberToUpd && p.RowContainsEditItemInfo)) {
        _deleteWagersOnExit = false;
      }

      CancelWager();
    }

    private void btnPlaceBet_Click(object sender, EventArgs e) {
      _deleteWagersOnExit = false;
      PlaceBet();
    }

    private void chbActionReverse_Click(object sender, EventArgs e) {
      SetActionReverseMode(chbActionReverse.Checked);
    }

    private void chbARBC_Click(object sender, EventArgs e) {
      SetBirdCageMode(chbARBC.Checked);
    }

    private void chbARBC_CheckedChanged(object sender, EventArgs e) {
      if (!((CheckBox)sender).Checked) {
        if (IfBet != null && IfBet.Number != null && Mdi.BirdCageIfBets != null && Mdi.BirdCageIfBets.Count > 0)
          Mdi.BirdCageIfBets.RemoveAll(bcif => bcif.ArBirdcageLink == IfBet.Number);
      }
    }

    private void chbActionReverse_CheckedChanged(object sender, EventArgs e) {
      if (!((CheckBox)sender).Checked) {
        if (IfBet != null && IfBet.Number != null && Mdi.ActionReverseIfBets != null && Mdi.ActionReverseIfBets.Count > 0)
          Mdi.ActionReverseIfBets.RemoveAll(arif => arif.ArLink == IfBet.Number);
      }
    }

    private void chbContinueOnPush_CheckedChanged(object sender, EventArgs e) {
      if (!CheckIfItIsCheckable(sender)) {
        MessageBox.Show(Resources.FrmPlaceIfBet_chbContinueOnPush_CheckedChanged_Option_not_allowed_because_some_item_s__reinvest_winnings_from_previous_items___The_if_bet_must_be_cancelled_and_re_entered_if_insufficient_funds_approval_is_desired_);
        ((CheckBox)sender).Checked = false;
      }

    }

    private void dgvwIfBet_CellDoubleClick(object sender, DataGridViewCellEventArgs e) {
      ModifyWagerItem();
    }

    private void dgvwIfBet_DataError(object sender, DataGridViewDataErrorEventArgs e) {
      e.Cancel = true;
    }

    private void editToolStripMenuItem_Click(object sender, EventArgs e) {
      ModifyWagerItem();
    }

    private void deleteToolStripMenuItem_Click(object sender, EventArgs e) {
      DeleteWagerItem();
    }

    private void FrmPlaceIfBet_FormClosed(object sender, FormClosedEventArgs e) {
      if (ActiveMdiChildName == null) {
        return;
      }
      foreach (var mdiChild in Mdi.MdiChildren) {
        if (mdiChild.Name.ToUpper() == ActiveMdiChildName.ToUpper()) {
          mdiChild.Select();
          break;
        }
      }
      _itemsToRemove = null;
      _wagerEditBackup = null;
      _itemsBackupCopy = null;
    }

    private void frmPlaceIfBet_FormClosing(object sender, FormClosingEventArgs e) {
      try {
        if (IfBet != null) {
          var amountsStr = " Risking: " + IfBet.AmountWagered + ", to Win:" + IfBet.ToWinAmount + ", adding " + (IfBet.Items.Count) + (IfBet.Items.Count == 1 ? " item" : " items");
          Tag = "Added " + FrmSportAndGameSelection.SelectedWagerType + " to Ticket in " + (String.IsNullOrEmpty(FrmMode) ? "New" : FrmMode) + " mode. Customer: " + FrmSportAndGameSelection.Customer.CustomerID.Trim() + " " +
                IfBet.TicketNumber + "." + (IfBet.WagerNumber ?? 0) + "." + amountsStr;
        }
      }
      catch (Exception ex) {
        Log(ex);
      }
      CancelWager(false);
      ResetBetType();
    }

    private void txtAmount_KeyDown(object sender, KeyEventArgs e) {
      if (e.KeyCode == Keys.Enter) {
        PlaceBet();
      }
    }

    private void txtAmount_KeyPress(object sender, KeyPressEventArgs e) {
      if (!KeyValidator.IsValidNumericKey(e)) {
        e.Handled = true;
      }
    }

    private void txtAmount_TextChanged(object sender, EventArgs e) {
      FillItemsNumbers();
    }

    #endregion
  }
}