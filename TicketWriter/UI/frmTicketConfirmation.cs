﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using GUILibraries.Utilities;
using TicketWriter.Properties;
using SIDLibraries.BusinessLayer;
using GUILibraries.Forms;
using SIDLibraries.Entities;

namespace TicketWriter.UI {
  public sealed partial class FrmTicketConfirmation : SIDForm {

    #region private Constants

    private const int CP_NOCLOSE_BUTTON = 0x200;

    #endregion

    #region Public Properties

    private MdiTicketWriter Mdi {
      get {
        return _mdiTicketWriter ??
               (_mdiTicketWriter = (MdiTicketWriter)FormF.GetFormByName("MdiTicketWriter", this));
      }
    }

    private FrmSportAndGameSelection FrmSportAndGameSelection {
      get {
        return _frmSportAndGameSelection ??
               (_frmSportAndGameSelection =
                (FrmSportAndGameSelection)FormF.GetFormByName("FrmSportAndGameSelection", this));
      }
    }

    #endregion

    #region Protected Properties

    protected override CreateParams CreateParams {
      get {
        var myCp = base.CreateParams;
        myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
        return myCp;
      }
    }

    #endregion

    #region Private vars

    private readonly MdiTicketWriter _parentFrm;
    private FrmSportAndGameSelection _frmSportAndGameSelection;
    private MdiTicketWriter _mdiTicketWriter;

    #endregion

    #region Constructors

    public FrmTicketConfirmation(SIDForm parentForm)
      : base(parentForm.AppModuleInfo) {
      _parentFrm = (MdiTicketWriter)parentForm;
      InitializeComponent();
    }

    #endregion

    #region Private Methods

    private void AddRollingIfBetTicketInfoFromReadback(String ifBetType) {
      var info = new RifBackwardTicketInfo.RifbTicketInfo();

      var row = dgvwReadbackInfo.SelectedRows[0];

      var ticketNumber = int.Parse(row.Cells[15].Value.ToString());
      var wagerNumber = int.Parse(row.Cells[4].Value.ToString());

      var wager = (from w in Mdi.Ticket.Wagers where w.TicketNumber == ticketNumber && w.WagerNumber == wagerNumber select w).FirstOrDefault();

      info.TicketNumber = ticketNumber;
      info.WagerNumber = wagerNumber;
      info.RifCurrentPlayFlag = "N";
      if (wager != null) {
        if (wager.AmountWagered != null) info.RifRiskAmount = (double)wager.AmountWagered;
        if (wager.ToWinAmount != null) info.RifToWinAmount = (double)wager.ToWinAmount;
      }

      switch (ifBetType) {
        case "If WIN Only":
          info.WinOnlyFlag = "Y";
          break;
        case "If WIN Or PUSH":
          info.WinOnlyFlag = "N";
          break;

      }

      info.LimitRifToRisk = Mdi.Customer.LimitRifToRiskFlag == null ? "N" : Mdi.Customer.LimitRifToRiskFlag.Trim();

      var parentAvailAmtTxt = (TextBox)Mdi.Controls.Find("txtAvailableAmount", true).FirstOrDefault();

      if (parentAvailAmtTxt != null)
        if (info.LimitRifToRisk == "Y") {
          parentAvailAmtTxt.Text = FormatNumber(info.RifRiskAmount) + @" *" + ifBetType + @"*";
        }
        else {
          parentAvailAmtTxt.Text = FormatNumber(info.RifRiskAmount + info.RifToWinAmount) + @" *" + ifBetType + @"*";
        }

      Mdi.RifBTicketInfo = info;

    }

    private void AppendRifOptionsToContextMenu(bool createNewRifOpt, bool showRifInfo) {
      if (ctxModifyWager == null) return;

      if (ctxModifyWager.Items.Count > 0 && (createNewRifOpt || showRifInfo)) {
        ctxModifyWager.Items.Add(new ToolStripSeparator());
      }

      ToolStripMenuItem mItem;

      if (createNewRifOpt) {
        mItem = new ToolStripMenuItem {
          Name = "rollingIFWINONLYToolStripMenuItem",
          Text = Resources.FrmTicketConfirmation_AppendRifOptionsToContextMenu_Rolling_IF_WIN_ONLY,

        };
        mItem.MouseUp += rifWinOnlyItem_MouseUp;
        ctxModifyWager.Items.Add(mItem);

        mItem = new ToolStripMenuItem {
          Name = "rollingIFWINORPUSHToolStripMenuItem",
          Text = Resources.FrmTicketConfirmation_AppendRifOptionsToContextMenu_Rolling_IF_WIN_OR_PUSH,

        };
        mItem.MouseUp += rifWinOrPushItem_MouseUp;
        ctxModifyWager.Items.Add(mItem);
      }

      if (showRifInfo) {
        mItem = new ToolStripMenuItem {
          Name = "rollingIFHookupToolStripMenuItem",
          Text = Resources.FrmTicketConfirmation_AppendRifOptionsToContextMenu_Rolling_IF_Hookup,

        };
        mItem.MouseUp += rifHookupItem_MouseUp;
        ctxModifyWager.Items.Add(mItem);
      }
    }

    private void BuildRifWinOnlyFromReadback() {
      Mdi.RIfBetFromReadback = "true";
      Mdi.ToggleRifCheckBox(true);
      Mdi.RollingIfBetMode = true;
      AddRollingIfBetTicketInfoFromReadback("If WIN Only");
    }

    private void BuildRifWinOrPushFromReadback() {
      Mdi.RIfBetFromReadback = "true";
      Mdi.ToggleRifCheckBox(true);
      Mdi.RollingIfBetMode = true;
      AddRollingIfBetTicketInfoFromReadback("If WIN Or PUSH");
    }

    private Boolean CheckIfRifBetIsEditable(int ticketNumber, int wagerNumber) {
      var wager = (from p in Mdi.Ticket.Wagers
                   where p.RifBackwardTicketNumber == ticketNumber.ToString(CultureInfo.InvariantCulture) &&
                       p.RifBackwardWagerNumber == wagerNumber.ToString(CultureInfo.InvariantCulture)
                   select p).FirstOrDefault();

      if (wager == null)
        return true;

      var rifLinkedWagerNumber = wager.WagerNumber;

      if (rifLinkedWagerNumber > 0) {
        MessageBox.Show(Resources.FrmTicketConfirmation_CheckIfRifBetIsEditable_The_rolling_if_bet_in_play_ + rifLinkedWagerNumber + Resources.FrmTicketConfirmation_CheckIfRifBetIsEditable__must_be_deleted_before_editing_this_bet_, Resources.FrmTicketConfirmation_CheckIfRifBetIsEditable_Unable_to_edit_RIF);
        return false;
      }

      wager = (from p in Mdi.Ticket.Wagers where p.TicketNumber == ticketNumber && p.WagerNumber == wagerNumber select p).FirstOrDefault();

      var rifBackwardTicketNumber = "";

      if (wager != null && wager.RifBackwardTicketNumber != null) rifBackwardTicketNumber = wager.RifBackwardTicketNumber;

      var rifBackwardWagerNumber = "";

      if (wager != null && wager.RifBackwardWagerNumber != "") rifBackwardWagerNumber = wager.RifBackwardWagerNumber;

      if (rifBackwardTicketNumber != "" && rifBackwardWagerNumber != "") {
        Mdi.RIfBetFromReadback = "true";
        Mdi.RollingIfBetMode = true;
        Mdi.ToggleRifCheckBox(true);
      }
      return true;
    }

    private void CreateBaseContextMenu() {
      if (ctxModifyWager == null) return;
      if (ctxModifyWager.Items.Count > 0)
        ctxModifyWager.Items.Clear();

      var mItem = new ToolStripMenuItem {
        Name = "editToolStripMenuItem",
        Text = Resources.FrmTicketConfirmation_CreateBaseContextMenu_Edit___,

      };
      mItem.MouseUp += editItem_MouseUp;
      ctxModifyWager.Items.Add(mItem);

      mItem = new ToolStripMenuItem {
        Name = "deleteToolStripMenuItem",
        Text = Resources.FrmTicketConfirmation_CreateBaseContextMenu_Delete,
      };
      mItem.MouseUp += deleteItem_MouseUp;
      ctxModifyWager.Items.Add(mItem);

      mItem = new ToolStripMenuItem {
        Name = "deleteAllToolStripMenuItem",
        Text = Resources.FrmTicketConfirmation_CreateBaseContextMenu_Delete_All,
      };
      mItem.MouseUp += deleteAllItem_MouseUp;
      ctxModifyWager.Items.Add(mItem);

    }

    private void DeleteSelectedWagerAndItems() {
      var row = dgvwReadbackInfo.SelectedRows[0];
      var gvWagerNumber = int.Parse(row.Cells[4].Value.ToString());
      var gvItemNumber = int.Parse(row.Cells[5].Value.ToString());
      var gvTicketNumber = int.Parse(row.Cells[15].Value.ToString());

      var gameNumber = int.Parse(row.Cells[8].Value.ToString());
      var rbList = FrmSportAndGameSelection.ReadbackItems;

      var rbListItem = (from p in rbList where p.TicketNumber == gvTicketNumber && p.WagerNumber == gvWagerNumber && p.ItemNumber == gvItemNumber select p).FirstOrDefault();

      if (rbListItem == null) {
        return;
      }
      var isFreePlay = "N";

      var periodNumber = rbListItem.WiEditInfo.PeriodNumber;

      var wagerType = "";
      if (Mdi.Ticket != null && Mdi.Ticket.Wagers != null) {
        var result = (from w in Mdi.Ticket.Wagers where w.TicketNumber == gvTicketNumber && w.WagerNumber == gvWagerNumber select new { wagerType = w.WagerType, wagerTypeName = w.WagerTypeName }).FirstOrDefault();
        if (result != null) {
          wagerType = result.wagerType;
        }

        if (wagerType == null)
          wagerType = "";

        isFreePlay = (from w in Mdi.Ticket.Wagers where w.TicketNumber == gvTicketNumber && w.WagerNumber == gvWagerNumber select w.FreePlayFlag).FirstOrDefault();
      }

      var periodCutoffTimeBypassed = true;
      spGLGetActiveGamesByCustomer_Result selectedGamePeriodInfo = null;
      spCnGetActiveContests_Result selectedContestInfo = null;

      switch (wagerType) {
        case SIDLibraries.BusinessLayer.WagerType.MANUALPLAY:
          if (MessageBox.Show(Resources.FrmTicketConfirmation_DeleteSelectedWagerAndItems_This_action_will_delete_wager_Number_ + gvWagerNumber, Resources.FrmTicketConfirmation_DeleteSelectedWagerAndItems_Delete_bet, MessageBoxButtons.OKCancel) == DialogResult.OK) {
            if (Mdi.Ticket != null) {
              Mdi.Ticket.DeleteWager(gvTicketNumber, gvWagerNumber);
              Mdi.Ticket.ReIndexWagers(gvTicketNumber, gvWagerNumber, Mdi.ActionReverseIfBets, Mdi.BirdCageIfBets);
            }

            if (dgvwReadbackInfo != null) {
              if (dgvwReadbackInfo.Rows.Count > 0) {
                dgvwReadbackInfo.Rows.Clear();
              }
            }
            FrmSportAndGameSelection.BuildReadbackLogInfoData(false);
            FrmSportAndGameSelection.PopulateReadbackLogInfoDgvw();
          }
          break;
        case SIDLibraries.BusinessLayer.WagerType.CONTEST:
          if (FrmSportAndGameSelection.ActiveContests != null) {
            selectedContestInfo = (from s in FrmSportAndGameSelection.ActiveContests where s.ContestNum == gameNumber && s.ContestantNum == rbListItem.CwiEditInfo.ContestantNum select s).FirstOrDefault();
          }

          FrmSportAndGameSelection.CheckIfContestWagerCutoffHasBeenReachedInReadback("Accept Any Wager", "Request to Delete a READBACK Item Past Post", selectedContestInfo, rbListItem.CwiEditInfo.ContestantDescription, rbListItem.CwiEditInfo.SelectedAmountWagered, rbListItem.CwiEditInfo.SelectedToWinAmount, out periodCutoffTimeBypassed);
          TakeActionIfPeriodCutoffTimeBypassed(periodCutoffTimeBypassed, gvTicketNumber, gvWagerNumber, Mdi, dgvwReadbackInfo);
          break;
        case SIDLibraries.BusinessLayer.WagerType.SPREAD:
        case SIDLibraries.BusinessLayer.WagerType.MONEYLINE:
        case SIDLibraries.BusinessLayer.WagerType.TOTALPOINTS:
        case SIDLibraries.BusinessLayer.WagerType.TEAMTOTALPOINTS:
          if (FrmSportAndGameSelection.FilteredActiveGames != null) {
            selectedGamePeriodInfo = (from s in FrmSportAndGameSelection.ActiveGames where s.GameNum == gameNumber && s.PeriodNumber == periodNumber select s).FirstOrDefault();
          }

          FrmSportAndGameSelection.CheckIfWagerCutoffHasBeenReachedInReadback("Accept Any Wager", "Request to Delete a READBACK Item Past Post", selectedGamePeriodInfo, rbListItem.WiEditInfo.Description, rbListItem.WiEditInfo.SelectedAmountWagered, rbListItem.WiEditInfo.SelectedToWinAmount, out periodCutoffTimeBypassed);
          TakeActionIfPeriodCutoffTimeBypassed(periodCutoffTimeBypassed, gvTicketNumber, gvWagerNumber, Mdi, dgvwReadbackInfo);
          break;
        case SIDLibraries.BusinessLayer.WagerType.PARLAY:
        case SIDLibraries.BusinessLayer.WagerType.TEASER:
        case SIDLibraries.BusinessLayer.WagerType.IFBET:
          List<Ticket.WagerItem> compoundWagerItems = null;
          if (wagerType == SIDLibraries.BusinessLayer.WagerType.PARLAY || wagerType == SIDLibraries.BusinessLayer.WagerType.TEASER) {
            if (Mdi.Ticket != null)
              compoundWagerItems = (from wi in Mdi.Ticket.WagerItems where wi.TicketNumber == gvTicketNumber && wi.WagerNumber == gvWagerNumber && string.IsNullOrEmpty(wi.FromPreviousTicket) select wi).ToList();
          }
          else {
            if (Mdi.Ticket != null)
              compoundWagerItems = (from wi in Mdi.Ticket.WagerItems where wi.TicketNumber == gvTicketNumber && wi.WagerNumber == gvWagerNumber select wi).ToList();
          }

          if (compoundWagerItems != null)
            foreach (var wi in compoundWagerItems) {
              if (wi.IsContest) {
                if (FrmSportAndGameSelection.ActiveContests != null) {
                  selectedContestInfo = (from s in FrmSportAndGameSelection.ActiveContests where s.ContestNum == wi.GameNum && s.ContestantNum == wi.ContestantNum select s).FirstOrDefault();
                }
                FrmSportAndGameSelection.CheckIfContestWagerCutoffHasBeenReachedInReadback("Accept Any Wager", "Request to Delete a READBACK Item Past Post",
                    selectedContestInfo, wi.ShortDescription, (wi.AmountWagered ?? 0).ToString(), (wi.ToWinAmount ?? 0).ToString(), out periodCutoffTimeBypassed);
              }
              else {
                if (FrmSportAndGameSelection.FilteredActiveGames != null) {
                  selectedGamePeriodInfo = (from s in FrmSportAndGameSelection.ActiveGames where s.GameNum == wi.GameNum && s.PeriodNumber == wi.PeriodNumber select s).FirstOrDefault();
                }

                FrmSportAndGameSelection.CheckIfWagerCutoffHasBeenReachedInReadback("Accept Any Wager", "Request to Delete a READBACK Item Past Post", selectedGamePeriodInfo, rbListItem.WiEditInfo.Description, rbListItem.WiEditInfo.SelectedAmountWagered, rbListItem.WiEditInfo.SelectedToWinAmount, out periodCutoffTimeBypassed);
              }
              if (!periodCutoffTimeBypassed)
                break;
            }

          if (periodCutoffTimeBypassed) {
            if (MessageBox.Show(Resources.FrmTicketConfirmation_DeleteSelectedWagerAndItems_This_action_will_delete_wager_Number_ + gvWagerNumber, Resources.FrmTicketConfirmation_DeleteSelectedWagerAndItems_Delete_bet, MessageBoxButtons.OKCancel) == DialogResult.OK) {
              if (Mdi.Ticket != null) {
                Mdi.Ticket.DeleteWager(gvTicketNumber, gvWagerNumber);
                Mdi.DeleteSpecialCompoundWager(wagerType, gvTicketNumber, gvWagerNumber);
                Mdi.Ticket.ReIndexWagers(gvTicketNumber, gvWagerNumber, Mdi.ActionReverseIfBets, Mdi.BirdCageIfBets);
              }

              if (dgvwReadbackInfo != null) {
                if (dgvwReadbackInfo.Rows.Count > 0) {
                  dgvwReadbackInfo.Rows.Clear();
                }
              }
              FrmSportAndGameSelection.BuildReadbackLogInfoData(false);
              FrmSportAndGameSelection.PopulateReadbackLogInfoDgvw();

              if ((Mdi.Ticket.Wagers == null || Mdi.Ticket.Wagers.Count() == 0) &&
                  (Mdi.Ticket.WagerItems == null || Mdi.Ticket.WagerItems.Count() == 0) &&
                  (Mdi.Ticket.ManualWagerItems == null || Mdi.Ticket.ManualWagerItems.Count() == 0) &&
                  (Mdi.Ticket.ContestWagerItems == null || Mdi.Ticket.ContestWagerItems.Count() == 0)) {
                if (dgvwReadbackInfo != null) {
                  if (dgvwReadbackInfo.Rows.Count > 0) {
                    dgvwReadbackInfo.Rows.Clear();
                  }
                }
              }
            }

          }
          break;
      }
      if (isFreePlay == "Y") {
        Mdi.FillCustomerDataTextBoxes("F");
      }
    }

    private void TakeActionIfPeriodCutoffTimeBypassed(bool periodCutoffTimeBypassed, int gvTicketNumber, int gvWagerNumber, MdiTicketWriter mdi, DataGridView readbackInfoGrid) {
      if (!periodCutoffTimeBypassed) return;
      if (MessageBox.Show(Resources.FrmTicketConfirmation_DeleteSelectedWagerAndItems_This_action_will_delete_wager_Number_ + gvWagerNumber, Resources.FrmTicketConfirmation_DeleteSelectedWagerAndItems_Delete_bet, MessageBoxButtons.OKCancel) != DialogResult.OK) return;
      if (mdi.Ticket != null) {
        mdi.Ticket.DeleteWager(gvTicketNumber, gvWagerNumber);
        mdi.Ticket.ReIndexWagers(gvTicketNumber, gvWagerNumber, mdi.ActionReverseIfBets, mdi.BirdCageIfBets);
      }

      if (readbackInfoGrid != null) {
        if (readbackInfoGrid.Rows.Count > 0) {
          readbackInfoGrid.Rows.Clear();
        }
      }
      FrmSportAndGameSelection.BuildReadbackLogInfoData(false);
      FrmSportAndGameSelection.PopulateReadbackLogInfoDgvw();
    }

    private List<ReadbackItem> FilterOutReadbackItemsForDisplayInHookup(List<ReadbackItem> filteredReadbackItems) {
      foreach (var rbi in filteredReadbackItems) {
        var ticketNumber = rbi.TicketNumber.ToString(CultureInfo.InvariantCulture);
        var wagerNumber = rbi.WagerNumber.ToString(CultureInfo.InvariantCulture);
        var rifTicketNumber = rbi.RifBackwardTicketNumber;
        var rifWagerNumber = rbi.RifBackwardWagerNumber;

        var linkedRifWager = (from p in FrmSportAndGameSelection.ReadbackItems where p.RifBackwardTicketNumber == ticketNumber && p.RifBackwardWagerNumber == wagerNumber select p).FirstOrDefault();

        if (rifTicketNumber == "" && rifWagerNumber == "" && linkedRifWager == null)
          rbi.ShowInHookupFrm = false;
      }

      return filteredReadbackItems.Where(p => p.ShowInHookupFrm).OrderBy(q => q.SeqNumber).ToList();
    }

    private void LoadParlayMakeAWagerFromSb(MouseEventArgs e) {
      var hti = dgvwReadbackInfo.HitTest(e.X, e.Y);

      if (hti.RowIndex < 0) {
        return;
      }

      dgvwReadbackInfo.Rows[hti.RowIndex].Selected = true;
      var row = dgvwReadbackInfo.SelectedRows[0];
      var gvTicketNumber = int.Parse(row.Cells[15].Value.ToString());

      if (gvTicketNumber != Mdi.TicketNumber)
        return;

      var wagerTypeName = row.Cells[3].Value.ToString();
      var wagerType = row.Cells[13].Value.ToString();

      if (wagerTypeName != "Straight Bet" || wagerType == SIDLibraries.BusinessLayer.WagerType.CONTEST) return;
      var gameNumber = int.Parse(row.Cells[8].Value.ToString());
      var gamePeriod = int.Parse(row.Cells[9].Value.ToString());
      var gvWagerNumber = int.Parse(row.Cells[4].Value.ToString());
      var gvItemNumber = int.Parse(row.Cells[5].Value.ToString());
      var isFreePlay = false;
      var rbList = FrmSportAndGameSelection.ReadbackItems;
      var rbListItem = (from p in rbList where p.TicketNumber == gvTicketNumber && p.WagerNumber == gvWagerNumber && p.ItemNumber == gvItemNumber select p).FirstOrDefault();

      if (rbListItem != null && rbListItem.WiEditInfo.Description == null) {
        row.Selected = false;
        return;
      }

      if (rbListItem == null) return;
      var rowGroupNum = rbListItem.WiEditInfo.RowGroupNum;
      var periodWagerCutoff = rbListItem.WiEditInfo.PeriodWagerCutoff;
      /*var rowIdx = rbListItem.WiEditInfo.SelectedRow;*/
      var cellIdx = rbListItem.WiEditInfo.SelectedCell;
      var ticketNumber = rbListItem.TicketNumber;
      var wagerNumber = rbListItem.WagerNumber;
      var itemNumber = rbListItem.ItemNumber;
      var selectedOdds = rbListItem.WiEditInfo.SelectedOdds;
      var selectedAmericanPrice = rbListItem.WiEditInfo.SelectedAmericanPrice;
      var selectedBPointsOption = FrmSportAndGameSelection.SelectedWagerType != "Teaser" ? rbListItem.WiEditInfo.SelectedBPointsOption : rbListItem.WiEditInfo.SelectedOdds.Split(' ')[0];
      var amtWagered = rbListItem.WiEditInfo.SelectedAmountWagered;
      var toWinAmt = rbListItem.WiEditInfo.SelectedToWinAmount;
      var wagerTypeMode = rbListItem.WiEditInfo.WagerTypeMode;

      var priceType = rbListItem.WiEditInfo.PriceType;
      if (rbListItem.FreePlayFlag == "Y")
        isFreePlay = true;
      var wageringMode = rbListItem.WiEditInfo.WageringMode;
      var wagerLayoff = rbListItem.WiEditInfo.LayoffWager;
      var fixedPrice = rbListItem.WiEditInfo.FixedPrice;
      var pitcher1MStart = rbListItem.WiEditInfo.Pitcher1MStart;
      var pitcher2MStart = rbListItem.WiEditInfo.Pitcher2MStart;
      var totalsOuFlag = rbListItem.WiEditInfo.TotalsOuFlag;

      FrmSportAndGameSelection.ParlayOrTeaserItemFromSb = true;

      foreach (DataGridViewRow trow in dgvwReadbackInfo.Rows) {
        trow.Selected = false;
      }

      var currentGameAndPeriod = (from g in FrmSportAndGameSelection.ActiveGames where g.GameNum == gameNumber && g.PeriodNumber == gamePeriod select g).FirstOrDefault();

      if (currentGameAndPeriod == null) return;
      var sportType = currentGameAndPeriod.SportType;
      var sportSubType = currentGameAndPeriod.SportSubType;

      switch (FrmSportAndGameSelection.SelectedWagerType) {
        case "Teaser":
          if (FrmSportAndGameSelection.IsWagerTypeAvailable(wagerType, sportType, sportSubType) && gamePeriod == 0) {
            FrmSportAndGameSelection.LoadMakeAWagerFrm(gameNumber, gamePeriod, currentGameAndPeriod.PeriodDescription, rowGroupNum, periodWagerCutoff, /*rowIdx,*/ cellIdx, "Edit", ticketNumber, wagerNumber, itemNumber, selectedOdds, selectedAmericanPrice,
              selectedBPointsOption, amtWagered, toWinAmt, wagerTypeMode, isFreePlay, priceType, wageringMode, wagerLayoff, fixedPrice, pitcher1MStart, pitcher2MStart, "ReadBack", totalsOuFlag);
          }
          else {
            MessageBox.Show(Resources.FrmSportAndGameSelection_dgvwGame_CellClick_Game_selection_not_allowed_in_selected_teaser_);
          }
          break;
        case "Parlay":
          if (currentGameAndPeriod.ParlayRestriction == GamesAndLines.PARLAY_DENY_ALL) {
            var message = "This game cannot be used in " + FrmSportAndGameSelection.SelectedWagerType + "s.";
            MessageBox.Show(message, FrmSportAndGameSelection.ParameterList.GetItem("AppName").Value, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            return;
          }
          var chosenTeamId = "";
          switch (rowGroupNum) {
            case 1:
              chosenTeamId = currentGameAndPeriod.Team1ID;
              break;
            case 2:
              chosenTeamId = currentGameAndPeriod.Team2ID;
              break;
            case 3:
              chosenTeamId = "";
              break;
          }

          if ((currentGameAndPeriod.SportType.Trim() == SportTypes.SOCCER || currentGameAndPeriod.SportType.Trim() == SportTypes.HOCKEY) && TwUtilities.IsAsianLine(wagerType, chosenTeamId, currentGameAndPeriod)) {
            var message = "Asian handicap not allowed in " + FrmSportAndGameSelection.SelectedWagerType + "s.";
            MessageBox.Show(message, FrmSportAndGameSelection.ParameterList.GetItem("AppName").Value, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            return;
          }

          if (isFreePlay) {
            var mdiFreePlayCheckBox = (CheckBox)FrmSportAndGameSelection.Mdi.Controls.Find("chbFreePlay", true).FirstOrDefault();
            if (mdiFreePlayCheckBox != null && !mdiFreePlayCheckBox.Checked)
              mdiFreePlayCheckBox.Checked = true;
          }

          FrmSportAndGameSelection.LoadMakeAWagerFrm(gameNumber, gamePeriod, currentGameAndPeriod.PeriodDescription, rowGroupNum, periodWagerCutoff, /*rowIdx,*/ cellIdx, "Edit", ticketNumber, wagerNumber, itemNumber, selectedOdds, selectedAmericanPrice,
            selectedBPointsOption, amtWagered, toWinAmt, wagerTypeMode, isFreePlay, priceType, wageringMode, wagerLayoff, fixedPrice, pitcher1MStart, pitcher2MStart, "ReadBack", totalsOuFlag);
          break;
      }
    }

    private bool NoOtherFormIsOpen() {
      var noneVisible = true;
      var formNamesList = new[] { "FrmMakeAWager", "frmPlaceManual", "frmPlaceIfBet", "frmPlaceParlay", "frmPlaceTeaser", "frmPayCard" };

      foreach (var frm in formNamesList.Select(frmName => FormF.GetFormByName(frmName, this)).Where(frm => frm != null).Where(frm => frm.Visible)) {
        noneVisible = !frm.Visible;
        break;
      }

      return noneVisible;
    }

    private void PrepareContextMenuToShow(object sender, MouseEventArgs e) {
      if (e.Button != MouseButtons.Right || ((DataGridView)sender).SelectedRows.Count == 0) {
        if (ctxModifyWager.Items.Count > 0)
          ctxModifyWager.Items.Clear();
        return;
      }


      var rowObj = ((DataGridView)sender).SelectedRows[0];
      var rowIsClickable = rowObj.Cells[6].Value.ToString().ToLower();

      var rifBTicketNumber = rowObj.Cells[10].Value;
      //var rifBWagerNumber = rowObj.Cells[11].Value;
      var currentTicketNumber = rowObj.Cells[15].Value;
      var currentWagerNumber = rowObj.Cells[4].Value;
      var isFreePlay = rowObj.Cells[12].Value;

      if (rowIsClickable == "false") {
        if (ctxModifyWager.Items.Count > 0)
          ctxModifyWager.Items.Clear();
      }
      else {
        CreateBaseContextMenu();
        var wagerTypeName = rowObj.Cells[3].Value.ToString();
        if (wagerTypeName == "Manual Play" || isFreePlay == null || isFreePlay.ToString() != "N" || (int.Parse(currentTicketNumber.ToString()) != Mdi.TicketNumber)) return;
        var createNewRifOpt = ShowRifOptions(currentTicketNumber, currentWagerNumber);
        var showRifInfo = ShowRifInfoFrm(rifBTicketNumber, currentTicketNumber, currentWagerNumber);
        AppendRifOptionsToContextMenu(createNewRifOpt, showRifInfo);
      }
    }

    private void RemoveAllWagersFromReadback() {
      // this method clears all reaback info, along with _wagers, wagerItems and readbackItems objects

      Boolean cutoffTimeBypassed;
      spCnGetActiveContests_Result selectedContestInfo = null;
      spGLGetActiveGamesByCustomer_Result selectedGamePeriodInfo = null;

      if (Mdi.Ticket.WagerItems != null) {
        foreach (var wi in Mdi.Ticket.WagerItems.Where(wi2 => wi2.Outcome == "P")) {
          if (!string.IsNullOrEmpty(wi.FromPreviousTicket) || !string.IsNullOrEmpty(wi.FromPreviousWagerNumber))
            continue;
          var gameNumber = (wi.GameNum ?? 0);
          var periodNumber = wi.PeriodNumber ?? 0;

          if (wi.IsContest) {
            var contestNumber = wi.GameNum ?? 0;
            var contestantNumber = wi.ContestantNum ?? 0;

            if (FrmSportAndGameSelection.ActiveContests != null) {
              selectedContestInfo = (from s in FrmSportAndGameSelection.ActiveContests where s.ContestNum == contestNumber && s.ContestantNum == contestantNumber select s).FirstOrDefault();
            }

            FrmSportAndGameSelection.CheckIfContestWagerCutoffHasBeenReachedInReadback("Accept Any Wager", "Request to Delete a READBACK Item Past Post", selectedContestInfo, wi.ShortDescription, (wi.AmountWagered ?? 0).ToString(), (wi.ToWinAmount ?? 0).ToString(), out cutoffTimeBypassed);
          }
          else {
            if (FrmSportAndGameSelection.FilteredActiveGames != null) {
              selectedGamePeriodInfo = (from s in FrmSportAndGameSelection.ActiveGames where s.GameNum == gameNumber && s.PeriodNumber == periodNumber select s).FirstOrDefault();
            }

            FrmSportAndGameSelection.CheckIfWagerCutoffHasBeenReachedInReadback("Accept Any Wager", "Request to Delete a READBACK Item Past Post", selectedGamePeriodInfo, wi.Description, wi.AmountWagered.ToString(), wi.ToWinAmount.ToString(), out cutoffTimeBypassed);
          }
          if (!cutoffTimeBypassed)
            return;
        }
      }

      if (Mdi.Ticket.ContestWagerItems != null) {
        foreach (var ci in Mdi.Ticket.ContestWagerItems) {
          var contestNumber = ci.ContestNum ?? 0;
          var contestantNumber = ci.ContestantNum ?? 0;

          if (FrmSportAndGameSelection.ActiveContests != null) {
            selectedContestInfo = (from s in FrmSportAndGameSelection.ActiveContests where s.ContestNum == contestNumber && s.ContestantNum == contestantNumber select s).FirstOrDefault();
          }

          FrmSportAndGameSelection.CheckIfContestWagerCutoffHasBeenReachedInReadback("Accept Any Wager", "Request to Delete a READBACK Item Past Post", selectedContestInfo, ci.ContestDesc, ci.AmountWagered.ToString(), ci.ToWinAmount.ToString(), out cutoffTimeBypassed);
          if (!cutoffTimeBypassed)
            return;
        }
      }

      if (
        MessageBox.Show(
          Resources.FrmTicketConfirmation_RemoveAllWagersFromReadback_This_action_will_delete_all_bets_,
          Resources.FrmTicketConfirmation_RemoveAllWagersFromReadback_Delete_all_bets, MessageBoxButtons.OKCancel) !=
        DialogResult.OK) return;
      if (Mdi.Ticket.Wagers != null) {
        Mdi.Ticket.Wagers.Clear();
        Mdi.Ticket.Wagers = null;
        Mdi.Ticket.OriginalGameOptions = null;
        Mdi.Ticket.OriginalContestOptions = null;
      }

      if (Mdi.Ticket.WagerItems != null) {
        if (Mdi.Ticket.WagerItems.Count > 0) {
          Mdi.Ticket.WagerItems.Clear();
        }
        Mdi.Ticket.WagerItems = null;
      }

      if (Mdi.Ticket.ContestWagerItems != null) {
        if (Mdi.Ticket.ContestWagerItems.Count > 0) {
          Mdi.Ticket.ContestWagerItems.Clear();
        }
        Mdi.Ticket.ContestWagerItems = null;
      }

      if (Mdi.Ticket.ManualWagerItems != null) {
        if (Mdi.Ticket.ManualWagerItems.Count > 0) {
          Mdi.Ticket.ManualWagerItems.Clear();
        }
        Mdi.Ticket.ManualWagerItems = null;
      }

      if (FrmSportAndGameSelection.ReadbackItems != null) {
        if (FrmSportAndGameSelection.ReadbackItems.Count > 0) {
          FrmSportAndGameSelection.ReadbackItems.Clear();
        }
        FrmSportAndGameSelection.ReadbackItems = null;
      }

      if (Mdi.ActionReverseIfBets != null)
        Mdi.ActionReverseIfBets.Clear();
      if (Mdi.BirdCageIfBets != null)
        Mdi.BirdCageIfBets.Clear();
      if (Mdi.Ticket.RoundRobinParlays != null)
        Mdi.Ticket.RoundRobinParlays.Clear();
      if (Mdi.Ticket.RoundRobinTeasers != null)
        Mdi.Ticket.RoundRobinTeasers.Clear();

      if (dgvwReadbackInfo == null) return;
      if (dgvwReadbackInfo.Rows.Count > 0) {
        dgvwReadbackInfo.Rows.Clear();
      }
    }

    private bool ShowRifInfoFrm(object rifBTicketNumber, object currentTicketNumber, object currentWagerNumber) {

      if (int.Parse(currentTicketNumber.ToString()) != Mdi.TicketNumber)
        return false;

      var rCnt = (from p in FrmSportAndGameSelection.ReadbackItems
                  where p.RifBackwardWagerNumber == currentWagerNumber.ToString()
                  select p).FirstOrDefault();

      return (rifBTicketNumber != null && rifBTicketNumber.ToString() != "") || rCnt != null;
    }

    private bool ShowRifOptions(object currentTicketNumber, object currentWagerNumber) {
      if (int.Parse(currentTicketNumber.ToString()) != Mdi.TicketNumber)
        return false;

      var rCnt = (from p in FrmSportAndGameSelection.ReadbackItems
                  where p.RifBackwardWagerNumber == currentWagerNumber.ToString()
                  select p).FirstOrDefault();

      if (rCnt != null)
        return false;

      return true;
    }

    private void ShowRifHookupFrm() {
      if (dgvwReadbackInfo.SelectedRows.Count <= 0) return;
      var readbackItemsSubList = (from p in FrmSportAndGameSelection.ReadbackItems where p.RowContainsEditItemInfo && p.ShowInHookupFrm && p.WagerTypeName != "Manual Play" select p).OrderBy(q => q.SeqNumber).ToList();

      var hookupFrm = new FrmRollingIfBetHookup(_parentFrm.TicketNumber, int.Parse(dgvwReadbackInfo.SelectedRows[0].Cells[4].Value.ToString()), dgvwReadbackInfo.SelectedRows[0].Cells[10].Value.ToString(), dgvwReadbackInfo.SelectedRows[0].Cells[11].Value.ToString(), FilterOutReadbackItemsForDisplayInHookup(readbackItemsSubList), Mdi.AppModuleInfo, true) {
        Icon = Icon
      };
      hookupFrm.ShowDialog();
    }

    private void ShowRequiredFormInEditMode() {
      if (dgvwReadbackInfo.SelectedRows.Count == 0) {
        return;
      }
      var row = dgvwReadbackInfo.SelectedRows[0];

      var gameNumber = int.Parse(row.Cells[8].Value.ToString());
      var periodNumber = int.Parse(row.Cells[9].Value.ToString());
      var wagerTypeName = row.Cells[3].Value.ToString();
      var wagerType = row.Cells[13].Value.ToString();

      var gvTicketNumber = int.Parse(row.Cells[15].Value.ToString());
      var gvWagerNumber = int.Parse(row.Cells[4].Value.ToString());

      if (wagerTypeName == "Straight Bet" && wagerType != SIDLibraries.BusinessLayer.WagerType.CONTEST) {
        if (!CheckIfRifBetIsEditable(gvTicketNumber, gvWagerNumber))
          return;
      }

      var gvItemNumber = int.Parse(row.Cells[5].Value.ToString());


      var rbList = FrmSportAndGameSelection.ReadbackItems;

      ReadbackItem rbListItem;

      spGLGetActiveGamesByCustomer_Result selectedGamePeriodInfo = null;

      spCnGetActiveContests_Result selectedContestInfo = null;
      var isFreePlay = false;
      var mdiFreePlayCheckBox = (CheckBox)FrmSportAndGameSelection.Mdi.Controls.Find("chbFreePlay", true).FirstOrDefault();

      switch (wagerTypeName) {
        case "Straight Bet":
          rbListItem = (from p in rbList where p.TicketNumber == gvTicketNumber && p.WagerNumber == gvWagerNumber && p.ItemNumber == gvItemNumber select p).FirstOrDefault();

          if (rbListItem == null) return;
          var ticketNumber = rbListItem.TicketNumber;
          var wagerNumber = rbListItem.WagerNumber;
          var itemNumber = rbListItem.ItemNumber;
          var periodDescription = rbListItem.PeriodDescription;
          bool cutoffTimeBypassed;
          if (wagerType != SIDLibraries.BusinessLayer.WagerType.CONTEST) {
            var gamesGv = (DataGridView)FrmSportAndGameSelection.Controls.Find(FrmSportAndGameSelection.GamesGridViewInUse, true).FirstOrDefault();

            if (gamesGv != null) {
              /*var rowIdx = rbListItem.WiEditInfo.SelectedRow;*/
              var cellIdx = rbListItem.WiEditInfo.SelectedCell;
              var selectedOdds = rbListItem.WiEditInfo.SelectedOdds;
              var selectedAmericanPrice = rbListItem.WiEditInfo.SelectedAmericanPrice;
              var selectedBPointsOption = rbListItem.WiEditInfo.SelectedBPointsOption;
              var amtWagered = rbListItem.WiEditInfo.SelectedAmountWagered;
              var toWinAmt = rbListItem.WiEditInfo.SelectedToWinAmount;
              var wagerTypeMode = rbListItem.WiEditInfo.WagerTypeMode;
              var priceType = rbListItem.WiEditInfo.PriceType;
              if (rbListItem.FreePlayFlag == "Y")
                isFreePlay = true;

              var rowGroupNum = rbListItem.WiEditInfo.RowGroupNum;
              var periodWagerCutoff = rbListItem.WiEditInfo.PeriodWagerCutoff;
              var wageringMode = rbListItem.WiEditInfo.WageringMode;
              var wagerLayoff = rbListItem.WiEditInfo.LayoffWager;
              var fixedPrice = rbListItem.WiEditInfo.FixedPrice;
              var pitcher1MStart = rbListItem.WiEditInfo.Pitcher1MStart;
              var pitcher2MStart = rbListItem.WiEditInfo.Pitcher2MStart;
              var totalsOuFlag = rbListItem.WiEditInfo.TotalsOuFlag;

              if (FrmSportAndGameSelection.FilteredActiveGames != null) {
                selectedGamePeriodInfo = (from s in FrmSportAndGameSelection.ActiveGames where s.GameNum == gameNumber && s.PeriodNumber == periodNumber select s).FirstOrDefault();
              }
              FrmSportAndGameSelection.CheckIfWagerCutoffHasBeenReachedInReadback("Accept Any Wager", "Request to Edit a READBACK Item Past Post", selectedGamePeriodInfo, rbListItem.WiEditInfo.Description, rbListItem.WiEditInfo.SelectedAmountWagered, rbListItem.WiEditInfo.SelectedToWinAmount, out cutoffTimeBypassed);

              if (cutoffTimeBypassed) {
                if (mdiFreePlayCheckBox != null && !mdiFreePlayCheckBox.Checked && isFreePlay) {
                  if (MessageBox.Show(@"Must be in FREE PLAY mode to edit this bet." + Environment.NewLine +
                                              @"Do you want to switch to free play mode and continue?", @"Free Play Mode Required", MessageBoxButtons.YesNo) == DialogResult.Yes) {
                    mdiFreePlayCheckBox.Checked = true;
                  }
                  else {
                    isFreePlay = false;
                  }
                }

                if (mdiFreePlayCheckBox!= null && mdiFreePlayCheckBox.Checked && !isFreePlay) {
                  if (MessageBox.Show(@"Do you want to switch to FREE PLAY mode to Edit the wager?", @"Free Play Mode Required", MessageBoxButtons.YesNo) == DialogResult.Yes) {
                    isFreePlay = true;
                  }
                  else {
                    mdiFreePlayCheckBox.Checked = false;
                  }
                }

                FrmSportAndGameSelection.ShowFormInEditMode(gameNumber, periodNumber, periodDescription, rowGroupNum, periodWagerCutoff, /*rowIdx,*/ cellIdx, ticketNumber, wagerNumber, itemNumber,
                selectedOdds, selectedAmericanPrice, selectedBPointsOption, amtWagered, toWinAmt, wagerTypeMode, isFreePlay, priceType, wageringMode, wagerLayoff,
                fixedPrice, pitcher1MStart, pitcher2MStart, "ReadBack", totalsOuFlag);
              }
            }
          }
          else {
            var contestNumber = rbListItem.CwiEditInfo.ContestNum;
            var contestantNumber = rbListItem.CwiEditInfo.ContestantNum;
            var contestantDesc = rbListItem.CwiEditInfo.ContestantDescription;
            var amountWagered = rbListItem.CwiEditInfo.SelectedAmountWagered;
            var odds = rbListItem.CwiEditInfo.SelectedOdds;
            var toWinAmount = rbListItem.CwiEditInfo.SelectedToWinAmount;
            var thresholdLine = rbListItem.CwiEditInfo.SelectedThresholdLine;
            var wageringMode = rbListItem.CwiEditInfo.WageringMode;
            var layoffWager = rbListItem.CwiEditInfo.LayoffWager;
            var xtoYRepresentation = rbListItem.CwiEditInfo.XtoYRep;
            var toBase = rbListItem.CwiEditInfo.ToBase;

            if (rbListItem.FreePlayFlag == "Y")
              isFreePlay = true;

            var priceType = rbListItem.CwiEditInfo.PriceType;

            if (FrmSportAndGameSelection.ActiveContests != null) {
              selectedContestInfo = (from s in FrmSportAndGameSelection.ActiveContests where s.ContestNum == contestNumber && s.ContestantNum == contestantNumber select s).FirstOrDefault();
            }

            FrmSportAndGameSelection.CheckIfContestWagerCutoffHasBeenReachedInReadback("Accept Any Wager", "Request to Edit a READBACK Item Past Post", selectedContestInfo, rbListItem.WiEditInfo.Description, rbListItem.WiEditInfo.SelectedAmountWagered, rbListItem.WiEditInfo.SelectedToWinAmount, out cutoffTimeBypassed);

            if (cutoffTimeBypassed)
              FrmSportAndGameSelection.ShowContestFormInEditMode("Edit", ticketNumber, wagerNumber, itemNumber, contestNumber, contestantNumber, contestantDesc, amountWagered, odds, toWinAmount, isFreePlay, priceType, thresholdLine, wageringMode, layoffWager, xtoYRepresentation, toBase);
          }
          break;
        case "Parlay":
        case "Teaser":
        case "If-Bet":
        case "Action Reverse":
          var wager = (from w in Mdi.Ticket.Wagers where w.TicketNumber == gvTicketNumber && w.WagerNumber == gvWagerNumber select w).FirstOrDefault();
          var wagerItems = (from wi in Mdi.Ticket.WagerItems where wi.TicketNumber == gvTicketNumber && wi.WagerNumber == gvWagerNumber select wi).ToList();
          if (wager != null && wagerItems.Count > 0)
            FrmSportAndGameSelection.ShowComplexBetInEditMode(wagerTypeName, wager, wagerItems);
          break;
        case "Manual Play":
          rbListItem = (from p in FrmSportAndGameSelection.ReadbackItems where p.TicketNumber == gvTicketNumber && p.WagerNumber == gvWagerNumber && p.RowContainsEditItemInfo select p).FirstOrDefault();
          if (rbListItem != null) {
            var frmManual = new FrmPlaceManual(Mdi.LayoffWagersEnabled, AppModuleInfo) {
              FrmMode = "Edit",
              EditModeInfo = rbListItem.MwiEditInfo,
              TicketNumberToUpd = rbListItem.TicketNumber,
              WagerNumberToUpd = rbListItem.WagerNumber,
              WagerItemNumberToUpd = rbListItem.ItemNumber,
              XToy = FrmPlaceManual.BuildxToyObj(rbListItem.MwiEditInfo.Odds)
            };
            Mdi.DisplayWageringForm(frmManual, "Manual Play");
          }
          break;
      }
    }

    #endregion

    #region Protected Methods

    protected override bool ProcessCmdKey(ref Message msg, Keys keyData) {
      var baseResult = base.ProcessCmdKey(ref msg, keyData);

      return keyData == (Keys.Control | Keys.Tab) ? Mdi.ActivateWageringForm(Name, baseResult) : baseResult;
    }

    #endregion

    #region Events

    private void ctxModifyWager_Opening(object sender, CancelEventArgs e) {
      if (!NoOtherFormIsOpen()) {
        e.Cancel = true;
      }
    }

    private void deleteAllItem_MouseUp(object sender, MouseEventArgs e) {
      RemoveAllWagersFromReadback();
    }

    private void deleteItem_MouseUp(object sender, MouseEventArgs e) {
      DeleteSelectedWagerAndItems();
    }

    private void dgvwReadbackInfo_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e) {
      if (FrmSportAndGameSelection.SelectedWagerType == "Parlay" || FrmSportAndGameSelection.SelectedWagerType == "Teaser") {
        dgvwReadbackInfo.ClearSelection();
        return;
      }

      if (NoOtherFormIsOpen()) {
        var rowIsClickable = ((DataGridView)sender).Rows[e.RowIndex].Cells[6].Value.ToString().ToLower();

        if (rowIsClickable == "true" && e.RowIndex >= 0 && e.ColumnIndex >= 0 && (e.Button == MouseButtons.Right || e.Button == MouseButtons.Left)) {
          ((DataGridView)sender).Rows[e.RowIndex].Selected = true;
        }
      }
    }

    private void dgvwReadbackInfo_CellDoubleClick(object sender, DataGridViewCellEventArgs e) {
      if (NoOtherFormIsOpen()) {
        var rowIsClickable = ((DataGridView)sender).Rows[e.RowIndex].Cells[6].Value.ToString().ToLower();

        if (rowIsClickable == "true" && e.RowIndex >= 0 && e.ColumnIndex >= 0) {
          ShowRequiredFormInEditMode();
        }
      }

    }

    private void dgvwReadbackInfo_MouseDown(object sender, MouseEventArgs e) {
      if (FrmSportAndGameSelection.SelectedWagerType == "Parlay" || FrmSportAndGameSelection.SelectedWagerType == "Teaser") {
        LoadParlayMakeAWagerFromSb(e);
        return;
      }
      if (NoOtherFormIsOpen()) {
        if (e.Button == MouseButtons.Right) {
          var hti = dgvwReadbackInfo.HitTest(e.X, e.Y);
          dgvwReadbackInfo.ClearSelection();

          if (hti.RowIndex >= 0) {
            dgvwReadbackInfo.Rows[hti.RowIndex].Selected = true;
          }
          PrepareContextMenuToShow(sender, e);
        }
      }
    }

    private void editItem_MouseUp(object sender, MouseEventArgs e) {
      ShowRequiredFormInEditMode();
    }

    private void frmTicketConfirmation_KeyDown(object sender, KeyEventArgs e) {
    }

    private void frmTicketConfirmation_Load(object sender, EventArgs e) {
      Text = Resources.FrmTicketConfirmation_frmTicketConfirmation_Load_Pending_Wagers_for_ticket_ + _parentFrm.TicketNumber + Resources.FrmTicketConfirmation_frmTicketConfirmation_Load____F12_for_Read_Back;
    }

    private void rifHookupItem_MouseUp(object sender, MouseEventArgs e) {
      ShowRifHookupFrm();
    }

    private void rifWinOnlyItem_MouseUp(object sender, MouseEventArgs e) {
      BuildRifWinOnlyFromReadback();
    }

    private void rifWinOrPushItem_MouseUp(object sender, MouseEventArgs e) {
      BuildRifWinOrPushFromReadback();
    }

    #endregion
  }
}