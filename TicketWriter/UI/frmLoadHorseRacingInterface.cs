﻿using GUILibraries.Forms;
using GUILibraries.Utilities;
using InstanceManager.BusinessLayer;
using System;
using System.Windows.Forms;

namespace TicketWriter.UI {
  public partial class frmLoadHorseRacingInterface : SIDForm {
    private bool _firstCall = true;
    readonly ModuleInfo _moduleInfo;
    readonly String _userPassword;
    readonly String _customerId;

    private const string ROOT_URL = "https://horses.trdwd.ec/BOSSWagering/Racebook/PhoneBetTaker/";

    public frmLoadHorseRacingInterface(ModuleInfo moduleInfo, String customerId, String userPassword)
      : base(moduleInfo) {
      _moduleInfo = moduleInfo;
      _customerId = customerId;
      _userPassword = userPassword;
      InitializeComponent();
    }

    private void frmLoadHorseRacingInterface_Load(object sender, EventArgs e) {
      HandleLoadEvent();
    }

    private void HandleLoadEvent() {
      webHorseRacingBrowser.Navigate(ROOT_URL + "?userName=" + _moduleInfo.WindowsUserId + "&userPassword=" + _userPassword + "&player=" + _customerId + "&uhap=1");



    }

    private void ResetBetType() {
      var frm = FormF.GetFormByName("frmSportAndGameSelection", this);

      if (frm == null) {
        return;
      }

      var frmSports = (FrmSportAndGameSelection)frm;

      if (frmSports.SelectedWagerType != null)
        frmSports.SelectedWagerType = "Straight Bet";

      frm = FormF.GetFormByName("MdiTicketWriter", this);

      if (frm == null) {
        return;
      }

      var frmMdi = (MdiTicketWriter)frm;

      if (frmSports.SelectedWagerType != null)
        frmMdi.ToggleFpAndRifChecks(true);
    }

    private void frmHorseRacing_FormClosing(object sender, FormClosingEventArgs e) {
      ResetBetType();
    }
  }
}
