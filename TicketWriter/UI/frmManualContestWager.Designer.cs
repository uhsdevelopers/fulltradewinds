﻿namespace TicketWriter.UI
{
    partial class FrmManualContestWager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtContestOdds = new System.Windows.Forms.TextBox();
            this.txtChosenTeam = new System.Windows.Forms.TextBox();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.txtLineAdj = new System.Windows.Forms.TextBox();
            this.lblPrice = new System.Windows.Forms.Label();
            this.lblWagerType = new System.Windows.Forms.Label();
            this.panWagerAmounts = new System.Windows.Forms.Panel();
            this.lblMaxWinDisplay = new System.Windows.Forms.Label();
            this.lblMaxRiskDisplay = new System.Windows.Forms.Label();
            this.grpRiskAmount = new System.Windows.Forms.GroupBox();
            this.txtRisk = new GUILibraries.Controls.NumberTextBox();
            this.grpWinAmount = new System.Windows.Forms.GroupBox();
            this.txtToWin = new GUILibraries.Controls.NumberTextBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.chbLayoffWager = new System.Windows.Forms.CheckBox();
            this.btnPlaceBet = new System.Windows.Forms.Button();
            this.panWagerAmounts.SuspendLayout();
            this.grpRiskAmount.SuspendLayout();
            this.grpWinAmount.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtContestOdds
            // 
            this.txtContestOdds.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtContestOdds.Enabled = false;
            this.txtContestOdds.Location = new System.Drawing.Point(24, 7);
            this.txtContestOdds.Multiline = true;
            this.txtContestOdds.Name = "txtContestOdds";
            this.txtContestOdds.ReadOnly = true;
            this.txtContestOdds.Size = new System.Drawing.Size(325, 38);
            this.txtContestOdds.TabIndex = 6;
            this.txtContestOdds.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtChosenTeam
            // 
            this.txtChosenTeam.Enabled = false;
            this.txtChosenTeam.Location = new System.Drawing.Point(64, 65);
            this.txtChosenTeam.Name = "txtChosenTeam";
            this.txtChosenTeam.ReadOnly = true;
            this.txtChosenTeam.Size = new System.Drawing.Size(244, 20);
            this.txtChosenTeam.TabIndex = 14;
            this.txtChosenTeam.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtPrice
            // 
            this.txtPrice.Location = new System.Drawing.Point(250, 101);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(68, 20);
            this.txtPrice.TabIndex = 20;
            this.txtPrice.TextChanged += new System.EventHandler(this.txtPrice_TextChanged);
            this.txtPrice.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPrice_KeyDown);
            this.txtPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrice_KeyPress);
            this.txtPrice.Leave += new System.EventHandler(this.txtPrice_Leave);
            // 
            // txtLineAdj
            // 
            this.txtLineAdj.Location = new System.Drawing.Point(54, 101);
            this.txtLineAdj.Name = "txtLineAdj";
            this.txtLineAdj.Size = new System.Drawing.Size(68, 20);
            this.txtLineAdj.TabIndex = 10;
            this.txtLineAdj.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtLineAdj_KeyDown);
            this.txtLineAdj.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLineAdj_KeyPress);
            // 
            // lblPrice
            // 
            this.lblPrice.Location = new System.Drawing.Point(202, 101);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(48, 16);
            this.lblPrice.TabIndex = 23;
            this.lblPrice.Text = "Price:";
            this.lblPrice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblWagerType
            // 
            this.lblWagerType.Location = new System.Drawing.Point(126, 99);
            this.lblWagerType.Name = "lblWagerType";
            this.lblWagerType.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblWagerType.Size = new System.Drawing.Size(74, 20);
            this.lblWagerType.TabIndex = 22;
            this.lblWagerType.Text = "Team Under:";
            this.lblWagerType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // panWagerAmounts
            // 
            this.panWagerAmounts.Controls.Add(this.lblMaxWinDisplay);
            this.panWagerAmounts.Controls.Add(this.lblMaxRiskDisplay);
            this.panWagerAmounts.Controls.Add(this.grpRiskAmount);
            this.panWagerAmounts.Controls.Add(this.grpWinAmount);
            this.panWagerAmounts.Location = new System.Drawing.Point(24, 133);
            this.panWagerAmounts.Name = "panWagerAmounts";
            this.panWagerAmounts.Size = new System.Drawing.Size(325, 77);
            this.panWagerAmounts.TabIndex = 26;
            // 
            // lblMaxWinDisplay
            // 
            this.lblMaxWinDisplay.Location = new System.Drawing.Point(184, 52);
            this.lblMaxWinDisplay.Name = "lblMaxWinDisplay";
            this.lblMaxWinDisplay.Size = new System.Drawing.Size(120, 23);
            this.lblMaxWinDisplay.TabIndex = 1006;
            this.lblMaxWinDisplay.Text = "Max To Win";
            // 
            // lblMaxRiskDisplay
            // 
            this.lblMaxRiskDisplay.Location = new System.Drawing.Point(14, 52);
            this.lblMaxRiskDisplay.Name = "lblMaxRiskDisplay";
            this.lblMaxRiskDisplay.Size = new System.Drawing.Size(120, 23);
            this.lblMaxRiskDisplay.TabIndex = 1005;
            this.lblMaxRiskDisplay.Text = "Max Risk";
            // 
            // grpRiskAmount
            // 
            this.grpRiskAmount.Controls.Add(this.txtRisk);
            this.grpRiskAmount.Location = new System.Drawing.Point(10, 3);
            this.grpRiskAmount.Name = "grpRiskAmount";
            this.grpRiskAmount.Size = new System.Drawing.Size(130, 46);
            this.grpRiskAmount.TabIndex = 5;
            this.grpRiskAmount.TabStop = false;
            this.grpRiskAmount.Text = "Risk Amount";
            // 
            // txtRisk
            // 
            this.txtRisk.AllowCents = false;
            this.txtRisk.AllowNegatives = false;
            this.txtRisk.DividedBy = 1;
            this.txtRisk.DivideFlag = false;
            this.txtRisk.Location = new System.Drawing.Point(7, 20);
            this.txtRisk.MaxLength = 8;
            this.txtRisk.Name = "txtRisk";
            this.txtRisk.ShowIfZero = true;
            this.txtRisk.Size = new System.Drawing.Size(100, 20);
            this.txtRisk.TabIndex = 30;
            this.txtRisk.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtRisk_KeyUp);
            // 
            // grpWinAmount
            // 
            this.grpWinAmount.Controls.Add(this.txtToWin);
            this.grpWinAmount.Location = new System.Drawing.Point(174, 3);
            this.grpWinAmount.Name = "grpWinAmount";
            this.grpWinAmount.Size = new System.Drawing.Size(142, 46);
            this.grpWinAmount.TabIndex = 6;
            this.grpWinAmount.TabStop = false;
            this.grpWinAmount.Text = "To Win Amount";
            // 
            // txtToWin
            // 
            this.txtToWin.AllowCents = false;
            this.txtToWin.AllowNegatives = false;
            this.txtToWin.DividedBy = 1;
            this.txtToWin.DivideFlag = false;
            this.txtToWin.Location = new System.Drawing.Point(13, 19);
            this.txtToWin.MaxLength = 8;
            this.txtToWin.Name = "txtToWin";
            this.txtToWin.ShowIfZero = true;
            this.txtToWin.Size = new System.Drawing.Size(100, 20);
            this.txtToWin.TabIndex = 40;
            this.txtToWin.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtToWin_KeyUp);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(194, 240);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(137, 23);
            this.btnCancel.TabIndex = 70;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // chbLayoffWager
            // 
            this.chbLayoffWager.AutoSize = true;
            this.chbLayoffWager.Location = new System.Drawing.Point(228, 216);
            this.chbLayoffWager.Name = "chbLayoffWager";
            this.chbLayoffWager.Size = new System.Drawing.Size(90, 17);
            this.chbLayoffWager.TabIndex = 50;
            this.chbLayoffWager.Text = "Layoff Wager";
            this.chbLayoffWager.UseVisualStyleBackColor = true;
            // 
            // btnPlaceBet
            // 
            this.btnPlaceBet.Location = new System.Drawing.Point(47, 240);
            this.btnPlaceBet.Name = "btnPlaceBet";
            this.btnPlaceBet.Size = new System.Drawing.Size(134, 23);
            this.btnPlaceBet.TabIndex = 60;
            this.btnPlaceBet.Text = "Place Bet";
            this.btnPlaceBet.UseVisualStyleBackColor = true;
            this.btnPlaceBet.Click += new System.EventHandler(this.btnPlaceBet_Click);
            // 
            // FrmManualContestWager
            // 
            this.AcceptButton = this.btnPlaceBet;
            this.AccessibleDescription = "Manual Contest";
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(372, 271);
            this.Controls.Add(this.btnPlaceBet);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.chbLayoffWager);
            this.Controls.Add(this.panWagerAmounts);
            this.Controls.Add(this.txtPrice);
            this.Controls.Add(this.txtLineAdj);
            this.Controls.Add(this.lblPrice);
            this.Controls.Add(this.lblWagerType);
            this.Controls.Add(this.txtChosenTeam);
            this.Controls.Add(this.txtContestOdds);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmManualContestWager";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manual Contest Wager";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmManualContestWager_FormClosing);
            this.Load += new System.EventHandler(this.frmManualContestWager_Load);
            this.Shown += new System.EventHandler(this.frmManualContestWager_Shown);
            this.panWagerAmounts.ResumeLayout(false);
            this.grpRiskAmount.ResumeLayout(false);
            this.grpRiskAmount.PerformLayout();
            this.grpWinAmount.ResumeLayout(false);
            this.grpWinAmount.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtContestOdds;
        private System.Windows.Forms.TextBox txtChosenTeam;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.TextBox txtLineAdj;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.Label lblWagerType;
        private System.Windows.Forms.Panel panWagerAmounts;
        private System.Windows.Forms.GroupBox grpRiskAmount;
        private GUILibraries.Controls.NumberTextBox txtRisk;
        private System.Windows.Forms.GroupBox grpWinAmount;
        private GUILibraries.Controls.NumberTextBox txtToWin;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.CheckBox chbLayoffWager;
        private System.Windows.Forms.Button btnPlaceBet;
        private System.Windows.Forms.Label lblMaxWinDisplay;
        private System.Windows.Forms.Label lblMaxRiskDisplay;
    }
}