﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using GUILibraries.BusinessLayer;
using GUILibraries.Forms;
using GUILibraries.Utilities;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using SIDLibraries.Utilities;
using TicketWriter.Properties;
using Common = TicketWriter.Utilities.Common;
using GUILibraries.Controls;

namespace TicketWriter.UI {
  public sealed partial class FrmPlaceParlay : SIDForm {
    #region Public Properties

    public string ActiveMdiChildName { private get; set; }

    private spCstGetCustomer_Result Customer {
      get {
        return _customer ??
               (_customer = ((MdiTicketWriter)FormF.GetFormByName("MdiTicketWriter", this)).Customer);
      }
    }

    private FormParameterList ParameterList {
      get {
        return _parameterList ??
               (_parameterList =
                ((MdiTicketWriter)(FormF.GetFormByName("MdiTicketWriter", this))).ParameterList);
      }
    }

    public FrmSportAndGameSelection FrmSportAndGameSelection {
      get {
        return _frmSportAndGameSelection ??
               (_frmSportAndGameSelection =
                (FrmSportAndGameSelection)FormF.GetFormByName("FrmSportAndGameSelection", this));
      }
    }

    private bool NudTeamsFrozen { get; set; }

    private MdiTicketWriter Mdi {
      get {
        return _mdiTicketWriter ??
               (_mdiTicketWriter = (MdiTicketWriter)FormF.GetFormByName("MdiTicketWriter", this));
      }
    }

    public int RelativeXPos { private get; set; }

    public int RelativeYPos { private get; set; }

    public Parlay Parlay { get; set; }

    public String FrmMode { private get; set; }

    public int TicketNumberToUpd { private get; set; }

    public int WagerNumberToUpd { private get; set; }

    public Boolean ItemFromMakeAWagerForm { private get; set; }

    #endregion

    #region Private Properties

    private String ModuleName {
      get {
        return ParameterList.GetItem("ModuleName").Value;
      }
    }

    #endregion

    #region Private vars

    private spCstGetCustomer_Result _customer;
    private bool _deleteWagersOnExit = true;
    private FrmSportAndGameSelection _frmSportAndGameSelection;

    private Boolean _isFreePlayFlag;
    private FormParameterList _parameterList;
    private string _parlayName = "";
    private MdiTicketWriter _mdiTicketWriter;

    private String _editChosenTeamsCnt;
    private String _editRiskAmt;
    private String _editToWinAmt;
    private Boolean _editRoundRobinFlag;

    private String _editSelectedRoundRobin;
    private String _editSelectedRRobinOutcome;

    private bool _byPassMinimumWager;

    private Ticket.Wager _wagerEditBackup;
    private List<Ticket.WagerItem> _itemsBackupCopy;
    private List<Ticket.ContestWagerItem> _contestItemsBackupCopy;
    private List<Ticket.WagerAndItemNumber> _itemsToRemove;
    private bool _cancelActionPerformed;

    private const int AMNT_WAGERED_IDX = 8;
    private const int CELLIDX = 3;
    private const int FIXEDPRICE_IDX = 18;
    private const int GAMENUM_IDX = 12;

    private const int GROUPNUM_IDX = 14;
    private const int LAYOFF_IDX = 17;
    private const int PERIODNUM_IDX = 13;
    private const int PITCHER1_MSTART_IDX = 19;

    private const int PITCHER2_MSTART_IDX = 20;
    private const int PRICE_TYPE_IDX = 11;
    private const int ROWIDX = 2;
    private const int SEL_BPOINTS_IDX = 7;

    private const int SEL_ODDS_IDX = 6;
    private const int SEL_AMERICAN_PRICE_IDX = 22;
    private const int TOWIN_IDX = 9;
    private const int WAGERCUTOFF_IDX = 15;
    private const int WAGERINGMODE_IDX = 16;

    private const int WINUM_IDX = 5;
    private const int WNUM_IDX = 4;
    private const int TNUM_IDX = 25;

    private const int WTYPE_MODE_IDX = 0;

    private const int TOTALS_OU_FLAG_IDX = 21;
    private const int SEL_GAME_PERIOD_DESCRIPTION = 23;

    #endregion

    #region Constructors

    public FrmPlaceParlay(ModuleInfo moduleInfo)
      : base(moduleInfo) {
      InitializeComponent();
      SetCentsNumericInputs();
    }

    #endregion

    #region Public Methods

    public void AddItem(ParlayItem item) {
      if (Parlay.Frozen && string.IsNullOrEmpty(item.FromPreviousTicket)) {
        Parlay.Frozen = false;
      }
      if (Parlay.Items.Count + 1 > Parlay.MaxPicks) {
        Mdi.Ticket.DeleteWagerItem(item.TicketNumber, item.WagerNumber, item.Number);
        Mdi.Ticket.ReIndexWagerItems(item.TicketNumber, item.WagerNumber);
        MessageBox.Show(@"Too many teams have been selected. A maximum of " + Parlay.MaxPicks + @" is permitted.");
        return;
      }
      bndsrc.Add(item);

      if (FrmMode != "Edit") {
        dgvwParlays.ClearSelection();
        dgvwParlays.Refresh();
      }
      if (chbRoundRobin.Checked && Parlay.Items.Count < 2) {
        return;
      }
      CalculateToWinAmount();
    }

    public void AdjustFrmPosition() {
      var xw = RelativeXPos - Width;
      var yw = RelativeYPos - Height - 10;


      StartPosition = FormStartPosition.Manual;
      Location = new Point(xw, yw);
    }

    private void RemoveItem(int wagerNumber, int itemIdx) {
      for (var i = 0; i < bndsrc.Count; i++) {
        var localItem = (ParlayItem)bndsrc[i];

        if (localItem.WagerNumber == wagerNumber && i == itemIdx) {
          bndsrc.RemoveAt(i);
          break;
        }
      }
      dgvwParlays.Refresh();
    }

    public void UpdateItem(ParlayItem item) {
      for (var i = 0; i < bndsrc.Count; i++) {
        var localItem = (ParlayItem)bndsrc[i];

        if (localItem.WagerNumber == item.WagerNumber && localItem.Number == item.Number) {
          bndsrc[i] = item;
          break;
        }
      }
      dgvwParlays.Refresh();
      CalculateToWinAmount();
    }

    public void SetCursorInTeamNumberBox() {
      nudTeams.Select();
      nudTeams.Select(0, nudTeams.Value.ToString(CultureInfo.InvariantCulture).Length);
    }

    #endregion

    #region Private Methods

    private void CalculateToWinAmount() {
      if (Parlay == null || txtRisk.Text.Length == 0 || (chbRoundRobin.Checked && cmbRoundRobin.SelectedIndex < 0)) {
        txtToWin.Text = "";
        return;
      }
      if (chbRoundRobin.Checked && cmbRoundRobin.SelectedIndex >= 0 && Parlay.RoundRobin == null) {
        EnableRoundRobin(true);
      }
      Parlay.AmountWagered = NumberF.ParseDouble(txtRisk.Text.Replace(",", ""));
      Parlay.ShowCents = Params.IncludeCents;
      txtToWin.Text = FormatNumber(Parlay.ToWinAmount);
    }

    private void ChangeUpDownValue(int count) {
      if (count < nudTeams.Minimum || count > nudTeams.Maximum || NudTeamsFrozen || count < nudTeams.Value) {
        if (NudTeamsFrozen && count > nudTeams.Value && count <= nudTeams.Maximum) {
          nudTeams.Value = count;
        }
        return;
      }
      if (count <= nudTeams.Maximum) {
        nudTeams.Value = count;
      }
    }

    private void EnableRoundRobin(bool enable) {
      if (Parlay == null) {
        chbRoundRobin.Checked = false;
        return;
      }
      cmbRoundRobin.Enabled = enable;

      if (cmbRoundRobin.Enabled) {
        if (cmbRoundRobin.Items.Count == 0) {
          PopulateRounRobinTypeCmb();
        }
        if (Parlay.Number != null) {
          var roundRobin = new RoundRobin(Parlay.TicketNumber, Parlay.Number.Value, Mdi.AppModuleInfo) {
            ModuleName = Parlay.ModuleName,
            ParlayName = Parlay.Name,
            SeedItems = Parlay.Items
          };
          Parlay.RoundRobin = roundRobin;
        }
        SetRoundRobinItemsCalculation();
        txtToWin.Text = "";
      }
      else {
        Parlay.RoundRobin = null;
      }
      Parlay.ItemUpdated = true;
      CalculateToWinAmount();
    }

    private void InitializeForm() {
      EnableRoundRobinCheckBoxByDefault();

      if (FrmMode == "Edit") {
        _itemsToRemove = new List<Ticket.WagerAndItemNumber>();
        ItemFromMakeAWagerForm = false;
        _wagerEditBackup = Mdi.Ticket.CreateWagerSafeCopy(TicketNumberToUpd, WagerNumberToUpd);
        GetEditParlayInfo();
        txtRisk.Text = _editRiskAmt;
        txtToWin.Text = _editToWinAmt;
        int nudTeamsValue;
        int.TryParse(_editChosenTeamsCnt, out nudTeamsValue);

        if (nudTeamsValue >= nudTeams.Minimum && nudTeamsValue <= nudTeams.Maximum) {
          nudTeams.Value = nudTeamsValue;
        }
        FreezeShownItemsQty();
        FillPlaceParlayGvInEditMode();

        if (_editRoundRobinFlag) {
          chbRoundRobin.Checked = true;
          cmbRoundRobin.Text = _editSelectedRoundRobin;
          lblRoundRobinOutcome.Text = _editSelectedRRobinOutcome;
        }

        btnPlaceBet.Text = Resources.FrmPlaceParlay_frmPlaceParlay_Load_Modify_ + FrmSportAndGameSelection.SelectedWagerType;
        if ((FrmSportAndGameSelection.ReadbackItems != null && (FrmSportAndGameSelection.ReadbackItems.Where(rb => rb.TicketNumber == TicketNumberToUpd && rb.WagerNumber == WagerNumberToUpd).Any()))) {
          _itemsBackupCopy = Mdi.Ticket.CreateWagerItemsListSafeCopy(TicketNumberToUpd, WagerNumberToUpd);
          _contestItemsBackupCopy = Mdi.Ticket.CreateContestWagerItemsListSafeCopy(TicketNumberToUpd, WagerNumberToUpd);
        }
      }

      if (Parlay == null && Mdi.OpenPlayWagerInfo != null && Mdi.OpenPlayWagerInfo.WagerStatus == "Open") {
        var newWagerNumber = Mdi.Ticket.Wagers != null && Mdi.Ticket.Wagers.Where(w => w.TicketNumber == Mdi.TicketNumber).Count() > 0 ? Mdi.Ticket.Wagers.Where(w => w.TicketNumber == Mdi.TicketNumber).Count() + 1 : 1;
        Parlay = Parlay.Create(Mdi.AppModuleInfo, Mdi.OpenPlayWagerInfo, ModuleName);
        Parlay.ExchangeRate = (int)(Mdi.Customer.ExchangeRate ?? 1);
        Parlay.TicketNumber = Mdi.TicketNumber;
        Parlay.Number = newWagerNumber;
        Mdi.Ticket.AddWager(Parlay.ToTicketWager(Parlay));
        var parlayItems = WagerItem.Get(Mdi.AppModuleInfo, int.Parse(Parlay.FromPreviousTicket), int.Parse(Parlay.FromPreviousWagerNumber), Mdi.TicketNumber, newWagerNumber, Mdi.OpenPlayItemsDescription);

        foreach (var item in parlayItems) {
          AddItem(ParlayItem.Convert(item));
        }
        var wagerItems = WagerItem.ToTicketWagerItem(Parlay.Items, Mdi.Ticket.TicketNumber);

        foreach (var wagerItem in wagerItems) {
          wagerItem.AlreadyBetItemCount = FrmSportAndGameSelection.GetMultiSelectionItemsCount(WagerType.PARLAY, false, wagerItem);
          SyncParlayAlreadyBetItemCount(wagerItem.ItemNumber, wagerItem.AlreadyBetItemCount);
          Mdi.Ticket.AddWagerItem(wagerItem);
        }
        nudTeams.Maximum = Parlay.TotalPicks;
        nudTeams.Value = Parlay.TotalPicks;
      }

      if (Parlay != null && Parlay.IsOpen) {
        Parlay.OriginalItemCount = Parlay.Items.Count(wi => !string.IsNullOrEmpty(wi.FromPreviousTicket) && wi.FromPreviousTicket != Mdi.Ticket.TicketNumber.ToString(CultureInfo.InvariantCulture));
        grpRoundRobin.Enabled = nudTeams.Enabled = txtRisk.Enabled = false;
        txtRisk.Text = Parlay.AmountWagered.HasValue ? Parlay.AmountWagered.Value.ToString(CultureInfo.InvariantCulture) : "";
        lblWin.Visible = txtToWin.Visible = false;
        _byPassMinimumWager = true;
      }
      SetCursorInTeamNumberBox();
      if (!FrmSportAndGameSelection.ShowWagerLimitsInFrm.IsActive()) return;
      FrmSportAndGameSelection.ShowWagerLimitsInFrm.FrmPlaceParlay = this;
      int? parlayWagerNumber = null;
      int ticketNumber = Mdi.TicketNumber;
      if (Parlay != null) {
        ticketNumber = Parlay.TicketNumber;
        parlayWagerNumber = Parlay.Number;
      }
      FrmSportAndGameSelection.ShowWagerLimitsInFrm.DisplayParlayMaxWagerLimits(ticketNumber, parlayWagerNumber);
    }

    private void SyncParlayAlreadyBetItemCount(int targetItemNumber, int? alreadyBetItemCount) {
      foreach (var item in Parlay.Items) {
        if (item.Number != targetItemNumber)
          continue;
        item.AlreadyBetItemCount = alreadyBetItemCount;
      }
    }

    private void EnableRoundRobinCheckBoxByDefault() {
      if (FrmMode == "Edit" && TicketNumberToUpd != Mdi.TicketNumber) {
        grpRoundRobin.Enabled = false;
        return;
      }

      chbRoundRobin.Enabled = true;
      cmbRoundRobin.Enabled = true;
      if (cmbRoundRobin.Items.Count == 0) {
        PopulateRounRobinTypeCmb();
      }

    }

    private string ManageRoundRobin() {
      if (Parlay.RoundRobin == null) {
        return "";
      }
      var message = "";

      if (bndsrc.List.Count < 3) {
        message = "At least three wagers must be entered for a valid round robin.";
      }
      if (message.Length == 0 && bndsrc.List.Count != nudTeams.Value) {
        message = "The teams entry must be equal to the number of teams selected for a round robin.";
      }
      if (message.Length == 0 && cmbRoundRobin.SelectedIndex < 0) {
        message = "Please select the parlay combinations for the round robin";
      }
      else if (message.Length == 0 && !Parlay.RoundRobin.IsValidCombination) {
        message = "The parlay combinations selected for the round robin are not correct for the number of selections";
      }
      if (message.Length > 0) {
        return message;
      }
      if (Mdi.Ticket.RoundRobinParlays == null) {
        Mdi.Ticket.RoundRobinParlays = new List<Parlay>();
      }

      Mdi.Ticket.RoundRobinParlays.RemoveAll(p => p.TicketNumber == Parlay.TicketNumber && p.RrBaseWagerNumber == Parlay.Number);

      var itemCount = 1;

      foreach (var parlay in Parlay.RoundRobin.Combinations.SelectMany(combination => combination.Parlays)) {
        parlay.Type = Parlay.Type;
        if (Parlay.Number != null) parlay.RrBaseWagerNumber = (int)Parlay.Number;
        parlay.RRARBCBaseWagerItemNumber = itemCount;
        Mdi.Ticket.RoundRobinParlays.Add(parlay);
        itemCount++;
      }

      Mdi.Ticket.RoundRobinParlays = Mdi.Ticket.RoundRobinParlays.OrderBy(x => x.RrBaseWagerNumber).ThenBy(x => x.RRARBCBaseWagerItemNumber).ToList();

      return message;
    }

    private void PlaceBet() {
      if (Parlay == null || !Parlay.Number.HasValue) {
        return;
      }
      var message = "";

      if (chbRoundRobin.Checked) {
        message = ManageRoundRobin();
      }
      else {
        if (Mdi.Ticket.RoundRobinParlays != null)
          Mdi.Ticket.RoundRobinParlays.RemoveAll(p => p.TicketNumber == Parlay.TicketNumber && p.RrBaseWagerNumber == Parlay.Number);
      }

      if (message.Length == 0 && bndsrc.List.Count > nudTeams.Maximum) {
        message = "Too many teams have been selected. A maximum of " + nudTeams.Maximum + " is permitted.";
      }
      if (message.Length == 0 && Parlay.IsOpen && Parlay.Items.Count == Parlay.OriginalItemCount) {
        message = "Please add at least one team to this open parlay or press \"Cancel\" to leave it as is.";
      }
      if (message.Length > 0) {
        MessageBox.Show(message, ParameterList.GetItem("AppName").Value, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        return;
      }
      Boolean continueValidating, minWagerBypassed; // not using minWagerBypassed actually, just filling the requirement of this next method
      FrmSportAndGameSelection.CheckIfBelowMinimumWager(this, LoginsAndProfiles.ACCEPT_ANY_WAGER, Parlay.AmountWagered.ToString(), out continueValidating,
          out minWagerBypassed, false, false, true);

      if (!continueValidating)
        return;

      CalculateToWinAmount();

      if (Parlay.Number != null) {
        var wager = Mdi.Ticket.GetWager(Parlay.TicketNumber, Parlay.Number.Value);
        wager.AmountWagered = Parlay.AmountWagered;
        wager.RoundRobinLink = null;

        if (chbRoundRobin.Checked) {
          wager.RoundRobinLink = wager.WagerNumber;
          wager.SelectedRoundRobin = cmbRoundRobin.Text;
          wager.SelectedRRobinOutcome = lblRoundRobinOutcome.Text;
          wager.RoundRobinItemsCount = Parlay.RoundRobin.TotalParlaysCount;
        }
        var maxPayout = Parlay.MaxPayout;

        if (maxPayout.HasValue) {
          maxPayout *= 100;
        }
        wager.MaxPayout = maxPayout;
        wager.TotalPicks = (int)nudTeams.Value;
        wager.ToWinAmount = Parlay.ToWinAmount;

        wager.WagerStatus = dgvwParlays.Rows.Count < nudTeams.Value ? "O" : "P";
        wager.FreePlayFlag = FrmSportAndGameSelection.IsFreePlay ? "Y" : "N";
        Mdi.Ticket.UpdateWager(wager);

        var wagerItems = Mdi.Ticket.GetWagerItems(Parlay.TicketNumber, Parlay.Number.Value, Parlay.FromPreviousTicket, Parlay.FromPreviousWagerNumber);

        var amountWagered = NumberF.ParseDouble(txtRisk.Text);

        foreach (var item in Parlay.Items) {
          item.AmountWagered = amountWagered;

          for (var i = 0; i < wagerItems.Count; i++) {
            if (wagerItems[i].ItemNumber == item.Number) {
              var wagerItem = wagerItems[i];
              wagerItem.AmountWagered = item.AmountWagered;
              wagerItem.ToWinAmount = item.ToWinAmount;
              wagerItem.VolumeAmount = item.VolumeAmount;
              wagerItem.FreePlayFlag = FrmSportAndGameSelection.IsFreePlay ? "Y" : "N";
              wagerItems[i] = wagerItem;
              break;
            }
          }
        }
        Mdi.Ticket.UpdateWagerItems(wagerItems);
      }

      var amountsStr = " Risking: " + txtRisk.Text + ", to Win:" + txtToWin.Text + ", adding " + (Parlay.Items.Count) + (Parlay.Items.Count == 1 ? " item" : " items");

      Tag = "Added " + FrmSportAndGameSelection.SelectedWagerType + " to Ticket in " + (String.IsNullOrEmpty(FrmMode) ? "New" : FrmMode) + " mode. Customer: " + FrmSportAndGameSelection.Customer.CustomerID.Trim()
          + " " + Parlay.TicketNumber + "." + (Parlay.WagerNumber ?? 0) + amountsStr;

      FrmSportAndGameSelection.BuildReadbackLogInfoData(true);
      FrmSportAndGameSelection.PopulateReadbackLogInfoDgvw();
      Mdi.OpenPlayWagerInfo = null;
      Mdi.WageringPanelName = null;
      WageringForm.Close(this);
    }

    private void PopulateRounRobinTypeCmb() {
      Common.PopulateRounRobinTypeCmb(bndsrcRoundRobinType);
      cmbRoundRobin.SelectedIndexChanged -= cmbRoundRobin_SelectedIndexChanged;
      cmbRoundRobin.SelectedIndex = -1;
      cmbRoundRobin.SelectedIndexChanged += cmbRoundRobin_SelectedIndexChanged;
    }

    private void ModifyWagerItem() {
      if (dgvwParlays.SelectedRows.Count == 0) {
        return;
      }
      if (dgvwParlays.SelectedRows[0].Cells["Number"].Value == null || Parlay.Items.Count == 0) {
        return;
      }
      var boundItem = Parlay.Items.FirstOrDefault(wi => wi.Number == (int)dgvwParlays.SelectedRows[0].Cells["Number"].Value);

      if (boundItem == null) {
        throw new Exception("Bound item not found");
      }

      if (!string.IsNullOrEmpty(boundItem.FromPreviousTicket)) {
        return;
      }

      if (boundItem.IsContest) {
        if (!LoginsAndProfiles.ValidateUserFunctionalityAccess(Mdi.CurrentUserPermissions, LoginsAndProfiles.ALLOW_CONTESTS_IN_PARLAYS)) {
          MessageBox.Show(@"User Not Allowed to Change contests in " + WagerType.PARLAYNAME.ToLower() + @"s.");
          return;
        }

        if (!FrmSportAndGameSelection.ContestIsAvailable(boundItem.GameNumber)) {
          MessageBox.Show(@"Contest Not Available", @"Not Available", MessageBoxButtons.OK);
          return;
        }

        FrmSportAndGameSelection.ShowContestFormInEditMode(FrmMode, boundItem.TicketNumber, boundItem.WagerNumber, boundItem.Number, boundItem.GameNumber, boundItem.ContestantNumber, "", "",
            boundItem.FinalMoney.ToString(), "", _isFreePlayFlag, boundItem.PriceType, "", boundItem.WageringMode, boundItem.LayoffWager == "Y", "", "");
        return;
      }

      var dgvwR = dgvwParlays.SelectedRows[0];

      const string wagerTypeMode = "Risk";
      const string boxType = "ParlayBox";
      Common.ShowFormInEditMode(dgvwR, FrmSportAndGameSelection, wagerTypeMode, boxType, _isFreePlayFlag,
            AMNT_WAGERED_IDX, CELLIDX, FIXEDPRICE_IDX, GAMENUM_IDX, GROUPNUM_IDX, LAYOFF_IDX, PERIODNUM_IDX, PITCHER1_MSTART_IDX, PITCHER2_MSTART_IDX,
            PRICE_TYPE_IDX, SEL_AMERICAN_PRICE_IDX, SEL_BPOINTS_IDX, SEL_GAME_PERIOD_DESCRIPTION, SEL_ODDS_IDX, TOTALS_OU_FLAG_IDX, TOWIN_IDX,
            WAGERCUTOFF_IDX, WAGERINGMODE_IDX, TNUM_IDX, WINUM_IDX, WNUM_IDX, WTYPE_MODE_IDX);
    }

    private void CancelWager(bool closeForm = true) {
      if (Parlay != null &&
          (FrmMode == null ||
          FrmMode.Trim().ToUpper() != "EDIT" ||
          (FrmMode.Trim().ToUpper() == "EDIT" && Parlay.TicketNumber != Mdi.TicketNumber))
          && _deleteWagersOnExit) {
        Mdi.Ticket.DeleteWager(Parlay.TicketNumber, Parlay.Number);
      }
      Mdi.Ticket.RemoveCancelledWagerAndItems(FrmMode, _itemsToRemove, _wagerEditBackup, _itemsBackupCopy, _contestItemsBackupCopy, _cancelActionPerformed);

      if (Parlay != null && Mdi.Ticket.TicketNumber != Parlay.TicketNumber) {
        Mdi.Ticket.RestoreListForRemovingPendingItems(Parlay.TicketNumber, Parlay.WagerNumber ?? 0);
      }

      Mdi.WageringPanelName = null;
      Mdi.OpenPlayWagerInfo = null;
      Mdi.OpenPlayItemsDescription = null;

      if (Parlay != null && (FrmMode == null || FrmMode.Trim().ToUpper() != "EDIT" || (FrmMode.Trim().ToUpper() == "EDIT" && Parlay.TicketNumber != Mdi.TicketNumber))) {
        WageringForm.Close(this, FrmMode, Parlay.TicketNumber, Parlay.Number, closeForm);
      }
      else {
        WageringForm.Close(this, FrmMode, null, null, closeForm);
      }
    }

    /*private void RemoveCancelledItems()
    {
        if (FrmMode != null && FrmMode.Trim().ToUpper() == "EDIT" && _cancelActionPerformed)
        {
            if (Mdi.Ticket.WagerItems != null && Mdi.Ticket.WagerItems.Count > 0)
            {
                if (_itemsToRemove != null && _itemsToRemove.Count() > 0)
                {
                    foreach (var item in _itemsToRemove)
                    {
                        var wi = (from i in Mdi.Ticket.WagerItems
                                  where
                                      i.TicketNumber == Parlay.TicketNumber &&
                                      i.WagerNumber == Parlay.Number &&
                                      i.ItemNumber == item.ItemNumber &&
                                      i.GameNum == item.GameNumber &&
                                      i.PeriodNumber == item.PeriodNumber &&
                                      i.Description.Trim() == item.ItemDescription.Trim()
                                  select i).FirstOrDefault();
                        if (wi != null)
                        {
                            Mdi.Ticket.DeleteWagerItem(item.TicketNumber, item.WagerNumber, item.ItemNumber);
                        }
                    }
                }

                if (itemsBackupCopy != null && itemsBackupCopy.Count() > 0)
                {
                    for (var i = 0; i < Mdi.Ticket.WagerItems.Count; i++)
                    {
                        if (Mdi.Ticket.WagerItems[i].TicketNumber != Parlay.TicketNumber || Mdi.Ticket.WagerItems[i].WagerNumber != Parlay.Number)
                            continue;
                        var bkpItem = (from x in itemsBackupCopy where x.ItemNumber == Mdi.Ticket.WagerItems[i].ItemNumber select x).FirstOrDefault();
                        if (bkpItem != null)
                        {
                            Mdi.Ticket.WagerItems[i] = bkpItem;
                        }
                    }
                }
            }

            if (Mdi.Ticket.ContestWagerItems != null && Mdi.Ticket.ContestWagerItems.Count > 0)
            {
                if (_itemsToRemove != null && _itemsToRemove.Count() > 0)
                {
                    foreach (var item in _itemsToRemove)
                    {
                        var wi = (from i in Mdi.Ticket.ContestWagerItems
                                  where
                                      i.TicketNumber == Parlay.TicketNumber &&
                                      i.WagerNumber == Parlay.Number &&
                                      i.ItemNumber == item.ItemNumber &&
                                      i.ContestNum == item.GameNumber
                                  select i).FirstOrDefault();
                        if (wi != null)
                        {
                            Mdi.Ticket.DeleteContestWagerItem(item.TicketNumber, item.WagerNumber, item.ItemNumber);
                        }
                    }
                }

                if (contestItemsBackupCopy != null && contestItemsBackupCopy.Count() > 0)
                {
                    for (var i = 0; i < Mdi.Ticket.ContestWagerItems.Count; i++)
                    {
                        if (Mdi.Ticket.ContestWagerItems[i].TicketNumber != Parlay.TicketNumber || Mdi.Ticket.ContestWagerItems[i].WagerNumber != Parlay.Number)
                            continue;
                        var bkpItem = (from x in contestItemsBackupCopy where x.ItemNumber == Mdi.Ticket.ContestWagerItems[i].ItemNumber select x).FirstOrDefault();
                        if (bkpItem != null)
                        {
                            Mdi.Ticket.ContestWagerItems[i] = bkpItem;
                        }
                    }
                }
            }
        }
    }
    */

    private void DeleteWagerItem() {
      if (Parlay.TicketNumber != Mdi.TicketNumber) {
        var item = (from wi in Mdi.Ticket.WagerItems
                    where wi.TicketNumber == Parlay.TicketNumber &&
                        wi.WagerNumber == Parlay.Number.Value && wi.ItemNumber == (dgvwParlays.SelectedRows[0].Index + 1)
                    select wi).FirstOrDefault();
        if (item != null && item.Outcome != "P") {
          MessageBox.Show(@"Item cannot be removed from the database", @"Item not pending", MessageBoxButtons.OK);
          return;
        }
      }

      if (MessageBox.Show(Resources.FrmPlaceParlay_DeleteWagerItem_This_action_will_remove_the_bet_from_the_Parlay_, Resources.FrmPlaceParlay_DeleteWagerItem_remove_Wager, MessageBoxButtons.OKCancel) == DialogResult.OK) {
        // Delete wi and fix itemnumbers:
        if (dgvwParlays.SelectedRows.Count > 0) {
          RemoveItem(Parlay.Number ?? 0, dgvwParlays.SelectedRows[0].Index);
          if (Parlay.Number != null) {
            var wager = Mdi.Ticket.GetWager(Parlay.TicketNumber, Parlay.Number.Value);
            wager.WagerStatus = dgvwParlays.Rows.Count < nudTeams.Value ? "O" : "P";
          }
        }
      }
    }

    private void GetEditParlayInfo() {
      var wagerToEdit = (from p in Mdi.Ticket.Wagers where p.TicketNumber == TicketNumberToUpd && p.WagerNumber == WagerNumberToUpd select p).FirstOrDefault();

      if (wagerToEdit != null) {
        _editChosenTeamsCnt = wagerToEdit.TotalPicks.ToString();
        _editRiskAmt = wagerToEdit.AmountWagered.ToString();
        _editToWinAmt = wagerToEdit.ToWinAmount.ToString();

        if (wagerToEdit.RoundRobinLink != null && (int)wagerToEdit.RoundRobinLink > 0) {
          _editRoundRobinFlag = true;
          _editSelectedRoundRobin = wagerToEdit.SelectedRoundRobin.ToString(CultureInfo.InvariantCulture);
          _editSelectedRRobinOutcome = wagerToEdit.SelectedRRobinOutcome;
        }
      }

      if (Parlay == null) {
        if (wagerToEdit != null)
          FrmSportAndGameSelection.CreateParlayWager(this, "Edit", TicketNumberToUpd, WagerNumberToUpd, new TextBox { Name = "txtRiskFromMakeWager", Text = _editRiskAmt },
              new TextBox { Name = "txtToWinMakeWager", Text = _editToWinAmt }, wagerToEdit.WagerTypeName, wagerToEdit.WagerType, wagerToEdit.FreePlayFlag == "Y",
              wagerToEdit.LayoffFlag);
      }
    }

    private void FillPlaceParlayGvInEditMode() {
      if (bndsrc != null) {
        bndsrc.Clear();
      }
      else {
        bndsrc = new BindingSource();
      }
      var existingItems = (from p in Mdi.Ticket.WagerItems where p.TicketNumber == TicketNumberToUpd && p.WagerNumber == WagerNumberToUpd select p).ToList();

      foreach (var wi in existingItems) {
        var item = new ParlayItem {
          AlreadyBetItemCount = wi.AlreadyBetItemCount,
          AmountWagered = wi.AmountWagered,
          FinalMoney = wi.FinalMoney,
          FromPreviousTicket = wi.FromPreviousTicket,
          FromPreviousWagerNumber = wi.FromPreviousWagerNumber,

          TicketNumber = wi.TicketNumber,
          Number = wi.ItemNumber,
          OriginatingTicketNumber = wi.OriginatingTicketNumber,
          PriceType = wi.PriceType,
          SportSubType = wi.SportSubType,

          SportType = wi.SportType,
          Description = wi.Description,
          IsContest = wi.IsContest,

          SelectedCell = wi.SelectedCell,
          SelectedOdds = wi.SelectedOdds,
          SelectedAmericanPrice = wi.SelectedAmericanPrice,
          SelectedBPointsOption = wi.SelectedBPointsOption,
          SelectedAmountWagered = (wi.AmountWagered ?? 0).ToString(CultureInfo.InvariantCulture),
          SelectedToWinAmount = (wi.ToWinAmount ?? 0).ToString(CultureInfo.InvariantCulture),
          WagerTypeMode = wi.WagerTypeMode,
          RowGroupNum = wi.RowGroupNum,
          PeriodWagerCutoff = wi.PeriodWagerCutoff,
          WageringMode = wi.WageringMode,
          LayoffWager = wi.LayoffFlag,
          FixedPrice = (wi.AdjustableOddsFlag == "N" || string.IsNullOrEmpty(wi.AdjustableOddsFlag) ? "Y" : "N"),
          Pitcher1MStart = wi.Pitcher1ReqFlag,
          Pitcher2MStart = wi.Pitcher2ReqFlag,
          TotalPointsOu = wi.TotalPointsOu
        };

        if (wi.GameNum != null) {
          item.GameNumber = (int)wi.GameNum;
        }
        if (wi.ContestantNum != null) {
          item.ContestantNumber = (int)wi.ContestantNum;
        }
        if (wi.PeriodNumber != null) {
          item.PeriodNumber = (int)wi.PeriodNumber;
        }
        item.PeriodDescription = wi.PeriodDescription;
        item.WagerNumber = wi.WagerNumber;
        item.Type = WagerType.GetFromCode(wi.WagerType);
        _isFreePlayFlag = wi.FreePlayFlag == "Y";

        if (FrmSportAndGameSelection.ReadbackItems != null) {
          var existingReadbackItem = (from p in FrmSportAndGameSelection.ReadbackItems
                                      where p.TicketNumber == TicketNumberToUpd && p.WagerNumber == WagerNumberToUpd && p.ItemNumber == wi.ItemNumber && p.RowContainsEditItemInfo
                                      select p).FirstOrDefault();

          if (existingReadbackItem != null) {
            item.SelectedCell = existingReadbackItem.WiEditInfo.SelectedCell;
            item.SelectedOdds = existingReadbackItem.WiEditInfo.SelectedOdds;
            item.SelectedAmericanPrice = existingReadbackItem.WiEditInfo.SelectedAmericanPrice;
            item.SelectedBPointsOption = existingReadbackItem.WiEditInfo.SelectedBPointsOption;
            item.SelectedAmountWagered = "0";
            item.SelectedToWinAmount = "0";
            item.WagerTypeMode = "Risk";
            item.RowGroupNum = existingReadbackItem.WiEditInfo.RowGroupNum;
            item.PeriodWagerCutoff = item.PeriodWagerCutoff;
            item.WageringMode = existingReadbackItem.WiEditInfo.WageringMode;
            item.LayoffWager = existingReadbackItem.WiEditInfo.LayoffWager;
            item.FixedPrice = existingReadbackItem.WiEditInfo.FixedPrice;
            item.Pitcher1MStart = existingReadbackItem.WiEditInfo.Pitcher1MStart;
            item.Pitcher2MStart = existingReadbackItem.WiEditInfo.Pitcher2MStart;
            item.TotalPointsOu = existingReadbackItem.WiEditInfo.TotalsOuFlag;
            _isFreePlayFlag = existingReadbackItem.FreePlayFlag == "Y";
          }
        }
        AddItem(item);
      }
      dgvwParlays.Refresh();
    }

    private void FreezeShownItemsQty() {
      NudTeamsFrozen = true;
    }

    private void ResetBetType() {
      var frm = FormF.GetFormByName("FrmSportAndGameSelection", this);

      if (frm == null) {
        return;
      }
      var frmSports = (FrmSportAndGameSelection)frm;

      if (frmSports.SelectedWagerType != null)
        frmSports.SelectedWagerType = "Straight Bet";

      Parlay = null;

      frm = FormF.GetFormByName("MdiTicketWriter", this);

      if (frm == null) {
        return;
      }

      var frmMdi = (MdiTicketWriter)frm;

      if (frmSports.SelectedWagerType != null)
        frmMdi.ToggleFpAndRifChecks(true);
    }

    private void SetRoundRobinItemsCalculation() {
      if (nudTeams.Value < 3 || !chbRoundRobin.Checked) {
        lblRoundRobinOutcome.Text = "";
      }
      if (!chbRoundRobin.Checked || cmbRoundRobin.SelectedIndex < 0) {
        return;
      }
      var itemsPerParlayList = new List<string>(cmbRoundRobin.Text.Replace("'s", "").Split(','));
      Parlay.RoundRobin.ItemsPerParlays = new List<int>();
      var result = "";

      foreach (var itemsPerParlayText in itemsPerParlayList) {
        var itemsPerParlay = int.Parse(itemsPerParlayText.Trim());
        Parlay.RoundRobin.ItemsPerParlays.Add(itemsPerParlay);
        var combination = NumberF.Combination((int)nudTeams.Value, itemsPerParlay);

        if (combination == 0) {
          break;
        }
        result += combination.ToString(CultureInfo.InvariantCulture) + " - " + itemsPerParlayText.Trim() + "'s, ";
      }
      if (result.Length == 0) {
        return;
      }
      Parlay.RoundRobin.ResetCombinations();
      Parlay.ItemUpdated = true;
      lblRoundRobinOutcome.Text = result.Substring(0, result.Length - 2);

      if (Parlay.RoundRobin.IsValidCombination) {
        CalculateToWinAmount();
        for (var j = 0; j < Parlay.Items.Count; j++) {
          Parlay.Items[j].Number = j + 1;
        }
      }
      else {
        txtToWin.Text = "";
      }
    }

    private void SetRoundRobinMode(bool enable) {
      if (Parlay != null) {
        EnableRoundRobin(enable);
      }
    }

    private void ShowPayCard() {
      if (_parlayName == "") {
        var frm = FormF.GetFormByName("MdiTicketWriter", this);

        if (frm == null) {
          return;
        }
        var mdi = (MdiTicketWriter)frm;
        _parlayName = mdi.Customer.ParlayName;
      }
      PayCard.Show(this, _parlayName, "Parlay Pay Card", ModuleName, "Parlay");
    }

    private void ToggleContextMenuInability() {
      if (dgvwParlays.SelectedRows.Count == 0) {
        return;
      }
      if (dgvwParlays.SelectedRows[0].Cells["Number"].Value == null || Parlay.Items.Count == 0) {
        return;
      }
      var boundItem = Parlay.Items.FirstOrDefault(wi => wi.Number == (int)dgvwParlays.SelectedRows[0].Cells["Number"].Value);

      if (boundItem == null) {
        throw new Exception("Bound item not found");
      }
      string retStr;
      if (string.IsNullOrEmpty(boundItem.FromPreviousTicket) && FrmSportAndGameSelection.GameIsAvailable(boundItem.GameNumber, boundItem.PeriodNumber, out retStr)) {
        ctxModifyWager.Enabled = true;

        foreach (ToolStripItem toolstrip in ctxModifyWager.Items) {
          toolstrip.Enabled = true;
        }
      }
      else {
        ctxModifyWager.Enabled = false;
      }
    }

    private void UpdateParlay(object sender, ListChangedEventArgs e) {
      if (Parlay == null) {
        return;
      }
      switch (e.ListChangedType) {
        case ListChangedType.ItemAdded:
          Parlay.Items.Add((ParlayItem)((BindingSource)sender).List[e.NewIndex]);

          if (e.NewIndex == 0) {
            chbRoundRobin.Enabled = nudTeams.Enabled = true;
            nudTeams.Maximum = Parlay.MaxPicks;
          }
          if (FrmMode == "Edit" && ItemFromMakeAWagerForm) {
            _itemsToRemove.Add(new Ticket.WagerAndItemNumber {
              GameNumber = ((ParlayItem)((BindingSource)sender).List[e.NewIndex]).GameNumber,
              PeriodNumber = ((ParlayItem)((BindingSource)sender).List[e.NewIndex]).PeriodNumber,
              TicketNumber = ((ParlayItem)((BindingSource)sender).List[e.NewIndex]).TicketNumber,
              WagerNumber = Parlay.Number ?? 0,
              ItemNumber = ((ParlayItem)((BindingSource)sender).List[e.NewIndex]).Number,
              ItemDescription = ((ParlayItem)((BindingSource)sender).List[e.NewIndex]).Description
            });
          }
          break;
        case ListChangedType.ItemChanged:
          Parlay.Items[e.NewIndex] = (ParlayItem)((BindingSource)sender).List[e.NewIndex];
          break;
        case ListChangedType.ItemDeleted:
          if (Mdi.TicketNumber != Parlay.TicketNumber) {
            Ticket.WagerItem wi = null;
            if (Mdi.Ticket.WagerItems != null)
              wi = (from wi2 in Mdi.Ticket.WagerItems where wi2.TicketNumber == Parlay.TicketNumber && wi2.WagerNumber == Parlay.Number && wi2.ItemNumber == (e.NewIndex + 1) select wi2).FirstOrDefault();

            Ticket.ContestWagerItem cwi = null;
            if (Mdi.Ticket.ContestWagerItems != null)
              cwi = (from wi3 in Mdi.Ticket.ContestWagerItems where wi3.TicketNumber == Parlay.TicketNumber && wi3.WagerNumber == Parlay.Number && wi3.ItemNumber == (e.NewIndex + 1) select wi3).FirstOrDefault();
            Mdi.Ticket.AddToItemsToRemoveFromDb("P", wi, cwi);
          }
          Parlay.Items.RemoveAt(e.NewIndex);
          Mdi.Ticket.DeleteWagerItem(Parlay.TicketNumber, Parlay.Number, e.NewIndex + 1);
          Mdi.Ticket.DeleteContestWagerItem(Parlay.TicketNumber, Parlay.Number, e.NewIndex + 1);
          Mdi.Ticket.ReIndexWagerItems(Parlay.TicketNumber, Parlay.Number);
          dgvwParlays.Refresh();
          dgvwParlays.ClearSelection();
          break;
      }
      Parlay.ItemUpdated = true;

      if (Parlay.RoundRobin != null) {
        Parlay.RoundRobin.SeedItems = Parlay.Items;
        Parlay.RoundRobin.ResetCombinations();
      }
      if (Parlay != null && Parlay.IsOpen) {
        return;
      }
      ChangeUpDownValue(((BindingSource)sender).List.Count);
      if (FrmSportAndGameSelection.ShowWagerLimitsInFrm.IsActive()) {
        int? wagerNumber = null;
        int ticketNumber = Mdi.TicketNumber;
        if (Parlay != null) {
          ticketNumber = Parlay.TicketNumber;
          wagerNumber = Parlay.Number;
        }
        FrmSportAndGameSelection.ShowWagerLimitsInFrm.FrmPlaceParlay = this;
        FrmSportAndGameSelection.ShowWagerLimitsInFrm.DisplayParlayMaxWagerLimits(ticketNumber, wagerNumber);
      }
      CalculateToWinAmount();
    }

    private void ValidateWager() {
      if (bndsrc.Count == 0) {
        _deleteWagersOnExit = true;
        return;
      }

      if (chbRoundRobin.Checked && cmbRoundRobin.SelectedIndex > -1) {
        Parlay.RoundRobin = null;
        EnableRoundRobin(true);
      }

      if (chbRoundRobin.Checked && cmbRoundRobin.SelectedIndex == -1) {
        const string message = "Please select the parlay combinations for the round robin";
        MessageBox.Show(message, ParameterList.GetItem("AppName").Value, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        cmbRoundRobin.Focus();
        _deleteWagersOnExit = true;
        return;
      }

      if (nudTeams.Value < bndsrc.Count && bndsrc.Count <= nudTeams.Maximum) {
        nudTeams.Value = bndsrc.Count;
      }
      Boolean continueValidating;
      Boolean minWagerBypassed;
      Boolean availableFundsOk;
      Boolean wagerLimitOk;

      var wagerDescription = "";
      if (Customer != null && Customer.ParlayName != null)
        wagerDescription += "Parlay: " + Customer.ParlayName.Trim();
      wagerDescription += " ";

      if (nudTeams.Value == 1) {
        wagerDescription += nudTeams.Value + " team";
      }
      else {
        wagerDescription += nudTeams.Value + " teams";
      }

      wagerDescription += " ";

      if (dgvwParlays.Rows.Count > 0) {
        int i;
        for (i = 0; i < dgvwParlays.Rows.Count; i++) {
          if (i == dgvwParlays.Rows.Count - 1) {
            wagerDescription += dgvwParlays.Rows[i].Cells[1].Value;
          }
          else {
            wagerDescription += dgvwParlays.Rows[i].Cells[1].Value + " ";
          }
        }
      }

      var riskAmount = txtRisk.Text;

      if (chbRoundRobin.Checked) {
        if (Parlay.RoundRobin != null && txtRisk.Text.Length != 0) {
          riskAmount = (Parlay.RoundRobin.TotalParlaysCount * NumberF.ParseDouble(txtRisk.Text)).ToString(CultureInfo.InvariantCulture);
        }
      }
      var minimumWager = FrmSportAndGameSelection.CheckIfBelowMinimumWager(this, LoginsAndProfiles.ACCEPT_ANY_WAGER, txtRisk.Text, out continueValidating, out minWagerBypassed, false, false, _byPassMinimumWager);
      var riskAmountParsed = NumberF.ParseInt(txtRisk.Text);

      if (minimumWager >= 0 && (txtRisk.Text.Length == 0 || riskAmountParsed < minimumWager) && !_byPassMinimumWager) {
        txtRisk.Text = minimumWager.ToString(CultureInfo.InvariantCulture);
      }
      if (!continueValidating) {
        _deleteWagersOnExit = true;
        txtRisk.Focus();
        return;
      }

      if (Parlay.Status != "O")
        FrmSportAndGameSelection.CheckForParlayTeaserAvailableFunds(this, FrmMode, Parlay.TicketNumber, Parlay.Number, LoginsAndProfiles.ACCEPT_ANY_WAGER, wagerDescription, riskAmount, txtToWin.Text, FrmSportAndGameSelection.IsFreePlay, out continueValidating, out availableFundsOk); //Validate if user has sufficient Funds for wager
      else {
        continueValidating = true;
        availableFundsOk = true;
      }

      if (!continueValidating) {
        _deleteWagersOnExit = true;
        return;
      }

      FrmSportAndGameSelection.CheckIfParlayOrTeaserExceedsWagerLimit(this, Parlay.TicketNumber, Parlay.Number, LoginsAndProfiles.ACCEPT_ANY_WAGER, wagerDescription, dgvwParlays.Rows.Count < nudTeams.Value ? "O" : "P", txtRisk.Text, txtToWin.Text, double.Parse(txtRisk.Text), out continueValidating, out wagerLimitOk);

      if (!wagerLimitOk) {
        _deleteWagersOnExit = true;
        return;
      }

      if (minWagerBypassed && availableFundsOk && wagerLimitOk)
        PlaceBet();
    }

    private void SetCentsNumericInputs() {
      //txtRisk.AllowCents = Params.IncludeCents;
      txtToWin.AllowCents = Params.IncludeCents;
    }

    #endregion

    #region Protected Methods

    protected override bool ProcessCmdKey(ref Message msg, Keys keyData) {
      return Mdi.ActivateMdiChildForm(keyData, base.ProcessCmdKey(ref msg, keyData), ActiveMdiChildName);
    }

    #endregion

    #region Events

    private void frmPlaceParlay_Load(object sender, EventArgs e) {
      InitializeForm();
    }

    private void bndsrc_ListChanged(object sender, ListChangedEventArgs e) {
      UpdateParlay(sender, e);
    }

    private void btnCancel_Click(object sender, EventArgs e) {
      _cancelActionPerformed = true;

      if (FrmSportAndGameSelection.ReadbackItems != null &&
          FrmSportAndGameSelection.ReadbackItems.Any(p => p.TicketNumber == TicketNumberToUpd && p.WagerNumber == WagerNumberToUpd && p.RowContainsEditItemInfo)) {
        _deleteWagersOnExit = false;
      }
      CancelWager();
    }

    private void btnPayTable_Click(object sender, EventArgs e) {
      ShowPayCard();
    }

    private void btnPlaceBet_Click(object sender, EventArgs e) {
      _deleteWagersOnExit = false;
      ValidateWager();
    }

    private void chbRoundRobin_CheckedChanged(object sender, EventArgs e) {
      SetRoundRobinMode(((CheckBox)sender).Checked);
    }

    private void txtRisk_KeyUp(object sender, KeyEventArgs e) {
      if (!KeyValidator.IsValidNumericKey(e)) {
        e.Handled = true;
      }
    }

    private void FrmPlaceParlay_FormClosed(object sender, FormClosedEventArgs e) {
      if (ActiveMdiChildName == null) {
        return;
      }
      foreach (var mdiChild in Mdi.MdiChildren) {
        if (mdiChild.Name.ToUpper() == ActiveMdiChildName.ToUpper()) {
          mdiChild.Select();
          break;
        }
      }
      _itemsToRemove = null;
      _wagerEditBackup = null;
      _itemsBackupCopy = null;
      _contestItemsBackupCopy = null;
    }

    private void frmPlaceParlay_FormClosing(object sender, FormClosingEventArgs e) {
      CancelWager(false);
      ResetBetType();
    }

    private void dgvwParlays_CellDoubleClick(object sender, DataGridViewCellEventArgs e) {
      ModifyWagerItem();
    }

    private void dgvwParlays_KeyDown(object sender, KeyEventArgs e) {
      if (e.KeyCode != Keys.Enter) return;
      e.SuppressKeyPress = true;
      btnPlaceBet.PerformClick();
    }

    private void nudTeams_ValueChanged(object sender, EventArgs e) {
      if (Parlay != null && Parlay.RoundRobin != null)
        SetRoundRobinItemsCalculation();
    }

    private void cmbRoundRobin_SelectedIndexChanged(object sender, EventArgs e) {
      if (!chbRoundRobin.Checked)
        chbRoundRobin.Checked = true;
      if (Parlay != null && Parlay.RoundRobin != null)
        SetRoundRobinItemsCalculation();
    }

    private void EditToolStripMenuItem_Click(object sender, EventArgs e) {
      ModifyWagerItem();
    }

    private void DeleteToolStripMenuItem_Click(object sender, EventArgs e) {
      DeleteWagerItem();
    }

    private void dgvwParlays_SelectionChanged(object sender, EventArgs e) {
      ToggleContextMenuInability();
    }

    private void dgvwParlays_MouseDown(object sender, MouseEventArgs e) {
      if (e.Button == MouseButtons.Right) {
        var hti = dgvwParlays.HitTest(e.X, e.Y);
        dgvwParlays.ClearSelection();

        if (hti.RowIndex >= 0) {
          dgvwParlays.Rows[hti.RowIndex].Selected = true;
        }
      }
    }

    private void nudTeams_Click(object sender, EventArgs e) {
      FreezeShownItemsQty();
    }

    private void nudTeams_Enter(object sender, EventArgs e) {
      ((NumericUpDown)sender).Select(0, ((NumericUpDown)sender).Value.ToString(CultureInfo.InvariantCulture).Length);
    }

    private void nudTeams_KeyUp(object sender, KeyEventArgs e) {
      int tryNumber;
      int.TryParse(e.KeyCode.ToString(), out tryNumber);

      if (tryNumber < 1) {
        return;
      }
      FreezeShownItemsQty();
    }

    private void txtRisk_TextChanged(object sender, EventArgs e) {
      try {
        var w = ((NumberTextBox)sender).Text.Replace(",", "").Replace("½", "");
        if (w == "") return;
        double textVal;
        double.TryParse(w, out textVal);
        if (!(textVal > -1)) return;
        double dw;
        double.TryParse(w, out dw);
        CalculateToWinAmount();
        ((NumberTextBox) sender).AllowCents = false;
        ((NumberTextBox) sender).TextChanged -= txtRisk_TextChanged;
        ((NumberTextBox) sender).Text = String.Format("{0:#,###,###}", dw);
        ((NumberTextBox) sender).SelectionStart = ((NumberTextBox) sender).Text.Length;
        ((NumberTextBox) sender).SelectionLength = 0;
        ((NumberTextBox) sender).TextChanged += txtRisk_TextChanged;
        ((NumberTextBox) sender).AllowCents = true;
      }
      catch (Exception ex) {
        Log(ex);
      }
    }

    #endregion

  }
}