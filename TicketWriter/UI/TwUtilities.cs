﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using SIDLibraries.Utilities;

namespace TicketWriter.UI {
  public static class TwUtilities {

    private const string EXTRA40_LEN = "                                        ";
    private const string DRAW = "Draw";

    private sealed class TeamNameAndId {
      public readonly String TeamId;
      public readonly String TeamRotNumber;

      public TeamNameAndId(String teamId, String teamRotNumber) {
        TeamId = teamId;
        TeamRotNumber = teamRotNumber;
      }
    }

    internal static string BuildDescriptionAndAdjustedPrice(spGLGetActiveGamesByCustomer_Result selectedGamePeriodInfo, String wagerType, String totalPntsOu, String chosenTeamId, String adjustedLine, String gamePeriodName) {
      var description = "";
      if (adjustedLine == "0" || adjustedLine == "+0" || adjustedLine == "-0")
          adjustedLine = GamesAndLines.PICK;

      if (adjustedLine == ("0" + GamesAndLines.HALFSYMBOL) || adjustedLine == ("-0" + GamesAndLines.HALFSYMBOL) || adjustedLine == ("+0" + GamesAndLines.HALFSYMBOL))
        adjustedLine = adjustedLine.Replace("0", "");

      switch (wagerType) {
        case WagerType.SPREAD:
        case WagerType.MONEYLINE:
          description = chosenTeamId + " " + adjustedLine;
          break;
        case WagerType.TOTALPOINTS:
          if (selectedGamePeriodInfo != null)
            description = (selectedGamePeriodInfo.Team1ID ?? "").Trim() + "/" + (selectedGamePeriodInfo.Team2ID ?? "").Trim();
          else return "";
          description += " " + gamePeriodName;
          description += " total points ";
          if (totalPntsOu != null) {
            if (totalPntsOu == "O") {
              description += "over";
            }
            else {
              description += "under";
            }
          }
          description += " " + adjustedLine.Replace("+", "");
          break;
        case WagerType.TEAMTOTALPOINTS:
          description = chosenTeamId;
          description += " " + gamePeriodName;
          description += " team points ";
          if (totalPntsOu != null) {
            if (totalPntsOu == "O") {
              description += "over";
            }
            else {
              description += "under";
            }
          }
          description += " " + adjustedLine.Replace("+", "");
          break;
      }

      return description;
    }

    internal static List<string> CalculateTeaserAdjustedLine(string teaserName, IEnumerable<spLOGetTeaserSportSpec_Result> teaserSpecs, String wagerType, String ouType, String sport, String subSport, int rowNumberInGroup, String line, out double teaserPoints) {
      var teaserDefinedLine = "";

      if (teaserSpecs != null) {
        teaserDefinedLine = (from p in teaserSpecs
                             where p.TeaserName.Trim() == teaserName && p.SportType.Trim() == sport && p.SportSubType.Trim() == subSport && p.WagerType.Trim() == wagerType

                             select p.Points).FirstOrDefault().ToString();
      }

      var selectedLine = rowNumberInGroup != 3 ? LineOffering.ConvertToStringBaseLine(line) : "0";

      var result = new List<string>();
      String adjustedLine;

      double parsedSelectedLine, parsedTeaserDefinedLine;
      double.TryParse(selectedLine, out parsedSelectedLine);
      double.TryParse(teaserDefinedLine, out parsedTeaserDefinedLine);

      if (wagerType == WagerType.TEAMTOTALPOINTS || wagerType == WagerType.TOTALPOINTS) {
        adjustedLine = ouType == "O" ? (parsedSelectedLine - parsedTeaserDefinedLine).ToString(CultureInfo.InvariantCulture) : (parsedSelectedLine + parsedTeaserDefinedLine).ToString(CultureInfo.InvariantCulture);
      }
      else {
        adjustedLine = (parsedSelectedLine + parsedTeaserDefinedLine).ToString(CultureInfo.InvariantCulture);
      }

      if (double.Parse(adjustedLine) > 0) {
        adjustedLine = "+" + adjustedLine;
      }

      result.Add(adjustedLine);
      result.Add(teaserDefinedLine);
      teaserPoints = parsedTeaserDefinedLine;
      return result;
    }


    internal static int GetDayOfWeekIdx(DayOfWeek dayOfWeek) {
      var weekDays = new[] { "Everyday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };
      var retIdx = -1;

      for (var i = 0; i < weekDays.Count(); i++) {
        if (weekDays[i] != dayOfWeek.ToString()) continue;
        retIdx = i;
        break;
      }
      return retIdx;
    }

    internal static string DetermineIfShowOnChart(String wagerType, String rifTicketNumber, int itemNumber) {
      string showOnChart;

      if (wagerType == "Straight Bet" || wagerType == "Correlation???") {
        showOnChart = rifTicketNumber == "" ? "Y" : "N";

      }
      else {
        if (wagerType == "If-Bet") {
          showOnChart = itemNumber == 1 ? "Y" : "N";
        }
        else {
          showOnChart = "N";
        }
      }

      return showOnChart;
    }

    internal static double DetermineXtoYToWinAmount(double riskAmt, double moneyLine, double toBase) {
      double toWinAmount = 0;

      if (toBase > 0) {
        toWinAmount = (riskAmt * moneyLine / toBase);
      }

      return toWinAmount;

    }


    internal static double DetermineXtoYRiskAmount(double toWinAmt, double moneyLine, double toBase) {
      double riskAmt = 0;

      if (moneyLine > 0) {
        riskAmt = (toWinAmt * toBase / moneyLine);
      }

      return riskAmt;
    }

    internal static String FormatShortContestDescription(spCnGetActiveContests_Result ci, String priceType, String finalMoney, String thresholdLine, String finalToBase, String wagerTypeName = null) {
      var descStr = "";

      if (wagerTypeName == "Parlay") {
        if (ci.ContestType3 != null && ci.ContestType3.Trim() != ".") {
          descStr += ci.ContestType3.Trim();
        }
        else if (ci.ContestType2 != null && ci.ContestType2.Trim() != ".") {
          descStr += ci.ContestType2.Trim();
        }
        else {
          if (ci.ContestType != null) {
            descStr += ci.ContestType.Trim();
          }
        }

        descStr += " - " + (ci.RotNum ?? 0).ToString();
        descStr += " " + (ci.ContestantName).Trim();
        descStr += " ";
      }
      else {

        if (ci.RotNum != null)
          descStr = ci.RotNum + " - ";

        if (ci.ContestType != null)
          descStr += ci.ContestType.Trim();

        if (ci.ContestType2 != null && ci.ContestType2.Trim() != ".") {
          descStr += " - ";
          descStr += ci.ContestType2.Trim();
        }

        if (ci.ContestType3 != null && ci.ContestType3.Trim() != ".") {
          descStr += " - ";
          descStr += ci.ContestType3.Trim();
        }

        descStr += " - ";
        if (ci.ContestDesc != null)
          descStr += ci.ContestDesc.Trim();

        if (ci.ContestantName != null) {
          descStr += " - ";
          descStr += ci.ContestantName.Trim();
          descStr += " ";
        }
      }

      if (ci.ThresholdType == "P") {
        descStr += LineOffering.ConvertToStringBaseLine(thresholdLine).Replace("+", "").Replace("-", "");
        descStr += " ";
        if (ci.ThresholdUnits != null)
          descStr += ci.ThresholdUnits.Trim();
      }

      if (ci.ThresholdType == "S") {
        descStr += thresholdLine.Replace(".5", GamesAndLines.HALFSYMBOL);
        descStr += " ";
        if (ci.ThresholdUnits != null)
          descStr += ci.ThresholdUnits;
      }

      if (!descStr.EndsWith(" ")) {
        descStr += " ";
      }

      switch (priceType) {
        case "American":
        case "Xtrange":
        case LineOffering.PRICE_AMERICAN:
          if (!string.IsNullOrEmpty(finalToBase) && double.Parse(finalToBase) > 0) {
            descStr += finalMoney;
            descStr += " to ";
            descStr += finalToBase;
          }
          else {
            if (double.Parse(finalMoney) > 0) {
              if (!finalMoney.StartsWith("+"))
                descStr += "+";
            }
            descStr += finalMoney;
          }
          break;
        case "Decimal":
        case LineOffering.PRICE_DECIMAL:
          var finalDecimal = double.Parse(finalMoney);

          descStr += finalDecimal;
          if (finalDecimal == Math.Floor(finalDecimal)) {
            descStr += ".0";
          }
          break;
        case "Fractional":
        case LineOffering.PRICE_FRACTIONAL:
          descStr += finalMoney;
          descStr += "/";
          descStr += finalToBase;
          break;
      }
      return descStr;
    }

    internal static void GetAvailableGamePeriodsForDropDown(ComboBox cmb, IEnumerable<spGLGetActiveGamesByCustomer_Result> selectedGameOptions, int selectedGameNumber, String currentGamePeriodName) {
      var selGameOptions = (from g in selectedGameOptions where g.GameNum == selectedGameNumber orderby g.PeriodNumber ascending select g).ToList();

      if (cmb.Items.Count > 0) {
        cmb.Items.Clear();
      }
      var item = new ComboBoxItem();
      var holdPeriodNumber = -1;

      foreach (var av in selGameOptions) {
        item.Text = av.PeriodDescription;
        item.Value = av.PeriodNumber;
        if (item.Text.Trim() != currentGamePeriodName.Trim() && holdPeriodNumber != av.PeriodNumber)
          cmb.Items.Add(item);
        holdPeriodNumber = av.PeriodNumber;
      }
    }

    internal static string GetChosenTeamRotNumber(spGLGetActiveGamesByCustomer_Result selectedGamePeriodInfo, String chosenTeamId) {
      var chosenTeamIdRotNum = "";

      var teamsIds = new List<TeamNameAndId>
                {
                    new TeamNameAndId(selectedGamePeriodInfo.Team1ID, selectedGamePeriodInfo.Team1RotNum.ToString()),
                    new TeamNameAndId(selectedGamePeriodInfo.Team2ID, selectedGamePeriodInfo.Team2RotNum.ToString())
                };


      if (chosenTeamId == FrmMakeAWager.Draw) {
        chosenTeamIdRotNum = selectedGamePeriodInfo.DrawRotNum.ToString();
      }
      else {
        foreach (var t in teamsIds.Where(t => t.TeamId.Trim() == chosenTeamId.Trim())) {
          chosenTeamIdRotNum = t.TeamRotNumber.ToString(CultureInfo.InvariantCulture);
          break;
        }
      }

      return chosenTeamIdRotNum;
    }

    internal static string GetChosenTeamIdDescription(spGLGetActiveGamesByCustomer_Result selectedGameOptions, string wagerType, String teamId) {
      var chosenTeamId = teamId;

      if (teamId == DRAW) {
        var drawTeamId = "";
        if (selectedGameOptions != null && selectedGameOptions.Team1ID != null && selectedGameOptions.Team2ID != null)
          drawTeamId = " (" + selectedGameOptions.Team1ID.Trim() + " vs " + selectedGameOptions.Team2ID.Trim() + ")";

        chosenTeamId += drawTeamId;
      }

      if (wagerType != WagerType.TOTALPOINTS) return chosenTeamId;
      if (selectedGameOptions != null && selectedGameOptions.Team1ID != null && selectedGameOptions.Team2ID != null)
        chosenTeamId = selectedGameOptions.Team1ID.Trim() + "/" + selectedGameOptions.Team2ID.Trim();

      return chosenTeamId;
    }

    internal static double GetOriginalLine(string wagerType, spGLGetActiveGamesByCustomer_Result selectedGamePeriodInfo, String chosenTeamId, int selectedRowNumberInGroup) {
      double baseLine = 0;
      if (selectedGamePeriodInfo == null)
        return baseLine;

      switch (wagerType) {
        case WagerType.SPREAD:
          baseLine = selectedGamePeriodInfo.Spread ?? 0;
          var chosenIsFavored = chosenTeamId != null && chosenTeamId.Trim() == (selectedGamePeriodInfo.FavoredTeamID ?? "").Trim();
          if (!chosenIsFavored)
            baseLine = baseLine * -1;
          break;
        case WagerType.MONEYLINE:
          switch (selectedRowNumberInGroup) {
            case 1:
              if (selectedGamePeriodInfo.MoneyLine1 == null)
                baseLine = 0;
              else
                baseLine = (double)selectedGamePeriodInfo.MoneyLine1;
              break;
            case 2:
              if (selectedGamePeriodInfo.MoneyLine2 == null)
                baseLine = 0;
              else
                baseLine = (double)selectedGamePeriodInfo.MoneyLine2;
              break;
            case 3:
              if (selectedGamePeriodInfo.MoneyLineDraw == null)
                baseLine = 0;
              else
                baseLine = (double)selectedGamePeriodInfo.MoneyLineDraw;
              break;
          }
          break;
        case WagerType.TOTALPOINTS:
          baseLine = selectedGamePeriodInfo.TotalPoints ?? 0;
          break;
        case WagerType.TEAMTOTALPOINTS:
          switch (selectedRowNumberInGroup) {
            case 1:
              baseLine = selectedGamePeriodInfo.Team1TotalPoints ?? 0;
              break;
            case 2:
              baseLine = selectedGamePeriodInfo.Team2TotalPoints ?? 0;
              break;
            case 3:
              baseLine = 0;
              break;
          }
          break;
      }

      return baseLine;
    }

    internal static string GetSelectedSportSubType(spGLGetActiveGamesByCustomer_Result selectedGamePeriodInfo) {
      var sportSubType = "";
      if (selectedGamePeriodInfo != null) {
        sportSubType = selectedGamePeriodInfo.SportSubType;
      }
      return sportSubType;
    }

    internal static string GetSelectedSportType(spGLGetActiveGamesByCustomer_Result selectedGamePeriodInfo) {
      var sportType = "";
      if (selectedGamePeriodInfo != null) {
        sportType = selectedGamePeriodInfo.SportType;
      }
      return sportType;
    }

    public static void AdjustGridColumnsWidth(DataGridView grid) {
      for (var i = 0; i < grid.Columns.Count; i++) {
        grid.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
        var widthCol = grid.Columns[i].Width;
        if (i == 1 || i == 2)
          widthCol += EXTRA40_LEN.Length;
        grid.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
        grid.Columns[i].Width = widthCol;
      }
      if (grid.Columns.Count > 2 && !(grid.Columns[2].Width > 600)) grid.Columns[2].Width = 600;

    }

    internal static string AddLeadingZeroes(string datePart) {
      var retVal = "";
      if (!datePart.Contains('/')) return retVal;
      var dateParts = datePart.Split('/');

      if (int.Parse(dateParts[0]) < 10) {
        if (!dateParts[0].Contains("0"))
          retVal += "0" + dateParts[0];
        else
          retVal += dateParts[0];
      }
      else
        retVal += dateParts[0];
      retVal += "/";
      if (int.Parse(dateParts[1]) < 10) {
        if (!dateParts[1].Contains("0"))
          retVal += "0" + dateParts[1];
        else
          retVal += dateParts[1];
      }
      else
        retVal += dateParts[1];

      return retVal;
    }

    internal static Ticket.SelectedContestAndLine ConvertToContestInfoObject(spCnGetActiveContests_Result contestInfo, int ticketNumber, int wagerNumber, int itemNumber) {
      var retObj = new Ticket.SelectedContestAndLine {
        TicketNumber = ticketNumber,
        WagerNumber = wagerNumber,
        ItemNumber = itemNumber,
        ContestNum = contestInfo.ContestNum,
        ContestDesc = contestInfo.ContestDesc,
        Status = contestInfo.Status,
        ContestDateTime = contestInfo.ContestDateTime,
        WagerCutoff = contestInfo.WagerCutoff,
        ContestType = contestInfo.ContestType,
        Comments = contestInfo.Comments,
        ContestantName = contestInfo.ContestantName,
        RotNum = contestInfo.RotNum,
        Outcome = contestInfo.Outcome,
        Store = contestInfo.Store,
        CustProfile = contestInfo.CustProfile,
        LineSeq = contestInfo.LineSeq,
        MoneyLine = contestInfo.MoneyLine,
        ToBase = contestInfo.ToBase,
        CustomerID = contestInfo.CustomerID,
        ContestType2 = contestInfo.ContestType2,
        ContestType3 = contestInfo.ContestType3,
        ThresholdLine = contestInfo.ThresholdLine,
        ThresholdType = contestInfo.ThresholdType,
        ThresholdUnits = contestInfo.ThresholdUnits,
        XtoYLineRep = contestInfo.XtoYLineRep,
        CircledMaxWager = contestInfo.CircledMaxWager,
        ContestantNum = contestInfo.ContestantNum,
        CorrelationID = contestInfo.CorrelationID,
        DecimalOdds = contestInfo.DecimalOdds,
        Numerator = contestInfo.Numerator,
        Denominator = contestInfo.Denominator,
        ContestantMaxWager = contestInfo.ContestantMaxWager,
        GradeNum = contestInfo.GradeNum,
        AdjWagersToFinal = contestInfo.AdjWagersToFinal,
        NumberOfContestants = contestInfo.NumberOfContestants,
        Scratched = contestInfo.Scratched,
        ContestantSeq = contestInfo.ContestantSeq,
        FirstRotNum = contestInfo.FirstRotNum
      };

      return retObj;
    }

    internal static spCnGetActiveContests_Result ConvertToContestInfoObject(Ticket.SelectedContestAndLine contestInfo) {
      var retObj = new spCnGetActiveContests_Result {
        ContestNum = contestInfo.ContestNum,
        ContestDesc = contestInfo.ContestDesc,
        Status = contestInfo.Status,
        ContestDateTime = contestInfo.ContestDateTime,
        WagerCutoff = contestInfo.WagerCutoff,
        ContestType = contestInfo.ContestType,
        Comments = contestInfo.Comments,
        ContestantName = contestInfo.ContestantName,
        RotNum = contestInfo.RotNum,
        Outcome = contestInfo.Outcome,
        Store = contestInfo.Store,
        CustProfile = contestInfo.CustProfile,
        LineSeq = contestInfo.LineSeq,
        MoneyLine = contestInfo.MoneyLine,
        ToBase = contestInfo.ToBase,
        CustomerID = contestInfo.CustomerID,
        ContestType2 = contestInfo.ContestType2,
        ContestType3 = contestInfo.ContestType3,
        ThresholdLine = contestInfo.ThresholdLine,
        ThresholdType = contestInfo.ThresholdType,
        ThresholdUnits = contestInfo.ThresholdUnits,
        XtoYLineRep = contestInfo.XtoYLineRep,
        CircledMaxWager = contestInfo.CircledMaxWager,
        ContestantNum = contestInfo.ContestantNum,
        CorrelationID = contestInfo.CorrelationID,
        DecimalOdds = contestInfo.DecimalOdds,
        Numerator = contestInfo.Numerator,
        Denominator = contestInfo.Denominator,
        ContestantMaxWager = contestInfo.ContestantMaxWager,
        GradeNum = contestInfo.GradeNum,
        AdjWagersToFinal = contestInfo.AdjWagersToFinal,
        NumberOfContestants = contestInfo.NumberOfContestants,
        Scratched = contestInfo.Scratched,
        ContestantSeq = contestInfo.ContestantSeq,
        FirstRotNum = contestInfo.FirstRotNum
      };

      return retObj;
    }

    internal static spGLGetActiveGamesByCustomer_Result ConvertToGameObject(Ticket.SelectedGameAndLine gameInfo, string currentWagerTypeName, bool vigDiscountForAllWagerTypes) {
      var retObj = new spGLGetActiveGamesByCustomer_Result {
        CustomerID = gameInfo.CustomerID,
        Store = gameInfo.Store,
        CustProfile = gameInfo.CustProfile,
        GameNum = gameInfo.GameNum,
        GameDateTime = gameInfo.GameDateTime,
        WagerCutoff = gameInfo.WagerCutoff,
        Status = gameInfo.Status,
        SportType = gameInfo.SportType,
        Team1ID = gameInfo.Team1ID,
        Team1RotNum = gameInfo.Team1RotNum,
        Team2ID = gameInfo.Team2ID,
        Team2RotNum = gameInfo.Team2RotNum,
        DrawRotNum = gameInfo.DrawRotNum,
        Comments = gameInfo.Comments,
        SportSubType = gameInfo.SportSubType,
        OnTV = gameInfo.OnTV,
        Team1FinalScore = gameInfo.Team1FinalScore,
        Team2FinalScore = gameInfo.Team2FinalScore,
        FinalWinnerID = gameInfo.FinalWinnerID,
        PeriodNumber = gameInfo.PeriodNumber,
        LineSeq = gameInfo.LineSeq,
        FavoredTeamID = gameInfo.FavoredTeamID,
        Spread = gameInfo.Spread,
        SpreadAdj1 = gameInfo.SpreadAdj1,
        SpreadAdj2 = gameInfo.SpreadAdj2,
        TotalPoints = gameInfo.TotalPoints,
        TtlPtsAdj1 = gameInfo.TtlPtsAdj1,
        TtlPtsAdj2 = gameInfo.TtlPtsAdj2,
        MoneyLine1 = gameInfo.MoneyLine1,
        MoneyLine2 = gameInfo.MoneyLine2,
        MoneyLineDraw = gameInfo.MoneyLineDraw,
        PeriodDescription = gameInfo.PeriodDescription,
        Team1TotalPoints = gameInfo.Team1TotalPoints,
        Team2TotalPoints = gameInfo.Team2TotalPoints,
        Team1TtlPtsAdj1 = gameInfo.Team1TtlPtsAdj1,
        Team1TtlPtsAdj2 = gameInfo.Team1TtlPtsAdj2,
        Team2TtlPtsAdj1 = gameInfo.Team2TtlPtsAdj1,
        Team2TtlPtsAdj2 = gameInfo.Team2TtlPtsAdj2,
        ScheduleDate = gameInfo.ScheduleDate,
        BroadcastInfo = gameInfo.BroadcastInfo,
        ListedPitcher1 = gameInfo.ListedPitcher1,
        ListedPitcher2 = gameInfo.ListedPitcher2,
        ParlayRestriction = gameInfo.ParlayRestriction,
        PuckLine = gameInfo.PuckLine,
        LastSpreadChange = gameInfo.LastSpreadChange,
        LastMoneyLineChange = gameInfo.LastMoneyLineChange,
        LastTtlPtsChange = gameInfo.LastTtlPtsChange,
        LastTeamPtsChange = gameInfo.LastTeamPtsChange,
        PeriodWagerCutoff = gameInfo.PeriodWagerCutoff,
        ScheduleText = gameInfo.ScheduleText,
        CircledMaxWagerMoneyLine = gameInfo.CircledMaxWagerMoneyLine,
        CircledMaxWagerSpread = gameInfo.CircledMaxWagerSpread,
        CircledMaxWagerTeamTotal = gameInfo.CircledMaxWagerTeamTotal,
        CircledMaxWagerTotal = gameInfo.CircledMaxWagerTotal,
        TimeChangeFlag = gameInfo.TimeChangeFlag,
        CorrelationID = gameInfo.CorrelationID,
        PreventPointBuyingFlag = gameInfo.PreventPointBuyingFlag,
        AllowBuyOnPoints = gameInfo.AllowBuyOnPoints,
        AllowBuyOnSpread = gameInfo.AllowBuyOnSpread,
        CircledMaxWager = gameInfo.CircledMaxWager,
        TeamActionLinePos = gameInfo.TeamActionLinePos,
        GradeDateTime = gameInfo.GradeDateTime,
        KeepOpenFlag = gameInfo.KeepOpenFlag,
        LinkedToStoreFlag = gameInfo.LinkedToStoreFlag,
        MoneyLineDecimal1 = gameInfo.MoneyLineDecimal1,
        MoneyLineDecimal2 = gameInfo.MoneyLineDecimal2,
        MoneyLineDecimalDraw = gameInfo.MoneyLineDecimalDraw,
        MoneyLineDenominator1 = gameInfo.MoneyLineDenominator1,
        MoneyLineDenominator2 = gameInfo.MoneyLineDenominator2,
        MoneyLineDenominatorDraw = gameInfo.MoneyLineDenominatorDraw,
        MoneyLineNumerator1 = gameInfo.MoneyLineNumerator1,
        MoneyLineNumerator2 = gameInfo.MoneyLineNumerator2,
        MoneyLineNumeratorDraw = gameInfo.MoneyLineNumeratorDraw,
        OnlineFeedMoneyLine = gameInfo.OnlineFeedMoneyLine,
        OnlineFeedSpread = gameInfo.OnlineFeedSpread,
        OnlineFeedTotalPoints = gameInfo.OnlineFeedTotalPoints,
        Pitcher1StartedFlag = gameInfo.Pitcher1StartedFlag,
        Pitcher2StartedFlag = gameInfo.Pitcher2StartedFlag,
        ScheduleID = gameInfo.ScheduleID,
        SpreadDecimal1 = gameInfo.SpreadDecimal1,
        SpreadDecimal2 = gameInfo.SpreadDecimal2,
        SpreadDenominator1 = gameInfo.SpreadDenominator1,
        SpreadDenominator2 = gameInfo.SpreadDenominator2,
        SpreadNumerator1 = gameInfo.SpreadNumerator1,
        SpreadNumerator2 = gameInfo.SpreadNumerator2,
        Team1Nss = gameInfo.Team1Nss,
        Team1TtlPtsDecimal1 = gameInfo.Team1TtlPtsDecimal1,
        Team1TtlPtsDecimal2 = gameInfo.Team1TtlPtsDecimal2,
        Team1TtlPtsDenominator1 = gameInfo.Team1TtlPtsDenominator1,
        Team1TtlPtsDenominator2 = gameInfo.Team1TtlPtsDenominator2,
        Team1TtlPtsNumerator1 = gameInfo.Team1TtlPtsNumerator1,
        Team1TtlPtsNumerator2 = gameInfo.Team1TtlPtsNumerator2,
        Team2Nss = gameInfo.Team2Nss,
        Team2TtlPtsDecimal1 = gameInfo.Team2TtlPtsDecimal1,
        Team2TtlPtsDecimal2 = gameInfo.Team2TtlPtsDecimal2,
        Team2TtlPtsDenominator1 = gameInfo.Team2TtlPtsDenominator1,
        Team2TtlPtsDenominator2 = gameInfo.Team2TtlPtsDenominator2,
        Team2TtlPtsNumerator1 = gameInfo.Team2TtlPtsNumerator1,
        Team2TtlPtsNumerator2 = gameInfo.Team2TtlPtsNumerator2,
        TtlPointsDecimal1 = gameInfo.TtlPointsDecimal1,
        TtlPointsDecimal2 = gameInfo.TtlPointsDecimal2,
        TtlPointsDenominator1 = gameInfo.TtlPointsDenominator1,
        TtlPointsDenominator2 = gameInfo.TtlPointsDenominator2,
        TtlPointsNumerator1 = gameInfo.TtlPointsNumerator1,
        TtlPointsNumerator2 = gameInfo.TtlPointsNumerator2,
        AltScorePeriodNumber1 = gameInfo.AltScorePeriodNumber1,
        AltScorePeriodNumber2 = gameInfo.AltScorePeriodNumber2,
        PreReqPeriodNumber = gameInfo.PreReqPeriodNumber,
        ShortDescription = gameInfo.ShortDescription
      };

      if (gameInfo.PreviousToApplyVigDiscountAdjsObj != null && (currentWagerTypeName != "Straight Bet" && !vigDiscountForAllWagerTypes)) {
        retObj.SpreadAdj1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.SpreadAdj1;
        retObj.SpreadDecimal1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.SpreadDecimal1;
        retObj.SpreadNumerator1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.SpreadNumerator1;
        retObj.SpreadDenominator1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.SpreadDenominator1;

        retObj.SpreadAdj2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.SpreadAdj2;
        retObj.SpreadDecimal2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.SpreadDecimal2;
        retObj.SpreadNumerator2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.SpreadNumerator2;
        retObj.SpreadDenominator2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.SpreadDenominator2;

        retObj.MoneyLine1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLine1;
        retObj.MoneyLineDecimal1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLineDecimal1;
        retObj.MoneyLineNumerator1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLineNumerator1;
        retObj.MoneyLineDenominator1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLineDenominator1;

        retObj.MoneyLine2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLine2;
        retObj.MoneyLineDecimal2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLineDecimal2;
        retObj.MoneyLineNumerator2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLineNumerator2;
        retObj.MoneyLineDenominator2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLineDenominator2;

        retObj.MoneyLineDraw = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLineDraw;
        retObj.MoneyLineDecimalDraw = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLineDecimalDraw;
        retObj.MoneyLineNumeratorDraw = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLineNumeratorDraw;
        retObj.MoneyLineDenominatorDraw = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLineDenominatorDraw;

        retObj.TtlPtsAdj1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.TtlPtsAdj1;
        retObj.TtlPointsDecimal1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.TtlPointsDecimal1;
        retObj.TtlPointsNumerator1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.TtlPointsNumerator1;
        retObj.TtlPointsDenominator1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.TtlPointsDenominator1;

        retObj.TtlPtsAdj2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.TtlPtsAdj2;
        retObj.TtlPointsDecimal2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.TtlPointsDecimal2;
        retObj.TtlPointsNumerator2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.TtlPointsNumerator2;
        retObj.TtlPointsDenominator2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.TtlPointsDenominator2;

        retObj.Team1TtlPtsAdj1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team1TtlPtsAdj1;
        retObj.Team1TtlPtsDecimal1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team1TtlPtsDecimal1;
        retObj.Team1TtlPtsNumerator1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team1TtlPtsNumerator1;
        retObj.Team1TtlPtsDenominator1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team1TtlPtsDenominator1;

        retObj.Team1TtlPtsAdj2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team1TtlPtsAdj2;
        retObj.Team1TtlPtsDecimal2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team1TtlPtsDecimal2;
        retObj.Team1TtlPtsNumerator2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team1TtlPtsNumerator2;
        retObj.Team1TtlPtsDenominator2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team1TtlPtsDenominator2;

        retObj.Team2TtlPtsAdj1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team2TtlPtsAdj1;
        retObj.Team2TtlPtsDecimal1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team2TtlPtsDecimal1;
        retObj.Team2TtlPtsNumerator1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team2TtlPtsNumerator1;
        retObj.Team2TtlPtsDenominator1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team2TtlPtsDenominator1;

        retObj.Team2TtlPtsAdj2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team2TtlPtsAdj2;
        retObj.Team2TtlPtsDecimal2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team2TtlPtsDecimal2;
        retObj.Team2TtlPtsNumerator2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team2TtlPtsNumerator2;
        retObj.Team2TtlPtsDenominator2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team2TtlPtsDenominator2;
      }

      return retObj;
    }

    internal static spGLGetActiveGamesByCustomer_Result ConvertToGameObject(spGLGetActiveGamesByCustomer_Result gameInfo, String currentWagerTypeName, bool vigDiscountForAllWagerTypes) {
      if (gameInfo.PreviousToApplyVigDiscountAdjsObj == null || vigDiscountForAllWagerTypes || currentWagerTypeName == "Straight Bet") {
        return gameInfo;
      }
      var retObj = new spGLGetActiveGamesByCustomer_Result {
        CustomerID = gameInfo.CustomerID,
        Store = gameInfo.Store,
        CustProfile = gameInfo.CustProfile,
        GameNum = gameInfo.GameNum,
        GameDateTime = gameInfo.GameDateTime,
        WagerCutoff = gameInfo.WagerCutoff,
        Status = gameInfo.Status,
        SportType = gameInfo.SportType,
        Team1ID = gameInfo.Team1ID,
        Team1RotNum = gameInfo.Team1RotNum,
        Team2ID = gameInfo.Team2ID,
        Team2RotNum = gameInfo.Team2RotNum,
        DrawRotNum = gameInfo.DrawRotNum,
        Comments = gameInfo.Comments,
        SportSubType = gameInfo.SportSubType,
        OnTV = gameInfo.OnTV,
        Team1FinalScore = gameInfo.Team1FinalScore,
        Team2FinalScore = gameInfo.Team2FinalScore,
        FinalWinnerID = gameInfo.FinalWinnerID,
        PeriodNumber = gameInfo.PeriodNumber,
        LineSeq = gameInfo.LineSeq,
        FavoredTeamID = gameInfo.FavoredTeamID,
        Spread = gameInfo.Spread,
        SpreadAdj1 = gameInfo.SpreadAdj1,
        SpreadAdj2 = gameInfo.SpreadAdj2,
        TotalPoints = gameInfo.TotalPoints,
        TtlPtsAdj1 = gameInfo.TtlPtsAdj1,
        TtlPtsAdj2 = gameInfo.TtlPtsAdj2,
        MoneyLine1 = gameInfo.MoneyLine1,
        MoneyLine2 = gameInfo.MoneyLine2,
        MoneyLineDraw = gameInfo.MoneyLineDraw,
        PeriodDescription = gameInfo.PeriodDescription,
        Team1TotalPoints = gameInfo.Team1TotalPoints,
        Team2TotalPoints = gameInfo.Team2TotalPoints,
        Team1TtlPtsAdj1 = gameInfo.Team1TtlPtsAdj1,
        Team1TtlPtsAdj2 = gameInfo.Team1TtlPtsAdj2,
        Team2TtlPtsAdj1 = gameInfo.Team2TtlPtsAdj1,
        Team2TtlPtsAdj2 = gameInfo.Team2TtlPtsAdj2,
        ScheduleDate = gameInfo.ScheduleDate,
        BroadcastInfo = gameInfo.BroadcastInfo,
        ListedPitcher1 = gameInfo.ListedPitcher1,
        ListedPitcher2 = gameInfo.ListedPitcher2,
        ParlayRestriction = gameInfo.ParlayRestriction,
        PuckLine = gameInfo.PuckLine,
        LastSpreadChange = gameInfo.LastSpreadChange,
        LastMoneyLineChange = gameInfo.LastMoneyLineChange,
        LastTtlPtsChange = gameInfo.LastTtlPtsChange,
        LastTeamPtsChange = gameInfo.LastTeamPtsChange,
        PeriodWagerCutoff = gameInfo.PeriodWagerCutoff,
        ScheduleText = gameInfo.ScheduleText,
        CircledMaxWagerMoneyLine = gameInfo.CircledMaxWagerMoneyLine,
        CircledMaxWagerSpread = gameInfo.CircledMaxWagerSpread,
        CircledMaxWagerTeamTotal = gameInfo.CircledMaxWagerTeamTotal,
        CircledMaxWagerTotal = gameInfo.CircledMaxWagerTotal,
        TimeChangeFlag = gameInfo.TimeChangeFlag,
        CorrelationID = gameInfo.CorrelationID,
        PreventPointBuyingFlag = gameInfo.PreventPointBuyingFlag,
        AllowBuyOnPoints = gameInfo.AllowBuyOnPoints,
        AllowBuyOnSpread = gameInfo.AllowBuyOnSpread,
        CircledMaxWager = gameInfo.CircledMaxWager,
        TeamActionLinePos = gameInfo.TeamActionLinePos,
        GradeDateTime = gameInfo.GradeDateTime,
        KeepOpenFlag = gameInfo.KeepOpenFlag,
        LinkedToStoreFlag = gameInfo.LinkedToStoreFlag,
        MoneyLineDecimal1 = gameInfo.MoneyLineDecimal1,
        MoneyLineDecimal2 = gameInfo.MoneyLineDecimal2,
        MoneyLineDecimalDraw = gameInfo.MoneyLineDecimalDraw,
        MoneyLineDenominator1 = gameInfo.MoneyLineDenominator1,
        MoneyLineDenominator2 = gameInfo.MoneyLineDenominator2,
        MoneyLineDenominatorDraw = gameInfo.MoneyLineDenominatorDraw,
        MoneyLineNumerator1 = gameInfo.MoneyLineNumerator1,
        MoneyLineNumerator2 = gameInfo.MoneyLineNumerator2,
        MoneyLineNumeratorDraw = gameInfo.MoneyLineNumeratorDraw,
        OnlineFeedMoneyLine = gameInfo.OnlineFeedMoneyLine,
        OnlineFeedSpread = gameInfo.OnlineFeedSpread,
        OnlineFeedTotalPoints = gameInfo.OnlineFeedTotalPoints,
        Pitcher1StartedFlag = gameInfo.Pitcher1StartedFlag,
        Pitcher2StartedFlag = gameInfo.Pitcher2StartedFlag,
        ScheduleID = gameInfo.ScheduleID,
        SpreadDecimal1 = gameInfo.SpreadDecimal1,
        SpreadDecimal2 = gameInfo.SpreadDecimal2,
        SpreadDenominator1 = gameInfo.SpreadDenominator1,
        SpreadDenominator2 = gameInfo.SpreadDenominator2,
        SpreadNumerator1 = gameInfo.SpreadNumerator1,
        SpreadNumerator2 = gameInfo.SpreadNumerator2,
        Team1Nss = gameInfo.Team1Nss,
        Team1TtlPtsDecimal1 = gameInfo.Team1TtlPtsDecimal1,
        Team1TtlPtsDecimal2 = gameInfo.Team1TtlPtsDecimal2,
        Team1TtlPtsDenominator1 = gameInfo.Team1TtlPtsDenominator1,
        Team1TtlPtsDenominator2 = gameInfo.Team1TtlPtsDenominator2,
        Team1TtlPtsNumerator1 = gameInfo.Team1TtlPtsNumerator1,
        Team1TtlPtsNumerator2 = gameInfo.Team1TtlPtsNumerator2,
        Team2Nss = gameInfo.Team2Nss,
        Team2TtlPtsDecimal1 = gameInfo.Team2TtlPtsDecimal1,
        Team2TtlPtsDecimal2 = gameInfo.Team2TtlPtsDecimal2,
        Team2TtlPtsDenominator1 = gameInfo.Team2TtlPtsDenominator1,
        Team2TtlPtsDenominator2 = gameInfo.Team2TtlPtsDenominator2,
        Team2TtlPtsNumerator1 = gameInfo.Team2TtlPtsNumerator1,
        Team2TtlPtsNumerator2 = gameInfo.Team2TtlPtsNumerator2,
        TtlPointsDecimal1 = gameInfo.TtlPointsDecimal1,
        TtlPointsDecimal2 = gameInfo.TtlPointsDecimal2,
        TtlPointsDenominator1 = gameInfo.TtlPointsDenominator1,
        TtlPointsDenominator2 = gameInfo.TtlPointsDenominator2,
        TtlPointsNumerator1 = gameInfo.TtlPointsNumerator1,
        TtlPointsNumerator2 = gameInfo.TtlPointsNumerator2,
        AltScorePeriodNumber1 = gameInfo.AltScorePeriodNumber1,
        AltScorePeriodNumber2 = gameInfo.AltScorePeriodNumber2,
        PreReqPeriodNumber = gameInfo.PreReqPeriodNumber,
        ShortDescription = gameInfo.ShortDescription
      };

      if (gameInfo.PreviousToApplyVigDiscountAdjsObj != null /*&& currentWagerTypeName != "Straight Bet" && !vigDiscountForAllWagerTypes*/) {
        retObj.SpreadAdj1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.SpreadAdj1;
        retObj.SpreadDecimal1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.SpreadDecimal1;
        retObj.SpreadNumerator1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.SpreadNumerator1;
        retObj.SpreadDenominator1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.SpreadDenominator1;

        retObj.SpreadAdj2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.SpreadAdj2;
        retObj.SpreadDecimal2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.SpreadDecimal2;
        retObj.SpreadNumerator2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.SpreadNumerator2;
        retObj.SpreadDenominator2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.SpreadDenominator2;

        retObj.MoneyLine1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLine1;
        retObj.MoneyLineDecimal1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLineDecimal1;
        retObj.MoneyLineNumerator1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLineNumerator1;
        retObj.MoneyLineDenominator1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLineDenominator1;

        retObj.MoneyLine2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLine2;
        retObj.MoneyLineDecimal2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLineDecimal2;
        retObj.MoneyLineNumerator2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLineNumerator2;
        retObj.MoneyLineDenominator2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLineDenominator2;

        retObj.MoneyLineDraw = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLineDraw;
        retObj.MoneyLineDecimalDraw = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLineDecimalDraw;
        retObj.MoneyLineNumeratorDraw = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLineNumeratorDraw;
        retObj.MoneyLineDenominatorDraw = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLineDenominatorDraw;

        retObj.TtlPtsAdj1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.TtlPtsAdj1;
        retObj.TtlPointsDecimal1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.TtlPointsDecimal1;
        retObj.TtlPointsNumerator1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.TtlPointsNumerator1;
        retObj.TtlPointsDenominator1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.TtlPointsDenominator1;

        retObj.TtlPtsAdj2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.TtlPtsAdj2;
        retObj.TtlPointsDecimal2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.TtlPointsDecimal2;
        retObj.TtlPointsNumerator2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.TtlPointsNumerator2;
        retObj.TtlPointsDenominator2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.TtlPointsDenominator2;

        retObj.Team1TtlPtsAdj1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team1TtlPtsAdj1;
        retObj.Team1TtlPtsDecimal1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team1TtlPtsDecimal1;
        retObj.Team1TtlPtsNumerator1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team1TtlPtsNumerator1;
        retObj.Team1TtlPtsDenominator1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team1TtlPtsDenominator1;

        retObj.Team1TtlPtsAdj2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team1TtlPtsAdj2;
        retObj.Team1TtlPtsDecimal2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team1TtlPtsDecimal2;
        retObj.Team1TtlPtsNumerator2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team1TtlPtsNumerator2;
        retObj.Team1TtlPtsDenominator2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team1TtlPtsDenominator2;

        retObj.Team2TtlPtsAdj1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team2TtlPtsAdj1;
        retObj.Team2TtlPtsDecimal1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team2TtlPtsDecimal1;
        retObj.Team2TtlPtsNumerator1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team2TtlPtsNumerator1;
        retObj.Team2TtlPtsDenominator1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team2TtlPtsDenominator1;

        retObj.Team2TtlPtsAdj2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team2TtlPtsAdj2;
        retObj.Team2TtlPtsDecimal2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team2TtlPtsDecimal2;
        retObj.Team2TtlPtsNumerator2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team2TtlPtsNumerator2;
        retObj.Team2TtlPtsDenominator2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team2TtlPtsDenominator2;
      }
      return retObj;
    }

    internal static Ticket.SelectedGameAndLine ConvertToGameObject(spGLGetActiveGamesByCustomer_Result gameInfo, int ticketNumber, int wagerNumber, int itemNumber) {
      var retObj = new Ticket.SelectedGameAndLine {
        TicketNumber = ticketNumber,
        WagerNumber = wagerNumber,
        ItemNumber = itemNumber,
        CustomerID = gameInfo.CustomerID,
        Store = gameInfo.Store,
        CustProfile = gameInfo.CustProfile,
        GameNum = gameInfo.GameNum,
        GameDateTime = gameInfo.GameDateTime,
        WagerCutoff = gameInfo.WagerCutoff,
        Status = gameInfo.Status,
        SportType = gameInfo.SportType,
        Team1ID = gameInfo.Team1ID,
        Team1RotNum = gameInfo.Team1RotNum,
        Team2ID = gameInfo.Team2ID,
        Team2RotNum = gameInfo.Team2RotNum,
        DrawRotNum = gameInfo.DrawRotNum,
        Comments = gameInfo.Comments,
        SportSubType = gameInfo.SportSubType,
        OnTV = gameInfo.OnTV,
        Team1FinalScore = gameInfo.Team1FinalScore,
        Team2FinalScore = gameInfo.Team2FinalScore,
        FinalWinnerID = gameInfo.FinalWinnerID,
        PeriodNumber = gameInfo.PeriodNumber,
        LineSeq = gameInfo.LineSeq,
        FavoredTeamID = gameInfo.FavoredTeamID,
        Spread = gameInfo.Spread,
        SpreadAdj1 = gameInfo.SpreadAdj1,
        SpreadAdj2 = gameInfo.SpreadAdj2,
        TotalPoints = gameInfo.TotalPoints,
        TtlPtsAdj1 = gameInfo.TtlPtsAdj1,
        TtlPtsAdj2 = gameInfo.TtlPtsAdj2,
        MoneyLine1 = gameInfo.MoneyLine1,
        MoneyLine2 = gameInfo.MoneyLine2,
        MoneyLineDraw = gameInfo.MoneyLineDraw,
        PeriodDescription = gameInfo.PeriodDescription,
        Team1TotalPoints = gameInfo.Team1TotalPoints,
        Team2TotalPoints = gameInfo.Team2TotalPoints,
        Team1TtlPtsAdj1 = gameInfo.Team1TtlPtsAdj1,
        Team1TtlPtsAdj2 = gameInfo.Team1TtlPtsAdj2,
        Team2TtlPtsAdj1 = gameInfo.Team2TtlPtsAdj1,
        Team2TtlPtsAdj2 = gameInfo.Team2TtlPtsAdj2,
        ScheduleDate = gameInfo.ScheduleDate,
        BroadcastInfo = gameInfo.BroadcastInfo,
        ListedPitcher1 = gameInfo.ListedPitcher1,
        ListedPitcher2 = gameInfo.ListedPitcher2,
        ParlayRestriction = gameInfo.ParlayRestriction,
        PuckLine = gameInfo.PuckLine,
        LastSpreadChange = gameInfo.LastSpreadChange,
        LastMoneyLineChange = gameInfo.LastMoneyLineChange,
        LastTtlPtsChange = gameInfo.LastTtlPtsChange,
        LastTeamPtsChange = gameInfo.LastTeamPtsChange,
        PeriodWagerCutoff = gameInfo.PeriodWagerCutoff,
        ScheduleText = gameInfo.ScheduleText,
        CircledMaxWagerMoneyLine = gameInfo.CircledMaxWagerMoneyLine,
        CircledMaxWagerSpread = gameInfo.CircledMaxWagerSpread,
        CircledMaxWagerTeamTotal = gameInfo.CircledMaxWagerTeamTotal,
        CircledMaxWagerTotal = gameInfo.CircledMaxWagerTotal,
        TimeChangeFlag = gameInfo.TimeChangeFlag,
        CorrelationID = gameInfo.CorrelationID,
        PreventPointBuyingFlag = gameInfo.PreventPointBuyingFlag,
        AllowBuyOnPoints = gameInfo.AllowBuyOnPoints,
        AllowBuyOnSpread = gameInfo.AllowBuyOnSpread,
        CircledMaxWager = gameInfo.CircledMaxWager,
        TeamActionLinePos = gameInfo.TeamActionLinePos,
        GradeDateTime = gameInfo.GradeDateTime,
        KeepOpenFlag = gameInfo.KeepOpenFlag,
        LinkedToStoreFlag = gameInfo.LinkedToStoreFlag,
        MoneyLineDecimal1 = gameInfo.MoneyLineDecimal1,
        MoneyLineDecimal2 = gameInfo.MoneyLineDecimal2,
        MoneyLineDecimalDraw = gameInfo.MoneyLineDecimalDraw,
        MoneyLineDenominator1 = gameInfo.MoneyLineDenominator1,
        MoneyLineDenominator2 = gameInfo.MoneyLineDenominator2,
        MoneyLineDenominatorDraw = gameInfo.MoneyLineDenominatorDraw,
        MoneyLineNumerator1 = gameInfo.MoneyLineNumerator1,
        MoneyLineNumerator2 = gameInfo.MoneyLineNumerator2,
        MoneyLineNumeratorDraw = gameInfo.MoneyLineNumeratorDraw,
        OnlineFeedMoneyLine = gameInfo.OnlineFeedMoneyLine,
        OnlineFeedSpread = gameInfo.OnlineFeedSpread,
        OnlineFeedTotalPoints = gameInfo.OnlineFeedTotalPoints,
        Pitcher1StartedFlag = gameInfo.Pitcher1StartedFlag,
        Pitcher2StartedFlag = gameInfo.Pitcher2StartedFlag,
        ScheduleID = gameInfo.ScheduleID,
        SpreadDecimal1 = gameInfo.SpreadDecimal1,
        SpreadDecimal2 = gameInfo.SpreadDecimal2,
        SpreadDenominator1 = gameInfo.SpreadDenominator1,
        SpreadDenominator2 = gameInfo.SpreadDenominator2,
        SpreadNumerator1 = gameInfo.SpreadNumerator1,
        SpreadNumerator2 = gameInfo.SpreadNumerator2,
        Team1Nss = gameInfo.Team1Nss,
        Team1TtlPtsDecimal1 = gameInfo.Team1TtlPtsDecimal1,
        Team1TtlPtsDecimal2 = gameInfo.Team1TtlPtsDecimal2,
        Team1TtlPtsDenominator1 = gameInfo.Team1TtlPtsDenominator1,
        Team1TtlPtsDenominator2 = gameInfo.Team1TtlPtsDenominator2,
        Team1TtlPtsNumerator1 = gameInfo.Team1TtlPtsNumerator1,
        Team1TtlPtsNumerator2 = gameInfo.Team1TtlPtsNumerator2,
        Team2Nss = gameInfo.Team2Nss,
        Team2TtlPtsDecimal1 = gameInfo.Team2TtlPtsDecimal1,
        Team2TtlPtsDecimal2 = gameInfo.Team2TtlPtsDecimal2,
        Team2TtlPtsDenominator1 = gameInfo.Team2TtlPtsDenominator1,
        Team2TtlPtsDenominator2 = gameInfo.Team2TtlPtsDenominator2,
        Team2TtlPtsNumerator1 = gameInfo.Team2TtlPtsNumerator1,
        Team2TtlPtsNumerator2 = gameInfo.Team2TtlPtsNumerator2,
        TtlPointsDecimal1 = gameInfo.TtlPointsDecimal1,
        TtlPointsDecimal2 = gameInfo.TtlPointsDecimal2,
        TtlPointsDenominator1 = gameInfo.TtlPointsDenominator1,
        TtlPointsDenominator2 = gameInfo.TtlPointsDenominator2,
        TtlPointsNumerator1 = gameInfo.TtlPointsNumerator1,
        TtlPointsNumerator2 = gameInfo.TtlPointsNumerator2,
        AltScorePeriodNumber1 = gameInfo.AltScorePeriodNumber1,
        AltScorePeriodNumber2 = gameInfo.AltScorePeriodNumber2,
        PreReqPeriodNumber = gameInfo.PreReqPeriodNumber,
        ShortDescription = gameInfo.ShortDescription
      };

      return retObj;
    }

    internal static void FillIfBetItem(IfBetItem item, Ticket.WagerItem wagerItem, string wagerTypeMode, /*int selectedGvRow,*/ string totalsOorU, string selectedOdds, string selectedPrice, string selectedBPointsOption, TextBox txtRisk, TextBox txtToWin, string itemShortDescription, string wageringMode, string layoffFlag, string fixedPrice, string pitcher1IsReq, string pitcher2IsReq) {
      item.WagerTypeMode = wagerTypeMode;
      item.FinalMoney = wagerItem.FinalMoney;
      item.Number = wagerItem.ItemNumber;
      item.PriceType = wagerItem.PriceType;
      item.SportSubType = wagerItem.SportSubType;
      item.SportType = wagerItem.SportType;
      item.Type = WagerType.GetFromCode(wagerItem.WagerType);
      item.TicketNumber = wagerItem.TicketNumber;
      item.WagerNumber = wagerItem.WagerNumber;
      /*item.SelectedRow = selectedGvRow;*/
      item.SelectedCell = DetermineSelectedCell(item.Type, totalsOorU);
      item.SelectedOdds = selectedOdds;
      item.SelectedAmericanPrice = int.Parse(selectedPrice);
      item.SelectedBPointsOption = selectedBPointsOption;
      item.SelectedAmountWagered = txtRisk.Text;
      item.SelectedToWinAmount = txtToWin.Text;
      item.Description = itemShortDescription;
      item.WagerTypeMode = wagerTypeMode;
      item.WageringMode = wageringMode;
      item.GameNumber = wagerItem.GameNum ?? 0;
      item.PeriodNumber = wagerItem.PeriodNumber ?? 0;
      item.RowGroupNum = wagerItem.RowGroupNum;
      item.PeriodWagerCutoff = wagerItem.PeriodWagerCutoff;
      item.LayoffWager = layoffFlag;
      item.FixedPrice = fixedPrice;
      item.Pitcher1MStart = pitcher1IsReq;
      item.Pitcher2MStart = pitcher2IsReq;
      item.TotalPointsOu = totalsOorU;
      item.PeriodDescription = wagerItem.PeriodDescription;
      item.PendingPlayItemInDb = wagerItem.PendingPlayItemInDb;
    }

    internal static string GetContestThresholdValue(spCnGetActiveContests_Result res) {
      var thresholdValue = "";

      if (res.ThresholdLine == null || res.Status == "H") return thresholdValue;
      if (res.ThresholdUnits != null)
        thresholdValue = LinesFormat.FormatStringPoints(res.ThresholdLine, res.ThresholdType) + " " + res.ThresholdUnits.Trim();
      else
        thresholdValue = LinesFormat.FormatStringPoints(res.ThresholdLine, res.ThresholdType);
      if (res.ThresholdType == "P")
        thresholdValue = thresholdValue.Replace("+", "").Replace("-", "");
      return thresholdValue;
    }

    internal static ReadbackItem.ContestItemEditInfo AddWagerContestItemEditInfo(int contestNum, int contestantNum, String contestantDescription, String selectedOdds, String selectedThresholdLine, String selectedAmountWagered, String selectedToWinAmount, String priceType, String wageringMode, Boolean layoffFlag, String xToYRep, String toBase) {
      var cWiEditInfo = new ReadbackItem.ContestItemEditInfo {
        ContestNum = contestNum,
        ContestantNum = contestantNum,
        ContestantDescription = contestantDescription,
        SelectedOdds = selectedOdds,
        SelectedThresholdLine = selectedThresholdLine,
        SelectedAmountWagered = selectedAmountWagered,
        SelectedToWinAmount = selectedToWinAmount,
        PriceType = priceType,
        WageringMode = wageringMode,
        LayoffWager = layoffFlag,
        XtoYRep = xToYRep,
        ToBase = toBase
      };

      return cWiEditInfo;
    }

    internal static ReadbackItem.WagerItemEditInfo AddWagerItemEditInfo(/*int selectedRow,*/ int selectedCell, String selectedOdds, int selectedAmericanPrice, String selectedBPointsOption,
                                                                String selectedAmountWagered, String selectedToWinAmount, String shortDescription, String wagerTypeMode,
                                                                    String priceType, String wageringMode, int gamePeriod, int rowGroupNum, DateTime periodWagerCutoff, String layoffWager, String oddsAreAdjustable, String pitcher1MStart, String pitcher2MStart, String totalsOu) {
      var wiEditInfo = new ReadbackItem.WagerItemEditInfo {
        /*SelectedRow = selectedRow,*/
        SelectedCell = selectedCell,
        SelectedOdds = selectedOdds,
        SelectedAmericanPrice = selectedAmericanPrice,
        SelectedBPointsOption = selectedBPointsOption,
        SelectedAmountWagered = selectedAmountWagered,
        SelectedToWinAmount = selectedToWinAmount,
        Description = shortDescription,
        WagerTypeMode = wagerTypeMode,
        PriceType = priceType,
        WageringMode = wageringMode,
        PeriodNumber = gamePeriod,
        RowGroupNum = rowGroupNum,
        PeriodWagerCutoff = periodWagerCutoff,
        LayoffWager = layoffWager,
        Pitcher1MStart = pitcher1MStart,
        Pitcher2MStart = pitcher2MStart,
        TotalsOuFlag = totalsOu,
        FixedPrice = oddsAreAdjustable == "N" ? "Y" : "N"
      };

      return wiEditInfo;
    }

    internal static ReadbackItem.ManualWagerItemEditInfo AddWagerItemEditInfo(String type, String description, DateTime gradeDateTime, String odds, String selectedAmountWagered, String selectedToWinAmount, Boolean layoffWager) {
      var mWiEditInfo = new ReadbackItem.ManualWagerItemEditInfo {
        Type = type,
        Description = description,
        GradeDateTime = gradeDateTime,
        Odds = odds,
        SelectedAmountWagered = selectedAmountWagered,
        SelectedToWinAmount = selectedToWinAmount,
        LayoffWager = layoffWager
      };


      return mWiEditInfo;
    }

    internal static int DetermineSelectedCell(WagerType wagerType, String totalsOu) {
      if (wagerType.Code == WagerType.TEAMTOTALPOINTS) {
        return totalsOu == "O" ? 6 : 7;
      }
      var selectedCell = Enums.SelectedWagerTypePosition(wagerType.Code);

      return selectedCell;
    }

    internal static string GetCurrencyCode(string currentCustomerCurrency) {
      return currentCustomerCurrency != null ? currentCustomerCurrency.Split(' ')[0] : "";
    }

    internal static string GetKeyDownChar(KeyEventArgs e) {
      if (e.KeyCode < Keys.NumPad0 || e.KeyCode > Keys.NumPad9)
        return ((char)e.KeyValue).ToString(CultureInfo.InvariantCulture);
      switch (e.KeyCode) {
        case Keys.NumPad0: return "0";
        case Keys.NumPad1: return "1";
        case Keys.NumPad2: return "2";
        case Keys.NumPad3: return "3";
        case Keys.NumPad4: return "4";
        case Keys.NumPad5: return "5";
        case Keys.NumPad6: return "6";
        case Keys.NumPad7: return "7";
        case Keys.NumPad8: return "8";
        default: return "9";
      }
    }

    internal static string GetPriceTypeName(String priceType) {
      var priceName = "";

      switch (priceType) {
        case LineOffering.PRICE_AMERICAN:
          priceName = "American";
          break;
        case LineOffering.PRICE_DECIMAL:
          priceName = "Decimal";
          break;
        case LineOffering.PRICE_FRACTIONAL:
          priceName = "Fractional";
          break;
      }

      return priceName;
    }

    internal static double? GetContestCircleLimit(spCnGetActiveContests_Result gameInfo) {
      var cLimit = gameInfo.CircledMaxWager;

      return cLimit;
    }

    internal static bool IsAsianLine(String itemWagerType, String chosenTeamId, spGLGetActiveGamesByCustomer_Result currentGameAndPeriod) {

      switch (itemWagerType) {
        case WagerType.SPREAD:
          return currentGameAndPeriod.Spread.ToString().Contains(".25") || currentGameAndPeriod.Spread.ToString().Contains(".75");
        case WagerType.MONEYLINE:
          return false;
        case WagerType.TOTALPOINTS:
          return currentGameAndPeriod.TotalPoints.ToString().Contains(".25") || currentGameAndPeriod.TotalPoints.ToString().Contains(".75");
        case WagerType.TEAMTOTALPOINTS:
          if (chosenTeamId.Trim() == currentGameAndPeriod.Team1ID.Trim()) {
            return currentGameAndPeriod.Team1TotalPoints.ToString().Contains(".25") || currentGameAndPeriod.Team1TotalPoints.ToString().Contains(".75");
          }
          return currentGameAndPeriod.Team2TotalPoints.ToString().Contains(".25") || currentGameAndPeriod.Team2TotalPoints.ToString().Contains(".75");
      }

      return false;
    }

    internal static bool IsAsianLine(int columnIndex, int rowGroupNum, spGLGetActiveGamesByCustomer_Result currentGameAndPeriod) {
      if (columnIndex == 4)
        return false;

      switch (columnIndex) {
        case 3:
          return currentGameAndPeriod.Spread.ToString().Contains(".25") || currentGameAndPeriod.Spread.ToString().Contains(".75");
        case 4:
          return false;
        case 5:
          return currentGameAndPeriod.TotalPoints.ToString().Contains(".25") || currentGameAndPeriod.TotalPoints.ToString().Contains(".75");
        case 6:
        case 7:
          switch (rowGroupNum) {
            case 1:
              return currentGameAndPeriod.Team1TotalPoints.ToString().Contains(".25") || currentGameAndPeriod.Team1TotalPoints.ToString().Contains(".75");
            case 2:
              return currentGameAndPeriod.Team2TotalPoints.ToString().Contains(".25") || currentGameAndPeriod.Team2TotalPoints.ToString().Contains(".75");
          }
          break;
      }

      return false;
    }

    internal static string RebuildTeaserReadbackDescription(string itemDescription, string wagerType, string periodDescription, string teaserPoints) {
      var idx = itemDescription.IndexOf(periodDescription, StringComparison.Ordinal);

      var retDescription = itemDescription.Substring(0, idx);

      retDescription = retDescription.Replace(" for", "");
      retDescription = retDescription + "(taking " + teaserPoints + " pts)";

      switch (wagerType) {
        case WagerType.SPREAD:
        case WagerType.MONEYLINE:
          break;
        case WagerType.TOTALPOINTS:
        case WagerType.TEAMTOTALPOINTS:
          retDescription = retDescription.IndexOf(" over ", StringComparison.Ordinal) > -1 ? retDescription.Replace(" over ", " " + periodDescription + " " + WagerType.GetFromCode(wagerType).Name + " over ") : retDescription.Replace(" under ", " " + periodDescription + " " + WagerType.GetFromCode(wagerType).Name + " under ");
          break;
      }

      return retDescription;
    }

    internal static void WriteDgvwPropsAndContestsHeader(DataGridView caller) {

      var dgrvwColNames = (from DataGridViewTextBoxColumn col in caller.Columns select col.Name).ToList();

      foreach (var str in dgrvwColNames) {
        caller.Columns.Remove(str);
      }

      var titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = @"Contest",
        Width = 350
      };
      titleColumn.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = @"#."
      };
      var style = new DataGridViewCellStyle { Alignment = DataGridViewContentAlignment.MiddleLeft };
      titleColumn.DefaultCellStyle = style;
      titleColumn.Width = 50;
      titleColumn.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = @"Contestant",
        Width = 150,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleLeft }
      };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = @"Odds",
        Width = 100,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleLeft }
      };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = @"ContestanNum",
        Width = 50,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleLeft },
        Visible = false
      };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = @"ContestDateTime",
        Width = 50,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleLeft },
        Visible = false
      };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = @"HiddenContestDesc",
        Width = 50,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleLeft },
        Visible = false
      };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = @"ContestComments",
        Width = 50,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleLeft },
        Visible = false
      };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = @"ContestNum",
        Width = 50,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleLeft },
        Visible = false
      };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        Name = "CustProfile",
        HeaderText = @"Cust Profile",
        Width = 20,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleLeft },
        Visible = false
      };
      caller.Columns.Add(titleColumn);
    }

    internal static int GetChosenTeamIdIdx(spGLGetActiveGamesByCustomer_Result gameInfo, string chosenTeamId) {
      var teamIdx = 0;

      if (gameInfo == null) return teamIdx;
      var team1Id = "";
      if (gameInfo.Team1ID != null) {
        team1Id = gameInfo.Team1ID.Trim();
      }

      var team2Id = "";
      if (gameInfo.Team1ID != null) {
        team2Id = gameInfo.Team2ID.Trim();
      }

      if (chosenTeamId == null) return teamIdx;
      if (chosenTeamId.Trim() == team1Id) {
        teamIdx = 1;
      }
      else {
        teamIdx = chosenTeamId.Trim() == team2Id ? 2 : 3;
      }

      return teamIdx;
    }

    internal static double GetOriginalTotalPoints(spGLGetActiveGamesByCustomer_Result gameInfo, String wagerType, String chosenTeamId) {
      double totalPnts = 0;
      var chosenTeamIdx = GetChosenTeamIdIdx(gameInfo, chosenTeamId);
      switch (wagerType) {
        case WagerType.TOTALPOINTS:
          if (gameInfo.TotalPoints == null)
            totalPnts = 0;
          else
            totalPnts = (double)gameInfo.TotalPoints;
          break;
        case WagerType.TEAMTOTALPOINTS:
          switch (chosenTeamIdx) {
            case 1:
              if (gameInfo.Team1TotalPoints == null)
                totalPnts = 0;
              else
                totalPnts = (double)gameInfo.Team1TotalPoints;
              break;
            case 2:
              if (gameInfo.Team2TotalPoints == null)
                totalPnts = 0;
              else
                totalPnts = (double)gameInfo.Team2TotalPoints;
              break;
          }
          break;
      }
      return totalPnts;
    }

    internal static bool ElementExistsInList(IEnumerable<RadioButton> enabledRadList, int newTabIndex) {
      return enabledRadList.Any(rad => rad.TabIndex == newTabIndex);
    }

    internal static spGLGetActiveGamesByCustomer_Result ConvertToNewGameObject(spGLGetActiveGamesByCustomer_Result gameInfo, String currentWagerTypeName, bool vigDiscountForAllWagerTypes) {
      if (gameInfo.PreviousToApplyVigDiscountAdjsObj == null || currentWagerTypeName == "Straight Bet" || vigDiscountForAllWagerTypes) {
        return gameInfo;
      }

      var retObj = new spGLGetActiveGamesByCustomer_Result {
        CustomerID = gameInfo.CustomerID,
        Store = gameInfo.Store,
        CustProfile = gameInfo.CustProfile,
        GameNum = gameInfo.GameNum,
        GameDateTime = gameInfo.GameDateTime,
        WagerCutoff = gameInfo.WagerCutoff,
        Status = gameInfo.Status,
        SportType = gameInfo.SportType,
        Team1ID = gameInfo.Team1ID,
        Team1RotNum = gameInfo.Team1RotNum,
        Team2ID = gameInfo.Team2ID,
        Team2RotNum = gameInfo.Team2RotNum,
        DrawRotNum = gameInfo.DrawRotNum,
        Comments = gameInfo.Comments,
        SportSubType = gameInfo.SportSubType,
        OnTV = gameInfo.OnTV,
        Team1FinalScore = gameInfo.Team1FinalScore,
        Team2FinalScore = gameInfo.Team2FinalScore,
        FinalWinnerID = gameInfo.FinalWinnerID,
        PeriodNumber = gameInfo.PeriodNumber,
        LineSeq = gameInfo.LineSeq,
        FavoredTeamID = gameInfo.FavoredTeamID,
        Spread = gameInfo.Spread,
        SpreadAdj1 = gameInfo.SpreadAdj1,
        SpreadAdj2 = gameInfo.SpreadAdj2,
        TotalPoints = gameInfo.TotalPoints,
        TtlPtsAdj1 = gameInfo.TtlPtsAdj1,
        TtlPtsAdj2 = gameInfo.TtlPtsAdj2,
        MoneyLine1 = gameInfo.MoneyLine1,
        MoneyLine2 = gameInfo.MoneyLine2,
        MoneyLineDraw = gameInfo.MoneyLineDraw,
        PeriodDescription = gameInfo.PeriodDescription,
        Team1TotalPoints = gameInfo.Team1TotalPoints,
        Team2TotalPoints = gameInfo.Team2TotalPoints,
        Team1TtlPtsAdj1 = gameInfo.Team1TtlPtsAdj1,
        Team1TtlPtsAdj2 = gameInfo.Team1TtlPtsAdj2,
        Team2TtlPtsAdj1 = gameInfo.Team2TtlPtsAdj1,
        Team2TtlPtsAdj2 = gameInfo.Team2TtlPtsAdj2,
        ScheduleDate = gameInfo.ScheduleDate,
        BroadcastInfo = gameInfo.BroadcastInfo,
        ListedPitcher1 = gameInfo.ListedPitcher1,
        ListedPitcher2 = gameInfo.ListedPitcher2,
        ParlayRestriction = gameInfo.ParlayRestriction,
        PuckLine = gameInfo.PuckLine,
        LastSpreadChange = gameInfo.LastSpreadChange,
        LastMoneyLineChange = gameInfo.LastMoneyLineChange,
        LastTtlPtsChange = gameInfo.LastTtlPtsChange,
        LastTeamPtsChange = gameInfo.LastTeamPtsChange,
        PeriodWagerCutoff = gameInfo.PeriodWagerCutoff,
        ScheduleText = gameInfo.ScheduleText,
        CircledMaxWagerMoneyLine = gameInfo.CircledMaxWagerMoneyLine,
        CircledMaxWagerSpread = gameInfo.CircledMaxWagerSpread,
        CircledMaxWagerTeamTotal = gameInfo.CircledMaxWagerTeamTotal,
        CircledMaxWagerTotal = gameInfo.CircledMaxWagerTotal,
        TimeChangeFlag = gameInfo.TimeChangeFlag,
        CorrelationID = gameInfo.CorrelationID,
        PreventPointBuyingFlag = gameInfo.PreventPointBuyingFlag,
        AllowBuyOnPoints = gameInfo.AllowBuyOnPoints,
        AllowBuyOnSpread = gameInfo.AllowBuyOnSpread,
        CircledMaxWager = gameInfo.CircledMaxWager,
        TeamActionLinePos = gameInfo.TeamActionLinePos,
        GradeDateTime = gameInfo.GradeDateTime,
        KeepOpenFlag = gameInfo.KeepOpenFlag,
        LinkedToStoreFlag = gameInfo.LinkedToStoreFlag,
        MoneyLineDecimal1 = gameInfo.MoneyLineDecimal1,
        MoneyLineDecimal2 = gameInfo.MoneyLineDecimal2,
        MoneyLineDecimalDraw = gameInfo.MoneyLineDecimalDraw,
        MoneyLineDenominator1 = gameInfo.MoneyLineDenominator1,
        MoneyLineDenominator2 = gameInfo.MoneyLineDenominator2,
        MoneyLineDenominatorDraw = gameInfo.MoneyLineDenominatorDraw,
        MoneyLineNumerator1 = gameInfo.MoneyLineNumerator1,
        MoneyLineNumerator2 = gameInfo.MoneyLineNumerator2,
        MoneyLineNumeratorDraw = gameInfo.MoneyLineNumeratorDraw,
        OnlineFeedMoneyLine = gameInfo.OnlineFeedMoneyLine,
        OnlineFeedSpread = gameInfo.OnlineFeedSpread,
        OnlineFeedTotalPoints = gameInfo.OnlineFeedTotalPoints,
        Pitcher1StartedFlag = gameInfo.Pitcher1StartedFlag,
        Pitcher2StartedFlag = gameInfo.Pitcher2StartedFlag,
        ScheduleID = gameInfo.ScheduleID,
        SpreadDecimal1 = gameInfo.SpreadDecimal1,
        SpreadDecimal2 = gameInfo.SpreadDecimal2,
        SpreadDenominator1 = gameInfo.SpreadDenominator1,
        SpreadDenominator2 = gameInfo.SpreadDenominator2,
        SpreadNumerator1 = gameInfo.SpreadNumerator1,
        SpreadNumerator2 = gameInfo.SpreadNumerator2,
        Team1Nss = gameInfo.Team1Nss,
        Team1TtlPtsDecimal1 = gameInfo.Team1TtlPtsDecimal1,
        Team1TtlPtsDecimal2 = gameInfo.Team1TtlPtsDecimal2,
        Team1TtlPtsDenominator1 = gameInfo.Team1TtlPtsDenominator1,
        Team1TtlPtsDenominator2 = gameInfo.Team1TtlPtsDenominator2,
        Team1TtlPtsNumerator1 = gameInfo.Team1TtlPtsNumerator1,
        Team1TtlPtsNumerator2 = gameInfo.Team1TtlPtsNumerator2,
        Team2Nss = gameInfo.Team2Nss,
        Team2TtlPtsDecimal1 = gameInfo.Team2TtlPtsDecimal1,
        Team2TtlPtsDecimal2 = gameInfo.Team2TtlPtsDecimal2,
        Team2TtlPtsDenominator1 = gameInfo.Team2TtlPtsDenominator1,
        Team2TtlPtsDenominator2 = gameInfo.Team2TtlPtsDenominator2,
        Team2TtlPtsNumerator1 = gameInfo.Team2TtlPtsNumerator1,
        Team2TtlPtsNumerator2 = gameInfo.Team2TtlPtsNumerator2,
        TtlPointsDecimal1 = gameInfo.TtlPointsDecimal1,
        TtlPointsDecimal2 = gameInfo.TtlPointsDecimal2,
        TtlPointsDenominator1 = gameInfo.TtlPointsDenominator1,
        TtlPointsDenominator2 = gameInfo.TtlPointsDenominator2,
        TtlPointsNumerator1 = gameInfo.TtlPointsNumerator1,
        TtlPointsNumerator2 = gameInfo.TtlPointsNumerator2,
        AltScorePeriodNumber1 = gameInfo.AltScorePeriodNumber1,
        AltScorePeriodNumber2 = gameInfo.AltScorePeriodNumber2,
        PreReqPeriodNumber = gameInfo.PreReqPeriodNumber,
        ShortDescription = gameInfo.ShortDescription
      };

      if (gameInfo.PreviousToApplyVigDiscountAdjsObj != null /*&& (currentWagerTypeName != "Straight Bet" && !vigDiscountForAllWagerTypes)*/) {
        retObj.SpreadAdj1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.SpreadAdj1;
        retObj.SpreadDecimal1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.SpreadDecimal1;
        retObj.SpreadNumerator1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.SpreadNumerator1;
        retObj.SpreadDenominator1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.SpreadDenominator1;

        retObj.SpreadAdj2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.SpreadAdj2;
        retObj.SpreadDecimal2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.SpreadDecimal2;
        retObj.SpreadNumerator2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.SpreadNumerator2;
        retObj.SpreadDenominator2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.SpreadDenominator2;

        retObj.MoneyLine1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLine1;
        retObj.MoneyLineDecimal1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLineDecimal1;
        retObj.MoneyLineNumerator1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLineNumerator1;
        retObj.MoneyLineDenominator1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLineDenominator1;

        retObj.MoneyLine2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLine2;
        retObj.MoneyLineDecimal2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLineDecimal2;
        retObj.MoneyLineNumerator2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLineNumerator2;
        retObj.MoneyLineDenominator2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLineDenominator2;

        retObj.MoneyLineDraw = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLineDraw;
        retObj.MoneyLineDecimalDraw = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLineDecimalDraw;
        retObj.MoneyLineNumeratorDraw = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLineNumeratorDraw;
        retObj.MoneyLineDenominatorDraw = gameInfo.PreviousToApplyVigDiscountAdjsObj.MoneyLineDenominatorDraw;

        retObj.TtlPtsAdj1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.TtlPtsAdj1;
        retObj.TtlPointsDecimal1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.TtlPointsDecimal1;
        retObj.TtlPointsNumerator1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.TtlPointsNumerator1;
        retObj.TtlPointsDenominator1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.TtlPointsDenominator1;

        retObj.TtlPtsAdj2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.TtlPtsAdj2;
        retObj.TtlPointsDecimal2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.TtlPointsDecimal2;
        retObj.TtlPointsNumerator2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.TtlPointsNumerator2;
        retObj.TtlPointsDenominator2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.TtlPointsDenominator2;

        retObj.Team1TtlPtsAdj1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team1TtlPtsAdj1;
        retObj.Team1TtlPtsDecimal1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team1TtlPtsDecimal1;
        retObj.Team1TtlPtsNumerator1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team1TtlPtsNumerator1;
        retObj.Team1TtlPtsDenominator1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team1TtlPtsDenominator1;

        retObj.Team1TtlPtsAdj2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team1TtlPtsAdj2;
        retObj.Team1TtlPtsDecimal2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team1TtlPtsDecimal2;
        retObj.Team1TtlPtsNumerator2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team1TtlPtsNumerator2;
        retObj.Team1TtlPtsDenominator2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team1TtlPtsDenominator2;

        retObj.Team2TtlPtsAdj1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team2TtlPtsAdj1;
        retObj.Team2TtlPtsDecimal1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team2TtlPtsDecimal1;
        retObj.Team2TtlPtsNumerator1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team2TtlPtsNumerator1;
        retObj.Team2TtlPtsDenominator1 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team2TtlPtsDenominator1;

        retObj.Team2TtlPtsAdj2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team2TtlPtsAdj2;
        retObj.Team2TtlPtsDecimal2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team2TtlPtsDecimal2;
        retObj.Team2TtlPtsNumerator2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team2TtlPtsNumerator2;
        retObj.Team2TtlPtsDenominator2 = gameInfo.PreviousToApplyVigDiscountAdjsObj.Team2TtlPtsDenominator2;
      }
      return retObj;
    }

    internal static double? GetPeriodGameCircleLimit(string wagerType, spGLGetActiveGamesByCustomer_Result gameInfo, out String circleLimitType) {
      double? cLimit = null;
      circleLimitType = null;
      if (gameInfo.Status == GamesAndLines.EVENT_CIRCLED) {
        switch (wagerType) {
          case WagerType.SPREAD:
            if (gameInfo.CircledMaxWagerSpread != null) {
              cLimit = gameInfo.CircledMaxWagerSpread;
              circleLimitType = gameInfo.CircledMaxWagerSpreadType;
            }
            break;
          case WagerType.MONEYLINE:
            if (gameInfo.CircledMaxWagerMoneyLine != null) {
              cLimit = gameInfo.CircledMaxWagerMoneyLine;
              circleLimitType = gameInfo.CircledMaxWagerMoneyLineType;
            }
            break;
          case WagerType.TOTALPOINTS:
            if (gameInfo.CircledMaxWagerTotal != null) {
              cLimit = gameInfo.CircledMaxWagerTotal;
              circleLimitType = gameInfo.CircledMaxWagerTotalType;
            }
            break;
          case WagerType.TEAMTOTALPOINTS:
            if (gameInfo.CircledMaxWagerTeamTotal != null) {
              cLimit = gameInfo.CircledMaxWagerTeamTotal;
              circleLimitType = gameInfo.CircledMaxWagerTeamTotalType;
            }
            break;
        }
      }

      return cLimit;
    }

    internal static bool IsTitle(spGLGetActiveGamesByCustomer_Result game) {
      return (game != null && (game.IsGameTitle || (game.Team1ID.StartsWith("*") && game.Team2ID.StartsWith("*"))));
    }

    internal static String FormatSpreadTotal(double movingSpreadTotal, String wagerType) {
      String retVal;

      if (movingSpreadTotal >= 0 && wagerType == WagerType.SPREAD) {
        retVal = "+" + movingSpreadTotal.ToString(CultureInfo.InvariantCulture);
      }
      else {
        retVal = movingSpreadTotal.ToString(CultureInfo.InvariantCulture);
      }

      retVal = retVal.Replace(".5", GamesAndLines.HALFSYMBOL);
      if (retVal == ("+0" + GamesAndLines.HALFSYMBOL) || retVal == ("-0" + GamesAndLines.HALFSYMBOL))
          retVal = retVal == ("+0" + GamesAndLines.HALFSYMBOL) ? ("+" + GamesAndLines.HALFSYMBOL) : ("-" + GamesAndLines.HALFSYMBOL);

      return retVal;
    }

    internal static string GetChosenTeamId(int rowNumberInGroup, spGLGetActiveGamesByCustomer_Result selectedGamePeriodInfo) {
      var chosenTeamId = "";
      if (selectedGamePeriodInfo == null) return chosenTeamId;
      switch (rowNumberInGroup) {
        case 1:
          chosenTeamId = selectedGamePeriodInfo.Team1ID;
          break;
        case 2:
          chosenTeamId = selectedGamePeriodInfo.Team2ID;
          break;
        case 3:
          chosenTeamId = "Draw";
          break;
      }

      return chosenTeamId;
    }

    internal static string GetSelectedWagerOption(String wagerType, int rowNumberInGroup, out String totalsOu) {
      var wagerTypeCode = "";
      totalsOu = "";

      switch (wagerType) {
        case "Spread":
          wagerTypeCode = WagerType.SPREAD;
          break;
        case "MoneyLine":
          wagerTypeCode = WagerType.MONEYLINE;
          break;
        case "Totals":
          wagerTypeCode = WagerType.TOTALPOINTS;
          switch (rowNumberInGroup) {
            case 1:
              totalsOu = "O";
              break;
            case 2:
              totalsOu = "U";
              break;
          }
          break;
        case "TeamTotalsOver":
          wagerTypeCode = WagerType.TEAMTOTALPOINTS;
          totalsOu = "O";
          break;
        case "TeamTotalsUnder":
          wagerTypeCode = WagerType.TEAMTOTALPOINTS;
          totalsOu = "U";
          break;
      }

      return wagerTypeCode;
    }

  }
}