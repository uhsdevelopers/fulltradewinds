﻿namespace TicketWriter.UI
{
    partial class FrmSportAndGameSelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSportAndGameSelection));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.treSports = new System.Windows.Forms.TreeView();
            this.ilSportsTree = new System.Windows.Forms.ImageList(this.components);
            this.panGames = new System.Windows.Forms.Panel();
            this.dgvwGame = new System.Windows.Forms.DataGridView();
            this.dgvwPropsAndContests = new System.Windows.Forms.DataGridView();
            this.bndsrcGame = new System.Windows.Forms.BindingSource(this.components);
            this.panGamesInfo = new System.Windows.Forms.Panel();
            this.panGamePeriodDates = new System.Windows.Forms.Panel();
            this.lblGameNumber = new System.Windows.Forms.Label();
            this.lblTimeChanged = new System.Windows.Forms.Label();
            this.lblGameStatus = new System.Windows.Forms.Label();
            this.lblPeriodWagerCutoff = new System.Windows.Forms.Label();
            this.lblGameDateTime = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblWagerCutOff = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.panLastLineChangeDate = new System.Windows.Forms.Panel();
            this.lblLastSpread = new System.Windows.Forms.Label();
            this.lblTotalPoints = new System.Windows.Forms.Label();
            this.lblLastSpreadChange = new System.Windows.Forms.Label();
            this.lblLastTeamTotals = new System.Windows.Forms.Label();
            this.lblLastMoneyLineChange = new System.Windows.Forms.Label();
            this.lblLastMoney = new System.Windows.Forms.Label();
            this.lblLastTotalPntsChange = new System.Windows.Forms.Label();
            this.lblLastTeamTotalsChange = new System.Windows.Forms.Label();
            this.chbActiveOnly = new System.Windows.Forms.CheckBox();
            this.panDecimalToFract = new System.Windows.Forms.Panel();
            this.btnConvertToFrac = new System.Windows.Forms.Button();
            this.lblFracValue = new System.Windows.Forms.Label();
            this.lblProcessDuration = new System.Windows.Forms.Label();
            this.txtDecimalOdds = new System.Windows.Forms.TextBox();
            this.lblDuration = new System.Windows.Forms.Label();
            this.lblFractionalOdds = new System.Windows.Forms.Label();
            this.btnLineDisplayToggle = new System.Windows.Forms.Button();
            this.txtTeam1ID = new System.Windows.Forms.TextBox();
            this.btnUpdateLines = new System.Windows.Forms.Button();
            this.txtTeam2ID = new System.Windows.Forms.TextBox();
            this.lblCurrentlyShowing = new System.Windows.Forms.Label();
            this.txtGameDateTime = new System.Windows.Forms.TextBox();
            this.panGamePeriodSelection = new System.Windows.Forms.Panel();
            this.radAltRL_3 = new System.Windows.Forms.RadioButton();
            this.radThirdPeriod_3 = new System.Windows.Forms.RadioButton();
            this.radSecondPeriod_2 = new System.Windows.Forms.RadioButton();
            this.radFirstPeriod_1 = new System.Windows.Forms.RadioButton();
            this.radFourthQuarter_6 = new System.Windows.Forms.RadioButton();
            this.radThirdQuarter_5 = new System.Windows.Forms.RadioButton();
            this.radSecondQuarter_4 = new System.Windows.Forms.RadioButton();
            this.radFirstQuarter_3 = new System.Windows.Forms.RadioButton();
            this.radSecondHalf_2 = new System.Windows.Forms.RadioButton();
            this.radFirstHalf_1 = new System.Windows.Forms.RadioButton();
            this.radGame_0 = new System.Windows.Forms.RadioButton();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.lblShiftIndication = new System.Windows.Forms.Label();
            this.txtTeam1Score = new System.Windows.Forms.TextBox();
            this.txtTeam2Score = new System.Windows.Forms.TextBox();
            this.txtGameInfo = new System.Windows.Forms.TextBox();
            this.PeriodWagerCutoff = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chbFreePlay = new System.Windows.Forms.CheckBox();
            this.panLeft = new System.Windows.Forms.Panel();
            this.panRight = new System.Windows.Forms.Panel();
            this.spl = new System.Windows.Forms.Splitter();
            this.bgwrkUpdateLines = new System.ComponentModel.BackgroundWorker();
            this.panGames.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvwGame)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvwPropsAndContests)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bndsrcGame)).BeginInit();
            this.panGamesInfo.SuspendLayout();
            this.panGamePeriodDates.SuspendLayout();
            this.panLastLineChangeDate.SuspendLayout();
            this.panDecimalToFract.SuspendLayout();
            this.panGamePeriodSelection.SuspendLayout();
            this.panLeft.SuspendLayout();
            this.panRight.SuspendLayout();
            this.SuspendLayout();
            // 
            // treSports
            // 
            this.treSports.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treSports.ImageIndex = 0;
            this.treSports.ImageList = this.ilSportsTree;
            this.treSports.Location = new System.Drawing.Point(0, 0);
            this.treSports.Name = "treSports";
            this.treSports.SelectedImageIndex = 0;
            this.treSports.Size = new System.Drawing.Size(180, 683);
            this.treSports.TabIndex = 0;
            this.treSports.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler(this.treSports_BeforeExpand);
            this.treSports.BeforeSelect += new System.Windows.Forms.TreeViewCancelEventHandler(this.treSports_BeforeSelect);
            this.treSports.Click += new System.EventHandler(this.treSports_Click);
            this.treSports.KeyDown += new System.Windows.Forms.KeyEventHandler(this.treSports_KeyDown);
            this.treSports.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.treSports_KeyPress);
            this.treSports.KeyUp += new System.Windows.Forms.KeyEventHandler(this.treSports_KeyUp);
            this.treSports.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.treSports_PreviewKeyDown);
            // 
            // ilSportsTree
            // 
            this.ilSportsTree.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilSportsTree.ImageStream")));
            this.ilSportsTree.TransparentColor = System.Drawing.Color.Transparent;
            this.ilSportsTree.Images.SetKeyName(0, "closedFolder");
            this.ilSportsTree.Images.SetKeyName(1, "openFolder");
            this.ilSportsTree.Images.SetKeyName(2, "soccer.png");
            this.ilSportsTree.Images.SetKeyName(3, "icon_basketball_small.png");
            this.ilSportsTree.Images.SetKeyName(4, "icon_football_small.png");
            this.ilSportsTree.Images.SetKeyName(5, "field-hockey-logo.jpg");
            this.ilSportsTree.Images.SetKeyName(6, "baseball-icon.jpg");
            this.ilSportsTree.Images.SetKeyName(7, "1st.png");
            this.ilSportsTree.Images.SetKeyName(8, "runnig.gif");
            // 
            // panGames
            // 
            this.panGames.AutoScroll = true;
            this.panGames.Controls.Add(this.dgvwGame);
            this.panGames.Controls.Add(this.dgvwPropsAndContests);
            this.panGames.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panGames.Location = new System.Drawing.Point(0, 113);
            this.panGames.Name = "panGames";
            this.panGames.Size = new System.Drawing.Size(1272, 570);
            this.panGames.TabIndex = 25;
            // 
            // dgvwGame
            // 
            this.dgvwGame.AllowUserToAddRows = false;
            this.dgvwGame.AllowUserToDeleteRows = false;
            this.dgvwGame.AllowUserToResizeColumns = false;
            this.dgvwGame.AllowUserToResizeRows = false;
            this.dgvwGame.BackgroundColor = System.Drawing.Color.White;
            this.dgvwGame.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvwGame.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvwGame.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvwGame.GridColor = System.Drawing.Color.Black;
            this.dgvwGame.Location = new System.Drawing.Point(0, 0);
            this.dgvwGame.MultiSelect = false;
            this.dgvwGame.Name = "dgvwGame";
            this.dgvwGame.ReadOnly = true;
            this.dgvwGame.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvwGame.RowHeadersVisible = false;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            this.dgvwGame.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvwGame.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvwGame.Size = new System.Drawing.Size(1272, 570);
            this.dgvwGame.TabIndex = 24;
            this.dgvwGame.TabStop = false;
            this.dgvwGame.Visible = false;
            this.dgvwGame.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvwGame_CellClick);
            this.dgvwGame.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvwGame_CellEnter);
            this.dgvwGame.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgvwGame_RowPostPaint);
            this.dgvwGame.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvwGame_KeyDown);
            this.dgvwGame.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvwGame_KeyPress);
            // 
            // dgvwPropsAndContests
            // 
            this.dgvwPropsAndContests.AllowUserToAddRows = false;
            this.dgvwPropsAndContests.AllowUserToDeleteRows = false;
            this.dgvwPropsAndContests.AllowUserToResizeColumns = false;
            this.dgvwPropsAndContests.AllowUserToResizeRows = false;
            dataGridViewCellStyle2.Padding = new System.Windows.Forms.Padding(1);
            this.dgvwPropsAndContests.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvwPropsAndContests.BackgroundColor = System.Drawing.Color.White;
            this.dgvwPropsAndContests.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvwPropsAndContests.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvwPropsAndContests.Location = new System.Drawing.Point(0, 0);
            this.dgvwPropsAndContests.MultiSelect = false;
            this.dgvwPropsAndContests.Name = "dgvwPropsAndContests";
            this.dgvwPropsAndContests.ReadOnly = true;
            this.dgvwPropsAndContests.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvwPropsAndContests.RowHeadersVisible = false;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvwPropsAndContests.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvwPropsAndContests.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvwPropsAndContests.Size = new System.Drawing.Size(1272, 570);
            this.dgvwPropsAndContests.TabIndex = 25;
            this.dgvwPropsAndContests.TabStop = false;
            this.dgvwPropsAndContests.Visible = false;
            this.dgvwPropsAndContests.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvwPropsAndContests_CellClick);
            this.dgvwPropsAndContests.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvwPropsAndContests_CellEnter);
            this.dgvwPropsAndContests.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.dgvwPropsAndContests_RowPostPaint);
            this.dgvwPropsAndContests.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvwPropsAndContests_KeyDown);
            this.dgvwPropsAndContests.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgvwPropsAndContests_KeyPress);
            // 
            // bndsrcGame
            // 
            this.bndsrcGame.DataSource = typeof(SIDLibraries.BusinessLayer.CustomerActiveGame);
            // 
            // panGamesInfo
            // 
            this.panGamesInfo.Controls.Add(this.panGamePeriodDates);
            this.panGamesInfo.Controls.Add(this.panLastLineChangeDate);
            this.panGamesInfo.Controls.Add(this.chbActiveOnly);
            this.panGamesInfo.Controls.Add(this.panDecimalToFract);
            this.panGamesInfo.Controls.Add(this.btnLineDisplayToggle);
            this.panGamesInfo.Controls.Add(this.txtTeam1ID);
            this.panGamesInfo.Controls.Add(this.btnUpdateLines);
            this.panGamesInfo.Controls.Add(this.txtTeam2ID);
            this.panGamesInfo.Controls.Add(this.lblCurrentlyShowing);
            this.panGamesInfo.Controls.Add(this.txtGameDateTime);
            this.panGamesInfo.Controls.Add(this.panGamePeriodSelection);
            this.panGamesInfo.Controls.Add(this.textBox3);
            this.panGamesInfo.Controls.Add(this.lblShiftIndication);
            this.panGamesInfo.Controls.Add(this.txtTeam1Score);
            this.panGamesInfo.Controls.Add(this.txtTeam2Score);
            this.panGamesInfo.Controls.Add(this.txtGameInfo);
            this.panGamesInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panGamesInfo.Location = new System.Drawing.Point(0, 0);
            this.panGamesInfo.Name = "panGamesInfo";
            this.panGamesInfo.Size = new System.Drawing.Size(1272, 113);
            this.panGamesInfo.TabIndex = 44;
            // 
            // panGamePeriodDates
            // 
            this.panGamePeriodDates.Controls.Add(this.lblGameNumber);
            this.panGamePeriodDates.Controls.Add(this.lblTimeChanged);
            this.panGamePeriodDates.Controls.Add(this.lblGameStatus);
            this.panGamePeriodDates.Controls.Add(this.lblPeriodWagerCutoff);
            this.panGamePeriodDates.Controls.Add(this.lblGameDateTime);
            this.panGamePeriodDates.Controls.Add(this.label3);
            this.panGamePeriodDates.Controls.Add(this.label5);
            this.panGamePeriodDates.Controls.Add(this.lblWagerCutOff);
            this.panGamePeriodDates.Controls.Add(this.label7);
            this.panGamePeriodDates.Controls.Add(this.label8);
            this.panGamePeriodDates.Location = new System.Drawing.Point(914, 3);
            this.panGamePeriodDates.Name = "panGamePeriodDates";
            this.panGamePeriodDates.Size = new System.Drawing.Size(259, 109);
            this.panGamePeriodDates.TabIndex = 44;
            this.panGamePeriodDates.Visible = false;
            // 
            // lblGameNumber
            // 
            this.lblGameNumber.AutoSize = true;
            this.lblGameNumber.Location = new System.Drawing.Point(16, 91);
            this.lblGameNumber.Name = "lblGameNumber";
            this.lblGameNumber.Size = new System.Drawing.Size(75, 13);
            this.lblGameNumber.TabIndex = 44;
            this.lblGameNumber.Text = "GameNumber:";
            this.lblGameNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTimeChanged
            // 
            this.lblTimeChanged.AutoSize = true;
            this.lblTimeChanged.Location = new System.Drawing.Point(15, 77);
            this.lblTimeChanged.Name = "lblTimeChanged";
            this.lblTimeChanged.Size = new System.Drawing.Size(79, 13);
            this.lblTimeChanged.TabIndex = 43;
            this.lblTimeChanged.Text = "Time Changed:";
            this.lblTimeChanged.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGameStatus
            // 
            this.lblGameStatus.AutoSize = true;
            this.lblGameStatus.Location = new System.Drawing.Point(16, 59);
            this.lblGameStatus.Name = "lblGameStatus";
            this.lblGameStatus.Size = new System.Drawing.Size(40, 13);
            this.lblGameStatus.TabIndex = 42;
            this.lblGameStatus.Text = "Status:";
            this.lblGameStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPeriodWagerCutoff
            // 
            this.lblPeriodWagerCutoff.AutoSize = true;
            this.lblPeriodWagerCutoff.Location = new System.Drawing.Point(16, 44);
            this.lblPeriodWagerCutoff.Name = "lblPeriodWagerCutoff";
            this.lblPeriodWagerCutoff.Size = new System.Drawing.Size(106, 13);
            this.lblPeriodWagerCutoff.TabIndex = 41;
            this.lblPeriodWagerCutoff.Text = "Period Wager Cutoff:";
            this.lblPeriodWagerCutoff.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblGameDateTime
            // 
            this.lblGameDateTime.AutoSize = true;
            this.lblGameDateTime.Location = new System.Drawing.Point(16, 13);
            this.lblGameDateTime.Name = "lblGameDateTime";
            this.lblGameDateTime.Size = new System.Drawing.Size(84, 13);
            this.lblGameDateTime.TabIndex = 39;
            this.lblGameDateTime.Text = "GameDateTime:";
            this.lblGameDateTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(103, 11);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 35;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(103, 40);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 13);
            this.label5.TabIndex = 36;
            // 
            // lblWagerCutOff
            // 
            this.lblWagerCutOff.AutoSize = true;
            this.lblWagerCutOff.Location = new System.Drawing.Point(15, 31);
            this.lblWagerCutOff.Name = "lblWagerCutOff";
            this.lblWagerCutOff.Size = new System.Drawing.Size(73, 13);
            this.lblWagerCutOff.TabIndex = 40;
            this.lblWagerCutOff.Text = "Wager Cutoff:";
            this.lblWagerCutOff.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(103, 117);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 13);
            this.label7.TabIndex = 37;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(103, 78);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(0, 13);
            this.label8.TabIndex = 38;
            // 
            // panLastLineChangeDate
            // 
            this.panLastLineChangeDate.Controls.Add(this.lblLastSpread);
            this.panLastLineChangeDate.Controls.Add(this.lblTotalPoints);
            this.panLastLineChangeDate.Controls.Add(this.lblLastSpreadChange);
            this.panLastLineChangeDate.Controls.Add(this.lblLastTeamTotals);
            this.panLastLineChangeDate.Controls.Add(this.lblLastMoneyLineChange);
            this.panLastLineChangeDate.Controls.Add(this.lblLastMoney);
            this.panLastLineChangeDate.Controls.Add(this.lblLastTotalPntsChange);
            this.panLastLineChangeDate.Controls.Add(this.lblLastTeamTotalsChange);
            this.panLastLineChangeDate.Location = new System.Drawing.Point(826, 10);
            this.panLastLineChangeDate.Name = "panLastLineChangeDate";
            this.panLastLineChangeDate.Size = new System.Drawing.Size(72, 102);
            this.panLastLineChangeDate.TabIndex = 43;
            this.panLastLineChangeDate.Visible = false;
            // 
            // lblLastSpread
            // 
            this.lblLastSpread.AutoSize = true;
            this.lblLastSpread.Location = new System.Drawing.Point(15, 8);
            this.lblLastSpread.Name = "lblLastSpread";
            this.lblLastSpread.Size = new System.Drawing.Size(41, 13);
            this.lblLastSpread.TabIndex = 39;
            this.lblLastSpread.Text = "Spread";
            this.lblLastSpread.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblTotalPoints
            // 
            this.lblTotalPoints.AutoSize = true;
            this.lblTotalPoints.Location = new System.Drawing.Point(19, 73);
            this.lblTotalPoints.Name = "lblTotalPoints";
            this.lblTotalPoints.Size = new System.Drawing.Size(63, 13);
            this.lblTotalPoints.TabIndex = 42;
            this.lblTotalPoints.Text = "Total Points";
            this.lblTotalPoints.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblLastSpreadChange
            // 
            this.lblLastSpreadChange.AutoSize = true;
            this.lblLastSpreadChange.Location = new System.Drawing.Point(103, 11);
            this.lblLastSpreadChange.Name = "lblLastSpreadChange";
            this.lblLastSpreadChange.Size = new System.Drawing.Size(0, 13);
            this.lblLastSpreadChange.TabIndex = 35;
            // 
            // lblLastTeamTotals
            // 
            this.lblLastTeamTotals.AutoSize = true;
            this.lblLastTeamTotals.Location = new System.Drawing.Point(16, 51);
            this.lblLastTeamTotals.Name = "lblLastTeamTotals";
            this.lblLastTeamTotals.Size = new System.Drawing.Size(66, 13);
            this.lblLastTeamTotals.TabIndex = 41;
            this.lblLastTeamTotals.Text = "Team Totals";
            this.lblLastTeamTotals.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblLastMoneyLineChange
            // 
            this.lblLastMoneyLineChange.AutoSize = true;
            this.lblLastMoneyLineChange.Location = new System.Drawing.Point(103, 40);
            this.lblLastMoneyLineChange.Name = "lblLastMoneyLineChange";
            this.lblLastMoneyLineChange.Size = new System.Drawing.Size(0, 13);
            this.lblLastMoneyLineChange.TabIndex = 36;
            // 
            // lblLastMoney
            // 
            this.lblLastMoney.AutoSize = true;
            this.lblLastMoney.Location = new System.Drawing.Point(15, 29);
            this.lblLastMoney.Name = "lblLastMoney";
            this.lblLastMoney.Size = new System.Drawing.Size(62, 13);
            this.lblLastMoney.TabIndex = 40;
            this.lblLastMoney.Text = "Money Line";
            this.lblLastMoney.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblLastTotalPntsChange
            // 
            this.lblLastTotalPntsChange.AutoSize = true;
            this.lblLastTotalPntsChange.Location = new System.Drawing.Point(103, 117);
            this.lblLastTotalPntsChange.Name = "lblLastTotalPntsChange";
            this.lblLastTotalPntsChange.Size = new System.Drawing.Size(0, 13);
            this.lblLastTotalPntsChange.TabIndex = 37;
            // 
            // lblLastTeamTotalsChange
            // 
            this.lblLastTeamTotalsChange.AutoSize = true;
            this.lblLastTeamTotalsChange.Location = new System.Drawing.Point(103, 78);
            this.lblLastTeamTotalsChange.Name = "lblLastTeamTotalsChange";
            this.lblLastTeamTotalsChange.Size = new System.Drawing.Size(0, 13);
            this.lblLastTeamTotalsChange.TabIndex = 38;
            // 
            // chbActiveOnly
            // 
            this.chbActiveOnly.Appearance = System.Windows.Forms.Appearance.Button;
            this.chbActiveOnly.AutoSize = true;
            this.chbActiveOnly.Location = new System.Drawing.Point(3, 10);
            this.chbActiveOnly.Name = "chbActiveOnly";
            this.chbActiveOnly.Size = new System.Drawing.Size(92, 23);
            this.chbActiveOnly.TabIndex = 9;
            this.chbActiveOnly.TabStop = false;
            this.chbActiveOnly.Text = "Active Only (F9)";
            this.chbActiveOnly.UseVisualStyleBackColor = true;
            this.chbActiveOnly.CheckedChanged += new System.EventHandler(this.chbActiveOnly_CheckedChanged);
            // 
            // panDecimalToFract
            // 
            this.panDecimalToFract.Controls.Add(this.btnConvertToFrac);
            this.panDecimalToFract.Controls.Add(this.lblFracValue);
            this.panDecimalToFract.Controls.Add(this.lblProcessDuration);
            this.panDecimalToFract.Controls.Add(this.txtDecimalOdds);
            this.panDecimalToFract.Controls.Add(this.lblDuration);
            this.panDecimalToFract.Controls.Add(this.lblFractionalOdds);
            this.panDecimalToFract.Location = new System.Drawing.Point(656, 10);
            this.panDecimalToFract.Name = "panDecimalToFract";
            this.panDecimalToFract.Size = new System.Drawing.Size(164, 86);
            this.panDecimalToFract.TabIndex = 34;
            this.panDecimalToFract.Visible = false;
            // 
            // btnConvertToFrac
            // 
            this.btnConvertToFrac.Location = new System.Drawing.Point(5, 4);
            this.btnConvertToFrac.Name = "btnConvertToFrac";
            this.btnConvertToFrac.Size = new System.Drawing.Size(70, 23);
            this.btnConvertToFrac.TabIndex = 27;
            this.btnConvertToFrac.TabStop = false;
            this.btnConvertToFrac.Text = "DecToFrac";
            this.btnConvertToFrac.UseVisualStyleBackColor = true;
            this.btnConvertToFrac.Click += new System.EventHandler(this.btnConvertToFrac_Click);
            // 
            // lblFracValue
            // 
            this.lblFracValue.AutoSize = true;
            this.lblFracValue.Location = new System.Drawing.Point(3, 30);
            this.lblFracValue.Name = "lblFracValue";
            this.lblFracValue.Size = new System.Drawing.Size(90, 13);
            this.lblFracValue.TabIndex = 33;
            this.lblFracValue.Text = "Frac Equivalence";
            // 
            // lblProcessDuration
            // 
            this.lblProcessDuration.AutoSize = true;
            this.lblProcessDuration.Location = new System.Drawing.Point(100, 61);
            this.lblProcessDuration.Name = "lblProcessDuration";
            this.lblProcessDuration.Size = new System.Drawing.Size(0, 13);
            this.lblProcessDuration.TabIndex = 32;
            // 
            // txtDecimalOdds
            // 
            this.txtDecimalOdds.Location = new System.Drawing.Point(80, 5);
            this.txtDecimalOdds.Name = "txtDecimalOdds";
            this.txtDecimalOdds.Size = new System.Drawing.Size(100, 20);
            this.txtDecimalOdds.TabIndex = 28;
            this.txtDecimalOdds.TabStop = false;
            // 
            // lblDuration
            // 
            this.lblDuration.AutoSize = true;
            this.lblDuration.Location = new System.Drawing.Point(3, 49);
            this.lblDuration.Name = "lblDuration";
            this.lblDuration.Size = new System.Drawing.Size(72, 13);
            this.lblDuration.TabIndex = 29;
            this.lblDuration.Text = "Proc Duration";
            // 
            // lblFractionalOdds
            // 
            this.lblFractionalOdds.AutoSize = true;
            this.lblFractionalOdds.Location = new System.Drawing.Point(186, 31);
            this.lblFractionalOdds.Name = "lblFractionalOdds";
            this.lblFractionalOdds.Size = new System.Drawing.Size(0, 13);
            this.lblFractionalOdds.TabIndex = 26;
            // 
            // btnLineDisplayToggle
            // 
            this.btnLineDisplayToggle.Location = new System.Drawing.Point(3, 33);
            this.btnLineDisplayToggle.Name = "btnLineDisplayToggle";
            this.btnLineDisplayToggle.Size = new System.Drawing.Size(92, 23);
            this.btnLineDisplayToggle.TabIndex = 1;
            this.btnLineDisplayToggle.TabStop = false;
            this.btnLineDisplayToggle.Text = "American";
            this.btnLineDisplayToggle.UseVisualStyleBackColor = true;
            this.btnLineDisplayToggle.Click += new System.EventHandler(this.btnLineDisplayToggle_Click);
            // 
            // txtTeam1ID
            // 
            this.txtTeam1ID.Enabled = false;
            this.txtTeam1ID.Location = new System.Drawing.Point(101, 12);
            this.txtTeam1ID.Name = "txtTeam1ID";
            this.txtTeam1ID.ReadOnly = true;
            this.txtTeam1ID.Size = new System.Drawing.Size(145, 20);
            this.txtTeam1ID.TabIndex = 2;
            // 
            // btnUpdateLines
            // 
            this.btnUpdateLines.Location = new System.Drawing.Point(1179, 13);
            this.btnUpdateLines.Name = "btnUpdateLines";
            this.btnUpdateLines.Size = new System.Drawing.Size(91, 23);
            this.btnUpdateLines.TabIndex = 2;
            this.btnUpdateLines.TabStop = false;
            this.btnUpdateLines.Text = "Refresh Lines";
            this.btnUpdateLines.UseVisualStyleBackColor = true;
            this.btnUpdateLines.Visible = false;
            this.btnUpdateLines.Click += new System.EventHandler(this.btnUpdateLines_Click);
            // 
            // txtTeam2ID
            // 
            this.txtTeam2ID.Enabled = false;
            this.txtTeam2ID.Location = new System.Drawing.Point(101, 32);
            this.txtTeam2ID.Name = "txtTeam2ID";
            this.txtTeam2ID.ReadOnly = true;
            this.txtTeam2ID.Size = new System.Drawing.Size(145, 20);
            this.txtTeam2ID.TabIndex = 3;
            // 
            // lblCurrentlyShowing
            // 
            this.lblCurrentlyShowing.AutoSize = true;
            this.lblCurrentlyShowing.Location = new System.Drawing.Point(9, 59);
            this.lblCurrentlyShowing.Name = "lblCurrentlyShowing";
            this.lblCurrentlyShowing.Size = new System.Drawing.Size(0, 13);
            this.lblCurrentlyShowing.TabIndex = 21;
            // 
            // txtGameDateTime
            // 
            this.txtGameDateTime.Enabled = false;
            this.txtGameDateTime.Location = new System.Drawing.Point(252, 12);
            this.txtGameDateTime.Name = "txtGameDateTime";
            this.txtGameDateTime.ReadOnly = true;
            this.txtGameDateTime.Size = new System.Drawing.Size(178, 20);
            this.txtGameDateTime.TabIndex = 4;
            this.txtGameDateTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panGamePeriodSelection
            // 
            this.panGamePeriodSelection.Controls.Add(this.radAltRL_3);
            this.panGamePeriodSelection.Controls.Add(this.radThirdPeriod_3);
            this.panGamePeriodSelection.Controls.Add(this.radSecondPeriod_2);
            this.panGamePeriodSelection.Controls.Add(this.radFirstPeriod_1);
            this.panGamePeriodSelection.Controls.Add(this.radFourthQuarter_6);
            this.panGamePeriodSelection.Controls.Add(this.radThirdQuarter_5);
            this.panGamePeriodSelection.Controls.Add(this.radSecondQuarter_4);
            this.panGamePeriodSelection.Controls.Add(this.radFirstQuarter_3);
            this.panGamePeriodSelection.Controls.Add(this.radSecondHalf_2);
            this.panGamePeriodSelection.Controls.Add(this.radFirstHalf_1);
            this.panGamePeriodSelection.Controls.Add(this.radGame_0);
            this.panGamePeriodSelection.Location = new System.Drawing.Point(71, 76);
            this.panGamePeriodSelection.Name = "panGamePeriodSelection";
            this.panGamePeriodSelection.Size = new System.Drawing.Size(599, 34);
            this.panGamePeriodSelection.TabIndex = 20;
            // 
            // radAltRL_3
            // 
            this.radAltRL_3.AccessibleDescription = "Alternate Runlines";
            this.radAltRL_3.Appearance = System.Windows.Forms.Appearance.Button;
            this.radAltRL_3.AutoSize = true;
            this.radAltRL_3.Location = new System.Drawing.Point(528, 5);
            this.radAltRL_3.Name = "radAltRL_3";
            this.radAltRL_3.Size = new System.Drawing.Size(52, 23);
            this.radAltRL_3.TabIndex = 11;
            this.radAltRL_3.Text = "Alt Run";
            this.radAltRL_3.UseVisualStyleBackColor = true;
            this.radAltRL_3.CheckedChanged += new System.EventHandler(this.radObj_CheckedChanged);
            // 
            // radThirdPeriod_3
            // 
            this.radThirdPeriod_3.AccessibleDescription = "3rd Period";
            this.radThirdPeriod_3.Appearance = System.Windows.Forms.Appearance.Button;
            this.radThirdPeriod_3.AutoSize = true;
            this.radThirdPeriod_3.Location = new System.Drawing.Point(467, 5);
            this.radThirdPeriod_3.Name = "radThirdPeriod_3";
            this.radThirdPeriod_3.Size = new System.Drawing.Size(42, 23);
            this.radThirdPeriod_3.TabIndex = 10;
            this.radThirdPeriod_3.Text = "3rd P";
            this.radThirdPeriod_3.UseVisualStyleBackColor = true;
            this.radThirdPeriod_3.CheckedChanged += new System.EventHandler(this.radObj_CheckedChanged);
            // 
            // radSecondPeriod_2
            // 
            this.radSecondPeriod_2.AccessibleDescription = "2nd Period";
            this.radSecondPeriod_2.Appearance = System.Windows.Forms.Appearance.Button;
            this.radSecondPeriod_2.AutoSize = true;
            this.radSecondPeriod_2.Location = new System.Drawing.Point(421, 5);
            this.radSecondPeriod_2.Name = "radSecondPeriod_2";
            this.radSecondPeriod_2.Size = new System.Drawing.Size(45, 23);
            this.radSecondPeriod_2.TabIndex = 9;
            this.radSecondPeriod_2.Text = "2nd P";
            this.radSecondPeriod_2.UseVisualStyleBackColor = true;
            this.radSecondPeriod_2.CheckedChanged += new System.EventHandler(this.radObj_CheckedChanged);
            // 
            // radFirstPeriod_1
            // 
            this.radFirstPeriod_1.AccessibleDescription = "1st Period";
            this.radFirstPeriod_1.Appearance = System.Windows.Forms.Appearance.Button;
            this.radFirstPeriod_1.AutoSize = true;
            this.radFirstPeriod_1.Location = new System.Drawing.Point(379, 5);
            this.radFirstPeriod_1.Name = "radFirstPeriod_1";
            this.radFirstPeriod_1.Size = new System.Drawing.Size(41, 23);
            this.radFirstPeriod_1.TabIndex = 8;
            this.radFirstPeriod_1.Text = "1st P";
            this.radFirstPeriod_1.UseVisualStyleBackColor = true;
            this.radFirstPeriod_1.CheckedChanged += new System.EventHandler(this.radObj_CheckedChanged);
            // 
            // radFourthQuarter_6
            // 
            this.radFourthQuarter_6.AccessibleDescription = "4th Quarter";
            this.radFourthQuarter_6.Appearance = System.Windows.Forms.Appearance.Button;
            this.radFourthQuarter_6.AutoSize = true;
            this.radFourthQuarter_6.Location = new System.Drawing.Point(315, 5);
            this.radFourthQuarter_6.Name = "radFourthQuarter_6";
            this.radFourthQuarter_6.Size = new System.Drawing.Size(43, 23);
            this.radFourthQuarter_6.TabIndex = 7;
            this.radFourthQuarter_6.Text = "4th Q";
            this.radFourthQuarter_6.UseVisualStyleBackColor = true;
            this.radFourthQuarter_6.CheckedChanged += new System.EventHandler(this.radObj_CheckedChanged);
            // 
            // radThirdQuarter_5
            // 
            this.radThirdQuarter_5.AccessibleDescription = "3rd Quarter";
            this.radThirdQuarter_5.Appearance = System.Windows.Forms.Appearance.Button;
            this.radThirdQuarter_5.AutoSize = true;
            this.radThirdQuarter_5.Location = new System.Drawing.Point(271, 5);
            this.radThirdQuarter_5.Name = "radThirdQuarter_5";
            this.radThirdQuarter_5.Size = new System.Drawing.Size(43, 23);
            this.radThirdQuarter_5.TabIndex = 6;
            this.radThirdQuarter_5.Text = "3rd Q";
            this.radThirdQuarter_5.UseVisualStyleBackColor = true;
            this.radThirdQuarter_5.CheckedChanged += new System.EventHandler(this.radObj_CheckedChanged);
            // 
            // radSecondQuarter_4
            // 
            this.radSecondQuarter_4.AccessibleDescription = "2nd Quarter";
            this.radSecondQuarter_4.Appearance = System.Windows.Forms.Appearance.Button;
            this.radSecondQuarter_4.AutoSize = true;
            this.radSecondQuarter_4.Location = new System.Drawing.Point(224, 5);
            this.radSecondQuarter_4.Name = "radSecondQuarter_4";
            this.radSecondQuarter_4.Size = new System.Drawing.Size(46, 23);
            this.radSecondQuarter_4.TabIndex = 5;
            this.radSecondQuarter_4.Text = "2nd Q";
            this.radSecondQuarter_4.UseVisualStyleBackColor = true;
            this.radSecondQuarter_4.CheckedChanged += new System.EventHandler(this.radObj_CheckedChanged);
            // 
            // radFirstQuarter_3
            // 
            this.radFirstQuarter_3.AccessibleDescription = "1st Quarter, 3rd Period";
            this.radFirstQuarter_3.Appearance = System.Windows.Forms.Appearance.Button;
            this.radFirstQuarter_3.AutoSize = true;
            this.radFirstQuarter_3.Location = new System.Drawing.Point(181, 5);
            this.radFirstQuarter_3.Name = "radFirstQuarter_3";
            this.radFirstQuarter_3.Size = new System.Drawing.Size(42, 23);
            this.radFirstQuarter_3.TabIndex = 4;
            this.radFirstQuarter_3.Text = "1st Q";
            this.radFirstQuarter_3.UseVisualStyleBackColor = true;
            this.radFirstQuarter_3.CheckedChanged += new System.EventHandler(this.radObj_CheckedChanged);
            // 
            // radSecondHalf_2
            // 
            this.radSecondHalf_2.AccessibleDescription = "2nd Half, 2nd Period, Last 4 Innings";
            this.radSecondHalf_2.Appearance = System.Windows.Forms.Appearance.Button;
            this.radSecondHalf_2.AutoSize = true;
            this.radSecondHalf_2.Location = new System.Drawing.Point(114, 5);
            this.radSecondHalf_2.Name = "radSecondHalf_2";
            this.radSecondHalf_2.Size = new System.Drawing.Size(46, 23);
            this.radSecondHalf_2.TabIndex = 3;
            this.radSecondHalf_2.Text = "2nd H";
            this.radSecondHalf_2.UseVisualStyleBackColor = true;
            this.radSecondHalf_2.CheckedChanged += new System.EventHandler(this.radObj_CheckedChanged);
            // 
            // radFirstHalf_1
            // 
            this.radFirstHalf_1.AccessibleDescription = "1st Half, 1st 5 Innings, 1st Period";
            this.radFirstHalf_1.Appearance = System.Windows.Forms.Appearance.Button;
            this.radFirstHalf_1.AutoSize = true;
            this.radFirstHalf_1.Location = new System.Drawing.Point(71, 5);
            this.radFirstHalf_1.Name = "radFirstHalf_1";
            this.radFirstHalf_1.Size = new System.Drawing.Size(42, 23);
            this.radFirstHalf_1.TabIndex = 2;
            this.radFirstHalf_1.Text = "1st H";
            this.radFirstHalf_1.UseVisualStyleBackColor = true;
            this.radFirstHalf_1.CheckedChanged += new System.EventHandler(this.radObj_CheckedChanged);
            // 
            // radGame_0
            // 
            this.radGame_0.AccessibleDescription = "Game";
            this.radGame_0.Appearance = System.Windows.Forms.Appearance.Button;
            this.radGame_0.AutoSize = true;
            this.radGame_0.Location = new System.Drawing.Point(5, 5);
            this.radGame_0.Name = "radGame_0";
            this.radGame_0.Size = new System.Drawing.Size(45, 23);
            this.radGame_0.TabIndex = 1;
            this.radGame_0.Text = "Game";
            this.radGame_0.UseVisualStyleBackColor = true;
            this.radGame_0.CheckedChanged += new System.EventHandler(this.radObj_CheckedChanged);
            // 
            // textBox3
            // 
            this.textBox3.Enabled = false;
            this.textBox3.Location = new System.Drawing.Point(252, 32);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(178, 20);
            this.textBox3.TabIndex = 5;
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblShiftIndication
            // 
            this.lblShiftIndication.AutoSize = true;
            this.lblShiftIndication.Location = new System.Drawing.Point(6, 86);
            this.lblShiftIndication.Name = "lblShiftIndication";
            this.lblShiftIndication.Size = new System.Drawing.Size(64, 13);
            this.lblShiftIndication.TabIndex = 10;
            this.lblShiftIndication.Text = "Shift  + <- ->";
            // 
            // txtTeam1Score
            // 
            this.txtTeam1Score.Enabled = false;
            this.txtTeam1Score.Location = new System.Drawing.Point(451, 12);
            this.txtTeam1Score.Name = "txtTeam1Score";
            this.txtTeam1Score.ReadOnly = true;
            this.txtTeam1Score.Size = new System.Drawing.Size(40, 20);
            this.txtTeam1Score.TabIndex = 6;
            // 
            // txtTeam2Score
            // 
            this.txtTeam2Score.Enabled = false;
            this.txtTeam2Score.Location = new System.Drawing.Point(451, 32);
            this.txtTeam2Score.Name = "txtTeam2Score";
            this.txtTeam2Score.ReadOnly = true;
            this.txtTeam2Score.Size = new System.Drawing.Size(40, 20);
            this.txtTeam2Score.TabIndex = 7;
            // 
            // txtGameInfo
            // 
            this.txtGameInfo.Enabled = false;
            this.txtGameInfo.Location = new System.Drawing.Point(493, 10);
            this.txtGameInfo.Multiline = true;
            this.txtGameInfo.Name = "txtGameInfo";
            this.txtGameInfo.ReadOnly = true;
            this.txtGameInfo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtGameInfo.Size = new System.Drawing.Size(623, 53);
            this.txtGameInfo.TabIndex = 8;
            this.txtGameInfo.TabStop = false;
            // 
            // PeriodWagerCutoff
            // 
            this.PeriodWagerCutoff.DataPropertyName = "PeriodWagerCutoff";
            this.PeriodWagerCutoff.HeaderText = "PeriodWagerCutoff";
            this.PeriodWagerCutoff.Name = "PeriodWagerCutoff";
            this.PeriodWagerCutoff.Visible = false;
            // 
            // chbFreePlay
            // 
            this.chbFreePlay.AutoSize = true;
            this.chbFreePlay.Location = new System.Drawing.Point(586, 4);
            this.chbFreePlay.Name = "chbFreePlay";
            this.chbFreePlay.Size = new System.Drawing.Size(15, 14);
            this.chbFreePlay.TabIndex = 23;
            this.chbFreePlay.UseVisualStyleBackColor = true;
            this.chbFreePlay.Visible = false;
            // 
            // panLeft
            // 
            this.panLeft.Controls.Add(this.treSports);
            this.panLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.panLeft.Location = new System.Drawing.Point(5, 5);
            this.panLeft.Name = "panLeft";
            this.panLeft.Size = new System.Drawing.Size(180, 683);
            this.panLeft.TabIndex = 45;
            // 
            // panRight
            // 
            this.panRight.Controls.Add(this.panGames);
            this.panRight.Controls.Add(this.panGamesInfo);
            this.panRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panRight.Location = new System.Drawing.Point(185, 5);
            this.panRight.Name = "panRight";
            this.panRight.Size = new System.Drawing.Size(1272, 683);
            this.panRight.TabIndex = 46;
            // 
            // spl
            // 
            this.spl.Location = new System.Drawing.Point(185, 5);
            this.spl.Name = "spl";
            this.spl.Size = new System.Drawing.Size(3, 683);
            this.spl.TabIndex = 47;
            this.spl.TabStop = false;
            // 
            // bgwrkUpdateLines
            // 
            this.bgwrkUpdateLines.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgwrkUpdateLines_DoWork);
            this.bgwrkUpdateLines.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgwrkUpdateLines_RunWorkerCompleted);
            // 
            // FrmSportAndGameSelection
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1462, 693);
            this.Controls.Add(this.spl);
            this.Controls.Add(this.panRight);
            this.Controls.Add(this.panLeft);
            this.KeyPreview = true;
            this.Name = "FrmSportAndGameSelection";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "CUSTOMERID (TYPE - STORE) found in DATABASE";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmSportAndGameSelection_FormClosing);
            this.Load += new System.EventHandler(this.frmSportAndGameSelection_Load);
            this.Shown += new System.EventHandler(this.FrmSportAndGameSelection_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmSportAndGameSelection_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FrmSportAndGameSelection_KeyUp);
            this.panGames.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvwGame)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvwPropsAndContests)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bndsrcGame)).EndInit();
            this.panGamesInfo.ResumeLayout(false);
            this.panGamesInfo.PerformLayout();
            this.panGamePeriodDates.ResumeLayout(false);
            this.panGamePeriodDates.PerformLayout();
            this.panLastLineChangeDate.ResumeLayout(false);
            this.panLastLineChangeDate.PerformLayout();
            this.panDecimalToFract.ResumeLayout(false);
            this.panDecimalToFract.PerformLayout();
            this.panGamePeriodSelection.ResumeLayout(false);
            this.panGamePeriodSelection.PerformLayout();
            this.panLeft.ResumeLayout(false);
            this.panRight.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView treSports;
        private System.Windows.Forms.Button btnLineDisplayToggle;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox txtGameDateTime;
        private System.Windows.Forms.TextBox txtTeam2ID;
        private System.Windows.Forms.TextBox txtTeam1ID;
        private System.Windows.Forms.TextBox txtTeam2Score;
        private System.Windows.Forms.TextBox txtTeam1Score;
        private System.Windows.Forms.TextBox txtGameInfo;
        private System.Windows.Forms.CheckBox chbActiveOnly;
        private System.Windows.Forms.Label lblShiftIndication;
        private System.Windows.Forms.Panel panGamePeriodSelection;
        private System.Windows.Forms.RadioButton radThirdPeriod_3;
        private System.Windows.Forms.RadioButton radSecondPeriod_2;
        private System.Windows.Forms.RadioButton radFirstPeriod_1;
        private System.Windows.Forms.RadioButton radFourthQuarter_6;
        private System.Windows.Forms.RadioButton radThirdQuarter_5;
        private System.Windows.Forms.RadioButton radSecondQuarter_4;
        private System.Windows.Forms.RadioButton radFirstQuarter_3;
        private System.Windows.Forms.RadioButton radSecondHalf_2;
        private System.Windows.Forms.RadioButton radFirstHalf_1;
        private System.Windows.Forms.RadioButton radGame_0;
        private System.Windows.Forms.Label lblCurrentlyShowing;
        private System.Windows.Forms.Button btnUpdateLines;
        private System.Windows.Forms.BindingSource bndsrcGame;
        private System.Windows.Forms.Timer tmrKeysPressed;
        private System.Windows.Forms.CheckBox chbFreePlay;
        private System.Windows.Forms.DataGridViewTextBoxColumn PeriodWagerCutoff;
        private System.Windows.Forms.DataGridView dgvwGame;
        private System.Windows.Forms.Panel panGames;
        private System.Windows.Forms.Label lblProcessDuration;
        private System.Windows.Forms.Label lblDuration;
        private System.Windows.Forms.TextBox txtDecimalOdds;
        private System.Windows.Forms.Button btnConvertToFrac;
        private System.Windows.Forms.Label lblFractionalOdds;
        private System.Windows.Forms.Label lblFracValue;
        private System.Windows.Forms.Panel panDecimalToFract;
        private System.Windows.Forms.DataGridView dgvwPropsAndContests;
        private System.Windows.Forms.Label lblLastTeamTotalsChange;
        private System.Windows.Forms.Label lblLastTotalPntsChange;
        private System.Windows.Forms.Label lblLastMoneyLineChange;
        private System.Windows.Forms.Label lblLastSpreadChange;
        private System.Windows.Forms.Label lblTotalPoints;
        private System.Windows.Forms.Label lblLastTeamTotals;
        private System.Windows.Forms.Label lblLastMoney;
        private System.Windows.Forms.Label lblLastSpread;
        private System.Windows.Forms.Panel panLastLineChangeDate;
        private System.Windows.Forms.Panel panGamesInfo;
        private System.Windows.Forms.Panel panLeft;
        private System.Windows.Forms.Panel panRight;
        private System.Windows.Forms.Splitter spl;
        private System.Windows.Forms.Panel panGamePeriodDates;
        private System.Windows.Forms.Label lblGameDateTime;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblWagerCutOff;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblPeriodWagerCutoff;
        private System.Windows.Forms.Label lblGameStatus;
        private System.Windows.Forms.Label lblTimeChanged;
        private System.Windows.Forms.Label lblGameNumber;
        private System.Windows.Forms.ImageList ilSportsTree;
        private System.ComponentModel.BackgroundWorker bgwrkUpdateLines;
        private System.Windows.Forms.RadioButton radAltRL_3;
    }
}
