﻿namespace TicketWriter.UI
{
    partial class FrmTicketConfirmation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvwReadbackInfo = new System.Windows.Forms.DataGridView();
            this.ctxModifyWager = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.String1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.String2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.String3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WagerTypeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WagerNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RowIsClickable = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TargetFrm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GameNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PeriodNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RifBackwardTicketNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RifBackwardWagerNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FreePlayFlag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WagerType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SpecialIfBetAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvwReadbackInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvwReadbackInfo
            // 
            this.dgvwReadbackInfo.AllowUserToAddRows = false;
            this.dgvwReadbackInfo.AllowUserToDeleteRows = false;
            this.dgvwReadbackInfo.AllowUserToResizeRows = false;
            this.dgvwReadbackInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCellsExceptHeader;
            this.dgvwReadbackInfo.BackgroundColor = System.Drawing.Color.White;
            this.dgvwReadbackInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvwReadbackInfo.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvwReadbackInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvwReadbackInfo.ColumnHeadersVisible = false;
            this.dgvwReadbackInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.String1,
            this.String2,
            this.String3,
            this.WagerTypeName,
            this.WagerNumber,
            this.ItemNumber,
            this.RowIsClickable,
            this.TargetFrm,
            this.GameNumber,
            this.PeriodNumber,
            this.RifBackwardTicketNumber,
            this.RifBackwardWagerNumber,
            this.FreePlayFlag,
            this.WagerType,
            this.SpecialIfBetAmount});
            this.dgvwReadbackInfo.ContextMenuStrip = this.ctxModifyWager;
            this.dgvwReadbackInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvwReadbackInfo.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvwReadbackInfo.Location = new System.Drawing.Point(5, 5);
            this.dgvwReadbackInfo.MultiSelect = false;
            this.dgvwReadbackInfo.Name = "dgvwReadbackInfo";
            this.dgvwReadbackInfo.ReadOnly = true;
            this.dgvwReadbackInfo.RowHeadersVisible = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvwReadbackInfo.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvwReadbackInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvwReadbackInfo.Size = new System.Drawing.Size(603, 221);
            this.dgvwReadbackInfo.TabIndex = 0;
            this.dgvwReadbackInfo.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvwReadbackInfo_CellDoubleClick);
            this.dgvwReadbackInfo.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvwReadbackInfo_CellMouseClick);
            this.dgvwReadbackInfo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmTicketConfirmation_KeyDown);
            this.dgvwReadbackInfo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgvwReadbackInfo_MouseDown);
            // 
            // ctxModifyWager
            // 
            this.ctxModifyWager.Name = "ctxModifyWager";
            this.ctxModifyWager.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.ctxModifyWager.Size = new System.Drawing.Size(61, 4);
            this.ctxModifyWager.Opening += new System.ComponentModel.CancelEventHandler(this.ctxModifyWager_Opening);
            // 
            // String1
            // 
            this.String1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.String1.HeaderText = "String1";
            this.String1.MinimumWidth = 25;
            this.String1.Name = "String1";
            this.String1.ReadOnly = true;
            this.String1.Width = 25;
            // 
            // String2
            // 
            this.String2.HeaderText = "String2";
            this.String2.Name = "String2";
            this.String2.ReadOnly = true;
            this.String2.Width = 5;
            // 
            // String3
            // 
            this.String3.HeaderText = "String3";
            this.String3.Name = "String3";
            this.String3.ReadOnly = true;
            this.String3.Width = 5;
            // 
            // WagerTypeName
            // 
            this.WagerTypeName.HeaderText = "WagerTypeName";
            this.WagerTypeName.Name = "WagerTypeName";
            this.WagerTypeName.ReadOnly = true;
            this.WagerTypeName.Width = 5;
            // 
            // WagerNumber
            // 
            this.WagerNumber.HeaderText = "WagerNumber";
            this.WagerNumber.Name = "WagerNumber";
            this.WagerNumber.ReadOnly = true;
            this.WagerNumber.Width = 5;
            // 
            // ItemNumber
            // 
            this.ItemNumber.HeaderText = "ItemNumber";
            this.ItemNumber.Name = "ItemNumber";
            this.ItemNumber.ReadOnly = true;
            this.ItemNumber.Width = 5;
            // 
            // RowIsClickable
            // 
            this.RowIsClickable.HeaderText = "RowIsClickable";
            this.RowIsClickable.Name = "RowIsClickable";
            this.RowIsClickable.ReadOnly = true;
            this.RowIsClickable.Width = 5;
            // 
            // TargetFrm
            // 
            this.TargetFrm.HeaderText = "TargetFrm";
            this.TargetFrm.Name = "TargetFrm";
            this.TargetFrm.ReadOnly = true;
            this.TargetFrm.Width = 5;
            // 
            // GameNumber
            // 
            this.GameNumber.HeaderText = "GameNumber";
            this.GameNumber.Name = "GameNumber";
            this.GameNumber.ReadOnly = true;
            this.GameNumber.Width = 5;
            // 
            // PeriodNumber
            // 
            this.PeriodNumber.HeaderText = "PeriodNumber";
            this.PeriodNumber.Name = "PeriodNumber";
            this.PeriodNumber.ReadOnly = true;
            this.PeriodNumber.Width = 5;
            // 
            // RifBackwardTicketNumber
            // 
            this.RifBackwardTicketNumber.HeaderText = "RifBackwardTicketNumber";
            this.RifBackwardTicketNumber.Name = "RifBackwardTicketNumber";
            this.RifBackwardTicketNumber.ReadOnly = true;
            this.RifBackwardTicketNumber.Width = 5;
            // 
            // RifBackwardWagerNumber
            // 
            this.RifBackwardWagerNumber.HeaderText = "RifBackwardWagerNumber";
            this.RifBackwardWagerNumber.Name = "RifBackwardWagerNumber";
            this.RifBackwardWagerNumber.ReadOnly = true;
            this.RifBackwardWagerNumber.Width = 5;
            // 
            // FreePlayFlag
            // 
            this.FreePlayFlag.HeaderText = "FreePlayFlag";
            this.FreePlayFlag.Name = "FreePlayFlag";
            this.FreePlayFlag.ReadOnly = true;
            this.FreePlayFlag.Width = 5;
            // 
            // WagerType
            // 
            this.WagerType.HeaderText = "WagerType";
            this.WagerType.Name = "WagerType";
            this.WagerType.ReadOnly = true;
            this.WagerType.Width = 5;
            // 
            // SpecialIfBetAmount
            // 
            this.SpecialIfBetAmount.HeaderText = "SpecialIfBetAmount";
            this.SpecialIfBetAmount.Name = "SpecialIfBetAmount";
            this.SpecialIfBetAmount.ReadOnly = true;
            this.SpecialIfBetAmount.Width = 5;
            // 
            // FrmTicketConfirmation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(613, 231);
            this.Controls.Add(this.dgvwReadbackInfo);
            this.MinimizeBox = false;
            this.Name = "FrmTicketConfirmation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Pending Wagers for ticket TICKETNUMBER - F12 for Read Back";
            this.Load += new System.EventHandler(this.frmTicketConfirmation_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmTicketConfirmation_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvwReadbackInfo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvwReadbackInfo;
        private System.Windows.Forms.ContextMenuStrip ctxModifyWager;
        private System.Windows.Forms.DataGridViewTextBoxColumn String1;
        private System.Windows.Forms.DataGridViewTextBoxColumn String2;
        private System.Windows.Forms.DataGridViewTextBoxColumn String3;
        private System.Windows.Forms.DataGridViewTextBoxColumn WagerTypeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn WagerNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn RowIsClickable;
        private System.Windows.Forms.DataGridViewTextBoxColumn TargetFrm;
        private System.Windows.Forms.DataGridViewTextBoxColumn GameNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn PeriodNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn RifBackwardTicketNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn RifBackwardWagerNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn FreePlayFlag;
        private System.Windows.Forms.DataGridViewTextBoxColumn WagerType;
        private System.Windows.Forms.DataGridViewTextBoxColumn SpecialIfBetAmount;

    }
}