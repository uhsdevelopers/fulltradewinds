﻿namespace TicketWriter.UI
{
    partial class FrmManualGameWager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbGoToGamePeriod = new System.Windows.Forms.ComboBox();
            this.lblGoTo = new System.Windows.Forms.Label();
            this.txtGameInfo = new System.Windows.Forms.TextBox();
            this.panWagerAmounts = new System.Windows.Forms.Panel();
            this.lblMaxWinDisplay = new System.Windows.Forms.Label();
            this.lblMaxRiskDisplay = new System.Windows.Forms.Label();
            this.grpRiskAmount = new System.Windows.Forms.GroupBox();
            this.txtRisk = new GUILibraries.Controls.NumberTextBox();
            this.grpWinAmount = new System.Windows.Forms.GroupBox();
            this.txtToWin = new GUILibraries.Controls.NumberTextBox();
            this.txtChosenTeam = new System.Windows.Forms.TextBox();
            this.chbLayoffWager = new System.Windows.Forms.CheckBox();
            this.btnPlaceBet = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblWagerType = new System.Windows.Forms.Label();
            this.lblPrice = new System.Windows.Forms.Label();
            this.txtPeriod = new System.Windows.Forms.TextBox();
            this.btnAddToIfBet = new System.Windows.Forms.Button();
            this.btnAddToTeaser = new System.Windows.Forms.Button();
            this.btnAddToParlay = new System.Windows.Forms.Button();
            this.panFixedOdds = new System.Windows.Forms.Panel();
            this.chbFixedOdds = new System.Windows.Forms.CheckBox();
            this.panPitcher2 = new System.Windows.Forms.Panel();
            this.chbPitcher2MustStart = new System.Windows.Forms.CheckBox();
            this.panPitcher1 = new System.Windows.Forms.Panel();
            this.chbPitcher1MustStart = new System.Windows.Forms.CheckBox();
            this.panTxtLineAdj = new System.Windows.Forms.Panel();
            this.panTxtPrice = new System.Windows.Forms.Panel();
            this.panWagerAmounts.SuspendLayout();
            this.grpRiskAmount.SuspendLayout();
            this.grpWinAmount.SuspendLayout();
            this.panFixedOdds.SuspendLayout();
            this.panPitcher2.SuspendLayout();
            this.panPitcher1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbGoToGamePeriod
            // 
            this.cmbGoToGamePeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGoToGamePeriod.FormattingEnabled = true;
            this.cmbGoToGamePeriod.Location = new System.Drawing.Point(176, 76);
            this.cmbGoToGamePeriod.Name = "cmbGoToGamePeriod";
            this.cmbGoToGamePeriod.Size = new System.Drawing.Size(93, 21);
            this.cmbGoToGamePeriod.TabIndex = 130;
            this.cmbGoToGamePeriod.SelectedIndexChanged += new System.EventHandler(this.cmbGoToGamePeriod_SelectedIndexChanged);
            // 
            // lblGoTo
            // 
            this.lblGoTo.AutoSize = true;
            this.lblGoTo.Location = new System.Drawing.Point(135, 80);
            this.lblGoTo.Name = "lblGoTo";
            this.lblGoTo.Size = new System.Drawing.Size(36, 13);
            this.lblGoTo.TabIndex = 1000;
            this.lblGoTo.Text = "Go to:";
            // 
            // txtGameInfo
            // 
            this.txtGameInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtGameInfo.Enabled = false;
            this.txtGameInfo.Location = new System.Drawing.Point(6, 7);
            this.txtGameInfo.Multiline = true;
            this.txtGameInfo.Name = "txtGameInfo";
            this.txtGameInfo.ReadOnly = true;
            this.txtGameInfo.Size = new System.Drawing.Size(325, 63);
            this.txtGameInfo.TabIndex = 1000;
            this.txtGameInfo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panWagerAmounts
            // 
            this.panWagerAmounts.Controls.Add(this.lblMaxWinDisplay);
            this.panWagerAmounts.Controls.Add(this.lblMaxRiskDisplay);
            this.panWagerAmounts.Controls.Add(this.grpRiskAmount);
            this.panWagerAmounts.Controls.Add(this.grpWinAmount);
            this.panWagerAmounts.Location = new System.Drawing.Point(6, 252);
            this.panWagerAmounts.Name = "panWagerAmounts";
            this.panWagerAmounts.Size = new System.Drawing.Size(325, 79);
            this.panWagerAmounts.TabIndex = 50;
            // 
            // lblMaxWinDisplay
            // 
            this.lblMaxWinDisplay.Location = new System.Drawing.Point(185, 49);
            this.lblMaxWinDisplay.Name = "lblMaxWinDisplay";
            this.lblMaxWinDisplay.Size = new System.Drawing.Size(120, 23);
            this.lblMaxWinDisplay.TabIndex = 1004;
            this.lblMaxWinDisplay.Text = "Max To Win";
            // 
            // lblMaxRiskDisplay
            // 
            this.lblMaxRiskDisplay.Location = new System.Drawing.Point(15, 50);
            this.lblMaxRiskDisplay.Name = "lblMaxRiskDisplay";
            this.lblMaxRiskDisplay.Size = new System.Drawing.Size(120, 23);
            this.lblMaxRiskDisplay.TabIndex = 1003;
            this.lblMaxRiskDisplay.Text = "Max Risk";
            // 
            // grpRiskAmount
            // 
            this.grpRiskAmount.Controls.Add(this.txtRisk);
            this.grpRiskAmount.Location = new System.Drawing.Point(10, 3);
            this.grpRiskAmount.Name = "grpRiskAmount";
            this.grpRiskAmount.Size = new System.Drawing.Size(130, 46);
            this.grpRiskAmount.TabIndex = 60;
            this.grpRiskAmount.TabStop = false;
            this.grpRiskAmount.Text = "Risk Amount";
            // 
            // txtRisk
            // 
            this.txtRisk.AccessibleName = "R";
            this.txtRisk.AllowCents = false;
            this.txtRisk.AllowNegatives = false;
            this.txtRisk.DividedBy = 1;
            this.txtRisk.DivideFlag = false;
            this.txtRisk.Location = new System.Drawing.Point(7, 20);
            this.txtRisk.MaxLength = 8;
            this.txtRisk.Name = "txtRisk";
            this.txtRisk.ShowIfZero = true;
            this.txtRisk.Size = new System.Drawing.Size(100, 20);
            this.txtRisk.TabIndex = 70;
            this.txtRisk.TextChanged += new System.EventHandler(this.txtRisk_TextChanged);
            this.txtRisk.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtRisk_KeyUp);
            // 
            // grpWinAmount
            // 
            this.grpWinAmount.Controls.Add(this.txtToWin);
            this.grpWinAmount.Location = new System.Drawing.Point(174, 3);
            this.grpWinAmount.Name = "grpWinAmount";
            this.grpWinAmount.Size = new System.Drawing.Size(142, 46);
            this.grpWinAmount.TabIndex = 80;
            this.grpWinAmount.TabStop = false;
            this.grpWinAmount.Text = "To Win Amount";
            // 
            // txtToWin
            // 
            this.txtToWin.AllowCents = false;
            this.txtToWin.AllowNegatives = false;
            this.txtToWin.DividedBy = 1;
            this.txtToWin.DivideFlag = false;
            this.txtToWin.Location = new System.Drawing.Point(13, 19);
            this.txtToWin.MaxLength = 8;
            this.txtToWin.Name = "txtToWin";
            this.txtToWin.ShowIfZero = true;
            this.txtToWin.Size = new System.Drawing.Size(100, 20);
            this.txtToWin.TabIndex = 90;
            this.txtToWin.TextChanged += new System.EventHandler(this.txtToWin_TextChanged);
            this.txtToWin.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtToWin_KeyUp);
            // 
            // txtChosenTeam
            // 
            this.txtChosenTeam.Enabled = false;
            this.txtChosenTeam.Location = new System.Drawing.Point(7, 103);
            this.txtChosenTeam.Name = "txtChosenTeam";
            this.txtChosenTeam.ReadOnly = true;
            this.txtChosenTeam.Size = new System.Drawing.Size(325, 20);
            this.txtChosenTeam.TabIndex = 1000;
            this.txtChosenTeam.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // chbLayoffWager
            // 
            this.chbLayoffWager.AutoSize = true;
            this.chbLayoffWager.Location = new System.Drawing.Point(239, 337);
            this.chbLayoffWager.Name = "chbLayoffWager";
            this.chbLayoffWager.Size = new System.Drawing.Size(90, 17);
            this.chbLayoffWager.TabIndex = 100;
            this.chbLayoffWager.Text = "Layoff Wager";
            this.chbLayoffWager.UseVisualStyleBackColor = true;
            // 
            // btnPlaceBet
            // 
            this.btnPlaceBet.Location = new System.Drawing.Point(26, 364);
            this.btnPlaceBet.Name = "btnPlaceBet";
            this.btnPlaceBet.Size = new System.Drawing.Size(134, 23);
            this.btnPlaceBet.TabIndex = 110;
            this.btnPlaceBet.Text = "Place Bet";
            this.btnPlaceBet.UseVisualStyleBackColor = true;
            this.btnPlaceBet.Click += new System.EventHandler(this.btnPlaceBet_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(173, 364);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(137, 23);
            this.btnCancel.TabIndex = 120;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblWagerType
            // 
            this.lblWagerType.Location = new System.Drawing.Point(4, 212);
            this.lblWagerType.Name = "lblWagerType";
            this.lblWagerType.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblWagerType.Size = new System.Drawing.Size(74, 20);
            this.lblWagerType.TabIndex = 1000;
            this.lblWagerType.Text = "Team Under:";
            this.lblWagerType.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblPrice
            // 
            this.lblPrice.Location = new System.Drawing.Point(172, 212);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(48, 16);
            this.lblPrice.TabIndex = 1000;
            this.lblPrice.Text = "Price:";
            this.lblPrice.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtPeriod
            // 
            this.txtPeriod.Enabled = false;
            this.txtPeriod.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPeriod.Location = new System.Drawing.Point(9, 77);
            this.txtPeriod.Name = "txtPeriod";
            this.txtPeriod.ReadOnly = true;
            this.txtPeriod.Size = new System.Drawing.Size(100, 20);
            this.txtPeriod.TabIndex = 1000;
            this.txtPeriod.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnAddToIfBet
            // 
            this.btnAddToIfBet.Location = new System.Drawing.Point(26, 364);
            this.btnAddToIfBet.Name = "btnAddToIfBet";
            this.btnAddToIfBet.Size = new System.Drawing.Size(134, 23);
            this.btnAddToIfBet.TabIndex = 110;
            this.btnAddToIfBet.Text = "Add to If Bet";
            this.btnAddToIfBet.UseVisualStyleBackColor = true;
            this.btnAddToIfBet.Visible = false;
            this.btnAddToIfBet.Click += new System.EventHandler(this.btnAddToIfBet_Click);
            // 
            // btnAddToTeaser
            // 
            this.btnAddToTeaser.Location = new System.Drawing.Point(26, 364);
            this.btnAddToTeaser.Name = "btnAddToTeaser";
            this.btnAddToTeaser.Size = new System.Drawing.Size(134, 23);
            this.btnAddToTeaser.TabIndex = 110;
            this.btnAddToTeaser.Text = "Add to Teaser";
            this.btnAddToTeaser.UseVisualStyleBackColor = true;
            this.btnAddToTeaser.Visible = false;
            this.btnAddToTeaser.Click += new System.EventHandler(this.btnAddToTeaser_Click);
            // 
            // btnAddToParlay
            // 
            this.btnAddToParlay.Location = new System.Drawing.Point(26, 363);
            this.btnAddToParlay.Name = "btnAddToParlay";
            this.btnAddToParlay.Size = new System.Drawing.Size(134, 23);
            this.btnAddToParlay.TabIndex = 110;
            this.btnAddToParlay.Text = "Add to Parlay";
            this.btnAddToParlay.UseVisualStyleBackColor = true;
            this.btnAddToParlay.Visible = false;
            this.btnAddToParlay.Click += new System.EventHandler(this.btnAddToParlay_Click);
            // 
            // panFixedOdds
            // 
            this.panFixedOdds.Controls.Add(this.chbFixedOdds);
            this.panFixedOdds.Location = new System.Drawing.Point(58, 123);
            this.panFixedOdds.Name = "panFixedOdds";
            this.panFixedOdds.Size = new System.Drawing.Size(152, 27);
            this.panFixedOdds.TabIndex = 1001;
            // 
            // chbFixedOdds
            // 
            this.chbFixedOdds.AutoSize = true;
            this.chbFixedOdds.Location = new System.Drawing.Point(15, 5);
            this.chbFixedOdds.Name = "chbFixedOdds";
            this.chbFixedOdds.Size = new System.Drawing.Size(81, 17);
            this.chbFixedOdds.TabIndex = 70;
            this.chbFixedOdds.Text = "Fixed Price:";
            this.chbFixedOdds.UseVisualStyleBackColor = true;
            this.chbFixedOdds.CheckedChanged += new System.EventHandler(this.chbFixedOdds_CheckedChanged);
            // 
            // panPitcher2
            // 
            this.panPitcher2.Controls.Add(this.chbPitcher2MustStart);
            this.panPitcher2.Location = new System.Drawing.Point(57, 178);
            this.panPitcher2.Name = "panPitcher2";
            this.panPitcher2.Size = new System.Drawing.Size(225, 27);
            this.panPitcher2.TabIndex = 1003;
            // 
            // chbPitcher2MustStart
            // 
            this.chbPitcher2MustStart.AutoSize = true;
            this.chbPitcher2MustStart.Checked = true;
            this.chbPitcher2MustStart.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbPitcher2MustStart.Location = new System.Drawing.Point(15, 7);
            this.chbPitcher2MustStart.Name = "chbPitcher2MustStart";
            this.chbPitcher2MustStart.Size = new System.Drawing.Size(100, 17);
            this.chbPitcher2MustStart.TabIndex = 60;
            this.chbPitcher2MustStart.Text = "Who Must Start";
            this.chbPitcher2MustStart.UseVisualStyleBackColor = true;
            // 
            // panPitcher1
            // 
            this.panPitcher1.Controls.Add(this.chbPitcher1MustStart);
            this.panPitcher1.Location = new System.Drawing.Point(57, 149);
            this.panPitcher1.Name = "panPitcher1";
            this.panPitcher1.Size = new System.Drawing.Size(225, 27);
            this.panPitcher1.TabIndex = 1002;
            // 
            // chbPitcher1MustStart
            // 
            this.chbPitcher1MustStart.AutoSize = true;
            this.chbPitcher1MustStart.Checked = true;
            this.chbPitcher1MustStart.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbPitcher1MustStart.Location = new System.Drawing.Point(15, 7);
            this.chbPitcher1MustStart.Name = "chbPitcher1MustStart";
            this.chbPitcher1MustStart.Size = new System.Drawing.Size(100, 17);
            this.chbPitcher1MustStart.TabIndex = 40;
            this.chbPitcher1MustStart.Text = "Who Must Start";
            this.chbPitcher1MustStart.UseVisualStyleBackColor = true;
            // 
            // panTxtLineAdj
            // 
            this.panTxtLineAdj.Location = new System.Drawing.Point(78, 211);
            this.panTxtLineAdj.Name = "panTxtLineAdj";
            this.panTxtLineAdj.Size = new System.Drawing.Size(40, 25);
            this.panTxtLineAdj.TabIndex = 1004;
            // 
            // panTxtPrice
            // 
            this.panTxtPrice.Location = new System.Drawing.Point(221, 211);
            this.panTxtPrice.Name = "panTxtPrice";
            this.panTxtPrice.Size = new System.Drawing.Size(68, 25);
            this.panTxtPrice.TabIndex = 1005;
            // 
            // FrmManualGameWager
            // 
            this.AccessibleDescription = "Manual Game Wager";
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(337, 509);
            this.Controls.Add(this.panTxtPrice);
            this.Controls.Add(this.panTxtLineAdj);
            this.Controls.Add(this.panPitcher2);
            this.Controls.Add(this.panPitcher1);
            this.Controls.Add(this.panFixedOdds);
            this.Controls.Add(this.btnAddToIfBet);
            this.Controls.Add(this.btnAddToTeaser);
            this.Controls.Add(this.btnAddToParlay);
            this.Controls.Add(this.txtPeriod);
            this.Controls.Add(this.lblPrice);
            this.Controls.Add(this.lblWagerType);
            this.Controls.Add(this.btnPlaceBet);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.chbLayoffWager);
            this.Controls.Add(this.txtChosenTeam);
            this.Controls.Add(this.panWagerAmounts);
            this.Controls.Add(this.txtGameInfo);
            this.Controls.Add(this.cmbGoToGamePeriod);
            this.Controls.Add(this.lblGoTo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmManualGameWager";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Manual Game Wager";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmManualGameWager_FormClosing);
            this.Load += new System.EventHandler(this.FrmManualGameWager_Load);
            this.panWagerAmounts.ResumeLayout(false);
            this.grpRiskAmount.ResumeLayout(false);
            this.grpRiskAmount.PerformLayout();
            this.grpWinAmount.ResumeLayout(false);
            this.grpWinAmount.PerformLayout();
            this.panFixedOdds.ResumeLayout(false);
            this.panFixedOdds.PerformLayout();
            this.panPitcher2.ResumeLayout(false);
            this.panPitcher2.PerformLayout();
            this.panPitcher1.ResumeLayout(false);
            this.panPitcher1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbGoToGamePeriod;
        private System.Windows.Forms.Label lblGoTo;
        private System.Windows.Forms.TextBox txtGameInfo;
        private System.Windows.Forms.Panel panWagerAmounts;
        private System.Windows.Forms.GroupBox grpRiskAmount;
        private GUILibraries.Controls.NumberTextBox txtRisk;
        private System.Windows.Forms.GroupBox grpWinAmount;
        private GUILibraries.Controls.NumberTextBox txtToWin;
        private System.Windows.Forms.TextBox txtChosenTeam;
        private System.Windows.Forms.CheckBox chbLayoffWager;
        private System.Windows.Forms.Button btnPlaceBet;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblWagerType;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.TextBox txtPeriod;
        private System.Windows.Forms.Button btnAddToIfBet;
        private System.Windows.Forms.Button btnAddToTeaser;
        private System.Windows.Forms.Button btnAddToParlay;
        private System.Windows.Forms.Panel panFixedOdds;
        private System.Windows.Forms.CheckBox chbFixedOdds;
        private System.Windows.Forms.Panel panPitcher2;
        private System.Windows.Forms.CheckBox chbPitcher2MustStart;
        private System.Windows.Forms.Panel panPitcher1;
        private System.Windows.Forms.CheckBox chbPitcher1MustStart;
        private System.Windows.Forms.Label lblMaxWinDisplay;
        private System.Windows.Forms.Label lblMaxRiskDisplay;
        private System.Windows.Forms.Panel panTxtLineAdj;
        private System.Windows.Forms.Panel panTxtPrice;
    }
}