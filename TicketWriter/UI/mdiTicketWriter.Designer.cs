﻿namespace TicketWriter.UI {
  partial class MdiTicketWriter {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MdiTicketWriter));
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.btnArrangeWindows = new System.Windows.Forms.Button();
            this.btnNewHorseWager = new System.Windows.Forms.Button();
            this.btnNewManualPlay = new System.Windows.Forms.Button();
            this.btnNewIfBet = new System.Windows.Forms.Button();
            this.btnNewTeaser = new System.Windows.Forms.Button();
            this.btnNewParlay = new System.Windows.Forms.Button();
            this.btnPropsAndFutures = new System.Windows.Forms.Button();
            this.btnHangUpPhone = new System.Windows.Forms.Button();
            this.btnAnswerPhone = new System.Windows.Forms.Button();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openCustomerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeCustomerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showTicketInfoAfterClosingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsspCalc = new System.Windows.Forms.ToolStripSeparator();
            this.calculatorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsspExit = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayContestsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ticketInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dailyFiguresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openPlaysToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.accountHistoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.commentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.refreshBalanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshLinesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolbarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusBarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.insertToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.parlayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.teaserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ifBetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manualPlayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.horseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.automaticToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manualLineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.availableBalanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.freePlayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rollingIfBetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.windowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tileAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showGamesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showReadbackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutSIDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panToolbar = new System.Windows.Forms.Panel();
            this.btnShowReadback = new System.Windows.Forms.Button();
            this.txtTeamOrAsterisk = new System.Windows.Forms.TextBox();
            this.lblTeamOrAsterisk = new System.Windows.Forms.Label();
            this.chbRollingIF = new System.Windows.Forms.CheckBox();
            this.chbFreePlay = new System.Windows.Forms.CheckBox();
            this.btnShowCustomerFigures = new System.Windows.Forms.Button();
            this.txtAvailableAmount = new System.Windows.Forms.TextBox();
            this.txtCustomerPIN = new System.Windows.Forms.TextBox();
            this.lblAvail = new System.Windows.Forms.Label();
            this.lblPIN = new System.Windows.Forms.Label();
            this.tsslMain = new System.Windows.Forms.ToolStripStatusLabel();
            this.statBar = new System.Windows.Forms.StatusStrip();
            this.tssLabelReady = new System.Windows.Forms.ToolStripStatusLabel();
            this.tssLabelWagerStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblSearchByRot = new System.Windows.Forms.Label();
            this.menuStrip.SuspendLayout();
            this.panToolbar.SuspendLayout();
            this.statBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnArrangeWindows
            // 
            this.btnArrangeWindows.Enabled = false;
            this.btnArrangeWindows.Image = global::TicketWriter.Properties.Resources.ArrangeWindowsIcon;
            this.btnArrangeWindows.Location = new System.Drawing.Point(733, 0);
            this.btnArrangeWindows.Name = "btnArrangeWindows";
            this.btnArrangeWindows.Size = new System.Drawing.Size(42, 42);
            this.btnArrangeWindows.TabIndex = 17;
            this.btnArrangeWindows.TabStop = false;
            this.toolTip.SetToolTip(this.btnArrangeWindows, "Arrange Windows");
            this.btnArrangeWindows.UseVisualStyleBackColor = true;
            this.btnArrangeWindows.Click += new System.EventHandler(this.btnArrangeWindows_Click);
            this.btnArrangeWindows.MouseEnter += new System.EventHandler(this.btnArrangeWindows_MouseEnter);
            this.btnArrangeWindows.MouseLeave += new System.EventHandler(this.MainToolbar_MouseLeave);
            // 
            // btnNewHorseWager
            // 
            this.btnNewHorseWager.Enabled = false;
            this.btnNewHorseWager.Image = global::TicketWriter.Properties.Resources.NewHorseWagerIcon;
            this.btnNewHorseWager.Location = new System.Drawing.Point(691, 0);
            this.btnNewHorseWager.Name = "btnNewHorseWager";
            this.btnNewHorseWager.Size = new System.Drawing.Size(42, 42);
            this.btnNewHorseWager.TabIndex = 16;
            this.btnNewHorseWager.TabStop = false;
            this.toolTip.SetToolTip(this.btnNewHorseWager, "New Horse Wager");
            this.btnNewHorseWager.UseVisualStyleBackColor = true;
            this.btnNewHorseWager.Click += new System.EventHandler(this.btnNewHorseWager_Click);
            this.btnNewHorseWager.MouseEnter += new System.EventHandler(this.btnNewHorseWager_MouseEnter);
            this.btnNewHorseWager.MouseLeave += new System.EventHandler(this.MainToolbar_MouseLeave);
            // 
            // btnNewManualPlay
            // 
            this.btnNewManualPlay.Enabled = false;
            this.btnNewManualPlay.Image = global::TicketWriter.Properties.Resources.NewManualPlayIcon;
            this.btnNewManualPlay.Location = new System.Drawing.Point(649, 0);
            this.btnNewManualPlay.Name = "btnNewManualPlay";
            this.btnNewManualPlay.Size = new System.Drawing.Size(42, 42);
            this.btnNewManualPlay.TabIndex = 15;
            this.btnNewManualPlay.TabStop = false;
            this.toolTip.SetToolTip(this.btnNewManualPlay, "New Manual Play");
            this.btnNewManualPlay.UseVisualStyleBackColor = true;
            this.btnNewManualPlay.Click += new System.EventHandler(this.btnNewManualPlay_Click);
            this.btnNewManualPlay.MouseEnter += new System.EventHandler(this.btnNewManualPlay_MouseEnter);
            this.btnNewManualPlay.MouseLeave += new System.EventHandler(this.MainToolbar_MouseLeave);
            // 
            // btnNewIfBet
            // 
            this.btnNewIfBet.Enabled = false;
            this.btnNewIfBet.Image = global::TicketWriter.Properties.Resources.NewIfBetIcon;
            this.btnNewIfBet.Location = new System.Drawing.Point(607, 0);
            this.btnNewIfBet.Name = "btnNewIfBet";
            this.btnNewIfBet.Size = new System.Drawing.Size(42, 42);
            this.btnNewIfBet.TabIndex = 14;
            this.btnNewIfBet.TabStop = false;
            this.toolTip.SetToolTip(this.btnNewIfBet, "New If-Bet");
            this.btnNewIfBet.UseVisualStyleBackColor = true;
            this.btnNewIfBet.Click += new System.EventHandler(this.btnNewIfBet_Click);
            this.btnNewIfBet.MouseEnter += new System.EventHandler(this.btnNewIfBet_MouseEnter);
            this.btnNewIfBet.MouseLeave += new System.EventHandler(this.MainToolbar_MouseLeave);
            // 
            // btnNewTeaser
            // 
            this.btnNewTeaser.Enabled = false;
            this.btnNewTeaser.Image = global::TicketWriter.Properties.Resources.NewTeaserIcon;
            this.btnNewTeaser.Location = new System.Drawing.Point(565, 0);
            this.btnNewTeaser.Name = "btnNewTeaser";
            this.btnNewTeaser.Size = new System.Drawing.Size(42, 42);
            this.btnNewTeaser.TabIndex = 13;
            this.btnNewTeaser.TabStop = false;
            this.toolTip.SetToolTip(this.btnNewTeaser, "New Teaser");
            this.btnNewTeaser.UseVisualStyleBackColor = true;
            this.btnNewTeaser.Click += new System.EventHandler(this.btnNewTeaser_Click);
            this.btnNewTeaser.MouseEnter += new System.EventHandler(this.btnNewTeaser_MouseEnter);
            this.btnNewTeaser.MouseLeave += new System.EventHandler(this.MainToolbar_MouseLeave);
            // 
            // btnNewParlay
            // 
            this.btnNewParlay.Enabled = false;
            this.btnNewParlay.Image = global::TicketWriter.Properties.Resources.NewParlayIcon;
            this.btnNewParlay.Location = new System.Drawing.Point(523, 0);
            this.btnNewParlay.Name = "btnNewParlay";
            this.btnNewParlay.Size = new System.Drawing.Size(42, 42);
            this.btnNewParlay.TabIndex = 12;
            this.btnNewParlay.TabStop = false;
            this.toolTip.SetToolTip(this.btnNewParlay, "New Parlay");
            this.btnNewParlay.UseVisualStyleBackColor = true;
            this.btnNewParlay.Click += new System.EventHandler(this.btnNewParlay_Click);
            this.btnNewParlay.MouseEnter += new System.EventHandler(this.btnNewParlay_MouseEnter);
            this.btnNewParlay.MouseLeave += new System.EventHandler(this.MainToolbar_MouseLeave);
            // 
            // btnPropsAndFutures
            // 
            this.btnPropsAndFutures.Enabled = false;
            this.btnPropsAndFutures.Image = global::TicketWriter.Properties.Resources.ShowAvailablePropsAndFuturesIcon;
            this.btnPropsAndFutures.Location = new System.Drawing.Point(481, 0);
            this.btnPropsAndFutures.Name = "btnPropsAndFutures";
            this.btnPropsAndFutures.Size = new System.Drawing.Size(42, 42);
            this.btnPropsAndFutures.TabIndex = 0;
            this.btnPropsAndFutures.TabStop = false;
            this.toolTip.SetToolTip(this.btnPropsAndFutures, "Show Contests");
            this.btnPropsAndFutures.UseVisualStyleBackColor = true;
            this.btnPropsAndFutures.Click += new System.EventHandler(this.btnPropsAndFutures_Click);
            this.btnPropsAndFutures.MouseEnter += new System.EventHandler(this.btnPropsAndFutures_MouseEnter);
            this.btnPropsAndFutures.MouseLeave += new System.EventHandler(this.MainToolbar_MouseLeave);
            // 
            // btnHangUpPhone
            // 
            this.btnHangUpPhone.AutoSize = true;
            this.btnHangUpPhone.Enabled = false;
            this.btnHangUpPhone.Image = global::TicketWriter.Properties.Resources.HangupPhoneIcon;
            this.btnHangUpPhone.Location = new System.Drawing.Point(42, 0);
            this.btnHangUpPhone.Name = "btnHangUpPhone";
            this.btnHangUpPhone.Size = new System.Drawing.Size(42, 42);
            this.btnHangUpPhone.TabIndex = 1;
            this.btnHangUpPhone.TabStop = false;
            this.toolTip.SetToolTip(this.btnHangUpPhone, "Hangup Phone");
            this.btnHangUpPhone.UseVisualStyleBackColor = true;
            this.btnHangUpPhone.Click += new System.EventHandler(this.btnHangUpPhone_Click);
            this.btnHangUpPhone.MouseEnter += new System.EventHandler(this.btnHangUpPhone_MouseEnter);
            this.btnHangUpPhone.MouseLeave += new System.EventHandler(this.MainToolbar_MouseLeave);
            // 
            // btnAnswerPhone
            // 
            this.btnAnswerPhone.Image = global::TicketWriter.Properties.Resources.AnswerPhoneIcon;
            this.btnAnswerPhone.Location = new System.Drawing.Point(0, 0);
            this.btnAnswerPhone.Name = "btnAnswerPhone";
            this.btnAnswerPhone.Size = new System.Drawing.Size(42, 42);
            this.btnAnswerPhone.TabIndex = 0;
            this.btnAnswerPhone.TabStop = false;
            this.toolTip.SetToolTip(this.btnAnswerPhone, "Answer phone");
            this.btnAnswerPhone.UseVisualStyleBackColor = true;
            this.btnAnswerPhone.Click += new System.EventHandler(this.btnAnswerPhone_Click);
            this.btnAnswerPhone.MouseEnter += new System.EventHandler(this.btnAnswerPhone_MouseEnter);
            this.btnAnswerPhone.MouseLeave += new System.EventHandler(this.MainToolbar_MouseLeave);
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.insertToolStripMenuItem,
            this.modeToolStripMenuItem,
            this.windowToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(5, 5);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1243, 24);
            this.menuStrip.TabIndex = 5;
            this.menuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openCustomerToolStripMenuItem,
            this.closeCustomerToolStripMenuItem,
            this.showTicketInfoAfterClosingToolStripMenuItem,
            this.tsspCalc,
            this.calculatorToolStripMenuItem,
            this.tsspExit,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // openCustomerToolStripMenuItem
            // 
            this.openCustomerToolStripMenuItem.Name = "openCustomerToolStripMenuItem";
            this.openCustomerToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.openCustomerToolStripMenuItem.Text = "&Open Customer...";
            this.openCustomerToolStripMenuItem.Click += new System.EventHandler(this.openCustomerToolStripMenuItem_Click);
            // 
            // closeCustomerToolStripMenuItem
            // 
            this.closeCustomerToolStripMenuItem.Name = "closeCustomerToolStripMenuItem";
            this.closeCustomerToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.closeCustomerToolStripMenuItem.Text = "&Close Customer";
            this.closeCustomerToolStripMenuItem.Visible = false;
            this.closeCustomerToolStripMenuItem.Click += new System.EventHandler(this.closeCustomerToolStripMenuItem_Click);
            // 
            // showTicketInfoAfterClosingToolStripMenuItem
            // 
            this.showTicketInfoAfterClosingToolStripMenuItem.Checked = true;
            this.showTicketInfoAfterClosingToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.showTicketInfoAfterClosingToolStripMenuItem.Name = "showTicketInfoAfterClosingToolStripMenuItem";
            this.showTicketInfoAfterClosingToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.showTicketInfoAfterClosingToolStripMenuItem.Text = "Show Ticket Info After Closing Acct";
            this.showTicketInfoAfterClosingToolStripMenuItem.Click += new System.EventHandler(this.showTicketInfoAfterClosingToolStripMenuItem_Click);
            // 
            // tsspCalc
            // 
            this.tsspCalc.Name = "tsspCalc";
            this.tsspCalc.Size = new System.Drawing.Size(258, 6);
            // 
            // calculatorToolStripMenuItem
            // 
            this.calculatorToolStripMenuItem.Name = "calculatorToolStripMenuItem";
            this.calculatorToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F11;
            this.calculatorToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.calculatorToolStripMenuItem.Text = "Calculator";
            this.calculatorToolStripMenuItem.Click += new System.EventHandler(this.calculatorToolStripMenuItem_Click);
            // 
            // tsspExit
            // 
            this.tsspExit.Name = "tsspExit";
            this.tsspExit.Size = new System.Drawing.Size(258, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(261, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.displayContestsToolStripMenuItem,
            this.ticketInfoToolStripMenuItem,
            this.dailyFiguresToolStripMenuItem,
            this.openPlaysToolStripMenuItem,
            this.accountHistoryToolStripMenuItem,
            this.commentsToolStripMenuItem,
            this.toolStripSeparator2,
            this.refreshBalanceToolStripMenuItem,
            this.refreshLinesToolStripMenuItem,
            this.toolStripSeparator1,
            this.toolbarToolStripMenuItem,
            this.statusBarToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "&View";
            // 
            // displayContestsToolStripMenuItem
            // 
            this.displayContestsToolStripMenuItem.CheckOnClick = true;
            this.displayContestsToolStripMenuItem.Name = "displayContestsToolStripMenuItem";
            this.displayContestsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.C)));
            this.displayContestsToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.displayContestsToolStripMenuItem.Text = "Display Contests";
            this.displayContestsToolStripMenuItem.Visible = false;
            this.displayContestsToolStripMenuItem.Click += new System.EventHandler(this.displayContestsToolStripMenuItem_Click);
            // 
            // ticketInfoToolStripMenuItem
            // 
            this.ticketInfoToolStripMenuItem.Name = "ticketInfoToolStripMenuItem";
            this.ticketInfoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.K)));
            this.ticketInfoToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.ticketInfoToolStripMenuItem.Text = "T&icket Info...";
            this.ticketInfoToolStripMenuItem.Visible = false;
            this.ticketInfoToolStripMenuItem.Click += new System.EventHandler(this.ticketInfoToolStripMenuItem_Click);
            // 
            // dailyFiguresToolStripMenuItem
            // 
            this.dailyFiguresToolStripMenuItem.Name = "dailyFiguresToolStripMenuItem";
            this.dailyFiguresToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.dailyFiguresToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.dailyFiguresToolStripMenuItem.Text = "Daily Figures";
            this.dailyFiguresToolStripMenuItem.Visible = false;
            this.dailyFiguresToolStripMenuItem.Click += new System.EventHandler(this.dailyFiguresToolStripMenuItem_Click);
            // 
            // openPlaysToolStripMenuItem
            // 
            this.openPlaysToolStripMenuItem.Name = "openPlaysToolStripMenuItem";
            this.openPlaysToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openPlaysToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.openPlaysToolStripMenuItem.Text = "&Open Plays...";
            this.openPlaysToolStripMenuItem.Visible = false;
            this.openPlaysToolStripMenuItem.Click += new System.EventHandler(this.openPlaysToolStripMenuItem_Click);
            // 
            // accountHistoryToolStripMenuItem
            // 
            this.accountHistoryToolStripMenuItem.Name = "accountHistoryToolStripMenuItem";
            this.accountHistoryToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.accountHistoryToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.accountHistoryToolStripMenuItem.Text = "Account History";
            this.accountHistoryToolStripMenuItem.Visible = false;
            this.accountHistoryToolStripMenuItem.Click += new System.EventHandler(this.accountHistoryToolStripMenuItem_Click);
            // 
            // commentsToolStripMenuItem
            // 
            this.commentsToolStripMenuItem.Name = "commentsToolStripMenuItem";
            this.commentsToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F4;
            this.commentsToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.commentsToolStripMenuItem.Text = "Comments";
            this.commentsToolStripMenuItem.Visible = false;
            this.commentsToolStripMenuItem.Click += new System.EventHandler(this.commentsToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(232, 6);
            this.toolStripSeparator2.Visible = false;
            // 
            // refreshBalanceToolStripMenuItem
            // 
            this.refreshBalanceToolStripMenuItem.Name = "refreshBalanceToolStripMenuItem";
            this.refreshBalanceToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.refreshBalanceToolStripMenuItem.Text = "Refresh Balance";
            this.refreshBalanceToolStripMenuItem.Visible = false;
            this.refreshBalanceToolStripMenuItem.Click += new System.EventHandler(this.refreshBalanceToolStripMenuItem_Click);
            // 
            // refreshLinesToolStripMenuItem
            // 
            this.refreshLinesToolStripMenuItem.Name = "refreshLinesToolStripMenuItem";
            this.refreshLinesToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.refreshLinesToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.refreshLinesToolStripMenuItem.Text = "Refresh Lines";
            this.refreshLinesToolStripMenuItem.Visible = false;
            this.refreshLinesToolStripMenuItem.Click += new System.EventHandler(this.refreshLinesToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(232, 6);
            this.toolStripSeparator1.Visible = false;
            // 
            // toolbarToolStripMenuItem
            // 
            this.toolbarToolStripMenuItem.Checked = true;
            this.toolbarToolStripMenuItem.CheckOnClick = true;
            this.toolbarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolbarToolStripMenuItem.Name = "toolbarToolStripMenuItem";
            this.toolbarToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.toolbarToolStripMenuItem.Text = "&Toolbar";
            this.toolbarToolStripMenuItem.Click += new System.EventHandler(this.toolbarToolStripMenuItem_Click);
            // 
            // statusBarToolStripMenuItem
            // 
            this.statusBarToolStripMenuItem.Checked = true;
            this.statusBarToolStripMenuItem.CheckOnClick = true;
            this.statusBarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.statusBarToolStripMenuItem.Name = "statusBarToolStripMenuItem";
            this.statusBarToolStripMenuItem.Size = new System.Drawing.Size(235, 22);
            this.statusBarToolStripMenuItem.Text = "&Status Bar";
            this.statusBarToolStripMenuItem.Click += new System.EventHandler(this.statusBarToolStripMenuItem_Click);
            // 
            // insertToolStripMenuItem
            // 
            this.insertToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.parlayToolStripMenuItem,
            this.teaserToolStripMenuItem,
            this.ifBetToolStripMenuItem,
            this.manualPlayToolStripMenuItem,
            this.horseToolStripMenuItem});
            this.insertToolStripMenuItem.Name = "insertToolStripMenuItem";
            this.insertToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.insertToolStripMenuItem.Text = "&Insert";
            this.insertToolStripMenuItem.Visible = false;
            // 
            // parlayToolStripMenuItem
            // 
            this.parlayToolStripMenuItem.Name = "parlayToolStripMenuItem";
            this.parlayToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.parlayToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.parlayToolStripMenuItem.Text = "&Parlay";
            this.parlayToolStripMenuItem.Click += new System.EventHandler(this.parlayToolStripMenuItem_Click);
            // 
            // teaserToolStripMenuItem
            // 
            this.teaserToolStripMenuItem.Name = "teaserToolStripMenuItem";
            this.teaserToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this.teaserToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.teaserToolStripMenuItem.Text = "&Teaser";
            this.teaserToolStripMenuItem.Click += new System.EventHandler(this.teaserToolStripMenuItem_Click);
            // 
            // ifBetToolStripMenuItem
            // 
            this.ifBetToolStripMenuItem.Name = "ifBetToolStripMenuItem";
            this.ifBetToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I)));
            this.ifBetToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.ifBetToolStripMenuItem.Text = "&If Bet";
            this.ifBetToolStripMenuItem.Click += new System.EventHandler(this.ifBetToolStripMenuItem_Click);
            // 
            // manualPlayToolStripMenuItem
            // 
            this.manualPlayToolStripMenuItem.Name = "manualPlayToolStripMenuItem";
            this.manualPlayToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.M)));
            this.manualPlayToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.manualPlayToolStripMenuItem.Text = "&Manual Play";
            this.manualPlayToolStripMenuItem.Click += new System.EventHandler(this.manualPlayToolStripMenuItem_Click);
            // 
            // horseToolStripMenuItem
            // 
            this.horseToolStripMenuItem.Name = "horseToolStripMenuItem";
            this.horseToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H)));
            this.horseToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.horseToolStripMenuItem.Text = "&Horse";
            this.horseToolStripMenuItem.Click += new System.EventHandler(this.horseToolStripMenuItem_Click);
            // 
            // modeToolStripMenuItem
            // 
            this.modeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.automaticToolStripMenuItem,
            this.manualLineToolStripMenuItem,
            this.toolStripSeparator3,
            this.availableBalanceToolStripMenuItem,
            this.freePlayToolStripMenuItem,
            this.rollingIfBetToolStripMenuItem});
            this.modeToolStripMenuItem.Name = "modeToolStripMenuItem";
            this.modeToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.modeToolStripMenuItem.Text = "&Mode";
            this.modeToolStripMenuItem.Visible = false;
            // 
            // automaticToolStripMenuItem
            // 
            this.automaticToolStripMenuItem.Checked = true;
            this.automaticToolStripMenuItem.CheckOnClick = true;
            this.automaticToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.automaticToolStripMenuItem.Name = "automaticToolStripMenuItem";
            this.automaticToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.automaticToolStripMenuItem.Text = "Automatic";
            this.automaticToolStripMenuItem.Click += new System.EventHandler(this.automaticToolStripMenuItem_Click);
            // 
            // manualLineToolStripMenuItem
            // 
            this.manualLineToolStripMenuItem.CheckOnClick = true;
            this.manualLineToolStripMenuItem.Name = "manualLineToolStripMenuItem";
            this.manualLineToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.manualLineToolStripMenuItem.Text = "Manual Line";
            this.manualLineToolStripMenuItem.Click += new System.EventHandler(this.manualLineToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(163, 6);
            // 
            // availableBalanceToolStripMenuItem
            // 
            this.availableBalanceToolStripMenuItem.Checked = true;
            this.availableBalanceToolStripMenuItem.CheckOnClick = true;
            this.availableBalanceToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.availableBalanceToolStripMenuItem.Name = "availableBalanceToolStripMenuItem";
            this.availableBalanceToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.availableBalanceToolStripMenuItem.Text = "Available Balance";
            this.availableBalanceToolStripMenuItem.Click += new System.EventHandler(this.availableBalanceToolStripMenuItem_Click);
            // 
            // freePlayToolStripMenuItem
            // 
            this.freePlayToolStripMenuItem.CheckOnClick = true;
            this.freePlayToolStripMenuItem.Name = "freePlayToolStripMenuItem";
            this.freePlayToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.freePlayToolStripMenuItem.Text = "Free Play";
            this.freePlayToolStripMenuItem.Click += new System.EventHandler(this.freePlayToolStripMenuItem_Click);
            // 
            // rollingIfBetToolStripMenuItem
            // 
            this.rollingIfBetToolStripMenuItem.CheckOnClick = true;
            this.rollingIfBetToolStripMenuItem.Name = "rollingIfBetToolStripMenuItem";
            this.rollingIfBetToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.rollingIfBetToolStripMenuItem.Text = "Rolling If Bet";
            this.rollingIfBetToolStripMenuItem.Click += new System.EventHandler(this.rollingIfBetToolStripMenuItem_Click);
            // 
            // windowToolStripMenuItem
            // 
            this.windowToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tileAllToolStripMenuItem,
            this.showGamesToolStripMenuItem,
            this.showReadbackToolStripMenuItem});
            this.windowToolStripMenuItem.Name = "windowToolStripMenuItem";
            this.windowToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.windowToolStripMenuItem.Text = "&Window";
            this.windowToolStripMenuItem.Visible = false;
            // 
            // tileAllToolStripMenuItem
            // 
            this.tileAllToolStripMenuItem.Name = "tileAllToolStripMenuItem";
            this.tileAllToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.tileAllToolStripMenuItem.Text = "&Tile All";
            this.tileAllToolStripMenuItem.Click += new System.EventHandler(this.tileAllToolStripMenuItem_Click);
            // 
            // showGamesToolStripMenuItem
            // 
            this.showGamesToolStripMenuItem.Name = "showGamesToolStripMenuItem";
            this.showGamesToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.showGamesToolStripMenuItem.Text = "Show &Games";
            // 
            // showReadbackToolStripMenuItem
            // 
            this.showReadbackToolStripMenuItem.Name = "showReadbackToolStripMenuItem";
            this.showReadbackToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.showReadbackToolStripMenuItem.Text = "Show &Readback";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutSIDToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutSIDToolStripMenuItem
            // 
            this.aboutSIDToolStripMenuItem.Name = "aboutSIDToolStripMenuItem";
            this.aboutSIDToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.aboutSIDToolStripMenuItem.Text = "&About SID";
            this.aboutSIDToolStripMenuItem.Click += new System.EventHandler(this.aboutSIDToolStripMenuItem_Click);
            // 
            // panToolbar
            // 
            this.panToolbar.Controls.Add(this.btnShowReadback);
            this.panToolbar.Controls.Add(this.btnArrangeWindows);
            this.panToolbar.Controls.Add(this.btnNewHorseWager);
            this.panToolbar.Controls.Add(this.btnNewManualPlay);
            this.panToolbar.Controls.Add(this.btnNewIfBet);
            this.panToolbar.Controls.Add(this.btnNewTeaser);
            this.panToolbar.Controls.Add(this.btnNewParlay);
            this.panToolbar.Controls.Add(this.txtTeamOrAsterisk);
            this.panToolbar.Controls.Add(this.lblTeamOrAsterisk);
            this.panToolbar.Controls.Add(this.chbRollingIF);
            this.panToolbar.Controls.Add(this.chbFreePlay);
            this.panToolbar.Controls.Add(this.btnShowCustomerFigures);
            this.panToolbar.Controls.Add(this.txtAvailableAmount);
            this.panToolbar.Controls.Add(this.txtCustomerPIN);
            this.panToolbar.Controls.Add(this.lblAvail);
            this.panToolbar.Controls.Add(this.lblPIN);
            this.panToolbar.Controls.Add(this.btnPropsAndFutures);
            this.panToolbar.Controls.Add(this.btnHangUpPhone);
            this.panToolbar.Controls.Add(this.btnAnswerPhone);
            this.panToolbar.Dock = System.Windows.Forms.DockStyle.Top;
            this.panToolbar.Location = new System.Drawing.Point(5, 29);
            this.panToolbar.Name = "panToolbar";
            this.panToolbar.Size = new System.Drawing.Size(1243, 45);
            this.panToolbar.TabIndex = 6;
            // 
            // btnShowReadback
            // 
            this.btnShowReadback.Enabled = false;
            this.btnShowReadback.Image = global::TicketWriter.Properties.Resources.ShowReadbackIcon;
            this.btnShowReadback.Location = new System.Drawing.Point(775, 0);
            this.btnShowReadback.Name = "btnShowReadback";
            this.btnShowReadback.Size = new System.Drawing.Size(42, 42);
            this.btnShowReadback.TabIndex = 18;
            this.btnShowReadback.TabStop = false;
            this.btnShowReadback.UseVisualStyleBackColor = true;
            this.btnShowReadback.Click += new System.EventHandler(this.btnShowReadback_Click);
            this.btnShowReadback.MouseEnter += new System.EventHandler(this.btnShowReadback_MouseEnter);
            this.btnShowReadback.MouseLeave += new System.EventHandler(this.MainToolbar_MouseLeave);
            // 
            // txtTeamOrAsterisk
            // 
            this.txtTeamOrAsterisk.AcceptsTab = true;
            this.txtTeamOrAsterisk.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTeamOrAsterisk.Location = new System.Drawing.Point(408, 21);
            this.txtTeamOrAsterisk.Multiline = true;
            this.txtTeamOrAsterisk.Name = "txtTeamOrAsterisk";
            this.txtTeamOrAsterisk.Size = new System.Drawing.Size(64, 20);
            this.txtTeamOrAsterisk.TabIndex = 1;
            this.txtTeamOrAsterisk.Visible = false;
            this.txtTeamOrAsterisk.TextChanged += new System.EventHandler(this.txtTeamOrAsterisk_TextChanged);
            this.txtTeamOrAsterisk.Enter += new System.EventHandler(this.txtTeamOrAsterisk_Enter);
            this.txtTeamOrAsterisk.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTeamOrAsterisk_KeyDown);
            this.txtTeamOrAsterisk.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTeamOrAsterisk_KeyPress);
            this.txtTeamOrAsterisk.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtTeamOrAsterisk_KeyUp);
            this.txtTeamOrAsterisk.Leave += new System.EventHandler(this.txtTeamOrAsterisk_Leave);
            // 
            // lblTeamOrAsterisk
            // 
            this.lblTeamOrAsterisk.AutoSize = true;
            this.lblTeamOrAsterisk.Location = new System.Drawing.Point(414, 2);
            this.lblTeamOrAsterisk.Name = "lblTeamOrAsterisk";
            this.lblTeamOrAsterisk.Size = new System.Drawing.Size(53, 13);
            this.lblTeamOrAsterisk.TabIndex = 10;
            this.lblTeamOrAsterisk.Text = "Team or *";
            this.lblTeamOrAsterisk.Visible = false;
            // 
            // chbRollingIF
            // 
            this.chbRollingIF.AutoSize = true;
            this.chbRollingIF.Location = new System.Drawing.Point(331, 20);
            this.chbRollingIF.Name = "chbRollingIF";
            this.chbRollingIF.Size = new System.Drawing.Size(70, 17);
            this.chbRollingIF.TabIndex = 9;
            this.chbRollingIF.TabStop = false;
            this.chbRollingIF.Text = "Rolling IF";
            this.chbRollingIF.UseVisualStyleBackColor = true;
            this.chbRollingIF.Visible = false;
            this.chbRollingIF.CheckedChanged += new System.EventHandler(this.chbRollingIF_CheckedChanged);
            // 
            // chbFreePlay
            // 
            this.chbFreePlay.AutoSize = true;
            this.chbFreePlay.Location = new System.Drawing.Point(331, 1);
            this.chbFreePlay.Name = "chbFreePlay";
            this.chbFreePlay.Size = new System.Drawing.Size(70, 17);
            this.chbFreePlay.TabIndex = 8;
            this.chbFreePlay.TabStop = false;
            this.chbFreePlay.Text = "Free Play";
            this.chbFreePlay.UseVisualStyleBackColor = true;
            this.chbFreePlay.Visible = false;
            this.chbFreePlay.CheckedChanged += new System.EventHandler(this.chbFreePlay_CheckedChanged);
            // 
            // btnShowCustomerFigures
            // 
            this.btnShowCustomerFigures.Location = new System.Drawing.Point(301, 20);
            this.btnShowCustomerFigures.Name = "btnShowCustomerFigures";
            this.btnShowCustomerFigures.Size = new System.Drawing.Size(23, 23);
            this.btnShowCustomerFigures.TabIndex = 7;
            this.btnShowCustomerFigures.TabStop = false;
            this.btnShowCustomerFigures.Text = "?";
            this.btnShowCustomerFigures.UseVisualStyleBackColor = true;
            this.btnShowCustomerFigures.Visible = false;
            this.btnShowCustomerFigures.Click += new System.EventHandler(this.btnShowCustomerFigures_Click);
            // 
            // txtAvailableAmount
            // 
            this.txtAvailableAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAvailableAmount.Location = new System.Drawing.Point(117, 22);
            this.txtAvailableAmount.Name = "txtAvailableAmount";
            this.txtAvailableAmount.ReadOnly = true;
            this.txtAvailableAmount.Size = new System.Drawing.Size(178, 20);
            this.txtAvailableAmount.TabIndex = 6;
            this.txtAvailableAmount.TabStop = false;
            this.txtAvailableAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtAvailableAmount.Visible = false;
            // 
            // txtCustomerPIN
            // 
            this.txtCustomerPIN.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCustomerPIN.Location = new System.Drawing.Point(117, 2);
            this.txtCustomerPIN.Name = "txtCustomerPIN";
            this.txtCustomerPIN.ReadOnly = true;
            this.txtCustomerPIN.Size = new System.Drawing.Size(178, 20);
            this.txtCustomerPIN.TabIndex = 5;
            this.txtCustomerPIN.TabStop = false;
            this.txtCustomerPIN.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCustomerPIN.Visible = false;
            // 
            // lblAvail
            // 
            this.lblAvail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAvail.Location = new System.Drawing.Point(84, 24);
            this.lblAvail.Name = "lblAvail";
            this.lblAvail.Size = new System.Drawing.Size(32, 16);
            this.lblAvail.TabIndex = 4;
            this.lblAvail.Text = "Fig:";
            this.lblAvail.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblAvail.Visible = false;
            // 
            // lblPIN
            // 
            this.lblPIN.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPIN.Location = new System.Drawing.Point(85, 4);
            this.lblPIN.Name = "lblPIN";
            this.lblPIN.Size = new System.Drawing.Size(32, 16);
            this.lblPIN.TabIndex = 3;
            this.lblPIN.Text = "PIN:";
            this.lblPIN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblPIN.Visible = false;
            // 
            // tsslMain
            // 
            this.tsslMain.Name = "tsslMain";
            this.tsslMain.Size = new System.Drawing.Size(0, 20);
            // 
            // statBar
            // 
            this.statBar.AutoSize = false;
            this.statBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsslMain,
            this.tssLabelReady,
            this.tssLabelWagerStatus});
            this.statBar.Location = new System.Drawing.Point(5, 672);
            this.statBar.Name = "statBar";
            this.statBar.Size = new System.Drawing.Size(1243, 25);
            this.statBar.TabIndex = 2;
            this.statBar.Text = "*** AVAILABLE BALANCE Mode / AUTOMATIC LINE mode ***";
            // 
            // tssLabelReady
            // 
            this.tssLabelReady.AutoSize = false;
            this.tssLabelReady.Name = "tssLabelReady";
            this.tssLabelReady.Size = new System.Drawing.Size(50, 20);
            this.tssLabelReady.Text = "Ready";
            this.tssLabelReady.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tssLabelWagerStatus
            // 
            this.tssLabelWagerStatus.Name = "tssLabelWagerStatus";
            this.tssLabelWagerStatus.Size = new System.Drawing.Size(0, 20);
            // 
            // lblSearchByRot
            // 
            this.lblSearchByRot.AutoSize = true;
            this.lblSearchByRot.Location = new System.Drawing.Point(416, 15);
            this.lblSearchByRot.Name = "lblSearchByRot";
            this.lblSearchByRot.Size = new System.Drawing.Size(0, 13);
            this.lblSearchByRot.TabIndex = 8;
            // 
            // MdiTicketWriter
            // 
            this.AcceptButton = this.btnAnswerPhone;
            this.AccessibleDescription = "isbtw";
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1253, 702);
            this.Controls.Add(this.lblSearchByRot);
            this.Controls.Add(this.panToolbar);
            this.Controls.Add(this.menuStrip);
            this.Controls.Add(this.statBar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.KeyPreview = true;
            this.Name = "MdiTicketWriter";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MdiTicketWriter_FormClosing);
            this.Load += new System.EventHandler(this.mdiTicketWriter_Load);
            this.LocationChanged += new System.EventHandler(this.mdiTicketWriter_LocationChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MdiTicketWriter_KeyDown);
            this.Resize += new System.EventHandler(this.mdiTicketWriter_Resize);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.panToolbar.ResumeLayout(false);
            this.panToolbar.PerformLayout();
            this.statBar.ResumeLayout(false);
            this.statBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

    }
    #endregion

    private System.Windows.Forms.ToolTip toolTip;
    private System.Windows.Forms.MenuStrip menuStrip;
    private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem openCustomerToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator tsspCalc;
    private System.Windows.Forms.ToolStripMenuItem calculatorToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator tsspExit;
    private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem toolbarToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem statusBarToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem aboutSIDToolStripMenuItem;
    private System.Windows.Forms.Panel panToolbar;
    private System.Windows.Forms.Button btnShowReadback;
    private System.Windows.Forms.Button btnArrangeWindows;
    private System.Windows.Forms.Button btnNewHorseWager;
    private System.Windows.Forms.Button btnNewManualPlay;
    private System.Windows.Forms.Button btnNewIfBet;
    private System.Windows.Forms.Button btnNewTeaser;
    private System.Windows.Forms.Button btnNewParlay;
    private System.Windows.Forms.TextBox txtTeamOrAsterisk;
    private System.Windows.Forms.Label lblTeamOrAsterisk;
    private System.Windows.Forms.CheckBox chbRollingIF;
    private System.Windows.Forms.CheckBox chbFreePlay;
    private System.Windows.Forms.Button btnShowCustomerFigures;
    private System.Windows.Forms.TextBox txtAvailableAmount;
    private System.Windows.Forms.TextBox txtCustomerPIN;
    private System.Windows.Forms.Label lblAvail;
    private System.Windows.Forms.Label lblPIN;
    private System.Windows.Forms.Button btnPropsAndFutures;
    private System.Windows.Forms.Button btnHangUpPhone;
    private System.Windows.Forms.Button btnAnswerPhone;
    private System.Windows.Forms.ToolStripMenuItem closeCustomerToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem insertToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem parlayToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem teaserToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem ifBetToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem manualPlayToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem horseToolStripMenuItem;
    public System.Windows.Forms.ToolStripMenuItem displayContestsToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem ticketInfoToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem dailyFiguresToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem openPlaysToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem accountHistoryToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem commentsToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    private System.Windows.Forms.ToolStripMenuItem refreshLinesToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem refreshBalanceToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    private System.Windows.Forms.ToolStripMenuItem modeToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem automaticToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem manualLineToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
    private System.Windows.Forms.ToolStripMenuItem availableBalanceToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem freePlayToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem rollingIfBetToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem windowToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem tileAllToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem showGamesToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem showReadbackToolStripMenuItem;
    private System.Windows.Forms.ToolStripStatusLabel tsslMain;
    private System.Windows.Forms.StatusStrip statBar;
    private System.Windows.Forms.ToolStripStatusLabel tssLabelReady;
    private System.Windows.Forms.ToolStripStatusLabel tssLabelWagerStatus;
    private System.Windows.Forms.Label lblSearchByRot;
    private System.Windows.Forms.ToolStripMenuItem showTicketInfoAfterClosingToolStripMenuItem;
  }
}



