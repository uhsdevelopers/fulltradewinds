﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using GUILibraries.Forms;
using GUILibraries.BusinessLayer;
using GUILibraries.Utilities;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using SIDLibraries.Utilities;
using TicketWriter.Properties;
using System.Threading;
using Common = TicketWriter.Utilities.Common;
using TicketWriter.Extensions;
using System.Configuration;

namespace TicketWriter.UI {
  public sealed partial class MdiTicketWriter : SIDModuleForm {

    #region public Classes
    #endregion

    #region private Constants

    #endregion

    #region Public Properties

    public List<IfBet> ActionReverseIfBets {
      get { return _actionReverseIfBets ?? (_actionReverseIfBets = new List<IfBet>()); }
      set {
        _actionReverseIfBets = value;
      }
    }

    public CustomerBalanceInfo BalanceInfo { get; set; }

    public List<IfBet> BirdCageIfBets {
      get { return _birdCageIfBets ?? (_birdCageIfBets = new List<IfBet>()); }
      set {
        _birdCageIfBets = value;
      }
    }

    private DateTime ClientDateTime {
      get {
        return DateTime.Now;
      }
    }

    public List<spULPGetCurrentUserPermissions_Result> CurrentUserPermissions { get; set; }

    public List<spCstGetRestrictionsByCustomer_Result> CustomerRestrictions { get; set; }

    public spCstGetCustomer_Result Customer { get; set; }

    public List<spCstGetCustomerTeaserSpecs_Result> CustomerAvailableTeasers { get; private set; }

    public spCstGetCustomerStoreProfile_Result CustomerStore {
      get {
        if (CustomerStoreProfile != null) return CustomerStoreProfile;
        using (var cstSvc = new Customers(AppModuleInfo)) {
          if (Customer != null && Customer.CustomerID != null) {
            CustomerStoreProfile = cstSvc.GetCustomerStoreProfile(Customer.CustomerID.Trim());
          }
        }
        return CustomerStoreProfile;
      }
    }

    private Boolean CustomerWasClosed { get; set; }

    public spCstGetCustomer_Result LastCustomer { get; private set; }

    public LayoutManager LayoutManager { get; private set; }

    public List<spCstGetCustomerWagerLimits_Result> CustomerWagerLimits {
      get {
        if (_customerWagerLimits != null) return _customerWagerLimits;
        using (var cs = new Customers(AppModuleInfo)) {
          _customerWagerLimits = cs.GetCustomerWagerLimits(Customer.CustomerID).ToList();
        }
        return _customerWagerLimits;
      }
    }

    public IEnumerable<spCstGetStoreWagerLimits_Result> StoreWagerLimits {
      get {
        if (_storeWagerLimits != null) return _storeWagerLimits;
        using (var cs = new Customers(AppModuleInfo)) {
          _storeWagerLimits = cs.GetStoreWagerLimits(CustomerStore.Store);
        }
        return _storeWagerLimits;
      }
    }

    public List<spCstGetVigDiscount_Result> VigDiscountLimits {
      get {
        if (_vigDiscountLimits != null) return _vigDiscountLimits;
        using (var cs = new Customers(AppModuleInfo)) {
          _vigDiscountLimits = cs.GetCustomerVigDiscountLimits(Customer.CustomerID);
        }
        return _vigDiscountLimits;
      }
    }

    public Boolean LayoffWagersEnabled {
      get {
        if (_layoffWagersEnabled != null) return _layoffWagersEnabled == "True";
        _layoffWagersEnabled = UserCanCreateLayoffWagers();
        return _layoffWagersEnabled == "True";
      }
    }

    public spULPGetLoginInUse_Result LoginInUse { get; set; }

    //public LogWriter Log { get; set; }

    public FormParameterList ParameterList { get; set; }

    public List<spLOGetParlayPayCard_Result> ParlayPayCard { get; private set; }

    public String ParlayPayoutType {
      get {
        if (string.IsNullOrEmpty(_parlayPayoutType)) {
          _parlayPayoutType = Params.ParlayPayoutType;
        }
        return _parlayPayoutType;
      }
    }

    private List<spTkWGetCustomerWagerOutcome_Result> RifBBackwardTicketInfo { get; set; }

    public String RIfBetFromReadback { get; set; }

    public RifBackwardTicketInfo.RifbTicketInfo RifBTicketInfo { get; set; }

    public Boolean RollingIfBetMode { get; set; }

    public spTkWGetCustomerWagerDetails_Result OpenPlayWagerInfo { get; set; }

    public List<WagerItem.ItemDescription> OpenPlayItemsDescription { get; set; }

    public List<spTkWGetWagerItems_Result> PendingPlayItemsInDb { get; set; }

    public spTkWGetWagerManualPlayItem_Result PendingManualPlayItemInDb { get; set; }

    public spTkWGetContestWagerItem_Result PendingContestItemInDb { get; set; }

    public spTkWGetCustomerWager_Result PendingWagerInDb { get; set; }

    private Boolean ShowingCorrelatedGames { get; set; }

    public Boolean ShowingLatestLines { get; private set; }

    public Boolean ShowingLikeTeams { get; private set; }

    public Boolean SoundCardDetected {
      get {
        if (_soundCardDetected != null) return _soundCardDetected == "True";
        _soundCardDetected = SoundCard.Detected().ToString();
        return _soundCardDetected == "True";
      }
    }

    public LineOffering.Teaser TeaserInfo { get; private set; }

    public List<spLOGetTeaserPayCard_Result> TeasersPayCard { get; private set; }

    public List<spLOGetTeaserSpecs_Result> TeasersSpecs { get; private set; }

    public List<spLOGetTeaserSportSpec_Result> TeasersSportSpecs { get; private set; }

    public int TicketNumber { get; set; }

    public DateTime WageringStartDate { get; set; }

    public Ticket Ticket { get; set; }

    // ReSharper disable once MemberCanBePrivate.Global
    public String WageringMode { get; set; } //Automatic (A) or Manual (M), when form loads it defaults to Automatic

    public String WageringPanelName { private get; set; }

    private FrmSportAndGameSelection FrmSportAndGameSelection {
      get {
        return _frmSportAndGameSelection ??
               (_frmSportAndGameSelection = (FrmSportAndGameSelection)FormF.GetFormByName("frmSportAndGameSelection", this));
      }
    }

    public Boolean ShowPropsInSportsTreeView { get; private set; }

    #endregion

    #region Private Properties

    private MdiClient MdiClient { get; set; }

    private spCstGetCustomerStoreProfile_Result CustomerStoreProfile { get; set; }

    private TimeSpan ClientServerTimeDiff {
      get {
        if (_clientServerTimeDiff != null) return (TimeSpan)_clientServerTimeDiff;
        _clientServerTimeDiff = ClientDateTime - ServerDateTime;
        return (TimeSpan)_clientServerTimeDiff;
      }
    }

    private bool ShowTicketInfoAfterClosing { get; set; }

    #endregion

    #region Public Vars

    #endregion

    #region Private Vars

    private List<IfBet> _actionReverseIfBets;
    private List<IfBet> _birdCageIfBets;
    private PropertyInfo _childFrmProperty;
    private TimeSpan? _clientServerTimeDiff;
    private List<spCstGetCustomerWagerLimits_Result> _customerWagerLimits;
    TicketDr _dr;
    private String _layoffWagersEnabled;
    private String _parlayPayoutType;
    //private List<Parlay> _roundRobinParlays;
    //private List<Teaser> _roundRobinTeasers;
    private String _soundCardDetected;
    private List<spCstGetStoreWagerLimits_Result> _storeWagerLimits;
    private List<spCstGetVigDiscount_Result> _vigDiscountLimits;
    private FrmSportAndGameSelection _frmSportAndGameSelection;

    #endregion

    #region Constructors

    public MdiTicketWriter(ModuleInfo moduleInfo)
      : base(moduleInfo) {
      AppModuleInfo.MaintAction = SetModuleInMaintenance;
      InitializeComponent();
    }

    #endregion

    #region Public Methods

    private void ActivateChild(string formName) {
      var childForm = MdiChildren.FirstOrDefault(f => f.Name.ToUpper() == formName.ToUpper());

      if (childForm != null) {
        childForm.Select();
      }
    }

    public bool ActivateMdiChildForm(Keys keyData, bool baseResult, string activeMdiChild) {
      if (keyData == (Keys.Control | Keys.Tab)) {
        ActivateChild(activeMdiChild ?? "FrmSportAndGameSelection");
        return true;
      }
      return baseResult;
    }

    public bool ActivateWageringForm(string activeMdiChild, bool baseResult = true) {
      if (OwnedForms.Length < 2) {
        return baseResult;
      }
      var targetWageringForm = GetWageringForm();

      if (targetWageringForm == null) {
        return baseResult;
      }
      targetWageringForm.Select();

      switch (targetWageringForm.Name) {
        case "FrmPlaceIfBet":
          ((FrmPlaceIfBet)targetWageringForm).ActiveMdiChildName = activeMdiChild;
          ((FrmPlaceIfBet)targetWageringForm).SetCursorInActionReverseCheckBox();
          break;
        case "FrmPlaceParlay":
          ((FrmPlaceParlay)targetWageringForm).ActiveMdiChildName = activeMdiChild;
          ((FrmPlaceParlay)targetWageringForm).SetCursorInTeamNumberBox();
          break;
        case "FrmPlaceTeaser":
          ((FrmPlaceTeaser)targetWageringForm).ActiveMdiChildName = activeMdiChild;
          ((FrmPlaceTeaser)targetWageringForm).SetCursorInTeaserCombo();
          break;
      }
      return true;
    }

    public void DeleteSpecialCompoundWager(string wagerType, int gvTicketNumber, int gvWagerNumber) {
      switch (wagerType) {
        case WagerType.IFBET:
          if (ActionReverseIfBets != null) {
            ActionReverseIfBets.RemoveAll(s => s.TicketNumber == gvTicketNumber && s.ArLink == gvWagerNumber);
          }

          if (BirdCageIfBets != null) {
            BirdCageIfBets.RemoveAll(s => s.TicketNumber == gvTicketNumber && s.ArBirdcageLink == gvWagerNumber);
          }
          break;
        case WagerType.PARLAY:
          if (Ticket.RoundRobinParlays != null) {
            Ticket.RoundRobinParlays.RemoveAll(s => s.TicketNumber == gvTicketNumber && s.RoundRobinLink == gvWagerNumber);
          }
          break;
        case WagerType.TEASER:
          if (Ticket.RoundRobinTeasers != null) {
            Ticket.RoundRobinTeasers.RemoveAll(s => s.TicketNumber == gvTicketNumber && s.RoundRobinLink == gvWagerNumber);
          }
          break;
      }
    }

    // ReSharper disable UnusedMember.Global
    public void DisplayOpenBetWageringForm()
      // ReSharper restore UnusedMember.Global
    {
      if (OpenPlayWagerInfo == null) return;
      switch (OpenPlayWagerInfo.WagerType) {
        case "Parlay":
          DisplayWageringForm(new FrmPlaceParlay(AppModuleInfo), OpenPlayWagerInfo.WagerType, OpenPlayWagerInfo.TicketNumber.ToString(CultureInfo.InvariantCulture), OpenPlayWagerInfo.WagerNumber.ToString(CultureInfo.InvariantCulture));
          break;
        case "Teaser":
          DisplayWageringForm(new FrmPlaceTeaser(AppModuleInfo), OpenPlayWagerInfo.WagerType, OpenPlayWagerInfo.TicketNumber.ToString(CultureInfo.InvariantCulture), OpenPlayWagerInfo.WagerNumber.ToString(CultureInfo.InvariantCulture));
          break;
      }
    }

    public void DisplayWageringFormInEditMode() {
      if (OpenPlayWagerInfo == null || OpenPlayWagerInfo.WagerStatus != "Pending") return;
      bool cutoffTimeBypassed;

      if (Ticket.Wagers != null && Ticket.Wagers.Count > 0 &&
          (from w in Ticket.Wagers where w.TicketNumber == OpenPlayWagerInfo.TicketNumber && w.WagerNumber == OpenPlayWagerInfo.WagerNumber select w).Any()) {
        MessageBox.Show(@"Wager in Readback already.", @"Not available", MessageBoxButtons.OK);
        return;
      }

      switch (OpenPlayWagerInfo.WagerTypeCode) {
        case WagerType.SPREAD:
        case WagerType.MONEYLINE:
        case WagerType.TOTALPOINTS:
        case WagerType.TEAMTOTALPOINTS:
          if (PendingPlayItemsInDb == null || !PendingPlayItemsInDb.Any() || PendingPlayItemsInDb[0] == null)
            return;
          var wiEditInfo = new ReadbackItem.WagerItemEditInfo();
          var activeGame = _frmSportAndGameSelection.ActiveGames.FirstOrDefault(a => a.GameNum == (int)PendingPlayItemsInDb[0].GameNum && a.PeriodNumber == (int)PendingPlayItemsInDb[0].PeriodNumber);
          if (activeGame == null) {
            MessageBox.Show(@"Required game Info for editing the wager is not available.", @"Not available", MessageBoxButtons.OK);
            return;
          }
          wiEditInfo.RowGroupNum = GetChosenTeamIdx(PendingPlayItemsInDb[0], activeGame);
          if (wiEditInfo.RowGroupNum == 0)
            return;
          wiEditInfo.SelectedCell = GetChosenWagerType(PendingPlayItemsInDb[0]);
          if (wiEditInfo.SelectedCell == 0)
            return;
          wiEditInfo.SelectedOdds = GetSelectedOdds(PendingPlayItemsInDb[0], activeGame);
          if (String.IsNullOrEmpty(wiEditInfo.SelectedOdds))
            return;

          wiEditInfo.SelectedAmericanPrice = PendingPlayItemsInDb[0].FinalMoney ?? 0;
          if (wiEditInfo.SelectedAmericanPrice == 0)
            return;
          wiEditInfo.Description = OpenPlayWagerInfo.Description;
          wiEditInfo.SelectedBPointsOption = GetPointsBought(PendingPlayItemsInDb[0]);
          wiEditInfo.SelectedAmountWagered = ((PendingPlayItemsInDb[0].AmountWagered ?? 0) / 100).ToString(CultureInfo.InvariantCulture);
          wiEditInfo.SelectedToWinAmount = ((PendingPlayItemsInDb[0].ToWinAmount ?? 0) / 100).ToString(CultureInfo.InvariantCulture);
          wiEditInfo.WagerTypeMode = (PendingPlayItemsInDb[0].FinalMoney ?? 0) >= 0 ? "Risk" : "ToWin";
          wiEditInfo.WageringMode = String.IsNullOrEmpty(PendingPlayItemsInDb[0].WageringMode) ? "A" : PendingPlayItemsInDb[0].WageringMode;
          wiEditInfo.PriceType = PendingPlayItemsInDb[0].PriceType;
          wiEditInfo.PeriodNumber = (int)PendingPlayItemsInDb[0].PeriodNumber;
          wiEditInfo.PeriodWagerCutoff = (DateTime)activeGame.PeriodWagerCutoff;
          wiEditInfo.LayoffWager = PendingPlayItemsInDb[0].LayoffFlag;

          wiEditInfo.FixedPrice = GetFixedPriceValue(PendingPlayItemsInDb[0]);

          wiEditInfo.Pitcher1MStart = PendingPlayItemsInDb[0].Pitcher1ReqFlag;
          wiEditInfo.Pitcher2MStart = PendingPlayItemsInDb[0].Pitcher2ReqFlag;
          wiEditInfo.TotalsOuFlag = PendingPlayItemsInDb[0].TotalPointsOU;

          _frmSportAndGameSelection.SelectedWagerType = "Straight Bet";
          if (PendingPlayItemsInDb[0].FreePlayFlag == "Y")
            chbFreePlay.Checked = true;

          automaticToolStripMenuItem.Click -= automaticToolStripMenuItem_Click;
          automaticToolStripMenuItem.Checked = true;
          automaticToolStripMenuItem.Click += automaticToolStripMenuItem_Click;

          if (wiEditInfo.WageringMode == "M") {
            manualLineToolStripMenuItem.Checked = true;
            HandleManualLineToolStripMenuItemClick();
            automaticToolStripMenuItem.Checked = false;

          }

          if (FrmSportAndGameSelection.ActiveGames != null) {
            var selectedGamePeriodInfo = (from s in FrmSportAndGameSelection.ActiveGames where s.GameNum == (int)PendingPlayItemsInDb[0].GameNum && s.PeriodNumber == wiEditInfo.PeriodNumber select s).FirstOrDefault();

            if (selectedGamePeriodInfo != null) {
              FrmSportAndGameSelection.CheckIfWagerCutoffHasBeenReachedInReadback("Accept Any Wager", "Request to Edit a READBACK Item Past Post", selectedGamePeriodInfo, wiEditInfo.Description, wiEditInfo.SelectedAmountWagered, wiEditInfo.SelectedToWinAmount, out cutoffTimeBypassed);
              if (cutoffTimeBypassed) {
                _frmSportAndGameSelection.ShowFormInEditMode((int)PendingPlayItemsInDb[0].GameNum, wiEditInfo.PeriodNumber,
                     PendingPlayItemsInDb[0].PeriodDescription, wiEditInfo.RowGroupNum, wiEditInfo.PeriodWagerCutoff, wiEditInfo.SelectedCell, PendingPlayItemsInDb[0].TicketNumber, PendingPlayItemsInDb[0].WagerNumber,
                     PendingPlayItemsInDb[0].ItemNumber, wiEditInfo.SelectedOdds, wiEditInfo.SelectedAmericanPrice, wiEditInfo.SelectedBPointsOption, wiEditInfo.SelectedAmountWagered, wiEditInfo.SelectedToWinAmount, wiEditInfo.WagerTypeMode, PendingPlayItemsInDb[0].FreePlayFlag == "Y",
                     wiEditInfo.PriceType,
                     wiEditInfo.WageringMode,
                 wiEditInfo.LayoffWager, wiEditInfo.FixedPrice,
                 wiEditInfo.Pitcher1MStart, wiEditInfo.Pitcher2MStart, "WagerDetails", wiEditInfo.TotalsOuFlag);
              }
              else {
                MessageBox.Show(@"The game period has already started or is not currently available.", @"Not available", MessageBoxButtons.OK);
              }
            }
          }
          break;
        case WagerType.MANUALPLAY:
          if (PendingManualPlayItemInDb == null)
            return;
          var mWiEditInfo = new ReadbackItem.ManualWagerItemEditInfo {Type = ""};
          switch (OpenPlayWagerInfo.WagerTypeCode) {
            default:
              mWiEditInfo.Type = "Action Points";
              break;
          }
          mWiEditInfo.Description = PendingManualPlayItemInDb.Description;
          mWiEditInfo.GradeDateTime = (DateTime)PendingManualPlayItemInDb.GradeDateTime;
          mWiEditInfo.Odds = (PendingManualPlayItemInDb.FinalMoney ?? 0).ToString(CultureInfo.InvariantCulture) + "to" + (PendingManualPlayItemInDb.FinalToBase ?? 0).ToString(CultureInfo.InvariantCulture);
          mWiEditInfo.SelectedAmountWagered = ((PendingManualPlayItemInDb.AmountWagered ?? 0) / 100).ToString(CultureInfo.InvariantCulture);
          mWiEditInfo.SelectedToWinAmount = ((PendingManualPlayItemInDb.ToWinAmount ?? 0) / 100).ToString(CultureInfo.InvariantCulture);
          mWiEditInfo.LayoffWager = PendingManualPlayItemInDb.LayoffFlag == "Y";

          var frmManual = new FrmPlaceManual(LayoffWagersEnabled, AppModuleInfo) {
            FrmMode = "Edit",
            EditModeInfo = mWiEditInfo,
            TicketNumberToUpd = PendingManualPlayItemInDb.TicketNumber,
            WagerNumberToUpd = PendingManualPlayItemInDb.WagerNumber,
            WagerItemNumberToUpd = PendingManualPlayItemInDb.ItemNumber,
            XToy = FrmPlaceManual.BuildxToyObj(mWiEditInfo.Odds)
          };
          DisplayWageringForm(frmManual, "Manual Play");
          break;
        case WagerType.CONTEST:
          if (PendingContestItemInDb == null)
            return;
          var cWiEditInfo = new ReadbackItem.ContestItemEditInfo {ContestantDescription = PendingContestItemInDb.ContestantName, ContestantNum = PendingContestItemInDb.ContestantNum ?? 0};
          if (cWiEditInfo.ContestantNum == 0)
            return;
          cWiEditInfo.ContestNum = PendingContestItemInDb.ContestNum ?? 0;
          if (cWiEditInfo.ContestNum == 0)
            return;
          cWiEditInfo.LayoffWager = PendingContestItemInDb.LayoffFlag == "Y";
          cWiEditInfo.PriceType = PendingContestItemInDb.PriceType;
          cWiEditInfo.SelectedAmountWagered = ((PendingContestItemInDb.AmountWagered ?? 0) / 100).ToString(CultureInfo.InvariantCulture);
          cWiEditInfo.SelectedToWinAmount = ((PendingContestItemInDb.ToWinAmount ?? 0) / 100).ToString(CultureInfo.InvariantCulture);
          cWiEditInfo.ToBase = (PendingContestItemInDb.FinalToBase ?? 0).ToString(CultureInfo.InvariantCulture);
          cWiEditInfo.WageringMode = PendingContestItemInDb.WageringMode;
          cWiEditInfo.XtoYRep = String.IsNullOrEmpty(PendingContestItemInDb.XtoYLineRep) ? "N" : PendingContestItemInDb.XtoYLineRep;
          if ((PendingContestItemInDb.FinalToBase ?? 0) == 0) {
            cWiEditInfo.SelectedOdds = (PendingContestItemInDb.FinalMoney ?? 0).ToString(CultureInfo.InvariantCulture);
          }
          else {
            cWiEditInfo.SelectedOdds = (PendingContestItemInDb.FinalMoney ?? 0) + "to" + (PendingContestItemInDb.FinalToBase ?? 0);
          }

          if (FrmSportAndGameSelection.ActiveContests != null) {
            var selectedContestInfo = (from s in FrmSportAndGameSelection.ActiveContests where s.ContestNum == cWiEditInfo.ContestNum && s.ContestantNum == cWiEditInfo.ContestantNum select s).FirstOrDefault();
            if (selectedContestInfo != null) {
              cWiEditInfo.SelectedThresholdLine = (selectedContestInfo.ThresholdLine ?? 0).ToString(CultureInfo.InvariantCulture);

              _frmSportAndGameSelection.SelectedWagerType = "Straight Bet";
              if (PendingPlayItemsInDb[0].FreePlayFlag == "Y")
                chbFreePlay.Checked = true;

              automaticToolStripMenuItem.Click -= automaticToolStripMenuItem_Click;
              automaticToolStripMenuItem.Checked = true;
              automaticToolStripMenuItem.Click += automaticToolStripMenuItem_Click;

              if (cWiEditInfo.WageringMode == "M") {
                manualLineToolStripMenuItem.Checked = true;
                HandleManualLineToolStripMenuItemClick();
                automaticToolStripMenuItem.Checked = false;
              }

              FrmSportAndGameSelection.CheckIfContestWagerCutoffHasBeenReachedInReadback("Accept Any Wager", "Request to Edit a READBACK Item Past Post", selectedContestInfo, OpenPlayWagerInfo.Description, cWiEditInfo.SelectedAmountWagered, cWiEditInfo.SelectedToWinAmount, out cutoffTimeBypassed);

              if (cutoffTimeBypassed)
                FrmSportAndGameSelection.ShowContestFormInEditMode("Edit", PendingContestItemInDb.TicketNumber, PendingContestItemInDb.WagerNumber, PendingContestItemInDb.ItemNumber,
                    cWiEditInfo.ContestNum, cWiEditInfo.ContestantNum, cWiEditInfo.ContestantDescription,
                    cWiEditInfo.SelectedAmountWagered, cWiEditInfo.SelectedOdds, cWiEditInfo.SelectedToWinAmount, PendingContestItemInDb.FreePlayFlag == "Y",
                    cWiEditInfo.PriceType, cWiEditInfo.SelectedThresholdLine, cWiEditInfo.WageringMode, cWiEditInfo.LayoffWager, cWiEditInfo.XtoYRep, cWiEditInfo.ToBase);
            }
            else {
              MessageBox.Show(@"Required contest Info for editing the wager is not available.", @"Not available", MessageBoxButtons.OK);
            }
          }
          break;
        case WagerType.PARLAY:
        case WagerType.TEASER:
        case WagerType.IFBET:
          var wager = FrmSportAndGameSelection.BuildWObject(OpenPlayWagerInfo);
          if (Ticket.Wagers == null) {
            Ticket.Wagers = new List<Ticket.Wager>();
          }
          if (wager != null)
            Ticket.Wagers.Add(wager);
          var wagerItems = FrmSportAndGameSelection.BuildWiObject(PendingPlayItemsInDb);
          if (Ticket.WagerItems == null) {
            Ticket.WagerItems = new List<Ticket.WagerItem>();
          }
          if (wagerItems != null && wagerItems.Count > 0)
            Ticket.WagerItems.AddRange(wagerItems);

          FrmSportAndGameSelection.ShowComplexBetInEditMode(OpenPlayWagerInfo.WagerType, wager, wagerItems);
          break;
      }
    }

    public string GetPointsBought(spTkWGetWagerItems_Result item) {
      var points = "";
      switch (item.WagerType) {
        case WagerType.SPREAD:
          points = (item.AdjSpread ?? 0).ToString(CultureInfo.InvariantCulture).Replace(".5", GamesAndLines.HALFSYMBOL);
          if (item.AdjSpread >= 0)
            points = points.Insert(0, "+");
          break;
        case WagerType.TOTALPOINTS:
          points = (item.AdjTotalPoints ?? 0).ToString(CultureInfo.InvariantCulture).Replace(".5", GamesAndLines.HALFSYMBOL);
          if (item.AdjTotalPoints >= 0)
            points = points.Insert(0, "+");
          break;
      }

      return points;
    }

    public string GetFixedPriceValue(spTkWGetWagerItems_Result item) {
      var fpV = "N";
      if (item.AdjustableOddsFlag == "N" || string.IsNullOrEmpty(item.AdjustableOddsFlag))
        fpV = "Y";
      return fpV;
    }

    public string GetSelectedOdds(spTkWGetWagerItems_Result item, spGLGetActiveGamesByCustomer_Result game) {
      var odds = "";
      if (item == null || game == null)
        return odds;

      switch (item.WagerType) {
        case WagerType.SPREAD:
          odds = LineOffering.DetermineSpreadValue(Math.Abs((item.OrigSpread ?? 0)), item.ChosenTeamID.Trim(), game.FavoredTeamID.Trim()) + " " + LineOffering.GetNumAdjSign(item.FinalMoney ?? 0);
          break;
        case WagerType.MONEYLINE:
          odds = LineOffering.GetNumAdjSign(item.FinalMoney ?? 0);
          break;
        case WagerType.TOTALPOINTS:
        case WagerType.TEAMTOTALPOINTS:
          odds = LineOffering.DetermineTotalValue(Math.Abs((item.OrigTotalPoints ?? 0))) + " " + LineOffering.GetNumAdjSign(item.FinalMoney ?? 0);
          break;
      }
      return odds;
    }

    public int GetChosenWagerType(spTkWGetWagerItems_Result item) {
      var idx = 0;
      if (item == null)
        return idx;
      switch (item.WagerType) {
        case WagerType.SPREAD:
          idx = 3;
          break;
        case WagerType.MONEYLINE:
          idx = 4;
          break;
        case WagerType.TOTALPOINTS:
          idx = 5;
          break;
        case WagerType.TEAMTOTALPOINTS:
          idx = item.TotalPointsOU.ToLower() == "o" ? 6 : 7;
          break;
      }
      return idx;
    }

    public int GetChosenTeamIdx(spTkWGetWagerItems_Result item, spGLGetActiveGamesByCustomer_Result game) {
      var idx = 0;
      if (game == null)
        return idx;

      switch (item.WagerType) {
        case WagerType.SPREAD:
        case WagerType.TEAMTOTALPOINTS:
          idx = item.ChosenTeamID.ToLower().Trim() == game.Team1ID.ToLower().Trim() ? 1 : 2;
          break;
        case WagerType.MONEYLINE:
          if (game.SportType.ToLower().Trim() == "soccer") {
            if (item.ChosenTeamID.ToLower().Contains("draw")) {
              idx = 3;
            }
            else {
              idx = item.ChosenTeamID.ToLower().Trim() == game.Team1ID.ToLower().Trim() ? 1 : 2;
            }
          }
          else {
            idx = item.ChosenTeamID.ToLower().Trim() == game.Team1ID.ToLower().Trim() ? 1 : 2;
          }

          break;
        case WagerType.TOTALPOINTS:
          idx = item.TotalPointsOU.ToLower() == "o" ? 1 : 2;
          break;
      }
      return idx;
    }

    public DateTime GetCurrentDateTime() {
      var newDateTime = DateTime.Now.AddDays(ClientServerTimeDiff.Days * -1);
      newDateTime = newDateTime.AddHours(ClientServerTimeDiff.Hours * -1);
      newDateTime = newDateTime.AddMinutes(ClientServerTimeDiff.Minutes * -1);
      newDateTime = newDateTime.AddSeconds(ClientServerTimeDiff.Seconds * -1);
      newDateTime = newDateTime.AddMilliseconds(ClientServerTimeDiff.Milliseconds * -1);
      return newDateTime;
    }

    public void ToggleEnableRifOptionsForCustomer(Boolean enable) {
      chbRollingIF.Enabled = rollingIfBetToolStripMenuItem.Enabled = enable;
    }

    public void GetCustomerAvailableTeasers() {
      CustomerAvailableTeasers = new List<spCstGetCustomerTeaserSpecs_Result>();

      using (var customerService = new Customers(AppModuleInfo)) {
        CustomerAvailableTeasers = customerService.GetCustomerTeaserSpecs(Customer.CustomerID);
      }
    }

    public void GetCustomerParlayPayCard() {
      if (Customer.ParlayName == null) return;
      if (Customer.ParlayName.Length <= 0) return;
      ParlayPayCard = new List<spLOGetParlayPayCard_Result>();

      var parlayName = Customer.ParlayName.Trim();

      using (var parlayInfo = new LineOffering.Parlay(AppModuleInfo)) {
        ParlayPayCard = parlayInfo.GetCustomerParlayPayCard(parlayName);
      }
    }

    public void GetCustomerTeasersPayCard() {
      if (CustomerAvailableTeasers == null || CustomerAvailableTeasers.Count <= 0) return;
      TeasersPayCard = new List<spLOGetTeaserPayCard_Result>();

      using (var teaserInfo = new LineOffering.Teaser(AppModuleInfo)) {
        foreach (var res in CustomerAvailableTeasers) {
          var teaserName = res.TeaserName.Trim();
          var teaserObj = teaserInfo.GetTeaserPayCard(teaserName);

          foreach (var ele in teaserObj) {
            TeasersPayCard.Add(ele);
          }
        }
      }
    }

    public void GetCustomerTeasersSpecs() {
      if (CustomerAvailableTeasers == null || CustomerAvailableTeasers.Count <= 0) return;
      TeasersSpecs = new List<spLOGetTeaserSpecs_Result>();

      using (var teaserInfo = new LineOffering.Teaser(AppModuleInfo)) {
        foreach (var res in CustomerAvailableTeasers) {
          var teaserName = res.TeaserName.Trim();
          var teaserObj = teaserInfo.GetTeaserSpecs(teaserName);
          TeasersSpecs.Add(teaserObj);
        }
      }
    }

    public void GetTeasersSportsSpecs() {
      if (CustomerAvailableTeasers == null || CustomerAvailableTeasers.Count <= 0) return;
      TeasersSportSpecs = new List<spLOGetTeaserSportSpec_Result>();

      using (var teaserInfo = new LineOffering.Teaser(AppModuleInfo)) {
        foreach (var res in CustomerAvailableTeasers) {
          var teaserName = res.TeaserName.Trim();
          var teaserObj = teaserInfo.GetSportSpecs(teaserName);

          foreach (var ele in teaserObj) {
            TeasersSportSpecs.Add(ele);
          }
        }
      }
    }

    public void OpenCustomerTransactionsForm(List<spULPGetCurrentUserPermissions_Result> currentUserPermissions, spCstGetCustomer_Result customer, FormParameterList parameterList) {

      using (var frmCustomerTransactions = new FrmCustomerTransactions(AppModuleInfo) {
        CurrentUserPermissions = currentUserPermissions,
        Customer = customer,
        ParameterList = parameterList
      }) {
        frmCustomerTransactions.Icon = Icon;
        frmCustomerTransactions.ShowDialog();
      }
    }

    public void SetWageringButtonsAppereance(bool value) {
      btnNewIfBet.Enabled = btnNewManualPlay.Enabled =
                                                       btnNewParlay.Enabled = btnNewTeaser.Enabled = value;
    }

    public void SetWageringToolStripsAppereance(bool value) {
      parlayToolStripMenuItem.Enabled = teaserToolStripMenuItem.Enabled = ifBetToolStripMenuItem.Enabled =
                                                       manualPlayToolStripMenuItem.Enabled = value;
    }

    private void ShowCustomerFigures() {
      using (var frmCustomerBalanceInformation = new FrmCustomerBalanceInformationTw(this) {
        ControlBox = false,
        FormBorderStyle = FormBorderStyle.None,
        StartPosition = FormStartPosition.CenterParent,
        Owner = this
      }) {
        frmCustomerBalanceInformation.Icon = Icon;
        frmCustomerBalanceInformation.ShowDialog();
      }
    }

    public void StopAudioRecording() {
      if (_dr == null) return;
      _dr.StopRecording();
      var t = new Thread(() => {
        _dr = new TicketDr(AppModuleInfo, TicketNumber);
        var retParm = Params.UtilitiesDirectory;
        _dr.UtilitiesFolder = retParm;
        _dr.TransferAudioToRepository();

      }) { IsBackground = true };
      t.Start();
      _dr = null;
    }

    public void ToggleRifCheckBox(Boolean newStatus) {
      chbRollingIF.Checked = newStatus;
    }

    #endregion

    #region Private Methods

    private void ArrangeWindows() {

      try {
        if (tssLabelReady != null && Width > 0 && tssLabelWagerStatus != null)
          tssLabelReady.Size = new Size(Width / 2 - tssLabelWagerStatus.Size.Width / 2, tssLabelReady.Size.Height);

        if (MdiChildren.Length < 2 || MdiClient == null) return;

        int baseHeight = MdiClient.Size.Height,
            baseWidth = MdiClient.Size.Width,
            firstFormHeight = Convert.ToInt32(baseHeight * 0.8);

        if (MdiChildren[1] != null) {
          MdiChildren[1].Location = new Point(0, 0);
          MdiChildren[1].Size = new Size(baseWidth, firstFormHeight);
        }

        if (MdiChildren[0] != null) {
          MdiChildren[0].Location = new Point(0, firstFormHeight);
          MdiChildren[0].Size = new Size(baseWidth, baseHeight - firstFormHeight);
        }
      }
      catch (Exception e) {
        MessageBox.Show(e.Message);
      }

      var frmPlaceParlay = (FrmPlaceParlay)FormF.GetFormByName("FrmPlaceParlay", this);

      if (frmPlaceParlay != null) {
        frmPlaceParlay.RelativeXPos = Width + Location.X;
        frmPlaceParlay.RelativeYPos = Height + Location.Y;

        frmPlaceParlay.AdjustFrmPosition();
      }

      var frmPlaceIfBet = (FrmPlaceIfBet)FormF.GetFormByName("FrmPlaceIfBet", this);

      if (frmPlaceIfBet != null) {
        frmPlaceIfBet.RelativeXPos = Width + Location.X;
        frmPlaceIfBet.RelativeYPos = Height + Location.Y;

        frmPlaceIfBet.AdjustFrmPosition();
      }

      var frmPlaceTeaser = (FrmPlaceTeaser)FormF.GetFormByName("FrmPlaceTeaser", this);

      if (frmPlaceTeaser != null) {
        frmPlaceTeaser.RelativeXPos = Width + Location.X;
        frmPlaceTeaser.RelativeYPos = Height + Location.Y;

        frmPlaceTeaser.AdjustFrmPosition();
      }

      var frmPlaceManual = (FrmPlaceManual)FormF.GetFormByName("frmPlaceManual", this);

      if (frmPlaceManual == null) return;
      frmPlaceManual.RelativeXPos = Width + Location.X;
      frmPlaceManual.RelativeYPos = Height + Location.Y;

      frmPlaceManual.AdjustFrmPosition();
    }

    private Boolean CheckForNonMdiForms(out String wagerName) {
      var formNamesList = new[] { "FrmMakeAWager", "frmPlaceManual", "frmPlaceIfBet", "frmPlaceParlay", "frmPlaceTeaser", "frmPayCard", "frmManualContestWager", "FrmManualGameWager", "FrmPlaceContestOrProp" };
      wagerName = "";
      var atLeastOneOpen = false;

      foreach (var frmName in formNamesList) {
        var frm = FormF.GetFormByName(frmName, this);
        if (frm == null || !frm.Visible) continue;
        atLeastOneOpen = true;
        wagerName = frm.AccessibleDescription;
        break;
      }
      return atLeastOneOpen;
    }

    private void ClearGridView() {
      var sportsFrm = (FrmSportAndGameSelection)FormF.GetFormByName("FrmSportAndGameSelection", this);

      if (sportsFrm == null) return;
      DataGridView gamesPropsGvw;
      if (sportsFrm.GamesGridViewInUse != null) {
        gamesPropsGvw = (DataGridView)sportsFrm.Controls.Find((sportsFrm.GamesGridViewInUse ?? ""), true).FirstOrDefault();

        if (gamesPropsGvw != null && gamesPropsGvw.Visible) {
          gamesPropsGvw.Rows.Clear();
        }
      }

      if (sportsFrm.PropsGridViewInUse != null) {
        gamesPropsGvw = (DataGridView)sportsFrm.Controls.Find(sportsFrm.PropsGridViewInUse, true).FirstOrDefault();

        if (gamesPropsGvw != null && gamesPropsGvw.Visible) {
          gamesPropsGvw.Rows.Clear();
        }
      }
    }

    private void CloseCustomer() {
      if (WageringPanelName != null) {
        MessageBox.Show(@"Please close the " + WageringPanelName + @" panel first.");
        return;
      }
      if (BalanceInfo != null && BalanceInfo.AvailableBalance.HasValue && BalanceInfo.AvailableBalance < 0 && Ticket.Wagers != null && Ticket.Wagers.Count > 0 && MessageBox.Show(@"The Customer's available balance is NEGATIVE (" + FormatNumber((double)BalanceInfo.AvailableBalance, true, 100, true) + @") which may be caused by recent Internet, Casino, or Customer Service activity.  Do you still want to post the wagers?", @"Insufficient Funds", MessageBoxButtons.YesNo) == DialogResult.No) {
        return;
      }

      int? ticketNumber = null;
      var playCount = 0;

      if (ShowTicketInfoAfterClosing) {
        ticketNumber = Ticket.TicketNumber;
        playCount = Ticket.PlayCount;
      }

      chbRollingIF.Checked = chbFreePlay.Checked = false;
      CustomerWasClosed = true;

      if (MdiChildren.Count() > 1) {
        var frm = (FrmSportAndGameSelection)MdiChildren[1];
        var ticketToInsert = new TicketAndWagersInsertion(Customer, Ticket, AppModuleInfo, ClientServerTimeDiff, ActionReverseIfBets, BirdCageIfBets);
        ticketToInsert.ExceptionFound += TicketToInsert_ExceptionFound;
        ticketToInsert.InsertTicketAndWager();

        if (frm.LinesUpdater != null && frm.LinesUpdater.Enabled) {
          frm.LinesUpdater.Tick -= frm.LinesUpdater_Tick;
          frm.LinesUpdater.Stop();
          frm.LinesUpdater = null;
        }
      }

      foreach (var childForm in MdiChildren) {
        childForm.Close();
        childForm.Dispose();
      }
      CloseNonMdiForms();
      StopAudioRecording();
      if (ParameterList != null && Customer != null) {
        using (var log = new LogWriter(AppModuleInfo)) {
          log.WriteToCuAccessLog(ParameterList.GetItem("ModuleName").Value, "Close Customer", Customer.CustomerID);
        }
      }
      if (Customer != null) {
        LastCustomer = Customer;
      }

      CleanCustomerRelatedObjects();

      using (var logins = new LoginsAndProfiles(AppModuleInfo)) {
        logins.UpdateCurrentLoginInUseInfo(LoginInUse);
      }

      RestoreFormInitialStatus();
      GC.Collect();
      GC.WaitForPendingFinalizers();

      if (ShowTicketInfoAfterClosing && playCount > 0) {
        HandleShowticketInfoRequest(ticketNumber, false, false);
      }
    }

    private void TicketToInsert_ExceptionFound(object sender, EventArgs e) {
      var ev = (InsertTicketEventArgs)e;
      MessageBox.Show(ev.MessageToShow, @"Error", MessageBoxButtons.OK);
    }

    private void CleanCustomerRelatedObjects() {
      Customer = null;
      Ticket = null;
      BalanceInfo = null;
      CustomerAvailableTeasers = null;
      CustomerStoreProfile = null;
      OpenPlayWagerInfo = null;
      OpenPlayItemsDescription = null;
      PendingPlayItemsInDb = null;
      PendingManualPlayItemInDb = null;
      PendingContestItemInDb = null;
      PendingWagerInDb = null;

      ParlayPayCard = null;

      LoginInUse.CallCustomerID = null;
      LoginInUse.CallStartTime = null;

      RifBBackwardTicketInfo = null;
      RIfBetFromReadback = null;
      RollingIfBetMode = false;
      ShowingCorrelatedGames = false;
      ShowingLatestLines = false;
      ShowingLikeTeams = false;
      TeaserInfo = null;
      TeasersPayCard = null;
      TeasersSpecs = null;
      TeasersSportSpecs = null;

      _customerWagerLimits = null;
      _actionReverseIfBets = null;
      _birdCageIfBets = null;
      _storeWagerLimits = null;
      _vigDiscountLimits = null;
      _parlayPayoutType = null;
    }

    private void CloseNonMdiForms() {
      var formNamesList = new[] { "FrmMakeAWager", "frmPlaceManual", "frmPlaceIfBet", "frmPlaceParlay", "frmPlaceTeaser", "frmPayCard", "frmManualContestWager", "FrmManualGameWager", "FrmPlaceContestOrProp", "FrmOpenCustomer" };

      foreach (var frmName in formNamesList) {
        var frm = FormF.GetFormByName(frmName, this);
        if (frm == null) continue;
        frm.Tag = null;
        frm.Close();
        frm.Dispose();
      }
    }

    private void CreateDefaultRifProperty() {
      RollingIfBetMode = false;
      RifBTicketInfo = new RifBackwardTicketInfo.RifbTicketInfo();
      var stdInfo = new RifBackwardTicketInfo.RifbTicketInfo { TicketNumber = 0 };
      RifBTicketInfo = stdInfo;

      var rifEnabledByDef = "Y";

      if (Customer != null)
        rifEnabledByDef = Customer.EnableRifFlag;
      rifEnabledByDef = rifEnabledByDef == null ? "N" : rifEnabledByDef.Trim();

      ToggleEnableRifOptionsForCustomer(rifEnabledByDef == "Y");
    }

    private void DisplayCommentsForTw() {
      var commentsForTw = BalanceInfo.CommentsForTw;
      var commentsForCustomer = Customer.CommentsForCustomer;

      if (string.IsNullOrEmpty(commentsForTw) && string.IsNullOrEmpty(commentsForCustomer)) {
        MessageBox.Show(@"No comments to Show", @"No comments");
      }
      else {
        using (var frmImportantComments = new FrmImportantComments(AppModuleInfo)) {
          frmImportantComments.Comment = commentsForTw + Environment.NewLine + commentsForCustomer;
          frmImportantComments.Icon = Icon;
          frmImportantComments.ShowDialog();
        }
      }
    }

    private void EnableAllPeriodRadioButtons() {
      if (FrmSportAndGameSelection == null) return;
      var periodsPanel = FrmSportAndGameSelection.Controls.Find("panGamePeriodSelection", true).FirstOrDefault();

      if (periodsPanel == null) return;
      foreach (Control ctrl in periodsPanel.Controls) {
        if (ctrl.GetType().ToString() != "System.Windows.Forms.RadioButton") continue;
        var rad = (RadioButton)ctrl;
        rad.Enabled = true;
      }
    }

    private Form GetWageringForm() {
      return OwnedForms.FirstOrDefault(form => form.Name.Substring(0, 8) == "FrmPlace");
    }

    public void DisplayHorseWageringForm(Form frm, string wagerType) {
      frm.Owner = this;
      var frmProperty = frm.GetType().GetProperty("ActiveMdiChildName");

      if (frmProperty != null) {
        if (ActiveMdiChild != null) frmProperty.SetValue(frm, ActiveMdiChild.Name, null);
      }
      _childFrmProperty = MdiChildren[1].GetType().GetProperty("SelectedWagerType");
      if (_childFrmProperty != null) {
        _childFrmProperty.SetValue(MdiChildren[1], wagerType, null);
      }
      frm.Show();
    }

    public void DisplayWageringForm(Form frm, string wagerType, String ticketNumber = null, String wagerNumber = null, DateTime? postedDateTime = null) {
      // ((FrmSportAndGameSelection) MdiChildren[1]).SelectedWagerType = wagerType;
      frm.Owner = this;
      PlaceFormBottomRight(frm);
      SetWageringButtonsAppereance(false);
      SetWageringToolStripsAppereance(false);

      var frmProperty = frm.GetType().GetProperty("ActiveMdiChildName");

      if (frmProperty != null) {
        if (ActiveMdiChild != null) frmProperty.SetValue(frm, ActiveMdiChild.Name, null);
      }
      _childFrmProperty = MdiChildren[1].GetType().GetProperty("SelectedWagerType");
      if (_childFrmProperty != null) {
        _childFrmProperty.SetValue(MdiChildren[1], wagerType, null);
      }

      if (ticketNumber != null) {
        _childFrmProperty = MdiChildren[1].GetType().GetProperty("OpenPlayTicketNumber");
        if (_childFrmProperty != null) {
          _childFrmProperty.SetValue(MdiChildren[1], ticketNumber, null);
        }
        _childFrmProperty = MdiChildren[1].GetType().GetProperty("TicketNumberToUpd");
        if (_childFrmProperty != null) {
          _childFrmProperty.SetValue(MdiChildren[1], ticketNumber, null);
        }
      }

      if (wagerNumber != null) {
        _childFrmProperty = MdiChildren[1].GetType().GetProperty("WagerNumberToUpd");
        if (_childFrmProperty != null) {
          _childFrmProperty.SetValue(MdiChildren[1], wagerNumber, null);
        }
      }

      if (postedDateTime != null) {
        _childFrmProperty = MdiChildren[1].GetType().GetProperty("OpenPlayTicketPostedDateTime");
        if (_childFrmProperty != null) {
          _childFrmProperty.SetValue(MdiChildren[1], postedDateTime, null);
        }
      }

      if (wagerType == "Parlay" || wagerType == "Teaser") {
        ToggleFpAndRifChecks(true);
      }
      else {
        ToggleFpAndRifChecks(false);
      }

      frm.Show();
      WageringPanelName = wagerType;
    }

    public void FillCustomerDataTextBoxes(String mode) {
      if (BalanceInfo == null) return;

      txtCustomerPIN.Text = BalanceInfo.CustomerId;

      switch (mode) {
        case "F":
          txtAvailableAmount.Text =
              string.Format("{0} {1} *Free Play*", FormatNumber(double.Parse(BalanceInfo.FreePlayAvailable.ToString()), true, 100, true), BalanceInfo.Currency.Split(' ')[0]);
          break;

        case "R":
          txtAvailableAmount.Text =
              string.Format("{0} {1} *RIF*", FormatNumber(double.Parse(BalanceInfo.FreePlayBalance.ToString()), true, 100, true), BalanceInfo.Currency.Split(' ')[0]);
          break;

        default:
          txtAvailableAmount.Text =
              string.Format("{0} {1}", FormatNumber(double.Parse(BalanceInfo.CurrentBalance.ToString()), true, 100, true), BalanceInfo.Currency.Split(' ')[0]);
          break;
      }
    }

    private MdiClient GetMdiClientWindow() {
      return Controls.OfType<MdiClient>().Select(ctl => ctl).FirstOrDefault();
    }

    private void InitializeTicketWriter() {
      _dr = new TicketDr(AppModuleInfo, 0) { ServerDateTime = ServerDateTime };
      _dr.CheckForOldLocalDrFiles();

      ActionReverseIfBets = new List<IfBet>();
      BirdCageIfBets = new List<IfBet>();
      LayoutManager = new LayoutManager();

      if (Params.RecordAudio && !SoundCardDetected) {
        MessageBox.Show(Resources.mdiTicketWriter_InitializeTicketWriter_No_sound_card_found_, ParameterList.GetItem("AppName").Value, MessageBoxButtons.OK, MessageBoxIcon.Warning);
      }

      tssLabelWagerStatus.Text = @"*** AVAILABLE BALANCE Mode / AUTOMATIC LINE mode ***";
      statBar.Visible = false;

      manualLineToolStripMenuItem.Visible = LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.MANUAL_LINE_TW_MODE);
      horseToolStripMenuItem.Visible = horseToolStripMenuItem.Enabled = (LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.RUN_HORSES));

      ShowTicketInfoAfterClosing = showTicketInfoAfterClosingToolStripMenuItem.Visible = LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.REVIEW_WAGERS);

      MdiClient = GetMdiClientWindow();

      using (var lw = new LogWriter(AppModuleInfo)) {
        lw.LogUserInOrOutToOrFromCuTb("Login");
      }

      new ShowCents(AppModuleInfo, Params);
    }

    private void OpenCustomerDialog() {
      using (var frmOpenCustomer = new FrmOpenCustomer(AppModuleInfo) {
        CurrentUserPermissions = CurrentUserPermissions,
        Owner = this,
        Customer = Customer,
        ParameterList = ParameterList
      }) {
        frmOpenCustomer.Icon = Icon;
        var dialogResult = frmOpenCustomer.ShowDialog();

        if (dialogResult == DialogResult.Cancel) {
          return;
        }
      }
      if (Customer == null) {
        return;
      }
      SetVerifiedCustomerFormStatus();

      var frmTicketConfirmation = new FrmTicketConfirmation(this) { MdiParent = this };

      var frmSportAndGameSelection = new FrmSportAndGameSelection(this, MdiChildren[0],
                                                                  Customer) {
                                                                    MdiParent = this
                                                                  };

      ArrangeWindows();

      frmTicketConfirmation.Icon = Icon;
      frmTicketConfirmation.Show();
      frmSportAndGameSelection.Icon = Icon;
      frmSportAndGameSelection.Show();
      CreateDefaultRifProperty();
      ActiveControl = txtTeamOrAsterisk;
    }

    private void PlaceFormBottomRight(Form frm) {
      frm.Location = new Point(Location.X + (Width - frm.Width - 10), Location.Y + (Height - frm.Height - 10));
      frm.TopMost = true;
    }

    private void ReleaseLoginInUse() {
      using (var log = new LogWriter(AppModuleInfo)) {
        log.LogUserInOrOutToOrFromCuTb("Logout");
      }
    }

    private void RefreshGamesLines() {
      if (MdiChildren.Length < 2) return;
      var dgvw = (DataGridView)Controls.Find(((FrmSportAndGameSelection)MdiChildren[1]).GamesGridViewInUse, true).FirstOrDefault();

      if (dgvw == null) return;
      if (dgvw.Rows.Count > 0) {
        ((FrmSportAndGameSelection)MdiChildren[1])._lastGameChangeNum = 0;
        ((FrmSportAndGameSelection)MdiChildren[1]).UpdateGameLines();
      }
    }

    private void RestoreFormInitialStatus() {
      SetControlsStatuses(false);

      _clientServerTimeDiff = null;
      _frmSportAndGameSelection = null;
      ShowPropsInSportsTreeView = false;
      WageringMode = "A";

    }

    private void SetControlsStatuses(bool value) {
      btnAnswerPhone.Enabled = openCustomerToolStripMenuItem.Enabled = !value;
      displayContestsToolStripMenuItem.Checked = false;
      automaticToolStripMenuItem.Checked = true;
      manualLineToolStripMenuItem.Checked = false;
      availableBalanceToolStripMenuItem.Checked = true;
      freePlayToolStripMenuItem.Checked = false;
      rollingIfBetToolStripMenuItem.Checked = false;
      tssLabelWagerStatus.Text = @"*** AVAILABLE BALANCE Mode / AUTOMATIC LINE mode ***";
      chbRollingIF.Enabled = Customer != null && Customer.EnableRifFlag == "Y";
      accountHistoryToolStripMenuItem.Visible =
          btnArrangeWindows.Enabled =
          btnHangUpPhone.Enabled = btnNewIfBet.Enabled =
                                   btnNewHorseWager.Enabled = btnNewManualPlay.Enabled = btnNewParlay.Enabled =
                                                                                         btnNewTeaser.Enabled =
                                                                                         btnPropsAndFutures
                                                                                             .Enabled =
                                                                                         btnShowCustomerFigures
                                                                                             .Visible =
                                                                                         btnShowReadback.Enabled =
                                                                                         chbFreePlay.Visible =
                                                                                         chbRollingIF.Visible =
                                                                                         closeCustomerToolStripMenuItem
                                                                                             .Visible =
                                                                                         commentsToolStripMenuItem
                                                                                             .Visible =
                                                                                         displayContestsToolStripMenuItem
                                                                                             .Visible =
                                                                                         dailyFiguresToolStripMenuItem
                                                                                             .Visible =
                                                                                         insertToolStripMenuItem
                                                                                             .Visible =
                                                                                         lblAvail.Visible =
                                                                                         lblPIN.Visible =
                                                                                         lblTeamOrAsterisk.Visible
                                                                                         =
                                                                                         modeToolStripMenuItem
                                                                                             .Visible =
                                                                                         openPlaysToolStripMenuItem
                                                                                             .Visible =
                                                                                         refreshBalanceToolStripMenuItem
                                                                                             .Visible =
                                                                                         refreshLinesToolStripMenuItem
                                                                                             .Visible =
                                                                                         ticketInfoToolStripMenuItem
                                                                                             .Visible =
                                                                                         toolStripSeparator1
                                                                                             .Visible =
                                                                                         toolStripSeparator2
                                                                                             .Visible =
                                                                                         txtAvailableAmount
                                                                                             .Visible =
                                                                                         txtCustomerPIN.Visible =
                                                                                         txtTeamOrAsterisk.Visible
                                                                                         =
                                                                                         windowToolStripMenuItem
                                                                                             .Visible =
                                                                                         value;
    }

    private void SetModuleInMaintenance() {
      if (InvokeRequired) {
        Invoke(new MaintenanceAction(SetModuleInMaintenance));
      }
      else {
        FormClosing -= MdiTicketWriter_FormClosing;
        Close();
      }
    }

    private void SetVerifiedCustomerFormStatus() {
      SetControlsStatuses(true);
      horseToolStripMenuItem.Enabled = btnNewHorseWager.Enabled = (LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.RUN_HORSES)
          && !CustomerRestrictions.Any(c => c.Code != null && c.Code.ToUpper() == "HORSES"));

    }

    private void ShowLatestChangesOrAlikeTeams(object sender) {
      var textValue = ((TextBox)sender).Text;

      var sportsFrm = (FrmSportAndGameSelection)FormF.GetFormByName("FrmSportAndGameSelection", this);

      if (sportsFrm == null) return;
      var gamesGvw = (DataGridView)sportsFrm.Controls.Find(sportsFrm.GamesGridViewInUse, true).FirstOrDefault();
      var contestsGvw = (DataGridView)sportsFrm.Controls.Find(sportsFrm.PropsGridViewInUse, true).FirstOrDefault();

      if (contestsGvw != null && contestsGvw.Visible)
        contestsGvw.Visible = false;

      if (gamesGvw == null) return;
      gamesGvw.Visible = true;
      var rad = (RadioButton)sportsFrm.Controls.Find("radGame_0", true).FirstOrDefault();

      if (rad != null)
        rad.Checked = true;

      switch (textValue.Trim()) {
        case "*": {
            var lastCall = GetCurrentDateTime();
            if (Customer != null && Customer.LastCallDateTime != null)
              lastCall = (DateTime)Customer.LastCallDateTime;
            gamesGvw.Rows.Clear();
            ShowingLikeTeams = false;
            ShowingLatestLines = true;
            sportsFrm.FillGamesGridView(null, lastCall, 0, null, null, "Game");
          }
          break;
        case "":
          gamesGvw.Rows.Clear();
          ShowingLikeTeams = false;
          ShowingLatestLines = false;
          break;
        default:
          ShowingLikeTeams = true;
          ShowingLatestLines = false;
          gamesGvw.Rows.Clear();
          sportsFrm.FillGamesGridView(null, null, 0, textValue.Trim(), null, "Game");
          break;
      }

      var selectedFromTree = (Label)sportsFrm.Controls.Find("lblCurrentlyShowing", true).FirstOrDefault();

      if (selectedFromTree != null)
        selectedFromTree.Text = "";

      var lastSpreadChange = (Label)sportsFrm.Controls.Find("lblLastSpreadChange", true).FirstOrDefault();

      if (lastSpreadChange != null)
        lastSpreadChange.Text = "";

      var lastMoneyLineChange = (Label)sportsFrm.Controls.Find("lblLastMoneyLineChange", true).FirstOrDefault();

      if (lastMoneyLineChange != null)
        lastMoneyLineChange.Text = "";


      var lastTeamTotalsLineChange = (Label)sportsFrm.Controls.Find("lblLastTeamTotalsChange", true).FirstOrDefault();

      if (lastTeamTotalsLineChange != null)
        lastTeamTotalsLineChange.Text = "";

      var lastTotalPointsLineChange = (Label)sportsFrm.Controls.Find("lblLastTotalPntsChange", true).FirstOrDefault();

      if (lastTotalPointsLineChange != null)
        lastTotalPointsLineChange.Text = "";
    }

    private void ShowInFreePlayMode(object sender) {
      try {
        _childFrmProperty = MdiChildren.Length > 0 ? MdiChildren[1].GetType().GetProperty("IsFreePlay") : null;
        if (((CheckBox) sender).Checked) {
          if (_childFrmProperty != null) {
            _childFrmProperty.SetValue(MdiChildren[1], ((CheckBox) sender).Checked, null);
          }

          if (chbRollingIF.Checked)
            chbRollingIF.Checked = false;

          statBar.BackColor = Color.LightGreen;

          tssLabelWagerStatus.Text = WageringMode == "M" ? @"*** FREE PLAY Mode / MANUAL LINE mode ***" : @"*** FREE PLAY Mode / AUTOMATIC LINE mode ***";

          FillCustomerDataTextBoxes("F");
        }
        else {
          if (_childFrmProperty != null) {
            _childFrmProperty.SetValue(MdiChildren[1], ((CheckBox) sender).Checked, null);
          }

          statBar.BackColor = DefaultBackColor;

          tssLabelWagerStatus.Text = WageringMode == "M" ? "*** AVAILABLE BALANCE Mode / MANUAL LINE mode ***" : "*** AVAILABLE BALANCE Mode / AUTOMATIC LINE mode ***";

          FillCustomerDataTextBoxes("A");
        }
      }
      catch (Exception ex) {
        Log(ex);
      }
    }

    private void ShowInRollingIfBetMode(Boolean show) {
      if (show) {
        statBar.BackColor = Color.LightSalmon;

        tssLabelWagerStatus.Text = WageringMode == "M" ? "*** ROLLING IF BET Mode / MANUAL LINE mode ***" : "*** ROLLING IF BET Mode / AUTOMATIC LINE mode ***";

        if (chbFreePlay.Checked)
          chbFreePlay.Checked = false;

        if (RIfBetFromReadback == null) {
          using (var ticketInfoFrm = new FrmTicketInformation(this, 0, 0, GetCurrentDateTime(),
                                                       AppModuleInfo,
                                                       Customer.CustomerID, CurrentUserPermissions,
                                                       Customer.Currency, SoundCardDetected, null) {
                                                         RollingIfBetMode = true,
                                                         OpenPlaysMode = false,
                                                         EnableToEditPendingPlays = false
                                                       }) {
            ticketInfoFrm.Icon = Icon;
            ticketInfoFrm.ShowDialog();
          }
        }
      }
      else {
        statBar.BackColor = DefaultBackColor;

        tssLabelWagerStatus.Text = WageringMode == "M" ? "*** AVAILABLE BALANCE Mode / MANUAL LINE mode ***" : "*** AVAILABLE BALANCE Mode / AUTOMATIC LINE mode ***";

        CreateDefaultRifProperty();
        FillCustomerDataTextBoxes("A");
      }
    }

    private void StartAudioRecording(int ticketNumber) {
      if (_dr != null && _dr.TicketNumber == 0) {
        _dr.TicketNumber = ticketNumber;
      }
      else {
        _dr = new TicketDr(AppModuleInfo, ticketNumber);
      }
      _dr.RecordAudio();
    }

    public void ToggleFpAndRifChecks(bool enabled) {
      chbFreePlay.Enabled = enabled;
      if (!chbFreePlay.Enabled) chbFreePlay.Checked = false;
      chbRollingIF.Enabled = enabled;
      if (!chbRollingIF.Enabled) chbRollingIF.Checked = false;
    }

    private void ToggleReadbackWindowSize() {
      try {
        var frm = (FrmTicketConfirmation)MdiChildren[0];

        if (frm.WindowState != FormWindowState.Maximized && frm.WindowState != FormWindowState.Normal) return;
        frm.WindowState = frm.WindowState == FormWindowState.Maximized
                              ? FormWindowState.Normal
                              : FormWindowState.Maximized;
      }
      catch (Exception ex) {
        Log(ex);
      }
    }

    private string UserCanCreateLayoffWagers() {
      return LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.ACCEPT_LAYOFF_WAGERS) ? "False" : "True";
    }

    private void LoadHorseRacingWageringInterface() {
      if (ConfigurationManager.AppSettings["HorseRacingROOT_URL"] != null) {
      var currentUserPassword = (from u in CurrentUserPermissions where u.AccessType == LoginsAndProfiles.RUN_HORSES select u.HorsePassword).FirstOrDefault();
      if (!string.IsNullOrEmpty(currentUserPassword)) {
        var customerId = Customer.CustomerID.Trim();
        DisplayHorseWageringForm(new FrmHorseRacing(AppModuleInfo, customerId, currentUserPassword), "Horse");
        //DisplayHorseWageringForm(new frmLoadHorseRacingInterface(AppModuleInfo, customerId, currentUserPassword), "Horse");
      }
      else {
        MessageBox.Show(@"Unable to log you in.  Your Password could not be retrieved.  Please contact Prog/Tech Support.", @"No password found.", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
      }
    }
      else {
        MessageBox.Show(@"Problems Loading Horse Racing Interface.  Check Config for a Valid ROOT_URL", @"Not Loaded");
      }
    }

    private void HandleShowticketInfoRequest(int? ticketNumber, Boolean showInOpenPlayMode, Boolean enableEditPendingWager) {
      if (Customer == null && LastCustomer == null) {
        return;
      }
      String customerId;
      String currency;
      Common.SetCustomerAndCurrency(Customer, LastCustomer, out customerId, out currency);
      if (LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.REVIEW_WAGERS)) {
        using (var ticketInfoFrm = new FrmTicketInformation(this, 0, 0, GetCurrentDateTime(), AppModuleInfo, customerId, CurrentUserPermissions, currency, SoundCardDetected, ticketNumber)) {
          try {
            ticketInfoFrm.OpenPlaysMode = showInOpenPlayMode;
            ticketInfoFrm.EnableToEditPendingPlays = enableEditPendingWager;
            ticketInfoFrm.Icon = Icon;
            ticketInfoFrm.ShowDialog();
          }
          catch (Exception ex) {
            Log(ex);
          }
        }
      }
      else {
        MessageBox.Show(Environment.UserName + @" does not have permissions to " + LoginsAndProfiles.REVIEW_WAGERS);
      }
    }

    #endregion

    #region Protected Methods

    protected override bool ProcessCmdKey(ref Message msg, Keys keyData) {
      var baseResult = base.ProcessCmdKey(ref msg, keyData);

      if (keyData == (Keys.Control | Keys.Tab)) {
        return ActivateWageringForm("FrmSportAndGameSelection", baseResult);
      }
      return baseResult;
    }

    #endregion

    #region Events

    private void mdiTicketWriter_Load(object sender, EventArgs e) {
      InitializeTicketWriter();
    }

    private void aboutSIDToolStripMenuItem_Click(object sender, EventArgs e) {
      if (Resources.MdiTicketWriter_aboutSIDToolStripMenuItem_Click_About_box != null)
        MessageBox.Show(Resources.MdiTicketWriter_aboutSIDToolStripMenuItem_Click_About_box);
    }

    private void accountHistoryToolStripMenuItem_Click(object sender, EventArgs e) {
      if (Customer == null) {
        return;
      }

      OpenCustomerTransactionsForm(CurrentUserPermissions, Customer, ParameterList);
    }

    private void automaticToolStripMenuItem_Click(object sender, EventArgs e) {
      HandleAutomaticToolStripMenuItemClick();
    }

    private void HandleAutomaticToolStripMenuItemClick() {
      if (Customer == null) {
        return;
      }
      WageringMode = "A";
      manualLineToolStripMenuItem.Checked = false;
      _childFrmProperty = MdiChildren[1].GetType().GetProperty("WageringMode");
      if (_childFrmProperty != null) {
        _childFrmProperty.SetValue(MdiChildren[1], "A", null);
      }

      tssLabelWagerStatus.Text = @"*** AVAILABLE BALANCE Mode / AUTOMATIC LINE mode ***";

      if (chbFreePlay.Checked) {
        tssLabelWagerStatus.Text = @"*** FREE PLAY Mode / AUTOMATIC LINE mode ***";
      }

      if (chbRollingIF.Checked) {
        tssLabelWagerStatus.Text = @"*** ROLLING IF BET Mode / AUTOMATIC LINE mode ***";
      }
    }

    private void availableBalanceToolStripMenuItem_Click(object sender, EventArgs e) {
      if (Customer == null) {
        return;
      }

      freePlayToolStripMenuItem.Checked = false;
      rollingIfBetToolStripMenuItem.Checked = false;

      if (chbFreePlay.Checked)
        chbFreePlay.Checked = false;

      if (chbRollingIF.Checked)
        chbRollingIF.Checked = false;

      tssLabelWagerStatus.Text = WageringMode == "M" ? @"*** AVAILABLE BALANCE Mode / MANUAL LINE mode ***" : @"*** AVAILABLE BALANCE Mode / AUTOMATIC LINE mode ***";
    }

    private void btnAnswerPhone_Click(object sender, EventArgs e) {
      Ticket = new Ticket(); // changed before OpenCustomerDialog by DMN 20130825
      OpenCustomerDialog();

      if (SoundCardDetected && Params.RecordAudio && TicketNumber != 0)
        StartAudioRecording(TicketNumber);
    }

    private void btnAnswerPhone_MouseEnter(object sender, EventArgs e) {
      tsslMain.Text = Resources.MdiTicketWriter_btnAnswerPhone_MouseEnter_Answer_the_Phone_and_Get_Customer_Info;
    }

    private void btnArrangeWindows_Click(object sender, EventArgs e) {
      if (Customer == null) {
        return;
      }

      ArrangeWindows();
    }

    private void btnArrangeWindows_MouseEnter(object sender, EventArgs e) {
      tsslMain.Text = Resources.MdiTicketWriter_btnArrangeWindows_MouseEnter_Arrange_Windows;
    }

    private void btnHangUpPhone_Click(object sender, EventArgs e) {
      CloseCustomer();
    }

    private void btnHangUpPhone_MouseEnter(object sender, EventArgs e) {
      tsslMain.Text =
          Resources.MdiTicketWriter_btnHangUpPhone_MouseEnter_Hang_up_the_Phone_and_Post_Entered_Wagers;
    }

    private void btnNewHorseWager_Click(object sender, EventArgs e) {
      LoadHorseRacingWageringInterface();
    }

    private void btnNewHorseWager_MouseEnter(object sender, EventArgs e) {
      tsslMain.Text = Resources.MdiTicketWriter_btnNewHorseWager_MouseEnter_Start_a_New_Horse_Bet;
    }

    private void btnNewIfBet_Click(object sender, EventArgs e) {
      DisplayWageringForm(new FrmPlaceIfBet(AppModuleInfo), "If-Bet");
    }

    private void btnNewIfBet_MouseEnter(object sender, EventArgs e) {
      tsslMain.Text = Resources.MdiTicketWriter_btnNewIfBet_MouseEnter_Start_a_new_If_Bet;
    }

    private void btnNewManualPlay_Click(object sender, EventArgs e) {
      DisplayWageringForm(new FrmPlaceManual(LayoffWagersEnabled, AppModuleInfo), "Manual Play");
    }

    private void btnNewManualPlay_MouseEnter(object sender, EventArgs e) {
      tsslMain.Text = Resources.MdiTicketWriter_btnNewManualPlay_MouseEnter_Start_a_New_Manual_Play;
    }

    private void btnNewParlay_Click(object sender, EventArgs e) {
      DisplayWageringForm(new FrmPlaceParlay(AppModuleInfo), "Parlay");

    }

    private void btnNewParlay_MouseEnter(object sender, EventArgs e) {
      tsslMain.Text = Resources.MdiTicketWriter_btnNewParlay_MouseEnter_Start_New_Parlay;
    }

    private void btnNewTeaser_Click(object sender, EventArgs e) {
      DisplayWageringForm(new FrmPlaceTeaser(AppModuleInfo), "Teaser");
    }

    private void btnNewTeaser_MouseEnter(object sender, EventArgs e) {
      tsslMain.Text = Resources.MdiTicketWriter_btnNewTeaser_MouseEnter_Start_New_Teaser;
    }

    private void btnPropsAndFutures_Click(object sender, EventArgs e) {
      ShowPropsInSportsTreeView = !ShowPropsInSportsTreeView;
      displayContestsToolStripMenuItem.Checked = ShowPropsInSportsTreeView;
      ((FrmSportAndGameSelection)MdiChildren[1]).FillAvailableSportsTree(ShowPropsInSportsTreeView);
    }

    private void btnPropsAndFutures_MouseEnter(object sender, EventArgs e) {
      tsslMain.Text = Resources.MdiTicketWriter_btnPropsAndFutures_MouseEnter_Show_Available_Props_and_Futures;
    }

    private void btnShowCustomerFigures_Click(object sender, EventArgs e) {
      ShowCustomerFigures();
    }

    private void btnShowReadback_Click(object sender, EventArgs e) {
      ToggleReadbackWindowSize();
    }

    private void btnShowReadback_MouseEnter(object sender, EventArgs e) {
      tsslMain.Text = Resources.MdiTicketWriter_btnShowReadback_MouseEnter_Show_Readback;
    }

    private void calculatorToolStripMenuItem_Click(object sender, EventArgs e) {
      Calculator.Show();
    }

    private void chbFreePlay_CheckedChanged(object sender, EventArgs e) {
      ShowInFreePlayMode(sender);
    }

    private void chbRollingIF_CheckedChanged(object sender, EventArgs e) {
      ShowInRollingIfBetMode(((CheckBox)sender).Checked);
    }

    private void closeCustomerToolStripMenuItem_Click(object sender, EventArgs e) {
      CloseCustomer();
    }

    private void commentsToolStripMenuItem_Click(object sender, EventArgs e) {
      if (Customer == null) {
        return;
      }

      DisplayCommentsForTw();
    }

    private void dailyFiguresToolStripMenuItem_Click(object sender, EventArgs e) {
      if (Customer == null && LastCustomer == null) {
        return;
      }
      String customerId;
      String currency;
      Common.SetCustomerAndCurrency(Customer, LastCustomer, out customerId, out currency);
      using (var dfiguresFrm = new FrmDailyFigures(customerId, currency, AppModuleInfo, CurrentUserPermissions, SoundCardDetected)) {
        try {
          dfiguresFrm.Icon = Icon;
          dfiguresFrm.ShowDialog();
        }
        catch (Exception ex) {
          Log(ex);
        }
      }
    }

    private void displayContestsToolStripMenuItem_Click(object sender, EventArgs e) {
      if (Customer == null) {
        return;
      }

      var obj = (ToolStripMenuItem)sender;
      if (obj.Checked) {
        ShowPropsInSportsTreeView = true;
        ((FrmSportAndGameSelection)MdiChildren[1]).FillAvailableSportsTree(ShowPropsInSportsTreeView);
      }
      else {
        ShowPropsInSportsTreeView = false;
        ((FrmSportAndGameSelection)MdiChildren[1]).FillAvailableSportsTree(ShowPropsInSportsTreeView);

        var grvw = (DataGridView)Controls.Find(((FrmSportAndGameSelection)MdiChildren[1]).PropsGridViewInUse, true).FirstOrDefault();

        if (grvw != null && grvw.Rows.Count > 0) {
          grvw.Rows.Clear();
        }
      }
    }

    private void exitToolStripMenuItem_Click(object sender, EventArgs e) {
      Close();
    }

    private void freePlayToolStripMenuItem_Click(object sender, EventArgs e) {
      if (Customer == null) {
        return;
      }

      availableBalanceToolStripMenuItem.Checked = false;
      rollingIfBetToolStripMenuItem.Checked = false;
      chbFreePlay.Checked = true;
      if (chbRollingIF.Checked)
        chbRollingIF.Checked = false;

      tssLabelWagerStatus.Text = WageringMode == "M" ? "*** FREE PLAY Mode / MANUAL LINE mode ***" : "*** FREE PLAY Mode / AUTOMATIC LINE mode ***";
    }

    private void ifBetToolStripMenuItem_Click(object sender, EventArgs e) {
      if (Customer == null) {
        return;
      }

      DisplayWageringForm(new FrmPlaceIfBet(AppModuleInfo), "If-Bet");
    }

    private void MainToolbar_MouseLeave(object sender, EventArgs e) {
      tsslMain.Text = "";
    }

    private void manualLineToolStripMenuItem_Click(object sender, EventArgs e) {
      HandleManualLineToolStripMenuItemClick();
    }

    private void HandleManualLineToolStripMenuItemClick() {
      if (Customer == null) {
        return;
      }

      WageringMode = "M";
      automaticToolStripMenuItem.Checked = false;
      _childFrmProperty = MdiChildren[1].GetType().GetProperty("WageringMode");
      if (_childFrmProperty != null) {
        _childFrmProperty.SetValue(MdiChildren[1], "M", null);
      }

      tssLabelWagerStatus.Text = @"*** AVAILABLE BALANCE Mode / MANUAL LINE mode ***";

      if (chbFreePlay.Checked) {
        tssLabelWagerStatus.Text = @"*** FREE PLAY Mode / MANUAL LINE mode ***";
      }

      if (chbRollingIF.Checked) {
        tssLabelWagerStatus.Text = @"*** ROLLING IF BET Mode / MANUAL LINE mode ***";
      }
    }

    private void manualPlayToolStripMenuItem_Click(object sender, EventArgs e) {
      if (Customer == null) {
        return;
      }

      DisplayWageringForm(new FrmPlaceManual(LayoffWagersEnabled, AppModuleInfo), "Manual Play");
    }

    private void MdiTicketWriter_FormClosing(object sender, FormClosingEventArgs e) {
      if (WageringPanelName == null) {
        if (!CustomerWasClosed && Customer != null)
          CloseCustomer();
      }
      else {
        e.Cancel = true;
        MessageBox.Show(@"Please close the " + WageringPanelName + @" panel first.");
      }
      ReleaseLoginInUse();
    }

    protected override void OnShown(EventArgs e) {
      txtTeamOrAsterisk.Focus();
    }

    private void MdiTicketWriter_KeyDown(object sender, KeyEventArgs e) {
      if (Customer == null)
        return;

      if (e.KeyCode.ToString() == Keys.F9.ToString()) {
        FrmSportAndGameSelection.ToggleInactiveGamesDisplay();
        return;
      }

      if (e.Control && e.KeyCode == Keys.C) {
        CloseCustomer();
        return;
      }

      if (e.Shift && e.KeyCode.ToString() == Keys.OemQuestion.ToString()) {
        if (Customer != null)
          ShowCustomerFigures();
        return;
      }

      if (e.KeyCode.ToString() == Keys.F12.ToString()) {
        ToggleReadbackWindowSize();
      }

      if (e.Shift && (e.KeyCode.ToString() == Keys.Left.ToString() || e.KeyCode.ToString() == Keys.Right.ToString())) {
        //FrmSportAndGameSelection.Focus();
        FrmSportAndGameSelection.MoveToNextPeriodOption(e.KeyCode.ToString());
        return;
      }

      if (e.Control && e.KeyCode.ToString() == Keys.Tab.ToString()) {
        var formNamesList = new[] { "frmPlaceIfBet", "frmPlaceParlay", "frmPlaceTeaser" };

        foreach (var frmName in formNamesList) {
          var frm = FormF.GetFormByName(frmName, this);
          if (frm == null || frm.WindowState != FormWindowState.Normal) continue;
          frm.Focus();
          return;
        }
      }

      if (e.KeyCode.ToString() != Keys.Tab.ToString()) return;
      if (txtTeamOrAsterisk.Text.Trim() != "" && !txtTeamOrAsterisk.Focused) {
        txtTeamOrAsterisk.Enter -= txtTeamOrAsterisk_Enter;
        txtTeamOrAsterisk.Focus();
        txtTeamOrAsterisk.SelectAll();
        txtTeamOrAsterisk.Enter += txtTeamOrAsterisk_Enter;
        return;
      }

      if (FrmSportAndGameSelection == null) return;
      var tr = (TreeView)FrmSportAndGameSelection.Controls.Find("treSports", true).FirstOrDefault();

      if (tr == null) return;
      tr.Focus();

      if (FrmSportAndGameSelection != null) {
        FrmSportAndGameSelection.LoadSportsTreeSelection(tr);
      }
    }

    private void mdiTicketWriter_LocationChanged(object sender, EventArgs e) {
      ArrangeWindows();
    }

    private void mdiTicketWriter_Resize(object sender, EventArgs e) {
      ArrangeWindows();
    }

    private void openCustomerToolStripMenuItem_Click(object sender, EventArgs e) {
      OpenCustomerDialog();
    }

    private void openPlaysToolStripMenuItem_Click(object sender, EventArgs e) {
      if (Customer == null) {
        return;
      }

      if (LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.REVIEW_WAGERS)) {
        String wagerName;
        if (!CheckForNonMdiForms(out wagerName)) {
          using (var ticketInfoFrm = new FrmTicketInformation(this, 0, 0, GetCurrentDateTime(), AppModuleInfo, Customer.CustomerID.Trim(), CurrentUserPermissions, Customer.Currency.Trim(), SoundCardDetected, null)) {
            try {
              ticketInfoFrm.OpenPlaysMode = true;
              ticketInfoFrm.EnableToEditPendingPlays = true;
              ticketInfoFrm.Icon = Icon;
              ticketInfoFrm.ShowDialog();
            }
            catch (Exception ex) {
              Log(ex);
            }
          }
        }
        else {
          MessageBox.Show(@"Please complete the " + wagerName + @" in progress before checking open Plays", @"wager in Progress");
        }
      }
      else {
        MessageBox.Show(Environment.UserName + @" does not have permissions to " + LoginsAndProfiles.REVIEW_WAGERS);
      }
    }

    private void parlayToolStripMenuItem_Click(object sender, EventArgs e) {
      if (Customer == null) {
        return;
      }

      if (Customer.ParlayName != null && Customer.ParlayName.Trim() != "") {
        DisplayWageringForm(new FrmPlaceParlay(AppModuleInfo), "Parlay");
      }
      else {
        MessageBox.Show(@"Customer not assigned a valid Parlay Schedule." + Environment.NewLine + @"Cannot accept parlays for this customer until one is assigned", "");
      }
    }

    private void refreshBalanceToolStripMenuItem_Click(object sender, EventArgs e) {
      if (Customer == null) {
        return;
      }

      using (var info = new CustomerBalanceInfo(Customer.CustomerID.Trim(), AppModuleInfo)) {
        BalanceInfo = info;
        var balanceMode = "A"; //Free Play - F, Rolling If - R, Available Balance - A;
        if (chbFreePlay.Checked)
          balanceMode = "F";
        if (chbRollingIF.Checked)
          balanceMode = "R";

        FillCustomerDataTextBoxes(balanceMode);
        _storeWagerLimits = null;
      }
    }

    private void refreshLinesToolStripMenuItem_Click(object sender, EventArgs e) {
      if (Customer == null) {
        return;
      }

      RefreshGamesLines();
    }

    private void rollingIfBetToolStripMenuItem_Click(object sender, EventArgs e) {
      if (Customer == null) {
        return;
      }

      availableBalanceToolStripMenuItem.Checked = false;
      freePlayToolStripMenuItem.Checked = false;
      chbRollingIF.Checked = true;

      tssLabelWagerStatus.Text = WageringMode == "M" ? "*** ROLLING IF BET Mode / MANUAL LINE mode ***" : "*** ROLLING IF BET Mode / AUTOMATIC LINE mode ***";
    }

    private void statusBarToolStripMenuItem_Click(object sender, EventArgs e) {
      statBar.Visible = ((ToolStripMenuItem)sender).Checked;
    }

    private void tileAllToolStripMenuItem_Click(object sender, EventArgs e) {
      if (Customer == null) {
        return;
      }

      ArrangeWindows();
    }

    private void teaserToolStripMenuItem_Click(object sender, EventArgs e) {
      if (Customer == null) {
        return;
      }

      DisplayWageringForm(new FrmPlaceTeaser(AppModuleInfo), "Teaser");
    }

    private void ticketInfoToolStripMenuItem_Click(object sender, EventArgs e) {
      HandleShowticketInfoRequest(null, false, true);
    }

    private void toolbarToolStripMenuItem_Click(object sender, EventArgs e) {
      panToolbar.Visible = ((ToolStripMenuItem)sender).Checked;
    }

    private void txtTeamOrAsterisk_Enter(object sender, EventArgs e) {
      ((TextBox)sender).Text = "";
      lblSearchByRot.Text = "";
      EnableAllPeriodRadioButtons();
      ClearGridView();
    }

    private void txtTeamOrAsterisk_KeyDown(object sender, KeyEventArgs e) {
      var numericKey = e.KeyCode == Keys.NumPad0 || e.KeyCode == Keys.NumPad1 || e.KeyCode == Keys.NumPad2 || e.KeyCode == Keys.NumPad3 ||
          e.KeyCode == Keys.NumPad4 || e.KeyCode == Keys.NumPad5 || e.KeyCode == Keys.NumPad6 || e.KeyCode == Keys.NumPad7 || e.KeyCode == Keys.NumPad8 ||
          e.KeyCode == Keys.NumPad9 || e.KeyCode == Keys.D0 || e.KeyCode == Keys.D1 || e.KeyCode == Keys.D2 || e.KeyCode == Keys.D3 || e.KeyCode == Keys.D4 ||
          e.KeyCode == Keys.D5 || e.KeyCode == Keys.D6 || e.KeyCode == Keys.D7 || e.KeyCode == Keys.D8 || e.KeyCode == Keys.D9;

      if (txtTeamOrAsterisk.Text != "" && numericKey) {
        e.SuppressKeyPress = true;
        FrmSportAndGameSelection.FocusOnGamesOrContestsGridView();
        return;
      }

      if (numericKey) {
        var t = lblSearchByRot.Text;
        var kc = new KeysConverter();
        lblSearchByRot.Text = t + (kc.ConvertToString(e.KeyData) ?? "").Replace("NumPad", "");
      }

      if (e.KeyCode == Keys.Back) {
        var t = lblSearchByRot.Text;
        if (t.Length > 0) {
          t = t.Remove(t.Length - 1, 1);
          lblSearchByRot.Text = FrmSportAndGameSelection.KeyPressBuffer = t;
        }

        var y = txtTeamOrAsterisk.Text;
        if (y.Length > 0) {
          y = y.Remove(y.Length - 1, 1);
          txtTeamOrAsterisk.Text = y;
          if (txtTeamOrAsterisk.Text.Length > 0) {
            txtTeamOrAsterisk.SelectionStart = txtTeamOrAsterisk.Text.Length;
            txtTeamOrAsterisk.SelectionLength = 0;
          }
        }
      }

      if (!numericKey && e.KeyCode != Keys.Back) return;
      e.SuppressKeyPress = true;

      if (e.KeyCode != Keys.Enter && e.KeyCode != Keys.Back) {
        if ((GetCurrentDateTime() - FrmSportAndGameSelection.KeyPressLastTime).Seconds >= 1) FrmSportAndGameSelection.KeyPressBuffer = TwUtilities.GetKeyDownChar(e);
        else FrmSportAndGameSelection.KeyPressBuffer += TwUtilities.GetKeyDownChar(e);
      }

      FrmSportAndGameSelection.KeyPressLastTime = GetCurrentDateTime();
    }

    private void txtTeamOrAsterisk_KeyPress(object sender, KeyPressEventArgs e) {
      if (e.KeyChar.ToString(CultureInfo.InvariantCulture) != "\t" && e.KeyChar.ToString(CultureInfo.InvariantCulture) != "?") return;
      ((TextBox)sender).Text = "";
      e.Handled = true;
    }

    private void txtTeamOrAsterisk_TextChanged(object sender, EventArgs e) {
      ShowLatestChangesOrAlikeTeams(sender);
      EnableAllPeriodRadioButtons();
    }

    private void txtTeamOrAsterisk_KeyUp(object sender, KeyEventArgs e) {
      if (e.KeyCode != Keys.Enter || FrmSportAndGameSelection.KeyPressBuffer == "") return;
      FrmSportAndGameSelection.FindGameOrContestByRotation(FrmSportAndGameSelection.KeyPressBuffer);
      FrmSportAndGameSelection.KeyPressBuffer = "";
      lblSearchByRot.Text = "";
    }

    private void txtTeamOrAsterisk_Leave(object sender, EventArgs e) {
      lblSearchByRot.Text = "";
    }

    private void horseToolStripMenuItem_Click(object sender, EventArgs e) {
      LoadHorseRacingWageringInterface();
    }

    private void showTicketInfoAfterClosingToolStripMenuItem_Click(object sender, EventArgs e) {
      ((ToolStripMenuItem)sender).Checked = !((ToolStripMenuItem)sender).Checked;
      ShowTicketInfoAfterClosing = ((ToolStripMenuItem)sender).Checked;
    }

    #endregion
  }
}