﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using GUILibraries.Utilities;
using GUILibraries.Forms;
using SIDLibraries.Entities;
using SIDLibraries.Utilities;
using SIDLibraries.BusinessLayer;
using GUILibraries.Controls;
using GUILibraries.BusinessLayer;
using System.Drawing;
using Common = TicketWriter.Utilities.Common;
using GUILibraries;


namespace TicketWriter.UI {

  public sealed partial class FrmManualGameWager : SIDForm {

    #region private Constants

    private const string DRAW = "Draw";

    #endregion

    #region Public Properties

    public spCstGetCustomer_Result Customer {
      get {
        return _customer ??
               (_customer = ((MdiTicketWriter)FormF.GetFormByName("MdiTicketWriter", this)).Customer);
      }
    }

    public String FrmMode { get; set; }

    public FrmPlaceTeaser FrmPlaceTeaser {
      get {
        return _frmPlaceTeaser ??
               (_frmPlaceTeaser = (FrmPlaceTeaser)FormF.GetFormByName("FrmPlaceTeaser", this));
      }
    }

    public FrmPlaceParlay FrmPlaceParlay {
      get {
        return _frmPlaceParlay ??
               (_frmPlaceParlay = (FrmPlaceParlay)FormF.GetFormByName("FrmPlaceParlay", this));
      }
    }

    public FrmPlaceIfBet FrmPlaceIfBet {
      get {
        return _frmPlaceIfBet ??
               (_frmPlaceIfBet = (FrmPlaceIfBet)FormF.GetFormByName("FrmPlaceIfBet", this));
      }
    }

    public FrmSportAndGameSelection FrmSportAndGameSelection {
      get {
        return _frmSportAndGameSelection ??
               (_frmSportAndGameSelection =
                (FrmSportAndGameSelection)FormF.GetFormByName("FrmSportAndGameSelection", this));
      }
    }

    public string GameDataSource {
      get {
        return string.IsNullOrEmpty(_gameDataSource) ? "GameSelection" : _gameDataSource;
      }
      set {
        _gameDataSource = value;
      }
    }

    public Boolean IsFreePlay { get; set; }

    public MdiTicketWriter Mdi {
      get {
        return _mdiTicketWriter ??
               (_mdiTicketWriter = (MdiTicketWriter)FormF.GetFormByName("MdiTicketWriter", this));
      }
    }

    public FormParameterList ParameterList {
      get {
        return _parameterList ??
               (_parameterList =
                ((MdiTicketWriter)(FormF.GetFormByName("MdiTicketWriter", this))).ParameterList);
      }
    }

    public DateTime PeriodWagerCutoff { get; set; }

    public String PreviouslySelectedAmtWagered { get; set; }

    public String PreviouslySelectedLayoff { get; set; }

    public String PreviouslySelectedFixedPrice { get; set; }

    public String PreviouslySelectedOdds { get; set; }

    public String PreviouslySelectedPitcher1 { get; set; }

    public String PreviouslySelectedPitcher2 { get; set; }

    public String PreviouslySelectedToWinAmt { get; set; }

    public int WagerItemNumberToUpd { get; set; }

    public int WagerNumberToUpd { get; set; }

    public int TicketNumberToUpd { get; set; }

    public String WagerTypeMode { get; set; } // Risk or ToWin

    public List<Ticket.SelectedGameAndLine> OriginalGamesAndPeriodsInfo {
      get {
        if (Mdi.Ticket == null) return null;
        var list = Mdi.Ticket.OriginalGameOptions;

        return list;
      }
    }

    private spGLGetActiveGamesByCustomer_Result _selectedGamePeriodInfo;
    public spGLGetActiveGamesByCustomer_Result SelectedGamePeriodInfo {
        get
        {
            _selectedGamePeriodInfo = GetSelectedGamePeriodInfo();

            if (FrmMode == "Edit" && Mdi.TicketNumber != TicketNumberToUpd)
            {
                _selectedGamePeriodInfo = RestoreOriginalGameLineInfoGivenToCustomer(_selectedGamePeriodInfo); 
            }
            _selectedGamePeriodInfo = AdjustSelectedGamePrices(_selectedGamePeriodInfo);
            return _selectedGamePeriodInfo;
        }
        set { _selectedGamePeriodInfo = value; }
    }

    #endregion

    #region Private Properties

    private TextBox _txtLineAdj;
    private TextBox txtLineAdj
    {
        get
        {
            if (panTxtLineAdj.Controls.Count == 0)
                throw new Exception("Error creating txtLineAdj Control");
            var txtLineAdjControl = (TextBox)panTxtLineAdj.Controls.Find("txtLineAdj", true).FirstOrDefault();
            if (txtLineAdjControl != null)
            {
                _txtLineAdj = txtLineAdjControl;
            }
            return _txtLineAdj;
        }
        set
        {
            _txtLineAdj = value;
        }
    }

    private TextBox _txtPrice;
    private TextBox txtPrice
    {
        get
        {
            if (panTxtPrice.Controls.Count == 0)
                throw new Exception("Error creating txtPrice Control");
            var txtPrice = (TextBox)panTxtPrice.Controls.Find("txtPrice", true).FirstOrDefault();

            if (txtPrice != null)
            {
                _txtPrice = txtPrice;
            }

            return _txtPrice;
        }
        set
        {
            _txtPrice = value;
        }
    }


    #endregion

    #region Public vars

    #endregion

    #region Private vars

    private readonly String _currentCustomerBaseballAction;
    private readonly String _easternLineEnabled;
    public readonly String CurrentWagerTypeName;
    private spCstGetCustomer_Result _customer;
    private readonly String _customerHasStaticLines;
    private readonly List<spGLGetEasternLineConversionInfo_Result> _easternLineConversionInfo;
    private WagerTypeOptions _formattedWagerTypes;
    private FrmPlaceTeaser _frmPlaceTeaser;
    private FrmPlaceParlay _frmPlaceParlay;
    private FrmPlaceIfBet _frmPlaceIfBet;
    private FrmSportAndGameSelection _frmSportAndGameSelection;
    private string _gameDataSource;
    private readonly Boolean _layoffWagersAccepted;
    private List<string> _lineAndPrice;
    private MdiTicketWriter _mdiTicketWriter;
    private FormParameterList _parameterList;
    private readonly int _selectedGameNumber;
    private readonly List<spGLGetActiveGamesByCustomer_Result> _selectedGameOptions;
    private int _selectedGamePeriod;
    private readonly String _selectedGamePeriodDescription;
    //private readonly int _selectedGvRowNumber;
    public String SelectedPrice;
    public int SelectedRowNumberInGroup;
    private String _selectedSportType;
    public readonly String SelectedWagerType;
    private readonly List<spGLGetAvailablePeriodsBySport_Result> _sportPeriods;
    private List<TeamNameAndId> _teamsIds;
    public readonly FrmSportAndGameSelection ParentFrm;
    private double _teaserPoints;

    private String _wagerItemDescription;
    private String _itemWagerType;
    public String TotalsOu;
    private String _chosenTeamRotNum;
    private String _chosenTeamId;
    private string _chosenTeamIdDesc;
    private String _chosenLine;
    private String _chosenAmericanPrice;

    private bool _validKeyEntered;

    #endregion

    #region Structures

    private struct TeamNameAndId {
      public readonly String TeamId;
      public readonly String TeamRotNumber;

      public TeamNameAndId(String teamId, String teamRotNumber) {
        TeamId = teamId;
        TeamRotNumber = teamRotNumber;
      }
    }

    #endregion

    #region Constructors

    public FrmManualGameWager(String wagerTypeName, SIDForm parentForm, IEnumerable<spGLGetActiveGamesByCustomer_Result> gameOptions, List<spGLGetAvailablePeriodsBySport_Result> gamePeriods, int gameNumber, int gamePeriod, String selectedGamePeriodDescription, String priceType, /*int gvRowNumber,*/ int wagerType, int rowGroupNumber, String easternLineEnabled, List<spGLGetEasternLineConversionInfo_Result> easternLineConversion, Boolean layoffWagersAccepted)
      : base(parentForm.AppModuleInfo) {
      CurrentWagerTypeName = wagerTypeName;
      ParentFrm = (FrmSportAndGameSelection)parentForm;
      _easternLineEnabled = easternLineEnabled;
      _currentCustomerBaseballAction = ParentFrm.CurrentCustBaseballActionType;
      _easternLineConversionInfo = easternLineConversion;
      _selectedGameNumber = gameNumber;
      _selectedGamePeriod = gamePeriod;
      _selectedGamePeriodDescription = selectedGamePeriodDescription;
      SelectedPrice = priceType;
      _selectedGameOptions = gameOptions.OrderBy(x => x.SportType).ThenBy(x => x.ScheduleDate).ThenBy(x => x.ScheduleText).
                          ThenBy(x => x.Team1RotNum).ThenBy(x => x.SportSubType).ThenBy(x => x.GameDateTime).ThenBy(x => x.GameNum).ThenBy(x => x.PeriodNumber).
                          ThenByDescending(x => x.LineSeq).ToList();
      _sportPeriods = gamePeriods;
      /*_selectedGvRowNumber = gvRowNumber;*/
      SelectedWagerType = Enums.SelectedWagerTypeName(wagerType);
      SelectedRowNumberInGroup = rowGroupNumber;
      _customerHasStaticLines = ParentFrm.StaticLinesDuringCall;
      _layoffWagersAccepted = layoffWagersAccepted;

      InitializeComponent();
      SetCentsNumericInputs();
      BuildAdjustmentPriceControls();
    }

    #endregion

    #region Public Methods

    #endregion

    #region Private Methods

    private spGLGetActiveGamesByCustomer_Result AdjustSelectedGamePrices(spGLGetActiveGamesByCustomer_Result selectedGamePeriodInfo)
    {
        var currentGameInfo = selectedGamePeriodInfo;

        switch (SelectedWagerType)
        {
            case WagerType.SPREADNAME:
                if (txtPrice != null && !String.IsNullOrEmpty(txtPrice.Text))
                    currentGameInfo.SpreadAdj1 = currentGameInfo.SpreadAdj2 = int.Parse(txtPrice.Text);
                break;
            case WagerType.TOTALPOINTSNAME:
                if (SelectedRowNumberInGroup == 1 && txtPrice != null && txtPrice.Text.Length > 0)
                {
                    currentGameInfo.TtlPtsAdj1 = currentGameInfo.TtlPtsAdj2 = int.Parse(txtPrice.Text);
                }
                else
                {
                    if (txtPrice != null && txtPrice.Text.Length > 0)
                        currentGameInfo.TtlPtsAdj1 = currentGameInfo.TtlPtsAdj2 = int.Parse(txtPrice.Text);
                }
                break;
            case WagerType.MONEYLINENAME:
                if (txtPrice != null && txtPrice.Text.Length > 0)
                {
                    if (SelectedRowNumberInGroup == 1 || SelectedRowNumberInGroup == 2)
                        currentGameInfo.MoneyLine1 = currentGameInfo.MoneyLine1 = int.Parse(txtPrice.Text);
                    else
                        currentGameInfo.MoneyLineDraw = int.Parse(txtPrice.Text);
                }
                break;
            case WagerType.TEAMTOTALPOINTSOVERNAME:
                if (txtPrice != null && txtPrice.Text.Length > 0)
                {
                    if (SelectedRowNumberInGroup == 1)
                        currentGameInfo.Team1TtlPtsAdj1 = currentGameInfo.Team1TtlPtsAdj2 = int.Parse(txtPrice.Text);
                    else
                        currentGameInfo.Team2TtlPtsAdj1 = currentGameInfo.Team2TtlPtsAdj2 = int.Parse(txtPrice.Text);
                }
                break;
            case WagerType.TEAMTOTALPOINTSUNDERNAME:
                if (txtPrice != null && txtPrice.Text.Length > 0)
                {
                    if (SelectedRowNumberInGroup == 1)
                        currentGameInfo.Team1TtlPtsAdj1 = currentGameInfo.Team1TtlPtsAdj2 = int.Parse(txtPrice.Text);
                    else
                        currentGameInfo.Team2TtlPtsAdj1 = currentGameInfo.Team2TtlPtsAdj2 = int.Parse(txtPrice.Text);
                }
                break;
        }
        return currentGameInfo;
    }

    private void BuildAdjustmentPriceControls()
    {
        var txtLineAdjControl = new TxtLineAdjControl(_selectedSportType, this);
        txtLineAdjControl.SelectedWagerType = SelectedWagerType;
        txtLineAdjControl.Name = "txtLineAdj";
        txtLineAdjControl.Text = "";
        panTxtLineAdj.Controls.Add(txtLineAdjControl);

        var txtPriceControl = new TxtLinePriceControl(SelectedWagerType, this);
        txtPriceControl.Name = "txtPrice";
        txtPriceControl.Text = "";
        txtPriceControl.ControlLeft += txtPriceControl_ControlLeft;
        panTxtPrice.Controls.Add(txtPriceControl);
    }
      
    private void ClearAmountTextBoxes() {
      txtRisk.Text = txtToWin.Text = string.Empty;
    }

    private void ChangeAcceptButtonText() {
      var bntText = "Update " + CurrentWagerTypeName + " item";
      Common.WriteCaptionOnAcceptButton(CurrentWagerTypeName, bntText, btnPlaceBet, btnAddToParlay, btnAddToTeaser, btnAddToIfBet);
    }

    private string DetermineSelectedOdds(string itemWagerType) {
      string selectedOdds;

      if (itemWagerType == WagerType.MONEYLINE) {
        selectedOdds = txtPrice.Text;
      }
      else {
        selectedOdds = txtLineAdj.Text + " " + txtPrice.Text;
      }

      return selectedOdds;
    }

    private void GoToPlaceBet() {
      if (_itemWagerType == null)
        _itemWagerType = TwUtilities.GetSelectedWagerOption(SelectedWagerType, SelectedRowNumberInGroup, out TotalsOu);
      if (_chosenTeamId == null)
        _chosenTeamId = TwUtilities.GetChosenTeamId(SelectedRowNumberInGroup, SelectedGamePeriodInfo).Trim();
      if (_chosenTeamIdDesc == null)
        _chosenTeamIdDesc = TwUtilities.GetChosenTeamIdDescription(SelectedGamePeriodInfo, _itemWagerType, _chosenTeamId);
      if (_chosenTeamRotNum == null)
        _chosenTeamRotNum = GetChosenTeamRotNumber(_chosenTeamId);
      if (_chosenLine == null)
        _chosenLine = txtLineAdj.Text;
      if (_chosenAmericanPrice == null)
        _chosenAmericanPrice = txtPrice.Text;

      if (_itemWagerType == WagerType.MONEYLINE && _chosenLine == "") {
        _chosenLine = _chosenAmericanPrice;
      }

      //var item = WagerType.GetFromCode(_itemWagerType);

      DetermineSelectedPrice(txtPrice);

      if (CurrentWagerTypeName == "Teaser") {
        var frm = FormF.GetFormByName("frmPlaceTeaser", this);

        if (frm == null) {
          return;
        }

        var frmPt = (FrmPlaceTeaser)frm;
        if (frmPt.TeaserSportSpecsByTeaser != null && frmPt.TeaserName != null && SelectedGamePeriodInfo != null)
          _lineAndPrice = TwUtilities.CalculateTeaserAdjustedLine(frmPt.TeaserName, frmPt.TeaserSportSpecsByTeaser, _itemWagerType, TotalsOu, TwUtilities.GetSelectedSportType(SelectedGamePeriodInfo), TwUtilities.GetSelectedSportSubType(SelectedGamePeriodInfo), SelectedRowNumberInGroup, _chosenLine, out _teaserPoints);
      }
      else {
        //_lineAndPrice = CreateManualLineAndPrice();
        _lineAndPrice = new List<string> { _chosenLine, _chosenAmericanPrice };
      }

      double chosenLineToNumber = 0;
      if (_chosenLine.Length > 0)
        chosenLineToNumber = LinesFormat.GetNumericPoints(_chosenLine);

      if (_wagerItemDescription == null)
        _wagerItemDescription = FrmSportAndGameSelection.BuildWagerDescription(_chosenTeamRotNum, _chosenLine, CurrentWagerTypeName == "Teaser" ? _teaserPoints.ToString(CultureInfo.InvariantCulture) : _chosenAmericanPrice, _itemWagerType, TotalsOu, _chosenTeamId, _selectedSportType, SelectedGamePeriodInfo, CurrentWagerTypeName, SelectedPrice, chosenLineToNumber, chosenLineToNumber, chbPitcher1MustStart.Checked ? "Y" : "N", chbPitcher2MustStart.Checked ? "Y" : "N", chbFixedOdds.Checked ? "Y" : "N");

      FrmSportAndGameSelection.PlaceBet(FrmMode, txtRisk, txtToWin, CurrentWagerTypeName, _itemWagerType, WagerTypeMode,
          SelectedPrice, FrmMode == "Edit" ? TicketNumberToUpd : Mdi.TicketNumber, WagerNumberToUpd, WagerItemNumberToUpd, /*_selectedGvRowNumber,*/ SelectedRowNumberInGroup, _chosenLine, _chosenAmericanPrice,
          DetermineSelectedOdds(_itemWagerType), "", _chosenTeamIdDesc, _teaserPoints,
          chbPitcher1MustStart.Checked ? "Y" : "N", chbPitcher2MustStart.Checked ? "Y" : "N", chbFixedOdds.Checked ? "Y" : "N", TotalsOu, "", _wagerItemDescription, _lineAndPrice, SelectedGamePeriodInfo, IsFreePlay, "M", chbLayoffWager.Checked ? "Y" : "N", _easternLineEnabled, _easternLineConversionInfo, GameDataSource);

      var amountsStr = "";

      if (CurrentWagerTypeName == "Straight Bet" || (CurrentWagerTypeName == "If-Bet")) {
        amountsStr = " Risking: " + txtRisk.Text + ", to Win:" + txtToWin.Text;
      }

      Tag = "Added Manual " + _itemWagerType + " item to " + CurrentWagerTypeName + " in " + FrmMode + " mode. " + _frmSportAndGameSelection.Customer.CustomerID.Trim() + " "
          + TicketNumberToUpd.ToString() + "." + WagerNumberToUpd.ToString() + "." + WagerItemNumberToUpd.ToString() + "(" + _wagerItemDescription + ")" + amountsStr;
    }

    private void HandleKeyUpEvent(NumberTextBox txtRiskOrToWin, NumberTextBox numberTextBox, KeyEventArgs e) {
      txtRiskOrToWin.Text = "";
      if (numberTextBox.Text == "" || !KeyValidator.IsValidNumericKey(e)) return;
      SelectedPrice = "American";
      if (txtPrice.Text.Contains("."))
        SelectedPrice = "Decimal";
      if (txtPrice.Text.Contains("/"))
        SelectedPrice = "Fractional";

      var cleanSourceAmount = numberTextBox.Text.Replace(",", "");

      double adj;
      double convertedToAmerican;

      switch (SelectedPrice) {
        case "American":
          if (double.TryParse(txtPrice.Text, out adj)) {
            txtRiskOrToWin.Text = numberTextBox.AccessibleName == @"R" ? LineOffering.DetermineToWinAmount(double.Parse(cleanSourceAmount), adj, Params.IncludeCents).ToString(CultureInfo.InvariantCulture) : LineOffering.DetermineRiskAmount(double.Parse(cleanSourceAmount), adj, Params.IncludeCents).ToString(CultureInfo.InvariantCulture);
            WagerTypeMode = "Risk";
          }
          else {
            txtRiskOrToWin.Text = "";
            WagerTypeMode = "ToWin";
          }
          break;
        case "Decimal":
          if (double.TryParse(txtPrice.Text, out adj)) {
            convertedToAmerican = OddsTypes.DecimalToUs(adj);
            txtRiskOrToWin.Text = LineOffering.DetermineToWinAmount(double.Parse(cleanSourceAmount), convertedToAmerican, Params.IncludeCents).ToString(CultureInfo.InvariantCulture);
            WagerTypeMode = "Risk";
          }
          else {
            txtRiskOrToWin.Text = "";
            WagerTypeMode = "ToWin";
          }
          break;
        case "Fractional":
          int numerator;
          int denominator;
          if (int.TryParse(txtPrice.Text.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries)[0].Trim(), out numerator) && int.TryParse(txtPrice.Text.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries)[1].Trim(), out denominator)) {
            double convertedToDecimal = OddsTypes.FractionalToDecimal(numerator, denominator);
            convertedToAmerican = OddsTypes.DecimalToUs(convertedToDecimal);
            txtRiskOrToWin.Text = LineOffering.DetermineToWinAmount(double.Parse(cleanSourceAmount), convertedToAmerican, Params.IncludeCents).ToString(CultureInfo.InvariantCulture);
            WagerTypeMode = "Risk";
          }
          else {
            txtRiskOrToWin.Text = "";
            WagerTypeMode = "ToWin";
          }
          break;
      }
    }

    private void LoadFormInfo() {
      txtPeriod.Text = _selectedGamePeriodDescription ?? (from go in _selectedGameOptions where go.GameNum == _selectedGameNumber && go.PeriodNumber == _selectedGamePeriod select go.PeriodDescription).FirstOrDefault();

      TwUtilities.GetAvailableGamePeriodsForDropDown(cmbGoToGamePeriod, _selectedGameOptions, _selectedGameNumber, txtPeriod.Text);
      GetSelectedGamePeriodInfo();

      _selectedSportType = TwUtilities.GetSelectedSportType(SelectedGamePeriodInfo);
      if (FrmMode == "Edit") {
        WriteGameGeneralInfoInEditMode();
        ChangeAcceptButtonText();
      }
      else {
        WriteGameGeneralInfo();
      }
    }

    private spGLGetActiveGamesByCustomer_Result GetSelectedGamePeriodInfo()
    {
        spGLGetActiveGamesByCustomer_Result selectedGamePeriodInfo = new spGLGetActiveGamesByCustomer_Result();
        if (FrmMode == "Edit")
        {
            if (_customerHasStaticLines == "N" || Mdi.TicketNumber != TicketNumberToUpd)
            {
                selectedGamePeriodInfo = (from gO in _selectedGameOptions where gO.GameNum == _selectedGameNumber && gO.PeriodNumber == _selectedGamePeriod select gO).First();
            }
            else
            {
                List<Ticket.SelectedGameAndLine> selectedGamesInfo = null;
                if (OriginalGamesAndPeriodsInfo != null)
                    selectedGamesInfo = (from gO in OriginalGamesAndPeriodsInfo where gO.GameNum == _selectedGameNumber && gO.PeriodNumber == _selectedGamePeriod && 
                                             gO.TicketNumber == TicketNumberToUpd && gO.WagerNumber == WagerNumberToUpd && gO.ItemNumber == WagerItemNumberToUpd select gO).ToList();

                if (selectedGamesInfo != null)
                {
                    selectedGamePeriodInfo = selectedGamesInfo.First();
                }
            }
        }
        else
        {
            selectedGamePeriodInfo = (from gO in _selectedGameOptions where gO.GameNum == _selectedGameNumber && gO.PeriodNumber == _selectedGamePeriod select gO).First();
        }
        return selectedGamePeriodInfo;
    }

    private void WriteGameGeneralInfo() {
      if (SelectedGamePeriodInfo != null) {
        _formattedWagerTypes = LineOffering.FormatWagerTypesOptionsDisplay(SelectedGamePeriodInfo, SelectedPrice, false, _easternLineEnabled, _selectedSportType, _easternLineConversionInfo);

        if (SelectedGamePeriodInfo.Team1ID != null)
          txtGameInfo.Text = SelectedGamePeriodInfo.Team1ID.Trim();
        txtGameInfo.Text += @" at ";
        if (SelectedGamePeriodInfo.Team2ID != null)
          txtGameInfo.Text += SelectedGamePeriodInfo.Team2ID.Trim();
        txtGameInfo.Text += Environment.NewLine;
        if (SelectedGamePeriodInfo.GameDateTime != null)
          txtGameInfo.Text += DisplayDate.Format((DateTime)SelectedGamePeriodInfo.GameDateTime, DisplayDateFormat.DayNameAndLongDate);

        if (SelectedWagerType == "Spread" || SelectedWagerType == "MoneyLine") {
          switch (SelectedRowNumberInGroup) {
            case 1:
              if (SelectedGamePeriodInfo.Team1ID != null)
                txtChosenTeam.Text = SelectedGamePeriodInfo.Team1ID.Trim();
              if (SelectedWagerType == "Spread") {
                lblWagerType.Text = @"Spread:";
                if (_formattedWagerTypes.Spread.Contains("  ")) {
                  txtLineAdj.Text = _formattedWagerTypes.Spread.Split(' ')[0];
                  txtPrice.Text = _formattedWagerTypes.Spread.Split(' ')[2];
                }
              }
              else {
                lblWagerType.Visible = false;
                txtLineAdj.Visible = false;
                txtPrice.Text = _formattedWagerTypes.MoneyLine1;
              }
              break;
            case 2:
              if (SelectedGamePeriodInfo.Team2ID != null)
                txtChosenTeam.Text = SelectedGamePeriodInfo.Team2ID.Trim();
              if (SelectedWagerType == "Spread") {
                lblWagerType.Text = @"Spread:";
                if (_formattedWagerTypes.Spread2.Contains("  ")) {
                  txtLineAdj.Text = _formattedWagerTypes.Spread2.Split(' ')[0];
                  txtPrice.Text = _formattedWagerTypes.Spread2.Split(' ')[2];
                }
              }
              else {
                lblWagerType.Visible = false;
                txtLineAdj.Visible = false;
                txtPrice.Text = _formattedWagerTypes.MoneyLine2;
                txtPrice.Focus();
              }
              break;
            case 3:
              lblWagerType.Visible = false;
              txtLineAdj.Visible = false;
              txtChosenTeam.Text = @"Draw";
              txtPrice.Text = _formattedWagerTypes.MoneyLineDraw;
              break;
          }
        }
        else {
          switch (SelectedRowNumberInGroup) {
            case 1:
              switch (SelectedWagerType) {
                case "Totals":
                  txtChosenTeam.Text = @"Total Points";
                  lblWagerType.Text = @"Total Over:";
                  if (_formattedWagerTypes.TotalPoints.Contains("  ")) {
                    txtLineAdj.Text = _formattedWagerTypes.TotalPoints.Split(' ')[1];
                    txtPrice.Text = _formattedWagerTypes.TotalPoints.Split(' ')[3];
                  }
                  break;
                case "TeamTotalsOver":
                  if (SelectedGamePeriodInfo.Team1ID != null)
                    txtChosenTeam.Text = @"Team Total - " + SelectedGamePeriodInfo.Team1ID.Trim();
                  lblWagerType.Text = @"Team Over:";
                  if (_formattedWagerTypes.TeamTotalsOver.Contains("  ")) {
                    txtLineAdj.Text = _formattedWagerTypes.TeamTotalsOver.Split(' ')[1];
                    txtPrice.Text = _formattedWagerTypes.TeamTotalsOver.Split(' ')[3];
                  }
                  break;
                case "TeamTotalsUnder":
                  if (SelectedGamePeriodInfo.Team1ID != null)
                    txtChosenTeam.Text = @"Team Total - " + SelectedGamePeriodInfo.Team1ID.Trim();
                  lblWagerType.Text = @"Team Under:";
                  if (_formattedWagerTypes.TeamTotalsUnder.Contains("  ")) {
                    txtLineAdj.Text = _formattedWagerTypes.TeamTotalsUnder.Split(' ')[1];
                    txtPrice.Text = _formattedWagerTypes.TeamTotalsUnder.Split(' ')[3];
                  }
                  break;
              }
              break;
            case 2:
              switch (SelectedWagerType) {
                case "Totals":
                  txtChosenTeam.Text = @"Total Points";
                  lblWagerType.Text = @"Total Under:";
                  if (_formattedWagerTypes.TotalPoints2.Contains("  ")) {
                    txtLineAdj.Text = _formattedWagerTypes.TotalPoints2.Split(' ')[1];
                    txtPrice.Text = _formattedWagerTypes.TotalPoints2.Split(' ')[3];
                  }
                  break;
                case "TeamTotalsOver":
                  if (SelectedGamePeriodInfo.Team1ID != null)
                    txtChosenTeam.Text = @"Team Total - " + SelectedGamePeriodInfo.Team1ID.Trim();
                  lblWagerType.Text = @"Team Over:";
                  if (_formattedWagerTypes.TeamTotalsOver2.Contains("  ")) {
                    txtLineAdj.Text = _formattedWagerTypes.TeamTotalsOver2.Split(' ')[1];
                    txtPrice.Text = _formattedWagerTypes.TeamTotalsOver2.Split(' ')[3];
                  }
                  break;
                case "TeamTotalsUnder":
                  if (SelectedGamePeriodInfo.Team1ID != null)
                    txtChosenTeam.Text = @"Team Total - " + SelectedGamePeriodInfo.Team1ID.Trim();
                  lblWagerType.Text = @"Team Under:";
                  if (_formattedWagerTypes.TeamTotalsUnder2.Contains("  ")) {
                    txtLineAdj.Text = _formattedWagerTypes.TeamTotalsUnder2.Split(' ')[1];
                    txtPrice.Text = _formattedWagerTypes.TeamTotalsUnder2.Split(' ')[3];
                  }
                  break;
              }
              break;
            case 3:
              txtChosenTeam.Text = @"Draw";
              break;
          }
        }

        if (SelectedGamePeriodInfo.SportType != null && SelectedGamePeriodInfo.SportType.Trim() == SportTypes.BASEBALL) {
          if (SelectedGamePeriodInfo.ListedPitcher1 != null)
            chbPitcher1MustStart.Text = chbPitcher1MustStart.Text.Replace("Who", SelectedGamePeriodInfo.ListedPitcher1.Trim());
          if (SelectedGamePeriodInfo.ListedPitcher2 != null)
            chbPitcher2MustStart.Text = chbPitcher2MustStart.Text.Replace("Who", SelectedGamePeriodInfo.ListedPitcher2.Trim());
          switch (_currentCustomerBaseballAction) {
            case "Listed":
              chbFixedOdds.Checked = false;
              chbPitcher1MustStart.Checked = true;
              chbPitcher2MustStart.Checked = true;
              break;
            case "Action":
              chbFixedOdds.Checked = false;
              chbPitcher1MustStart.Checked = false;
              chbPitcher2MustStart.Checked = false;
              break;
            case "Fixed":
              chbFixedOdds.Checked = true;
              chbPitcher1MustStart.Checked = false;
              chbPitcher2MustStart.Checked = false;
              break;
          }
          chbFixedOdds.Enabled = _selectedGamePeriod == 0;
        }
      }
    }

    private spGLGetActiveGamesByCustomer_Result RestoreOriginalGameLineInfoGivenToCustomer(spGLGetActiveGamesByCustomer_Result existingGamePeriodInfo)
    {
        spGLGetActiveGamesByCustomer_Result selectedGamePeriodInfo = existingGamePeriodInfo;
        if (Mdi != null && Mdi.PendingPlayItemsInDb != null && Mdi.PendingPlayItemsInDb.Count > 0)
        {
            var item = (from i in Mdi.PendingPlayItemsInDb
                        where i.TicketNumber == TicketNumberToUpd && i.WagerNumber == WagerNumberToUpd && i.ItemNumber ==
                            WagerItemNumberToUpd
                        select i).FirstOrDefault();
            if (item == null)
                return selectedGamePeriodInfo;
            switch (SelectedWagerType)
            {
                case WagerType.SPREADNAME:
                    selectedGamePeriodInfo.Spread = item.AdjSpread;
                    if (selectedGamePeriodInfo.Team1ID.Trim().ToLower() == item.ChosenTeamID.Trim().ToLower())
                    {
                        selectedGamePeriodInfo.SpreadAdj1 = item.FinalMoney;
                        selectedGamePeriodInfo.SpreadDecimal1 = item.FinalDecimal;
                        selectedGamePeriodInfo.SpreadDenominator1 = item.FinalDenominator;
                        selectedGamePeriodInfo.SpreadNumerator1 = item.FinalNumerator;
                    }
                    else
                    {
                        selectedGamePeriodInfo.SpreadAdj2 = item.FinalMoney;
                        selectedGamePeriodInfo.SpreadDecimal2 = item.FinalDecimal;
                        selectedGamePeriodInfo.SpreadDenominator2 = item.FinalDenominator;
                        selectedGamePeriodInfo.SpreadNumerator2 = item.FinalNumerator;
                    }
                    break;
                case WagerType.MONEYLINENAME:
                    if (selectedGamePeriodInfo.Team1ID.Trim().ToLower() == item.ChosenTeamID.Trim().ToLower())
                    {
                        selectedGamePeriodInfo.MoneyLine1 = item.FinalMoney;
                        selectedGamePeriodInfo.MoneyLineDecimal1 = item.FinalDecimal;
                        selectedGamePeriodInfo.MoneyLineNumerator1 = item.FinalNumerator;
                        selectedGamePeriodInfo.MoneyLineDenominator1 = item.FinalDenominator;
                    }
                    else
                        if (selectedGamePeriodInfo.Team2ID.Trim().ToLower() == item.ChosenTeamID.Trim().ToLower())
                        {
                            selectedGamePeriodInfo.MoneyLine2 = item.FinalMoney;
                            selectedGamePeriodInfo.MoneyLineDecimal2 = item.FinalDecimal;
                            selectedGamePeriodInfo.MoneyLineNumerator2 = item.FinalNumerator;
                            selectedGamePeriodInfo.MoneyLineDenominator2 = item.FinalDenominator;
                        }
                        else
                        {
                            selectedGamePeriodInfo.MoneyLineDraw = item.FinalMoney;
                            selectedGamePeriodInfo.MoneyLineDecimalDraw = item.FinalDecimal;
                            selectedGamePeriodInfo.MoneyLineNumeratorDraw = item.FinalNumerator;
                            selectedGamePeriodInfo.MoneyLineDenominatorDraw = item.FinalDenominator;
                        }
                    break;
                case WagerType.TOTALPOINTSNAME:
                    selectedGamePeriodInfo.TotalPoints = item.AdjTotalPoints;
                    if (item.TotalPointsOU == "O")
                    {
                        selectedGamePeriodInfo.TtlPtsAdj1 = item.FinalMoney;
                        selectedGamePeriodInfo.TtlPointsDecimal1 = item.FinalDecimal;
                        selectedGamePeriodInfo.TtlPointsNumerator1 = item.FinalNumerator;
                        selectedGamePeriodInfo.TtlPointsDenominator1 = item.FinalDenominator;
                    }
                    else
                    {
                        selectedGamePeriodInfo.TtlPtsAdj2 = item.FinalMoney;
                        selectedGamePeriodInfo.TtlPointsDecimal2 = item.FinalDecimal;
                        selectedGamePeriodInfo.TtlPointsNumerator2 = item.FinalNumerator;
                        selectedGamePeriodInfo.TtlPointsDenominator2 = item.FinalDenominator;
                    }

                    break;
                case WagerType.TEAMTOTALPOINTSOVERNAME:
                    if (selectedGamePeriodInfo.Team1ID.Trim().ToLower() == item.ChosenTeamID.Trim().ToLower())
                    {
                        selectedGamePeriodInfo.Team1TotalPoints = item.AdjTotalPoints;
                        selectedGamePeriodInfo.Team1TtlPtsAdj1 = item.FinalMoney;
                        selectedGamePeriodInfo.Team1TtlPtsDecimal1 = item.FinalDecimal;
                        selectedGamePeriodInfo.Team1TtlPtsNumerator1 = item.FinalNumerator;
                        selectedGamePeriodInfo.Team1TtlPtsDenominator1 = item.FinalDenominator;
                    }
                    else
                    {
                        selectedGamePeriodInfo.Team2TotalPoints = item.AdjTotalPoints;
                        selectedGamePeriodInfo.Team2TtlPtsAdj1 = item.FinalMoney;
                        selectedGamePeriodInfo.Team2TtlPtsDecimal1 = item.FinalDecimal;
                        selectedGamePeriodInfo.Team2TtlPtsNumerator1 = item.FinalNumerator;
                        selectedGamePeriodInfo.Team2TtlPtsDenominator1 = item.FinalDenominator;
                    }
                    break;
                case WagerType.TEAMTOTALPOINTSUNDERNAME:
                    if (selectedGamePeriodInfo.Team1ID.Trim().ToLower() == item.ChosenTeamID.Trim().ToLower())
                    {
                        selectedGamePeriodInfo.Team1TotalPoints = item.AdjTotalPoints;
                        selectedGamePeriodInfo.Team1TtlPtsAdj2 = item.FinalMoney;
                        selectedGamePeriodInfo.Team1TtlPtsDecimal2 = item.FinalDecimal;
                        selectedGamePeriodInfo.Team1TtlPtsNumerator2 = item.FinalNumerator;
                        selectedGamePeriodInfo.Team1TtlPtsDenominator2 = item.FinalDenominator;
                    }
                    else
                    {
                        selectedGamePeriodInfo.Team2TotalPoints = item.AdjTotalPoints;
                        selectedGamePeriodInfo.Team2TtlPtsAdj2 = item.FinalMoney;
                        selectedGamePeriodInfo.Team2TtlPtsDecimal2 = item.FinalDecimal;
                        selectedGamePeriodInfo.Team2TtlPtsNumerator2 = item.FinalNumerator;
                        selectedGamePeriodInfo.Team2TtlPtsDenominator2 = item.FinalDenominator;
                    }
                    break;
            }
      }
        return selectedGamePeriodInfo;
    }

    private void WriteGameGeneralInfoInEditMode() {
      if (SelectedGamePeriodInfo != null) {
        if (SelectedGamePeriodInfo.Team1ID != null)
          txtGameInfo.Text = SelectedGamePeriodInfo.Team1ID.Trim();
        txtGameInfo.Text += @" at ";
        if (SelectedGamePeriodInfo.Team2ID != null)
          txtGameInfo.Text += SelectedGamePeriodInfo.Team2ID.Trim();
        txtGameInfo.Text += Environment.NewLine;
        if (SelectedGamePeriodInfo.GameDateTime != null)
          txtGameInfo.Text += DisplayDate.Format((DateTime)SelectedGamePeriodInfo.GameDateTime, DisplayDateFormat.DayNameAndLongDate);

        if (SelectedWagerType == "Spread" || SelectedWagerType == "MoneyLine") {
          if (SelectedWagerType == "Spread") {
            lblWagerType.Text = @"Spread:";
            if (PreviouslySelectedOdds != null) {
              if (PreviouslySelectedOdds.Contains(" ")) {
                txtLineAdj.Text = PreviouslySelectedOdds.Split(' ')[0];
                txtPrice.Text = PreviouslySelectedOdds.Split(' ')[1];
                txtLineAdj.Focus();
              }
            }
          }
          else {
            lblWagerType.Visible = false;
            txtLineAdj.Visible = false;
            txtPrice.Text = PreviouslySelectedOdds;
            txtPrice.Focus();
          }

          switch (SelectedRowNumberInGroup) {
            case 1:
              if (SelectedGamePeriodInfo.Team1ID != null)
                txtChosenTeam.Text = SelectedGamePeriodInfo.Team1ID.Trim();
              break;
            case 2:
              if (SelectedGamePeriodInfo.Team2ID != null)
                txtChosenTeam.Text = SelectedGamePeriodInfo.Team2ID.Trim();
              break;
            case 3:
              txtChosenTeam.Text = @"Draw";
              break;
          }
        }
        else {
          if (PreviouslySelectedOdds != null) {
            if (PreviouslySelectedOdds.Contains(" ")) {
              txtLineAdj.Text = PreviouslySelectedOdds.Split(' ')[0];
              txtPrice.Text = PreviouslySelectedOdds.Split(' ')[1];
              txtLineAdj.Focus();
            }
          }
          switch (SelectedRowNumberInGroup) {
            case 1:
            case 2:
              switch (SelectedWagerType) {
                case "Totals":
                  txtChosenTeam.Text = @"Total Points";
                  lblWagerType.Text = SelectedRowNumberInGroup == 1 ? @"Total Over:" : @"Total Under:";
                  break;
                case "TeamTotalsOver":
                  if (SelectedGamePeriodInfo.Team1ID != null)
                    txtChosenTeam.Text = @"Team Total - " + SelectedGamePeriodInfo.Team1ID.Trim();
                  lblWagerType.Text = @"Team Over:";
                  break;
                case "TeamTotalsUnder":
                  if (SelectedGamePeriodInfo.Team1ID != null)
                    txtChosenTeam.Text = @"Team Total - " + SelectedGamePeriodInfo.Team1ID.Trim();
                  lblWagerType.Text = @"Team Under:";
                  break;
              }
              break;
            case 3:
              txtChosenTeam.Text = @"Draw";
              break;
          }
        }
      }
      if (PreviouslySelectedAmtWagered != null) {
        txtRisk.Text = PreviouslySelectedAmtWagered;
      }

      if (PreviouslySelectedToWinAmt != null) {
        txtToWin.Text = PreviouslySelectedToWinAmt;
      }

      if (SelectedGamePeriodInfo != null && (SelectedGamePeriodInfo.SportType != null && SelectedGamePeriodInfo.SportType.Trim() == SportTypes.BASEBALL)) {
        if (SelectedGamePeriodInfo.ListedPitcher1 != null)
          chbPitcher1MustStart.Text = chbPitcher1MustStart.Text.Replace("Who", SelectedGamePeriodInfo.ListedPitcher1.Trim());
        if (SelectedGamePeriodInfo.ListedPitcher2 != null)
          chbPitcher2MustStart.Text = chbPitcher2MustStart.Text.Replace("Who", SelectedGamePeriodInfo.ListedPitcher2.Trim());

        chbFixedOdds.Checked = PreviouslySelectedFixedPrice == "Y";
        chbLayoffWager.Checked = PreviouslySelectedLayoff == "Y";
        chbPitcher1MustStart.Checked = PreviouslySelectedPitcher1 == "Y";
        chbPitcher2MustStart.Checked = PreviouslySelectedPitcher2 == "Y";
      }
    }

    private bool IsValidWager() {
      return Common.IsValidWager(txtRisk, txtToWin, grpRiskAmount, grpWinAmount, Mdi, TicketNumberToUpd, WagerNumberToUpd, FrmMode, ParentFrm.CurrentCustomerCurrency);
    }

    private string GetChosenTeamRotNumber(String teamId) {
      var chosenTeamIdRotNum = "";

      _teamsIds = new List<TeamNameAndId>
                {
                    new TeamNameAndId(SelectedGamePeriodInfo.Team1ID, SelectedGamePeriodInfo.Team1RotNum.ToString()),
                    new TeamNameAndId(SelectedGamePeriodInfo.Team2ID, SelectedGamePeriodInfo.Team2RotNum.ToString())
                };


      if (teamId == DRAW) {
        chosenTeamIdRotNum = SelectedGamePeriodInfo.DrawRotNum.ToString();
      }
      else {
        foreach (var t in _teamsIds.Where(t => t.TeamId.Trim() == teamId.Trim())) {
          chosenTeamIdRotNum = t.TeamRotNumber.ToString(CultureInfo.InvariantCulture);
          break;
        }
      }

      return chosenTeamIdRotNum;
    }

    private void SetLayout() {
      if (txtLineAdj.Visible) {
        txtLineAdj.Focus();
      }
      else
        txtPrice.Focus();

      switch (CurrentWagerTypeName) {
        case "Straight Bet":
          AcceptButton = btnPlaceBet;
          btnPlaceBet.Visible = true;
          btnAddToParlay.Visible = false;
          btnAddToTeaser.Visible = false;
          btnAddToIfBet.Visible = false;
          chbLayoffWager.Visible = true;
          break;

        case "If-Bet":
          AcceptButton = btnAddToIfBet;
          btnAddToIfBet.Visible = true;
          btnAddToParlay.Visible = false;
          btnAddToTeaser.Visible = false;
          btnPlaceBet.Visible = false;
          chbLayoffWager.Visible = false;

          var ifBetFrm = (FrmPlaceIfBet)FormF.GetFormByName("FrmPlaceIfBet", this);
          if (ifBetFrm != null) {
            if (ifBetFrm.ActionReverse != null || ifBetFrm.BirdCage != null) {
              panWagerAmounts.Visible = false;
            }
            else {
              panWagerAmounts.Visible = true;
            }
          }


          break;
        case "Teaser":
          AcceptButton = btnAddToTeaser;
          btnAddToTeaser.Visible = true;
          btnAddToParlay.Visible = false;
          btnAddToIfBet.Visible = false;
          btnPlaceBet.Visible = false;
          panWagerAmounts.Visible = false;
          chbLayoffWager.Visible = false;
          break;
        case "Parlay":
          AcceptButton = btnAddToParlay;
          btnAddToParlay.Visible = true;
          btnAddToTeaser.Visible = false;
          btnAddToIfBet.Visible = false;
          btnPlaceBet.Visible = false;
          panWagerAmounts.Visible = false;
          chbLayoffWager.Visible = false;
          break;
      }

      if (_selectedSportType != SportTypes.BASEBALL) {
        panFixedOdds.Visible = false;
        panPitcher1.Visible = false;
        panPitcher2.Visible = false;
      }
      else {
        if (SelectedWagerType != "MoneyLine") {
          panPitcher1.Visible = false;
          panPitcher2.Visible = false;
        }
        chbFixedOdds.Enabled = _selectedGamePeriod == 0;
        switch (_currentCustomerBaseballAction) {
          case "Listed":
            if (_selectedGamePeriod == 1) {
              chbPitcher1MustStart.Enabled = false;
              chbPitcher2MustStart.Enabled = false;
            }
            else {
              chbPitcher1MustStart.Enabled = true;
              chbPitcher2MustStart.Enabled = true;
            }
            break;
          case "Action":
            if (_selectedGamePeriod == 1) {
              chbPitcher1MustStart.Enabled = false;
              chbPitcher2MustStart.Enabled = false;
            }
            else {
              chbPitcher1MustStart.Enabled = true;
              chbPitcher2MustStart.Enabled = true;
            }
            break;
          case "Fixed":
            chbPitcher1MustStart.Enabled = false;
            chbPitcher2MustStart.Enabled = false;
            break;
        }
      }

      chbLayoffWager.Visible = _layoffWagersAccepted;
    }

    private void SetFormPosition() {
      if (FrmMode != "Edit") return;
      if (CurrentWagerTypeName == "Straight Bet") return;
      var parentWidth = FrmSportAndGameSelection.Width;
      var parentHeight = FrmSportAndGameSelection.Height;

      Location = new Point(parentWidth / 2 - Width / 2, parentHeight / 2 - Height / 4);

      StartPosition = FormStartPosition.Manual;
    }

    private void ValidateWagerItem() {

      _itemWagerType = TwUtilities.GetSelectedWagerOption(SelectedWagerType, SelectedRowNumberInGroup, out TotalsOu);
      WagerType.GetFromCode(_itemWagerType);
      _chosenTeamId = TwUtilities.GetChosenTeamId(SelectedRowNumberInGroup, SelectedGamePeriodInfo).Trim();
      _chosenTeamRotNum = GetChosenTeamRotNumber(_chosenTeamId);
      _chosenTeamIdDesc = TwUtilities.GetChosenTeamIdDescription(SelectedGamePeriodInfo, _itemWagerType, _chosenTeamId);
      _chosenLine = txtLineAdj.Text;
      if (_chosenAmericanPrice == null)
        _chosenAmericanPrice = txtPrice.Text;

      if (_itemWagerType == WagerType.MONEYLINE && _chosenLine == "") {
        _chosenLine = _chosenAmericanPrice;
      }

      DetermineSelectedPrice(txtPrice);

      double chosenLineToNumber = 0;
      if (_chosenLine.Length > 0)
        chosenLineToNumber = LinesFormat.GetNumericPoints(_chosenLine);

      _wagerItemDescription = FrmSportAndGameSelection.BuildWagerDescription(_chosenTeamRotNum, LinesFormat.GetNumericPoints(_chosenLine).ToString(CultureInfo.InvariantCulture), CurrentWagerTypeName == "Teaser" ? _teaserPoints.ToString(CultureInfo.InvariantCulture) : _chosenAmericanPrice, _itemWagerType, TotalsOu, _chosenTeamId, _selectedSportType, SelectedGamePeriodInfo, CurrentWagerTypeName, SelectedPrice, chosenLineToNumber, chosenLineToNumber, chbPitcher1MustStart.Checked ? "Y" : "N", chbPitcher2MustStart.Checked ? "Y" : "N", chbFixedOdds.Checked ? "Y" : "N");

      if (IsValidWager()) {
        var gameStatus = SelectedGamePeriodInfo.Status;
        var cutoffTime = SelectedGamePeriodInfo.PeriodWagerCutoff.GetValueOrDefault();

        if (gameStatus == null) return;
        switch (gameStatus) {
          case GamesAndLines.EVENT_COMPLETED: //Complete
          case GamesAndLines.EVENT_OFFLINE: //Offline
          case GamesAndLines.EVENT_CANCELLED: //Cancelled?
            MessageBox.Show(@"The game period has already started or is not currently available", @"Not available");
            txtToWin.Focus();
            return;
          case GamesAndLines.EVENT_CIRCLED: //Circled
          case GamesAndLines.EVENT_OPEN: //Open
            Boolean continueValidating;
            Boolean cutoffTimeBypassed;
            FrmSportAndGameSelection.CheckIfWagerCutoffHasBeenReached(this, LoginsAndProfiles.ACCEPT_ANY_WAGER, SelectedGamePeriodInfo, _wagerItemDescription, txtRisk.Text, txtToWin.Text, cutoffTime, out continueValidating, out cutoffTimeBypassed);
            if (!continueValidating)
              return;
            Boolean minWagerBypassed;
            FrmSportAndGameSelection.CheckIfBelowMinimumWager(this, LoginsAndProfiles.ACCEPT_ANY_WAGER, txtRisk.Text, out continueValidating, out minWagerBypassed);
            if (!continueValidating)
              return;
            bool availableFundsOk;
            FrmSportAndGameSelection.CheckForAvailableFunds(this, FrmMode, TicketNumberToUpd, WagerNumberToUpd, WagerItemNumberToUpd, LoginsAndProfiles.ACCEPT_ANY_WAGER, SelectedGamePeriodInfo, null, _wagerItemDescription, txtRisk.Text, txtToWin.Text, IsFreePlay, out continueValidating, out availableFundsOk); //Validate if user has sufficientFunds for wager
            if (!continueValidating)
              return;
            Boolean wagerLimitOk;
            FrmSportAndGameSelection.CheckIfExceedsWagerLimit(this, FrmMode, TicketNumberToUpd, WagerNumberToUpd, WagerItemNumberToUpd, LoginsAndProfiles.ACCEPT_ANY_WAGER, SelectedGamePeriodInfo, _wagerItemDescription, txtRisk.Text, txtToWin.Text, _itemWagerType, _chosenTeamId, TotalsOu, out continueValidating, out wagerLimitOk);

            if (cutoffTimeBypassed && minWagerBypassed && availableFundsOk && wagerLimitOk) {
              GoToPlaceBet();
              Close();
            }

            break;
        }
      }
      else {
        txtToWin.Focus();
      }
    }

    private void SetCentsNumericInputs() {
      txtRisk.AllowCents = Params.IncludeCents;
      txtToWin.AllowCents = Params.IncludeCents;
    }

    private void DetermineSelectedPrice(TextBox txtPriceField) {
      SelectedPrice = "American";

      if (txtPriceField.Text.Contains(".")) {
        SelectedPrice = "Decimal";
        double adj;
        if (double.TryParse(txtPriceField.Text, out adj)) {
          _chosenAmericanPrice = OddsTypes.DecimalToUs(adj).ToString(CultureInfo.InvariantCulture);
        }
      }

      if (txtPriceField.Text.Contains("/")) {
        SelectedPrice = "Fractional";
        int numerator;
        int denominator;
        if (int.TryParse(txtPriceField.Text.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries)[0].Trim(), out numerator) && int.TryParse(txtPriceField.Text.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries)[1].Trim(), out denominator)) {
          double convertedToDecimal = OddsTypes.FractionalToDecimal(numerator, denominator);
          _chosenAmericanPrice = OddsTypes.DecimalToUs(convertedToDecimal).ToString(CultureInfo.InvariantCulture);
        }
      }
      _chosenAmericanPrice = Math.Round(double.Parse(_chosenAmericanPrice)).ToString(CultureInfo.InvariantCulture);
    }

    private void RelocateForm() {
      var frmTy = 0;
      if (CurrentWagerTypeName == "Teaser") {
        if (FrmPlaceTeaser == null) return;
        frmTy = FrmPlaceTeaser.Location.Y;
      }

      if (CurrentWagerTypeName == "Parlay") {
        if (FrmPlaceParlay == null) return;
        frmTy = FrmPlaceParlay.Location.Y;
      }

      if (CurrentWagerTypeName == "If-Bet") {
        if (FrmPlaceIfBet == null) return;
        frmTy = FrmPlaceIfBet.Location.Y;
      }
      var thisX = Location.X;
      var thisH = Height;
      Location = new Point(thisX, (frmTy - thisH));
    }

    #endregion

    #region Events

    private void FrmManualGameWager_Load(object sender, EventArgs e)
    {
        LoadFormInfo();
        SetLayout();
        SetFormPosition();
        if (CurrentWagerTypeName == "Teaser" || CurrentWagerTypeName == "Parlay" || CurrentWagerTypeName == "If-Bet")
            RelocateForm();
        if (FrmSportAndGameSelection.ShowWagerLimitsInFrm.IsActive())
        {
            FrmSportAndGameSelection.ShowWagerLimitsInFrm.FrmManualGameWager = this;
            FrmSportAndGameSelection.ShowWagerLimitsInFrm.DisplayMaxManualWagerLimits();
        }
    }

    private void btnAddToIfBet_Click(object sender, EventArgs e) {
      GoToPlaceBet();
      Close();
    }

    private void btnAddToParlay_Click(object sender, EventArgs e) {
      GoToPlaceBet();
      Close();
    }

    private void btnAddToTeaser_Click(object sender, EventArgs e) {
      GoToPlaceBet();
      Close();
    }

    private void btnCancel_Click(object sender, EventArgs e) {
      Close();
    }

    private void btnPlaceBet_Click(object sender, EventArgs e) {
      ValidateWagerItem();
    }

    private void cmbGoToGamePeriod_SelectedIndexChanged(object sender, EventArgs e) {
      var cBx = (ComboBox)sender;
      var periodName = cBx.Text.Trim();

      var periodNumber = _sportPeriods == null ? (from go in _selectedGameOptions where go.GameNum == _selectedGameNumber && go.PeriodDescription.Trim() == periodName select go.PeriodNumber).FirstOrDefault() : FrmSportAndGameSelection.GetPeriodNumber(periodName, _sportPeriods);

      txtPeriod.Text = periodName;
      _selectedGamePeriod = periodNumber;

      SelectedGamePeriodInfo = (from gO in _selectedGameOptions where gO.GameNum == _selectedGameNumber && gO.PeriodNumber == _selectedGamePeriod select gO).First();

      WriteGameGeneralInfo();

      TwUtilities.GetAvailableGamePeriodsForDropDown(((ComboBox)sender), _selectedGameOptions, _selectedGameNumber, txtPeriod.Text);
    }

    private void txtPriceControl_ControlLeft(object sender, EventArgs e)
    {
        if (!FrmSportAndGameSelection.ShowWagerLimitsInFrm.IsActive())
            return;
        FrmSportAndGameSelection.ShowWagerLimitsInFrm.FrmManualGameWager = this;
        FrmSportAndGameSelection.ShowWagerLimitsInFrm.DisplayMaxManualWagerLimits();
    }
      
    private void txtRisk_KeyUp(object sender, KeyEventArgs e) {
      HandleKeyUpEvent(txtToWin, ((NumberTextBox)sender), e);
    }

    private void txtToWin_KeyUp(object sender, KeyEventArgs e) {
      HandleKeyUpEvent(txtRisk, ((NumberTextBox)sender), e);
    }

    private void FrmManualGameWager_FormClosing(object sender, FormClosingEventArgs e) {
      if (FrmMode == "Edit") {
        if (CurrentWagerTypeName == "If-Bet") {
          var ifBetFrm = (FrmPlaceIfBet)FormF.GetFormByName("FrmPlaceIfBet", this);
          if (ifBetFrm != null) {
            if (ifBetFrm.ActionReverse != null || ifBetFrm.BirdCage != null) {
              ifBetFrm.FillItemsNumbers();
            }
          }
        }
      }
      SelectedGamePeriodInfo = null;
    }

    private void chbFixedOdds_CheckedChanged(object sender, EventArgs e) {
      var chb = (CheckBox)sender;

      if (chb.Checked) {
        chbPitcher1MustStart.Checked = false;
        chbPitcher1MustStart.Enabled = false;

        chbPitcher2MustStart.Checked = false;
        chbPitcher2MustStart.Enabled = false;
      }
      else {
        chbPitcher1MustStart.Checked = true;
        chbPitcher1MustStart.Enabled = true;

        chbPitcher2MustStart.Checked = true;
        chbPitcher2MustStart.Enabled = true;
      }
    }

    private void txtRisk_TextChanged(object sender, EventArgs e) {
        var w = ((NumberTextBox)sender).Text.Replace(",", "");
      if (w == "") return;
      double textVal;
      double.TryParse(w, out textVal);
      if (!(textVal > -1)) return;
      ((NumberTextBox)sender).AllowCents = false;
      ((NumberTextBox)sender).TextChanged -= txtRisk_TextChanged;
      ((NumberTextBox)sender).Text = String.Format("{0:#,###,###}", double.Parse(w));
      ((NumberTextBox)sender).SelectionStart = ((NumberTextBox)sender).Text.Length;
      ((NumberTextBox)sender).SelectionLength = 0;
      ((NumberTextBox)sender).TextChanged += txtRisk_TextChanged;
      ((NumberTextBox)sender).AllowCents = true;
    }

    private void txtToWin_TextChanged(object sender, EventArgs e) {
        var r = ((NumberTextBox)sender).Text.Replace(",", "");
      if (r == "") return;
      double textVal;
      double.TryParse(r, out textVal);
      if (!(textVal > -1)) return;
      ((NumberTextBox)sender).AllowCents = false;
      ((NumberTextBox)sender).TextChanged -= txtToWin_TextChanged;
      ((NumberTextBox)sender).Text = String.Format("{0:#,###,###}", double.Parse(r));
      ((NumberTextBox)sender).SelectionStart = ((NumberTextBox)sender).Text.Length;
      ((NumberTextBox)sender).SelectionLength = 0;
      ((NumberTextBox)sender).TextChanged += txtToWin_TextChanged;
      ((NumberTextBox)sender).AllowCents = true;
    }

    #endregion

  }
}