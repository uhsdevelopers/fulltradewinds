﻿namespace TicketWriter.UI
{
    partial class FrmCustomerTransactions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblLastVerifiedBalance = new System.Windows.Forms.Label();
            this.txtLastVerifiedBalance = new System.Windows.Forms.TextBox();
            this.txtCurrency = new System.Windows.Forms.TextBox();
            this.chbShowLastMonth = new System.Windows.Forms.CheckBox();
            this.dgvwTransactions = new System.Windows.Forms.DataGridView();
            this.dgvwTransactionsDocumentNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvwTransactionsDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FormatedDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FormattedCredit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FormattedDebit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FormattedBalance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvwTransactionsGradeNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvwTransactionsTranType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bndsrcTransactions = new System.Windows.Forms.BindingSource(this.components);
            this.btnTicketDetails = new System.Windows.Forms.Button();
            this.lblFreePlayTransactions = new System.Windows.Forms.Label();
            this.dgvwFreePlayTransactions = new System.Windows.Forms.DataGridView();
            this.dgvwFreePlayTransactionsDocumentNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvwFreePlayTransactionsDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descriptionDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvwFreePlayTransactionsGradeNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvwFreePlayTransactionsTranType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bndsrcFreePlays = new System.Windows.Forms.BindingSource(this.components);
            this.btnFreePlayTicketsDetails = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvwTransactions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bndsrcTransactions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvwFreePlayTransactions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bndsrcFreePlays)).BeginInit();
            this.SuspendLayout();
            // 
            // lblLastVerifiedBalance
            // 
            this.lblLastVerifiedBalance.AutoSize = true;
            this.lblLastVerifiedBalance.Location = new System.Drawing.Point(12, 16);
            this.lblLastVerifiedBalance.Name = "lblLastVerifiedBalance";
            this.lblLastVerifiedBalance.Size = new System.Drawing.Size(110, 13);
            this.lblLastVerifiedBalance.TabIndex = 0;
            this.lblLastVerifiedBalance.Text = "Last Verified Balance:";
            // 
            // txtLastVerifiedBalance
            // 
            this.txtLastVerifiedBalance.Enabled = false;
            this.txtLastVerifiedBalance.Location = new System.Drawing.Point(130, 13);
            this.txtLastVerifiedBalance.Name = "txtLastVerifiedBalance";
            this.txtLastVerifiedBalance.ReadOnly = true;
            this.txtLastVerifiedBalance.Size = new System.Drawing.Size(100, 20);
            this.txtLastVerifiedBalance.TabIndex = 1;
            this.txtLastVerifiedBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtCurrency
            // 
            this.txtCurrency.Enabled = false;
            this.txtCurrency.Location = new System.Drawing.Point(237, 13);
            this.txtCurrency.Name = "txtCurrency";
            this.txtCurrency.ReadOnly = true;
            this.txtCurrency.Size = new System.Drawing.Size(177, 20);
            this.txtCurrency.TabIndex = 2;
            this.txtCurrency.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // chbShowLastMonth
            // 
            this.chbShowLastMonth.AutoSize = true;
            this.chbShowLastMonth.Location = new System.Drawing.Point(584, 13);
            this.chbShowLastMonth.Name = "chbShowLastMonth";
            this.chbShowLastMonth.Size = new System.Drawing.Size(118, 17);
            this.chbShowLastMonth.TabIndex = 4;
            this.chbShowLastMonth.Text = "Show Last 30 Days";
            this.chbShowLastMonth.UseVisualStyleBackColor = true;
            this.chbShowLastMonth.CheckedChanged += new System.EventHandler(this.chbShowLastMonth_CheckedChanged);
            // 
            // dgvwTransactions
            // 
            this.dgvwTransactions.AllowUserToAddRows = false;
            this.dgvwTransactions.AllowUserToDeleteRows = false;
            this.dgvwTransactions.AutoGenerateColumns = false;
            this.dgvwTransactions.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvwTransactions.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgvwTransactions.BackgroundColor = System.Drawing.Color.White;
            this.dgvwTransactions.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvwTransactions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvwTransactions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvwTransactionsDocumentNumber,
            this.dgvwTransactionsDate,
            this.FormatedDate,
            this.descriptionDataGridViewTextBoxColumn,
            this.FormattedCredit,
            this.FormattedDebit,
            this.FormattedBalance,
            this.dgvwTransactionsGradeNumber,
            this.dgvwTransactionsTranType});
            this.dgvwTransactions.DataSource = this.bndsrcTransactions;
            this.dgvwTransactions.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvwTransactions.Location = new System.Drawing.Point(15, 36);
            this.dgvwTransactions.MultiSelect = false;
            this.dgvwTransactions.Name = "dgvwTransactions";
            this.dgvwTransactions.ReadOnly = true;
            this.dgvwTransactions.RowHeadersVisible = false;
            this.dgvwTransactions.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvwTransactions.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvwTransactions.Size = new System.Drawing.Size(704, 179);
            this.dgvwTransactions.StandardTab = true;
            this.dgvwTransactions.TabIndex = 5;
            this.dgvwTransactions.SelectionChanged += new System.EventHandler(this.dgvwTransactions_SelectionChanged);
            // 
            // dgvwTransactionsDocumentNumber
            // 
            this.dgvwTransactionsDocumentNumber.DataPropertyName = "DocumentNumber";
            this.dgvwTransactionsDocumentNumber.HeaderText = "DocumentNumber";
            this.dgvwTransactionsDocumentNumber.Name = "dgvwTransactionsDocumentNumber";
            this.dgvwTransactionsDocumentNumber.ReadOnly = true;
            this.dgvwTransactionsDocumentNumber.Visible = false;
            // 
            // dgvwTransactionsDate
            // 
            this.dgvwTransactionsDate.DataPropertyName = "Date";
            this.dgvwTransactionsDate.HeaderText = "Date";
            this.dgvwTransactionsDate.Name = "dgvwTransactionsDate";
            this.dgvwTransactionsDate.ReadOnly = true;
            this.dgvwTransactionsDate.Visible = false;
            // 
            // FormatedDate
            // 
            this.FormatedDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.FormatedDate.DataPropertyName = "FormatedDate";
            this.FormatedDate.HeaderText = "Date";
            this.FormatedDate.Name = "FormatedDate";
            this.FormatedDate.ReadOnly = true;
            this.FormatedDate.Width = 175;
            // 
            // descriptionDataGridViewTextBoxColumn
            // 
            this.descriptionDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.descriptionDataGridViewTextBoxColumn.DataPropertyName = "Description";
            this.descriptionDataGridViewTextBoxColumn.HeaderText = "Description";
            this.descriptionDataGridViewTextBoxColumn.Name = "descriptionDataGridViewTextBoxColumn";
            this.descriptionDataGridViewTextBoxColumn.ReadOnly = true;
            this.descriptionDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.descriptionDataGridViewTextBoxColumn.Width = 250;
            // 
            // FormattedCredit
            // 
            this.FormattedCredit.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.FormattedCredit.DataPropertyName = "FormattedCredit";
            this.FormattedCredit.HeaderText = "Credit";
            this.FormattedCredit.Name = "FormattedCredit";
            this.FormattedCredit.ReadOnly = true;
            this.FormattedCredit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FormattedCredit.Width = 90;
            // 
            // FormattedDebit
            // 
            this.FormattedDebit.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.FormattedDebit.DataPropertyName = "FormattedDebit";
            this.FormattedDebit.HeaderText = "Debit";
            this.FormattedDebit.Name = "FormattedDebit";
            this.FormattedDebit.ReadOnly = true;
            this.FormattedDebit.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FormattedDebit.Width = 90;
            // 
            // FormattedBalance
            // 
            this.FormattedBalance.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.FormattedBalance.DataPropertyName = "FormattedBalance";
            this.FormattedBalance.HeaderText = "Balance";
            this.FormattedBalance.Name = "FormattedBalance";
            this.FormattedBalance.ReadOnly = true;
            this.FormattedBalance.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FormattedBalance.Width = 90;
            // 
            // dgvwTransactionsGradeNumber
            // 
            this.dgvwTransactionsGradeNumber.DataPropertyName = "GradeNumber";
            this.dgvwTransactionsGradeNumber.HeaderText = "GradeNumber";
            this.dgvwTransactionsGradeNumber.Name = "dgvwTransactionsGradeNumber";
            this.dgvwTransactionsGradeNumber.ReadOnly = true;
            this.dgvwTransactionsGradeNumber.Visible = false;
            // 
            // dgvwTransactionsTranType
            // 
            this.dgvwTransactionsTranType.DataPropertyName = "TranType";
            this.dgvwTransactionsTranType.HeaderText = "TranType";
            this.dgvwTransactionsTranType.Name = "dgvwTransactionsTranType";
            this.dgvwTransactionsTranType.ReadOnly = true;
            this.dgvwTransactionsTranType.Visible = false;
            // 
            // bndsrcTransactions
            // 
            this.bndsrcTransactions.DataSource = typeof(SIDLibraries.BusinessLayer.CustomerTransaction);
            this.bndsrcTransactions.Sort = "";
            // 
            // btnTicketDetails
            // 
            this.btnTicketDetails.AutoSize = true;
            this.btnTicketDetails.Enabled = false;
            this.btnTicketDetails.Location = new System.Drawing.Point(319, 221);
            this.btnTicketDetails.Name = "btnTicketDetails";
            this.btnTicketDetails.Size = new System.Drawing.Size(91, 23);
            this.btnTicketDetails.TabIndex = 6;
            this.btnTicketDetails.Text = "Ticket Details...";
            this.btnTicketDetails.UseVisualStyleBackColor = true;
            this.btnTicketDetails.Click += new System.EventHandler(this.btnTicketDetails_Click);
            // 
            // lblFreePlayTransactions
            // 
            this.lblFreePlayTransactions.AutoSize = true;
            this.lblFreePlayTransactions.Location = new System.Drawing.Point(12, 255);
            this.lblFreePlayTransactions.Name = "lblFreePlayTransactions";
            this.lblFreePlayTransactions.Size = new System.Drawing.Size(115, 13);
            this.lblFreePlayTransactions.TabIndex = 7;
            this.lblFreePlayTransactions.Text = "Free Play Transactions";
            // 
            // dgvwFreePlayTransactions
            // 
            this.dgvwFreePlayTransactions.AutoGenerateColumns = false;
            this.dgvwFreePlayTransactions.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvwFreePlayTransactions.BackgroundColor = System.Drawing.Color.White;
            this.dgvwFreePlayTransactions.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvwFreePlayTransactions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvwFreePlayTransactions.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvwFreePlayTransactionsDocumentNumber,
            this.dgvwFreePlayTransactionsDate,
            this.dataGridViewTextBoxColumn3,
            this.descriptionDataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dgvwFreePlayTransactionsGradeNumber,
            this.dgvwFreePlayTransactionsTranType});
            this.dgvwFreePlayTransactions.DataSource = this.bndsrcFreePlays;
            this.dgvwFreePlayTransactions.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvwFreePlayTransactions.Location = new System.Drawing.Point(13, 271);
            this.dgvwFreePlayTransactions.MultiSelect = false;
            this.dgvwFreePlayTransactions.Name = "dgvwFreePlayTransactions";
            this.dgvwFreePlayTransactions.RowHeadersVisible = false;
            this.dgvwFreePlayTransactions.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvwFreePlayTransactions.Size = new System.Drawing.Size(702, 150);
            this.dgvwFreePlayTransactions.StandardTab = true;
            this.dgvwFreePlayTransactions.TabIndex = 8;
            this.dgvwFreePlayTransactions.SelectionChanged += new System.EventHandler(this.dgvwFreePlayTransactions_SelectionChanged);
            // 
            // dgvwFreePlayTransactionsDocumentNumber
            // 
            this.dgvwFreePlayTransactionsDocumentNumber.DataPropertyName = "DocumentNumber";
            this.dgvwFreePlayTransactionsDocumentNumber.HeaderText = "DocumentNumber";
            this.dgvwFreePlayTransactionsDocumentNumber.Name = "dgvwFreePlayTransactionsDocumentNumber";
            this.dgvwFreePlayTransactionsDocumentNumber.Visible = false;
            // 
            // dgvwFreePlayTransactionsDate
            // 
            this.dgvwFreePlayTransactionsDate.DataPropertyName = "Date";
            this.dgvwFreePlayTransactionsDate.HeaderText = "Date";
            this.dgvwFreePlayTransactionsDate.Name = "dgvwFreePlayTransactionsDate";
            this.dgvwFreePlayTransactionsDate.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "FormatedDate";
            this.dataGridViewTextBoxColumn3.HeaderText = "Date";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 175;
            // 
            // descriptionDataGridViewTextBoxColumn1
            // 
            this.descriptionDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.descriptionDataGridViewTextBoxColumn1.DataPropertyName = "Description";
            this.descriptionDataGridViewTextBoxColumn1.HeaderText = "Description";
            this.descriptionDataGridViewTextBoxColumn1.Name = "descriptionDataGridViewTextBoxColumn1";
            this.descriptionDataGridViewTextBoxColumn1.Width = 250;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn4.DataPropertyName = "FormattedCredit";
            this.dataGridViewTextBoxColumn4.HeaderText = "Credit";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 90;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn5.DataPropertyName = "FormattedDebit";
            this.dataGridViewTextBoxColumn5.HeaderText = "Debit";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 90;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewTextBoxColumn6.DataPropertyName = "FormattedBalance";
            this.dataGridViewTextBoxColumn6.HeaderText = "Balance";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 90;
            // 
            // dgvwFreePlayTransactionsGradeNumber
            // 
            this.dgvwFreePlayTransactionsGradeNumber.DataPropertyName = "GradeNumber";
            this.dgvwFreePlayTransactionsGradeNumber.HeaderText = "GradeNumber";
            this.dgvwFreePlayTransactionsGradeNumber.Name = "dgvwFreePlayTransactionsGradeNumber";
            this.dgvwFreePlayTransactionsGradeNumber.Visible = false;
            // 
            // dgvwFreePlayTransactionsTranType
            // 
            this.dgvwFreePlayTransactionsTranType.DataPropertyName = "TranType";
            this.dgvwFreePlayTransactionsTranType.HeaderText = "TranType";
            this.dgvwFreePlayTransactionsTranType.Name = "dgvwFreePlayTransactionsTranType";
            this.dgvwFreePlayTransactionsTranType.Visible = false;
            // 
            // bndsrcFreePlays
            // 
            this.bndsrcFreePlays.DataSource = typeof(SIDLibraries.BusinessLayer.CustomerTransaction);
            // 
            // btnFreePlayTicketsDetails
            // 
            this.btnFreePlayTicketsDetails.AutoSize = true;
            this.btnFreePlayTicketsDetails.Enabled = false;
            this.btnFreePlayTicketsDetails.Location = new System.Drawing.Point(293, 427);
            this.btnFreePlayTicketsDetails.Name = "btnFreePlayTicketsDetails";
            this.btnFreePlayTicketsDetails.Size = new System.Drawing.Size(142, 23);
            this.btnFreePlayTicketsDetails.TabIndex = 9;
            this.btnFreePlayTicketsDetails.Text = "Free PLay Ticket Details...";
            this.btnFreePlayTicketsDetails.UseVisualStyleBackColor = true;
            this.btnFreePlayTicketsDetails.Click += new System.EventHandler(this.btnFreePlayTicketsDetails_Click);
            // 
            // FrmCustomerTransactions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(729, 462);
            this.Controls.Add(this.btnFreePlayTicketsDetails);
            this.Controls.Add(this.dgvwFreePlayTransactions);
            this.Controls.Add(this.lblFreePlayTransactions);
            this.Controls.Add(this.btnTicketDetails);
            this.Controls.Add(this.dgvwTransactions);
            this.Controls.Add(this.chbShowLastMonth);
            this.Controls.Add(this.txtCurrency);
            this.Controls.Add(this.txtLastVerifiedBalance);
            this.Controls.Add(this.lblLastVerifiedBalance);
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCustomerTransactions";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Customer Transactions";
            this.Load += new System.EventHandler(this.frmCustomerTransactions_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmCustomerTransactions_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvwTransactions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bndsrcTransactions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvwFreePlayTransactions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bndsrcFreePlays)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblLastVerifiedBalance;
        private System.Windows.Forms.TextBox txtLastVerifiedBalance;
        private System.Windows.Forms.TextBox txtCurrency;
        private System.Windows.Forms.CheckBox chbShowLastMonth;
        private System.Windows.Forms.DataGridView dgvwTransactions;
        private System.Windows.Forms.Button btnTicketDetails;
        private System.Windows.Forms.Label lblFreePlayTransactions;
        private System.Windows.Forms.DataGridView dgvwFreePlayTransactions;
        private System.Windows.Forms.Button btnFreePlayTicketsDetails;
        private System.Windows.Forms.BindingSource bndsrcTransactions;
        private System.Windows.Forms.BindingSource bndsrcFreePlays;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvwTransactionsDocumentNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvwTransactionsDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn FormatedDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn descriptionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn FormattedCredit;
        private System.Windows.Forms.DataGridViewTextBoxColumn FormattedDebit;
        private System.Windows.Forms.DataGridViewTextBoxColumn FormattedBalance;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvwTransactionsGradeNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvwTransactionsTranType;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvwFreePlayTransactionsDocumentNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvwFreePlayTransactionsDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn descriptionDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvwFreePlayTransactionsGradeNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvwFreePlayTransactionsTranType;
    }
}