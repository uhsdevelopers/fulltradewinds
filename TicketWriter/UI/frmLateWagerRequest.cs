﻿using System;
using System.Windows.Forms;
using GUILibraries.Utilities;
using SIDLibraries.Entities;
using System.Reflection;
using SIDLibraries.BusinessLayer;
using GUILibraries.Forms;

namespace TicketWriter.UI {
  public sealed partial class FrmLateWagerRequest : SIDForm {

    #region Private Constants

    //private const String requestType = "Late Wager Request";
    //private const string requestType2 = "Request to Exceed Maximum Wager";
    //private const string requestType2 = "Insufficient Funds Request";

    #endregion

    #region Private Properties

    #endregion

    #region Public Properties

    private MdiTicketWriter Mdi {
      get {
        return _mdiTicketWriter ??
               (_mdiTicketWriter = (MdiTicketWriter)FormF.GetFormByName("MdiTicketWriter", this));
      }
    }

    #endregion

    #region Public vars

    public String WagerDesc;

    #endregion

    #region Private vars

    DateTime _approvalRequest;
    Boolean _requestSent;
    String _requestResponse;
    Boolean _responseObtained;
    private readonly SIDForm _parentFrm;
    private spGLGetActiveGamesByCustomer_Result _gameInfo;
    private spCnGetActiveContests_Result _contestInfo;
    private readonly spCstGetCustomer_Result _customer;
    private readonly String _amountWagered;
    private readonly String _toWinAmount;
    private readonly String _wagerTypeName;
    private MdiTicketWriter _mdiTicketWriter;

    #endregion

    #region Structures

    #endregion

    #region Constructors

    public FrmLateWagerRequest(SIDForm parentFrm, spGLGetActiveGamesByCustomer_Result gameInfo, spCstGetCustomer_Result customer, String wagerDesc, String amountWagered, String toWinAmount)
      : base(parentFrm.AppModuleInfo) {
      _parentFrm = parentFrm;
      _gameInfo = gameInfo;
      _customer = customer;
      WagerDesc = wagerDesc;
      _amountWagered = amountWagered;
      if (string.IsNullOrEmpty(_amountWagered))
        _amountWagered = "0";
      _toWinAmount = toWinAmount;
      if (string.IsNullOrEmpty(_toWinAmount))
        _toWinAmount = "0";

      InitializeComponent();
    }

    public FrmLateWagerRequest(SIDForm parentFrm, spCnGetActiveContests_Result contestInfo, spCstGetCustomer_Result customer, String wagerDesc, String amountWagered, String toWinAmount)
      : base(parentFrm.AppModuleInfo) {
      _parentFrm = parentFrm;
      _contestInfo = contestInfo;
      _customer = customer;
      WagerDesc = wagerDesc;
      _amountWagered = amountWagered;
      if (string.IsNullOrEmpty(_amountWagered))
        _amountWagered = "0";
      _toWinAmount = toWinAmount;
      if (string.IsNullOrEmpty(toWinAmount))
        _toWinAmount = "0";

      InitializeComponent();
    }

    public FrmLateWagerRequest(SIDForm parentFrm, spCstGetCustomer_Result customer, String wagerTypeName, String wagerDesc, String amountWagered, String toWinAmount)
      : base(parentFrm.AppModuleInfo) {
      _parentFrm = parentFrm;
      _customer = customer;
      WagerDesc = wagerDesc;
      _amountWagered = amountWagered;
      if (string.IsNullOrEmpty(_amountWagered))
        _amountWagered = "0";
      _toWinAmount = toWinAmount;
      if (string.IsNullOrEmpty(toWinAmount))
        _toWinAmount = "0";
      _wagerTypeName = wagerTypeName;

      InitializeComponent();
    }

    #endregion

    #region Public Methods

    #endregion

    #region Private Methods

    private void ChangeBtnSubmitAppeareance() {
      btnSubmit.Text = @"Please wait for Authorization";
      btnSubmit.Enabled = false;
    }

    private void FillInstructionsTxtBox() {
      txtInstructions.Text = "";
      switch (Text) {
        case "Late Wager Request":
          txtInstructions.Text += @"The wager cutoff time for this game has expired.";
          break;
        case "Request to Edit a READBACK Item Past Post":
        case "Request to Delete a READBACK Item Past Post":
          txtInstructions.Text += @"The wager cutoff time for at least one of the selections has expired.";
          break;
        case "Insufficient Funds Request":
          txtInstructions.Text += @"Insufficient funds available.";
          break;
        case "Request to Exceed Maximum Wager":
          txtInstructions.Text += WagerDesc;
          break;
      }


      txtInstructions.Text += Environment.NewLine;
      txtInstructions.Text += @"Press the Submit for Authorization button to proceed";
    }

    private void SendGameRequest() {
      if (_gameInfo != null) {
        SendRequest(_gameInfo.GameNum, 0);
      }
    }

    private void SendContestRequest() {
      if (_contestInfo != null) {
        SendRequest(0, _contestInfo.ContestNum);
      }
    }

    private void SendRequest(int? gameNum, int? contestNum) {
      var requestText = WagerDesc;
      BuildAndSendRequest(gameNum, contestNum, requestText);
    }

    private void BuildAndSendRequest(int? gameNum, int? contestNum, string requestText) {
      using (var ta = new TicketsAndWagers(Mdi.AppModuleInfo)) {
        var request = new spTkWGetApprovalRequests_Result();
        _approvalRequest = Mdi.GetCurrentDateTime();
        _approvalRequest = _approvalRequest.AddMilliseconds(-_approvalRequest.Millisecond);
        request.RequestDateTime = _approvalRequest;
        request.RequestLoginID = Environment.UserName + " at " + Environment.MachineName;
        request.GameNum = gameNum;
        request.ContestNum = contestNum;
        request.RequestText = requestText;
        request.AmountWagered = double.Parse(_amountWagered) * 100;
        request.ToWinAmount = double.Parse(_toWinAmount) * 100;
        var currencyCode = new[] { "", "" };
        if (_customer != null) {
          if (_customer.Currency != null)
            currencyCode = _customer.Currency.Split(' ');
          request.CustomerID = _customer.CustomerID.Trim();
          request.CurrencyCode = currencyCode[0].Trim();
        }
        request.RequestType = Text;
        var effectiveDate = Mdi.GetCurrentDateTime();
        effectiveDate = new DateTime(effectiveDate.Year, effectiveDate.Month, effectiveDate.Day, 0, 0, 0);
        spCurGetCurrencyRateByDate_Result todaysRecord;

        using (var cur = new Currencies(Mdi.AppModuleInfo)) {
          todaysRecord = cur.GetCurrencyRateByDate(currencyCode[0], effectiveDate);
        }

        if (todaysRecord != null) {
          request.LocalAmountWagered = request.AmountWagered * todaysRecord.Rate;
          request.LocalToWinAmount = request.ToWinAmount * todaysRecord.Rate;
        }

        ta.SubmitApprovalRequest(request);
        _requestSent = true;
      }
    }

    private void SendCompoundWagerRequest(string wagerTypeName) {
      switch (wagerTypeName) {
        case "Parlay":
        case "Teaser":
        case "If-Bet":
          if (wagerTypeName == "Teaser")
            WagerDesc = WagerDesc.Replace("already bet", "already bet on identical Teaser");
          string requestText = wagerTypeName + " - " + WagerDesc;
          const int gameNum = 0;
          const int contestNum = 0;
          BuildAndSendRequest(gameNum, contestNum, requestText);
          break;
      }
    }

    private void tmr_Tick(object sender, EventArgs e) {
      spTkWGetRequestApprovalResponse_Result requestResult = CheckRequestResponse();
      if (requestResult != null) {
        _requestResponse = requestResult.Response;
      }

      if (_requestResponse != null) {
        _responseObtained = true;
        ((Timer)sender).Stop();
      }

      if (_responseObtained) {
        MessageBox.Show(_requestResponse == "A" ? @"Request is APPROVED" : @"Request is DECLINED", @"Response");

        PropertyInfo parentProperty = _parentFrm.GetType().GetProperty("AddWagerAuthorizationGranted");

        parentProperty.SetValue(_parentFrm, _requestResponse == "A", null);

        //parentProperty = _parentFrm.GetType().GetProperty("AddWagerAuthorizationGrantedBy");

        //parentProperty.SetValue(_parentFrm, _requestResponseBy, null);

        using (var ta = new TicketsAndWagers(Mdi.AppModuleInfo)) {
          ta.RemoveApprovalRequest(Environment.UserName + " at " + Environment.MachineName, _approvalRequest);
        }
        Close();
      }
    }

    private spTkWGetRequestApprovalResponse_Result CheckRequestResponse() {
      spTkWGetRequestApprovalResponse_Result response;
      using (var ta = new TicketsAndWagers(Mdi.AppModuleInfo)) {
        response = ta.GetApprovalRequestResponse(Environment.UserName + " at " + Environment.MachineName, _approvalRequest);
      }
      return response;
    }

    private void WaitForManagerResponse() {
      var tmr = new Timer { Interval = 1000 };
      tmr.Tick += tmr_Tick;
      tmr.Start();
    }

    #endregion

    #region Events

    private void btnCancel_Click(object sender, EventArgs e) {
      if (_requestSent) {
        using (var ta = new TicketsAndWagers(Mdi.AppModuleInfo)) {
          ta.RemoveApprovalRequest(Environment.UserName + " at " + Environment.MachineName, _approvalRequest);
        }
      }
      Close();
    }

    private void btnSubmit_Click(object sender, EventArgs e) {
      ChangeBtnSubmitAppeareance();
      if (_gameInfo != null)
        SendGameRequest();
      else
        if (_contestInfo != null)
          SendContestRequest();
        else
          SendCompoundWagerRequest(_wagerTypeName);
      WaitForManagerResponse();
    }

    private void frmLateWagerRequest_Load(object sender, EventArgs e) {
      FillInstructionsTxtBox();
    }

    private void frmLateWagerRequest_FormClosing(object sender, FormClosingEventArgs e) {
      _gameInfo = null;
      _contestInfo = null;
    }

    #endregion

  }
}