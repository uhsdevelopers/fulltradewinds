﻿namespace TicketWriter.UI
{
    partial class frmLoadHorseRacingInterface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.webHorseRacingBrowser = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // webHorseRacingBrowser
            // 
            this.webHorseRacingBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webHorseRacingBrowser.Location = new System.Drawing.Point(5, 5);
            this.webHorseRacingBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.webHorseRacingBrowser.Name = "webHorseRacingBrowser";
            this.webHorseRacingBrowser.Size = new System.Drawing.Size(1195, 567);
            this.webHorseRacingBrowser.TabIndex = 0;
            this.webHorseRacingBrowser.Url = new System.Uri("", System.UriKind.Relative);
            // 
            // frmLoadHorseRacingInterface
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1205, 577);
            this.Controls.Add(this.webHorseRacingBrowser);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmLoadHorseRacingInterface";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Phone Bet Taker";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmLoadHorseRacingInterface_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.WebBrowser webHorseRacingBrowser;

    }
}