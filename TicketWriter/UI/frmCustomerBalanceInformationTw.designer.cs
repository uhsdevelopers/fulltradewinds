﻿namespace TicketWriter.UI
{
    partial class FrmCustomerBalanceInformationTw
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtBalanceInfoHeader = new System.Windows.Forms.TextBox();
            this.grpCurrentBalance = new System.Windows.Forms.GroupBox();
            this.lblOnReadbackFigure = new System.Windows.Forms.Label();
            this.lblOnReadback = new System.Windows.Forms.Label();
            this.lblLine = new System.Windows.Forms.Label();
            this.lblAvailableBalanceFigure = new System.Windows.Forms.Label();
            this.lblPostedPendingBetsFigure = new System.Windows.Forms.Label();
            this.lblNonPostedCasinoBalanceFigure = new System.Windows.Forms.Label();
            this.lblPendingCreditFigure = new System.Windows.Forms.Label();
            this.lblCreditLimitFigure = new System.Windows.Forms.Label();
            this.lblCurrentBalanceFigure = new System.Windows.Forms.Label();
            this.lblAvailableBalance = new System.Windows.Forms.Label();
            this.lblPostedPendingBets = new System.Windows.Forms.Label();
            this.lblNonPostedCasinoBalance = new System.Windows.Forms.Label();
            this.lblPendingCredit = new System.Windows.Forms.Label();
            this.lblCreditLimit = new System.Windows.Forms.Label();
            this.lblCurrentBalance = new System.Windows.Forms.Label();
            this.grpFreePlayBalance = new System.Windows.Forms.GroupBox();
            this.lblFPAvailableFigure = new System.Windows.Forms.Label();
            this.lblFPAvailable = new System.Windows.Forms.Label();
            this.lblFreePlayOnReadbackFigure = new System.Windows.Forms.Label();
            this.lblLine2 = new System.Windows.Forms.Label();
            this.lblFreePlayOnReadback = new System.Windows.Forms.Label();
            this.lblFreePlayBalanceFigure = new System.Windows.Forms.Label();
            this.lblFreePlayBalance = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.grpCurrentRIF = new System.Windows.Forms.GroupBox();
            this.lblCurrentRIFAvailableFigure = new System.Windows.Forms.Label();
            this.lblCurrentRIFAvailable = new System.Windows.Forms.Label();
            this.panFormContainer = new System.Windows.Forms.Panel();
            this.grpCurrentBalance.SuspendLayout();
            this.grpFreePlayBalance.SuspendLayout();
            this.grpCurrentRIF.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtBalanceInfoHeader
            // 
            this.txtBalanceInfoHeader.Enabled = false;
            this.txtBalanceInfoHeader.Location = new System.Drawing.Point(22, 15);
            this.txtBalanceInfoHeader.Multiline = true;
            this.txtBalanceInfoHeader.Name = "txtBalanceInfoHeader";
            this.txtBalanceInfoHeader.ReadOnly = true;
            this.txtBalanceInfoHeader.Size = new System.Drawing.Size(279, 123);
            this.txtBalanceInfoHeader.TabIndex = 0;
            this.txtBalanceInfoHeader.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // grpCurrentBalance
            // 
            this.grpCurrentBalance.Controls.Add(this.lblOnReadbackFigure);
            this.grpCurrentBalance.Controls.Add(this.lblOnReadback);
            this.grpCurrentBalance.Controls.Add(this.lblLine);
            this.grpCurrentBalance.Controls.Add(this.lblAvailableBalanceFigure);
            this.grpCurrentBalance.Controls.Add(this.lblPostedPendingBetsFigure);
            this.grpCurrentBalance.Controls.Add(this.lblNonPostedCasinoBalanceFigure);
            this.grpCurrentBalance.Controls.Add(this.lblPendingCreditFigure);
            this.grpCurrentBalance.Controls.Add(this.lblCreditLimitFigure);
            this.grpCurrentBalance.Controls.Add(this.lblCurrentBalanceFigure);
            this.grpCurrentBalance.Controls.Add(this.lblAvailableBalance);
            this.grpCurrentBalance.Controls.Add(this.lblPostedPendingBets);
            this.grpCurrentBalance.Controls.Add(this.lblNonPostedCasinoBalance);
            this.grpCurrentBalance.Controls.Add(this.lblPendingCredit);
            this.grpCurrentBalance.Controls.Add(this.lblCreditLimit);
            this.grpCurrentBalance.Controls.Add(this.lblCurrentBalance);
            this.grpCurrentBalance.Location = new System.Drawing.Point(22, 140);
            this.grpCurrentBalance.Name = "grpCurrentBalance";
            this.grpCurrentBalance.Size = new System.Drawing.Size(279, 199);
            this.grpCurrentBalance.TabIndex = 1;
            this.grpCurrentBalance.TabStop = false;
            // 
            // lblOnReadbackFigure
            // 
            this.lblOnReadbackFigure.Location = new System.Drawing.Point(168, 111);
            this.lblOnReadbackFigure.Name = "lblOnReadbackFigure";
            this.lblOnReadbackFigure.Size = new System.Drawing.Size(96, 13);
            this.lblOnReadbackFigure.TabIndex = 14;
            this.lblOnReadbackFigure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblOnReadback
            // 
            this.lblOnReadback.Location = new System.Drawing.Point(6, 111);
            this.lblOnReadback.Name = "lblOnReadback";
            this.lblOnReadback.Size = new System.Drawing.Size(153, 16);
            this.lblOnReadback.TabIndex = 13;
            this.lblOnReadback.Text = "On Readback:";
            this.lblOnReadback.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblLine
            // 
            this.lblLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLine.Location = new System.Drawing.Point(3, 152);
            this.lblLine.Name = "lblLine";
            this.lblLine.Size = new System.Drawing.Size(273, 15);
            this.lblLine.TabIndex = 12;
            this.lblLine.Text = "___________________________________________";
            this.lblLine.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAvailableBalanceFigure
            // 
            this.lblAvailableBalanceFigure.Location = new System.Drawing.Point(168, 172);
            this.lblAvailableBalanceFigure.Name = "lblAvailableBalanceFigure";
            this.lblAvailableBalanceFigure.Size = new System.Drawing.Size(96, 13);
            this.lblAvailableBalanceFigure.TabIndex = 11;
            this.lblAvailableBalanceFigure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblPostedPendingBetsFigure
            // 
            this.lblPostedPendingBetsFigure.Location = new System.Drawing.Point(168, 133);
            this.lblPostedPendingBetsFigure.Name = "lblPostedPendingBetsFigure";
            this.lblPostedPendingBetsFigure.Size = new System.Drawing.Size(96, 13);
            this.lblPostedPendingBetsFigure.TabIndex = 10;
            this.lblPostedPendingBetsFigure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblNonPostedCasinoBalanceFigure
            // 
            this.lblNonPostedCasinoBalanceFigure.Location = new System.Drawing.Point(168, 86);
            this.lblNonPostedCasinoBalanceFigure.Name = "lblNonPostedCasinoBalanceFigure";
            this.lblNonPostedCasinoBalanceFigure.Size = new System.Drawing.Size(96, 13);
            this.lblNonPostedCasinoBalanceFigure.TabIndex = 9;
            this.lblNonPostedCasinoBalanceFigure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblPendingCreditFigure
            // 
            this.lblPendingCreditFigure.Location = new System.Drawing.Point(168, 60);
            this.lblPendingCreditFigure.Name = "lblPendingCreditFigure";
            this.lblPendingCreditFigure.Size = new System.Drawing.Size(96, 13);
            this.lblPendingCreditFigure.TabIndex = 8;
            this.lblPendingCreditFigure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCreditLimitFigure
            // 
            this.lblCreditLimitFigure.Location = new System.Drawing.Point(168, 38);
            this.lblCreditLimitFigure.Name = "lblCreditLimitFigure";
            this.lblCreditLimitFigure.Size = new System.Drawing.Size(96, 13);
            this.lblCreditLimitFigure.TabIndex = 7;
            this.lblCreditLimitFigure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCurrentBalanceFigure
            // 
            this.lblCurrentBalanceFigure.Location = new System.Drawing.Point(168, 16);
            this.lblCurrentBalanceFigure.Name = "lblCurrentBalanceFigure";
            this.lblCurrentBalanceFigure.Size = new System.Drawing.Size(96, 13);
            this.lblCurrentBalanceFigure.TabIndex = 6;
            this.lblCurrentBalanceFigure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblAvailableBalance
            // 
            this.lblAvailableBalance.Location = new System.Drawing.Point(25, 172);
            this.lblAvailableBalance.Name = "lblAvailableBalance";
            this.lblAvailableBalance.Size = new System.Drawing.Size(134, 15);
            this.lblAvailableBalance.TabIndex = 5;
            this.lblAvailableBalance.Text = "Available Balance:";
            this.lblAvailableBalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblPostedPendingBets
            // 
            this.lblPostedPendingBets.Location = new System.Drawing.Point(6, 133);
            this.lblPostedPendingBets.Name = "lblPostedPendingBets";
            this.lblPostedPendingBets.Size = new System.Drawing.Size(153, 18);
            this.lblPostedPendingBets.TabIndex = 4;
            this.lblPostedPendingBets.Text = "Posted Pending Bets:";
            this.lblPostedPendingBets.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblNonPostedCasinoBalance
            // 
            this.lblNonPostedCasinoBalance.Location = new System.Drawing.Point(6, 86);
            this.lblNonPostedCasinoBalance.Name = "lblNonPostedCasinoBalance";
            this.lblNonPostedCasinoBalance.Size = new System.Drawing.Size(153, 16);
            this.lblNonPostedCasinoBalance.TabIndex = 3;
            this.lblNonPostedCasinoBalance.Text = "Non-Posted Casino Balance:";
            this.lblNonPostedCasinoBalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblPendingCredit
            // 
            this.lblPendingCredit.Location = new System.Drawing.Point(31, 58);
            this.lblPendingCredit.Name = "lblPendingCredit";
            this.lblPendingCredit.Size = new System.Drawing.Size(128, 17);
            this.lblPendingCredit.TabIndex = 2;
            this.lblPendingCredit.Text = "Pending Credit (If-Bets):";
            this.lblPendingCredit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCreditLimit
            // 
            this.lblCreditLimit.AutoSize = true;
            this.lblCreditLimit.Location = new System.Drawing.Point(98, 38);
            this.lblCreditLimit.Name = "lblCreditLimit";
            this.lblCreditLimit.Size = new System.Drawing.Size(61, 13);
            this.lblCreditLimit.TabIndex = 1;
            this.lblCreditLimit.Text = "Credit Limit:";
            this.lblCreditLimit.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCurrentBalance
            // 
            this.lblCurrentBalance.Location = new System.Drawing.Point(63, 16);
            this.lblCurrentBalance.Name = "lblCurrentBalance";
            this.lblCurrentBalance.Size = new System.Drawing.Size(96, 13);
            this.lblCurrentBalance.TabIndex = 0;
            this.lblCurrentBalance.Text = "Current Balance:";
            this.lblCurrentBalance.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // grpFreePlayBalance
            // 
            this.grpFreePlayBalance.Controls.Add(this.lblFPAvailableFigure);
            this.grpFreePlayBalance.Controls.Add(this.lblFPAvailable);
            this.grpFreePlayBalance.Controls.Add(this.lblFreePlayOnReadbackFigure);
            this.grpFreePlayBalance.Controls.Add(this.lblLine2);
            this.grpFreePlayBalance.Controls.Add(this.lblFreePlayOnReadback);
            this.grpFreePlayBalance.Controls.Add(this.lblFreePlayBalanceFigure);
            this.grpFreePlayBalance.Controls.Add(this.lblFreePlayBalance);
            this.grpFreePlayBalance.Location = new System.Drawing.Point(22, 339);
            this.grpFreePlayBalance.Name = "grpFreePlayBalance";
            this.grpFreePlayBalance.Size = new System.Drawing.Size(279, 108);
            this.grpFreePlayBalance.TabIndex = 2;
            this.grpFreePlayBalance.TabStop = false;
            // 
            // lblFPAvailableFigure
            // 
            this.lblFPAvailableFigure.Location = new System.Drawing.Point(168, 77);
            this.lblFPAvailableFigure.Name = "lblFPAvailableFigure";
            this.lblFPAvailableFigure.Size = new System.Drawing.Size(96, 13);
            this.lblFPAvailableFigure.TabIndex = 15;
            this.lblFPAvailableFigure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblFPAvailable
            // 
            this.lblFPAvailable.Location = new System.Drawing.Point(25, 77);
            this.lblFPAvailable.Name = "lblFPAvailable";
            this.lblFPAvailable.Size = new System.Drawing.Size(134, 15);
            this.lblFPAvailable.TabIndex = 15;
            this.lblFPAvailable.Text = "Free Play Available:";
            this.lblFPAvailable.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblFreePlayOnReadbackFigure
            // 
            this.lblFreePlayOnReadbackFigure.Location = new System.Drawing.Point(168, 38);
            this.lblFreePlayOnReadbackFigure.Name = "lblFreePlayOnReadbackFigure";
            this.lblFreePlayOnReadbackFigure.Size = new System.Drawing.Size(96, 13);
            this.lblFreePlayOnReadbackFigure.TabIndex = 16;
            this.lblFreePlayOnReadbackFigure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblLine2
            // 
            this.lblLine2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLine2.Location = new System.Drawing.Point(4, 54);
            this.lblLine2.Name = "lblLine2";
            this.lblLine2.Size = new System.Drawing.Size(273, 15);
            this.lblLine2.TabIndex = 15;
            this.lblLine2.Text = "___________________________________________";
            this.lblLine2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblFreePlayOnReadback
            // 
            this.lblFreePlayOnReadback.AutoSize = true;
            this.lblFreePlayOnReadback.Location = new System.Drawing.Point(35, 38);
            this.lblFreePlayOnReadback.Name = "lblFreePlayOnReadback";
            this.lblFreePlayOnReadback.Size = new System.Drawing.Size(124, 13);
            this.lblFreePlayOnReadback.TabIndex = 14;
            this.lblFreePlayOnReadback.Text = "Free Play On Readback:";
            // 
            // lblFreePlayBalanceFigure
            // 
            this.lblFreePlayBalanceFigure.Location = new System.Drawing.Point(168, 16);
            this.lblFreePlayBalanceFigure.Name = "lblFreePlayBalanceFigure";
            this.lblFreePlayBalanceFigure.Size = new System.Drawing.Size(96, 13);
            this.lblFreePlayBalanceFigure.TabIndex = 13;
            this.lblFreePlayBalanceFigure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblFreePlayBalance
            // 
            this.lblFreePlayBalance.AutoSize = true;
            this.lblFreePlayBalance.Location = new System.Drawing.Point(63, 16);
            this.lblFreePlayBalance.Name = "lblFreePlayBalance";
            this.lblFreePlayBalance.Size = new System.Drawing.Size(96, 13);
            this.lblFreePlayBalance.TabIndex = 0;
            this.lblFreePlayBalance.Text = "Free Play Balance:";
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(124, 506);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // grpCurrentRIF
            // 
            this.grpCurrentRIF.Controls.Add(this.lblCurrentRIFAvailableFigure);
            this.grpCurrentRIF.Controls.Add(this.lblCurrentRIFAvailable);
            this.grpCurrentRIF.Location = new System.Drawing.Point(24, 453);
            this.grpCurrentRIF.Name = "grpCurrentRIF";
            this.grpCurrentRIF.Size = new System.Drawing.Size(279, 39);
            this.grpCurrentRIF.TabIndex = 17;
            this.grpCurrentRIF.TabStop = false;
            // 
            // lblCurrentRIFAvailableFigure
            // 
            this.lblCurrentRIFAvailableFigure.Location = new System.Drawing.Point(166, 18);
            this.lblCurrentRIFAvailableFigure.Name = "lblCurrentRIFAvailableFigure";
            this.lblCurrentRIFAvailableFigure.Size = new System.Drawing.Size(96, 13);
            this.lblCurrentRIFAvailableFigure.TabIndex = 15;
            this.lblCurrentRIFAvailableFigure.Text = "(no selection)";
            this.lblCurrentRIFAvailableFigure.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblCurrentRIFAvailable
            // 
            this.lblCurrentRIFAvailable.Location = new System.Drawing.Point(17, 16);
            this.lblCurrentRIFAvailable.Name = "lblCurrentRIFAvailable";
            this.lblCurrentRIFAvailable.Size = new System.Drawing.Size(140, 15);
            this.lblCurrentRIFAvailable.TabIndex = 15;
            this.lblCurrentRIFAvailable.Text = "Current Rolling IF Available:";
            this.lblCurrentRIFAvailable.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panFormContainer
            // 
            this.panFormContainer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panFormContainer.Location = new System.Drawing.Point(0, 0);
            this.panFormContainer.Name = "panFormContainer";
            this.panFormContainer.Size = new System.Drawing.Size(326, 536);
            this.panFormContainer.TabIndex = 18;
            // 
            // FrmCustomerBalanceInformationTw
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(326, 536);
            this.Controls.Add(this.grpCurrentRIF);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.grpFreePlayBalance);
            this.Controls.Add(this.grpCurrentBalance);
            this.Controls.Add(this.txtBalanceInfoHeader);
            this.Controls.Add(this.panFormContainer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCustomerBalanceInformationTw";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Balance Information";
            this.Load += new System.EventHandler(this.frmCustomerBalanceInformation_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmCustomerBalanceInformationTw_KeyDown);
            this.grpCurrentBalance.ResumeLayout(false);
            this.grpCurrentBalance.PerformLayout();
            this.grpFreePlayBalance.ResumeLayout(false);
            this.grpFreePlayBalance.PerformLayout();
            this.grpCurrentRIF.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtBalanceInfoHeader;
        private System.Windows.Forms.GroupBox grpCurrentBalance;
        private System.Windows.Forms.GroupBox grpFreePlayBalance;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblFreePlayBalance;
        private System.Windows.Forms.Label lblAvailableBalance;
        private System.Windows.Forms.Label lblPostedPendingBets;
        private System.Windows.Forms.Label lblNonPostedCasinoBalance;
        private System.Windows.Forms.Label lblPendingCredit;
        private System.Windows.Forms.Label lblCreditLimit;
        private System.Windows.Forms.Label lblCurrentBalance;
        private System.Windows.Forms.Label lblAvailableBalanceFigure;
        private System.Windows.Forms.Label lblPostedPendingBetsFigure;
        private System.Windows.Forms.Label lblNonPostedCasinoBalanceFigure;
        private System.Windows.Forms.Label lblPendingCreditFigure;
        private System.Windows.Forms.Label lblCreditLimitFigure;
        private System.Windows.Forms.Label lblCurrentBalanceFigure;
        private System.Windows.Forms.Label lblLine;
        private System.Windows.Forms.Label lblFreePlayBalanceFigure;
        private System.Windows.Forms.Label lblOnReadbackFigure;
        private System.Windows.Forms.Label lblOnReadback;
        private System.Windows.Forms.Label lblFreePlayOnReadback;
        private System.Windows.Forms.Label lblLine2;
        private System.Windows.Forms.Label lblFreePlayOnReadbackFigure;
        private System.Windows.Forms.Label lblFPAvailableFigure;
        private System.Windows.Forms.Label lblFPAvailable;
        private System.Windows.Forms.GroupBox grpCurrentRIF;
        private System.Windows.Forms.Label lblCurrentRIFAvailableFigure;
        private System.Windows.Forms.Label lblCurrentRIFAvailable;
        private System.Windows.Forms.Panel panFormContainer;
    }
}