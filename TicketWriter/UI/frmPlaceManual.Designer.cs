﻿namespace TicketWriter.UI
{
    partial class FrmPlaceManual
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblType = new System.Windows.Forms.Label();
            this.cmbType = new System.Windows.Forms.ComboBox();
            this.lblDescription = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.btnPlaceBet = new System.Windows.Forms.Button();
            this.panWagerAmounts = new System.Windows.Forms.Panel();
            this.grpRiskAmount = new System.Windows.Forms.GroupBox();
            this.txtRisk = new GUILibraries.Controls.NumberTextBox();
            this.grpWinAmount = new System.Windows.Forms.GroupBox();
            this.txtToWin = new GUILibraries.Controls.NumberTextBox();
            this.txtOdds = new System.Windows.Forms.TextBox();
            this.lblOdds = new System.Windows.Forms.Label();
            this.lblOddsOptional = new System.Windows.Forms.Label();
            this.lblGradeDate = new System.Windows.Forms.Label();
            this.txtManualPlayGradeDateTime = new System.Windows.Forms.TextBox();
            this.dtmManualPlayGradeDate = new System.Windows.Forms.DateTimePicker();
            this.chbLayoffWager = new System.Windows.Forms.CheckBox();
            this.panWagerAmounts.SuspendLayout();
            this.grpRiskAmount.SuspendLayout();
            this.grpWinAmount.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(506, 125);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(12, 15);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(34, 13);
            this.lblType.TabIndex = 1;
            this.lblType.Text = "Type:";
            // 
            // cmbType
            // 
            this.cmbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbType.FormattingEnabled = true;
            this.cmbType.Location = new System.Drawing.Point(80, 12);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(121, 21);
            this.cmbType.TabIndex = 1;
            this.cmbType.SelectedIndexChanged += new System.EventHandler(this.cmbType_SelectedIndexChanged);
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(12, 37);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(63, 13);
            this.lblDescription.TabIndex = 3;
            this.lblDescription.Text = "Description:";
            // 
            // txtDescription
            // 
            this.txtDescription.AcceptsReturn = true;
            this.txtDescription.Location = new System.Drawing.Point(82, 40);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(240, 100);
            this.txtDescription.TabIndex = 2;
            // 
            // btnPlaceBet
            // 
            this.btnPlaceBet.Location = new System.Drawing.Point(425, 125);
            this.btnPlaceBet.Name = "btnPlaceBet";
            this.btnPlaceBet.Size = new System.Drawing.Size(75, 23);
            this.btnPlaceBet.TabIndex = 10;
            this.btnPlaceBet.Text = "Place Bet";
            this.btnPlaceBet.UseVisualStyleBackColor = true;
            this.btnPlaceBet.Click += new System.EventHandler(this.btnPlaceBet_Click);
            // 
            // panWagerAmounts
            // 
            this.panWagerAmounts.Controls.Add(this.grpRiskAmount);
            this.panWagerAmounts.Controls.Add(this.grpWinAmount);
            this.panWagerAmounts.Location = new System.Drawing.Point(328, 65);
            this.panWagerAmounts.Name = "panWagerAmounts";
            this.panWagerAmounts.Size = new System.Drawing.Size(262, 53);
            this.panWagerAmounts.TabIndex = 6;
            // 
            // grpRiskAmount
            // 
            this.grpRiskAmount.Controls.Add(this.txtRisk);
            this.grpRiskAmount.Location = new System.Drawing.Point(5, 3);
            this.grpRiskAmount.Name = "grpRiskAmount";
            this.grpRiskAmount.Size = new System.Drawing.Size(105, 46);
            this.grpRiskAmount.TabIndex = 6;
            this.grpRiskAmount.TabStop = false;
            this.grpRiskAmount.Text = "Risk Amount";
            // 
            // txtRisk
            // 
            this.txtRisk.AllowCents = true;
            this.txtRisk.DividedBy = 1;
            this.txtRisk.DivideFlag = false;
            this.txtRisk.Location = new System.Drawing.Point(7, 20);
            this.txtRisk.Name = "txtRisk";
            this.txtRisk.ShowIfZero = true;
            this.txtRisk.Size = new System.Drawing.Size(85, 20);
            this.txtRisk.TabIndex = 1;
            this.txtRisk.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtRisk_KeyUp);
            // 
            // grpWinAmount
            // 
            this.grpWinAmount.Controls.Add(this.txtToWin);
            this.grpWinAmount.Location = new System.Drawing.Point(143, 3);
            this.grpWinAmount.Name = "grpWinAmount";
            this.grpWinAmount.Size = new System.Drawing.Size(105, 46);
            this.grpWinAmount.TabIndex = 7;
            this.grpWinAmount.TabStop = false;
            this.grpWinAmount.Text = "To Win Amount";
            // 
            // txtToWin
            // 
            this.txtToWin.AllowCents = true;
            this.txtToWin.DividedBy = 1;
            this.txtToWin.DivideFlag = false;
            this.txtToWin.Location = new System.Drawing.Point(13, 19);
            this.txtToWin.Name = "txtToWin";
            this.txtToWin.ShowIfZero = true;
            this.txtToWin.Size = new System.Drawing.Size(85, 20);
            this.txtToWin.TabIndex = 1;
            this.txtToWin.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtToWin_KeyUp);
            // 
            // txtOdds
            // 
            this.txtOdds.Location = new System.Drawing.Point(400, 37);
            this.txtOdds.Name = "txtOdds";
            this.txtOdds.Size = new System.Drawing.Size(100, 20);
            this.txtOdds.TabIndex = 5;
            this.txtOdds.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtOdds_KeyUp);
            // 
            // lblOdds
            // 
            this.lblOdds.AutoSize = true;
            this.lblOdds.Location = new System.Drawing.Point(363, 40);
            this.lblOdds.Name = "lblOdds";
            this.lblOdds.Size = new System.Drawing.Size(35, 13);
            this.lblOdds.TabIndex = 14;
            this.lblOdds.Text = "Odds:";
            // 
            // lblOddsOptional
            // 
            this.lblOddsOptional.AutoSize = true;
            this.lblOddsOptional.Location = new System.Drawing.Point(503, 40);
            this.lblOddsOptional.Name = "lblOddsOptional";
            this.lblOddsOptional.Size = new System.Drawing.Size(50, 13);
            this.lblOddsOptional.TabIndex = 15;
            this.lblOddsOptional.Text = "(optional)";
            // 
            // lblGradeDate
            // 
            this.lblGradeDate.AutoSize = true;
            this.lblGradeDate.Location = new System.Drawing.Point(334, 12);
            this.lblGradeDate.Name = "lblGradeDate";
            this.lblGradeDate.Size = new System.Drawing.Size(65, 13);
            this.lblGradeDate.TabIndex = 16;
            this.lblGradeDate.Text = "Grade Date:";
            // 
            // txtManualPlayGradeDateTime
            // 
            this.txtManualPlayGradeDateTime.Location = new System.Drawing.Point(400, 5);
            this.txtManualPlayGradeDateTime.Name = "txtManualPlayGradeDateTime";
            this.txtManualPlayGradeDateTime.ReadOnly = true;
            this.txtManualPlayGradeDateTime.Size = new System.Drawing.Size(100, 20);
            this.txtManualPlayGradeDateTime.TabIndex = 3;
            this.txtManualPlayGradeDateTime.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtManualPlayGradeDateTime_KeyUp);
            // 
            // dtmManualPlayGradeDate
            // 
            this.dtmManualPlayGradeDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtmManualPlayGradeDate.Location = new System.Drawing.Point(508, 6);
            this.dtmManualPlayGradeDate.Name = "dtmManualPlayGradeDate";
            this.dtmManualPlayGradeDate.Size = new System.Drawing.Size(16, 20);
            this.dtmManualPlayGradeDate.TabIndex = 4;
            this.dtmManualPlayGradeDate.ValueChanged += new System.EventHandler(this.dtmManualPlayGradeDate_ValueChanged);
            // 
            // chbLayoffWager
            // 
            this.chbLayoffWager.AutoSize = true;
            this.chbLayoffWager.Location = new System.Drawing.Point(329, 128);
            this.chbLayoffWager.Name = "chbLayoffWager";
            this.chbLayoffWager.Size = new System.Drawing.Size(90, 17);
            this.chbLayoffWager.TabIndex = 9;
            this.chbLayoffWager.Text = "Layoff Wager";
            this.chbLayoffWager.UseVisualStyleBackColor = true;
            // 
            // FrmPlaceManual
            // 
            this.AcceptButton = this.btnPlaceBet;
            this.AccessibleDescription = "Manual";
            this.AccessibleName = "Manual";
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(594, 152);
            this.ControlBox = false;
            this.Controls.Add(this.chbLayoffWager);
            this.Controls.Add(this.dtmManualPlayGradeDate);
            this.Controls.Add(this.txtManualPlayGradeDateTime);
            this.Controls.Add(this.lblGradeDate);
            this.Controls.Add(this.lblOddsOptional);
            this.Controls.Add(this.lblOdds);
            this.Controls.Add(this.txtOdds);
            this.Controls.Add(this.panWagerAmounts);
            this.Controls.Add(this.btnPlaceBet);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.cmbType);
            this.Controls.Add(this.lblType);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FrmPlaceManual";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmPlaceManual_FormClosing);
            this.Load += new System.EventHandler(this.frmPlaceManual_Load);
            this.panWagerAmounts.ResumeLayout(false);
            this.grpRiskAmount.ResumeLayout(false);
            this.grpRiskAmount.PerformLayout();
            this.grpWinAmount.ResumeLayout(false);
            this.grpWinAmount.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.ComboBox cmbType;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Button btnPlaceBet;
        private System.Windows.Forms.Panel panWagerAmounts;
        private System.Windows.Forms.GroupBox grpRiskAmount;
        private GUILibraries.Controls.NumberTextBox txtRisk;
        private System.Windows.Forms.GroupBox grpWinAmount;
        private GUILibraries.Controls.NumberTextBox txtToWin;
        private System.Windows.Forms.TextBox txtOdds;
        private System.Windows.Forms.Label lblOdds;
        private System.Windows.Forms.Label lblOddsOptional;
        private System.Windows.Forms.Label lblGradeDate;
        private System.Windows.Forms.TextBox txtManualPlayGradeDateTime;
        private System.Windows.Forms.DateTimePicker dtmManualPlayGradeDate;
        private System.Windows.Forms.CheckBox chbLayoffWager;
    }
}