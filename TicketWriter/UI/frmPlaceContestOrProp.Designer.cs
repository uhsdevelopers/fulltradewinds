﻿namespace TicketWriter.UI {
  partial class FrmPlaceContestOrProp {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
      this.txtContestOdds = new System.Windows.Forms.TextBox();
      this.btnGoToGame = new System.Windows.Forms.Button();
      this.lblToWin = new System.Windows.Forms.Label();
      this.lblWagerAmount = new System.Windows.Forms.Label();
      this.btnPlaceBet = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.txtToWin = new GUILibraries.Controls.NumberTextBox();
      this.txtRisk = new GUILibraries.Controls.NumberTextBox();
      this.lblMaxWinDisplay = new System.Windows.Forms.Label();
      this.lblMaxRiskDisplay = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // txtContestOdds
      // 
      this.txtContestOdds.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.txtContestOdds.Enabled = false;
      this.txtContestOdds.Location = new System.Drawing.Point(35, 13);
      this.txtContestOdds.Multiline = true;
      this.txtContestOdds.Name = "txtContestOdds";
      this.txtContestOdds.ReadOnly = true;
      this.txtContestOdds.Size = new System.Drawing.Size(303, 52);
      this.txtContestOdds.TabIndex = 0;
      this.txtContestOdds.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // btnGoToGame
      // 
      this.btnGoToGame.Location = new System.Drawing.Point(263, 71);
      this.btnGoToGame.Name = "btnGoToGame";
      this.btnGoToGame.Size = new System.Drawing.Size(75, 23);
      this.btnGoToGame.TabIndex = 10;
      this.btnGoToGame.Text = "Go to game";
      this.btnGoToGame.UseVisualStyleBackColor = true;
      this.btnGoToGame.Click += new System.EventHandler(this.btnGoToGame_Click);
      // 
      // lblToWin
      // 
      this.lblToWin.AutoSize = true;
      this.lblToWin.Location = new System.Drawing.Point(150, 165);
      this.lblToWin.Name = "lblToWin";
      this.lblToWin.Size = new System.Drawing.Size(45, 13);
      this.lblToWin.TabIndex = 2;
      this.lblToWin.Text = "To Win:";
      // 
      // lblWagerAmount
      // 
      this.lblWagerAmount.AutoSize = true;
      this.lblWagerAmount.Location = new System.Drawing.Point(114, 135);
      this.lblWagerAmount.Name = "lblWagerAmount";
      this.lblWagerAmount.Size = new System.Drawing.Size(81, 13);
      this.lblWagerAmount.TabIndex = 3;
      this.lblWagerAmount.Text = "Wager Amount:";
      // 
      // btnPlaceBet
      // 
      this.btnPlaceBet.Location = new System.Drawing.Point(67, 209);
      this.btnPlaceBet.Name = "btnPlaceBet";
      this.btnPlaceBet.Size = new System.Drawing.Size(128, 23);
      this.btnPlaceBet.TabIndex = 40;
      this.btnPlaceBet.Text = "Place Bet";
      this.btnPlaceBet.UseVisualStyleBackColor = true;
      this.btnPlaceBet.Click += new System.EventHandler(this.btnPlaceBet_Click);
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.Location = new System.Drawing.Point(201, 209);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(128, 23);
      this.btnCancel.TabIndex = 50;
      this.btnCancel.Text = "Cancel";
      this.btnCancel.UseVisualStyleBackColor = true;
      // 
      // txtToWin
      // 
      this.txtToWin.AllowCents = false;
      this.txtToWin.AllowNegatives = false;
      this.txtToWin.DividedBy = 1;
      this.txtToWin.DivideFlag = false;
      this.txtToWin.Location = new System.Drawing.Point(202, 165);
      this.txtToWin.Name = "txtToWin";
      this.txtToWin.ShowIfZero = true;
      this.txtToWin.Size = new System.Drawing.Size(100, 20);
      this.txtToWin.TabIndex = 30;
      this.txtToWin.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtToWin_KeyUp);
      // 
      // txtRisk
      // 
      this.txtRisk.AllowCents = false;
      this.txtRisk.AllowNegatives = false;
      this.txtRisk.DividedBy = 1;
      this.txtRisk.DivideFlag = false;
      this.txtRisk.Location = new System.Drawing.Point(201, 135);
      this.txtRisk.Name = "txtRisk";
      this.txtRisk.ShowIfZero = true;
      this.txtRisk.Size = new System.Drawing.Size(100, 20);
      this.txtRisk.TabIndex = 20;
      this.txtRisk.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtRisk_KeyUp);
      // 
      // lblMaxWinDisplay
      // 
      this.lblMaxWinDisplay.Location = new System.Drawing.Point(304, 166);
      this.lblMaxWinDisplay.Name = "lblMaxWinDisplay";
      this.lblMaxWinDisplay.Size = new System.Drawing.Size(120, 23);
      this.lblMaxWinDisplay.TabIndex = 1004;
      this.lblMaxWinDisplay.Text = "Max To Win";
      // 
      // lblMaxRiskDisplay
      // 
      this.lblMaxRiskDisplay.Location = new System.Drawing.Point(303, 135);
      this.lblMaxRiskDisplay.Name = "lblMaxRiskDisplay";
      this.lblMaxRiskDisplay.Size = new System.Drawing.Size(120, 23);
      this.lblMaxRiskDisplay.TabIndex = 1003;
      this.lblMaxRiskDisplay.Text = "Max Risk";
      // 
      // FrmPlaceContestOrProp
      // 
      this.AcceptButton = this.btnPlaceBet;
      this.AccessibleDescription = "Place Contest";
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(429, 262);
      this.Controls.Add(this.lblMaxWinDisplay);
      this.Controls.Add(this.lblMaxRiskDisplay);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.btnPlaceBet);
      this.Controls.Add(this.txtToWin);
      this.Controls.Add(this.txtRisk);
      this.Controls.Add(this.lblWagerAmount);
      this.Controls.Add(this.lblToWin);
      this.Controls.Add(this.btnGoToGame);
      this.Controls.Add(this.txtContestOdds);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "FrmPlaceContestOrProp";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Contest Or Prop";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmPlaceContestOrProp_FormClosing);
      this.Load += new System.EventHandler(this.FrmPlaceContestOrProp_Load);
      this.Shown += new System.EventHandler(this.FrmPlaceContestOrProp_Shown);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.TextBox txtContestOdds;
    private System.Windows.Forms.Button btnGoToGame;
    private System.Windows.Forms.Label lblToWin;
    private System.Windows.Forms.Label lblWagerAmount;
    private GUILibraries.Controls.NumberTextBox txtRisk;
    private GUILibraries.Controls.NumberTextBox txtToWin;
    private System.Windows.Forms.Button btnPlaceBet;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.Label lblMaxWinDisplay;
    private System.Windows.Forms.Label lblMaxRiskDisplay;
  }
}