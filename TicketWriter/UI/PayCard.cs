﻿using GUILibraries.Forms;

namespace TicketWriter.UI {
  public static class PayCard {
    public static void Show(SIDForm frm, string payCardType, string title, string moduleName, string wagerType) {
      var frmPayCard = new FrmPayCard(frm.AppModuleInfo) {
        PayCardType = payCardType,
        Title = title,
        TopMost = true,
        Parent = frm.Parent,
        ModuleName = moduleName,
        WagerType = wagerType,
        Owner = frm
      };
      frmPayCard.ShowDialog();
    }
  }
}
