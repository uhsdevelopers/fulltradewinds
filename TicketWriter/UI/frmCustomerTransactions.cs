﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using GUILibraries.Forms;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using SIDLibraries.Utilities;
using TicketWriter.Properties;

namespace TicketWriter.UI {

  public sealed partial class FrmCustomerTransactions : SIDForm {

    #region Public Properties

    public List<spULPGetCurrentUserPermissions_Result> CurrentUserPermissions { private get; set; }

    public spCstGetCustomer_Result Customer { private get; set; }

    public FormParameterList ParameterList { get; set; }

    private Boolean SoundCardDetected {
      get {
        if (_soundCardDetected != null) return _soundCardDetected == "True";
        _soundCardDetected = SoundCard.Detected().ToString();
        return _soundCardDetected == "True";
      }
    }

    #endregion

    #region Private Properties

    #endregion

    #region Private Variables

    private String _soundCardDetected;

    #endregion

    #region Constructors

    /// <summary>
    /// Initializes a new instance of the <see cref="FrmCustomerTransactions"/> class.
    /// </summary>
    public FrmCustomerTransactions(ModuleInfo moduleInfo) : base (moduleInfo) {
      InitializeComponent();
    }

    #endregion

    #region Private Methods

    /// <summary>
    /// Closes the form by keyboard.
    /// </summary>
    /// <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
    private void CloseFormByKeyboard(KeyEventArgs e) {
      if (e.KeyCode == Keys.Escape) {
        Close();
      }
    }

    /// <summary>
    /// Fills the grid view.
    /// </summary>
    /// <param name="bndsrc">The BNDSRC.</param>
    /// <param name="transactions">The transactions.</param>
    /// <param name="dgvw">The DGVW.</param>
    private static void FillGridView(BindingSource bndsrc, List<CustomerTransaction> transactions, DataGridView dgvw) {
      bndsrc.Clear();

      foreach (var ct in transactions) {
        bndsrc.Add(ct);
      }
      dgvw.ClearSelection();
      if (transactions.Count > 0)
        dgvw.FirstDisplayedScrollingRowIndex = transactions.Count - 1;
    }

    /// <summary>
    /// Loads the gridview data.
    /// </summary>
    /// <param name="dateRangeCode">The date range code.</param>
    private void LoadGridviewData(int dateRangeCode) {
      var originalText = chbShowLastMonth.Text;

      chbShowLastMonth.Text = Resources.FrmCustomerTransactions_LoadGridviewData_Fetching_data___;
      chbShowLastMonth.Enabled = !chbShowLastMonth.Enabled;
      btnFreePlayTicketsDetails.Enabled = btnTicketDetails.Enabled = false;

      double balance = 0;

      using (var transactions = new Transactions(AppModuleInfo)) {
        var transactionList = transactions.GetCustomerTransactionsByDate(Customer.CustomerID, DateTimeF.StartTransactionDateTime(dateRangeCode));
        var custTransList = new List<CustomerTransaction>();

        double runningAmount;
        foreach (var t in transactionList) {
          if (custTransList.Count > 0) {
            runningAmount = custTransList.Last().Credit > 0 ? custTransList.Last().Credit : custTransList.Last().Debit * (-1);
            balance = custTransList.Last().Balance - runningAmount;
          }
          else {
            if (Customer.CurrentBalance != null) balance = Customer.CurrentBalance.Value;
          }
          if (t.TranDateTime != null)
            if (t.Amount != null)
              custTransList.Add(new CustomerTransaction(t.TranDateTime.Value, t.DocumentNumber, t.Description, t.TranCode == "C" ? t.Amount.Value : 0, t.TranCode == "D" ? t.Amount.Value : 0, balance, t.TranType, t.DailyFigureDate, t.GradeNum, Params.IncludeCents));
        }
        custTransList.Reverse();
        FillGridView(bndsrcTransactions, custTransList, dgvwTransactions);

        custTransList.Clear();
        using (var freePlays = new FreePlays(AppModuleInfo)) {
          var freePlayList = freePlays.GetCustomerFreePlayTransactionsByDate(Customer.CustomerID, DateTimeF.StartTransactionDateTime(dateRangeCode));

          foreach (var t in freePlayList) {
            if (custTransList.Count > 0) {
              runningAmount = custTransList.Last().Credit > 0 ? custTransList.Last().Credit : custTransList.Last().Debit * (-1);
              balance = custTransList.Last().Balance - runningAmount;
            }
            else {
              if (Customer.FreePlayBalance != null) balance = Customer.FreePlayBalance.Value;
            }
            custTransList.Add(new CustomerTransaction(t.TranDateTime, t.DocumentNumber, t.Description, t.TranCode == "C" ? t.Amount : 0, t.TranCode == "D" ? t.Amount : 0, balance, t.TranType, t.GradeNum, Params.IncludeCents));
          }
        }
        // free play balance shows a wrong number *
        FillGridView(bndsrcFreePlays, custTransList, dgvwFreePlayTransactions);
        chbShowLastMonth.Enabled = !chbShowLastMonth.Enabled;
        chbShowLastMonth.Text = originalText;
      }
    }

    /// <summary>
    /// Initializefrms the customer transactions.
    /// </summary>
    private void InitializefrmCustomerTransactions() {
      if (Customer == null) {
        Close();
        return;
      }
      txtLastVerifiedBalance.Text = FormatNumber(Customer.LastVerBalance);
      if (Customer.Currency != null)
        txtCurrency.Text = Customer.Currency.Trim();
      LoadGridviewData(5);
    }

    /// <summary>
    /// Shows the free play details.
    /// </summary>
    private void ShowFreePlayDetails() {
      ShowTransactionDetails(dgvwFreePlayTransactions, btnFreePlayTicketsDetails);
    }

    /// <summary>
    /// Shows the ticket details.
    /// </summary>
    private void ShowTicketDetails() {
      ShowTransactionDetails(dgvwTransactions, btnTicketDetails);
    }

    /// <summary>
    /// Shows the transaction details.
    /// </summary>
    /// <param name="dgvw">The DGVW.</param>
    /// <param name="btnRelated"></param>
    private void ShowTransactionDetails(DataGridView dgvw, Button btnRelated) {
      var originalBtnText = btnRelated.Text;
      btnRelated.Text = Resources.FrmOpenCustomer_OpenDailyFigure_Opening_ + originalBtnText;
      btnRelated.Enabled = !btnRelated.Enabled;

      var ticketNumber = (int)dgvw.SelectedRows[0].Cells[dgvw.Name + "DocumentNumber"].Value;
      var gradeNumber = (int)dgvw.SelectedRows[0].Cells[dgvw.Name + "GradeNumber"].Value;
      var tranDateTime = (DateTime)dgvw.SelectedRows[0].Cells[dgvw.Name + "Date"].Value;
      var frmTicketInformation = new FrmTicketInformation(this, ticketNumber, gradeNumber, tranDateTime, AppModuleInfo, Customer.CustomerID, CurrentUserPermissions, Customer.Currency, SoundCardDetected, null) {
          Icon = Icon
      };
      frmTicketInformation.OpenPlaysMode = false;
      frmTicketInformation.EnableToEditPendingPlays = false;
      frmTicketInformation.ShowDialog();

      btnRelated.Text = originalBtnText;
      btnRelated.Enabled = !btnRelated.Enabled;
    }

    #endregion

    #region Events

    /// <summary>
    /// Handles the Click event of the btnFreePlayTicketsDetails control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    private void btnFreePlayTicketsDetails_Click(object sender, EventArgs e) {
      ShowFreePlayDetails();
    }

    /// <summary>
    /// Handles the Click event of the btnTicketDetails control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    private void btnTicketDetails_Click(object sender, EventArgs e) {
      ShowTicketDetails();
    }

    /// <summary>
    /// Handles the CheckedChanged event of the chbShowLastMonth control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    private void chbShowLastMonth_CheckedChanged(object sender, EventArgs e) {
      LoadGridviewData(((CheckBox)sender).Checked ? 2 : 5);
    }

    /// <summary>
    /// Handles the SelectionChanged event of the dgvwFreePlayTransactions control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    private void dgvwFreePlayTransactions_SelectionChanged(object sender, EventArgs e) {
      var tranType = "";
      if (((DataGridView)sender).SelectedRows.Count > 0 && ((DataGridView)sender).SelectedRows[0].Cells[8].Value != null) {
        tranType = ((DataGridView)sender).SelectedRows[0].Cells[8].Value.ToString();
      }

      switch (tranType) {
        case "A":
        case "L":
        case "W":
        case "R":
          btnFreePlayTicketsDetails.Enabled = true;
          break;
        default:
          btnFreePlayTicketsDetails.Enabled = false;
          break;
      }

      //ActivateDetailsButton(btnFreePlayTicketsDetails, dgvwFreePlayTransactions);
    }

    /// <summary>
    /// Handles the SelectionChanged event of the dgvwTransactions control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    private void dgvwTransactions_SelectionChanged(object sender, EventArgs e) {
      var tranType = "";
      if (((DataGridView)sender).SelectedRows.Count > 0 && ((DataGridView)sender).SelectedRows[0].Cells[8].Value != null) {
        tranType = ((DataGridView)sender).SelectedRows[0].Cells[8].Value.ToString();
      }

      switch (tranType) {
        case "A":
        case "L":
        case "W":
        case "R":
          btnTicketDetails.Enabled = true;
          break;
        default:
          btnTicketDetails.Enabled = false;
          break;
      }

      //ActivateDetailsButton(btnTicketDetails, dgvwTransactions);
    }

    /// <summary>
    /// Handles the KeyDown event of the frmCustomerTransactions control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.Windows.Forms.KeyEventArgs"/> instance containing the event data.</param>
    private void frmCustomerTransactions_KeyDown(object sender, KeyEventArgs e) {
      CloseFormByKeyboard(e);
    }

    /// <summary>
    /// Handles the Load event of the frmCustomerTransactions control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
    private void frmCustomerTransactions_Load(object sender, EventArgs e) {
      InitializefrmCustomerTransactions();
    }

    #endregion
  }
}
