﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using GUILibraries.BusinessLayer;
using GUILibraries.Utilities;
using GUILibraries.Forms;
using SIDLibraries.Entities;
using SIDLibraries.Utilities;
using SIDLibraries.BusinessLayer;
using GUILibraries.Controls;
using System.Drawing;
using Common = TicketWriter.Utilities.Common;
using GUILibraries;

namespace TicketWriter.UI {
  public sealed partial class FrmMakeAWager : SIDForm {
    #region private Constants

    public const string Draw = "Draw";

    #endregion

    #region Public Properties

    public String FrmMode { private get; set; }

    private FrmPlaceTeaser FrmPlaceTeaser {
      get {
        return _frmPlaceTeaser ??
               (_frmPlaceTeaser = (FrmPlaceTeaser)FormF.GetFormByName("FrmPlaceTeaser", this));
      }
    }

    private FrmSportAndGameSelection FrmSportAndGameSelection {
      get {
        return _frmSportAndGameSelection ??
               (_frmSportAndGameSelection =
                (FrmSportAndGameSelection)FormF.GetFormByName("FrmSportAndGameSelection", this));
      }
    }

    public string GameDataSource {
      private get {
        return string.IsNullOrEmpty(_gameDataSource) ? "GameSelection" : _gameDataSource;
      }
      set {
        _gameDataSource = value;
      }
    }

    // ReSharper disable once MemberCanBePrivate.Global
    public Boolean IsFreePlay { get; set; }

    private MdiTicketWriter Mdi {
      get {
        return _mdiTicketWriter ??
               (_mdiTicketWriter = (MdiTicketWriter)FormF.GetFormByName("MdiTicketWriter", this));
      }
    }

    public DateTime PeriodWagerCutoff { get; set; }

    public String PreviouslySelectedAmtWagered { get; set; }

    public String PreviouslySelectedBPtsOpt { get; set; }

    public String PreviouslySelectedFixedPrice { get; set; }

    public String PreviouslySelectedOdds { get; set; }

    public int PreviouslySelectedAmericanPrice { get; set; }

    public String PreviouslySelectedPitcher1 { get; set; }

    public String PreviouslySelectedPitcher2 { get; set; }

    public String PreviouslySelectedToWinAmt { get; set; }

    public String PreviouslySelectedOUflag { get; set; }

    public int WagerItemNumberToUpd { get; set; }

    // ReSharper disable once MemberCanBePrivate.Global
    public int WagerNumberToUpd { get; set; }

    public int TicketNumberToUpd { get; set; }

    public String WagerTypeMode { get; set; } // Risk or ToWin

    private List<Ticket.SelectedGameAndLine> OriginalGamesAndPeriodsInfo {
      get {
        if (Mdi.Ticket == null) return null;
        var list = Mdi.Ticket.OriginalGameOptions;

        return list;
      }
    }

    //public List<spGLGetActiveGamesByCustomer_Result> SelectedGamePeriodInfo;

    private spGLGetActiveGamesByCustomer_Result _selectedGamePeriodInfo;
    public spGLGetActiveGamesByCustomer_Result SelectedGamePeriodInfo {

        get {
            _selectedGamePeriodInfo = GetSelectedGamePeriodInfo();

            if (FrmMode == "Edit" && Mdi.TicketNumber != TicketNumberToUpd)
            {
                _selectedGamePeriodInfo = RestoreOriginalGameLineInfoGivenToCustomer(_selectedGamePeriodInfo);
                _selectedGamePeriodInfo = AdjustSelectedGamePrices(_selectedGamePeriodInfo);
            }

            return _selectedGamePeriodInfo;
        }
        set { _selectedGamePeriodInfo = value; }
    }

    #endregion

    #region Private Properties

    private int? DecimalPrecision {
      get { return _decimalPrecision ?? (_decimalPrecision = FrmSportAndGameSelection.OddsDecimalPrecision); }
    }

    private int? FinalAmericanMoneyLineAdj { get; set; }

    private int? FinalAmericanSpreadAdj { get; set; }

    private int? FinalAmericanTeamTotalsOAdj { get; set; }

    private int? FinalAmericanTeamTotalsUAdj { get; set; }

    private int? FinalAmericanTotalsOAdj { get; set; }

    private int? FinalAmericanTotalsUAdj { get; set; }

    private int? FractionalMaxDenominator {
      get {
        return _fractionalMaxDenominator ??
               (_fractionalMaxDenominator = FrmSportAndGameSelection.OddsFractionMaxDenominator);
      }
    }

    public double? AmericanOdds {
      get {
        if (_easternLineEnabled == "Y" && _selectedSportType == SportTypes.BASEBALL) {
          double easternOdds;
          if (double.TryParse(LineOffering.ConvertToStringBaseLine(lblMoneyLine.Text), out easternOdds))
          {
            _americanOdds = OddsTypes.ConvertFromEasternToAmerican(easternOdds, _easternLineConversionInfo);
          }
        }
        else {
          _americanOdds = GetWagerTypeAdjustment();
        }
        return _americanOdds;
      }
    }

    TextBox SpreadLineAdjControl { get; set; }
    TextBox SpreadPriceControl { get; set; }

    TextBox TotalPointsOverLineAdjControl { get; set; }
    TextBox TotalPointsOverLinePriceControl { get; set; }
    TextBox TotalPointsUnderLineAdjControl { get; set; }
    TextBox TotalPointsUnderLinePriceControl { get; set; }

    TextBox MoneyLinePriceControl { get; set; }

    TextBox TeamTotalsOverLineAdjControl { get; set; }
    TextBox TeamTotalsOverLinePriceControl { get; set; }
    TextBox TeamTotalsUnderLineAdjControl { get; set; }
    TextBox TeamTotalsUnderLinePriceControl { get; set; }

    #endregion

    #region Public vars

    #endregion

    #region Private vars

    private double? _americanOdds;
    private int _adjTotalOver;
    private int _adjTotalUnder;
    private double _baseSpread;
    private double _baseTotal;
    private Boolean _chosenIsFavored = true;
    private spCstGetCostToBuyPointsByCustomer_Result _costToBuyPointsByCustomer;
    private List<spBpGetProgressivePointCost_Result> _costToBuyProgressivePoints;
    private List<spBpGetProgressivePointCost_Result> _allCostToBuyProgressivePoints;
    private List<BuyPoints.BuyPointsLineAndAdjustment> _buyPointsOptions;
    private readonly BuyPoints.CostToBuyPoints _costToBuyPointsObj;
    private readonly String _easternLineEnabled;
    private readonly String _currentCustomerBaseballAction;
    private readonly String _currentCustomerId;
    //private readonly String _currentCustomerIsWise;
    //private readonly int? _currentCustomerPercentBook;
    public readonly String CurrentWagerTypeName;
    private int? _decimalPrecision;
    private readonly List<spGLGetEasternLineConversionInfo_Result> _easternLineConversionInfo;
    private WagerTypeOptions _formattedWagerTypes;
    private int? _fractionalMaxDenominator;
    private FrmPlaceTeaser _frmPlaceTeaser;
    private FrmSportAndGameSelection _frmSportAndGameSelection;
    private string _gameDataSource;
    //private int _initialAdjSpread;
    private List<string> _lineAndPrice;
    private MdiTicketWriter _mdiTicketWriter;
    public readonly FrmSportAndGameSelection ParentFrm;
    private readonly String[] _radNames = { "radSpread", "radTotalsOver", "radTotalsUnder", "radMoneyLine", "radTeamTotalsOver", "radTeamTotalsUnder" };
    private readonly int _selectedGameNumber;
    private readonly List<spGLGetActiveGamesByCustomer_Result> _selectedGameOptions;
    private int _selectedGamePeriod;
    private readonly String _selectedGamePeriodDescription;
    /*private readonly int _selectedGvRowNumber;*/
    private readonly String _selectedPrice;
    private int _selectedRowNumberInGroup;
    private String _selectedSportSubType;
    private String _selectedSportType;
    private String _selectedWagerType;
    //private readonly String _selectedWagerTypeCode;
    private readonly List<spGLGetAvailablePeriodsBySport_Result> _sportPeriods;
    private readonly String _customerHasStaticLines;
    //private string _customerWagerLimitsByLine;
    //private string _creditAccount;
    //private string _customerCurrency;
    private double _teaserPoints;

    private String _wagerItemDescription;
    private String _itemWagerType;
    private String _totalsOu;
    private WagerType _item;
    private String _chosenTeamRotNum;
    private String _chosenTeamId;
    private string _chosenTeamIdDesc;
    private String _chosenLine;
    private String _chosenAmericanPrice;
    private String _complexIfBetAmount = "";

    #endregion

    #region Structures

    #endregion

    #region Constructors

    public FrmMakeAWager(String wagerTypeName, SIDForm parentForm, String customerId, IEnumerable<spGLGetActiveGamesByCustomer_Result> gameOptions, List<spGLGetAvailablePeriodsBySport_Result> gamePeriods, int gameNumber, int gamePeriod, String gamePeriodDescription, String priceType, /*int gvRowNumber,*/ int wagerType, int rowGroupNumber, String easternLineEnabled, List<spGLGetEasternLineConversionInfo_Result> easternLineConversion)
      : base(parentForm.AppModuleInfo) {
      CurrentWagerTypeName = wagerTypeName;
      ParentFrm = (FrmSportAndGameSelection)parentForm;
      _currentCustomerId = customerId;
      _easternLineEnabled = easternLineEnabled;
      _currentCustomerBaseballAction = ParentFrm.CurrentCustBaseballActionType;
      _easternLineConversionInfo = easternLineConversion;
      _selectedGameNumber = gameNumber;
      _selectedGamePeriod = gamePeriod;
      _selectedGamePeriodDescription = gamePeriodDescription;
      _selectedPrice = priceType;
      _selectedGameOptions = gameOptions.OrderBy(x => x.SportType).ThenBy(x => x.ScheduleDate).ThenBy(x => x.ScheduleText).
                          ThenBy(x => x.Team1RotNum).ThenBy(x => x.SportSubType).ThenBy(x => x.GameDateTime).ThenBy(x => x.GameNum).ThenBy(x => x.PeriodNumber).
                          ThenByDescending(x => x.LineSeq).ToList();
      _sportPeriods = gamePeriods;
      /*_selectedGvRowNumber = gvRowNumber;*/
      _selectedWagerType = Enums.SelectedWagerTypeName(wagerType);
      _selectedRowNumberInGroup = rowGroupNumber;
      _customerHasStaticLines = ParentFrm.StaticLinesDuringCall;
      _costToBuyPointsObj = new BuyPoints.CostToBuyPoints();
      InitializeComponent();
      SetCentsNumericInputs();
    }

    #endregion

    #region Public Methods

    #endregion

    #region Private Methods

    private void AddEditingLineValueControls()
    {
        var txtLineAdjControl = new TxtLineAdjControl(_selectedSportType, this);
        txtLineAdjControl.SelectedWagerType = _selectedWagerType;
        txtLineAdjControl.Name = "txtSpreadLineAdjControl";
        txtLineAdjControl.Text = "";
        txtLineAdjControl.NewTextChanged += txtLineAdjControl_NewTextChanged;
        panSpreadTxtLineAdj.Controls.Add(txtLineAdjControl);

        var txtPriceControl = new TxtLinePriceControl(_selectedWagerType, this);
        txtPriceControl.Name = "txtSpreadPriceControl";
        txtPriceControl.Text = "";
        txtPriceControl.ControlLeft += txtPriceControl_ControlLeft;
        txtPriceControl.NewTextChanged += txtPriceControl_NewTextChanged;
        panSpreadTxtPrice.Controls.Add(txtPriceControl);

        txtLineAdjControl = new TxtLineAdjControl(_selectedSportType, this);
        txtLineAdjControl.SelectedWagerType = _selectedWagerType;
        txtLineAdjControl.Name = "txtTotalPointsOverLineAdjControl";
        txtLineAdjControl.Text = "";
        txtLineAdjControl.NewTextChanged += txtLineAdjControl_NewTextChanged;
        panTotalsOverTxtLineAdj.Controls.Add(txtLineAdjControl);

        txtPriceControl = new TxtLinePriceControl(_selectedWagerType, this);
        txtPriceControl.Name = "txtTotalPointsOverLinePriceControl";
        txtPriceControl.Text = "";
        txtPriceControl.ControlLeft += txtPriceControl_ControlLeft;
        txtPriceControl.NewTextChanged += txtPriceControl_NewTextChanged;
        panTotalsOverTxtPrice.Controls.Add(txtPriceControl);

        txtLineAdjControl = new TxtLineAdjControl(_selectedSportType, this);
        txtLineAdjControl.SelectedWagerType = _selectedWagerType;
        txtLineAdjControl.Name = "txtTotalPointsUnderLineAdjControl";
        txtLineAdjControl.Text = "";
        txtLineAdjControl.NewTextChanged += txtLineAdjControl_NewTextChanged;
        panTotalsUnderTxtLineAdj.Controls.Add(txtLineAdjControl);

        txtPriceControl = new TxtLinePriceControl(_selectedWagerType, this);
        txtPriceControl.Name = "txtTotalPointsUnderLinePriceControl";
        txtPriceControl.Text = "";
        txtPriceControl.ControlLeft += txtPriceControl_ControlLeft;
        txtPriceControl.NewTextChanged += txtPriceControl_NewTextChanged;
        panTotalsUnderTxtPrice.Controls.Add(txtPriceControl);

        txtPriceControl = new TxtLinePriceControl(_selectedWagerType, this);
        txtPriceControl.Name = "txtMoneyLinePriceControl";
        txtPriceControl.Text = "";
        txtPriceControl.ControlLeft += txtPriceControl_ControlLeft;
        txtPriceControl.NewTextChanged += txtPriceControl_NewTextChanged;
        panMoneyLineTxtPrice.Controls.Add(txtPriceControl);

        txtLineAdjControl = new TxtLineAdjControl(_selectedSportType, this);
        txtLineAdjControl.SelectedWagerType = _selectedWagerType;
        txtLineAdjControl.Name = "txtTeamTotalsOverLineAdjControl";
        txtLineAdjControl.Text = "";
        txtLineAdjControl.NewTextChanged += txtLineAdjControl_NewTextChanged;
        panTeamTotalsOverTxtLineAdj.Controls.Add(txtLineAdjControl);

        txtPriceControl = new TxtLinePriceControl(_selectedWagerType, this);
        txtPriceControl.Name = "txtTeamTotalsOverLinePriceControl";
        txtPriceControl.Text = "";
        txtPriceControl.ControlLeft += txtPriceControl_ControlLeft;
        txtPriceControl.NewTextChanged += txtPriceControl_NewTextChanged;
        panTeamTotalsOverTxtPrice.Controls.Add(txtPriceControl);

        txtLineAdjControl = new TxtLineAdjControl(_selectedSportType, this);
        txtLineAdjControl.SelectedWagerType = _selectedWagerType;
        txtLineAdjControl.Name = "txtTeamTotalsUnderLineAdjControl";
        txtLineAdjControl.Text = "";
        txtLineAdjControl.NewTextChanged += txtLineAdjControl_NewTextChanged;
        panTeamTotalsUnderTxtLineAdj.Controls.Add(txtLineAdjControl);

        txtPriceControl = new TxtLinePriceControl(_selectedWagerType, this);
        txtPriceControl.Name = "txtTeamTotalsUnderLinePriceControl";
        txtPriceControl.Text = "";
        txtPriceControl.ControlLeft += txtPriceControl_ControlLeft;
        txtPriceControl.NewTextChanged += txtPriceControl_NewTextChanged;
        panTeamTotalsUnderTxtPrice.Controls.Add(txtPriceControl);
    }

    private void BuildWagerSpecsDescription(int teamNumber) {
      if (SelectedGamePeriodInfo == null) return;
      _formattedWagerTypes = LineOffering.FormatWagerTypesOptionsDisplay(SelectedGamePeriodInfo, _selectedPrice, false, _easternLineEnabled, SelectedGamePeriodInfo != null ? SelectedGamePeriodInfo.SportType : "", _easternLineConversionInfo);

      SpreadLineAdjControl = (TextBox)panSpreadTxtLineAdj.Controls.Find("txtSpreadLineAdjControl", true).FirstOrDefault();
      SpreadPriceControl = (TextBox)panSpreadTxtPrice.Controls.Find("txtSpreadPriceControl", true).FirstOrDefault();

      TotalPointsOverLineAdjControl = (TextBox)panTotalsOverTxtLineAdj.Controls.Find("txtTotalPointsOverLineAdjControl", true).FirstOrDefault();
      TotalPointsOverLinePriceControl = (TextBox)panTotalsOverTxtPrice.Controls.Find("txtTotalPointsOverLinePriceControl", true).FirstOrDefault();
      TotalPointsUnderLineAdjControl = (TextBox)panTotalsUnderTxtLineAdj.Controls.Find("txtTotalPointsUnderLineAdjControl", true).FirstOrDefault();
      TotalPointsUnderLinePriceControl = (TextBox)panTotalsUnderTxtPrice.Controls.Find("txtTotalPointsUnderLinePriceControl", true).FirstOrDefault();

      MoneyLinePriceControl = (TextBox)panMoneyLineTxtPrice.Controls.Find("txtMoneyLinePriceControl", true).FirstOrDefault();

      TeamTotalsOverLineAdjControl = (TextBox)panTeamTotalsOverTxtLineAdj.Controls.Find("txtTeamTotalsOverLineAdjControl", true).FirstOrDefault();
      TeamTotalsOverLinePriceControl = (TextBox)panTeamTotalsOverTxtPrice.Controls.Find("txtTeamTotalsOverLinePriceControl", true).FirstOrDefault();
      TeamTotalsUnderLineAdjControl = (TextBox)panTeamTotalsUnderTxtLineAdj.Controls.Find("txtTeamTotalsUnderLineAdjControl", true).FirstOrDefault();
      TeamTotalsUnderLinePriceControl = (TextBox)panTeamTotalsUnderTxtPrice.Controls.Find("txtTeamTotalsUnderLinePriceControl", true).FirstOrDefault();

      panSpreadTxtLineAdj.Visible = panSpreadTxtPrice.Visible =
      panTotalsOverTxtLineAdj.Visible = panTotalsOverTxtPrice.Visible =
      panTotalsUnderTxtLineAdj.Visible = panTotalsUnderTxtPrice.Visible =
      panMoneyLineTxtPrice.Visible =
      panTeamTotalsOverTxtLineAdj.Visible = panTeamTotalsOverTxtPrice.Visible =
      panTeamTotalsUnderTxtLineAdj.Visible = panTeamTotalsUnderTxtPrice.Visible = false;

      switch (teamNumber) {
        case 1:
        case 2:
          lblSpread.Text = teamNumber == 1 ? _formattedWagerTypes.Spread : _formattedWagerTypes.Spread2;
          lblTeaserSpread.Text = lblSpread.Text.Split(' ')[0];
          lblMoneyLine.Text = teamNumber == 1 ? _formattedWagerTypes.MoneyLine1 : _formattedWagerTypes.MoneyLine2;
          lblTotalsOver.Text = _formattedWagerTypes.TotalPoints.Replace("o ", "");
          lblTeaserTotalsOver.Text = lblTotalsOver.Text.Split(' ')[0];
          lblTotalsUnder.Text = _formattedWagerTypes.TotalPoints2.Replace("u ", "");
          lblTeaserTotalsUnder.Text = lblTotalsUnder.Text.Split(' ')[0];
          lblTeamTotalsOver.Text = teamNumber == 1 ? _formattedWagerTypes.TeamTotalsOver.Replace("o ", "") : _formattedWagerTypes.TeamTotalsOver2.Replace("o ", "");
          lblTeaserTeamTotalsOver.Text = lblTeamTotalsOver.Text.Split(' ')[0];
          lblTeamTotalsUnder.Text = teamNumber == 1 ? _formattedWagerTypes.TeamTotalsUnder.Replace("u ", "") : _formattedWagerTypes.TeamTotalsUnder2.Replace("u ", "");
          lblTeaserTeamTotalsUnder.Text = lblTeamTotalsUnder.Text.Split(' ')[0];

          FinalAmericanSpreadAdj = SelectedGamePeriodInfo != null ? (teamNumber == 1 ? SelectedGamePeriodInfo.SpreadAdj1 : SelectedGamePeriodInfo.SpreadAdj2) : null;
          FinalAmericanTotalsOAdj = SelectedGamePeriodInfo != null ? SelectedGamePeriodInfo.TtlPtsAdj1 : null;
          FinalAmericanTotalsUAdj = SelectedGamePeriodInfo != null ? SelectedGamePeriodInfo.TtlPtsAdj2 : null;
          FinalAmericanMoneyLineAdj = SelectedGamePeriodInfo != null ? (teamNumber == 1 ? SelectedGamePeriodInfo.MoneyLine1 : SelectedGamePeriodInfo.MoneyLine2) : null;
          FinalAmericanTeamTotalsOAdj = SelectedGamePeriodInfo != null ? (teamNumber == 1 ? SelectedGamePeriodInfo.Team1TtlPtsAdj1: SelectedGamePeriodInfo.Team2TtlPtsAdj1) : null;
          FinalAmericanTeamTotalsUAdj = SelectedGamePeriodInfo != null ? (teamNumber == 1 ? SelectedGamePeriodInfo.Team1TtlPtsAdj2 : SelectedGamePeriodInfo.Team2TtlPtsAdj2) : null;

          if (SpreadLineAdjControl != null && lblSpread.Text.Trim().Length > 0)
            {
                SpreadLineAdjControl.Text = lblSpread.Text.Replace("  ", " ").Split(' ')[0];
                panSpreadTxtLineAdj.Visible = true;
            }

          if (SpreadPriceControl != null && lblSpread.Text.Trim().Length > 0)
            {
                SpreadPriceControl.Text = lblSpread.Text.Replace("  ", " ").Split(' ')[1];
                panSpreadTxtPrice.Visible = true;
            }

          if (TotalPointsOverLineAdjControl != null && lblTotalsOver.Text.Trim().Length > 0)
            {
                TotalPointsOverLineAdjControl.Text = lblTotalsOver.Text.Replace("  ", " ").Split(' ')[0];
                panTotalsOverTxtLineAdj.Visible = true;
            }

          if (TotalPointsOverLinePriceControl != null && lblTotalsOver.Text.Trim().Length > 0)
            {
                TotalPointsOverLinePriceControl.Text = lblTotalsOver.Text.Replace("  ", " ").Split(' ')[1];
                panTotalsOverTxtPrice.Visible = true;
            }

          if (TotalPointsUnderLineAdjControl != null && lblTotalsUnder.Text.Trim().Length > 0)
            {
                TotalPointsUnderLineAdjControl.Text = lblTotalsUnder.Text.Replace("  ", " ").Split(' ')[0];
                panTotalsUnderTxtLineAdj.Visible = true;
            }

          if (TotalPointsUnderLinePriceControl != null && lblTotalsUnder.Text.Trim().Length > 0)
            {
                TotalPointsUnderLinePriceControl.Text = lblTotalsUnder.Text.Replace("  ", " ").Split(' ')[1];
                panTotalsUnderTxtPrice.Visible = true;
            }

          if (MoneyLinePriceControl != null && lblMoneyLine.Text.Trim().Length > 0)
            {
                MoneyLinePriceControl.Text = lblMoneyLine.Text;
                panMoneyLineTxtPrice.Visible = true;
            }

            if (TeamTotalsOverLineAdjControl != null && lblTeamTotalsOver.Text.Trim().Length > 0)
            {
                TeamTotalsOverLineAdjControl.Text = lblTeamTotalsOver.Text.Replace("  ", " ").Split(' ')[0];
                panTeamTotalsOverTxtLineAdj.Visible = true;
            }

            if (TeamTotalsOverLinePriceControl != null && lblTeamTotalsOver.Text.Trim().Length > 0)
            {
                TeamTotalsOverLinePriceControl.Text = lblTeamTotalsOver.Text.Replace("  ", " ").Split(' ')[1];
                panTeamTotalsOverTxtPrice.Visible = true;
            }

            if (TeamTotalsUnderLineAdjControl != null && lblTeamTotalsUnder.Text.Trim().Length > 0)
            {
                TeamTotalsUnderLineAdjControl.Text = lblTeamTotalsUnder.Text.Replace("  ", " ").Split(' ')[0];
                panTeamTotalsUnderTxtLineAdj.Visible = true;
            }

            if (TeamTotalsUnderLinePriceControl != null && lblTeamTotalsUnder.Text.Trim().Length > 0)
            {
                TeamTotalsUnderLinePriceControl.Text = lblTeamTotalsUnder.Text.Replace("  ", " ").Split(' ')[1];
                panTeamTotalsUnderTxtPrice.Visible = true;
            }

          break;
        case 3:
          lblMoneyLine.Text = _formattedWagerTypes.MoneyLineDraw;
          if (SelectedGamePeriodInfo == null) return;

          if (MoneyLinePriceControl != null && lblMoneyLine.Text.Trim().Length > 0)
          {
              MoneyLinePriceControl.Text = lblMoneyLine.Text;
              panMoneyLineTxtPrice.Visible = true;
          }

          FinalAmericanSpreadAdj = 0;
          FinalAmericanTotalsOAdj = 0;
          FinalAmericanTotalsUAdj = 0;
          FinalAmericanMoneyLineAdj = SelectedGamePeriodInfo.MoneyLineDraw;
          FinalAmericanTeamTotalsOAdj = 0;
          FinalAmericanTeamTotalsUAdj = 0;
          break;
      }
    }

    private spGLGetActiveGamesByCustomer_Result GetSelectedGamePeriodInfo()
    {
        var selectedGamePeriodInfo = new spGLGetActiveGamesByCustomer_Result();
        if (FrmMode == "Edit")
        {
            if (_customerHasStaticLines == "N" || Mdi.TicketNumber != TicketNumberToUpd)
            {
                selectedGamePeriodInfo = TwUtilities.ConvertToNewGameObject((from gO in _selectedGameOptions
                                                                              where gO.GameNum == _selectedGameNumber && gO.PeriodNumber == _selectedGamePeriod
                                                                              select gO).OrderByDescending(s => s.CustProfile).FirstOrDefault(), CurrentWagerTypeName,
                                                                                           FrmSportAndGameSelection.VigDiscountForAllWagerTypes);
            }
            else
            {
                List<Ticket.SelectedGameAndLine> selectedGamesInfo = null;
                if (OriginalGamesAndPeriodsInfo != null)
                {
                    selectedGamesInfo = (from gO in OriginalGamesAndPeriodsInfo
                                         where gO.GameNum == _selectedGameNumber && gO.PeriodNumber == _selectedGamePeriod && gO.TicketNumber == TicketNumberToUpd && gO.WagerNumber == WagerNumberToUpd &&
                                             gO.ItemNumber == WagerItemNumberToUpd
                                         select gO).OrderByDescending(s => s.CustProfile).ToList();
                }

                if (selectedGamesInfo != null)
                {
                    selectedGamePeriodInfo = TwUtilities.ConvertToGameObject(selectedGamesInfo[0], CurrentWagerTypeName, FrmSportAndGameSelection.VigDiscountForAllWagerTypes);
                }
                selectedGamePeriodInfo = SyncOriginalGameRelatedInfoWithLatest(selectedGamePeriodInfo);
            }
        }
        else
        {
            selectedGamePeriodInfo = TwUtilities.ConvertToNewGameObject((from gO in _selectedGameOptions where gO.GameNum == _selectedGameNumber && gO.PeriodNumber == _selectedGamePeriod select gO).
OrderByDescending(s => s.CustProfile).FirstOrDefault(), CurrentWagerTypeName, FrmSportAndGameSelection.VigDiscountForAllWagerTypes);
        }
        return selectedGamePeriodInfo;
    }

    private spGLGetActiveGamesByCustomer_Result RestoreOriginalGameLineInfoGivenToCustomer(spGLGetActiveGamesByCustomer_Result existingGamePeriodInfo)
    {
        var selectedGamePeriodInfo = CloneExistingGamePeriodInfo(existingGamePeriodInfo);

        if(Mdi != null && Mdi.PendingPlayItemsInDb != null && Mdi.PendingPlayItemsInDb.Count > 0)
        {
            var item = (from i in Mdi.PendingPlayItemsInDb where i.TicketNumber == TicketNumberToUpd && i.WagerNumber == WagerNumberToUpd && i.ItemNumber == 
                            WagerItemNumberToUpd select i).FirstOrDefault();
            if(item == null)
                return selectedGamePeriodInfo;
            switch (_selectedWagerType)
            {
                case WagerType.SPREADNAME:
                    if (item.WagerType != _selectedWagerType)
                        break;
                    selectedGamePeriodInfo.Spread = item.AdjSpread;
                    if (selectedGamePeriodInfo.Team1ID.Trim().ToLower() == item.ChosenTeamID.Trim().ToLower())
                    {
                        selectedGamePeriodInfo.SpreadAdj1 = item.FinalMoney;
                        selectedGamePeriodInfo.SpreadDecimal1 = item.FinalDecimal;
                        selectedGamePeriodInfo.SpreadDenominator1 = item.FinalDenominator;
                        selectedGamePeriodInfo.SpreadNumerator1 = item.FinalNumerator;
                    }
                    else
                    {
                        selectedGamePeriodInfo.SpreadAdj2 = item.FinalMoney;
                        selectedGamePeriodInfo.SpreadDecimal2 = item.FinalDecimal;
                        selectedGamePeriodInfo.SpreadDenominator2 = item.FinalDenominator;
                        selectedGamePeriodInfo.SpreadNumerator2 = item.FinalNumerator;
                    }
                    break;
                case WagerType.MONEYLINENAME:
                    if (item.WagerType != _selectedWagerType)
                        break;
                    if (selectedGamePeriodInfo.Team1ID.Trim().ToLower() == item.ChosenTeamID.Trim().ToLower())
                    {
                        selectedGamePeriodInfo.MoneyLine1 = item.FinalMoney;
                        selectedGamePeriodInfo.MoneyLineDecimal1 = item.FinalDecimal;
                        selectedGamePeriodInfo.MoneyLineNumerator1 = item.FinalNumerator;
                        selectedGamePeriodInfo.MoneyLineDenominator1 = item.FinalDenominator;
                    }
                    else
                        if (selectedGamePeriodInfo.Team2ID.Trim().ToLower() == item.ChosenTeamID.Trim().ToLower())
                        {
                            selectedGamePeriodInfo.MoneyLine2 = item.FinalMoney;
                            selectedGamePeriodInfo.MoneyLineDecimal2 = item.FinalDecimal;
                            selectedGamePeriodInfo.MoneyLineNumerator2 = item.FinalNumerator;
                            selectedGamePeriodInfo.MoneyLineDenominator2 = item.FinalDenominator;
                        }
                        else
                        {
                            selectedGamePeriodInfo.MoneyLineDraw = item.FinalMoney;
                            selectedGamePeriodInfo.MoneyLineDecimalDraw = item.FinalDecimal;
                            selectedGamePeriodInfo.MoneyLineNumeratorDraw = item.FinalNumerator;
                            selectedGamePeriodInfo.MoneyLineDenominatorDraw = item.FinalDenominator;
                        }
                    break;
                case WagerType.TOTALPOINTSNAME:
                    if (item.WagerType != _selectedWagerType)
                        break;
                    selectedGamePeriodInfo.TotalPoints = item.AdjTotalPoints;
                    if (item.TotalPointsOU == "O")
                    {
                        selectedGamePeriodInfo.TtlPtsAdj1 = item.FinalMoney;
                        selectedGamePeriodInfo.TtlPointsDecimal1 = item.FinalDecimal;
                        selectedGamePeriodInfo.TtlPointsNumerator1 = item.FinalNumerator;
                        selectedGamePeriodInfo.TtlPointsDenominator1 = item.FinalDenominator;
                    }
                    else
                    {
                        selectedGamePeriodInfo.TtlPtsAdj2 = item.FinalMoney;
                        selectedGamePeriodInfo.TtlPointsDecimal2 = item.FinalDecimal;
                        selectedGamePeriodInfo.TtlPointsNumerator2 = item.FinalNumerator;
                        selectedGamePeriodInfo.TtlPointsDenominator2 = item.FinalDenominator;
                    }

                    break;
                case WagerType.TEAMTOTALPOINTSOVERNAME:
                    if (item.WagerType != _selectedWagerType)
                        break;
                    if (selectedGamePeriodInfo.Team1ID.Trim().ToLower() == item.ChosenTeamID.Trim().ToLower())
                    {
                        selectedGamePeriodInfo.Team1TotalPoints = item.AdjTotalPoints;
                        selectedGamePeriodInfo.Team1TtlPtsAdj1 = item.FinalMoney;
                        selectedGamePeriodInfo.Team1TtlPtsDecimal1 = item.FinalDecimal;
                        selectedGamePeriodInfo.Team1TtlPtsNumerator1 = item.FinalNumerator;
                        selectedGamePeriodInfo.Team1TtlPtsDenominator1 = item.FinalDenominator;
                    }
                    else
                    {
                        selectedGamePeriodInfo.Team2TotalPoints = item.AdjTotalPoints;
                        selectedGamePeriodInfo.Team2TtlPtsAdj1 = item.FinalMoney;
                        selectedGamePeriodInfo.Team2TtlPtsDecimal1 = item.FinalDecimal;
                        selectedGamePeriodInfo.Team2TtlPtsNumerator1 = item.FinalNumerator;
                        selectedGamePeriodInfo.Team2TtlPtsDenominator1 = item.FinalDenominator;
                    }
                    break;
                case WagerType.TEAMTOTALPOINTSUNDERNAME:
                    if (item.WagerType != _selectedWagerType)
                        break;
                    if (selectedGamePeriodInfo.Team1ID.Trim().ToLower() == item.ChosenTeamID.Trim().ToLower())
                    {
                        selectedGamePeriodInfo.Team1TotalPoints = item.AdjTotalPoints;
                        selectedGamePeriodInfo.Team1TtlPtsAdj2 = item.FinalMoney;
                        selectedGamePeriodInfo.Team1TtlPtsDecimal2 = item.FinalDecimal;
                        selectedGamePeriodInfo.Team1TtlPtsNumerator2 = item.FinalNumerator;
                        selectedGamePeriodInfo.Team1TtlPtsDenominator2 = item.FinalDenominator;
                    }
                    else
                    {
                        selectedGamePeriodInfo.Team2TotalPoints = item.AdjTotalPoints;
                        selectedGamePeriodInfo.Team2TtlPtsAdj2 = item.FinalMoney;
                        selectedGamePeriodInfo.Team2TtlPtsDecimal2 = item.FinalDecimal;
                        selectedGamePeriodInfo.Team2TtlPtsNumerator2 = item.FinalNumerator;
                        selectedGamePeriodInfo.Team2TtlPtsDenominator2 = item.FinalDenominator;
                    }
                    break;
            }
      }
        return selectedGamePeriodInfo;
    }

    private spGLGetActiveGamesByCustomer_Result CloneExistingGamePeriodInfo(spGLGetActiveGamesByCustomer_Result existingGamePeriodInfo)
    {
        var gamePeriodInfo = new spGLGetActiveGamesByCustomer_Result();

        gamePeriodInfo.CustomerID = existingGamePeriodInfo.CustomerID;
        gamePeriodInfo.Store = existingGamePeriodInfo.Store;
        gamePeriodInfo.CustProfile = existingGamePeriodInfo.CustProfile;
        gamePeriodInfo.GameNum = existingGamePeriodInfo.GameNum;
        gamePeriodInfo.GameDateTime = existingGamePeriodInfo.GameDateTime;
        gamePeriodInfo.WagerCutoff = existingGamePeriodInfo.WagerCutoff;
        gamePeriodInfo.Status = existingGamePeriodInfo.Status;
        gamePeriodInfo.SportType = existingGamePeriodInfo.SportType;
        gamePeriodInfo.Team1ID = existingGamePeriodInfo.Team1ID;
        gamePeriodInfo.Team1RotNum = existingGamePeriodInfo.Team1RotNum;
        gamePeriodInfo.Team2ID = existingGamePeriodInfo.Team2ID;
        gamePeriodInfo.Team2RotNum = existingGamePeriodInfo.Team2RotNum;
        gamePeriodInfo.DrawRotNum = existingGamePeriodInfo.DrawRotNum;
        gamePeriodInfo.Comments = existingGamePeriodInfo.Comments;
        gamePeriodInfo.SportSubType = existingGamePeriodInfo.SportSubType;
        gamePeriodInfo.OnTV = existingGamePeriodInfo.OnTV;
        gamePeriodInfo.Team1FinalScore = existingGamePeriodInfo.Team1FinalScore;
        gamePeriodInfo.Team2FinalScore = existingGamePeriodInfo.Team2FinalScore;
        gamePeriodInfo.FinalWinnerID = existingGamePeriodInfo.FinalWinnerID;
        gamePeriodInfo.PeriodNumber = existingGamePeriodInfo.PeriodNumber;
        gamePeriodInfo.LineSeq = existingGamePeriodInfo.LineSeq;
        gamePeriodInfo.FavoredTeamID = existingGamePeriodInfo.FavoredTeamID;
        gamePeriodInfo.Spread = existingGamePeriodInfo.Spread;
        gamePeriodInfo.SpreadAdj1 = existingGamePeriodInfo.SpreadAdj1;
        gamePeriodInfo.SpreadAdj2 = existingGamePeriodInfo.SpreadAdj2;
        gamePeriodInfo.TotalPoints = existingGamePeriodInfo.TotalPoints;
        gamePeriodInfo.TtlPtsAdj1 = existingGamePeriodInfo.TtlPtsAdj1;
        gamePeriodInfo.TtlPtsAdj2 = existingGamePeriodInfo.TtlPtsAdj2;
        gamePeriodInfo.MoneyLine1 = existingGamePeriodInfo.MoneyLine1;
        gamePeriodInfo.MoneyLine2 = existingGamePeriodInfo.MoneyLine2;
        gamePeriodInfo.MoneyLineDraw = existingGamePeriodInfo.MoneyLineDraw;
        gamePeriodInfo.Team1TotalPoints = existingGamePeriodInfo.Team1TotalPoints;
        gamePeriodInfo.Team1TtlPtsAdj1 = existingGamePeriodInfo.Team1TtlPtsAdj1;
        gamePeriodInfo.Team1TtlPtsAdj2 = existingGamePeriodInfo.Team1TtlPtsAdj2;
        gamePeriodInfo.Team2TotalPoints = existingGamePeriodInfo.Team2TotalPoints;
        gamePeriodInfo.Team2TtlPtsAdj1 = existingGamePeriodInfo.Team2TtlPtsAdj1;
        gamePeriodInfo.Team2TtlPtsAdj2 = existingGamePeriodInfo.Team2TtlPtsAdj2;
        gamePeriodInfo.SpreadDecimal1 = existingGamePeriodInfo.SpreadDecimal1;
        gamePeriodInfo.SpreadNumerator1 = existingGamePeriodInfo.SpreadNumerator1;
        gamePeriodInfo.SpreadDenominator1 = existingGamePeriodInfo.SpreadDenominator1;
        gamePeriodInfo.SpreadDecimal2 = existingGamePeriodInfo.SpreadDecimal2;
        gamePeriodInfo.SpreadNumerator2 = existingGamePeriodInfo.SpreadNumerator2;
        gamePeriodInfo.SpreadDenominator2 = existingGamePeriodInfo.SpreadDenominator2;
        gamePeriodInfo.MoneyLineDecimal1 = existingGamePeriodInfo.MoneyLineDecimal1;
        gamePeriodInfo.MoneyLineNumerator1 = existingGamePeriodInfo.MoneyLineNumerator1;
        gamePeriodInfo.MoneyLineDenominator1 = existingGamePeriodInfo.MoneyLineDenominator1;
        gamePeriodInfo.MoneyLineDecimal2 = existingGamePeriodInfo.MoneyLineDecimal2;
        gamePeriodInfo.MoneyLineNumerator2 = existingGamePeriodInfo.MoneyLineNumerator2;
        gamePeriodInfo.MoneyLineDenominator2 = existingGamePeriodInfo.MoneyLineDenominator2;
        gamePeriodInfo.MoneyLineDecimalDraw = existingGamePeriodInfo.MoneyLineDecimalDraw;
        gamePeriodInfo.MoneyLineNumeratorDraw = existingGamePeriodInfo.MoneyLineNumeratorDraw;
        gamePeriodInfo.MoneyLineDenominatorDraw = existingGamePeriodInfo.MoneyLineDenominatorDraw;
        gamePeriodInfo.TtlPointsDecimal1 = existingGamePeriodInfo.TtlPointsDecimal1;
        gamePeriodInfo.TtlPointsNumerator1 = existingGamePeriodInfo.TtlPointsNumerator1;
        gamePeriodInfo.TtlPointsDenominator1 = existingGamePeriodInfo.TtlPointsDenominator1;
        gamePeriodInfo.TtlPointsDecimal2 = existingGamePeriodInfo.TtlPointsDecimal2;
        gamePeriodInfo.TtlPointsNumerator2 = existingGamePeriodInfo.TtlPointsNumerator2;
        gamePeriodInfo.TtlPointsDenominator2 = existingGamePeriodInfo.TtlPointsDenominator2;
        gamePeriodInfo.Team1TtlPtsDecimal1 = existingGamePeriodInfo.Team1TtlPtsDecimal1;
        gamePeriodInfo.Team1TtlPtsNumerator1 = existingGamePeriodInfo.Team1TtlPtsNumerator1;
        gamePeriodInfo.Team1TtlPtsDenominator1 = existingGamePeriodInfo.Team1TtlPtsDenominator1;
        gamePeriodInfo.Team1TtlPtsDecimal2 = existingGamePeriodInfo.Team1TtlPtsDecimal2;
        gamePeriodInfo.Team1TtlPtsNumerator2 = existingGamePeriodInfo.Team1TtlPtsNumerator2;
        gamePeriodInfo.Team1TtlPtsDenominator2 = existingGamePeriodInfo.Team1TtlPtsDenominator2;
        gamePeriodInfo.Team2TtlPtsDecimal1 = existingGamePeriodInfo.Team2TtlPtsDecimal1;
        gamePeriodInfo.Team2TtlPtsNumerator1 = existingGamePeriodInfo.Team2TtlPtsNumerator1;
        gamePeriodInfo.Team2TtlPtsDenominator1 = existingGamePeriodInfo.Team2TtlPtsDenominator1;
        gamePeriodInfo.Team2TtlPtsDecimal2 = existingGamePeriodInfo.Team2TtlPtsDecimal2;
        gamePeriodInfo.Team2TtlPtsNumerator2 = existingGamePeriodInfo.Team2TtlPtsNumerator2;
        gamePeriodInfo.Team2TtlPtsDenominator2 = existingGamePeriodInfo.Team2TtlPtsDenominator2;
        gamePeriodInfo.PeriodDescription = existingGamePeriodInfo.PeriodDescription;
        gamePeriodInfo.ScheduleDate = existingGamePeriodInfo.ScheduleDate;
        gamePeriodInfo.BroadcastInfo = existingGamePeriodInfo.BroadcastInfo;
        gamePeriodInfo.ListedPitcher1 = existingGamePeriodInfo.ListedPitcher1;
        gamePeriodInfo.ListedPitcher2 = existingGamePeriodInfo.ListedPitcher2;
        gamePeriodInfo.ParlayRestriction = existingGamePeriodInfo.ParlayRestriction;
        gamePeriodInfo.PuckLine = existingGamePeriodInfo.PuckLine;
        gamePeriodInfo.LastSpreadChange = existingGamePeriodInfo.LastSpreadChange;
        gamePeriodInfo.LastMoneyLineChange = existingGamePeriodInfo.LastMoneyLineChange;
        gamePeriodInfo.LastTtlPtsChange = existingGamePeriodInfo.LastTtlPtsChange;
        gamePeriodInfo.LastTeamPtsChange = existingGamePeriodInfo.LastTeamPtsChange;
        gamePeriodInfo.PeriodWagerCutoff = existingGamePeriodInfo.PeriodWagerCutoff;
        gamePeriodInfo.ScheduleText = existingGamePeriodInfo.ScheduleText;
        gamePeriodInfo.CircledMaxWagerMoneyLine = existingGamePeriodInfo.CircledMaxWagerMoneyLine;
        gamePeriodInfo.CircledMaxWagerSpread = existingGamePeriodInfo.CircledMaxWagerSpread;
        gamePeriodInfo.CircledMaxWagerTeamTotal = existingGamePeriodInfo.CircledMaxWagerTeamTotal;
        gamePeriodInfo.CircledMaxWagerTotal = existingGamePeriodInfo.CircledMaxWagerTotal;
        gamePeriodInfo.TimeChangeFlag = existingGamePeriodInfo.TimeChangeFlag;
        gamePeriodInfo.CorrelationID = existingGamePeriodInfo.CorrelationID;
        gamePeriodInfo.PreventPointBuyingFlag = existingGamePeriodInfo.PreventPointBuyingFlag;
        gamePeriodInfo.AllowBuyOnPoints = existingGamePeriodInfo.AllowBuyOnPoints;
        gamePeriodInfo.AllowBuyOnSpread = existingGamePeriodInfo.AllowBuyOnSpread;
        gamePeriodInfo.CircledMaxWager = existingGamePeriodInfo.CircledMaxWager;
        gamePeriodInfo.TeamActionLinePos = existingGamePeriodInfo.TeamActionLinePos;
        gamePeriodInfo.GradeDateTime = existingGamePeriodInfo.GradeDateTime;
        gamePeriodInfo.KeepOpenFlag = existingGamePeriodInfo.KeepOpenFlag;
        gamePeriodInfo.LinkedToStoreFlag = existingGamePeriodInfo.LinkedToStoreFlag;
        gamePeriodInfo.OnlineFeedMoneyLine = existingGamePeriodInfo.OnlineFeedMoneyLine;
        gamePeriodInfo.OnlineFeedSpread = existingGamePeriodInfo.OnlineFeedSpread;
        gamePeriodInfo.OnlineFeedTotalPoints = existingGamePeriodInfo.OnlineFeedTotalPoints;
        gamePeriodInfo.Pitcher1StartedFlag = existingGamePeriodInfo.Pitcher1StartedFlag;
        gamePeriodInfo.Pitcher2StartedFlag = existingGamePeriodInfo.Pitcher2StartedFlag;
        gamePeriodInfo.ScheduleID = existingGamePeriodInfo.ScheduleID;
        gamePeriodInfo.Team1Nss = existingGamePeriodInfo.Team1Nss;
        gamePeriodInfo.Team2Nss = existingGamePeriodInfo.Team2Nss;
        gamePeriodInfo.AltScorePeriodNumber1 = existingGamePeriodInfo.AltScorePeriodNumber1;
        gamePeriodInfo.AltScorePeriodNumber2 = existingGamePeriodInfo.AltScorePeriodNumber2;
        gamePeriodInfo.PreReqPeriodNumber = existingGamePeriodInfo.PreReqPeriodNumber;
        gamePeriodInfo.ShortDescription = existingGamePeriodInfo.ShortDescription;
        gamePeriodInfo.SportSequenceNumber = existingGamePeriodInfo.SportSequenceNumber;
        gamePeriodInfo.IsOnline = existingGamePeriodInfo.IsOnline;
        gamePeriodInfo.CircledMaxWagerSpreadType = existingGamePeriodInfo.CircledMaxWagerSpreadType;
        gamePeriodInfo.CircledMaxWagerMoneyLineType = existingGamePeriodInfo.CircledMaxWagerMoneyLineType;
        gamePeriodInfo.CircledMaxWagerTotalType = existingGamePeriodInfo.CircledMaxWagerTotalType;
        gamePeriodInfo.CircledMaxWagerTeamTotalType = existingGamePeriodInfo.CircledMaxWagerTeamTotalType;
        gamePeriodInfo.AdjIncrease = existingGamePeriodInfo.AdjIncrease;
        gamePeriodInfo.PointsIncrease = existingGamePeriodInfo.PointsIncrease;
        gamePeriodInfo.EventBetsFlag = existingGamePeriodInfo.EventBetsFlag;
        gamePeriodInfo.PreviousStatus = existingGamePeriodInfo.PreviousStatus;
        gamePeriodInfo.IsGameTitle = existingGamePeriodInfo.IsGameTitle;
        gamePeriodInfo.SpreadLineStatus = existingGamePeriodInfo.SpreadLineStatus;
        gamePeriodInfo.MoneyLineStatus = existingGamePeriodInfo.MoneyLineStatus;
        gamePeriodInfo.TotalsLineStatus = existingGamePeriodInfo.TotalsLineStatus;
        gamePeriodInfo.TeamTotalsLineStatus = existingGamePeriodInfo.TeamTotalsLineStatus;
        return gamePeriodInfo;
    }

    private spGLGetActiveGamesByCustomer_Result SyncOriginalGameRelatedInfoWithLatest(spGLGetActiveGamesByCustomer_Result selectedGamePeriodInfo)
    {
      var retGamePeriodInfo = selectedGamePeriodInfo;
      if (selectedGamePeriodInfo == null) return retGamePeriodInfo;
      if (_selectedGameOptions == null || _selectedGameOptions.Count <= 0) return retGamePeriodInfo;
            var updatedGameOption = (from g in _selectedGameOptions where g.GameNum == selectedGamePeriodInfo.GameNum && g.PeriodNumber == selectedGamePeriodInfo.PeriodNumber select g).FirstOrDefault();
      if (updatedGameOption == null) return retGamePeriodInfo;
                retGamePeriodInfo.Status = updatedGameOption.Status;
                retGamePeriodInfo.CircledMaxWager = updatedGameOption.CircledMaxWager;
                retGamePeriodInfo.CircledMaxWagerMoneyLine = updatedGameOption.CircledMaxWagerMoneyLine;
                retGamePeriodInfo.CircledMaxWagerSpread = updatedGameOption.CircledMaxWagerSpread;
                retGamePeriodInfo.CircledMaxWagerTeamTotal = updatedGameOption.CircledMaxWagerTeamTotal;
                retGamePeriodInfo.CircledMaxWagerTotal = updatedGameOption.CircledMaxWagerTotal;
        return retGamePeriodInfo;
    }

    private void ChangeAcceptButtonText() {
      String bntText;
      if (FrmSportAndGameSelection.ParlayOrTeaserItemFromSb) {
        bntText = "Add to " + CurrentWagerTypeName;
      }
      else {
        bntText = "Update " + CurrentWagerTypeName + " item";
      }
      Common.WriteCaptionOnAcceptButton(CurrentWagerTypeName, bntText, btnPlaceBet, btnAddToParlay, btnAddToTeaser, btnAddToIfBet);
    }

    private void CheckProperWagerOption(string selectedWagerType) {
      const string radName = "rad";

      switch (selectedWagerType) {
        case "Spread":
        case "MoneyLine":
        case "TeamTotalsOver":
        case "TeamTotalsUnder":
          var wagerOption = (RadioButton)Controls.Find(radName + selectedWagerType, true).FirstOrDefault();
          if (wagerOption != null) {
            wagerOption.Checked = false;
            wagerOption.Checked = true;
          }
          EnableEditWagerTextBoxes(selectedWagerType, null);
          break;

        case "Totals":
          if (_selectedRowNumberInGroup == 1) {
            var totalsO = (RadioButton)Controls.Find(radName + selectedWagerType + "Over", true).FirstOrDefault();
            if (totalsO != null)
            {
                totalsO.Checked = true;
                EnableEditWagerTextBoxes(selectedWagerType, "O");
            }
          }
          else {
            var totalsU = (RadioButton)Controls.Find(radName + selectedWagerType + "Under", true).FirstOrDefault();
            if (totalsU != null)
            {
                EnableEditWagerTextBoxes(selectedWagerType, "U");
                totalsU.Checked = true;
            }
          }
          break;
      }
    }

    private void CheckSelectedTeam(int selectedGvRowNumber) {
      var team1Id = (from g in _selectedGameOptions where g.GameNum == _selectedGameNumber select g.Team1ID).FirstOrDefault();
      radTeam1.Text = team1Id;

      var team2Id = (from g in _selectedGameOptions where g.GameNum == _selectedGameNumber select g.Team2ID).FirstOrDefault();
      radTeam2.Text = team2Id;

      switch (selectedGvRowNumber) {
        case 1:
          radTeam1.Checked = true;
          radDraw.Visible = false;
          break;

        case 2:
          radTeam2.Checked = true;
          radDraw.Visible = false;
          break;

        case 3:
          radDraw.Checked = true;
          radDraw.Text = Draw;
          break;
      }
    }

    private void DefineSelectedTeam(int teamNumber) {
      _selectedRowNumberInGroup = teamNumber;
      BuildWagerSpecsDescription(_selectedRowNumberInGroup);
      var wagerType = GetCheckedWagerTypeCode();
      var ouValue = "";

      if (wagerType == WagerType.TOTALPOINTS || wagerType == WagerType.TEAMTOTALPOINTS) {
        ouValue = GetCheckedTotalsOverUnderSelection();
      }
      _buyPointsOptions = null;
      SetBuyingPointsOptions(wagerType, ouValue);
      ShowHideFormObjects();
      SetLayout();

      if (grpRiskAmount.Visible) {
        txtRisk.Text = "";
      }
      if (grpWinAmount.Visible) {
        txtToWin.Text = "";
      }
      var adj = GetWagerTypeAdjustment();
      WagerTypeMode = adj >= 0 ? "Risk" : "ToWin";
    }

    private string DetermineSelectedBPointsOption(WagerType wagerType) {
      var selectedBPointsOption = "";

      if (CurrentWagerTypeName == "Straight Bet" || CurrentWagerTypeName == "Parlay" || CurrentWagerTypeName == "If-Bet") {

        switch (wagerType.Code) {
          case WagerType.MONEYLINE:
          case WagerType.TEAMTOTALPOINTS:
            break;

          case WagerType.SPREAD:
            selectedBPointsOption = dudBuyPointsSpread.Text;
            break;

          case WagerType.TOTALPOINTS:
            selectedBPointsOption = dudBuyPointsTotalPoints.Text;
            break;
        }
      }

      return selectedBPointsOption;
    }

    private static int DetermineSelectedRow(int originalSelectedRow, int originalRowInGrp, int newRowInGroup, string wagerType, string totalsOu) {
      if (wagerType == WagerType.TOTALPOINTS || wagerType == WagerType.TEAMTOTALPOINTS) {
        newRowInGroup = totalsOu == "O" ? 1 : 2;
      }

      var rowsDiff = newRowInGroup - originalRowInGrp;
      var newRowNumber = originalSelectedRow + rowsDiff;

      return newRowNumber;
    }

    private string DetermineSelectedOdds() {
      var selectedOdds = "";

      var radioBtnName = "";
      Label oddsLabel;
      foreach (var radName in _radNames) {
        var rad = (RadioButton)Controls.Find(radName, true).FirstOrDefault();
        if (rad == null) continue;
        if (!rad.Checked) continue;
            radioBtnName = rad.Name;
            break;
          }

      if (CurrentWagerTypeName == "Teaser") {
        oddsLabel = (Label)Controls.Find(radioBtnName.Replace("rad", "lblTeaser"), true).FirstOrDefault();
      }
      else {
        oddsLabel = (Label)Controls.Find(radioBtnName.Replace("rad", "lbl"), true).FirstOrDefault();
      }

      if (oddsLabel != null) {
        selectedOdds = oddsLabel.Text;
      }

      return selectedOdds;
    }

    private double GetAdjustedBpLine(string wagerType, string standardLine) {
      double newLine = 0;
      switch (wagerType) {
        case WagerType.SPREAD:
          if (dudBuyPointsSpread.Visible) {
              newLine = double.Parse(LineOffering.ConvertToStringBaseLine(dudBuyPointsSpread.Text).Replace("+", ""));
          }
          else {
              double.TryParse(LineOffering.ConvertToStringBaseLine(standardLine).Replace("+", ""), out newLine);

          }
          break;
        case WagerType.TOTALPOINTS:
          newLine = double.Parse(dudBuyPointsTotalPoints.Visible ? LineOffering.ConvertToStringBaseLine(dudBuyPointsTotalPoints.Text).Replace("+", "") : LineOffering.ConvertToStringBaseLine(standardLine).Replace("+", ""));
          break;
        case WagerType.MONEYLINE:
        case WagerType.TEAMTOTALPOINTS:
          break;
      }

      return newLine;
    }

    private string GetCheckedTotalsOverUnderSelection() {
      var ou = "";
      foreach (var radName in _radNames) {
        var rad = (RadioButton)Controls.Find(radName, true).FirstOrDefault();
        if (rad == null) continue;
        if (!rad.Checked) continue;
            if (rad.AccessibleDescription.Split('_')[0] == WagerType.TEAMTOTALPOINTS || rad.AccessibleDescription.Split('_')[0] == WagerType.TOTALPOINTS)
              ou = rad.AccessibleDescription.Split('_')[1];
            break;
          }

      return ou;
    }

    private RadioButton GetCheckedWagerOption(string selectedWagerType) {
      RadioButton rad = null;
      const string radName = "rad";

      switch (selectedWagerType) {
        case "Spread":
        case "MoneyLine":
        case "TeamTotalsOver":
        case "TeamTotalsUnder":
          rad = ((RadioButton)Controls.Find(radName + selectedWagerType, true).FirstOrDefault());
          if (rad != null) rad.Checked = true;
          break;

        case "Totals":
          if (!string.IsNullOrEmpty(PreviouslySelectedOUflag)) {
            if (PreviouslySelectedOUflag == "O") {
              rad = ((RadioButton)Controls.Find(radName + selectedWagerType + "Over", true).FirstOrDefault());
              if (rad != null) rad.Checked = true;
            }
            else {
              rad = ((RadioButton)Controls.Find(radName + selectedWagerType + "Under", true).FirstOrDefault());
              if (rad != null) rad.Checked = true;
            }
          }

          break;
      }

      return rad;
    }

    public string GetCheckedWagerTypeCode() {
      var wagerType = "";
      foreach (var radName in _radNames) {
        var rad = (RadioButton)Controls.Find(radName, true).FirstOrDefault();
        if (rad == null) continue;
        if (!rad.Checked) continue;
        wagerType = rad.AccessibleDescription.Split('_')[0];
        break;
      }

      return wagerType;
    }

    private string GetCheckedWagerTypeName() {
      var checkedRadName = "";
      foreach (var radName in _radNames) {
        var rad = (RadioButton)Controls.Find(radName, true).FirstOrDefault();
        if (rad == null) continue;
        if (!rad.Checked) continue;
        checkedRadName = rad.Name;
        break;
      }

      return checkedRadName;
    }

    private string GetChosenTeamId() {
      var chosenTeamId = "";

      foreach (Control ctrl in grpTeams.Controls) {
        if (ctrl.GetType().ToString() == "System.Windows.Forms.RadioButton") {
          if (((RadioButton)ctrl).Checked) {
            chosenTeamId = ctrl.Text.Trim();
            break;
          }
        }
      }

      return chosenTeamId;
    }

    private double GetRiskAmount(string toWinAmount) {
      double riskAmount = 0;

      if (toWinAmount != "") {
        double workingToWin;
        double.TryParse(toWinAmount.Replace(",", ""), out workingToWin);
        if (AmericanOdds != null)
          riskAmount = LineOffering.DetermineRiskAmount(workingToWin, (double)AmericanOdds, Params.IncludeCents);
        WagerTypeMode = "ToWin";
      }
      else {
        riskAmount = 0;
        WagerTypeMode = "Risk";
      }
      return riskAmount;
    }

    private double GetToWinAmount(string riskAmount) {
      double toWinAmount = 0;
      if (riskAmount != "") {
        double workingRisk;
        double.TryParse(riskAmount.Replace(",", ""), out workingRisk);
        if (AmericanOdds != null)
          toWinAmount = LineOffering.DetermineToWinAmount(workingRisk, (double)AmericanOdds, Params.IncludeCents);
        WagerTypeMode = "Risk";
      }
      else {
        toWinAmount = 0;
        WagerTypeMode = "ToWin";
      }
      return toWinAmount;
    }

    private string GetSelectedWagerOption(String optionType) {
      var retOptionType = "";

      var controlName = GetCheckedWagerTypeName();

      var rad = (RadioButton)Controls.Find(controlName, true).FirstOrDefault();

      if (rad != null) {
        retOptionType = optionType == "wagerType" ? rad.AccessibleDescription.ToString(CultureInfo.InvariantCulture).Split('_')[0] : rad.AccessibleDescription.ToString(CultureInfo.InvariantCulture).Split('_')[1];
      }

      return retOptionType;
    }

    public double GetWagerTypeAdjustment() {
      double adjustment = 0;

      var controlName = GetCheckedWagerTypeName();

      if (string.IsNullOrEmpty(controlName)) return adjustment;

      var rad = (RadioButton)Controls.Find(controlName, true).FirstOrDefault();

      var ctrlAccessDesc = "";

      if (rad != null) ctrlAccessDesc = rad.AccessibleDescription;
      switch (ctrlAccessDesc.Split('_')[0])
      {
          case WagerType.SPREAD:
              if (FinalAmericanSpreadAdj != null) adjustment = (double)FinalAmericanSpreadAdj;
              break;
          case WagerType.TOTALPOINTS:
              if (ctrlAccessDesc.Split('_')[1] == "O")
              {
                  if (FinalAmericanTotalsOAdj != null) adjustment = (double)FinalAmericanTotalsOAdj;
              }
              else
              {
                  if (FinalAmericanTotalsUAdj != null) adjustment = (double)FinalAmericanTotalsUAdj;
              }
              break;
          case WagerType.MONEYLINE:
              if (FinalAmericanMoneyLineAdj != null) adjustment = (double)FinalAmericanMoneyLineAdj;
              break;
          case WagerType.TEAMTOTALPOINTS:
              if (ctrlAccessDesc.Split('_')[1] == "O")
              {
                  if (FinalAmericanTeamTotalsOAdj != null) adjustment = (double)FinalAmericanTeamTotalsOAdj;
              }
              else
              {
                  if (FinalAmericanTeamTotalsUAdj != null) adjustment = (double)FinalAmericanTeamTotalsUAdj;
              }
              break;
      }
      return adjustment;
    }

    private String GetWagerTypeLine() {
      String buyingPtsLine;
      var adjustmentValue = "";
      var controlName = GetCheckedWagerTypeName();
      var lblWagerType = (Label)Controls.Find(controlName.Replace("rad", "lbl"), true).FirstOrDefault();
      if (lblWagerType != null) adjustmentValue = lblWagerType.Text;

      var line = LineOffering.ConvertToStringBaseLine(adjustmentValue.Split(' ')[0]);

      var isAsian = TwUtilities.IsAsianLine(WagerType.SPREAD, _chosenTeamId, SelectedGamePeriodInfo);
      if (controlName == "radSpread" || isAsian) {
        if (dudBuyPointsSpread.Visible && dudBuyPointsSpread.SelectedItem != null || (dudBuyPointsSpread.SelectedItem != null && isAsian)) {
            buyingPtsLine = LineOffering.ConvertToStringBaseLine(dudBuyPointsSpread.SelectedItem.ToString());

          if (buyingPtsLine.Trim() != line.Trim()) {
            line = buyingPtsLine.Trim();
          }
        }
      }

      isAsian = TwUtilities.IsAsianLine(WagerType.TOTALPOINTS, _chosenTeamId, SelectedGamePeriodInfo);
      if (controlName == "radTotalsOver" || controlName == "radTotalsUnder" || isAsian) {
        if (dudBuyPointsTotalPoints.Visible && dudBuyPointsTotalPoints.SelectedItem != null || (dudBuyPointsTotalPoints.SelectedItem != null && isAsian)) {
            buyingPtsLine = LineOffering.ConvertToStringBaseLine(dudBuyPointsTotalPoints.SelectedItem.ToString());
          if (buyingPtsLine.Trim() != line.Trim()) {
            line = buyingPtsLine.Trim();
          }
        }
      }

      //if (line.Contains(",")) {
      //return LineOffering.ConvertToStringBaseLine(line);//LineOffering.ConvertToBaseLineFromAsianDisplay(adjustmentValue.Split(' ')[0]);
      //}
      return line;
    }

    private void GoToPlaceBet() {
      if (_itemWagerType == null || FrmMode == "Edit")
        _itemWagerType = GetSelectedWagerOption("wagerType");
      if (_totalsOu == null || FrmMode == "Edit")
        _totalsOu = GetSelectedWagerOption("");
      if (_item == null || FrmMode == "Edit")
        _item = WagerType.GetFromCode(_itemWagerType);
      if (_chosenTeamRotNum == null || FrmMode == "Edit") {
        var chosenTeamId = "";
        foreach (var ctrl in from object ctrl in grpTeams.Controls
                             where ctrl.GetType().ToString() == "System.Windows.Forms.RadioButton"
                             where ((RadioButton)ctrl).Checked
                             select ctrl) {
          chosenTeamId = ((RadioButton)ctrl).Text;
          break;
        }
        _chosenTeamRotNum = TwUtilities.GetChosenTeamRotNumber(SelectedGamePeriodInfo, chosenTeamId);
      }
      if (_chosenTeamId == null || FrmMode == "Edit")
        _chosenTeamId = GetChosenTeamId();
      if (_chosenTeamIdDesc == null || FrmMode == "Edit")
        _chosenTeamIdDesc = TwUtilities.GetChosenTeamIdDescription(SelectedGamePeriodInfo, _itemWagerType, _chosenTeamId);
      if (_chosenLine == null || FrmMode == "Edit")
        _chosenLine = GetWagerTypeLine();
      if (_chosenAmericanPrice == null || FrmMode == "Edit")
        _chosenAmericanPrice = GetWagerTypeAdjustment().ToString(CultureInfo.InvariantCulture);

      if (CurrentWagerTypeName == WagerType.TEASERNAME)
      {
        var frm = FormF.GetFormByName("frmPlaceTeaser", this);

        if (frm == null) {
          return;
        }

        var frmPt = (FrmPlaceTeaser)frm;
        if (frmPt.TeaserSportSpecsByTeaser != null && frmPt.TeaserName != null)
          _lineAndPrice = TwUtilities.CalculateTeaserAdjustedLine(frmPt.TeaserName, frmPt.TeaserSportSpecsByTeaser, GetCheckedWagerTypeCode(), GetCheckedTotalsOverUnderSelection(), _selectedSportType, _selectedSportSubType, _selectedRowNumberInGroup, GetWagerTypeLine(), out _teaserPoints);
      }
      else {
        _lineAndPrice = new List<string> { _chosenLine, _chosenAmericanPrice };
      }

      var selectedGameInfo = SelectedGamePeriodInfo;
      switch (_itemWagerType)
      {
          case WagerType.SPREAD:
              if (FrmMode == "Edit" && Mdi.TicketNumber != TicketNumberToUpd && SpreadLineAdjControl != null && !String.IsNullOrEmpty(SpreadLineAdjControl.Text))
              {
                  selectedGameInfo.Spread = double.Parse(LineOffering.ConvertToStringBaseLine(String.IsNullOrEmpty(SpreadLineAdjControl.Text) ? "0" : SpreadLineAdjControl.Text));
              }
              break;
          case WagerType.TOTALPOINTS:
              if (FrmMode == "Edit" && Mdi.TicketNumber != TicketNumberToUpd)
              {
                  var line = (selectedGameInfo.TotalPoints ?? 0).ToString(CultureInfo.InvariantCulture);
                  if (_totalsOu == "O" && TotalPointsOverLineAdjControl != null && !String.IsNullOrEmpty(TotalPointsOverLineAdjControl.Text))
                  {
                      line = TotalPointsOverLineAdjControl.Text;
                  }
                  else if (_totalsOu == "U" && TotalPointsUnderLineAdjControl != null && !String.IsNullOrEmpty(TotalPointsUnderLineAdjControl.Text))
                  {
                      line = TotalPointsUnderLineAdjControl.Text;
                  }
                  selectedGameInfo.TotalPoints = double.Parse(LineOffering.ConvertToStringBaseLine(line));
              }
              break;
      }

      if (_wagerItemDescription == null || FrmMode == "Edit")
          _wagerItemDescription = FrmSportAndGameSelection.BuildWagerDescription(_chosenTeamRotNum, _chosenLine, CurrentWagerTypeName == "Teaser" ? _teaserPoints.ToString(CultureInfo.InvariantCulture) : _chosenAmericanPrice, _itemWagerType, _totalsOu, _chosenTeamId, _selectedSportType, selectedGameInfo, CurrentWagerTypeName, _selectedPrice, TwUtilities.GetOriginalLine(_itemWagerType, selectedGameInfo, _chosenTeamId, _selectedRowNumberInGroup), GetAdjustedBpLine(_itemWagerType, _chosenLine), chbPitcher1MustStart.Checked ? "Y" : "N", chbPitcher2MustStart.Checked ? "Y" : "N", chbFixedOdds.Checked ? "Y" : "N");

      FrmSportAndGameSelection.PlaceBet(FrmMode, txtRisk, txtToWin, CurrentWagerTypeName, _itemWagerType, WagerTypeMode,
          _selectedPrice, TicketNumberToUpd, WagerNumberToUpd, WagerItemNumberToUpd, /*DetermineSelectedRow(_selectedGvRowNumber, _originalRowNumberInGroup,
          _selectedRowNumberInGroup, _itemWagerType, _totalsOu),*/ _selectedRowNumberInGroup, _chosenLine, _chosenAmericanPrice,
          DetermineSelectedOdds(), DetermineSelectedBPointsOption(_item), _chosenTeamIdDesc, _teaserPoints,
          chbPitcher1MustStart.Checked ? "Y" : "N", chbPitcher2MustStart.Checked ? "Y" : "N", chbFixedOdds.Checked ? "Y" : "N", _totalsOu, "", _wagerItemDescription, _lineAndPrice, selectedGameInfo, IsFreePlay, "A", "N", _easternLineEnabled, _easternLineConversionInfo, GameDataSource, _complexIfBetAmount);

      var amountsStr = "";

      if (CurrentWagerTypeName == "Straight Bet" || (CurrentWagerTypeName == "If-Bet" && String.IsNullOrEmpty(_complexIfBetAmount))) {
        amountsStr = " Risking: " + txtRisk.Text + ", to Win:" + txtToWin.Text;
      }

      if (CurrentWagerTypeName == "If-Bet") {
        amountsStr = " Risking: " + _complexIfBetAmount;
      }

      Tag = "Added " + _selectedWagerType + " item to " + CurrentWagerTypeName + " in " + (String.IsNullOrEmpty(FrmMode) ? "New" : FrmMode) + " mode. Customer: " + _currentCustomerId.Trim() + " " + TicketNumberToUpd.ToString()
        + "." + WagerNumberToUpd.ToString() + "." + WagerItemNumberToUpd.ToString() + " (" + _wagerItemDescription + ")" + amountsStr;

    }

    private void GoToProps() {
      if (_selectedGameOptions != null) {
        var correlationId = (from g in _selectedGameOptions where g.GameNum == _selectedGameNumber && g.PeriodNumber == _selectedGamePeriod select g.CorrelationID).FirstOrDefault();

        if (correlationId != null)
          correlationId = correlationId.Trim();
        else {
          MessageBox.Show(@"No correlated contests found.");
          return;
        }

        var contestsCount = (from a in FrmSportAndGameSelection.ActiveContests where a.CorrelationID.Trim() == correlationId select a).Count();

        if (contestsCount > 0) {
          var gamesGvw = (DataGridView)FrmSportAndGameSelection.Controls.Find(FrmSportAndGameSelection.GamesGridViewInUse, true).FirstOrDefault();
          var contestsGvw = (DataGridView)FrmSportAndGameSelection.Controls.Find(FrmSportAndGameSelection.PropsGridViewInUse, true).FirstOrDefault();

          if (contestsGvw != null) {
            if (contestsGvw.Rows.Count > 0)
              contestsGvw.Rows.Clear();
            contestsGvw.Visible = true;
          }
          else
            return;

          if (gamesGvw != null) {
            if (gamesGvw.Rows.Count > 0)
              gamesGvw.Rows.Clear();
            gamesGvw.Visible = false;
          }

          var lblCurrentlyShowing = (Label)FrmSportAndGameSelection.Controls.Find("lblCurrentlyShowing", true).FirstOrDefault();
          if (lblCurrentlyShowing != null) {
            lblCurrentlyShowing.Text = "";
          }

          FrmSportAndGameSelection.ContestNodesInfo = new List<FrmSportAndGameSelection.ContestTreeNodeDesc>();

          var nodeInfo = new FrmSportAndGameSelection.ContestTreeNodeDesc { CorrelationId = correlationId };

          FrmSportAndGameSelection.ContestNodesInfo.Add(nodeInfo);

          FrmSportAndGameSelection.FillContestsGridView(FrmSportAndGameSelection.ContestNodesInfo);
          FrmSportAndGameSelection.DisableGamePeriodsRadBtns();
          Close();
        }
        else {
          MessageBox.Show(@"No correlated contests found.");
        }
      }
      else {
        MessageBox.Show(@"No correlated contests found.");
      }
    }

    private void HandleSpreadPointsBuying(object sender) {
      //var costToBuySpreadLbl = lblBuyPtsSpreadSpec.Text.Split(' ')[2];

      if (!lblBuyPtsSpreadSpec.Text.Contains("progressive") && lblBuyPtsSpreadSpec.Text.Split(' ')[2] == "##") return;

      if (((DomainUpDown)sender).SelectedItem == null) return;
      double baseSpread;
      var baseSpreadSpec = lblSpread.Text.Split(' ')[0] + "  ";

      double.TryParse(LineOffering.ConvertToStringBaseLine(baseSpreadSpec), out baseSpread);
      var selectedSpread = LineOffering.ConvertToStringBaseLine(((DomainUpDown)sender).SelectedItem.ToString());

      double selSpread;
      double.TryParse(selectedSpread, out selSpread);

      double newSpreadAdj;
      var currentGameInfo = TwUtilities.ConvertToGameObject(SelectedGamePeriodInfo/*(from go in _selectedGameOptions where go.GameNum == _selectedGameNumber && go.PeriodNumber == _selectedGamePeriod select go).FirstOrDefault()*/,
          CurrentWagerTypeName, FrmSportAndGameSelection.VigDiscountForAllWagerTypes);
      if (FrmMode == "Edit" && Mdi.TicketNumber != TicketNumberToUpd && SpreadLineAdjControl != null && SpreadPriceControl.Text.Length > 0)
      {
          currentGameInfo.Spread = baseSpread;
          currentGameInfo.SpreadAdj1 = currentGameInfo.SpreadAdj2 = int.Parse(SpreadPriceControl.Text);
      }  
      if (_buyPointsOptions == null) {        
        _buyPointsOptions = BuyPoints.CreateBuyPointsOptionsObject(WagerType.SPREAD, _costToBuyPointsObj, currentGameInfo, _costToBuyProgressivePoints, _selectedRowNumberInGroup);
        }

      if (_buyPointsOptions != null && _buyPointsOptions.Count > 0) {
        newSpreadAdj = (from bp in _buyPointsOptions where bp.SpreadOrTotal == selSpread select bp.Adjustment).FirstOrDefault();
        if ((from bp2 in _buyPointsOptions where bp2.SpreadOrTotal == selSpread select bp2.IsProgressive).FirstOrDefault()) {
          WriteProgressiveBuyPointsDesc(WagerType.SPREAD, (from bp1 in _buyPointsOptions where bp1.SpreadOrTotal == selSpread select bp1).FirstOrDefault(), baseSpread == selSpread);
        }
      }
      else {
        int? spreadAdj1 = SelectedGamePeriodInfo.SpreadAdj1 ?? 0;
        int? spreadAdj2 = SelectedGamePeriodInfo.SpreadAdj2 ?? 0;

        if (_selectedRowNumberInGroup == 1) {
          newSpreadAdj = (double)spreadAdj1;
        }
        else {
          newSpreadAdj = (double)spreadAdj2;
        }
        if (FrmMode == "Edit" && Mdi.TicketNumber != TicketNumberToUpd && SpreadPriceControl != null && SpreadPriceControl.Text.Length > 0)
        {
            newSpreadAdj = double.Parse(SpreadPriceControl.Text);
        }
      }

      switch (_selectedPrice) {
        case "American":
        case LineOffering.PRICE_AMERICAN:
          if (newSpreadAdj > 0)
            baseSpreadSpec += "+" + newSpreadAdj.ToString(CultureInfo.InvariantCulture);
          else
            baseSpreadSpec += newSpreadAdj.ToString(CultureInfo.InvariantCulture);
          break;
        case "Decimal":
        case LineOffering.PRICE_DECIMAL:
          baseSpreadSpec += OddsTypes.UStoDecimal(newSpreadAdj, DecimalPrecision);
          break;
        case "Fractional":
        case LineOffering.PRICE_FRACTIONAL:
          var newSpreadDec = OddsTypes.UStoDecimal(newSpreadAdj, DecimalPrecision);
          if (FractionalMaxDenominator != null) {
            var fractionAry = OddsTypes.DecToFraction(double.Parse(newSpreadDec.ToString(CultureInfo.InvariantCulture)), (int)FractionalMaxDenominator);
            baseSpreadSpec += fractionAry.Numerator + "/" + fractionAry.Denominator;
          }
          break;
      }
      FinalAmericanSpreadAdj = (int)newSpreadAdj;
      lblSpread.Text = baseSpreadSpec;
      _americanOdds = null;

      if (WagerTypeMode != null) {
        if (WagerTypeMode == "Risk") {
          txtToWin.Text = txtRisk.Text != "" ? LineOffering.DetermineToWinAmount(double.Parse(txtRisk.Text), newSpreadAdj, Params.IncludeCents).ToString(CultureInfo.InvariantCulture) : "";
        }
        else {
          txtRisk.Text = txtToWin.Text != "" ? LineOffering.DetermineRiskAmount(double.Parse(txtToWin.Text), newSpreadAdj, Params.IncludeCents).ToString(CultureInfo.InvariantCulture) : "";
        }
      }
      else {
        if (newSpreadAdj >= 0) {
          txtToWin.Text = txtRisk.Text != "" ? LineOffering.DetermineToWinAmount(double.Parse(txtRisk.Text), newSpreadAdj, Params.IncludeCents).ToString(CultureInfo.InvariantCulture) : "";
        }
        else {
          txtRisk.Text = txtToWin.Text != "" ? LineOffering.DetermineRiskAmount(double.Parse(txtToWin.Text), newSpreadAdj, Params.IncludeCents).ToString(CultureInfo.InvariantCulture) : "";
        }
      }
      if (!FrmSportAndGameSelection.ShowWagerLimitsInFrm.IsActive()) return;
      FrmSportAndGameSelection.ShowWagerLimitsInFrm.FrmMakeAWager = this;
      FrmSportAndGameSelection.ShowWagerLimitsInFrm.DisplayMaxWagerLimits();
    }

    private void HandleTotalPointsPointsBuying(object sender) {
      var currentGameInfo = TwUtilities.ConvertToGameObject((from go in _selectedGameOptions where go.GameNum == _selectedGameNumber && go.PeriodNumber == _selectedGamePeriod select go).FirstOrDefault(), CurrentWagerTypeName, FrmSportAndGameSelection.VigDiscountForAllWagerTypes);
      if (FrmMode == "Edit" && Mdi.TicketNumber != TicketNumberToUpd)
      {
          if (radTotalsOver.Checked && TotalPointsOverLinePriceControl != null && TotalPointsOverLinePriceControl.Text.Length > 0)
          {
              currentGameInfo.TtlPtsAdj1 = currentGameInfo.TtlPtsAdj2 = int.Parse(TotalPointsOverLinePriceControl.Text);
          }
          else
          {
              if (TotalPointsUnderLinePriceControl != null && TotalPointsUnderLinePriceControl.Text.Length > 0)
                  currentGameInfo.TtlPtsAdj1 = currentGameInfo.TtlPtsAdj2 = int.Parse(TotalPointsUnderLinePriceControl.Text);
          }
      }

      if (_buyPointsOptions == null) {          
        _buyPointsOptions = BuyPoints.CreateBuyPointsOptionsObject(WagerType.TOTALPOINTS, _costToBuyPointsObj, currentGameInfo, _costToBuyProgressivePoints, _selectedRowNumberInGroup);
      }

      if (!lblBuyPtsTotalsSpec.Text.Contains("progressive") && lblBuyPtsTotalsSpec.Text.Split(' ')[2] == "##") return;
      if (((DomainUpDown)sender).SelectedItem == null) return;
      var selectedTotal = LineOffering.ConvertToStringBaseLine(((DomainUpDown)sender).SelectedItem.ToString());

      String newTotalSpec;

      if (radTotalsOver.Checked) {
        newTotalSpec = lblTotalsOver.Text.Split(' ')[0] + "  ";
      }
      else {
        newTotalSpec = lblTotalsUnder.Text.Split(' ')[0] + "  ";
      }


      var difInTotals = Math.Abs(Math.Abs(_baseTotal) - Math.Abs(double.Parse(selectedTotal)));
      var factor = difInTotals / 0.5F;

      double newAdjTotal = 0;

      if (_costToBuyPointsByCustomer.ProgressivePointBuyingFlag == "Y" && _costToBuyProgressivePoints != null) {
        if (_buyPointsOptions != null && _buyPointsOptions.Count > 0) {
          newAdjTotal = (from bp in _buyPointsOptions where bp.SpreadOrTotal == (double.Parse(selectedTotal)) select bp.Adjustment).FirstOrDefault();
          WriteProgressiveBuyPointsDesc(WagerType.TOTALPOINTS, (from bp1 in _buyPointsOptions where bp1.Adjustment == newAdjTotal select bp1).FirstOrDefault(), difInTotals == 0);
        }

        newTotalSpec += newAdjTotal;
        if (radTotalsOver.Checked) {
          lblTotalsOver.Text = newTotalSpec;
        }
        else {
          lblTotalsUnder.Text = newTotalSpec;
        }
        FinalAmericanTotalsOAdj = (int)newAdjTotal;
        _americanOdds = null;
      }
      else {
        if (radTotalsOver.Checked) {
          if (_adjTotalOver > 0) {
            newAdjTotal = _adjTotalOver + (factor * _costToBuyPointsObj.CostToBuyTotals);
          }
          else {
            newAdjTotal = _adjTotalOver - (factor * _costToBuyPointsObj.CostToBuyTotals);
          }
          newTotalSpec += Common.CalculateTotalSpec(_selectedPrice, _adjTotalOver, newAdjTotal, DecimalPrecision, FractionalMaxDenominator);
          FinalAmericanTotalsOAdj = (int)newAdjTotal;
          lblTotalsOver.Text = newTotalSpec;
          _americanOdds = null;
        }
        else {
          if (_adjTotalUnder > 0) {
            newAdjTotal = _adjTotalUnder + (factor * _costToBuyPointsObj.CostToBuyTotals);
          }
          else {
            newAdjTotal = _adjTotalUnder - (factor * _costToBuyPointsObj.CostToBuyTotals);
          }
          newTotalSpec += Common.CalculateTotalSpec(_selectedPrice, _adjTotalUnder, newAdjTotal, DecimalPrecision, FractionalMaxDenominator);
          FinalAmericanTotalsUAdj = (int)newAdjTotal;
          lblTotalsUnder.Text = newTotalSpec;
          _americanOdds = null;
        }

      }

      if (WagerTypeMode != null) {
        if (WagerTypeMode == "Risk") {
          txtToWin.Text = txtRisk.Text != "" ? LineOffering.DetermineToWinAmount(double.Parse(txtRisk.Text), newAdjTotal, Params.IncludeCents).ToString(CultureInfo.InvariantCulture) : "";
        }
        else {
          txtRisk.Text = txtToWin.Text != "" ? LineOffering.DetermineRiskAmount(double.Parse(txtToWin.Text), newAdjTotal, Params.IncludeCents).ToString(CultureInfo.InvariantCulture) : "";
        }
      }
      else {
        if (newAdjTotal >= 0) {
          txtToWin.Text = txtRisk.Text != "" ? LineOffering.DetermineToWinAmount(double.Parse(txtRisk.Text), newAdjTotal, Params.IncludeCents).ToString(CultureInfo.InvariantCulture) : "";
        }
        else {
          txtRisk.Text = txtToWin.Text != "" ? LineOffering.DetermineRiskAmount(double.Parse(txtToWin.Text), newAdjTotal, Params.IncludeCents).ToString(CultureInfo.InvariantCulture) : "";
        }
      }
      if (FrmSportAndGameSelection.ShowWagerLimitsInFrm.IsActive()) {
        FrmSportAndGameSelection.ShowWagerLimitsInFrm.FrmMakeAWager = this;
        FrmSportAndGameSelection.ShowWagerLimitsInFrm.DisplayMaxWagerLimits();
      }
    }

    private void HandleTeamTotalsCheckChanged(object sender)
    {
        var currentGameInfo = SelectedGamePeriodInfo;
        if (((RadioButton)sender).Checked)
        {
            _selectedWagerType = ((RadioButton)sender).AccessibleDescription == @"E_O" ? WagerType.TEAMTOTALPOINTSOVERNAME : WagerType.TEAMTOTALPOINTSUNDERNAME;
            UncheckTheOtherRadioButtons(((RadioButton)sender).Name);
            if (_selectedWagerType == WagerType.TEAMTOTALPOINTSOVERNAME)
            {
                var txtLineAdjControl = panTeamTotalsOverTxtLineAdj.Controls.Find("txtTeamTotalsOverLineLineAdj", true).FirstOrDefault();
                if (txtLineAdjControl != null)
                {
                    ((TxtLineAdjControl)txtLineAdjControl).SelectedWagerType = _selectedWagerType;
                    if (FrmMode == "Edit" && Mdi.TicketNumber != TicketNumberToUpd)
                    {
                        if (_selectedRowNumberInGroup == 1)
                        {
                            currentGameInfo.Team1TtlPtsAdj1 = currentGameInfo.Team1TtlPtsAdj2 = int.Parse(txtLineAdjControl.Text);
                        }
                        else
                        {
                            currentGameInfo.Team2TtlPtsAdj1 = currentGameInfo.Team2TtlPtsAdj2 = int.Parse(txtLineAdjControl.Text);
                        }
                    }

                }
            }
            else
            {
                var txtLineAdjControl = panTeamTotalsUnderTxtLineAdj.Controls.Find("txtTeamTotalsUnderLineLineAdj", true).FirstOrDefault();
                if (txtLineAdjControl != null)
                {
                    ((TxtLineAdjControl)txtLineAdjControl).SelectedWagerType = _selectedWagerType;
                    if (FrmMode == "Edit" && Mdi.TicketNumber != TicketNumberToUpd)
                    {
                        if (_selectedRowNumberInGroup == 1)
                        {
                            currentGameInfo.Team1TtlPtsAdj1 = currentGameInfo.Team1TtlPtsAdj2 = int.Parse(txtLineAdjControl.Text);
                        }
                        else
                        {
                            currentGameInfo.Team2TtlPtsAdj1 = currentGameInfo.Team2TtlPtsAdj2 = int.Parse(txtLineAdjControl.Text);
                        }
                    }
                }
            }
            EnableEditWagerTextBoxes(_selectedWagerType, null);

            _selectedRowNumberInGroup = _selectedWagerType == WagerType.TEAMTOTALPOINTSOVERNAME ? 1 : 2;
            SetBuyingPointsOptions(WagerType.TEAMTOTALPOINTS, "");
            ShowHideFormObjects();
            chbPitcher1MustStart.Checked = true;
            chbPitcher2MustStart.Checked = true;
            RecalculateWagerAmounts();
            /*if (txtRisk.Text.Length > 0)
            {
                txtToWin.Text = LineOffering.DetermineToWinAmount(double.Parse(txtRisk.Text), GetWagerTypeAdjustment(), Params.IncludeCents).ToString(CultureInfo.InvariantCulture);
            }
            txtToWin.Focus();*/
        }
        _buyPointsOptions = null;
        SetLayout();
        if (FrmSportAndGameSelection.ShowWagerLimitsInFrm.IsActive())
        {
            FrmSportAndGameSelection.ShowWagerLimitsInFrm.FrmMakeAWager = this;
            FrmSportAndGameSelection.ShowWagerLimitsInFrm.DisplayMaxWagerLimits();
        }
    }

    private void HandleAdjustmentValueChange(string objectName, string newText)
    {
        double outNum;
        if (!Double.TryParse(LineOffering.ConvertToStringBaseLine(newText), out outNum))
            return;

        switch (objectName)
        {
            case "SpreadLineAdjControl":
                if (dudBuyPointsSpread.Visible)
                {
                    dudBuyPointsSpread.SelectedItemChanged -= dudBuyPointsSpread_SelectedItemChanged;
                    dudBuyPointsSpread.SelectedIndex = -1;
                    dudBuyPointsSpread.Items.Clear();
                    dudBuyPointsSpread.Refresh();
                    dudBuyPointsSpread.SelectedItemChanged += dudBuyPointsSpread_SelectedItemChanged;
                    _buyPointsOptions = null;
                    SetBuyingPointsOptions(WagerType.SPREAD, "");
                }
                if (SpreadPriceControl.Text.Length >= 3)
                {
                    lblSpread.Text = newText + @"  " + SpreadPriceControl.Text;
                    lblTeaserSpread.Text = newText;
                }
                break;
            case "TotalPointsOverLineAdjControl":
                if (dudBuyPointsTotalPoints.Visible)
                {
                    dudBuyPointsTotalPoints.SelectedItemChanged -= dudBuyPointsTotalPoints_SelectedItemChanged;
                    dudBuyPointsTotalPoints.SelectedIndex = -1;
                    dudBuyPointsTotalPoints.Items.Clear();
                    dudBuyPointsTotalPoints.Refresh();
                    _buyPointsOptions = null;
                    dudBuyPointsTotalPoints.SelectedItemChanged += dudBuyPointsTotalPoints_SelectedItemChanged;
                    SetBuyingPointsOptions(WagerType.TOTALPOINTS, "O");
                }
                if (TotalPointsOverLinePriceControl.Text.Length >= 3)
                {
                    lblTotalsOver.Text = newText + @"  " + TotalPointsOverLinePriceControl.Text;
                    lblTeaserTotalsOver.Text = newText;
                }
                break;
            case "TotalPointsUnderLineAdjControl":
                if (dudBuyPointsTotalPoints.Visible)
                {
                    dudBuyPointsTotalPoints.SelectedItemChanged -= dudBuyPointsTotalPoints_SelectedItemChanged;
                    dudBuyPointsTotalPoints.SelectedIndex = -1;
                    dudBuyPointsTotalPoints.Items.Clear();
                    dudBuyPointsTotalPoints.Refresh();
                    _buyPointsOptions = null;
                    dudBuyPointsTotalPoints.SelectedItemChanged += dudBuyPointsTotalPoints_SelectedItemChanged;
                    SetBuyingPointsOptions(WagerType.TOTALPOINTS, "U");
                }
                if (TotalPointsUnderLinePriceControl.Text.Length >= 3)
                {
                    lblTotalsUnder.Text = newText + @"  " + TotalPointsUnderLinePriceControl.Text;
                    lblTeaserTotalsUnder.Text = newText;
                }
                break;
            case "TeamTotalsOverLineAdjControl":
                if (TeamTotalsOverLinePriceControl.Text.Length >= 3)
                {
                    lblTeamTotalsOver.Text = newText + @"  " + TeamTotalsOverLinePriceControl.Text;
                    lblTeaserTeamTotalsOver.Text = newText;
                }
                break;
            case "TeamTotalsUnderLineAdjControl":
                if (TeamTotalsUnderLinePriceControl.Text.Length >= 3)
                {
                    lblTeamTotalsUnder.Text = newText + @"  " + TeamTotalsUnderLinePriceControl.Text;
                    lblTeaserTeamTotalsUnder.Text = newText;
                }
                break;
        }
    }

    private void HandlePriceValueChange(string objectName, string newText)
    {
        switch (objectName)
        {
            case "SpreadPriceControl":
                if (newText.Length >= 3)
                {
                    FinalAmericanSpreadAdj = int.Parse(newText);
                    if (dudBuyPointsSpread.Visible)
                    {
                        dudBuyPointsSpread.SelectedItemChanged -= dudBuyPointsSpread_SelectedItemChanged;
                        dudBuyPointsSpread.SelectedIndex = -1;
                        dudBuyPointsSpread.Items.Clear();
                        dudBuyPointsSpread.Refresh();
                        dudBuyPointsSpread.SelectedItemChanged += dudBuyPointsSpread_SelectedItemChanged;
                        _buyPointsOptions = null;
                        SetBuyingPointsOptions(WagerType.SPREAD, "");
                    }

                    lblSpread.Text = SpreadLineAdjControl.Text + @"  " + newText;
                }
                break;
            case "TotalPointsOverLinePriceControl":
                if (newText.Length >= 3)
                {
                    FinalAmericanTotalsOAdj = int.Parse(newText);
                    if (dudBuyPointsTotalPoints.Visible)
                    {
                        dudBuyPointsTotalPoints.SelectedItemChanged -= dudBuyPointsTotalPoints_SelectedItemChanged;
                        dudBuyPointsTotalPoints.SelectedIndex = -1;
                        dudBuyPointsTotalPoints.Items.Clear();
                        dudBuyPointsTotalPoints.Refresh();
                        _buyPointsOptions = null;
                        dudBuyPointsTotalPoints.SelectedItemChanged += dudBuyPointsTotalPoints_SelectedItemChanged;
                        SetBuyingPointsOptions(WagerType.TOTALPOINTS, "O");
                    }

                    lblTotalsOver.Text = TotalPointsOverLineAdjControl.Text + @"  " + newText;
                }
                break;
            case "TotalPointsUnderLinePriceControl":
                if (newText.Length >= 3)
                {
                    FinalAmericanTotalsUAdj = int.Parse(newText);
                    if (dudBuyPointsTotalPoints.Visible)
                    {
                        dudBuyPointsTotalPoints.SelectedItemChanged -= dudBuyPointsTotalPoints_SelectedItemChanged;
                        dudBuyPointsTotalPoints.SelectedIndex = -1;
                        dudBuyPointsTotalPoints.Items.Clear();
                        dudBuyPointsTotalPoints.Refresh();
                        _buyPointsOptions = null;
                        dudBuyPointsTotalPoints.SelectedItemChanged += dudBuyPointsTotalPoints_SelectedItemChanged;
                        SetBuyingPointsOptions(WagerType.TOTALPOINTS, "U");
                    }

                    lblTotalsUnder.Text = TotalPointsUnderLineAdjControl.Text + @"  " + newText;
                }
                break;
            case "MoneyLinePriceControl":
                if (newText.Length >= 3)
                {
                    FinalAmericanMoneyLineAdj = int.Parse(newText);
                    lblMoneyLine.Text = newText;
                }
                break;
            case "TeamTotalsOverLinePriceControl":
                if (newText.Length >= 3)
                {
                    FinalAmericanTeamTotalsOAdj = int.Parse(newText);
                    lblTeamTotalsOver.Text = TeamTotalsOverLineAdjControl.Text + @"  " + newText;
                }
                break;
            case "TeamTotalsUnderLinePriceControl":
                if (newText.Length >= 3)
                {
                    FinalAmericanTeamTotalsUAdj = int.Parse(newText);
                    lblTeamTotalsUnder.Text = TeamTotalsUnderLineAdjControl.Text + @"  " + newText;
                }
                break;
        }
    }

    private void WriteProgressiveBuyPointsDesc(String wagerType, BuyPoints.BuyPointsLineAndAdjustment buyPointsLineAndAdjustment, bool isBase)
    {
        if (isBase)
        {
            lblBuyPtsTotalsSpec.Text = lblBuyPtsSpreadSpec.Text = @"progressive";
            return;
        }
        var buyPointsSlLabelText = "@" + buyPointsLineAndAdjustment.CostPerHalfPoint.ToString(CultureInfo.InvariantCulture) + " progressive";

        switch (wagerType)
        {
            case WagerType.SPREAD:
                buyPointsSlLabelText += "\r\n+" + buyPointsLineAndAdjustment.OnOff3Ratio.ToString(CultureInfo.InvariantCulture) + " - On 3     +" + buyPointsLineAndAdjustment.Off3Ratio + " - Off 3"
                                      + "\r\n+" + buyPointsLineAndAdjustment.OnOff7Ratio.ToString(CultureInfo.InvariantCulture) + " - On 7     +" + buyPointsLineAndAdjustment.Off7Ratio + " - Off 7";
                lblBuyPtsSpreadSpec.Text = buyPointsSlLabelText;
                break;
            case WagerType.TOTALPOINTS:
                lblBuyPtsTotalsSpec.Text = buyPointsSlLabelText;
                break;
        }

    }

    private void InitializeForm() {
      txtPeriod.Text = FrmMode == "Edit" ? _selectedGamePeriodDescription : (from go in _selectedGameOptions where go.GameNum == _selectedGameNumber && go.PeriodNumber == _selectedGamePeriod select go.PeriodDescription).FirstOrDefault();
      cmbGoToGamePeriod.SelectedIndexChanged -= cmbGoToGamePeriod_SelectedIndexChanged;
      radTeam1.CheckedChanged -= radTeam1_CheckedChanged;
      radTeam2.CheckedChanged -= radTeam2_CheckedChanged;
      radSpread.CheckedChanged -= radSpread_CheckedChanged;
      radMoneyLine.CheckedChanged -= radMoneyLine_CheckedChanged;
      radDraw.CheckedChanged -= radDraw_CheckedChanged;
      radTotalsOver.CheckedChanged -= radTotalsOver_CheckedChanged;
      radTotalsUnder.CheckedChanged -= radTotalsUnder_CheckedChanged;
      radTeamTotalsOver.CheckedChanged -= radTeamTotalsOver_CheckedChanged;
      radTeamTotalsUnder.CheckedChanged -= radTeamTotalsUnder_CheckedChanged;

      if (SelectedGamePeriodInfo != null)
      {
          _selectedSportType = TwUtilities.GetSelectedSportType(SelectedGamePeriodInfo);
          _selectedSportSubType = TwUtilities.GetSelectedSportSubType(SelectedGamePeriodInfo);
      }

      if (FrmMode == "Edit" && Mdi.TicketNumber != TicketNumberToUpd)
      {
          AddEditingLineValueControls();
          EnableEditWagerTextBoxes(_selectedWagerType, null);
      }

      BuildWagerSpecsDescription(_selectedRowNumberInGroup);
      CheckProperWagerOption(_selectedWagerType);
      cmbGoToGamePeriod.SelectedIndexChanged += cmbGoToGamePeriod_SelectedIndexChanged;
      radTeam1.CheckedChanged += radTeam1_CheckedChanged;
      radTeam2.CheckedChanged += radTeam2_CheckedChanged;
      radSpread.CheckedChanged += radSpread_CheckedChanged;
      radMoneyLine.CheckedChanged += radMoneyLine_CheckedChanged;
      radDraw.CheckedChanged += radDraw_CheckedChanged;
      radTotalsOver.CheckedChanged += radTotalsOver_CheckedChanged;
      radTotalsUnder.CheckedChanged += radTotalsUnder_CheckedChanged;
      radTeamTotalsOver.CheckedChanged += radTeamTotalsOver_CheckedChanged;
      radTeamTotalsUnder.CheckedChanged += radTeamTotalsUnder_CheckedChanged;
      CheckSelectedTeam(_selectedRowNumberInGroup);
      TwUtilities.GetAvailableGamePeriodsForDropDown(cmbGoToGamePeriod, _selectedGameOptions, _selectedGameNumber, txtPeriod.Text);
      if (FrmMode == "Edit") {
        txtRisk.TextChanged -= txtRisk_TextChanged;
        txtToWin.TextChanged -= txtToWin_TextChanged;
        ChangeAcceptButtonText();
        dudBuyPointsSpread.SelectedItemChanged -= dudBuyPointsSpread_SelectedItemChanged;
        dudBuyPointsTotalPoints.SelectedItemChanged -= dudBuyPointsTotalPoints_SelectedItemChanged;
        if (TicketNumberToUpd == Mdi.TicketNumber)
          UpdatePreviouslySelectedOdds(GetCheckedWagerOption(_selectedWagerType));
        dudBuyPointsSpread.SelectedItemChanged += dudBuyPointsSpread_SelectedItemChanged;
        dudBuyPointsTotalPoints.SelectedItemChanged += dudBuyPointsTotalPoints_SelectedItemChanged;
        if (CurrentWagerTypeName == "Straight Bet" || CurrentWagerTypeName == "If-Bet") {
          txtRisk.Text = PreviouslySelectedAmtWagered;
          txtToWin.Text = PreviouslySelectedToWinAmt;
        }
        txtRisk.TextChanged += txtRisk_TextChanged;
        txtToWin.TextChanged += txtToWin_TextChanged;
      }
      ShowHideFormObjects();
      SetLayout();
      SetFormPosition();
      Mdi.LayoutManager.ApplyLayout(this);
      // the origin of this value set comes from the special if bets (Action Reverse and Bird Cage),
      // that define an amount that needs to be targeted in the to win or to risk depending on
      // the adjustment sign

      if (CurrentWagerTypeName == "Teaser") {
        cmbGoToGamePeriod.Enabled = false;
        btnAddToTeaser.Focus();
      }

      if (txtToWin.Text == "" || txtRisk.Text != "") return;
      var adj = GetWagerTypeAdjustment();
      WagerTypeMode = adj >= 0 ? "Risk" : "ToWin";

      if (adj < 0) {
        txtRisk.Text = GetRiskAmount(txtToWin.Text).ToString(CultureInfo.InvariantCulture);
      }
      else {
        txtRisk.Text = txtToWin.Text;
        txtToWin.Text = LineOffering.DetermineToWinAmount(double.Parse(txtRisk.Text), adj, Params.IncludeCents).ToString(CultureInfo.InvariantCulture);
      }
    }

    private bool IsValidWager(string wagerTypeName) {
      if (wagerTypeName == "Parlay" || wagerTypeName == "Teaser")
        return true;

      return Common.IsValidWager(txtRisk, txtToWin, grpRiskAmount, grpWinAmount, Mdi, TicketNumberToUpd, WagerNumberToUpd, FrmMode, ParentFrm.CurrentCustomerCurrency);
    }

    private bool IsWagerTypeAvailable(string wagerType) {
      var wtAllowed = false;

      if (_selectedSportType == null && _selectedSportSubType == null)
        return false;

      List<spLOGetTeaserSportSpec_Result> selectedTeaserSportsSpecs = null;

      var frm = FormF.GetFormByName("FrmPlaceTeaser", this);

      if (frm == null) {
        return false;
      }

      var frmPt = (FrmPlaceTeaser)frm;

      if (frmPt.TeaserSportSpecsByTeaser != null && frmPt.TeaserSportSpecsByTeaser.Count > 0) {

        selectedTeaserSportsSpecs = frmPt.TeaserSportSpecsByTeaser;
      }

      if (selectedTeaserSportsSpecs == null || _selectedSportType == null) return false;
      if (selectedTeaserSportsSpecs.Any(item => wagerType.Trim() == item.WagerType.Trim() && _selectedSportType.Trim() == item.SportType.Trim() && _selectedSportSubType.Trim() == item.SportSubType.Trim())) {
        wtAllowed = true;
      }

      return wtAllowed;

    }

    private void SetBuyingPointsOptions(string wagerType, string overUnder) {
      // overUnder = 1 ==> over, overUnder = 2 ==> under
      dudBuyPointsSpread.SelectedItemChanged -= dudBuyPointsSpread_SelectedItemChanged;
      dudBuyPointsTotalPoints.SelectedItemChanged -= dudBuyPointsTotalPoints_SelectedItemChanged;

      var chosenTeamId = "";

      if (SelectedGamePeriodInfo != null) {
        var sportType = SelectedGamePeriodInfo.SportType.ToString(CultureInfo.InvariantCulture).Trim();
        var sportSubType = SelectedGamePeriodInfo.SportSubType.Trim();
        using (var bp = new BuyPoints(AppModuleInfo)) {
          if (_costToBuyPointsByCustomer == null) _costToBuyPointsByCustomer = bp.GetCostToBuyPointsByCustomer(_currentCustomerId).SingleOrDefault();
          if (_costToBuyPointsByCustomer != null) _costToBuyPointsObj.ProgressivePointBuyingFlag = _costToBuyPointsByCustomer.ProgressivePointBuyingFlag;

          if (_costToBuyPointsByCustomer != null && _costToBuyPointsByCustomer.ProgressivePointBuyingFlag == "Y") {
            if (_allCostToBuyProgressivePoints == null) {
              _allCostToBuyProgressivePoints = bp.GetProgressivePointCost(_costToBuyPointsByCustomer.ProgressiveChartName, sportType, sportSubType, null).ToList();

            }
            _costToBuyProgressivePoints = _allCostToBuyProgressivePoints.Where(p => p.WagerType == wagerType).ToList();
          }
        }


        switch (_selectedRowNumberInGroup) {
          case 1:
            chosenTeamId = SelectedGamePeriodInfo.Team1ID.Trim();
            break;
          case 2:
            chosenTeamId = SelectedGamePeriodInfo.Team2ID.Trim();
            break;
          case 3:
            chosenTeamId = Draw;
            break;
        }

        var favoredTeamId = SelectedGamePeriodInfo.FavoredTeamID ?? "".Trim();
        _baseSpread = double.Parse((SelectedGamePeriodInfo.Spread ?? 0).ToString(CultureInfo.InvariantCulture));
        _baseTotal = double.Parse((SelectedGamePeriodInfo.TotalPoints ?? 0).ToString(CultureInfo.InvariantCulture));

        switch (wagerType) {
          case WagerType.MONEYLINE:
          case WagerType.TEAMTOTALPOINTS:
            _adjTotalOver = 0;
            _adjTotalUnder = 0;
            break;

          case WagerType.SPREAD:
            if (FrmMode == "Edit" && Mdi.TicketNumber != TicketNumberToUpd && SpreadLineAdjControl != null)
            {
                var line = LineOffering.ConvertToStringBaseLine(SpreadLineAdjControl.Text);           
                _baseSpread = -1 * Math.Abs(double.Parse(line));
                if (double.Parse(line) < 0)
                {
                    favoredTeamId = chosenTeamId;
                }
                else
                {
                    favoredTeamId = (SelectedGamePeriodInfo.Team1ID.Trim() == chosenTeamId ? SelectedGamePeriodInfo.Team2ID.Trim() : SelectedGamePeriodInfo.Team1ID.Trim());
                }

            }
            break;

          case WagerType.TOTALPOINTS:
            int? ttlPtsAdj1 = SelectedGamePeriodInfo.TtlPtsAdj1 ?? 0;
            int? ttlPtsAdj2 = SelectedGamePeriodInfo.TtlPtsAdj2 ?? 0;

            _adjTotalOver = int.Parse(ttlPtsAdj1.ToString());
            _adjTotalUnder = int.Parse(ttlPtsAdj2.ToString());
            if (FrmMode == "Edit" && Mdi.TicketNumber != TicketNumberToUpd && overUnder == "O" && TotalPointsOverLineAdjControl != null 
                && TotalPointsOverLinePriceControl != null && TotalPointsOverLinePriceControl.Text.Length > 0)
            {
                _baseTotal = double.Parse(LineOffering.ConvertToStringBaseLine(TotalPointsOverLineAdjControl.Text));
                _adjTotalOver = int.Parse(TotalPointsOverLinePriceControl.Text);
            }

            if (FrmMode == "Edit" && Mdi.TicketNumber != TicketNumberToUpd && overUnder == "U" && TotalPointsUnderLineAdjControl != null
                && TotalPointsUnderLinePriceControl != null && TotalPointsUnderLinePriceControl.Text.Length > 0)
            {
                _baseTotal = double.Parse(LineOffering.ConvertToStringBaseLine(TotalPointsUnderLineAdjControl.Text));
                _adjTotalUnder = int.Parse(TotalPointsUnderLinePriceControl.Text);
            }
            break;
        }

        _costToBuyPointsObj.CostToBuyMax = 0;
        if (sportType == SportTypes.BASKETBALL) {
          var sportSubTypeBasketballCollege = Params.SportSubTypeBasketballCollege;
          if (sportSubTypeBasketballCollege != null && sportSubTypeBasketballCollege.Trim() != "" && sportSubType.Trim().ToUpper().Contains(sportSubTypeBasketballCollege.Trim().ToUpper())) {
            _costToBuyPointsObj.CostToBuySpread = int.Parse(_costToBuyPointsByCustomer.CollegeBasketballSpreadBuy.ToString());
            _costToBuyPointsObj.CostToBuyTotals = int.Parse(_costToBuyPointsByCustomer.CollegeBasketballTotalBuy.ToString());
            _costToBuyPointsObj.CostToBuyMax = int.Parse(wagerType == WagerType.SPREAD ? _costToBuyPointsByCustomer.CollegeBasketballSpreadBuyMax.ToString() : _costToBuyPointsByCustomer.CollegeBasketballTotalBuyMax.ToString());

            _costToBuyPointsObj.CostToBuyOnThree =
            _costToBuyPointsObj.CostToBuyOnSeven =
            _costToBuyPointsObj.CostToBuyOffThree =
           _costToBuyPointsObj.CostToBuyOffSeven = int.Parse(wagerType == WagerType.SPREAD ? _costToBuyPointsByCustomer.CollegeBasketballSpreadBuy.ToString() : _costToBuyPointsByCustomer.CollegeBasketballTotalBuy.ToString());
          }
          else {
            _costToBuyPointsObj.CostToBuySpread = int.Parse(_costToBuyPointsByCustomer.BasketballSpreadBuy.ToString());
            _costToBuyPointsObj.CostToBuyTotals = int.Parse(_costToBuyPointsByCustomer.BasketballTotalBuy.ToString());
            _costToBuyPointsObj.CostToBuyMax = int.Parse(wagerType == WagerType.SPREAD ? _costToBuyPointsByCustomer.BasketballSpreadBuyMax.ToString() : _costToBuyPointsByCustomer.BasketballTotalBuyMax.ToString());

            _costToBuyPointsObj.CostToBuyOnThree =
            _costToBuyPointsObj.CostToBuyOnSeven =
            _costToBuyPointsObj.CostToBuyOffThree =
            _costToBuyPointsObj.CostToBuyOffSeven = int.Parse(wagerType == WagerType.SPREAD ? _costToBuyPointsByCustomer.BasketballSpreadBuy.ToString() : _costToBuyPointsByCustomer.BasketballTotalBuy.ToString());
          }
        }
        else {
          var sportSubTypeFootballCollege = Params.SportSubTypeFootballCollege;
          if (sportSubTypeFootballCollege != null && sportSubTypeFootballCollege.Trim() != "" && sportSubType.Trim().ToUpper() == sportSubTypeFootballCollege.Trim().ToUpper()) {
            _costToBuyPointsObj.CostToBuySpread = int.Parse(_costToBuyPointsByCustomer.CollegeFootballSpreadBuy.ToString());
            _costToBuyPointsObj.CostToBuyTotals = int.Parse(_costToBuyPointsByCustomer.CollegeFootballTotalBuy.ToString());

            _costToBuyPointsObj.CostToBuyOnThree = int.Parse(_costToBuyPointsByCustomer.CollegeFootballSpreadBuyOn3.ToString());
            _costToBuyPointsObj.CostToBuyOnSeven = int.Parse(_costToBuyPointsByCustomer.CollegeFootballSpreadBuyOn7.ToString());
            _costToBuyPointsObj.CostToBuyOffThree = int.Parse(_costToBuyPointsByCustomer.CollegeFootballSpreadBuyOff3.ToString());
            _costToBuyPointsObj.CostToBuyOffSeven = int.Parse(_costToBuyPointsByCustomer.CollegeFootballSpreadBuyOff7.ToString());

            _costToBuyPointsObj.CostToBuyMax = int.Parse(wagerType == WagerType.SPREAD ? _costToBuyPointsByCustomer.CollegeFootballSpreadBuyMax.ToString() : _costToBuyPointsByCustomer.CollegeFootballTotalBuyMax.ToString());
          }
          else {
            _costToBuyPointsObj.CostToBuySpread = int.Parse(_costToBuyPointsByCustomer.FootballSpreadBuy.ToString());
            _costToBuyPointsObj.CostToBuyTotals = int.Parse(_costToBuyPointsByCustomer.FootballTotalBuy.ToString());
            _costToBuyPointsObj.CostToBuyOnThree = int.Parse(_costToBuyPointsByCustomer.FootballSpreadBuyOn3.ToString());
            _costToBuyPointsObj.CostToBuyOnSeven = int.Parse(_costToBuyPointsByCustomer.FootballSpreadBuyOn7.ToString());
            _costToBuyPointsObj.CostToBuyOffThree = int.Parse(_costToBuyPointsByCustomer.FootballSpreadBuyOff3.ToString());
            _costToBuyPointsObj.CostToBuyOffSeven = int.Parse(_costToBuyPointsByCustomer.FootballSpreadBuyOff7.ToString());

            _costToBuyPointsObj.CostToBuyMax = int.Parse(wagerType == WagerType.SPREAD ? _costToBuyPointsByCustomer.FootballSpreadBuyMax.ToString() : _costToBuyPointsByCustomer.FootballTotalBuyMax.ToString());
          }
        }

        _chosenIsFavored = chosenTeamId == favoredTeamId;

          double movingSpreadTotal;
        var halfPoint = 0.5F;

        switch (wagerType) {
          case WagerType.TEAMTOTALPOINTS:
          case WagerType.MONEYLINE:
            break;
          case WagerType.SPREAD:
            if (dudBuyPointsSpread.Items.Count > 0)
              dudBuyPointsSpread.Items.RemoveRange(0, dudBuyPointsSpread.Items.Count);
            if (_chosenIsFavored) {
              movingSpreadTotal = _baseSpread + _costToBuyPointsObj.CostToBuyMax * halfPoint;
              dudBuyPointsSpread.Items.Add(TwUtilities.FormatSpreadTotal(movingSpreadTotal, wagerType));

              for (var i = 0; i < _costToBuyPointsObj.CostToBuyMax; i++) {

                movingSpreadTotal -= halfPoint;
                dudBuyPointsSpread.Items.Add(TwUtilities.FormatSpreadTotal(movingSpreadTotal, wagerType));
              }
              dudBuyPointsSpread.Text = "";
              dudBuyPointsSpread.SelectedIndex = _costToBuyPointsObj.CostToBuyMax;
            }
            else {
              movingSpreadTotal = (-1) * _baseSpread + _costToBuyPointsObj.CostToBuyMax * halfPoint;
              dudBuyPointsSpread.Items.Add(TwUtilities.FormatSpreadTotal(movingSpreadTotal, wagerType));

              for (var i = 0; i < _costToBuyPointsObj.CostToBuyMax; i++) {

                movingSpreadTotal -= halfPoint;
                dudBuyPointsSpread.Items.Add(TwUtilities.FormatSpreadTotal(movingSpreadTotal, wagerType));
              }
              dudBuyPointsSpread.Text = "";
              dudBuyPointsSpread.SelectedIndex = _costToBuyPointsObj.CostToBuyMax;
            }
            break;
          case WagerType.TOTALPOINTS:
            if (dudBuyPointsTotalPoints.Items.Count > 0)
              dudBuyPointsTotalPoints.Items.RemoveRange(0, dudBuyPointsTotalPoints.Items.Count);
            if (overUnder == "U") {
              movingSpreadTotal = _baseTotal + _costToBuyPointsObj.CostToBuyMax * halfPoint;
              halfPoint *= -1;
              dudBuyPointsTotalPoints.Items.Add(TwUtilities.FormatSpreadTotal(movingSpreadTotal, wagerType));
              for (var i = 0; i < _costToBuyPointsObj.CostToBuyMax; i++) {
                movingSpreadTotal += halfPoint;
                dudBuyPointsTotalPoints.Items.Add(TwUtilities.FormatSpreadTotal(movingSpreadTotal, wagerType));
              }
              dudBuyPointsTotalPoints.SelectedIndex = _costToBuyPointsObj.CostToBuyMax;
            }
            else {
              movingSpreadTotal = _baseTotal;
              dudBuyPointsTotalPoints.Items.Add(TwUtilities.FormatSpreadTotal(movingSpreadTotal, wagerType));
              for (var i = 0; i < _costToBuyPointsObj.CostToBuyMax; i++) {
                movingSpreadTotal -= halfPoint;
                dudBuyPointsTotalPoints.Items.Add(TwUtilities.FormatSpreadTotal(movingSpreadTotal, wagerType));
              }
              dudBuyPointsTotalPoints.SelectedIndex = 0;
            }
            break;
        }

        if (dudBuyPointsSpread.Items.Count > 0 && dudBuyPointsSpread.Visible && dudBuyPointsSpread.SelectedItem != null) { }

        if (_costToBuyProgressivePoints != null && _costToBuyProgressivePoints.Count > 0) {
          lblBuyPtsSpreadSpec.Text = lblBuyPtsTotalsSpec.Text = @"progressive";
        }
        else {
          var buyPointsSpreadLabelText = lblBuyPtsSpreadSpec.Text.Replace("##", _costToBuyPointsObj.CostToBuySpread.ToString(CultureInfo.InvariantCulture));

          if (_costToBuyPointsObj.CostToBuyOnThree + _costToBuyPointsObj.CostToBuyOnSeven + _costToBuyPointsObj.CostToBuyOffThree + _costToBuyPointsObj.CostToBuyOffSeven > 0 && buyPointsSpreadLabelText.IndexOf("On 3", StringComparison.Ordinal) < 0) {
            buyPointsSpreadLabelText += "\r\n+" + _costToBuyPointsObj.CostToBuyOnThree.ToString(CultureInfo.InvariantCulture) + " - On 3     +" + _costToBuyPointsObj.CostToBuyOffThree + " - Off 3"
                                     + "\r\n+" + _costToBuyPointsObj.CostToBuyOnSeven.ToString(CultureInfo.InvariantCulture) + " - On 7     +" + _costToBuyPointsObj.CostToBuyOffSeven + " - Off 7";
          }
          lblBuyPtsSpreadSpec.Text = buyPointsSpreadLabelText;
          lblBuyPtsTotalsSpec.Text = lblBuyPtsTotalsSpec.Text.Replace("##", _costToBuyPointsObj.CostToBuyTotals.ToString(CultureInfo.InvariantCulture));
        }
        _buyPointsOptions = null;
      }

      dudBuyPointsSpread.SelectedItemChanged += dudBuyPointsSpread_SelectedItemChanged;
      dudBuyPointsTotalPoints.SelectedItemChanged += dudBuyPointsTotalPoints_SelectedItemChanged;

    }

    private void RelocateForm() {
      if (FormF.IsLowResolution()) Location = new Point(Location.X, Location.Y - 80);

      if (FrmPlaceTeaser == null) return;
      var frmTy = FrmPlaceTeaser.Location.Y;
      var thisX = Location.X;
      var thisH = Height;
      Location = new Point(thisX, (frmTy - thisH));
    }

    private void SetFormPosition() {
      if (FrmMode == "Edit") {
        if (CurrentWagerTypeName != "Straight Bet") {

          //var xParentLocation = FrmSportAndGameSelection.Location.X;
          //var yParentLocation = FrmSportAndGameSelection.Location.Y;
          var parentWidth = FrmSportAndGameSelection.Width;
          var parentHeight = FrmSportAndGameSelection.Height;

          Location = new Point(parentWidth / 2 - Width / 2, parentHeight / 2 - Height / 4);

          StartPosition = FormStartPosition.Manual;
        }
      }
    }

    private void SetLayout() {
      var visibleControls = new List<Control>();
      var invisibleControls = new List<Control>();

      switch (CurrentWagerTypeName) {
        case "Straight Bet":
          AcceptButton = btnPlaceBet;
          visibleControls.Add(btnPlaceBet);
          invisibleControls.Add(btnAddToParlay);
          invisibleControls.Add(btnAddToTeaser);
          invisibleControls.Add(btnAddToIfBet);
          invisibleControls.Add(lblTeaserSpread);
          invisibleControls.Add(lblTeaserTotalsOver);
          invisibleControls.Add(lblTeaserTotalsUnder);
          invisibleControls.Add(lblTeaserTeamTotalsOver);
          invisibleControls.Add(lblTeaserTeamTotalsUnder);
          break;
        case "Parlay":
          AcceptButton = btnAddToParlay;
          visibleControls.Add(btnAddToParlay);
          invisibleControls.Add(panWagerAmounts);
          invisibleControls.Add(btnPlaceBet);
          invisibleControls.Add(btnAddToTeaser);
          invisibleControls.Add(btnAddToIfBet);
          invisibleControls.Add(lblTeaserSpread);
          invisibleControls.Add(lblTeaserTotalsOver);
          invisibleControls.Add(lblTeaserTotalsUnder);
          invisibleControls.Add(lblTeaserTeamTotalsOver);
          invisibleControls.Add(lblTeaserTeamTotalsUnder);
          if (!LoginsAndProfiles.ValidateUserFunctionalityAccess(Mdi.CurrentUserPermissions, LoginsAndProfiles.ACCEPT_ANY_WAGER)) {
            invisibleControls.Add(panTeamTotals);
          }

          break;
        case "Teaser":
          AcceptButton = btnAddToTeaser;
          visibleControls.Add(btnAddToTeaser);
          visibleControls.Add(lblTeaserSpread);
          invisibleControls.Add(btnPlaceBet);
          invisibleControls.Add(btnAddToParlay);
          invisibleControls.Add(btnAddToIfBet);
          invisibleControls.Add(panWagerAmounts);
          invisibleControls.Add(lblSpread);
          invisibleControls.Add(lblTotalsOver);
          invisibleControls.Add(lblTotalsUnder);
          invisibleControls.Add(lblTeamTotalsOver);
          invisibleControls.Add(lblTeamTotalsUnder);

          if (IsWagerTypeAvailable(WagerType.SPREAD)) {
            visibleControls.Add(panSpread);
          }
          else {
            invisibleControls.Add(panSpread);
          }

          invisibleControls.Add(panSpreadBuyPoints);

          if (IsWagerTypeAvailable(WagerType.TOTALPOINTS)) {
            visibleControls.Add(panTotalPoints);
          }
          else {
            invisibleControls.Add(panTotalPoints);
          }

          invisibleControls.Add(panTotalsBuyPoints);

          if (IsWagerTypeAvailable(WagerType.MONEYLINE)) {
            visibleControls.Add(panMoneyLine);
          }
          else {
            invisibleControls.Add(panMoneyLine);
          }

          if (IsWagerTypeAvailable(WagerType.TEAMTOTALPOINTS)) {
            visibleControls.Add(panTeamTotals);
          }
          else {
            invisibleControls.Add(panTeamTotals);
          }
          if (!LoginsAndProfiles.ValidateUserFunctionalityAccess(Mdi.CurrentUserPermissions, LoginsAndProfiles.ACCEPT_ANY_WAGER)) {
            invisibleControls.Add(panTeamTotals);
          }
          break;
        case "If-Bet":
          AcceptButton = btnAddToIfBet;
          visibleControls.Add(btnAddToIfBet);
          invisibleControls.Add(btnPlaceBet);
          invisibleControls.Add(btnAddToParlay);
          invisibleControls.Add(btnAddToTeaser);
          invisibleControls.Add(lblTeaserSpread);
          invisibleControls.Add(lblTeaserTotalsOver);
          invisibleControls.Add(lblTeaserTotalsUnder);
          invisibleControls.Add(lblTeaserTeamTotalsOver);
          invisibleControls.Add(lblTeaserTeamTotalsUnder);
          if (!LoginsAndProfiles.ValidateUserFunctionalityAccess(Mdi.CurrentUserPermissions, LoginsAndProfiles.ACCEPT_ANY_WAGER)) {
            invisibleControls.Add(panTeamTotals);
          }
          break;
      }

      foreach (var control in visibleControls) {
        control.Visible = true;
      }
      foreach (var control in invisibleControls) {
        control.Visible = false;
      }
    }

    private void SetRiskAmount(string senderText) {
      txtRisk.TextChanged -= txtRisk_TextChanged;
      txtRisk.AllowCents = true;
      txtRisk.Text = GetRiskAmount(senderText).ToString(CultureInfo.InvariantCulture);
      txtRisk.TextChanged += txtRisk_TextChanged;

      if (WagerTypeMode == "Risk" &&
          (txtRisk.Text == @"0" || txtRisk.Text == @"0.00")) {
        txtRisk.Text = "";
      }
    }

    private void SetToWinAmount(string senderText) {
      txtToWin.TextChanged -= txtToWin_TextChanged;
      txtToWin.AllowCents = true;
      txtToWin.Text = GetToWinAmount(senderText).ToString(CultureInfo.InvariantCulture);
      txtToWin.TextChanged += txtToWin_TextChanged;

      if (WagerTypeMode == "ToWin" &&
          (txtToWin.Text == @"0" || txtToWin.Text == @"0.00")) {
        txtToWin.Text = "";
      }
    }

    private bool ShowBuyPointsOption() {
      if (SelectedGamePeriodInfo == null) return false;
      var gamePreventPointsBuying = SelectedGamePeriodInfo.PreventPointBuyingFlag ?? "N";
      var sportType = SelectedGamePeriodInfo.SportType.ToString(CultureInfo.InvariantCulture).Trim();

      return (sportType == SportTypes.BASKETBALL || sportType == SportTypes.FOOTBALL) && gamePreventPointsBuying != "Y" && _selectedGamePeriod == 0;
    }

    private void ShowHideFormObjects() {
      var showBuyPointsPanel = ShowBuyPointsOption();
      if (_selectedRowNumberInGroup == 3) {
        radSpread.Visible = false;
        lblSpread.Visible = false;
        panSpreadBuyPoints.Visible = false;
        radTotalsOver.Visible = false;
        lblTotalsOver.Visible = false;
        radTotalsUnder.Visible = false;
        lblTotalsUnder.Visible = false;
        panTotalsBuyPoints.Visible = false;
        radTeamTotalsOver.Visible = false;
        lblTeamTotalsOver.Visible = false;
        radTeamTotalsUnder.Visible = false;
        lblTeamTotalsUnder.Visible = false;

      }
      else {
        radSpread.Visible = true;
        lblSpread.Visible = true;
        panSpreadBuyPoints.Visible = true;
        radTotalsOver.Visible = true;
        lblTotalsOver.Visible = true;
        radTotalsUnder.Visible = true;
        lblTotalsUnder.Visible = true;
        panTotalsBuyPoints.Visible = true;
        radTeamTotalsOver.Visible = true;
        lblTeamTotalsOver.Visible = true;
        radTeamTotalsUnder.Visible = true;
        lblTeamTotalsUnder.Visible = true;

        if (lblSpread.Text.Trim().Length == 0) {
          radSpread.Visible = false;
          panSpreadBuyPoints.Visible = false;
        }
        else {
          panSpreadBuyPoints.Visible = showBuyPointsPanel;
        }

        radTotalsOver.Visible = lblTotalsOver.Text.Trim().Length != 0;

        if (lblTotalsUnder.Text.Trim().Length == 0) {
          radTotalsUnder.Visible = false;
          panTotalsBuyPoints.Visible = false;
        }
        else {
          panTotalsBuyPoints.Visible = showBuyPointsPanel;
        }

        radMoneyLine.Visible = lblMoneyLine.Text.Trim().Length != 0;

        radTeamTotalsOver.Visible = lblTeamTotalsOver.Text.Trim().Length != 0;

        radTeamTotalsUnder.Visible = lblTeamTotalsUnder.Text.Trim().Length != 0;
      }

      panPitcher1.Visible = false;
      panPitcher2.Visible = false;
      panFixedOdds.Visible = false;
      if (_selectedSportType == SportTypes.BASEBALL) {
        if (SelectedGamePeriodInfo != null && SelectedGamePeriodInfo.ListedPitcher1 != null) {
          chbPitcher1MustStart.Text = chbPitcher1MustStart.Text.Replace("Who", SelectedGamePeriodInfo.ListedPitcher1.Trim());
          panPitcher1.Visible = true;
        }
        if (SelectedGamePeriodInfo != null && SelectedGamePeriodInfo.ListedPitcher2 != null) {
          chbPitcher2MustStart.Text = chbPitcher2MustStart.Text.Replace("Who", SelectedGamePeriodInfo.ListedPitcher2.Trim());
          panPitcher2.Visible = true;
        }

        if (SelectedGamePeriodInfo != null && SelectedGamePeriodInfo.ListedPitcher1 != null && SelectedGamePeriodInfo.ListedPitcher2 != null) {
          panFixedOdds.Visible = true;
        }

        if (FrmMode == "Edit") {
          switch (_currentCustomerBaseballAction) {
            case "Listed":
              chbPitcher1MustStart.Checked = true;
              chbPitcher2MustStart.Checked = true;
              if (_selectedGamePeriod == 1) {
                chbPitcher1MustStart.Enabled = false;
                chbPitcher2MustStart.Enabled = false;
              }
              else {
                chbPitcher1MustStart.Enabled = GetCheckedWagerTypeCode() == WagerType.MONEYLINE;
                chbPitcher2MustStart.Enabled = GetCheckedWagerTypeCode() == WagerType.MONEYLINE;
              }
              break;
            case "Action":
              TogglePitcherCheckBoxes();
              break;
            case "Fixed":
              chbPitcher1MustStart.Checked = false;
              chbPitcher1MustStart.Enabled = false;
              chbPitcher2MustStart.Checked = false;
              chbPitcher2MustStart.Enabled = false;
              break;
          }

          chbFixedOdds.Checked = PreviouslySelectedFixedPrice == "Y";
          chbPitcher1MustStart.Checked = PreviouslySelectedPitcher1 == "Y";
          chbPitcher2MustStart.Checked = PreviouslySelectedPitcher2 == "Y";
        }
        else {
          chbFixedOdds.Enabled = _selectedGamePeriod == 0;
          switch (_currentCustomerBaseballAction) {
            case "Listed":
              chbPitcher1MustStart.Checked = true;
              chbPitcher2MustStart.Checked = true;
              chbFixedOdds.Checked = false;
              chbFixedOdds.Enabled = _selectedGamePeriod != 1;
              if (_selectedGamePeriod == 1) {
                chbPitcher1MustStart.Enabled = false;
                chbPitcher2MustStart.Enabled = false;
              }
              else {
                chbPitcher1MustStart.Enabled = GetCheckedWagerTypeCode() == WagerType.MONEYLINE;
                chbPitcher2MustStart.Enabled = GetCheckedWagerTypeCode() == WagerType.MONEYLINE;
              }
              break;
            case "Action":
              TogglePitcherCheckBoxes();
              chbFixedOdds.Checked = false;
              chbFixedOdds.Enabled = _selectedGamePeriod != 1;
              break;
            case "Fixed":
              chbPitcher1MustStart.Checked = false;
              chbPitcher1MustStart.Enabled = false;
              chbPitcher2MustStart.Checked = false;
              chbPitcher2MustStart.Enabled = false;
              chbFixedOdds.Checked = true;
              chbFixedOdds.Enabled = _selectedGamePeriod != 1;
              break;
          }
        }

        if (_easternLineEnabled == "Y") {
          radMoneyLine.Text = @"Eastern Line";
        }
      }
      var res = (from go in _selectedGameOptions where go.GameNum == _selectedGameNumber && go.PeriodNumber == _selectedGamePeriod select go).FirstOrDefault();
      if (!ParentFrm.RestrictMoneyLines.WagerTypeNotRestricted(res)) {
        panMoneyLine.Visible = false;
      }
    }

    private void TogglePitcherCheckBoxes() {
      chbPitcher1MustStart.Checked = GetCheckedWagerTypeCode() != WagerType.MONEYLINE;
      chbPitcher2MustStart.Checked = GetCheckedWagerTypeCode() != WagerType.MONEYLINE;
      panFixedOdds.Enabled = true;

      if (_selectedGamePeriod == 1) {
        chbPitcher1MustStart.Checked = true;
        chbPitcher2MustStart.Checked = true;
        chbPitcher1MustStart.Enabled = false;
        chbPitcher2MustStart.Enabled = false;
        panFixedOdds.Enabled = false;
      }
      else {
        chbPitcher1MustStart.Enabled = GetCheckedWagerTypeCode() == WagerType.MONEYLINE;
        chbPitcher2MustStart.Enabled = GetCheckedWagerTypeCode() == WagerType.MONEYLINE;
      }
    }

    private void UncheckTheOtherRadioButtons(String radNameToExclude) {
      foreach (var rad in from radName in _radNames where radName != radNameToExclude select (RadioButton)Controls.Find(radName, true).FirstOrDefault() into rad where rad != null select rad) {
        rad.Checked = false;
      }
    }

    private void EnableEditWagerTextBoxes(String wagerType, string ou)
    {
        panSpreadTxtLineAdj.Enabled = panSpreadTxtPrice.Enabled = panTotalsOverTxtLineAdj.Enabled = panTotalsOverTxtPrice.Enabled = panTotalsUnderTxtLineAdj.Enabled =
        panTotalsUnderTxtPrice.Enabled = panMoneyLineTxtPrice.Enabled = panTeamTotalsOverTxtLineAdj.Enabled = panTeamTotalsOverTxtPrice.Enabled = 
        panTeamTotalsUnderTxtLineAdj.Enabled = panTeamTotalsUnderTxtPrice.Enabled = false;

        switch (wagerType)
        {
            case WagerType.SPREADNAME:
                panSpreadTxtLineAdj.Enabled = panSpreadTxtPrice.Enabled = true;

                if (SpreadPriceControl != null)
                    SpreadPriceControl.Visible = CurrentWagerTypeName != WagerType.TEASERNAME;

                break;
            case WagerType.TOTALPOINTSNAME:
                if (ou == "O")
                {
                    panTotalsOverTxtLineAdj.Enabled = panTotalsOverTxtPrice.Enabled = true;
                    if (TotalPointsOverLinePriceControl != null)
                        TotalPointsOverLinePriceControl.Visible = CurrentWagerTypeName != WagerType.TEASERNAME;
                }
                else
                {
                    panTotalsUnderTxtLineAdj.Enabled = panTotalsUnderTxtPrice.Enabled = true;
                    if (TotalPointsUnderLinePriceControl != null)
                        TotalPointsUnderLinePriceControl.Visible = CurrentWagerTypeName != WagerType.TEASERNAME;
                }
                break;
            case WagerType.MONEYLINENAME:
                panMoneyLineTxtPrice.Enabled = true;
                if (MoneyLinePriceControl != null)
                    MoneyLinePriceControl.Visible = CurrentWagerTypeName != WagerType.TEASERNAME;
                break;
            case WagerType.TEAMTOTALPOINTSOVERNAME:
                panTeamTotalsOverTxtLineAdj.Enabled = panTeamTotalsOverTxtPrice.Enabled = true;
                if (TeamTotalsOverLinePriceControl != null)
                    TeamTotalsOverLinePriceControl.Visible = CurrentWagerTypeName != WagerType.TEASERNAME;
                break;
            case WagerType.TEAMTOTALPOINTSUNDERNAME:
                panTeamTotalsUnderTxtLineAdj.Enabled = panTeamTotalsUnderTxtPrice.Enabled = true;
                if (TeamTotalsUnderLinePriceControl != null)
                    TeamTotalsUnderLinePriceControl.Visible = CurrentWagerTypeName != WagerType.TEASERNAME;
                break;

        }

    }

    private void UpdatePreviouslySelectedOdds(RadioButton radioButton) {
      if (radioButton == null || String.IsNullOrEmpty(radioButton.AccessibleDescription)) return;
      var wagerTypeAry = radioButton.AccessibleDescription.Split('_');

      switch (wagerTypeAry[0]) {
        case WagerType.SPREAD:
          dudBuyPointsSpread.Text = PreviouslySelectedBPtsOpt;
          dudBuyPointsSpread.SelectedItem = PreviouslySelectedBPtsOpt;
          FinalAmericanSpreadAdj = PreviouslySelectedAmericanPrice;
          break;
        case WagerType.TOTALPOINTS:
          dudBuyPointsTotalPoints.Text = PreviouslySelectedBPtsOpt;
          dudBuyPointsTotalPoints.SelectedItem = PreviouslySelectedBPtsOpt;
          if (PreviouslySelectedOUflag == "U") {
            FinalAmericanTotalsUAdj = PreviouslySelectedAmericanPrice;
          }
          else {
            FinalAmericanTotalsOAdj = PreviouslySelectedAmericanPrice;
          }
          break;

        case WagerType.MONEYLINE:
        case WagerType.TEAMTOTALPOINTS:
          break;
      }

      var labelToFind = radioButton.Name.Replace("rad", CurrentWagerTypeName == "Teaser" ? "lblTeaser" : "lbl");

      var lblToFind = (Label)Controls.Find(labelToFind, true).FirstOrDefault();

      if (lblToFind != null) {
        lblToFind.Text = PreviouslySelectedOdds;
      }
    }

    private void ValidateWagerItem() {
      if (CurrentWagerTypeName != "Teaser") {
        _itemWagerType = GetSelectedWagerOption("wagerType");
        _totalsOu = GetSelectedWagerOption("");
        _item = WagerType.GetFromCode(_itemWagerType);

        var chosenTeamId = "";
        foreach (var ctrl in from object ctrl in grpTeams.Controls
                             where ctrl.GetType().ToString() == "System.Windows.Forms.RadioButton"
                             where ((RadioButton)ctrl).Checked
                             select ctrl) {
          chosenTeamId = ((RadioButton)ctrl).Text;
          break;
        }

        _chosenTeamRotNum = TwUtilities.GetChosenTeamRotNumber(SelectedGamePeriodInfo, chosenTeamId);
        _chosenTeamId = GetChosenTeamId();
        _chosenTeamIdDesc = TwUtilities.GetChosenTeamIdDescription(SelectedGamePeriodInfo, _itemWagerType, _chosenTeamId);

        _chosenLine = GetWagerTypeLine();

        _chosenAmericanPrice = GetWagerTypeAdjustment().ToString(CultureInfo.InvariantCulture);

        _wagerItemDescription = FrmSportAndGameSelection.BuildWagerDescription(_chosenTeamRotNum, _chosenLine, CurrentWagerTypeName == "Teaser" ? _teaserPoints.ToString(CultureInfo.InvariantCulture) : _chosenAmericanPrice, _itemWagerType, _totalsOu, _chosenTeamId, _selectedSportType, SelectedGamePeriodInfo, CurrentWagerTypeName, _selectedPrice, TwUtilities.GetOriginalLine(_itemWagerType, SelectedGamePeriodInfo, _chosenTeamId, _selectedRowNumberInGroup), GetAdjustedBpLine(_itemWagerType, _chosenLine), chbPitcher1MustStart.Checked ? "Y" : "N", chbPitcher2MustStart.Checked ? "Y" : "N", chbFixedOdds.Checked ? "Y" : "N");
      }

      if (IsValidWager(CurrentWagerTypeName)) {
        var gameStatus = SelectedGamePeriodInfo.Status;
        var periodWagerCutoff = SelectedGamePeriodInfo.PeriodWagerCutoff;
        if (periodWagerCutoff == null) return;
        var cutoffTime = (DateTime)periodWagerCutoff;
        var minWagerBypassed = true;
        var availableFundsOk = true;
        var wagerLimitOk = true;

        if (gameStatus != null) {
          switch (gameStatus) {
            case GamesAndLines.EVENT_COMPLETED: //Complete
            case GamesAndLines.EVENT_OFFLINE: //Offline
            case GamesAndLines.EVENT_CANCELLED: //Cancelled?
              MessageBox.Show(@"The game period has already started or is not currently available", @"Not available");
              txtToWin.Focus();
              return;
            case GamesAndLines.EVENT_CIRCLED: //Circled
            case GamesAndLines.EVENT_OPEN: //Open
              Boolean continueValidating;
              Boolean cutoffTimeBypassed;
              FrmSportAndGameSelection.CheckIfWagerCutoffHasBeenReached(this, LoginsAndProfiles.ACCEPT_ANY_WAGER, SelectedGamePeriodInfo, _wagerItemDescription, txtRisk.Text, txtToWin.Text, cutoffTime, out continueValidating, out cutoffTimeBypassed);
              if (!continueValidating)
                return;
              if (CurrentWagerTypeName == "Straight Bet" || CurrentWagerTypeName == "If-Bet") {
                FrmPlaceIfBet ifBetFrm = null;

                if (CurrentWagerTypeName == "If-Bet") {
                  ifBetFrm = (FrmPlaceIfBet)FormF.GetFormByName("FrmPlaceIfBet", this);
                }
                // skip minimun wager when action reverse or birdcage
                if (ifBetFrm == null || (ifBetFrm.ActionReverse == null && ifBetFrm.BirdCage == null)) {
                  FrmSportAndGameSelection.CheckIfBelowMinimumWager(this, LoginsAndProfiles.ACCEPT_ANY_WAGER, txtRisk.Text, out continueValidating, out minWagerBypassed);
                }
                if (!continueValidating) {
                  return;
                }
                String ifBetType = null; // winOrPush or winOnly
                int? currentIfBetItemNumber = null;
                DataGridView ifBetGrView = null;
                var ifBetComplexType = false;

                if (CurrentWagerTypeName == "If-Bet") {
                  if (ifBetFrm != null) {
                    ifBetGrView = (DataGridView)ifBetFrm.Controls.Find("dgvwIfBet", true).FirstOrDefault();
                    if (ifBetGrView != null) {
                      if (FrmMode == "Edit") {
                        currentIfBetItemNumber = WagerItemNumberToUpd;
                      }
                      else {
                        currentIfBetItemNumber = ifBetGrView.RowCount + 1;
                      }
                    }
                  }

                  if (ifBetFrm != null) {
                    var chb = (CheckBox)ifBetFrm.Controls.Find("chbContinueOnPush", true).FirstOrDefault();

                    if (chb != null) {
                      ifBetType = chb.Checked ? "winOrPush" : "winOnly";
                    }

                    chb = (CheckBox)ifBetFrm.Controls.Find("chbActionReverse", true).FirstOrDefault();

                    if (chb != null) {
                      if (chb.Checked) {
                        ifBetComplexType = true;
                      }
                    }

                    chb = (CheckBox)ifBetFrm.Controls.Find("chbARBC", true).FirstOrDefault();

                    if (chb != null) {
                      if (chb.Checked) {
                        ifBetComplexType = true;
                      }
                    }
                  }

                  if (ifBetFrm != null) {
                    var ifBetAmt = (TextBox)ifBetFrm.Controls.Find("txtAmount", true).FirstOrDefault();
                    if (ifBetAmt != null) {
                      _complexIfBetAmount = ifBetAmt.Text;
                    }
                  }
                }
                FrmSportAndGameSelection.CheckForAvailableFunds(this, FrmMode, TicketNumberToUpd, WagerNumberToUpd, WagerItemNumberToUpd, LoginsAndProfiles.ACCEPT_ANY_WAGER, SelectedGamePeriodInfo, null, _wagerItemDescription, txtRisk.Text, txtToWin.Text, IsFreePlay, out continueValidating, out availableFundsOk, ifBetType, currentIfBetItemNumber, ifBetGrView, ifBetComplexType); //Validate if customer has sufficient Funds for wager

                if (!continueValidating) {
                  return;
                }
                FrmSportAndGameSelection.CheckIfExceedsWagerLimit(this, FrmMode, TicketNumberToUpd, WagerNumberToUpd, WagerItemNumberToUpd, LoginsAndProfiles.ACCEPT_ANY_WAGER, SelectedGamePeriodInfo, _wagerItemDescription, txtRisk.Text, txtToWin.Text, _itemWagerType, _chosenTeamId, _totalsOu, out continueValidating, out wagerLimitOk, ifBetType, currentIfBetItemNumber, ifBetGrView, ifBetComplexType);
              }

              if (cutoffTimeBypassed && minWagerBypassed && availableFundsOk && wagerLimitOk) {
                GoToPlaceBet();
                FrmSportAndGameSelection.KeyPressBuffer = "";
                Close();
              }
              break;
          }

        }
      }
      else {
        txtToWin.Focus();
      }
    }

    private void SetCentsNumericInputs() {
      txtRisk.AllowCents = Params.IncludeCents;
      txtToWin.AllowCents = Params.IncludeCents;
    }

    private void RecalculateWagerAmounts()
    {
        var adj = GetWagerTypeAdjustment();
        WagerTypeMode = adj >= 0 ? "Risk" : "ToWin";
        if (WagerTypeMode == "Risk")
        {
            if (txtRisk.Text.Length > 0)
            {
                SetToWinAmount(txtRisk.Text);
                txtRisk.Focus();
            }
        }
        else
        {
            if (txtToWin.Text.Length > 0)
            {
                SetRiskAmount(txtToWin.Text);
                txtToWin.Focus();
            }
        }
    }

    #endregion

    #region Events

    private void frmMakeAWager_Load(object sender, EventArgs e) {
      if (FrmSportAndGameSelection.WageringDisabled) {
        MessageBox.Show(@"Wagering is not allowed for this account!");
        Close();
      }
      InitializeForm();
      RelocateForm();
      Tag = "Loading form in " + FrmMode + " mode";
    }

    private void btnAddToIfBet_Click(object sender, EventArgs e) {
      ValidateWagerItem();
    }

    private void btnAddToParlay_Click(object sender, EventArgs e) {
      ValidateWagerItem();
    }

    private void btnAddToTeaser_Click(object sender, EventArgs e) {
      ValidateWagerItem();
    }

    private void btnCancel_Click(object sender, EventArgs e) {
      Close();
    }

    private void btnPlaceBet_Click(object sender, EventArgs e) {
      ValidateWagerItem();
    }

    private void btnProps_Click(object sender, EventArgs e) {
      GoToProps();
    }

    private void chbFixedOdds_CheckedChanged(object sender, EventArgs e) {
      var wagerType = GetSelectedWagerOption("wagerType");
      var wasChecked = ((CheckBox)sender).Checked;

      if (wagerType == WagerType.MONEYLINE) {
        if (wasChecked) {
          chbPitcher1MustStart.Checked = false;
          chbPitcher1MustStart.Enabled = false;
          chbPitcher2MustStart.Checked = false;
          chbPitcher2MustStart.Enabled = false;
        }
        else {
          chbPitcher1MustStart.Checked = _currentCustomerBaseballAction == "Listed";
          chbPitcher1MustStart.Enabled = true;
          chbPitcher2MustStart.Checked = _currentCustomerBaseballAction == "Listed";
          chbPitcher2MustStart.Enabled = true;
        }
      }
      else {
        if (wasChecked) {
          chbPitcher1MustStart.Checked = false;
          chbPitcher2MustStart.Checked = false;
        }
        else {
          chbPitcher1MustStart.Checked = true;
          chbPitcher2MustStart.Checked = true;
        }
        chbPitcher1MustStart.Enabled = wagerType == WagerType.MONEYLINE;
        chbPitcher2MustStart.Enabled = wagerType == WagerType.MONEYLINE;
      }
    }

    private void cmbGoToGamePeriod_SelectedIndexChanged(object sender, EventArgs e) {
      var cBx = (ComboBox)sender;
      var periodName = cBx.Text.Trim();

      var periodNumber = _sportPeriods == null ? (from go in _selectedGameOptions where go.GameNum == _selectedGameNumber && go.PeriodDescription.Trim() == periodName select go.PeriodNumber).FirstOrDefault() : FrmSportAndGameSelection.GetPeriodNumber(periodName, _sportPeriods);

      txtPeriod.Text = periodName;
      _selectedGamePeriod = periodNumber;
      BuildWagerSpecsDescription(_selectedRowNumberInGroup);

      CheckProperWagerOption(_selectedWagerType);
      CheckSelectedTeam(_selectedRowNumberInGroup);

      TwUtilities.GetAvailableGamePeriodsForDropDown(((ComboBox)sender), _selectedGameOptions, _selectedGameNumber, txtPeriod.Text);
      ShowHideFormObjects();
      SetLayout();

      _buyPointsOptions = null;
    }

    private void dudBuyPointsSpread_SelectedItemChanged(object sender, EventArgs e) {
      txtRisk.TextChanged -= txtRisk_TextChanged;
      txtToWin.TextChanged -= txtToWin_TextChanged;
      HandleSpreadPointsBuying(sender);
      txtRisk.TextChanged += txtRisk_TextChanged;
      txtToWin.TextChanged += txtToWin_TextChanged;
    }

    private void dudBuyPointsTotalPoints_SelectedItemChanged(object sender, EventArgs e) {

      txtRisk.TextChanged -= txtRisk_TextChanged;
      txtToWin.TextChanged -= txtToWin_TextChanged;
      HandleTotalPointsPointsBuying(sender);
      txtRisk.TextChanged += txtRisk_TextChanged;
      txtToWin.TextChanged += txtToWin_TextChanged;
    }

    private void FrmMakeAWager_FormClosing(object sender, FormClosingEventArgs e) {
      SelectedGamePeriodInfo = null;
    }

    private void frmMakeAWager_KeyDown(object sender, KeyEventArgs e) {
      if (e.KeyCode.ToString() == Keys.Escape.ToString()) {
        Close();
      }

      if (e.KeyCode.ToString() == Keys.Enter.ToString()) {
        ValidateWagerItem();
      }
    }

    private void frmMakeAWager_Shown(object sender, EventArgs e) {
        var currentGameInfo = SelectedGamePeriodInfo;
        if (panWagerAmounts.Visible) {
        //var x = _selectedGameOptions[0].SpreadAdj1;
        //var j = _selectedRowNumberInGroup;

        int? price = 0;

        switch (_selectedWagerType) {
          case "Spread":
            price = _selectedRowNumberInGroup == 1 ? SelectedGamePeriodInfo.SpreadAdj1 : SelectedGamePeriodInfo.SpreadAdj2;
            if (FrmMode == "Edit" && Mdi.TicketNumber != TicketNumberToUpd && SpreadPriceControl != null && !String.IsNullOrEmpty(SpreadPriceControl.Text))
            {
                currentGameInfo.SpreadAdj1 = currentGameInfo.SpreadAdj2 = int.Parse(SpreadPriceControl.Text);
            }
            break;
          case "MoneyLine":
            switch (_selectedRowNumberInGroup) {
              case 1:
                price = SelectedGamePeriodInfo.MoneyLine1;
                break;
              case 2:
                price = SelectedGamePeriodInfo.MoneyLine2;
                break;
              case 3:
                price = SelectedGamePeriodInfo.MoneyLineDraw;
                break;
            }
            if (FrmMode == "Edit" && Mdi.TicketNumber != TicketNumberToUpd && MoneyLinePriceControl != null && !String.IsNullOrEmpty(MoneyLinePriceControl.Text))
            {
                currentGameInfo.MoneyLine1 = currentGameInfo.MoneyLine2 = currentGameInfo.MoneyLineDraw = int.Parse(MoneyLinePriceControl.Text);
            }
            break;
          case "Totals":
            price = _selectedRowNumberInGroup == 1 ? SelectedGamePeriodInfo.TtlPtsAdj1 : SelectedGamePeriodInfo.TtlPtsAdj2;
            if (FrmMode == "Edit" && Mdi.TicketNumber != TicketNumberToUpd)
            {
                if (_selectedRowNumberInGroup == 1 && TotalPointsOverLinePriceControl != null && !String.IsNullOrEmpty(TotalPointsOverLinePriceControl.Text))
                {
                    currentGameInfo.TtlPtsAdj1 = currentGameInfo.TtlPtsAdj2 = int.Parse(TotalPointsOverLinePriceControl.Text);
                }
                else
                {
                    if (TotalPointsUnderLinePriceControl != null && !String.IsNullOrEmpty(TotalPointsUnderLinePriceControl.Text))
                    {
                        currentGameInfo.TtlPtsAdj1 = currentGameInfo.TtlPtsAdj2 = int.Parse(TotalPointsUnderLinePriceControl.Text);
                    }
                }
            }
            break;
          case "TeamTotalsOver":
            price = _selectedRowNumberInGroup == 1 ? SelectedGamePeriodInfo.Team1TtlPtsAdj1 : SelectedGamePeriodInfo.Team2TtlPtsAdj1;
            if (FrmMode == "Edit" && Mdi.TicketNumber != TicketNumberToUpd)
            {
                if (_selectedRowNumberInGroup == 1 && TeamTotalsOverLinePriceControl != null && !String.IsNullOrEmpty(TeamTotalsOverLinePriceControl.Text))
                {
                    currentGameInfo.Team1TtlPtsAdj1 = currentGameInfo.Team1TtlPtsAdj2 = int.Parse(TeamTotalsOverLinePriceControl.Text);
                }
                else
                {
                    if (TeamTotalsUnderLinePriceControl != null && !String.IsNullOrEmpty(TeamTotalsUnderLinePriceControl.Text))
                    {
                        currentGameInfo.Team2TtlPtsAdj1 = currentGameInfo.Team2TtlPtsAdj2 = int.Parse(TeamTotalsOverLinePriceControl.Text);
                    }
                }
            }
            break;
          case "TeamTotalsUnder":
            price = _selectedRowNumberInGroup == 1 ? SelectedGamePeriodInfo.Team1TtlPtsAdj2 : SelectedGamePeriodInfo.Team2TtlPtsAdj2;
            if (FrmMode == "Edit" && Mdi.TicketNumber != TicketNumberToUpd)
            {
                if (_selectedRowNumberInGroup == 1 && TeamTotalsUnderLinePriceControl != null && !String.IsNullOrEmpty(TeamTotalsUnderLinePriceControl.Text))
                {
                    currentGameInfo.Team1TtlPtsAdj1 = currentGameInfo.Team1TtlPtsAdj2 = int.Parse(TeamTotalsUnderLinePriceControl.Text);
                }
                else
                {
                    if (TeamTotalsUnderLinePriceControl != null && !String.IsNullOrEmpty(TeamTotalsUnderLinePriceControl.Text))
                    {
                        currentGameInfo.Team2TtlPtsAdj1 = currentGameInfo.Team2TtlPtsAdj2 = int.Parse(TeamTotalsUnderLinePriceControl.Text);
                    }
                }
            }
            break;
        }

        if (price >= 0) {
          txtRisk.Focus();
          WagerTypeMode = "Risk";
        }
        else {
          txtToWin.Focus();
          WagerTypeMode = "ToWin";
        }
      }
      if (FrmSportAndGameSelection.ShowWagerLimitsInFrm.IsActive()) {
        FrmSportAndGameSelection.ShowWagerLimitsInFrm.FrmMakeAWager = this;
        FrmSportAndGameSelection.ShowWagerLimitsInFrm.DisplayMaxWagerLimits();
      }

      if (FrmSportAndGameSelection.ParlayOrTeaserItemFromSb) {
        if (CurrentWagerTypeName == "Teaser") {
          lblSpread.Visible = false;
          lblTeaserSpread.Visible = true;
          lblTeaserSpread.Text = lblSpread.Text.Split(' ')[0];

          lblTotalsOver.Visible = false;
          lblTeaserTotalsOver.Visible = true;
          lblTeaserTotalsOver.Text = lblTotalsOver.Text.Split(' ')[0];


          lblTotalsUnder.Visible = false;
          lblTeaserTotalsUnder.Visible = true;
          lblTeaserTotalsUnder.Text = lblTotalsUnder.Text.Split(' ')[0];

          lblTeamTotalsOver.Visible = false;
          lblTeaserTeamTotalsOver.Visible = true;
          lblTeaserTeamTotalsOver.Text = lblTeamTotalsOver.Text.Split(' ')[0];

          lblTeamTotalsUnder.Visible = false;
          lblTeaserTeamTotalsUnder.Visible = true;
          lblTeaserTeamTotalsUnder.Text = lblTeamTotalsUnder.Text.Split(' ')[0];
        }
      }

      if (CurrentWagerTypeName == "Teaser") {
        btnAddToTeaser.Focus();
      }
    }

    private void radDraw_CheckedChanged(object sender, EventArgs e) {
      if (((RadioButton)sender).Checked) {
        _selectedWagerType = WagerType.MONEYLINENAME;
        _selectedRowNumberInGroup = 3;
        BuildWagerSpecsDescription(_selectedRowNumberInGroup);
        ShowHideFormObjects();
        txtRisk.Text = "";
        txtToWin.Text = "";
        _buyPointsOptions = null;
        if (FrmSportAndGameSelection.ShowWagerLimitsInFrm.IsActive()) {
          FrmSportAndGameSelection.ShowWagerLimitsInFrm.FrmMakeAWager = this;
          FrmSportAndGameSelection.ShowWagerLimitsInFrm.DisplayMaxWagerLimits();
        }
      }
    }

    private void radMoneyLine_CheckedChanged(object sender, EventArgs e) {
      if (((RadioButton)sender).Checked) {
        _selectedWagerType = WagerType.MONEYLINENAME;
        UncheckTheOtherRadioButtons(((RadioButton)sender).Name);
        EnableEditWagerTextBoxes(_selectedWagerType, null);
        SetBuyingPointsOptions(WagerType.MONEYLINE, "");
        ShowHideFormObjects();
        chbPitcher1MustStart.Checked = true;
        chbPitcher2MustStart.Checked = true;
        RecalculateWagerAmounts();
        /*
          var adjustment = GetWagerTypeAdjustment();
        if (txtRisk.Text.Length > 0) {
          txtToWin.Text = LineOffering.DetermineToWinAmount(double.Parse(txtRisk.Text), adjustment, Params.IncludeCents).ToString(CultureInfo.InvariantCulture);
        }
        if (adjustment < 0)
          txtToWin.Focus();
        else
          txtRisk.Focus();*/
        if (FrmSportAndGameSelection.ShowWagerLimitsInFrm.IsActive()) {
          FrmSportAndGameSelection.ShowWagerLimitsInFrm.FrmMakeAWager = this;
          var currentGameInfo = SelectedGamePeriodInfo;
          if (FrmMode == "Edit" && Mdi.TicketNumber != TicketNumberToUpd && MoneyLinePriceControl != null && MoneyLinePriceControl.Text.Length > 0)
          {
              currentGameInfo.MoneyLineDraw = int.Parse(MoneyLinePriceControl.Text);
          }
          FrmSportAndGameSelection.ShowWagerLimitsInFrm.DisplayMaxWagerLimits();
        }
        _buyPointsOptions = null;
      }
      SetLayout();
    }

    private void radSpread_CheckedChanged(object sender, EventArgs e) {
      if (((RadioButton)sender).Checked) {
        _selectedWagerType = WagerType.SPREADNAME;
        UncheckTheOtherRadioButtons(((RadioButton)sender).Name);

        var txtLineAdjControl = panSpreadTxtLineAdj.Controls.Find("txtSpreadLineAdjControl", true).FirstOrDefault();
        if (txtLineAdjControl != null)
        {
            ((TxtLineAdjControl)txtLineAdjControl).SelectedWagerType = _selectedWagerType;
        }
        EnableEditWagerTextBoxes(_selectedWagerType, null);

        dudBuyPointsSpread.Items.Clear();
        dudBuyPointsSpread.Refresh();
        SetBuyingPointsOptions(WagerType.SPREAD, "");
        ShowHideFormObjects();
        chbPitcher1MustStart.Checked = true;
        chbPitcher2MustStart.Checked = true;
        dudBuyPointsSpread.Enabled = true;
        RecalculateWagerAmounts();
      }
      else {
        dudBuyPointsSpread.Enabled = false;
      }
      _buyPointsOptions = null;
      SetLayout();
      if (!FrmSportAndGameSelection.ShowWagerLimitsInFrm.IsActive()) return;
        FrmSportAndGameSelection.ShowWagerLimitsInFrm.FrmMakeAWager = this;
        var currentGameInfo = SelectedGamePeriodInfo;
        if (FrmMode == "Edit" && Mdi.TicketNumber != TicketNumberToUpd && SpreadPriceControl != null && SpreadPriceControl.Text.Length > 0)
        {
            currentGameInfo.SpreadAdj1 = currentGameInfo.SpreadAdj2 = int.Parse(SpreadPriceControl.Text);
        }
        FrmSportAndGameSelection.ShowWagerLimitsInFrm.DisplayMaxWagerLimits();
      }

    private void radTeam1_CheckedChanged(object sender, EventArgs e) {
      if (!((RadioButton)sender).Checked) return;
        DefineSelectedTeam(1);
        if (FrmSportAndGameSelection.ShowWagerLimitsInFrm.IsActive()) {
          FrmSportAndGameSelection.ShowWagerLimitsInFrm.FrmMakeAWager = this;
          FrmSportAndGameSelection.ShowWagerLimitsInFrm.DisplayMaxWagerLimits();
        }
        _buyPointsOptions = null;
      }

    private void radTeam2_CheckedChanged(object sender, EventArgs e) {
      if (((RadioButton)sender).Checked) {
        DefineSelectedTeam(2);
        if (FrmSportAndGameSelection.ShowWagerLimitsInFrm.IsActive()) {
          FrmSportAndGameSelection.ShowWagerLimitsInFrm.FrmMakeAWager = this;
          FrmSportAndGameSelection.ShowWagerLimitsInFrm.DisplayMaxWagerLimits();
        }
        _buyPointsOptions = null;
      }
    }

    private void radTeamTotalsOver_CheckedChanged(object sender, EventArgs e) {
      HandleTeamTotalsCheckChanged(sender);
    }

    private void radTeamTotalsUnder_CheckedChanged(object sender, EventArgs e) {
      HandleTeamTotalsCheckChanged(sender);
    }

    private void radTotalsOver_CheckedChanged(object sender, EventArgs e) {
      dudBuyPointsTotalPoints.Items.Clear();
      dudBuyPointsTotalPoints.Refresh();
      _buyPointsOptions = null;

      if (((RadioButton)sender).Checked) {
        _selectedWagerType = WagerType.TOTALPOINTSNAME;
        UncheckTheOtherRadioButtons(((RadioButton)sender).Name);

        var txtLineAdjControl = panTotalsOverTxtLineAdj.Controls.Find("txtTotalPointsOverLineLineAdj", true).FirstOrDefault();
        if (txtLineAdjControl != null)
        {
            ((TxtLineAdjControl)txtLineAdjControl).SelectedWagerType = _selectedWagerType;
        }
        EnableEditWagerTextBoxes(_selectedWagerType, "O");

        _selectedRowNumberInGroup = 1;
        SetBuyingPointsOptions(WagerType.TOTALPOINTS, "O");
        ShowHideFormObjects();
        chbPitcher1MustStart.Checked = true;
        chbPitcher2MustStart.Checked = true;
        dudBuyPointsTotalPoints.Enabled = true;
        RecalculateWagerAmounts();
        /*if (txtRisk.Text.Length > 0) {
          txtToWin.Text = LineOffering.DetermineToWinAmount(double.Parse(txtRisk.Text), GetWagerTypeAdjustment(), Params.IncludeCents).ToString(CultureInfo.InvariantCulture);
        }
        txtToWin.Focus();*/
      }
      else {
        dudBuyPointsTotalPoints.SelectedIndex = -1;
        dudBuyPointsTotalPoints.Enabled = false;

        var restoreTotalsSpec = lblTotalsOver.Text.Split(' ')[0] + "  ";

        if (_adjTotalOver > 0)
          restoreTotalsSpec += "+" + _adjTotalOver.ToString(CultureInfo.InvariantCulture);
        else
          restoreTotalsSpec += _adjTotalOver.ToString(CultureInfo.InvariantCulture);

        lblTotalsOver.Text = restoreTotalsSpec;
      }
      SetLayout();
      if (FrmSportAndGameSelection.ShowWagerLimitsInFrm.IsActive()) {
        FrmSportAndGameSelection.ShowWagerLimitsInFrm.FrmMakeAWager = this;
        var currentGameInfo = SelectedGamePeriodInfo;
        if (FrmMode == "Edit" && Mdi.TicketNumber != TicketNumberToUpd && TotalPointsOverLinePriceControl != null && !String.IsNullOrEmpty(TotalPointsOverLinePriceControl.Text))
        {
            currentGameInfo.TtlPtsAdj1 = int.Parse(TotalPointsOverLinePriceControl.Text);
        }
        FrmSportAndGameSelection.ShowWagerLimitsInFrm.DisplayMaxWagerLimits();
      }
    }

    private void radTotalsUnder_CheckedChanged(object sender, EventArgs e) {
      dudBuyPointsTotalPoints.Items.Clear();
      dudBuyPointsTotalPoints.Refresh();
      _buyPointsOptions = null;

      if (((RadioButton)sender).Checked) {
        _selectedWagerType = WagerType.TOTALPOINTSNAME;
        UncheckTheOtherRadioButtons(((RadioButton)sender).Name);

        var txtLineAdjControl = panTotalsUnderTxtLineAdj.Controls.Find("txtTotalPointsUnderLineLineAdj", true).FirstOrDefault();
        if (txtLineAdjControl != null)
        {
            ((TxtLineAdjControl)txtLineAdjControl).SelectedWagerType = _selectedWagerType;
        }
        EnableEditWagerTextBoxes(_selectedWagerType, "U");

        _selectedRowNumberInGroup = 2;
        SetBuyingPointsOptions(WagerType.TOTALPOINTS, "U");
        ShowHideFormObjects();
        chbPitcher1MustStart.Checked = true;
        chbPitcher2MustStart.Checked = true;
        dudBuyPointsTotalPoints.Enabled = true;
        RecalculateWagerAmounts();
        /*
          if (txtRisk.Text.Length > 0) {
          txtToWin.Text = LineOffering.DetermineToWinAmount(double.Parse(txtRisk.Text), GetWagerTypeAdjustment(), Params.IncludeCents).ToString(CultureInfo.InvariantCulture);
        }
        txtToWin.Focus();*/
      }
      else {
        dudBuyPointsTotalPoints.SelectedIndex = -1;
        dudBuyPointsTotalPoints.Enabled = false;

        var restoreTotalsSpec = lblTotalsUnder.Text.Split(' ')[0] + "  ";

        if (_adjTotalUnder > 0)
          restoreTotalsSpec += "+" + _adjTotalUnder.ToString(CultureInfo.InvariantCulture);
        else
          restoreTotalsSpec += _adjTotalUnder.ToString(CultureInfo.InvariantCulture);

        lblTotalsUnder.Text = restoreTotalsSpec;
      }
      SetLayout();
      if (FrmSportAndGameSelection.ShowWagerLimitsInFrm.IsActive()) {
        FrmSportAndGameSelection.ShowWagerLimitsInFrm.FrmMakeAWager = this;
        var currentGameInfo = SelectedGamePeriodInfo;
        if (FrmMode == "Edit" && Mdi.TicketNumber != TicketNumberToUpd && TotalPointsUnderLinePriceControl != null && !String.IsNullOrEmpty(TotalPointsUnderLinePriceControl.Text))
        {
            currentGameInfo.TtlPtsAdj2 = int.Parse(TotalPointsUnderLinePriceControl.Text);
        }
        FrmSportAndGameSelection.ShowWagerLimitsInFrm.DisplayMaxWagerLimits();
      }
    }

    private void txtRisk_KeyUp(object sender, KeyEventArgs e) {
      if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
        SetToWinAmount(((NumberTextBox)sender).Text);

    }

    private void txtToWin_KeyUp(object sender, KeyEventArgs e) {
      if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back)
        SetRiskAmount(((NumberTextBox)sender).Text);
    }

    private void txtRisk_KeyPress(object sender, KeyPressEventArgs e) {
      if (KeyValidator.IsValidNumericKey(e)) {
        if (((NumberTextBox)sender).SelectionLength > 0) {
          ((NumberTextBox)sender).Text = ((NumberTextBox)sender).Text.Replace(((NumberTextBox)sender).SelectedText, e.KeyChar.ToString(CultureInfo.InvariantCulture)).Replace("\b", "");
          ((NumberTextBox)sender).Select(((NumberTextBox)sender).Text.Length, 0);
          e.Handled = true;
        }
      }
      else {
        e.Handled = true;
      }
    }

    private void txtToWin_KeyPress(object sender, KeyPressEventArgs e) {
      if (KeyValidator.IsValidNumericKey(e)) {
        if (((NumberTextBox)sender).SelectionLength > 0) {
          ((NumberTextBox)sender).Text = ((NumberTextBox)sender).Text.Replace(((NumberTextBox)sender).SelectedText, e.KeyChar.ToString(CultureInfo.InvariantCulture)).Replace("\b", "");
          ((NumberTextBox)sender).Select(((NumberTextBox)sender).Text.Length, 0);
          e.Handled = true;
        }
      }
      else {
        e.Handled = true;
      }
    }

    private void txtRisk_Leave(object sender, EventArgs e) {
      SetToWinAmount(((NumberTextBox)sender).Text);
      _americanOdds = null;
    }

    private void txtToWin_Leave(object sender, EventArgs e) {
      SetRiskAmount(((NumberTextBox)sender).Text);
      _americanOdds = null;
    }

    private void txtRisk_TextChanged(object sender, EventArgs e) {
        var r = ((NumberTextBox)sender).Text.Replace(",", "");
      if (r == "") return;
      double textVal;
      double.TryParse(r, out textVal);
      if (!(textVal > -1)) return;
      SetToWinAmount(textVal.ToString(CultureInfo.InvariantCulture));
      ((NumberTextBox)sender).AllowCents = false;
      ((NumberTextBox)sender).TextChanged -= txtRisk_TextChanged;
      ((NumberTextBox)sender).Text = String.Format("{0:#,###,###}", double.Parse(r));
      ((NumberTextBox)sender).SelectionStart = ((NumberTextBox)sender).Text.Length;
      ((NumberTextBox)sender).SelectionLength = 0;
      ((NumberTextBox)sender).TextChanged += txtRisk_TextChanged;
      ((NumberTextBox)sender).AllowCents = true;
    }

    private void txtToWin_TextChanged(object sender, EventArgs e) {
        var w = ((NumberTextBox)sender).Text.Replace(",", "");
      if (w == "") return;
      double textVal;
      double.TryParse(w, out textVal);
      if (!(textVal > -1)) return;
      SetRiskAmount(textVal.ToString(CultureInfo.InvariantCulture));
      ((NumberTextBox)sender).AllowCents = false;
      ((NumberTextBox)sender).TextChanged -= txtToWin_TextChanged;
      ((NumberTextBox)sender).Text = String.Format("{0:#,###,###}", double.Parse(w));
      ((NumberTextBox)sender).SelectionStart = ((NumberTextBox)sender).Text.Length;
      ((NumberTextBox)sender).SelectionLength = 0;
      ((NumberTextBox)sender).TextChanged += txtToWin_TextChanged;
      ((NumberTextBox)sender).AllowCents = true;
    }

    private void txtPriceControl_ControlLeft(object sender, EventArgs e)
    {
        if (!FrmSportAndGameSelection.ShowWagerLimitsInFrm.IsActive()) return;
        FrmSportAndGameSelection.ShowWagerLimitsInFrm.FrmMakeAWager = this;
        FrmSportAndGameSelection.ShowWagerLimitsInFrm.DisplayMaxWagerLimits();
    }

    private spGLGetActiveGamesByCustomer_Result AdjustSelectedGamePrices(spGLGetActiveGamesByCustomer_Result selectedGamePeriodInfo)
    {
        var currentGameInfo = selectedGamePeriodInfo;

        switch (_selectedWagerType)
        {
            case WagerType.SPREADNAME:
                if (SpreadPriceControl != null && !String.IsNullOrEmpty(SpreadPriceControl.Text))
                    currentGameInfo.SpreadAdj1 = currentGameInfo.SpreadAdj2 = int.Parse(SpreadPriceControl.Text);
                break;
            case WagerType.TOTALPOINTSNAME:
                if (_selectedRowNumberInGroup == 1 && TotalPointsOverLinePriceControl != null && TotalPointsOverLinePriceControl.Text.Length > 0)
                {
                    currentGameInfo.TtlPtsAdj1 = currentGameInfo.TtlPtsAdj2 = int.Parse(TotalPointsOverLinePriceControl.Text);
                }
                else
                {
                    if (TotalPointsUnderLinePriceControl != null && TotalPointsUnderLinePriceControl.Text.Length > 0)
                        currentGameInfo.TtlPtsAdj1 = currentGameInfo.TtlPtsAdj2 = int.Parse(TotalPointsUnderLinePriceControl.Text);
                }
                break;
            case WagerType.MONEYLINENAME:
                if (MoneyLinePriceControl != null && MoneyLinePriceControl.Text.Length > 0)
                {
                    if (_selectedRowNumberInGroup == 1 || _selectedRowNumberInGroup == 2)
                        currentGameInfo.MoneyLine1 = currentGameInfo.MoneyLine1 = int.Parse(MoneyLinePriceControl.Text);
                    else
                        currentGameInfo.MoneyLineDraw = int.Parse(MoneyLinePriceControl.Text);
                }
                break;
            case WagerType.TEAMTOTALPOINTSOVERNAME:
                if (TeamTotalsOverLinePriceControl != null && TeamTotalsOverLinePriceControl.Text.Length > 0)
                {
                    if (_selectedRowNumberInGroup == 1)
                        currentGameInfo.Team1TtlPtsAdj1 = currentGameInfo.Team1TtlPtsAdj2 = int.Parse(TeamTotalsOverLinePriceControl.Text);
                    else
                        currentGameInfo.Team2TtlPtsAdj1 = currentGameInfo.Team2TtlPtsAdj2 = int.Parse(TeamTotalsOverLinePriceControl.Text);
                }
                break;
            case WagerType.TEAMTOTALPOINTSUNDERNAME:
                if (TeamTotalsUnderLinePriceControl != null && TeamTotalsUnderLinePriceControl.Text.Length > 0)
                {
                    if (_selectedRowNumberInGroup == 1)
                        currentGameInfo.Team1TtlPtsAdj1 = currentGameInfo.Team1TtlPtsAdj2 = int.Parse(TeamTotalsUnderLinePriceControl.Text);
                    else
                        currentGameInfo.Team2TtlPtsAdj1 = currentGameInfo.Team2TtlPtsAdj2 = int.Parse(TeamTotalsUnderLinePriceControl.Text);
                }
                break;
        }
        return currentGameInfo;
    }

    private void txtLineAdjControl_NewTextChanged(object sender, EventArgs e)
    {
        HandleAdjustmentValueChange(((TextBox)sender).Name.Replace("txt", ""), ((ControlTextChangedEventArgs)e).NewText);
    }

    private void txtPriceControl_NewTextChanged(object sender, EventArgs e)
    {
        HandlePriceValueChange(((TextBox)sender).Name.Replace("txt", ""), ((ControlTextChangedEventArgs)e).NewText);
    }

    #endregion

  }
}