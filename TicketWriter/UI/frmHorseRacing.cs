﻿using GUILibraries.Forms;
using GUILibraries.Utilities;
using InstanceManager.BusinessLayer;
using System;
using System.Configuration;
using System.Windows.Forms;
using Microsoft.Win32;

namespace TicketWriter.UI {
  public partial class FrmHorseRacing : SIDForm {
    readonly ModuleInfo _moduleInfo;
    readonly String _userPassword;
    readonly String _customerId;

    public FrmHorseRacing(ModuleInfo moduleInfo, String customerId, String userPassword)
      : base(moduleInfo) {
      _moduleInfo = moduleInfo;
      _customerId = customerId;
      _userPassword = userPassword;
      InitializeComponent();
    }

    private void frmHorseRacing_Load(object sender, EventArgs e) {
      //var appName = System.Diagnostics.Process.GetCurrentProcess().ProcessName + ".exe";
      //SetIE8KeyforWebBrowserControl(appName);
      HandleLoadEvent();
      //CheckIEVersion();
    }

    private void CheckIEVersion() {
      var ieVersion = "8";
      try {
        var openSubKey = Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\Internet Explorer");
        if (openSubKey != null) ieVersion = openSubKey.GetValue("svcVersion").ToString();
      }
      catch (Exception) {
        var openSubKey = Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\Internet Explorer");
        if (openSubKey != null) ieVersion = openSubKey.GetValue("Version").ToString();
      }
      var versionValueToSet = Convert.ToInt32(ieVersion.Split('.')[0]) * 1000;
    }

    private void HandleLoadEvent() {
      webHorseRacingBrowser.Navigate(ConfigurationManager.AppSettings["HorseRacingROOT_URL"] + "?userName=" + _moduleInfo.WindowsUserId + "&userPassword=" + _userPassword + "&player=" + _customerId + "&uhap=1");
    }

    private void ResetBetType() {
      var frm = FormF.GetFormByName("frmSportAndGameSelection", this);

      if (frm == null) {
        return;
      }

      var frmSports = (FrmSportAndGameSelection)frm;

      if (frmSports.SelectedWagerType != null)
        frmSports.SelectedWagerType = "Straight Bet";

      frm = FormF.GetFormByName("MdiTicketWriter", this);

      if (frm == null) {
        return;
      }

      var frmMdi = (MdiTicketWriter)frm;

      if (frmSports.SelectedWagerType != null)
        frmMdi.ToggleFpAndRifChecks(true);
    }

    private void frmHorseRacing_FormClosing(object sender, FormClosingEventArgs e) {
      ResetBetType();
    }

    private void SetIE8KeyforWebBrowserControl(string appName) {
      RegistryKey regkey = null;
      try {
        // For 64 bit machine
        regkey = Registry.LocalMachine.OpenSubKey(Environment.Is64BitOperatingSystem ? @"SOFTWARE\\Wow6432Node\\Microsoft\\Internet Explorer\\MAIN\\FeatureControl\\FEATURE_BROWSER_EMULATION" : @"SOFTWARE\\Microsoft\\Internet Explorer\\Main\\FeatureControl\\FEATURE_BROWSER_EMULATION", true);

        // If the path is not correct or
        // if the user haven't priviledges to access the registry
        if (regkey == null) {
          MessageBox.Show(@"Application Settings Failed - Address Not found");
          return;
        }

        var findAppkey = Convert.ToString(regkey.GetValue(appName));

        // Check if key is already present
        if (findAppkey == "8000") {
          MessageBox.Show(@"Required Application Settings Present");
          regkey.Close();
          return;
        }

        // If a key is not present add the key, Key value 8000 (decimal)
        //if (string.IsNullOrEmpty(FindAppkey))
        regkey.SetValue(appName, unchecked((int)0x2AF8), RegistryValueKind.DWord);

        // Check for the key after adding
        findAppkey = Convert.ToString(regkey.GetValue(appName));

        if (/*FindAppkey == "8000" || */findAppkey != "11000")
          MessageBox.Show(@"Application Settings Failed, Ref: " + findAppkey);
        //MessageBox.Show("Application Settings Applied Successfully");
        //else

      }
      catch (Exception ex) {
        MessageBox.Show(@"Application Settings Failed");
        MessageBox.Show(ex.Message);
      }
      finally {
        // Close the Registry
        if (regkey != null)
          regkey.Close();
      }
    }

  }
}
