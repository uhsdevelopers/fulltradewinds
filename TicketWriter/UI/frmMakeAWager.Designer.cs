﻿namespace TicketWriter.UI
{
    partial class FrmMakeAWager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPeriod = new System.Windows.Forms.TextBox();
            this.lblGoTo = new System.Windows.Forms.Label();
            this.cmbGoToGamePeriod = new System.Windows.Forms.ComboBox();
            this.btnProps = new System.Windows.Forms.Button();
            this.grpTeams = new System.Windows.Forms.GroupBox();
            this.panFixedOdds = new System.Windows.Forms.Panel();
            this.chbFixedOdds = new System.Windows.Forms.CheckBox();
            this.panPitcher2 = new System.Windows.Forms.Panel();
            this.chbPitcher2MustStart = new System.Windows.Forms.CheckBox();
            this.panPitcher1 = new System.Windows.Forms.Panel();
            this.chbPitcher1MustStart = new System.Windows.Forms.CheckBox();
            this.radDraw = new System.Windows.Forms.RadioButton();
            this.lblvs = new System.Windows.Forms.Label();
            this.radTeam2 = new System.Windows.Forms.RadioButton();
            this.radTeam1 = new System.Windows.Forms.RadioButton();
            this.grpRiskAmount = new System.Windows.Forms.GroupBox();
            this.txtRisk = new GUILibraries.Controls.NumberTextBox();
            this.grpWinAmount = new System.Windows.Forms.GroupBox();
            this.txtToWin = new GUILibraries.Controls.NumberTextBox();
            this.grpWagerTypes = new System.Windows.Forms.GroupBox();
            this.panTeamTotals = new System.Windows.Forms.Panel();
            this.lblTeaserTeamTotalsUnder = new System.Windows.Forms.Label();
            this.lblTeaserTeamTotalsOver = new System.Windows.Forms.Label();
            this.radTeamTotalsUnder = new System.Windows.Forms.RadioButton();
            this.radTeamTotalsOver = new System.Windows.Forms.RadioButton();
            this.lblTeamTotalsUnder = new System.Windows.Forms.Label();
            this.lblTeamTotalsOver = new System.Windows.Forms.Label();
            this.panMoneyLine = new System.Windows.Forms.Panel();
            this.radMoneyLine = new System.Windows.Forms.RadioButton();
            this.lblMoneyLine = new System.Windows.Forms.Label();
            this.panSpread = new System.Windows.Forms.Panel();
            this.lblTeaserSpread = new System.Windows.Forms.Label();
            this.panSpreadBuyPoints = new System.Windows.Forms.Panel();
            this.dudBuyPointsSpread = new System.Windows.Forms.DomainUpDown();
            this.lblBuyPtsSpreadSpec = new System.Windows.Forms.Label();
            this.lblBuyToSpread = new System.Windows.Forms.Label();
            this.lblSpread = new System.Windows.Forms.Label();
            this.radSpread = new System.Windows.Forms.RadioButton();
            this.panTotalPoints = new System.Windows.Forms.Panel();
            this.lblTeaserTotalsUnder = new System.Windows.Forms.Label();
            this.lblTeaserTotalsOver = new System.Windows.Forms.Label();
            this.panTotalsBuyPoints = new System.Windows.Forms.Panel();
            this.dudBuyPointsTotalPoints = new System.Windows.Forms.DomainUpDown();
            this.lblBuyPtsTotalsSpec = new System.Windows.Forms.Label();
            this.lblBuyToTotals = new System.Windows.Forms.Label();
            this.lblTotalsOver = new System.Windows.Forms.Label();
            this.lblTotalsUnder = new System.Windows.Forms.Label();
            this.radTotalsOver = new System.Windows.Forms.RadioButton();
            this.radTotalsUnder = new System.Windows.Forms.RadioButton();
            this.btnPlaceBet = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnAddToParlay = new System.Windows.Forms.Button();
            this.panWagerAmounts = new System.Windows.Forms.Panel();
            this.btnAddToTeaser = new System.Windows.Forms.Button();
            this.btnAddToIfBet = new System.Windows.Forms.Button();
            this.lblMaxRiskDisplay = new System.Windows.Forms.Label();
            this.lblMaxWinDisplay = new System.Windows.Forms.Label();
            this.panSpreadTxtPrice = new System.Windows.Forms.Panel();
            this.panSpreadTxtLineAdj = new System.Windows.Forms.Panel();
            this.panTotalsOverTxtPrice = new System.Windows.Forms.Panel();
            this.panTotalsOverTxtLineAdj = new System.Windows.Forms.Panel();
            this.panTotalsUnderTxtPrice = new System.Windows.Forms.Panel();
            this.panTotalsUnderTxtLineAdj = new System.Windows.Forms.Panel();
            this.panMoneyLineTxtPrice = new System.Windows.Forms.Panel();
            this.panTeamTotalsUnderTxtPrice = new System.Windows.Forms.Panel();
            this.panTeamTotalsOverTxtPrice = new System.Windows.Forms.Panel();
            this.panTeamTotalsUnderTxtLineAdj = new System.Windows.Forms.Panel();
            this.panTeamTotalsOverTxtLineAdj = new System.Windows.Forms.Panel();
            this.grpTeams.SuspendLayout();
            this.panFixedOdds.SuspendLayout();
            this.panPitcher2.SuspendLayout();
            this.panPitcher1.SuspendLayout();
            this.grpRiskAmount.SuspendLayout();
            this.grpWinAmount.SuspendLayout();
            this.grpWagerTypes.SuspendLayout();
            this.panTeamTotals.SuspendLayout();
            this.panMoneyLine.SuspendLayout();
            this.panSpread.SuspendLayout();
            this.panSpreadBuyPoints.SuspendLayout();
            this.panTotalPoints.SuspendLayout();
            this.panTotalsBuyPoints.SuspendLayout();
            this.panWagerAmounts.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtPeriod
            // 
            this.txtPeriod.Enabled = false;
            this.txtPeriod.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPeriod.Location = new System.Drawing.Point(13, 11);
            this.txtPeriod.Name = "txtPeriod";
            this.txtPeriod.ReadOnly = true;
            this.txtPeriod.Size = new System.Drawing.Size(112, 20);
            this.txtPeriod.TabIndex = 1000;
            this.txtPeriod.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblGoTo
            // 
            this.lblGoTo.AutoSize = true;
            this.lblGoTo.Location = new System.Drawing.Point(131, 13);
            this.lblGoTo.Name = "lblGoTo";
            this.lblGoTo.Size = new System.Drawing.Size(36, 13);
            this.lblGoTo.TabIndex = 1000;
            this.lblGoTo.Text = "Go to:";
            // 
            // cmbGoToGamePeriod
            // 
            this.cmbGoToGamePeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGoToGamePeriod.FormattingEnabled = true;
            this.cmbGoToGamePeriod.Location = new System.Drawing.Point(172, 11);
            this.cmbGoToGamePeriod.Name = "cmbGoToGamePeriod";
            this.cmbGoToGamePeriod.Size = new System.Drawing.Size(93, 21);
            this.cmbGoToGamePeriod.TabIndex = 10;
            this.cmbGoToGamePeriod.SelectedIndexChanged += new System.EventHandler(this.cmbGoToGamePeriod_SelectedIndexChanged);
            // 
            // btnProps
            // 
            this.btnProps.Location = new System.Drawing.Point(284, 10);
            this.btnProps.Name = "btnProps";
            this.btnProps.Size = new System.Drawing.Size(46, 23);
            this.btnProps.TabIndex = 20;
            this.btnProps.Text = "Props";
            this.btnProps.UseVisualStyleBackColor = true;
            this.btnProps.Click += new System.EventHandler(this.btnProps_Click);
            // 
            // grpTeams
            // 
            this.grpTeams.Controls.Add(this.panFixedOdds);
            this.grpTeams.Controls.Add(this.panPitcher2);
            this.grpTeams.Controls.Add(this.panPitcher1);
            this.grpTeams.Controls.Add(this.radDraw);
            this.grpTeams.Controls.Add(this.lblvs);
            this.grpTeams.Controls.Add(this.radTeam2);
            this.grpTeams.Controls.Add(this.radTeam1);
            this.grpTeams.Location = new System.Drawing.Point(13, 39);
            this.grpTeams.Name = "grpTeams";
            this.grpTeams.Size = new System.Drawing.Size(317, 211);
            this.grpTeams.TabIndex = 1000;
            this.grpTeams.TabStop = false;
            // 
            // panFixedOdds
            // 
            this.panFixedOdds.Controls.Add(this.chbFixedOdds);
            this.panFixedOdds.Location = new System.Drawing.Point(156, 143);
            this.panFixedOdds.Name = "panFixedOdds";
            this.panFixedOdds.Size = new System.Drawing.Size(152, 27);
            this.panFixedOdds.TabIndex = 1000;
            // 
            // chbFixedOdds
            // 
            this.chbFixedOdds.AutoSize = true;
            this.chbFixedOdds.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chbFixedOdds.Location = new System.Drawing.Point(65, 5);
            this.chbFixedOdds.Name = "chbFixedOdds";
            this.chbFixedOdds.Size = new System.Drawing.Size(82, 17);
            this.chbFixedOdds.TabIndex = 70;
            this.chbFixedOdds.Text = "Fixed Odds:";
            this.chbFixedOdds.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chbFixedOdds.UseVisualStyleBackColor = true;
            this.chbFixedOdds.CheckedChanged += new System.EventHandler(this.chbFixedOdds_CheckedChanged);
            // 
            // panPitcher2
            // 
            this.panPitcher2.Controls.Add(this.chbPitcher2MustStart);
            this.panPitcher2.Location = new System.Drawing.Point(83, 109);
            this.panPitcher2.Name = "panPitcher2";
            this.panPitcher2.Size = new System.Drawing.Size(225, 27);
            this.panPitcher2.TabIndex = 1000;
            // 
            // chbPitcher2MustStart
            // 
            this.chbPitcher2MustStart.AutoSize = true;
            this.chbPitcher2MustStart.Checked = true;
            this.chbPitcher2MustStart.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbPitcher2MustStart.Location = new System.Drawing.Point(15, 7);
            this.chbPitcher2MustStart.Name = "chbPitcher2MustStart";
            this.chbPitcher2MustStart.Size = new System.Drawing.Size(100, 17);
            this.chbPitcher2MustStart.TabIndex = 60;
            this.chbPitcher2MustStart.Text = "Who Must Start";
            this.chbPitcher2MustStart.UseVisualStyleBackColor = true;
            // 
            // panPitcher1
            // 
            this.panPitcher1.Controls.Add(this.chbPitcher1MustStart);
            this.panPitcher1.Location = new System.Drawing.Point(86, 35);
            this.panPitcher1.Name = "panPitcher1";
            this.panPitcher1.Size = new System.Drawing.Size(225, 27);
            this.panPitcher1.TabIndex = 1000;
            // 
            // chbPitcher1MustStart
            // 
            this.chbPitcher1MustStart.AutoSize = true;
            this.chbPitcher1MustStart.Checked = true;
            this.chbPitcher1MustStart.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbPitcher1MustStart.Location = new System.Drawing.Point(15, 7);
            this.chbPitcher1MustStart.Name = "chbPitcher1MustStart";
            this.chbPitcher1MustStart.Size = new System.Drawing.Size(100, 17);
            this.chbPitcher1MustStart.TabIndex = 40;
            this.chbPitcher1MustStart.Text = "Who Must Start";
            this.chbPitcher1MustStart.UseVisualStyleBackColor = true;
            // 
            // radDraw
            // 
            this.radDraw.Appearance = System.Windows.Forms.Appearance.Button;
            this.radDraw.AutoSize = true;
            this.radDraw.Location = new System.Drawing.Point(83, 179);
            this.radDraw.Name = "radDraw";
            this.radDraw.Size = new System.Drawing.Size(147, 23);
            this.radDraw.TabIndex = 80;
            this.radDraw.TabStop = true;
            this.radDraw.Text = "biggest Team3 ID Available";
            this.radDraw.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radDraw.UseVisualStyleBackColor = true;
            this.radDraw.CheckedChanged += new System.EventHandler(this.radDraw_CheckedChanged);
            // 
            // lblvs
            // 
            this.lblvs.AutoSize = true;
            this.lblvs.Location = new System.Drawing.Point(141, 57);
            this.lblvs.Name = "lblvs";
            this.lblvs.Size = new System.Drawing.Size(21, 13);
            this.lblvs.TabIndex = 1000;
            this.lblvs.Text = "vs.";
            // 
            // radTeam2
            // 
            this.radTeam2.Appearance = System.Windows.Forms.Appearance.Button;
            this.radTeam2.AutoSize = true;
            this.radTeam2.Location = new System.Drawing.Point(83, 87);
            this.radTeam2.MinimumSize = new System.Drawing.Size(147, 0);
            this.radTeam2.Name = "radTeam2";
            this.radTeam2.Size = new System.Drawing.Size(147, 23);
            this.radTeam2.TabIndex = 50;
            this.radTeam2.TabStop = true;
            this.radTeam2.Text = "biggest Team2 ID Available";
            this.radTeam2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radTeam2.UseVisualStyleBackColor = true;
            this.radTeam2.CheckedChanged += new System.EventHandler(this.radTeam2_CheckedChanged);
            // 
            // radTeam1
            // 
            this.radTeam1.Appearance = System.Windows.Forms.Appearance.Button;
            this.radTeam1.AutoSize = true;
            this.radTeam1.Location = new System.Drawing.Point(83, 13);
            this.radTeam1.MinimumSize = new System.Drawing.Size(147, 0);
            this.radTeam1.Name = "radTeam1";
            this.radTeam1.Size = new System.Drawing.Size(147, 23);
            this.radTeam1.TabIndex = 30;
            this.radTeam1.TabStop = true;
            this.radTeam1.Text = "biggest Team1 ID Available";
            this.radTeam1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radTeam1.UseVisualStyleBackColor = true;
            this.radTeam1.CheckedChanged += new System.EventHandler(this.radTeam1_CheckedChanged);
            // 
            // grpRiskAmount
            // 
            this.grpRiskAmount.Controls.Add(this.txtRisk);
            this.grpRiskAmount.Location = new System.Drawing.Point(1, 3);
            this.grpRiskAmount.Name = "grpRiskAmount";
            this.grpRiskAmount.Size = new System.Drawing.Size(130, 46);
            this.grpRiskAmount.TabIndex = 1000;
            this.grpRiskAmount.TabStop = false;
            this.grpRiskAmount.Text = "Risk Amount";
            // 
            // txtRisk
            // 
            this.txtRisk.AllowCents = false;
            this.txtRisk.AllowNegatives = false;
            this.txtRisk.DividedBy = 1;
            this.txtRisk.DivideFlag = false;
            this.txtRisk.Location = new System.Drawing.Point(7, 20);
            this.txtRisk.MaxLength = 11;
            this.txtRisk.Name = "txtRisk";
            this.txtRisk.ShowIfZero = true;
            this.txtRisk.Size = new System.Drawing.Size(100, 20);
            this.txtRisk.TabIndex = 90;
            this.txtRisk.TextChanged += new System.EventHandler(this.txtRisk_TextChanged);
            this.txtRisk.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRisk_KeyPress);
            this.txtRisk.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtRisk_KeyUp);
            this.txtRisk.Leave += new System.EventHandler(this.txtRisk_Leave);
            // 
            // grpWinAmount
            // 
            this.grpWinAmount.Controls.Add(this.txtToWin);
            this.grpWinAmount.Location = new System.Drawing.Point(174, 3);
            this.grpWinAmount.Name = "grpWinAmount";
            this.grpWinAmount.Size = new System.Drawing.Size(142, 46);
            this.grpWinAmount.TabIndex = 1000;
            this.grpWinAmount.TabStop = false;
            this.grpWinAmount.Text = "To Win Amount";
            // 
            // txtToWin
            // 
            this.txtToWin.AllowCents = false;
            this.txtToWin.AllowNegatives = false;
            this.txtToWin.DividedBy = 1;
            this.txtToWin.DivideFlag = false;
            this.txtToWin.Location = new System.Drawing.Point(13, 19);
            this.txtToWin.MaxLength = 11;
            this.txtToWin.Name = "txtToWin";
            this.txtToWin.ShowIfZero = true;
            this.txtToWin.Size = new System.Drawing.Size(100, 20);
            this.txtToWin.TabIndex = 100;
            this.txtToWin.TextChanged += new System.EventHandler(this.txtToWin_TextChanged);
            this.txtToWin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtToWin_KeyPress);
            this.txtToWin.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtToWin_KeyUp);
            this.txtToWin.Leave += new System.EventHandler(this.txtToWin_Leave);
            // 
            // grpWagerTypes
            // 
            this.grpWagerTypes.Controls.Add(this.panSpreadTxtLineAdj);
            this.grpWagerTypes.Controls.Add(this.panTeamTotals);
            this.grpWagerTypes.Controls.Add(this.panMoneyLine);
            this.grpWagerTypes.Controls.Add(this.panSpread);
            this.grpWagerTypes.Controls.Add(this.panTotalPoints);
            this.grpWagerTypes.Location = new System.Drawing.Point(336, 5);
            this.grpWagerTypes.Name = "grpWagerTypes";
            this.grpWagerTypes.Size = new System.Drawing.Size(354, 354);
            this.grpWagerTypes.TabIndex = 1000;
            this.grpWagerTypes.TabStop = false;
            // 
            // panTeamTotals
            // 
            this.panTeamTotals.Controls.Add(this.panTeamTotalsUnderTxtPrice);
            this.panTeamTotals.Controls.Add(this.panTeamTotalsOverTxtPrice);
            this.panTeamTotals.Controls.Add(this.panTeamTotalsUnderTxtLineAdj);
            this.panTeamTotals.Controls.Add(this.panTeamTotalsOverTxtLineAdj);
            this.panTeamTotals.Controls.Add(this.lblTeaserTeamTotalsUnder);
            this.panTeamTotals.Controls.Add(this.lblTeaserTeamTotalsOver);
            this.panTeamTotals.Controls.Add(this.radTeamTotalsUnder);
            this.panTeamTotals.Controls.Add(this.radTeamTotalsOver);
            this.panTeamTotals.Controls.Add(this.lblTeamTotalsUnder);
            this.panTeamTotals.Controls.Add(this.lblTeamTotalsOver);
            this.panTeamTotals.Location = new System.Drawing.Point(3, 272);
            this.panTeamTotals.Name = "panTeamTotals";
            this.panTeamTotals.Size = new System.Drawing.Size(339, 70);
            this.panTeamTotals.TabIndex = 1000;
            // 
            // lblTeaserTeamTotalsUnder
            // 
            this.lblTeaserTeamTotalsUnder.AutoSize = true;
            this.lblTeaserTeamTotalsUnder.Location = new System.Drawing.Point(129, 46);
            this.lblTeaserTeamTotalsUnder.Name = "lblTeaserTeamTotalsUnder";
            this.lblTeaserTeamTotalsUnder.Size = new System.Drawing.Size(46, 13);
            this.lblTeaserTeamTotalsUnder.TabIndex = 1000;
            this.lblTeaserTeamTotalsUnder.Text = "45  -110";
            // 
            // lblTeaserTeamTotalsOver
            // 
            this.lblTeaserTeamTotalsOver.AutoSize = true;
            this.lblTeaserTeamTotalsOver.Location = new System.Drawing.Point(129, 12);
            this.lblTeaserTeamTotalsOver.Name = "lblTeaserTeamTotalsOver";
            this.lblTeaserTeamTotalsOver.Size = new System.Drawing.Size(46, 13);
            this.lblTeaserTeamTotalsOver.TabIndex = 1000;
            this.lblTeaserTeamTotalsOver.Text = "45  -115";
            // 
            // radTeamTotalsUnder
            // 
            this.radTeamTotalsUnder.AccessibleDescription = "E_U";
            this.radTeamTotalsUnder.AutoSize = true;
            this.radTeamTotalsUnder.Location = new System.Drawing.Point(5, 44);
            this.radTeamTotalsUnder.Name = "radTeamTotalsUnder";
            this.radTeamTotalsUnder.Size = new System.Drawing.Size(126, 17);
            this.radTeamTotalsUnder.TabIndex = 180;
            this.radTeamTotalsUnder.TabStop = true;
            this.radTeamTotalsUnder.Text = "Team Points UNDER";
            this.radTeamTotalsUnder.UseVisualStyleBackColor = true;
            this.radTeamTotalsUnder.CheckedChanged += new System.EventHandler(this.radTeamTotalsUnder_CheckedChanged);
            // 
            // radTeamTotalsOver
            // 
            this.radTeamTotalsOver.AccessibleDescription = "E_O";
            this.radTeamTotalsOver.AutoSize = true;
            this.radTeamTotalsOver.Location = new System.Drawing.Point(5, 9);
            this.radTeamTotalsOver.Name = "radTeamTotalsOver";
            this.radTeamTotalsOver.Size = new System.Drawing.Size(117, 17);
            this.radTeamTotalsOver.TabIndex = 170;
            this.radTeamTotalsOver.TabStop = true;
            this.radTeamTotalsOver.Text = "Team Points OVER";
            this.radTeamTotalsOver.UseVisualStyleBackColor = true;
            this.radTeamTotalsOver.CheckedChanged += new System.EventHandler(this.radTeamTotalsOver_CheckedChanged);
            // 
            // lblTeamTotalsUnder
            // 
            this.lblTeamTotalsUnder.AutoSize = true;
            this.lblTeamTotalsUnder.Location = new System.Drawing.Point(129, 44);
            this.lblTeamTotalsUnder.Name = "lblTeamTotalsUnder";
            this.lblTeamTotalsUnder.Size = new System.Drawing.Size(46, 13);
            this.lblTeamTotalsUnder.TabIndex = 1000;
            this.lblTeamTotalsUnder.Text = "45  -110";
            // 
            // lblTeamTotalsOver
            // 
            this.lblTeamTotalsOver.AutoSize = true;
            this.lblTeamTotalsOver.Location = new System.Drawing.Point(129, 10);
            this.lblTeamTotalsOver.Name = "lblTeamTotalsOver";
            this.lblTeamTotalsOver.Size = new System.Drawing.Size(46, 13);
            this.lblTeamTotalsOver.TabIndex = 1000;
            this.lblTeamTotalsOver.Text = "45  -115";
            // 
            // panMoneyLine
            // 
            this.panMoneyLine.Controls.Add(this.panMoneyLineTxtPrice);
            this.panMoneyLine.Controls.Add(this.radMoneyLine);
            this.panMoneyLine.Controls.Add(this.lblMoneyLine);
            this.panMoneyLine.Location = new System.Drawing.Point(3, 221);
            this.panMoneyLine.Name = "panMoneyLine";
            this.panMoneyLine.Size = new System.Drawing.Size(339, 32);
            this.panMoneyLine.TabIndex = 1000;
            // 
            // radMoneyLine
            // 
            this.radMoneyLine.AccessibleDescription = "M_";
            this.radMoneyLine.AutoSize = true;
            this.radMoneyLine.Location = new System.Drawing.Point(5, 7);
            this.radMoneyLine.Name = "radMoneyLine";
            this.radMoneyLine.Size = new System.Drawing.Size(80, 17);
            this.radMoneyLine.TabIndex = 160;
            this.radMoneyLine.TabStop = true;
            this.radMoneyLine.Text = "Money Line";
            this.radMoneyLine.UseVisualStyleBackColor = true;
            this.radMoneyLine.CheckedChanged += new System.EventHandler(this.radMoneyLine_CheckedChanged);
            // 
            // lblMoneyLine
            // 
            this.lblMoneyLine.AutoSize = true;
            this.lblMoneyLine.Location = new System.Drawing.Point(85, 9);
            this.lblMoneyLine.Name = "lblMoneyLine";
            this.lblMoneyLine.Size = new System.Drawing.Size(31, 13);
            this.lblMoneyLine.TabIndex = 1000;
            this.lblMoneyLine.Text = "+130";
            // 
            // panSpread
            // 
            this.panSpread.Controls.Add(this.panSpreadTxtPrice);
            this.panSpread.Controls.Add(this.lblTeaserSpread);
            this.panSpread.Controls.Add(this.panSpreadBuyPoints);
            this.panSpread.Controls.Add(this.lblSpread);
            this.panSpread.Controls.Add(this.radSpread);
            this.panSpread.Location = new System.Drawing.Point(3, 10);
            this.panSpread.Name = "panSpread";
            this.panSpread.Size = new System.Drawing.Size(339, 94);
            this.panSpread.TabIndex = 1000;
            // 
            // lblTeaserSpread
            // 
            this.lblTeaserSpread.AutoSize = true;
            this.lblTeaserSpread.Location = new System.Drawing.Point(92, 7);
            this.lblTeaserSpread.Name = "lblTeaserSpread";
            this.lblTeaserSpread.Size = new System.Drawing.Size(46, 13);
            this.lblTeaserSpread.TabIndex = 1000;
            this.lblTeaserSpread.Text = "-3  +100";
            // 
            // panSpreadBuyPoints
            // 
            this.panSpreadBuyPoints.Controls.Add(this.dudBuyPointsSpread);
            this.panSpreadBuyPoints.Controls.Add(this.lblBuyPtsSpreadSpec);
            this.panSpreadBuyPoints.Controls.Add(this.lblBuyToSpread);
            this.panSpreadBuyPoints.Location = new System.Drawing.Point(3, 30);
            this.panSpreadBuyPoints.Name = "panSpreadBuyPoints";
            this.panSpreadBuyPoints.Size = new System.Drawing.Size(278, 63);
            this.panSpreadBuyPoints.TabIndex = 1000;
            // 
            // dudBuyPointsSpread
            // 
            this.dudBuyPointsSpread.Location = new System.Drawing.Point(65, 8);
            this.dudBuyPointsSpread.Name = "dudBuyPointsSpread";
            this.dudBuyPointsSpread.ReadOnly = true;
            this.dudBuyPointsSpread.Size = new System.Drawing.Size(59, 20);
            this.dudBuyPointsSpread.TabIndex = 120;
            this.dudBuyPointsSpread.SelectedItemChanged += new System.EventHandler(this.dudBuyPointsSpread_SelectedItemChanged);
            // 
            // lblBuyPtsSpreadSpec
            // 
            this.lblBuyPtsSpreadSpec.AutoSize = true;
            this.lblBuyPtsSpreadSpec.Location = new System.Drawing.Point(131, 10);
            this.lblBuyPtsSpreadSpec.Name = "lblBuyPtsSpreadSpec";
            this.lblBuyPtsSpreadSpec.Size = new System.Drawing.Size(105, 13);
            this.lblBuyPtsSpreadSpec.TabIndex = 1000;
            this.lblBuyPtsSpreadSpec.Text = "@ +## per half point";
            // 
            // lblBuyToSpread
            // 
            this.lblBuyToSpread.AutoSize = true;
            this.lblBuyToSpread.Location = new System.Drawing.Point(13, 10);
            this.lblBuyToSpread.Name = "lblBuyToSpread";
            this.lblBuyToSpread.Size = new System.Drawing.Size(44, 13);
            this.lblBuyToSpread.TabIndex = 1000;
            this.lblBuyToSpread.Text = "Buy To:";
            // 
            // lblSpread
            // 
            this.lblSpread.AutoSize = true;
            this.lblSpread.Location = new System.Drawing.Point(96, 7);
            this.lblSpread.Name = "lblSpread";
            this.lblSpread.Size = new System.Drawing.Size(46, 13);
            this.lblSpread.TabIndex = 1000;
            this.lblSpread.Text = "-3  +100";
            // 
            // radSpread
            // 
            this.radSpread.AccessibleDescription = "S_";
            this.radSpread.AutoSize = true;
            this.radSpread.Location = new System.Drawing.Point(4, 5);
            this.radSpread.Name = "radSpread";
            this.radSpread.Size = new System.Drawing.Size(86, 17);
            this.radSpread.TabIndex = 110;
            this.radSpread.TabStop = true;
            this.radSpread.Text = "Point Spread";
            this.radSpread.UseVisualStyleBackColor = true;
            this.radSpread.CheckedChanged += new System.EventHandler(this.radSpread_CheckedChanged);
            // 
            // panTotalPoints
            // 
            this.panTotalPoints.Controls.Add(this.panTotalsUnderTxtPrice);
            this.panTotalPoints.Controls.Add(this.panTotalsOverTxtPrice);
            this.panTotalPoints.Controls.Add(this.panTotalsUnderTxtLineAdj);
            this.panTotalPoints.Controls.Add(this.lblTeaserTotalsUnder);
            this.panTotalPoints.Controls.Add(this.panTotalsOverTxtLineAdj);
            this.panTotalPoints.Controls.Add(this.lblTeaserTotalsOver);
            this.panTotalPoints.Controls.Add(this.panTotalsBuyPoints);
            this.panTotalPoints.Controls.Add(this.lblTotalsOver);
            this.panTotalPoints.Controls.Add(this.lblTotalsUnder);
            this.panTotalPoints.Controls.Add(this.radTotalsOver);
            this.panTotalPoints.Controls.Add(this.radTotalsUnder);
            this.panTotalPoints.Location = new System.Drawing.Point(3, 106);
            this.panTotalPoints.Name = "panTotalPoints";
            this.panTotalPoints.Size = new System.Drawing.Size(339, 110);
            this.panTotalPoints.TabIndex = 1000;
            // 
            // lblTeaserTotalsUnder
            // 
            this.lblTeaserTotalsUnder.AutoSize = true;
            this.lblTeaserTotalsUnder.Location = new System.Drawing.Point(129, 42);
            this.lblTeaserTotalsUnder.Name = "lblTeaserTotalsUnder";
            this.lblTeaserTotalsUnder.Size = new System.Drawing.Size(52, 13);
            this.lblTeaserTotalsUnder.TabIndex = 1000;
            this.lblTeaserTotalsUnder.Text = "188  -110";
            // 
            // lblTeaserTotalsOver
            // 
            this.lblTeaserTotalsOver.AutoSize = true;
            this.lblTeaserTotalsOver.Location = new System.Drawing.Point(129, 8);
            this.lblTeaserTotalsOver.Name = "lblTeaserTotalsOver";
            this.lblTeaserTotalsOver.Size = new System.Drawing.Size(52, 13);
            this.lblTeaserTotalsOver.TabIndex = 1000;
            this.lblTeaserTotalsOver.Text = "188  -110";
            // 
            // panTotalsBuyPoints
            // 
            this.panTotalsBuyPoints.Controls.Add(this.dudBuyPointsTotalPoints);
            this.panTotalsBuyPoints.Controls.Add(this.lblBuyPtsTotalsSpec);
            this.panTotalsBuyPoints.Controls.Add(this.lblBuyToTotals);
            this.panTotalsBuyPoints.Location = new System.Drawing.Point(4, 70);
            this.panTotalsBuyPoints.Name = "panTotalsBuyPoints";
            this.panTotalsBuyPoints.Size = new System.Drawing.Size(259, 32);
            this.panTotalsBuyPoints.TabIndex = 1000;
            // 
            // dudBuyPointsTotalPoints
            // 
            this.dudBuyPointsTotalPoints.Location = new System.Drawing.Point(65, 6);
            this.dudBuyPointsTotalPoints.Name = "dudBuyPointsTotalPoints";
            this.dudBuyPointsTotalPoints.ReadOnly = true;
            this.dudBuyPointsTotalPoints.Size = new System.Drawing.Size(59, 20);
            this.dudBuyPointsTotalPoints.TabIndex = 150;
            this.dudBuyPointsTotalPoints.SelectedItemChanged += new System.EventHandler(this.dudBuyPointsTotalPoints_SelectedItemChanged);
            // 
            // lblBuyPtsTotalsSpec
            // 
            this.lblBuyPtsTotalsSpec.AutoSize = true;
            this.lblBuyPtsTotalsSpec.Location = new System.Drawing.Point(131, 10);
            this.lblBuyPtsTotalsSpec.Name = "lblBuyPtsTotalsSpec";
            this.lblBuyPtsTotalsSpec.Size = new System.Drawing.Size(105, 13);
            this.lblBuyPtsTotalsSpec.TabIndex = 1000;
            this.lblBuyPtsTotalsSpec.Text = "@ +## per half point";
            // 
            // lblBuyToTotals
            // 
            this.lblBuyToTotals.AutoSize = true;
            this.lblBuyToTotals.Location = new System.Drawing.Point(13, 10);
            this.lblBuyToTotals.Name = "lblBuyToTotals";
            this.lblBuyToTotals.Size = new System.Drawing.Size(44, 13);
            this.lblBuyToTotals.TabIndex = 1000;
            this.lblBuyToTotals.Text = "Buy To:";
            // 
            // lblTotalsOver
            // 
            this.lblTotalsOver.AutoSize = true;
            this.lblTotalsOver.Location = new System.Drawing.Point(129, 8);
            this.lblTotalsOver.Name = "lblTotalsOver";
            this.lblTotalsOver.Size = new System.Drawing.Size(52, 13);
            this.lblTotalsOver.TabIndex = 1000;
            this.lblTotalsOver.Text = "188  -110";
            // 
            // lblTotalsUnder
            // 
            this.lblTotalsUnder.AutoSize = true;
            this.lblTotalsUnder.Location = new System.Drawing.Point(129, 43);
            this.lblTotalsUnder.Name = "lblTotalsUnder";
            this.lblTotalsUnder.Size = new System.Drawing.Size(52, 13);
            this.lblTotalsUnder.TabIndex = 1000;
            this.lblTotalsUnder.Text = "188  -110";
            // 
            // radTotalsOver
            // 
            this.radTotalsOver.AccessibleDescription = "L_O";
            this.radTotalsOver.AutoSize = true;
            this.radTotalsOver.Location = new System.Drawing.Point(5, 7);
            this.radTotalsOver.Name = "radTotalsOver";
            this.radTotalsOver.Size = new System.Drawing.Size(114, 17);
            this.radTotalsOver.TabIndex = 130;
            this.radTotalsOver.TabStop = true;
            this.radTotalsOver.Text = "Total Points OVER";
            this.radTotalsOver.UseVisualStyleBackColor = true;
            this.radTotalsOver.CheckedChanged += new System.EventHandler(this.radTotalsOver_CheckedChanged);
            // 
            // radTotalsUnder
            // 
            this.radTotalsUnder.AccessibleDescription = "L_U";
            this.radTotalsUnder.AutoSize = true;
            this.radTotalsUnder.Location = new System.Drawing.Point(5, 39);
            this.radTotalsUnder.Name = "radTotalsUnder";
            this.radTotalsUnder.Size = new System.Drawing.Size(123, 17);
            this.radTotalsUnder.TabIndex = 140;
            this.radTotalsUnder.TabStop = true;
            this.radTotalsUnder.Text = "Total Points UNDER";
            this.radTotalsUnder.UseVisualStyleBackColor = true;
            this.radTotalsUnder.CheckedChanged += new System.EventHandler(this.radTotalsUnder_CheckedChanged);
            // 
            // btnPlaceBet
            // 
            this.btnPlaceBet.Location = new System.Drawing.Point(339, 365);
            this.btnPlaceBet.Name = "btnPlaceBet";
            this.btnPlaceBet.Size = new System.Drawing.Size(134, 23);
            this.btnPlaceBet.TabIndex = 190;
            this.btnPlaceBet.Text = "Place Bet";
            this.btnPlaceBet.UseVisualStyleBackColor = true;
            this.btnPlaceBet.Visible = false;
            this.btnPlaceBet.Click += new System.EventHandler(this.btnPlaceBet_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(486, 365);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(137, 23);
            this.btnCancel.TabIndex = 200;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnAddToParlay
            // 
            this.btnAddToParlay.Location = new System.Drawing.Point(337, 365);
            this.btnAddToParlay.Name = "btnAddToParlay";
            this.btnAddToParlay.Size = new System.Drawing.Size(134, 23);
            this.btnAddToParlay.TabIndex = 190;
            this.btnAddToParlay.Text = "Add to Parlay";
            this.btnAddToParlay.UseVisualStyleBackColor = true;
            this.btnAddToParlay.Visible = false;
            this.btnAddToParlay.Click += new System.EventHandler(this.btnAddToParlay_Click);
            // 
            // panWagerAmounts
            // 
            this.panWagerAmounts.Controls.Add(this.grpRiskAmount);
            this.panWagerAmounts.Controls.Add(this.grpWinAmount);
            this.panWagerAmounts.Location = new System.Drawing.Point(13, 257);
            this.panWagerAmounts.Name = "panWagerAmounts";
            this.panWagerAmounts.Size = new System.Drawing.Size(317, 60);
            this.panWagerAmounts.TabIndex = 1000;
            // 
            // btnAddToTeaser
            // 
            this.btnAddToTeaser.Location = new System.Drawing.Point(341, 365);
            this.btnAddToTeaser.Name = "btnAddToTeaser";
            this.btnAddToTeaser.Size = new System.Drawing.Size(134, 23);
            this.btnAddToTeaser.TabIndex = 190;
            this.btnAddToTeaser.Text = "Add to Teaser";
            this.btnAddToTeaser.UseVisualStyleBackColor = true;
            this.btnAddToTeaser.Visible = false;
            this.btnAddToTeaser.Click += new System.EventHandler(this.btnAddToTeaser_Click);
            // 
            // btnAddToIfBet
            // 
            this.btnAddToIfBet.Location = new System.Drawing.Point(339, 365);
            this.btnAddToIfBet.Name = "btnAddToIfBet";
            this.btnAddToIfBet.Size = new System.Drawing.Size(134, 23);
            this.btnAddToIfBet.TabIndex = 190;
            this.btnAddToIfBet.Text = "Add to If Bet";
            this.btnAddToIfBet.UseVisualStyleBackColor = true;
            this.btnAddToIfBet.Visible = false;
            this.btnAddToIfBet.Click += new System.EventHandler(this.btnAddToIfBet_Click);
            // 
            // lblMaxRiskDisplay
            // 
            this.lblMaxRiskDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaxRiskDisplay.Location = new System.Drawing.Point(21, 320);
            this.lblMaxRiskDisplay.Name = "lblMaxRiskDisplay";
            this.lblMaxRiskDisplay.Size = new System.Drawing.Size(120, 23);
            this.lblMaxRiskDisplay.TabIndex = 1001;
            // 
            // lblMaxWinDisplay
            // 
            this.lblMaxWinDisplay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaxWinDisplay.Location = new System.Drawing.Point(200, 320);
            this.lblMaxWinDisplay.Name = "lblMaxWinDisplay";
            this.lblMaxWinDisplay.Size = new System.Drawing.Size(120, 23);
            this.lblMaxWinDisplay.TabIndex = 1002;
            // 
            // panSpreadTxtPrice
            // 
            this.panSpreadTxtPrice.Location = new System.Drawing.Point(261, 3);
            this.panSpreadTxtPrice.Name = "panSpreadTxtPrice";
            this.panSpreadTxtPrice.Size = new System.Drawing.Size(68, 25);
            this.panSpreadTxtPrice.TabIndex = 1007;
            // 
            // panSpreadTxtLineAdj
            // 
            this.panSpreadTxtLineAdj.Location = new System.Drawing.Point(193, 13);
            this.panSpreadTxtLineAdj.Name = "panSpreadTxtLineAdj";
            this.panSpreadTxtLineAdj.Size = new System.Drawing.Size(50, 25);
            this.panSpreadTxtLineAdj.TabIndex = 1006;
            // 
            // panTotalsOverTxtPrice
            // 
            this.panTotalsOverTxtPrice.Location = new System.Drawing.Point(263, 2);
            this.panTotalsOverTxtPrice.Name = "panTotalsOverTxtPrice";
            this.panTotalsOverTxtPrice.Size = new System.Drawing.Size(68, 25);
            this.panTotalsOverTxtPrice.TabIndex = 1007;
            // 
            // panTotalsOverTxtLineAdj
            // 
            this.panTotalsOverTxtLineAdj.Location = new System.Drawing.Point(189, 2);
            this.panTotalsOverTxtLineAdj.Name = "panTotalsOverTxtLineAdj";
            this.panTotalsOverTxtLineAdj.Size = new System.Drawing.Size(50, 25);
            this.panTotalsOverTxtLineAdj.TabIndex = 1006;
            // 
            // panTotalsUnderTxtPrice
            // 
            this.panTotalsUnderTxtPrice.Location = new System.Drawing.Point(263, 37);
            this.panTotalsUnderTxtPrice.Name = "panTotalsUnderTxtPrice";
            this.panTotalsUnderTxtPrice.Size = new System.Drawing.Size(68, 25);
            this.panTotalsUnderTxtPrice.TabIndex = 1009;
            // 
            // panTotalsUnderTxtLineAdj
            // 
            this.panTotalsUnderTxtLineAdj.Location = new System.Drawing.Point(189, 37);
            this.panTotalsUnderTxtLineAdj.Name = "panTotalsUnderTxtLineAdj";
            this.panTotalsUnderTxtLineAdj.Size = new System.Drawing.Size(50, 25);
            this.panTotalsUnderTxtLineAdj.TabIndex = 1008;
            // 
            // panMoneyLineTxtPrice
            // 
            this.panMoneyLineTxtPrice.Location = new System.Drawing.Point(129, 4);
            this.panMoneyLineTxtPrice.Name = "panMoneyLineTxtPrice";
            this.panMoneyLineTxtPrice.Size = new System.Drawing.Size(68, 25);
            this.panMoneyLineTxtPrice.TabIndex = 1011;
            // 
            // panTeamTotalsUnderTxtPrice
            // 
            this.panTeamTotalsUnderTxtPrice.Location = new System.Drawing.Point(261, 39);
            this.panTeamTotalsUnderTxtPrice.Name = "panTeamTotalsUnderTxtPrice";
            this.panTeamTotalsUnderTxtPrice.Size = new System.Drawing.Size(68, 25);
            this.panTeamTotalsUnderTxtPrice.TabIndex = 1017;
            // 
            // panTeamTotalsOverTxtPrice
            // 
            this.panTeamTotalsOverTxtPrice.Location = new System.Drawing.Point(261, 5);
            this.panTeamTotalsOverTxtPrice.Name = "panTeamTotalsOverTxtPrice";
            this.panTeamTotalsOverTxtPrice.Size = new System.Drawing.Size(68, 25);
            this.panTeamTotalsOverTxtPrice.TabIndex = 1015;
            // 
            // panTeamTotalsUnderTxtLineAdj
            // 
            this.panTeamTotalsUnderTxtLineAdj.Location = new System.Drawing.Point(187, 39);
            this.panTeamTotalsUnderTxtLineAdj.Name = "panTeamTotalsUnderTxtLineAdj";
            this.panTeamTotalsUnderTxtLineAdj.Size = new System.Drawing.Size(50, 25);
            this.panTeamTotalsUnderTxtLineAdj.TabIndex = 1016;
            // 
            // panTeamTotalsOverTxtLineAdj
            // 
            this.panTeamTotalsOverTxtLineAdj.Location = new System.Drawing.Point(187, 5);
            this.panTeamTotalsOverTxtLineAdj.Name = "panTeamTotalsOverTxtLineAdj";
            this.panTeamTotalsOverTxtLineAdj.Size = new System.Drawing.Size(50, 25);
            this.panTeamTotalsOverTxtLineAdj.TabIndex = 1014;
            // 
            // FrmMakeAWager
            // 
            this.AccessibleDescription = "Make A Wager";
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(703, 399);
            this.Controls.Add(this.lblMaxWinDisplay);
            this.Controls.Add(this.lblMaxRiskDisplay);
            this.Controls.Add(this.panWagerAmounts);
            this.Controls.Add(this.btnAddToIfBet);
            this.Controls.Add(this.btnAddToTeaser);
            this.Controls.Add(this.btnAddToParlay);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnPlaceBet);
            this.Controls.Add(this.grpWagerTypes);
            this.Controls.Add(this.grpTeams);
            this.Controls.Add(this.btnProps);
            this.Controls.Add(this.cmbGoToGamePeriod);
            this.Controls.Add(this.lblGoTo);
            this.Controls.Add(this.txtPeriod);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmMakeAWager";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Make A Wager";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMakeAWager_FormClosing);
            this.Load += new System.EventHandler(this.frmMakeAWager_Load);
            this.Shown += new System.EventHandler(this.frmMakeAWager_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmMakeAWager_KeyDown);
            this.grpTeams.ResumeLayout(false);
            this.grpTeams.PerformLayout();
            this.panFixedOdds.ResumeLayout(false);
            this.panFixedOdds.PerformLayout();
            this.panPitcher2.ResumeLayout(false);
            this.panPitcher2.PerformLayout();
            this.panPitcher1.ResumeLayout(false);
            this.panPitcher1.PerformLayout();
            this.grpRiskAmount.ResumeLayout(false);
            this.grpRiskAmount.PerformLayout();
            this.grpWinAmount.ResumeLayout(false);
            this.grpWinAmount.PerformLayout();
            this.grpWagerTypes.ResumeLayout(false);
            this.panTeamTotals.ResumeLayout(false);
            this.panTeamTotals.PerformLayout();
            this.panMoneyLine.ResumeLayout(false);
            this.panMoneyLine.PerformLayout();
            this.panSpread.ResumeLayout(false);
            this.panSpread.PerformLayout();
            this.panSpreadBuyPoints.ResumeLayout(false);
            this.panSpreadBuyPoints.PerformLayout();
            this.panTotalPoints.ResumeLayout(false);
            this.panTotalPoints.PerformLayout();
            this.panTotalsBuyPoints.ResumeLayout(false);
            this.panTotalsBuyPoints.PerformLayout();
            this.panWagerAmounts.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPeriod;
        private System.Windows.Forms.Label lblGoTo;
        private System.Windows.Forms.ComboBox cmbGoToGamePeriod;
        private System.Windows.Forms.Button btnProps;
        private System.Windows.Forms.GroupBox grpTeams;
        private System.Windows.Forms.Label lblvs;
        private System.Windows.Forms.RadioButton radTeam2;
        private System.Windows.Forms.RadioButton radTeam1;
        private System.Windows.Forms.GroupBox grpRiskAmount;
        private GUILibraries.Controls.NumberTextBox txtRisk;
        private System.Windows.Forms.GroupBox grpWinAmount;
        private GUILibraries.Controls.NumberTextBox txtToWin;
        private System.Windows.Forms.GroupBox grpWagerTypes;
        private System.Windows.Forms.Label lblTeamTotalsUnder;
        private System.Windows.Forms.Label lblTeamTotalsOver;
        private System.Windows.Forms.Label lblMoneyLine;
        private System.Windows.Forms.Label lblTotalsUnder;
        private System.Windows.Forms.Label lblTotalsOver;
        private System.Windows.Forms.Panel panTotalsBuyPoints;
        private System.Windows.Forms.Label lblBuyPtsTotalsSpec;
        private System.Windows.Forms.Label lblBuyToTotals;
        private System.Windows.Forms.Label lblSpread;
        private System.Windows.Forms.Panel panSpreadBuyPoints;
        private System.Windows.Forms.Label lblBuyPtsSpreadSpec;
        private System.Windows.Forms.Label lblBuyToSpread;
        private System.Windows.Forms.RadioButton radTeamTotalsUnder;
        private System.Windows.Forms.RadioButton radMoneyLine;
        private System.Windows.Forms.RadioButton radTeamTotalsOver;
        private System.Windows.Forms.RadioButton radTotalsUnder;
        private System.Windows.Forms.RadioButton radTotalsOver;
        private System.Windows.Forms.RadioButton radSpread;
        private System.Windows.Forms.RadioButton radDraw;
        private System.Windows.Forms.DomainUpDown dudBuyPointsSpread;
        private System.Windows.Forms.DomainUpDown dudBuyPointsTotalPoints;
        private System.Windows.Forms.Button btnPlaceBet;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel panPitcher2;
        private System.Windows.Forms.CheckBox chbPitcher2MustStart;
        private System.Windows.Forms.Panel panPitcher1;
        private System.Windows.Forms.CheckBox chbPitcher1MustStart;
        private System.Windows.Forms.Panel panFixedOdds;
        private System.Windows.Forms.CheckBox chbFixedOdds;
        private System.Windows.Forms.Button btnAddToParlay;
        private System.Windows.Forms.Panel panWagerAmounts;
        private System.Windows.Forms.Panel panSpread;
        private System.Windows.Forms.Panel panTotalPoints;
        private System.Windows.Forms.Panel panMoneyLine;
        private System.Windows.Forms.Panel panTeamTotals;
        private System.Windows.Forms.Button btnAddToTeaser;
        private System.Windows.Forms.Button btnAddToIfBet;
        private System.Windows.Forms.Label lblTeaserTeamTotalsUnder;
        private System.Windows.Forms.Label lblTeaserTeamTotalsOver;
        private System.Windows.Forms.Label lblTeaserSpread;
        private System.Windows.Forms.Label lblTeaserTotalsUnder;
        private System.Windows.Forms.Label lblTeaserTotalsOver;
        private System.Windows.Forms.Label lblMaxRiskDisplay;
        private System.Windows.Forms.Label lblMaxWinDisplay;
        private System.Windows.Forms.Panel panSpreadTxtLineAdj;
        private System.Windows.Forms.Panel panSpreadTxtPrice;
        private System.Windows.Forms.Panel panMoneyLineTxtPrice;
        private System.Windows.Forms.Panel panTotalsUnderTxtPrice;
        private System.Windows.Forms.Panel panTotalsOverTxtPrice;
        private System.Windows.Forms.Panel panTotalsUnderTxtLineAdj;
        private System.Windows.Forms.Panel panTotalsOverTxtLineAdj;
        private System.Windows.Forms.Panel panTeamTotalsUnderTxtPrice;
        private System.Windows.Forms.Panel panTeamTotalsOverTxtPrice;
        private System.Windows.Forms.Panel panTeamTotalsUnderTxtLineAdj;
        private System.Windows.Forms.Panel panTeamTotalsOverTxtLineAdj;
    }
}
