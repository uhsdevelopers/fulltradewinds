﻿using System;
using System.Windows.Forms;
using GUILibraries.Forms;

namespace TicketWriter.UI {
  public sealed partial class FrmCustomerBalanceInformationTw : SIDForm {
    #region Public Properties

    /*private MdiTicketWriter Mdi {
      get {
        return _mdiTicketWriter ??
               (_mdiTicketWriter = (MdiTicketWriter)FormF.GetFormByName("MdiTicketWriter", this));
      }
    }*/

    #endregion

    #region Private Properties

    #endregion

    #region Public vars

    #endregion

    #region Private vars

    private double? _customerAvailableBalance;
    private readonly String _customerAccountType;
    private int? _customerPendingWagerCount;
    private readonly double? _customerPendingWagerBalance = 0;
    private double? _customerPendingOnReadback;
    private double? _customerFreePlayAvailable;
    private readonly int? _customerFreePlayPendingCount;
    private readonly double? _customerFreePlayPendingBalance;
    private double? _customerFreePlayOnReadback;
    private readonly String _customerCurrency;
    private readonly double? _customerCurrentBalance;
    private readonly double? _customerCreditLimit;
    private readonly double? _customerPendingCredit = 0;
    private readonly double? _customerNonPostedCasinoBalance;
    private readonly double? _customerFreePlayBalance;
    //private MdiTicketWriter _mdiTicketWriter;
    private readonly double? _todaysExchangeRate;

    #endregion

    #region Structures

    #endregion

    #region Constructors

    public FrmCustomerBalanceInformationTw(MdiTicketWriter parent)
      : base(parent.AppModuleInfo) {
      var balanceInfo = parent.BalanceInfo;
      if (balanceInfo != null) {
        _customerAccountType = balanceInfo.AccountType;
        _customerAvailableBalance = balanceInfo.AvailableBalance;
        _customerPendingWagerCount = balanceInfo.PendingWagerCount;
        _customerFreePlayAvailable = balanceInfo.FreePlayAvailable;
        _customerFreePlayPendingCount = balanceInfo.FreePlayPendingCount;
        _customerFreePlayPendingBalance = balanceInfo.FreePlayPendingBalance;
        _customerFreePlayOnReadback = balanceInfo.FreePlayRiskedOnReadback;
        _customerCurrency = balanceInfo.Currency;
        _customerCurrentBalance = balanceInfo.WeeklyLimitFlag == "Y" ? balanceInfo.ThisWeeksFigure : balanceInfo.CurrentBalance;
        _customerCreditLimit = balanceInfo.CreditLimit;
        _customerPendingWagerBalance = balanceInfo.PendingWagerBalance;
        _customerPendingOnReadback = balanceInfo.RiskingOnReadback;
        _customerPendingCredit = balanceInfo.PendingCredit;
        _customerNonPostedCasinoBalance = balanceInfo.CasinoNetAmount;
        _customerFreePlayBalance = balanceInfo.FreePlayBalance;
        _todaysExchangeRate = balanceInfo.TodaysExchangeRate;
      }
      InitializeComponent();

      if (balanceInfo == null) return;
      if (balanceInfo.WeeklyLimitFlag == "Y") {
        lblCurrentBalance.Text = @"Week To Date:";
      }
    }

    #endregion

    #region Public Methods

    #endregion

    #region Private Methods

    private void FillFormObjects() {
      try {
        txtBalanceInfoHeader.Text = _customerAccountType.ToUpper();
        txtBalanceInfoHeader.Text += Environment.NewLine;
        txtBalanceInfoHeader.Text += Environment.NewLine;
        txtBalanceInfoHeader.Text += @"Pending Count/Risk: " + _customerPendingWagerCount.ToString() + @" / " + FormatNumber(double.Parse(_customerPendingWagerBalance.ToString()), true, 100, true);
        txtBalanceInfoHeader.Text += Environment.NewLine;
        txtBalanceInfoHeader.Text += Environment.NewLine;
        txtBalanceInfoHeader.Text += @"Free Play Pending Count/Risk: " + _customerFreePlayPendingCount + @" / " + FormatNumber(double.Parse(_customerFreePlayPendingBalance.ToString()), true, 100, true);
        txtBalanceInfoHeader.Text += Environment.NewLine;
        txtBalanceInfoHeader.Text += Environment.NewLine;
        txtBalanceInfoHeader.Text += _customerCurrency;
        txtBalanceInfoHeader.Text += Environment.NewLine;
        txtBalanceInfoHeader.Text += Environment.NewLine;

        txtBalanceInfoHeader.Text += GetTodaysRate();

        lblCurrentBalanceFigure.Text = FormatNumber(double.Parse(_customerCurrentBalance.ToString()), true, 100, true);
        lblCreditLimitFigure.Text = FormatNumber(double.Parse(_customerCreditLimit.ToString()), true, 100, true);
        lblPendingCreditFigure.Text = FormatNumber(double.Parse(_customerPendingCredit.ToString()), true, 100, true);
        lblNonPostedCasinoBalanceFigure.Text = FormatNumber(double.Parse(_customerNonPostedCasinoBalance.ToString()), true, 100, true);

        if (_customerPendingOnReadback == null)
          _customerPendingOnReadback = 0;

        if (_customerPendingOnReadback == 0) {

          lblOnReadbackFigure.Text = FormatNumber(double.Parse(_customerPendingOnReadback.ToString()), false, 1, true);
        }
        else
          lblOnReadbackFigure.Text = @"-" + FormatNumber(double.Parse(_customerPendingOnReadback.ToString()), false, 1, true);

        if (_customerAccountType != null && _customerAccountType.ToUpper() != "POSTUP") {
          lblPostedPendingBets.Visible = true;
          lblPostedPendingBetsFigure.Visible = true;
          if (_customerPendingWagerBalance == 0)
            lblPostedPendingBetsFigure.Text = FormatNumber(double.Parse(_customerPendingWagerBalance.ToString()), true, 100, true);
          else
            lblPostedPendingBetsFigure.Text = @"-" + FormatNumber(double.Parse(_customerPendingWagerBalance.ToString()), true, 100, true);
        }
        else {
          lblPostedPendingBets.Visible = false;
          lblPostedPendingBetsFigure.Visible = false;
        }
        lblAvailableBalanceFigure.Text = FormatNumber(double.Parse(_customerAvailableBalance.ToString()), true, 100, true);

        lblFreePlayBalanceFigure.Text = FormatNumber(double.Parse(_customerFreePlayBalance.ToString()), true, 100, true);

        if (_customerFreePlayOnReadback == null) {
          _customerFreePlayOnReadback = 0;
        }

        if (_customerFreePlayOnReadback == 0) {
          lblFreePlayOnReadbackFigure.Text = FormatNumber(double.Parse(_customerFreePlayOnReadback.ToString()), false, 1, true);
        }
        else {
          lblFreePlayOnReadbackFigure.Text = @"-" + FormatNumber(double.Parse(_customerFreePlayOnReadback.ToString()), false, 1, true);
        }

        lblFPAvailableFigure.Text = FormatNumber(double.Parse(_customerFreePlayAvailable.ToString()), true, 100, true);
      }
      catch (Exception ex) {
        Log(ex);
      }
    }

    private string GetTodaysRate() {
      var todaysRate = "Today's Rate: " + (_todaysExchangeRate ?? 1.0);
      return todaysRate;
    }


    #endregion

    #region Events

    private void btnClose_Click(object sender, EventArgs e) {
      Close();
      Dispose();
    }

    private void frmCustomerBalanceInformation_Load(object sender, EventArgs e) {
      FillFormObjects();
    }

    #endregion

    private void FrmCustomerBalanceInformationTw_KeyDown(object sender, KeyEventArgs e) {
      if (e.KeyCode.ToString() == Keys.Escape.ToString()) {
        Close();
        Dispose();
      }
    }
  }
}