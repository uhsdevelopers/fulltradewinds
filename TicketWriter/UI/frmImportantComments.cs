﻿using System;
using GUILibraries.Forms;
using InstanceManager.BusinessLayer;

namespace TicketWriter.UI {
  public sealed partial class FrmImportantComments : SIDForm {

    #region Public Properties

    public string Comment { private get; set; }

    #endregion

    #region Private Properties

    #endregion

    #region Public vars

    #endregion

    #region Private vars

    #endregion

    #region Structures

    #endregion

    #region Constructors

    public FrmImportantComments(ModuleInfo moduleInfo)
      : base(moduleInfo) {
      InitializeComponent();
    }

    #endregion

    #region Public Methods

    #endregion

    #region Private Methods

    #endregion

    #region Events

    private void btnClose_Click(object sender, EventArgs e) {
      Close();
    }

    private void frmImportantComments_Load(object sender, EventArgs e) {
      txtMessage.Text = Comment;
    }

    #endregion

  }
}
