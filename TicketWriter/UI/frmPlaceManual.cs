﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using GUILibraries.Forms;
using GUILibraries.Utilities;
using InstanceManager.BusinessLayer;
using TicketWriter.Properties;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using GUILibraries.Controls;
using System.Linq;

namespace TicketWriter.UI {
  public sealed partial class FrmPlaceManual : SIDForm {
    #region Public Properties

    public ReadbackItem.ManualWagerItemEditInfo EditModeInfo { private get; set; }

    public String FrmMode { private get; set; }

    private FrmSportAndGameSelection FrmSportAndGameSelection {
      get {
        return _frmSportAndGameSelection ??
               (_frmSportAndGameSelection =
                (FrmSportAndGameSelection)FormF.GetFormByName("FrmSportAndGameSelection", this));
      }
    }

    private MdiTicketWriter Mdi {
      get {
        return _mdiTicketWriter ??
               (_mdiTicketWriter = (MdiTicketWriter)FormF.GetFormByName("MdiTicketWriter", this));
      }
    }

    public int RelativeXPos { private get; set; }

    public int RelativeYPos { private get; set; }

    private int TicketNumber { get; set; }

    private int WagerNumber { get; set; }

    public int WagerItemNumberToUpd { private get; set; }

    public int WagerNumberToUpd { private get; set; }

    public int TicketNumberToUpd { private get; set; }

    #endregion

    #region Private Properties

    private String ModuleName { get; set; }

    private List<spLOGetManualPlayTypes_Result> TypesList { get; set; }

    public int[] XToy;

    #endregion

    #region Public Vars

    #endregion

    #region Private Vars

    private bool _deleteWagersOnExit = true;
    private FrmSportAndGameSelection _frmSportAndGameSelection;
    private MdiTicketWriter _mdiTicketWriter;
    private readonly Boolean _layoffWagersAccepted;

    #endregion

    #region Structures

    #endregion

    #region Constructors

    public FrmPlaceManual(Boolean layoffWagersAccepted, ModuleInfo moduleInfo)
      : base(moduleInfo) {
      _layoffWagersAccepted = layoffWagersAccepted;
      InitializeComponent();
    }

    #endregion

    #region Public Methods

    public void AdjustFrmPosition() {
      var xw = RelativeXPos - Width;
      var yw = RelativeYPos - Height - 10;


      StartPosition = FormStartPosition.Manual;
      Location = new Point(xw, yw);
    }

    public static int[] BuildxToyObj(String inputValue) {
      int[] xToy = null;

      var enteredValue = inputValue;

      if (enteredValue != null && enteredValue.IndexOf("to", StringComparison.Ordinal) > -1) {
        enteredValue = enteredValue.Replace("to", "_");
        int xout;
        int yout;

        if (int.TryParse(enteredValue.Split('_')[0], out xout) && int.TryParse(enteredValue.Split('_')[1], out yout)) {

          xToy = new[] { xout, yout };
        }
      }

      return xToy;
    }

    #endregion

    #region Private Methods

    private void AddToWagerItemsList() {
      var rbItemLog = new Ticket.ManualWagerItem { TicketNumber = TicketNumber, WagerNumber = WagerNumber };

      if (FrmMode == "Edit") {
        rbItemLog.ItemNumber = WagerItemNumberToUpd;
      }
      else {
        var itemsCnt = FrmSportAndGameSelection.GetManualWagerItemsCount(TicketNumber, WagerNumber);
        if (itemsCnt == null) {
          rbItemLog.ItemNumber = 1;
        }
        else {
          rbItemLog.ItemNumber = (int)itemsCnt + 1;
        }
      }

      rbItemLog.Description = GetManualPlayDescription();

      if (txtManualPlayGradeDateTime.Text != "")
        rbItemLog.GradeDateTime = DateTime.Parse(txtManualPlayGradeDateTime.Text);

      rbItemLog.AmountWagered = txtRisk.Text.Length > 0 ? double.Parse(txtRisk.Text) : 0;

      rbItemLog.ToWinAmount = txtToWin.Text.Length > 0 ? double.Parse(txtToWin.Text) : 0;

      rbItemLog.FinalMoney = 0;
      rbItemLog.FinalToBase = 0;

      if (XToy != null) {
        rbItemLog.StrOdds = txtOdds.Text;
        rbItemLog.FinalMoney = XToy[0];
        rbItemLog.FinalToBase = XToy[1];
      }

      rbItemLog.Outcome = "P";

      rbItemLog.LayoffFlag = chbLayoffWager.Checked ? "Y" : "N";

      rbItemLog.FreePlayFlag = FrmSportAndGameSelection.IsFreePlay ? "Y" : "N";

      rbItemLog.Type = cmbType.Text;

      if (Mdi.TicketNumber != rbItemLog.TicketNumber && Mdi.PendingManualPlayItemInDb != null &&
          Mdi.PendingManualPlayItemInDb.TicketNumber == rbItemLog.TicketNumber &&
          Mdi.PendingManualPlayItemInDb.WagerNumber == rbItemLog.WagerNumber &&
          Mdi.PendingManualPlayItemInDb.ItemNumber == rbItemLog.ItemNumber) {
        rbItemLog.PendingManualPlayItemInDb = Mdi.PendingManualPlayItemInDb;
      }

      if (FrmMode == "Edit") {

        if (Mdi.Ticket.ManualWagerItems != null && Mdi.Ticket.ManualWagerItems.Any(w => w.TicketNumber == rbItemLog.TicketNumber && w.WagerNumber == rbItemLog.WagerNumber && w.ItemNumber == rbItemLog.ItemNumber)) {
          Mdi.Ticket.UpdateManualWagerItem(rbItemLog);
        }
        else {
          Mdi.Ticket.AddManualWagerItem(rbItemLog);
        }
      }
      else {
        Mdi.Ticket.AddManualWagerItem(rbItemLog);
      }
    }

    private void AddToWagersList() {

      var rbLog = new Ticket.Wager();

      if (FrmMode == "Edit") {
        rbLog.TicketNumber = TicketNumberToUpd;
        rbLog.WagerNumber = WagerNumberToUpd;
        TicketNumber = TicketNumberToUpd;
        WagerNumber = WagerNumberToUpd;
      }
      else {
        rbLog.WagerNumber = FrmSportAndGameSelection.Mdi.Ticket.GetWagersCount(Mdi.TicketNumber) + 1;
        WagerNumber = rbLog.WagerNumber;
        rbLog.TicketNumber = Mdi.TicketNumber;
        TicketNumber = rbLog.TicketNumber;
      }

      rbLog.LogDateTime = Mdi.GetCurrentDateTime();
      rbLog.AmountWagered = txtRisk.Text.Length > 0 ? double.Parse(txtRisk.Text) : 0;

      rbLog.WagerTypeName = "Manual Play";
      rbLog.WagerType = WagerType.MANUALPLAY;

      rbLog.WagerStatus = "P";
      rbLog.ToWinAmount = txtToWin.Text.Length > 0 ? double.Parse(txtToWin.Text) : 0;

      rbLog.TeaserName = null;
      rbLog.ParlayName = null;
      rbLog.MinimumPicks = 1;
      rbLog.Ties = 0;
      rbLog.RoundRobinLink = null;
      rbLog.ArBirdcageLink = null;
      rbLog.ArLink = null;
      rbLog.ContinueOnPushFlag = null;
      rbLog.PlayNumber = rbLog.WagerNumber;
      rbLog.AcceptedDateTime = Mdi.GetCurrentDateTime();
      rbLog.BoxLink = null;
      rbLog.LayoffFlag = chbLayoffWager.Checked ? "Y" : "N";
      rbLog.MaxPayout = 0;
      rbLog.ParlayPayoutType = "";
      rbLog.FreePlayFlag = FrmSportAndGameSelection.IsFreePlay ? "Y" : "N";
      rbLog.RifBackwardTicketNumber = null;
      rbLog.RifBackwardWagerNumber = null;
      rbLog.RifWinOnlyFlag = null;
      rbLog.RifCurrentPlayFlag = null;

      if (FrmMode == "Edit") {
        if (Mdi.Ticket.Wagers != null && Mdi.Ticket.Wagers.Any(w => w.TicketNumber == rbLog.TicketNumber && w.WagerNumber == rbLog.WagerNumber)) {
          Mdi.Ticket.UpdateWager(rbLog);
        }
        else {
          Mdi.Ticket.AddWager(rbLog);
        }
      }
      else {
        Mdi.Ticket.AddWager(rbLog);
      }

      var amountsStr = " Risking: " + rbLog.AmountWagered + ", to Win:" + rbLog.ToWinAmount;
      Tag = "Added " + rbLog.WagerTypeName + " to Ticket in " + (String.IsNullOrEmpty(FrmMode) ? "New" : FrmMode) + " mode. Customer : " + FrmSportAndGameSelection.Customer.CustomerID.Trim()
        + " " + rbLog.TicketNumber + "." + (rbLog.WagerNumber) + amountsStr;
    }

    private void CancelWager(bool closeForm = true) {
      if (WagerNumber > 0 && FrmMode != "Edit" && _deleteWagersOnExit) {
        Mdi.Ticket.DeleteWager(TicketNumber, WagerNumber);
        Mdi.Ticket.DeleteManualWagerItem(TicketNumber, WagerNumber);
      }

      Mdi.WageringPanelName = null;
      WageringForm.Close(this, "New", null, null, closeForm);
    }

    private string GetManualPlayDescription() {
      var desc = txtDescription.Text.Replace(Environment.NewLine, " | ");

      return desc;
    }

    private bool IsValidWager() {
      if (txtManualPlayGradeDateTime.Text.Length == 0) {
        MessageBox.Show(Resources.FrmPlaceManual_IsValidWager_Invalid_Date_entered___Must_be_in_MM_DD_YY_or_MM_DD_YYYY_format_, Resources.FrmPlaceManual_IsValidWager_Invalid_Date);
        dtmManualPlayGradeDate.Focus();
        return false;
      }

      if (txtDescription.Text.Length == 0) {
        MessageBox.Show(Resources.FrmPlaceManual_IsValidWager_Please_enter_the_description_of_the_manual_play_, Resources.FrmPlaceManual_IsValidWager_Invalid_Description);
        txtDescription.Focus();
        return false;
      }

      if (XToy == null && txtOdds.Enabled && txtOdds.Text.Length > 0) {
        MessageBox.Show(Resources.FrmPlaceManual_IsValidWager_Odds_must_be_in_the_format__9_to_9__where_9_is_an_unsigned_integer_or_decimal_between_1_and_9999_, Resources.FrmPlaceManual_IsValidWager_Invalid_Amount);
        txtOdds.Focus();
        return false;
      }

      if (txtRisk.Text.Length == 0 || txtToWin.Text.Length == 0) {
        MessageBox.Show(Resources.FrmPlaceManual_IsValidWager_Both_a_risk_and_to_win_amounts_must_be_entered_, Resources.FrmPlaceManual_IsValidWager_Invalid_Amount);
        txtRisk.Focus();
        return false;
      }

      return true;
    }

    private void PopulateManualPlayTypeList() {
      //declare @text varchar(255)
      //SET @text = 'Number of 3 pts Scores:' + CHAR(13)+ CHAR(10) + 'Player Name:'
      //insert into tbManualPlayTemplate (ManualPlayType, TemplateText)
      //values ('Long Distance Shoots', @text)
      using (var mp = new LineOffering.ManualPlay(Mdi.AppModuleInfo)) {
        TypesList = mp.GetTypes();
      }

      cmbType.Items.Add("");

      foreach (var li in TypesList) {
        cmbType.Items.Add(li.ManualPlayType.Trim());
      }
    }

    private void ResetBetType() {
      var frm = FormF.GetFormByName("frmSportAndGameSelection", this);

      if (frm == null) {
        return;
      }

      var frmSports = (FrmSportAndGameSelection)frm;

      if (frmSports.SelectedWagerType != null)
        frmSports.SelectedWagerType = "Straight Bet";

      frm = FormF.GetFormByName("MdiTicketWriter", this);

      if (frm == null) {
        return;
      }

      var frmMdi = (MdiTicketWriter)frm;

      if (frmSports.SelectedWagerType != null)
        frmMdi.ToggleFpAndRifChecks(true);
    }

    #endregion

    #region Events

    private void btnCancel_Click(object sender, EventArgs e) {
      CancelWager();
    }

    private void btnPlaceBet_Click(object sender, EventArgs e) {
      if (IsValidWager()) {
        _deleteWagersOnExit = false;
        AddToWagersList();
        AddToWagerItemsList();

        FrmSportAndGameSelection.BuildReadbackLogInfoData(true);
        FrmSportAndGameSelection.PopulateReadbackLogInfoDgvw();
        Mdi.WageringPanelName = null;
        WageringForm.Close(this);
      }
    }

    private void cmbType_SelectedIndexChanged(object sender, EventArgs e) {
      var selectedType = ((ComboBox)sender).Text;
      var typeTemplate = "";
      if (TypesList != null) {
        foreach (var tmp in TypesList) {
          if (tmp.ManualPlayType.Trim() == selectedType) {
            typeTemplate = tmp.TemplateText.Trim();
            break;
          }
        }
      }

      var typeTemplateAry = typeTemplate.Split(new[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

      txtDescription.Text = "";
      foreach (var str in typeTemplateAry) {
        txtDescription.Text += str;
        txtDescription.Text += Environment.NewLine;
      }


    }

    private void dtmManualPlayGradeDate_ValueChanged(object sender, EventArgs e) {
      var dtp = (DateTimePicker)sender;

      txtManualPlayGradeDateTime.Text = dtp.Text;
    }

    private void frmPlaceManual_FormClosing(object sender, FormClosingEventArgs e) {
      CancelWager(false);
      ResetBetType();
    }

    private void frmPlaceManual_Load(object sender, EventArgs e) {
      if (FrmSportAndGameSelection.WageringDisabled) {
        MessageBox.Show(@"Wagering is not allowed for this account!");
        Close();
      }
      if (ModuleName == "") {
        var mdi = (MdiTicketWriter)Parent;

        if (mdi != null) {
          ModuleName = mdi.ParameterList.GetItem("moduleName").Value;
        }
      }

      if (FrmMode == "Edit") {
        txtDescription.Text = EditModeInfo.Description.Replace(" . ", "\r\n");
        txtManualPlayGradeDateTime.Text = EditModeInfo.GradeDateTime.ToShortDateString();
        txtOdds.Text = EditModeInfo.Odds;
        txtRisk.Text = EditModeInfo.SelectedAmountWagered;
        txtToWin.Text = EditModeInfo.SelectedToWinAmount;
        chbLayoffWager.Checked = EditModeInfo.LayoffWager;
        btnPlaceBet.Text = Resources.FrmPlaceTeaser_frmPlaceTeaser_Load_Modify_ + FrmSportAndGameSelection.SelectedWagerType;
      }

      PopulateManualPlayTypeList();

      chbLayoffWager.Visible = _layoffWagersAccepted;


    }

    private void txtManualPlayGradeDateTime_KeyUp(object sender, KeyEventArgs e) {
      if (e.KeyCode == Keys.Delete || e.KeyCode == Keys.Back) {
        ((TextBox)sender).Text = "";
      }
    }

    private void txtOdds_KeyUp(object sender, KeyEventArgs e) {
      XToy = null;
      XToy = BuildxToyObj(((TextBox)sender).Text);
    }

    private void txtRisk_KeyUp(object sender, KeyEventArgs e) {
      if (XToy != null && XToy[1] > 0) {
        txtToWin.Text = ((NumberTextBox)sender).Text != "" ? TwUtilities.DetermineXtoYToWinAmount(double.Parse(((NumberTextBox)sender).Text), XToy[0], XToy[1]).ToString(CultureInfo.InvariantCulture) : "";
      }
      else {
        if (txtRisk.Text.Length > 0 && txtOdds.Text.Length == 0)
          txtOdds.Enabled = false;
        else
          txtOdds.Enabled = true;
      }
    }

    private void txtToWin_KeyUp(object sender, KeyEventArgs e) {
      if (XToy != null && XToy[0] > 0) {
        txtRisk.Text = ((NumberTextBox)sender).Text != "" ? TwUtilities.DetermineXtoYRiskAmount(double.Parse(((NumberTextBox)sender).Text), XToy[0], XToy[1]).ToString(CultureInfo.InvariantCulture) : "";
      }
    }

    #endregion
  }
}