﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using GUILibraries.Forms;
using GUILibraries.Utilities;
using InstanceManager.BusinessLayer;
using SIDLibraries.Entities;

namespace TicketWriter.UI {
  public sealed partial class FrmPayCard : SIDForm {
    #region Public Properties

    public string ModuleName { get; set; }

    public string PayCardType { get; set; }

    public string Title { get; set; }

    public string WagerType { get; set; }

    #endregion

    #region Private Properties

    #endregion

    #region Public vars


    #endregion

    #region Private vars


    #endregion

    #region Structures


    #endregion

    #region Constructors

    public FrmPayCard(ModuleInfo moduleInfo)
      : base(moduleInfo) {
      InitializeComponent();
    }

    #endregion

    #region Public Methods

    #endregion

    #region Private Methods

    private void FillParlayPayCardGridView() {
      Form frm = FormF.GetFormByName("MdiTicketWriter", this);

      if (frm == null) {
        return;
      }

      var mdi = (MdiTicketWriter)frm;

      if (mdi.ParlayPayCard == null || mdi.ParlayPayCard.Count == 0) {

        mdi.GetCustomerParlayPayCard();
      }

      List<spLOGetParlayPayCard_Result> parlayPayCard = mdi.ParlayPayCard;

      foreach (var pc in parlayPayCard) {
        String payCardInfo1 = pc.GamesPicked.ToString(CultureInfo.InvariantCulture) + " games picked pays... \t\t ";
        String payCardInfo2 = pc.MoneyLine.ToString() + " to " + pc.ToBase.ToString(); // +" \t\t ";
        String payCardInfo3 = " Max Payout: " + pc.MaxPayoutMoneyLine + " " + pc.MaxPayoutToBase;
        dgvwPayCard.Rows.Add(payCardInfo1, payCardInfo2, payCardInfo3);
      }

    }

    private void FillTeaserPayCardGridView(string payCardType) {
      var frm = FormF.GetFormByName("MdiTicketWriter", this);

      if (frm == null) {
        return;
      }

      var mdi = (MdiTicketWriter)frm;

      if (mdi.TeasersPayCard == null || mdi.TeasersPayCard.Count == 0) {

        mdi.GetCustomerTeasersPayCard();
      }

      List<spLOGetTeaserPayCard_Result> teaserPayCard = (from p in mdi.TeasersPayCard where p.TeaserName.Trim() == payCardType select p).ToList();
      const string payCardInfo3 = "";

      foreach (var pc in teaserPayCard) {
        String payCardInfo1 = pc.GamesWon.ToString(CultureInfo.InvariantCulture) + " games won of " + pc.GamesPicked.ToString(CultureInfo.InvariantCulture) + " pays... \t\t ";
        String payCardInfo2 = pc.MoneyLine + " to " + pc.ToBase;
        dgvwPayCard.Rows.Add(payCardInfo1, payCardInfo2, payCardInfo3);
      }

    }

    #endregion

    #region Events

    private void btnOK_Click(object sender, EventArgs e) {
      Close();
    }

    private void FrmPayCard_Load(object sender, EventArgs e) {
      Text = Title;

      switch (WagerType) {
        case "Parlay":
          lblPayCard.Text = PayCardType;
          FillParlayPayCardGridView();
          break;

        case "Teaser":
          lblPayCard.Text = PayCardType + @" " + WagerType;
          FillTeaserPayCardGridView(PayCardType);
          break;
      }

    }

    #endregion
  }
}
