﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using GUILibraries.Forms;
using GUILibraries.Utilities;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using SIDLibraries.Utilities;
using TicketWriter.Properties;

namespace TicketWriter.UI {
  public sealed partial class FrmOpenCustomer : SIDForm {
    #region Public Properties

    public List<spULPGetCurrentUserPermissions_Result> CurrentUserPermissions { get; set; }

    public spCstGetCustomer_Result Customer {
// ReSharper disable once MemberCanBePrivate.Global
      get {
        return Mdi.Customer;
      }
      set {
        Mdi.Customer = value;
      }
    }


    private List<spCstGetRestrictionsByCustomer_Result> CustomerRestrictions {
      get {
        return Mdi.CustomerRestrictions;
      }
      set {
        Mdi.CustomerRestrictions = value;
      }
    }

    private MdiTicketWriter Mdi {
      get {
        return _mdiTicketWriter ??
               (_mdiTicketWriter = (MdiTicketWriter)FormF.GetFormByName("MdiTicketWriter", this));
      }
    }

    public FormParameterList ParameterList { get; set; }


    #endregion

    #region Private Vars
    private MdiTicketWriter _mdiTicketWriter;

    #endregion

    #region Constructors

    public FrmOpenCustomer(ModuleInfo moduleInfo) : base (moduleInfo) {
      InitializeComponent();
    }

    #endregion

    #region Private Methods

    private void CloseForm() {
      if (!string.IsNullOrEmpty(Customer.AccessedBy)) {
        string[] stringSeparator = { "at" };
        string userName = Customer.AccessedBy.Split(stringSeparator, StringSplitOptions.RemoveEmptyEntries)[0].Trim(), machineName = Customer.AccessedBy.Split(stringSeparator, StringSplitOptions.RemoveEmptyEntries)[1].Trim(),
           message = "The customer is already being used by " + userName
                   + " at " + machineName + ". Opening a customer simultaneously "
                   + "by two different ticket writers can result in the display "
                   + "of an inaccurate available balance. Continue opening the customer?";
        var dialogResult = MessageBox.Show(message, ParameterList.GetItem("AppName").Value, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);

        if (dialogResult == DialogResult.No) {
          txtCustomerPIN.Select();
          DialogResult = DialogResult.None;
          return;
        }
      }

      using (var customerService = new Customers(AppModuleInfo)) {
        Customer.AccessedBy = Environment.UserName + " at " + Environment.MachineName;
        Customer.LastVerBalance = Customer.CurrentBalance;
        Customer.LastVerDocumentNumber = Customer.LastInsDocumentNumber;
        Customer.LastVerDateTime = Mdi.GetCurrentDateTime();
        customerService.SetCustomerInUse(Customer.CustomerID, Customer.AccessedBy, Customer.LastVerBalance, Customer.LastVerDocumentNumber);
        //customerService.UpdateCustomerInfo(Customer);
      }

      using (var log = new LogWriter(AppModuleInfo)) {
        log.WriteToCuAccessLog(ParameterList.GetItem("ModuleName").Value, "Open Customer", Customer.CustomerID);
      }



      ((MdiTicketWriter)Owner).Customer = Customer;

      using (var trn = new Transactions(Mdi.AppModuleInfo)) {
        ((MdiTicketWriter)Owner).TicketNumber = trn.GetNextDocumentNumber();
      }
      ((MdiTicketWriter)Owner).WageringStartDate = Mdi.GetCurrentDateTime();
      Mdi.Ticket = new Ticket { CustomerId = Customer.CustomerID.Trim(), TicketNumber = ((MdiTicketWriter)Owner).TicketNumber, StartDateTime = ((MdiTicketWriter)Owner).WageringStartDate, AgentId = (Customer.AgentID ?? "").Trim() };

      Mdi.BalanceInfo = new CustomerBalanceInfo(Customer.CustomerID.Trim(), Mdi.AppModuleInfo, Mdi.Ticket);
      Mdi.FillCustomerDataTextBoxes("A");
      Mdi.ToggleEnableRifOptionsForCustomer(Customer.EnableRifFlag == "Y");
      Tag = "Retrieved Customer : " + (Customer.CustomerID ?? "").Trim();
      Close();
    }

    private void GetLastCustomer() {
      txtCustomerPIN.Text = @"TW";

      if (((MdiTicketWriter)Owner).LastCustomer == null) {
        return;
      }
      txtCustomerPIN.Text = ((MdiTicketWriter)Owner).LastCustomer.CustomerID;
      RetrieveCustomer();
    }

    private void KeepFormClean() {
      Customer = null;
      CustomerRestrictions = null;
      btnVerified.Visible = false;
      btnRetrieve.Visible = true;
      btnBalanceInfo.Enabled = false;
      btnDailyFigure.Enabled = false;
      btnHistory.Enabled = false;
      txtPassword.Text = txtCurrentBalance.Text =
                          txtFreePlayBalance.Text =
                          txtBalance.Text =
                          txtDate.Text =
                          txtPendingWagersCount.Text =
                          txtPendingWagersDebitAmount.Text =
                          txtPendingWagersCreditAmount.Text =
                          txtPendingFreePlaysCount.Text =
                          txtPendingFreePlaysAmount.Text = "";
      AcceptButton = btnRetrieve;
    }

    private void RetrieveCustomer() {

      if (txtCustomerPIN.Text.Trim().Length == 2) {
        MessageBox.Show(Resources.FrmOpenCustomer_RetrieveCustomer_Please_enter_a_customer_ID, ParameterList.GetItem("AppName").Value, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        return;
      }

      if (Customer == null) {
        using (var customerService = new Customers(AppModuleInfo)) {
          Customer = customerService.GetCustomer(txtCustomerPIN.Text);
        }
      }

      if (CustomerRestrictions == null) {
        using (var customerService = new Customers(AppModuleInfo)) {
          CustomerRestrictions = customerService.GetRestrictionsByCustomer(txtCustomerPIN.Text);
        }
      }

      if (Customer == null) {
        MessageBox.Show(Resources.FrmOpenCustomer_RetrieveCustomer_Customer_ID_not_found_, ParameterList.GetItem("AppName").Value, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        return;
      }

      if (LoginsAndProfiles.ValidateUserFunctionalityAccess(CurrentUserPermissions, LoginsAndProfiles.TEST_LOGIN)) {
        using (var ag = new Agents(AppModuleInfo)) {
          if (!ag.IsTestAgent(String.IsNullOrWhiteSpace(Customer.AgentType) ? Customer.AgentID : Customer.CustomerID)) {
            MessageBox.Show(@"User only can view test Agents/Customers.");
            Customer = null;
            return;
          }
        }
      }

      var commentsForCustomer = "";

      if (Customer.CommentsForCustomer != null && Customer.CommentsForCustomer.Trim().Length > 0) {
        commentsForCustomer = Customer.CommentsForCustomer.Trim();
        commentsForCustomer += Environment.NewLine;
      }

      if (Customer.CommentsForTW != null && Customer.CommentsForTW.Trim().Length > 0) {
        commentsForCustomer += Environment.NewLine;
        commentsForCustomer += Customer.CommentsForTW.Trim();
      }

      if (commentsForCustomer.Trim().Length > 0) {
        var frmImportantComments = new FrmImportantComments(AppModuleInfo) { Comment = commentsForCustomer.Trim() };
        frmImportantComments.ShowDialog();
      }

      Mdi.LoginInUse.CallCustomerID = txtCustomerPIN.Text.Trim();
      Mdi.LoginInUse.CallStartTime = Mdi.GetCurrentDateTime();
      using (var logins = new LoginsAndProfiles(AppModuleInfo)) {
        logins.UpdateCurrentLoginInUseInfo(Mdi.LoginInUse);
      }

      if (Customer.Active != "Y") {
        MessageBox.Show(Resources.FrmOpenCustomer_RetrieveCustomer_Wagering_has_been_suspended_for_this_customer_, ParameterList.GetItem("AppName").Value, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        txtCustomerPIN.Focus();
        return;
      }

      txtBalance.Text = FormatNumber(Customer.LastVerBalance);
      txtCurrentBalance.Text = FormatNumber(Customer.CurrentBalance);
      txtDate.Text = DisplayDate.Format(Customer.LastVerDateTime); // .ToString("dddd - MM/dd/yyyy HH:mm tt");
      txtFreePlayBalance.Text = FormatNumber(Customer.FreePlayBalance);

      txtPassword.Text = Customer.Password;
      txtPendingWagersCount.Text = FormatNumber(Customer.PendingWagerCount, false);
      txtPendingWagersCreditAmount.Text = FormatNumber(0); // do not know where this value comes from *
      txtPendingWagersDebitAmount.Text = FormatNumber(Customer.PendingWagerBalance);

      txtPendingFreePlaysAmount.Text = FormatNumber(Customer.FreePlayPendingBalance);
      txtPendingFreePlaysCount.Text = FormatNumber(Customer.FreePlayPendingCount, false);

      // if the cust has been verified today, display different than this *
      var today = Mdi.GetCurrentDateTime();

      if (Customer.LastVerDateTime != null && (Customer.LastVerDateTime.Value.Year == today.Year && Customer.LastVerDateTime.Value.Month == today.Month && Customer.LastVerDateTime.Value.Day == today.Day)) {
        SetVerifiedInfoFormStatus();
        return;
      }
      SetRetrievedInfoFormStatus();
    }

    private void OpenDailyFigure() {
      var originalText = btnDailyFigure.Text;
      btnDailyFigure.Text = Resources.FrmOpenCustomer_OpenDailyFigure_Opening_ + btnDailyFigure.Text;
      btnDailyFigure.Enabled = !btnDailyFigure.Enabled;

      var frmDailyFigures = new FrmDailyFigures(txtCustomerPIN.Text, String.IsNullOrEmpty(Mdi.Customer.Currency) ? Currencies.DEFAULT_CURRENCY : Mdi.Customer.Currency, AppModuleInfo, CurrentUserPermissions, Mdi.SoundCardDetected) {
        StartPosition = FormStartPosition.CenterParent,
        Icon = Icon
      };
      frmDailyFigures.ShowDialog();

      btnDailyFigure.Enabled = !btnDailyFigure.Enabled;
      btnDailyFigure.Text = originalText;
    }

    private void OpenTransactionHistory() {
      var originalText = btnHistory.Text;
      btnHistory.Text = Resources.FrmOpenCustomer_OpenDailyFigure_Opening_ + originalText;
      btnHistory.Enabled = !btnHistory.Enabled;

      Mdi.OpenCustomerTransactionsForm(CurrentUserPermissions, Customer, ParameterList);

      btnHistory.Enabled = !btnHistory.Enabled;
      btnHistory.Text = originalText;
    }

    private void SetRetrievedInfoFormStatus() {
      AcceptButton = btnVerified;
      lblMessages.Visible = btnBalanceInfo.Visible = btnRetrieve.Visible = false;

      btnDailyFigure.Enabled = btnHistory.Enabled = btnVerified.Enabled = btnVerified.Visible =
          lblBalance.Visible = lblPendingWagersCreditAmount.Visible =
          lblCurrentBalance.Visible = lblDate.Visible =
          lblPendingWagersDebitAmount.Visible = lblFreeplayAmt.Visible =
          lblFreePlayBalance.Visible = lblPendingFreePlaysCount.Visible =
          lblPendingWagersCount.Visible = txtBalance.Visible =
          txtCurrentBalance.Visible = txtDate.Visible = txtFreePlayBalance.Visible =
          txtPendingWagersCount.Visible = txtPendingWagersCreditAmount.Visible =
          txtPendingWagersDebitAmount.Visible = txtPendingFreePlaysCount.Visible =
          txtPendingFreePlaysAmount.Visible = true;
    }

    private void ShowAudioRecordingStatus() {
      var recordAudio = Params.RecordAudio;

      if (recordAudio && Mdi.SoundCardDetected) {
        lblMessages.Text = @"Audio Recording has Started...";
      }
      else {
        lblMessages.Text = @"Audio Recording is Disabled...";
      }
    }

    #endregion

    #region Public methods

    private void SetVerifiedInfoFormStatus() {
      AcceptButton = btnVerified;
      lblMessages.Visible = btnRetrieve.Visible = false;
      btnBalanceInfo.Enabled = btnBalanceInfo.Visible = btnDailyFigure.Enabled = btnVerified.Enabled = btnVerified.Visible = true;
      btnVerified.Focus();
    }

    #endregion

    #region Events

    private void btnBalanceInfo_Click(object sender, EventArgs e) {
      SetRetrievedInfoFormStatus();
    }

    private void btnCancel_Click(object sender, EventArgs e) {
      Mdi.LoginInUse.CallCustomerID = null;
      Mdi.LoginInUse.CallStartTime = null;
      using (var logins = new LoginsAndProfiles(AppModuleInfo)) {
        logins.UpdateCurrentLoginInUseInfo(Mdi.LoginInUse);
      }
      Mdi.StopAudioRecording();
      Close();
    }

    private void btnDailyFigure_Click(object sender, EventArgs e) {
      OpenDailyFigure();
    }

    private void btnHistory_Click(object sender, EventArgs e) {
      OpenTransactionHistory();
    }

    private void btnLast_Click(object sender, EventArgs e) {
      GetLastCustomer();
    }

    private void btnRetrieve_Click(object sender, EventArgs e) {
      Customer = null;
      CustomerRestrictions = null;
      RetrieveCustomer();
    }

    private void btnVerified_Click(object sender, EventArgs e) {
      CloseForm();
    }

    private void FrmOpenCustomer_Load(object sender, EventArgs e) {
      ShowAudioRecordingStatus();
    }

    private void txtCustomerPIN_TextChanged(object sender, EventArgs e) {
      KeepFormClean();
    }

    #endregion
  }
}
