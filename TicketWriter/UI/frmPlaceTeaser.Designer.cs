﻿namespace TicketWriter.UI {
  partial class FrmPlaceTeaser {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
      this.components = new System.ComponentModel.Container();
      this.lblTeaser = new System.Windows.Forms.Label();
      this.cmbTeaser = new System.Windows.Forms.ComboBox();
      this.CustomerTeasersBindingSource = new System.Windows.Forms.BindingSource(this.components);
      this.lblTeams = new System.Windows.Forms.Label();
      this.nudTeams = new System.Windows.Forms.NumericUpDown();
      this.lblRisk = new System.Windows.Forms.Label();
      this.lblWin = new System.Windows.Forms.Label();
      this.btnPlaceBet = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.btnPayTable = new System.Windows.Forms.Button();
      this.txtSportsSpecsInfo = new System.Windows.Forms.TextBox();
      this.dgvwPlaceTeaser = new System.Windows.Forms.DataGridView();
      this.TypeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.ctxModifyWager = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.descriptionAndAdjustedPriceDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.selectedCellDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.wagerNumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Number = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.selectedOddsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.selectedBPointsOptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.selectedAmountWageredDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.selectedToWinAmountDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.GameNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.WagerTypeMode = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.PriceType = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.PeriodNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.rowGroupNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.periodWagerCutoff = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.WageringMode = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.LayoffWager = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.FixedPrice = new System.Windows.Forms.DataGridViewCheckBoxColumn();
      this.Pitcher1MStart = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Pitcher2MStart = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.TotalPointsOu = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.SelectedAmericanPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.PeriodDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.AlreadyBetItemCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.TicketNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.bndsrc = new System.Windows.Forms.BindingSource(this.components);
      this.wagerTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.descriptionAndAdjustedPriceDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.txtToWin = new GUILibraries.Controls.NumberTextBox();
      this.txtRisk = new GUILibraries.Controls.NumberTextBox();
      this.typeNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.descriptionAndAdjustedPriceDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.descriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.typeCodeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.typeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.selectedRowDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.selectedCellDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.grpRoundRobin = new System.Windows.Forms.GroupBox();
      this.lblRoundRobinOutcome = new System.Windows.Forms.Label();
      this.cmbRoundRobin = new System.Windows.Forms.ComboBox();
      this.bndsrcRoundRobinType = new System.Windows.Forms.BindingSource(this.components);
      this.chbRoundRobin = new System.Windows.Forms.CheckBox();
      this.lblMaxWinDisplay = new System.Windows.Forms.Label();
      this.lblMaxRiskDisplay = new System.Windows.Forms.Label();
      ((System.ComponentModel.ISupportInitialize)(this.CustomerTeasersBindingSource)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudTeams)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dgvwPlaceTeaser)).BeginInit();
      this.ctxModifyWager.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.bndsrc)).BeginInit();
      this.grpRoundRobin.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.bndsrcRoundRobinType)).BeginInit();
      this.SuspendLayout();
      // 
      // lblTeaser
      // 
      this.lblTeaser.AutoSize = true;
      this.lblTeaser.Location = new System.Drawing.Point(5, 7);
      this.lblTeaser.Name = "lblTeaser";
      this.lblTeaser.Size = new System.Drawing.Size(43, 13);
      this.lblTeaser.TabIndex = 0;
      this.lblTeaser.Text = "Teaser:";
      // 
      // cmbTeaser
      // 
      this.cmbTeaser.DataSource = this.CustomerTeasersBindingSource;
      this.cmbTeaser.DisplayMember = "TeaserName";
      this.cmbTeaser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbTeaser.FormattingEnabled = true;
      this.cmbTeaser.Location = new System.Drawing.Point(51, 4);
      this.cmbTeaser.Name = "cmbTeaser";
      this.cmbTeaser.Size = new System.Drawing.Size(217, 21);
      this.cmbTeaser.TabIndex = 1;
      this.cmbTeaser.ValueMember = "TeaserName";
      this.cmbTeaser.SelectedIndexChanged += new System.EventHandler(this.cmbTeaser_SelectedIndexChanged);
      // 
      // CustomerTeasersBindingSource
      // 
      this.CustomerTeasersBindingSource.DataSource = typeof(SIDLibraries.Entities.spCstGetCustomerTeaserSpecs_Result);
      // 
      // lblTeams
      // 
      this.lblTeams.AutoSize = true;
      this.lblTeams.Location = new System.Drawing.Point(5, 31);
      this.lblTeams.Name = "lblTeams";
      this.lblTeams.Size = new System.Drawing.Size(42, 13);
      this.lblTeams.TabIndex = 2;
      this.lblTeams.Text = "Teams:";
      // 
      // nudTeams
      // 
      this.nudTeams.Location = new System.Drawing.Point(48, 29);
      this.nudTeams.Name = "nudTeams";
      this.nudTeams.ReadOnly = true;
      this.nudTeams.Size = new System.Drawing.Size(35, 20);
      this.nudTeams.TabIndex = 2;
      this.nudTeams.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
      this.nudTeams.ValueChanged += new System.EventHandler(this.nudTeams_ValueChanged);
      this.nudTeams.Enter += new System.EventHandler(this.nudTeams_Enter);
      // 
      // lblRisk
      // 
      this.lblRisk.AutoSize = true;
      this.lblRisk.Location = new System.Drawing.Point(83, 31);
      this.lblRisk.Name = "lblRisk";
      this.lblRisk.Size = new System.Drawing.Size(31, 13);
      this.lblRisk.TabIndex = 4;
      this.lblRisk.Text = "Risk:";
      // 
      // lblWin
      // 
      this.lblWin.AutoSize = true;
      this.lblWin.Location = new System.Drawing.Point(177, 31);
      this.lblWin.Name = "lblWin";
      this.lblWin.Size = new System.Drawing.Size(29, 13);
      this.lblWin.TabIndex = 6;
      this.lblWin.Text = "Win:";
      // 
      // btnPlaceBet
      // 
      this.btnPlaceBet.Location = new System.Drawing.Point(8, 126);
      this.btnPlaceBet.Name = "btnPlaceBet";
      this.btnPlaceBet.Size = new System.Drawing.Size(75, 23);
      this.btnPlaceBet.TabIndex = 5;
      this.btnPlaceBet.Text = "Place Bet";
      this.btnPlaceBet.UseVisualStyleBackColor = true;
      this.btnPlaceBet.Click += new System.EventHandler(this.btnPlaceBet_Click);
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.Location = new System.Drawing.Point(92, 126);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 6;
      this.btnCancel.Text = "Cancel";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // btnPayTable
      // 
      this.btnPayTable.Location = new System.Drawing.Point(193, 126);
      this.btnPayTable.Name = "btnPayTable";
      this.btnPayTable.Size = new System.Drawing.Size(75, 23);
      this.btnPayTable.TabIndex = 7;
      this.btnPayTable.Text = "Pay Table";
      this.btnPayTable.UseVisualStyleBackColor = true;
      this.btnPayTable.Click += new System.EventHandler(this.btnPayTable_Click);
      // 
      // txtSportsSpecsInfo
      // 
      this.txtSportsSpecsInfo.Location = new System.Drawing.Point(746, 4);
      this.txtSportsSpecsInfo.Multiline = true;
      this.txtSportsSpecsInfo.Name = "txtSportsSpecsInfo";
      this.txtSportsSpecsInfo.ReadOnly = true;
      this.txtSportsSpecsInfo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.txtSportsSpecsInfo.Size = new System.Drawing.Size(246, 131);
      this.txtSportsSpecsInfo.TabIndex = 9;
      this.txtSportsSpecsInfo.Enter += new System.EventHandler(this.txtSportsSpecsInfo_Enter);
      // 
      // dgvwPlaceTeaser
      // 
      this.dgvwPlaceTeaser.AllowUserToAddRows = false;
      this.dgvwPlaceTeaser.AllowUserToDeleteRows = false;
      this.dgvwPlaceTeaser.AllowUserToResizeRows = false;
      this.dgvwPlaceTeaser.AutoGenerateColumns = false;
      this.dgvwPlaceTeaser.BackgroundColor = System.Drawing.Color.White;
      this.dgvwPlaceTeaser.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgvwPlaceTeaser.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
      this.dgvwPlaceTeaser.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgvwPlaceTeaser.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TypeName,
            this.descriptionAndAdjustedPriceDataGridViewTextBoxColumn2,
            this.selectedCellDataGridViewTextBoxColumn1,
            this.wagerNumberDataGridViewTextBoxColumn,
            this.Number,
            this.selectedOddsDataGridViewTextBoxColumn,
            this.selectedBPointsOptionDataGridViewTextBoxColumn,
            this.selectedAmountWageredDataGridViewTextBoxColumn,
            this.selectedToWinAmountDataGridViewTextBoxColumn,
            this.GameNumber,
            this.WagerTypeMode,
            this.PriceType,
            this.PeriodNumber,
            this.rowGroupNum,
            this.periodWagerCutoff,
            this.WageringMode,
            this.LayoffWager,
            this.FixedPrice,
            this.Pitcher1MStart,
            this.Pitcher2MStart,
            this.TotalPointsOu,
            this.SelectedAmericanPrice,
            this.PeriodDescription,
            this.AlreadyBetItemCount,
            this.TicketNumber});
      this.dgvwPlaceTeaser.DataSource = this.bndsrc;
      this.dgvwPlaceTeaser.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
      this.dgvwPlaceTeaser.Location = new System.Drawing.Point(272, 4);
      this.dgvwPlaceTeaser.MultiSelect = false;
      this.dgvwPlaceTeaser.Name = "dgvwPlaceTeaser";
      this.dgvwPlaceTeaser.ReadOnly = true;
      this.dgvwPlaceTeaser.RowHeadersVisible = false;
      this.dgvwPlaceTeaser.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
      this.dgvwPlaceTeaser.Size = new System.Drawing.Size(468, 131);
      this.dgvwPlaceTeaser.TabIndex = 8;
      this.dgvwPlaceTeaser.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvwPlaceTeaser_CellDoubleClick);
      this.dgvwPlaceTeaser.CellStateChanged += new System.Windows.Forms.DataGridViewCellStateChangedEventHandler(this.dgvwPlaceTeaser_CellStateChanged);
      this.dgvwPlaceTeaser.SelectionChanged += new System.EventHandler(this.dgvwPlaceTeaser_SelectionChanged);
      this.dgvwPlaceTeaser.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgvwPlaceTeaser_MouseDown);
      // 
      // TypeName
      // 
      this.TypeName.ContextMenuStrip = this.ctxModifyWager;
      this.TypeName.DataPropertyName = "TypeName";
      this.TypeName.HeaderText = "Type";
      this.TypeName.Name = "TypeName";
      this.TypeName.ReadOnly = true;
      this.TypeName.Width = 70;
      // 
      // ctxModifyWager
      // 
      this.ctxModifyWager.Enabled = false;
      this.ctxModifyWager.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editToolStripMenuItem,
            this.deleteToolStripMenuItem});
      this.ctxModifyWager.Name = "ctxModifyWager";
      this.ctxModifyWager.Size = new System.Drawing.Size(108, 48);
      // 
      // editToolStripMenuItem
      // 
      this.editToolStripMenuItem.Enabled = false;
      this.editToolStripMenuItem.Name = "editToolStripMenuItem";
      this.editToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
      this.editToolStripMenuItem.Text = "Edit...";
      this.editToolStripMenuItem.Click += new System.EventHandler(this.editToolStripMenuItem_Click);
      // 
      // deleteToolStripMenuItem
      // 
      this.deleteToolStripMenuItem.Enabled = false;
      this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
      this.deleteToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
      this.deleteToolStripMenuItem.Text = "Delete";
      this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
      // 
      // descriptionAndAdjustedPriceDataGridViewTextBoxColumn2
      // 
      this.descriptionAndAdjustedPriceDataGridViewTextBoxColumn2.DataPropertyName = "DescriptionAndAdjustedPrice";
      this.descriptionAndAdjustedPriceDataGridViewTextBoxColumn2.HeaderText = "Description and Adjusted Price";
      this.descriptionAndAdjustedPriceDataGridViewTextBoxColumn2.Name = "descriptionAndAdjustedPriceDataGridViewTextBoxColumn2";
      this.descriptionAndAdjustedPriceDataGridViewTextBoxColumn2.ReadOnly = true;
      this.descriptionAndAdjustedPriceDataGridViewTextBoxColumn2.Width = 375;
      // 
      // selectedCellDataGridViewTextBoxColumn1
      // 
      this.selectedCellDataGridViewTextBoxColumn1.DataPropertyName = "SelectedCell";
      this.selectedCellDataGridViewTextBoxColumn1.HeaderText = "SelectedCell";
      this.selectedCellDataGridViewTextBoxColumn1.Name = "selectedCellDataGridViewTextBoxColumn1";
      this.selectedCellDataGridViewTextBoxColumn1.ReadOnly = true;
      this.selectedCellDataGridViewTextBoxColumn1.Visible = false;
      // 
      // wagerNumberDataGridViewTextBoxColumn
      // 
      this.wagerNumberDataGridViewTextBoxColumn.DataPropertyName = "WagerNumber";
      this.wagerNumberDataGridViewTextBoxColumn.HeaderText = "WagerNumber";
      this.wagerNumberDataGridViewTextBoxColumn.Name = "wagerNumberDataGridViewTextBoxColumn";
      this.wagerNumberDataGridViewTextBoxColumn.ReadOnly = true;
      this.wagerNumberDataGridViewTextBoxColumn.Visible = false;
      // 
      // Number
      // 
      this.Number.DataPropertyName = "Number";
      this.Number.HeaderText = "Number";
      this.Number.Name = "Number";
      this.Number.ReadOnly = true;
      this.Number.Visible = false;
      // 
      // selectedOddsDataGridViewTextBoxColumn
      // 
      this.selectedOddsDataGridViewTextBoxColumn.DataPropertyName = "SelectedOdds";
      this.selectedOddsDataGridViewTextBoxColumn.HeaderText = "SelectedOdds";
      this.selectedOddsDataGridViewTextBoxColumn.Name = "selectedOddsDataGridViewTextBoxColumn";
      this.selectedOddsDataGridViewTextBoxColumn.ReadOnly = true;
      this.selectedOddsDataGridViewTextBoxColumn.Visible = false;
      // 
      // selectedBPointsOptionDataGridViewTextBoxColumn
      // 
      this.selectedBPointsOptionDataGridViewTextBoxColumn.DataPropertyName = "SelectedBPointsOption";
      this.selectedBPointsOptionDataGridViewTextBoxColumn.HeaderText = "SelectedBPointsOption";
      this.selectedBPointsOptionDataGridViewTextBoxColumn.Name = "selectedBPointsOptionDataGridViewTextBoxColumn";
      this.selectedBPointsOptionDataGridViewTextBoxColumn.ReadOnly = true;
      this.selectedBPointsOptionDataGridViewTextBoxColumn.Visible = false;
      // 
      // selectedAmountWageredDataGridViewTextBoxColumn
      // 
      this.selectedAmountWageredDataGridViewTextBoxColumn.DataPropertyName = "SelectedAmountWagered";
      this.selectedAmountWageredDataGridViewTextBoxColumn.HeaderText = "SelectedAmountWagered";
      this.selectedAmountWageredDataGridViewTextBoxColumn.Name = "selectedAmountWageredDataGridViewTextBoxColumn";
      this.selectedAmountWageredDataGridViewTextBoxColumn.ReadOnly = true;
      this.selectedAmountWageredDataGridViewTextBoxColumn.Visible = false;
      // 
      // selectedToWinAmountDataGridViewTextBoxColumn
      // 
      this.selectedToWinAmountDataGridViewTextBoxColumn.DataPropertyName = "SelectedToWinAmount";
      this.selectedToWinAmountDataGridViewTextBoxColumn.HeaderText = "SelectedToWinAmount";
      this.selectedToWinAmountDataGridViewTextBoxColumn.Name = "selectedToWinAmountDataGridViewTextBoxColumn";
      this.selectedToWinAmountDataGridViewTextBoxColumn.ReadOnly = true;
      this.selectedToWinAmountDataGridViewTextBoxColumn.Visible = false;
      // 
      // GameNumber
      // 
      this.GameNumber.DataPropertyName = "GameNumber";
      this.GameNumber.HeaderText = "GameNumber";
      this.GameNumber.Name = "GameNumber";
      this.GameNumber.ReadOnly = true;
      this.GameNumber.Visible = false;
      // 
      // WagerTypeMode
      // 
      this.WagerTypeMode.DataPropertyName = "WagerTypeMode";
      this.WagerTypeMode.HeaderText = "WagerTypeMode";
      this.WagerTypeMode.Name = "WagerTypeMode";
      this.WagerTypeMode.ReadOnly = true;
      this.WagerTypeMode.Visible = false;
      // 
      // PriceType
      // 
      this.PriceType.DataPropertyName = "PriceType";
      this.PriceType.HeaderText = "PriceType";
      this.PriceType.Name = "PriceType";
      this.PriceType.ReadOnly = true;
      this.PriceType.Visible = false;
      // 
      // PeriodNumber
      // 
      this.PeriodNumber.DataPropertyName = "PeriodNumber";
      this.PeriodNumber.HeaderText = "PeriodNumber";
      this.PeriodNumber.Name = "PeriodNumber";
      this.PeriodNumber.ReadOnly = true;
      this.PeriodNumber.Visible = false;
      // 
      // rowGroupNum
      // 
      this.rowGroupNum.DataPropertyName = "rowGroupNum";
      this.rowGroupNum.HeaderText = "rowGroupNum";
      this.rowGroupNum.Name = "rowGroupNum";
      this.rowGroupNum.ReadOnly = true;
      this.rowGroupNum.Visible = false;
      // 
      // periodWagerCutoff
      // 
      this.periodWagerCutoff.DataPropertyName = "periodWagerCutoff";
      this.periodWagerCutoff.HeaderText = "periodWagerCutoff";
      this.periodWagerCutoff.Name = "periodWagerCutoff";
      this.periodWagerCutoff.ReadOnly = true;
      this.periodWagerCutoff.Visible = false;
      // 
      // WageringMode
      // 
      this.WageringMode.DataPropertyName = "WageringMode";
      this.WageringMode.HeaderText = "WageringMode";
      this.WageringMode.Name = "WageringMode";
      this.WageringMode.ReadOnly = true;
      this.WageringMode.Visible = false;
      // 
      // LayoffWager
      // 
      this.LayoffWager.DataPropertyName = "LayoffWager";
      this.LayoffWager.HeaderText = "LayoffWager";
      this.LayoffWager.Name = "LayoffWager";
      this.LayoffWager.ReadOnly = true;
      this.LayoffWager.Visible = false;
      // 
      // FixedPrice
      // 
      this.FixedPrice.DataPropertyName = "FixedPrice";
      this.FixedPrice.HeaderText = "FixedPrice";
      this.FixedPrice.Name = "FixedPrice";
      this.FixedPrice.ReadOnly = true;
      this.FixedPrice.Visible = false;
      // 
      // Pitcher1MStart
      // 
      this.Pitcher1MStart.DataPropertyName = "Pitcher1MStart";
      this.Pitcher1MStart.HeaderText = "Pitcher1MStart";
      this.Pitcher1MStart.Name = "Pitcher1MStart";
      this.Pitcher1MStart.ReadOnly = true;
      this.Pitcher1MStart.Visible = false;
      // 
      // Pitcher2MStart
      // 
      this.Pitcher2MStart.DataPropertyName = "Pitcher2MStart";
      this.Pitcher2MStart.HeaderText = "Pitcher2MStart";
      this.Pitcher2MStart.Name = "Pitcher2MStart";
      this.Pitcher2MStart.ReadOnly = true;
      this.Pitcher2MStart.Visible = false;
      // 
      // TotalPointsOu
      // 
      this.TotalPointsOu.DataPropertyName = "TotalPointsOu";
      this.TotalPointsOu.HeaderText = "TotalPointsOu";
      this.TotalPointsOu.Name = "TotalPointsOu";
      this.TotalPointsOu.ReadOnly = true;
      this.TotalPointsOu.Visible = false;
      // 
      // SelectedAmericanPrice
      // 
      this.SelectedAmericanPrice.DataPropertyName = "SelectedAmericanPrice";
      this.SelectedAmericanPrice.HeaderText = "SelectedAmericanPrice";
      this.SelectedAmericanPrice.Name = "SelectedAmericanPrice";
      this.SelectedAmericanPrice.ReadOnly = true;
      this.SelectedAmericanPrice.Visible = false;
      // 
      // PeriodDescription
      // 
      this.PeriodDescription.DataPropertyName = "PeriodDescription";
      this.PeriodDescription.HeaderText = "PeriodDescription";
      this.PeriodDescription.Name = "PeriodDescription";
      this.PeriodDescription.ReadOnly = true;
      this.PeriodDescription.Visible = false;
      // 
      // AlreadyBetItemCount
      // 
      this.AlreadyBetItemCount.DataPropertyName = "AlreadyBetItemCount";
      this.AlreadyBetItemCount.HeaderText = "";
      this.AlreadyBetItemCount.Name = "AlreadyBetItemCount";
      this.AlreadyBetItemCount.ReadOnly = true;
      this.AlreadyBetItemCount.Width = 25;
      // 
      // TicketNumber
      // 
      this.TicketNumber.DataPropertyName = "TicketNumber";
      this.TicketNumber.HeaderText = "TicketNumber";
      this.TicketNumber.Name = "TicketNumber";
      this.TicketNumber.ReadOnly = true;
      this.TicketNumber.Visible = false;
      // 
      // bndsrc
      // 
      this.bndsrc.DataSource = typeof(SIDLibraries.BusinessLayer.TeaserItem);
      this.bndsrc.ListChanged += new System.ComponentModel.ListChangedEventHandler(this.bndsrc_ListChanged);
      // 
      // wagerTypeDataGridViewTextBoxColumn
      // 
      this.wagerTypeDataGridViewTextBoxColumn.Name = "wagerTypeDataGridViewTextBoxColumn";
      // 
      // descriptionAndAdjustedPriceDataGridViewTextBoxColumn
      // 
      this.descriptionAndAdjustedPriceDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
      this.descriptionAndAdjustedPriceDataGridViewTextBoxColumn.DataPropertyName = "DescriptionAndAdjustedPrice";
      this.descriptionAndAdjustedPriceDataGridViewTextBoxColumn.HeaderText = "Description and Adjusted Price";
      this.descriptionAndAdjustedPriceDataGridViewTextBoxColumn.Name = "descriptionAndAdjustedPriceDataGridViewTextBoxColumn";
      // 
      // txtToWin
      // 
      this.txtToWin.AllowCents = false;
      this.txtToWin.AllowNegatives = false;
      this.txtToWin.DividedBy = 1;
      this.txtToWin.DivideFlag = false;
      this.txtToWin.Location = new System.Drawing.Point(208, 28);
      this.txtToWin.Name = "txtToWin";
      this.txtToWin.ShowIfZero = true;
      this.txtToWin.Size = new System.Drawing.Size(60, 20);
      this.txtToWin.TabIndex = 4;
      this.txtToWin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtToWin_KeyPress);
      this.txtToWin.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtToWin_KeyUp);
      // 
      // txtRisk
      // 
      this.txtRisk.AllowCents = false;
      this.txtRisk.AllowNegatives = false;
      this.txtRisk.DividedBy = 1;
      this.txtRisk.DivideFlag = false;
      this.txtRisk.Location = new System.Drawing.Point(111, 28);
      this.txtRisk.Name = "txtRisk";
      this.txtRisk.ShowIfZero = true;
      this.txtRisk.Size = new System.Drawing.Size(60, 20);
      this.txtRisk.TabIndex = 3;
      this.txtRisk.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRisk_KeyPress);
      this.txtRisk.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtRisk_KeyUp);
      // 
      // typeNameDataGridViewTextBoxColumn
      // 
      this.typeNameDataGridViewTextBoxColumn.DataPropertyName = "TypeName";
      this.typeNameDataGridViewTextBoxColumn.HeaderText = "Type";
      this.typeNameDataGridViewTextBoxColumn.Name = "typeNameDataGridViewTextBoxColumn";
      this.typeNameDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
      this.typeNameDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
      // 
      // descriptionAndAdjustedPriceDataGridViewTextBoxColumn1
      // 
      this.descriptionAndAdjustedPriceDataGridViewTextBoxColumn1.DataPropertyName = "DescriptionAndAdjustedPrice";
      this.descriptionAndAdjustedPriceDataGridViewTextBoxColumn1.HeaderText = "Description And Adjusted Price";
      this.descriptionAndAdjustedPriceDataGridViewTextBoxColumn1.Name = "descriptionAndAdjustedPriceDataGridViewTextBoxColumn1";
      this.descriptionAndAdjustedPriceDataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
      this.descriptionAndAdjustedPriceDataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
      this.descriptionAndAdjustedPriceDataGridViewTextBoxColumn1.Width = 400;
      // 
      // descriptionDataGridViewTextBoxColumn
      // 
      this.descriptionDataGridViewTextBoxColumn.DataPropertyName = "Description";
      this.descriptionDataGridViewTextBoxColumn.HeaderText = "Description";
      this.descriptionDataGridViewTextBoxColumn.Name = "descriptionDataGridViewTextBoxColumn";
      this.descriptionDataGridViewTextBoxColumn.Visible = false;
      // 
      // typeCodeDataGridViewTextBoxColumn
      // 
      this.typeCodeDataGridViewTextBoxColumn.DataPropertyName = "TypeCode";
      this.typeCodeDataGridViewTextBoxColumn.HeaderText = "TypeCode";
      this.typeCodeDataGridViewTextBoxColumn.Name = "typeCodeDataGridViewTextBoxColumn";
      this.typeCodeDataGridViewTextBoxColumn.Visible = false;
      // 
      // typeDataGridViewTextBoxColumn
      // 
      this.typeDataGridViewTextBoxColumn.DataPropertyName = "Type";
      this.typeDataGridViewTextBoxColumn.HeaderText = "Type";
      this.typeDataGridViewTextBoxColumn.Name = "typeDataGridViewTextBoxColumn";
      this.typeDataGridViewTextBoxColumn.Visible = false;
      // 
      // selectedRowDataGridViewTextBoxColumn
      // 
      this.selectedRowDataGridViewTextBoxColumn.DataPropertyName = "SelectedRow";
      this.selectedRowDataGridViewTextBoxColumn.HeaderText = "SelectedRow";
      this.selectedRowDataGridViewTextBoxColumn.Name = "selectedRowDataGridViewTextBoxColumn";
      this.selectedRowDataGridViewTextBoxColumn.Visible = false;
      // 
      // selectedCellDataGridViewTextBoxColumn
      // 
      this.selectedCellDataGridViewTextBoxColumn.DataPropertyName = "SelectedCell";
      this.selectedCellDataGridViewTextBoxColumn.HeaderText = "SelectedCell";
      this.selectedCellDataGridViewTextBoxColumn.Name = "selectedCellDataGridViewTextBoxColumn";
      this.selectedCellDataGridViewTextBoxColumn.Visible = false;
      // 
      // grpRoundRobin
      // 
      this.grpRoundRobin.Controls.Add(this.lblRoundRobinOutcome);
      this.grpRoundRobin.Controls.Add(this.cmbRoundRobin);
      this.grpRoundRobin.Controls.Add(this.chbRoundRobin);
      this.grpRoundRobin.Location = new System.Drawing.Point(9, 70);
      this.grpRoundRobin.Name = "grpRoundRobin";
      this.grpRoundRobin.Size = new System.Drawing.Size(259, 53);
      this.grpRoundRobin.TabIndex = 10;
      this.grpRoundRobin.TabStop = false;
      // 
      // lblRoundRobinOutcome
      // 
      this.lblRoundRobinOutcome.Location = new System.Drawing.Point(6, 38);
      this.lblRoundRobinOutcome.Name = "lblRoundRobinOutcome";
      this.lblRoundRobinOutcome.Size = new System.Drawing.Size(238, 15);
      this.lblRoundRobinOutcome.TabIndex = 2;
      this.lblRoundRobinOutcome.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // cmbRoundRobin
      // 
      this.cmbRoundRobin.DataSource = this.bndsrcRoundRobinType;
      this.cmbRoundRobin.DisplayMember = "Name";
      this.cmbRoundRobin.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbRoundRobin.Enabled = false;
      this.cmbRoundRobin.FormattingEnabled = true;
      this.cmbRoundRobin.Location = new System.Drawing.Point(130, 12);
      this.cmbRoundRobin.Name = "cmbRoundRobin";
      this.cmbRoundRobin.Size = new System.Drawing.Size(115, 21);
      this.cmbRoundRobin.TabIndex = 2;
      this.cmbRoundRobin.ValueMember = "Index";
      this.cmbRoundRobin.SelectedIndexChanged += new System.EventHandler(this.cmbRoundRobin_SelectedIndexChanged);
      // 
      // bndsrcRoundRobinType
      // 
      this.bndsrcRoundRobinType.DataSource = typeof(SIDLibraries.BusinessLayer.RoundRobinType);
      // 
      // chbRoundRobin
      // 
      this.chbRoundRobin.AutoSize = true;
      this.chbRoundRobin.Enabled = false;
      this.chbRoundRobin.Location = new System.Drawing.Point(7, 14);
      this.chbRoundRobin.Name = "chbRoundRobin";
      this.chbRoundRobin.Size = new System.Drawing.Size(89, 17);
      this.chbRoundRobin.TabIndex = 1;
      this.chbRoundRobin.Text = "Round Robin";
      this.chbRoundRobin.UseVisualStyleBackColor = true;
      this.chbRoundRobin.CheckedChanged += new System.EventHandler(this.chbRoundRobin_CheckedChanged);
      // 
      // lblMaxWinDisplay
      // 
      this.lblMaxWinDisplay.Location = new System.Drawing.Point(146, 51);
      this.lblMaxWinDisplay.Name = "lblMaxWinDisplay";
      this.lblMaxWinDisplay.Size = new System.Drawing.Size(120, 23);
      this.lblMaxWinDisplay.TabIndex = 1004;
      this.lblMaxWinDisplay.Text = "Max To Win";
      this.lblMaxWinDisplay.TextAlign = System.Drawing.ContentAlignment.TopRight;
      // 
      // lblMaxRiskDisplay
      // 
      this.lblMaxRiskDisplay.Location = new System.Drawing.Point(12, 51);
      this.lblMaxRiskDisplay.Name = "lblMaxRiskDisplay";
      this.lblMaxRiskDisplay.Size = new System.Drawing.Size(120, 23);
      this.lblMaxRiskDisplay.TabIndex = 1003;
      this.lblMaxRiskDisplay.Text = "Max Risk";
      this.lblMaxRiskDisplay.TextAlign = System.Drawing.ContentAlignment.TopRight;
      // 
      // FrmPlaceTeaser
      // 
      this.AcceptButton = this.btnPlaceBet;
      this.AccessibleDescription = "Teaser";
      this.AccessibleName = "Teaser";
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(997, 151);
      this.ControlBox = false;
      this.Controls.Add(this.lblMaxWinDisplay);
      this.Controls.Add(this.lblMaxRiskDisplay);
      this.Controls.Add(this.grpRoundRobin);
      this.Controls.Add(this.txtToWin);
      this.Controls.Add(this.txtRisk);
      this.Controls.Add(this.dgvwPlaceTeaser);
      this.Controls.Add(this.txtSportsSpecsInfo);
      this.Controls.Add(this.btnPayTable);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.btnPlaceBet);
      this.Controls.Add(this.lblWin);
      this.Controls.Add(this.lblRisk);
      this.Controls.Add(this.nudTeams);
      this.Controls.Add(this.lblTeams);
      this.Controls.Add(this.cmbTeaser);
      this.Controls.Add(this.lblTeaser);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.Name = "FrmPlaceTeaser";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmPlaceTeaser_FormClosing);
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmPlaceTeaser_FormClosed);
      this.Load += new System.EventHandler(this.frmPlaceTeaser_Load);
      ((System.ComponentModel.ISupportInitialize)(this.CustomerTeasersBindingSource)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudTeams)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dgvwPlaceTeaser)).EndInit();
      this.ctxModifyWager.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.bndsrc)).EndInit();
      this.grpRoundRobin.ResumeLayout(false);
      this.grpRoundRobin.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.bndsrcRoundRobinType)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label lblTeaser;
    private System.Windows.Forms.ComboBox cmbTeaser;
    private System.Windows.Forms.Label lblTeams;
    private System.Windows.Forms.NumericUpDown nudTeams;
    private System.Windows.Forms.Label lblRisk;
    private System.Windows.Forms.Label lblWin;
    private System.Windows.Forms.Button btnPlaceBet;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.Button btnPayTable;
    private System.Windows.Forms.TextBox txtSportsSpecsInfo;
    private System.Windows.Forms.DataGridView dgvwPlaceTeaser;
    private System.Windows.Forms.DataGridViewTextBoxColumn wagerTypeDataGridViewTextBoxColumn;
    private System.Windows.Forms.BindingSource bndsrc;
    private System.Windows.Forms.DataGridViewTextBoxColumn Type;
    private System.Windows.Forms.DataGridViewTextBoxColumn descriptionAndAdjustedPriceDataGridViewTextBoxColumn;
    private System.Windows.Forms.BindingSource CustomerTeasersBindingSource;
    private GUILibraries.Controls.NumberTextBox txtRisk;
    private GUILibraries.Controls.NumberTextBox txtToWin;
    private System.Windows.Forms.DataGridViewTextBoxColumn wagerNumber;
    private System.Windows.Forms.DataGridViewTextBoxColumn itemNumber;
    private System.Windows.Forms.DataGridViewTextBoxColumn typeNameDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn descriptionAndAdjustedPriceDataGridViewTextBoxColumn1;
    private System.Windows.Forms.DataGridViewTextBoxColumn descriptionDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn typeCodeDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn typeDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn selectedRowDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn selectedCellDataGridViewTextBoxColumn;
    private System.Windows.Forms.ContextMenuStrip ctxModifyWager;
    private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
    private System.Windows.Forms.DataGridViewTextBoxColumn itemNumberDataGridViewTextBoxColumn;
    private System.Windows.Forms.GroupBox grpRoundRobin;
    private System.Windows.Forms.Label lblRoundRobinOutcome;
    private System.Windows.Forms.ComboBox cmbRoundRobin;
    private System.Windows.Forms.CheckBox chbRoundRobin;
    private System.Windows.Forms.BindingSource bndsrcRoundRobinType;
    private System.Windows.Forms.Label lblMaxWinDisplay;
    private System.Windows.Forms.Label lblMaxRiskDisplay;
    private System.Windows.Forms.DataGridViewTextBoxColumn selectedRowDataGridViewTextBoxColumn1;
    private System.Windows.Forms.DataGridViewTextBoxColumn TypeName;
    private System.Windows.Forms.DataGridViewTextBoxColumn descriptionAndAdjustedPriceDataGridViewTextBoxColumn2;
    private System.Windows.Forms.DataGridViewTextBoxColumn selectedCellDataGridViewTextBoxColumn1;
    private System.Windows.Forms.DataGridViewTextBoxColumn wagerNumberDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn Number;
    private System.Windows.Forms.DataGridViewTextBoxColumn selectedOddsDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn selectedBPointsOptionDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn selectedAmountWageredDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn selectedToWinAmountDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn GameNumber;
    private System.Windows.Forms.DataGridViewTextBoxColumn WagerTypeMode;
    private System.Windows.Forms.DataGridViewTextBoxColumn PriceType;
    private System.Windows.Forms.DataGridViewTextBoxColumn PeriodNumber;
    private System.Windows.Forms.DataGridViewTextBoxColumn rowGroupNum;
    private System.Windows.Forms.DataGridViewTextBoxColumn periodWagerCutoff;
    private System.Windows.Forms.DataGridViewTextBoxColumn WageringMode;
    private System.Windows.Forms.DataGridViewTextBoxColumn LayoffWager;
    private System.Windows.Forms.DataGridViewCheckBoxColumn FixedPrice;
    private System.Windows.Forms.DataGridViewTextBoxColumn Pitcher1MStart;
    private System.Windows.Forms.DataGridViewTextBoxColumn Pitcher2MStart;
    private System.Windows.Forms.DataGridViewTextBoxColumn TotalPointsOu;
    private System.Windows.Forms.DataGridViewTextBoxColumn SelectedAmericanPrice;
    private System.Windows.Forms.DataGridViewTextBoxColumn PeriodDescription;
    private System.Windows.Forms.DataGridViewTextBoxColumn AlreadyBetItemCount;
    private System.Windows.Forms.DataGridViewTextBoxColumn TicketNumber;
  }
}
