﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using GUILibraries.BusinessLayer;
using GUILibraries.Forms;
using GUILibraries.Utilities;
using TicketWriter.Properties;
using SIDLibraries.Entities;
using SIDLibraries.BusinessLayer;
using GUILibraries.Controls;

namespace TicketWriter.UI {
  public sealed partial class FrmPlaceContestOrProp : SIDForm {
    #region Public Properties

    public String FrmMode { private get; set; }

    private FrmSportAndGameSelection FrmSportAndGameSelection {
      get {
        return _frmSportAndGameSelection ??
               (_frmSportAndGameSelection =
                (FrmSportAndGameSelection)FormF.GetFormByName("FrmSportAndGameSelection", this));
      }
    }

    public Boolean IsFreePlay { private get; set; }

    private MdiTicketWriter Mdi {
      get {
        return _mdiTicketWriter ??
               (_mdiTicketWriter = (MdiTicketWriter)FormF.GetFormByName("MdiTicketWriter", this));
      }
    }

    public int WagerItemNumberToUpd { private get; set; }

    public int WagerNumberToUpd { private get; set; }

    public int TicketNumberToUpd { private get; set; }

    public String PreviouslySelectedAmtWagered { private get; set; }

    public String PreviouslySelectedOdds { private get; set; }

    public String PreviouslySelectedToWinAmt { private get; set; }

    public String PreviouslySelectedDescription { get; set; }

    #endregion

    #region Private Properties

    public String WagerTypeMode { get; private set; }

    #endregion

    #region Public vars

    private List<Ticket.SelectedContestAndLine> OriginalContestsAndPeriodsInfo {
      get {
        if (Mdi.Ticket == null) return null;
        var list = Mdi.Ticket.OriginalContestOptions;

        return list;
      }
    }

    #endregion

    #region Private vars
    private readonly List<spCnGetActiveContests_Result> _activeContests;
    private readonly int _contestNumber;
    private readonly int _contestantNumber;
    private String _contestShortDescription;
    private readonly String _contestant;
    private readonly String _currentWagerTypeName;
    private readonly String _customerHasStaticLines;
    public FrmSportAndGameSelection _frmSportAndGameSelection;
    private MdiTicketWriter _mdiTicketWriter;
    public spCnGetActiveContests_Result SelectedContest;
    public double SelectedAdjustment;
    private readonly String _selectedPrice;

    #endregion

    #region Structures

    #endregion

    #region Constructors

    public FrmPlaceContestOrProp(String wagerTypeName, FrmSportAndGameSelection parentForm, IEnumerable<spCnGetActiveContests_Result> activeContests, int contestNumber, int contestantNumber, String contestant, String priceType, int? percentBook)
      : base(parentForm.AppModuleInfo) {
      if (percentBook == null) throw new ArgumentNullException("percentBook");
      _currentWagerTypeName = wagerTypeName;
      _activeContests = activeContests.OrderBy(x => x.ContestType).ThenBy(x => x.RotNum).ToList();
      _contestNumber = contestNumber;
      _contestantNumber = contestantNumber;
      _contestant = contestant;
      _selectedPrice = priceType;
      _customerHasStaticLines = parentForm.StaticLinesDuringCall;
      _frmSportAndGameSelection = parentForm;
      InitializeComponent();
      SetCentsNumericInputs();
    }

    #endregion

    #region Public Methods

    #endregion

    #region Private Methods

    private void FillWageringForm(Boolean isEditMode) {
      if (isEditMode) {
        if (_customerHasStaticLines == "N" || Mdi.TicketNumber != TicketNumberToUpd) {
          if (_activeContests != null) {
            SelectedContest = (from p in _activeContests where p.ContestantNum == _contestantNumber select p).FirstOrDefault();
          }
        }
        else {
          Ticket.SelectedContestAndLine selectedContestInfo = null;

          if (OriginalContestsAndPeriodsInfo != null)
            selectedContestInfo = (from gO in OriginalContestsAndPeriodsInfo
                                   where
                                       gO.ContestNum == _contestNumber &&
                                       gO.TicketNumber == TicketNumberToUpd &&
                                       gO.WagerNumber == WagerNumberToUpd &&
                                       gO.ItemNumber == WagerItemNumberToUpd
                                   select gO).FirstOrDefault();

          if (selectedContestInfo != null) {
            SelectedContest = TwUtilities.ConvertToContestInfoObject(selectedContestInfo);
          }
        }
      }
      else {
        if (_activeContests != null) {
          SelectedContest = (from p in _activeContests where p.ContestantNum == _contestantNumber select p).FirstOrDefault();
        }
      }

      txtContestOdds.Text = (SelectedContest.ContestantName ?? "").Trim();
      txtContestOdds.Text += Environment.NewLine;

      if (SelectedContest == null) return;
      var formattedMoneyLine = FormatNumber(SelectedContest.MoneyLine ?? 0, false);
      Text = SelectedContest.ContestDesc.Trim();

      if (SelectedContest.XtoYLineRep == "N") {
        if (SelectedContest.MoneyLine == null)
          return;
        SelectedAdjustment = (double)SelectedContest.MoneyLine;

        switch (_selectedPrice) {
          case "American":
          case LineOffering.PRICE_AMERICAN:
            if (SelectedContest.MoneyLine > 0) {
              txtContestOdds.Text += @"+" + formattedMoneyLine;
            }
            else {
              txtContestOdds.Text += formattedMoneyLine;
            }
            break;
          case "Decimal":
          case LineOffering.PRICE_DECIMAL:
            if (SelectedContest.DecimalOdds != null) {
              txtContestOdds.Text += SelectedContest.DecimalOdds;
            }
            break;
          case "Fractional":
          case LineOffering.PRICE_FRACTIONAL:
            if (SelectedContest.Numerator != null && SelectedContest.Denominator != null) {
              txtContestOdds.Text += SelectedContest.Numerator + @"/" + SelectedContest.Denominator;
            }
            break;
        }
      }
      else {
        if (SelectedContest.MoneyLine == null) return;
        SelectedAdjustment = (double)SelectedContest.MoneyLine;
        txtContestOdds.Text += formattedMoneyLine + @" to " + SelectedContest.ToBase;
      }

      btnGoToGame.Visible = _currentWagerTypeName == "Straight Bet";
      txtRisk.Visible = _currentWagerTypeName == "Straight Bet";
      txtToWin.Visible = _currentWagerTypeName == "Straight Bet";
      lblMaxRiskDisplay.Visible = _currentWagerTypeName == "Straight Bet";
      lblMaxWinDisplay.Visible = _currentWagerTypeName == "Straight Bet";
      lblWagerAmount.Visible = _currentWagerTypeName == "Straight Bet";
      lblToWin.Visible = _currentWagerTypeName == "Straight Bet";

      if (_currentWagerTypeName == "Parlay") {
        btnPlaceBet.Text = @"Add To Parlay";
      }
    }

    private void GoToGame() {
      if (SelectedContest != null && SelectedContest.CorrelationID != null) {
        var correlationId = SelectedContest.CorrelationID.Trim();
        var gamesCnt = 0;
        var sportType = "";
        var sportSubType = "";
        DateTime? gameDateTime = null;
        var startDatePart = "";
        int? gameNumber = null;
        if (FrmSportAndGameSelection.ActiveGames != null) {
          var gamesInfo = (from a in FrmSportAndGameSelection.ActiveGames where a.CorrelationID.Trim() == correlationId && a.PeriodNumber == 0 select new { a.GameNum, a.SportType, a.SportSubType, a.GameDateTime }).FirstOrDefault();
          if (gamesInfo != null) {
            gamesCnt = 1;
            sportType = gamesInfo.SportType;
            sportSubType = gamesInfo.SportSubType;
            gameDateTime = gamesInfo.GameDateTime;
            gameNumber = gamesInfo.GameNum;
          }
        }

        if (gamesCnt > 0) {
          var gamesGvw = (DataGridView)FrmSportAndGameSelection.Controls.Find(FrmSportAndGameSelection.GamesGridViewInUse, true).FirstOrDefault();
          var contestsGvw = (DataGridView)FrmSportAndGameSelection.Controls.Find(FrmSportAndGameSelection.PropsGridViewInUse, true).FirstOrDefault();

          if (gamesGvw != null) {
            if (gamesGvw.Rows.Count > 0)
              gamesGvw.Rows.Clear();
            gamesGvw.Visible = true;
          }
          else
            return;

          if (contestsGvw != null) {
            if (contestsGvw.Rows.Count > 0)
              contestsGvw.Rows.Clear();
            contestsGvw.Visible = false;
          }
          Close();
          var lblCurrentlyShowing = (Label)FrmSportAndGameSelection.Controls.Find("lblCurrentlyShowing", true).FirstOrDefault();
          if (lblCurrentlyShowing != null) {
            var startDt = ServerDateTime;
            lblCurrentlyShowing.Text = @"==>>";
            lblCurrentlyShowing.Text += sportType.Trim();
            lblCurrentlyShowing.Text += @"==>>";
            if (gameDateTime != null) {
              startDt = (DateTime)gameDateTime;
              var month = startDt.Month;
              var day = startDt.Day;
              startDatePart = month < 10 ? "0" + month : month.ToString(CultureInfo.InvariantCulture);
              startDatePart += "/";
              startDatePart += day < 10 ? "0" + day : day.ToString(CultureInfo.InvariantCulture);
              lblCurrentlyShowing.Text += startDatePart;
            }
            lblCurrentlyShowing.Text += @"==>>";
            lblCurrentlyShowing.Text += sportSubType.Trim();

            if (FrmSportAndGameSelection.CurrSelectedSportBranch != null && FrmSportAndGameSelection.CurrSelectedSportBranch.Count > 0) {
              FrmSportAndGameSelection.CurrSelectedSportBranch.RemoveAt(0);
            }

            if (FrmSportAndGameSelection.CurrSelectedSportBranch == null)
              FrmSportAndGameSelection.CurrSelectedSportBranch = new List<FrmSportAndGameSelection.SelectedSportBranch>();
            FrmSportAndGameSelection.CurrSelectedSportBranch.Add(new FrmSportAndGameSelection.SelectedSportBranch { StartDatePart = startDatePart, EndDatePart = startDatePart, StartDateTime = startDt, EndDateTime = startDt, SportSubType = sportSubType, PeriodNumber = 0, SportType = sportType, GameNumber = gameNumber });
          }

          FrmSportAndGameSelection.FillGamesGridView(FrmSportAndGameSelection.CurrSelectedSportBranch, null, 0, null, correlationId);
        }
        else {
          MessageBox.Show(@"No correlated games found.");
        }
      }
      else {
        MessageBox.Show(@"No correlated games found.");
      }
    }

    private void GoToPlaceBet() {
      int ticketNumberForWi = FrmMode == "Edit" ? TicketNumberToUpd : Mdi.TicketNumber;
      int wagerNumberForWi = FrmMode == "Edit" ? WagerNumberToUpd : 1;
      int itemNumberForcWi = 1;
      var frmPlaceParlay = (FrmPlaceParlay)FormF.GetFormByName("frmPlaceParlay", this);
      if (frmPlaceParlay != null)
        frmPlaceParlay.ItemFromMakeAWagerForm = true;

      if (_contestShortDescription == null)
        _contestShortDescription = TwUtilities.FormatShortContestDescription(SelectedContest, _selectedPrice, SelectedAdjustment.ToString(CultureInfo.InvariantCulture), SelectedContest.ThresholdLine.ToString(), SelectedContest.ToBase.ToString(), _currentWagerTypeName);

      if (_currentWagerTypeName == "Straight Bet") {
        FrmSportAndGameSelection.CreateStageWager(FrmMode, ticketNumberForWi, WagerNumberToUpd, out wagerNumberForWi, txtRisk, txtToWin, _currentWagerTypeName, WagerType.CONTEST, "", "", 1, 0, null, null, null, "", null, "", IsFreePlay, "N", _contestShortDescription);
      }

      if (_currentWagerTypeName == "Parlay") {
        if (frmPlaceParlay == null)
          throw new Exception("Unable to find Parlay Form");
        var parlay = frmPlaceParlay.Parlay;
        if (parlay == null) {
          parlay = FrmSportAndGameSelection.CreateParlayWager(frmPlaceParlay, FrmMode, ticketNumberForWi, WagerNumberToUpd, txtRisk, txtToWin, _currentWagerTypeName, WagerType.MONEYLINE, IsFreePlay, "N");

          if (parlay == null) {
            return;
          }
        }
        ticketNumberForWi = parlay.TicketNumber;
        wagerNumberForWi = parlay.Number ?? 0;
        itemNumberForcWi = parlay.Items.Count;

      }
      Ticket.ContestWagerItem cWi;
      string itemShortDescription;
      if (FrmMode == "Edit") {
        cWi = FrmSportAndGameSelection.CreateStageContestWagerItem(FrmMode, TicketNumberToUpd, WagerNumberToUpd, WagerItemNumberToUpd, double.Parse(String.IsNullOrEmpty(txtRisk.Text) ? "0" : txtRisk.Text), SelectedContest, _selectedPrice, _contestant, "A", "N", SelectedAdjustment);
        if (Mdi.Ticket.ContestWagerItems != null && Mdi.Ticket.ContestWagerItems.Any(w => w.TicketNumber == cWi.TicketNumber && w.WagerNumber == cWi.WagerNumber)) {
          FrmSportAndGameSelection.UpdateContestWagerItem(cWi);

          if (_currentWagerTypeName == "Parlay") {
            var item = new ParlayItem();
            FrmSportAndGameSelection.FillParlayContestItem(item, cWi, _selectedPrice, txtRisk, txtToWin, _contestShortDescription, WagerTypeMode, "A", "N");

            if (frmPlaceParlay.Parlay.Items.Any(pwi => pwi.TicketNumber == cWi.TicketNumber && pwi.WagerNumber == cWi.WagerNumber && pwi.Number == cWi.ItemNumber)) {
              frmPlaceParlay.UpdateItem(item);
            }
            else {
              frmPlaceParlay.AddItem(item);
            }
            var wagerItem = FrmSportAndGameSelection.CreateStageWagerItemFromContestItem(FrmMode, _currentWagerTypeName, "M", WagerTypeMode,
                                                _selectedPrice, cWi.TicketNumber, cWi.WagerNumber,
                                                cWi.ItemNumber, SelectedAdjustment.ToString(CultureInfo.InvariantCulture),
                                                0, "", _contestShortDescription,
                                                cWi,
                                                out itemShortDescription, IsFreePlay, cWi.WageringMode, cWi.LayoffFlag);

            if (Mdi.Ticket.WagerItems != null && Mdi.Ticket.WagerItems.Any(w => w.TicketNumber == cWi.TicketNumber && w.WagerNumber == cWi.WagerNumber && w.IsContest)) {
              Mdi.Ticket.UpdateWagerItem(wagerItem);
            }
            else {
              Mdi.Ticket.AddWagerItem(wagerItem);
            }
          }
        }
        else {
          FrmSportAndGameSelection.AddStageContestWagerItem(cWi, SelectedContest);
          if (_currentWagerTypeName == "Parlay") {
            var item = new ParlayItem();
            FrmSportAndGameSelection.FillParlayContestItem(item, cWi, _selectedPrice, txtRisk, txtToWin, _contestShortDescription, WagerTypeMode, "A", "N");
            FrmSportAndGameSelection.ParlayOrTeaserItemFromSb = false;
            var wagerItem = FrmSportAndGameSelection.CreateStageWagerItemFromContestItem(FrmMode, _currentWagerTypeName, "M", WagerTypeMode,
        _selectedPrice, ticketNumberForWi, wagerNumberForWi,
        itemNumberForcWi, SelectedAdjustment.ToString(CultureInfo.InvariantCulture),
        0, "", _contestShortDescription,
        cWi,
        out itemShortDescription, IsFreePlay, "A", "N");

            Mdi.Ticket.AddWagerItem(wagerItem);
            frmPlaceParlay.AddItem(item);
          }
        }
      }
      else {
        cWi = FrmSportAndGameSelection.CreateStageContestWagerItem(FrmMode, ticketNumberForWi, wagerNumberForWi, null, double.Parse(String.IsNullOrEmpty(txtRisk.Text) ? "0" : txtRisk.Text), SelectedContest,
            _selectedPrice, _contestant, "A", "N", SelectedAdjustment);
        if (_currentWagerTypeName == "Parlay") {
          cWi.ItemNumber = itemNumberForcWi + 1;
        }
        FrmSportAndGameSelection.AddStageContestWagerItem(cWi, SelectedContest);
        if (_currentWagerTypeName == "Parlay") {
          var item = new ParlayItem();
          FrmSportAndGameSelection.FillParlayContestItem(item, cWi, _selectedPrice, txtRisk, txtToWin, _contestShortDescription, WagerTypeMode, "A", "N");
          FrmSportAndGameSelection.ParlayOrTeaserItemFromSb = false;
          var wagerItem = FrmSportAndGameSelection.CreateStageWagerItemFromContestItem(FrmMode, _currentWagerTypeName, "M", WagerTypeMode,
          _selectedPrice, ticketNumberForWi, wagerNumberForWi,
          itemNumberForcWi, SelectedAdjustment.ToString(CultureInfo.InvariantCulture),
          0, "", _contestShortDescription,
          cWi,
          out itemShortDescription, IsFreePlay, "A", "N");

          Mdi.Ticket.AddWagerItem(wagerItem);

          frmPlaceParlay.AddItem(item);
        }
      }
      if (_currentWagerTypeName == "Straight Bet") {
        FrmSportAndGameSelection.BuildReadbackLogInfoData(true);
        FrmSportAndGameSelection.PopulateReadbackLogInfoDgvw();
      }
      Close();
    }

    private bool IsValidWager() {
      var isValidWager = true;

      if (((txtRisk.Text.Length == 0 || txtToWin.Text.Length == 0) && _currentWagerTypeName == "Straight Bet")) {
        MessageBox.Show(Resources.FrmMakeAWager_IsValidWager_A_wager_amount_must_be_entered_, Resources.FrmMakeAWager_IsValidWager_Invalid_Amount);
        isValidWager = false;
      }
      return isValidWager;
    }

    private void ValidateContestWagerItem() {

      _contestShortDescription = TwUtilities.FormatShortContestDescription(SelectedContest, _selectedPrice, SelectedAdjustment.ToString(CultureInfo.InvariantCulture), SelectedContest.ThresholdLine.ToString(), SelectedContest.ToBase.ToString(), _currentWagerTypeName);

      if (IsValidWager()) {
        var gameStatus = SelectedContest.Status;
        var cutoffTime = SelectedContest.WagerCutoff.GetValueOrDefault();

        if (gameStatus == null) return;
        switch (gameStatus) {
          case GamesAndLines.EVENT_COMPLETED: //Complete
          case GamesAndLines.EVENT_OFFLINE: //Offline
          case GamesAndLines.EVENT_CANCELLED: //Cancelled?
            MessageBox.Show(@"The contest has already started or is not currently available", @"Not available");
            txtToWin.Focus();
            return;
          case GamesAndLines.EVENT_CIRCLED: //Circled
          case GamesAndLines.EVENT_OPEN: //Open
            bool continueValidating;
            bool cutoffTimeBypassed;
            FrmSportAndGameSelection.CheckIfContestWagerCutoffHasBeenReached(this, LoginsAndProfiles.ACCEPT_ANY_WAGER, SelectedContest, "Contest: " + _contestShortDescription, txtRisk.Text, txtToWin.Text, cutoffTime, out continueValidating, out cutoffTimeBypassed);
            if (!continueValidating)
              return;
            bool minWagerBypassed = true;
            if (_currentWagerTypeName == "Straight Bet") {
              FrmSportAndGameSelection.CheckIfBelowMinimumWager(this, LoginsAndProfiles.ACCEPT_ANY_WAGER, txtRisk.Text, out continueValidating, out minWagerBypassed);
              if (!continueValidating)
                return;
            }
            bool availableFundsOk = true;
            if (_currentWagerTypeName == "Straight Bet") {
              FrmSportAndGameSelection.CheckForAvailableFunds(this, FrmMode, TicketNumberToUpd, WagerNumberToUpd, WagerItemNumberToUpd, LoginsAndProfiles.ACCEPT_ANY_WAGER, null, SelectedContest, _contestShortDescription, txtRisk.Text, txtToWin.Text, IsFreePlay, out continueValidating, out availableFundsOk); //Validate if user has sufficientFunds for wager
              if (!continueValidating)
                return;
            }
            bool wagerLimitOk = true;
            if (_currentWagerTypeName == "Straight Bet") {
              FrmSportAndGameSelection.CheckIfExceedsContestWagerLimit(this, FrmMode, TicketNumberToUpd, WagerNumberToUpd, WagerItemNumberToUpd, LoginsAndProfiles.ACCEPT_ANY_WAGER, SelectedContest, _contestShortDescription, txtRisk.Text, txtToWin.Text, out continueValidating, out wagerLimitOk);
              if (!continueValidating)
                return;
            }
            if (cutoffTimeBypassed && minWagerBypassed && availableFundsOk && wagerLimitOk) {
              GoToPlaceBet();
              Close();
            }

            break;
        }
      }
      else {
        txtToWin.Focus();
      }
    }

    private void SetCentsNumericInputs() {
      txtRisk.AllowCents = Params.IncludeCents;
      txtToWin.AllowCents = Params.IncludeCents;
    }

    #endregion

    #region Events

    private void btnGoToGame_Click(object sender, EventArgs e) {
      GoToGame();
    }

    private void btnPlaceBet_Click(object sender, EventArgs e) {
      ValidateContestWagerItem();
    }

    private void FrmPlaceContestOrProp_Load(object sender, EventArgs e) {
      if (FrmSportAndGameSelection.WageringDisabled) {
        MessageBox.Show(@"Wagering is not allowed for this account!");
        Close();
      }
      if (FrmMode == "Edit") {
        btnPlaceBet.Text = @"Update Prop item";
        txtRisk.Text = PreviouslySelectedAmtWagered;
        txtToWin.Text = PreviouslySelectedToWinAmt;
        SelectedAdjustment = double.Parse(PreviouslySelectedOdds);
      }
      FillWageringForm(FrmMode == "Edit");
    }

    private void FrmPlaceContestOrProp_Shown(object sender, EventArgs e) {
      double moneyLine;
      switch (_selectedPrice) {
        case "American":
        case LineOffering.PRICE_AMERICAN:
          moneyLine = SelectedContest.MoneyLine ?? 0;
          if (moneyLine > 0) {
            txtRisk.Focus();
            WagerTypeMode = "Risk";
          }
          else {
            txtToWin.Focus();
            WagerTypeMode = "ToWin";
          }

          break;
        case "Decimal":
        case LineOffering.PRICE_DECIMAL:
        case "Fractional":
        case LineOffering.PRICE_FRACTIONAL:
          moneyLine = SelectedContest.DecimalOdds ?? 0;

          if (moneyLine > 2) {
            txtRisk.Focus();
            WagerTypeMode = "Risk";
          }
          else {
            txtToWin.Focus();
            WagerTypeMode = "ToWin";
          }
          break;
      }
      if (!_frmSportAndGameSelection.ShowWagerLimitsInFrm.IsActive()) return;
      _frmSportAndGameSelection.ShowWagerLimitsInFrm.FrmPlaceContestOrProp = this;
      _frmSportAndGameSelection.ShowWagerLimitsInFrm.DisplayMaxContestWagerLimits();
    }

    private void txtRisk_KeyUp(object sender, KeyEventArgs e) {
      if (((NumberTextBox)sender).Text != "" && KeyValidator.IsValidNumericKey(e)) {
        txtToWin.Text = SelectedContest.XtoYLineRep == "Y" ? FormatNumber(TwUtilities.DetermineXtoYToWinAmount(double.Parse(((NumberTextBox)sender).Text), SelectedAdjustment, SelectedContest.ToBase.GetValueOrDefault())) : LineOffering.DetermineToWinAmount(double.Parse(((NumberTextBox)sender).Text), SelectedAdjustment, Params.IncludeCents).ToString(CultureInfo.InvariantCulture);
      }
      else {
        txtToWin.Text = "";
      }
    }

    private void txtToWin_KeyUp(object sender, KeyEventArgs e) {
      if (((NumberTextBox)sender).Text != "" && KeyValidator.IsValidNumericKey(e)) {
        if (SelectedContest.XtoYLineRep == "Y") {
          txtToWin.Text = TwUtilities.DetermineXtoYRiskAmount(double.Parse(((NumberTextBox)sender).Text), SelectedAdjustment, SelectedContest.ToBase.GetValueOrDefault()).ToString(CultureInfo.InvariantCulture);
        }
        else {
          txtRisk.Text = LineOffering.DetermineRiskAmount(double.Parse(((NumberTextBox)sender).Text), SelectedAdjustment, Params.IncludeCents).ToString(CultureInfo.InvariantCulture);
        }
      }
      else {
        txtRisk.Text = "";
      }
    }

    #endregion

    private void FrmPlaceContestOrProp_FormClosing(object sender, FormClosingEventArgs e) {
      Tag = "Added " + WagerType.MONEYLINENAME + " item to " + _currentWagerTypeName + " in " + FrmMode + " mode. " + _frmSportAndGameSelection.Customer.CustomerID.Trim() + " " + TicketNumberToUpd.ToString() + "." + WagerNumberToUpd.ToString() + "." + WagerItemNumberToUpd.ToString() + "(" + _contestShortDescription + ")"
          + (_currentWagerTypeName == "Straight Bet" ? " Risking: " + txtRisk.Text + ", to Win:" + txtToWin.Text : "");
    }

  }
}