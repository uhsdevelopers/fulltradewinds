﻿using System.Windows.Forms;
using GUILibraries.Utilities;
using System;

namespace TicketWriter.UI {
  public static class WageringForm {
    public static void Close(Form frm, String frmMode = "New", int? ticketNumber = null, int? wagerNumber = null, bool closeForm = true) {
      try {
        ((MdiTicketWriter) frm.Owner).SetWageringButtonsAppereance(true);
        ((MdiTicketWriter) frm.Owner).SetWageringToolStripsAppereance(true);
        var frmMakeAWager = (FrmMakeAWager) FormF.GetFormByName("FrmMakeAWager", frm);
        var frmSportAndGameSelection = (FrmSportAndGameSelection) FormF.GetFormByName("FrmSportAndGameSelection", frm);

        if (frmSportAndGameSelection != null) {
          frmSportAndGameSelection.RemoveOwnedForm(frmMakeAWager);
        }
        if (wagerNumber != null) {
          var mdiTicketWriter = (MdiTicketWriter) FormF.GetFormByName("MdiTicketWriter", frm);
          if (mdiTicketWriter != null) {
            if (mdiTicketWriter.Ticket != null && frmMode == "Edit" && ticketNumber == mdiTicketWriter.TicketNumber) {
              mdiTicketWriter.Ticket.DeleteWager(ticketNumber, wagerNumber);
            }
          }
        }
        if (closeForm) {
          frm.Close();
        }
      }
      catch {
        // ignored
      }
    }

  }
}