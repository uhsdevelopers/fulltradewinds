﻿namespace TicketWriter.UI {
  partial class FrmPlaceParlay {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
      this.components = new System.ComponentModel.Container();
      this.dgvwParlays = new System.Windows.Forms.DataGridView();
      this.ctxModifyWager = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.bndsrc = new System.Windows.Forms.BindingSource(this.components);
      this.lblTeams = new System.Windows.Forms.Label();
      this.nudTeams = new System.Windows.Forms.NumericUpDown();
      this.lblRisk = new System.Windows.Forms.Label();
      this.lblWin = new System.Windows.Forms.Label();
      this.txtToWin = new GUILibraries.Controls.NumberTextBox();
      this.btnPlaceBet = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.btnPayTable = new System.Windows.Forms.Button();
      this.grpRoundRobin = new System.Windows.Forms.GroupBox();
      this.lblRoundRobinOutcome = new System.Windows.Forms.Label();
      this.cmbRoundRobin = new System.Windows.Forms.ComboBox();
      this.bndsrcRoundRobinType = new System.Windows.Forms.BindingSource(this.components);
      this.chbRoundRobin = new System.Windows.Forms.CheckBox();
      this.txtRisk = new GUILibraries.Controls.NumberTextBox();
      this.lblMaxRiskDisplay = new System.Windows.Forms.Label();
      this.lblMaxRiskToMaxPayout = new System.Windows.Forms.Label();
      this.lblMaxPayout = new System.Windows.Forms.Label();
      this.TypeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.descriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.SelectedRow = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.SelectedCell = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.WagerNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Number = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.SelectedOdds = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.SelectedBPointsOption = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.SelectedAmountWagered = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.SelectedToWinAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.WagerTypeMode = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.PriceType = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.GameNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.PeriodNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.rowGroupNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.periodWagerCutoff = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.WageringMode = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.LayoffWager = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.FixedPrice = new System.Windows.Forms.DataGridViewCheckBoxColumn();
      this.Pitcher1MStart = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Pitcher2MStart = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.TotalPointsOu = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.SelectedAmericanPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.PeriodDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.AlreadyBetItemCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.TicketNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
      ((System.ComponentModel.ISupportInitialize)(this.dgvwParlays)).BeginInit();
      this.ctxModifyWager.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.bndsrc)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudTeams)).BeginInit();
      this.grpRoundRobin.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.bndsrcRoundRobinType)).BeginInit();
      this.SuspendLayout();
      // 
      // dgvwParlays
      // 
      this.dgvwParlays.AllowUserToAddRows = false;
      this.dgvwParlays.AllowUserToDeleteRows = false;
      this.dgvwParlays.AllowUserToResizeRows = false;
      this.dgvwParlays.AutoGenerateColumns = false;
      this.dgvwParlays.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
      this.dgvwParlays.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgvwParlays.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TypeName,
            this.descriptionDataGridViewTextBoxColumn,
            this.SelectedRow,
            this.SelectedCell,
            this.WagerNumber,
            this.Number,
            this.SelectedOdds,
            this.SelectedBPointsOption,
            this.SelectedAmountWagered,
            this.SelectedToWinAmount,
            this.WagerTypeMode,
            this.PriceType,
            this.GameNumber,
            this.PeriodNumber,
            this.rowGroupNum,
            this.periodWagerCutoff,
            this.WageringMode,
            this.LayoffWager,
            this.FixedPrice,
            this.Pitcher1MStart,
            this.Pitcher2MStart,
            this.TotalPointsOu,
            this.SelectedAmericanPrice,
            this.PeriodDescription,
            this.AlreadyBetItemCount,
            this.TicketNumber});
      this.dgvwParlays.ContextMenuStrip = this.ctxModifyWager;
      this.dgvwParlays.DataSource = this.bndsrc;
      this.dgvwParlays.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
      this.dgvwParlays.Location = new System.Drawing.Point(5, 5);
      this.dgvwParlays.MultiSelect = false;
      this.dgvwParlays.Name = "dgvwParlays";
      this.dgvwParlays.ReadOnly = true;
      this.dgvwParlays.RowHeadersVisible = false;
      this.dgvwParlays.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.dgvwParlays.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgvwParlays.ShowEditingIcon = false;
      this.dgvwParlays.Size = new System.Drawing.Size(445, 112);
      this.dgvwParlays.TabIndex = 8;
      this.dgvwParlays.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvwParlays_CellDoubleClick);
      this.dgvwParlays.SelectionChanged += new System.EventHandler(this.dgvwParlays_SelectionChanged);
      this.dgvwParlays.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvwParlays_KeyDown);
      this.dgvwParlays.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgvwParlays_MouseDown);
      // 
      // ctxModifyWager
      // 
      this.ctxModifyWager.Enabled = false;
      this.ctxModifyWager.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editToolStripMenuItem,
            this.deleteToolStripMenuItem});
      this.ctxModifyWager.Name = "ctxModifyWager";
      this.ctxModifyWager.Size = new System.Drawing.Size(108, 48);
      // 
      // editToolStripMenuItem
      // 
      this.editToolStripMenuItem.Enabled = false;
      this.editToolStripMenuItem.Name = "editToolStripMenuItem";
      this.editToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
      this.editToolStripMenuItem.Text = "Edit...";
      this.editToolStripMenuItem.Click += new System.EventHandler(this.EditToolStripMenuItem_Click);
      // 
      // deleteToolStripMenuItem
      // 
      this.deleteToolStripMenuItem.Enabled = false;
      this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
      this.deleteToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
      this.deleteToolStripMenuItem.Text = "Delete";
      this.deleteToolStripMenuItem.Click += new System.EventHandler(this.DeleteToolStripMenuItem_Click);
      // 
      // bndsrc
      // 
      this.bndsrc.DataSource = typeof(SIDLibraries.BusinessLayer.ParlayItem);
      this.bndsrc.ListChanged += new System.ComponentModel.ListChangedEventHandler(this.bndsrc_ListChanged);
      // 
      // lblTeams
      // 
      this.lblTeams.AutoSize = true;
      this.lblTeams.Location = new System.Drawing.Point(452, 9);
      this.lblTeams.Name = "lblTeams";
      this.lblTeams.Size = new System.Drawing.Size(42, 13);
      this.lblTeams.TabIndex = 1;
      this.lblTeams.Text = "Teams:";
      // 
      // nudTeams
      // 
      this.nudTeams.Location = new System.Drawing.Point(493, 6);
      this.nudTeams.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            0});
      this.nudTeams.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
      this.nudTeams.Name = "nudTeams";
      this.nudTeams.Size = new System.Drawing.Size(35, 20);
      this.nudTeams.TabIndex = 1;
      this.nudTeams.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
      this.nudTeams.ValueChanged += new System.EventHandler(this.nudTeams_ValueChanged);
      this.nudTeams.Click += new System.EventHandler(this.nudTeams_Click);
      this.nudTeams.Enter += new System.EventHandler(this.nudTeams_Enter);
      this.nudTeams.KeyUp += new System.Windows.Forms.KeyEventHandler(this.nudTeams_KeyUp);
      // 
      // lblRisk
      // 
      this.lblRisk.AutoSize = true;
      this.lblRisk.Location = new System.Drawing.Point(529, 9);
      this.lblRisk.Name = "lblRisk";
      this.lblRisk.Size = new System.Drawing.Size(31, 13);
      this.lblRisk.TabIndex = 3;
      this.lblRisk.Text = "Risk:";
      // 
      // lblWin
      // 
      this.lblWin.AutoSize = true;
      this.lblWin.Location = new System.Drawing.Point(618, 9);
      this.lblWin.Name = "lblWin";
      this.lblWin.Size = new System.Drawing.Size(29, 13);
      this.lblWin.TabIndex = 5;
      this.lblWin.Text = "Win:";
      // 
      // txtToWin
      // 
      this.txtToWin.AllowCents = false;
      this.txtToWin.AllowNegatives = false;
      this.txtToWin.DividedBy = 1;
      this.txtToWin.DivideFlag = false;
      this.txtToWin.Location = new System.Drawing.Point(647, 6);
      this.txtToWin.Name = "txtToWin";
      this.txtToWin.ReadOnly = true;
      this.txtToWin.ShowIfZero = true;
      this.txtToWin.Size = new System.Drawing.Size(60, 20);
      this.txtToWin.TabIndex = 9;
      // 
      // btnPlaceBet
      // 
      this.btnPlaceBet.Location = new System.Drawing.Point(457, 121);
      this.btnPlaceBet.Name = "btnPlaceBet";
      this.btnPlaceBet.Size = new System.Drawing.Size(75, 23);
      this.btnPlaceBet.TabIndex = 5;
      this.btnPlaceBet.Text = "Place Bet";
      this.btnPlaceBet.UseVisualStyleBackColor = true;
      this.btnPlaceBet.Click += new System.EventHandler(this.btnPlaceBet_Click);
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.Location = new System.Drawing.Point(538, 121);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 6;
      this.btnCancel.Text = "Cancel";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // btnPayTable
      // 
      this.btnPayTable.Location = new System.Drawing.Point(632, 121);
      this.btnPayTable.Name = "btnPayTable";
      this.btnPayTable.Size = new System.Drawing.Size(75, 23);
      this.btnPayTable.TabIndex = 7;
      this.btnPayTable.Text = "Pay Table";
      this.btnPayTable.UseVisualStyleBackColor = true;
      this.btnPayTable.Click += new System.EventHandler(this.btnPayTable_Click);
      // 
      // grpRoundRobin
      // 
      this.grpRoundRobin.Controls.Add(this.lblRoundRobinOutcome);
      this.grpRoundRobin.Controls.Add(this.cmbRoundRobin);
      this.grpRoundRobin.Controls.Add(this.chbRoundRobin);
      this.grpRoundRobin.Location = new System.Drawing.Point(457, 39);
      this.grpRoundRobin.Name = "grpRoundRobin";
      this.grpRoundRobin.Size = new System.Drawing.Size(250, 56);
      this.grpRoundRobin.TabIndex = 3;
      this.grpRoundRobin.TabStop = false;
      // 
      // lblRoundRobinOutcome
      // 
      this.lblRoundRobinOutcome.Location = new System.Drawing.Point(6, 38);
      this.lblRoundRobinOutcome.Name = "lblRoundRobinOutcome";
      this.lblRoundRobinOutcome.Size = new System.Drawing.Size(238, 15);
      this.lblRoundRobinOutcome.TabIndex = 2;
      this.lblRoundRobinOutcome.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // cmbRoundRobin
      // 
      this.cmbRoundRobin.DataSource = this.bndsrcRoundRobinType;
      this.cmbRoundRobin.DisplayMember = "Name";
      this.cmbRoundRobin.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cmbRoundRobin.Enabled = false;
      this.cmbRoundRobin.FormattingEnabled = true;
      this.cmbRoundRobin.Location = new System.Drawing.Point(130, 12);
      this.cmbRoundRobin.Name = "cmbRoundRobin";
      this.cmbRoundRobin.Size = new System.Drawing.Size(115, 21);
      this.cmbRoundRobin.TabIndex = 2;
      this.cmbRoundRobin.ValueMember = "Index";
      this.cmbRoundRobin.SelectedIndexChanged += new System.EventHandler(this.cmbRoundRobin_SelectedIndexChanged);
      // 
      // bndsrcRoundRobinType
      // 
      this.bndsrcRoundRobinType.DataSource = typeof(SIDLibraries.BusinessLayer.RoundRobinType);
      // 
      // chbRoundRobin
      // 
      this.chbRoundRobin.AutoSize = true;
      this.chbRoundRobin.Enabled = false;
      this.chbRoundRobin.Location = new System.Drawing.Point(7, 14);
      this.chbRoundRobin.Name = "chbRoundRobin";
      this.chbRoundRobin.Size = new System.Drawing.Size(89, 17);
      this.chbRoundRobin.TabIndex = 1;
      this.chbRoundRobin.Text = "Round Robin";
      this.chbRoundRobin.UseVisualStyleBackColor = true;
      this.chbRoundRobin.CheckedChanged += new System.EventHandler(this.chbRoundRobin_CheckedChanged);
      // 
      // txtRisk
      // 
      this.txtRisk.AllowCents = false;
      this.txtRisk.AllowNegatives = false;
      this.txtRisk.DividedBy = 1;
      this.txtRisk.DivideFlag = false;
      this.txtRisk.Location = new System.Drawing.Point(564, 6);
      this.txtRisk.Name = "txtRisk";
      this.txtRisk.ShowIfZero = true;
      this.txtRisk.Size = new System.Drawing.Size(51, 20);
      this.txtRisk.TabIndex = 2;
      this.txtRisk.TextChanged += new System.EventHandler(this.txtRisk_TextChanged);
      this.txtRisk.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtRisk_KeyUp);
      // 
      // lblMaxRiskDisplay
      // 
      this.lblMaxRiskDisplay.Location = new System.Drawing.Point(561, 29);
      this.lblMaxRiskDisplay.Name = "lblMaxRiskDisplay";
      this.lblMaxRiskDisplay.Size = new System.Drawing.Size(120, 16);
      this.lblMaxRiskDisplay.TabIndex = 1002;
      this.lblMaxRiskDisplay.Text = "Max Risk";
      // 
      // lblMaxRiskToMaxPayout
      // 
      this.lblMaxRiskToMaxPayout.Location = new System.Drawing.Point(454, 98);
      this.lblMaxRiskToMaxPayout.Name = "lblMaxRiskToMaxPayout";
      this.lblMaxRiskToMaxPayout.Size = new System.Drawing.Size(120, 16);
      this.lblMaxRiskToMaxPayout.TabIndex = 1003;
      this.lblMaxRiskToMaxPayout.Text = "MRisk For Max Payout";
      this.lblMaxRiskToMaxPayout.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // lblMaxPayout
      // 
      this.lblMaxPayout.Location = new System.Drawing.Point(587, 98);
      this.lblMaxPayout.Name = "lblMaxPayout";
      this.lblMaxPayout.Size = new System.Drawing.Size(120, 16);
      this.lblMaxPayout.TabIndex = 1004;
      this.lblMaxPayout.Text = "Max Payout";
      this.lblMaxPayout.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // TypeName
      // 
      this.TypeName.DataPropertyName = "TypeName";
      this.TypeName.HeaderText = "Type";
      this.TypeName.Name = "TypeName";
      this.TypeName.ReadOnly = true;
      // 
      // descriptionDataGridViewTextBoxColumn
      // 
      this.descriptionDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
      this.descriptionDataGridViewTextBoxColumn.DataPropertyName = "Description";
      this.descriptionDataGridViewTextBoxColumn.HeaderText = "Description";
      this.descriptionDataGridViewTextBoxColumn.Name = "descriptionDataGridViewTextBoxColumn";
      this.descriptionDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // SelectedRow
      // 
      this.SelectedRow.DataPropertyName = "SelectedRow";
      this.SelectedRow.HeaderText = "SelectedRow";
      this.SelectedRow.Name = "SelectedRow";
      this.SelectedRow.ReadOnly = true;
      this.SelectedRow.Visible = false;
      // 
      // SelectedCell
      // 
      this.SelectedCell.DataPropertyName = "SelectedCell";
      this.SelectedCell.HeaderText = "SelectedCell";
      this.SelectedCell.Name = "SelectedCell";
      this.SelectedCell.ReadOnly = true;
      this.SelectedCell.Visible = false;
      // 
      // WagerNumber
      // 
      this.WagerNumber.DataPropertyName = "WagerNumber";
      this.WagerNumber.HeaderText = "WagerNumber";
      this.WagerNumber.Name = "WagerNumber";
      this.WagerNumber.ReadOnly = true;
      this.WagerNumber.Visible = false;
      // 
      // Number
      // 
      this.Number.DataPropertyName = "Number";
      this.Number.HeaderText = "ItemNumber";
      this.Number.Name = "Number";
      this.Number.ReadOnly = true;
      this.Number.Visible = false;
      // 
      // SelectedOdds
      // 
      this.SelectedOdds.DataPropertyName = "SelectedOdds";
      this.SelectedOdds.HeaderText = "SelectedOdds";
      this.SelectedOdds.Name = "SelectedOdds";
      this.SelectedOdds.ReadOnly = true;
      this.SelectedOdds.Visible = false;
      // 
      // SelectedBPointsOption
      // 
      this.SelectedBPointsOption.DataPropertyName = "SelectedBPointsOption";
      this.SelectedBPointsOption.HeaderText = "SelectedBPointsOption";
      this.SelectedBPointsOption.Name = "SelectedBPointsOption";
      this.SelectedBPointsOption.ReadOnly = true;
      this.SelectedBPointsOption.Visible = false;
      // 
      // SelectedAmountWagered
      // 
      this.SelectedAmountWagered.DataPropertyName = "SelectedAmountWagered";
      this.SelectedAmountWagered.HeaderText = "SelectedAmountWagered";
      this.SelectedAmountWagered.Name = "SelectedAmountWagered";
      this.SelectedAmountWagered.ReadOnly = true;
      this.SelectedAmountWagered.Visible = false;
      // 
      // SelectedToWinAmount
      // 
      this.SelectedToWinAmount.DataPropertyName = "SelectedToWinAmount";
      this.SelectedToWinAmount.HeaderText = "SelectedToWinAmount";
      this.SelectedToWinAmount.Name = "SelectedToWinAmount";
      this.SelectedToWinAmount.ReadOnly = true;
      this.SelectedToWinAmount.Visible = false;
      // 
      // WagerTypeMode
      // 
      this.WagerTypeMode.DataPropertyName = "WagerTypeMode";
      this.WagerTypeMode.HeaderText = "WagerTypeMode";
      this.WagerTypeMode.Name = "WagerTypeMode";
      this.WagerTypeMode.ReadOnly = true;
      this.WagerTypeMode.Visible = false;
      // 
      // PriceType
      // 
      this.PriceType.DataPropertyName = "PriceType";
      this.PriceType.HeaderText = "PriceType";
      this.PriceType.Name = "PriceType";
      this.PriceType.ReadOnly = true;
      this.PriceType.Visible = false;
      // 
      // GameNumber
      // 
      this.GameNumber.DataPropertyName = "GameNumber";
      this.GameNumber.HeaderText = "GameNumber";
      this.GameNumber.Name = "GameNumber";
      this.GameNumber.ReadOnly = true;
      this.GameNumber.Visible = false;
      // 
      // PeriodNumber
      // 
      this.PeriodNumber.DataPropertyName = "PeriodNumber";
      this.PeriodNumber.HeaderText = "PeriodNumber";
      this.PeriodNumber.Name = "PeriodNumber";
      this.PeriodNumber.ReadOnly = true;
      this.PeriodNumber.Visible = false;
      // 
      // rowGroupNum
      // 
      this.rowGroupNum.DataPropertyName = "rowGroupNum";
      this.rowGroupNum.HeaderText = "rowGroupNum";
      this.rowGroupNum.Name = "rowGroupNum";
      this.rowGroupNum.ReadOnly = true;
      this.rowGroupNum.Visible = false;
      // 
      // periodWagerCutoff
      // 
      this.periodWagerCutoff.DataPropertyName = "periodWagerCutoff";
      this.periodWagerCutoff.HeaderText = "periodWagerCutoff";
      this.periodWagerCutoff.Name = "periodWagerCutoff";
      this.periodWagerCutoff.ReadOnly = true;
      this.periodWagerCutoff.Visible = false;
      // 
      // WageringMode
      // 
      this.WageringMode.DataPropertyName = "WageringMode";
      this.WageringMode.HeaderText = "WageringMode";
      this.WageringMode.Name = "WageringMode";
      this.WageringMode.ReadOnly = true;
      this.WageringMode.Visible = false;
      // 
      // LayoffWager
      // 
      this.LayoffWager.DataPropertyName = "LayoffWager";
      this.LayoffWager.HeaderText = "LayoffWager";
      this.LayoffWager.Name = "LayoffWager";
      this.LayoffWager.ReadOnly = true;
      this.LayoffWager.Visible = false;
      // 
      // FixedPrice
      // 
      this.FixedPrice.DataPropertyName = "FixedPrice";
      this.FixedPrice.HeaderText = "FixedPrice";
      this.FixedPrice.Name = "FixedPrice";
      this.FixedPrice.ReadOnly = true;
      this.FixedPrice.Visible = false;
      // 
      // Pitcher1MStart
      // 
      this.Pitcher1MStart.DataPropertyName = "Pitcher1MStart";
      this.Pitcher1MStart.HeaderText = "Pitcher1MStart";
      this.Pitcher1MStart.Name = "Pitcher1MStart";
      this.Pitcher1MStart.ReadOnly = true;
      this.Pitcher1MStart.Visible = false;
      // 
      // Pitcher2MStart
      // 
      this.Pitcher2MStart.DataPropertyName = "Pitcher2MStart";
      this.Pitcher2MStart.HeaderText = "Pitcher2MStart";
      this.Pitcher2MStart.Name = "Pitcher2MStart";
      this.Pitcher2MStart.ReadOnly = true;
      this.Pitcher2MStart.Visible = false;
      // 
      // TotalPointsOu
      // 
      this.TotalPointsOu.DataPropertyName = "TotalPointsOu";
      this.TotalPointsOu.HeaderText = "TotalPointsOu";
      this.TotalPointsOu.Name = "TotalPointsOu";
      this.TotalPointsOu.ReadOnly = true;
      this.TotalPointsOu.Visible = false;
      // 
      // SelectedAmericanPrice
      // 
      this.SelectedAmericanPrice.DataPropertyName = "SelectedAmericanPrice";
      this.SelectedAmericanPrice.HeaderText = "SelectedAmericanPrice";
      this.SelectedAmericanPrice.Name = "SelectedAmericanPrice";
      this.SelectedAmericanPrice.ReadOnly = true;
      this.SelectedAmericanPrice.Visible = false;
      // 
      // PeriodDescription
      // 
      this.PeriodDescription.DataPropertyName = "PeriodDescription";
      this.PeriodDescription.HeaderText = "PeriodDescription";
      this.PeriodDescription.Name = "PeriodDescription";
      this.PeriodDescription.ReadOnly = true;
      this.PeriodDescription.Visible = false;
      // 
      // AlreadyBetItemCount
      // 
      this.AlreadyBetItemCount.DataPropertyName = "AlreadyBetItemCount";
      this.AlreadyBetItemCount.HeaderText = "PostedCnt";
      this.AlreadyBetItemCount.Name = "AlreadyBetItemCount";
      this.AlreadyBetItemCount.ReadOnly = true;
      this.AlreadyBetItemCount.Width = 50;
      // 
      // TicketNumber
      // 
      this.TicketNumber.DataPropertyName = "TicketNumber";
      this.TicketNumber.HeaderText = "TicketNumber";
      this.TicketNumber.Name = "TicketNumber";
      this.TicketNumber.ReadOnly = true;
      this.TicketNumber.Visible = false;
      // 
      // FrmPlaceParlay
      // 
      this.AcceptButton = this.btnPlaceBet;
      this.AccessibleDescription = "Parlay";
      this.AccessibleName = "Parlay";
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(714, 146);
      this.ControlBox = false;
      this.Controls.Add(this.lblMaxPayout);
      this.Controls.Add(this.lblMaxRiskToMaxPayout);
      this.Controls.Add(this.lblMaxRiskDisplay);
      this.Controls.Add(this.grpRoundRobin);
      this.Controls.Add(this.btnPayTable);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.btnPlaceBet);
      this.Controls.Add(this.txtToWin);
      this.Controls.Add(this.lblWin);
      this.Controls.Add(this.txtRisk);
      this.Controls.Add(this.lblRisk);
      this.Controls.Add(this.nudTeams);
      this.Controls.Add(this.lblTeams);
      this.Controls.Add(this.dgvwParlays);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.Name = "FrmPlaceParlay";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmPlaceParlay_FormClosing);
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmPlaceParlay_FormClosed);
      this.Load += new System.EventHandler(this.frmPlaceParlay_Load);
      ((System.ComponentModel.ISupportInitialize)(this.dgvwParlays)).EndInit();
      this.ctxModifyWager.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.bndsrc)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudTeams)).EndInit();
      this.grpRoundRobin.ResumeLayout(false);
      this.grpRoundRobin.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.bndsrcRoundRobinType)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.DataGridView dgvwParlays;
    private System.Windows.Forms.Label lblTeams;
    private System.Windows.Forms.NumericUpDown nudTeams;
    private System.Windows.Forms.Label lblRisk;
    private System.Windows.Forms.Label lblWin;
    private System.Windows.Forms.Button btnPlaceBet;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.Button btnPayTable;
    private System.Windows.Forms.GroupBox grpRoundRobin;
    private System.Windows.Forms.CheckBox chbRoundRobin;
    private System.Windows.Forms.ComboBox cmbRoundRobin;
    private System.Windows.Forms.BindingSource bndsrc;
    private System.Windows.Forms.BindingSource bndsrcRoundRobinType;
    private System.Windows.Forms.Label lblRoundRobinOutcome;
    private GUILibraries.Controls.NumberTextBox txtRisk;
    private System.Windows.Forms.ContextMenuStrip ctxModifyWager;
    private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
    private GUILibraries.Controls.NumberTextBox txtToWin;
    private System.Windows.Forms.Label lblMaxRiskDisplay;
    private System.Windows.Forms.Label lblMaxRiskToMaxPayout;
    private System.Windows.Forms.Label lblMaxPayout;
    private System.Windows.Forms.DataGridViewTextBoxColumn TypeName;
    private System.Windows.Forms.DataGridViewTextBoxColumn descriptionDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn SelectedRow;
    private System.Windows.Forms.DataGridViewTextBoxColumn SelectedCell;
    private System.Windows.Forms.DataGridViewTextBoxColumn WagerNumber;
    private System.Windows.Forms.DataGridViewTextBoxColumn Number;
    private System.Windows.Forms.DataGridViewTextBoxColumn SelectedOdds;
    private System.Windows.Forms.DataGridViewTextBoxColumn SelectedBPointsOption;
    private System.Windows.Forms.DataGridViewTextBoxColumn SelectedAmountWagered;
    private System.Windows.Forms.DataGridViewTextBoxColumn SelectedToWinAmount;
    private System.Windows.Forms.DataGridViewTextBoxColumn WagerTypeMode;
    private System.Windows.Forms.DataGridViewTextBoxColumn PriceType;
    private System.Windows.Forms.DataGridViewTextBoxColumn GameNumber;
    private System.Windows.Forms.DataGridViewTextBoxColumn PeriodNumber;
    private System.Windows.Forms.DataGridViewTextBoxColumn rowGroupNum;
    private System.Windows.Forms.DataGridViewTextBoxColumn periodWagerCutoff;
    private System.Windows.Forms.DataGridViewTextBoxColumn WageringMode;
    private System.Windows.Forms.DataGridViewTextBoxColumn LayoffWager;
    private System.Windows.Forms.DataGridViewCheckBoxColumn FixedPrice;
    private System.Windows.Forms.DataGridViewTextBoxColumn Pitcher1MStart;
    private System.Windows.Forms.DataGridViewTextBoxColumn Pitcher2MStart;
    private System.Windows.Forms.DataGridViewTextBoxColumn TotalPointsOu;
    private System.Windows.Forms.DataGridViewTextBoxColumn SelectedAmericanPrice;
    private System.Windows.Forms.DataGridViewTextBoxColumn PeriodDescription;
    private System.Windows.Forms.DataGridViewTextBoxColumn AlreadyBetItemCount;
    private System.Windows.Forms.DataGridViewTextBoxColumn TicketNumber;
  }
}