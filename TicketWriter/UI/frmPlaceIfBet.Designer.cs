﻿namespace TicketWriter.UI {
  partial class FrmPlaceIfBet {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
      this.components = new System.ComponentModel.Container();
      this.btnPlaceBet = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.chbActionReverse = new System.Windows.Forms.CheckBox();
      this.chbContinueOnPush = new System.Windows.Forms.CheckBox();
      this.chbARBC = new System.Windows.Forms.CheckBox();
      this.dgvwIfBet = new System.Windows.Forms.DataGridView();
      this.ctxModifyWager = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.bndsrc = new System.Windows.Forms.BindingSource(this.components);
      this.lblAmount = new System.Windows.Forms.Label();
      this.txtAmount = new System.Windows.Forms.TextBox();
      this.lblArDesc = new System.Windows.Forms.Label();
      this.lblBcDesc = new System.Windows.Forms.Label();
      this.lblMaxRiskDisplay = new System.Windows.Forms.Label();
      this.typeNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.amountWageredDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.toWinAmountDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.descriptionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.SelectedRow = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.SelectedCell = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.WagerNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Number = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.SelectedOdds = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.SelectedBPointsOption = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.GameNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.WagerTypeMode = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.PriceType = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.PeriodNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.rowGroupNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.periodWagerCutoff = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.WageringMode = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.LayoffWager = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.FixedPrice = new System.Windows.Forms.DataGridViewCheckBoxColumn();
      this.Pitcher1MStart = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Pitcher2MStart = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.TotalPointsOu = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.SelectedAmericanPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.PeriodDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.TicketNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
      ((System.ComponentModel.ISupportInitialize)(this.dgvwIfBet)).BeginInit();
      this.ctxModifyWager.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.bndsrc)).BeginInit();
      this.SuspendLayout();
      // 
      // btnPlaceBet
      // 
      this.btnPlaceBet.Location = new System.Drawing.Point(3, 87);
      this.btnPlaceBet.Name = "btnPlaceBet";
      this.btnPlaceBet.Size = new System.Drawing.Size(75, 23);
      this.btnPlaceBet.TabIndex = 5;
      this.btnPlaceBet.Text = "Place Bet";
      this.btnPlaceBet.UseVisualStyleBackColor = true;
      this.btnPlaceBet.Click += new System.EventHandler(this.btnPlaceBet_Click);
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.Location = new System.Drawing.Point(84, 87);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 6;
      this.btnCancel.Text = "Cancel";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // chbActionReverse
      // 
      this.chbActionReverse.AutoSize = true;
      this.chbActionReverse.Location = new System.Drawing.Point(3, 2);
      this.chbActionReverse.Name = "chbActionReverse";
      this.chbActionReverse.Size = new System.Drawing.Size(99, 17);
      this.chbActionReverse.TabIndex = 1;
      this.chbActionReverse.Text = "Action Reverse";
      this.chbActionReverse.UseVisualStyleBackColor = true;
      this.chbActionReverse.CheckedChanged += new System.EventHandler(this.chbActionReverse_CheckedChanged);
      this.chbActionReverse.Click += new System.EventHandler(this.chbActionReverse_Click);
      // 
      // chbContinueOnPush
      // 
      this.chbContinueOnPush.AutoSize = true;
      this.chbContinueOnPush.Location = new System.Drawing.Point(3, 19);
      this.chbContinueOnPush.Name = "chbContinueOnPush";
      this.chbContinueOnPush.Size = new System.Drawing.Size(110, 17);
      this.chbContinueOnPush.TabIndex = 4;
      this.chbContinueOnPush.Text = "Continue on Push";
      this.chbContinueOnPush.UseVisualStyleBackColor = true;
      this.chbContinueOnPush.CheckedChanged += new System.EventHandler(this.chbContinueOnPush_CheckedChanged);
      // 
      // chbARBC
      // 
      this.chbARBC.AutoSize = true;
      this.chbARBC.Location = new System.Drawing.Point(108, 2);
      this.chbARBC.Name = "chbARBC";
      this.chbARBC.Size = new System.Drawing.Size(55, 17);
      this.chbARBC.TabIndex = 2;
      this.chbARBC.Text = "ARBC";
      this.chbARBC.UseVisualStyleBackColor = true;
      this.chbARBC.CheckedChanged += new System.EventHandler(this.chbARBC_CheckedChanged);
      this.chbARBC.Click += new System.EventHandler(this.chbARBC_Click);
      // 
      // dgvwIfBet
      // 
      this.dgvwIfBet.AllowUserToAddRows = false;
      this.dgvwIfBet.AllowUserToDeleteRows = false;
      this.dgvwIfBet.AllowUserToResizeRows = false;
      this.dgvwIfBet.AutoGenerateColumns = false;
      this.dgvwIfBet.BackgroundColor = System.Drawing.Color.White;
      this.dgvwIfBet.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
      this.dgvwIfBet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dgvwIfBet.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.typeNameDataGridViewTextBoxColumn,
            this.amountWageredDataGridViewTextBoxColumn,
            this.toWinAmountDataGridViewTextBoxColumn,
            this.descriptionDataGridViewTextBoxColumn,
            this.SelectedRow,
            this.SelectedCell,
            this.WagerNumber,
            this.Number,
            this.SelectedOdds,
            this.SelectedBPointsOption,
            this.GameNumber,
            this.WagerTypeMode,
            this.PriceType,
            this.PeriodNumber,
            this.rowGroupNum,
            this.periodWagerCutoff,
            this.WageringMode,
            this.LayoffWager,
            this.FixedPrice,
            this.Pitcher1MStart,
            this.Pitcher2MStart,
            this.TotalPointsOu,
            this.SelectedAmericanPrice,
            this.PeriodDescription,
            this.TicketNumber});
      this.dgvwIfBet.ContextMenuStrip = this.ctxModifyWager;
      this.dgvwIfBet.DataSource = this.bndsrc;
      this.dgvwIfBet.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
      this.dgvwIfBet.Location = new System.Drawing.Point(170, 2);
      this.dgvwIfBet.Name = "dgvwIfBet";
      this.dgvwIfBet.RowHeadersVisible = false;
      this.dgvwIfBet.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgvwIfBet.Size = new System.Drawing.Size(542, 108);
      this.dgvwIfBet.TabIndex = 7;
      this.dgvwIfBet.TabStop = false;
      this.dgvwIfBet.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvwIfBet_CellDoubleClick);
      this.dgvwIfBet.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvwIfBet_DataError);
      this.dgvwIfBet.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAmount_KeyDown);
      // 
      // ctxModifyWager
      // 
      this.ctxModifyWager.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editToolStripMenuItem,
            this.deleteToolStripMenuItem});
      this.ctxModifyWager.Name = "ctxModifyWager";
      this.ctxModifyWager.Size = new System.Drawing.Size(108, 48);
      // 
      // editToolStripMenuItem
      // 
      this.editToolStripMenuItem.Name = "editToolStripMenuItem";
      this.editToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
      this.editToolStripMenuItem.Text = "Edit...";
      this.editToolStripMenuItem.Click += new System.EventHandler(this.editToolStripMenuItem_Click);
      // 
      // deleteToolStripMenuItem
      // 
      this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
      this.deleteToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
      this.deleteToolStripMenuItem.Text = "Delete";
      this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
      // 
      // bndsrc
      // 
      this.bndsrc.DataSource = typeof(SIDLibraries.BusinessLayer.IfBetItem);
      this.bndsrc.ListChanged += new System.ComponentModel.ListChangedEventHandler(this.bndsrc_ListChanged);
      // 
      // lblAmount
      // 
      this.lblAmount.AutoSize = true;
      this.lblAmount.Location = new System.Drawing.Point(32, 37);
      this.lblAmount.Name = "lblAmount";
      this.lblAmount.Size = new System.Drawing.Size(46, 13);
      this.lblAmount.TabIndex = 6;
      this.lblAmount.Text = "Amount:";
      this.lblAmount.Visible = false;
      // 
      // txtAmount
      // 
      this.txtAmount.Location = new System.Drawing.Point(84, 34);
      this.txtAmount.Name = "txtAmount";
      this.txtAmount.Size = new System.Drawing.Size(75, 20);
      this.txtAmount.TabIndex = 3;
      this.txtAmount.Visible = false;
      this.txtAmount.TextChanged += new System.EventHandler(this.txtAmount_TextChanged);
      this.txtAmount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAmount_KeyDown);
      this.txtAmount.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAmount_KeyPress);
      // 
      // lblArDesc
      // 
      this.lblArDesc.AutoSize = true;
      this.lblArDesc.Location = new System.Drawing.Point(115, 56);
      this.lblArDesc.Name = "lblArDesc";
      this.lblArDesc.Size = new System.Drawing.Size(44, 13);
      this.lblArDesc.TabIndex = 8;
      this.lblArDesc.Text = "2 if-bets";
      this.lblArDesc.Visible = false;
      // 
      // lblBcDesc
      // 
      this.lblBcDesc.AutoSize = true;
      this.lblBcDesc.Location = new System.Drawing.Point(81, 48);
      this.lblBcDesc.Name = "lblBcDesc";
      this.lblBcDesc.Size = new System.Drawing.Size(0, 13);
      this.lblBcDesc.TabIndex = 9;
      // 
      // lblMaxRiskDisplay
      // 
      this.lblMaxRiskDisplay.Location = new System.Drawing.Point(2, 67);
      this.lblMaxRiskDisplay.Name = "lblMaxRiskDisplay";
      this.lblMaxRiskDisplay.Size = new System.Drawing.Size(157, 21);
      this.lblMaxRiskDisplay.TabIndex = 1003;
      this.lblMaxRiskDisplay.Text = "Max Risk";
      this.lblMaxRiskDisplay.Visible = false;
      // 
      // typeNameDataGridViewTextBoxColumn
      // 
      this.typeNameDataGridViewTextBoxColumn.DataPropertyName = "TypeName";
      this.typeNameDataGridViewTextBoxColumn.HeaderText = "Type";
      this.typeNameDataGridViewTextBoxColumn.Name = "typeNameDataGridViewTextBoxColumn";
      this.typeNameDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // amountWageredDataGridViewTextBoxColumn
      // 
      this.amountWageredDataGridViewTextBoxColumn.DataPropertyName = "AmountWagered";
      this.amountWageredDataGridViewTextBoxColumn.HeaderText = "Wagered";
      this.amountWageredDataGridViewTextBoxColumn.Name = "amountWageredDataGridViewTextBoxColumn";
      // 
      // toWinAmountDataGridViewTextBoxColumn
      // 
      this.toWinAmountDataGridViewTextBoxColumn.DataPropertyName = "ToWinAmount";
      this.toWinAmountDataGridViewTextBoxColumn.HeaderText = "To Win";
      this.toWinAmountDataGridViewTextBoxColumn.Name = "toWinAmountDataGridViewTextBoxColumn";
      this.toWinAmountDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // descriptionDataGridViewTextBoxColumn
      // 
      this.descriptionDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
      this.descriptionDataGridViewTextBoxColumn.DataPropertyName = "Description";
      this.descriptionDataGridViewTextBoxColumn.HeaderText = "Description";
      this.descriptionDataGridViewTextBoxColumn.Name = "descriptionDataGridViewTextBoxColumn";
      // 
      // SelectedRow
      // 
      this.SelectedRow.DataPropertyName = "SelectedRow";
      this.SelectedRow.HeaderText = "SelectedRow";
      this.SelectedRow.Name = "SelectedRow";
      this.SelectedRow.ReadOnly = true;
      this.SelectedRow.Visible = false;
      // 
      // SelectedCell
      // 
      this.SelectedCell.DataPropertyName = "SelectedCell";
      this.SelectedCell.HeaderText = "SelectedCell";
      this.SelectedCell.Name = "SelectedCell";
      this.SelectedCell.ReadOnly = true;
      this.SelectedCell.Visible = false;
      // 
      // WagerNumber
      // 
      this.WagerNumber.DataPropertyName = "WagerNumber";
      this.WagerNumber.HeaderText = "WagerNumber";
      this.WagerNumber.Name = "WagerNumber";
      this.WagerNumber.ReadOnly = true;
      this.WagerNumber.Visible = false;
      // 
      // Number
      // 
      this.Number.DataPropertyName = "Number";
      this.Number.HeaderText = "ItemNumber";
      this.Number.Name = "Number";
      this.Number.ReadOnly = true;
      this.Number.Visible = false;
      // 
      // SelectedOdds
      // 
      this.SelectedOdds.DataPropertyName = "SelectedOdds";
      this.SelectedOdds.HeaderText = "SelectedOdds";
      this.SelectedOdds.Name = "SelectedOdds";
      this.SelectedOdds.ReadOnly = true;
      this.SelectedOdds.Visible = false;
      // 
      // SelectedBPointsOption
      // 
      this.SelectedBPointsOption.DataPropertyName = "SelectedBPointsOption";
      this.SelectedBPointsOption.HeaderText = "SelectedBPointsOption";
      this.SelectedBPointsOption.Name = "SelectedBPointsOption";
      this.SelectedBPointsOption.ReadOnly = true;
      this.SelectedBPointsOption.Visible = false;
      // 
      // GameNumber
      // 
      this.GameNumber.DataPropertyName = "GameNumber";
      this.GameNumber.HeaderText = "GameNumber";
      this.GameNumber.Name = "GameNumber";
      this.GameNumber.ReadOnly = true;
      this.GameNumber.Visible = false;
      // 
      // WagerTypeMode
      // 
      this.WagerTypeMode.DataPropertyName = "WagerTypeMode";
      this.WagerTypeMode.HeaderText = "WagerTypeMode";
      this.WagerTypeMode.Name = "WagerTypeMode";
      this.WagerTypeMode.ReadOnly = true;
      this.WagerTypeMode.Visible = false;
      // 
      // PriceType
      // 
      this.PriceType.DataPropertyName = "PriceType";
      this.PriceType.HeaderText = "PriceType";
      this.PriceType.Name = "PriceType";
      this.PriceType.Visible = false;
      // 
      // PeriodNumber
      // 
      this.PeriodNumber.DataPropertyName = "PeriodNumber";
      this.PeriodNumber.HeaderText = "PeriodNumber";
      this.PeriodNumber.Name = "PeriodNumber";
      this.PeriodNumber.Visible = false;
      // 
      // rowGroupNum
      // 
      this.rowGroupNum.DataPropertyName = "rowGroupNum";
      this.rowGroupNum.HeaderText = "rowGroupNum";
      this.rowGroupNum.Name = "rowGroupNum";
      this.rowGroupNum.Visible = false;
      // 
      // periodWagerCutoff
      // 
      this.periodWagerCutoff.DataPropertyName = "periodWagerCutoff";
      this.periodWagerCutoff.HeaderText = "periodWagerCutoff";
      this.periodWagerCutoff.Name = "periodWagerCutoff";
      this.periodWagerCutoff.Visible = false;
      // 
      // WageringMode
      // 
      this.WageringMode.DataPropertyName = "WageringMode";
      this.WageringMode.HeaderText = "WageringMode";
      this.WageringMode.Name = "WageringMode";
      this.WageringMode.Visible = false;
      // 
      // LayoffWager
      // 
      this.LayoffWager.DataPropertyName = "LayoffWager";
      this.LayoffWager.HeaderText = "LayoffWager";
      this.LayoffWager.Name = "LayoffWager";
      this.LayoffWager.Visible = false;
      // 
      // FixedPrice
      // 
      this.FixedPrice.DataPropertyName = "FixedPrice";
      this.FixedPrice.HeaderText = "FixedPrice";
      this.FixedPrice.Name = "FixedPrice";
      this.FixedPrice.Visible = false;
      // 
      // Pitcher1MStart
      // 
      this.Pitcher1MStart.DataPropertyName = "Pitcher1MStart";
      this.Pitcher1MStart.HeaderText = "Pitcher1MStart";
      this.Pitcher1MStart.Name = "Pitcher1MStart";
      this.Pitcher1MStart.Visible = false;
      // 
      // Pitcher2MStart
      // 
      this.Pitcher2MStart.DataPropertyName = "Pitcher2MStart";
      this.Pitcher2MStart.HeaderText = "Pitcher2MStart";
      this.Pitcher2MStart.Name = "Pitcher2MStart";
      this.Pitcher2MStart.Visible = false;
      // 
      // TotalPointsOu
      // 
      this.TotalPointsOu.DataPropertyName = "TotalPointsOu";
      this.TotalPointsOu.HeaderText = "TotalPointsOu";
      this.TotalPointsOu.Name = "TotalPointsOu";
      this.TotalPointsOu.Visible = false;
      // 
      // SelectedAmericanPrice
      // 
      this.SelectedAmericanPrice.DataPropertyName = "SelectedAmericanPrice";
      this.SelectedAmericanPrice.HeaderText = "SelectedAmericanPrice";
      this.SelectedAmericanPrice.Name = "SelectedAmericanPrice";
      this.SelectedAmericanPrice.Visible = false;
      // 
      // PeriodDescription
      // 
      this.PeriodDescription.DataPropertyName = "PeriodDescription";
      this.PeriodDescription.HeaderText = "PeriodDescription";
      this.PeriodDescription.Name = "PeriodDescription";
      this.PeriodDescription.Visible = false;
      // 
      // TicketNumber
      // 
      this.TicketNumber.DataPropertyName = "TicketNumber";
      this.TicketNumber.HeaderText = "TicketNumber";
      this.TicketNumber.Name = "TicketNumber";
      this.TicketNumber.Visible = false;
      // 
      // FrmPlaceIfBet
      // 
      this.AcceptButton = this.btnPlaceBet;
      this.AccessibleDescription = "If-Bet";
      this.AccessibleName = "If-Bet";
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(714, 119);
      this.ControlBox = false;
      this.Controls.Add(this.lblMaxRiskDisplay);
      this.Controls.Add(this.lblBcDesc);
      this.Controls.Add(this.lblArDesc);
      this.Controls.Add(this.txtAmount);
      this.Controls.Add(this.lblAmount);
      this.Controls.Add(this.dgvwIfBet);
      this.Controls.Add(this.chbARBC);
      this.Controls.Add(this.chbContinueOnPush);
      this.Controls.Add(this.chbActionReverse);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.btnPlaceBet);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.Name = "FrmPlaceIfBet";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmPlaceIfBet_FormClosing);
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmPlaceIfBet_FormClosed);
      this.Load += new System.EventHandler(this.frmPlaceIfBet_Load);
      ((System.ComponentModel.ISupportInitialize)(this.dgvwIfBet)).EndInit();
      this.ctxModifyWager.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.bndsrc)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button btnPlaceBet;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.CheckBox chbActionReverse;
    private System.Windows.Forms.CheckBox chbContinueOnPush;
    private System.Windows.Forms.CheckBox chbARBC;
    private System.Windows.Forms.DataGridView dgvwIfBet;
    private System.Windows.Forms.BindingSource bndsrc;
    private System.Windows.Forms.Label lblAmount;
    private System.Windows.Forms.TextBox txtAmount;
    private System.Windows.Forms.Label lblArDesc;
    private System.Windows.Forms.ContextMenuStrip ctxModifyWager;
    private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
    private System.Windows.Forms.Label lblBcDesc;
    private System.Windows.Forms.Label lblMaxRiskDisplay;
    private System.Windows.Forms.DataGridViewTextBoxColumn typeNameDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn amountWageredDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn toWinAmountDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn descriptionDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn SelectedRow;
    private System.Windows.Forms.DataGridViewTextBoxColumn SelectedCell;
    private System.Windows.Forms.DataGridViewTextBoxColumn WagerNumber;
    private System.Windows.Forms.DataGridViewTextBoxColumn Number;
    private System.Windows.Forms.DataGridViewTextBoxColumn SelectedOdds;
    private System.Windows.Forms.DataGridViewTextBoxColumn SelectedBPointsOption;
    private System.Windows.Forms.DataGridViewTextBoxColumn GameNumber;
    private System.Windows.Forms.DataGridViewTextBoxColumn WagerTypeMode;
    private System.Windows.Forms.DataGridViewTextBoxColumn PriceType;
    private System.Windows.Forms.DataGridViewTextBoxColumn PeriodNumber;
    private System.Windows.Forms.DataGridViewTextBoxColumn rowGroupNum;
    private System.Windows.Forms.DataGridViewTextBoxColumn periodWagerCutoff;
    private System.Windows.Forms.DataGridViewTextBoxColumn WageringMode;
    private System.Windows.Forms.DataGridViewTextBoxColumn LayoffWager;
    private System.Windows.Forms.DataGridViewCheckBoxColumn FixedPrice;
    private System.Windows.Forms.DataGridViewTextBoxColumn Pitcher1MStart;
    private System.Windows.Forms.DataGridViewTextBoxColumn Pitcher2MStart;
    private System.Windows.Forms.DataGridViewTextBoxColumn TotalPointsOu;
    private System.Windows.Forms.DataGridViewTextBoxColumn SelectedAmericanPrice;
    private System.Windows.Forms.DataGridViewTextBoxColumn PeriodDescription;
    private System.Windows.Forms.DataGridViewTextBoxColumn TicketNumber;
  }
}