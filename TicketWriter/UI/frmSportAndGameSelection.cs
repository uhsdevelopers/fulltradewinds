﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using GUILibraries.Controls;
using GUILibraries.Forms;
using GUILibraries.Utilities;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using SIDLibraries.Utilities;
using TicketWriter.Extensions;
using TicketWriter.Properties;
using Common = TicketWriter.Utilities.Common;
using System.Data.SqlClient;
using System.Data;

namespace TicketWriter.UI {
  public sealed partial class FrmSportAndGameSelection : SIDForm {

    #region private Classes
    private sealed class SportScheduleDate {
      public String SportType { get; set; }
      public String SportSubType { get; set; }
      public DateTime? ScheduleDate { get; set; }
    }

    #endregion

    #region public Classes

    private sealed class RowItem {
      public string String1 { get; set; }
      public string String2 { get; set; }
      public string String3 { get; set; }
      public string WagerTypeName { get; set; }
      public string WagerNumber { get; set; }
      public string ItemNumber { get; set; }
      public string RowIsClickable { get; set; }
      public string TargetFrm { get; set; }
      public string GameNumber { get; set; }
      public string PeriodNumber { get; set; }
      public string RifBackwardTicketNumber { get; set; }
      public string RifBackwardWagerNumber { get; set; }
      public string FreePlayFlag { get; set; }
      public string WagerType { get; set; }
      public string SpecialIfBetAmount { get; set; }
      public string TicketNumber { get; set; }
    }

    #endregion

    #region Extensions

    public RestrictMoneyLines RestrictMoneyLines;
    private FormatGameTitles _formatGameTitles;
    private LimitParlaysAndTeasersKeyRule _limitParlaysAndTeasersKeyRule;
    private LimitTeasersOnlyKeyRule _limitTeasersOnlyKeyRule;
      public ShowWagerLimitsInFrm ShowWagerLimitsInFrm;

      #endregion

    #region private Constants

    private const int CP_NOCLOSE_BUTTON = 0x200;
    private const string SERIES = "Series";
    private const int GRIDVIEWROWHEIGHT = 16;

    private const int CONTEST_CUSTOM_DESC_INDEX = 0;
    private const int CONTEST_ROTATION_INDEX = 1;
    private const int CONTESTANTNAME_INDEX = 2;
    private const int CONTESTANTODDS_INDEX = 3;
    private const int CONTESTANTNUM_INDEX = 4;
    private const int CONTEST_DATE_INDEX = 5;
    private const int CONTEST_DESC_INDEX = 6;
    private const int CONTEST_COMMENTS_INDEX = 7;
    private const int CONTESTNUM_INDEX = 8;
    private const int CONTEST_CUSTPROFILE_INDEX = 9;

    #endregion

    #region public Constants

    public const int ROTATION_INDEX = 0;
    private const int GAMEDATE_INDEX = 1;
    public const int TEAMS_NAMES_INDEX = 2;
    private const int SPREAD_INDEX = 3;
    private const int MONEY_INDEX = 4;
    private const int TOTAL_INDEX = 5;
    private const int TEAM_TOTAL_OVER_INDEX = 6;
    private const int TEAM_TOTAL_UNDER_INDEX = 7;
    public const int GAME_NUM_INDEX = 8;
    public const int PERIOD_NUM_INDEX = 9;
    public const int ROWGROUPNUM_INDEX = 10;
    private const int TBD11_INDEX = 11;
    private const int TBD12_INDEX = 12;
    private const int TBD13_INDEX = 13;
    private const int TBD14_INDEX = 14;
    private const int TBD15_INDEX = 15;
    public const int SPORT_INDEX = 16;
    public const int SUBSPORT_INDEX = 17;
    public const int PERIOD_CUTOFF_INDEX = 18;
    public const int GAME_STATUS_INDEX = 19;
    public const int TEAM1_NAME_INDEX = 20;
    public const int CUSTPROFILE_INDEX = 21;
    public const int SCHEDULEDATE_INDEX = 22;

    #endregion

    #region Public Properties

    public List<spCnGetActiveContests_Result> ActiveContests {

      get {
        if (_activeContests != null) return _activeContests;
        using (var con = new Contests(Mdi.AppModuleInfo)) {
          _activeContests = con.GetActiveContestsByCustomer(Customer.CustomerID.Trim());
        }
        return _activeContests;
      }

    }

    public List<spGLGetActiveGamesByCustomer_Result> ActiveGames { get; set; }

    // ReSharper disable once MemberCanBePrivate.Global
    public Boolean AddWagerAuthorizationGranted { get; set; } // must be public, used in frmLateWagerRequest.cs

    //public String AddWagerAuthorizationGrantedBy { get; set; }

    public List<ContestTreeNodeDesc> ContestNodesInfo { get; set; }

    public String CurrentCustomerCurrency { get; set; }

    public String CurrentCustBaseballActionType { get; set; }

    public spCstGetCustomer_Result Customer {
      get {
        return _customer ??
               (_customer = ((MdiTicketWriter)FormF.GetFormByName("MdiTicketWriter", this)).Customer);
      }
    }

    // this property is used for testing
    public String GamesGridViewInUse { get; private set; }
    public String PropsGridViewInUse { get; private set; }

    public bool IsFreePlay { get; set; }

    public List<spGLGetActiveGamesByCustomer_Result> FilteredActiveGames { get; set; }

    private FrmPlaceTeaser FrmPlaceTeaser {
      get {
        _frmPlaceTeaser = (FrmPlaceTeaser)FormF.GetFormByName("FrmPlaceTeaser", this);
        return _frmPlaceTeaser;
      }
    }

    private FrmPlaceIfBet FrmPlaceIfBet {
      get {
        _frmPlaceIfBet = (FrmPlaceIfBet)FormF.GetFormByName("FrmPlaceIfBet", this);
        return _frmPlaceIfBet;
      }
    }

    private FrmPlaceParlay FrmPlaceParlay {
      get {
        _frmPlaceParlay = (FrmPlaceParlay)FormF.GetFormByName("FrmPlaceParlay", this);
        return _frmPlaceParlay;
      }
    }

    public MdiTicketWriter Mdi {
      get {
        return _mdiTicketWriter ??
               (_mdiTicketWriter = (MdiTicketWriter)FormF.GetFormByName("MdiTicketWriter", this));
      }
    }

    public FormParameterList ParameterList {
      get {
        return _parameterList ??
               (_parameterList =
                ((MdiTicketWriter)(FormF.GetFormByName("MdiTicketWriter", this))).ParameterList);
      }
    }

    private List<string> PropertyNames { get; set; }

    public List<ReadbackItem> ReadbackItems { get; set; }

    public List<SelectedSportBranch> CurrSelectedSportBranch { get; set; }

    public String SelectedWagerType { get; set; }

    public String StaticLinesDuringCall { get; set; }

    public Timer LinesUpdater { get; set; }

    private String WagerLimitsByLine { get; set; }

    private String WagerLimitsByGame { get; set; }

    public int? OddsDecimalPrecision {
      get {
        if (_oddsDecimalPrecision != null) return _oddsDecimalPrecision;
        _oddsDecimalPrecision = Params.DecimalOddsPrecision;
        return _oddsDecimalPrecision;
      }
    }

    public int? OddsFractionMaxDenominator {
      get { return _oddsFractionMaxDenominator ?? (_oddsFractionMaxDenominator = Params.FractionalOddsMaxDenominator); }
    }

    private String WageringMode {
      get {
        if (Mdi.WageringMode == null) return "A";
        return Mdi.WageringMode;
      }
    } //Automatic (A) or Manual (M), when form loads it defaults to Automatic

    public Boolean ParlayOrTeaserItemFromSb { get; set; }

    private Boolean ApplyVigDiscount { get; set; }

    private bool ShowingActiVeGamesOnly { get; set; }

    private ScheduleDates? ThisWeekScheduleDates { get; set; }

    public double? MaxSpreadForMlBasketballRestriction {
      get {
        if (Mdi.CustomerRestrictions == null) return null;
        try {
          return (from r in Mdi.CustomerRestrictions where r.Code == "BASKMLDIS" select double.Parse(r.ParamValue)).FirstOrDefault();
        }
        catch (Exception) {
          return null;
        }
      }
    }

    public bool WageringDisabled {
      get {
        if (Mdi.CustomerRestrictions == null) return false;
        try {
          return (from r in Mdi.CustomerRestrictions where r.Code == "NOWAGERING" select r.ParamName).Any();
        }
        catch (Exception) {
          return false;
        }
      }
    }

    public double? MaxSpreadForMlFootballRestriction {
      get {
        if (Mdi.CustomerRestrictions == null) return null;
        try {
          return (from r in Mdi.CustomerRestrictions where r.Code == "FOOTMLDIS" select double.Parse(r.ParamValue)).FirstOrDefault();
        }
        catch (Exception) {
          return null;
        }
      }
    }

    public int? ParlayAndTeaserRestrictionKeyFactor {
      get {
        if (Mdi.CustomerRestrictions == null) return null;
        try {
          return (from r in Mdi.CustomerRestrictions where r.Code == "PTKEYRULE" select int.Parse(r.ParamValue)).FirstOrDefault();
        }
        catch (Exception) {
          return null;
        }
      }
    }

    public bool VigDiscountForAllWagerTypes {
      get {
        if (Mdi.CustomerRestrictions == null) return false;
        try {
          return Mdi.CustomerRestrictions.Any(r => r.Code == "VIGDISCALL");
        }
        catch (Exception) {
          return false;
        }
      }
    }

    public WagerLimitValidator.LimitTypes MaxWagerLimitTypeCode { get; set; }

    public bool OfferHighLimitsActive {
      get {
        if (Mdi.CustomerRestrictions == null) return false;
        try {
          return Mdi.CustomerRestrictions.Any(r => r.Code == "VIGDISCALL");
        }
        catch (Exception) {
          return false;
        }
      }
    }

    public string KeyPressBuffer = "";
    public DateTime KeyPressLastTime;

    #endregion

    #region Private Properties

    private bool FormClosePendingFlag { get; set; }

    private List<spCnGetActiveContests_Result> _filteredActiveContests;

    private SqlConnection Connection { get; set; }
    SIDSQLDependency SqlDepLinesChange { get; set; }

    #endregion

    #region Protected Properties

    protected override CreateParams CreateParams {
      get {
        var myCp = base.CreateParams;
        myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
        return myCp;
      }
    }

    #endregion

    #region Public vars

    GamesAndPropsTreeView _gamesAndProps;
    public readonly String CurrentCustomerId;

    #endregion

    #region Private vars

    private List<spCnGetActiveContests_Result> _activeContests;
    private readonly String _currentCustomerAgentId;
    private readonly String _currentCustomerEastLineEnabled;
    private readonly double? _currentCustomerFpBalance;
    private readonly String _currentCustomerIsWise;
    private readonly DateTime? _currentCustomerLastCall;
    private readonly String _currentCustomerPassword;
    private readonly int? _currentCustomerPercentBook;
    private readonly String _currentCustomerPriceType;
    private readonly String _currentCustomerStore;
    private spCstGetCustomer_Result _customer;
    private bool _expand = true;
    private readonly MdiTicketWriter _parentFrm;
    private readonly FrmTicketConfirmation _ticketConfirmation;
    private List<spGLGetEasternLineConversionInfo_Result> _easternLineConversion;
    private FrmPlaceTeaser _frmPlaceTeaser;
    private FrmPlaceIfBet _frmPlaceIfBet;
    private FrmPlaceParlay _frmPlaceParlay;
    public int? _lastGameChangeNum;
    private List<spGLGetLastLineChangeByCustomer_Result> _lastGamesLineChangeByCustomer;
    private MdiTicketWriter _mdiTicketWriter;
    private int? _oddsDecimalPrecision;
    private int? _oddsFractionMaxDenominator;
    private FormParameterList _parameterList;
    private List<SportScheduleDate> _sportsAndScheduleDates;

    private readonly Color _currentgamecolor = Color.FromArgb(247, 243, 206); // Color.LightYellow;
    private readonly Color _timechangecolor = Color.FromArgb(255, 255, 255); // Color.White;
    private readonly Color FUTUREGAMECOLOR = Color.FromArgb(255, 211, 214); // Color.LightPink;
    private readonly Color DOWNGAMECOLOR = Color.FromArgb(214, 243, 255); // Color.LightSkyBlue;
    private readonly Color LOW_CIRCLED_GAME_COLOR = Color.FromArgb(162, 0, 0); // Color.Red;
    private readonly Color HIGH_CIRCLED_GAME_COLOR = Color.FromArgb(0, 100, 0);//Color.DarkGreen;
    //private Color HIGH_CIRCLED_GAME_COLOR = Color.FromArgb(158, 231, 178); // Color.Green;

    private readonly Color BLACKGAMECOLOR = Color.FromArgb(0, 0, 0); // Color.Black;
    private readonly Color GRAYGAMECOLOR = Color.Gray; // Color.Gray;
    private readonly Color BLUEGAMECOLOR = Color.FromArgb(0, 130, 198); // Color.Black;

    private readonly Color WHITEGAMECOLOR = Color.White; // Color.White;

    private int _enteredCellIdx = -1;

    private List<DayOfTheWeek> _daysOfTheWeek;

    private readonly DayOfTheWeek _monday = new DayOfTheWeek { DayIntValue = 1, DayStrValue = DayOfWeek.Monday.ToString() };
    private readonly DayOfTheWeek _tuesday = new DayOfTheWeek { DayIntValue = 2, DayStrValue = DayOfWeek.Tuesday.ToString() };
    private readonly DayOfTheWeek _wednesday = new DayOfTheWeek { DayIntValue = 3, DayStrValue = DayOfWeek.Wednesday.ToString() };
    private readonly DayOfTheWeek _thursday = new DayOfTheWeek { DayIntValue = 4, DayStrValue = DayOfWeek.Thursday.ToString() };
    private readonly DayOfTheWeek _friday = new DayOfTheWeek { DayIntValue = 5, DayStrValue = DayOfWeek.Friday.ToString() };
    private readonly DayOfTheWeek _saturday = new DayOfTheWeek { DayIntValue = 6, DayStrValue = DayOfWeek.Saturday.ToString() };
    private readonly DayOfTheWeek _sunday = new DayOfTheWeek { DayIntValue = 7, DayStrValue = DayOfWeek.Sunday.ToString() };

    private DayOfTheWeek _footballScheduleStartDay = new DayOfTheWeek { DayIntValue = 2, DayStrValue = DayOfWeek.Tuesday.ToString() };

    #endregion

    #region Structures

    public struct SelectedSportBranch {
      public String StartDatePart;
      public String EndDatePart;
      public DateTime StartDateTime;
      public DateTime EndDateTime;
      public int PeriodNumber;
      public String SportSubType;
      public String SportType;
      public int? GameNumber;
    }

    public struct ContestTreeNodeDesc {
      public String LevelTarget;
      public String LevelValue;
      public String CorrelationId;
    }

    private struct DayOfTheWeek {
      public int DayIntValue;
      public String DayStrValue;
    }

    private struct ScheduleDates {
      public DateTime? ScheduleStartDate;
      public DateTime? ScheduleEndDate;
    }

    #endregion

    #region Constructors

    public FrmSportAndGameSelection(SIDForm parentForm, Form siblingForm, spCstGetCustomer_Result customer)
      : base(parentForm.AppModuleInfo) {
        KeyPressLastTime = Mdi != null ? Mdi.GetCurrentDateTime() : DateTime.Now;
      _parentFrm = (MdiTicketWriter)parentForm;
      _ticketConfirmation = (FrmTicketConfirmation)siblingForm;
      CurrentCustomerId = customer.CustomerID;
      _currentCustomerLastCall = customer.LastCallDateTime;
      _currentCustomerEastLineEnabled = customer.EasternLineFlag ?? "N".Trim();
      _currentCustomerIsWise = customer.WiseActionFlag ?? "N".Trim();
      _currentCustomerAgentId = customer.AgentID;
      _currentCustomerPassword = customer.Password;
      _currentCustomerPriceType = customer.PriceType;
      CurrentCustomerCurrency = customer.Currency;
      CurrentCustBaseballActionType = customer.BaseballAction;
      _currentCustomerPercentBook = customer.PercentBook;
      _currentCustomerFpBalance = customer.FreePlayBalance;
      _currentCustomerStore = _parentFrm.CustomerStore.Store.Trim();

      using (var gl = new GamesAndLines(AppModuleInfo)) {
        ActiveGames = gl.GetActiveGamesByCustomer(customer.CustomerID, _currentCustomerStore);
      }

      StaticLinesDuringCall = customer.StaticLinesFlag != null ? customer.StaticLinesFlag.Trim() : "N";

      WagerLimitsByLine = customer.EnforceAccumWagerLimitsByLineFlag != null ? customer.EnforceAccumWagerLimitsByLineFlag.Trim() : "N";

      WagerLimitsByGame = customer.EnforceAccumWagerLimitsFlag != null ? customer.EnforceAccumWagerLimitsFlag.Trim() : "N";

      if ((String.IsNullOrEmpty(WagerLimitsByGame) && String.IsNullOrEmpty(WagerLimitsByLine)) || (WagerLimitsByGame == "N" && WagerLimitsByLine == "N"))
        WagerLimitsByGame = "Y"; //Default

      //WageringMode = _parentFrm.WageringMode ?? "A";

      var statBar = (StatusStrip)_parentFrm.Controls.Find("statBar", true).FirstOrDefault();
      if (statBar != null)
        statBar.Visible = true;

      LoadDaysOfTheWeek();

      InitializeComponent();
    }

    private void LoadDaysOfTheWeek() {
      if (_daysOfTheWeek != null)
        _daysOfTheWeek.Clear();
      else
        _daysOfTheWeek = new List<DayOfTheWeek>();

      _daysOfTheWeek.Add(_monday);
      _daysOfTheWeek.Add(_tuesday);
      _daysOfTheWeek.Add(_wednesday);
      _daysOfTheWeek.Add(_thursday);
      _daysOfTheWeek.Add(_friday);
      _daysOfTheWeek.Add(_saturday);
      _daysOfTheWeek.Add(_sunday);
    }

    #endregion

    #region Public Methods

    public void AddStageContestWagerItem(Ticket.ContestWagerItem cItem, spCnGetActiveContests_Result selectedContest) {
      Mdi.Ticket.AddContestWagerItem(cItem);

      var selectedContestInfo = TwUtilities.ConvertToContestInfoObject(selectedContest, cItem.TicketNumber, cItem.WagerNumber, cItem.ItemNumber);

      Mdi.Ticket.SaveSelectedContestInfo(selectedContestInfo);
    }

    public void BuildReadbackLogInfoData(Boolean affectDb) {
      using (var taW = new TicketsAndWagers(AppModuleInfo)) {
        var rbItem = new ReadbackItem();

        var freePlaysCnt = 0;
        const string freePlayBalanceLbl = "New FREE PLAY Balance: ";
        var newFreePlayBalance = _currentCustomerFpBalance;
        var rifWagerType = "";

        if (ReadbackItems != null)
          if (ReadbackItems.Count > 0)
            ReadbackItems.Clear();

        if (Mdi.Ticket.PlayCount <= 0) return;
        if (affectDb)
          taW.DeleteReadbackLogInfoData(Mdi.TicketNumber);

        List<Ticket.WagerItem> wi = null;
        List<Ticket.ContestWagerItem> cWi = null;
        List<Ticket.ManualWagerItem> mwi = null;

        var j = 0;
        if (Mdi.Ticket.PlayCount == 1) {
          if (affectDb)
            taW.InsertReadbackLogInfoData(Mdi.TicketNumber, j, Mdi.GetCurrentDateTime(), null, null, CurrentCustomerId + " ( " + _currentCustomerPassword + " ) " + Mdi.Ticket.PlayCount.ToString(CultureInfo.InvariantCulture) + " play");
          //AddToContestReadbackLogInfoList(0, 0, _ticketNumber, 0, 0, "", "", j, Mdi.GetCurrentDateTime(), null, null, _currentCustomerId + " ( " + _currentCustomerPassword + " ) " + Mdi.Ticket.PlayCount.ToString(CultureInfo.InvariantCulture) + " play", false, "", "");
          AddToReadbackLogInfoList(0, 0, null, Mdi.TicketNumber, 0, 0, "", "", j, Mdi.GetCurrentDateTime(), null, null, CurrentCustomerId + " ( " + _currentCustomerPassword + " ) " + Mdi.Ticket.PlayCount.ToString(CultureInfo.InvariantCulture) + " play", false, "", "");
        }

        else {
          if (affectDb)
            taW.InsertReadbackLogInfoData(Mdi.TicketNumber, j, Mdi.GetCurrentDateTime(), null, null, CurrentCustomerId + " ( " + _currentCustomerPassword + " ) " + Mdi.Ticket.PlayCount.ToString(CultureInfo.InvariantCulture) + " plays");
          //AddToContestReadbackLogInfoList(0, 0, _ticketNumber, 0, 0, "", "", j, Mdi.GetCurrentDateTime(), null, null, _currentCustomerId + " ( " + _currentCustomerPassword + " ) " + Mdi.Ticket.PlayCount.ToString(CultureInfo.InvariantCulture) + " plays", false, "", "");
          AddToReadbackLogInfoList(0, 0, null, Mdi.TicketNumber, 0, 0, "", "", j, Mdi.GetCurrentDateTime(), null, null, CurrentCustomerId + " ( " + _currentCustomerPassword + " ) " + Mdi.Ticket.PlayCount.ToString(CultureInfo.InvariantCulture) + " plays", false, "", "");
        }

        foreach (var rbL in Mdi.Ticket.Wagers.OrderBy(x => x.TicketNumber).ThenBy(x => x.WagerNumber)) {
          j++;
          var openWager = "(open) ";
          const string contestParlay = " " + WagerType.PARLAYNAME + " (Contains Contests)";
          if (affectDb)
            taW.InsertReadbackLogInfoData(Mdi.TicketNumber, j, rbL.LogDateTime, null, null, null);
          AddToReadbackLogInfoList(0, 0, null, rbL.TicketNumber, rbL.WagerNumber, 0, rbL.WagerType, rbL.WagerTypeName, j, Mdi.GetCurrentDateTime(), null, null, null, false, "", rbL.FreePlayFlag, rbL.SpecialIfBetAmt);
          rbItem.TicketNumber = rbL.TicketNumber;
          rbItem.SeqNumber = j;
          rbItem.LogDateTime = rbL.LogDateTime;
          rbItem.TicketWriter = Environment.UserName;
          rbItem.String1 = null;
          rbItem.String2 = null;
          rbItem.String3 = null;
          j++;
          switch (rbL.WagerType) {
            case WagerType.MANUALPLAY:
              mwi = (from a in Mdi.Ticket.ManualWagerItems where a.TicketNumber == rbL.TicketNumber && a.WagerNumber == rbL.WagerNumber select a).ToList();
              break;
            case WagerType.CONTEST:
              cWi = (from a in Mdi.Ticket.ContestWagerItems where a.TicketNumber == rbL.TicketNumber && a.WagerNumber == rbL.WagerNumber select a).ToList();
              break;
            default:
              if ((rbL.FromPreviousTicket != null & rbL.FromPreviousTicket != "") && !string.IsNullOrEmpty(rbL.FromPreviousWagerNumber)) {
                wi = (from a in Mdi.Ticket.WagerItems where a.TicketNumber == rbL.TicketNumber && a.WagerNumber == rbL.WagerNumber || (a.FromPreviousTicket == rbL.FromPreviousTicket && a.FromPreviousWagerNumber == rbL.FromPreviousWagerNumber) select a).ToList();
              }
              else {
                wi = (from a in Mdi.Ticket.WagerItems where a.TicketNumber == rbL.TicketNumber && a.WagerNumber == rbL.WagerNumber select a).ToList();
              }
              break;
          }

          var isFreePlay = rbL.FreePlayFlag;
          String isFreePlayLbl;
          var riskLabel = "Risking ";
          if (isFreePlay == "Y") {
            isFreePlayLbl = "   *** Free Play ***";
            freePlaysCnt++;
          }
          else {
            isFreePlayLbl = "";
          }

          var isLayOffWager = rbL.LayoffFlag.Trim() == "Y" ? "   *** LAYOFF WAGER ***" : "";

          ReadbackItem.WagerItemEditInfo wiEdit;
          switch (rbL.WagerTypeName) {
            case "Straight Bet":

              if (rbL.WagerType == WagerType.CONTEST) {

                var compositeContestDesc = cWi[0].RotNum + " - " + (cWi[0].CompositeContestDesc ?? "").Trim();
                if (cWi[0].XtoYLineRep == null || cWi[0].XtoYLineRep == "N") {
                  switch (cWi[0].PriceType) {
                    case LineOffering.PRICE_AMERICAN:
                      if (cWi[0].FinalMoney > 0)
                        compositeContestDesc += " +" + cWi[0].FinalMoney;
                      else {
                        compositeContestDesc += cWi[0].FinalMoney;
                      }
                      break;
                    case LineOffering.PRICE_DECIMAL:
                      if (cWi[0].FinalDecimal != null)
                        compositeContestDesc += " " + cWi[0].FinalDecimal;
                      break;
                    case LineOffering.PRICE_FRACTIONAL:
                      if (cWi[0].FinalNumerator != null && cWi[0].FinalDenominator != null)
                        compositeContestDesc += " " + cWi[0].FinalNumerator + "/" + cWi[0].FinalDenominator;
                      break;
                  }
                }
                else {
                  compositeContestDesc += " " + cWi[0].FinalMoney + " to " + cWi[0].FinalToBase;
                }

                if (cWi[0].ContestType2 != null && (cWi[0].ContestType2.Trim() != "" && cWi[0].ContestType2.Trim() != ".")) {
                  compositeContestDesc += " - " + cWi[0].ContestType2.Trim();
                }

                if (cWi[0].ContestType3 != null && (cWi[0].ContestType3.Trim() != "" && cWi[0].ContestType3.Trim() != ".")) {
                  compositeContestDesc += " - " + cWi[0].ContestType3.Trim();
                }

                if (cWi[0].ContestDesc != null && (cWi[0].ContestDesc.Trim() != "" && cWi[0].ContestDesc.Trim() != ".")) {
                  compositeContestDesc += " - " + cWi[0].ContestDesc.Trim();
                }

                if (affectDb)
                  taW.InsertReadbackLogInfoData(Mdi.TicketNumber, j, rbL.LogDateTime, rbL.WagerNumber.ToString(CultureInfo.InvariantCulture), rbL.WagerTypeName, compositeContestDesc + isFreePlayLbl + (Mdi.TicketNumber != rbL.TicketNumber ? " (Editing Pending Ticket " + rbL.TicketNumber + ")" : ""));
                string thresholdLine = null;
                if (cWi[0].ThresholdLine != null)
                  thresholdLine = cWi[0].ThresholdLine.ToString();

                var xtoYLineRep = "";
                var finalToBase = "";
                if (cWi[0].XtoYLineRep != null)
                  xtoYLineRep = cWi[0].XtoYLineRep;
                if (cWi[0].FinalToBase != null)
                  finalToBase = cWi[0].FinalToBase.ToString();

                var cWiEdit = TwUtilities.AddWagerContestItemEditInfo((int)cWi[0].ContestNum, (int)cWi[0].ContestantNum, cWi[0].CompositeContestDesc, cWi[0].FinalMoney.ToString(), thresholdLine, FormatNumber((double)cWi[0].AmountWagered), FormatNumber((double)cWi[0].ToWinAmount), TwUtilities.GetPriceTypeName(cWi[0].PriceType), cWi[0].WageringMode, rbL.LayoffFlag.Trim() == "Y", xtoYLineRep, finalToBase);
                AddToContestReadbackLogInfoList((int)cWi[0].ContestNum, rbL.TicketNumber, rbL.WagerNumber, cWi[0].ItemNumber, rbL.WagerType, rbL.WagerTypeName, j, (DateTime)rbL.LogDateTime, rbL.WagerNumber.ToString(CultureInfo.InvariantCulture), rbL.WagerTypeName, compositeContestDesc + isFreePlayLbl + " " + isLayOffWager + (Mdi.TicketNumber != rbL.TicketNumber ? " (Editing Pending Ticket " + rbL.TicketNumber + ")" : ""), cWiEdit, true, "wi", rbL.FreePlayFlag);

                j++;

                if (affectDb)
                  taW.InsertReadbackLogInfoData(Mdi.TicketNumber, j, rbL.LogDateTime, null, rifWagerType, "Risking " + FormatNumber((double)rbL.AmountWagered) + " to win " + FormatNumber((double)rbL.ToWinAmount));
                AddToContestReadbackLogInfoList((int)cWi[0].ContestNum, cWi[0].TicketNumber, cWi[0].WagerNumber, cWi[0].ItemNumber, rbL.WagerType, rbL.WagerTypeName, j, (DateTime)rbL.LogDateTime, null, rifWagerType, "Risking " + rbL.AmountWagered + " to win " + rbL.ToWinAmount, cWiEdit, true, "wi", rbL.FreePlayFlag);
              }
              else {
                switch (rbL.RifWinOnlyFlag) {
                  case "Y":
                    rifWagerType = "If-Win-Only";
                    break;
                  case "N":
                    rifWagerType = "If-Win-Push";
                    break;
                }

                if (_parentFrm.RIfBetFromReadback == "true" && rbL.RifBackwardWagerNumber != "") {
                  rifWagerType += " #" + rbL.RifBackwardWagerNumber;
                }

                if (affectDb)
                  taW.InsertReadbackLogInfoData(Mdi.TicketNumber, j, rbL.LogDateTime, rbL.WagerNumber.ToString(CultureInfo.InvariantCulture), rbL.WagerTypeName, wi[0].Description + isFreePlayLbl + (Mdi.TicketNumber != rbL.TicketNumber ? " (Editing Pending Ticket " + rbL.TicketNumber + ")" : ""));
                wiEdit = TwUtilities.AddWagerItemEditInfo(/*wi[0].SelectedRow,*/ wi[0].SelectedCell, wi[0].SelectedOdds, wi[0].SelectedAmericanPrice, wi[0].SelectedBPointsOption, FormatNumber((double)rbL.AmountWagered), FormatNumber((double)rbL.ToWinAmount), wi[0].ShortDescription, wi[0].WagerTypeMode, TwUtilities.GetPriceTypeName(wi[0].PriceType), wi[0].WageringMode, (int)wi[0].PeriodNumber, wi[0].RowGroupNum, wi[0].PeriodWagerCutoff, wi[0].LayoffFlag, wi[0].AdjustableOddsFlag, wi[0].Pitcher1ReqFlag, wi[0].Pitcher2ReqFlag, wi[0].TotalPointsOu);
                AddToReadbackLogInfoList((int)wi[0].GameNum, (int)wi[0].PeriodNumber, wi[0].PeriodDescription, rbL.TicketNumber, rbL.WagerNumber, wi[0].ItemNumber, rbL.WagerType, rbL.WagerTypeName, j, (DateTime)rbL.LogDateTime, rbL.WagerNumber.ToString(CultureInfo.InvariantCulture), rbL.WagerTypeName, wi[0].Description + isFreePlayLbl + (Mdi.TicketNumber != rbL.TicketNumber ? " (Editing Pending Play)" : ""), wiEdit, true, "wi", _parentFrm.RollingIfBetMode, rbL.RifWinOnlyFlag, rbL.RifBackwardTicketNumber, rbL.RifBackwardWagerNumber, true, wi[0].GameDateTime, rbL.FreePlayFlag, "");

                j++;

                if (affectDb)
                  taW.InsertReadbackLogInfoData(Mdi.TicketNumber, j, rbL.LogDateTime, null, rifWagerType, "Risking " + FormatNumber((double)rbL.AmountWagered) + " to win " + FormatNumber((double)rbL.ToWinAmount) + isLayOffWager);
                AddToReadbackLogInfoList((int)wi[0].GameNum, (int)wi[0].PeriodNumber, wi[0].PeriodDescription, wi[0].TicketNumber, wi[0].WagerNumber, wi[0].ItemNumber, rbL.WagerType, rbL.WagerTypeName, j, (DateTime)rbL.LogDateTime, null, rifWagerType, "Risking " + FormatNumber((double)rbL.AmountWagered) + " to win " + FormatNumber((double)rbL.ToWinAmount) + isLayOffWager, wiEdit, true, "wi", _parentFrm.RollingIfBetMode, rbL.RifWinOnlyFlag, rbL.RifBackwardTicketNumber, rbL.RifBackwardWagerNumber, false, wi[0].GameDateTime, rbL.FreePlayFlag);
                if (isFreePlay == "Y") {
                  newFreePlayBalance -= rbL.AmountWagered * 100;
                }
              }
              break;

            case "Parlay":

              if (rbL.WagerStatus == "P") {
                openWager = "";
              }

              if (rbL.RoundRobinLink != null && rbL.RoundRobinLink > 0) {
                if (affectDb)
                  taW.InsertReadbackLogInfoData(Mdi.TicketNumber, j, rbL.LogDateTime, rbL.WagerNumber.ToString(CultureInfo.InvariantCulture), rbL.WagerTypeName + " R Robin", rbL.TotalPicks + " team " + openWager + "Round Robin" + isFreePlayLbl + " - " + rbL.SelectedRRobinOutcome);
                AddToReadbackLogInfoList(0, 0, null, rbL.TicketNumber, rbL.WagerNumber, 0, rbL.WagerType, rbL.WagerTypeName, j, (DateTime)rbL.LogDateTime, rbL.WagerNumber.ToString(CultureInfo.InvariantCulture), rbL.WagerTypeName + " Round Robin", rbL.TotalPicks + " team " + openWager + "Round Robin" + isFreePlayLbl + " - " + rbL.SelectedRRobinOutcome, true, "w", rbL.FreePlayFlag, "");
              }
              else {
                if (affectDb)
                  taW.InsertReadbackLogInfoData(Mdi.TicketNumber, j, rbL.LogDateTime, rbL.WagerNumber.ToString(CultureInfo.InvariantCulture), rbL.WagerTypeName, rbL.TotalPicks + " team " + openWager +
                      ((from a in Mdi.Ticket.WagerItems where a.TicketNumber == rbL.TicketNumber && a.WagerNumber == rbL.WagerNumber && a.IsContest select a).Any() ? contestParlay : WagerType.PARLAYNAME) + isFreePlayLbl + (Mdi.TicketNumber != rbL.TicketNumber ? " (Editing Pending Ticket " + rbL.TicketNumber + ")" : ""));
                AddToReadbackLogInfoList(0, 0, null, rbL.TicketNumber, rbL.WagerNumber, 0, rbL.WagerType, rbL.WagerTypeName, j, (DateTime)rbL.LogDateTime, rbL.WagerNumber.ToString(CultureInfo.InvariantCulture), rbL.WagerTypeName, rbL.TotalPicks + " team " + openWager +
                      ((from a in Mdi.Ticket.WagerItems where a.TicketNumber == rbL.TicketNumber && a.WagerNumber == rbL.WagerNumber && a.IsContest select a).Any() ? contestParlay : WagerType.PARLAYNAME) + isFreePlayLbl + (Mdi.TicketNumber != rbL.TicketNumber ? " (Editing Pending Ticket " + rbL.TicketNumber + ")" : ""), true, "w", rbL.FreePlayFlag);
              }
              j++;

              foreach (var i in wi) {
                var openPlayStatus = "";
                if (rbL.FromPreviousTicket != null) {
                  if (i.FromPreviousTicket != null) {
                    openPlayStatus = "(existing) ";
                    riskLabel = "Original Risk ";
                  }
                  else {
                    openPlayStatus = "** Added ** ";
                  }
                }

                if (affectDb)
                  taW.InsertReadbackLogInfoData(Mdi.TicketNumber, j, rbL.LogDateTime, null, null, openPlayStatus + i.Description
                      + (i.OriginatingTicketNumber != null && i.OriginatingTicketNumber > 0 ? " (Ref Ticket# " + int.Parse(i.OriginatingTicketNumber.ToString()) + ")" : ""));
                wiEdit = TwUtilities.AddWagerItemEditInfo(/*i.SelectedRow,*/ i.SelectedCell, i.SelectedOdds, i.SelectedAmericanPrice, i.SelectedBPointsOption, FormatNumber((double)rbL.AmountWagered), FormatNumber((double)rbL.ToWinAmount), i.ShortDescription, i.WagerTypeMode, TwUtilities.GetPriceTypeName(i.PriceType), i.WageringMode, (int)i.PeriodNumber, i.RowGroupNum, i.PeriodWagerCutoff, i.LayoffFlag, i.AdjustableOddsFlag, i.Pitcher1ReqFlag, i.Pitcher2ReqFlag, i.TotalPointsOu);
                AddToReadbackLogInfoList((int)i.GameNum, (int)i.PeriodNumber, i.PeriodDescription, i.TicketNumber, i.WagerNumber, i.ItemNumber, rbL.WagerType, rbL.WagerTypeName, j, (DateTime)rbL.LogDateTime, null, null, openPlayStatus + i.Description
                    + (i.OriginatingTicketNumber != null && i.OriginatingTicketNumber > 0 ? " (Ref Ticket# " + int.Parse(i.OriginatingTicketNumber.ToString()) + ")" : ""),
                    wiEdit, true, "w", false, null, rbL.RifBackwardTicketNumber, rbL.RifBackwardWagerNumber, true, wi[0].GameDateTime, rbL.FreePlayFlag);
                j++;
              }

              if (rbL.RoundRobinLink != null && rbL.RoundRobinLink > 0) {
                if (affectDb)
                  taW.InsertReadbackLogInfoData(Mdi.TicketNumber, j, rbL.LogDateTime, null, null, "Risking " + FormatNumber((double)rbL.AmountWagered) + " each parlay.  Total Risk of " + FormatNumber(double.Parse((rbL.RoundRobinItemsCount * rbL.AmountWagered).ToString())) + " to win " + FormatNumber((double)rbL.ToWinAmount));
                AddToReadbackLogInfoList(0, 0, null, rbL.TicketNumber, rbL.WagerNumber, 0, rbL.WagerType, rbL.WagerTypeName, j, (DateTime)rbL.LogDateTime, null, null, "Risking " + FormatNumber((double)rbL.AmountWagered) + " each parlay.  Total Risk of " + FormatNumber(double.Parse((rbL.RoundRobinItemsCount * rbL.AmountWagered).ToString())) + " to win " + FormatNumber((double)rbL.ToWinAmount), true, "w", rbL.FreePlayFlag);
              }
              else {
                if (rbL.WagerStatus == "P") {
                  if (affectDb)
                    taW.InsertReadbackLogInfoData(Mdi.TicketNumber, j, rbL.LogDateTime, null, null, riskLabel + FormatNumber((double)rbL.AmountWagered) + " to win " + FormatNumber((double)rbL.ToWinAmount));
                  AddToReadbackLogInfoList(0, 0, null, rbL.TicketNumber, rbL.WagerNumber, 0, rbL.WagerType, rbL.WagerTypeName, j, (DateTime)rbL.LogDateTime, null, null, riskLabel + FormatNumber((double)rbL.AmountWagered) + " to win " + FormatNumber((double)rbL.ToWinAmount), true, "w", rbL.FreePlayFlag);
                }
                else {
                  if (affectDb)
                    taW.InsertReadbackLogInfoData(Mdi.TicketNumber, j, rbL.LogDateTime, null, null, riskLabel + FormatNumber((double)rbL.AmountWagered));
                  AddToReadbackLogInfoList(0, 0, null, rbL.TicketNumber, rbL.WagerNumber, 0, rbL.WagerType, rbL.WagerTypeName, j, (DateTime)rbL.LogDateTime, null, null, riskLabel + FormatNumber((double)rbL.AmountWagered), true, "w", rbL.FreePlayFlag);
                }
              }

              if (isFreePlay == "Y") {
                newFreePlayBalance -= rbL.AmountWagered * 100;
              }
              break;

            case "If-Bet":
              var specialIfBet = false;
              if ((rbL.ArLink != null && rbL.ArLink > 0) || (rbL.ArBirdcageLink != null && rbL.ArBirdcageLink > 0)) {
                specialIfBet = true;
                var specialIfName = "Action Reverse";
                var ifBetsCnt = "2";
                if (rbL.ArBirdcageLink > 0) {
                  specialIfName = "AR Birdcage";
                  ifBetsCnt = (from p in Mdi.BirdCageIfBets where p.TicketNumber == rbL.TicketNumber && p.ArBirdcageLink == rbL.WagerNumber select p).Count().ToString(CultureInfo.InvariantCulture);
                }

                if (affectDb)
                  taW.InsertReadbackLogInfoData(Mdi.TicketNumber, j, rbL.LogDateTime, rbL.WagerNumber.ToString(CultureInfo.InvariantCulture), specialIfName, rbL.TotalPicks + " teams " + ifBetsCnt + " if-bets " + "   " + isFreePlayLbl);
                AddToReadbackLogInfoList(0, 0, null, rbL.TicketNumber, rbL.WagerNumber, 0, rbL.WagerType, specialIfName, j, (DateTime)rbL.LogDateTime, rbL.WagerNumber.ToString(CultureInfo.InvariantCulture), specialIfName, rbL.TotalPicks + " teams " + ifBetsCnt + " if-bets " + "   " + isFreePlayLbl, true, "w", rbL.FreePlayFlag, rbL.SpecialIfBetAmt);
              }
              else {
                if (affectDb)
                  taW.InsertReadbackLogInfoData(Mdi.TicketNumber, j, rbL.LogDateTime, rbL.WagerNumber.ToString(CultureInfo.InvariantCulture), rbL.WagerTypeName, rbL.TotalPicks + " teams" + "   " + isFreePlayLbl);
                AddToReadbackLogInfoList(0, 0, null, rbL.TicketNumber, rbL.WagerNumber, 0, rbL.WagerType, rbL.WagerTypeName, j, (DateTime)rbL.LogDateTime, rbL.WagerNumber.ToString(CultureInfo.InvariantCulture), rbL.WagerTypeName, rbL.TotalPicks + " teams" + "   " + isFreePlayLbl, true, "w", rbL.FreePlayFlag, rbL.SpecialIfBetAmt);
              }
              j++;

              var ifBetType = rbL.ContinueOnPushFlag.Trim() == "Y" ? "-if win or push-" : "-if win only-";

              var wiCnt = 0;

              foreach (var i in wi) {
                if (affectDb)
                  taW.InsertReadbackLogInfoData(Mdi.TicketNumber, j, rbL.LogDateTime, null, null, i.Description);
                wiEdit = TwUtilities.AddWagerItemEditInfo(/*i.SelectedRow,*/ i.SelectedCell, i.SelectedOdds, i.SelectedAmericanPrice, i.SelectedBPointsOption, FormatNumber((double)i.AmountWagered), FormatNumber((double)i.ToWinAmount), i.ShortDescription, i.WagerTypeMode, TwUtilities.GetPriceTypeName(i.PriceType), i.WageringMode, (int)i.PeriodNumber, i.RowGroupNum, i.PeriodWagerCutoff, i.LayoffFlag, i.AdjustableOddsFlag, i.Pitcher1ReqFlag, i.Pitcher2ReqFlag, i.TotalPointsOu);
                AddToReadbackLogInfoList((int)i.GameNum, (int)i.PeriodNumber, i.PeriodDescription, i.TicketNumber, i.WagerNumber, i.ItemNumber, rbL.WagerType, rbL.WagerTypeName, j, (DateTime)rbL.LogDateTime, null, null, i.Description, wiEdit, true, "wi", false, null, rbL.RifBackwardTicketNumber, rbL.RifBackwardWagerNumber, true, wi[0].GameDateTime, rbL.FreePlayFlag, rbL.SpecialIfBetAmt);
                j++;
                if (!specialIfBet) {
                  if (affectDb)
                    taW.InsertReadbackLogInfoData(Mdi.TicketNumber, j, rbL.LogDateTime, null, null, "Risking " + FormatNumber((double)i.AmountWagered) + " to win " + FormatNumber((double)i.ToWinAmount));
                  AddToReadbackLogInfoList((int)i.GameNum, (int)i.PeriodNumber, i.PeriodDescription, i.TicketNumber, i.WagerNumber, i.ItemNumber, rbL.WagerType, rbL.WagerTypeName, j, (DateTime)rbL.LogDateTime, null, null, "Risking " + FormatNumber((double)i.AmountWagered) + " to win " + FormatNumber((double)i.ToWinAmount), wiEdit, true, "w", false, null, rbL.RifBackwardTicketNumber, rbL.RifBackwardWagerNumber, false, wi[0].GameDateTime, rbL.FreePlayFlag, rbL.SpecialIfBetAmt);
                  j++;
                  if (wiCnt < wi.Count - 1) {
                    if (affectDb)
                      taW.InsertReadbackLogInfoData(Mdi.TicketNumber, j, rbL.LogDateTime, null, null, ifBetType);
                    AddToReadbackLogInfoList((int)i.GameNum, (int)i.PeriodNumber, i.PeriodDescription, i.TicketNumber, rbL.WagerNumber, i.ItemNumber, rbL.WagerType, rbL.WagerTypeName, j, (DateTime)rbL.LogDateTime, null, null, ifBetType, true, "wi", rbL.FreePlayFlag, rbL.SpecialIfBetAmt);
                    j++;
                  }
                }
                if (isFreePlay == "Y") {
                  newFreePlayBalance -= rbL.AmountWagered * 100;
                }
                wiCnt++;
              }

              if ((rbL.ArLink != null && rbL.ArLink > 0) || (rbL.ArBirdcageLink != null && rbL.ArBirdcageLink > 0)) {
                if (affectDb)
                  taW.InsertReadbackLogInfoData(Mdi.TicketNumber, j, rbL.LogDateTime, null, null, "Risking " + FormatNumber(double.Parse((rbL.AmountWagered).ToString())) + " to win " + FormatNumber((double)rbL.ToWinAmount));
                AddToReadbackLogInfoList(0, 0, null, rbL.TicketNumber, rbL.WagerNumber, 0, rbL.WagerType, rbL.WagerTypeName, j, (DateTime)rbL.LogDateTime, null, null, "Risking " + FormatNumber(double.Parse((rbL.AmountWagered).ToString())) + " to win " + FormatNumber((double)rbL.ToWinAmount), true, "w", rbL.FreePlayFlag);
              }
              j++;
              break;

            case "Teaser":
              if (rbL.WagerStatus == "P")
                openWager = "";
              if (rbL.RoundRobinLink != null && rbL.RoundRobinLink > 0) {
                if (affectDb)
                  taW.InsertReadbackLogInfoData(Mdi.TicketNumber, j, rbL.LogDateTime, rbL.WagerNumber.ToString(CultureInfo.InvariantCulture), rbL.WagerTypeName + " R Robin", rbL.TotalPicks + " team " + openWager + "Round Robin" + isFreePlayLbl + " - " + rbL.SelectedRRobinOutcome);
                AddToReadbackLogInfoList(0, 0, null, rbL.TicketNumber, rbL.WagerNumber, 0, rbL.WagerType, rbL.WagerTypeName, j, (DateTime)rbL.LogDateTime, rbL.WagerNumber.ToString(CultureInfo.InvariantCulture), rbL.WagerTypeName + " Round Robin", rbL.TotalPicks + " team " + openWager + "Round Robin" + isFreePlayLbl + " - " + rbL.SelectedRRobinOutcome, true, "w", rbL.FreePlayFlag, "");
              }
              else {

                if (affectDb)
                  taW.InsertReadbackLogInfoData(Mdi.TicketNumber, j, rbL.LogDateTime, rbL.WagerNumber.ToString(CultureInfo.InvariantCulture), rbL.WagerTypeName, rbL.TotalPicks + " team " + openWager + rbL.TeaserName.Trim() + isFreePlayLbl + (Mdi.TicketNumber != rbL.TicketNumber ? " (Editing Pending Ticket " + rbL.TicketNumber + ")" : ""));
                AddToReadbackLogInfoList(0, 0, null, rbL.TicketNumber, rbL.WagerNumber, 0, rbL.WagerType, rbL.WagerTypeName, j, (DateTime)rbL.LogDateTime, rbL.WagerNumber.ToString(CultureInfo.InvariantCulture), rbL.WagerTypeName, rbL.TotalPicks + " team " + openWager + rbL.TeaserName.Trim() + isFreePlayLbl + (Mdi.TicketNumber != rbL.TicketNumber ? " (Editing Pending Ticket " + rbL.TicketNumber + ")" : ""), true, "w", rbL.FreePlayFlag);
              }
              j++;
              foreach (var i in wi) {
                var openPlayStatus = "";
                if (rbL.FromPreviousTicket != null) {
                  if (i.FromPreviousTicket != null) {
                    openPlayStatus = "(existing) ";
                    riskLabel = "Original Risk ";
                  }
                  else {
                    openPlayStatus = "** Added ** ";
                  }
                }

                if (affectDb)
                  taW.InsertReadbackLogInfoData(Mdi.TicketNumber, j, rbL.LogDateTime, null, null, TwUtilities.RebuildTeaserReadbackDescription(openPlayStatus + i.Description.Trim(), i.WagerType.Trim(), i.PeriodDescription.Trim(), i.TeaserPoints.ToString()));
                wiEdit = TwUtilities.AddWagerItemEditInfo(/*i.SelectedRow,*/ i.SelectedCell, i.SelectedOdds, i.SelectedAmericanPrice, i.SelectedBPointsOption, FormatNumber((double)rbL.AmountWagered), FormatNumber((double)rbL.ToWinAmount), i.ShortDescription, i.WagerTypeMode, TwUtilities.GetPriceTypeName(i.PriceType), i.WageringMode, (int)i.PeriodNumber, i.RowGroupNum, i.PeriodWagerCutoff, i.LayoffFlag, i.AdjustableOddsFlag, i.Pitcher1ReqFlag, i.Pitcher2ReqFlag, i.TotalPointsOu);
                AddToReadbackLogInfoList((int)i.GameNum, (int)i.PeriodNumber, i.PeriodDescription, i.TicketNumber, i.WagerNumber, i.ItemNumber, rbL.WagerType, rbL.WagerTypeName, j, (DateTime)rbL.LogDateTime, null, null, TwUtilities.RebuildTeaserReadbackDescription(openPlayStatus + i.Description.Trim(), i.WagerType.Trim(), i.PeriodDescription.Trim(), i.TeaserPoints.ToString()), wiEdit, true, "w", false, null, rbL.RifBackwardTicketNumber, rbL.RifBackwardWagerNumber, true, wi[0].GameDateTime, rbL.FreePlayFlag);
                j++;
              }

              if (rbL.RoundRobinLink != null && rbL.RoundRobinLink > 0) {
                if (affectDb)
                  taW.InsertReadbackLogInfoData(Mdi.TicketNumber, j, rbL.LogDateTime, null, null, "Risking " + FormatNumber((double)rbL.AmountWagered) + " each parlay.  Total Risk of " + FormatNumber(double.Parse((rbL.RoundRobinItemsCount * rbL.AmountWagered).ToString())) + " to win " + FormatNumber((double)rbL.ToWinAmount));
                AddToReadbackLogInfoList(0, 0, null, rbL.TicketNumber, rbL.WagerNumber, 0, rbL.WagerType, rbL.WagerTypeName, j, (DateTime)rbL.LogDateTime, null, null, "Risking " + FormatNumber((double)rbL.AmountWagered) + " each parlay.  Total Risk of " + FormatNumber(double.Parse((rbL.RoundRobinItemsCount * rbL.AmountWagered).ToString())) + " to win " + FormatNumber((double)rbL.ToWinAmount), true, "w", rbL.FreePlayFlag);
              }
              else {
                if (affectDb)
                  taW.InsertReadbackLogInfoData(Mdi.TicketNumber, j, rbL.LogDateTime, null, null, riskLabel + FormatNumber((double)rbL.AmountWagered) + " to win " + FormatNumber((double)rbL.ToWinAmount));
                AddToReadbackLogInfoList(0, 0, null, rbL.TicketNumber, rbL.WagerNumber, 0, rbL.WagerType, rbL.WagerTypeName, j, (DateTime)rbL.LogDateTime, null, null, riskLabel + FormatNumber((double)rbL.AmountWagered) + " to win " + FormatNumber((double)rbL.ToWinAmount), true, "w", rbL.FreePlayFlag);
              }

              if (isFreePlay == "Y") {
                newFreePlayBalance -= rbL.AmountWagered * 100;
              }
              break;

            case "Manual Play":
              var mWiEdit = new ReadbackItem.ManualWagerItemEditInfo();

              if (mwi.Count > 0) {
                if (affectDb)
                  taW.InsertReadbackLogInfoData(Mdi.TicketNumber, j, rbL.LogDateTime, rbL.WagerNumber.ToString(CultureInfo.InvariantCulture), rbL.WagerTypeName, mwi[0].Description + " Accepted at " + DisplayDate.Format(Mdi.GetCurrentDateTime(), DisplayDateFormat.ShortDateTime) + " " + isFreePlayLbl + " " + isLayOffWager + (Mdi.TicketNumber != rbL.TicketNumber ? " (Editing Pending Ticket " + rbL.TicketNumber + ")" : ""));
                mWiEdit = TwUtilities.AddWagerItemEditInfo(mwi[0].Type, mwi[0].Description, mwi[0].GradeDateTime, mwi[0].StrOdds, FormatNumber((double)rbL.AmountWagered), FormatNumber((double)rbL.ToWinAmount), rbL.LayoffFlag.Trim() == "Y");
                AddToReadbackLogInfoList(0, 0, null, rbL.TicketNumber, rbL.WagerNumber, mwi[0].ItemNumber, rbL.WagerType, rbL.WagerTypeName, j, (DateTime)rbL.LogDateTime, rbL.WagerNumber.ToString(CultureInfo.InvariantCulture), rbL.WagerTypeName, mwi[0].Description + " Accepted at " + DisplayDate.Format(Mdi.GetCurrentDateTime(), DisplayDateFormat.ShortDateTime) + " " + isFreePlayLbl + " " + isLayOffWager + (Mdi.TicketNumber != rbL.TicketNumber ? " (Editing Pending Ticket " + rbL.TicketNumber + ")" : ""), mWiEdit, true, "w");
              }
              j++;
              if (affectDb)
                taW.InsertReadbackLogInfoData(Mdi.TicketNumber, j, rbL.LogDateTime, null, null, "Risking " + FormatNumber((double)rbL.AmountWagered) + " to win " + FormatNumber((double)rbL.ToWinAmount));
              if (mwi.Count > 0) {
                AddToReadbackLogInfoList(0, 0, null, mwi[0].TicketNumber, mwi[0].WagerNumber, mwi[0].ItemNumber, rbL.WagerType, rbL.WagerTypeName, j, (DateTime)rbL.LogDateTime,
                                  null, null, "Risking " + FormatNumber((double)rbL.AmountWagered) + " to win " + FormatNumber((double)rbL.ToWinAmount), mWiEdit, true, "w");
              }
              if (isFreePlay == "Y") {
                newFreePlayBalance -= rbL.AmountWagered * 100;
              }
              break;
          }
        }
        j++;
        if (freePlaysCnt > 0) {
          if (affectDb)
            taW.InsertReadbackLogInfoData(Mdi.TicketNumber, j, Mdi.GetCurrentDateTime(), null, CurrentCustomerId, freePlayBalanceLbl + FormatNumber((double)newFreePlayBalance, true, 100, true).ToString(CultureInfo.InvariantCulture) + "     " + "(Press \"Ctrl C\" to post all wagers)");
          AddToReadbackLogInfoList(0, 0, null, Mdi.TicketNumber, 0, 0, "", "", j, Mdi.GetCurrentDateTime(), null, CurrentCustomerId, freePlayBalanceLbl + FormatNumber((double)newFreePlayBalance, true, 100, true).ToString(CultureInfo.InvariantCulture) + "     " + "(Press \"Ctrl C\" to post all wagers)", false, "", "");
        }
        else {
          if (affectDb)
            taW.InsertReadbackLogInfoData(Mdi.TicketNumber, j, Mdi.GetCurrentDateTime(), null, CurrentCustomerId, "(Press \"Ctrl C\" to post all wagers)");
          AddToReadbackLogInfoList(0, 0, null, Mdi.TicketNumber, 0, 0, "", "", j, Mdi.GetCurrentDateTime(), null, CurrentCustomerId, "(Press \"Ctrl C\" to post all wagers)", false, "", "");
        }
        j++;
        if (affectDb)
          taW.InsertReadbackLogInfoData(Mdi.TicketNumber, j, Mdi.GetCurrentDateTime(), null, null, null);
        AddToReadbackLogInfoList(0, 0, null, Mdi.TicketNumber, 0, 0, "", "", j, Mdi.GetCurrentDateTime(), null, null, null, false, "", "");
      }
    }

    public Ticket.Wager BuildWObject(spTkWGetCustomerWagerDetails_Result tbWagerObj) {
      var wagerRow = new Ticket.Wager {
        TicketNumber = tbWagerObj.TicketNumber,
        WagerNumber = tbWagerObj.WagerNumber,
        AmountWagered = tbWagerObj.AmountWagered,
        ToWinAmount = tbWagerObj.ToWinAmount,
        WagerType = tbWagerObj.WagerTypeCode,
        WagerStatus = tbWagerObj.WagerStatus,
        TeaserName = tbWagerObj.TeaserName ?? "",
        WagerTypeName = tbWagerObj.WagerType
      };


      switch (tbWagerObj.WagerTypeCode) {
        case WagerType.SPREAD:
        case WagerType.MONEYLINE:
        case WagerType.TOTALPOINTS:
        case WagerType.TEAMTOTALPOINTS:
          wagerRow.WagerTypeName = "Straight Bet";
          break;
        case WagerType.PARLAY:
          wagerRow.WagerTypeName = WagerType.PARLAYNAME;
          break;
        case WagerType.TEASER:
          wagerRow.WagerTypeName = WagerType.TEASERNAME;
          break;
        case WagerType.IFBET:
          wagerRow.WagerTypeName = "If-Bet";
          wagerRow.SpecialIfBetAmt = "";
          break;
        case WagerType.MANUALPLAY:
          wagerRow.WagerTypeName = "Manual Play";
          break;
      }

      wagerRow.Description = tbWagerObj.Description;
      wagerRow.ParlayName = tbWagerObj.ParlayName;
      wagerRow.MinimumPicks = tbWagerObj.MinimumPicks;
      wagerRow.Ties = tbWagerObj.Ties == "Push" ? 0 : 2;
      wagerRow.RoundRobinLink = tbWagerObj.RoundRobinLink;
      wagerRow.ArLink = tbWagerObj.ARLink;
      wagerRow.ArBirdcageLink = tbWagerObj.ARBirdcageLink;
      wagerRow.CreditAcctFlag = tbWagerObj.CreditAcctFlag;
      wagerRow.ContinueOnPushFlag = tbWagerObj.ContinueOnPushFlag;
      wagerRow.PlayNumber = tbWagerObj.PlayNumber;
      wagerRow.TotalPicks = tbWagerObj.TotalPicks;
      wagerRow.AcceptedDateTime = tbWagerObj.AcceptedDateTime;
      wagerRow.CurrencyCode = tbWagerObj.CurrencyCode;
      wagerRow.ValueDate = tbWagerObj.ValueDate;
      wagerRow.BoxLink = tbWagerObj.BoxLink;
      wagerRow.LayoffFlag = tbWagerObj.LayoffFlag;
      wagerRow.MaxPayout = tbWagerObj.MaxPayout;
      wagerRow.ParlayPayoutType = tbWagerObj.ParlayPayoutType;
      wagerRow.FreePlayFlag = tbWagerObj.FreePlayFlag;

      wagerRow.RifBackwardTicketNumber = tbWagerObj.RifBackwardTicketNumber != null ? tbWagerObj.RifBackwardTicketNumber.ToString() : "";
      wagerRow.RifBackwardWagerNumber = tbWagerObj.RifBackwardWagerNumber != null ? tbWagerObj.RifBackwardTicketNumber.ToString() : "";
      wagerRow.RifWinOnlyFlag = tbWagerObj.RifWinOnlyFlag;
      wagerRow.RifCurrentPlayFlag = tbWagerObj.RifCurrentPlayFlag;

      wagerRow.LogDateTime = tbWagerObj.AcceptedDateTime;

      wagerRow.RifMaxReinvestAmt = null;
      wagerRow.OpenPlayWagerInfo = tbWagerObj;

      return wagerRow;
    }

    public List<Ticket.WagerItem> BuildWiObject(List<spTkWGetWagerItems_Result> tbWagerGameItemObj) {
      var wis = new List<Ticket.WagerItem>();

      foreach (var tbWi in tbWagerGameItemObj) {
        var wi = new Ticket.WagerItem {
          TicketNumber = tbWi.TicketNumber,
          WagerNumber = tbWi.WagerNumber,
          ItemNumber = tbWi.ItemNumber,
          GameNum = tbWi.GameNum,
          GameDateTime = tbWi.GameDateTime,
          SportType = tbWi.SportType,
          SportSubType = tbWi.SportSubType,
          WagerType = tbWi.WagerType,
          AdjSpread = tbWi.AdjSpread,
          AdjTotalPoints = tbWi.AdjTotalPoints,
          TotalPointsOu = tbWi.TotalPointsOU,
          FinalMoney = tbWi.FinalMoney,
          ChosenTeamId = tbWi.ChosenTeamID,
          Outcome = tbWi.Outcome,
          AmountWagered = tbWi.AmountWagered / 100,
          ToWinAmount = tbWi.ToWinAmount / 100,
          TeaserPoints = tbWi.TeaserPoints,
          Store = tbWi.Store,
          CustProfile = tbWi.CustProfile,
          PeriodNumber = tbWi.PeriodNumber,
          PeriodDescription = tbWi.PeriodDescription,
          OriginatingTicketNumber = tbWi.OriginatingTicketNumber,
          AdjustableOddsFlag = tbWi.AdjustableOddsFlag,
          ListedPitcher1 = tbWi.ListedPitcher1,
          ListedPitcher2 = tbWi.ListedPitcher2,
          Pitcher1ReqFlag = tbWi.Pitcher1ReqFlag,
          Pitcher2ReqFlag = tbWi.Pitcher2ReqFlag,
          PercentBook = tbWi.PercentBook,
          VolumeAmount = tbWi.VolumeAmount / 100,
          ShowOnChartFlag = tbWi.ShowOnChartFlag,
          CurrencyCode = tbWi.CurrencyCode,
          ValueDate = tbWi.ValueDate,
          LayoffFlag = tbWi.LayoffFlag,
          AgentId = tbWi.AgentID,
          EasternLine = tbWi.EasternLine,
          FreePlayFlag = tbWi.FreePlayFlag,
          WiseActionFlag = tbWi.WiseActionFlag,
          OrigSpread = tbWi.OrigSpread,
          OrigTotalPoints = tbWi.OrigTotalPoints,
          OrigMoney = tbWi.OrigMoney,
          FinalDecimal = tbWi.FinalDecimal,
          FinalNumerator = tbWi.FinalNumerator,
          FinalDenominator = tbWi.FinalDenominator,
          OrigDecimal = tbWi.OrigDecimal,
          OrigNumerator = tbWi.OrigNumerator,
          OrigDenominator = tbWi.OrigDenominator,
          PriceType = tbWi.PriceType,
          SelectedCell = 0,
          SelectedOdds = "",
          SelectedBPointsOption = "",
          ShortDescription = "",
          RowGroupNum = 1,
          WagerTypeMode = (tbWi.FinalMoney ?? 0) >= 0 ? "Risk" : "ToWin",
          WageringMode = tbWi.WageringMode,
          ComplexIfBetAmount = ((tbWi.ToWinAmount ?? 0) / 100).ToString(CultureInfo.InvariantCulture),
          FromPreviousTicket = "",
          FreeHalfPointGiven = ((tbWi.FreeHalfPointGiven ?? false)),
          FreeHalfPointNewWager = false,
          Loo = null,
          IsContest = (tbWi.IsContest ?? false),
          PendingPlayItemInDb = tbWi
        };

        if (wi.IsContest) {
          using (var tAw = new TicketsAndWagers(AppModuleInfo)) {
            var cItem = tAw.GetContestWagerItem(wi.TicketNumber, wi.WagerNumber);
            if (cItem != null) {
              wi.ContestantNum = cItem.ContestantNum;
              if (Mdi.Ticket.ContestWagerItems == null) {
                Mdi.Ticket.ContestWagerItems = new List<Ticket.ContestWagerItem>();
              }
              if (ActiveContests != null) {
                var selectedContestInfo = (from s in ActiveContests where s.ContestNum == wi.GameNum && s.ContestantNum == wi.ContestantNum select s).FirstOrDefault();
                if (selectedContestInfo != null) {
                  var cWi = CreateStageContestWagerItem("Edit", cItem.TicketNumber, cItem.WagerNumber, cItem.ItemNumber, 0, selectedContestInfo, cItem.PriceType,
          cItem.ContestantName, cItem.WageringMode, cItem.LayoffFlag, cItem.FinalMoney ?? 0, (cItem.FinalToBase ?? 0).ToString(CultureInfo.InvariantCulture), (cItem.ThresholdLine ?? 0).ToString(CultureInfo.InvariantCulture));
                  Mdi.Ticket.ContestWagerItems.Add(cWi);
                }
              }
            }
          }
        }

          var activeGame = ActiveGames.FirstOrDefault(a => tbWi.PeriodNumber != null && (a.GameNum == (tbWi.GameNum ?? 0) && a.PeriodNumber == tbWi.PeriodNumber));
          var wagerType = (from w in Mdi.Ticket.Wagers where w.TicketNumber == wi.TicketNumber && w.WagerNumber == wi.WagerNumber select w.WagerTypeName).First();
        var wagerDescription = (from w in Mdi.Ticket.Wagers where w.TicketNumber == wi.TicketNumber && w.WagerNumber == wi.WagerNumber select w.Description).First();
        if (wagerDescription != null)
          wi.ShortDescription = wi.Description = (wagerDescription.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None)[wi.ItemNumber - 1]);

        if (wagerType != null && wagerType == "Teaser") {
          wi.ShortDescription = TwUtilities.BuildDescriptionAndAdjustedPrice(activeGame, wi.WagerType, wi.TotalPointsOu, wi.ChosenTeamId,
              GetAdjustedLine(wi),
              wi.PeriodDescription);
        }

        if (activeGame != null) {
          wi.RowGroupNum = wi.ChosenTeamIdx = Mdi.GetChosenTeamIdx(tbWi, activeGame);
          wi.Loo = activeGame;
          wi.PeriodWagerCutoff = activeGame.PeriodWagerCutoff ?? new DateTime();
          wi.SelectedOdds = Mdi.GetSelectedOdds(tbWi, activeGame);
          if (String.IsNullOrEmpty(wi.SelectedOdds))
            return null;
        }
        wi.SelectedAmericanPrice = tbWi.FinalMoney ?? 0;
        if (wi.SelectedAmericanPrice == 0)
          return null;
        wi.SelectedCell = Mdi.GetChosenWagerType(tbWi);
        if (wi.SelectedCell == 0)
          return null;
        wi.SelectedBPointsOption = Mdi.GetPointsBought(tbWi);
        wi.ComplexIfBetAmount = "";
        wis.Add(wi);
      }
      return wis;
    }

    public string BuildWagerDescription(string rotNumber, string line, string linePrice, string wagerType, string overUnder, string selectedTeamId, String sportType, spGLGetActiveGamesByCustomer_Result selectedGamePeriodInfo, String wagerTypeName, String priceType, double baseLine, double adjustedBpLine, String pitcher1MustStart, String pitcher2MustStart, String baseBallPriceIsFixed, Boolean freeHalfPointAdded = false) {
      var isAsian = (line.Contains(".25") || line.Contains(".75"));
      var periodName = "";
      if (selectedGamePeriodInfo.PeriodDescription != null) {
        if (selectedGamePeriodInfo.SportType != null && selectedGamePeriodInfo.SportSubType != null &&
            (selectedGamePeriodInfo.SportType.Trim() == SportTypes.BASEBALL || selectedGamePeriodInfo.SportType.Trim() == SportTypes.BASKETBALL) &&
            selectedGamePeriodInfo.SportSubType.Trim() == SportTypes.SERIES_PRICES
            ) {
          periodName = SERIES;
        }
        else
          periodName = selectedGamePeriodInfo.PeriodDescription.Trim();
      }
      line = line.Replace("+", "").Replace(GamesAndLines.PICK, "0");
      linePrice = linePrice.Replace("+", "");

      var chosenTeamId = selectedTeamId;

      var wagerDescription = "";
      wagerDescription += selectedGamePeriodInfo.SportType != null ? selectedGamePeriodInfo.SportType.ToString(CultureInfo.InvariantCulture).Trim() : string.Empty;
      wagerDescription += " - " + rotNumber;

      if ((selectedGamePeriodInfo.SportType != null ? selectedGamePeriodInfo.SportType.Trim() : string.Empty) == SportTypes.SOCCER && wagerType == WagerType.MONEYLINE && chosenTeamId.Trim().ToLower() == "draw") {
        chosenTeamId += " (" + selectedGamePeriodInfo.Team1ID.Trim() + " vs " + selectedGamePeriodInfo.Team2ID.Trim() + ")";
      }

      if (wagerType == WagerType.TOTALPOINTS) {
        chosenTeamId = selectedGamePeriodInfo.Team1ID.Trim() + "/" + selectedGamePeriodInfo.Team2ID.Trim();
        if (overUnder == "O") {
          chosenTeamId += " over";
        }
        else {
          chosenTeamId += " under";
        }
      }

      wagerDescription += " " + chosenTeamId;

      if (wagerType == WagerType.TEAMTOTALPOINTS) {
        if (overUnder == "O") {
          wagerDescription += " team total over";
        }
        else {
          wagerDescription += " team total under";
        }
      }

      if (isAsian) {
        if (wagerType == WagerType.SPREAD) {
          line = LineOffering.DetermineSpreadValue(Math.Abs(double.Parse(line)), selectedTeamId, selectedGamePeriodInfo.FavoredTeamID);
        }
        else if (wagerType == WagerType.TOTALPOINTS || wagerType != WagerType.TEAMTOTALPOINTS) {
          line = LineOffering.DetermineTotalValue(double.Parse(line));
        }
      }
      else {
          line = line.Replace(".5", GamesAndLines.HALFSYMBOL);
      }

      if (wagerTypeName == WagerType.TEASERNAME) {
          line = LineOffering.ConvertToStringBaseLine(line);
        if (wagerType == WagerType.TOTALPOINTS || wagerType == WagerType.TEAMTOTALPOINTS) {
          line = overUnder == "O" ? (double.Parse(line) - double.Parse(linePrice)).ToString(CultureInfo.InvariantCulture) : (double.Parse(line) + double.Parse(linePrice)).ToString(CultureInfo.InvariantCulture);
        }
        else {
          line = (double.Parse(line) + double.Parse(linePrice)).ToString(CultureInfo.InvariantCulture);
        }

        if (wagerType != WagerType.TOTALPOINTS && wagerType != WagerType.TEAMTOTALPOINTS) {
          if (double.Parse(line) > 0)
            line = "+" + line;
        }

        if (line == "0" || line == "-0" || line == "+0")
            line = GamesAndLines.PICK;

        line = line.Replace(".5", GamesAndLines.HALFSYMBOL);

        if (line == ("0" + GamesAndLines.HALFSYMBOL) || line == ("+0" + GamesAndLines.HALFSYMBOL) || line == ("-0" + GamesAndLines.HALFSYMBOL))
          line = line.Replace("0", "");

        wagerDescription += " " + line + " ";
        wagerDescription += "for " + periodName;
        wagerDescription += " (" + linePrice + " Pt Teaser) ";

        return wagerDescription;
      }

      if (priceType == "American" || priceType == LineOffering.PRICE_AMERICAN) {
          if (line != "" && !isAsian && double.Parse(line.Replace(GamesAndLines.HALFSYMBOL, ".5")) > 0 && (wagerType != WagerType.TOTALPOINTS && wagerType != WagerType.TEAMTOTALPOINTS))
              line = "+" + line.Replace(".5", GamesAndLines.HALFSYMBOL);
      }

      if (line == "0") {
          line = GamesAndLines.PICK;
      }
      else if (line == ("0" + GamesAndLines.HALFSYMBOL) || line == ("-0" + GamesAndLines.HALFSYMBOL) || line == ("+0" + GamesAndLines.HALFSYMBOL))
      {
          line = line.Replace("0" + GamesAndLines.HALFSYMBOL, GamesAndLines.HALFSYMBOL);
      }

      switch (priceType) {
        case "American":
        case LineOffering.PRICE_AMERICAN:
          if (wagerType != WagerType.MONEYLINE) {
            if (double.Parse(linePrice) > 0 && (wagerType == WagerType.SPREAD || wagerType == WagerType.TOTALPOINTS || wagerType == WagerType.TEAMTOTALPOINTS))
              wagerDescription += " " + line + " +" + linePrice;
            else
              wagerDescription += " " + line + " " + linePrice;
          }
          else {
            wagerDescription += " " + line + " ";
          }
          break;
        case "Decimal":
        case LineOffering.PRICE_DECIMAL:
          if (wagerType != WagerType.MONEYLINE) {
            wagerDescription += " " + line + " " + OddsTypes.UStoDecimal(double.Parse(linePrice), OddsDecimalPrecision);
          }
          else {
            wagerDescription += " " + line + " ";
          }
          break;
        case "Fractional":
        case LineOffering.PRICE_FRACTIONAL:
          var newSpreadDec = OddsTypes.UStoDecimal(double.Parse(linePrice), OddsDecimalPrecision);


          if (OddsFractionMaxDenominator != null) {
            var fractionAry = OddsTypes.DecToFraction(double.Parse(newSpreadDec.ToString(CultureInfo.InvariantCulture)), (int)OddsFractionMaxDenominator);

            if (wagerType != WagerType.MONEYLINE) {
              wagerDescription += " " + line + " " + fractionAry.Numerator + "/" + fractionAry.Denominator;
            }
            else {
              wagerDescription += " " + line + " ";
            }
          }

          break;
      }

      if (wagerType == WagerType.SPREAD || wagerType == WagerType.TOTALPOINTS) {
        if (Math.Abs(Math.Abs(baseLine) - Math.Abs(adjustedBpLine)) > 0 && !isAsian) {
          var adjustedLine = (Math.Abs(baseLine - adjustedBpLine)).ToString(CultureInfo.InvariantCulture);

          if (double.Parse(adjustedLine) > 1)
              wagerDescription += " buying " + adjustedLine.Replace(".5", GamesAndLines.HALFSYMBOL) + " points for " + periodName;
          else
              wagerDescription += " buying " + adjustedLine.ToString(CultureInfo.InvariantCulture).Replace(".5", GamesAndLines.HALFSYMBOL).Replace("0", "") + " point for " + periodName;
        }
        else {
          wagerDescription += " for " + periodName;
        }

        if (freeHalfPointAdded) {
          wagerDescription += " *** Free Half Point Added ***";
        }
      }
      else {
        wagerDescription += " for " + periodName;
      }

      if (sportType != SportTypes.BASEBALL) return wagerDescription;
      if (selectedGamePeriodInfo.SportSubType != null && selectedGamePeriodInfo.SportSubType.Trim() == SportTypes.SERIES_PRICES) {
        if (baseBallPriceIsFixed == "Y" || (String.IsNullOrEmpty(selectedGamePeriodInfo.ListedPitcher1) && String.IsNullOrEmpty(selectedGamePeriodInfo.ListedPitcher2))) {
          wagerDescription += ". Price is Fixed.";
        }
        return wagerDescription;
      }

      var pitchersInfo = "";

      if (selectedGamePeriodInfo.ListedPitcher1 != null) {
        pitchersInfo += selectedGamePeriodInfo.ListedPitcher1.Trim();
      }

      var baseballAction = pitcher1MustStart == "N" ? "(action)" : "(must start)";

      if (selectedGamePeriodInfo.Pitcher1StartedFlag == null || selectedGamePeriodInfo.Pitcher1StartedFlag == "Y") {
        pitchersInfo += " " + baseballAction + ", ";
      }

      if (selectedGamePeriodInfo.ListedPitcher2 != null) {
        pitchersInfo += selectedGamePeriodInfo.ListedPitcher2.Trim();
      }

      baseballAction = pitcher2MustStart == "N" ? "(action)" : "(must start)";

      if (selectedGamePeriodInfo.Pitcher2StartedFlag == null || selectedGamePeriodInfo.Pitcher2StartedFlag == "Y") {
        pitchersInfo += " " + baseballAction;
      }

      if (selectedGamePeriodInfo.ListedPitcher1 != null && selectedGamePeriodInfo.ListedPitcher2 != null) {
        if (baseBallPriceIsFixed == "Y")
          wagerDescription += " Price is Fixed";

        wagerDescription += " Pitchers: ";
        wagerDescription += pitchersInfo;
      }
      return wagerDescription;
    }

    private bool CheckForInvalidPickCombinations(spGLGetActiveGamesByCustomer_Result newSelection, string wagerItemTypeCode, string wagerTypeName, int ticketNumber, int? wagerNumber, string gameDataSource, string fromPreviousTicketNumber = null, string fromPreviousWagerNumber = null, int? itemNumber = null) {
      if (Mdi.Ticket == null || Mdi.Ticket.Wagers == null || Mdi.Ticket.WagerItems == null) return false;
      List<Ticket.WagerItem> wis;
      if (!string.IsNullOrEmpty(fromPreviousTicketNumber) && !string.IsNullOrEmpty(fromPreviousWagerNumber)) {
        wis = (from wi in Mdi.Ticket.WagerItems where wi.TicketNumber == ticketNumber && wi.WagerNumber == wagerNumber || (wi.TicketNumber == ticketNumber && wi.FromPreviousTicket == fromPreviousTicketNumber && wi.FromPreviousWagerNumber == fromPreviousWagerNumber) select wi).OrderBy(p => p.ItemNumber).ToList();
      }
      else {
        wis = (from wi in Mdi.Ticket.WagerItems where wi.TicketNumber == ticketNumber && wi.WagerNumber == wagerNumber select wi).OrderBy(p => p.ItemNumber).ToList();
      }

      var errorPending = false;

      foreach (var wi in wis) {
        var sportType1 = "";
        var sportType2 = "";

        if (wi.ItemNumber == itemNumber) continue;
        if (newSelection.SportType != null)
          sportType1 += newSelection.SportType;

        if (wi.SportType != null)
          sportType2 += wi.SportType;

        if (sportType1.Trim() != sportType2.Trim()) continue;
        var selected = (from a in ActiveGames where a.GameNum == wi.GameNum && a.PeriodNumber == wi.PeriodNumber select a).FirstOrDefault();

        var newSelectionCorrelationId = newSelection.CorrelationID;
        if (newSelectionCorrelationId != null)
          newSelectionCorrelationId = newSelectionCorrelationId.Trim();

        var selectedCorrelationId = selected == null ? null : selected.CorrelationID;
        if (selectedCorrelationId != null)
          selectedCorrelationId = selectedCorrelationId.Trim();

        if (selected == null
          || (newSelection.GameNum != wi.GameNum && (newSelectionCorrelationId != selectedCorrelationId || newSelectionCorrelationId == null))
          || (gameDataSource != "GameSelection" && (wagerTypeName != "Parlay" || (gameDataSource != "ReadBack" && gameDataSource != "ParlayBox")))) continue;

        if ((newSelection.ParlayRestriction == GamesAndLines.PARLAY_DENY_SAME_GAME || newSelection.ParlayRestriction == GamesAndLines.PARLAY_DENY_ALL) && (wagerTypeName == "Parlay" || wagerTypeName == "If-Bet" || wagerTypeName == "Teaser")) {
          errorPending = true;
          if ((newSelection.ParlayRestriction != GamesAndLines.PARLAY_DENY_SAME_GAME && newSelection.ParlayRestriction != GamesAndLines.PARLAY_DENY_ALL) || wagerTypeName != "Parlay" || !LoginsAndProfiles.ValidateUserFunctionalityAccess(Mdi.CurrentUserPermissions, LoginsAndProfiles.IGNORE_PARLAY_RESTRICTION)) continue;
          if (MessageBox.Show(@"Override Parlay Restriction?", @"Warning", MessageBoxButtons.OKCancel) == DialogResult.OK) {
            errorPending = false;
          }
        }
        else {
          if (Params.BlockStQuarterParlayFlag && newSelection.PeriodNumber == wi.PeriodNumber && newSelection.PeriodNumber > 2 &&
              (wagerItemTypeCode == WagerType.SPREAD && wi.WagerType == WagerType.TOTALPOINTS || wagerItemTypeCode == WagerType.TOTALPOINTS && wi.WagerType == WagerType.SPREAD) && wagerTypeName != "If-Bet") {
            errorPending = true;
          }

          if ((wagerItemTypeCode == wi.WagerType && wagerTypeName != "Teaser") || wagerItemTypeCode == WagerType.SPREAD && wi.WagerType == WagerType.MONEYLINE || wagerItemTypeCode == WagerType.MONEYLINE && wi.WagerType == WagerType.SPREAD) {
            if (CheckForIntersectingPeriod(newSelection.SportType, newSelection.SportSubType,
              newSelection.PeriodNumber, wi.PeriodNumber)) {
              errorPending = true;
            }
          }

          if ((wagerItemTypeCode == WagerType.TOTALPOINTS && wi.WagerType == WagerType.TEAMTOTALPOINTS || wagerItemTypeCode == WagerType.TEAMTOTALPOINTS && wi.WagerType == WagerType.TOTALPOINTS) && wagerTypeName == "Parlay" && newSelection.GameNum == wi.GameNum) {
            errorPending = true;
          }

          if (wagerItemTypeCode == wi.WagerType && wagerTypeName == "Teaser" && newSelection.GameNum != wi.GameNum) {
            errorPending = true;
          }

          if (Params.BlockStBaseballFlag) {
            if ((newSelection.SportType == SportTypes.BASEBALL && wi.SportType == SportTypes.BASEBALL) && (wagerItemTypeCode == WagerType.SPREAD && wi.WagerType == WagerType.TOTALPOINTS || wagerItemTypeCode == WagerType.TOTALPOINTS && wi.WagerType == WagerType.SPREAD)) {
              if (CheckForIntersectingPeriod(newSelection.SportType, newSelection.SportSubType,
                newSelection.PeriodNumber, wi.PeriodNumber)) {
                errorPending = true;
              }
            }
          }

          if (!Params.BlockStHockeyFlag) continue;
          if ((newSelection.SportType != SportTypes.HOCKEY || wi.SportType != SportTypes.HOCKEY) || ((wagerItemTypeCode != WagerType.SPREAD || wi.WagerType != WagerType.TOTALPOINTS) && (wagerItemTypeCode != WagerType.TOTALPOINTS || wi.WagerType != WagerType.SPREAD))) continue;
          if (CheckForIntersectingPeriod(newSelection.SportType, newSelection.SportSubType,
            newSelection.PeriodNumber, wi.PeriodNumber)) {
            errorPending = true;
          }
        }
      }
      return errorPending;
    }

    public bool ContestIsAvailable(int contestNumber) {

      var contestIsAvailable = true;

      var contestStatus = (from p in ActiveContests where p.ContestNum == contestNumber select p.Status).FirstOrDefault();
      var periodWagerCutoff = (from p in ActiveContests where p.ContestNum == contestNumber select p.WagerCutoff).FirstOrDefault();

      switch (contestStatus) {
        case GamesAndLines.EVENT_COMPLETED:
        case GamesAndLines.EVENT_CANCELLED:
          contestIsAvailable = false;
          break;
        case GamesAndLines.EVENT_OPEN:
        case GamesAndLines.EVENT_CIRCLED:
        case GamesAndLines.EVENT_OFFLINE:
          contestIsAvailable = periodWagerCutoff == null || PeriodWagerCutoffBypassed((DateTime)periodWagerCutoff);
          break;
      }
      return contestIsAvailable;
    }

    public IfBet CreateIfBetWager(FrmPlaceIfBet frmPlaceIfBet, String frmMode, int ticketNumber, int wagerNumberToUpd, TextBox txtRisk, TextBox txtToWin, String wagerTypeName, String wagerType, Boolean isFreePlay, String layoffWager, String complexIfBetAmount = null) {
      Ticket.Wager wager;
      if (frmMode.ToUpper() != "EDIT") {
        PrepareIfBetStageWager(frmMode, ticketNumber, wagerNumberToUpd, txtRisk, txtToWin, wagerTypeName, wagerType, null, null, "N", null, isFreePlay, layoffWager, complexIfBetAmount);
        wager = Mdi.Ticket.Wagers.Last();
      }
      else {
        wager = (from w in Mdi.Ticket.Wagers where w.TicketNumber == ticketNumber && w.WagerNumber == wagerNumberToUpd select w).FirstOrDefault();
      }


      var ifBet = new IfBet {
        ModuleName = ParameterList.GetItem("ModuleName").Value,
        Number = wager != null ? wager.WagerNumber : 0,
        TicketNumber = wager != null ? wager.TicketNumber : 0
      };

      frmPlaceIfBet.IfBet = ifBet;
      return ifBet;
    }

    public Parlay CreateParlayWager(FrmPlaceParlay frmPlaceParlay, String frmMode, int ticketNumber, int wagerNumberToUpd, TextBox txtRisk, TextBox txtToWin, String wagerTypeName, String wagerType, Boolean isFreePlay, String layoffWager) {
      var parlay = new Parlay(AppModuleInfo) {
        ModuleName = ParameterList.GetItem("ModuleName").Value,
        Name = Customer.ParlayName.Trim(),
        CustomerMaxPayout = (Customer.ParlayMaxPayout ?? 0) == 0 ? AppModuleInfo.Parameters.MaxParlayPayout : Customer.ParlayMaxPayout ?? 0,
        ExchangeRate = (int)(Customer.ExchangeRate ?? 1),
        TicketNumber = ticketNumber
      };

      if (frmMode.ToUpper() != "EDIT") {
        PrepareParlayStageWager(frmMode, ticketNumber, wagerNumberToUpd, txtRisk, txtToWin, wagerTypeName, wagerType, Mdi.Customer.ParlayName, 0, Mdi.ParlayPayoutType, isFreePlay, layoffWager);
        parlay.AmountWagered = 0;
        parlay.Number = Mdi.Ticket.Wagers.Last(w => w.TicketNumber == ticketNumber).WagerNumber;
        frmPlaceParlay.Parlay = parlay;
        return parlay;
      }
      var wager = (from w in Mdi.Ticket.Wagers where w.TicketNumber == ticketNumber && w.WagerNumber == wagerNumberToUpd select w).FirstOrDefault();
      parlay.Frozen = true;

      if (Customer.ParlayMaxPayout == null) return null;
      if (wager != null && !string.IsNullOrEmpty(wager.FromPreviousTicket)) {
        parlay.IsOpen = true;
      }
      if (wager != null) {
        parlay.AmountWagered = wager.AmountWagered;
        parlay.Number = wager.WagerNumber;
        parlay.TotalPicks = wager.TotalPicks ?? 0;
      }
      frmPlaceParlay.Parlay = parlay;
      return parlay;
    }

    public Teaser CreateTeaserWager(FrmPlaceTeaser frmPlaceTeaser, String frmMode, int ticketNumber, int wagerNumberToUpd, TextBox txtRisk, TextBox txtToWin, String wagerTypeName,
                                String wagerType, Boolean isFreePlay, String layoffWager) {

      var teaser = new Teaser {
        TeaserName = FrmPlaceTeaser.TeaserName,
        TeaserTies = FrmPlaceTeaser.TeaserTies,
        MinPicks = FrmPlaceTeaser.MinPicks,
        MaxPicks = FrmPlaceTeaser.MaxPicks,
        Type = new WagerType(WagerType.TEASER, "Teaser"),
        TicketNumber = ticketNumber
      };

      if (frmMode.ToUpper() != "EDIT") {
        PrepareTeaserStageWager(frmMode, ticketNumber, wagerNumberToUpd, txtRisk, txtToWin, wagerTypeName, wagerType, teaser.TeaserName, teaser.MinPicks, teaser.TeaserTies, isFreePlay, layoffWager);
        teaser.AmountWagered = 0;
        teaser.Number = Mdi.Ticket.Wagers.Last(w => w.TicketNumber == ticketNumber).WagerNumber;
      }
      else {
        var wager = (from w in Mdi.Ticket.Wagers where w.TicketNumber == ticketNumber && w.WagerNumber == wagerNumberToUpd select w).FirstOrDefault();

        if (wager != null) {
          teaser.AmountWagered = wager.AmountWagered;
          teaser.Number = wager.WagerNumber;

          if (!string.IsNullOrEmpty(wager.FromPreviousTicket)) {
            teaser.IsOpen = true;
          }
        }
      }

      frmPlaceTeaser.Teaser = teaser;
      return teaser;
    }

    public Ticket.ContestWagerItem CreateStageContestWagerItem(String frmMode, int ticketNumber, int? wagerNumber, int? wagerItemNumberToUpd, double? amountWagered, spCnGetActiveContests_Result selectedContest, String selectedPriceName, String contestant, String wageringMode, String layOffWagerFlag, double selectedAdjustment, String selectedToBase = null, String thresholdLine = null) {
      if (wagerNumber != null && selectedContest != null) {
        var stageContestItem = new Ticket.ContestWagerItem { TicketNumber = ticketNumber, WagerNumber = (int)wagerNumber };

        if (frmMode == "Edit") {
          if (wagerItemNumberToUpd != null) stageContestItem.ItemNumber = (int)wagerItemNumberToUpd;
        }

        else {
          var itemsCnt = GetContestWagerItemsCount(ticketNumber, (int)wagerNumber);
          if (itemsCnt == null) {
            stageContestItem.ItemNumber = 1;
          }
          else {
            stageContestItem.ItemNumber = (int)itemsCnt + 1;
          }
        }

        stageContestItem.ContestNum = selectedContest.ContestNum;
        stageContestItem.ContestantNum = selectedContest.ContestantNum;
        stageContestItem.ContestDateTime = selectedContest.ContestDateTime;
        stageContestItem.ContestDesc = selectedContest.ContestDesc;
        stageContestItem.ContestantName = selectedContest.ContestantName;
        stageContestItem.Outcome = "P";
        stageContestItem.AmountWagered = amountWagered;
        stageContestItem.XtoYLineRep = selectedContest.XtoYLineRep;

        stageContestItem.Store = selectedContest.Store ?? "".Trim();
        stageContestItem.CustProfile = selectedContest.CustProfile ?? "".Trim();
        stageContestItem.WinPlaceOrShow = null;
        stageContestItem.OriginatingTicketNumber = null;

        stageContestItem.ThresholdType = selectedContest.ThresholdType;
        stageContestItem.ThresholdUnits = selectedContest.ThresholdUnits;
        stageContestItem.WagerCutoff = selectedContest.WagerCutoff;

        if (!string.IsNullOrEmpty(thresholdLine)) {
          stageContestItem.ThresholdLine = LinesFormat.GetNumericPoints(thresholdLine);
        }
        else {

          if (selectedContest.ThresholdLine != null)
            stageContestItem.ThresholdLine = selectedContest.ThresholdLine;
        }

        stageContestItem.ContestType = selectedContest.ContestType;
        stageContestItem.ContestType2 = selectedContest.ContestType2;
        stageContestItem.ContestType3 = selectedContest.ContestType3;
        stageContestItem.PercentBook = _currentCustomerPercentBook;

        stageContestItem.ShowOnChartFlag = TwUtilities.DetermineIfShowOnChart("Straight Bet", "", stageContestItem.ItemNumber);
        stageContestItem.LayoffFlag = layOffWagerFlag;
        stageContestItem.AgentId = _currentCustomerAgentId;
        stageContestItem.FreePlayFlag = IsFreePlay ? "Y" : "N";
        stageContestItem.WiseActionFlag = _currentCustomerIsWise;
        stageContestItem.PriceType = selectedPriceName.Substring(0, 1);

        if (selectedContest.MoneyLine != null && (double)selectedContest.MoneyLine != selectedAdjustment) {
          DecimalToFraction.Fraction fractionObj;
          switch (selectedPriceName) {
            case "American":
            case LineOffering.PRICE_AMERICAN:
              stageContestItem.FinalMoney = selectedAdjustment;
              stageContestItem.FinalDecimal = OddsTypes.UStoDecimal(selectedAdjustment, OddsDecimalPrecision);
              fractionObj = OddsTypes.DecToFraction(double.Parse(stageContestItem.FinalDecimal.ToString()), (OddsFractionMaxDenominator ?? 0));
              stageContestItem.FinalNumerator = fractionObj.Numerator;
              stageContestItem.FinalDenominator = fractionObj.Denominator;

              stageContestItem.OriginalMoney = stageContestItem.FinalMoney;
              stageContestItem.OriginalDecimal = stageContestItem.FinalDecimal;
              stageContestItem.OriginalNumerator = stageContestItem.FinalNumerator;
              stageContestItem.OriginalDenominator = stageContestItem.FinalDenominator;

              break;
            case "Decimal":
            case LineOffering.PRICE_DECIMAL:
              stageContestItem.FinalMoney = Math.Round(OddsTypes.DecimalToUs(selectedAdjustment));
              stageContestItem.FinalDecimal = double.Parse(selectedAdjustment.ToString(CultureInfo.InvariantCulture));
              fractionObj = OddsTypes.DecToFraction(double.Parse(stageContestItem.FinalDecimal.ToString()), (OddsFractionMaxDenominator ?? 0));
              stageContestItem.FinalNumerator = fractionObj.Numerator;
              stageContestItem.FinalDenominator = fractionObj.Denominator;

              stageContestItem.OriginalMoney = stageContestItem.FinalMoney;
              stageContestItem.OriginalDecimal = stageContestItem.FinalDecimal;
              stageContestItem.OriginalNumerator = stageContestItem.FinalNumerator;
              stageContestItem.OriginalDenominator = stageContestItem.FinalDenominator;
              break;
            case "Fractional":
            case LineOffering.PRICE_FRACTIONAL:
            case "Xtrange":
              stageContestItem.FinalNumerator = (int)selectedAdjustment;
              stageContestItem.FinalDenominator = int.Parse(selectedToBase);
              stageContestItem.FinalDecimal = OddsTypes.FractionalToDecimal((decimal)stageContestItem.FinalNumerator, (decimal)stageContestItem.FinalDenominator);
              stageContestItem.FinalMoney = Math.Round(OddsTypes.DecimalToUs((double)stageContestItem.FinalDecimal));

              stageContestItem.OriginalMoney = stageContestItem.FinalMoney;
              stageContestItem.OriginalDecimal = stageContestItem.FinalDecimal;
              stageContestItem.OriginalNumerator = stageContestItem.FinalNumerator;
              stageContestItem.OriginalDenominator = stageContestItem.FinalDenominator;
              break;
          }

          if (stageContestItem.XtoYLineRep == "Y") {
            stageContestItem.OriginalToBase = stageContestItem.FinalDenominator;
            stageContestItem.FinalToBase = stageContestItem.FinalDenominator;
          }
          else {
            stageContestItem.OriginalToBase = selectedContest.ToBase;
            stageContestItem.FinalToBase = selectedContest.ToBase;
          }
        }
        else {
          stageContestItem.OriginalMoney = selectedContest.MoneyLine;
          stageContestItem.OriginalDecimal = selectedContest.DecimalOdds;
          stageContestItem.OriginalNumerator = selectedContest.Numerator;
          stageContestItem.OriginalDenominator = selectedContest.Denominator;

          stageContestItem.FinalMoney = selectedContest.MoneyLine;
          stageContestItem.FinalDecimal = selectedContest.DecimalOdds;
          stageContestItem.FinalNumerator = selectedContest.Numerator;
          stageContestItem.FinalDenominator = selectedContest.Denominator;

          stageContestItem.OriginalToBase = selectedContest.ToBase;
          stageContestItem.FinalToBase = selectedContest.ToBase;
        }

        if (amountWagered != 0) {

          if (!string.IsNullOrEmpty(selectedToBase) && (selectedPriceName == "American" || selectedPriceName == LineOffering.PRICE_AMERICAN)) {
            stageContestItem.ToWinAmount = TwUtilities.DetermineXtoYToWinAmount(double.Parse(stageContestItem.AmountWagered.ToString()), (double)stageContestItem.FinalMoney, double.Parse(selectedToBase));
          }
          else {
            if (stageContestItem.XtoYLineRep == null || stageContestItem.XtoYLineRep == "N") {
              stageContestItem.ToWinAmount = LineOffering.DetermineToWinAmount(double.Parse(stageContestItem.AmountWagered.ToString()), (double)stageContestItem.FinalMoney, Params.IncludeCents);
            }
            else {
              stageContestItem.ToWinAmount = TwUtilities.DetermineXtoYToWinAmount(double.Parse(stageContestItem.AmountWagered.ToString()), (double)stageContestItem.FinalMoney, (double)selectedContest.ToBase);
            }
          }
        }

        var volAmount = new[] { stageContestItem.AmountWagered, stageContestItem.ToWinAmount };
        stageContestItem.VolumeAmount = volAmount.Min();

        stageContestItem.CompositeContestDesc = contestant;
        stageContestItem.WageringMode = wageringMode;
        stageContestItem.RotNum = selectedContest.RotNum ?? 0;

        if (Mdi.TicketNumber != stageContestItem.TicketNumber && Mdi.PendingContestItemInDb != null &&
             Mdi.PendingContestItemInDb.TicketNumber == stageContestItem.TicketNumber &&
             Mdi.PendingContestItemInDb.WagerNumber == stageContestItem.WagerNumber &&
            Mdi.PendingContestItemInDb.ItemNumber == stageContestItem.ItemNumber) {
          stageContestItem.PendingContestItemInDb = Mdi.PendingContestItemInDb;
        }

        return stageContestItem;
      }
      return null;
    }

    public void CreateStageWager(String frmMode, int ticketNumber, int wagerNumberToUpd, out int wagerNumberForWi, TextBox txtRisk, TextBox txtToWin, String wagerTypeName, String wagerType, String teaserName, String parlayName, int minPicks, int ties, int? roundRobinLink, int? aRBirdcageLink, int? aRLink, String continueOnPushFlag, int? boxLink, String parlayPayoutType, Boolean isFreePlay, String layoffFlag, String description = null, String complexIfBetAmount = null) {
      var stageWager = new Ticket.Wager { TicketNumber = ticketNumber };
      if (frmMode.ToUpper() == "EDIT") {
        stageWager.WagerNumber = wagerNumberToUpd;
        wagerNumberForWi = wagerNumberToUpd;
      }
      else {
        stageWager.WagerNumber = Mdi.Ticket.GetWagersCount(ticketNumber) + 1;
        wagerNumberForWi = stageWager.WagerNumber;
      }

      stageWager.LogDateTime = Mdi.GetCurrentDateTime();
      stageWager.AmountWagered = txtRisk.Text.Length > 0 ? double.Parse(txtRisk.Text) : 0;

      stageWager.WagerTypeName = wagerTypeName;
      switch (wagerTypeName) {
        case "Straight Bet":
          stageWager.WagerType = wagerType;
          break;
        case "Parlay":
          stageWager.WagerType = WagerType.PARLAY;
          break;
        case "Teaser":
          stageWager.WagerType = WagerType.TEASER;
          break;
        case "If-Bet":
          stageWager.WagerType = WagerType.IFBET;
          stageWager.SpecialIfBetAmt = complexIfBetAmount;
          break;
        case "Manual Play":
          stageWager.WagerType = WagerType.MANUALPLAY;
          break;
      }

      if (description != null)
        stageWager.Description = description;

      stageWager.WagerStatus = "P";
      stageWager.ToWinAmount = txtToWin.Text.Length > 0 ? double.Parse(txtToWin.Text) : 0;

      stageWager.TeaserName = teaserName;
      stageWager.ParlayName = parlayName;
      stageWager.MinimumPicks = minPicks;
      stageWager.Ties = ties;
      stageWager.RoundRobinLink = roundRobinLink;
      stageWager.ArBirdcageLink = aRBirdcageLink;
      stageWager.ArLink = aRLink;
      stageWager.ContinueOnPushFlag = continueOnPushFlag;
      stageWager.PlayNumber = stageWager.WagerNumber;
      stageWager.AcceptedDateTime = Mdi.GetCurrentDateTime();
      stageWager.BoxLink = boxLink;
      stageWager.LayoffFlag = layoffFlag;
      stageWager.MaxPayout = 0;
      stageWager.ParlayPayoutType = parlayPayoutType;
      stageWager.FreePlayFlag = isFreePlay ? "Y" : "N";

      var rifBackwardTicketNumber = "";
      var rifBackwardWagerNumber = "";
      String rifWinOnlyFlag = null;
      String rifCurrentPlayFlag = null;
      double? rifMaxReinvestAmt = null;

      if (Mdi.RollingIfBetMode && frmMode != "Edit") {
        if (Mdi.RifBTicketInfo.TicketNumber > 0) {
          rifBackwardTicketNumber = Mdi.RifBTicketInfo.TicketNumber.ToString(CultureInfo.InvariantCulture);
          rifBackwardWagerNumber = Mdi.RifBTicketInfo.WagerNumber.ToString(CultureInfo.InvariantCulture);
          rifWinOnlyFlag = Mdi.RifBTicketInfo.WinOnlyFlag;
          rifCurrentPlayFlag = Mdi.RifBTicketInfo.RifCurrentPlayFlag;
          if (Mdi.RifBTicketInfo.LimitRifToRisk == "N") {
            rifMaxReinvestAmt = Mdi.RifBTicketInfo.RifRiskAmount + Mdi.RifBTicketInfo.RifToWinAmount;
          }
          else {
            rifMaxReinvestAmt = Mdi.RifBTicketInfo.RifRiskAmount;
          }
        }
      }

      if (frmMode == "Edit") {
        if (Mdi.Ticket.Wagers != null) {
          var eW = (from w in Mdi.Ticket.Wagers where w.TicketNumber == ticketNumber && w.WagerNumber == wagerNumberToUpd select w).FirstOrDefault();
          if (eW != null && eW.WagerNumber > 0) {
            rifBackwardTicketNumber = eW.RifBackwardTicketNumber;
            rifBackwardWagerNumber = eW.RifBackwardWagerNumber;
            rifWinOnlyFlag = eW.RifWinOnlyFlag;
            rifCurrentPlayFlag = eW.RifCurrentPlayFlag;
            rifMaxReinvestAmt = eW.RifMaxReinvestAmt;
          }
        }
        else {
          //this could happen if editing a wager from the CustomerWagerDetails form.  New functionality. NOT the Open Parlays/Teasers functionality.
          if (Mdi.PendingWagerInDb != null && Mdi.PendingWagerInDb.TicketNumber == ticketNumber) {
            rifBackwardTicketNumber = (Mdi.PendingWagerInDb.RifBackwardTicketNumber ?? 0).ToString(CultureInfo.InvariantCulture);
            rifBackwardWagerNumber = (Mdi.PendingWagerInDb.RifBackwardWagerNumber ?? 0).ToString(CultureInfo.InvariantCulture);
            rifWinOnlyFlag = Mdi.PendingWagerInDb.RifWinOnlyFlag;
            rifCurrentPlayFlag = Mdi.PendingWagerInDb.RifCurrentPlayFlag;
            rifMaxReinvestAmt = ((Mdi.PendingWagerInDb.AmountWagered ?? 0) / 100) + ((Mdi.PendingWagerInDb.ToWinAmount ?? 0) / 100);
          }

        }
      }

      stageWager.RifBackwardTicketNumber = rifBackwardTicketNumber == "0" ? null : rifBackwardTicketNumber;
      stageWager.RifBackwardWagerNumber = rifBackwardWagerNumber == "0" ? null : rifBackwardWagerNumber;
      stageWager.RifWinOnlyFlag = rifWinOnlyFlag;
      stageWager.RifCurrentPlayFlag = rifCurrentPlayFlag;
      stageWager.RifMaxReinvestAmt = rifMaxReinvestAmt;
      if (Mdi.OpenPlayWagerInfo != null && Mdi.OpenPlayWagerInfo.TicketNumber == stageWager.TicketNumber && Mdi.OpenPlayWagerInfo.WagerNumber == stageWager.WagerNumber)
        stageWager.OpenPlayWagerInfo = Mdi.OpenPlayWagerInfo;

      if (frmMode == "Edit") {
        if (Mdi.Ticket.Wagers != null && Mdi.Ticket.Wagers.Any(w => w.TicketNumber == stageWager.TicketNumber && w.WagerNumber == stageWager.WagerNumber)) {
          Mdi.Ticket.UpdateWager(stageWager);
        }
        else {
          Mdi.Ticket.AddWager(stageWager);
        }
      }
      else {
        Mdi.Ticket.AddWager(stageWager);
      }
    }

    private Ticket.WagerItem CreateStageWagerItem(String frmMode, String selectedWagerTypeName, String selectedWagerType, String wagerTypeMode,
                                                            String selectedPriceName, int ticketNumber, int? wagerNumber,
                                                            int wagerItemNumberToUpd, /*int selectedGvRow,*/ int selectedRowNumberInGroup, String selectedLine,
                                                            String selectedPrice, String selectedOdds, string selectedBPointsOption,
                                                            String chosenTeamId, double? teaserPoints, String pitcher1IsReq,
                                                            String pitcher2IsReq, String oddsAreFixed, String totalsOorU,
                                                            double? amountWagered, String rifBackwardTicketNumber, String wagerItemDescription,
                                                            List<string> lineAndPoints,
                                                            spGLGetActiveGamesByCustomer_Result selectedGamePeriodInfo,
                                                            out String itemShortDescription, Boolean isFreePlay, String wageringMode, String layoffFlag,
            String easternLineEnabled, List<spGLGetEasternLineConversionInfo_Result> easternLineConversionInfo, String complexIfBetAmount = null) {
      itemShortDescription = "";
      if (wagerNumber == null) return null;
      var stageWagerItem = new Ticket.WagerItem { TicketNumber = ticketNumber, WagerNumber = (int)wagerNumber, Loo = selectedGamePeriodInfo };

      if (frmMode == "Edit") {
        stageWagerItem.ItemNumber = wagerItemNumberToUpd;
      }

      else {
        var itemsCnt = GetWagerItemsCount(ticketNumber, (int)wagerNumber);
        if (itemsCnt == null) {
          stageWagerItem.ItemNumber = 1;
        }
        else {
          stageWagerItem.ItemNumber = (int)itemsCnt + 1;
        }
      }

      stageWagerItem.GameNum = selectedGamePeriodInfo.GameNum;
      stageWagerItem.GameDateTime = selectedGamePeriodInfo.GameDateTime;
      stageWagerItem.SportType = selectedGamePeriodInfo.SportType.Trim();
      stageWagerItem.SportSubType = selectedGamePeriodInfo.SportSubType.Trim();
      stageWagerItem.TicketNumber = ticketNumber;
      stageWagerItem.WagerType = selectedWagerType;

      String line;
      String price;


      if (selectedRowNumberInGroup != 3) {
        line = selectedLine;
        price = selectedPrice;
      }
      else {
        line = "0";
        price = selectedLine;
      }

      stageWagerItem.ChosenTeamIdx = selectedRowNumberInGroup;
      stageWagerItem.ChosenTeamId = chosenTeamId;
      stageWagerItem.Outcome = "P";

      stageWagerItem.AmountWagered = amountWagered;
      stageWagerItem.ToWinAmount = 0;

      stageWagerItem.SelectedAmericanPrice = int.Parse(selectedPrice);

      double americanPrice = 0;
      double doubleParsed;

      if (amountWagered != 0) {
        if (easternLineEnabled == "Y" && selectedGamePeriodInfo.SportType == SportTypes.BASEBALL) {
          double easternOdds;
          if (double.TryParse(LineOffering.ConvertToStringBaseLine(selectedOdds), out easternOdds)) {
            americanPrice = OddsTypes.ConvertFromEasternToAmerican(easternOdds, easternLineConversionInfo);
          }

          double.TryParse(stageWagerItem.AmountWagered.ToString(), out doubleParsed);
          stageWagerItem.ToWinAmount = LineOffering.DetermineToWinAmount(doubleParsed, americanPrice, Params.IncludeCents);
        }
        else {
          americanPrice = double.Parse(price);
          double.TryParse(stageWagerItem.AmountWagered.ToString(), out doubleParsed);
          stageWagerItem.ToWinAmount = LineOffering.DetermineToWinAmount(doubleParsed, americanPrice, Params.IncludeCents);
        }
        stageWagerItem.SelectedAmericanPrice = int.Parse(americanPrice.ToString(CultureInfo.InvariantCulture));
      }
      stageWagerItem.TeaserPoints = teaserPoints;
      stageWagerItem.Store = selectedGamePeriodInfo.Store ?? "".Trim();
      stageWagerItem.CustProfile = selectedGamePeriodInfo.CustProfile ?? "".Trim();
      stageWagerItem.PeriodNumber = selectedGamePeriodInfo.PeriodNumber;
      stageWagerItem.PeriodDescription = selectedGamePeriodInfo.PeriodDescription;

      // check if the wager is open DMN 20131003
      var wager = Mdi.Ticket.Wagers.FirstOrDefault(w => w.TicketNumber == ticketNumber && w.WagerNumber == wagerNumber);

      if (wager != null && (wager.WagerStatus == "O" || wager.WagerStatus == "P") && !string.IsNullOrEmpty(wager.FromPreviousTicket)) {
        stageWagerItem.OriginatingTicketNumber = Mdi.Ticket.TicketNumber;
        stageWagerItem.AddToOpenSpot = true;
      }

      stageWagerItem.ListedPitcher1 = selectedGamePeriodInfo.ListedPitcher1;
      stageWagerItem.ListedPitcher2 = selectedGamePeriodInfo.ListedPitcher2;

      if (selectedGamePeriodInfo.SportType == SportTypes.BASEBALL) {
        stageWagerItem.Pitcher1ReqFlag = pitcher1IsReq;
        stageWagerItem.Pitcher2ReqFlag = pitcher2IsReq;

        stageWagerItem.AdjustableOddsFlag = oddsAreFixed == "Y" ? "N" : "Y";

      }
      else {
        stageWagerItem.Pitcher1ReqFlag = null;
        stageWagerItem.Pitcher2ReqFlag = null;
        stageWagerItem.AdjustableOddsFlag = "N";
      }

      stageWagerItem.PercentBook = _currentCustomerPercentBook;

      if (stageWagerItem.ToWinAmount == 0) {
        stageWagerItem.VolumeAmount = stageWagerItem.AmountWagered;
      }
      else {
        var volAmount = new[] { stageWagerItem.AmountWagered, stageWagerItem.ToWinAmount };
        stageWagerItem.VolumeAmount = volAmount.Min();
      }

      stageWagerItem.ShowOnChartFlag = TwUtilities.DetermineIfShowOnChart(selectedWagerTypeName, rifBackwardTicketNumber, stageWagerItem.ItemNumber);
      stageWagerItem.LayoffFlag = layoffFlag;
      stageWagerItem.AgentId = _currentCustomerAgentId;

      if (wageringMode == "A") {
        stageWagerItem.EasternLine = LineOffering.CheckEasternLineValue(_currentCustomerEastLineEnabled, stageWagerItem.SportType, price, _easternLineConversion);
      }

      stageWagerItem.FreePlayFlag = isFreePlay ? "Y" : "N";

      stageWagerItem.WiseActionFlag = SetWiseActionFlagValue(stageWagerItem.ItemNumber, selectedWagerTypeName);
      stageWagerItem.PriceType = selectedPriceName.Substring(0, 1);

      stageWagerItem.ChosenTeamIdx = selectedRowNumberInGroup;

      if (stageWagerItem.WagerType == WagerType.TOTALPOINTS || stageWagerItem.WagerType == WagerType.TEAMTOTALPOINTS)
        stageWagerItem.TotalPointsOu = totalsOorU;
      else
        stageWagerItem.TotalPointsOu = null;

      line = LineOffering.ConvertToStringBaseLine(line).Replace("+", "");


      stageWagerItem.Description = wagerItemDescription;


      DecimalToFraction.Fraction fractionAry;
      double realLine;
      switch (stageWagerItem.WagerType) {
        case WagerType.SPREAD:
          stageWagerItem.AdjSpread = double.TryParse(line, out realLine) ? realLine : double.Parse(selectedGamePeriodInfo.Spread.ToString());

          stageWagerItem.FinalMoney = int.Parse(price);
          stageWagerItem.AdjTotalPoints = 0;

          if (selectedGamePeriodInfo.Spread != null) {
            stageWagerItem.OrigSpread = double.Parse(selectedGamePeriodInfo.Spread.ToString());
          }
          stageWagerItem.OrigTotalPoints = null;

          var spreadAdj1 = selectedGamePeriodInfo.SpreadAdj1;
          var spreadAdj2 = selectedGamePeriodInfo.SpreadAdj2;

          if (wageringMode != WagerType.MONEYLINE) /*(Manual mode actually)*/ {

            switch (selectedRowNumberInGroup) {
              case 1:
                stageWagerItem.OrigMoney = spreadAdj1;

                stageWagerItem.OrigDecimal = double.Parse(selectedGamePeriodInfo.SpreadDecimal1.ToString());
                stageWagerItem.OrigNumerator = selectedGamePeriodInfo.SpreadNumerator1;
                stageWagerItem.OrigDenominator = selectedGamePeriodInfo.SpreadDenominator1;
                break;

              case 2:

                stageWagerItem.OrigMoney = spreadAdj2;

                stageWagerItem.OrigDecimal = double.Parse(selectedGamePeriodInfo.SpreadDecimal2.ToString());
                stageWagerItem.OrigNumerator = selectedGamePeriodInfo.SpreadNumerator2;
                stageWagerItem.OrigDenominator = selectedGamePeriodInfo.SpreadDenominator2;
                break;

              case 3:
                stageWagerItem.AdjSpread = 0;
                stageWagerItem.OrigMoney = selectedGamePeriodInfo.MoneyLineDraw;

                stageWagerItem.OrigDecimal = double.Parse(selectedGamePeriodInfo.MoneyLineDecimalDraw.ToString());
                stageWagerItem.OrigNumerator = selectedGamePeriodInfo.MoneyLineNumeratorDraw;
                stageWagerItem.OrigDenominator = selectedGamePeriodInfo.MoneyLineDenominatorDraw;
                break;
            }
          }

          Common.SetFinalMoney(stageWagerItem, price, wageringMode, OddsFractionMaxDenominator, OddsDecimalPrecision);
          break;

        case WagerType.MONEYLINE:

          if (wageringMode != "M") {
            if (selectedGamePeriodInfo.FavoredTeamID != null) {
              if (stageWagerItem.ChosenTeamId.Trim() == selectedGamePeriodInfo.FavoredTeamID.Trim()) {
                if (selectedGamePeriodInfo.Spread != null)
                  stageWagerItem.AdjSpread = double.Parse(selectedGamePeriodInfo.Spread.ToString());
              }
              else {
                if (selectedGamePeriodInfo.Spread != null)
                  stageWagerItem.AdjSpread = -1 * double.Parse(selectedGamePeriodInfo.Spread.ToString());
              }
            }
          }
          else {
            stageWagerItem.AdjSpread = 0;
          }

          stageWagerItem.FinalMoney = int.Parse(price);
          stageWagerItem.AdjTotalPoints = 0;

          stageWagerItem.OrigSpread = null;

          var moneyLine1 = selectedGamePeriodInfo.MoneyLine1;
          var moneyLine2 = selectedGamePeriodInfo.MoneyLine2;
          var moneyLineDraw = selectedGamePeriodInfo.MoneyLineDraw;

          if (wageringMode != "M") {
            switch (selectedRowNumberInGroup) {
              case 1:
                stageWagerItem.OrigMoney = moneyLine1;

                if (selectedGamePeriodInfo.MoneyLineDecimal1 != null) {
                  stageWagerItem.OrigDecimal = double.Parse(selectedGamePeriodInfo.MoneyLineDecimal1.ToString());
                }
                else {
                  double.TryParse(stageWagerItem.OrigMoney.ToString(), out doubleParsed);
                  stageWagerItem.OrigDecimal = OddsTypes.UStoDecimal(doubleParsed, OddsDecimalPrecision);
                }

                if (selectedGamePeriodInfo.MoneyLineNumerator1 != null) {
                  stageWagerItem.OrigNumerator = selectedGamePeriodInfo.MoneyLineNumerator1;
                }
                else {
                  double.TryParse(stageWagerItem.OrigDecimal.ToString(), out doubleParsed);
                  fractionAry = OddsTypes.DecToFraction(doubleParsed, (int)OddsFractionMaxDenominator);
                  stageWagerItem.OrigNumerator = fractionAry.Numerator;
                }

                if (selectedGamePeriodInfo.MoneyLineDenominator1 != null) {
                  stageWagerItem.OrigDenominator = selectedGamePeriodInfo.MoneyLineDenominator1;
                }
                else {
                  double.TryParse(stageWagerItem.OrigDecimal.ToString(), out doubleParsed);
                  fractionAry = OddsTypes.DecToFraction(doubleParsed, (int)OddsFractionMaxDenominator);
                  stageWagerItem.OrigDenominator = fractionAry.Denominator;
                }
                break;

              case 2:
                stageWagerItem.OrigMoney = moneyLine2;

                if (selectedGamePeriodInfo.MoneyLineDecimal2 != null) {
                  stageWagerItem.OrigDecimal = double.Parse(selectedGamePeriodInfo.MoneyLineDecimal2.ToString());
                }
                else {
                  double.TryParse(stageWagerItem.OrigMoney.ToString(), out doubleParsed);
                  stageWagerItem.OrigDecimal = OddsTypes.UStoDecimal(doubleParsed, OddsDecimalPrecision);
                }

                if (selectedGamePeriodInfo.MoneyLineNumerator2 != null) {
                  stageWagerItem.OrigNumerator = selectedGamePeriodInfo.MoneyLineNumerator2;
                }
                else {
                  double.TryParse(stageWagerItem.OrigDecimal.ToString(), out doubleParsed);
                  fractionAry = OddsTypes.DecToFraction(doubleParsed, (int)OddsFractionMaxDenominator);
                  stageWagerItem.OrigNumerator = fractionAry.Numerator;
                }

                if (selectedGamePeriodInfo.MoneyLineDenominator2 != null) {
                  stageWagerItem.OrigDenominator = selectedGamePeriodInfo.MoneyLineDenominator2;
                }
                else {
                  double.TryParse(stageWagerItem.OrigDecimal.ToString(), out doubleParsed);
                  fractionAry = OddsTypes.DecToFraction(doubleParsed, (int)OddsFractionMaxDenominator);
                  stageWagerItem.OrigDenominator = fractionAry.Denominator;
                }
                break;

              case 3:
                stageWagerItem.AdjSpread = 0;
                stageWagerItem.OrigTotalPoints = 0;
                stageWagerItem.OrigMoney = moneyLineDraw;

                if (selectedGamePeriodInfo.MoneyLineDecimalDraw != null) {
                  stageWagerItem.OrigDecimal = double.Parse(selectedGamePeriodInfo.MoneyLineDecimalDraw.ToString());
                }
                else {
                  double.TryParse(stageWagerItem.OrigMoney.ToString(), out doubleParsed);
                  stageWagerItem.OrigDecimal = OddsTypes.UStoDecimal(doubleParsed, OddsDecimalPrecision);
                }

                if (selectedGamePeriodInfo.MoneyLineNumeratorDraw != null) {
                  stageWagerItem.OrigNumerator = selectedGamePeriodInfo.MoneyLineNumeratorDraw;
                }
                else {
                  double.TryParse(stageWagerItem.OrigDecimal.ToString(), out doubleParsed);
                  fractionAry = OddsTypes.DecToFraction(doubleParsed, (int)OddsFractionMaxDenominator);
                  stageWagerItem.OrigNumerator = fractionAry.Numerator;
                }

                if (selectedGamePeriodInfo.MoneyLineDenominatorDraw != null) {
                  stageWagerItem.OrigDenominator = selectedGamePeriodInfo.MoneyLineDenominatorDraw;
                }
                else {
                  double.TryParse(stageWagerItem.OrigDecimal.ToString(), out doubleParsed);
                  fractionAry = OddsTypes.DecToFraction(doubleParsed, (int)OddsFractionMaxDenominator);
                  stageWagerItem.OrigDenominator = fractionAry.Denominator;
                }
                break;
            }
          }

          Common.SetFinalMoney(stageWagerItem, price, wageringMode, OddsFractionMaxDenominator, OddsDecimalPrecision);
          break;

        case WagerType.TOTALPOINTS:
          if (wageringMode != "M") {
            if (stageWagerItem.TotalPointsOu == "O") {
              if (selectedGamePeriodInfo.FavoredTeamID != null) {
                if (selectedGamePeriodInfo.Team1ID.Trim() == selectedGamePeriodInfo.FavoredTeamID.Trim()) {
                  if (selectedGamePeriodInfo.Spread != null)
                    stageWagerItem.AdjSpread = double.Parse(selectedGamePeriodInfo.Spread.ToString());
                }
                else {
                  if (selectedGamePeriodInfo.Spread != null)
                    stageWagerItem.AdjSpread = -1 * double.Parse(selectedGamePeriodInfo.Spread.ToString());
                }
              }
            }
            else {
              if (selectedGamePeriodInfo.FavoredTeamID != null) {
                if (selectedGamePeriodInfo.Team2ID.Trim() == selectedGamePeriodInfo.FavoredTeamID.Trim()) {
                  if (selectedGamePeriodInfo.Spread != null) {
                    stageWagerItem.AdjSpread = double.Parse(selectedGamePeriodInfo.Spread.ToString());
                  }
                }
                else {
                  if (selectedGamePeriodInfo.Spread != null)
                    stageWagerItem.AdjSpread = -1 * double.Parse(selectedGamePeriodInfo.Spread.ToString());
                }
              }
            }
          }
          else {
            stageWagerItem.AdjSpread = 0;
          }

          stageWagerItem.AdjTotalPoints = double.TryParse(line, out realLine) ? realLine : double.Parse(selectedGamePeriodInfo.TotalPoints.ToString());
          stageWagerItem.FinalMoney = int.Parse(price);
          stageWagerItem.OrigSpread = null;

          var ttlPtsAdj1 = selectedGamePeriodInfo.TtlPtsAdj1;
          var ttlPtsAdj2 = selectedGamePeriodInfo.TtlPtsAdj2;

          if (wageringMode != "M") {
            stageWagerItem.OrigTotalPoints = double.Parse(selectedGamePeriodInfo.TotalPoints.ToString());

            if (stageWagerItem.TotalPointsOu == "O") {
              stageWagerItem.OrigMoney = ttlPtsAdj1;

              stageWagerItem.OrigDecimal = double.Parse(selectedGamePeriodInfo.TtlPointsDecimal1.ToString());
              stageWagerItem.OrigNumerator = selectedGamePeriodInfo.TtlPointsNumerator1;
              stageWagerItem.OrigDenominator = selectedGamePeriodInfo.TtlPointsDenominator1;
            }
            else {
              stageWagerItem.OrigMoney = ttlPtsAdj2;
              stageWagerItem.OrigDecimal = double.Parse(selectedGamePeriodInfo.TtlPointsDecimal2.ToString());
              stageWagerItem.OrigNumerator = selectedGamePeriodInfo.TtlPointsNumerator2;
              stageWagerItem.OrigDenominator = selectedGamePeriodInfo.TtlPointsDenominator2;
            }
          }
          else {
            stageWagerItem.OrigTotalPoints = stageWagerItem.AdjTotalPoints;
          }

          Common.SetFinalMoney(stageWagerItem, price, wageringMode, OddsFractionMaxDenominator, OddsDecimalPrecision);
          break;

        case WagerType.TEAMTOTALPOINTS:

          stageWagerItem.AdjTotalPoints = double.TryParse(line, out realLine) ? realLine : double.Parse(selectedGamePeriodInfo.TotalPoints.ToString());
          stageWagerItem.FinalMoney = int.Parse(price);

          if (wageringMode != "M") {
            if (selectedGamePeriodInfo.FavoredTeamID != null) {
              if (stageWagerItem.ChosenTeamId.Trim() == selectedGamePeriodInfo.FavoredTeamID.Trim())
                if (selectedGamePeriodInfo.Spread != null) {
                  stageWagerItem.AdjSpread = double.Parse(selectedGamePeriodInfo.Spread.ToString());
                }
            }
            else
              if (selectedGamePeriodInfo.Spread != null) {
                stageWagerItem.AdjSpread = -1 * double.Parse(selectedGamePeriodInfo.Spread.ToString());
              }

          }
          else {
            stageWagerItem.AdjSpread = 0;
          }

          if (stageWagerItem.ChosenTeamId.Trim() == selectedGamePeriodInfo.Team1ID.Trim()) {


            if (wageringMode != "M") {
              stageWagerItem.OrigTotalPoints = double.Parse(selectedGamePeriodInfo.Team1TotalPoints.ToString());
              if (stageWagerItem.TotalPointsOu == "O") {
                stageWagerItem.OrigDecimal = double.Parse(selectedGamePeriodInfo.Team1TtlPtsDecimal1.ToString());
                stageWagerItem.OrigNumerator = int.Parse(selectedGamePeriodInfo.Team1TtlPtsNumerator1.ToString());
                stageWagerItem.OrigDenominator = int.Parse(selectedGamePeriodInfo.Team1TtlPtsDenominator1.ToString());
              }
              else {
                stageWagerItem.OrigDecimal = double.Parse(selectedGamePeriodInfo.Team1TtlPtsDecimal2.ToString());
                stageWagerItem.OrigNumerator = int.Parse(selectedGamePeriodInfo.Team1TtlPtsNumerator2.ToString());
                stageWagerItem.OrigDenominator = int.Parse(selectedGamePeriodInfo.Team1TtlPtsDenominator2.ToString());
              }
            }
            else {
              stageWagerItem.OrigTotalPoints = stageWagerItem.AdjTotalPoints;
            }
          }
          else {

            if (wageringMode != "M") {
              stageWagerItem.OrigTotalPoints = double.Parse(selectedGamePeriodInfo.Team2TotalPoints.ToString());
              if (stageWagerItem.TotalPointsOu == "O") {
                stageWagerItem.OrigDecimal = double.Parse(selectedGamePeriodInfo.Team2TtlPtsDecimal1.ToString());
                stageWagerItem.OrigNumerator = int.Parse(selectedGamePeriodInfo.Team2TtlPtsNumerator1.ToString());
                stageWagerItem.OrigDenominator = int.Parse(selectedGamePeriodInfo.Team2TtlPtsDenominator1.ToString());
              }
              else {
                stageWagerItem.OrigDecimal = double.Parse(selectedGamePeriodInfo.Team2TtlPtsDecimal2.ToString());
                stageWagerItem.OrigNumerator = int.Parse(selectedGamePeriodInfo.Team2TtlPtsNumerator2.ToString());
                stageWagerItem.OrigDenominator = int.Parse(selectedGamePeriodInfo.Team2TtlPtsDenominator2.ToString());
              }
            }
            else {
              stageWagerItem.OrigTotalPoints = stageWagerItem.AdjTotalPoints;
            }
          }


          if (stageWagerItem.FinalDecimal != stageWagerItem.OrigDecimal || wageringMode == "M") {
            double.TryParse(price, out doubleParsed);
            stageWagerItem.FinalDecimal = OddsTypes.UStoDecimal(doubleParsed, OddsDecimalPrecision);
            double.TryParse(stageWagerItem.FinalDecimal.ToString(), out doubleParsed);
            fractionAry = OddsTypes.DecToFraction(doubleParsed, (int)OddsFractionMaxDenominator);
            stageWagerItem.FinalNumerator = fractionAry.Numerator;
            stageWagerItem.FinalDenominator = fractionAry.Denominator;
          }
          else {
            stageWagerItem.FinalDecimal = stageWagerItem.OrigDecimal;
            stageWagerItem.FinalNumerator = stageWagerItem.OrigNumerator;
            stageWagerItem.FinalDenominator = stageWagerItem.OrigDenominator;
          }

          stageWagerItem.OrigSpread = null;
          stageWagerItem.OrigMoney = int.Parse(price);
          break;
      }

      if (wageringMode == "M") {
        stageWagerItem.OrigMoney = stageWagerItem.FinalMoney;
        stageWagerItem.OrigDecimal = stageWagerItem.FinalDecimal;
        stageWagerItem.OrigNumerator = stageWagerItem.FinalNumerator;
        stageWagerItem.OrigDenominator = stageWagerItem.FinalDenominator;
      }

      var item = WagerType.GetFromCode(stageWagerItem.WagerType);

      //stageWagerItem.SelectedRow = selectedGvRow;
      stageWagerItem.SelectedCell = TwUtilities.DetermineSelectedCell(item, totalsOorU);
      stageWagerItem.SelectedOdds = selectedOdds;
      stageWagerItem.SelectedBPointsOption = selectedBPointsOption;

      switch (selectedWagerTypeName) {
        case "Straight Bet":
          stageWagerItem.ShortDescription = stageWagerItem.Description;
          stageWagerItem.FreeHalfPointGiven = ApplyHalfPointAdjustment(stageWagerItem, frmMode);

          if (stageWagerItem.FreeHalfPointGiven) {
            var fhPointAmericanPrice = stageWagerItem.SelectedAmericanPrice.ToString(CultureInfo.InvariantCulture);
            var fhPointLine = stageWagerItem.WagerType == WagerType.SPREAD ? stageWagerItem.AdjSpread.ToString() : stageWagerItem.AdjTotalPoints.ToString();

            var fhPointOddsAreFixed = "";

            if (stageWagerItem.SportType != null && stageWagerItem.SportType.Trim() == SportTypes.BASEBALL) {
              fhPointOddsAreFixed = stageWagerItem.AdjustableOddsFlag == "N" ? "Y" : "N";
            }

            double fhPointsAdjustedBpLine = 0;

            if (!string.IsNullOrEmpty(stageWagerItem.SelectedBPointsOption)) {
              double.TryParse(LineOffering.ConvertToStringBaseLine(stageWagerItem.SelectedBPointsOption).Replace("+", ""), out fhPointsAdjustedBpLine);
            }

            stageWagerItem.ShortDescription = stageWagerItem.Description = BuildWagerDescription(TwUtilities.GetChosenTeamRotNumber(stageWagerItem.Loo, stageWagerItem.ChosenTeamId), fhPointLine, fhPointAmericanPrice, stageWagerItem.WagerType, stageWagerItem.TotalPointsOu, stageWagerItem.ChosenTeamId, stageWagerItem.SportType, stageWagerItem.Loo, "Straight Bet", stageWagerItem.PriceType, TwUtilities.GetOriginalLine(stageWagerItem.WagerType, stageWagerItem.Loo, stageWagerItem.ChosenTeamId, stageWagerItem.RowGroupNum), fhPointsAdjustedBpLine, stageWagerItem.Pitcher1ReqFlag, stageWagerItem.Pitcher2ReqFlag, fhPointOddsAreFixed, true);
          }

          break;
        case "Parlay":
          stageWagerItem.ShortDescription = stageWagerItem.Description;
          break;
        case "Teaser":
          stageWagerItem.ShortDescription = TwUtilities.BuildDescriptionAndAdjustedPrice(selectedGamePeriodInfo, stageWagerItem.WagerType, stageWagerItem.TotalPointsOu, stageWagerItem.ChosenTeamId.Trim(), lineAndPoints[0].Replace(".5", GamesAndLines.HALFSYMBOL), stageWagerItem.PeriodDescription);
          break;
        case "If-Bet":
          stageWagerItem.ShortDescription = stageWagerItem.Description;
          break;
      }

      itemShortDescription = stageWagerItem.ShortDescription;
      stageWagerItem.WagerTypeMode = wagerTypeMode;
      stageWagerItem.WageringMode = wageringMode;
      stageWagerItem.RowGroupNum = selectedRowNumberInGroup;
      stageWagerItem.PeriodWagerCutoff = (DateTime)selectedGamePeriodInfo.PeriodWagerCutoff;
      stageWagerItem.ComplexIfBetAmount = complexIfBetAmount;

      return stageWagerItem;
    }


    public Ticket.WagerItem CreateStageWagerItemFromContestItem(String frmMode, String selectedWagerTypeName, String selectedWagerType, String wagerTypeMode,
                                                          String selectedPriceName, int ticketNumber, int? wagerNumber,
                                                          int wagerItemNumberToUpd,
                                                          String selectedLine,
                                                          double? amountWagered, String rifBackwardTicketNumber, String wagerItemDescription,
                                                          Ticket.ContestWagerItem contestWagerItemInfo,
                                                          out String itemShortDescription, Boolean isFreePlay, String wageringMode, String layoffFlag) {
      itemShortDescription = "";
      if (wagerNumber == null) return null;
      var stageWagerItem = new Ticket.WagerItem { TicketNumber = ticketNumber, WagerNumber = (int)wagerNumber, ItemNumber = frmMode == "Edit" ? wagerItemNumberToUpd : contestWagerItemInfo.ItemNumber, GameNum = contestWagerItemInfo.ContestNum, ContestantNum = contestWagerItemInfo.ContestantNum, GameDateTime = contestWagerItemInfo.ContestDateTime, SportType = "", SportSubType = "", WagerType = selectedWagerType };



      var price = selectedLine;


      stageWagerItem.ChosenTeamIdx = 1;
      stageWagerItem.ChosenTeamId = contestWagerItemInfo.ContestantName.Trim();
      stageWagerItem.Outcome = "P";

      stageWagerItem.AmountWagered = amountWagered;
      stageWagerItem.ToWinAmount = 0;

      stageWagerItem.SelectedAmericanPrice = int.Parse(selectedLine.Replace(",", ""));

      stageWagerItem.Store = contestWagerItemInfo.Store ?? "".Trim();
      stageWagerItem.CustProfile = contestWagerItemInfo.CustProfile ?? "".Trim();
      stageWagerItem.PeriodNumber = 0;
      stageWagerItem.PeriodDescription = "Game";

      // check if the wager is open DMN 20131003
      var wager = Mdi.Ticket.Wagers.FirstOrDefault(w => w.TicketNumber == ticketNumber && w.WagerNumber == wagerNumber);

      if ((wager != null && wager.WagerStatus == "O" && !string.IsNullOrEmpty(wager.FromPreviousTicket))
          || (wager != null && wager.WagerStatus == "P" && !string.IsNullOrEmpty(wager.FromPreviousTicket)))
      {
        stageWagerItem.OriginatingTicketNumber = Mdi.Ticket.TicketNumber;
        stageWagerItem.AddToOpenSpot = true;
      }

      stageWagerItem.Pitcher1ReqFlag = null;
      stageWagerItem.Pitcher2ReqFlag = null;
      stageWagerItem.AdjustableOddsFlag = "N";

      stageWagerItem.PercentBook = _currentCustomerPercentBook;

      if (stageWagerItem.ToWinAmount == 0) {
        stageWagerItem.VolumeAmount = stageWagerItem.AmountWagered;
      }
      else {
        var volAmount = new[] { stageWagerItem.AmountWagered, stageWagerItem.ToWinAmount };
        stageWagerItem.VolumeAmount = volAmount.Min();
      }

      stageWagerItem.ShowOnChartFlag = TwUtilities.DetermineIfShowOnChart(selectedWagerTypeName, rifBackwardTicketNumber, stageWagerItem.ItemNumber);
      stageWagerItem.LayoffFlag = layoffFlag;
      stageWagerItem.AgentId = _currentCustomerAgentId;

      stageWagerItem.FreePlayFlag = isFreePlay ? "Y" : "N";

      stageWagerItem.WiseActionFlag = SetWiseActionFlagValue(stageWagerItem.ItemNumber, selectedWagerTypeName);
      stageWagerItem.PriceType = selectedPriceName.Substring(0, 1);

      stageWagerItem.ChosenTeamIdx = 1;

      stageWagerItem.TotalPointsOu = null;

      stageWagerItem.Description = wagerItemDescription;

      stageWagerItem.AdjSpread = 0;
      stageWagerItem.AdjTotalPoints = 0;

      stageWagerItem.OrigMoney = (int?)(contestWagerItemInfo.OriginalMoney ?? 0);
      stageWagerItem.OrigDecimal = contestWagerItemInfo.OriginalDecimal;
      stageWagerItem.OrigNumerator = contestWagerItemInfo.OriginalNumerator;
      stageWagerItem.OrigDenominator = contestWagerItemInfo.OriginalDenominator;

      stageWagerItem.FinalMoney = (int?)(contestWagerItemInfo.FinalMoney ?? 0);
      stageWagerItem.FinalDecimal = contestWagerItemInfo.FinalDecimal;
      stageWagerItem.FinalNumerator = contestWagerItemInfo.FinalNumerator;
      stageWagerItem.FinalDenominator = contestWagerItemInfo.FinalDenominator;

      if (wageringMode == "M") {
        stageWagerItem.OrigMoney = stageWagerItem.FinalMoney;
        stageWagerItem.OrigDecimal = stageWagerItem.FinalDecimal;
        stageWagerItem.OrigNumerator = stageWagerItem.FinalNumerator;
        stageWagerItem.OrigDenominator = stageWagerItem.FinalDenominator;
      }

      var item = WagerType.GetFromCode(stageWagerItem.WagerType);

      //stageWagerItem.SelectedRow = 1;
      stageWagerItem.SelectedCell = TwUtilities.DetermineSelectedCell(item, "");
      stageWagerItem.SelectedOdds = price;
      stageWagerItem.SelectedBPointsOption = "";
      stageWagerItem.ShortDescription = stageWagerItem.Description;

      itemShortDescription = stageWagerItem.ShortDescription;
      stageWagerItem.WagerTypeMode = wagerTypeMode;
      stageWagerItem.WageringMode = wageringMode;
      stageWagerItem.RowGroupNum = 1;
      stageWagerItem.PeriodWagerCutoff = stageWagerItem.PeriodWagerCutoff;
      stageWagerItem.ComplexIfBetAmount = "";
      stageWagerItem.IsContest = true;
      stageWagerItem.PendingPlayItemInDb = stageWagerItem.PendingPlayItemInDb;

      stageWagerItem.Loo = GetLooLikeForContest(stageWagerItem);

      return stageWagerItem;
    }

    private spGLGetActiveGamesByCustomer_Result GetLooLikeForContest(Ticket.WagerItem wi) {
      var loo = new spGLGetActiveGamesByCustomer_Result();

      spCnGetActiveContests_Result cLoo = (from c in ActiveContests where c.ContestNum == (wi.GameNum ?? 0) && c.ContestantNum == (wi.ContestantNum ?? 0) select c).FirstOrDefault();

      if (cLoo == null)
        return null;

      loo.SportType = "";
      loo.SportSubType = "";
      loo.PeriodNumber = 0;
      loo.CircledMaxWagerMoneyLine = cLoo.CircledMaxWager;
      loo.CircledMaxWagerMoneyLineType = "L";
      loo.CircledMaxWagerSpread = 0;
      loo.CircledMaxWagerSpreadType = "L";
      loo.CircledMaxWagerTeamTotal = 0;
      loo.CircledMaxWagerTeamTotalType = "L";
      loo.CircledMaxWagerTotalType = "L";
      loo.CircledMaxWagerTotal = 0;
      loo.Status = cLoo.Status;
      loo.GameNum = cLoo.ContestNum;

      wi.ContestItemWagerLimit = GetLimitForCurrentContestWager(cLoo);

      return loo;
    }


    private bool ApplyHalfPointAdjustment(Ticket.WagerItem wi, String frmMode) {

      if (frmMode == "New" && Mdi.Ticket.WagerItems != null) {
        var itemsInReadbackCount = (from i in Mdi.Ticket.WagerItems where i.TicketNumber == wi.TicketNumber && i.GameNum == wi.GameNum && i.PeriodNumber == 0 && i.WagerType == wi.WagerType select i).Count();

        if (itemsInReadbackCount > 0 && Customer.HalfPointWagerLimitFlag != "Y")
          return false;
      }

      var betsOnReadBack = PreviousBetsOnReadbackForGame(wi.Loo.GameNum, wi);

      var hpa = new HalfPointAdjustment(AppModuleInfo, Customer.CustomerID, ServerDateTime, wi.Loo.GameNum, wi.Loo.PeriodNumber, wi.Loo.SportType, wi.Loo.SportSubType,
        wi.WagerType, wi.TotalPointsOu, frmMode, Customer.HalfPointMaxBet, wi.Loo.PreventPointBuyingFlag, betsOnReadBack, Customer.HalfPointDenyTotalsFlag, Customer.HalfPointWagerLimitFlag,
        Customer.HalfPointCuBasketballFlag, Customer.HalfPointCuBasketballDow ?? -1, Customer.HalfPointCuFootballFlag, Customer.HalfPointCuFootballDow ?? -1,
        Customer.HalfPointFootballOn3Flag, Customer.HalfPointFootballOff3Flag, Customer.HalfPointFootballOn7Flag, Customer.HalfPointFootballOff7Flag) {
          RiskAmount = wi.AmountWagered,
          ToWinAmount = wi.ToWinAmount,
          AdjSpread = wi.AdjSpread,
          AdjTotalPoints = wi.AdjTotalPoints
        };

      var ret = hpa.ApplyHalfPointAdjustment();
      if (!ret) return false;
      wi.AdjSpread = hpa.AdjSpread;
      wi.AdjTotalPoints = hpa.AdjTotalPoints;
      wi.FreeHalfPointNewWager = hpa.FreeHalfPointNewWager;
      return true;

    }

    private double PreviousBetsOnReadbackForGame(int gameNum, Ticket.WagerItem wi) {
        if (Mdi.Ticket.WagerItems == null || Mdi.Ticket.WagerItems.Count == 0)
        return 0;
        return (Mdi.Ticket.Wagers.Where(w => w.TicketNumber == wi.TicketNumber)
            .Where(w => w.WagerType == wi.WagerType)
            .Select(w => w.WagerNumber)
            .SelectMany(
                wagerNumber =>
                    Mdi.Ticket.WagerItems.Where(
                        i =>
                            i.FreeHalfPointGiven && i.GameNum == gameNum && i.WagerType == wi.WagerType &&
                            i.WagerNumber == wagerNumber),
                (wagerNumber, item) => item != null ? Math.Min(item.AmountWagered ?? 0, item.ToWinAmount ?? 0) : 0)).Sum();
    }

    public void DisableGamePeriodsRadBtns() {
      if (panGamePeriodSelection == null) return;
      foreach (var ctrl in panGamePeriodSelection.Controls.Cast<Control>().Where(ctrl => ctrl.GetType().ToString() == "System.Windows.Forms.RadioButton")) {
        ctrl.Enabled = false;
        ((RadioButton)ctrl).Checked = false;
      }
    }

    public void FillAvailableSportsTree(Boolean includeProps) {
      if (treSports.Nodes.Count > 0)
        treSports.Nodes.Clear();

      _gamesAndProps = includeProps ? new GamesAndPropsTreeView(treSports, ActiveGames, ActiveContests) :
        new GamesAndPropsTreeView(treSports, ActiveGames);

      _gamesAndProps.PopulateGamesAndContestsTree(Mdi.ParameterList.GetItem("ModuleName").Value);

    }

    public void FillContestsGridView(List<ContestTreeNodeDesc> nodesInfo) {
      if (nodesInfo == null)
        return;

      if (nodesInfo[0].CorrelationId == null) {
        _filteredActiveContests = GetLinesForSelectedContest(nodesInfo);
      }
      else {
        if (ActiveContests == null) return;
        _filteredActiveContests = ActiveContests.Where(
            p =>
                p != null && (p.Status != null && ((p.CorrelationID ?? "").Trim() == nodesInfo[0].CorrelationId.Trim() &&
                                                   (p.Status != GamesAndLines.EVENT_OFFLINE && p.Status != GamesAndLines.EVENT_CANCELLED &&
                                                    p.Status != GamesAndLines.EVENT_COMPLETED)))).OrderBy(p => p.FirstRotNum).ThenBy(p => p.ContestType).ThenBy(p => p.ContestType2).ThenBy(p => p.ContestType3).ThenBy(p => p.ContestNum).ThenBy(p => p.RotNum).ThenBy(p => p.ContestantSeq).ThenBy(p => p.LineSeq).ToList();
      }

      if (dgvwPropsAndContests.Rows.Count > 0) {
        dgvwPropsAndContests.Rows.Clear();
      }


      if (_filteredActiveContests != null) {
        String holdFullContestDesc = null;
        int? holdContestantNum = 0;
        foreach (var row in _filteredActiveContests) {
          var fullContestDesc = row.ContestType.Trim() + row.ContestType2.Trim() + row.ContestType3.Trim() + row.ContestDesc.Trim();
          var writeContestDesc = holdFullContestDesc != fullContestDesc;

          if (holdContestantNum != row.ContestantNum)
            AddNewContestToGridView(writeContestDesc, row);
          holdFullContestDesc = row.ContestType.Trim() + row.ContestType2.Trim() + row.ContestType3.Trim() + row.ContestDesc.Trim();
          holdContestantNum = row.ContestantNum;
        }

        FormF.PreventDataGridViewColumnsToBeSortedAndResized(dgvwPropsAndContests);
        dgvwPropsAndContests.CurrentCell = null;
        dgvwPropsAndContests.AllowUserToResizeColumns = true;
      }

      var actionToTake = chbActiveOnly.Checked ? "Hide" : "Show";
      ShowHideInactiveGames(actionToTake);
    }

    public void FillGamesGridView(List<SelectedSportBranch> selection, DateTime? lastCallDateTime, int? periodNumber, String likeClause = null, String correlationId = null, String periodDescription = null) {

      if (!Mdi.ShowingLatestLines && !Mdi.ShowingLikeTeams) {
        if (correlationId == null && selection != null && selection[0].GameNumber == null) {
          FilteredActiveGames = selection[0].SportSubType != "" ?
            GetSortedGames(ActiveGames.Where(s => s.GameDateTime != null && (!string.IsNullOrEmpty(s.SportType) && s.SportType.Trim() == selection[0].SportType) && (s.SportSubType != null && s.SportSubType.Trim() == selection[0].SportSubType) && (s.ScheduleDate >= selection[0].StartDateTime && s.ScheduleDate < selection[0].EndDateTime) && s.PeriodNumber == selection[0].PeriodNumber && s.Status != GamesAndLines.EVENT_CANCELLED))
            : GetSortedGames(ActiveGames.Where(s => s.GameDateTime != null && (!string.IsNullOrEmpty(s.SportType) && s.SportType.Trim() == selection[0].SportType) && (s.ScheduleDate >= selection[0].StartDateTime && s.ScheduleDate < selection[0].EndDateTime) && s.PeriodNumber == selection[0].PeriodNumber && s.Status != GamesAndLines.EVENT_CANCELLED));
        }
        else {
          if (selection != null && selection[0].GameNumber == null) {
            FilteredActiveGames = ActiveGames.Where(s => s.GameDateTime != null && s.PeriodNumber == periodNumber && (s.CorrelationID != null && s.CorrelationID.Trim() == correlationId)).
                                                ToList();
          }
          else {
            if (selection != null) {
              FilteredActiveGames =
                  ActiveGames.Where(
                      s =>
                      s.GameDateTime != null && s.GameNum == selection[0].GameNumber &&
                      s.PeriodNumber == periodNumber).
                              OrderBy(s => s.PeriodNumber).
                              ThenByDescending(s => s.CustProfile).ToList();
            }
          }
        }
      }
      else {
        if (Mdi.ShowingLatestLines && lastCallDateTime != null && periodNumber != null && periodDescription != null) {
          FilteredActiveGames = GetSortedGames(ActiveGames.Where(s => s.GameDateTime != null &&
              s.PeriodDescription != null && periodDescription.Contains(s.PeriodDescription.Trim()) &&
              (s.LastSpreadChange > lastCallDateTime || s.LastMoneyLineChange > lastCallDateTime ||
              s.LastTeamPtsChange > lastCallDateTime || s.LastTtlPtsChange > lastCallDateTime) && s.PeriodNumber == (int)periodNumber &&
                        (s.Status != GamesAndLines.EVENT_CANCELLED && s.Status != GamesAndLines.EVENT_COMPLETED)));
        }
        else {
          if (Mdi.ShowingLikeTeams && likeClause != null && periodNumber != null && periodDescription != null) {
            FilteredActiveGames = GetSortedGames(ActiveGames.Where(s => s.GameDateTime != null && s.PeriodNumber == (int)periodNumber && s.PeriodDescription != null && periodDescription.Contains(s.PeriodDescription.Trim()) &&
                                                                        ((s.Team1ID != null && s.Team1ID.ToUpper().StartsWith(likeClause.ToUpper())) || (s.Team2ID != null && s.Team2ID.ToUpper().StartsWith(likeClause.ToUpper()))) && (s.Status != GamesAndLines.EVENT_CANCELLED)));
          }
        }
      }

      var dgvw = (DataGridView)Controls.Find(GamesGridViewInUse, true).FirstOrDefault();

      if (FilteredActiveGames != null && FilteredActiveGames.Count > 0) {
        var holdGameNum = 0;
        foreach (var res in FilteredActiveGames) {
          if (holdGameNum != res.GameNum) {
            var thisWeeksScheduleDates = GetThisWeeksScheduleDates();
            AddNewGameToGridView(res, dgvw, (DateTime)(thisWeeksScheduleDates).ScheduleEndDate);
          }
          holdGameNum = res.GameNum;
        }
      }

      FormF.PreventDataGridViewColumnsToBeSortedAndResized(dgvw);
      if (dgvw != null) {
        dgvw.CurrentCell = null;
        dgvw.AllowUserToResizeColumns = true;
      }

      if (CurrSelectedSportBranch != null && !Mdi.ShowingLikeTeams) {
        EnableDisableGamePeriodsRadBtns(CurrSelectedSportBranch[0].SportType,
                                        CurrSelectedSportBranch[0].SportSubType);
      }
      var actionToTake = chbActiveOnly.Checked ? "Hide" : "Show";
      ShowHideInactiveGames(actionToTake);
    }

    public bool GameIsAvailable(int gameNumber, int periodNumber, out string gameStatus) {
      if (FilteredActiveGames == null) {
        FilteredActiveGames =
              ActiveGames.Where(
                  s =>
                  s.GameDateTime != null && s.GameNum == gameNumber &&
                  s.PeriodNumber == periodNumber).
                          OrderBy(s => s.PeriodNumber).
                          ThenByDescending(s => s.CustProfile).ToList();
      }

      if (FilteredActiveGames == null || !FilteredActiveGames.Any()) {
        gameStatus = GamesAndLines.EVENT_OPEN;
        return false;
      }
      var gameIsAvailable = true;
      gameStatus = (from p in FilteredActiveGames where p.GameNum == gameNumber && p.PeriodNumber == periodNumber select p.Status).FirstOrDefault();
      var periodWagerCutoff = (from p in FilteredActiveGames where p.GameNum == gameNumber && p.PeriodNumber == periodNumber select p.PeriodWagerCutoff).FirstOrDefault();

      switch (gameStatus) {
        case GamesAndLines.EVENT_COMPLETED:
        case GamesAndLines.EVENT_CANCELLED:
          gameIsAvailable = false;
          break;
        case GamesAndLines.EVENT_OPEN:
        case GamesAndLines.EVENT_CIRCLED:
        case GamesAndLines.EVENT_OFFLINE:
          gameIsAvailable = periodWagerCutoff == null || PeriodWagerCutoffBypassed((DateTime)periodWagerCutoff);
          break;
      }

      var gameOutcome = 0;

      using (var gl = new GamesAndLines(AppModuleInfo)) {
        var outc = gl.GetGameOutcomeByPeriod(gameNumber, periodNumber);
        if (outc != null) {
          gameOutcome = outc.Count();
        }
      }

      if (gameOutcome <= 0) return gameIsAvailable;
      MessageBox.Show(@"Period game already graded.  No more wagers allowed", @"Already graded");

      return false;
    }

    private int? GetContestWagerItemsCount(int ticketNumber, int wagerNumber) {
      int? itemsCount = null;
      if (Mdi.Ticket.ContestWagerItems == null) return null;
      var wi = Mdi.Ticket.ContestWagerItems.Where(o => o.TicketNumber == ticketNumber && o.WagerNumber == wagerNumber)
        .Select(p => p.WagerNumber).FirstOrDefault();

      if (wi > 0) {
        itemsCount = Mdi.Ticket.ContestWagerItems.Where(o => o.TicketNumber == ticketNumber && o.WagerNumber == wagerNumber).Max(p => p.ItemNumber);
      }
      return itemsCount;
    }

    public int? GetManualWagerItemsCount(int ticketNumber, int wagerNumber) {
      int? itemsCount = null;
      if (Mdi.Ticket.ManualWagerItems == null) return null;
      var wi = Mdi.Ticket.ManualWagerItems.Where(o => o.TicketNumber == ticketNumber && o.WagerNumber == wagerNumber)
        .Select(p => p.WagerNumber).FirstOrDefault();

      if (wi > 0) {
        itemsCount = Mdi.Ticket.ManualWagerItems.Where(o => o.TicketNumber == ticketNumber && o.WagerNumber == wagerNumber).Max(p => p.ItemNumber);
      }
      return itemsCount;
    }

    public int GetPeriodNumber(String periodName, IEnumerable<spGLGetAvailablePeriodsBySport_Result> sportPeriods) {
      return sportPeriods != null ? (from res in sportPeriods where res.PeriodDescription.Trim() == periodName.Trim() select res.PeriodNumber).FirstOrDefault() : 0;
    }

    private ScheduleDates GetThisWeeksScheduleDates() {
      if (ThisWeekScheduleDates != null) return (ScheduleDates)ThisWeekScheduleDates;
      ThisWeekScheduleDates = new ScheduleDates();
      var thisWeekStartScheduleDate = Mdi.GetCurrentDateTime().Date;
      int? today = (from d in _daysOfTheWeek where d.DayStrValue == thisWeekStartScheduleDate.DayOfWeek.ToString() select d.DayIntValue).FirstOrDefault();

      int deltaDays;
      if (today < _footballScheduleStartDay.DayIntValue) {
        deltaDays = 6;
      }
      else {
        deltaDays = (int)today - _footballScheduleStartDay.DayIntValue;
      }
      thisWeekStartScheduleDate = thisWeekStartScheduleDate.AddDays(-1 * deltaDays);
      var thisWeekEndScheduleDate = thisWeekStartScheduleDate.AddDays(7);
      thisWeekEndScheduleDate = thisWeekEndScheduleDate.AddMilliseconds(-1);


      var dates = new ScheduleDates {
        ScheduleStartDate = thisWeekStartScheduleDate,
        ScheduleEndDate = thisWeekEndScheduleDate
      };
      ThisWeekScheduleDates = dates;
      return dates;
    }

    private int? GetWagerItemsCount(int ticketNumber, int wagerNumber) {
      int? itemsCount = null;
      if (Mdi.Ticket.WagerItems == null) return null;
      var wi = Mdi.Ticket.WagerItems.Where(o => o.TicketNumber == ticketNumber && o.WagerNumber == wagerNumber)
        .Select(p => p.WagerNumber).FirstOrDefault();

      if (wi > 0) {
        itemsCount = Mdi.Ticket.WagerItems.Where(o => o.TicketNumber == ticketNumber && o.WagerNumber == wagerNumber).Max(p => p.ItemNumber);
      }
      return itemsCount;
    }

    public void LinesUpdater_Tick(object sender, EventArgs e) {
      if (!bgwrkUpdateLines.IsBusy) {
        bgwrkUpdateLines.RunWorkerAsync();
      }
    }

    public void LoadMakeAWagerFrm(int gameNumber, int gamePeriod, String gamePeriodDescription, int rowGroupNum, DateTime periodWagerCutoff, /*int rowIdx,*/ int cellIdx, String frmMode, int ticketNumber, int wagerNumber, int itemNumber, string selectedOdds, int selectedAmericanPrice, string selectedBPsOption, string selectedAmountWagered, string selectedToWinAmount, string wagerTypeMode, Boolean? isFreePlay, String priceType, String wageringMode, String wagerLayoff, String fixedPrice, String pitcher1MStart, String pitcher2MStart, string gameDataSource, String totalsOuFlag) {
      var sGameInfo = (from a in ActiveGames where a.GameNum == gameNumber && a.PeriodNumber == gamePeriod select a).FirstOrDefault();
      var sportType = TwUtilities.GetSelectedSportType(sGameInfo);
      var sportSubType = TwUtilities.GetSelectedSportSubType(sGameInfo);

      if (wageringMode == "M") {
        var makeManualAWagerFrm = new FrmManualGameWager(SelectedWagerType, this, ActiveGames, GetAvailableGamePeriods(sportType, sportSubType), gameNumber, gamePeriod, gamePeriodDescription, btnLineDisplayToggle.Text, /*rowIdx,*/ cellIdx, rowGroupNum, _currentCustomerEastLineEnabled, _easternLineConversion, Mdi.LayoffWagersEnabled) {
          FrmMode = frmMode,
          GameDataSource = gameDataSource,
          TicketNumberToUpd = ticketNumber,
          WagerNumberToUpd = wagerNumber,
          WagerItemNumberToUpd = itemNumber,
          PreviouslySelectedOdds = selectedOdds,
          PreviouslySelectedLayoff = wagerLayoff,
          PreviouslySelectedAmtWagered = selectedAmountWagered,
          PreviouslySelectedToWinAmt = selectedToWinAmount,
          PeriodWagerCutoff = periodWagerCutoff,
          PreviouslySelectedFixedPrice = fixedPrice,
          PreviouslySelectedPitcher1 = pitcher1MStart,
          PreviouslySelectedPitcher2 = pitcher2MStart,
          Owner = this,
          WagerTypeMode = wagerTypeMode
        };

        if (isFreePlay == null) {
          makeManualAWagerFrm.IsFreePlay = IsFreePlay;
        }
        else {
          makeManualAWagerFrm.IsFreePlay = (Boolean)isFreePlay;
        }
        makeManualAWagerFrm.Icon = Icon;
        makeManualAWagerFrm.ShowDialog();
      }
      else {
        if (SelectedWagerType != "Manual Play") {
          var makeAWagerFrm = new FrmMakeAWager(SelectedWagerType, this, CurrentCustomerId, ActiveGames, GetAvailableGamePeriods(sportType, sportSubType), gameNumber, gamePeriod, gamePeriodDescription,
                        priceType, /*rowIdx,*/ cellIdx, rowGroupNum, _currentCustomerEastLineEnabled, _easternLineConversion) {
                          FrmMode = frmMode,
                          GameDataSource = gameDataSource,
                          TicketNumberToUpd = ticketNumber,
                          WagerNumberToUpd = wagerNumber,
                          WagerItemNumberToUpd = itemNumber,
                          PreviouslySelectedOdds = selectedOdds,
                          PreviouslySelectedAmericanPrice = selectedAmericanPrice,
                          PreviouslySelectedBPtsOpt = selectedBPsOption,
                          PreviouslySelectedAmtWagered = selectedAmountWagered,
                          PreviouslySelectedToWinAmt = selectedToWinAmount,
                          PeriodWagerCutoff = periodWagerCutoff,
                          PreviouslySelectedFixedPrice = fixedPrice,
                          PreviouslySelectedPitcher1 = pitcher1MStart,
                          PreviouslySelectedPitcher2 = pitcher2MStart,
                          Owner = this,
                          WagerTypeMode = wagerTypeMode,
                          PreviouslySelectedOUflag = totalsOuFlag
                        };

          if (isFreePlay == null) {
            makeAWagerFrm.IsFreePlay = IsFreePlay;
          }
          else {
            makeAWagerFrm.IsFreePlay = (Boolean)isFreePlay;
          }
          makeAWagerFrm.Icon = Icon;
          makeAWagerFrm.ShowDialog();
          makeAWagerFrm.Dispose();
        }
      }
    }

    private void LoadMakeAWagerFrm(DataGridView callerGrvw, int rowIdx, int cellIdx, Boolean? isFreePlay, int ticketNumber) {
      Boolean showForm;

      var dgvw = callerGrvw;
      var gameNumber = int.Parse(dgvw.Rows[rowIdx].Cells[GAME_NUM_INDEX].Value.ToString());
      var periodNumber = int.Parse(dgvw.Rows[rowIdx].Cells[PERIOD_NUM_INDEX].Value.ToString());
      var sGameInfo = (from a in ActiveGames where a.GameNum == gameNumber && a.PeriodNumber == periodNumber select a).FirstOrDefault();
      var sportType = TwUtilities.GetSelectedSportType(sGameInfo);
      var sportSubType = TwUtilities.GetSelectedSportSubType(sGameInfo);
      var cellContent = callerGrvw.Rows[rowIdx].Cells[cellIdx].Value.ToString();
      var gameStatus = callerGrvw.Rows[rowIdx].Cells[GAME_STATUS_INDEX].Value.ToString();

      switch (gameStatus) {
        case GamesAndLines.EVENT_COMPLETED:
          MessageBox.Show(Resources.FrmSportAndGameSelection_LoadMakeAWagerFrm_The_game_has_already_been_graded___No_more__wagers_are_allowed_, Resources.FrmSportAndGameSelection_LoadMakeAWagerFrm_Game_over);
          break;
        case GamesAndLines.EVENT_OPEN:
        case GamesAndLines.EVENT_CIRCLED:
          break;
        case GamesAndLines.EVENT_OFFLINE:
          MessageBox.Show(Resources.FrmSportAndGameSelection_LoadMakeAWagerFrm_Wagering_has_been_suspended_for_this_game, Resources.FrmSportAndGameSelection_LoadMakeAWagerFrm_Game_taken_offline);
          break;
        case GamesAndLines.EVENT_CANCELLED:
          MessageBox.Show(@"The game period has already started or is not currently available", @"Not available");
          break;

      }

      if (cellContent == "") {
        showForm = false;
      }
      else {
        if (gameStatus == GamesAndLines.EVENT_COMPLETED || gameStatus == GamesAndLines.EVENT_OFFLINE || gameStatus == GamesAndLines.EVENT_CANCELLED)
          showForm = false;
        else
          showForm = Enums.CellIsClickable(cellIdx) && cellIdx != 2;
      }


      if (!showForm || !GameIsAvailable(gameNumber, periodNumber, out gameStatus)) return;
      var rowGroupNum = int.Parse(dgvw.Rows[rowIdx].Cells[ROWGROUPNUM_INDEX].Value.ToString());

      if (SelectedWagerType == "Manual Play") return;
      if (WageringMode == "M") {
        DateTime parsedPeriodWagerCutoff;
        DateTime.TryParse(dgvw.Rows[rowIdx].Cells[PERIOD_CUTOFF_INDEX].Value.ToString(), out parsedPeriodWagerCutoff);

        var makeManualAWagerFrm = new FrmManualGameWager(SelectedWagerType, this, ActiveGames, GetAvailableGamePeriods(sportType, sportSubType), gameNumber, periodNumber, null,
            btnLineDisplayToggle.Text, /*rowIdx,*/ cellIdx, rowGroupNum, _currentCustomerEastLineEnabled, _easternLineConversion,
                    Mdi.LayoffWagersEnabled) {
                      FrmMode = "New",
                      PeriodWagerCutoff = parsedPeriodWagerCutoff,
                      TicketNumberToUpd = ticketNumber,
                      Owner = this
                    };

        if (isFreePlay == null) {
          makeManualAWagerFrm.IsFreePlay = IsFreePlay;
        }
        else {
          makeManualAWagerFrm.IsFreePlay = (Boolean)isFreePlay;
        }
        makeManualAWagerFrm.Icon = Icon;
        makeManualAWagerFrm.ShowDialog();
      }
      else {
        DateTime parsedPeriodWagerCutoff;
        DateTime.TryParse(dgvw.Rows[rowIdx].Cells[PERIOD_CUTOFF_INDEX].Value.ToString(), out parsedPeriodWagerCutoff);

        var makeAWagerFrm = new FrmMakeAWager(SelectedWagerType, this, CurrentCustomerId, ActiveGames, GetAvailableGamePeriods(sportType, sportSubType), gameNumber, periodNumber, null,
                    btnLineDisplayToggle.Text, /*rowIdx,*/ cellIdx, rowGroupNum, _currentCustomerEastLineEnabled, _easternLineConversion) {
                      FrmMode = "New",
                      GameDataSource = "GameSelection",
                      PeriodWagerCutoff = parsedPeriodWagerCutoff,
                      TicketNumberToUpd = ticketNumber,
                      Owner = this
                    };

        if (isFreePlay == null) {
          makeAWagerFrm.IsFreePlay = IsFreePlay;
        }
        else {
          makeAWagerFrm.IsFreePlay = (Boolean)isFreePlay;
        }
        makeAWagerFrm.Icon = Icon;
        makeAWagerFrm.ShowDialog();
        makeAWagerFrm.Dispose();
      }
    }

    public void LoadSportsTreeSelection(TreeView tr) {
      var teamOrId = (TextBox)Mdi.Controls.Find("txtTeamOrAsterisk", true).FirstOrDefault();
      if (teamOrId != null)
        teamOrId.Text = "";

      var selectedNode = tr.SelectedNode;
      if (selectedNode == null) return;

      var type = ((BetOptionNode)selectedNode).NodeType;

      string[] nodeFullPath;
      int treeLenCnt;

      switch (type) {
        case "G":
          if (GamesGridViewInUse != null) {
            var grv = (DataGridView)Controls.Find(GamesGridViewInUse, true).FirstOrDefault();

            if (grv != null) {
              grv.Rows.Clear();
              grv.Visible = true;
              grv.Refresh();
            }
          }

          if (PropsGridViewInUse != null) {
            var grv = (DataGridView)Controls.Find(PropsGridViewInUse, true).FirstOrDefault();

            if (grv != null) {
              grv.Rows.Clear();
              grv.Visible = false;
              //grv.Refresh();
            }
          }

          //var selectedNode = tr.SelectedNode;

          string sportType;
          var startDatePart = "";
          var endDatePart = "";
          var sportSubType = "";
          var startScheduleDate = Mdi.GetCurrentDateTime();
          var endScheduleDate = startScheduleDate.AddDays(0);

          switch (selectedNode.Level) {
            case 0:
              sportType = selectedNode.Text.Trim();
              if (selectedNode.Text.Trim() == SportTypes.FOOTBALL) {
                var thisWeeksScheduleDates = GetThisWeeksScheduleDates();
                startScheduleDate = (thisWeeksScheduleDates).ScheduleStartDate.GetValueOrDefault();
                endScheduleDate = (thisWeeksScheduleDates).ScheduleEndDate.GetValueOrDefault();
                endScheduleDate = endScheduleDate.AddMilliseconds(-1);
                startDatePart = startScheduleDate.Month + "/" + startScheduleDate.Day;
                endDatePart = endScheduleDate.Month + "/" + endScheduleDate.Day;
              }
              else {
                foreach (BetOptionNode currNode in selectedNode.Nodes) {
                  var dateParts = currNode.Text.Split('-');
                  int startDateParts0;
                  int startDateParts1;

                  if (dateParts.Length > 1) {
                    var startDateParts = dateParts[0].Trim().Split('/');
                    var endDateParts = dateParts[1].Trim().Split('/');

                    if (currNode.ScheduleDate != null)
                      startScheduleDate = (DateTime)currNode.ScheduleDate;
                    int endDateParts0;
                    int endDateParts1;
                    if (!int.TryParse(startDateParts[0], out startDateParts0) ||
                      !int.TryParse(startDateParts[1], out startDateParts1) ||
                      !int.TryParse(endDateParts[0], out endDateParts0) ||
                      !int.TryParse(endDateParts[1], out endDateParts1)) continue;

                    var startDate = new DateTime(startScheduleDate.Year, int.Parse(startDateParts[0]), int.Parse(startDateParts[1]));
                    var endDate = new DateTime(endScheduleDate.Year, int.Parse(endDateParts[0]), int.Parse(endDateParts[1]));

                    if (startScheduleDate < startDate || startScheduleDate > endDate) continue;
                    sportType = selectedNode.Text.Trim();
                    endScheduleDate = startScheduleDate.AddDays(6);
                    endScheduleDate = endScheduleDate.AddSeconds(59 - endScheduleDate.Second);
                    endScheduleDate = endScheduleDate.AddMinutes(59 - endScheduleDate.Minute);
                    endScheduleDate = endScheduleDate.AddHours(23 - endScheduleDate.Hour);
                    endScheduleDate = endScheduleDate.AddMilliseconds(999 - endScheduleDate.Millisecond);

                    startDatePart = int.Parse(startDateParts[0]) + "/" + int.Parse(startDateParts[1]);
                    endDatePart = int.Parse(endDateParts[0]) + "/" + int.Parse(endDateParts[1]);
                    break;
                  }
                  else {
                    var startDateParts = dateParts[0].Trim().Split('/');
                    if (!int.TryParse(startDateParts[0], out startDateParts0) ||
                    !int.TryParse(startDateParts[1], out startDateParts1)) continue;

                    var startDate = new DateTime(startScheduleDate.Year, int.Parse(startDateParts[0]), int.Parse(startDateParts[1]));

                    if (startScheduleDate.Month != startDate.Month || startScheduleDate.Day != startDate.Day) continue;

                    startScheduleDate = startScheduleDate.Date;

                    endScheduleDate = endScheduleDate.AddMilliseconds(999 - endScheduleDate.Millisecond);
                    endScheduleDate = endScheduleDate.AddSeconds(59 - endScheduleDate.Second);
                    endScheduleDate = endScheduleDate.AddMinutes(59 - endScheduleDate.Minute);
                    endScheduleDate = endScheduleDate.AddHours(23 - endScheduleDate.Hour);

                    startDatePart = startScheduleDate.Month + "/" + startScheduleDate.Day;
                    endDatePart = endScheduleDate.Month + "/" + endScheduleDate.Day;
                    break;
                  }
                }
                if (startScheduleDate != endScheduleDate) {
                  sportType = selectedNode.Text.Trim();
                }
              }
              break;
            default:
              nodeFullPath = selectedNode.FullPath.ToString(CultureInfo.InvariantCulture).Split('\\');
              var scheduleDate = ((BetOptionNode)selectedNode).ScheduleDate;
              if (scheduleDate != null) startScheduleDate = (DateTime)scheduleDate;
              sportType = nodeFullPath[0].Trim();

              endScheduleDate = startScheduleDate.AddDays(sportType.Trim() == SportTypes.FOOTBALL ? 6 : 0);
              endScheduleDate = endScheduleDate.AddSeconds(59 - endScheduleDate.Second);
              endScheduleDate = endScheduleDate.AddMinutes(59 - endScheduleDate.Minute);
              endScheduleDate = endScheduleDate.AddHours(23 - endScheduleDate.Hour);
              endScheduleDate = endScheduleDate.AddMilliseconds(999 - endScheduleDate.Millisecond);

              treeLenCnt = nodeFullPath.Count();

              var converToIntDate = nodeFullPath[1].Split('-');
              var firstDate = converToIntDate[0].Split('/');
              var secondDate = converToIntDate.Length > 1 ? converToIntDate[1].Split('/') : firstDate;

              int ouputParsedInt;
              int.TryParse(firstDate[0], out ouputParsedInt);
              startDatePart = ouputParsedInt.ToString(CultureInfo.InvariantCulture);
              startDatePart += "/";
              int.TryParse(firstDate[1], out ouputParsedInt);
              startDatePart += ouputParsedInt.ToString(CultureInfo.InvariantCulture);

              int.TryParse(secondDate[0], out ouputParsedInt);
              endDatePart = ouputParsedInt.ToString(CultureInfo.InvariantCulture);
              endDatePart += "/";
              int.TryParse(secondDate[1], out ouputParsedInt);
              endDatePart += ouputParsedInt.ToString(CultureInfo.InvariantCulture);

              sportSubType = "";

              if (treeLenCnt == 3) {
                sportSubType = nodeFullPath[2].Trim();
              }
              break;
          }
          CurrSelectedSportBranch = new List<SelectedSportBranch> {
                      new SelectedSportBranch {
                        StartDatePart = startDatePart,
                        EndDatePart = endDatePart,
                        StartDateTime = startScheduleDate,
                        EndDateTime = endScheduleDate,
                        SportSubType = sportSubType,
                        PeriodNumber = 0,
                        SportType = sportType
                      }
                    };

          if (sportType != "") {
            lblShiftIndication.Enabled = true;
            radGame_0.Checked = false;
            radGame_0.Checked = true;
            //radGame_0.Focus();
          }

          if (startDatePart == "" && sportSubType == "") {
            lblCurrentlyShowing.Text = "";
          }
          else {
            if (sportSubType != "") {
              if (endDatePart == "") {
                lblCurrentlyShowing.Text = Resources.FrmSportAndGameSelection_treSports_MouseClick_ + sportType + Resources.FrmSportAndGameSelection_treSports_MouseClick_ + TwUtilities.AddLeadingZeroes(CurrSelectedSportBranch[0].StartDatePart) + Resources.FrmSportAndGameSelection_treSports_MouseClick_ + sportSubType;
              }
              else {
                lblCurrentlyShowing.Text = Resources.FrmSportAndGameSelection_treSports_MouseClick_ + sportType + Resources.FrmSportAndGameSelection_treSports_MouseClick_ + TwUtilities.AddLeadingZeroes(CurrSelectedSportBranch[0].StartDatePart) + @" to " + TwUtilities.AddLeadingZeroes(CurrSelectedSportBranch[0].EndDatePart) + Resources.FrmSportAndGameSelection_treSports_MouseClick_ + sportSubType;
              }
            }
            else {
              if (endDatePart == "") {
                lblCurrentlyShowing.Text = Resources.FrmSportAndGameSelection_treSports_MouseClick_ + sportType + Resources.FrmSportAndGameSelection_treSports_MouseClick_ + TwUtilities.AddLeadingZeroes(CurrSelectedSportBranch[0].StartDatePart);
              }
              else {
                lblCurrentlyShowing.Text = Resources.FrmSportAndGameSelection_treSports_MouseClick_ + sportType + Resources.FrmSportAndGameSelection_treSports_MouseClick_ + TwUtilities.AddLeadingZeroes(CurrSelectedSportBranch[0].StartDatePart) + @" to " + TwUtilities.AddLeadingZeroes(CurrSelectedSportBranch[0].EndDatePart);
              }
            }
          }

          ClearGameInfo();
          break;

        case "C":

          if (PropsGridViewInUse != null) {
            var grv = (DataGridView)Controls.Find(PropsGridViewInUse, true).FirstOrDefault();

            if (grv != null) {
              grv.Rows.Clear();
              grv.Visible = true;
              grv.Refresh();
            }
          }

          if (GamesGridViewInUse != null) {
            var grv = (DataGridView)Controls.Find(GamesGridViewInUse, true).FirstOrDefault();

            if (grv != null) {
              grv.Rows.Clear();
              grv.Visible = false;
              //grv.Refresh();
            }
          }

          var objNode = (BetOptionNode)tr.SelectedNode;

          //BetOptionNode objNode = (BetOptionNode)e.Node;

          ContestNodesInfo = new List<ContestTreeNodeDesc>();

          nodeFullPath = objNode.FullPath.ToString(CultureInfo.InvariantCulture).Split('\\');
          treeLenCnt = nodeFullPath.Count();

          var nodeInfo = new ContestTreeNodeDesc();

          switch (treeLenCnt) {
            case 1:
              nodeInfo.LevelTarget = objNode.ContestLevel;
              nodeInfo.LevelValue = nodeFullPath[0].Trim();
              ContestNodesInfo.Add(nodeInfo);
              lblCurrentlyShowing.Text = @"==>>" + nodeFullPath[0].Trim();
              break;

            case 2:
              nodeInfo.LevelTarget = ((BetOptionNode)objNode.Parent).ContestLevel;
              nodeInfo.LevelValue = nodeFullPath[0].Trim();
              ContestNodesInfo.Add(nodeInfo);

              nodeInfo.LevelTarget = objNode.ContestLevel;
              nodeInfo.LevelValue = nodeFullPath[1].Trim();
              ContestNodesInfo.Add(nodeInfo);
              lblCurrentlyShowing.Text = @"==>>" + nodeFullPath[0].Trim() + @"==>>" + nodeFullPath[1].Trim();
              break;

            case 3:
              nodeInfo.LevelTarget = ((BetOptionNode)objNode.Parent.Parent).ContestLevel;
              nodeInfo.LevelValue = nodeFullPath[0].Trim();
              ContestNodesInfo.Add(nodeInfo);

              nodeInfo.LevelTarget = ((BetOptionNode)objNode.Parent).ContestLevel;
              nodeInfo.LevelValue = nodeFullPath[1].Trim();
              ContestNodesInfo.Add(nodeInfo);

              nodeInfo.LevelTarget = objNode.ContestLevel;
              nodeInfo.LevelValue = nodeFullPath[2].Trim();
              ContestNodesInfo.Add(nodeInfo);
              lblCurrentlyShowing.Text = @"==>>" + nodeFullPath[0].Trim() + @"==>>" + nodeFullPath[1].Trim() + @"==>>" + nodeFullPath[2].Trim();
              break;

            case 4:

              nodeInfo.LevelTarget = ((BetOptionNode)objNode.Parent.Parent.Parent).ContestLevel;
              nodeInfo.LevelValue = nodeFullPath[0].Trim();
              ContestNodesInfo.Add(nodeInfo);

              nodeInfo.LevelTarget = ((BetOptionNode)objNode.Parent.Parent).ContestLevel;
              nodeInfo.LevelValue = nodeFullPath[1].Trim();
              ContestNodesInfo.Add(nodeInfo);

              nodeInfo.LevelTarget = ((BetOptionNode)objNode.Parent).ContestLevel;
              nodeInfo.LevelValue = nodeFullPath[2].Trim();
              ContestNodesInfo.Add(nodeInfo);

              nodeInfo.LevelTarget = objNode.ContestLevel;
              nodeInfo.LevelValue = nodeFullPath[3].Trim();
              ContestNodesInfo.Add(nodeInfo);
              lblCurrentlyShowing.Text = @"==>>" + nodeFullPath[0].Trim() + @"==>>" + nodeFullPath[1].Trim() + @"==>>" + nodeFullPath[2].Trim() + @"==>>" + nodeFullPath[3].Trim();
              break;
          }
          lblShiftIndication.Enabled = false;
          DisableGamePeriodsRadBtns();
          FillContestsGridView(ContestNodesInfo);
          break;
      }
    }

    public void PlaceBet(String frmMode, TextBox txtRisk, TextBox txtToWin, String selectedWagerTypeName, String selectedWagerType, String wagerTypeMode,
                            String selectedPriceName, int ticketNumberToUpd, int? wagerNumberToUpd, int? wagerItemNumberToUpd, /*int selectedGvRow,*/ int selectedRowNumberInGroup,
                            String selectedLine, String selectedPrice, String selectedOdds, String selectedBPointsOption, String chosenTeamId, double? teaserPoints, String pitcher1IsReq, String pitcher2IsReq, String fixedPrice,
                            String totalsOorU, String rifBackwardTicketNumber, String wagerItemDescription, List<string> lineAndPoints,
                                spGLGetActiveGamesByCustomer_Result selectedGamePeriodInfo, Boolean isFreePlay, String wageringMode, String layoffFlag, String easternLineEnabled, List<spGLGetEasternLineConversionInfo_Result> easternLineConversionInfo, string gameDataSource, String complexIfBetAmount = null) {
      var message = "";

      if (txtRisk.Visible && txtRisk.Text.Length == 0) {
        message = Resources.FrmMakeAWager_IsValidWager_A_wager_amount_must_be_entered_;
      }
      if (message.Length > 0) {
        MessageBox.Show(message, ParameterList.GetItem("AppName").Value, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        return;
      }

      switch (selectedWagerTypeName) {
        case "If-Bet":
          AddToIfBet(frmMode, txtRisk, txtToWin, selectedWagerTypeName, selectedWagerType, wagerTypeMode,
                      selectedPriceName, ticketNumberToUpd, wagerNumberToUpd, wagerItemNumberToUpd, /*selectedGvRow,*/ selectedRowNumberInGroup,
                      selectedLine, selectedPrice, selectedOdds, selectedBPointsOption, chosenTeamId, teaserPoints, pitcher1IsReq, pitcher2IsReq, fixedPrice,
                      totalsOorU, rifBackwardTicketNumber, wagerItemDescription, lineAndPoints,
                      selectedGamePeriodInfo, isFreePlay, wageringMode, layoffFlag, easternLineEnabled, easternLineConversionInfo, gameDataSource, complexIfBetAmount);
          break;
        case "Parlay":
          AddToParlay(frmMode, txtRisk, txtToWin, selectedWagerTypeName, selectedWagerType, wagerTypeMode,
                      selectedPriceName, ticketNumberToUpd, wagerNumberToUpd, wagerItemNumberToUpd, /*selectedGvRow,*/ selectedRowNumberInGroup,
                      selectedLine, selectedPrice, selectedOdds, selectedBPointsOption, chosenTeamId, teaserPoints, pitcher1IsReq, pitcher2IsReq, fixedPrice,
                      totalsOorU, rifBackwardTicketNumber, wagerItemDescription, lineAndPoints,
                      selectedGamePeriodInfo, isFreePlay, wageringMode, layoffFlag, easternLineEnabled, easternLineConversionInfo, gameDataSource);
          break;
        case "Teaser":
          AddToTeaser(frmMode, txtRisk, txtToWin, selectedWagerTypeName, selectedWagerType, wagerTypeMode,
                      selectedPriceName, ticketNumberToUpd, wagerNumberToUpd, wagerItemNumberToUpd, /*selectedGvRow,*/ selectedRowNumberInGroup,
                      selectedLine, selectedPrice, selectedOdds, selectedBPointsOption, chosenTeamId, teaserPoints, pitcher1IsReq, pitcher2IsReq, fixedPrice,
                      totalsOorU, rifBackwardTicketNumber, wagerItemDescription, lineAndPoints,
                      selectedGamePeriodInfo, isFreePlay, wageringMode, layoffFlag, easternLineEnabled, easternLineConversionInfo, gameDataSource);
          break;
        case "Straight Bet":
          string itemShortDescription;
          int wagerNumberForWi;
          int wagerNumber;

          PrepareStraightBetStageWager(frmMode, ticketNumberToUpd, wagerNumberToUpd ?? 0, out wagerNumberForWi, txtRisk, txtToWin, selectedWagerTypeName, selectedWagerType, isFreePlay, layoffFlag);

          if (frmMode == "New") {
            wagerNumber = wagerNumberForWi;
          }
          else { wagerNumber = wagerNumberToUpd ?? 0; }

          var wi = CreateStageWagerItem(frmMode, selectedWagerTypeName, selectedWagerType, wagerTypeMode,
                                  selectedPriceName, ticketNumberToUpd, wagerNumber,
                                  wagerItemNumberToUpd ?? 0, /*selectedGvRow,*/ selectedRowNumberInGroup, selectedLine,
                                  selectedPrice, selectedOdds, selectedBPointsOption,
                                  chosenTeamId, teaserPoints, pitcher1IsReq,
                                  pitcher2IsReq, fixedPrice, totalsOorU,
                                  double.Parse(txtRisk.Text), rifBackwardTicketNumber, wagerItemDescription,
                                  lineAndPoints,
                                  selectedGamePeriodInfo,
                                  out itemShortDescription, isFreePlay, wageringMode, layoffFlag, easternLineEnabled, easternLineConversionInfo);

          if (frmMode == "Edit" && !ParlayOrTeaserItemFromSb) {

            if (Mdi.Ticket.WagerItems != null && Mdi.Ticket.WagerItems.Any(w => w.TicketNumber == wi.TicketNumber && w.WagerNumber == wi.WagerNumber && w.ItemNumber == wi.ItemNumber)) {
              UpdateWagerItemReadbackLogInfo(wi);
            }
            else {
              AddStageWagerItem("New", wi, selectedGamePeriodInfo);
            }
          }
          else {
            AddStageWagerItem(frmMode, wi, selectedGamePeriodInfo);
          }

          BuildReadbackLogInfoData(true);
          PopulateReadbackLogInfoDgvw();

          if (Mdi.RollingIfBetMode) {
            Mdi.ToggleRifCheckBox(!Mdi.RollingIfBetMode);
            if (Mdi.RIfBetFromReadback == "true")
              Mdi.RIfBetFromReadback = null;
            Mdi.FillCustomerDataTextBoxes("A");
          }
          break;
      }
    }

    public void PopulateReadbackLogInfoDgvw() {
      if (ReadbackItems.Count <= 0) return;
      var finalReadbackItems = ReadbackItems/*.Where(p => p.TicketNumber == _ticketNumber)*/.OrderBy(s => s.SeqNumber).ToList();

      var gvw = (DataGridView)_ticketConfirmation.Controls.Find("dgvwReadbackInfo", true).FirstOrDefault();
      FormF.ClearDataGridView(gvw, true, true);

      var list = new BindingList<RowItem>();

      foreach (var vi in finalReadbackItems.Select(row => new RowItem {
        String1 = row.String1,
        String2 = row.String2,
        String3 = row.String3,
        WagerTypeName = row.WagerTypeName,
        WagerNumber = row.WagerNumber.ToString(CultureInfo.InvariantCulture),
        ItemNumber = row.ItemNumber.ToString(CultureInfo.InvariantCulture),
        RowIsClickable = row.RowIsClickable.ToString(),
        TargetFrm = row.TargetFrm,
        GameNumber = row.GameNumber.ToString(CultureInfo.InvariantCulture),
        PeriodNumber = row.PeriodNumber.ToString(CultureInfo.InvariantCulture),
        RifBackwardTicketNumber = row.RifBackwardTicketNumber,
        RifBackwardWagerNumber = row.RifBackwardWagerNumber,
        FreePlayFlag = row.FreePlayFlag,
        WagerType = row.WagerType,
        SpecialIfBetAmount = row.SpecialIfBetAmount,
        TicketNumber = row.TicketNumber.ToString()
      })) {
        list.Add(vi);
      }

      if (gvw == null) return;
      gvw.DataSource = list;
      gvw.ClearSelection();
      if (gvw.Rows.Count > 0)
        gvw.FirstDisplayedScrollingRowIndex = gvw.Rows.Count - 1;

      for (var j = 0; j < gvw.Columns.Count; j++) {
        gvw.Columns[j].Visible = j <= 2;

      }

      for (var j = 0; j < gvw.Rows.Count; j++) {
        gvw.Rows[j].Height = GRIDVIEWROWHEIGHT;
      }

      TwUtilities.AdjustGridColumnsWidth(gvw);
      if (IsFreePlay) {
        Mdi.FillCustomerDataTextBoxes("F");
      }
    }

    public void SetFocusOnSpecificGameOrContest(KeyEventArgs e) {
      if ((e.KeyCode < Keys.D0 || e.KeyCode > Keys.D9) && (e.KeyCode < Keys.NumPad0 || e.KeyCode > Keys.NumPad9) &&
          e.KeyCode != Keys.S && e.KeyCode != Keys.M && e.KeyCode != Keys.E && e.KeyCode != Keys.L) return;

      if ((Mdi.GetCurrentDateTime() - KeyPressLastTime).Seconds >= 1) KeyPressBuffer = TwUtilities.GetKeyDownChar(e);
      else KeyPressBuffer += TwUtilities.GetKeyDownChar(e);
      KeyPressLastTime = Mdi.GetCurrentDateTime();
      FindGameOrContestByRotation(KeyPressBuffer);
    }

    public void ShowContestFormInEditMode(String frmMode, int ticketNumber, int wagerNumber, int itemNumber, int contestNumber, int contestantNumber, String contestantDesc, String amtWagered, String odds, String toWinAmt, Boolean isFreePlay, String priceType, String thresholdLine, String wageringMode, Boolean layoffWager, String xToYRepres, String toBase) {
      if (wageringMode == "M") {
        var placeManualContestOrProp = new FrmManualContestWager(SelectedWagerType, this, ActiveContests, contestNumber, contestantNumber, contestantDesc, priceType, Mdi.LayoffWagersEnabled) {
          Owner = this,
          FrmMode = frmMode,
          TicketNumberToUpd = ticketNumber,
          WagerNumberToUpd = wagerNumber,
          WagerItemNumberToUpd = itemNumber,
          PreviouslySelectedAmtWagered = amtWagered,
          PreviouslySelectedOdds = odds,
          PreviouslySelectedToWinAmt = toWinAmt,
          PreviouslySelectedDescription = contestantDesc,
          IsFreePlay = isFreePlay,
          PreviouslySelectedThresholdLine = thresholdLine,
          PreviouslySelectedLayOffWager = layoffWager,
          PreviouslySelectedXtoY = xToYRepres,
          PreviouslySelectedToBase = toBase,
          Icon = Icon
        };
        placeManualContestOrProp.ShowDialog();
        placeManualContestOrProp.Dispose();

      }
      else {
        var placeContestOrProp = new FrmPlaceContestOrProp(SelectedWagerType, this, ActiveContests, contestNumber, contestantNumber, contestantDesc, priceType, _currentCustomerPercentBook) {
          Owner = this,
          FrmMode = frmMode,
          TicketNumberToUpd = ticketNumber,
          WagerNumberToUpd = wagerNumber,
          WagerItemNumberToUpd = itemNumber,
          PreviouslySelectedAmtWagered = amtWagered,
          PreviouslySelectedOdds = odds,
          PreviouslySelectedToWinAmt = toWinAmt,
          PreviouslySelectedDescription = contestantDesc,
          IsFreePlay = isFreePlay,
          Icon = Icon
        };
        placeContestOrProp.ShowDialog();
        placeContestOrProp.Dispose();
      }
    }

    public void ShowFormInEditMode(int gameNumber, int gamePeriod, String gamePeriodDescription, int rowGroupNum, DateTime periodWagerCutoff, /*int rowIdx,*/ int cellIdx, int ticketNumber, int wagerNumber, int itemNumber, string selectedOdds,
        int selectedAmericanPrice, string selectedBPointsOption, string amtWagered, string toWinAmt, string wagerTypeMode, Boolean isFreePlay, String priceType, String wageringMode,
            String wagerLayoff, String fixedPrice, String pitcher1MStart, String pitcher2MStart, string gameDataSource, String totalsOuFlag) {
      LoadMakeAWagerFrm(gameNumber, gamePeriod, gamePeriodDescription, rowGroupNum, periodWagerCutoff, /*rowIdx,*/ cellIdx, "Edit", ticketNumber, wagerNumber, itemNumber, selectedOdds, selectedAmericanPrice, selectedBPointsOption, amtWagered,
          toWinAmt, wagerTypeMode, isFreePlay, priceType, wageringMode, wagerLayoff, fixedPrice, pitcher1MStart, pitcher2MStart, gameDataSource, totalsOuFlag);
    }

    public void ShowComplexBetInEditMode(String wagerType, Ticket.Wager wager, List<Ticket.WagerItem> wagerItems) {
      if (wager == null || wagerItems == null || wagerItems.Count == 0)
        return;

      spCnGetActiveContests_Result selectedContestInfo = null;
      spGLGetActiveGamesByCustomer_Result selectedGamePeriodInfo = null;

      var cutoffTimeBypassed = true;

      var mdiFreePlayCheckBox = (CheckBox)Mdi.Controls.Find("chbFreePlay", true).FirstOrDefault();

      foreach (var wi in wagerItems.Where(wi2 => "P".Contains(wi2.Outcome))) {
        if (wi.IsContest) {
          if (ActiveContests != null) {
            selectedContestInfo = (from s in ActiveContests where s.ContestNum == wi.GameNum && s.ContestantNum == wi.ContestantNum select s).FirstOrDefault();
          }
          CheckIfContestWagerCutoffHasBeenReachedInReadback("Accept Any Wager", "Request to Delete a READBACK Item Past Post",
              selectedContestInfo, wi.ShortDescription, (wi.AmountWagered ?? 0).ToString(CultureInfo.InvariantCulture), (wi.ToWinAmount ?? 0).ToString(CultureInfo.InvariantCulture), out cutoffTimeBypassed);
        }
        else {
          if (ActiveGames != null) {
            selectedGamePeriodInfo = (from s in ActiveGames where s.GameNum == wi.GameNum && s.PeriodNumber == wi.PeriodNumber select s).FirstOrDefault();
          }

          CheckIfWagerCutoffHasBeenReachedInReadback("Accept Any Wager", "Request to Edit a READBACK Item Past Post", selectedGamePeriodInfo, wi.Description, wi.AmountWagered.ToString(), wi.ToWinAmount.ToString(), out cutoffTimeBypassed);
        }
        if (!cutoffTimeBypassed)
          break;
      }

        if (!cutoffTimeBypassed)
        {
            return;
        }

      switch (wager.WagerType) {
        case "Parlay":
        case WagerType.PARLAY:
          var frmParlay = new FrmPlaceParlay(AppModuleInfo) { FrmMode = "Edit", TicketNumberToUpd = wager.TicketNumber, WagerNumberToUpd = wager.WagerNumber };
          if (wager.FreePlayFlag == "Y") {
            if (mdiFreePlayCheckBox != null && !mdiFreePlayCheckBox.Checked) {
              if (MessageBox.Show(@"Must be in FREE PLAY mode to edit this bet." + Environment.NewLine +
                                  @"Do you want to switch to free play mode and continue?", @"Free Play Mode Required", MessageBoxButtons.YesNo) == DialogResult.Yes) {
                mdiFreePlayCheckBox.Checked = true;
                Mdi.DisplayWageringForm(frmParlay, wagerType);
              }
            }
            else {
              Mdi.DisplayWageringForm(frmParlay, wagerType);
            }
          }
          else {
            Mdi.DisplayWageringForm(frmParlay, wagerType);
          }
          break;
        case "Teaser":
        case WagerType.TEASER:
          var frmTeaser = new FrmPlaceTeaser(AppModuleInfo) { FrmMode = "Edit", TicketNumberToUpd = wager.TicketNumber, WagerNumberToUpd = wager.WagerNumber };
          if (wager.FreePlayFlag == "Y") {
            if (chbFreePlay != null && !chbFreePlay.Checked) {
              if (MessageBox.Show(@"Must be in FREE PLAY mode to edit this bet." + Environment.NewLine +
                                  @"Do you want to switch to free play mode and continue?", @"Free Play Mode Required", MessageBoxButtons.YesNo) == DialogResult.Yes) {
                chbFreePlay.Checked = true;
                Mdi.DisplayWageringForm(frmTeaser, wagerType);
              }
            }
            else {
              Mdi.DisplayWageringForm(frmTeaser, wagerType);
            }
          }
          else {
            Mdi.DisplayWageringForm(frmTeaser, wagerType);
          }
          break;
        case "If-Bet":
        case WagerType.IFBET:
        case "Action Reverse":
          var frmIfBet = new FrmPlaceIfBet(AppModuleInfo) { FrmMode = "Edit", TicketNumberToUpd = wager.TicketNumber, WagerNumberToUpd = wager.WagerNumber, ContinueOnPush = wager.ContinueOnPushFlag == "Y", SpecialIfBetAmount = wager.SpecialIfBetAmt };
          Mdi.DisplayWageringForm(frmIfBet, "If-Bet");
          break;
      }
    }

    public void ToggleInactiveGamesDisplay() {
      if (chbActiveOnly.Text == @"Show All (F9)" && chbActiveOnly.Checked) {
        chbActiveOnly.Checked = false;
        return;
      }
      if (chbActiveOnly.Text == @"Active Only (F9)" && !chbActiveOnly.Checked) {
        chbActiveOnly.Checked = true;
      }
      // chbActiveOnly.Checked = chbActiveOnly.Text == "Show All (F9)" && chbActiveOnly.Checked ? false : true;
    }

    public void UpdateContestWagerItem(Ticket.ContestWagerItem cItem) {
      Mdi.Ticket.UpdateContestWagerItem(cItem);
    }

    public void UpdateGameLines() {

      FillFrmText();

      var gDgvw = (DataGridView)Controls.Find(GamesGridViewInUse, true).FirstOrDefault();
      var cDgvw = (DataGridView)Controls.Find(PropsGridViewInUse, true).FirstOrDefault();

      var gIsVisible = false;
      if (gDgvw != null)
        gIsVisible = gDgvw.Visible;

      var cIsVisible = false;
      if (cDgvw != null)
        cIsVisible = cDgvw.Visible;

      if ((!gIsVisible || gDgvw.Rows.Count <= 0) && (!cIsVisible || cDgvw.Rows.Count <= 0)) return;
      if (_lastGameChangeNum == null) return;
      using (var gl = new GamesAndLines(AppModuleInfo)) {
          _lastGamesLineChangeByCustomer = gl.GetLastLineChangeByCustomer(CurrentCustomerId, _lastGameChangeNum);
      }

      if (_lastGamesLineChangeByCustomer.Count <= 0) return;
      _lastGameChangeNum = (from p in _lastGamesLineChangeByCustomer select p.ChangeNum).Max();
      var clearAllLines = "N";

      foreach (var lch in _lastGamesLineChangeByCustomer) {
        if (lch.ClearAllLinesFlag != null)
          clearAllLines = lch.ClearAllLinesFlag;

        var gameNum = lch.GameNum ?? 0;
        var periodNumber = lch.PeriodNumber.Value;
        var wagerType = lch.WagerType;
        var gameIsCircled = lch.CircledFlag == "Y";
        var takeGameOffline = lch.TakeOfflineFlag == "Y";
        var parlayRestriction = lch.ParlayRestriction;

        if (lch.IsFlagsChange == "Y") {
          if (wagerType == WagerType.CONTEST) {
            if (clearAllLines == "Y") {
              ActiveContests.RemoveAll(a => a.ContestNum == gameNum);
              _filteredActiveContests.RemoveAll(a => a.ContestNum == gameNum);
            }

            if (gameIsCircled) {
              ActiveContests.RemoveAll(a => a.ContestNum == gameNum);
              _filteredActiveContests.RemoveAll(a => a.ContestNum == gameNum);
              AddNewContestToList(gameNum);
            }

            if (takeGameOffline) {
              ChangeContestStatus(gameNum, GamesAndLines.EVENT_OFFLINE);
            }

          }
          else {
            if (clearAllLines == "Y") {
              ActiveGames.RemoveAll(a => a.GameNum == gameNum);
              FilteredActiveGames.RemoveAll(a => a.GameNum == gameNum);
            }


            if (gameIsCircled) {
              ActiveGames.RemoveAll(a => a.GameNum == gameNum);
              FilteredActiveGames.RemoveAll(a => a.GameNum == gameNum);
              AddNewGameToList(gameNum);
            }


            if (takeGameOffline) {
              ChangeGameStatus(gameNum, GamesAndLines.EVENT_OFFLINE);
            }
            if (parlayRestriction != null) {
              ChangeGameParlayRestrictionSetting(gameNum, periodNumber, parlayRestriction);
            }
          }
        }
        else {
          if (wagerType == WagerType.CONTEST) {
            ActiveContests.RemoveAll(a => a.ContestNum == gameNum);
            _filteredActiveContests.RemoveAll(a => a.ContestNum == gameNum);
            AddNewContestToList(gameNum);
          }
          else {
            ActiveGames.RemoveAll(a => a.GameNum == gameNum);
            FilteredActiveGames.RemoveAll(a => a.GameNum == gameNum);
            AddNewGameToList(gameNum);
          }
        }
      }
      UpdateGamesGridView(CurrSelectedSportBranch, Mdi.ShowingLatestLines, Mdi.ShowingLikeTeams);
    }

    private void UpdateGameLineObjectsWithBackgroundWorker() {
        if (_lastGameChangeNum == null) _lastGameChangeNum = 0;
      using (var gl = new GamesAndLines(AppModuleInfo)) {
        _lastGamesLineChangeByCustomer = gl.GetLastLineChangeByCustomer(CurrentCustomerId, _lastGameChangeNum);
      }

      if (_lastGamesLineChangeByCustomer == null || _lastGamesLineChangeByCustomer.Count <= 0) {
        VerifyCurrentlyDisplayedGamesStatus();
        return;
      }

      _lastGameChangeNum = (from p in _lastGamesLineChangeByCustomer select p.ChangeNum).Max();
      var clearAllLines = "N";

      foreach (var lch in _lastGamesLineChangeByCustomer) {
        if (lch.ClearAllLinesFlag != null)
          clearAllLines = lch.ClearAllLinesFlag;

        var gameNum = lch.GameNum.Value;
        var periodNumber = lch.PeriodNumber.Value;
        var wagerType = lch.WagerType;
        var gameIsCircled = lch.CircledFlag == "Y";
        var takeGameOffline = lch.TakeOfflineFlag == "Y";
        var parlayRestriction = lch.ParlayRestriction;

        if (lch.IsFlagsChange == "Y") {
          if (wagerType == WagerType.CONTEST) {
            if (clearAllLines == "Y") {
              ActiveContests.RemoveAll(a => a.ContestNum == gameNum);
              if (_filteredActiveContests != null) {
                _filteredActiveContests.RemoveAll(a => a.ContestNum == gameNum);
              }
            }

            if (gameIsCircled) {
              if (StaticLinesDuringCall == "N") {
                ActiveContests.RemoveAll(a => a.ContestNum == gameNum);
                if (_filteredActiveContests != null) {
                  _filteredActiveContests.RemoveAll(a => a.ContestNum == gameNum);
                }
                AddNewContestToList(gameNum);
              }
              else {
                ChangeContestCircledStatus(gameNum);
              }
            }
            else {
              ChangeContestCircledStatus(gameNum);
            }

            if (takeGameOffline) {
              ChangeContestStatus(gameNum, GamesAndLines.EVENT_OFFLINE);
            }
          }
          else {
            if (clearAllLines == "Y") {
              ActiveGames.RemoveAll(a => a.GameNum == gameNum);
              if (FilteredActiveGames != null) {
                FilteredActiveGames.RemoveAll(a => a.GameNum == gameNum);
              }
            }


            if (gameIsCircled) {
              if (StaticLinesDuringCall == "N") {
                ActiveGames.RemoveAll(a => a.GameNum == gameNum);
                if (FilteredActiveGames != null) {
                  FilteredActiveGames.RemoveAll(a => a.GameNum == gameNum);
                }
                AddNewGameToList(gameNum);
              }
              else {
                ChangeGameCircledStatus(gameNum);
              }
            }
            else {
              ChangeGameCircledStatus(gameNum);
            }

            if (takeGameOffline) {
              ChangeGameStatus(gameNum, GamesAndLines.EVENT_OFFLINE);
            }
            if (parlayRestriction != null) {
              ChangeGameParlayRestrictionSetting(gameNum, periodNumber, parlayRestriction);
            }
          }
        }
        else {
          if (StaticLinesDuringCall == "N") {
            if (wagerType == WagerType.CONTEST) {
              ActiveContests.RemoveAll(a => a.ContestNum == gameNum);
              if (_filteredActiveContests != null) {
                _filteredActiveContests.RemoveAll(a => a.ContestNum == gameNum);
              }
              AddNewContestToList(gameNum);
            }
            else {
              ActiveGames.RemoveAll(a => a.GameNum == gameNum);
              if (FilteredActiveGames != null) {
                FilteredActiveGames.RemoveAll(a => a.GameNum == gameNum);
              }
              AddNewGameToList(gameNum);
            }
          }

          if (lch.WagerType == null && lch.PeriodWagerCutoff != null) {
            ChangePeriodGameWagerCutoff(gameNum, periodNumber, (DateTime)lch.PeriodWagerCutoff);
          }
          ChangeGameLineStatus(gameNum, periodNumber, lch);
        }
      }
    }

    private void ChangeGameLineStatus(int gameNum, int periodNumber, spGLGetLastLineChangeByCustomer_Result lch) {
      if (ActiveGames == null)
        return;
      var activeGame = (from p in ActiveGames where p.GameNum == gameNum && p.PeriodNumber == periodNumber select p).ToList();

      if (activeGame.Count <= 0) return;
      foreach (var res in activeGame) {
        res.SpreadLineStatus = lch.SpreadLineStatus;
        res.MoneyLineStatus = lch.MoneyLineStatus;
        res.TotalsLineStatus = lch.TotalsLineStatus;
        res.TeamTotalsLineStatus = lch.TeamTotalsLineStatus;
      }

      if (FilteredActiveGames == null)
        return;
      var fiteredActiveGame = (from p in FilteredActiveGames where p.GameNum == gameNum && p.PeriodNumber == periodNumber select p).ToList();
      if (fiteredActiveGame.Count <= 0) return;
      foreach (var res in fiteredActiveGame) {
        res.SpreadLineStatus = lch.SpreadLineStatus;
        res.MoneyLineStatus = lch.MoneyLineStatus;
        res.TotalsLineStatus = lch.TotalsLineStatus;
        res.TeamTotalsLineStatus = lch.TeamTotalsLineStatus;
      }
    }

    private void ChangeGameParlayRestrictionSetting(int gameNum, int periodNumber, string newParlayRestrictionValue) {
      var activeGame = (from p in ActiveGames where p.GameNum == gameNum && p.PeriodNumber == periodNumber select p).ToList();
      var filteredActiveGame = (from p in ActiveGames where p.GameNum == gameNum && p.PeriodNumber == periodNumber select p).ToList();

      if (activeGame.Count <= 0) return;
      foreach (var res in activeGame) {
        res.ParlayRestriction = newParlayRestrictionValue;
      }

      if (filteredActiveGame.Count <= 0) return;
      foreach (var res in filteredActiveGame) {
        res.ParlayRestriction = newParlayRestrictionValue;
      }
    }

    private void VerifyCurrentlyDisplayedGamesStatus() {
      if (GamesGridViewInUse == null) return;
      var gv = (DataGridView)Controls.Find(GamesGridViewInUse, true).FirstOrDefault();
      if (gv == null || gv.Visible == false || gv.RowCount == 0 || gv.Rows.Count == 0 || gv.Name == "dgvwPropsAndContests")
        return;

      var thisWeekEndScheduleDate = GetThisWeeksScheduleDates().ScheduleEndDate.GetValueOrDefault();

      foreach (DataGridViewRow row in gv.Rows) {

        var gameNum = int.Parse(row.Cells[GAME_NUM_INDEX].Value.ToString());
        var periodNumber = int.Parse(row.Cells[PERIOD_NUM_INDEX].Value.ToString());
        var res = (from r in ActiveGames where r.GameNum == gameNum && r.PeriodNumber == periodNumber select r).FirstOrDefault();
        if (res != null) {
          if (res.PeriodWagerCutoff <= Mdi.GetCurrentDateTime()) {
            row.DefaultCellStyle.BackColor = GetGridRowColor(res.Status, res.PeriodWagerCutoff ?? DateTime.MinValue,
              res.TimeChangeFlag, res.ScheduleDate, res.SportType, thisWeekEndScheduleDate);
          }
        }
      }
    }

    private void UpdateGameLineUiElementsWithBackgroundWorker() {
      FillFrmText();

      if (_lastGamesLineChangeByCustomer != null && _lastGamesLineChangeByCustomer.Count > 0) {

        var gDgvw = (DataGridView)Controls.Find(GamesGridViewInUse, true).FirstOrDefault();
        var cDgvw = (DataGridView)Controls.Find(PropsGridViewInUse, true).FirstOrDefault();

        var gIsVisible = false;
        if (gDgvw != null)
          gIsVisible = gDgvw.Visible;

        var cIsVisible = false;
        if (cDgvw != null)
          cIsVisible = cDgvw.Visible;

        if (gIsVisible && gDgvw.Rows.Count > 0 || cIsVisible && cDgvw.Rows.Count > 0) {
          foreach (var lineChange in _lastGamesLineChangeByCustomer) {
            if (lineChange.WagerType == WagerType.CONTEST) {
              UpdateSpecificContestsInGridView(ContestNodesInfo, lineChange.GameNum, lineChange.ContestantNum, lineChange.CustProfile);
            }
            else {
              UpdateSpecificGameInGridView(CurrSelectedSportBranch, lineChange.GameNum, lineChange.CustProfile);
            }
          }
        }

        foreach (var lineChange in _lastGamesLineChangeByCustomer) {
          if (!RefreshingAvailableSportsTreeIsRequired(lineChange)) continue;
          RefreshAvailableSportsTree();
          GroupSportsByScheduleDate();
        }
      }
      _lastGamesLineChangeByCustomer = null;
    }

    //methods for validating SB and SBManual Wagers begin

    public void CheckForAvailableFunds(Form caller, String frmMode, int ticketNumber, int? wagerNumber, int? itemNumber, String attemptedAction,
      spGLGetActiveGamesByCustomer_Result selectedGamePeriodInfo, spCnGetActiveContests_Result selectedContestInfo,
      String wagerItemDescription, string amtWagered, string toWinAmount, Boolean isFreePlay, out Boolean contValidating, out Boolean availableFundsOk, String ifBetType = null, int? currentIfBetItemNumber = null, DataGridView ifBetGv = null, Boolean ifBetComplexType = false, Boolean checkIfBetComplexType = false) {
      if (ifBetComplexType && !checkIfBetComplexType) {
        contValidating = true;
        availableFundsOk = true;
        return;
      }

      contValidating = true;
      availableFundsOk = false;

      var callerToWinTxt = (TextBox)caller.Controls.Find("txtToWin", true).FirstOrDefault();

      var pendingFreePlayBalanceToRestore = GetFreePlayBalanceToRestoreFromPendingTickets(ticketNumber, wagerNumber);
      //the available balance should be recalculated in order show more funds (the original money placed on the wagers should be added to the available balance)

      var pendingBalanceToRestore = GetBalanceToRestoreFromPendingTickets(ticketNumber, wagerNumber);
      //the available balance should be recalculated in order show more funds (the original money placed on the wagers should be added to the available balance)

      if (isFreePlay) {
        double? availableFreePlayBalance = null;
        if (Mdi.BalanceInfo != null)
          availableFreePlayBalance = Mdi.BalanceInfo.FreePlayAvailable;

        if (frmMode == "Edit") {
          double? readbackFpAmtToRestore = null;
          if (Mdi.Ticket != null && Mdi.Ticket.Wagers != null && Mdi.Ticket.Wagers.Count > 0) {
            readbackFpAmtToRestore = (from w in Mdi.Ticket.Wagers
                                      where w.TicketNumber == ticketNumber && w.WagerNumber == wagerNumber && w.OpenPlayWagerInfo == null
                                      select w.AmountWagered).FirstOrDefault();
          }

          if (readbackFpAmtToRestore != null) {
            availableFreePlayBalance += readbackFpAmtToRestore * 100;
          }
        }

        if (availableFreePlayBalance == null) return;

        if (availableFreePlayBalance / 100 + (pendingFreePlayBalanceToRestore ?? 0) < double.Parse(amtWagered)) {
          var baseMessage = "Remaining free play balance: " + FormatNumber((double)availableFreePlayBalance, true, 100, true);
          baseMessage += Environment.NewLine;
          baseMessage += "Insufficient funds available for the wager of ";
          baseMessage += FormatNumber(double.Parse(amtWagered));
          MessageBox.Show(baseMessage, @"Insufficient Funds");
          if (callerToWinTxt != null)
            callerToWinTxt.Focus();
          contValidating = false;
        }
        else {
          contValidating = true;
          availableFundsOk = true;
        }
        return;
      }

      double? availableBalance = null;
      if (Mdi.BalanceInfo != null)
        availableBalance = Mdi.BalanceInfo.AvailableBalance;

      if (frmMode == "Edit" || (ifBetComplexType && checkIfBetComplexType)) {
        double? readbackAmtToRestore = null;

        if (selectedGamePeriodInfo != null || (ifBetComplexType && checkIfBetComplexType)) {
          if (Mdi.Ticket != null && Mdi.Ticket.Wagers != null && Mdi.Ticket.Wagers.Count > 0) {
            var wagerType = (from w in Mdi.Ticket.Wagers where w.TicketNumber == ticketNumber && w.WagerNumber == wagerNumber select w.WagerType).FirstOrDefault();
            if (wagerType != null) {
              int? itemsCnt;
              switch (wagerType) {
                case WagerType.PARLAY:
                  itemsCnt = (from w in Mdi.Ticket.Wagers where w.TicketNumber == ticketNumber && w.WagerNumber == wagerNumber select w.RoundRobinItemsCount).FirstOrDefault();
                  break;
                case WagerType.IFBET:
                  itemsCnt = (from w in Mdi.Ticket.Wagers where w.TicketNumber == ticketNumber && w.WagerNumber == wagerNumber select w.ArItemsCount).FirstOrDefault();
                  break;
                default:
                  itemsCnt = 1;
                  break;
              }
              readbackAmtToRestore = (from w in Mdi.Ticket.Wagers where w.TicketNumber == ticketNumber && w.WagerNumber == wagerNumber && w.OpenPlayWagerInfo == null select w.AmountWagered).FirstOrDefault();

              if (itemsCnt == null || itemsCnt == 0) {
                itemsCnt = 1;
              }

              if (readbackAmtToRestore != null) {
                if (ifBetComplexType && checkIfBetComplexType) {
                  availableBalance += readbackAmtToRestore * 100;
                }
                else {
                  availableBalance += itemsCnt * readbackAmtToRestore * 100;
                }
              }
            }
          }
        }
        else {
          if (selectedContestInfo != null) {
            if (Mdi.Ticket != null && Mdi.Ticket.ContestWagerItems != null && Mdi.Ticket.ContestWagerItems.Count > 0) {
              readbackAmtToRestore = (from wi in Mdi.Ticket.ContestWagerItems where wi.TicketNumber == ticketNumber && wi.WagerNumber == wagerNumber && wi.ItemNumber == itemNumber select wi.AmountWagered).FirstOrDefault();
            }
            if (readbackAmtToRestore != null) {
              availableBalance += readbackAmtToRestore * 100;
            }
          }
          else
            return;
        }
      }

      if (ifBetType == "winOnly" && currentIfBetItemNumber > 1) {
        double toWinAmt = 0;
        var rowsCnt = ifBetGv.Rows.Count;
        for (var j = 0; j < rowsCnt; j++) {
          var gvTicketNumber = int.Parse(ifBetGv.Rows[j].Cells[24].Value.ToString());
          if (gvTicketNumber == ticketNumber) {
            toWinAmt += double.Parse(ifBetGv.Rows[j].Cells[2].Value.ToString());
          }
        }
        availableBalance += toWinAmt * 100;
      }

      if (availableBalance == null) return;

      if (availableBalance / 100 + (pendingBalanceToRestore ?? 0) < double.Parse(amtWagered)) {
        var baseMessage = "Remaining available balance: " + FormatNumber((double)availableBalance, true, 100, true);

        FrmLateWagerRequest frm;
        var requestDetailsInfo = baseMessage;
        if (selectedGamePeriodInfo != null) {
          frm = new FrmLateWagerRequest(this, selectedGamePeriodInfo, Customer, requestDetailsInfo + " . " + wagerItemDescription, amtWagered, toWinAmount);
        }
        else {
          if (selectedContestInfo != null) {
            frm = new FrmLateWagerRequest(this, selectedContestInfo, Customer, requestDetailsInfo + " . " + wagerItemDescription, amtWagered, toWinAmount);
          }
          else {
            if (ifBetComplexType && checkIfBetComplexType) {
              frm = new FrmLateWagerRequest(this, Customer, "If-Bet", requestDetailsInfo + " . " + wagerItemDescription, amtWagered, toWinAmount);
            }
            else
              return;
          }
        }
        frm.Owner = this;
        frm.Text = @"Insufficient Funds Request";
        frm.Icon = Icon;
        HandleInsufficientAvailableFunds(frm, baseMessage, callerToWinTxt, attemptedAction, amtWagered, out availableFundsOk, out contValidating);
      }
      else {
        contValidating = true;
        availableFundsOk = true;
      }
    }

    private double? GetBalanceToRestoreFromPendingTickets(int ticketNumber, int? wagerNumber) {
      double? pendingBalanceToRestore = 0;
      if (Mdi.Ticket != null && Mdi.Ticket.Wagers != null && Mdi.Ticket.Wagers.Count > 0
        && (from w in Mdi.Ticket.Wagers where w.TicketNumber == ticketNumber && w.WagerNumber == wagerNumber select w).Any()) {
        pendingBalanceToRestore = (from w in Mdi.Ticket.Wagers
                                   where w.TicketNumber == ticketNumber && w.WagerNumber == wagerNumber
                                   && w.OpenPlayWagerInfo != null && w.OpenPlayWagerInfo.WagerStatus == "Pending" && (w.OpenPlayWagerInfo.FreePlayFlag == "Y") == false
                                   select w.AmountWagered).FirstOrDefault();
      }
      else {
        if (Mdi.OpenPlayWagerInfo != null && Mdi.OpenPlayWagerInfo.TicketNumber == ticketNumber &&
            Mdi.OpenPlayWagerInfo.WagerNumber == wagerNumber && Mdi.OpenPlayWagerInfo.WagerStatus == "Pending" && (Mdi.OpenPlayWagerInfo.FreePlayFlag == "Y") == false)
          pendingBalanceToRestore = Mdi.OpenPlayWagerInfo.AmountWagered;
      }

      if (Mdi.Ticket != null && Mdi.Ticket.Wagers != null && Mdi.Ticket.Wagers.Count > 0) {
        pendingBalanceToRestore += (from w in Mdi.Ticket.Wagers
                                    where w.TicketNumber != ticketNumber && w.WagerNumber != wagerNumber
                                    && w.OpenPlayWagerInfo != null && w.OpenPlayWagerInfo.WagerStatus == "Pending" && (w.OpenPlayWagerInfo.FreePlayFlag == "Y") == false
                                    select w.AmountWagered).Sum();
      }
      return pendingBalanceToRestore ?? 0;
    }

    private double? GetFreePlayBalanceToRestoreFromPendingTickets(int ticketNumber, int? wagerNumber) {
      double? pendingFreePlayBalanceToRestore = 0;
      if (Mdi.Ticket != null && Mdi.Ticket.Wagers != null && Mdi.Ticket.Wagers.Count > 0
        && (from w in Mdi.Ticket.Wagers where w.TicketNumber == ticketNumber && w.WagerNumber == wagerNumber select w).Any()) {
        pendingFreePlayBalanceToRestore = (from w in Mdi.Ticket.Wagers
                                           where w.TicketNumber == ticketNumber && w.WagerNumber == wagerNumber
                                           && w.OpenPlayWagerInfo != null && w.OpenPlayWagerInfo.WagerStatus == "Pending" && (w.OpenPlayWagerInfo.FreePlayFlag == "Y")
                                           select w.AmountWagered).FirstOrDefault();
      }
      else {
        if (Mdi.OpenPlayWagerInfo != null && Mdi.OpenPlayWagerInfo.TicketNumber == ticketNumber &&
            Mdi.OpenPlayWagerInfo.WagerNumber == wagerNumber && Mdi.OpenPlayWagerInfo.WagerStatus == "Pending" && (Mdi.OpenPlayWagerInfo.FreePlayFlag == "Y"))
          pendingFreePlayBalanceToRestore = Mdi.OpenPlayWagerInfo.AmountWagered;
      }

      if (Mdi.Ticket != null && Mdi.Ticket.Wagers != null && Mdi.Ticket.Wagers.Count > 0) {
        pendingFreePlayBalanceToRestore += (from w in Mdi.Ticket.Wagers
                                            where w.TicketNumber != ticketNumber && w.WagerNumber != wagerNumber
                                            && w.OpenPlayWagerInfo != null && w.OpenPlayWagerInfo.WagerStatus == "Pending" && (w.OpenPlayWagerInfo.FreePlayFlag == "Y")
                                            select w.AmountWagered).Sum();
      }
      return pendingFreePlayBalanceToRestore;
    }

    private void HandleInsufficientAvailableFunds(FrmLateWagerRequest frm, string baseMessage, TextBox callerToWinTxt, String attemptedAction, string amtWagered, out bool availableFundsOk, out bool contValidating) {
      contValidating = true;
      availableFundsOk = false;
      if (Customer != null && Customer.CreditAcctFlag != null) {
        if (Customer.CreditAcctFlag.Trim() == "Y") {
          baseMessage += " (Credit Limit: ";
          if (Customer.CreditLimit != null) {
            if (Mdi.BalanceInfo.CreditLimit != null)
              baseMessage += FormatNumber((double)Mdi.BalanceInfo.CreditLimit / 100, false);
          }
          baseMessage += ")";
        }
      }
      baseMessage += Environment.NewLine;
      baseMessage += "Insufficient funds available for the wager of ";
      baseMessage += FormatNumber(double.Parse(amtWagered), false, 1, true);

      if (Customer != null && Customer.Currency != null) {
        baseMessage += " " + Customer.Currency.Trim();
      }

      if (LoginsAndProfiles.ValidateUserFunctionalityAccess(Mdi.CurrentUserPermissions, attemptedAction)) {

        var conditionalMessage = baseMessage;
        conditionalMessage += ". Accept this wager anyway?";
        if (MessageBox.Show(conditionalMessage, @"Insufficient Funds", MessageBoxButtons.YesNo) == DialogResult.Yes) {
          availableFundsOk = true;
        }
        else {
          if (callerToWinTxt != null)
            callerToWinTxt.Focus();
          contValidating = false;
        }
      }
      else {
        if (Params.NotifyOfInsufficientFunds) {
          contValidating = false;
          var requestMessage = baseMessage;
          requestMessage += ". Do you want to submit a request for approval?";

          if (MessageBox.Show(requestMessage, @"Insufficient Funds", MessageBoxButtons.YesNo) == DialogResult.Yes) {
            frm.WagerDesc = baseMessage;
            frm.ShowDialog();

            if (AddWagerAuthorizationGranted) {
              availableFundsOk = true;
              contValidating = true;
            }
            else {
              if (callerToWinTxt != null)
                callerToWinTxt.Focus();
            }
          }
          else {
            if (callerToWinTxt != null)
              callerToWinTxt.Focus();
          }
        }
        else {
          MessageBox.Show(baseMessage, @"Insufficient Funds");
          if (callerToWinTxt != null)
            callerToWinTxt.Focus();
        }
      }
    }

    public int CheckIfBelowMinimumWager(Form caller, String attemptedAction, string wagerAmt, out bool contValidating, out bool minWagerByPassed, Boolean ifBetComplexType = false, Boolean checkIfBetComplexType = false, Boolean byPassMinimumWager = false) {
      if (ifBetComplexType && !checkIfBetComplexType) {
        contValidating = true;
        minWagerByPassed = true;
        return -1;
      }

      int minimumWager;
      contValidating = true;
      minWagerByPassed = false;

      var callerToWinTxt = (TextBox)caller.Controls.Find("txtToWin", true).FirstOrDefault() ??
                               (TextBox)caller.Controls.Find("txtRisk", true).FirstOrDefault();

      var customerCurrency = Customer.Currency;
      customerCurrency = customerCurrency != null ? customerCurrency.Trim() : "";

      if (IsBelowMinimumWager(wagerAmt, out minimumWager) && !byPassMinimumWager) {
        double wagerAmtDbl;
        if (!double.TryParse(wagerAmt, out wagerAmtDbl)) {
          minWagerByPassed = false;
          return minimumWager / 100;
        }
        if (LoginsAndProfiles.ValidateUserFunctionalityAccess(Mdi.CurrentUserPermissions, attemptedAction)) {
          if (MessageBox.Show(@"A minimum wager of " + (minimumWager) / 100 + @" " + customerCurrency.Trim() + @" should be entered. Accept this wager anyway?", @"Invalid Amount", MessageBoxButtons.YesNo) == DialogResult.Yes) {
            minWagerByPassed = true;
            if (wagerAmt != "")
              minimumWager = (int)double.Parse(wagerAmt) * 100;
          }
          else {
            if (callerToWinTxt != null)
              callerToWinTxt.Focus();
            contValidating = false;
          }
          return minimumWager / 100;
        }

        MessageBox.Show(@"A minimum wager of " + (minimumWager) / 100 + @" " + customerCurrency.Trim() + @" must be entered.", @"Invalid Amount");
        if (callerToWinTxt != null)
          callerToWinTxt.Focus();
        contValidating = false;
        return minimumWager / 100;
      }
      minWagerByPassed = true;
      return minimumWager / 100;
    }

    public void CheckIfExceedsWagerLimit(Form caller, String frmMode, int ticketNumber, int? wagerNumber, int? itemNumber, String attemptedAction, spGLGetActiveGamesByCustomer_Result selectedGamePeriodInfo, String wagerItemDescription, string amtWagered, string toWinAmount, String itemWagerType, String chosenTeamId, String totalsOu, out bool contValidating, out bool wagerLimitOk, String ifBetType = null, int? currentIfBetItemNumber = null, DataGridView ifBetGv = null, Boolean ifBetComplexType = false, Boolean checkIfBetComplexType = false) {
      if (ifBetComplexType && !checkIfBetComplexType) {
        contValidating = true;
        wagerLimitOk = true;
        return;
      }

      double? currentlyWageredAmt = null; // existing tickets A
      double? stageWageredAmt = null; // on readback B
      contValidating = true;
      wagerLimitOk = false;

      double adjustedWageredAmt = 0; // needed when editing pending plays (from the database)

      var callerToWinTxt = (TextBox)caller.Controls.Find("txtToWin", true).FirstOrDefault();
      var callerRiskTxt = (TextBox)caller.Controls.Find("txtRisk", true).FirstOrDefault();

      string riskOrWinMode;

      if (caller.GetType().Name == "FrmMakeAWager" || caller.GetType().Name == "FrmManualGameWager") {
        riskOrWinMode = caller.GetType().Name == "FrmMakeAWager" ? ((FrmMakeAWager)caller).WagerTypeMode : ((FrmManualGameWager)caller).WagerTypeMode;
      }
      else {
        riskOrWinMode = "ToWin";
      }

      var customerCurrency = Customer.Currency;
      customerCurrency = customerCurrency != null ? customerCurrency.Trim() : "";


      if (ifBetType != null) {
        var limitIfToRisk = Customer.LimitRifToRiskFlag != null ? Customer.LimitRifToRiskFlag.Trim() : "N";
        if (limitIfToRisk == "Y" && currentIfBetItemNumber > 1) {
          if (ifBetGv != null) {
            var targetAmt = ifBetGv.Rows[(int)currentIfBetItemNumber - 2].Cells[1].Value.ToString();
            if (double.Parse(amtWagered) > double.Parse(targetAmt)) {
              contValidating = false;
              var message = "This customer is limited to reinvesting previous risk only.";
              message += Environment.NewLine;
              message += "Reduce the risk amount, then add the if-bet item.";
              MessageBox.Show(message);
              if (callerToWinTxt != null)
                callerToWinTxt.Focus();
              return;
            }
          }
        }
      }

      if (ticketNumber != Mdi.TicketNumber) // needed when editing pending plays (from the database)
            {
        if (Mdi.Ticket.Wagers != null && Mdi.Ticket.WagerItems != null
            && (from wi in Mdi.Ticket.WagerItems
                where wi.TicketNumber == ticketNumber && wi.WagerNumber == wagerNumber && wi.ItemNumber == itemNumber
                    && wi.PendingPlayItemInDb != null && wi.PendingPlayItemInDb.TicketNumber == ticketNumber && wi.PendingPlayItemInDb.WagerNumber == wagerNumber
                    && wi.PendingPlayItemInDb.ItemNumber == itemNumber
                    && wi.PendingPlayItemInDb.GameNum == selectedGamePeriodInfo.GameNum && wi.PendingPlayItemInDb.PeriodNumber == selectedGamePeriodInfo.PeriodNumber
                    && wi.PendingPlayItemInDb.ChosenTeamID.Trim().ToLower() == chosenTeamId.Trim().ToLower()
                    && ((WagerLimitsByLine == "Y" && wi.PendingPlayItemInDb.WagerType == itemWagerType) || WagerLimitsByGame == "Y")
                select wi).Any()
            ) {
          adjustedWageredAmt = (from wi in Mdi.Ticket.WagerItems
                                where wi.TicketNumber == ticketNumber && wi.WagerNumber == wagerNumber && wi.ItemNumber == itemNumber
                                select (wi.PendingPlayItemInDb.VolumeAmount ?? 0)).First();
        }
        else {
          if (Mdi.PendingPlayItemsInDb != null
              && (from pi in Mdi.PendingPlayItemsInDb
                  where pi.TicketNumber == ticketNumber && pi.WagerNumber == wagerNumber && pi.ItemNumber == itemNumber
               && pi.GameNum == selectedGamePeriodInfo.GameNum && pi.PeriodNumber == selectedGamePeriodInfo.PeriodNumber
                && pi.ChosenTeamID.Trim().ToLower() == chosenTeamId.Trim().ToLower()
                && ((WagerLimitsByLine == "Y" && pi.WagerType == itemWagerType) || WagerLimitsByGame == "Y")
                  select pi).Any()
              ) {
            adjustedWageredAmt = (from wi2 in Mdi.PendingPlayItemsInDb
                                  where wi2.TicketNumber == ticketNumber && wi2.WagerNumber == wagerNumber && wi2.ItemNumber == itemNumber
                                  select (wi2.VolumeAmount ?? 0)).First();
          }
        }
      }

      if (WagerLimitsByLine == "Y") {
        var origTotalPoints = TwUtilities.GetOriginalTotalPoints(selectedGamePeriodInfo, itemWagerType, chosenTeamId);
        var origMoneyLine = GetOriginalMoneyLine(selectedGamePeriodInfo, itemWagerType, chosenTeamId, totalsOu);

        using (var gl = new GamesAndLines(AppModuleInfo)) {
          currentlyWageredAmt = gl.GetAccumWagerAmountByLine(CurrentCustomerId, selectedGamePeriodInfo.GameNum, selectedGamePeriodInfo.PeriodNumber, itemWagerType, chosenTeamId, totalsOu, selectedGamePeriodInfo.Spread, origTotalPoints, origMoneyLine);
        }
        currentlyWageredAmt -= adjustedWageredAmt;

        int tTicketNumber = 0;
        int? tWagerNumber = null, tItemNumber = null;
        if (ifBetComplexType) {
          tTicketNumber = ticketNumber;
          tWagerNumber = wagerNumber;
          tItemNumber = itemNumber;
        }
        stageWageredAmt = frmMode == "Edit" ?
            GamesAndLines.GetStageAccumWagerAmountByLine(Mdi.Ticket, selectedGamePeriodInfo.GameNum, selectedGamePeriodInfo.PeriodNumber, itemWagerType, chosenTeamId, totalsOu, selectedGamePeriodInfo.Spread, origTotalPoints, origMoneyLine, ticketNumber, wagerNumber, itemNumber, ifBetComplexType) :
            GamesAndLines.GetStageAccumWagerAmountByLine(Mdi.Ticket, selectedGamePeriodInfo.GameNum, selectedGamePeriodInfo.PeriodNumber, itemWagerType, chosenTeamId, totalsOu, selectedGamePeriodInfo.Spread, origTotalPoints, origMoneyLine, tTicketNumber, tWagerNumber, tItemNumber, ifBetComplexType);
      }
      else {
        if (WagerLimitsByGame == "Y") {
          using (var gl = new GamesAndLines(AppModuleInfo)) {
            currentlyWageredAmt = gl.GetAccumWagerAmount(CurrentCustomerId, selectedGamePeriodInfo.GameNum, selectedGamePeriodInfo.PeriodNumber, itemWagerType, chosenTeamId, totalsOu);
            currentlyWageredAmt -= adjustedWageredAmt;
          }

          using (var gl = new GamesAndLines(AppModuleInfo)) {
            var tTicketNumber = Mdi.TicketNumber;
            int? tWagerNumber = null, tItemNumber = null;
            if (ifBetComplexType) {
              tTicketNumber = ticketNumber;
              tWagerNumber = wagerNumber;
              tItemNumber = itemNumber;
            }
            stageWageredAmt = frmMode == "Edit" ?
                gl.GetStageAccumWagerAmount(Mdi.Ticket, selectedGamePeriodInfo.GameNum, selectedGamePeriodInfo.PeriodNumber, itemWagerType, chosenTeamId, totalsOu, ticketNumber, wagerNumber, itemNumber, ifBetComplexType) :
                gl.GetStageAccumWagerAmount(Mdi.Ticket, selectedGamePeriodInfo.GameNum, selectedGamePeriodInfo.PeriodNumber, itemWagerType, chosenTeamId, totalsOu, tTicketNumber, tWagerNumber, tItemNumber, ifBetComplexType);
          }
        }
      }

      var currentWagerLimitAmt = GetLimitForCurrentWager(selectedGamePeriodInfo, itemWagerType, itemWagerType);
      var riskToWin = new[] { double.Parse(amtWagered), double.Parse(toWinAmount) };
      var volAmt = riskToWin.Min();

      if (currentlyWageredAmt == null)
        currentlyWageredAmt = 0;
      if (stageWageredAmt == null)
        stageWageredAmt = 0;

      var totalAlreadyWageredAmt = (double)currentlyWageredAmt / 100 + (double)stageWageredAmt;

      if (currentWagerLimitAmt == 0 || (currentWagerLimitAmt > 0 && volAmt > ((currentWagerLimitAmt / 100) - totalAlreadyWageredAmt))) {
        String baseMessage;
        if (currentWagerLimitAmt == 0) {
          baseMessage = "(A)The current wager of " + FormatNumber(volAmt) + " " + customerCurrency + " exceeds the system maximum wager of " + FormatNumber((currentWagerLimitAmt / 100));
        }
        else {
          if ((currentWagerLimitAmt / 100) - totalAlreadyWageredAmt >= 0) {
            baseMessage = "(B)The current wager of " + FormatNumber(volAmt) + " " + customerCurrency + " exceeds the system maximum wager of " + FormatNumber((currentWagerLimitAmt / 100) - totalAlreadyWageredAmt);
          }
          else {
            baseMessage = "(C)The current wager of " + FormatNumber(volAmt) + " " + customerCurrency + " exceeds the system maximum wager of 0";
          }
        }

        if (totalAlreadyWageredAmt > 0) {
          baseMessage += " (" + FormatNumber(totalAlreadyWageredAmt) + " already bet)";
        }

        if (LoginsAndProfiles.ValidateUserFunctionalityAccess(Mdi.CurrentUserPermissions, attemptedAction)) {
          var conditionalMessage = baseMessage;
          conditionalMessage += ". Accept this wager anyway?";
          if (MessageBox.Show(conditionalMessage, @"", MessageBoxButtons.YesNo) == DialogResult.Yes) {
            wagerLimitOk = true;
          }
          else {
            if (riskOrWinMode == "ToWin") {
              if (callerToWinTxt != null)
                callerToWinTxt.Focus();
            }
            else {
              if (callerRiskTxt != null)
                callerRiskTxt.Focus();
            }
            contValidating = false;
          }
          return;
        }

        var permissionRequestMessage = baseMessage;
        var frm = new FrmLateWagerRequest(this, selectedGamePeriodInfo, Customer, permissionRequestMessage + " . " + wagerItemDescription, amtWagered, toWinAmount) {
          Owner = this,
          Text = @"Request to Exceed Maximum Wager",
          Icon = Icon
        };
        NotifyToExceedMaxWager(frm, baseMessage, callerToWinTxt, callerRiskTxt, riskOrWinMode, out contValidating, out wagerLimitOk);
      }
      else {
        contValidating = true;
        wagerLimitOk = true;
      }
    }

    private void NotifyToExceedMaxWager(FrmLateWagerRequest frm, string baseMessage, TextBox callerToWinTxt, TextBox callerRiskTxt, string riskOrWinMode, out bool contValidating, out bool wagerLimitOk) {
      wagerLimitOk = false;
      contValidating = false;
      if (Params.NotifyToExceedMaxWager) {
        var requestMessage = baseMessage;
        requestMessage += ". Do you want to submit a request for approval?";

        if (MessageBox.Show(requestMessage, @"Maximum Wager Exceeded", MessageBoxButtons.YesNo) == DialogResult.Yes) {
          frm.ShowDialog();

          if (AddWagerAuthorizationGranted) {
            wagerLimitOk = true;
            contValidating = true;
          }
          else {
            if (riskOrWinMode == "ToWin") {
              if (callerToWinTxt != null)
                callerToWinTxt.Focus();
            }
            else {
              if (callerRiskTxt != null)
                callerRiskTxt.Focus();
            }
          }
        }
        else {
          if (riskOrWinMode == "ToWin") {
            if (callerToWinTxt != null)
              callerToWinTxt.Focus();
          }
          else {
            if (callerRiskTxt != null)
              callerRiskTxt.Focus();
          }
        }
      }
      else {
        MessageBox.Show(baseMessage, @"Maximum Wager Exceeded");
        if (riskOrWinMode == "ToWin") {
          if (callerToWinTxt != null)
            callerToWinTxt.Focus();
        }
        else {
          if (callerRiskTxt != null)
            callerRiskTxt.Focus();
        }
      }
    }

    public void CheckIfWagerCutoffHasBeenReached(Form caller, String attemptedAction, spGLGetActiveGamesByCustomer_Result selectedGamePeriodInfo, String wagerItemDescription, string amtWagered, string toWinAmount, DateTime cutoffTime, out Boolean contValidating, out Boolean wagerCutoffByPassed) {
      contValidating = true;
      wagerCutoffByPassed = false;

      var callerToWinTxt = (TextBox)caller.Controls.Find("txtToWin", true).FirstOrDefault();
      string gameStatus;
      if (!GameIsAvailable(selectedGamePeriodInfo.GameNum, selectedGamePeriodInfo.PeriodNumber, out gameStatus))
        return;

      var frm = new FrmLateWagerRequest(this, selectedGamePeriodInfo, Customer, wagerItemDescription, amtWagered, toWinAmount) {
        Owner = this,
        Text = @"Late Wager Request",
        Icon = Icon
      };
      const string baseMessage = "The game period has already started or is not currently available.";
      CheckIfWagerCutoffHasBeenReached(cutoffTime, baseMessage, attemptedAction, callerToWinTxt, frm, out contValidating, out wagerCutoffByPassed);
      frm.Dispose();
    }

    private void CheckIfWagerCutoffHasBeenReached(DateTime cutoffTime, string baseMessage, string attemptedAction, TextBox callerToWinTxt, FrmLateWagerRequest frm, out bool contValidating, out bool wagerCutoffByPassed) {
      contValidating = true;
      wagerCutoffByPassed = false;
      if (cutoffTime < Mdi.GetCurrentDateTime()) // Validate if wagerCutoff has been passed
            {
        if (LoginsAndProfiles.ValidateUserFunctionalityAccess(Mdi.CurrentUserPermissions, attemptedAction)) {
          var conditionalMessage = baseMessage;
          conditionalMessage += " Accept this wager anyway?";
          if (MessageBox.Show(conditionalMessage, @"Not available", MessageBoxButtons.YesNo) == DialogResult.Yes) {
            wagerCutoffByPassed = true;
          }
          else {
            if (callerToWinTxt != null)
              callerToWinTxt.Focus();
            contValidating = false;
          }
          return;
        }
        if (Params.NotificationMinutesAfterCutoff > 0) {
          var dtNow = ServerDateTime;
          contValidating = false;
          if (dtNow > cutoffTime && dtNow < cutoffTime.AddMinutes(Params.NotificationMinutesAfterCutoff)) {
            var requestMessage = baseMessage;
            requestMessage += ". Do you want to submit a request for approval?";

            if (MessageBox.Show(requestMessage, @"Not available", MessageBoxButtons.YesNo) == DialogResult.Yes) {
              frm.ShowDialog();

              if (AddWagerAuthorizationGranted) {
                wagerCutoffByPassed = true;
                contValidating = true;
              }
              else {
                if (callerToWinTxt != null)
                  callerToWinTxt.Focus();
              }
            }
            else {
              if (callerToWinTxt != null)
                callerToWinTxt.Focus();
            }
          }
          else {
            MessageBox.Show(baseMessage, @"Not available");
            if (callerToWinTxt != null)
              callerToWinTxt.Focus();
          }
        }
        else {
          MessageBox.Show(baseMessage, @"Not available");
          if (callerToWinTxt != null)
            callerToWinTxt.Focus();
          contValidating = false;
        }
      }
      else {
        wagerCutoffByPassed = true;
      }
    }

    public void CheckIfWagerCutoffHasBeenReachedInReadback(String attemptedAction, String requestTypeDesc, spGLGetActiveGamesByCustomer_Result selectedGamePeriodInfo, String wagerItemDescription, string amtWagered, string toWinAmount, out Boolean wagerCutoffByPassed) {
      wagerCutoffByPassed = false;

      DateTime? cutoffTime;
      if (selectedGamePeriodInfo != null) {
        cutoffTime = selectedGamePeriodInfo.PeriodWagerCutoff;
      }
      else
        return;

      String gameStatus;

      if (cutoffTime != null && cutoffTime < Mdi.GetCurrentDateTime() || (!GameIsAvailable(selectedGamePeriodInfo.GameNum, selectedGamePeriodInfo.PeriodNumber, out gameStatus))) // Validate if wagerCutoff has been passed
            {
        if (LoginsAndProfiles.ValidateUserFunctionalityAccess(Mdi.CurrentUserPermissions, attemptedAction)) {
          wagerCutoffByPassed = true;
        }
        else {
          SendWagerPermissionRequest(selectedGamePeriodInfo, cutoffTime, wagerItemDescription, amtWagered, toWinAmount, requestTypeDesc, out wagerCutoffByPassed);
        }
      }
      else {
        wagerCutoffByPassed = true;
      }

    }

    //methods for validating SB and SBManual Wagers end

    //methods for validating Contests begin

    public void CheckIfContestWagerCutoffHasBeenReached(Form caller, String attemptedAction, spCnGetActiveContests_Result selectedGamePeriodInfo, String wagerItemDescription, string amtWagered, string toWinAmount, DateTime cutoffTime, out Boolean contValidating, out Boolean wagerCutoffByPassed) {
      var callerToWinTxt = (TextBox)caller.Controls.Find("txtToWin", true).FirstOrDefault();

      var frm = new FrmLateWagerRequest(this, selectedGamePeriodInfo, Customer, wagerItemDescription, amtWagered, toWinAmount) {
        Owner = this,
        Text = @"Late Wager Request",
        Icon = Icon
      };
      const string baseMessage = "The wager cutoff time has expired.";
      CheckIfWagerCutoffHasBeenReached(cutoffTime, baseMessage, attemptedAction, callerToWinTxt, frm, out contValidating, out wagerCutoffByPassed);
      frm.Dispose();
    }

    public void CheckIfContestWagerCutoffHasBeenReachedInReadback(String attemptedAction, String requestTypeDesc, spCnGetActiveContests_Result selectedContestInfo, String wagerItemDescription, string amtWagered, string toWinAmount, out Boolean wagerCutoffByPassed) {

      DateTime? cutoffTime = null;
      if (selectedContestInfo != null) {
        cutoffTime = selectedContestInfo.WagerCutoff;
      }

      if (cutoffTime != null && cutoffTime < Mdi.GetCurrentDateTime()) // Validate if wagerCutoff has been passed
            {
        if (LoginsAndProfiles.ValidateUserFunctionalityAccess(Mdi.CurrentUserPermissions, attemptedAction)) {
          wagerCutoffByPassed = true;
        }
        else {
          const string baseMessage = "The wager cutoff time has expired.";
          var frm = new FrmLateWagerRequest(this, selectedContestInfo, Customer, wagerItemDescription, amtWagered, toWinAmount) {
            Owner = this,
            Text = requestTypeDesc,
            Icon = Icon
          };
          SendNotificationMinutesAfterCutoff(frm, cutoffTime, baseMessage, out wagerCutoffByPassed);
        }
      }
      else {
        wagerCutoffByPassed = true;
      }

    }

    private void SendNotificationMinutesAfterCutoff(FrmLateWagerRequest frm, DateTime? cutoffTime, string baseMessage, out bool wagerCutoffByPassed) {
      wagerCutoffByPassed = false;
      if (Params.NotificationMinutesAfterCutoff > 0) {
        var dtNow = ServerDateTime;
        var cutoffTimeNoNull = cutoffTime.GetValueOrDefault();

        if (dtNow > cutoffTimeNoNull && dtNow < cutoffTimeNoNull.AddMinutes(Params.NotificationMinutesAfterCutoff)) {
          frm.ShowDialog();

          if (AddWagerAuthorizationGranted) {
            wagerCutoffByPassed = true;
          }
        }
        else {
          MessageBox.Show(baseMessage, @"Not available");
        }
      }
      else {
        MessageBox.Show(baseMessage, @"Not available");
      }
    }

    public void CheckIfExceedsContestWagerLimit(Form caller, String frmMode, int ticketNumber, int? wagerNumber, int? itemNumber, String attemptedAction, spCnGetActiveContests_Result selectedGamePeriodInfo, String wagerItemDescription, string amtWagered, string toWinAmount, out bool contValidating, out bool wagerLimitOk) {
      double currentlyWageredAmt; // existing tickets A
      double stageWageredAmt; // on readback B
      double adjustedWageredAmt = 0; // needed when editing pending plays (from the database)
      contValidating = true;
      wagerLimitOk = false;

      var callerToWinTxt = (TextBox)caller.Controls.Find("txtToWin", true).FirstOrDefault();

      var customerCurrency = Customer.Currency;
      customerCurrency = customerCurrency != null ? customerCurrency.Trim() : "";

      if (ticketNumber != Mdi.TicketNumber) // needed when editing pending plays (from the database)
            {
        if (Mdi.Ticket.Wagers != null && Mdi.Ticket.ContestWagerItems != null
            && (from cwi in Mdi.Ticket.ContestWagerItems
                where cwi.TicketNumber == ticketNumber && cwi.WagerNumber == wagerNumber && cwi.ItemNumber == itemNumber
                    && cwi.PendingContestItemInDb != null && cwi.PendingContestItemInDb.TicketNumber == ticketNumber && cwi.PendingContestItemInDb.WagerNumber == wagerNumber
                    && cwi.PendingContestItemInDb.ItemNumber == itemNumber
                    && cwi.PendingContestItemInDb.ContestNum == selectedGamePeriodInfo.ContestNum && cwi.PendingContestItemInDb.ContestantNum == selectedGamePeriodInfo.ContestantNum
                select cwi).Any()
            ) {
          adjustedWageredAmt = (from cwi in Mdi.Ticket.ContestWagerItems
                                where cwi.TicketNumber == ticketNumber && cwi.WagerNumber == wagerNumber && cwi.ItemNumber == itemNumber
                                select (cwi.PendingContestItemInDb.VolumeAmount ?? 0)).First();
        }
        else {
          if (Mdi.PendingPlayItemsInDb != null
              && (Mdi.PendingContestItemInDb.TicketNumber == ticketNumber && Mdi.PendingContestItemInDb.WagerNumber == wagerNumber && Mdi.PendingContestItemInDb.ItemNumber == itemNumber
                    && Mdi.PendingContestItemInDb.ContestNum == selectedGamePeriodInfo.ContestNum
                    && Mdi.PendingContestItemInDb.ContestantNum == selectedGamePeriodInfo.ContestantNum
                 )
              ) {
            adjustedWageredAmt = (from wi2 in Mdi.PendingPlayItemsInDb
                                  where wi2.TicketNumber == ticketNumber && wi2.WagerNumber == wagerNumber && wi2.ItemNumber == itemNumber
                                  select (wi2.VolumeAmount ?? 0)).First();
          }
        }
      }


      if (WagerLimitsByLine == "Y") {
        using (var gl = new GamesAndLines(AppModuleInfo)) {
          currentlyWageredAmt = gl.GetAccumContestWagerAmountByLine(CurrentCustomerId, selectedGamePeriodInfo.ContestNum, selectedGamePeriodInfo.ContestantNum, selectedGamePeriodInfo.XtoYLineRep, selectedGamePeriodInfo.ThresholdType, selectedGamePeriodInfo.ThresholdLine, selectedGamePeriodInfo.MoneyLine, selectedGamePeriodInfo.ToBase) ?? 0;
          currentlyWageredAmt -= adjustedWageredAmt;

          stageWageredAmt = frmMode == "Edit" ?
            gl.GetStageAccumContestWagerAmountByLine(Mdi.Ticket, selectedGamePeriodInfo.ContestNum, selectedGamePeriodInfo.ContestantNum, selectedGamePeriodInfo.XtoYLineRep, selectedGamePeriodInfo.ThresholdType, selectedGamePeriodInfo.ThresholdLine, selectedGamePeriodInfo.MoneyLine, selectedGamePeriodInfo.ToBase, ticketNumber, wagerNumber, itemNumber) ?? 0 :
            gl.GetStageAccumContestWagerAmountByLine(Mdi.Ticket, selectedGamePeriodInfo.ContestNum, selectedGamePeriodInfo.ContestantNum, selectedGamePeriodInfo.XtoYLineRep, selectedGamePeriodInfo.ThresholdType, selectedGamePeriodInfo.ThresholdLine, selectedGamePeriodInfo.MoneyLine, selectedGamePeriodInfo.ToBase, ticketNumber, null, null) ?? 0;
        }
      }
      else {
        using (var gl = new GamesAndLines(AppModuleInfo)) {
          currentlyWageredAmt = gl.GetAccumContestWagerAmount(CurrentCustomerId, selectedGamePeriodInfo.ContestNum, selectedGamePeriodInfo.ContestantNum) ?? 0;
          currentlyWageredAmt -= adjustedWageredAmt;

          //stageWageredAmt = frmMode == "Edit" ?
          stageWageredAmt = gl.GetStageAccumContestWagerAmount(Mdi.Ticket, selectedGamePeriodInfo.ContestNum, selectedGamePeriodInfo.ContestantNum, ticketNumber, wagerNumber, itemNumber) ?? 0; //:
          //gl.GetStageAccumContestWagerAmount(Mdi.Ticket, selectedGamePeriodInfo.ContestNum, selectedGamePeriodInfo.ContestantNum) ?? 0;
        }
      }

      var currentWagerLimitAmt = GetLimitForCurrentContestWager(selectedGamePeriodInfo).Limit;
      var volAmt = (new[] { double.Parse(amtWagered), double.Parse(toWinAmount) }).Min();

      var totalAlreadyWageredAmt = currentlyWageredAmt / 100 + stageWageredAmt;

      if (currentWagerLimitAmt == 0 || (currentWagerLimitAmt > 0 && volAmt > ((currentWagerLimitAmt / 100) - totalAlreadyWageredAmt))) {
        String baseMessage;
        if (currentWagerLimitAmt == 0) {
          baseMessage = "The current wager of " + FormatNumber(volAmt) + " " + customerCurrency + " exceeds the system maximum wager of " + FormatNumber((currentWagerLimitAmt / 100));
        }
        else {
          baseMessage = "The current wager of " + FormatNumber(volAmt) + " " + customerCurrency + " exceeds the system maximum wager of " + FormatNumber((currentWagerLimitAmt / 100) - totalAlreadyWageredAmt);
        }

        if (totalAlreadyWageredAmt > 0) {
          baseMessage += " (" + FormatNumber(totalAlreadyWageredAmt) + " already bet)";
        }

        if (LoginsAndProfiles.ValidateUserFunctionalityAccess(Mdi.CurrentUserPermissions, attemptedAction)) {
          var conditionalMessage = baseMessage;
          conditionalMessage += ". Accept this wager anyway?";
          if (MessageBox.Show(conditionalMessage, @"", MessageBoxButtons.YesNo) == DialogResult.Yes) {
            wagerLimitOk = true;
          }
          else {
            if (callerToWinTxt != null)
              callerToWinTxt.Focus();
            contValidating = false;
          }
        }
        else {
          var permissionRequestMessage = baseMessage;
          const string riskOrWinMode = "ToWin";
          var frm = new FrmLateWagerRequest(this, selectedGamePeriodInfo, Customer, permissionRequestMessage + " . " + wagerItemDescription, amtWagered, toWinAmount) {
            Owner = this,
            Text = @"Request to Exceed Maximum Wager",
            Icon = Icon
          };
          NotifyToExceedMaxWager(frm, baseMessage, callerToWinTxt, null, riskOrWinMode, out contValidating, out wagerLimitOk);
        }
      }
      else {
        wagerLimitOk = true;
      }
    }

    //methods for validating Contests end

    //methods for validating Parlays begin

    public void CheckForParlayTeaserAvailableFunds(Form caller, String frmMode, int ticketNumber, int? wagerNumber, String attemptedAction, String wagerDescription, string amtWagered, string toWinAmount, Boolean isFreePlay, out Boolean contValidating, out Boolean availableFundsOk) {
      contValidating = true;
      availableFundsOk = false;

      var callerToWinTxt = (TextBox)caller.Controls.Find("txtRisk", true).FirstOrDefault() ??
                         (TextBox)caller.Controls.Find("txtToWin", true).FirstOrDefault();

      var pendingFreePlayBalanceToRestore = GetFreePlayBalanceToRestoreFromPendingTickets(ticketNumber, wagerNumber);
      //the available balance should be recalculated in order show more funds (the original money placed on the wagers should be added to the available balance)

      var pendingBalanceToRestore = GetBalanceToRestoreFromPendingTickets(ticketNumber, wagerNumber);
      //the available balance should be recalculated in order show more funds (the original money placed on the wagers should be added to the available balance)

      //********************************
      if (isFreePlay) {
        double? availableFreePlayBalance = null;
        if (Mdi.BalanceInfo != null)
          availableFreePlayBalance = Mdi.BalanceInfo.FreePlayAvailable;

        if (frmMode == "Edit") {
          double? readbackFpAmtToRestore = null;
          if (Mdi.Ticket != null && Mdi.Ticket.Wagers != null && Mdi.Ticket.Wagers.Count > 0) {
            readbackFpAmtToRestore = (from w in Mdi.Ticket.Wagers where w.TicketNumber == ticketNumber && w.WagerNumber == wagerNumber select w.AmountWagered).FirstOrDefault();
          }

          if (readbackFpAmtToRestore != null) {
            availableFreePlayBalance += readbackFpAmtToRestore * 100;
          }
        }

        if (availableFreePlayBalance == null) return;
        if (availableFreePlayBalance / 100 + pendingFreePlayBalanceToRestore < double.Parse(amtWagered)) {
          var baseMessage = "Remaining free play balance: " + FormatNumber((double)availableFreePlayBalance, true, 100, true);
          baseMessage += Environment.NewLine;
          baseMessage += "Insufficient funds available for the wager of ";
          baseMessage += FormatNumber(double.Parse(amtWagered));
          MessageBox.Show(baseMessage, @"Insufficient Funds");
          if (callerToWinTxt != null)
            callerToWinTxt.Focus();
          contValidating = false;
        }
        else {
          contValidating = true;
          availableFundsOk = true;
        }
        return;
      }
      //********************************
      double? availableBalance = null;
      if (Mdi.BalanceInfo != null)
        availableBalance = Mdi.BalanceInfo.AvailableBalance;

      if (amtWagered == "")
        amtWagered = "0";

      if (toWinAmount == "")
        toWinAmount = "0";

      if (frmMode == "Edit") {
        double? readbackAmtToRestore = null;
        int? rrItemsCount = null;

        if (Mdi.Ticket != null && Mdi.Ticket.Wagers != null && Mdi.Ticket.Wagers.Count > 0) {
          readbackAmtToRestore = (from w in Mdi.Ticket.Wagers where w.TicketNumber == ticketNumber && w.WagerNumber == wagerNumber select w.AmountWagered).FirstOrDefault();
          rrItemsCount = (from w in Mdi.Ticket.Wagers where w.TicketNumber == ticketNumber && w.WagerNumber == wagerNumber select w.RoundRobinItemsCount).FirstOrDefault();
        }
        if (readbackAmtToRestore != null) {
          if (rrItemsCount > 0) {
            availableBalance += readbackAmtToRestore * (int)rrItemsCount * 100;
          }
          else {
            availableBalance += readbackAmtToRestore * 100;
          }
        }
      }

      if (availableBalance == null) return;
      if (availableBalance / 100 + pendingBalanceToRestore < double.Parse(amtWagered)) {
        var baseMessage = "Remaining available balance: " + FormatNumber((double)availableBalance, true, 100, true);
        var requestDetailsInfo = baseMessage;
        var frm = new FrmLateWagerRequest(this, Customer, SelectedWagerType, requestDetailsInfo + " . " + wagerDescription, amtWagered, toWinAmount) {
          Owner = this,
          Text = @"Insufficient Funds Request",
          Icon = Icon
        };
        HandleInsufficientAvailableFunds(frm, baseMessage, callerToWinTxt, attemptedAction, amtWagered, out availableFundsOk, out contValidating);
      }
      else {
        availableFundsOk = true;
      }
    }

    public void CheckIfParlayOrTeaserExceedsWagerLimit(Form caller, int ticketNumber, int? wagerNumber, String attemptedAction, String wagerDescription, string wagerStatus, string amtWagered, string toWinAmount, double volAmt, out bool contValidating, out bool wagerLimitOk) {
      //String itemWagerType, String chosenTeamId, String totalsOU

      double currentlyWageredAmt = 0; // existing wagers in db ("P" or "O")
      double currentlySameWageredAmt = 0; // existing exact same wagers in db and WagerStatus = "P"
      double stageWageredAmt = 0; // existing wagers in readback ("P" or "O")
      double stageSameWagerAmt = 0; // existing exact same wagers in readback and WagerStatus = "P"

      contValidating = true;
      wagerLimitOk = false;

      if (amtWagered == "")
        amtWagered = "0";

      var callerToWinTxt = (TextBox)caller.Controls.Find("txtToWin", true).FirstOrDefault();
      var callerRiskTxt = (TextBox)caller.Controls.Find("txtRisk", true).FirstOrDefault();

      var customerCurrency = Customer.Currency;
      customerCurrency = customerCurrency != null ? customerCurrency.Trim() : "";

      List<Ticket.WagerItem> multiSelectedItems = null;

      if (Mdi.Ticket != null && Mdi.Ticket.WagerItems != null && Mdi.Ticket.WagerItems.Count > 0) {
        multiSelectedItems = (from p in Mdi.Ticket.WagerItems where p.TicketNumber == ticketNumber && p.WagerNumber == wagerNumber select p).ToList();
      }

      var rrCheckBox = (CheckBox)caller.Controls.Find("chbRoundRobin", true).FirstOrDefault();
      var isRoundRobinWager = false;
      if (rrCheckBox != null)
        isRoundRobinWager = rrCheckBox.Checked;

      var allowTeaserWager = true;
      var currentWagerLimitAmt = GetLimitForCurrentParlayOrTeaser(caller.AccessibleName.Substring(0, 1), ticketNumber, wagerNumber).Limit;

      double keyRuleLimit = 0;

      using (var gl = new GamesAndLines(AppModuleInfo)) {
        if (SelectedWagerType == "Teaser" && _limitTeasersOnlyKeyRule.IsActive()) {
          allowTeaserWager = _limitTeasersOnlyKeyRule.LimitTeaserOnlyKeyRulePassed(gl, CurrentCustomerId, isRoundRobinWager, Mdi.Ticket, ticketNumber, wagerNumber, multiSelectedItems);
        }
        else {
          if (wagerStatus == "P") {
            currentlySameWageredAmt = gl.GetMultiSelectionWagerAmount(CurrentCustomerId, caller.AccessibleName.Substring(0, 1), isRoundRobinWager, multiSelectedItems) ?? 0;
            stageSameWagerAmt = GetStageWageredAmount(caller, Mdi.Ticket, ticketNumber, wagerNumber, multiSelectedItems, false) ?? 0;
          }

          if (((currentlySameWageredAmt / 100) + stageSameWagerAmt + volAmt) <= currentWagerLimitAmt / 100 && _limitParlaysAndTeasersKeyRule.IsActive() && ParlayAndTeaserRestrictionKeyFactor != null) {
              if (Mdi.Ticket != null)
              {
                  multiSelectedItems = (from p in Mdi.Ticket.WagerItems
                      where string.IsNullOrEmpty(p.FromPreviousTicket) && string.IsNullOrEmpty(p.FromPreviousWagerNumber) &&
                            p.TicketNumber == ticketNumber && p.WagerNumber == wagerNumber
                      select p).ToList();

                  currentlyWageredAmt = gl.GetAccumulatedMultiSelectionWagerAmount(CurrentCustomerId, caller.AccessibleName.Substring(0, 1), isRoundRobinWager, multiSelectedItems) ?? 0;
                  stageWageredAmt = _limitParlaysAndTeasersKeyRule.GetAccumulatedStageWageredAmount(caller, Mdi.Ticket, ticketNumber, wagerNumber, multiSelectedItems, true) ?? 0;
              }
          }
        }
      }
      var totalAlreadyWageredAmt = (currentlyWageredAmt / 100) + stageWageredAmt;

      if (_limitParlaysAndTeasersKeyRule.IsActive() && (currentlyWageredAmt / 100) + stageWageredAmt > 0 && volAmt <= (currentWagerLimitAmt / 100)) {
        keyRuleLimit = _limitParlaysAndTeasersKeyRule.AppplyParlaysAndTeasersKeyRule(currentWagerLimitAmt);
      }

      //Check For Exact wagers limitation
      if (currentWagerLimitAmt == 0
          || ((currentlySameWageredAmt / 100 + stageSameWagerAmt + volAmt) > (currentWagerLimitAmt / 100)) // checks for exact wager
          || (currentWagerLimitAmt > 0 && (keyRuleLimit == 0 && volAmt > ((currentWagerLimitAmt / 100) - (currentlySameWageredAmt / 100 + stageSameWagerAmt)))) // checks for exact wagers
          || (keyRuleLimit > 0 && volAmt > ((keyRuleLimit / 100) - totalAlreadyWageredAmt)) //checks for the key rule
          || (_limitTeasersOnlyKeyRule.IsActive() && !allowTeaserWager)) {

        /*}

        if (currentWagerLimitAmt == 0 ||
            (currentlyWageredAmt + volAmt > (currentWagerLimitAmt / 100)) ||
          (stageSameWagerAmt + volAmt > (currentWagerLimitAmt / 100)) ||
            (currentWagerLimitAmt > 0 && (keyRuleLimit == 0 && volAmt > ((currentWagerLimitAmt / 100) - totalAlreadyWageredAmt))) ||
            (keyRuleLimit > 0 && volAmt > ((keyRuleLimit / 100) - totalAlreadyWageredAmt)) ||
            (_limitTeasersOnlyKeyRule.IsActive() && !allowTeaserWager))
        {*/
        String baseMessage;
        if (_limitParlaysAndTeasersKeyRule.IsActive() && (keyRuleLimit > 0 && volAmt >= ((keyRuleLimit / 100) - totalAlreadyWageredAmt))) {
          baseMessage = "Wager not accepted. At least one of your selections has been keyed for the max allowed (" + ParlayAndTeaserRestrictionKeyFactor + " times)";
        }
        else {
          baseMessage = "The current wager of " + FormatNumber(volAmt) + " " + customerCurrency + " exceeds the system maximum wager of " + FormatNumber((currentWagerLimitAmt / 100)) + " " + customerCurrency;
          if (totalAlreadyWageredAmt > 0) {
            baseMessage += " (" + FormatNumber(totalAlreadyWageredAmt) + " already bet)";
          }
        }

        if (_limitTeasersOnlyKeyRule.IsActive() && !allowTeaserWager) {
          baseMessage = "Wager not accepted. Either the wager already exists in our database or at least two of your selections have been keyed already";
        }

        if (LoginsAndProfiles.ValidateUserFunctionalityAccess(Mdi.CurrentUserPermissions, attemptedAction)) {
          var conditionalMessage = baseMessage;
          conditionalMessage += ". Accept this wager anyway?";
          if (MessageBox.Show(conditionalMessage, @"", MessageBoxButtons.YesNo) == DialogResult.Yes) {
            wagerLimitOk = true;
          }
          else {
            if (callerToWinTxt != null && caller.AccessibleName.Substring(0, 1) == "T")
              callerToWinTxt.Focus();
            else if (callerRiskTxt != null && caller.AccessibleName.Substring(0, 1) == "P")
              callerRiskTxt.Focus();
            contValidating = false;
          }
        }
        else {
          var permissionRequestMessage = baseMessage;
          const string riskOrWinMode = "ToWin";
          var frm = new FrmLateWagerRequest(this, Customer, SelectedWagerType, permissionRequestMessage + ". " + wagerDescription, amtWagered, toWinAmount) {
            Owner = this,
            Text = @"Request to Exceed Maximum Wager",
            Icon = Icon
          };
          NotifyToExceedMaxWager(frm, baseMessage, callerToWinTxt, callerRiskTxt, riskOrWinMode, out contValidating, out wagerLimitOk);
        }
      }
      else {
        wagerLimitOk = true;
      }
    }

    private double? GetStageWageredAmount(Form caller, Ticket ticket, int ticketNumber, int? wagerNumber, List<Ticket.WagerItem> multiSelectedItems, bool isForKeyRule) {
      double? stageWageredAmt = 0;
      using (var gl = new GamesAndLines(AppModuleInfo)) {
        switch (caller.AccessibleName.Substring(0, 1)) {
          case WagerType.PARLAY:
            using (var parlay = new LineOffering.Parlay(AppModuleInfo)) {
              var defaultPrice = ((FrmPlaceParlay)caller).Parlay.DefaultPrice;
              var customerMaxPayout = ((FrmPlaceParlay)caller).Parlay.CustomerMaxPayout;
              stageWageredAmt = gl.GetReadbackMultiSelectionWagerAmount(parlay, defaultPrice, customerMaxPayout, ticket, ticketNumber, wagerNumber, multiSelectedItems, (_customer.ExchangeRate ?? 1), isForKeyRule);
            }
            break;
          case WagerType.TEASER:
            using (var teaser = new LineOffering.Teaser(AppModuleInfo)) {
              stageWageredAmt = gl.GetReadbackMultiSelectionWagerAmount(teaser, ticket, ticketNumber, wagerNumber, multiSelectedItems, isForKeyRule);
            }
            break;
        }
      }
      return stageWageredAmt;
    }

    //methods for validating Parlays end

    public WagerLimitValidator.LimitResult GetMaxWagerLimits(String wagerType, int ticketNumber, int? wagerNumber = null) {
      return GetLimitForCurrentParlayOrTeaser(wagerType, ticketNumber, wagerNumber);
    }

    public void FindGameOrContestByRotation(string rotationNumber) {
      var wagerTypeToLoad = "";

      if (rotationNumber.EndsWith("S") || rotationNumber.EndsWith("M") || rotationNumber.EndsWith("E") || rotationNumber.EndsWith("L")) {
        wagerTypeToLoad = rotationNumber.Substring(rotationNumber.Length - 1, 1);
        rotationNumber = rotationNumber.Replace("S", "").Replace("M", "").Replace("L", "").Replace("E", "");
      }
      int intRotNumber;
      int.TryParse(rotationNumber, out intRotNumber);

      DataGridView shownGridView;
      DataGridViewRow row = null;
      var columnToSearch = 0;
      var columnToFocus = 0;
      var isGamesGrid = false;
      spGLGetActiveGamesByCustomer_Result game = null;
      spCnGetActiveContests_Result contest = null;


      if (ActiveGames != null && ActiveGames.Count > 0) {
        game = (from a in ActiveGames
                where (a.Team1RotNum != null && a.Team1RotNum == intRotNumber) ||
                    (a.Team2RotNum != null && a.Team2RotNum == intRotNumber) && !a.IsGameTitle && a.IsOnline
                select a).FirstOrDefault();

        if (game != null) {
          GamesGridViewInUse = dgvwGame.Name;
          isGamesGrid = true;
        }
      }

      if (game == null && ActiveContests != null && ActiveContests.Count > 0) {
        contest = (from c in ActiveContests
                   where c.RotNum != null && c.RotNum == intRotNumber
                   select c).FirstOrDefault();
        if (contest != null) {
          PropsGridViewInUse = dgvwPropsAndContests.Name;
        }
      }

      if (game == null && contest == null) {
        return;
      }

      if (isGamesGrid) {
        shownGridView = (DataGridView)Controls.Find(GamesGridViewInUse, true).FirstOrDefault();
        if (shownGridView != null) {
          shownGridView.Visible = true;
          dgvwPropsAndContests.Rows.Clear();
          dgvwPropsAndContests.Visible = false;
          columnToSearch = 0;
          columnToFocus = 3;
          switch (wagerTypeToLoad) {
            case WagerType.SPREAD:
              break;
            case WagerType.MONEYLINE:
              columnToFocus += 1;
              break;
            case WagerType.TOTALPOINTS:
              columnToFocus += 2;
              break;
            case WagerType.TEAMTOTALPOINTS:
              columnToFocus += 3;
              break;
          }
          var scheduleDates = GetThisWeeksScheduleDates();
          lblCurrentlyShowing.Text = "";
          shownGridView.Rows.Clear();
          AddNewGameToGridView(game, shownGridView, scheduleDates.ScheduleEndDate.GetValueOrDefault());
          SetSelectedGameTreeViewNode(game);
        }
      }
      else {
        shownGridView = (DataGridView)Controls.Find(PropsGridViewInUse, true).FirstOrDefault();
        if (shownGridView != null) {
          shownGridView.Visible = true;
          dgvwGame.Rows.Clear();
          dgvwGame.Visible = false;
          columnToSearch = 1;
          columnToFocus = 3;
          FillAvailableSportsTree(true);
          if (!Mdi.displayContestsToolStripMenuItem.Checked)
            Mdi.displayContestsToolStripMenuItem.Checked = true;
          lblCurrentlyShowing.Text = "";
          shownGridView.Rows.Clear();
          AddNewContestToGridView(true, contest);
          SetSelectedContestTreeViewNode(contest);
        }
      }
      if (shownGridView != null && shownGridView.Rows.Count > 0) {
        shownGridView.Focus();
        row = shownGridView.Rows
        .Cast<DataGridViewRow>().FirstOrDefault(r => r.Cells[columnToSearch].Value.ToString().Equals(rotationNumber));
      }

      if (row != null) {
        row.Selected = true;
        if (row.Cells[columnToFocus].Visible)
          row.Cells[columnToFocus].Selected = true;

        if (shownGridView.Rows.Count > 0) {
          if (row.Visible)
            shownGridView.FirstDisplayedScrollingRowIndex = row.Index;
        }

        if (isGamesGrid) {
          if (wagerTypeToLoad != "")
            LoadWageringForm(shownGridView, row.Index, columnToFocus);
        }
        else {
          if (wagerTypeToLoad != "")
            LoadContestWageringForm(shownGridView, row.Index, columnToFocus);
        }
      }
      else {
        Debug.WriteLine("Row not found");
      }
    }

    #endregion

    #region Private Methods

    private void InitializeExtensions() {
      RestrictMoneyLines = new RestrictMoneyLines(AppModuleInfo, this);
      _formatGameTitles = new FormatGameTitles(AppModuleInfo);
      _limitParlaysAndTeasersKeyRule = new LimitParlaysAndTeasersKeyRule(AppModuleInfo, this);
      new ByPassParlayAndTeaserQuickWageringLimit(AppModuleInfo);
      _limitTeasersOnlyKeyRule = new LimitTeasersOnlyKeyRule(AppModuleInfo);
      ShowWagerLimitsInFrm = new ShowWagerLimitsInFrm(AppModuleInfo, Params.IncludeCents);
      new UseDetailedParlayTeaserWagerLimits(AppModuleInfo, this);
    }

    private void AddToIfBet(String frmMode, TextBox txtRisk, TextBox txtToWin, String selectedWagerTypeName, String selectedWagerType, String wagerTypeMode,
                            String selectedPriceName, int ticketNumber, int? wagerNumberToUpd, int? wagerItemNumberToUpd, /*int selectedGvRow,*/ int selectedRowNumberInGroup,
                            String selectedLine, String selectedPrice, String selectedOdds, String selectedBPointsOption, String chosenTeamId, double? teaserPoints, String pitcher1IsReq, String pitcher2IsReq, String fixedPrice,
                            String totalsOorU, String rifBackwardTicketNumber, String wagerItemDescription, List<string> lineAndPoints,
                                spGLGetActiveGamesByCustomer_Result selectedGamePeriodInfo, Boolean isFreePlay, String wageringMode, String layoffFlag, String easternLineEnabled, List<spGLGetEasternLineConversionInfo_Result> easternLineConversionInfo, string gameDataSource, String complexIfBetAmount = null) {
      string itemShortDescription;
      var frmPlaceIfBet = (FrmPlaceIfBet)FormF.GetFormByName("frmPlaceIfBet", this);
      frmPlaceIfBet.ItemFromMakeAWagerForm = true;
      var ifBet = frmPlaceIfBet.IfBet;

      if (ifBet == null) {
        ifBet = CreateIfBetWager(frmPlaceIfBet, frmMode, ticketNumber, wagerNumberToUpd ?? 0, txtRisk, txtToWin, selectedWagerTypeName, selectedWagerType, isFreePlay, layoffFlag, complexIfBetAmount);

        if (ifBet == null) {
          return;
        }
      }

      var item = new IfBetItem();
      double amountWagered;
      double.TryParse(txtRisk.Text, out amountWagered);
      double toWinAmount;
      double.TryParse(txtToWin.Text, out toWinAmount);

      if (frmMode == "New") {
        var iBetWagerItem = CreateStageWagerItem(frmMode, selectedWagerTypeName, selectedWagerType, wagerTypeMode,
                                                        selectedPriceName, ifBet.TicketNumber, ifBet.Number,
                                                        wagerItemNumberToUpd ?? 0, /*selectedGvRow,*/ selectedRowNumberInGroup, selectedLine,
                                                        selectedPrice, selectedOdds, selectedBPointsOption,
                                                        chosenTeamId, teaserPoints, pitcher1IsReq,
                                                        pitcher2IsReq, fixedPrice, totalsOorU,
                                                        amountWagered, rifBackwardTicketNumber, wagerItemDescription,
                                                        lineAndPoints,
                                                        selectedGamePeriodInfo,
                                                        out itemShortDescription, isFreePlay, wageringMode, layoffFlag, easternLineEnabled, easternLineConversionInfo, complexIfBetAmount);

        if (ifBet.Number != null && ifBet.Number > 0 && CheckForInvalidPickCombinations(selectedGamePeriodInfo, selectedWagerType, selectedWagerTypeName, ifBet.TicketNumber, ifBet.Number, gameDataSource)) {
          var errorMessage = "Overlapping periods detected for the same (or correlated) game.";
          errorMessage += Environment.NewLine;
          errorMessage += "The item cannot be added to the ";
          errorMessage += selectedWagerTypeName;
          errorMessage += ".";
          MessageBox.Show(errorMessage, @"Invalid Selection");
          return;
        }

        AddStageWagerItem(frmMode, iBetWagerItem, selectedGamePeriodInfo);
        if (ifBet.Number != null) {
          var wagerItem = Mdi.Ticket.GetWagerItems(ifBet.TicketNumber, ifBet.Number.Value).Last();
          item.AmountWagered = wagerItem.AmountWagered;
          item.FrmAmountWagered = amountWagered;
          item.FrmToWinAmount = toWinAmount;
          TwUtilities.FillIfBetItem(item, wagerItem, wagerTypeMode, /*selectedGvRow,*/ totalsOorU, selectedOdds, selectedPrice,
              selectedBPointsOption, txtRisk, txtToWin, itemShortDescription, wageringMode, layoffFlag, fixedPrice, pitcher1IsReq, pitcher2IsReq);
          frmPlaceIfBet.AddItem(item);
        }
      }
      else {
        var updatedItem = CreateStageWagerItem(frmMode, selectedWagerTypeName, selectedWagerType, wagerTypeMode,
                                                        selectedPriceName, ifBet.TicketNumber, ifBet.Number,
                                                        wagerItemNumberToUpd ?? 0, /*selectedGvRow,*/ selectedRowNumberInGroup, selectedLine,
                                                        selectedPrice, selectedOdds, selectedBPointsOption,
                                                        chosenTeamId, teaserPoints, pitcher1IsReq,
                                                        pitcher2IsReq, fixedPrice, totalsOorU,
                                                        amountWagered, rifBackwardTicketNumber, wagerItemDescription,
                                                        lineAndPoints, selectedGamePeriodInfo,
                                                        out itemShortDescription, isFreePlay, wageringMode, layoffFlag, easternLineEnabled, easternLineConversionInfo);
        updatedItem.WagerNumber = wagerNumberToUpd ?? 0;
        updatedItem.ItemNumber = wagerItemNumberToUpd ?? 0;
        item.OverrideRiskAndToWinAmounts(amountWagered, toWinAmount);
        TwUtilities.FillIfBetItem(item, updatedItem, wagerTypeMode, /*selectedGvRow,*/ totalsOorU, selectedOdds, selectedPrice,
                    selectedBPointsOption, txtRisk, txtToWin, itemShortDescription, wageringMode, layoffFlag, fixedPrice, pitcher1IsReq, pitcher2IsReq);
        if (ifBet.Number != null && ifBet.Number > 0 && CheckForInvalidPickCombinations(selectedGamePeriodInfo, selectedWagerType, selectedWagerTypeName, ifBet.TicketNumber, ifBet.Number, gameDataSource)) {
          var errorMessage = "Overlapping periods detected for the same (or correlated) game.";
          errorMessage += Environment.NewLine;
          errorMessage += "The item cannot be added to the ";
          errorMessage += selectedWagerTypeName;
          errorMessage += ".";
          MessageBox.Show(errorMessage, @"Invalid Selection");
          return;
        }

        frmPlaceIfBet.UpdateItem(item);
        UpdateWagerItemReadbackLogInfo(updatedItem);
      }
    }

    private void AddToParlay(String frmMode, TextBox txtRisk, TextBox txtToWin, String selectedWagerTypeName, String selectedWagerType, String wagerTypeMode,
                                String selectedPriceName, int ticketNumberToUpd, int? wagerNumberToUpd, int? wagerItemNumberToUpd, /*int selectedGvRow,*/ int selectedRowNumberInGroup,
                                String selectedLine, String selectedPrice, String selectedOdds, String selectedBPointsOption, String chosenTeamId, double? teaserPoints, String pitcher1IsReq, String pitcher2IsReq, String fixedPrice,
                                String totalsOorU, String rifBackwardTicketNumber, String wagerItemDescription, List<string> lineAndPoints,
                                    spGLGetActiveGamesByCustomer_Result selectedGamePeriodInfo, Boolean isFreePlay, String wageringMode, String layoffFlag, String easternLineEnabled, List<spGLGetEasternLineConversionInfo_Result> easternLineConversionInfo, string gameDataSource) {
      string itemShortDescription;
      var frmPlaceParlay = (FrmPlaceParlay)FormF.GetFormByName("frmPlaceParlay", this);
      var parlay = frmPlaceParlay.Parlay;
      frmPlaceParlay.ItemFromMakeAWagerForm = true;

      if (ParlayOrTeaserItemFromSb) {
        frmMode = "New";
        wagerNumberToUpd = 0;
      }

      if (parlay == null) {
        parlay = CreateParlayWager(frmPlaceParlay, frmMode, ticketNumberToUpd, (wagerNumberToUpd ?? 0), txtRisk, txtToWin, selectedWagerTypeName, selectedWagerType, isFreePlay, layoffFlag);

        if (parlay == null) {
          return;
        }
      }

      if (frmMode.Trim().ToUpper() != "EDIT" && frmPlaceParlay.Parlay.IsOpen && frmPlaceParlay.Parlay.Items.Count == frmPlaceParlay.Parlay.TotalPicks) {
        MessageBox.Show(@"The number of teams cannot be increased on an open parlay.", ParameterList.GetItem("AppName").Value, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        return;
      }

      var item = new ParlayItem();

      if (frmMode == "New") {
        var wagerItem = CreateStageWagerItem(frmMode, selectedWagerTypeName, selectedWagerType, wagerTypeMode,
                                                          selectedPriceName, parlay.TicketNumber, parlay.Number,
                                                          wagerItemNumberToUpd ?? 0, /*selectedGvRow,*/ selectedRowNumberInGroup, selectedLine,
                                                          selectedPrice, selectedOdds, selectedBPointsOption,
                                                          chosenTeamId, teaserPoints, pitcher1IsReq,
                                                          pitcher2IsReq, fixedPrice, totalsOorU,
                                                          parlay.AmountWagered, rifBackwardTicketNumber, wagerItemDescription,
                                                          lineAndPoints,
                                                          selectedGamePeriodInfo,
                                                          out itemShortDescription, isFreePlay, wageringMode, layoffFlag, easternLineEnabled, easternLineConversionInfo);

        if (parlay.Number != null && parlay.Number > 0 && CheckForInvalidPickCombinations(selectedGamePeriodInfo, selectedWagerType, selectedWagerTypeName, parlay.TicketNumber, parlay.Number, gameDataSource, parlay.FromPreviousTicket, parlay.FromPreviousWagerNumber)) {
          var errorMessage = "Overlapping periods detected for the same (or correlated) game.";
          errorMessage += Environment.NewLine;
          errorMessage += "The item cannot be added to the ";
          errorMessage += selectedWagerTypeName;
          errorMessage += ".";
          MessageBox.Show(errorMessage, @"Invalid Selection");
          return;
        }
        wagerItem.ItemNumber = parlay.Items.Count + 1;
        wagerItem.AlreadyBetItemCount = GetMultiSelectionItemsCount(WagerType.PARLAY, false, wagerItem);
        AddStageWagerItem(frmMode, wagerItem, selectedGamePeriodInfo);
        FillParlayItem(item, wagerItem, selectedPriceName, /*selectedGvRow,*/ totalsOorU, selectedOdds, selectedPrice, selectedBPointsOption, txtRisk, txtToWin,
            itemShortDescription, wagerTypeMode, wageringMode, layoffFlag, fixedPrice, pitcher1IsReq, pitcher2IsReq);
        ParlayOrTeaserItemFromSb = false;
        frmPlaceParlay.AddItem(item);
      }
      else {
        var updatedItem = CreateStageWagerItem(frmMode, selectedWagerTypeName, selectedWagerType, wagerTypeMode,
                                                        selectedPriceName, parlay.TicketNumber, parlay.Number,
                                                        wagerItemNumberToUpd ?? 0, /*selectedGvRow,*/ selectedRowNumberInGroup, selectedLine,
                                                        selectedPrice, selectedOdds, selectedBPointsOption,
                                                        chosenTeamId, teaserPoints, pitcher1IsReq,
                                                        pitcher2IsReq, fixedPrice, totalsOorU,
                                                        parlay.AmountWagered, rifBackwardTicketNumber, wagerItemDescription,
                                                        lineAndPoints,
                                                        selectedGamePeriodInfo,
                                                        out itemShortDescription, isFreePlay, wageringMode, layoffFlag, easternLineEnabled, easternLineConversionInfo);
        updatedItem.WagerNumber = wagerNumberToUpd ?? 0;
        updatedItem.ItemNumber = wagerItemNumberToUpd ?? 0;
        updatedItem.AlreadyBetItemCount = (from wi in Mdi.Ticket.WagerItems
                                           where wi.TicketNumber == updatedItem.TicketNumber && wi.WagerNumber == updatedItem.WagerNumber && wi.ItemNumber == updatedItem.ItemNumber
                                           select wi.AlreadyBetItemCount).FirstOrDefault();//GetMultiSelectionItemsCount(WagerType.PARLAY, false, updatedItem);
        FillParlayItem(item, updatedItem, selectedPriceName, /*selectedGvRow,*/ totalsOorU, selectedOdds, selectedPrice, selectedBPointsOption, txtRisk, txtToWin,
          itemShortDescription, wagerTypeMode, wageringMode, layoffFlag, fixedPrice, pitcher1IsReq, pitcher2IsReq);
        if (parlay.Number != null && parlay.Number > 0 && CheckForInvalidPickCombinations(selectedGamePeriodInfo, selectedWagerType, selectedWagerTypeName, parlay.TicketNumber, parlay.Number, gameDataSource, null, null, wagerItemNumberToUpd)) {
          var errorMessage = "Overlapping periods detected for the same (or correlated) game.";
          errorMessage += Environment.NewLine;
          errorMessage += "The item cannot be added to the ";
          errorMessage += selectedWagerTypeName;
          errorMessage += ".";
          MessageBox.Show(errorMessage, @"Invalid Selection");
          return;
        }
        frmPlaceParlay.UpdateItem(item);
        UpdateWagerItemReadbackLogInfo(updatedItem);
      }
    }

    private void AddToTeaser(String frmMode, TextBox txtRisk, TextBox txtToWin, String selectedWagerTypeName, String selectedWagerType, String wagerTypeMode,
                              String selectedPriceName, int ticketNumberToUpd, int? wagerNumberToUpd, int? wagerItemNumberToUpd, /*int selectedGvRow,*/ int selectedRowNumberInGroup,
                              String selectedLine, String selectedPrice, String selectedOdds, String selectedBPointsOption, String chosenTeamId, double? teaserPoints, String pitcher1IsReq, String pitcher2IsReq, String fixedPrice,
                              String totalsOorU, String rifBackwardTicketNumber, String wagerItemDescription, List<string> lineAndPoints,
                                  spGLGetActiveGamesByCustomer_Result selectedGamePeriodInfo, Boolean isFreePlay, String wageringMode, String layoffFlag, String easternLineEnabled, List<spGLGetEasternLineConversionInfo_Result> easternLineConversionInfo, string gameDataSource) {
      string itemShortDescription;
      var frmPlaceTeaser = (FrmPlaceTeaser)FormF.GetFormByName("FrmPlaceTeaser", this);
      if (frmPlaceTeaser != null)
        frmPlaceTeaser.ItemFromMakeAWagerForm = true;

      if (frmPlaceTeaser == null) {
        return;
      }

      if (ParlayOrTeaserItemFromSb) {
        frmMode = "New";
        wagerNumberToUpd = 0;
      }

      var teaser = frmPlaceTeaser.Teaser ?? CreateTeaserWager(frmPlaceTeaser, frmMode, ticketNumberToUpd, wagerNumberToUpd ?? 0, txtRisk, txtToWin, selectedWagerTypeName, selectedWagerType, isFreePlay, layoffFlag);

      if (teaser == null) {
        return;
      }

      if (frmMode.Trim().ToUpper() != "EDIT" && teaser.IsOpen && teaser.Items.Count == teaser.TotalPicks) {
        MessageBox.Show(@"The number of teams cannot be increased on an open teaser.", ParameterList.GetItem("AppName").Value, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        return;
      }

      var item = new TeaserItem();

      if (frmMode == "New") {
        var wagerItem = CreateStageWagerItem(frmMode, selectedWagerTypeName, selectedWagerType, wagerTypeMode,
                                                        selectedPriceName, teaser.TicketNumber, teaser.Number,
                                                        wagerItemNumberToUpd ?? 0, /*selectedGvRow,*/ selectedRowNumberInGroup, selectedLine,
                                                        selectedPrice, selectedOdds, selectedBPointsOption,
                                                        chosenTeamId, teaserPoints, pitcher1IsReq,
                                                        pitcher2IsReq, fixedPrice, totalsOorU,
                                                        teaser.AmountWagered, rifBackwardTicketNumber, wagerItemDescription,
                                                        lineAndPoints,
                                                        selectedGamePeriodInfo,
                                                        out itemShortDescription, isFreePlay, wageringMode, layoffFlag, easternLineEnabled, easternLineConversionInfo);

        if (teaser.Number != null && teaser.Number > 0 && CheckForInvalidPickCombinations(selectedGamePeriodInfo, selectedWagerType, selectedWagerTypeName, teaser.TicketNumber, teaser.Number, gameDataSource)) {
          var errorMessage = "Overlapping periods detected for the same (or correlated) game.";
          errorMessage += Environment.NewLine;
          errorMessage += "The item cannot be added to the ";
          errorMessage += selectedWagerTypeName;
          errorMessage += ".";
          MessageBox.Show(errorMessage, @"Invalid Selection");
          return;
        }

        if (teaser.Number != null && teaser.Number > 0 && TeaserItemAlreadyExists(wagerItem)) {
          const string errorMessage = "The game selection cannot be added due to a correlation to an existing selection.";
          MessageBox.Show(errorMessage, @"Invalid Selection");
          return;
        }
        wagerItem.AlreadyBetItemCount = GetMultiSelectionItemsCount(WagerType.TEASER, false, wagerItem);
        AddStageWagerItem(frmMode, wagerItem, selectedGamePeriodInfo);
        item.DescriptionAndAdjustedPrice = itemShortDescription;
        if (wagerItem.GameNum != null) item.GameNumber = (int)wagerItem.GameNum;
        item.TicketNumber = wagerItem.TicketNumber;
        item.WagerNumber = wagerItem.WagerNumber;
        item.Number = wagerItem.ItemNumber;
        item.Description = wagerItem.Description; //buildWagerDescription(getChosenTeamRotNumber(), _lineAndPoints[0], _lineAndPoints[1], wagerItem.wagerType, wagerItem.totalPointsOU);
        item.Type = WagerType.GetFromCode(wagerItem.WagerType);
        //item.SelectedRow = selectedGvRow; //DetermineSelectedRow(_selectedGvRowNumber, _originalRowNumberInGroup, _selectedRowNumberInGroup, item.Type.Code, wagerItem.TotalPointsOu);
        item.SelectedCell = TwUtilities.DetermineSelectedCell(item.Type, totalsOorU);
        item.SelectedOdds = selectedOdds; //DetermineSelectedOdds();
        item.SelectedAmericanPrice = int.Parse(selectedPrice);
        item.SelectedBPointsOption = selectedBPointsOption; //DetermineSelectedBPointsOption(item.Type);
        item.SelectedAmountWagered = txtRisk.Text;
        item.SelectedToWinAmount = txtToWin.Text;
        item.WagerTypeMode = wagerTypeMode;
        item.PriceType = selectedPriceName;
        item.WageringMode = wageringMode;
        item.GameNumber = wagerItem.GameNum ?? 0;
        item.PeriodNumber = wagerItem.PeriodNumber ?? 0;
        item.RowGroupNum = wagerItem.RowGroupNum;
        item.PeriodWagerCutoff = wagerItem.PeriodWagerCutoff;
        item.LayoffWager = layoffFlag;
        item.FixedPrice = fixedPrice;
        item.Pitcher1MStart = pitcher1IsReq;
        item.Pitcher2MStart = pitcher2IsReq;
        item.TotalPointsOu = totalsOorU;
        item.PeriodDescription = wagerItem.PeriodDescription;
        item.AlreadyBetItemCount = wagerItem.AlreadyBetItemCount;
        ParlayOrTeaserItemFromSb = false;
        frmPlaceTeaser.AddItem(item);
      }
      else {
        var updatedItem = CreateStageWagerItem(frmMode, selectedWagerTypeName, selectedWagerType, wagerTypeMode,
                                                        selectedPriceName, teaser.TicketNumber, teaser.Number,
                                                        wagerItemNumberToUpd ?? 0, /*selectedGvRow,*/ selectedRowNumberInGroup, selectedLine,
                                                        selectedPrice, selectedOdds, selectedBPointsOption,
                                                        chosenTeamId, teaserPoints, pitcher1IsReq,
                                                        pitcher2IsReq, fixedPrice, totalsOorU,
                                                        teaser.AmountWagered, rifBackwardTicketNumber, wagerItemDescription,
                                                        lineAndPoints,
                                                        selectedGamePeriodInfo,
                                                        out itemShortDescription, isFreePlay, wageringMode, layoffFlag, easternLineEnabled, easternLineConversionInfo);
        updatedItem.WagerNumber = wagerNumberToUpd ?? 0;
        updatedItem.ItemNumber = wagerItemNumberToUpd ?? 0;
        updatedItem.AlreadyBetItemCount = (from wi in Mdi.Ticket.WagerItems
                                           where wi.TicketNumber == updatedItem.TicketNumber && wi.WagerNumber == updatedItem.WagerNumber && wi.ItemNumber == updatedItem.ItemNumber
                                           select wi.AlreadyBetItemCount).FirstOrDefault();
        item.DescriptionAndAdjustedPrice = itemShortDescription;
        item.TicketNumber = ticketNumberToUpd;
        item.WagerNumber = wagerNumberToUpd ?? 0;
        item.Number = wagerItemNumberToUpd ?? 0;
        item.Type = WagerType.GetFromCode(updatedItem.WagerType);
        item.SelectedCell = TwUtilities.DetermineSelectedCell(item.Type, totalsOorU);
        item.SelectedOdds = selectedOdds; //DetermineSelectedOdds();
        item.SelectedAmericanPrice = int.Parse(selectedPrice);
        item.SelectedBPointsOption = selectedBPointsOption; //DetermineSelectedBPointsOption(item.Type);
        item.SelectedAmountWagered = txtRisk.Text;
        item.SelectedToWinAmount = txtToWin.Text;
        item.Description = itemShortDescription;
        item.WagerTypeMode = wagerTypeMode;
        item.PriceType = selectedPriceName;
        item.WageringMode = wageringMode;
        item.GameNumber = updatedItem.GameNum ?? 0;
        item.PeriodNumber = updatedItem.PeriodNumber ?? 0;
        item.RowGroupNum = updatedItem.RowGroupNum;
        item.PeriodWagerCutoff = updatedItem.PeriodWagerCutoff;
        item.LayoffWager = layoffFlag;
        item.FixedPrice = fixedPrice;
        item.Pitcher1MStart = pitcher1IsReq;
        item.Pitcher2MStart = pitcher2IsReq;
        item.PeriodDescription = updatedItem.PeriodDescription;
        item.AlreadyBetItemCount = updatedItem.AlreadyBetItemCount;

        if (teaser.Number != null && teaser.Number > 0 && CheckForInvalidPickCombinations(selectedGamePeriodInfo, selectedWagerType, selectedWagerTypeName, teaser.TicketNumber, teaser.Number, gameDataSource)) {
          var errorMessage = "Overlapping periods detected for the same (or correlated) game.";
          errorMessage += Environment.NewLine;
          errorMessage += "The item cannot be added to the ";
          errorMessage += selectedWagerTypeName;
          errorMessage += ".";
          MessageBox.Show(errorMessage, @"Invalid Selection");
          return;
        }
        frmPlaceTeaser.UpdateItem(item);
        UpdateWagerItemReadbackLogInfo(updatedItem);
      }
    }

    private void FillParlayItem(ParlayItem item, Ticket.WagerItem wagerItem, string selectedPriceName, /*int selectedGvRow,*/ string totalsOorU, string selectedOdds, string selectedPrice, string selectedBPointsOption, TextBox txtRisk, TextBox txtToWin, string itemShortDescription, string wagerTypeMode, string wageringMode, string layoffFlag, string fixedPrice, string pitcher1IsReq, string pitcher2IsReq) {
      item.AmountWagered = wagerItem.AmountWagered;
      item.FinalMoney = wagerItem.FinalMoney;
      item.Number = wagerItem.ItemNumber;
      item.PriceType = selectedPriceName;
      item.SportSubType = wagerItem.SportSubType;
      item.SportType = wagerItem.SportType;
      item.Type = WagerType.GetFromCode(wagerItem.WagerType);
      item.TicketNumber = wagerItem.TicketNumber;
      item.WagerNumber = wagerItem.WagerNumber;
      /*item.SelectedRow = selectedGvRow;*/
      item.SelectedCell = TwUtilities.DetermineSelectedCell(item.Type, totalsOorU);
      item.SelectedOdds = selectedOdds;
      item.SelectedAmericanPrice = int.Parse(selectedPrice);
      item.SelectedBPointsOption = selectedBPointsOption;
      item.SelectedAmountWagered = txtRisk.Text;
      item.SelectedToWinAmount = txtToWin.Text;
      item.Description = itemShortDescription;
      item.WagerTypeMode = wagerTypeMode;
      item.WageringMode = wageringMode;
      item.GameNumber = wagerItem.GameNum ?? 0;
      item.PeriodNumber = wagerItem.PeriodNumber ?? 0;
      item.RowGroupNum = wagerItem.RowGroupNum;
      item.PeriodWagerCutoff = wagerItem.PeriodWagerCutoff;
      item.LayoffWager = layoffFlag;
      item.FixedPrice = fixedPrice;
      item.Pitcher1MStart = pitcher1IsReq;
      item.Pitcher2MStart = pitcher2IsReq;
      item.TotalPointsOu = totalsOorU;
      item.PeriodDescription = wagerItem.PeriodDescription;
      item.AlreadyBetItemCount = wagerItem.AlreadyBetItemCount;
    }

    public void FillParlayContestItem(ParlayItem item, Ticket.ContestWagerItem contestWagerItem, string selectedPriceName,
        TextBox txtRisk, TextBox txtToWin, string itemShortDescription,
        string wagerTypeMode, string wageringMode, string layoffFlag) {
      item.AmountWagered = contestWagerItem.AmountWagered;
      item.FinalMoney = int.Parse((contestWagerItem.FinalMoney ?? 0).ToString(CultureInfo.InvariantCulture));
      item.Number = contestWagerItem.ItemNumber;
      item.PriceType = selectedPriceName;
      item.SportSubType = "";
      item.SportType = "";
      item.Type = WagerType.GetFromCode("M");
      item.TicketNumber = contestWagerItem.TicketNumber;
      item.WagerNumber = contestWagerItem.WagerNumber;
      /*item.SelectedRow = 1;*/
      item.SelectedCell = 0;
      item.SelectedOdds = (contestWagerItem.FinalMoney ?? 0).ToString(CultureInfo.InvariantCulture);
      item.SelectedAmericanPrice = int.Parse((contestWagerItem.FinalMoney ?? 0).ToString(CultureInfo.InvariantCulture));
      item.SelectedBPointsOption = "";
      item.SelectedAmountWagered = txtRisk.Text;
      item.SelectedToWinAmount = txtToWin.Text;
      item.Description = itemShortDescription;
      item.WagerTypeMode = wagerTypeMode;
      item.WageringMode = wageringMode;
      item.GameNumber = contestWagerItem.ContestNum ?? 0;
      item.PeriodNumber = 0;
      item.RowGroupNum = 0;
      if (contestWagerItem.WagerCutoff != null) item.PeriodWagerCutoff = (DateTime)contestWagerItem.WagerCutoff;
      item.LayoffWager = contestWagerItem.LayoffFlag;
      item.FixedPrice = "";
      item.Pitcher1MStart = "";
      item.Pitcher2MStart = "";
      item.TotalPointsOu = "";
      item.PeriodDescription = "";
      item.AlreadyBetItemCount = null;
      item.IsContest = true;
      item.ContestantNumber = contestWagerItem.ContestantNum ?? 0;
      item.PendingContestItemInDb = contestWagerItem.PendingContestItemInDb;
    }

    public int? GetMultiSelectionItemsCount(string wagerType, bool checkForRRs, Ticket.WagerItem wagerItem) {
      var itemsInDbCount = 0;
      var itemsInReadbackCount = 0;
      try {
        using (var gl = new GamesAndLines(Mdi.AppModuleInfo)) {
          itemsInDbCount = gl.GetMultiSelectionItemCount(Mdi.AppModuleInfo, Customer.CustomerID.Trim(), wagerType, checkForRRs, wagerItem);
          itemsInReadbackCount = gl.ReadbackMultiSelectionItemCount(wagerType, checkForRRs, wagerItem, Mdi.Ticket);
        }
      }
      catch (Exception ex) {
        Log(ex);
      }
      int? itemsCount = itemsInDbCount + itemsInReadbackCount;
      return itemsCount == 0 ? null : itemsCount;
    }

    private bool TeaserItemAlreadyExists(Ticket.WagerItem wagerItem) {
        if (wagerItem == null) return false;
      if (Mdi.Ticket == null || Mdi.Ticket.WagerItems == null || Mdi.Ticket.WagerItems.Count <= 0) return false;
      var teaserItems = (from wis in Mdi.Ticket.WagerItems where wis.TicketNumber == wagerItem.TicketNumber && wis.WagerNumber == wagerItem.WagerNumber select wis).ToList();

      if (!(teaserItems.Count > 0)) return false;

        return teaserItems.Any(wi => wagerItem.GameNum == wi.GameNum && wagerItem.PeriodNumber == wi.PeriodNumber && 
            wagerItem.WagerType == wi.WagerType && wagerItem.ChosenTeamId.Trim() == wi.ChosenTeamId.Trim() && 
            (wi.ChosenTeamIdx <= 0 || wagerItem.ChosenTeamIdx == wi.ChosenTeamIdx));
    }

    private void AddNewContestToGridView(Boolean writeDesc, spCnGetActiveContests_Result res) {
      var contestDescr = "";
      var priceType = btnLineDisplayToggle.Text;
      var contestantNumber = res.ContestantNum.ToString(CultureInfo.InvariantCulture);

      for (var i = 0; i < dgvwPropsAndContests.Rows.Count; i++) // this situation could happen if customer uses shade group lines
            {
        var gvwcontestantNumber = dgvwPropsAndContests.Rows[i].Cells[CONTESTANTNUM_INDEX].Value.ToString();
        if (contestantNumber == gvwcontestantNumber) {
          return;
        }
      }

      if (writeDesc) {
        contestDescr = res.ContestType.Trim();

        if (res.ContestType2.Trim() != "." && res.ContestType2.Trim() != "") {
          contestDescr = res.ContestType2.Trim();
        }

        if (res.ContestType3.Trim() != "." && res.ContestType3.Trim() != "") {
          contestDescr += " - " + res.ContestType3.Trim();
        }

        if (res.ContestDesc.Trim() != "." && res.ContestDesc.Trim() != "") {
          contestDescr += " - " + res.ContestDesc.Trim();
        }
      }

      var thresholdValue = TwUtilities.GetContestThresholdValue(res);

      var odds = GetContestantOdds(priceType, res);

      dgvwPropsAndContests.Rows.Add(contestDescr, res.RotNum, res.ContestantName.Trim() + " " + thresholdValue, odds, res.ContestantNum, res.ContestDateTime, res.ContestDesc, res.Comments, res.ContestNum, res.CustProfile.Trim());
      dgvwPropsAndContests.Rows[dgvwPropsAndContests.Rows.Count - 1].Height = GRIDVIEWROWHEIGHT;
      dgvwPropsAndContests.Rows[dgvwPropsAndContests.Rows.Count - 1].DefaultCellStyle.BackColor = GetContestRowColor(res.Status, res.WagerCutoff.GetValueOrDefault());

      if (!ShowingActiVeGamesOnly) return;
      if (dgvwPropsAndContests.Rows[dgvwPropsAndContests.Rows.Count - 1].DefaultCellStyle.BackColor == DOWNGAMECOLOR) {
        dgvwPropsAndContests.Rows[dgvwPropsAndContests.Rows.Count - 1].Visible = false;
      }
    }

    private string GetContestantOdds(string priceType, spCnGetActiveContests_Result res) {
      var odds = "";
      if (res.Status == GamesAndLines.EVENT_OFFLINE) {
        odds = "Offline";
      }
      else {
        if (res.XtoYLineRep == "N") {
          if (res.MoneyLine != null) {
            if (res.MoneyLine > 0) {
              odds = "+" + FormatNumber((double)res.MoneyLine, false);
            }
            else {
                odds = res.MoneyLine == 0 ? GamesAndLines.PICK : FormatNumber((double)res.MoneyLine, false);
            }
          }
        }
        else {
          if (res.MoneyLine != null) {
            odds = FormatNumber((double)res.MoneyLine, false) + " to " + res.ToBase;
          }
        }

        switch (priceType) {
          case "Decimal":
            if (res.DecimalOdds != null)
              odds = res.DecimalOdds.ToString();
            if (!odds.Contains("."))
              odds += ".";
            break;
          case "Fractional":
            if (res.Numerator != null && res.Denominator != null)
              odds = res.Numerator + "/" + res.Denominator;
            break;
        }
      }
      return odds;
    }

    private void AddNewGameToGridView(spGLGetActiveGamesByCustomer_Result res, DataGridView dgvw, DateTime thisWeekEndScheduleDate) {
      if (dgvw == null || res == null) return;

      var gameNumber = res.GameNum.ToString(CultureInfo.InvariantCulture);
      var periodNumber = res.PeriodNumber.ToString(CultureInfo.InvariantCulture);
      for (var i = 0; i < dgvw.Rows.Count; i++) {// this situation could happen if customer uses shade group lines
        if (dgvw.Rows[i].Cells[GAME_NUM_INDEX].Value == null || dgvw.Rows[i].Cells[PERIOD_NUM_INDEX].Value == null) continue;
        var gvwGameNumber = dgvw.Rows[i].Cells[GAME_NUM_INDEX].Value.ToString();
        var gvwPeriodNum = dgvw.Rows[i].Cells[PERIOD_NUM_INDEX].Value.ToString();
        if (gameNumber == gvwGameNumber && periodNumber == gvwPeriodNum) {
          return;
        }
      }

      var priceType = btnLineDisplayToggle.Text;
      var sportType = "";

      if (res.SportType != null) {
        sportType = res.SportType.Trim();
      }

      var addAt = dgvw.Rows.Count;

      string spread = "Offline", moneyLine = "", totalPoints = "", teamTotalsOver = "",
          teamTotalsUnder = "", spread2 = "", moneyLine2 = "", totalPoints2 = "",
          teamTotalsOver2 = "", teamTotalsUnder2 = "";

      if (res.Status.Trim() != GamesAndLines.EVENT_OFFLINE) {
        var formattedWagerTypes = LineOffering.FormatWagerTypesOptionsDisplay(res, priceType, true, Customer.EasternLineFlag, sportType, _easternLineConversion);

        spread = formattedWagerTypes.Spread;
        moneyLine = RestrictMoneyLines.CheckForMlRestriction(formattedWagerTypes.MoneyLine1, res);
        totalPoints = formattedWagerTypes.TotalPoints;
        teamTotalsOver = formattedWagerTypes.TeamTotalsOver;
        teamTotalsUnder = formattedWagerTypes.TeamTotalsUnder;

        spread2 = formattedWagerTypes.Spread2;
        moneyLine2 = RestrictMoneyLines.CheckForMlRestriction(formattedWagerTypes.MoneyLine2, res);
        totalPoints2 = formattedWagerTypes.TotalPoints2;
        teamTotalsOver2 = formattedWagerTypes.TeamTotalsOver2;
        teamTotalsUnder2 = formattedWagerTypes.TeamTotalsUnder2;
      }

      var rowGroupNum = 0;
      rowGroupNum++;

      var team1Desc = "";

      if (res.Team1ID != null) {
        team1Desc = res.Team1ID.Trim();
      }
      if (sportType == SportTypes.BASEBALL && res.ListedPitcher1 != null) {
        team1Desc += " - " + res.ListedPitcher1.Trim();
      }

      if (_formatGameTitles.IsActive() && TwUtilities.IsTitle(res)) {
        CreateGridViewRow(dgvw, res.Team1RotNum.ToString(), DisplayDate.Format(res.GameDateTime ?? Mdi.GetCurrentDateTime(), DisplayDateFormat.ShortDayNameMonthDayShortTime), "", spread, moneyLine, totalPoints, teamTotalsOver, teamTotalsUnder, res.GameNum, res.PeriodNumber, 1, "", "", "", "", "", sportType, res.SportSubType.Trim(), res.PeriodWagerCutoff.ToString().Trim(), res.Status, res.Team1ID ?? "".ToString(CultureInfo.InvariantCulture), res.CustProfile.Trim(), res.ScheduleDate);
        CreateGridViewRow(dgvw, res.Team2RotNum.ToString(), DisplayDate.Format(res.GameDateTime ?? Mdi.GetCurrentDateTime(), DisplayDateFormat.ShortDayNameMonthDayShortTime), "", spread, moneyLine, totalPoints, teamTotalsOver, teamTotalsUnder, res.GameNum, res.PeriodNumber, 2, "", "", "", "", "", sportType, res.SportSubType.Trim(), res.PeriodWagerCutoff.ToString().Trim(), res.Status, res.Team2ID ?? "".ToString(CultureInfo.InvariantCulture), res.CustProfile.Trim(), res.ScheduleDate);
        _formatGameTitles.FormatTitle(res, dgvw);
        return;
      }

      CreateGridViewRow(dgvw, res.Team1RotNum.ToString(), DisplayDate.Format(res.GameDateTime ?? Mdi.GetCurrentDateTime(), DisplayDateFormat.ShortDayNameMonthDayShortTime), team1Desc, spread, moneyLine, totalPoints, teamTotalsOver, teamTotalsUnder, res.GameNum, res.PeriodNumber, rowGroupNum, "", "", "", "", "", sportType, res.SportSubType.Trim(), res.PeriodWagerCutoff.ToString().Trim(), res.Status, res.Team1ID ?? "".ToString(CultureInfo.InvariantCulture), res.CustProfile.Trim(), res.ScheduleDate);
      SetGridViewRowStyle(dgvw, res, res.Team1ID, addAt, 1, thisWeekEndScheduleDate, spread);
      addAt++;
      rowGroupNum++;
      var team2Desc = "";

      if (res.Team2ID != null) {
        team2Desc = res.Team2ID.Trim();
      }
      if (sportType == SportTypes.BASEBALL) {
        if (res.ListedPitcher2 != null)
          team2Desc += " - " + res.ListedPitcher2.Trim();
      }

      CreateGridViewRow(dgvw, res.Team2RotNum.ToString(), sportType + " - " + res.SportSubType.Trim(), team2Desc, spread2, moneyLine2, totalPoints2, teamTotalsOver2, teamTotalsUnder2, res.GameNum, res.PeriodNumber, rowGroupNum, "", "", "", "", "", (res.SportType ?? "").Trim(), res.SportSubType.Trim(), res.PeriodWagerCutoff.ToString().Trim(), res.Status, res.Team2ID ?? "".ToString(CultureInfo.InvariantCulture), res.CustProfile.Trim(), res.ScheduleDate);
      SetGridViewRowStyle(dgvw, res, res.Team2ID, addAt, 2, thisWeekEndScheduleDate, spread);
      addAt++;

      if (res.DrawRotNum == null) return;
      var moneyLineDraw = "";

      if (res.MoneyLineDraw != null && res.MoneyLineStatus != GamesAndLines.EVENT_OFFLINE) {
        moneyLineDraw = LineOffering.GetNumAdjSign(int.Parse(res.MoneyLineDraw.ToString()));
      }
      rowGroupNum++;
      CreateGridViewRow(dgvw, res.DrawRotNum.ToString(), "", "Draw", "", moneyLineDraw, "", "", "", res.GameNum, res.PeriodNumber, rowGroupNum, "", "", "", "", "", (res.SportType ?? "").Trim(), res.SportSubType.Trim(), res.PeriodWagerCutoff.ToString().Trim(), res.Status, "", res.CustProfile.Trim(), res.ScheduleDate);
      SetGridViewRowStyle(dgvw, res, res.Team2ID, addAt, 3, thisWeekEndScheduleDate, spread);

      if (!ShowingActiVeGamesOnly || dgvw.Rows[addAt].DefaultCellStyle.BackColor != DOWNGAMECOLOR) return;
      dgvw.Rows[addAt].Visible = false;
      HideGridLineSeparator(addAt, dgvw);
    }

    private void SetGridViewRowStyle(DataGridView dgvw, spGLGetActiveGamesByCustomer_Result res, string teamId, int addAt, int teamIdx, DateTime thisWeekEndScheduleDate, string spread) {
      dgvw.Rows[addAt].Height = GRIDVIEWROWHEIGHT;

      dgvw.Rows[addAt].Cells[SPREAD_INDEX].Style.Font = new Font(dgvw.Font, FontStyle.Regular);
      dgvw.Rows[addAt].Cells[MONEY_INDEX].Style.Font = new Font(dgvw.Font, FontStyle.Regular);
      if ((teamIdx == 1 || teamIdx == 2) && res.FavoredTeamID != null && teamId != null && teamId == res.FavoredTeamID.Trim() && spread != "Offline") {
        dgvw.Rows[addAt].Cells[SPREAD_INDEX].Style.Font = new Font(dgvw.Font, FontStyle.Bold);
        dgvw.Rows[addAt].Cells[MONEY_INDEX].Style.Font = new Font(dgvw.Font, FontStyle.Bold);
      }

      /*
      if (teamIdx == 1 || teamIdx == 2) {
        if (res.FavoredTeamID != null) {
          if (teamId != null && teamId == res.FavoredTeamID.Trim()) {
            if (spread != "Offline") {
              dgvw.Rows[addAt].Cells[SPREAD_INDEX].Style.Font = new Font(dgvw.Font, FontStyle.Bold);
              dgvw.Rows[addAt].Cells[MONEY_INDEX].Style.Font = new Font(dgvw.Font, FontStyle.Bold);
            }
            else {
              dgvw.Rows[addAt].Cells[SPREAD_INDEX].Style.Font = new Font(dgvw.Font, FontStyle.Regular);
              dgvw.Rows[addAt].Cells[MONEY_INDEX].Style.Font = new Font(dgvw.Font, FontStyle.Regular);
            }
          }
        }
        else {
          dgvw.Rows[addAt].Cells[SPREAD_INDEX].Style.Font = new Font(dgvw.Font, FontStyle.Regular);
          dgvw.Rows[addAt].Cells[MONEY_INDEX].Style.Font = new Font(dgvw.Font, FontStyle.Regular);

        }
      }*/

      dgvw.Rows[addAt].DefaultCellStyle.BackColor = GetGridRowColor(res.Status, res.PeriodWagerCutoff ?? DateTime.MinValue, res.TimeChangeFlag, res.ScheduleDate, res.SportType, thisWeekEndScheduleDate);

      dgvw.Rows[addAt].Cells[SPREAD_INDEX].Style.ForeColor = spread == "Offline" && teamIdx == 1 ? BLACKGAMECOLOR : GetRowCellColor(SPREAD_INDEX, res);//GetCellColor0(res.LastSpreadChange);
      //dgvw.Rows[addAt].Cells[SPREAD_INDEX].Style.ForeColor = GetRowCellColor(SPREAD_INDEX, res);//GetCellColor0(res.LastSpreadChange);
      dgvw.Rows[addAt].Cells[MONEY_INDEX].Style.ForeColor = GetRowCellColor(MONEY_INDEX, res);//GetCellColor0(res.LastMoneyLineChange);
      dgvw.Rows[addAt].Cells[TOTAL_INDEX].Style.ForeColor = GetRowCellColor(TOTAL_INDEX, res);//GetCellColor0(res.LastTtlPtsChange);
      dgvw.Rows[addAt].Cells[TEAM_TOTAL_OVER_INDEX].Style.ForeColor = GetRowCellColor(TEAM_TOTAL_OVER_INDEX, res);//GetCellColor0(res.LastTeamPtsChange);
      dgvw.Rows[addAt].Cells[TEAM_TOTAL_UNDER_INDEX].Style.ForeColor = GetRowCellColor(TEAM_TOTAL_UNDER_INDEX, res);//GetCellColor0(res.LastTeamPtsChange);

      if (res.Status.Trim() == GamesAndLines.EVENT_CIRCLED) {
        dgvw.Rows[addAt].Cells[ROTATION_INDEX].Style.ForeColor = LOW_CIRCLED_GAME_COLOR; // Color.Red;
        dgvw.Rows[addAt].Cells[GAMEDATE_INDEX].Style.ForeColor = LOW_CIRCLED_GAME_COLOR; // Color.Red;
        dgvw.Rows[addAt].Cells[TEAMS_NAMES_INDEX].Style.ForeColor = LOW_CIRCLED_GAME_COLOR; // Color.Red;
      }
      else {
        dgvw.Rows[addAt].Cells[ROTATION_INDEX].Style.ForeColor = BLACKGAMECOLOR; // Color.Black;
        dgvw.Rows[addAt].Cells[GAMEDATE_INDEX].Style.ForeColor = BLACKGAMECOLOR; // Color.Black;
        dgvw.Rows[addAt].Cells[TEAMS_NAMES_INDEX].Style.ForeColor = BLACKGAMECOLOR; // Color.Black;
      }

      if (ShowingActiVeGamesOnly && (teamIdx == 1 || teamIdx == 2)) {
        if (dgvw.Rows[addAt].DefaultCellStyle.BackColor == DOWNGAMECOLOR) {
          dgvw.Rows[addAt].Visible = false;
          HideGridLineSeparator(addAt, dgvw);
        }
      }
    }

    private void CreateGridViewRow(DataGridView grid, string teamRotNum, string date, string teamDesc, string spread, string moneyLine, string totalPoints, string teamTotalsOver, string teamTotalsUnder, int gameNum, int periodNumber, int rowGroupNum, string tbd11, string tbd12, string tbd13, string tbd14, string tbd15, string sportType, string sportSubType, string periodWagerCutoff, string status, string team1Id, string custProfile, DateTime? scheduleDate) {
      grid.CellEnter -= dgvwGame_CellEnter;
      grid.Rows.Add();
      var row = grid.Rows[grid.Rows.Count - 1];
      //row.DefaultCellStyle.Font = new Font(FontFamily.GenericSerif, 12);
      row.Cells[ROTATION_INDEX].Value = teamRotNum;
      row.Cells[GAMEDATE_INDEX].Value = date;
      row.Cells[TEAMS_NAMES_INDEX].Value = teamDesc;
      row.Cells[SPREAD_INDEX].Value = spread;
      row.Cells[MONEY_INDEX].Value = moneyLine;
      row.Cells[TOTAL_INDEX].Value = totalPoints;
      row.Cells[TEAM_TOTAL_OVER_INDEX].Value = teamTotalsOver;
      row.Cells[TEAM_TOTAL_UNDER_INDEX].Value = teamTotalsUnder;
      row.Cells[GAME_NUM_INDEX].Value = gameNum;
      row.Cells[PERIOD_NUM_INDEX].Value = periodNumber;
      row.Cells[ROWGROUPNUM_INDEX].Value = rowGroupNum;
      row.Cells[TBD11_INDEX].Value = tbd11;
      row.Cells[TBD12_INDEX].Value = tbd12;
      row.Cells[TBD13_INDEX].Value = tbd13;
      row.Cells[TBD14_INDEX].Value = tbd14;
      row.Cells[TBD15_INDEX].Value = tbd15;
      row.Cells[SPORT_INDEX].Value = sportType;
      row.Cells[SUBSPORT_INDEX].Value = sportSubType;
      row.Cells[PERIOD_CUTOFF_INDEX].Value = periodWagerCutoff;
      row.Cells[GAME_STATUS_INDEX].Value = status;
      row.Cells[TEAM1_NAME_INDEX].Value = team1Id;
      row.Cells[CUSTPROFILE_INDEX].Value = custProfile;
      row.Cells[SCHEDULEDATE_INDEX].Value = scheduleDate;

      grid.CellEnter += dgvwGame_CellEnter;
    }

    private List<DataGridViewRow> BuildGridViewRowsForGame(DataGridViewRow gridRowInUse, spGLGetActiveGamesByCustomer_Result res, DateTime thisWeekEndScheduleDate) {
      var row = (DataGridViewRow)gridRowInUse.Clone();
      var gridInUse = new DataGridView();

      var returnedRowsList = new List<DataGridViewRow>();

      var priceType = btnLineDisplayToggle.Text;
      var sportType = "";

      if (res.SportType != null) {
        sportType = res.SportType.Trim();
      }

      string spread = "Offline", moneyLine = "", totalPoints = "", teamTotalsOver = "",
          teamTotalsUnder = "", spread2 = "", moneyLine2 = "", totalPoints2 = "",
          teamTotalsOver2 = "", teamTotalsUnder2 = "";


      if (res.Status.Trim() != GamesAndLines.EVENT_OFFLINE) // not offline
            {
        var formattedWagerTypes = LineOffering.FormatWagerTypesOptionsDisplay(res, priceType, true, Customer.EasternLineFlag, sportType, _easternLineConversion);

        spread = formattedWagerTypes.Spread;
        moneyLine = RestrictMoneyLines.CheckForMlRestriction(formattedWagerTypes.MoneyLine1, res);
        totalPoints = formattedWagerTypes.TotalPoints;
        teamTotalsOver = formattedWagerTypes.TeamTotalsOver;
        teamTotalsUnder = formattedWagerTypes.TeamTotalsUnder;

        spread2 = formattedWagerTypes.Spread2;
        moneyLine2 = RestrictMoneyLines.CheckForMlRestriction(formattedWagerTypes.MoneyLine2, res);
        totalPoints2 = formattedWagerTypes.TotalPoints2;
        teamTotalsOver2 = formattedWagerTypes.TeamTotalsOver2;
        teamTotalsUnder2 = formattedWagerTypes.TeamTotalsUnder2;
      }

      var rowGroupNum = 0;
      rowGroupNum++;

      var team1Desc = "";

      if (res.Team1ID != null) {
        team1Desc = res.Team1ID.Trim();
      }
      if (sportType == SportTypes.BASEBALL && res.ListedPitcher1 != null) {
        team1Desc += " - " + res.ListedPitcher1.Trim();
      }
      if (row != null) {
        FillDataGridViewRowCells(gridInUse, row, res, res.Team1RotNum.ToString(), DisplayDate.Format(res.GameDateTime ?? Mdi.GetCurrentDateTime(), DisplayDateFormat.ShortDayNameMonthDayShortTime), res.Team1ID, team1Desc, spread, moneyLine, totalPoints, teamTotalsOver, teamTotalsUnder, rowGroupNum, 1, thisWeekEndScheduleDate);
        returnedRowsList.Add(row);
      }

      row = (DataGridViewRow)gridRowInUse.Clone();

      rowGroupNum++;
      var team2Desc = "";

      if (res.Team2ID != null) {
        team2Desc = res.Team2ID.Trim();
      }
      if (sportType == SportTypes.BASEBALL) {
        if (res.ListedPitcher2 != null)
          team2Desc += " - " + res.ListedPitcher2.Trim();
      }

      if (row == null) return returnedRowsList;
      FillDataGridViewRowCells(gridInUse, row, res, res.Team2RotNum.ToString(), sportType + " - " + res.SportSubType.Trim(), res.Team2ID, team2Desc, spread2, moneyLine2, totalPoints2, teamTotalsOver2, teamTotalsUnder2, rowGroupNum, 2, thisWeekEndScheduleDate);

      returnedRowsList.Add(row);

      if (res.DrawRotNum == null) return returnedRowsList;
      var moneyLineDraw = "";

      if (res.MoneyLineDraw != null && res.Status.Trim() != GamesAndLines.EVENT_OFFLINE) {
        moneyLineDraw = LineOffering.GetNumAdjSign(int.Parse(res.MoneyLineDraw.ToString()));
      }
      row = (DataGridViewRow)gridRowInUse.Clone();
      rowGroupNum++;

      if (row != null) {
        FillDataGridViewRowCells(gridInUse, row, res, res.DrawRotNum.ToString(), "", "", "Draw", "", moneyLineDraw, "", "", "", rowGroupNum, 3, thisWeekEndScheduleDate);
      }
      returnedRowsList.Add(row);
      return returnedRowsList;
    }

    private void FillDataGridViewRowCells(DataGridView gridInUse, DataGridViewRow row, spGLGetActiveGamesByCustomer_Result res, string rotationNumber, string gameDateTime, string teamId, string teamDesc, string spread, string moneyLine, string totalPoints, string teamTotalsOver, string teamTotalsUnder, int rowGroupNum, int teamIndex, DateTime thisWeekEndScheduleDate) {
      row.Cells[ROTATION_INDEX].Value = rotationNumber;
      row.Cells[GAMEDATE_INDEX].Value = gameDateTime;
      row.Cells[TEAMS_NAMES_INDEX].Value = teamDesc;
      row.Cells[SPREAD_INDEX].Value = spread;
      row.Cells[MONEY_INDEX].Value = moneyLine;
      row.Cells[TOTAL_INDEX].Value = totalPoints;
      row.Cells[TEAM_TOTAL_OVER_INDEX].Value = teamTotalsOver;
      row.Cells[TEAM_TOTAL_UNDER_INDEX].Value = teamTotalsUnder;
      row.Cells[GAME_NUM_INDEX].Value = res.GameNum;
      row.Cells[PERIOD_NUM_INDEX].Value = res.PeriodNumber;
      row.Cells[ROWGROUPNUM_INDEX].Value = rowGroupNum;
      row.Cells[TBD11_INDEX].Value = "";
      row.Cells[TBD12_INDEX].Value = "";
      row.Cells[TBD13_INDEX].Value = "";
      row.Cells[TBD14_INDEX].Value = "";
      row.Cells[TBD15_INDEX].Value = "";
      row.Cells[SPORT_INDEX].Value = res.SportType != null ? res.SportType.Trim() : "";
      row.Cells[SUBSPORT_INDEX].Value = res.SportSubType.Trim();
      row.Cells[PERIOD_CUTOFF_INDEX].Value = res.PeriodWagerCutoff.ToString().Trim();
      row.Cells[GAME_STATUS_INDEX].Value = res.Status;
      row.Cells[TEAM1_NAME_INDEX].Value = teamId ?? "".ToString(CultureInfo.InvariantCulture);
      row.Cells[CUSTPROFILE_INDEX].Value = res.CustProfile.Trim();
      row.Cells[SCHEDULEDATE_INDEX].Value = res.ScheduleDate;

      row.Height = GRIDVIEWROWHEIGHT;

      row.Cells[SPREAD_INDEX].Style.Font = new Font(gridInUse.Font, FontStyle.Regular);
      row.Cells[MONEY_INDEX].Style.Font = new Font(gridInUse.Font, FontStyle.Regular);
      if ((teamIndex == 1 || teamIndex == 2) &&
          res.FavoredTeamID != null
          && teamId != null && teamId.Trim() == res.FavoredTeamID.Trim()
          && spread != "Offline") {
        row.Cells[SPREAD_INDEX].Style.Font = new Font(gridInUse.Font, FontStyle.Bold);
        row.Cells[MONEY_INDEX].Style.Font = new Font(gridInUse.Font, FontStyle.Bold);
      }

      /*if (teamIndex == 1 || teamIndex == 2) {
        if (res.FavoredTeamID != null) {
            if (teamId != null && teamId.Trim() == res.FavoredTeamID.Trim())
            {
                if (spread != "Offline")
                {
              row.Cells[SPREAD_INDEX].Style.Font = new Font(gridInUse.Font, FontStyle.Bold);
              row.Cells[MONEY_INDEX].Style.Font = new Font(gridInUse.Font, FontStyle.Bold);
            }
                else
                {
              row.Cells[SPREAD_INDEX].Style.Font = new Font(gridInUse.Font, FontStyle.Regular);
              row.Cells[MONEY_INDEX].Style.Font = new Font(gridInUse.Font, FontStyle.Regular);
            }
            }
            else
            {
                row.Cells[SPREAD_INDEX].Style.Font = new Font(gridInUse.Font, FontStyle.Regular);
                row.Cells[MONEY_INDEX].Style.Font = new Font(gridInUse.Font, FontStyle.Regular);
          }
            else
            {
                row.Cells[SPREAD_INDEX].Style.Font = new Font(gridInUse.Font, FontStyle.Regular);
                row.Cells[MONEY_INDEX].Style.Font = new Font(gridInUse.Font, FontStyle.Regular);
        }
        }
        else {
          row.Cells[SPREAD_INDEX].Style.Font = new Font(gridInUse.Font, FontStyle.Regular);
          row.Cells[MONEY_INDEX].Style.Font = new Font(gridInUse.Font, FontStyle.Regular);
        }
      }*/

      row.DefaultCellStyle.BackColor = GetGridRowColor(res.Status, res.PeriodWagerCutoff ?? DateTime.MinValue, res.TimeChangeFlag, res.ScheduleDate, res.SportType, thisWeekEndScheduleDate);

      row.Cells[SPREAD_INDEX].Style.ForeColor = spread == "Offline" && teamIndex == 1 ? BLACKGAMECOLOR : GetRowCellColor(SPREAD_INDEX, res);//GetCellColor0(res.LastSpreadChange);

      row.Cells[MONEY_INDEX].Style.ForeColor = GetRowCellColor(MONEY_INDEX, res);//GetCellColor0(res.LastMoneyLineChange);
      row.Cells[TOTAL_INDEX].Style.ForeColor = GetRowCellColor(TOTAL_INDEX, res);//GetCellColor0(res.LastTtlPtsChange);
      row.Cells[TEAM_TOTAL_OVER_INDEX].Style.ForeColor = GetRowCellColor(TEAM_TOTAL_OVER_INDEX, res);//GetCellColor0(res.LastTeamPtsChange);
      row.Cells[TEAM_TOTAL_UNDER_INDEX].Style.ForeColor = GetRowCellColor(TEAM_TOTAL_UNDER_INDEX, res);//GetCellColor0(res.LastTeamPtsChange);

      if (res.Status.Trim() == GamesAndLines.EVENT_CIRCLED) {
        row.Cells[ROTATION_INDEX].Style.ForeColor = LOW_CIRCLED_GAME_COLOR; // Color.Red;
        row.Cells[GAMEDATE_INDEX].Style.ForeColor = LOW_CIRCLED_GAME_COLOR; // Color.Red;
        row.Cells[TEAMS_NAMES_INDEX].Style.ForeColor = LOW_CIRCLED_GAME_COLOR; // Color.Red;

      }
      else {
        row.Cells[ROTATION_INDEX].Style.ForeColor = BLACKGAMECOLOR; // Color.Black;
        row.Cells[GAMEDATE_INDEX].Style.ForeColor = BLACKGAMECOLOR; // Color.Black;
        row.Cells[TEAMS_NAMES_INDEX].Style.ForeColor = BLACKGAMECOLOR; // Color.Black;
      }

      if (ShowingActiVeGamesOnly) {
        if (row.DefaultCellStyle.BackColor == DOWNGAMECOLOR) {
          row.Visible = false;
        }
      }
    }

    private List<DataGridViewRow> BuildGridViewRowsForContest(DataGridViewRow gridRowInUse, spCnGetActiveContests_Result res) {
      var row = (DataGridViewRow)gridRowInUse.Clone();

      var returnedRowsList = new List<DataGridViewRow>();

      var priceType = btnLineDisplayToggle.Text;


      var contestDescr = res.ContestType.Trim();

      if (res.ContestType2.Trim() != "." && res.ContestType2.Trim() != "") {
        contestDescr = res.ContestType2.Trim();
      }

      if (res.ContestType3.Trim() != "." && res.ContestType3.Trim() != "") {
        contestDescr += " - " + res.ContestType3.Trim();
      }

      if (res.ContestDesc.Trim() != "." && res.ContestDesc.Trim() != "") {
        contestDescr += " - " + res.ContestDesc.Trim();
      }

      var thresholdValue = TwUtilities.GetContestThresholdValue(res);

      var odds = GetContestantOdds(priceType, res);

      if (row == null) return returnedRowsList;
      row.Cells[CONTEST_CUSTOM_DESC_INDEX].Value = contestDescr;
      row.Cells[CONTEST_ROTATION_INDEX].Value = res.RotNum;
      row.Cells[CONTESTANTNAME_INDEX].Value = res.ContestantName.Trim() + " " + thresholdValue;
      row.Cells[CONTESTANTODDS_INDEX].Value = odds;
      row.Cells[CONTESTANTNUM_INDEX].Value = res.ContestantNum;
      row.Cells[CONTEST_DATE_INDEX].Value = res.ContestDateTime;
      row.Cells[CONTEST_DESC_INDEX].Value = res.ContestDesc;
      row.Cells[CONTEST_COMMENTS_INDEX].Value = res.Comments;
      row.Cells[CONTESTNUM_INDEX].Value = res.ContestNum;
      row.Cells[CONTEST_CUSTPROFILE_INDEX].Value = res.CustProfile.Trim();

      row.Height = GRIDVIEWROWHEIGHT;
      if (res.WagerCutoff != null)
        row.DefaultCellStyle.BackColor = GetContestRowColor(res.Status, (DateTime)res.WagerCutoff);

      if (ShowingActiVeGamesOnly) {
        if (row.DefaultCellStyle.BackColor == DOWNGAMECOLOR) {
          row.Visible = false;
        }
      }
      returnedRowsList.Add(row);

      return returnedRowsList;
    }

    private void AddNewContestToList(int contestNumber) {
      using (var con = new Contests(AppModuleInfo)) {
        var newContestLines = con.GetActiveContestsByCustomer(CurrentCustomerId, contestNumber);

        if (newContestLines == null || newContestLines.Count <= 0) return;
        foreach (var res in newContestLines) {
          if (ActiveContests != null)
            ActiveContests.Add(res);
          if (_filteredActiveContests != null)
            _filteredActiveContests.Add(res);
        }
      }
    }

    private void AddNewGameToList(int gameNumber) {
      using (var gl = new GamesAndLines(AppModuleInfo)) {
        var newGameLines = gl.GetActiveGamesByCustomer(CurrentCustomerId, _currentCustomerStore, gameNumber);

        if (newGameLines == null || newGameLines.Count <= 0) return;
        if (ApplyVigDiscount && Mdi.VigDiscountLimits.Count > 0) {
          LineOffering.ApplyVigDiscountInBatch(Mdi.VigDiscountLimits, newGameLines, OddsDecimalPrecision, OddsFractionMaxDenominator);
        }

        foreach (var res in newGameLines) {
          if (ActiveGames != null)
            ActiveGames.Add(res);
          if (FilteredActiveGames != null)
            FilteredActiveGames.Add(res);
        }
      }
    }

    private void AddStageWagerItem(String frmMode, Ticket.WagerItem rbItemLog, spGLGetActiveGamesByCustomer_Result selectedGamePeriodInfo) {
      Mdi.Ticket.AddWagerItem(rbItemLog);

      var gameNumber = selectedGamePeriodInfo.GameNum;
      var periodNumber = selectedGamePeriodInfo.PeriodNumber;


      var allGamePeriodsInfo = (from a in ActiveGames where a.GameNum == gameNumber select a).ToList();

      foreach (var j in allGamePeriodsInfo) {

        if (j.PeriodNumber != periodNumber)
          continue;

        var selectedGameInfo = TwUtilities.ConvertToGameObject(j, rbItemLog.TicketNumber, rbItemLog.WagerNumber, rbItemLog.ItemNumber);
        if (frmMode == "New")
          Mdi.Ticket.SaveSelectedGameInfo(selectedGameInfo);
      }
    }

    private void AddToReadbackLogInfoList(int gameNumber, int periodNumber, String periodDescription, int ticketNumber, int wagerNumber, int itemNumber, String wagerType, String wagerTypeName, int seqNumber, DateTime logDateTime, String string1, String string2, String string3, Boolean rowIsClickable, String targetFrm, String freePlayFlag, String complexIfBetAmount = null, String fromPreviousTicketNumber = null) {
      var rbackItem = new ReadbackItem {
        GameNumber = gameNumber,
        PeriodNumber = periodNumber,
        PeriodDescription = periodDescription,
        TicketNumber = ticketNumber,
        WagerNumber = wagerNumber,
        ItemNumber = itemNumber,
        WagerTypeName = wagerTypeName,
        SeqNumber = seqNumber,
        LogDateTime = logDateTime,
        TicketWriter = Environment.UserName,
        String1 = string1,
        String2 = string2,
        String3 = string3,
        RowIsClickable = rowIsClickable,
        TargetFrm = targetFrm,
        RowContainsEditItemInfo = false,
        WagerType = wagerType,
        FreePlayFlag = freePlayFlag,
        SpecialIfBetAmount = complexIfBetAmount,
        FromPreviousTicket = fromPreviousTicketNumber
      };

      if (ReadbackItems == null) {
        ReadbackItems = new List<ReadbackItem> { rbackItem };
      }
      else {
        ReadbackItems.Add(rbackItem);
      }
    }

    private void AddToReadbackLogInfoList(int gameNumber, int periodNumber, String periodDescription, int ticketNumber, int wagerNumber, int itemNumber, String wagerType, String wagerTypeName, int seqNumber, DateTime logDateTime, String string1, String string2, String string3, ReadbackItem.WagerItemEditInfo wiEditInfo, Boolean rowIsClickable, String targetFrm, Boolean isrifBet, String rifWinOnlyFlag, String rifBackwardTicketNumber, String rifBackwardWagerNumber, Boolean showInHookupFrm, DateTime? gameDateTime, String freePlayFlag, String complexIfBetAmt = null) {
      var rbackItem = new ReadbackItem {
        GameNumber = gameNumber,
        PeriodNumber = periodNumber,
        PeriodDescription = periodDescription,
        GameDateTime = gameDateTime,
        TicketNumber = ticketNumber,
        WagerNumber = wagerNumber,
        ItemNumber = itemNumber,
        WagerType = wagerType,
        WagerTypeName = wagerTypeName,
        SeqNumber = seqNumber,
        LogDateTime = logDateTime,
        TicketWriter = Environment.UserName,
        String1 = string1,
        String2 = string2,
        String3 = string3,
        WiEditInfo = wiEditInfo,
        RowIsClickable = rowIsClickable,
        TargetFrm = targetFrm,
        RowContainsEditItemInfo = true,
        IsrifBetType = isrifBet,
        RifWinOnlyFlag = rifWinOnlyFlag,
        RifBackwardTicketNumber = rifBackwardTicketNumber,
        RifBackwardWagerNumber = rifBackwardWagerNumber,
        ShowInHookupFrm = showInHookupFrm,
        FreePlayFlag = freePlayFlag,
        SpecialIfBetAmount = complexIfBetAmt
      };

      if (ReadbackItems == null) {
        ReadbackItems = new List<ReadbackItem> { rbackItem };
      }
      else {
        ReadbackItems.Add(rbackItem);
      }
    }

    private void AddToReadbackLogInfoList(int gameNumber, int periodNumber, String periodDescription, int ticketNumber, int wagerNumber, int itemNumber, String wagerType, String wagerTypeName, int seqNumber, DateTime logDateTime, String string1, String string2, String string3, ReadbackItem.ManualWagerItemEditInfo mWiEditInfo, Boolean rowIsClickable, String targetFrm) {
      var rbackItem = new ReadbackItem {
        GameNumber = gameNumber,
        PeriodNumber = periodNumber,
        PeriodDescription = periodDescription,
        TicketNumber = ticketNumber,
        WagerNumber = wagerNumber,
        ItemNumber = itemNumber,
        WagerType = wagerType,
        WagerTypeName = wagerTypeName,
        SeqNumber = seqNumber,
        LogDateTime = logDateTime,
        TicketWriter = Environment.UserName,
        String1 = string1,
        String2 = string2,
        String3 = string3,
        MwiEditInfo = mWiEditInfo,
        RowIsClickable = rowIsClickable,
        TargetFrm = targetFrm,
        RowContainsEditItemInfo = true
      };

      if (ReadbackItems == null) {
        ReadbackItems = new List<ReadbackItem> { rbackItem };
      }
      else {
        ReadbackItems.Add(rbackItem);
      }
    }

    private void AddToContestReadbackLogInfoList(int contestNumber, int ticketNumber, int wagerNumber, int itemNumber, String wagerType, String wagerTypeName, int seqNumber, DateTime logDateTime, String string1, String string2, String string3, ReadbackItem.ContestItemEditInfo cWiEditInfo, Boolean rowIsClickable, String targetFrm, String freePlayFlag) {
      var rbackItem = new ReadbackItem {
        GameNumber = contestNumber,
        TicketNumber = ticketNumber,
        WagerNumber = wagerNumber,
        ItemNumber = itemNumber,
        WagerType = wagerType,
        WagerTypeName = wagerTypeName,
        SeqNumber = seqNumber,
        LogDateTime = logDateTime,
        TicketWriter = Environment.UserName,
        String1 = string1,
        String2 = string2,
        String3 = string3,
        CwiEditInfo = cWiEditInfo,
        RowIsClickable = rowIsClickable,
        TargetFrm = targetFrm,
        RowContainsEditItemInfo = true,
        FreePlayFlag = freePlayFlag
      };

      if (ReadbackItems == null) {
        ReadbackItems = new List<ReadbackItem> { rbackItem };
      }
      else {
        ReadbackItems.Add(rbackItem);
      }
    }

    public static string GetAdjustedLine(Ticket.WagerItem wi) {
      double? adjustedLine = null;
      switch (wi.WagerType) {
        case WagerType.SPREAD:
          if (wi.AdjSpread != null && wi.TeaserPoints != null)
            adjustedLine = wi.AdjSpread + wi.TeaserPoints;
          break;
        case WagerType.TOTALPOINTS:
        case WagerType.TEAMTOTALPOINTS:
          if (wi.AdjTotalPoints != null && wi.TeaserPoints != null && !String.IsNullOrEmpty(wi.TotalPointsOu)) {
            if (wi.TotalPointsOu.ToLower() == "o") {
              adjustedLine = wi.AdjTotalPoints + wi.TeaserPoints;
            }
            else {
              adjustedLine = wi.AdjTotalPoints - wi.TeaserPoints;
            }
          }
          break;
        case WagerType.MONEYLINE:
          break;
      }
      if (adjustedLine == null)
        return "";

      if (adjustedLine == 0)
          return GamesAndLines.PICK;
        var t = adjustedLine.ToString().Replace(".5", GamesAndLines.HALFSYMBOL);
        if (adjustedLine > 0 && wi.WagerType == WagerType.SPREAD)
            t = t.Insert(0, "+");
        return t;
    }

    private void ChangeContestStatus(int contestNum, String contestStatus) {
      if (ActiveContests == null || ActiveContests.Count <= 0) return;

      var activeContestants = (from p in ActiveContests where p.ContestNum == contestNum select p).ToList();

      if (activeContestants.Count <= 0) return;
      foreach (var res in activeContestants) {
        res.Status = contestStatus;
      }

      if (_filteredActiveContests == null || _filteredActiveContests.Count <= 0) return;
      var filteredActiveContestants = (from p in _filteredActiveContests where p.ContestNum == contestNum select p).ToList();

      if (filteredActiveContestants.Count <= 0) return;
      foreach (var res in filteredActiveContestants) {
        res.Status = contestStatus;
      }
    }

    private void ChangeContestCircledStatus(int contestNum) {
      if (ActiveContests == null || ActiveContests.Count <= 0)
        return;
      var activeContestants = (from p in ActiveContests where p.ContestNum == contestNum select p).ToList();
      List<spCnGetActiveContests_Result> newContestLines;

      using (var con = new Contests(AppModuleInfo)) {
        newContestLines = con.GetActiveContestsByCustomer(CurrentCustomerId, contestNum);
      }

      if (activeContestants.Count <= 0) return;

      if (newContestLines == null) return;
      foreach (var res in activeContestants) {

        var newLine = (from ncl in newContestLines
                       where ncl.ContestNum == res.ContestNum &&
                             ncl.ContestantNum == res.ContestantNum
                       select ncl).FirstOrDefault();
        if (newLine == null) continue;
        res.Status = newLine.Status;
        res.CircledMaxWager = newLine.CircledMaxWager;
      }

      if (_filteredActiveContests == null)
        return;
      var filteredActiveContestants = (from p in _filteredActiveContests where p.ContestNum == contestNum select p).ToList();

      if (filteredActiveContestants.Count <= 0) return;
      foreach (var res in filteredActiveContestants) {

        var newLine = (from ncl in newContestLines
                       where ncl.ContestNum == res.ContestNum &&
                             ncl.ContestantNum == res.ContestantNum
                       select ncl).FirstOrDefault();
        if (newLine == null) continue;
        res.Status = newLine.Status;
        res.CircledMaxWager = newLine.CircledMaxWager;
      }
    }

    private void ChangeGameStatus(int gameNum, String gameStatus) {
      if (ActiveGames == null || ActiveGames.Count <= 0)
        return;
      var activeGame = (from p in ActiveGames where p.GameNum == gameNum select p).ToList();
      var filteredActiveGame = (from p in ActiveGames where p.GameNum == gameNum select p).ToList();

      if (activeGame.Count <= 0) return;
      foreach (var res in activeGame) {
        res.Status = gameStatus;
      }
      if (filteredActiveGame.Count <= 0) return;
      foreach (var res in filteredActiveGame) {
        res.Status = gameStatus;
      }
    }

    private void ChangeGameCircledStatus(int gameNum) {
      if (ActiveGames == null || ActiveGames.Count <= 0)
        return;
      var activeGame = (from p in ActiveGames where p.GameNum == gameNum select p).ToList();
      List<spGLGetActiveGamesByCustomer_Result> newGameLines;
      using (var gl = new GamesAndLines(AppModuleInfo)) {
        newGameLines = gl.GetActiveGamesByCustomer(CurrentCustomerId, _currentCustomerStore, gameNum);
      }

      if (activeGame.Count <= 0) return;

      if (newGameLines != null) {
        foreach (var res in activeGame) {
          var newLine = (from ngl in newGameLines
                         where ngl.GameNum == res.GameNum &&
                             ngl.PeriodNumber == res.PeriodNumber
                         select ngl).FirstOrDefault();
          if (newLine != null) {
            res.Status = newLine.Status;
            res.CircledMaxWager = newLine.CircledMaxWager;
            res.CircledMaxWagerMoneyLine = newLine.CircledMaxWagerMoneyLine;
            res.CircledMaxWagerSpread = newLine.CircledMaxWagerSpread;
            res.CircledMaxWagerTeamTotal = newLine.CircledMaxWagerTeamTotal;
            res.CircledMaxWagerTotal = newLine.CircledMaxWagerTotal;
          }
        }
        if (FilteredActiveGames == null) return;
        var filteredActiveGame = (from p in FilteredActiveGames where p.GameNum == gameNum select p).ToList();
        if (!filteredActiveGame.Any()) return;

        foreach (var res in filteredActiveGame) {
          var newLine = (from ngl in newGameLines
                         where ngl.GameNum == res.GameNum &&
                             ngl.PeriodNumber == res.PeriodNumber
                         select ngl).FirstOrDefault();
          if (newLine != null) {
            res.Status = newLine.Status;
            res.CircledMaxWager = newLine.CircledMaxWager;
            res.CircledMaxWagerMoneyLine = newLine.CircledMaxWagerMoneyLine;
            res.CircledMaxWagerSpread = newLine.CircledMaxWagerSpread;
            res.CircledMaxWagerTeamTotal = newLine.CircledMaxWagerTeamTotal;
            res.CircledMaxWagerTotal = newLine.CircledMaxWagerTotal;
          }
        }
      }
    }

    private void ChangePeriodGameWagerCutoff(int gameNum, int periodNumber, DateTime newPeriodWagerCutoff) {
      if (ActiveGames == null)
        return;
      var activeGame = (from p in ActiveGames where p.GameNum == gameNum && p.PeriodNumber == periodNumber select p).ToList();

      if (activeGame.Count <= 0) return;
      foreach (var res in activeGame) {
        res.PeriodWagerCutoff = newPeriodWagerCutoff;
      }

      if (FilteredActiveGames == null)
        return;
      var fiteredActiveGame = (from p in FilteredActiveGames where p.GameNum == gameNum && p.PeriodNumber == periodNumber select p).ToList();
      if (fiteredActiveGame.Count <= 0) return;
      foreach (var res in fiteredActiveGame) {
        res.PeriodWagerCutoff = newPeriodWagerCutoff;
      }
    }

    private Boolean CheckForIntersectingPeriod(string sportType, string sportSubType, int? periodNum1, int? periodNum2) {
      if (periodNum1 == periodNum2) {
        return true;
      }

      if (periodNum1 == null)
        periodNum1 = 0;
      if (periodNum2 == null)
        periodNum2 = 0;

      int intersectCount;

      using (var gl = new GamesAndLines(AppModuleInfo)) {
        intersectCount = gl.GetGamePeriodIntersectInfo(sportType, sportSubType, (int)periodNum1, (int)periodNum2);
      }

      var returnValue = intersectCount != 0;

      return returnValue;

    }

    private void ClearGameInfo() {
      txtTeam1ID.Text = "";
      txtTeam2ID.Text = "";
      txtGameDateTime.Text = "";
      textBox3.Text = "";
      txtTeam1Score.Text = "";
      txtTeam2Score.Text = "";
      txtGameInfo.Text = "";
    }

    private void DetermineLinesRefreshingMode() {
      if (StaticLinesDuringCall == null) return;
      StartLinesAutoRefreshProcess();
      switch (StaticLinesDuringCall) {
        case "Y":
          btnUpdateLines.Text = @"Manual Refresh";
          btnUpdateLines.Enabled = true;
          break;
        case "N":
          btnUpdateLines.Text = @"Automatic Refresh";
          btnUpdateLines.Enabled = false;
          break;
      }
    }

    private void DrawGridLines(object sender, DataGridViewRowPostPaintEventArgs e, int gameNumCellIdx) {
      const int lineThickness = 1;

      var selectedNode = _gamesAndProps.GetSelectedNodeObject();
      if (selectedNode == null) return;
      try {
        var type = ((BetOptionNode)selectedNode).NodeType;

        if ( /*rootType == "G" && */type == "G" && !((DataGridView)sender).Name.Contains("PropsAndContests") && _formatGameTitles.IsTitle(((DataGridView)sender).Rows[e.RowIndex]))
          return;

        var columnsTotalWidth = (from DataGridViewColumn column in ((DataGridView)sender).Columns where column.Visible select column.Width).Sum();

        var currentGameContestNum = "";
        if (((DataGridView)sender).Rows[e.RowIndex].Cells[gameNumCellIdx].Value != null)
          currentGameContestNum = ((DataGridView)sender).Rows[e.RowIndex].Cells[gameNumCellIdx].Value.ToString();

        var nextGameContestNum = "";
        if (e.RowIndex + 1 < ((DataGridView)sender).Rows.Count && ((DataGridView)sender).Rows[e.RowIndex + 1].Cells[gameNumCellIdx].Value != null)
          nextGameContestNum = ((DataGridView)sender).Rows[e.RowIndex + 1].Cells[gameNumCellIdx].Value.ToString();

        var p = new Pen(WHITEGAMECOLOR, lineThickness);
        var y = e.RowBounds.Bottom - (int)Math.Ceiling(p.Width / 2);

        if (nextGameContestNum != currentGameContestNum) {
          p = new Pen(BLACKGAMECOLOR, lineThickness);
          e.Graphics.DrawLine(p, e.RowBounds.Left, y, columnsTotalWidth, y);
        }
        else {
          p = new Pen(WHITEGAMECOLOR, lineThickness);
          e.Graphics.DrawLine(p, e.RowBounds.Left, y, columnsTotalWidth, y);
        }

        if (e.RowIndex > 0) {
          var previousGameContestNum = "";
          if (((DataGridView)sender).Rows[e.RowIndex - 1].Cells[gameNumCellIdx].Value != null)
            previousGameContestNum = ((DataGridView)sender).Rows[e.RowIndex - 1].Cells[gameNumCellIdx].Value.ToString();

          if (previousGameContestNum != currentGameContestNum) {
            p = new Pen(BLACKGAMECOLOR, lineThickness);
            var y1 = e.RowBounds.Top - (int)Math.Ceiling(p.Width / 2);
            e.Graphics.DrawLine(p, e.RowBounds.Left, y1, columnsTotalWidth, y1);
          }
          else {
            p = new Pen(WHITEGAMECOLOR, lineThickness);
            var y1 = e.RowBounds.Top - (int)Math.Ceiling(p.Width / 2);
            e.Graphics.DrawLine(p, e.RowBounds.Left, y1, columnsTotalWidth, y1);
          }
        }
        var x = e.RowBounds.Left;

        var colsCnt = (from DataGridViewColumn column in ((DataGridView)sender).Columns where column.Visible select column).Count();
        var cnt = 0;

        foreach (DataGridViewColumn column in ((DataGridView)sender).Columns) {
          if (column.Visible) {
            x += column.Width;
            var xStart = x - column.Width;
            var j = new Pen(WHITEGAMECOLOR, 1);
            if (cnt > 0)
              e.Graphics.DrawLine(j, xStart - 1, e.RowBounds.Bottom, xStart - 1, e.RowBounds.Top);
            if (cnt < colsCnt - 1)
              e.Graphics.DrawLine(p, x - 1, e.RowBounds.Bottom, x - 1, e.RowBounds.Top);
          }
          cnt++;
        }
      }
      catch (Exception ex) {
        Log(ex);
      }
    }

    private void EnableDisableGamePeriodsRadBtns(String sportType, String sportSubType) {
      var enableAll = Mdi.ShowingLatestLines;
      var availableGamePeriods = GetAvailableGamePeriods(sportType, sportSubType);

      if (availableGamePeriods.Count <= 0) return;
      foreach (var ctrl in panGamePeriodSelection.Controls.Cast<Control>().Where(ctrl => ctrl.GetType().ToString() == "System.Windows.Forms.RadioButton")) {
        ctrl.Enabled = enableAll;
        ctrl.Visible = true;
        var radText = ctrl.Text;
        if (sportType.Trim().ToLower() != "baseball" && ctrl.Name == "radAltRL_3")
          ctrl.Visible = false;
          if (availableGamePeriods.Any(res => res.ShortDescription.Trim() == radText))
          {
              ctrl.Enabled = true;
          }
          //foreach (var res in availableGamePeriods.Where(res => res.ShortDescription.Trim() == radText)) {
          //ctrl.Enabled = true;
          //break;
        //}
      }

    }

    private List<spGLGetAvailablePeriodsBySport_Result> GetAvailableGamePeriods(String sportType, String sportSubType) {
      List<spGLGetAvailablePeriodsBySport_Result> availableGamePeriods;
      using (var gl = new GamesAndLines(AppModuleInfo)) {
        availableGamePeriods = gl.GetAvailableGamePeriods(sportType, sportSubType);
      }
      return availableGamePeriods;
    }

    private void ExecuteUpdateLinesThreadedWork() {
      UpdateGameLineObjectsWithBackgroundWorker();
    }

    private void FillEasternLineObject() {
      _easternLineConversion = new List<spGLGetEasternLineConversionInfo_Result>();

      using (var gl = new GamesAndLines(AppModuleInfo)) {
        _easternLineConversion = gl.GetEasternLineConversionInfo();
      }
    }

    private void FillFrmText() {
      if (Customer != null && Customer.CustomerID != null)
        Text = Customer.CustomerID.ToUpper().Trim();
      Text += @" (";
      if (Customer != null && Customer.CreditAcctFlag != null)
        Text += Customer.CreditAcctFlag.Trim() == "Y" ? "Credit - " : "Post Up - ";
      if (Mdi.CustomerStore != null && Mdi.CustomerStore.Store != null)
        Text += Mdi.CustomerStore.Store.ToUpper().Trim();
      Text += @" ) found in ";
      if (Mdi.ParameterList != null)
        Text += Mdi.ParameterList.GetItem("DataBaseName").Value;

      if (Customer != null && Customer.LastCallDateTime != null)
        Text += @" || Last Call received on " + Customer.LastCallDateTime;

      var currentDateTime = Mdi.GetCurrentDateTime();

      Text += @" ==> Server Time: " + currentDateTime.ToLongTimeString();
      AppendCustomerServiceExtension();
    }

    private void AppendCustomerServiceExtension() {
      var xCs = "";
      if (Customer != null) {
        if (Customer.AgentID != null && Customer.AgentID.ToUpper().StartsWith("X")) {
          xCs = ConfigurationManager.AppSettings["XCSExtension"];
        }
        else {
          xCs = ConfigurationManager.AppSettings["DefaultCSExtension"];
        }
      }
      if (xCs != "") {
        Text += @" CS Ext." + xCs;
      }
    }

    private void FillPeriodGameInformation(RadioButton sender) {
      var rad = sender;

      if (!rad.Checked) {
        return;
      }
      var grvw = (DataGridView)Controls.Find(GamesGridViewInUse, true).FirstOrDefault();

      if (grvw != null) {
        grvw.Rows.Clear();
        grvw.Refresh();
      }

      var periodNumber = int.Parse(rad.Name.Split('_')[1]);
      var sportType = "";
      var txtTeamOrAsterisk = (TextBox)Mdi.Controls.Find("txtTeamOrAsterisk", true).FirstOrDefault();
      var periodDescription = rad.AccessibleDescription;

      if (!Mdi.ShowingLatestLines) {
        String startDatePart = "", endDatePart = "", sportSubType = "";
        DateTime startDateTime = Mdi.GetCurrentDateTime(), endDateTime = startDateTime;
        int? gameNumber = null;

        if (CurrSelectedSportBranch != null && CurrSelectedSportBranch.Count > 0) {
          startDatePart = CurrSelectedSportBranch[0].StartDatePart;
          endDatePart = CurrSelectedSportBranch[0].EndDatePart;
          startDateTime = CurrSelectedSportBranch[0].StartDateTime;
          endDateTime = CurrSelectedSportBranch[0].EndDateTime;
          sportType = CurrSelectedSportBranch[0].SportType;
          sportSubType = CurrSelectedSportBranch[0].SportSubType;
          gameNumber = CurrSelectedSportBranch[0].GameNumber;
          CurrSelectedSportBranch.RemoveAt(0);
        }
        if (CurrSelectedSportBranch != null) {
          CurrSelectedSportBranch.Add(new SelectedSportBranch {
            StartDatePart = startDatePart,
            EndDatePart = endDatePart,
            StartDateTime = startDateTime,
            EndDateTime = endDateTime,
            SportSubType = sportSubType,
            PeriodNumber = periodNumber,
            SportType = sportType,
            GameNumber = gameNumber
          });
        }
        if (sportSubType != "") {
          if (endDatePart == "") {
            lblCurrentlyShowing.Text = Resources.FrmSportAndGameSelection_treSports_MouseClick_ + sportType + Resources.FrmSportAndGameSelection_treSports_MouseClick_ + TwUtilities.AddLeadingZeroes(startDatePart) + Resources.FrmSportAndGameSelection_treSports_MouseClick_ + sportSubType;
          }
          else {
            lblCurrentlyShowing.Text = Resources.FrmSportAndGameSelection_treSports_MouseClick_ + sportType + Resources.FrmSportAndGameSelection_treSports_MouseClick_ + TwUtilities.AddLeadingZeroes(startDatePart) + @" to " + TwUtilities.AddLeadingZeroes(endDatePart) + Resources.FrmSportAndGameSelection_treSports_MouseClick_ + sportSubType;
          }
        }
        else {
          if (endDatePart == "") {
            lblCurrentlyShowing.Text = Resources.FrmSportAndGameSelection_treSports_MouseClick_ + sportType + Resources.FrmSportAndGameSelection_treSports_MouseClick_ + TwUtilities.AddLeadingZeroes(startDatePart);
          }
          else {
            lblCurrentlyShowing.Text = Resources.FrmSportAndGameSelection_treSports_MouseClick_ + sportType + Resources.FrmSportAndGameSelection_treSports_MouseClick_ + TwUtilities.AddLeadingZeroes(startDatePart) + @" to " + TwUtilities.AddLeadingZeroes(endDatePart);
          }
        }

        if (txtTeamOrAsterisk == null) return;
        var likeClause = txtTeamOrAsterisk.Text.Trim();
        if (likeClause != "") {
          lblCurrentlyShowing.Text = "";
          FillGamesGridView(null, null, periodNumber, likeClause, null, periodDescription);
        }
        else {
          FillGamesGridView(CurrSelectedSportBranch, null, periodNumber);
        }
        return;
      }
      if (Customer != null && Customer.LastCallDateTime != null) {
        FillGamesGridView(null, Customer.LastCallDateTime, periodNumber);
      }
      else {
        if (txtTeamOrAsterisk == null) return;
        var likeClause = txtTeamOrAsterisk.Text.Trim();
        if (likeClause != "") {
          FillGamesGridView(null, null, periodNumber, likeClause, null, periodDescription);
        }
        else {
          FillGamesGridView(null, null, periodNumber);
        }
      }
    }

    delegate void SetMethodCallback();

    private void FinishUpdateLinesThreadedWork() {
        if (InvokeRequired)
        {
            var d = new SetMethodCallback(FinishUpdateLinesThreadedWork);
            Invoke(d);
        }
        else
      UpdateGameLineUiElementsWithBackgroundWorker();
    }

    private Color GetRowCellColor(int cellIndex, spGLGetActiveGamesByCustomer_Result res) {
      var cellFontColor = BLACKGAMECOLOR;
      DateTime? lastLineChange = null;
      var circleLimitType = "L";
      var lineCircleStatus = "";
      double? circleLimitAmt = null;

      switch (cellIndex) {
        case SPREAD_INDEX:
          lastLineChange = res.LastSpreadChange;
          circleLimitType = res.CircledMaxWagerSpreadType ?? "L";
          lineCircleStatus = res.SpreadLineStatus ?? "";
          circleLimitAmt = res.CircledMaxWagerSpread;
          break;
        case MONEY_INDEX:
          lastLineChange = res.LastMoneyLineChange;
          circleLimitType = res.CircledMaxWagerMoneyLineType ?? "L";
          lineCircleStatus = res.MoneyLineStatus ?? "";
          circleLimitAmt = res.CircledMaxWagerMoneyLine;
          break;
        case TOTAL_INDEX:
          lastLineChange = res.LastTtlPtsChange;
          circleLimitType = res.CircledMaxWagerTotalType ?? "L";
          lineCircleStatus = res.TotalsLineStatus ?? "";
          circleLimitAmt = res.CircledMaxWagerTotal;
          break;
        case TEAM_TOTAL_OVER_INDEX:
        case TEAM_TOTAL_UNDER_INDEX:
          lastLineChange = res.LastTeamPtsChange;
          circleLimitType = res.CircledMaxWagerTeamTotalType ?? "L";
          lineCircleStatus = res.TeamTotalsLineStatus ?? "";
          circleLimitAmt = res.CircledMaxWagerTeamTotal;
          break;

      }
      if (lineCircleStatus == GamesAndLines.EVENT_CIRCLED && circleLimitAmt > 0) {
        cellFontColor = circleLimitType == GamesAndLines.EVENT_OFFLINE ? HIGH_CIRCLED_GAME_COLOR : LOW_CIRCLED_GAME_COLOR;
      }
      else {
        if (_currentCustomerLastCall == null || lastLineChange == null) return cellFontColor;
        if (lastLineChange < _currentCustomerLastCall)
          cellFontColor = BLUEGAMECOLOR; // Color.Blue;
      }
      return cellFontColor;
    }

    private List<spCnGetActiveContests_Result> GetLinesForSelectedContest(List<ContestTreeNodeDesc> nodesInfo) {
      if (ActiveContests == null) return null;
      List<spCnGetActiveContests_Result> filteredActiveContests = null;

      if (nodesInfo == null) return null;
      switch (nodesInfo.Count) {
        case 1:
          filteredActiveContests = (from p in ActiveContests where p.ContestType.Trim() == nodesInfo[0].LevelValue select p).OrderBy(p => p.FirstRotNum).ThenBy(p => p.ContestType).ThenBy(p => p.ContestType2).ThenBy(p => p.ContestType3).ThenBy(p => p.ContestNum).ThenBy(p => p.RotNum).ThenBy(p => p.ContestantSeq).ThenBy(p => p.LineSeq).ToList();
          break;

        case 2:
          switch (nodesInfo[1].LevelTarget) {
            case "ContestType2":
              filteredActiveContests = (from p in ActiveContests where p.ContestType.Trim() == nodesInfo[0].LevelValue && p.ContestType2.Trim() == nodesInfo[1].LevelValue select p).OrderBy(p => p.FirstRotNum).ThenBy(p => p.ContestType).ThenBy(p => p.ContestType2).ThenBy(p => p.ContestType3).ThenBy(p => p.ContestNum).ThenBy(p => p.RotNum).ThenBy(p => p.ContestantSeq).ThenBy(p => p.LineSeq).ToList();
              break;

            case "ContestType3":
              filteredActiveContests = (from p in ActiveContests where p.ContestType.Trim() == nodesInfo[0].LevelValue && p.ContestType3.Trim() == nodesInfo[1].LevelValue select p).OrderBy(p => p.FirstRotNum).ThenBy(p => p.ContestType).ThenBy(p => p.ContestType2).ThenBy(p => p.ContestType3).ThenBy(p => p.ContestNum).ThenBy(p => p.RotNum).ThenBy(p => p.ContestantSeq).ThenBy(p => p.LineSeq).ToList();
              break;

            case "ContestDesc":
              filteredActiveContests = (from p in ActiveContests where p.ContestType.Trim() == nodesInfo[0].LevelValue && p.ContestDesc.Trim() == nodesInfo[1].LevelValue select p).OrderBy(p => p.FirstRotNum).ThenBy(p => p.ContestType).ThenBy(p => p.ContestType2).ThenBy(p => p.ContestType3).ThenBy(p => p.ContestNum).ThenBy(p => p.RotNum).ThenBy(p => p.ContestantSeq).ThenBy(p => p.LineSeq).ToList();
              break;
          }
          break;

        case 3:
          switch (nodesInfo[2].LevelTarget) {
            case "ContestType3":
              filteredActiveContests = (from p in ActiveContests where p.ContestType.Trim() == nodesInfo[0].LevelValue && p.ContestType2.Trim() == nodesInfo[1].LevelValue && p.ContestType3.Trim() == nodesInfo[2].LevelValue select p).OrderBy(p => p.FirstRotNum).ThenBy(p => p.ContestType).ThenBy(p => p.ContestType2).ThenBy(p => p.ContestType3).ThenBy(p => p.ContestNum).ThenBy(p => p.RotNum).ThenBy(p => p.ContestantSeq).ThenBy(p => p.LineSeq).ToList();
              break;

            case "ContestDesc":
              filteredActiveContests = (from p in ActiveContests where p.ContestType.Trim() == nodesInfo[0].LevelValue && p.ContestType2.Trim() == nodesInfo[1].LevelValue && p.ContestDesc.Trim() == nodesInfo[2].LevelValue select p).OrderBy(p => p.FirstRotNum).ThenBy(p => p.ContestType).ThenBy(p => p.ContestType2).ThenBy(p => p.ContestType3).ThenBy(p => p.ContestNum).ThenBy(p => p.RotNum).ThenBy(p => p.ContestantSeq).ThenBy(p => p.LineSeq).ToList();
              break;
          }
          break;

        case 4:
          filteredActiveContests = (from p in ActiveContests where p.ContestType.Trim() == nodesInfo[0].LevelValue && p.ContestType2.Trim() == nodesInfo[1].LevelValue && p.ContestType2.Trim() == nodesInfo[2].LevelValue && p.ContestDesc.Trim() == nodesInfo[3].LevelValue select p).OrderBy(p => p.FirstRotNum).ThenBy(p => p.ContestType).ThenBy(p => p.ContestType2).ThenBy(p => p.ContestType3).ThenBy(p => p.ContestNum).ThenBy(p => p.RotNum).ThenBy(p => p.ContestantSeq).ThenBy(p => p.LineSeq).ToList();
          break;

      }

      return filteredActiveContests;
    }

    private int? GetLastLineChangeNum() {
      using (var gl = new GamesAndLines(AppModuleInfo)) {
        return gl.GetLastLineChangeNumberByCustomer(CurrentCustomerId);
      }
    }

    private WagerLimitValidator.LimitResult GetLimitForCurrentParlayOrTeaser(String wagerType, int ticketNumber, int? wagerNumber) {

      var args = new WagerLimitValidator.ValidatorArgs {
        CurrencyCode = Customer.Currency,
        CustomerId = Customer.CustomerID,
        MaxContestBet = Customer.ContestMaxBet ?? 0,
        CPL = Customer.ParlayMaxBet ?? 0,
        CTL = Customer.TeaserMaxBet ?? 0,
        SYPL = AppModuleInfo.Parameters.MaxParlayBet,
        SYTL = AppModuleInfo.Parameters.MaxTeaserBet,
        CQL = Customer.ActiveWagerLimit ?? 0,
        Store = _currentCustomerStore,
        CEPTL = wagerType == "P" ? Customer.EnforceParlayMaxBetFlag == "Y" : (wagerType == "T" && Customer.EnforceTeaserMaxBetFlag == "Y"),
        OfferHighLimitsActive = Mdi.CustomerRestrictions.Any(r => r.Code == "HIGHLIMITS")
      };

      var validator = new WagerLimitValidator(AppModuleInfo, args, WagerLimitValidator.LimitsSourceList.Cu);
      var wagersInfo = Mdi.Ticket.WagerItems == null ? new List<WagerLimitValidator.WagerItemLimitsInfo>() : Mdi.Ticket.WagerItems.Where(w => w.Loo != null &&
          ((wagerNumber != null && w.TicketNumber == ticketNumber && w.WagerNumber == (int)wagerNumber) || (w.TicketNumber == ticketNumber && wagerNumber == null))).Select(a => new WagerLimitValidator.WagerItemLimitsInfo {
            SportType = a.Loo.SportType,
            SportSubType = a.Loo.SportSubType,
            WagerType = a.WagerType,
            PeriodNumber = a.Loo.PeriodNumber,
            CircledMaxWagerMoneyLine = a.Loo.CircledMaxWagerMoneyLine ?? 0,
            CircledMaxWagerMoneyLineType = a.Loo.CircledMaxWagerMoneyLineType,
            CircledMaxWagerSpread = a.Loo.CircledMaxWagerSpread ?? 0,
            CircledMaxWagerSpreadType = a.Loo.CircledMaxWagerSpreadType,
            CircledMaxWagerTeamTotal = a.Loo.CircledMaxWagerTeamTotal ?? 0,
            CircledMaxWagerTeamTotalType = a.Loo.CircledMaxWagerTeamTotalType,
            CircledMaxWagerTotalType = a.Loo.CircledMaxWagerTotalType,
            CircledMaxWagerTotal = a.Loo.CircledMaxWagerTotal ?? 0,
            GameStatus = a.Loo.Status,
            GameNum = a.Loo.GameNum,
            ChosenTeamId = a.ChosenTeamId,
            IsContest = a.IsContest,
            ContestItemWagerLimit = a.ContestItemWagerLimit
          }).ToList();
      return validator.GetMaximumWagerLimit(wagersInfo, wagerType);
    }

    public WagerLimitValidator.LimitResult GetLimitForCurrentContestWager(spCnGetActiveContests_Result contestInfo) {
      var args = new WagerLimitValidator.ValidatorArgs {
        CurrencyCode = Customer.Currency,
        CustomerId = Customer.CustomerID,
        MaxContestBet = Customer.ContestMaxBet ?? 0,
        CPL = Customer.ParlayMaxBet ?? 0,
        CTL = Customer.TeaserMaxBet ?? 0,
        SYPL = AppModuleInfo.Parameters.MaxParlayBet,
        SYTL = AppModuleInfo.Parameters.MaxTeaserBet,
        CQL = Customer.ActiveWagerLimit ?? 0,
        Store = _currentCustomerStore,
        CEPTL = false,
        OfferHighLimitsActive = Mdi.CustomerRestrictions.Any(r => r.Code == "HIGHLIMITS")
      };

      var validator = new WagerLimitValidator(AppModuleInfo, args, WagerLimitValidator.LimitsSourceList.Cu);

      return validator.GetContestWagerLimit(contestInfo.Status, contestInfo.MoneyLine ?? 0, TwUtilities.GetContestCircleLimit(contestInfo) ?? 0, Customer.WagerLimit ?? 0, contestInfo.ContestantMaxWager ?? 0, contestInfo.ExposureLimit ?? 0, Customer.ContestMaxBet ?? 0, Params.MaxContestBet, Params.IncludeCents);
    }

    private Color GetGridRowColor(String gameStatus, DateTime wagerCutOff, String gameTimeChanged, DateTime? scheduleDate, String sportType, DateTime thisWeekEndScheduleDate) {

      var rowColor = WHITEGAMECOLOR;

      var currentGame = _currentgamecolor; // Color.LightYellow;
      var timeChange = _timechangecolor; // Color.White;
      var futureGame = FUTUREGAMECOLOR; // Color.LightPink;
      var downGame = DOWNGAMECOLOR; // Color.LightSkyBlue;

      if (sportType.Trim() == SportTypes.FOOTBALL) {
        if (scheduleDate != null) {
          var endScheduleDate = ((DateTime)scheduleDate).AddDays(7);

          if (thisWeekEndScheduleDate < scheduleDate) {
            rowColor = futureGame;
            return rowColor;
          }

          if (wagerCutOff <= Mdi.GetCurrentDateTime() || gameStatus == GamesAndLines.EVENT_CANCELLED || gameStatus == GamesAndLines.EVENT_COMPLETED) {
            rowColor = downGame;
            return rowColor;
          }

          if (!(wagerCutOff.Date >= scheduleDate) ||
              wagerCutOff.Date >= endScheduleDate || wagerCutOff <= Mdi.GetCurrentDateTime())
            return rowColor;
        }
        rowColor = currentGame;
        if (gameTimeChanged == null) return rowColor;
        if (gameTimeChanged.Trim() == "Y")
          rowColor = timeChange;
        return rowColor;
      }
      var currentDateToCompare = Mdi.GetCurrentDateTime().Date.AddDays(1);

      if (wagerCutOff > currentDateToCompare) {
        rowColor = futureGame;
        return rowColor;
      }

      if (wagerCutOff <= Mdi.GetCurrentDateTime() || gameStatus == GamesAndLines.EVENT_CANCELLED || gameStatus == GamesAndLines.EVENT_COMPLETED) {
        rowColor = downGame;
        return rowColor;
      }

      if (wagerCutOff.Date != Mdi.GetCurrentDateTime().Date || wagerCutOff <= Mdi.GetCurrentDateTime())
        return rowColor;
      rowColor = currentGame;
      if (gameTimeChanged == null) return rowColor;
      if (gameTimeChanged.Trim() == "Y")
        rowColor = timeChange;
      return rowColor;
    }

    private Color GetContestRowColor(String contestStatus, DateTime wagerCutOff) {
      var rowColor = BLACKGAMECOLOR;

      switch (contestStatus) {

        case GamesAndLines.EVENT_COMPLETED: //Complete DownGame??
        case GamesAndLines.EVENT_OFFLINE: //Offline
        case GamesAndLines.EVENT_CIRCLED: //Circled
        case GamesAndLines.EVENT_OPEN: //Open
          if (wagerCutOff >= Mdi.GetCurrentDateTime()) {
            rowColor = wagerCutOff.Day > Mdi.GetCurrentDateTime().Day ? FUTUREGAMECOLOR : _currentgamecolor;
            return rowColor;
          }
          rowColor = DOWNGAMECOLOR; // Color.LightSkyBlue;
          break;

        case GamesAndLines.EVENT_CANCELLED: //Cancelled?
          break;
      }

      return rowColor;
    }

    private void HideGridLineSeparator(int rowIndex, DataGridView currentlyShowing) {
      foreach (DataGridViewRow row in currentlyShowing.Rows) {
        if (row.Index <= rowIndex) continue;
        if (row.DefaultCellStyle.BackColor != GRAYGAMECOLOR) continue;
        row.Visible = false;
        break;
      }
    }

    private bool IsWagerTypeAvailable(DataGridView grvw, int cellIdx, int rowIdx) {
      var wagerType = Enums.SelectedWagerTypeCode(cellIdx);

      var sportType = grvw.Rows[rowIdx].Cells[SPORT_INDEX].Value.ToString().Trim();
      var sportSubType = grvw.Rows[rowIdx].Cells[SUBSPORT_INDEX].Value.ToString().Trim();

      List<spLOGetTeaserSportSpec_Result> selectedTeaserSportsSpecs = null;

      var frm = FormF.GetFormByName("frmPlaceTeaser", this);

      if (frm == null) {
        return false;
      }

      var frmPt = (FrmPlaceTeaser)frm;

      if (frmPt.TeaserSportSpecsByTeaser != null && frmPt.TeaserSportSpecsByTeaser.Count > 0) {

        selectedTeaserSportsSpecs = frmPt.TeaserSportSpecsByTeaser;
      }

      return selectedTeaserSportsSpecs != null && selectedTeaserSportsSpecs.Any(item => wagerType.Trim() == item.WagerType.Trim() && sportType == item.SportType.Trim() && sportSubType == item.SportSubType.Trim());

    }

    public bool IsWagerTypeAvailable(String wagerType, String sportType, String sportSubType) {

      List<spLOGetTeaserSportSpec_Result> selectedTeaserSportsSpecs = null;

      var frm = FormF.GetFormByName("frmPlaceTeaser", this);

      if (frm == null) {
        return false;
      }

      var frmPt = (FrmPlaceTeaser)frm;

      if (frmPt.TeaserSportSpecsByTeaser != null && frmPt.TeaserSportSpecsByTeaser.Count > 0) {

        selectedTeaserSportsSpecs = frmPt.TeaserSportSpecsByTeaser;
      }

      return selectedTeaserSportsSpecs != null && selectedTeaserSportsSpecs.Any(item => wagerType.Trim() == item.WagerType.Trim() && sportType == item.SportType.Trim() && sportSubType == item.SportSubType.Trim());

    }

    private void LoadContestWageringForm(DataGridView sender, int rowIndex, int columnIndex) {
      String contestNum;
      String contestantNumber;
      String contestant;
      String contestDesc;
      DateTime contestDateTime;
      var odds = "";

      if (sender.Rows.Count > 0 && rowIndex >= 0) {
        odds = sender.Rows[rowIndex].Cells[columnIndex].Value.ToString();
      }
      ShowContestInfo(sender, rowIndex, out contestNum, out contestantNumber, out contestant, out contestDesc, out contestDateTime);

      if (columnIndex == 3 && contestNum != "" && odds != "" && odds != "Offline" && ContestIsAvailable(int.Parse(contestNum))) {
        switch (SelectedWagerType.ToLower()) {
          case "if-bet":
            MessageBox.Show(@"Only games are accepted when creating an if-bet");
            return;
          case "manual play":
            return;
          case "parlay":
            if (!LoginsAndProfiles.ValidateUserFunctionalityAccess(Mdi.CurrentUserPermissions, LoginsAndProfiles.ALLOW_CONTESTS_IN_PARLAYS)) {
              MessageBox.Show(@"Contests are not allowed in " + SelectedWagerType.ToLower() + @"s.");
              return;
            }
            break;
          case "teaser":
            MessageBox.Show(@"Contests are not allowed in " + SelectedWagerType.ToLower() + @"s.");
            return;
        }
        if (WageringMode == "M") {
          var mFrm = new FrmManualContestWager(SelectedWagerType, this, ActiveContests, int.Parse(contestNum), int.Parse(contestantNumber), contestant, btnLineDisplayToggle.Text, Mdi.LayoffWagersEnabled) {
            Owner = this,
            FrmMode = "New",
            IsFreePlay = IsFreePlay,
            Icon = Icon
          };
          mFrm.ShowDialog();
          mFrm.Dispose();
        }
        else {
          var frm = new FrmPlaceContestOrProp(SelectedWagerType, this, ActiveContests, int.Parse(contestNum), int.Parse(contestantNumber), contestant, btnLineDisplayToggle.Text, _currentCustomerPercentBook) {
            Owner = this,
            FrmMode = "New",
            IsFreePlay = IsFreePlay,
            Icon = Icon
          };
          frm.ShowDialog();
          frm.Dispose();
        }
      }

      if (odds == "Offline") {
        MessageBox.Show(@"Wagering has been suspended for this contestant", @"Suspended");
      }
    }

    private void LoadSportAndGameSelection() {
      ApplyVigDiscount = false;
      if (Mdi.VigDiscountLimits != null && (SelectedWagerType == null || SelectedWagerType == "Straigt bet")) {
        if (Mdi.VigDiscountLimits.Count > 0) {
          ApplyVigDiscount = true;
          LineOffering.ApplyVigDiscountInBatch(Mdi.VigDiscountLimits, ActiveGames, OddsDecimalPrecision, OddsFractionMaxDenominator);
        }
      }

      treSports.AfterSelect -= treSports_AfterSelect;

      PropertyNames = new List<string> { "RowBackgroundColor", "RowBottomBorderColor", "RowBottomBorderThickness", "RowForegroundColor", "RowCellsFontWeight" };
      FillFrmText();
      _lastGameChangeNum = GetLastLineChangeNum();
      DetermineLinesRefreshingMode();

      FillAvailableSportsTree(false);
      GroupSportsByScheduleDate();
      btnLineDisplayToggle.Text = TwUtilities.GetPriceTypeName(_currentCustomerPriceType);
      DisableGamePeriodsRadBtns();
      WriteDgvwGameHeader(dgvwGame);
      TwUtilities.WriteDgvwPropsAndContestsHeader(dgvwPropsAndContests);

      if (_currentCustomerEastLineEnabled == "Y") {
        FillEasternLineObject();
      }
      SelectedWagerType = "Straight Bet";
    }

    private void GroupSportsByScheduleDate() {
      if (ActiveGames == null)
        return;

      if (_sportsAndScheduleDates == null) {
        _sportsAndScheduleDates = new List<SportScheduleDate>();
      }

      _sportsAndScheduleDates.Clear();

      _sportsAndScheduleDates = (from a in ActiveGames
                                 group a by new { a.SportType, a.SportSubType, a.ScheduleDate } into gAg
                                 select new SportScheduleDate { SportType = gAg.Key.SportType, SportSubType = gAg.Key.SportSubType, ScheduleDate = gAg.Key.ScheduleDate }).ToList();

    }

    private bool TeamTotalsIsAllowed(int columnIndex) {
      if (columnIndex != 6 && columnIndex != 7 ||
          SelectedWagerType == "Straight Bet" ||
          LoginsAndProfiles.ValidateUserFunctionalityAccess(Mdi.CurrentUserPermissions, LoginsAndProfiles.ACCEPT_ANY_WAGER))
        return true;
      return false;
    }

    private void LoadWageringForm(DataGridView sender, int rowIndex, int columnIndex) {
      if (rowIndex <= -1) return;

      if (_formatGameTitles.IsTitle(sender.Rows[rowIndex]))
        return;

      if (!TeamTotalsIsAllowed(columnIndex)) {
        var message = "Team Totals cannot be used in " + SelectedWagerType + "s.";
        MessageBox.Show(message, ParameterList.GetItem("AppName").Value, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        return;
      }

      var rowGroupNum = sender.Rows[rowIndex].Cells[ROWGROUPNUM_INDEX].Value.ToString();
      if (rowGroupNum == "4")
        return;

      if (!Enums.CellIsClickable(columnIndex)) return;
      int gameNumber;
      int periodNumber;
      DateTime gameWagerCutoffDateTime;
      String team1Id;
      String team2Id;
      int ticketNumber = Mdi.TicketNumber;


      ShowGameInfo(sender, rowIndex, out gameNumber, out periodNumber, out gameWagerCutoffDateTime, out team1Id, out team2Id);

      if (columnIndex == 0 || columnIndex == 1) {
        return;
      }

      if (columnIndex == 2) {

        using (var gameScores = new FrmGameScores(gameNumber, gameWagerCutoffDateTime, team1Id, team2Id, Mdi.AppModuleInfo)) {
          gameScores.Icon = Icon;
          gameScores.ShowDialog();
          return;
        }
      }

      var gridViewCell = sender.Rows[rowIndex].Cells[columnIndex];
      var cellValue = gridViewCell.Value.ToString();

      if (SelectedWagerType == "Parlay" || SelectedWagerType == "If-Bet" || SelectedWagerType == "Teaser") {
        var currentGameAndPeriod = (from g in FilteredActiveGames where g.GameNum == gameNumber && g.PeriodNumber == periodNumber select g).FirstOrDefault();
        if (currentGameAndPeriod == null) return;

        if ((currentGameAndPeriod.ParlayRestriction == GamesAndLines.PARLAY_DENY_ALL && (SelectedWagerType == "If-Bet" || SelectedWagerType == "Teaser")) ||
            (currentGameAndPeriod.ParlayRestriction == GamesAndLines.PARLAY_DENY_ALL && SelectedWagerType == "Parlay" && !LoginsAndProfiles.ValidateUserFunctionalityAccess(Mdi.CurrentUserPermissions, LoginsAndProfiles.IGNORE_PARLAY_RESTRICTION))
            ) {
          var message = "This game cannot be used in " + SelectedWagerType + "s.";
          MessageBox.Show(message, ParameterList.GetItem("AppName").Value, MessageBoxButtons.OK, MessageBoxIcon.Warning);
          return;
        }

        if ((currentGameAndPeriod.SportType.Trim() == SportTypes.SOCCER || currentGameAndPeriod.SportType.Trim() == SportTypes.HOCKEY) && TwUtilities.IsAsianLine(columnIndex, int.Parse(rowGroupNum), currentGameAndPeriod)) {
          var message = "Asian handicap not allowed in " + SelectedWagerType + "s.";
          MessageBox.Show(message, ParameterList.GetItem("AppName").Value, MessageBoxButtons.OK, MessageBoxIcon.Warning);
          return;
        }
      }

      var loadWagerFrm = true;
      switch (SelectedWagerType) {
        case "Straight Bet":
        case "Parlay":
        case "If-Bet":
          if (SelectedWagerType == "If-Bet") {
            if (FrmPlaceIfBet != null && FrmPlaceIfBet.IfBet != null) {
              if (!FrmPlaceIfBet.IsSpecialIfBet && FrmPlaceIfBet.CurrentPicks >= FrmPlaceIfBet.MaxPicks) {
                var message = "Too many teams have been selected. A maximum of " + FrmPlaceIfBet.MaxPicks + " is permitted.";
                MessageBox.Show(message, ParameterList.GetItem("AppName").Value, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                loadWagerFrm = false;
              }
              ticketNumber = FrmPlaceIfBet.IfBet.TicketNumber;
            }
          }

          if (SelectedWagerType == "Parlay") {
            if (FrmPlaceParlay != null && FrmPlaceParlay.Parlay != null) {
              ticketNumber = FrmPlaceParlay.Parlay.TicketNumber;
            }

          }

          if (cellValue != "" && cellValue != "Offline" && loadWagerFrm)
            LoadMakeAWagerFrm(sender, rowIndex, columnIndex, IsFreePlay, ticketNumber);
          break;
        case "Teaser":
          if (IsWagerTypeAvailable(sender, columnIndex, rowIndex) && periodNumber == 0) {
            if (cellValue != "" && cellValue != "Offline") {
              if (FrmPlaceTeaser != null && FrmPlaceTeaser.Teaser != null) {
                if (FrmPlaceTeaser.Teaser.MaxPicks > 0 && FrmPlaceTeaser.CurrentPicks != null) {
                  if (FrmPlaceTeaser.CurrentPicks >= FrmPlaceTeaser.Teaser.MaxPicks) {
                    var message = "Too many teams have been selected. A maximum of " + FrmPlaceTeaser.Teaser.MaxPicks + " is permitted.";
                    MessageBox.Show(message, ParameterList.GetItem("AppName").Value, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    loadWagerFrm = false;
                  }
                  ticketNumber = FrmPlaceTeaser.Teaser.TicketNumber;
                }
              }

              if (loadWagerFrm) {
                LoadMakeAWagerFrm(sender, rowIndex, columnIndex, IsFreePlay, ticketNumber);
              }
            }
          }
          else {
            if (cellValue != "")
              MessageBox.Show(Resources.FrmSportAndGameSelection_dgvwGame_CellClick_Game_selection_not_allowed_in_selected_teaser_);
          }
          break;
      }
    }

    private bool PeriodWagerCutoffBypassed(DateTime periodWagerCutoff) {
      var dtNow = Mdi.GetCurrentDateTime();
      const string attemptedAction = "Accept Any Wager";
      if (!(Params.NotificationMinutesAfterCutoff > 0)) return true;

      if (dtNow <= periodWagerCutoff) return true;
      if (dtNow < periodWagerCutoff.AddMinutes(Params.NotificationMinutesAfterCutoff) || LoginsAndProfiles.ValidateUserFunctionalityAccess(Mdi.CurrentUserPermissions, attemptedAction))
        return true;
      MessageBox.Show(@"The game period has already started or is not currently available", @"Not available");
      return false;
    }

    private void PrepareIfBetStageWager(String frmMode, int ticketNumber, int wagerNumberToUpd, TextBox txtRisk, TextBox txtToWin, String wagerTypeName,
                                String wagerType, int? aRBirdcageLink, int? aRLink, string continueOnPushFlag, int? boxLink, Boolean isFreePlay, String layoffWager, String complexIfBetAmount) {
      int wagerNumberForWi;
      CreateStageWager(frmMode, ticketNumber, wagerNumberToUpd, out wagerNumberForWi, txtRisk, txtToWin, wagerTypeName, wagerType, null, null, 1, 0, null, aRBirdcageLink, aRLink, continueOnPushFlag, boxLink, "", isFreePlay, layoffWager, null, complexIfBetAmount);
    }

    private void PrepareParlayStageWager(String frmMode, int ticketNumber, int wagerNumberToUpd, TextBox txtRisk, TextBox txtToWin, String wagerTypeName,
                                String wagerType, string parlayName, int? roundRobinLink, string parlayPayoutType, Boolean isFreePlay, String layoffWager) {
      int wagerNumberForWi;
      CreateStageWager(frmMode, ticketNumber, wagerNumberToUpd, out wagerNumberForWi, txtRisk, txtToWin, wagerTypeName, wagerType, null, parlayName, 1, 0, roundRobinLink, null, null, null, null, parlayPayoutType, isFreePlay, layoffWager);
    }

    private void PrepareStraightBetStageWager(String frmMode, int ticketNumber, int wagerNumberToUpd, out int wagerNumberForWi, TextBox txtRisk, TextBox txtToWin, String wagerTypeName,
                                String wagerType, Boolean isFreePlay, String layoffWager) {
      CreateStageWager(frmMode, ticketNumber, wagerNumberToUpd, out wagerNumberForWi, txtRisk, txtToWin, wagerTypeName, wagerType, null, null, 1, 0, null, null, null, null, null, "", isFreePlay, layoffWager);
    }

    private void PrepareTeaserStageWager(String frmMode, int ticketNumber, int wagerNumberToUpd, TextBox txtRisk, TextBox txtToWin, String wagerTypeName,
                                String wagerType, string teaserName, int minPicks, int ties, Boolean isFreePlay, String layoffWager) {
      int wagerNumberForWi;
      CreateStageWager(frmMode, ticketNumber, wagerNumberToUpd, out wagerNumberForWi, txtRisk, txtToWin, wagerTypeName, wagerType, teaserName, null, minPicks, ties, null, null, null, null, null, "", isFreePlay, layoffWager);
    }

    private void RemoveFormObjectsFromMemory() {
      _activeContests = null;
      _filteredActiveContests = null;
      ActiveGames = null;
      FilteredActiveGames = null;
      _daysOfTheWeek = null;
      CurrSelectedSportBranch = null;
      if (_gamesAndProps != null)
        _gamesAndProps.Dispose();
      _gamesAndProps = null;
      ReadbackItems = null;
      _sportsAndScheduleDates = null;
      if (SqlDepLinesChange != null)
      {
          SqlDepLinesChange.DependencyChanged -= SqlDepLinesChange_DependencyChanged;
          SqlDependency.Stop(SqlDepLinesChange.Connection.ConnectionString);
    }
      SqlDepLinesChange = null;
    }

    private string SetWiseActionFlagValue(int itemNumber, string selectedWagerTypeName) {
      var isWise = "";

      switch (selectedWagerTypeName) {
        case "Straight Bet":
          isWise = _currentCustomerIsWise;
          break;
        case "If-Bet":
          if (itemNumber == 1)
            isWise = _currentCustomerIsWise;

          if (isWise.Length == 0) {
            isWise = "N";
          }
          break;
        case "Parlay":
        case "Teaser":
        case "Manual":
          isWise = "N";
          break;
      }
      return isWise;
    }

    private void ShowContestInfo(object sender, int rowIndex, out string contestNumber, out String contestantNumber, out string contestant, out string contestDesc, out DateTime contestDateTime) {
      contestNumber = "";
      contestantNumber = "";
      contestant = "";
      contestDesc = "";
      contestDateTime = Mdi.GetCurrentDateTime();

      if (rowIndex < 0 || rowIndex >= ((DataGridView)sender).Rows.Count) {
        return;
      }
      var cellTwoValue = ((DataGridView)sender).Rows[rowIndex].Cells[CONTESTNUM_INDEX].Value;

      if (cellTwoValue == null || string.IsNullOrEmpty(cellTwoValue.ToString()))
        return;

      contestantNumber = ((DataGridView)sender).Rows[rowIndex].Cells[CONTESTANTNUM_INDEX].Value.ToString();
      contestNumber = ((DataGridView)sender).Rows[rowIndex].Cells[CONTESTNUM_INDEX].Value.ToString();

      contestant = ((DataGridView)sender).Rows[rowIndex].Cells[CONTESTANTNAME_INDEX].Value.ToString();
      contestDesc = ((DataGridView)sender).Rows[rowIndex].Cells[CONTEST_DESC_INDEX].Value.ToString();
      contestDateTime = DateTime.Parse(((DataGridView)sender).Rows[rowIndex].Cells[CONTEST_DATE_INDEX].Value.ToString());

      var contestComments = "";

      if (((DataGridView)sender).Rows[rowIndex].Cells[CONTEST_COMMENTS_INDEX].Value != null) {
        contestComments = ((DataGridView)sender).Rows[rowIndex].Cells[CONTEST_COMMENTS_INDEX].Value.ToString();
      }

      txtGameInfo.Text = contestDesc;
      txtGameInfo.Text += Environment.NewLine;
      txtGameInfo.Text += @" Contest Time: ";
      txtGameInfo.Text += contestDateTime.DayOfWeek.ToString();
      txtGameInfo.Text += @" - ";
      txtGameInfo.Text += DisplayDate.Format(contestDateTime, DisplayDateFormat.LongDateTime);
      txtGameInfo.Text += Environment.NewLine;
      txtGameInfo.Text += contestComments;
    }

    private void ShowGameInfo(DataGridView sender, int rowIndex, out int gameNumber, out int periodNumber, out DateTime gameWagerCutoffDateTime, out String team1Id, out String team2Id) {

      var gameNum = int.Parse(sender.Rows[rowIndex].Cells[GAME_NUM_INDEX].Value.ToString());
      var periodNum = int.Parse(sender.Rows[rowIndex].Cells[PERIOD_NUM_INDEX].Value.ToString());
      var periodWagerCutoffDateTime = Mdi.GetCurrentDateTime();
      DateTime? gameDateTime = periodWagerCutoffDateTime;

      gameNumber = gameNum;
      periodNumber = periodNum;

      if (ActiveGames == null) {
        gameWagerCutoffDateTime = DateTime.Now;
        team1Id = "";
        team2Id = "";
        return;
      }

      var selectedGame = (from p in ActiveGames where p.GameNum == gameNum && p.PeriodNumber == periodNum select p).FirstOrDefault();

      gameWagerCutoffDateTime = Mdi.GetCurrentDateTime();
      team1Id = "";
      var team1RotNum = "";
      team2Id = "";
      var team2RotNum = "";
      var gameStatus = "";
      var gameComments = "";

      if (selectedGame != null) {
        gameWagerCutoffDateTime = selectedGame.WagerCutoff.GetValueOrDefault();
        gameDateTime = selectedGame.GameDateTime;
        periodWagerCutoffDateTime = selectedGame.PeriodWagerCutoff ?? DateTime.MinValue;
        team1Id = selectedGame.Team1ID;
        team1RotNum = selectedGame.Team1RotNum.ToString();
        team2Id = selectedGame.Team2ID;
        team2RotNum = selectedGame.Team2RotNum.ToString();
        gameStatus = selectedGame.Status;
        if (selectedGame.Comments != null) {
          gameComments = selectedGame.Comments.Trim();
        }
        if (selectedGame.LastSpreadChange != null)
          lblLastSpreadChange.Text = selectedGame.LastSpreadChange.Value.ToString(CultureInfo.InvariantCulture);
        if (selectedGame.LastMoneyLineChange != null)
          lblLastMoneyLineChange.Text = selectedGame.LastMoneyLineChange.Value.ToString(CultureInfo.InvariantCulture);
        if (selectedGame.LastTeamPtsChange != null)
          lblLastTeamTotalsChange.Text = selectedGame.LastTeamPtsChange.Value.ToString(CultureInfo.InvariantCulture);
        if (selectedGame.LastTtlPtsChange != null)
          lblLastTotalPntsChange.Text = selectedGame.LastTtlPtsChange.Value.ToString(CultureInfo.InvariantCulture);

        lblGameDateTime.Text = @"GameDateTime: " + selectedGame.GameDateTime;
        lblWagerCutOff.Text = @"GameCutoff: " + selectedGame.WagerCutoff;
        lblPeriodWagerCutoff.Text = @"PeriodGameCutoff: " + selectedGame.PeriodWagerCutoff;
        lblGameStatus.Text = selectedGame.Status;
        lblTimeChanged.Text = selectedGame.TimeChangeFlag;
        lblGameNumber.Text = @"Game#: " + selectedGame.GameNum;
      }

      txtTeam1ID.Text = team1RotNum + @". " + team1Id.Trim();
      txtTeam2ID.Text = team2RotNum + @". " + team2Id.Trim();
      //txtGameDateTime.Text = DisplayDate.Format(gameWagerCutoffDateTime, DisplayDateFormat.ShortTimeAndShortDate);
      //textBox3.Text = "wCutoff: " + DisplayDate.Format(periodWagerCutoffDateTime, DisplayDateFormat.ShortTimeAndShortDate);

      if (gameDateTime != null)
        txtGameDateTime.Text = DisplayDate.Format((DateTime)gameDateTime, DisplayDateFormat.ShortTimeAndShortDate);
      if (selectedGame != null) {
        textBox3.Text = selectedGame.BroadcastInfo ?? "";

        var timeSpan = (periodWagerCutoffDateTime - Mdi.GetCurrentDateTime());
        var minutes = timeSpan.TotalMinutes;

        if (minutes > 0 && minutes < 60) {
          txtGameDateTime.Text = Math.Round(minutes) + @" minute(s) remaining";
        }
        else {
          if (minutes < 1) {
            txtGameDateTime.Text = @" < 1 minute remaining";
          }
        }


        if ((gameStatus != null && gameStatus == GamesAndLines.EVENT_COMPLETED) || (selectedGame.PeriodWagerCutoff != null && selectedGame.PeriodWagerCutoff < Mdi.GetCurrentDateTime())) {
          txtGameDateTime.Text = @"*** DOWN ***";
          textBox3.Text = "";

          if (gameStatus == GamesAndLines.EVENT_COMPLETED) {
            List<spGLGetGameScores_Result> gameResults;

            using (var gl = new GamesAndLines(AppModuleInfo)) {
              gameResults = gl.GetGameScores(gameNumber);
            }

            if (gameResults != null) {
              var gamePeriodResult = (from p in gameResults where p.PeriodNumber == 0 select p).FirstOrDefault();
              if (gamePeriodResult != null) {
                txtTeam1Score.Text = gamePeriodResult.Team1Score.ToString();
                txtTeam2Score.Text = gamePeriodResult.Team2Score.ToString();
              }
            }
          }
          else {
            txtTeam1Score.Text = "";
            txtTeam2Score.Text = "";
          }
        }
        else {
          txtTeam1Score.Text = "";
          txtTeam2Score.Text = "";
        }
      }

      txtGameInfo.Text = "";
      if (gameStatus != null && gameStatus == GamesAndLines.EVENT_CIRCLED) {
        txtGameInfo.Text += @"*** Circled ***";
        txtGameInfo.Text += Environment.NewLine;
      }

      txtGameInfo.Text += gameComments;
    }

    private void ShowHideInactiveGames(String actionToTake) {
      var filterProps = false;
      var firstDisplayedIdx = -1;
      try {
        switch (actionToTake) {
          case "Hide":
          case "Show":
            var currentlyShowing = (DataGridView) Controls.Find(GamesGridViewInUse, true).FirstOrDefault();
            if (currentlyShowing != null) {
              if (currentlyShowing.Visible) {
                if (currentlyShowing.Rows.Count > 0) {
                  if (actionToTake == "Hide") {
                    foreach (var row in currentlyShowing.Rows.Cast<DataGridViewRow>().Where(row => row.DefaultCellStyle.BackColor == DOWNGAMECOLOR)) {
                      row.Visible = false;
                      HideGridLineSeparator(row.Index, currentlyShowing);
                    }
                  }
                  else {
                    foreach (DataGridViewRow row in currentlyShowing.Rows) {
                      row.Visible = true;
                    }
                  }
                  foreach (var row in currentlyShowing.Rows.Cast<DataGridViewRow>().Where(row => row.Visible)) {
                    firstDisplayedIdx = row.Index;
                    break;
                  }
                  if (firstDisplayedIdx > -1) {
                    currentlyShowing.FirstDisplayedScrollingRowIndex = firstDisplayedIdx;
                    if (currentlyShowing.Rows[firstDisplayedIdx].Cells[ROTATION_INDEX].Visible)
                      currentlyShowing.Rows[firstDisplayedIdx].Cells[ROTATION_INDEX].Selected = true;
                  }
                }
              }
              else
                filterProps = true;
            }
            if (filterProps) {
              currentlyShowing = (DataGridView) Controls.Find(PropsGridViewInUse, true).FirstOrDefault();
              if (currentlyShowing != null) {
                if (currentlyShowing.Visible) {
                  if (currentlyShowing.Rows.Count > 0) {
                    if (actionToTake == "Hide") {
                      foreach (var row in currentlyShowing.Rows.Cast<DataGridViewRow>().Where(row => row.DefaultCellStyle.BackColor == DOWNGAMECOLOR)) {
                        row.Visible = false;
                      }
                    }
                    else {
                      foreach (DataGridViewRow row in currentlyShowing.Rows) {
                        row.Visible = true;
                      }
                    }

                    foreach (var row in currentlyShowing.Rows.Cast<DataGridViewRow>().Where(row => row.Visible)) {
                      firstDisplayedIdx = row.Index;
                      break;
                    }
                    if (firstDisplayedIdx > -1) {
                      currentlyShowing.FirstDisplayedScrollingRowIndex = firstDisplayedIdx;
                      if (currentlyShowing.Rows[firstDisplayedIdx].Cells[CONTEST_CUSTOM_DESC_INDEX].Visible)
                        currentlyShowing.Rows[firstDisplayedIdx].Cells[CONTEST_CUSTOM_DESC_INDEX].Selected = true;
                    }
                  }
                }
              }
            }
            break;
        }
      }
      catch (Exception ex) {
        Log(ex);
      }
    }

    private void StartLinesAutoRefreshProcess() {
      bgwrkUpdateLines.RunWorkerAsync();
      if (UseSqlDependency)
      {
          Connection = new SqlConnection(ConnectionManager.GetDatabaseConnectionString(AppModuleInfo));
          SqlDependency.Stop(Connection.ConnectionString);
          SqlDependency.Start(Connection.ConnectionString);
          SqlDependencyToGetLastLineChangeNotifications(Connection);
      }
      else
      {
      LinesUpdater = new Timer { Interval = Params.UpdateGameMilliSeconds };
      LinesUpdater.Tick += LinesUpdater_Tick;
      LinesUpdater.Start();
    }
    }

    private void SqlDependencyToGetLastLineChangeNotifications(SqlConnection connection)
    {
        if (SqlDepLinesChange == null)
        {
            SqlDepLinesChange = new SIDSQLDependency(AppModuleInfo, connection);
            SqlDepLinesChange.DependencyChanged += SqlDepLinesChange_DependencyChanged;
        }
        var paramsList = new List<SqlParameter>();
       
        
        var prm = new SqlParameter("@customerID", SqlDbType.VarChar)
        {
            Direction = ParameterDirection.Input,
            DbType = DbType.String,
            Value = _currentCustomerStore
        };
        paramsList.Add(prm);

        
        prm = new SqlParameter("@LastChangeNum", SqlDbType.Int)
        {
            Direction = ParameterDirection.Input,
            DbType = DbType.Int32,
            Value = (_lastGameChangeNum ?? 0)
        };
        paramsList.Add(prm);
        

        SqlDepLinesChange.ExecuteSQLCommand("spGLSqlDependencyGetLastLineChanges", paramsList);
        if (SqlDepLinesChange.Connection != null && SqlDepLinesChange.Connection.State == ConnectionState.Open)
        {
            SqlDepLinesChange.Connection.Close();
        }
    }

    private void ToggleLineDisplay(string currentLineDisplay) {
      string newLineDisplay;

      switch (currentLineDisplay) {
        case "American":
          newLineDisplay = "Decimal";
          break;
        case "Decimal":
          newLineDisplay = "Fractional";
          break;
        default:
          newLineDisplay = "American";
          break;
      }
      btnLineDisplayToggle.Text = newLineDisplay;

      var dgvw = (DataGridView)Controls.Find(GamesGridViewInUse, true).FirstOrDefault();

      if (dgvw != null) {
        if (dgvw.Rows.Count > 0) {
          dgvw.Rows.Clear();
          FillGamesGridView(CurrSelectedSportBranch, null, null);
        }
      }

      dgvw = (DataGridView)Controls.Find("dgvwPropsAndContests", true).FirstOrDefault();

      if (dgvw == null) return;
      if (dgvw.Rows.Count <= 0) return;
      dgvw.Rows.Clear();
      FillContestsGridView(ContestNodesInfo);
    }

    private void UpdateSpecificContestsInGridView(List<ContestTreeNodeDesc> selection, int? contestNum, int? contestantNum, String custProfile) {
      if (PropsGridViewInUse == null) return;
      var dgv = (DataGridView)Controls.Find(PropsGridViewInUse, true).FirstOrDefault();
      if (dgv != null && dgv.Rows.Count > 0) {
        if (dgv.Visible == false)
          return;
      }

      if (contestNum == null || contestantNum == null)
        return;

      DataGridViewRow gvR = null;
      var contestFound = false;
      if (dgv == null) return;
      for (var i = 0; i < dgv.Rows.Count; i++) {
        if (dgv.Rows[i].Cells[CONTESTNUM_INDEX].Value == null || contestNum != int.Parse(dgv.Rows[i].Cells[CONTESTNUM_INDEX].Value.ToString()) ||
          dgv.Rows[i].Cells[CONTESTANTNUM_INDEX].Value == null || contestantNum != int.Parse(dgv.Rows[i].Cells[CONTESTANTNUM_INDEX].Value.ToString()) ||
          dgv.Rows[i].Cells[CONTEST_CUSTPROFILE_INDEX].Value == null ||
          dgv.Rows[i].Cells[CONTEST_CUSTPROFILE_INDEX].Value.ToString().ToUpper() != custProfile.Trim().ToUpper()) continue;
        gvR = dgv.Rows[i];
        contestFound = true;
        break;
      }

      var updatedContest = ActiveContests.FirstOrDefault(s => s.ContestNum == contestNum && s.ContestantNum == contestantNum &&
                                                                                       s.CustProfile.Trim().ToUpper() == custProfile.Trim().ToUpper());

      if (contestFound) {
        if (updatedContest == null) return;
        var updatedRows = BuildGridViewRowsForContest(gvR, updatedContest);

        if (updatedRows == null || updatedRows.Count <= 0) return;
        for (var i = 0; i < dgv.Rows.Count; i++) {
          var gvContestNumber = dgv.Rows[i].Cells[CONTESTNUM_INDEX].Value;
          var gvContestantNumber = dgv.Rows[i].Cells[CONTESTANTNUM_INDEX].Value.ToString();
          var grvCustProfile = gvR.Cells[CONTEST_CUSTPROFILE_INDEX].Value.ToString();
          if (gvContestNumber == null) continue;
          foreach (var row in from row in updatedRows
                              let updatedContestNum = row.Cells[CONTESTNUM_INDEX].Value
                              let updatedContestantNumber = row.Cells[CONTESTANTNUM_INDEX].Value
                              let updatedCustProfile = row.Cells[CONTEST_CUSTPROFILE_INDEX].Value
                              where updatedContestNum != null && updatedContestantNumber != null && updatedCustProfile != null
                              where int.Parse(gvContestNumber.ToString()) == int.Parse(updatedContestNum.ToString()) &&
                                                                                                                                                                                                                                                                                                           int.Parse(gvContestantNumber) == int.Parse(updatedContestantNumber.ToString()) &&
                                                grvCustProfile.Trim().ToUpper() == updatedCustProfile.ToString().Trim().ToUpper()
                              select row) {
            dgv.Rows[i].Cells[CONTEST_ROTATION_INDEX].Value = row.Cells[CONTEST_ROTATION_INDEX].Value;
            dgv.Rows[i].Cells[CONTESTANTNAME_INDEX].Value = row.Cells[CONTESTANTNAME_INDEX].Value;
            dgv.Rows[i].Cells[CONTESTANTODDS_INDEX].Value = row.Cells[CONTESTANTODDS_INDEX].Value;
            dgv.Rows[i].Cells[CONTESTANTNUM_INDEX].Value = row.Cells[CONTESTANTNUM_INDEX].Value;
            dgv.Rows[i].Cells[CONTEST_DATE_INDEX].Value = row.Cells[CONTEST_DATE_INDEX].Value;
            dgv.Rows[i].Cells[CONTEST_DESC_INDEX].Value = row.Cells[CONTEST_DESC_INDEX].Value;
            dgv.Rows[i].Cells[CONTEST_COMMENTS_INDEX].Value = row.Cells[CONTEST_COMMENTS_INDEX].Value;
            dgv.Rows[i].Cells[CONTESTNUM_INDEX].Value = row.Cells[CONTESTNUM_INDEX].Value;
            dgv.Rows[i].Cells[CONTEST_CUSTPROFILE_INDEX].Value = row.Cells[CONTEST_CUSTPROFILE_INDEX].Value;

            dgv.Rows[i].DefaultCellStyle.BackColor = row.DefaultCellStyle.BackColor;
            dgv.Rows[i].Height = row.Height;
            dgv.Rows[i].Visible = row.Visible;
            break;
          }
        }
      }
      else {
        if (selection == null || selection.Count <= 0 || updatedContest == null) return;
        switch (selection.Count) {
          case 1: //root contestType
            if (updatedContest.ContestType.Trim() == selection[0].LevelValue.Trim()) {
              AddNewContestToGridView(true, updatedContest);
            }
            break;
          case 2: //goes down to contestType2
            if (updatedContest.ContestType.Trim() == selection[0].LevelValue.Trim() &&
                            updatedContest.ContestType2.Trim() == selection[1].LevelValue.Trim()) {
              AddNewContestToGridView(true, updatedContest);
            }
            break;
        }
      }
    }

    private static List<spGLGetActiveGamesByCustomer_Result> GetSortedGames(IEnumerable<spGLGetActiveGamesByCustomer_Result> gamesAndLines) {
      return gamesAndLines.OrderBy(s => s.SportType).
        ThenBy(s => s.ScheduleDate).
        ThenBy(s => s.Team1RotNum).
        ThenBy(s => s.SportSubType).
        ThenBy(s => s.GameDateTime != null ? s.GameDateTime.Value.Date : new DateTime().Date).
        ThenBy(s => s.GameDateTime).
        ThenBy(s => s.ScheduleText).
        ThenBy(s => s.GameNum).
        ThenBy(s => s.PeriodNumber).
        ThenByDescending(s => s.LineSeq).
        ThenByDescending(s => s.CustProfile).ToList();
    }

    private void UpdateGamesGridView(List<SelectedSportBranch> selection, Boolean showingLatestLines, Boolean showingLikeTeams) {

      if (GamesGridViewInUse != null) {
        var gv = (DataGridView)Controls.Find(GamesGridViewInUse, true).FirstOrDefault();
        if (gv != null) {
          if (gv.Visible == false)
            return;
        }
      }
      else return;

      if (ActiveGames == null) return;

      if (!showingLatestLines && !showingLikeTeams) {
        FilteredActiveGames = !string.IsNullOrEmpty(selection[0].SportSubType) ?
          GetSortedGames(ActiveGames.Where(s => s.GameDateTime != null && (s.SportType != null && s.SportType.Trim() == selection[0].SportType) && (s.SportSubType != null && s.SportSubType.Trim() == selection[0].SportSubType)
                                 && (s.ScheduleDate >= selection[0].StartDateTime && s.ScheduleDate < selection[0].EndDateTime) && s.PeriodNumber == selection[0].PeriodNumber))
          :
          GetSortedGames(ActiveGames.Where(s => s.ScheduleDate != null && (s.SportType != null && s.SportType.Trim() == selection[0].SportType) && (s.ScheduleDate >= selection[0].StartDateTime && s.ScheduleDate < selection[0].EndDateTime) && s.PeriodNumber == selection[0].PeriodNumber));

      }
      else {
        var currentSelectedPeriod = GetCurrentSelectedGamePeriod();

        if (showingLatestLines && Customer.LastCallDateTime != null) {
          FilteredActiveGames = GetSortedGames(ActiveGames.Where(s => s.GameDateTime != null && (s.LastSpreadChange > Customer.LastCallDateTime || s.LastMoneyLineChange > Customer.LastCallDateTime || s.LastTeamPtsChange > Customer.LastCallDateTime || s.LastTtlPtsChange > Customer.LastCallDateTime) && s.PeriodNumber == currentSelectedPeriod && (s.Status != GamesAndLines.EVENT_OFFLINE && s.Status != GamesAndLines.EVENT_CANCELLED && s.Status != GamesAndLines.EVENT_COMPLETED)));
        }
        else {
          var txtTeamOrAsterisk = (TextBox)Mdi.Controls.Find("txtTeamOrAsterisk", true).FirstOrDefault();
          var likeClause = "";
          if (txtTeamOrAsterisk != null) {
            likeClause = txtTeamOrAsterisk.Text.Trim();
          }

          FilteredActiveGames = GetSortedGames(ActiveGames.Where(s => s.GameDateTime != null && s.PeriodNumber == currentSelectedPeriod && (s.Team1ID.ToUpper().StartsWith(likeClause.ToUpper()) || s.Team2ID.ToUpper().StartsWith(likeClause.ToUpper())) && (s.Status != GamesAndLines.EVENT_OFFLINE && s.Status != GamesAndLines.EVENT_CANCELLED && s.Status != GamesAndLines.EVENT_COMPLETED)));
        }
      }

      if (FilteredActiveGames == null || FilteredActiveGames.Count <= 0) return;
      var priceType = btnLineDisplayToggle.Text;


      foreach (var res in FilteredActiveGames) {
        var team1RotNum = res.Team1RotNum.ToString();
        var team1Id = res.Team1ID.Trim();
        var gameNumber = res.GameNum.ToString(CultureInfo.InvariantCulture).Trim();

        var spread = "Offline";
        var moneyLine = "";
        var totalPoints = "";
        var teamTotalsOver = "";
        var teamTotalsUnder = "";

        var drawRotNum = "";
        if (res.DrawRotNum != null) {
          drawRotNum = res.DrawRotNum.ToString();
        }
        const string team3Id = "Draw";
        var moneyLineDraw = "";

        var team2RotNum = res.Team2RotNum.ToString();
        var team2Id = res.Team2ID.Trim();
        var spread2 = "";
        var moneyLine2 = "";
        var totalPoints2 = "";
        var teamTotalsOver2 = "";
        var teamTotalsUnder2 = "";

        var custProfile = res.CustProfile.Trim();

        if (res.Status.Trim() != GamesAndLines.EVENT_OFFLINE) // not offline
                {
          var formattedWagerTypes = LineOffering.FormatWagerTypesOptionsDisplay(res, priceType, true, Customer.EasternLineFlag, res.SportType, _easternLineConversion);

          spread = formattedWagerTypes.Spread;
          moneyLine = RestrictMoneyLines.CheckForMlRestriction(formattedWagerTypes.MoneyLine1, res);
          totalPoints = formattedWagerTypes.TotalPoints;
          teamTotalsOver = formattedWagerTypes.TeamTotalsOver;
          teamTotalsUnder = formattedWagerTypes.TeamTotalsUnder;

          spread2 = formattedWagerTypes.Spread2;
          moneyLine2 = RestrictMoneyLines.CheckForMlRestriction(formattedWagerTypes.MoneyLine2, res);
          totalPoints2 = formattedWagerTypes.TotalPoints2;
          teamTotalsOver2 = formattedWagerTypes.TeamTotalsOver2;
          teamTotalsUnder2 = formattedWagerTypes.TeamTotalsUnder2;

          moneyLineDraw = res.MoneyLineStatus;
        }
        var dgv = (DataGridView)Controls.Find(GamesGridViewInUse, true).FirstOrDefault();
        var gameFound = UpdateGamesGridViewCells(dgv, gameNumber, team1RotNum, custProfile, res, spread, moneyLine, totalPoints, teamTotalsOver, teamTotalsUnder, team1Id, team2RotNum, spread2, moneyLine2, totalPoints2, teamTotalsOver2, teamTotalsUnder2, team2Id, drawRotNum, moneyLineDraw, team3Id);

        if (gameFound) continue;
        var scheduleDates = GetThisWeeksScheduleDates();
        AddNewGameToGridView(res, dgv, scheduleDates.ScheduleEndDate.GetValueOrDefault());
      }
    }

    private bool UpdateGamesGridViewCells(DataGridView dgv, string gameNumber, string team1RotNum, string custProfile, spGLGetActiveGamesByCustomer_Result res, string spread, string moneyLine, string totalPoints, string teamTotalsOver, string teamTotalsUnder, string team1Id, string team2RotNum, string spread2, string moneyLine2, string totalPoints2, string teamTotalsOver2, string teamTotalsUnder2, string team2Id, string drawRotNum, string moneyLineDraw, string team3Id) {
      var gameFound = false;
      if (dgv == null) return false;
      var rowCnt = dgv.Rows.Count;

      for (var i = 0; i < rowCnt; i++) {
        try {
          var gvwGameNumber = dgv.Rows[i].Cells[GAME_NUM_INDEX].Value.ToString();
          var gvwTeamRotNum = dgv.Rows[i].Cells[ROTATION_INDEX].Value.ToString();
          var gvwCustProfile = dgv.Rows[i].Cells[CUSTPROFILE_INDEX].Value.ToString();

          var gvwSpread = "";
          var gvwMoneyLine = "";
          var gvwTotalPoints = "";
          var gvwTeamToTalsO = "";
          var gvwTeamToTalsU = "";

          if (gameNumber == gvwGameNumber && gvwTeamRotNum == team1RotNum && custProfile == gvwCustProfile) {
            gameFound = true;

            dgv.Rows[i].DefaultCellStyle.BackColor = GetGridRowColor(res.Status,
              (DateTime)res.PeriodWagerCutoff,
              res.TimeChangeFlag,
              res.ScheduleDate,
              res.SportType,
              (DateTime)(GetThisWeeksScheduleDates()).ScheduleEndDate);

            if (res.Status.Trim() == GamesAndLines.EVENT_CIRCLED) {
              dgv.Rows[i].Cells[ROTATION_INDEX].Style.ForeColor = LOW_CIRCLED_GAME_COLOR; // Color.Red;
              dgv.Rows[i].Cells[GAMEDATE_INDEX].Style.ForeColor = LOW_CIRCLED_GAME_COLOR; // Color.Red;
              dgv.Rows[i].Cells[TEAMS_NAMES_INDEX].Style.ForeColor = LOW_CIRCLED_GAME_COLOR; // Color.Red;
            }
            else {
              dgv.Rows[i].Cells[ROTATION_INDEX].Style.ForeColor = BLACKGAMECOLOR; // Color.Black;
              dgv.Rows[i].Cells[GAMEDATE_INDEX].Style.ForeColor = BLACKGAMECOLOR; // Color.Black;
              dgv.Rows[i].Cells[TEAMS_NAMES_INDEX].Style.ForeColor = BLACKGAMECOLOR; // Color.Black;
            }

            if (dgv.Rows[i].Cells[SPREAD_INDEX].Value != null) {
              gvwSpread = dgv.Rows[i].Cells[SPREAD_INDEX].Value.ToString();
            }
            if (dgv.Rows[i].Cells[MONEY_INDEX].Value != null) {
              gvwMoneyLine = dgv.Rows[i].Cells[MONEY_INDEX].Value.ToString();
            }

            if (dgv.Rows[i].Cells[TOTAL_INDEX].Value != null) {
              gvwTotalPoints = dgv.Rows[i].Cells[TOTAL_INDEX].Value.ToString();
            }

            if (dgv.Rows[i].Cells[TEAM_TOTAL_OVER_INDEX].Value != null) {
              gvwTeamToTalsO = dgv.Rows[i].Cells[TEAM_TOTAL_OVER_INDEX].Value.ToString();
            }

            if (dgv.Rows[i].Cells[TEAM_TOTAL_UNDER_INDEX].Value != null) {
              gvwTeamToTalsU = dgv.Rows[i].Cells[TEAM_TOTAL_UNDER_INDEX].Value.ToString();
            }

            if (spread != gvwSpread) {
              dgv.Rows[i].Cells[SPREAD_INDEX].Value = spread;
              dgv.Rows[i].Cells[SPREAD_INDEX].Style.ForeColor = BLACKGAMECOLOR;
            }
            else {
              dgv.Rows[i].Cells[SPREAD_INDEX].Style.ForeColor = GetRowCellColor(SPREAD_INDEX, res);//GetCellColor0(res.LastSpreadChange);
            }

            if (moneyLine != gvwMoneyLine) {
              dgv.Rows[i].Cells[MONEY_INDEX].Value = moneyLine;
              dgv.Rows[i].Cells[MONEY_INDEX].Style.ForeColor = BLACKGAMECOLOR;
            }
            else {
              dgv.Rows[i].Cells[MONEY_INDEX].Style.ForeColor = GetRowCellColor(MONEY_INDEX, res);//GetCellColor0(res.LastMoneyLineChange);
            }

            if (totalPoints != gvwTotalPoints) {
              dgv.Rows[i].Cells[TOTAL_INDEX].Value = totalPoints;
              dgv.Rows[i].Cells[TOTAL_INDEX].Style.ForeColor = BLACKGAMECOLOR;
            }
            else {
              dgv.Rows[i].Cells[TOTAL_INDEX].Style.ForeColor = GetRowCellColor(TOTAL_INDEX, res);//GetCellColor0(res.LastTtlPtsChange);
            }

            if (teamTotalsOver != gvwTeamToTalsO) {
              dgv.Rows[i].Cells[TEAM_TOTAL_OVER_INDEX].Value = teamTotalsOver;
              dgv.Rows[i].Cells[TEAM_TOTAL_OVER_INDEX].Style.ForeColor = BLACKGAMECOLOR;
            }
            else {
              dgv.Rows[i].Cells[TEAM_TOTAL_OVER_INDEX].Style.ForeColor = GetRowCellColor(TEAM_TOTAL_OVER_INDEX, res);//GetCellColor0(res.LastTeamPtsChange);
            }

            if (teamTotalsUnder != gvwTeamToTalsU) {
              dgv.Rows[i].Cells[TEAM_TOTAL_UNDER_INDEX].Value = teamTotalsUnder;
              dgv.Rows[i].Cells[TEAM_TOTAL_UNDER_INDEX].Style.ForeColor = BLACKGAMECOLOR;
            }
            else {
              dgv.Rows[i].Cells[TEAM_TOTAL_UNDER_INDEX].Style.ForeColor = GetRowCellColor(TEAM_TOTAL_UNDER_INDEX, res);//GetCellColor0(res.LastTeamPtsChange);
            }

            if (team1Id == res.FavoredTeamID) {
              dgv.Rows[i].Cells[SPREAD_INDEX].Style.Font = new Font(dgvwGame.Font, FontStyle.Bold);
              dgv.Rows[i].Cells[MONEY_INDEX].Style.Font = new Font(dgvwGame.Font, FontStyle.Bold);
            }
          }

          if (gameNumber == gvwGameNumber && gvwTeamRotNum == team2RotNum && custProfile == gvwCustProfile) {
            gameFound = true;


            dgv.Rows[i].DefaultCellStyle.BackColor = GetGridRowColor(res.Status, res.PeriodWagerCutoff.GetValueOrDefault(), res.TimeChangeFlag, res.ScheduleDate, res.SportType, (GetThisWeeksScheduleDates()).ScheduleEndDate.GetValueOrDefault());

            if (res.Status.Trim() == GamesAndLines.EVENT_CIRCLED) {
              dgv.Rows[i].Cells[ROTATION_INDEX].Style.ForeColor = LOW_CIRCLED_GAME_COLOR;
              dgv.Rows[i].Cells[GAMEDATE_INDEX].Style.ForeColor = LOW_CIRCLED_GAME_COLOR;
              dgv.Rows[i].Cells[TEAMS_NAMES_INDEX].Style.ForeColor = LOW_CIRCLED_GAME_COLOR;
            }
            else {
              dgv.Rows[i].Cells[ROTATION_INDEX].Style.ForeColor = BLACKGAMECOLOR; // Color.Black;
              dgv.Rows[i].Cells[GAMEDATE_INDEX].Style.ForeColor = BLACKGAMECOLOR; // Color.Black;
              dgv.Rows[i].Cells[TEAMS_NAMES_INDEX].Style.ForeColor = BLACKGAMECOLOR; // Color.Black;
            }

            if (dgv.Rows[i].Cells[SPREAD_INDEX].Value != null) {
              gvwSpread = dgv.Rows[i].Cells[SPREAD_INDEX].Value.ToString();
            }

            if (dgv.Rows[i].Cells[4].Value != null) {
              gvwMoneyLine = dgv.Rows[i].Cells[4].Value.ToString();
            }
            if (dgv.Rows[i].Cells[5].Value != null) {
              gvwTotalPoints = dgv.Rows[i].Cells[5].Value.ToString();
            }

            if (dgv.Rows[i].Cells[6].Value != null) {
              gvwTeamToTalsO = dgv.Rows[i].Cells[6].Value.ToString();
            }

            if (dgv.Rows[i].Cells[7].Value != null) {
              gvwTeamToTalsU = dgv.Rows[i].Cells[7].Value.ToString();
            }

            if (spread2 != gvwSpread) {
              dgv.Rows[i].Cells[SPREAD_INDEX].Value = spread2;
              dgv.Rows[i].Cells[SPREAD_INDEX].Style.ForeColor = BLACKGAMECOLOR;
            }
            else {
              dgv.Rows[i].Cells[SPREAD_INDEX].Style.ForeColor = GetRowCellColor(SPREAD_INDEX, res);//GetCellColor0(res.LastSpreadChange);
            }

            if (moneyLine2 != gvwMoneyLine) {
              dgv.Rows[i].Cells[MONEY_INDEX].Value = moneyLine2;
              dgv.Rows[i].Cells[MONEY_INDEX].Style.ForeColor = BLACKGAMECOLOR;
            }
            else {
              dgv.Rows[i].Cells[MONEY_INDEX].Style.ForeColor = GetRowCellColor(MONEY_INDEX, res);//GetCellColor0(res.LastMoneyLineChange);
            }

            if (totalPoints2 != gvwTotalPoints) {
              dgv.Rows[i].Cells[TOTAL_INDEX].Value = totalPoints2;
              dgv.Rows[i].Cells[TOTAL_INDEX].Style.ForeColor = BLACKGAMECOLOR;
            }
            else {
              dgv.Rows[i].Cells[TOTAL_INDEX].Style.ForeColor = GetRowCellColor(TOTAL_INDEX, res);//GetCellColor0(res.LastTtlPtsChange);
            }

            if (teamTotalsOver2 != gvwTeamToTalsO) {
              dgv.Rows[i].Cells[TEAM_TOTAL_OVER_INDEX].Value = teamTotalsOver2;
              dgv.Rows[i].Cells[TEAM_TOTAL_OVER_INDEX].Style.ForeColor = BLACKGAMECOLOR;
            }
            else {
              dgv.Rows[i].Cells[TEAM_TOTAL_OVER_INDEX].Style.ForeColor = GetRowCellColor(TEAM_TOTAL_OVER_INDEX, res);//GetCellColor0(res.LastTeamPtsChange);
            }

            if (teamTotalsUnder2 != gvwTeamToTalsU) {
              dgv.Rows[i].Cells[TEAM_TOTAL_UNDER_INDEX].Value = teamTotalsUnder2;
              dgv.Rows[i].Cells[TEAM_TOTAL_UNDER_INDEX].Style.ForeColor = BLACKGAMECOLOR;
            }
            else {
              dgv.Rows[i].Cells[TEAM_TOTAL_UNDER_INDEX].Style.ForeColor = GetRowCellColor(TEAM_TOTAL_UNDER_INDEX, res);//GetCellColor0(res.LastTeamPtsChange);
            }

            if (team2Id == res.FavoredTeamID) {
              dgv.Rows[i].Cells[SPREAD_INDEX].Style.Font = new Font(dgvwGame.Font, FontStyle.Bold);
              dgv.Rows[i].Cells[MONEY_INDEX].Style.Font = new Font(dgvwGame.Font, FontStyle.Bold);
            }
          }

          if (gameNumber == gvwGameNumber && gvwTeamRotNum == drawRotNum && drawRotNum != "0" && custProfile == gvwCustProfile) {
            gameFound = true;


            dgv.Rows[i].DefaultCellStyle.BackColor = GetGridRowColor(res.Status, res.PeriodWagerCutoff.GetValueOrDefault(), res.TimeChangeFlag, res.ScheduleDate, res.SportType, (GetThisWeeksScheduleDates()).ScheduleEndDate.GetValueOrDefault());

            if (res.Status.Trim() == GamesAndLines.EVENT_CIRCLED) {
              dgv.Rows[i].Cells[ROTATION_INDEX].Style.ForeColor = LOW_CIRCLED_GAME_COLOR;
              dgv.Rows[i].Cells[GAMEDATE_INDEX].Style.ForeColor = LOW_CIRCLED_GAME_COLOR;
              dgv.Rows[i].Cells[TEAMS_NAMES_INDEX].Style.ForeColor = LOW_CIRCLED_GAME_COLOR;
            }
            else {
              dgv.Rows[i].Cells[ROTATION_INDEX].Style.ForeColor = BLACKGAMECOLOR; // Color.Black;
              dgv.Rows[i].Cells[GAMEDATE_INDEX].Style.ForeColor = BLACKGAMECOLOR; // Color.Black;
              dgv.Rows[i].Cells[TEAMS_NAMES_INDEX].Style.ForeColor = BLACKGAMECOLOR; // Color.Black;
            }

            if (dgv.Rows[i].Cells[MONEY_INDEX].Value != null) {
              gvwMoneyLine = dgv.Rows[i].Cells[MONEY_INDEX].Value.ToString();
            }

            if (moneyLineDraw != gvwMoneyLine) {
              dgv.Rows[i].Cells[MONEY_INDEX].Value = moneyLineDraw;
              dgv.Rows[i].Cells[MONEY_INDEX].Style.ForeColor = BLACKGAMECOLOR;
            }
            else {
              dgv.Rows[i].Cells[MONEY_INDEX].Style.ForeColor = GetRowCellColor(MONEY_INDEX, res);//GetCellColor0(res.LastMoneyLineChange);
            }

            if (team3Id == res.FavoredTeamID) {
              dgv.Rows[i].Cells[SPREAD_INDEX].Style.Font = new Font(dgvwGame.Font, FontStyle.Bold);
              dgv.Rows[i].Cells[MONEY_INDEX].Style.Font = new Font(dgvwGame.Font, FontStyle.Bold);
            }
          }

          if (gvwTeamRotNum != "") continue;
          dgv.Rows[i].DefaultCellStyle.BackColor = GRAYGAMECOLOR;
          dgv.Rows[i].Height = 1;
        }
        catch (Exception ex) {
          Log(ex);
        }
      }
      return gameFound;
    }

    private void UpdateSpecificGameInGridView(List<SelectedSportBranch> sportsTreeSelection, int? gameNum, String custProfile) {
      if (GamesGridViewInUse == null) return;
      var gv = (DataGridView)Controls.Find(GamesGridViewInUse, true).FirstOrDefault();
      if (gv != null && gv.Rows.Count > 0) {
        if (gv.Visible == false)
          return;
      }

      if (gameNum == null)
        return;

      DataGridViewRow gvR = null;
      var gamePeriodNumber = -1;
      var gameFound = false;
      var hideGameInGrid = false;

      var activeGameScheduleDate = (from a in ActiveGames
                                    where a.GameNum == gameNum && a.CustProfile.Trim().ToUpper() == custProfile.Trim().ToUpper()
                                    select a.ScheduleDate).FirstOrDefault();
      if (gv == null) return;
      for (var i = 0; i < gv.Rows.Count; i++) {
        if (gv.Rows[i].Cells[GAME_NUM_INDEX].Value == null || gameNum != int.Parse(gv.Rows[i].Cells[GAME_NUM_INDEX].Value.ToString()) ||
          gv.Rows[i].Cells[CUSTPROFILE_INDEX].Value == null ||
          gv.Rows[i].Cells[CUSTPROFILE_INDEX].Value.ToString().ToUpper() != custProfile.Trim().ToUpper()) continue;
        gvR = gv.Rows[i];
        if (gvR.Cells[PERIOD_NUM_INDEX].Value != null)
          gamePeriodNumber = int.Parse((gvR.Cells[PERIOD_NUM_INDEX].Value.ToString()));
        if (gvR.Cells[CUSTPROFILE_INDEX].Value != null)
          gameFound = true;
        if (gvR.Cells[SCHEDULEDATE_INDEX].Value != null && activeGameScheduleDate != null && DateTime.Parse(gvR.Cells[SCHEDULEDATE_INDEX].Value.ToString()) != activeGameScheduleDate) {
          hideGameInGrid = true;
        }
        break;
      }

      var updatedGamePeriod = ActiveGames.FirstOrDefault(s => s.GameNum == gameNum && s.PeriodNumber == gamePeriodNumber &&
                                                                                    s.CustProfile.Trim().ToUpper() == custProfile.Trim().ToUpper());
      var scheduleEndDate = GetThisWeeksScheduleDates().ScheduleEndDate.GetValueOrDefault();
      if (gameFound && gamePeriodNumber > -1) {
        List<DataGridViewRow> updatedRows;

        if (updatedGamePeriod != null && !hideGameInGrid) {
          updatedRows = BuildGridViewRowsForGame(gvR, updatedGamePeriod, scheduleEndDate);
        }
        else {
          for (var i = 0; i < gv.Rows.Count; i++) {
            if (gv.Rows[i].Cells[GAME_NUM_INDEX].Value != null && gameNum == int.Parse(gv.Rows[i].Cells[GAME_NUM_INDEX].Value.ToString()) && gv.Rows[i].Cells[CUSTPROFILE_INDEX].Value != null
                          && gv.Rows[i].Cells[CUSTPROFILE_INDEX].Value.ToString().ToUpper() == custProfile.Trim().ToUpper()) {
              gv.Rows[i].Visible = false;
            }
          }
          return;
        }

        if (updatedRows == null || updatedRows.Count <= 0) return;
        for (var i = 0; i < gv.Rows.Count; i++) {
          var gvGameNumber = gv.Rows[i].Cells[GAME_NUM_INDEX].Value;
          var gvPositionInGroup = gv.Rows[i].Cells[ROWGROUPNUM_INDEX].Value;
          var gvTeamRotNum = gv.Rows[i].Cells[ROTATION_INDEX].Value.ToString();
          var grvCustProfile = gvR.Cells[CUSTPROFILE_INDEX].Value.ToString();

          if (gvGameNumber == null || gvPositionInGroup == null) continue;
          foreach (var row in updatedRows) {
            var updatedTeamRotNum = row.Cells[ROTATION_INDEX].Value;
            var updatedGameNumber = row.Cells[GAME_NUM_INDEX].Value;
            var updatedPositionInGroup = row.Cells[ROWGROUPNUM_INDEX].Value;
            var updatedCustProfile = row.Cells[CUSTPROFILE_INDEX].Value;
            if (updatedGameNumber == null || updatedPositionInGroup == null || updatedTeamRotNum == null ||
                updatedCustProfile == null) continue;
            if (int.Parse(gvGameNumber.ToString()) != int.Parse(updatedGameNumber.ToString()) ||
                int.Parse(gvPositionInGroup.ToString()) != int.Parse(updatedPositionInGroup.ToString()) ||
                gvTeamRotNum.Trim() != updatedTeamRotNum.ToString().Trim() ||
                grvCustProfile.Trim().ToUpper() != updatedCustProfile.ToString().Trim().ToUpper()) continue;
            gv.Rows[i].Cells[ROTATION_INDEX].Value = row.Cells[ROTATION_INDEX].Value;
            gv.Rows[i].Cells[GAMEDATE_INDEX].Value = row.Cells[GAMEDATE_INDEX].Value;
            gv.Rows[i].Cells[TEAMS_NAMES_INDEX].Value = row.Cells[TEAMS_NAMES_INDEX].Value;
            gv.Rows[i].Cells[SPREAD_INDEX].Value = row.Cells[SPREAD_INDEX].Value;
            gv.Rows[i].Cells[MONEY_INDEX].Value = row.Cells[MONEY_INDEX].Value;
            gv.Rows[i].Cells[TOTAL_INDEX].Value = row.Cells[TOTAL_INDEX].Value;
            gv.Rows[i].Cells[TEAM_TOTAL_OVER_INDEX].Value = row.Cells[TEAM_TOTAL_OVER_INDEX].Value;
            gv.Rows[i].Cells[TEAM_TOTAL_UNDER_INDEX].Value = row.Cells[TEAM_TOTAL_UNDER_INDEX].Value;
            gv.Rows[i].Cells[GAME_NUM_INDEX].Value = row.Cells[GAME_NUM_INDEX].Value;
            gv.Rows[i].Cells[PERIOD_NUM_INDEX].Value = row.Cells[PERIOD_NUM_INDEX].Value;
            gv.Rows[i].Cells[ROWGROUPNUM_INDEX].Value = row.Cells[ROWGROUPNUM_INDEX].Value;
            gv.Rows[i].Cells[TBD11_INDEX].Value = row.Cells[TBD11_INDEX].Value;
            gv.Rows[i].Cells[TBD12_INDEX].Value = row.Cells[TBD12_INDEX].Value;
            gv.Rows[i].Cells[TBD13_INDEX].Value = row.Cells[TBD13_INDEX].Value;
            gv.Rows[i].Cells[TBD14_INDEX].Value = row.Cells[TBD14_INDEX].Value;
            gv.Rows[i].Cells[TBD15_INDEX].Value = row.Cells[TBD15_INDEX].Value;
            gv.Rows[i].Cells[SPORT_INDEX].Value = row.Cells[SPORT_INDEX].Value;
            gv.Rows[i].Cells[SUBSPORT_INDEX].Value = row.Cells[SUBSPORT_INDEX].Value;
            gv.Rows[i].Cells[PERIOD_CUTOFF_INDEX].Value = row.Cells[PERIOD_CUTOFF_INDEX].Value;
            gv.Rows[i].Cells[GAME_STATUS_INDEX].Value = row.Cells[GAME_STATUS_INDEX].Value;
            gv.Rows[i].Cells[TEAM1_NAME_INDEX].Value = row.Cells[TEAM1_NAME_INDEX].Value;
            gv.Rows[i].Cells[CUSTPROFILE_INDEX].Value = row.Cells[CUSTPROFILE_INDEX].Value;

            gv.Rows[i].DefaultCellStyle.BackColor = row.DefaultCellStyle.BackColor;
            gv.Rows[i].Cells[ROTATION_INDEX].Style.ForeColor = row.Cells[ROTATION_INDEX].Style.ForeColor;
            gv.Rows[i].Cells[GAMEDATE_INDEX].Style.ForeColor = row.Cells[GAMEDATE_INDEX].Style.ForeColor;
            gv.Rows[i].Cells[TEAMS_NAMES_INDEX].Style.ForeColor = row.Cells[TEAMS_NAMES_INDEX].Style.ForeColor;
            gv.Rows[i].Cells[SPREAD_INDEX].Style.ForeColor = row.Cells[SPREAD_INDEX].Style.ForeColor;
            gv.Rows[i].Cells[SPREAD_INDEX].Style.Font = row.Cells[SPREAD_INDEX].Style.Font;

            gv.Rows[i].Cells[MONEY_INDEX].Style.ForeColor = row.Cells[MONEY_INDEX].Style.ForeColor;
            gv.Rows[i].Cells[TOTAL_INDEX].Style.ForeColor = row.Cells[TOTAL_INDEX].Style.ForeColor;
            gv.Rows[i].Cells[TEAM_TOTAL_OVER_INDEX].Style.ForeColor = row.Cells[TEAM_TOTAL_OVER_INDEX].Style.ForeColor;
            gv.Rows[i].Cells[TEAM_TOTAL_UNDER_INDEX].Style.ForeColor = row.Cells[TEAM_TOTAL_UNDER_INDEX].Style.ForeColor;

            gv.Rows[i].Height = row.Height;
            gv.Rows[i].Visible = row.Visible;
            break;
          }
        }
      }
      else {
        if (gameFound || updatedGamePeriod == null) return;
        var currentSelectedPeriod = GetCurrentSelectedGamePeriod();

        if (Mdi.ShowingLikeTeams) {
          var txtTeamOrAsterisk = (TextBox)Mdi.Controls.Find("txtTeamOrAsterisk", true).FirstOrDefault();
          var likeClause = "";
          if (txtTeamOrAsterisk != null) {
            likeClause = txtTeamOrAsterisk.Text.Trim();
          }

          updatedGamePeriod = ActiveGames.FirstOrDefault(s => s.GameDateTime != null && s.GameNum == gameNum && s.PeriodNumber == currentSelectedPeriod &&
                                                              (s.Team1ID.ToUpper().StartsWith(likeClause.ToUpper()) || s.Team2ID.ToUpper().StartsWith(likeClause.ToUpper())) &&
                                                              (s.Status != GamesAndLines.EVENT_OFFLINE && s.Status != GamesAndLines.EVENT_CANCELLED && s.Status != GamesAndLines.EVENT_COMPLETED) && s.CustProfile.Trim().ToUpper() == custProfile.Trim().ToUpper());
          if (updatedGamePeriod != null) {
            AddNewGameToGridView(updatedGamePeriod, gv, scheduleEndDate);
          }
        }
        else {
          if (Mdi.ShowingLatestLines && Customer.LastCallDateTime != null) {
            updatedGamePeriod = ActiveGames.FirstOrDefault(s => s.GameDateTime != null && s.GameNum == gameNum && s.PeriodNumber == currentSelectedPeriod && (s.LastSpreadChange > Customer.LastCallDateTime || s.LastMoneyLineChange > Customer.LastCallDateTime || s.LastTeamPtsChange > Customer.LastCallDateTime || s.LastTtlPtsChange > Customer.LastCallDateTime) && (s.Status != GamesAndLines.EVENT_OFFLINE && s.Status != GamesAndLines.EVENT_CANCELLED && s.Status != GamesAndLines.EVENT_COMPLETED));
            if (updatedGamePeriod != null) {
              AddNewGameToGridView(updatedGamePeriod, gv, scheduleEndDate);
            }
          }
          else
            if (sportsTreeSelection != null) {
              var selectedPeriodNumber = sportsTreeSelection[0].PeriodNumber;
              updatedGamePeriod = ActiveGames.FirstOrDefault(s => s.GameNum == gameNum && s.PeriodNumber == selectedPeriodNumber && (s.Status != GamesAndLines.EVENT_OFFLINE && s.Status != GamesAndLines.EVENT_CANCELLED && s.Status != GamesAndLines.EVENT_COMPLETED));
              if (updatedGamePeriod != null) {
                var sportType = updatedGamePeriod.SportType;
                var sportSubType = updatedGamePeriod.SportSubType;

                var scheduleDate = updatedGamePeriod.ScheduleDate;

                if (!(scheduleDate >= sportsTreeSelection[0].StartDateTime) ||
                    !(scheduleDate < sportsTreeSelection[0].EndDateTime)) return;
                if (!string.IsNullOrEmpty(sportsTreeSelection[0].SportSubType) && sportsTreeSelection[0].SportSubType.Trim() == sportSubType.Trim()) {
                  if (!string.IsNullOrEmpty(sportsTreeSelection[0].SportType) && sportsTreeSelection[0].SportType.Trim() == sportType.Trim()) {
                    AddNewGameToGridView(updatedGamePeriod, gv, scheduleEndDate);
                  }
                }
                else {
                  if (!string.IsNullOrEmpty(sportsTreeSelection[0].SportType) && sportsTreeSelection[0].SportType.Trim() == sportType.Trim()) {
                    AddNewGameToGridView(updatedGamePeriod, gv, scheduleEndDate);
                  }
                }
              }
            }
        }
      }
    }

    private int GetCurrentSelectedGamePeriod() {
      return (from Control ctrl in panGamePeriodSelection.Controls where ctrl.GetType().ToString() == "System.Windows.Forms.RadioButton" let rad = (RadioButton)ctrl where rad.Checked select ctrl.Name).Select(radText => int.Parse(radText.Split('_')[1])).FirstOrDefault();
    }

    private void UpdateWagerItemReadbackLogInfo(Ticket.WagerItem rbItemLog) {
      Mdi.Ticket.UpdateWagerItem(rbItemLog);
    }

    private void WriteDgvwGameHeader(DataGridView caller) {

      dgvwGame.Visible = false;
      GamesGridViewInUse = caller.Name;
      PropsGridViewInUse = dgvwPropsAndContests.Name;
      var dgrvwColNames = (from DataGridViewTextBoxColumn col in caller.Columns select col.Name).ToList();

      foreach (var str in dgrvwColNames) {
        caller.Columns.Remove(str);
      }

      var titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = Resources.FrmSportAndGameSelection_WriteDgvwGameHeader___,
        Width = 50
      };
      titleColumn.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
      titleColumn.DisplayIndex = 0;
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = Resources.FrmSportAndGameSelection_WriteDgvwGameHeader_Date
      };
      var style = new DataGridViewCellStyle { Alignment = DataGridViewContentAlignment.MiddleLeft };
      titleColumn.DefaultCellStyle = style;
      titleColumn.Width = 110;
      titleColumn.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
      titleColumn.DisplayIndex = 1;
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = Resources.FrmSportAndGameSelection_WriteDgvwGameHeader_Team,
        Width = 158,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleLeft },
        DisplayIndex = 2
      };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = Resources.FrmSportAndGameSelection_WriteDgvwGameHeader_Spread,
        Width = 96,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleLeft },
        DisplayIndex = 3
      };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = Resources.FrmSportAndGameSelection_WriteDgvwGameHeader_ML,
        Width = 49,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleLeft },
        DisplayIndex = 4
      };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = Resources.FrmSportAndGameSelection_WriteDgvwGameHeader_Ttl_Pnts,
        Width = 79,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleLeft },
        DisplayIndex = 5
      };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = Resources.FrmSportAndGameSelection_WriteDgvwGameHeader_Tt_Over,
        Width = 79,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleLeft },
        DisplayIndex = 6
      };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = Resources.FrmSportAndGameSelection_WriteDgvwGameHeader_Tt_Under,
        Width = 79,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleLeft },
        DisplayIndex = 7
      };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = Resources.FrmSportAndGameSelection_WriteDgvwGameHeader_Game_Number,
        Width = 20,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleLeft },
        Visible = false,
        DisplayIndex = 8
      };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = Resources.FrmSportAndGameSelection_WriteDgvwGameHeader_Period_Number,
        Width = 10,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleLeft },
        Visible = false,
        DisplayIndex = 9
      };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = Resources.FrmSportAndGameSelection_WriteDgvwGameHeader_Row_Group_Number,
        Width = 10,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleLeft },
        Visible = false,
        DisplayIndex = 10
      };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = Resources.FrmSportAndGameSelection_WriteDgvwGameHeader_Extra1,
        Width = 10,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleLeft },
        Visible = false,
        DisplayIndex = 11
      };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = Resources.FrmSportAndGameSelection_WriteDgvwGameHeader_Extra2,
        Width = 10,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleLeft },
        Visible = false,
        DisplayIndex = 12
      };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = Resources.FrmSportAndGameSelection_WriteDgvwGameHeader_Extra3,
        Width = 10,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleLeft },
        Visible = false,
        DisplayIndex = 13
      };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = Resources.FrmSportAndGameSelection_WriteDgvwGameHeader_Extra4,
        Width = 10,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleLeft },
        Visible = false,
        DisplayIndex = 14
      };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = Resources.FrmSportAndGameSelection_WriteDgvwGameHeader_Extra5,
        Width = 10,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleLeft },
        Visible = false,
        DisplayIndex = 15
      };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = Resources.FrmSportAndGameSelection_WriteDgvwGameHeader_Sport_Type,
        Width = 10,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleLeft },
        Visible = false,
        DisplayIndex = 16
      };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        HeaderText = Resources.FrmSportAndGameSelection_WriteDgvwGameHeader_Sport_Sub_Type,
        Width = 10,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleLeft },
        Visible = false,
        DisplayIndex = 17
      };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        Name = "PeriodWagerCutoff",
        HeaderText = Resources.FrmSportAndGameSelection_WriteDgvwGameHeader_Period_Wager_Cutoff,
        Width = 10,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleLeft },
        Visible = false,
        DisplayIndex = 18
      };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        Name = Resources.FrmSportAndGameSelection_WriteDgvwGameHeader_GameStatus,
        HeaderText = Resources.FrmSportAndGameSelection_WriteDgvwGameHeader_GameStatus,
        Width = 10,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleLeft },
        Visible = false,
        DisplayIndex = 19
      };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        Name = "TeamId",
        HeaderText = @"Team Id",
        Width = 10,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleLeft },
        Visible = false,
        DisplayIndex = 20
      };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        Name = "CustProfile",
        HeaderText = @"Cust Profile",
        Width = 20,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleLeft },
        Visible = false,
        DisplayIndex = 21
      };
      caller.Columns.Add(titleColumn);

      titleColumn = new DataGridViewTextBoxColumn {
        Name = "ScheduleDate",
        HeaderText = @"Schedule Date",
        Width = 20,
        DefaultCellStyle = { Alignment = DataGridViewContentAlignment.MiddleLeft },
        Visible = false,
        DisplayIndex = 22
      };
      caller.Columns.Add(titleColumn);

    }

    private void SendWagerPermissionRequest(spGLGetActiveGamesByCustomer_Result selectedGamePeriodInfo, DateTime? cutoffTime, String wagerItemDescription, String amtWagered, String toWinAmount, String requestTypeDesc, out Boolean wagerCutoffByPassed) {
      const string baseMessage = "The game period has already started or is not currently available.";
      var frm = new FrmLateWagerRequest(this, selectedGamePeriodInfo, Customer, wagerItemDescription, amtWagered, toWinAmount) {
        Owner = this,
        Text = requestTypeDesc,
        Icon = Icon
      };
      SendNotificationMinutesAfterCutoff(frm, cutoffTime, baseMessage, out wagerCutoffByPassed);
    }

    public double GetLimitForCurrentWager(spGLGetActiveGamesByCustomer_Result gameInfo, String wagerType, string subWagerType) {
      var args = new WagerLimitValidator.ValidatorArgs {
        CQL = Customer.ActiveWagerLimit ?? 0,
        CustomerId = Customer.CustomerID,
        MaxContestBet = Customer.ContestMaxBet ?? 0,
        CurrencyCode = Customer.Currency,
        CPL = Customer.ParlayMaxBet ?? 0,
        CTL = Customer.TeaserMaxBet ?? 0,
        SYPL = AppModuleInfo.Parameters.MaxParlayBet,
        SYTL = AppModuleInfo.Parameters.MaxTeaserBet,
        Store = _currentCustomerStore,
        CEPTL = wagerType == "P" ? Customer.EnforceParlayMaxBetFlag == "Y" : (wagerType == "T" && Customer.EnforceTeaserMaxBetFlag == "Y"),
        OfferHighLimitsActive = Mdi.CustomerRestrictions.Any(r => r.Code == "HIGHLIMITS")
      };
      var validator = new WagerLimitValidator(AppModuleInfo, args, WagerLimitValidator.LimitsSourceList.Cu);
      validator.GetMaximumWagerLimit(gameInfo, wagerType, subWagerType);
      MaxWagerLimitTypeCode = validator.WagerLimitType;
      return validator.MaxWagerLimit ?? 0;
    }

    private int GetOriginalMoneyLine(spGLGetActiveGamesByCustomer_Result gameInfo, String wagerType, String chosenTeamId, String totalPointsOu) {
      var moneyLine = 0;
      var chosenTeamIdx = TwUtilities.GetChosenTeamIdIdx(gameInfo, chosenTeamId);
      switch (wagerType) {
        case WagerType.SPREAD:
          switch (chosenTeamIdx) {
            case 1:
              if (gameInfo.SpreadAdj1 == null)
                moneyLine = 0;
              else
                moneyLine = (int)gameInfo.SpreadAdj1;
              break;
            case 2:
              if (gameInfo.SpreadAdj2 == null)
                moneyLine = 0;
              else
                moneyLine = (int)gameInfo.SpreadAdj2;
              break;
          }
          break;
        case WagerType.MONEYLINE:
          switch (chosenTeamIdx) {
            case 1:
              if (gameInfo.MoneyLine1 == null)
                moneyLine = 0;
              else
                moneyLine = (int)gameInfo.MoneyLine1;
              break;
            case 2:
              if (gameInfo.MoneyLine2 == null)
                moneyLine = 0;
              else
                moneyLine = (int)gameInfo.MoneyLine2;
              break;
            case 3:
              if (gameInfo.MoneyLineDraw == null)
                moneyLine = 0;
              else
                moneyLine = (int)gameInfo.MoneyLineDraw;
              break;
          }
          break;
        case WagerType.TOTALPOINTS:
          switch (chosenTeamIdx) {
            case 1:
              if (gameInfo.TtlPtsAdj1 == null)
                moneyLine = 0;
              else
                moneyLine = (int)gameInfo.TtlPtsAdj1;
              break;
            case 2:
              if (gameInfo.TtlPtsAdj2 == null)
                moneyLine = 0;
              else
                moneyLine = (int)gameInfo.TtlPtsAdj2;
              break;
          }
          break;
        case WagerType.TEAMTOTALPOINTS:
          switch (chosenTeamIdx) {
            case 1:
              if (totalPointsOu == "O") {
                if (gameInfo.Team1TtlPtsAdj1 == null)
                  moneyLine = 0;
                else
                  moneyLine = (int)gameInfo.Team1TtlPtsAdj1;
              }
              else {
                if (gameInfo.Team1TtlPtsAdj2 == null)
                  moneyLine = 0;
                else
                  moneyLine = (int)gameInfo.Team1TtlPtsAdj2;
              }
              break;
            case 2:
              if (totalPointsOu == "O") {
                if (gameInfo.Team2TtlPtsAdj1 == null)
                  moneyLine = 0;
                else
                  moneyLine = (int)gameInfo.Team2TtlPtsAdj1;
              }
              else {
                if (gameInfo.Team2TtlPtsAdj2 == null)
                  moneyLine = 0;
                else
                  moneyLine = (int)gameInfo.Team2TtlPtsAdj2;
              }
              break;
          }
          break;
      }
      return moneyLine;
    }

    private bool IsBelowMinimumWager(String amountWagered, out int minWagerAmt) {
      double amountToCheck = 0;
      minWagerAmt = 0;

      try {
        if (!string.IsNullOrEmpty(amountWagered)) {
          amountToCheck = double.Parse(amountWagered);
        }
        using (var customerService = new Customers(AppModuleInfo)) {
          minWagerAmt = customerService.GetCustomerMinCuWagerAmount(CurrentCustomerId) ?? 0;
        }
      }
      catch (ObjectDisposedException) {
        using (var cst = new Customers(AppModuleInfo)) {
          minWagerAmt = cst.GetCustomerMinCuWagerAmount(CurrentCustomerId) ?? 0;
        }
      }
      catch (Exception ex) {
        Log(ex);
      }
      return amountToCheck == 0 || ((minWagerAmt / 100) > amountToCheck);
    }

    public void MoveToNextPeriodOption(string arrowUsed) {
      var radIdx = 0;

      var enabledRadList = panGamePeriodSelection.Controls.Cast<RadioButton>().Where(rad => rad.Enabled).ToList();

      if (enabledRadList.Count == 0)
        return;

      var minRadIdx = (from r in enabledRadList select r.TabIndex).Min();
      var maxRadIdx = (from r in enabledRadList select r.TabIndex).Max();

      foreach (var radBtns in enabledRadList) {
        if (radBtns.TabIndex < minRadIdx)
          minRadIdx = radBtns.TabIndex;

        if (!radBtns.Checked) continue;
        radIdx = radBtns.TabIndex;
        break;
      }

      int newTabIndex;

      if (arrowUsed == "Right") {
        newTabIndex = radIdx + 1;
        while (true) {
          if (TwUtilities.ElementExistsInList(enabledRadList, newTabIndex)) {
            break;
          }
          if (newTabIndex > maxRadIdx)
            newTabIndex = minRadIdx - 1;
          newTabIndex++;
        }
      }
      else {
        newTabIndex = radIdx - 1;
        while (true) {
          if (TwUtilities.ElementExistsInList(enabledRadList, newTabIndex)) {
            break;
          }
          if (newTabIndex < minRadIdx)
            newTabIndex = maxRadIdx + 1;
          newTabIndex--;
        }
      }


      var newRadName = "";

      foreach (var rad in enabledRadList.Where(rad => rad.TabIndex == newTabIndex)) {
        newRadName = rad.Name;
        break;
      }

      var radBtn = (RadioButton)Controls.Find(newRadName, true).FirstOrDefault();

      if (radBtn != null) {
        radBtn.Focus();

        DataGridView shownGridView;
        var dgv = (DataGridView)Controls.Find(GamesGridViewInUse, true).FirstOrDefault();
        if (dgv != null && dgv.Visible) {
          shownGridView = dgv;
        }
        else {
          shownGridView = dgvwPropsAndContests;
        }
        shownGridView.Focus();
      }
    }

    private bool RefreshingAvailableSportsTreeIsRequired(spGLGetLastLineChangeByCustomer_Result lineChange) {

      if (lineChange.PeriodWagerCutoff == null || lineChange.WagerType == WagerType.CONTEST)
        return false;

      if (_sportsAndScheduleDates == null)
        return true;

      var refreshIsRequired = false;
      var itemFound = false;
      var sportType = (from a in ActiveGames where a.GameNum == lineChange.GameNum select a.SportType.Trim()).FirstOrDefault();
      var sportSubType = (from a in ActiveGames where a.GameNum == lineChange.GameNum select a.SportSubType.Trim()).FirstOrDefault();
      var scheduleDate = (from a in ActiveGames where a.GameNum == lineChange.GameNum select a.ScheduleDate).FirstOrDefault();

      if (sportType == null || sportSubType == null || scheduleDate == null) return false;
      foreach (var sportItem in _sportsAndScheduleDates) {
        if (sportType == sportItem.SportType && sportSubType == sportItem.SportSubType && scheduleDate == sportItem.ScheduleDate)
          itemFound = true;
      }
      if (!itemFound) {
        refreshIsRequired = true;
      }
      else {
        GroupSportsByScheduleDate();
        RemoveNonNeededNodes();
      }

      return refreshIsRequired;
    }

    private string[] GetTreeNodeFullPath(TreeNode node) {
      try {
        if (node == null || string.IsNullOrEmpty(node.FullPath)) return new string[] { };
        return node.FullPath.Split(new[] { treSports.PathSeparator }, StringSplitOptions.RemoveEmptyEntries);
      }
      catch { return new string[] { }; } //no log needed. Miscelaneous action.
    }

    private void RemoveNonNeededNodes() {
      if (_sportsAndScheduleDates == null)
        return;

      foreach (TreeNode rootNode in treSports.Nodes) //sport Node
            {
        if (rootNode == null || ((BetOptionNode)rootNode).NodeType != "G") continue;
        var nodeItemPath = GetTreeNodeFullPath(rootNode);
        if (nodeItemPath.Length == 0) continue;
        var sportsCount = (from s in _sportsAndScheduleDates where s.SportType.Trim() == nodeItemPath[0].Trim() select s).Count();
        if (sportsCount == 0)
          rootNode.Remove();
        else {
          foreach (TreeNode firstChildNode in rootNode.Nodes) //ScheduleDate Node
                    {
            nodeItemPath = GetTreeNodeFullPath(firstChildNode);
            if (nodeItemPath.Length == 0) continue;
            var schedulesCount = (from s in _sportsAndScheduleDates
                                  where s.SportType.Trim() == nodeItemPath[0].Trim() &&
                                       s.ScheduleDate == ((BetOptionNode)firstChildNode).ScheduleDate
                                  select s).Count();
            if (schedulesCount == 0)
              firstChildNode.Remove();
            else {
              foreach (TreeNode secondChildNode in firstChildNode.Nodes) //SportSubTypes Node
                            {
                nodeItemPath = GetTreeNodeFullPath(secondChildNode);
                if (nodeItemPath.Length == 0) continue;
                var sportSubTypeCount = (from s in _sportsAndScheduleDates
                                         where s.SportType.Trim() == nodeItemPath[0].Trim() &&
                                               s.SportSubType.Trim() == nodeItemPath[2].Trim() &&
                                               s.ScheduleDate == ((BetOptionNode)secondChildNode).ScheduleDate
                                         select s).Count();
                if (sportSubTypeCount == 0)
                  firstChildNode.Remove();
              }
            }
          }
        }
      }
    }

    private void RefreshAvailableSportsTree() {
      treSports.AfterSelect -= treSports_AfterSelect;
      string[] selectedNodeItems = null;
      var expandRootNode = false;
      if (treSports.SelectedNode != null) {
        selectedNodeItems = treSports.SelectedNode.FullPath.Split(new[] { treSports.PathSeparator }, StringSplitOptions.RemoveEmptyEntries);
        expandRootNode = treSports.SelectedNode.IsExpanded;
      }

      var treSportsHasFocus = treSports.Focused;

      FillAvailableSportsTree(Mdi.ShowPropsInSportsTreeView);

      if (selectedNodeItems != null) {

        switch (selectedNodeItems.Length) {
          case 0:
            break;
          case 1:
            foreach (TreeNode rootNode in treSports.Nodes) {
              if (rootNode.Text.Trim() == selectedNodeItems[0].Trim()) {
                treSports.SelectedNode = rootNode;
                if (expandRootNode)
                  treSports.SelectedNode.Expand();
                break;
              }
            }
            break;
          case 2:
            foreach (TreeNode rootNode in treSports.Nodes) {
              if (rootNode.Text.Trim() != selectedNodeItems[0].Trim()) continue;
              foreach (var secondLevelNode in rootNode.Nodes.Cast<TreeNode>().Where(secondLevelNode => secondLevelNode.Text.Trim() == selectedNodeItems[1].Trim())) {
                treSports.SelectedNode = secondLevelNode;
                treSports.SelectedNode.Expand();
                break;
              }
              break;
            }
            break;
          case 3:
            foreach (TreeNode rootNode in treSports.Nodes) {
              if (rootNode.Text.Trim() != selectedNodeItems[0].Trim()) continue;
              foreach (TreeNode secondLevelNode in rootNode.Nodes) {
                if (secondLevelNode.Text.Trim() != selectedNodeItems[1].Trim()) continue;
                foreach (var thirdLevelNode in secondLevelNode.Nodes.Cast<TreeNode>().Where(thirdLevelNode => thirdLevelNode.Text.Trim() == selectedNodeItems[2].Trim())) {
                  treSports.SelectedNode = thirdLevelNode;
                  treSports.SelectedNode.Expand();
                  break;
                }
                break;
              }
              break;
            }
            break;
        }
      }
      if (treSportsHasFocus)
        treSports.Focus();
      treSports.AfterSelect += treSports_AfterSelect;
    }

    private void SearchNextNode(TreeNode node, string targetNodeFullPath, out TreeNode targetNode) {
      targetNode = null;

      if (node.FullPath.ToLower().Replace(" ", "") == targetNodeFullPath.Replace(" ", "")) {
        targetNode = node;
        return;
      }

      foreach (TreeNode childNode in node.Nodes) {
        if (childNode.FullPath.ToLower().Replace(" ", "") == targetNodeFullPath.Replace(" ", "")) {
          targetNode = childNode;
          break;
        }
        if (targetNode == null && childNode.NextNode != null)
          SearchNextNode(childNode, targetNodeFullPath, out targetNode);
      }
    }

    private void SetSelectedContestTreeViewNode(spCnGetActiveContests_Result contest) {

      var targetNodeFullPath = "";
      if (contest.ContestType != null && contest.ContestType.Trim() != ".")
        targetNodeFullPath += contest.ContestType.Trim().ToLower();
      if (contest.ContestType2 != null && contest.ContestType2.Trim() != ".")
        targetNodeFullPath += treSports.PathSeparator + contest.ContestType2.Trim().ToLower();
      if (contest.ContestType2 != null && contest.ContestType2.Trim() != "." &&
          contest.ContestType3 != null && contest.ContestType3.Trim() != ".")
        targetNodeFullPath += treSports.PathSeparator + contest.ContestType3.Trim().ToLower();


      TreeNode targetNode = null;
      foreach (TreeNode node in treSports.Nodes) {
        if (targetNode != null)
          break;
        if (((BetOptionNode)node).NodeType != "C")
          continue;
        if (node.FullPath.ToLower().Replace(" ", "") != targetNodeFullPath.Replace(" ", "")) {
          SearchNextNode(node, targetNodeFullPath, out targetNode);
        }
        else {
          targetNode = node;
          break;
        }
      }

      if (targetNode != null) {
        treSports.CollapseAll();
        treSports.SelectedNode = targetNode;
        NodeExpand(treSports.SelectedNode.Parent);
      }

      /*
      TreeNode targetNode = null;

      foreach (TreeNode typeNode in treSports.Nodes)
      {
          if (((BetOptionNode)typeNode).NodeType != "C" || ((BetOptionNode)typeNode).Text.Trim().ToLower() != contest.ContestType.Trim().ToLower())
              continue;
          targetNode = typeNode;
          break;
      }

      if (targetNode == null)
          return;

      if (contest.ContestType2 != null && contest.ContestType2.Trim() != ".")
      {
          foreach (TreeNode typeNode in targetNode.Nodes)
          {
              if (((BetOptionNode)typeNode).Text.Trim().ToLower() != contest.ContestType2.Trim().ToLower())
                  continue;
              targetNode = typeNode;
              break;
          }

          if (targetNode == null)
              return;
      }

      if (contest.ContestType3 != null && contest.ContestType3.Trim() != ".")
      {
          foreach (TreeNode typeNode in targetNode.Nodes)
          {
              if (((BetOptionNode)typeNode).Text.Trim().ToLower() != contest.ContestType3.Trim().ToLower())
                  continue;
              targetNode = typeNode;
              break;
          }

          if (targetNode == null)
              return;
      }

      treSports.SelectedNode = targetNode;
      NodeExpand(treSports.SelectedNode.Parent);
      */
    }

    private void SetSelectedGameTreeViewNode(spGLGetActiveGamesByCustomer_Result game) {
      var targetNodeFullPath = game.SportType.Trim().ToLower() + treSports.PathSeparator + DateTimeF.FormatGameDayMonth(game.ScheduleDate.GetValueOrDefault()) + treSports.PathSeparator + game.SportSubType.Trim().ToLower();
      TreeNode targetNode = null;
      foreach (TreeNode node in treSports.Nodes) {
        if (targetNode != null)
          break;
        if (((BetOptionNode)node).NodeType != "G" || node.Text.Trim().ToLower() != game.SportType.Trim().ToLower())
          continue;
        if (node.FullPath.ToLower().Replace(" ", "") != targetNodeFullPath.Replace(" ", "")) {
          SearchNextNode(node, targetNodeFullPath, out targetNode);
        }
        else {
          targetNode = node;
          break;
        }
      }

      if (targetNode != null) {
        treSports.CollapseAll();
        treSports.SelectedNode = targetNode;
        NodeExpand(treSports.SelectedNode.Parent);
      }

      /*
        TreeNode targetNode = null;

        foreach (TreeNode sportNode in treSports.Nodes)
        {
            if (((BetOptionNode)sportNode).NodeType != "G" || ((BetOptionNode)sportNode).Text.Trim().ToLower() != game.SportType.Trim().ToLower())
                continue;
            targetNode = sportNode;
            break;
        }

        if (targetNode == null)
            return;

        foreach (TreeNode scheduleNode in targetNode.Nodes)
        {
            if (((BetOptionNode)scheduleNode).ScheduleDate != game.ScheduleDate)
                continue;
            targetNode = scheduleNode;
            break;
        }

        if (targetNode == null)
            return;

        foreach (TreeNode sportSubType in targetNode.Nodes)
        {
            if (((BetOptionNode)sportSubType).Text.Trim().ToLower() != game.SportSubType.Trim().ToLower())
                continue;
            targetNode = sportSubType;
            break;
        }

        if (targetNode == null)
            return;

        treSports.SelectedNode = targetNode;
        NodeExpand(treSports.SelectedNode.Parent);*/
    }

    private void NodeExpand(TreeNode nodeExpand) {
      while (nodeExpand != null) {
        nodeExpand.Expand();
        nodeExpand = nodeExpand.Parent;
      }
    }

    public void FocusOnGamesOrContestsGridView() {
      var shownGridView = (DataGridView)Controls.Find(GamesGridViewInUse, true).FirstOrDefault();
      if (shownGridView != null && shownGridView.Visible)
        shownGridView.Focus();
    }

    #endregion

    #region Protected Methods

    protected override bool ProcessCmdKey(ref Message msg, Keys keyData) {
      var baseResult = base.ProcessCmdKey(ref msg, keyData);

      return keyData == (Keys.Control | Keys.Tab) ? Mdi.ActivateWageringForm(Name, baseResult) : baseResult;
    }

    #endregion

    #region Events

    private void frmSportAndGameSelection_Load(object sender, EventArgs e) {
      InitializeExtensions();
      LoadSportAndGameSelection();
    }

    private void bgwrkUpdateLines_DoWork(object sender, DoWorkEventArgs e) {
      ExecuteUpdateLinesThreadedWork();
    }

    private void bgwrkUpdateLines_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
      FinishUpdateLinesThreadedWork();
      if (!FormClosePendingFlag) return;
      if (LinesUpdater != null) LinesUpdater.Stop();
      FormClosePendingFlag = false;
      Close();
    }

    private void btnConvertToFrac_Click(object sender, EventArgs e) {
      if (txtDecimalOdds.Text.Length == 0) return;
      double output;
      if (!double.TryParse(txtDecimalOdds.Text, out output)) return;
      lblFractionalOdds.Text = "";
      lblProcessDuration.Text = "";

      var dts = Mdi.GetCurrentDateTime();
      if (OddsFractionMaxDenominator == null) return;
      var fracValues = OddsTypes.DecToFraction(output, (int)OddsFractionMaxDenominator);
      var dte = Mdi.GetCurrentDateTime();
      lblProcessDuration.Text = dte.Subtract(dts).ToString();
      lblFractionalOdds.Text = fracValues.Numerator + @"/" + fracValues.Denominator;
    }

    private void btnLineDisplayToggle_Click(object sender, EventArgs e) {
      ToggleLineDisplay(btnLineDisplayToggle.Text);
    }

    private void btnUpdateLines_Click(object sender, EventArgs e) {
      UpdateGameLines();
    }

    private void chbActiveOnly_CheckedChanged(object sender, EventArgs e) {
      if (((CheckBox)sender).Checked) {
        ((CheckBox)sender).Text = @"Show All (F9)";
        ShowHideInactiveGames("Hide");
        ShowingActiVeGamesOnly = true;
      }
      else {
        ((CheckBox)sender).Text = @"Active Only (F9)";
        ShowHideInactiveGames("Show");
        ShowingActiVeGamesOnly = false;
      }
    }

    private void dgvwGame_CellClick(object sender, DataGridViewCellEventArgs e) {
      LoadWageringForm((DataGridView)sender, e.RowIndex, e.ColumnIndex);
    }

    private void dgvwGame_CellEnter(object sender, DataGridViewCellEventArgs e) {
      int gameNumber;
      int periodNumber;
      DateTime gameWagerCutoffDateTime;
      String team1Id;
      String team2Id;

      if (e.RowIndex <= -1 || KeyPressBuffer != "") return;
      var rowGroupNum = ((DataGridView)sender).Rows[e.RowIndex].Cells[ROWGROUPNUM_INDEX].Value.ToString();
      if (rowGroupNum == "4")
        return;

      if (!_formatGameTitles.IsTitle(((DataGridView)sender).Rows[e.RowIndex]))
        ShowGameInfo((DataGridView)sender, e.RowIndex, out gameNumber, out periodNumber, out gameWagerCutoffDateTime, out team1Id, out team2Id);
      else ClearGameInfo();
    }

    private void dgvwGame_KeyPress(object sender, KeyPressEventArgs e) {
      var gvw = (DataGridView)sender;
      var rowIndex = -1;
      var columnIndex = -1;

      if (gvw == null) return;
      if (gvw.SelectedCells.Count > 0) {
        rowIndex = gvw.SelectedCells[0].RowIndex;
        columnIndex = gvw.SelectedCells[0].ColumnIndex;
      }

      if (rowIndex > -1 && e.KeyChar == Convert.ToChar(Keys.Enter)) {
        var rowGroupNum = ((DataGridView)sender).Rows[rowIndex - 1].Cells[ROWGROUPNUM_INDEX].Value.ToString();
        if (rowGroupNum == "4")
          return;
        try {
          if (_enteredCellIdx > -1) {
            if (((DataGridView)sender).Rows[_enteredCellIdx].Cells[columnIndex].Visible)
              ((DataGridView)sender).Rows[_enteredCellIdx].Cells[columnIndex].Selected = true;
          }
        }
        catch (Exception exception) {
          Log(exception);
        }
        if (_enteredCellIdx > -1)
          LoadWageringForm((DataGridView)sender, _enteredCellIdx, columnIndex);

      }
      else if (e.KeyChar == Convert.ToChar(Keys.Tab)) {
        e.Handled = true;

        try {
          if (((DataGridView)sender).Rows[0].Cells[ROTATION_INDEX].Visible) {
            ((DataGridView)sender).Rows[0].Cells[ROTATION_INDEX].Selected = true;
          }
        }
        catch (Exception exception) {
          Log(exception);
        }
      }
    }

    private void dgvwPropsAndContests_CellClick(object sender, DataGridViewCellEventArgs e) {
      LoadContestWageringForm((DataGridView)sender, e.RowIndex, e.ColumnIndex);
    }

    private void dgvwPropsAndContests_CellEnter(object sender, DataGridViewCellEventArgs e) {
      String contestNum;
      String contestantNumber;
      String contestant;
      String contestDesc;
      DateTime contestDateTime;

      ShowContestInfo(sender, e.RowIndex, out contestNum, out contestantNumber, out contestant, out contestDesc, out contestDateTime);
    }

    private void dgvwPropsAndContests_KeyPress(object sender, KeyPressEventArgs e) {
      var gvw = (DataGridView)sender;

      if (gvw == null) return;
      var rowIndex = -1;
      var columnIndex = -1;
      if (gvw.SelectedCells.Count > 0) {
        rowIndex = gvw.SelectedCells[0].RowIndex;
        columnIndex = gvw.SelectedCells[0].ColumnIndex;
      }

      if (rowIndex <= -1 || columnIndex <= -1 || e.KeyChar != Convert.ToChar(Keys.Enter)) return;
      if (((DataGridView)sender).Rows[_enteredCellIdx].Cells[columnIndex].Visible)
        ((DataGridView)sender).Rows[_enteredCellIdx].Cells[columnIndex].Selected = true;
      LoadContestWageringForm(gvw, _enteredCellIdx, columnIndex);
    }

    private void dgvwGame_KeyDown(object sender, KeyEventArgs e) {
      SetFocusOnSpecificGameOrContest(e);
      _enteredCellIdx = 0;
      if (((DataGridView)sender).SelectedCells.Count > 0)
        _enteredCellIdx = ((DataGridView)sender).SelectedCells[0].RowIndex;
    }

    private void dgvwPropsAndContests_KeyDown(object sender, KeyEventArgs e) {
      SetFocusOnSpecificGameOrContest(e);
      _enteredCellIdx = 0;
      if (((DataGridView)sender).SelectedCells.Count > 0)
        _enteredCellIdx = ((DataGridView)sender).SelectedCells[0].RowIndex;
    }

    private void dgvwGame_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e) {
      DrawGridLines(sender, e, 8);
    }

    private void dgvwPropsAndContests_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e) {
      DrawGridLines(sender, e, 8);
    }

    private void FrmSportAndGameSelection_Shown(object sender, EventArgs e) {
      treSports.AfterSelect += treSports_AfterSelect;
      //treSports.SelectedNode = null;
    }

    private void FrmSportAndGameSelection_FormClosing(object sender, FormClosingEventArgs e) {
      Tag = "Closed Customer : " + (Customer.CustomerID ?? "").Trim() + " Ticket: " + Mdi.TicketNumber;
      if (bgwrkUpdateLines.IsBusy) {
        e.Cancel = true;
        FormClosePendingFlag = true;
      }
      else {
        RemoveFormObjectsFromMemory();
        FormClosePendingFlag = false;
      }
    }

    private void FrmSportAndGameSelection_KeyUp(object sender, KeyEventArgs e) {
      if (e.KeyCode == Keys.Enter && KeyPressBuffer != "") {
        FindGameOrContestByRotation(KeyPressBuffer);
        KeyPressBuffer = "";
      }
    }

    private void FrmSportAndGameSelection_KeyDown(object sender, KeyEventArgs e) {
      var numericKey = e.KeyCode == Keys.NumPad0 || e.KeyCode == Keys.NumPad1 || e.KeyCode == Keys.NumPad2 || e.KeyCode == Keys.NumPad3 ||
      e.KeyCode == Keys.NumPad4 || e.KeyCode == Keys.NumPad5 || e.KeyCode == Keys.NumPad6 || e.KeyCode == Keys.NumPad7 || e.KeyCode == Keys.NumPad8 ||
      e.KeyCode == Keys.NumPad9 || e.KeyCode == Keys.D0 || e.KeyCode == Keys.D1 || e.KeyCode == Keys.D2 || e.KeyCode == Keys.D3 || e.KeyCode == Keys.D4 ||
      e.KeyCode == Keys.D5 || e.KeyCode == Keys.D6 || e.KeyCode == Keys.D7 || e.KeyCode == Keys.D8 || e.KeyCode == Keys.D9;

      if (!numericKey && e.KeyCode != Keys.S && e.KeyCode != Keys.M && e.KeyCode != Keys.L && e.KeyCode != Keys.E) return;
      e.SuppressKeyPress = true;

      if (e.KeyCode != Keys.Enter) {
        if ((Mdi.GetCurrentDateTime() - KeyPressLastTime).Seconds >= 1) KeyPressBuffer = TwUtilities.GetKeyDownChar(e);
        else KeyPressBuffer += TwUtilities.GetKeyDownChar(e);
      }
      KeyPressLastTime = Mdi.GetCurrentDateTime();
    }

    private void radObj_CheckedChanged(object sender, EventArgs e) {
      FillPeriodGameInformation((RadioButton)sender);
    }

    private void treSports_AfterSelect(object sender, TreeViewEventArgs e) {
      TextBox t = null;
      if (Mdi.Controls.Find("txtTeamOrAsterisk", true).FirstOrDefault() != null) {
        t = (TextBox)Mdi.Controls.Find("txtTeamOrAsterisk", true).FirstOrDefault();
      }
      if (((TreeView)sender).SelectedNode != null && (t != null && t.Text.Trim() == ""))
        LoadSportsTreeSelection((TreeView)sender);
    }

    private void treSports_BeforeExpand(object sender, TreeViewCancelEventArgs e) {
      if (!_expand) {
        e.Cancel = true;
        _expand = true;
      }
    }

    private void treSports_BeforeSelect(object sender, TreeViewCancelEventArgs e) {
      if (!_expand) {
        e.Cancel = true;
        _expand = true;
      }
    }

    private void treSports_Click(object sender, EventArgs e) {
      LoadSportsTreeSelection((TreeView)sender);
    }

    private void treSports_KeyDown(object sender, KeyEventArgs e) {
      if (e.Shift && (e.KeyCode.ToString() == Keys.Left.ToString() || e.KeyCode.ToString() == Keys.Right.ToString())) {
        _expand = false;
        return;
      }
      if (e.Control && e.KeyCode == Keys.Tab) {
        Mdi.ActivateWageringForm(Name);
        return;
      }
      switch (e.KeyCode) {
        case Keys.Tab:
          e.Handled = true;
          if (Mdi != null) {
            Mdi.Activate();
            Mdi.Focus();
            if (Mdi.Controls.Find("txtTeamOrAsterisk", true).FirstOrDefault() != null) {
              var t = (TextBox)Mdi.Controls.Find("txtTeamOrAsterisk", true).FirstOrDefault();
              Mdi.ActiveControl = t;
              if (t != null) {
                t.Focus();
                t.Select();
              }
            }
          }
          break;
        case Keys.NumPad9:
        case Keys.NumPad8:
        case Keys.NumPad7:
        case Keys.NumPad6:
        case Keys.NumPad5:
        case Keys.NumPad4:
        case Keys.NumPad3:
        case Keys.NumPad2:
        case Keys.NumPad1:
        case Keys.NumPad0:
        case Keys.D9:
        case Keys.D8:
        case Keys.D7:
        case Keys.D6:
        case Keys.D5:
        case Keys.D4:
        case Keys.D3:
        case Keys.D2:
        case Keys.D1:
        case Keys.D0:
          try {
            FocusOnGamesOrContestsGridView();
            //SetFocusOnSpecificGameOrContest(e);
          }
          catch (InvalidOperationException) {
          }
          break;
      }
    }

    private void treSports_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e) {
      if (e.KeyValue == 9) {
        e.IsInputKey = true;
      }
    }

    private void treSports_KeyUp(object sender, KeyEventArgs e) {
      if (e.KeyCode == Keys.D0 || e.KeyCode == Keys.D1 || e.KeyCode == Keys.D2
          || e.KeyCode == Keys.D3 || e.KeyCode == Keys.D4 || e.KeyCode == Keys.D5 ||
          e.KeyCode == Keys.D6 || e.KeyCode == Keys.D7 || e.KeyCode == Keys.D8 || e.KeyCode == Keys.D9
          || e.KeyCode == Keys.NumPad0 || e.KeyCode == Keys.NumPad1 ||
          e.KeyCode == Keys.NumPad2 || e.KeyCode == Keys.NumPad3 || e.KeyCode == Keys.NumPad4 ||
          e.KeyCode == Keys.NumPad5 || e.KeyCode == Keys.NumPad6 || e.KeyCode == Keys.NumPad7 ||
          e.KeyCode == Keys.NumPad8 || e.KeyCode == Keys.NumPad9
                ) {
        FocusOnGamesOrContestsGridView();
        e.Handled = true;
      }
    }

    private void treSports_KeyPress(object sender, KeyPressEventArgs e) {
      if (e.KeyChar.ToString(CultureInfo.InvariantCulture) == "0" || e.KeyChar.ToString(CultureInfo.InvariantCulture) == "1" || e.KeyChar.ToString(CultureInfo.InvariantCulture) == "2"
          || e.KeyChar.ToString(CultureInfo.InvariantCulture) == "3" || e.KeyChar.ToString(CultureInfo.InvariantCulture) == "4" || e.KeyChar.ToString(CultureInfo.InvariantCulture) == "5" ||
          e.KeyChar.ToString(CultureInfo.InvariantCulture) == "6" || e.KeyChar.ToString(CultureInfo.InvariantCulture) == "7" || e.KeyChar.ToString(CultureInfo.InvariantCulture) == "8" || e.KeyChar.ToString(CultureInfo.InvariantCulture) == "9"
                ) {
        FocusOnGamesOrContestsGridView();
        e.Handled = true;
      }
    }

    private void SqlDepLinesChange_DependencyChanged(object sender, EventArgs e)
    {
        if (((SqlNotificationEventArgs)e).Type != SqlNotificationType.Subscribe)
        {
            if (!bgwrkUpdateLines.IsBusy)
            {
                bgwrkUpdateLines.RunWorkerAsync();
            }
            SqlDependencyToGetLastLineChangeNotifications(Connection);
        }
    }
    #endregion
  }
}