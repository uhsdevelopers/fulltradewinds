﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using GUILibraries.BusinessLayer;
using GUILibraries.Forms;
using GUILibraries.Utilities;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;
using TicketWriter.Properties;
using SIDLibraries.Utilities;
using SIDLibraries.Entities;
using Common = TicketWriter.Utilities.Common;

namespace TicketWriter.UI {
  public sealed partial class FrmPlaceTeaser : SIDForm {
    #region Public Properties

    public string ActiveMdiChildName { private get; set; }

    public String FrmMode { private get; set; }

    public FrmSportAndGameSelection FrmSportAndGameSelection {
      get {
        return _frmSportAndGameSelection ??
               (_frmSportAndGameSelection =
                (FrmSportAndGameSelection)FormF.GetFormByName("FrmSportAndGameSelection", this));
      }
    }

    public int? CurrentPicks {
      get {
        if (Teaser == null || Mdi == null || Mdi.Ticket == null || Mdi.Ticket.WagerItems == null) return null;
        var itemsCount = (from a in Mdi.Ticket.WagerItems where a.TicketNumber == Teaser.TicketNumber && a.WagerNumber == Teaser.Number select a).Count();
        if (itemsCount == 0)
          return null;
        return itemsCount;
      }
    }

    public int MaxPicks { get; private set; }

    public int MinPicks { get; private set; }

    public MdiTicketWriter Mdi {
      get {
        return _mdiTicketWriter ??
               (_mdiTicketWriter = (MdiTicketWriter)FormF.GetFormByName("MdiTicketWriter", this));
      }
    }

    public int RelativeXPos { private get; set; }

    public int RelativeYPos { private get; set; }

    private FormParameterList ParameterList {
      get {
        return _parameterList ??
               (_parameterList =
                ((MdiTicketWriter)(FormF.GetFormByName("MdiTicketWriter", this))).ParameterList);
      }
    }

    public Teaser Teaser { get; set; }

    private List<spLOGetTeaserSportSpec_Result> TeaserSportSpecs { get; set; }

    public List<spLOGetTeaserSportSpec_Result> TeaserSportSpecsByTeaser { get; private set; }

    private spLOGetTeaserSpecs_Result SelectedTeaserSpec { get; set; }

    public String TeaserName { get; private set; }

    public int TeaserTies { get; private set; }

    public int TicketNumberToUpd { private get; set; }

    public int WagerNumberToUpd { private get; set; }

    public Boolean ItemFromMakeAWagerForm { private get; set; }

    public String WagerTypeModeTeaser { get; private set; } // Risk or ToWin

    #endregion

    #region Private Properties

    private String ModuleName {
      get {
        return ParameterList.GetItem("ModuleName").Value;
      }
    }

    #endregion

    #region Private vars

    private string _betAmountMode = "Risk";
    private bool _deleteWagersOnExit = true;
    private String _editChosenTeamsCnt;
    private String _editRiskAmt;
    private String _editTeaserName;
    private String _editToWinAmt;

    private Boolean _editRoundRobinFlag;
    private String _editSelectedRoundRobin;
    private String _editSelectedRRobinOutcome;

    FrmSportAndGameSelection _frmSportAndGameSelection;
    private Boolean _isFreePlayFlag;
    private MdiTicketWriter _mdiTicketWriter;
    private FormParameterList _parameterList;
    private Boolean _performIdxChanged = true;

    private Boolean _allowRoundRobinTeasers;
    private bool _byPassMinimumWager;

    private Ticket.Wager _wagerEditBackup;
    private List<Ticket.WagerAndItemNumber> _itemsToRemove;
    private bool _cancelActionPerformed;
    List<Ticket.WagerItem> _itemsBackupCopy;

    #endregion

    #region Constructors

    public FrmPlaceTeaser(ModuleInfo moduleInfo)
      : base(moduleInfo) {
      InitializeComponent();
      SetCentsNumericInputs();
    }

    #endregion

    #region Public Methods

    public void AddItem(TeaserItem item) {
      if (Teaser.Frozen && string.IsNullOrEmpty(item.FromPreviousTicket)) {
        Teaser.Frozen = false;
      }
      item.WagerNumber = Teaser.Number ?? 0;
      bndsrc.Add(item);

      if (FrmMode != "Edit") {
        dgvwPlaceTeaser.ClearSelection();
        dgvwPlaceTeaser.Refresh();
      }
      var rCnt = bndsrc.Count;

      if (int.Parse(nudTeams.Value.ToString(CultureInfo.InvariantCulture)) < rCnt && (rCnt <= int.Parse(nudTeams.Maximum.ToString(CultureInfo.InvariantCulture)))) {
        nudTeams.Value = rCnt;
      }
    }

    public void AdjustFrmPosition() {
      var xw = RelativeXPos - Width;
      var yw = RelativeYPos - Height - 10;


      StartPosition = FormStartPosition.Manual;
      Location = new Point(xw, yw);
    }

    private void RemoveItem(int wNumber, int itNumber) {
      for (var i = 0; i < bndsrc.Count; i++) {
        var ti = (TeaserItem)bndsrc[i];
        if (ti.WagerNumber != wNumber || ti.Number != itNumber) continue;
        bndsrc.RemoveAt(i);
        break;
      }
      dgvwPlaceTeaser.Refresh();
    }

    private void SetCursorInTeamNumberBox() {
      nudTeams.Select();
      nudTeams.Select(0, nudTeams.Value.ToString(CultureInfo.InvariantCulture).Length);
    }

    public void SetCursorInTeaserCombo() {
      cmbTeaser.Select();
      cmbTeaser.Select(0, cmbTeaser.Text.Length);
    }

    public void UpdateItem(TeaserItem item) {
      for (var i = 0; i < bndsrc.Count; i++) {
        var ti = (TeaserItem)bndsrc[i];
        if (ti.TicketNumber == item.TicketNumber && ti.WagerNumber == item.WagerNumber && ti.Number == item.Number) {
          bndsrc[i] = item;
          break;
        }
      }

      dgvwPlaceTeaser.Refresh();
    }

    #endregion

    #region Private Methods

    private void CalculateRiskAmount() {
      var frm = FormF.GetFormByName("MdiTicketWriter", this);

      if (frm == null || txtToWin.Text.Length == 0) {
        txtRisk.Text = "";
        return;
      }

      var mdi = (MdiTicketWriter)frm;

      if (mdi.CustomerAvailableTeasers == null || mdi.CustomerAvailableTeasers.Count <= 0) return;
      double riskAmount;
      if (mdi.TeaserInfo == null) {
        using (var teaserInfo = new LineOffering.Teaser(Mdi.AppModuleInfo)) {
          riskAmount = teaserInfo.CalculateRisk(NumberF.ParseDouble(txtToWin.Text), cmbTeaser.Text, int.Parse(nudTeams.Value.ToString(CultureInfo.InvariantCulture)));
          txtRisk.Text = FormatNumber(riskAmount, false, 1, true);
        }

      }
      else {
        riskAmount = mdi.TeaserInfo.CalculateRisk(NumberF.ParseDouble(txtToWin.Text), cmbTeaser.Text, int.Parse(nudTeams.Value.ToString(CultureInfo.InvariantCulture)));
        txtRisk.Text = FormatNumber(riskAmount, false, 1, true);
      }
    }

    private void CalculateToWinAmount() {
      var frm = FormF.GetFormByName("MdiTicketWriter", this);

      if (frm == null || txtRisk.Text.Length == 0 || (chbRoundRobin.Checked && cmbRoundRobin.SelectedIndex < 0)) {
        txtToWin.Text = "";
        return;
      }

      if (chbRoundRobin.Checked && cmbRoundRobin.SelectedIndex > -1) {
        if (Teaser.RoundRobin == null) {
          EnableRoundRobin(true);
        }

        Teaser.AmountWagered = NumberF.ParseDouble(txtRisk.Text);
        txtToWin.Text = FormatNumber(Teaser.RRToWinAmount, false, 1, true);
        return;
      }

      var mdi = (MdiTicketWriter)frm;

      if (mdi.CustomerAvailableTeasers == null || mdi.CustomerAvailableTeasers.Count <= 0) return;
      double toWinAmount;
      if (mdi.TeaserInfo == null) {
        using (var teaserInfo = new LineOffering.Teaser(Mdi.AppModuleInfo)) {
          toWinAmount = teaserInfo.CalculateToWin(NumberF.ParseDouble(txtRisk.Text), cmbTeaser.Text, int.Parse(nudTeams.Value.ToString(CultureInfo.InvariantCulture)));
          txtToWin.Text = FormatNumber(toWinAmount, false, 1, true);
        }

      }
      else {
        toWinAmount = mdi.TeaserInfo.CalculateToWin(NumberF.ParseDouble(txtRisk.Text), cmbTeaser.Text, int.Parse(nudTeams.Value.ToString(CultureInfo.InvariantCulture)));
        txtToWin.Text = FormatNumber(toWinAmount, false, 1, true);
      }
    }

    private void CancelWager(bool closeForm = true) {
      try {
        var mode = ((string)Is.NullValue(FrmMode, "")).ToUpper();
        if (Teaser != null && (mode != "EDIT" || (mode == "EDIT" && Teaser.TicketNumber != Mdi.TicketNumber)) &&
            _deleteWagersOnExit) {
          Mdi.Ticket.DeleteWager(Teaser.TicketNumber, Teaser.Number);
        }

        Mdi.Ticket.RemoveCancelledWagerAndItems(FrmMode, _itemsToRemove, _wagerEditBackup, _itemsBackupCopy, null, _cancelActionPerformed);
        Mdi.WageringPanelName = null;

        Mdi.OpenPlayWagerInfo = null;

        if (Teaser != null && (mode != "EDIT" || (FrmMode.Trim().ToUpper() == "EDIT" && Teaser.TicketNumber != Mdi.TicketNumber)))
          WageringForm.Close(this, FrmMode, Teaser.TicketNumber, Teaser.Number, closeForm);
        else {
          /*if (Mdi.Ticket == null || Mdi.Ticket.WagerItems == null) {
          ReBuildWagerWithSafeCopy(new List<Ticket.WagerItem>());
        }
        else {
          ReBuildWagerWithSafeCopy(new List<Ticket.WagerItem>(Mdi.Ticket.WagerItems));
        }*/
          WageringForm.Close(this, FrmMode, null, null, closeForm);

        }
      }
      catch (Exception ex) {
        Log(ex);
      }
    }

    /*
    private void RemoveCancelledItems() {
      if (FrmMode == null || FrmMode.Trim().ToUpper() != "EDIT" || !_cancelActionPerformed) return;
      if (Mdi.Ticket.WagerItems.Count <= 0) return;
      foreach (var item in _itemsToRemove) {
        var wi = (from i in Mdi.Ticket.WagerItems
                  where
                    i.TicketNumber == Teaser.TicketNumber &&  
                    i.WagerNumber == Teaser.Number &&
                    i.ItemNumber == item.ItemNumber &&
                    i.GameNum == item.GameNumber &&
                    i.PeriodNumber == item.PeriodNumber &&
                    i.Description.Trim() == item.ItemDescription.Trim()
                  select i).FirstOrDefault();
        if (wi != null) {
          Mdi.Ticket.DeleteWagerItem(item.TicketNumber, item.WagerNumber, item.ItemNumber);
        }
      }

      if (itemsBackupCopy != null && itemsBackupCopy.Count() > 0)
      {
          for (var i = 0; i < Mdi.Ticket.WagerItems.Count; i++)
          {
              if (Mdi.Ticket.WagerItems[i].TicketNumber != Teaser.TicketNumber || Mdi.Ticket.WagerItems[i].WagerNumber != Teaser.Number)
                  continue;
              var bkpItem = (from x in itemsBackupCopy where x.ItemNumber == Mdi.Ticket.WagerItems[i].ItemNumber select x).FirstOrDefault();
              if (bkpItem != null)
              {
                  Mdi.Ticket.WagerItems[i] = bkpItem;
              }
          }
      }

    }

    private void ReBuildWagerWithSafeCopy(IEnumerable<Ticket.WagerItem> modifiedWagerItemList) {
      if (Mdi.Ticket == null) return;
      if (Mdi.Ticket.Wagers != null) {
        Mdi.Ticket.DeleteWager(TicketNumberToUpd, WagerNumberToUpd); // This deletes wager items, too. The new ones (not in the backup) are stripped
        if (_wagerEditBackup != null)
        {
          Mdi.Ticket.AddWager(_wagerEditBackup);
          Mdi.Ticket.SortWagersList();
        }
      }

      Mdi.Ticket.WagerItems = null;
      Mdi.Ticket.WagerItems = new List<Ticket.WagerItem>();

      foreach (var wi in modifiedWagerItemList) {
        Mdi.Ticket.AddWagerItem(wi);
      }

    }
    */

    private int ChangeSelectedTeaserName() {
      var itemIdx = 0;
      var counter = 0;

      foreach (spCstGetCustomerTeaserSpecs_Result item in cmbTeaser.Items) {
        if (item.TeaserName.ToString(CultureInfo.InvariantCulture).Trim() == _editTeaserName) {
          itemIdx = counter;
          break;
        }
        counter++;
      }

      return itemIdx;
    }

    private void DeleteWagerItem() {
      if (dgvwPlaceTeaser.SelectedCells.Count <= 0) return;
      var row = dgvwPlaceTeaser.SelectedCells[0].OwningRow;

      var wNumber = int.Parse(row.Cells[3].Value.ToString());
      var itNumber = int.Parse(row.Cells[4].Value.ToString());
      var tNumber = int.Parse(row.Cells[24].Value.ToString());

      if (Teaser.TicketNumber != Mdi.TicketNumber) {
        var item = (from wi in Mdi.Ticket.WagerItems where wi.TicketNumber == tNumber && wi.WagerNumber == wNumber && wi.ItemNumber == itNumber select wi).FirstOrDefault();
        if (item != null && item.Outcome != "P") {
          MessageBox.Show(@"Item cannot be removed from the database", @"Item not pending", MessageBoxButtons.OK);
          return;
        }
      }

      if (
      MessageBox.Show(Resources.FrmPlaceTeaser_DeleteWagerItem_This_action_will_remove_the_bet_from_the_teaser_,
        Resources.FrmPlaceTeaser_DeleteWagerItem_remove_Wager, MessageBoxButtons.OKCancel) != DialogResult.OK)
        return;
      //delete wi and fix itemnumbers;

      RemoveItem(wNumber, itNumber);
      Mdi.Ticket.DeleteWagerItem(tNumber, wNumber, itNumber);
      Mdi.Ticket.ReIndexWagerItems(tNumber, wNumber);
      if (Teaser.Number == null) return;
      var wager = Mdi.Ticket.GetWager(Teaser.TicketNumber, Teaser.Number.Value);
      wager.WagerStatus = dgvwPlaceTeaser.Rows.Count < nudTeams.Value ? "O" : "P";
    }

    private void FillAvailableTeasersDropDown() {
      var teaserNames = GetCustomerAvailableTeasers();
      foreach (var res in teaserNames) {
        CustomerTeasersBindingSource.Add(res);
      }
    }

    private void FillMinMaxAllowedTeams() {
      var teaserSpecs = GetAvailableTeasersSpecs();

      SelectedTeaserSpec = null;

      if (cmbTeaser.SelectedValue != null) {
        SelectedTeaserSpec = (from p in teaserSpecs where p.TeaserName.Trim() == cmbTeaser.SelectedValue.ToString() select p).FirstOrDefault();
      }

      if (SelectedTeaserSpec == null) return;
      if (SelectedTeaserSpec.MinPicks != null) nudTeams.Minimum = (int)SelectedTeaserSpec.MinPicks;
      if (SelectedTeaserSpec.MaxPicks != null) nudTeams.Maximum = (int)SelectedTeaserSpec.MaxPicks;
      nudTeams.Value = nudTeams.Minimum;

      if (SelectedTeaserSpec.TeaserTies != null) TeaserTies = (int)SelectedTeaserSpec.TeaserTies;
      if (SelectedTeaserSpec.MaxPicks != null) MaxPicks = (int)SelectedTeaserSpec.MaxPicks;
      if (SelectedTeaserSpec.MinPicks != null) MinPicks = (int)SelectedTeaserSpec.MinPicks;
    }

    private void FillPlaceTeaserGvInEditMode() {
      if (bndsrc != null) {
        bndsrc.Clear();
      }

      else {
        bndsrc = new BindingSource();
      }
      var existingItems = (from p in Mdi.Ticket.WagerItems where p.TicketNumber == TicketNumberToUpd && p.WagerNumber == WagerNumberToUpd select p).ToList();

      // Teaser teaser = RebuildTeaserWager(WagerNumberToUpd);

      foreach (var wi in existingItems) {
        var item = new TeaserItem {
          Description = wi.Description,
          DescriptionAndAdjustedPrice = wi.ShortDescription,
          FromPreviousTicket = wi.FromPreviousTicket,
          FromPreviousWagerNumber = wi.FromPreviousWagerNumber,
          AlreadyBetItemCount = wi.AlreadyBetItemCount,
          Number = wi.ItemNumber,
          PriceType = wi.PriceType,
          Type = WagerType.GetFromCode(wi.WagerType),
          WagerNumber = wi.WagerNumber,
          TicketNumber = wi.TicketNumber,

          SelectedCell = wi.SelectedCell,
          SelectedOdds = wi.SelectedOdds,
          SelectedAmericanPrice = wi.SelectedAmericanPrice,
          SelectedBPointsOption = wi.SelectedBPointsOption,
          SelectedAmountWagered = (wi.AmountWagered ?? 0).ToString(CultureInfo.InvariantCulture),
          SelectedToWinAmount = (wi.ToWinAmount ?? 0).ToString(CultureInfo.InvariantCulture),
          WagerTypeMode = wi.WagerTypeMode,
          RowGroupNum = wi.RowGroupNum,
          PeriodWagerCutoff = wi.PeriodWagerCutoff,
          WageringMode = wi.WageringMode,
          LayoffWager = wi.LayoffFlag,
          FixedPrice = (wi.AdjustableOddsFlag == "N" || string.IsNullOrEmpty(wi.AdjustableOddsFlag) ? "Y" : "N"),
          Pitcher1MStart = wi.Pitcher1ReqFlag,
          Pitcher2MStart = wi.Pitcher2ReqFlag,
          TotalPointsOu = wi.TotalPointsOu
        };

        if (wi.GameNum != null) {
          item.GameNumber = (int)wi.GameNum;
        }
        if (wi.PeriodNumber != null) {
          item.PeriodNumber = (int)wi.PeriodNumber;
        }

        item.PeriodDescription = wi.PeriodDescription;
        _isFreePlayFlag = wi.FreePlayFlag == "Y";

        if (FrmSportAndGameSelection.ReadbackItems != null) {
          var existingReadbackItem = (from p in FrmSportAndGameSelection.ReadbackItems
                                      where p.TicketNumber == TicketNumberToUpd && p.WagerNumber == WagerNumberToUpd && p.ItemNumber == wi.ItemNumber && p.RowContainsEditItemInfo
                                      select p).FirstOrDefault();

          if (existingReadbackItem != null) {
            if (string.IsNullOrEmpty(item.FromPreviousTicket)) {
              item.DescriptionAndAdjustedPrice = existingReadbackItem.WiEditInfo.Description;
            }
            item.SelectedCell = existingReadbackItem.WiEditInfo.SelectedCell;
            item.SelectedOdds = existingReadbackItem.WiEditInfo.SelectedOdds;
            item.SelectedAmericanPrice = existingReadbackItem.WiEditInfo.SelectedAmericanPrice;
            item.SelectedBPointsOption = existingReadbackItem.WiEditInfo.SelectedBPointsOption;
            item.SelectedAmountWagered = "0";
            item.SelectedToWinAmount = "0";
            item.WagerTypeMode = "Risk";
            item.RowGroupNum = existingReadbackItem.WiEditInfo.RowGroupNum;
            item.PeriodWagerCutoff = item.PeriodWagerCutoff;
            item.WageringMode = existingReadbackItem.WiEditInfo.WageringMode;
            item.LayoffWager = existingReadbackItem.WiEditInfo.LayoffWager;
            item.FixedPrice = existingReadbackItem.WiEditInfo.FixedPrice;
            item.Pitcher1MStart = existingReadbackItem.WiEditInfo.Pitcher1MStart;
            item.Pitcher2MStart = existingReadbackItem.WiEditInfo.Pitcher2MStart;
            item.TotalPointsOu = existingReadbackItem.WiEditInfo.TotalsOuFlag;
            _isFreePlayFlag = existingReadbackItem.FreePlayFlag == "Y";
          }
        }
        bndsrc.Add(item);
      }
      dgvwPlaceTeaser.Refresh();
    }

    private void FillSelectedTeaserSportSpecs() {
      TeaserSportSpecs = GetAvailableTeaserSportsSpecs();
      txtSportsSpecsInfo.Text = "";

      if (TeaserSportSpecs == null) return;
      if (cmbTeaser.SelectedValue == null) return;
      TeaserSportSpecsByTeaser = (from p in TeaserSportSpecs where p.TeaserName.Trim() == cmbTeaser.SelectedValue.ToString() select p).ToList();

      txtSportsSpecsInfo.Text = Resources.FrmPlaceTeaser_FillSelectedTeaserSportSpecs_Minimum_teams__ + nudTeams.Minimum.ToString(CultureInfo.InvariantCulture) + @"  " + Resources.FrmPlaceTeaser_FillSelectedTeaserSportSpecs_Maximum_teams__ + nudTeams.Maximum.ToString(CultureInfo.InvariantCulture);

      foreach (var spec in TeaserSportSpecsByTeaser) {
        txtSportsSpecsInfo.Text += Environment.NewLine;
        txtSportsSpecsInfo.Text += spec.Points + Resources.FrmPlaceTeaser_FillSelectedTeaserSportSpecs__points___ + spec.SportSubType.Trim() + @" " + spec.SportType.Trim() + @" " + WagerType.GetFromCode(spec.WagerType).Name;

      }
    }

    private static void ForceFirstCellSelection(DataGridViewCellStateChangedEventArgs e) {
      if ((e.Cell == null || e.StateChanged != DataGridViewElementStates.Selected) || (e.Cell.ColumnIndex == 0 || !e.Cell.Selected)) {
        return;
      }
      e.Cell.Selected = false;
    }

    private List<spLOGetTeaserSportSpec_Result> GetAvailableTeaserSportsSpecs() {
      var frm = FormF.GetFormByName("MdiTicketWriter", this);

      if (frm == null) {
        return null;
      }
      var mdi = (MdiTicketWriter)frm;

      if (mdi.CustomerAvailableTeasers == null || mdi.CustomerAvailableTeasers.Count <= 0) return null;
      if (mdi.TeasersSportSpecs == null || mdi.TeasersSportSpecs.Count == 0) {
        mdi.GetTeasersSportsSpecs();
      }

      var teaserSportsSpecs = mdi.TeasersSportSpecs;

      return teaserSportsSpecs;
    }

    private IEnumerable<spLOGetTeaserSpecs_Result> GetAvailableTeasersSpecs() {
      var frm = FormF.GetFormByName("MdiTicketWriter", this);

      if (frm == null) {
        return null;
      }
      var mdi = (MdiTicketWriter)frm;

      if (mdi.CustomerAvailableTeasers == null || mdi.CustomerAvailableTeasers.Count <= 0) return null;
      if (mdi.TeasersSpecs == null || mdi.TeasersSpecs.Count == 0) {
        mdi.GetCustomerTeasersSpecs();
      }

      var teasersSpecs = mdi.TeasersSpecs;

      return teasersSpecs;

    }

    private IEnumerable<spCstGetCustomerTeaserSpecs_Result> GetCustomerAvailableTeasers() {
      var frm = FormF.GetFormByName("MdiTicketWriter", this);

      if (frm == null) {
        return null;
      }
      var mdi = (MdiTicketWriter)frm;

      if (mdi.CustomerAvailableTeasers == null || mdi.CustomerAvailableTeasers.Count == 0) {
        mdi.GetCustomerAvailableTeasers();
      }

      var availableTeasers = mdi.CustomerAvailableTeasers;
      return availableTeasers;
    }

    private void GetEditTeaserInfo() {
      var wagerToEdit = (from p in Mdi.Ticket.Wagers where p.TicketNumber == TicketNumberToUpd && p.WagerNumber == WagerNumberToUpd select p).FirstOrDefault();

      if (wagerToEdit != null) {
        _editTeaserName = wagerToEdit.TeaserName.Trim();
        _editChosenTeamsCnt = wagerToEdit.TotalPicks.ToString();
        _editRiskAmt = wagerToEdit.AmountWagered.ToString();
        _editToWinAmt = wagerToEdit.ToWinAmount.ToString();

        if (wagerToEdit.RoundRobinLink != null && (int)wagerToEdit.RoundRobinLink > 0) {
          _editRoundRobinFlag = true;
          _editSelectedRoundRobin = wagerToEdit.SelectedRoundRobin.ToString(CultureInfo.InvariantCulture);
          _editSelectedRRobinOutcome = wagerToEdit.SelectedRRobinOutcome;
        }
      }
      if (Teaser != null) return;
      if (wagerToEdit != null)
        FrmSportAndGameSelection.CreateTeaserWager(this, "Edit", TicketNumberToUpd, WagerNumberToUpd, new TextBox { Name = "txtRiskFromMakeWager", Text = _editRiskAmt },
          new TextBox { Name = "txtToWinMakeWager", Text = _editToWinAmt }, wagerToEdit.WagerTypeName, wagerToEdit.WagerType, wagerToEdit.FreePlayFlag == "Y",
          wagerToEdit.LayoffFlag);
    }

    private void InitializeForm() {
      FillAvailableTeasersDropDown();
      _allowRoundRobinTeasers = Params.AllowRoundRobinTeasers;

      if (_allowRoundRobinTeasers) EnableRoundRobinCheckBoxByDefault();

      if (FrmMode == "Edit") {
        _itemsToRemove = new List<Ticket.WagerAndItemNumber>();
        ItemFromMakeAWagerForm = false;
        _wagerEditBackup = Mdi.Ticket.CreateWagerSafeCopy(TicketNumberToUpd, WagerNumberToUpd);
        GetEditTeaserInfo();
        _performIdxChanged = false;
        cmbTeaser.SelectedIndex = ChangeSelectedTeaserName();
        _performIdxChanged = true;
        txtRisk.Text = _editRiskAmt;
        txtToWin.Text = _editToWinAmt;
      }

      TeaserName = cmbTeaser.Text;
      if (Teaser != null) {
        Teaser.TeaserName = TeaserName;
      }

      FillSelectedTeaserSportSpecs();
      FillMinMaxAllowedTeams();

      if (FrmMode == "Edit") {
        nudTeams.Value = int.Parse(_editChosenTeamsCnt);
        if ((FrmSportAndGameSelection.ReadbackItems != null && (FrmSportAndGameSelection.ReadbackItems.Any(rb => rb.TicketNumber == TicketNumberToUpd && rb.WagerNumber == WagerNumberToUpd)))) {
          _itemsBackupCopy = Mdi.Ticket.CreateWagerItemsListSafeCopy(TicketNumberToUpd, WagerNumberToUpd);
        }

        FillPlaceTeaserGvInEditMode();

        if (_editRoundRobinFlag && _allowRoundRobinTeasers) {
          grpRoundRobin.Enabled = true;
          chbRoundRobin.Checked = true;
          cmbRoundRobin.Text = _editSelectedRoundRobin;
          lblRoundRobinOutcome.Text = _editSelectedRRobinOutcome;
        }

        btnPlaceBet.Text = Resources.FrmPlaceTeaser_frmPlaceTeaser_Load_Modify_ + FrmSportAndGameSelection.SelectedWagerType;
      }

      if (Teaser == null && Mdi.OpenPlayWagerInfo != null) {
        var newWagerNumber = Mdi.Ticket.Wagers != null && Mdi.Ticket.Wagers.Any(w => w.TicketNumber == Mdi.TicketNumber) ? Mdi.Ticket.Wagers.Count(w => w.TicketNumber == Mdi.TicketNumber) + 1 : 1;
        Teaser = Teaser.Create(ModuleName, Mdi.OpenPlayWagerInfo);
        Teaser.TicketNumber = Mdi.TicketNumber;
        Teaser.Number = newWagerNumber;
        Mdi.Ticket.AddWager(Teaser.ToTicketWager(Teaser));
        var teaserItems = WagerItem.Get(Mdi.AppModuleInfo, int.Parse(Teaser.FromPreviousTicket), int.Parse(Teaser.FromPreviousWagerNumber), Mdi.TicketNumber, newWagerNumber, Mdi.OpenPlayItemsDescription);

        foreach (var item in teaserItems) {
          AddItem(TeaserItem.Convert(item));
        }
        var wagerItems = WagerItem.ToTicketWagerItem(Teaser.Items, Mdi.Ticket.TicketNumber);

        foreach (var item in wagerItems) {
          var activeGame = _frmSportAndGameSelection.ActiveGames.FirstOrDefault(a => item.PeriodNumber != null && (a.GameNum == item.GameNum && a.PeriodNumber == item.PeriodNumber));
          item.ShortDescription =
              String.IsNullOrEmpty(TwUtilities.BuildDescriptionAndAdjustedPrice(activeGame, item.WagerType, item.TotalPointsOu, item.ChosenTeamId, FrmSportAndGameSelection.GetAdjustedLine(item), item.PeriodDescription)) ?
              item.Description :
              TwUtilities.BuildDescriptionAndAdjustedPrice(activeGame, item.WagerType, item.TotalPointsOu, item.ChosenTeamId, FrmSportAndGameSelection.GetAdjustedLine(item), item.PeriodDescription);

          item.AlreadyBetItemCount = FrmSportAndGameSelection.GetMultiSelectionItemsCount(WagerType.TEASER, false, item);
          SyncTeaserAlreadyBetItemCount(item.ItemNumber, item.AlreadyBetItemCount);
          Mdi.Ticket.AddWagerItem(item);
        }
        nudTeams.Maximum = Teaser.TotalPicks;
        nudTeams.Value = Teaser.TotalPicks;

        foreach (DataGridViewRow row in dgvwPlaceTeaser.Rows) {
          var wagerItem = (from wi in wagerItems
                           where wi.TicketNumber == int.Parse(row.Cells[24].Value.ToString()) &&
                               wi.WagerNumber == int.Parse(row.Cells[3].Value.ToString()) &&
                               wi.ItemNumber == int.Parse(row.Cells[4].Value.ToString())
                           select wi).FirstOrDefault();
          if (wagerItem != null)
            row.Cells[1].Value = wagerItem.ShortDescription;
        }
      }

      if (Teaser != null && Teaser.IsOpen) {
        //Teaser.OriginalItemCount = Teaser.Items.Count;
        Teaser.OriginalItemCount = Teaser.Items.Count(wi => !string.IsNullOrEmpty(wi.FromPreviousTicket) && wi.FromPreviousTicket != Mdi.Ticket.TicketNumber.ToString(CultureInfo.InvariantCulture));
        grpRoundRobin.Enabled = cmbTeaser.Enabled = nudTeams.Enabled = txtRisk.Enabled = txtToWin.Enabled = false;
        txtRisk.Text = Teaser.AmountWagered.HasValue ? Teaser.AmountWagered.Value.ToString(CultureInfo.InvariantCulture) : "";
        Teaser.MinPicks = (int)nudTeams.Minimum;
        Teaser.MaxPicks = (int)nudTeams.Maximum;
        _byPassMinimumWager = true;

        if (FrmMode != "Edit") {
          txtToWin.Text = Teaser.RRToWinAmount.ToString(CultureInfo.InvariantCulture);
        }
        _performIdxChanged = false;

        if (Mdi.OpenPlayWagerInfo != null) {
          cmbTeaser.Text = Mdi.OpenPlayWagerInfo.TeaserName.Trim(); // possible null in Mdi.OpenPlayWagerInfo, must be checked
        }
        TeaserName = cmbTeaser.Text;
        _performIdxChanged = true;
        FillSelectedTeaserSportSpecs();
        FillMinMaxAllowedTeams();
      }
      SetCursorInTeaserCombo();
      DetermineWagerTypeMode();
      if (!FrmSportAndGameSelection.ShowWagerLimitsInFrm.IsActive()) return;
      FrmSportAndGameSelection.ShowWagerLimitsInFrm.FrmPlaceTeaser = this;
      int? teaserWagerNumber = null;
      var ticketNumber = Mdi.TicketNumber;
      if (Teaser != null) {
        ticketNumber = Teaser.TicketNumber;
        teaserWagerNumber = Teaser.Number;
      }
      FrmSportAndGameSelection.ShowWagerLimitsInFrm.DisplayTeaserMaxWagerLimits(ticketNumber, teaserWagerNumber);
    }

    private void SyncTeaserAlreadyBetItemCount(int targetItemNumber, int? alreadyBetItemCount) {
      foreach (var item in Teaser.Items) {
        if (item.Number != targetItemNumber)
          continue;
        item.AlreadyBetItemCount = alreadyBetItemCount;
      }
    }

    private void DetermineWagerTypeMode() {
      var selectedTeaser = "";
      if (cmbTeaser.SelectedValue != null)
        selectedTeaser = cmbTeaser.SelectedValue.ToString();
      var teamsCnt = nudTeams.Value;
      var frm = FormF.GetFormByName("MdiTicketWriter", this);
      if (frm == null)
        return;
      var mdi = (MdiTicketWriter)frm;
      if (mdi.TeasersPayCard == null || mdi.TeasersPayCard.Count == 0) {

        mdi.GetCustomerTeasersPayCard();
      }

      var teaserPayCard = (from p in mdi.TeasersPayCard where p.TeaserName.Trim() == selectedTeaser && p.GamesPicked == teamsCnt select p).FirstOrDefault();
      if (teaserPayCard == null) return;
      var moneyLine = teaserPayCard.MoneyLine;
      var toBase = teaserPayCard.ToBase;
      if (moneyLine != null && toBase != null) {
        WagerTypeModeTeaser = moneyLine > toBase ? "Risk" : "ToWin";
      }
    }

    private void ModifyWagerItem(DataGridViewCellEventArgs e) {
      if (dgvwPlaceTeaser.SelectedCells.Count == 0) {
        return;
      }
      if (e != null && e.ColumnIndex > 0) {
        return;
      }
      var boundItem = Teaser.Items.FirstOrDefault(wi => wi.Number == (int)dgvwPlaceTeaser.SelectedCells[0].OwningRow.Cells["Number"].Value);

      if (boundItem == null) {
        throw new Exception("Bound item not found");
      }
      if (!string.IsNullOrEmpty(boundItem.FromPreviousTicket)) {
        return;
      }
      var dgvwR = dgvwPlaceTeaser.SelectedCells[0].OwningRow;

      var gameIsAvailable = true;
      var gameNumber = int.Parse(dgvwR.Cells[9].Value.ToString());
      var periodNumber = int.Parse(dgvwR.Cells[12].Value.ToString());
      String gameStatus;
      var periodDescription = dgvwR.Cells[22].Value.ToString();

      if (!FrmSportAndGameSelection.GameIsAvailable(gameNumber, periodNumber, out gameStatus)) {
        MessageBox.Show(Resources.FrmTicketConfirmation_ShowRequiredFormInEditMode_Game_no_longer_available_, Resources.FrmTicketConfirmation_ShowRequiredFormInEditMode_Game_not_Available);
        gameIsAvailable = false;
      }

      if (!gameIsAvailable) return;
      var cellIdx = int.Parse(dgvwR.Cells[2].Value.ToString());
      var tNumber = int.Parse(dgvwR.Cells[24].Value.ToString());
      var wNumber = int.Parse(dgvwR.Cells[3].Value.ToString());
      var itNumber = int.Parse(dgvwR.Cells[4].Value.ToString());
      var selectedOdds = dgvwR.Cells[5].Value.ToString();
      var selectedBPointsOption = dgvwR.Cells[6].Value.ToString();
      var amtWagered = dgvwR.Cells[7].Value.ToString();
      var toWinAmt = dgvwR.Cells[8].Value.ToString();
      const string wagerTypeMode = "Risk";
      var priceType = dgvwR.Cells[11].Value.ToString();
      var teaserRowGroupNum = int.Parse(dgvwR.Cells[13].Value.ToString());
      var teaserPeriodWagerCutoff = DateTime.Parse(dgvwR.Cells[14].Value.ToString());
      var wageringMode = dgvwR.Cells[15].Value.ToString();
      var layoffWager = dgvwR.Cells[16].Value.ToString();
      var fixedPrice = dgvwR.Cells[17].Value.ToString();
      var pitcher1MStart = dgvwR.Cells[18].Value != null ? dgvwR.Cells[18].Value.ToString() : "";
      var pitcher2MStart = dgvwR.Cells[19].Value != null ? dgvwR.Cells[19].Value.ToString() : "";
      var totalsOuFlag = dgvwR.Cells[20].Value != null ? dgvwR.Cells[20].Value.ToString() : null;
      var selectedAmericanPrice = int.Parse(dgvwR.Cells[21].Value.ToString());

      FrmSportAndGameSelection.ShowFormInEditMode(gameNumber, periodNumber, periodDescription, teaserRowGroupNum, teaserPeriodWagerCutoff, /*rowIdx,*/ cellIdx, tNumber, wNumber, itNumber, selectedOdds, selectedAmericanPrice,
        selectedBPointsOption, amtWagered, toWinAmt, wagerTypeMode, _isFreePlayFlag, priceType, wageringMode, layoffWager, fixedPrice, pitcher1MStart, pitcher2MStart, "TeaserBox", totalsOuFlag);
    }

    private bool PlaceBet() {
      if (Teaser == null) {
        return false;
      }

      var message = "";

      if (chbRoundRobin.Checked) {
        message = ManageRoundRobin();
      }
      else {
        if (Mdi.Ticket.RoundRobinTeasers != null)
          Mdi.Ticket.RoundRobinTeasers.RemoveAll(p => p.TicketNumber == Teaser.TicketNumber && p.RrBaseWagerNumber == Teaser.Number);
      }

      if (bndsrc.List.Count > nudTeams.Maximum) {
        message = "Too many teams have been selected. A maximum of " + nudTeams.Maximum + " is permitted.";
      }
      if (Teaser.IsOpen && Teaser.Items.Count == Teaser.OriginalItemCount) {
        message = "Please add at least one team to this open teaser or press \"Cancel\" to leave it as is.";
      }
      if (message.Length > 0) {
        MessageBox.Show(message, ParameterList.GetItem("AppName").Value, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        return false;
      }
      if (Teaser.Number != null) {
        var wager = FrmMode == "Edit" ? _wagerEditBackup : Mdi.Ticket.GetWager(Teaser.TicketNumber, Teaser.Number.Value);

        var riskAmt = NumberF.ParseDouble(txtRisk.Text);
        wager.AmountWagered = riskAmt;
        var toWinAmt = NumberF.ParseDouble(txtToWin.Text);
        wager.ToWinAmount = toWinAmt;

        wager.TeaserName = cmbTeaser.Text;
        wager.TotalPicks = int.Parse(nudTeams.Value.ToString(CultureInfo.InvariantCulture));
        wager.Ties = Teaser.TeaserTies;
        wager.MinimumPicks = Teaser.MinPicks;
        wager.ContinueOnPushFlag = "Y";

        wager.RoundRobinLink = null;

        if (chbRoundRobin.Checked) {
          wager.RoundRobinLink = wager.WagerNumber;
          wager.SelectedRoundRobin = cmbRoundRobin.Text;
          wager.SelectedRRobinOutcome = lblRoundRobinOutcome.Text;
          wager.RoundRobinItemsCount = Teaser.RoundRobin.TotalTeasersCount;
        }

        wager.WagerStatus = dgvwPlaceTeaser.Rows.Count < nudTeams.Value ? "O" : "P";
        wager.FreePlayFlag = FrmSportAndGameSelection.IsFreePlay ? "Y" : "N";

        Mdi.Ticket.UpdateWager(wager);

        var wagerItems = Mdi.Ticket.GetWagerItems(Teaser.TicketNumber, Teaser.Number.Value);

        var updatedWagerItems = new List<Ticket.WagerItem>();

        foreach (var t in wagerItems) {
          var wi = t;
          wi.AmountWagered = riskAmt;
          wi.VolumeAmount = wi.AmountWagered;
          wi.TicketNumber = Teaser.TicketNumber;
          wi.ToWinAmount = 0;
          wi.FreePlayFlag = FrmSportAndGameSelection.IsFreePlay ? "Y" : "N";
          updatedWagerItems.Add(wi);
        }

        Mdi.Ticket.UpdateWagerItems(updatedWagerItems);


        FrmSportAndGameSelection.BuildReadbackLogInfoData(true);
        FrmSportAndGameSelection.PopulateReadbackLogInfoDgvw();
      }
      Mdi.WageringPanelName = null;
      WageringForm.Close(this);
      return true;
    }

    private void RebuildTeaserOptions() {
      if (_performIdxChanged) {
        FillMinMaxAllowedTeams();
        FillSelectedTeaserSportSpecs();
        CalculateToWinAmount();
        if (FrmSportAndGameSelection.ShowWagerLimitsInFrm.IsActive()) {
          FrmSportAndGameSelection.ShowWagerLimitsInFrm.FrmPlaceTeaser = this;
          int? teaserWagerNumber = null;
          var ticketNumber = Mdi.TicketNumber;
          if (Teaser != null) {
            ticketNumber = Teaser.TicketNumber;
            teaserWagerNumber = Teaser.Number;
          }
          FrmSportAndGameSelection.ShowWagerLimitsInFrm.DisplayTeaserMaxWagerLimits(ticketNumber, teaserWagerNumber);
        }
        TeaserName = cmbTeaser.Text;
        if (Teaser != null) {
          Mdi.Ticket.DeleteWager(Teaser.TicketNumber, Teaser.Number);
          bndsrc.Clear();
          dgvwPlaceTeaser.Refresh();
        }
        if (FrmMode != "Edit")
          Teaser = null;
      }
      grpRoundRobin.Enabled = _allowRoundRobinTeasers;
      if (MinPicks != MaxPicks) return;
      chbRoundRobin.Checked = false;
      cmbRoundRobin.SelectedIndex = -1;
      lblRoundRobinOutcome.Text = "";
      grpRoundRobin.Enabled = false;
      DetermineWagerTypeMode();
    }

    private void ResetBetType() {
      var frm = FormF.GetFormByName("frmSportAndGameSelection", this);

      if (frm == null) {
        return;
      }

      var frmSports = (FrmSportAndGameSelection)frm;

      if (frmSports.SelectedWagerType != null)
        frmSports.SelectedWagerType = "Straight Bet";

      Teaser = null;

      frm = FormF.GetFormByName("MdiTicketWriter", this);

      if (frm == null) {
        return;
      }

      var frmMdi = (MdiTicketWriter)frm;

      if (frmSports.SelectedWagerType != null)
        frmMdi.ToggleFpAndRifChecks(true);
    }

    private void ToggleContextMenuInability() {
      if (dgvwPlaceTeaser.SelectedCells.Count == 0) {
        return;
      }
      if (dgvwPlaceTeaser.SelectedCells[0].OwningRow.Cells["Number"].Value == null || Teaser.Items.Count == 0) {
        return;
      }
      var boundItem = Teaser.Items.FirstOrDefault(wi => wi.Number == (int)dgvwPlaceTeaser.SelectedCells[0].OwningRow.Cells["Number"].Value);

      if (boundItem == null) {
        throw new Exception("Bound item not found");
      }
      string retStr;
      if (string.IsNullOrEmpty(boundItem.FromPreviousTicket) && FrmSportAndGameSelection.GameIsAvailable(boundItem.GameNumber, boundItem.PeriodNumber, out retStr)) {
        ctxModifyWager.Enabled = true;

        foreach (ToolStripItem toolstrip in ctxModifyWager.Items) {
          toolstrip.Enabled = true;
        }
      }
      else {
        ctxModifyWager.Enabled = false;
      }
    }

    private void UpdateTeaser(object sender, ListChangedEventArgs e) {
      if (Teaser == null) {
        return;
      }
      switch (e.ListChangedType) {
        case ListChangedType.ItemAdded:
          Teaser.Items.Add((TeaserItem)((BindingSource)sender).List[e.NewIndex]);

          if (FrmMode == "Edit" && ItemFromMakeAWagerForm) {
            _itemsToRemove.Add(new Ticket.WagerAndItemNumber {
              GameNumber = ((TeaserItem)((BindingSource)sender).List[e.NewIndex]).GameNumber,
              PeriodNumber = ((TeaserItem)((BindingSource)sender).List[e.NewIndex]).PeriodNumber,
              TicketNumber = ((TeaserItem)((BindingSource)sender).List[e.NewIndex]).TicketNumber,
              WagerNumber = Teaser.Number ?? 0,
              ItemNumber = ((TeaserItem)((BindingSource)sender).List[e.NewIndex]).Number,
              ItemDescription = ((TeaserItem)((BindingSource)sender).List[e.NewIndex]).Description
            });
          }

          break;
        case ListChangedType.ItemChanged:
          Teaser.Items[e.NewIndex] = (TeaserItem)((BindingSource)sender).List[e.NewIndex];
          break;
        case ListChangedType.ItemDeleted:
          if (Mdi.TicketNumber != Teaser.TicketNumber) {
            Ticket.WagerItem wi = null;
            if (Mdi.Ticket.WagerItems != null)
              wi = (from wi2 in Mdi.Ticket.WagerItems where wi2.TicketNumber == Teaser.TicketNumber && wi2.WagerNumber == Teaser.Number && wi2.ItemNumber == (e.NewIndex + 1) select wi2).FirstOrDefault();

            Ticket.ContestWagerItem cwi = null;
            if (Mdi.Ticket.ContestWagerItems != null)
              cwi = (from wi3 in Mdi.Ticket.ContestWagerItems where wi3.TicketNumber == Teaser.TicketNumber && wi3.WagerNumber == Teaser.Number && wi3.ItemNumber == (e.NewIndex + 1) select wi3).FirstOrDefault();
            Mdi.Ticket.AddToItemsToRemoveFromDb("T", wi, cwi);
          }
          Teaser.Items.RemoveAt(e.NewIndex);
          Mdi.Ticket.DeleteWagerItem(Teaser.TicketNumber, Teaser.Number, e.NewIndex + 1);
          break;
      }

      Teaser.ItemUpdated = true;
      CalculateToWinAmount();
      if (Teaser.RoundRobin != null) {
        Teaser.RoundRobin.SeedItems = Teaser.Items;
        Teaser.RoundRobin.ResetTeaserCombinations();
      }
      if (!FrmSportAndGameSelection.ShowWagerLimitsInFrm.IsActive()) return;
      FrmSportAndGameSelection.ShowWagerLimitsInFrm.FrmPlaceTeaser = this;
      int? teaserWagerNumber = null;
      var ticketNumber = Mdi.TicketNumber;
      if (Teaser != null) {
        ticketNumber = Teaser.TicketNumber;
        teaserWagerNumber = Teaser.Number;
      }
      FrmSportAndGameSelection.ShowWagerLimitsInFrm.DisplayTeaserMaxWagerLimits(ticketNumber, teaserWagerNumber);
    }

    private void ValidateWager() {
      if (bndsrc.Count == 0) {
        MessageBox.Show(@"At least one item must be selected at this time", @"Invalid Selection");
        _deleteWagersOnExit = true;
        return;
      }

      if (chbRoundRobin.Checked && cmbRoundRobin.SelectedIndex > -1) {
        Teaser.RoundRobin = null;
        EnableRoundRobin(true);
      }

      if (chbRoundRobin.Checked && cmbRoundRobin.SelectedIndex == -1) {
        const string message = "Please select the teaser combinations for the round robin";
        MessageBox.Show(message, ParameterList.GetItem("AppName").Value, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        cmbRoundRobin.Focus();
        _deleteWagersOnExit = true;
        return;
      }
      if (_betAmountMode == "Risk") CalculateToWinAmount();
      else CalculateRiskAmount();

      Boolean continueValidating;
      Boolean minWagerBypassed;
      Boolean availableFundsOk;
      Boolean wagerLimitOk;

      var wagerDescription = "";
      wagerDescription += "Teaser: " + cmbTeaser.Text.Trim();
      wagerDescription += " ";

      if (nudTeams.Value == 1) {
        wagerDescription += nudTeams.Value + " team";
      }
      else {
        wagerDescription += nudTeams.Value + " teams";
      }

      wagerDescription += " ";

      if (dgvwPlaceTeaser.Rows.Count > 0) {
        for (var i = 0; i < dgvwPlaceTeaser.Rows.Count; i++) {
          if (i == dgvwPlaceTeaser.Rows.Count - 1) {
            wagerDescription += dgvwPlaceTeaser.Rows[i].Cells[1].Value;
          }
          else {
            wagerDescription += dgvwPlaceTeaser.Rows[i].Cells[1].Value + " ";
          }
        }
      }

      var riskAmount = txtRisk.Text;

      if (chbRoundRobin.Checked) {
        if (Teaser.RoundRobin != null && txtRisk.Text.Length != 0) {
          riskAmount = (Teaser.RoundRobin.TotalTeasersCount * NumberF.ParseDouble(txtRisk.Text)).ToString(CultureInfo.InvariantCulture);
        }
      }

      FrmSportAndGameSelection.CheckIfBelowMinimumWager(this, LoginsAndProfiles.ACCEPT_ANY_WAGER, txtRisk.Text, out continueValidating, out minWagerBypassed, false, false, _byPassMinimumWager);

      if (!continueValidating) {
        _deleteWagersOnExit = true;
        return;
      }

      if (Teaser.Status != "O")
        FrmSportAndGameSelection.CheckForParlayTeaserAvailableFunds(this, FrmMode, Teaser.TicketNumber, Teaser.Number, LoginsAndProfiles.ACCEPT_ANY_WAGER, wagerDescription, riskAmount, txtToWin.Text, FrmSportAndGameSelection.IsFreePlay, out continueValidating, out availableFundsOk); //Validate if user has sufficient Funds for wager
      else {
        availableFundsOk = true;
      }

      if (!continueValidating) {
        _deleteWagersOnExit = true;
        return;
      }

      double numericRiskAmount;
      double.TryParse(txtRisk.Text, out numericRiskAmount);
      double numericToWinAmount;
      double.TryParse(txtToWin.Text, out numericToWinAmount);

      var volumeAmt = GetVolumeAmount(numericRiskAmount, numericToWinAmount);

      FrmSportAndGameSelection.CheckIfParlayOrTeaserExceedsWagerLimit(this, Teaser.TicketNumber, Teaser.Number, LoginsAndProfiles.ACCEPT_ANY_WAGER, wagerDescription, dgvwPlaceTeaser.Rows.Count < nudTeams.Value ? "O" : "P", txtRisk.Text, txtToWin.Text, volumeAmt, out continueValidating, out wagerLimitOk);

      var betPlaced = false;

      if (!wagerLimitOk) {
        _deleteWagersOnExit = true;
        return;
      }

      if (minWagerBypassed && availableFundsOk)
        betPlaced = PlaceBet();
      if (!betPlaced || Mdi.OpenPlayWagerInfo == null) return;
      var amountsStr = " Risking: " + txtRisk.Text + ", to Win:" + txtToWin.Text + ", adding " + (Teaser.Items.Count) + (Teaser.Items.Count == 1 ? " item" : " items");

      Tag = "Added " + FrmSportAndGameSelection.SelectedWagerType + " to Ticket in " + (String.IsNullOrEmpty(FrmMode) ? "New" : FrmMode) + " mode. Customer: " + FrmSportAndGameSelection.Customer.CustomerID.Trim()
            + " " + Teaser.TicketNumber.ToString() + "." + (Teaser.WagerNumber ?? 0).ToString() + amountsStr;
      Mdi.OpenPlayWagerInfo = null;
    }

    private double GetVolumeAmount(double riskAmt, double toWinAmt) {
      var minPicks = 2;
      var maxPicks = 2;

      var itemsCnt = int.Parse(nudTeams.Value.ToString(CultureInfo.InvariantCulture));//bndsrc.Count;

      if (SelectedTeaserSpec != null) {
        minPicks = SelectedTeaserSpec.MinPicks ?? 2;
        maxPicks = SelectedTeaserSpec.MaxPicks ?? 2;
      }

      if (minPicks == maxPicks || itemsCnt == 2) {
        return toWinAmt;
      }

      if (itemsCnt > 2 && minPicks != maxPicks) {
        return riskAmt;
      }
      return 0;
    }

    private void SetCentsNumericInputs() {
      txtRisk.AllowCents = Params.IncludeCents;
      txtToWin.AllowCents = Params.IncludeCents;
    }

    #endregion

    #region Protected Methods

    protected override bool ProcessCmdKey(ref Message msg, Keys keyData) {
      return Mdi.ActivateMdiChildForm(keyData, base.ProcessCmdKey(ref msg, keyData), ActiveMdiChildName);
    }

    #endregion

    #region Events

    private void frmPlaceTeaser_Load(object sender, EventArgs e) {
      InitializeForm();
    }

    private void btnCancel_Click(object sender, EventArgs e) {
      _cancelActionPerformed = true;
      if (FrmSportAndGameSelection.ReadbackItems != null &&
              FrmSportAndGameSelection.ReadbackItems.Any(p => p.TicketNumber == TicketNumberToUpd && p.WagerNumber == WagerNumberToUpd && p.RowContainsEditItemInfo)) {
        _deleteWagersOnExit = false;
      }
      CancelWager();
    }

    private void btnPayTable_Click(object sender, EventArgs e) {
      PayCard.Show(this, cmbTeaser.Text, "Teaser Pay Card", ModuleName, "Teaser");
    }

    private void btnPlaceBet_Click(object sender, EventArgs e) {
      _deleteWagersOnExit = false;
      ValidateWager();
    }

    private void cmbTeaser_SelectedIndexChanged(object sender, EventArgs e) {
      RebuildTeaserOptions();
    }

    private void deleteToolStripMenuItem_Click(object sender, EventArgs e) {
      DeleteWagerItem();
    }

    private void dgvwPlaceTeaser_CellDoubleClick(object sender, DataGridViewCellEventArgs e) {
      ModifyWagerItem(e);
    }

    private void dgvwPlaceTeaser_CellStateChanged(object sender, DataGridViewCellStateChangedEventArgs e) {
      ForceFirstCellSelection(e);
    }

    private void dgvwPlaceTeaser_MouseDown(object sender, MouseEventArgs e) {
      if (e.Button != MouseButtons.Right) return;
      var hti = dgvwPlaceTeaser.HitTest(e.X, e.Y);

      if (hti.RowIndex < 0 || hti.ColumnIndex < 0) return;
      dgvwPlaceTeaser.ClearSelection();
      dgvwPlaceTeaser.Rows[hti.RowIndex].Cells[hti.ColumnIndex].Selected = true;
    }

    private void editToolStripMenuItem_Click(object sender, EventArgs e) {
      ModifyWagerItem(null);
    }

    private void FrmPlaceTeaser_FormClosed(object sender, FormClosedEventArgs e) {
      if (ActiveMdiChildName == null) {
        return;
      }
      foreach (var mdiChild in Mdi.MdiChildren) {
        if (mdiChild.Name.ToUpper() != ActiveMdiChildName.ToUpper()) continue;
        mdiChild.Select();
        break;
      }
      _itemsToRemove = null;
      _wagerEditBackup = null;
      _itemsBackupCopy = null;
    }

    private void frmPlaceTeaser_FormClosing(object sender, FormClosingEventArgs e) {
      CancelWager(false);
      ResetBetType();
    }

    private void nudTeams_ValueChanged(object sender, EventArgs e) {
      CalculateToWinAmount();
      DetermineWagerTypeMode();
      if (!FrmSportAndGameSelection.ShowWagerLimitsInFrm.IsActive()) return;
      FrmSportAndGameSelection.ShowWagerLimitsInFrm.FrmPlaceTeaser = this;
      int? teaserWagerNumber = null;
      var ticketNumber = Mdi.TicketNumber;
      if (Teaser != null) {
        ticketNumber = Teaser.TicketNumber;
        teaserWagerNumber = Teaser.Number;
      }
      FrmSportAndGameSelection.ShowWagerLimitsInFrm.DisplayTeaserMaxWagerLimits(ticketNumber, teaserWagerNumber);
    }

    private void txtRisk_KeyUp(object sender, KeyEventArgs e) {
      if (!KeyValidator.IsValidNumericKey(e)) return;
      CalculateToWinAmount();
      if (!FrmSportAndGameSelection.ShowWagerLimitsInFrm.IsActive()) return;
      FrmSportAndGameSelection.ShowWagerLimitsInFrm.FrmPlaceTeaser = this;
      int? teaserWagerNumber = null;
      var ticketNumber = Mdi.TicketNumber;
      if (Teaser != null) {
        ticketNumber = Teaser.TicketNumber;
        teaserWagerNumber = Teaser.Number;
      }
      FrmSportAndGameSelection.ShowWagerLimitsInFrm.DisplayTeaserMaxWagerLimits(ticketNumber, teaserWagerNumber);
    }

    private void txtToWin_KeyUp(object sender, KeyEventArgs e) {
      if (KeyValidator.IsValidNumericKey(e) && !chbRoundRobin.Checked)
        CalculateRiskAmount();
    }

    private void bndsrc_ListChanged(object sender, ListChangedEventArgs e) {
      UpdateTeaser(sender, e);
    }

    private void dgvwPlaceTeaser_SelectionChanged(object sender, EventArgs e) {
      ToggleContextMenuInability();
    }

    private void nudTeams_Enter(object sender, EventArgs e) {
      SetCursorInTeamNumberBox();
    }

    private void txtSportsSpecsInfo_Enter(object sender, EventArgs e) {
      ((TextBox)sender).Select(0, ((TextBox)sender).Text.Length);
    }

    private void txtRisk_KeyPress(object sender, KeyPressEventArgs e) {
      _betAmountMode = "Risk";
    }

    private void txtToWin_KeyPress(object sender, KeyPressEventArgs e) {
      _betAmountMode = "ToWin";
    }

    #endregion

    private void chbRoundRobin_CheckedChanged(object sender, EventArgs e) {
      if (Teaser == null) return;
      SetRoundRobinMode(((CheckBox)sender).Checked);
      CalculateToWinAmount();
      if (!FrmSportAndGameSelection.ShowWagerLimitsInFrm.IsActive()) return;
      FrmSportAndGameSelection.ShowWagerLimitsInFrm.FrmPlaceTeaser = this;
      int? teaserWagerNumber = null;
      var ticketNumber = Mdi.TicketNumber;
      if (Teaser != null) {
        ticketNumber = Teaser.TicketNumber;
        teaserWagerNumber = Teaser.Number;
      }
      FrmSportAndGameSelection.ShowWagerLimitsInFrm.DisplayTeaserMaxWagerLimits(ticketNumber, teaserWagerNumber);
    }

    private void cmbRoundRobin_SelectedIndexChanged(object sender, EventArgs e) {
      if (!chbRoundRobin.Checked)
        chbRoundRobin.Checked = true;
      if (Teaser != null && Teaser.RoundRobin != null)
        SetRoundRobinItemsCalculation();
    }

    private void EnableRoundRobin(bool enable) {
      if (Teaser == null) {
        chbRoundRobin.Checked = false;
        return;
      }
      cmbRoundRobin.Enabled = enable;

      if (cmbRoundRobin.Enabled) {
        if (cmbRoundRobin.Items.Count == 0) {
          PopulateRounRobinTypeCmb();
        }
        if (Teaser.Number != null) {
          var roundRobin = new RoundRobin(Teaser.TicketNumber, Teaser.Number.Value, Mdi.AppModuleInfo) {
            ModuleName = Teaser.ModuleName,
            TeaserName = Teaser.TeaserName,
            SeedItems = Teaser.Items,
            TeaserMinPicks = Teaser.MinPicks,
            TeaserMaxPicks = Teaser.MaxPicks,
            TeaserTies = Teaser.TeaserTies
          };
          Teaser.RoundRobin = roundRobin;
        }
        SetRoundRobinItemsCalculation();
        txtToWin.Text = "";
      }
      else {
        Teaser.RoundRobin = null;
        cmbRoundRobin.SelectedIndex = -1;
        lblRoundRobinOutcome.Text = "";
      }
      Teaser.ItemUpdated = true;
      CalculateToWinAmount();
      if (!FrmSportAndGameSelection.ShowWagerLimitsInFrm.IsActive()) return;
      FrmSportAndGameSelection.ShowWagerLimitsInFrm.FrmPlaceTeaser = this;
      int? teaserWagerNumber = null;
      var ticketNumber = Mdi.TicketNumber;
      if (Teaser != null) {
        ticketNumber = Teaser.TicketNumber;
        teaserWagerNumber = Teaser.Number;
      }
      FrmSportAndGameSelection.ShowWagerLimitsInFrm.DisplayTeaserMaxWagerLimits(ticketNumber, teaserWagerNumber);
    }

    private void EnableRoundRobinCheckBoxByDefault() {

      if (FrmMode == "Edit" && TicketNumberToUpd != Mdi.TicketNumber) {
        grpRoundRobin.Enabled = false;
        return;
      }
      chbRoundRobin.Enabled = true;
      cmbRoundRobin.Enabled = true;
      if (cmbRoundRobin.Items.Count == 0) {
        PopulateRounRobinTypeCmb();
      }

      if (MinPicks != 2 && MinPicks != MaxPicks) return;
      chbRoundRobin.Checked = false;
      cmbRoundRobin.SelectedIndex = -1;
      lblRoundRobinOutcome.Text = "";
      grpRoundRobin.Enabled = false;
    }

    private string ManageRoundRobin() {
      if (Teaser.RoundRobin == null) {
        return "";
      }
      var message = "";

      if (bndsrc.List.Count < 3) {
        message = "At least three wagers must be entered for a valid round robin.";
      }
      if (message.Length == 0 && bndsrc.List.Count != nudTeams.Value) {
        message = "The teams entry must be equal to the number of teams selected for a round robin.";
      }
      if (message.Length == 0 && cmbRoundRobin.SelectedIndex < 0) {
        message = "Please select the teaser combinations for the round robin";
      }
      else if (message.Length == 0 && !Teaser.RoundRobin.IsValidTeaserCombination) {
        message = "The teaser combinations selected for the round robin are not correct for the number of selections";
      }
      if (message.Length > 0) {
        return message;
      }
      if (Mdi.Ticket.RoundRobinTeasers == null) {
        Mdi.Ticket.RoundRobinTeasers = new List<Teaser>();
      }

      Mdi.Ticket.RoundRobinTeasers.RemoveAll(p => p.TicketNumber == Teaser.TicketNumber && p.RrBaseWagerNumber == Teaser.Number);

      var itemCount = 1;

      foreach (var combination in Teaser.RoundRobin.TeaserCombinations) {
        foreach (var teaser in combination.Teasers) {
          teaser.Type = Teaser.Type;
          if (Teaser.Number != null) teaser.RrBaseWagerNumber = (int)Teaser.Number;
          teaser.RRARBCBaseWagerItemNumber = itemCount;
          Mdi.Ticket.RoundRobinTeasers.Add(teaser);
          itemCount++;
        }
      }

      Mdi.Ticket.RoundRobinTeasers = Mdi.Ticket.RoundRobinTeasers.Where(p => p.TicketNumber == Teaser.TicketNumber).OrderBy(x => x.RrBaseWagerNumber).ThenBy(x => x.RRARBCBaseWagerItemNumber).ToList();

      return message;
    }

    private void PopulateRounRobinTypeCmb() {
      Common.PopulateRounRobinTypeCmb(bndsrcRoundRobinType);
      cmbRoundRobin.SelectedIndexChanged -= cmbRoundRobin_SelectedIndexChanged;
      cmbRoundRobin.SelectedIndex = -1;
      cmbRoundRobin.SelectedIndexChanged += cmbRoundRobin_SelectedIndexChanged;
    }

    private void SetRoundRobinItemsCalculation() {
      if (nudTeams.Value < 3 || !chbRoundRobin.Checked) {
        lblRoundRobinOutcome.Text = "";
      }
      if (!chbRoundRobin.Checked || cmbRoundRobin.SelectedIndex < 0) {
        return;
      }
      var itemsPerTeaserList = new List<string>(cmbRoundRobin.Text.Replace("'s", "").Split(','));
      Teaser.RoundRobin.ItemsPerTeaser = new List<int>();
      var result = "";

      foreach (var itemsPerTeaserText in itemsPerTeaserList) {
        var itemsPerTeaser = int.Parse(itemsPerTeaserText.Trim());
        Teaser.RoundRobin.ItemsPerTeaser.Add(itemsPerTeaser);
        var combination = NumberF.Combination((int)nudTeams.Value, itemsPerTeaser);

        if (combination == 0) {
          break;
        }
        result += combination.ToString(CultureInfo.InvariantCulture) + " - " + itemsPerTeaserText.Trim() + "'s, ";
      }
      if (result.Length == 0) {
        return;
      }
      Teaser.RoundRobin.ResetTeaserCombinations();
      Teaser.ItemUpdated = true;
      lblRoundRobinOutcome.Text = result.Substring(0, result.Length - 2);
      CalculateToWinAmount();
      if (Teaser.RoundRobin.IsValidTeaserCombination) {
        if (FrmSportAndGameSelection.ShowWagerLimitsInFrm.IsActive()) {
          FrmSportAndGameSelection.ShowWagerLimitsInFrm.FrmPlaceTeaser = this;
          int? teaserWagerNumber = null;
          var ticketNumber = Mdi.TicketNumber;
          if (Teaser != null) {
            ticketNumber = Teaser.TicketNumber;
            teaserWagerNumber = Teaser.Number;
          }
          FrmSportAndGameSelection.ShowWagerLimitsInFrm.DisplayTeaserMaxWagerLimits(ticketNumber, teaserWagerNumber);
        }
        for (var j = 0; j < Teaser.Items.Count; j++) {
          Teaser.Items[j].Number = j + 1;
        }
      }
      else {
        txtToWin.Text = "";
      }
    }

    private void SetRoundRobinMode(bool enable) {
      EnableRoundRobin(enable);
    }
  }
}