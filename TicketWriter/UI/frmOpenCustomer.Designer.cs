﻿namespace TicketWriter.UI
{
    partial class FrmOpenCustomer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtCustomerPIN = new System.Windows.Forms.TextBox();
            this.lblCustomerPIN = new System.Windows.Forms.Label();
            this.btnLast = new System.Windows.Forms.Button();
            this.lblPassword = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.grpLastVerified = new System.Windows.Forms.GroupBox();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.lblDate = new System.Windows.Forms.Label();
            this.txtBalance = new System.Windows.Forms.TextBox();
            this.lblBalance = new System.Windows.Forms.Label();
            this.grpPendingWagers = new System.Windows.Forms.GroupBox();
            this.txtPendingWagersCreditAmount = new System.Windows.Forms.TextBox();
            this.lblPendingWagersCreditAmount = new System.Windows.Forms.Label();
            this.txtPendingWagersDebitAmount = new System.Windows.Forms.TextBox();
            this.lblPendingWagersDebitAmount = new System.Windows.Forms.Label();
            this.txtPendingWagersCount = new System.Windows.Forms.TextBox();
            this.lblPendingWagersCount = new System.Windows.Forms.Label();
            this.grpPendingFreePlays = new System.Windows.Forms.GroupBox();
            this.lblPendingFreePlaysCount = new System.Windows.Forms.Label();
            this.lblFreeplayAmt = new System.Windows.Forms.Label();
            this.txtPendingFreePlaysAmount = new System.Windows.Forms.TextBox();
            this.txtPendingFreePlaysCount = new System.Windows.Forms.TextBox();
            this.btnBalanceInfo = new System.Windows.Forms.Button();
            this.btnDailyFigure = new System.Windows.Forms.Button();
            this.btnRetrieve = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblMessages = new System.Windows.Forms.Label();
            this.lblCurrentBalance = new System.Windows.Forms.Label();
            this.txtCurrentBalance = new System.Windows.Forms.TextBox();
            this.txtFreePlayBalance = new System.Windows.Forms.TextBox();
            this.lblFreePlayBalance = new System.Windows.Forms.Label();
            this.btnHistory = new System.Windows.Forms.Button();
            this.btnVerified = new System.Windows.Forms.Button();
            this.grpLastVerified.SuspendLayout();
            this.grpPendingWagers.SuspendLayout();
            this.grpPendingFreePlays.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtCustomerPIN
            // 
            this.txtCustomerPIN.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCustomerPIN.Location = new System.Drawing.Point(100, 12);
            this.txtCustomerPIN.Name = "txtCustomerPIN";
            this.txtCustomerPIN.Size = new System.Drawing.Size(92, 20);
            this.txtCustomerPIN.TabIndex = 0;
            this.txtCustomerPIN.Text = "TW";
            this.txtCustomerPIN.TextChanged += new System.EventHandler(this.txtCustomerPIN_TextChanged);
            // 
            // lblCustomerPIN
            // 
            this.lblCustomerPIN.AutoSize = true;
            this.lblCustomerPIN.Location = new System.Drawing.Point(11, 15);
            this.lblCustomerPIN.Name = "lblCustomerPIN";
            this.lblCustomerPIN.Size = new System.Drawing.Size(75, 13);
            this.lblCustomerPIN.TabIndex = 1;
            this.lblCustomerPIN.Text = "Customer PIN:";
            // 
            // btnLast
            // 
            this.btnLast.Location = new System.Drawing.Point(199, 11);
            this.btnLast.Name = "btnLast";
            this.btnLast.Size = new System.Drawing.Size(44, 23);
            this.btnLast.TabIndex = 2;
            this.btnLast.Text = "Last";
            this.btnLast.UseVisualStyleBackColor = true;
            this.btnLast.Click += new System.EventHandler(this.btnLast_Click);
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(32, 43);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(56, 13);
            this.lblPassword.TabIndex = 4;
            this.lblPassword.Text = "Password:";
            this.lblPassword.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(100, 39);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.ReadOnly = true;
            this.txtPassword.Size = new System.Drawing.Size(143, 20);
            this.txtPassword.TabIndex = 3;
            // 
            // grpLastVerified
            // 
            this.grpLastVerified.Controls.Add(this.txtDate);
            this.grpLastVerified.Controls.Add(this.lblDate);
            this.grpLastVerified.Controls.Add(this.txtBalance);
            this.grpLastVerified.Controls.Add(this.lblBalance);
            this.grpLastVerified.Location = new System.Drawing.Point(252, 12);
            this.grpLastVerified.Name = "grpLastVerified";
            this.grpLastVerified.Size = new System.Drawing.Size(295, 72);
            this.grpLastVerified.TabIndex = 5;
            this.grpLastVerified.TabStop = false;
            this.grpLastVerified.Text = "Last Verified";
            // 
            // txtDate
            // 
            this.txtDate.Location = new System.Drawing.Point(63, 45);
            this.txtDate.Name = "txtDate";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(162, 20);
            this.txtDate.TabIndex = 3;
            this.txtDate.Visible = false;
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(24, 48);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(33, 13);
            this.lblDate.TabIndex = 2;
            this.lblDate.Text = "Date:";
            this.lblDate.Visible = false;
            // 
            // txtBalance
            // 
            this.txtBalance.Location = new System.Drawing.Point(63, 20);
            this.txtBalance.Name = "txtBalance";
            this.txtBalance.ReadOnly = true;
            this.txtBalance.Size = new System.Drawing.Size(100, 20);
            this.txtBalance.TabIndex = 1;
            this.txtBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBalance.Visible = false;
            // 
            // lblBalance
            // 
            this.lblBalance.AutoSize = true;
            this.lblBalance.Location = new System.Drawing.Point(8, 23);
            this.lblBalance.Name = "lblBalance";
            this.lblBalance.Size = new System.Drawing.Size(49, 13);
            this.lblBalance.TabIndex = 0;
            this.lblBalance.Text = "Balance:";
            this.lblBalance.Visible = false;
            // 
            // grpPendingWagers
            // 
            this.grpPendingWagers.Controls.Add(this.txtPendingWagersCreditAmount);
            this.grpPendingWagers.Controls.Add(this.lblPendingWagersCreditAmount);
            this.grpPendingWagers.Controls.Add(this.txtPendingWagersDebitAmount);
            this.grpPendingWagers.Controls.Add(this.lblPendingWagersDebitAmount);
            this.grpPendingWagers.Controls.Add(this.txtPendingWagersCount);
            this.grpPendingWagers.Controls.Add(this.lblPendingWagersCount);
            this.grpPendingWagers.Location = new System.Drawing.Point(252, 90);
            this.grpPendingWagers.Name = "grpPendingWagers";
            this.grpPendingWagers.Size = new System.Drawing.Size(138, 112);
            this.grpPendingWagers.TabIndex = 6;
            this.grpPendingWagers.TabStop = false;
            this.grpPendingWagers.Text = "Pending Wagers";
            // 
            // txtPendingWagersCreditAmount
            // 
            this.txtPendingWagersCreditAmount.Location = new System.Drawing.Point(63, 76);
            this.txtPendingWagersCreditAmount.Name = "txtPendingWagersCreditAmount";
            this.txtPendingWagersCreditAmount.ReadOnly = true;
            this.txtPendingWagersCreditAmount.Size = new System.Drawing.Size(66, 20);
            this.txtPendingWagersCreditAmount.TabIndex = 7;
            this.txtPendingWagersCreditAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPendingWagersCreditAmount.Visible = false;
            // 
            // lblPendingWagersCreditAmount
            // 
            this.lblPendingWagersCreditAmount.AutoSize = true;
            this.lblPendingWagersCreditAmount.Location = new System.Drawing.Point(8, 79);
            this.lblPendingWagersCreditAmount.Name = "lblPendingWagersCreditAmount";
            this.lblPendingWagersCreditAmount.Size = new System.Drawing.Size(58, 13);
            this.lblPendingWagersCreditAmount.TabIndex = 6;
            this.lblPendingWagersCreditAmount.Text = "Credit Amt:";
            this.lblPendingWagersCreditAmount.Visible = false;
            // 
            // txtPendingWagersDebitAmount
            // 
            this.txtPendingWagersDebitAmount.Location = new System.Drawing.Point(63, 50);
            this.txtPendingWagersDebitAmount.Name = "txtPendingWagersDebitAmount";
            this.txtPendingWagersDebitAmount.ReadOnly = true;
            this.txtPendingWagersDebitAmount.Size = new System.Drawing.Size(66, 20);
            this.txtPendingWagersDebitAmount.TabIndex = 5;
            this.txtPendingWagersDebitAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPendingWagersDebitAmount.Visible = false;
            // 
            // lblPendingWagersDebitAmount
            // 
            this.lblPendingWagersDebitAmount.AutoSize = true;
            this.lblPendingWagersDebitAmount.Location = new System.Drawing.Point(8, 53);
            this.lblPendingWagersDebitAmount.Name = "lblPendingWagersDebitAmount";
            this.lblPendingWagersDebitAmount.Size = new System.Drawing.Size(56, 13);
            this.lblPendingWagersDebitAmount.TabIndex = 4;
            this.lblPendingWagersDebitAmount.Text = "Debit Amt:";
            this.lblPendingWagersDebitAmount.Visible = false;
            // 
            // txtPendingWagersCount
            // 
            this.txtPendingWagersCount.Location = new System.Drawing.Point(63, 24);
            this.txtPendingWagersCount.Name = "txtPendingWagersCount";
            this.txtPendingWagersCount.ReadOnly = true;
            this.txtPendingWagersCount.Size = new System.Drawing.Size(66, 20);
            this.txtPendingWagersCount.TabIndex = 3;
            this.txtPendingWagersCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPendingWagersCount.Visible = false;
            // 
            // lblPendingWagersCount
            // 
            this.lblPendingWagersCount.AutoSize = true;
            this.lblPendingWagersCount.Location = new System.Drawing.Point(24, 27);
            this.lblPendingWagersCount.Name = "lblPendingWagersCount";
            this.lblPendingWagersCount.Size = new System.Drawing.Size(38, 13);
            this.lblPendingWagersCount.TabIndex = 2;
            this.lblPendingWagersCount.Text = "Count:";
            this.lblPendingWagersCount.Visible = false;
            // 
            // grpPendingFreePlays
            // 
            this.grpPendingFreePlays.Controls.Add(this.lblPendingFreePlaysCount);
            this.grpPendingFreePlays.Controls.Add(this.lblFreeplayAmt);
            this.grpPendingFreePlays.Controls.Add(this.txtPendingFreePlaysAmount);
            this.grpPendingFreePlays.Controls.Add(this.txtPendingFreePlaysCount);
            this.grpPendingFreePlays.Location = new System.Drawing.Point(396, 90);
            this.grpPendingFreePlays.Name = "grpPendingFreePlays";
            this.grpPendingFreePlays.Size = new System.Drawing.Size(151, 112);
            this.grpPendingFreePlays.TabIndex = 8;
            this.grpPendingFreePlays.TabStop = false;
            this.grpPendingFreePlays.Text = "Pending Free Plays";
            // 
            // lblPendingFreePlaysCount
            // 
            this.lblPendingFreePlaysCount.AutoSize = true;
            this.lblPendingFreePlaysCount.Location = new System.Drawing.Point(6, 27);
            this.lblPendingFreePlaysCount.Name = "lblPendingFreePlaysCount";
            this.lblPendingFreePlaysCount.Size = new System.Drawing.Size(38, 13);
            this.lblPendingFreePlaysCount.TabIndex = 8;
            this.lblPendingFreePlaysCount.Text = "Count:";
            this.lblPendingFreePlaysCount.Visible = false;
            // 
            // lblFreeplayAmt
            // 
            this.lblFreeplayAmt.AutoSize = true;
            this.lblFreeplayAmt.Location = new System.Drawing.Point(6, 53);
            this.lblFreeplayAmt.Name = "lblFreeplayAmt";
            this.lblFreeplayAmt.Size = new System.Drawing.Size(71, 13);
            this.lblFreeplayAmt.TabIndex = 8;
            this.lblFreeplayAmt.Text = "Freeplay Amt:";
            this.lblFreeplayAmt.Visible = false;
            // 
            // txtPendingFreePlaysAmount
            // 
            this.txtPendingFreePlaysAmount.Location = new System.Drawing.Point(79, 50);
            this.txtPendingFreePlaysAmount.Name = "txtPendingFreePlaysAmount";
            this.txtPendingFreePlaysAmount.ReadOnly = true;
            this.txtPendingFreePlaysAmount.Size = new System.Drawing.Size(66, 20);
            this.txtPendingFreePlaysAmount.TabIndex = 9;
            this.txtPendingFreePlaysAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPendingFreePlaysAmount.Visible = false;
            // 
            // txtPendingFreePlaysCount
            // 
            this.txtPendingFreePlaysCount.Location = new System.Drawing.Point(79, 24);
            this.txtPendingFreePlaysCount.Name = "txtPendingFreePlaysCount";
            this.txtPendingFreePlaysCount.ReadOnly = true;
            this.txtPendingFreePlaysCount.Size = new System.Drawing.Size(66, 20);
            this.txtPendingFreePlaysCount.TabIndex = 8;
            this.txtPendingFreePlaysCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPendingFreePlaysCount.Visible = false;
            // 
            // btnBalanceInfo
            // 
            this.btnBalanceInfo.Enabled = false;
            this.btnBalanceInfo.Location = new System.Drawing.Point(100, 150);
            this.btnBalanceInfo.Name = "btnBalanceInfo";
            this.btnBalanceInfo.Size = new System.Drawing.Size(143, 23);
            this.btnBalanceInfo.TabIndex = 9;
            this.btnBalanceInfo.Text = "Balance Info";
            this.btnBalanceInfo.UseVisualStyleBackColor = true;
            this.btnBalanceInfo.Click += new System.EventHandler(this.btnBalanceInfo_Click);
            // 
            // btnDailyFigure
            // 
            this.btnDailyFigure.Enabled = false;
            this.btnDailyFigure.Location = new System.Drawing.Point(100, 179);
            this.btnDailyFigure.Name = "btnDailyFigure";
            this.btnDailyFigure.Size = new System.Drawing.Size(143, 23);
            this.btnDailyFigure.TabIndex = 10;
            this.btnDailyFigure.Text = "Daily Figure";
            this.btnDailyFigure.UseVisualStyleBackColor = true;
            this.btnDailyFigure.Click += new System.EventHandler(this.btnDailyFigure_Click);
            // 
            // btnRetrieve
            // 
            this.btnRetrieve.Location = new System.Drawing.Point(168, 251);
            this.btnRetrieve.Name = "btnRetrieve";
            this.btnRetrieve.Size = new System.Drawing.Size(90, 23);
            this.btnRetrieve.TabIndex = 11;
            this.btnRetrieve.Text = "Retrieve";
            this.btnRetrieve.UseVisualStyleBackColor = true;
            this.btnRetrieve.Click += new System.EventHandler(this.btnRetrieve_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(336, 251);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(87, 23);
            this.btnCancel.TabIndex = 12;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblMessages
            // 
            this.lblMessages.AutoSize = true;
            this.lblMessages.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMessages.Location = new System.Drawing.Point(222, 223);
            this.lblMessages.Name = "lblMessages";
            this.lblMessages.Size = new System.Drawing.Size(2, 15);
            this.lblMessages.TabIndex = 13;
            // 
            // lblCurrentBalance
            // 
            this.lblCurrentBalance.AutoSize = true;
            this.lblCurrentBalance.Location = new System.Drawing.Point(3, 73);
            this.lblCurrentBalance.Name = "lblCurrentBalance";
            this.lblCurrentBalance.Size = new System.Drawing.Size(83, 39);
            this.lblCurrentBalance.TabIndex = 14;
            this.lblCurrentBalance.Text = "Current Balance\r\n(incl non-posted\r\ncasino):";
            this.lblCurrentBalance.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblCurrentBalance.Visible = false;
            // 
            // txtCurrentBalance
            // 
            this.txtCurrentBalance.Location = new System.Drawing.Point(100, 83);
            this.txtCurrentBalance.Name = "txtCurrentBalance";
            this.txtCurrentBalance.ReadOnly = true;
            this.txtCurrentBalance.Size = new System.Drawing.Size(143, 20);
            this.txtCurrentBalance.TabIndex = 15;
            this.txtCurrentBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCurrentBalance.Visible = false;
            // 
            // txtFreePlayBalance
            // 
            this.txtFreePlayBalance.Location = new System.Drawing.Point(100, 122);
            this.txtFreePlayBalance.Name = "txtFreePlayBalance";
            this.txtFreePlayBalance.ReadOnly = true;
            this.txtFreePlayBalance.Size = new System.Drawing.Size(143, 20);
            this.txtFreePlayBalance.TabIndex = 17;
            this.txtFreePlayBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtFreePlayBalance.Visible = false;
            // 
            // lblFreePlayBalance
            // 
            this.lblFreePlayBalance.AutoSize = true;
            this.lblFreePlayBalance.Location = new System.Drawing.Point(3, 125);
            this.lblFreePlayBalance.Name = "lblFreePlayBalance";
            this.lblFreePlayBalance.Size = new System.Drawing.Size(93, 13);
            this.lblFreePlayBalance.TabIndex = 16;
            this.lblFreePlayBalance.Text = "Free Play Balance";
            this.lblFreePlayBalance.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.lblFreePlayBalance.Visible = false;
            // 
            // btnHistory
            // 
            this.btnHistory.Enabled = false;
            this.btnHistory.Location = new System.Drawing.Point(100, 150);
            this.btnHistory.Name = "btnHistory";
            this.btnHistory.Size = new System.Drawing.Size(143, 23);
            this.btnHistory.TabIndex = 18;
            this.btnHistory.Text = "History...";
            this.btnHistory.UseVisualStyleBackColor = true;
            this.btnHistory.Click += new System.EventHandler(this.btnHistory_Click);
            // 
            // btnVerified
            // 
            this.btnVerified.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnVerified.Enabled = false;
            this.btnVerified.Location = new System.Drawing.Point(168, 251);
            this.btnVerified.Name = "btnVerified";
            this.btnVerified.Size = new System.Drawing.Size(90, 23);
            this.btnVerified.TabIndex = 19;
            this.btnVerified.Text = "Verified";
            this.btnVerified.UseVisualStyleBackColor = true;
            this.btnVerified.Click += new System.EventHandler(this.btnVerified_Click);
            // 
            // FrmOpenCustomer
            // 
            this.AcceptButton = this.btnRetrieve;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(559, 289);
            this.Controls.Add(this.txtFreePlayBalance);
            this.Controls.Add(this.lblFreePlayBalance);
            this.Controls.Add(this.txtCurrentBalance);
            this.Controls.Add(this.lblCurrentBalance);
            this.Controls.Add(this.lblMessages);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnRetrieve);
            this.Controls.Add(this.btnDailyFigure);
            this.Controls.Add(this.btnBalanceInfo);
            this.Controls.Add(this.grpPendingFreePlays);
            this.Controls.Add(this.grpPendingWagers);
            this.Controls.Add(this.grpLastVerified);
            this.Controls.Add(this.lblPassword);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.btnLast);
            this.Controls.Add(this.lblCustomerPIN);
            this.Controls.Add(this.txtCustomerPIN);
            this.Controls.Add(this.btnVerified);
            this.Controls.Add(this.btnHistory);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmOpenCustomer";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Open Customer";
            this.Load += new System.EventHandler(this.FrmOpenCustomer_Load);
            this.grpLastVerified.ResumeLayout(false);
            this.grpLastVerified.PerformLayout();
            this.grpPendingWagers.ResumeLayout(false);
            this.grpPendingWagers.PerformLayout();
            this.grpPendingFreePlays.ResumeLayout(false);
            this.grpPendingFreePlays.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtCustomerPIN;
        private System.Windows.Forms.Label lblCustomerPIN;
        private System.Windows.Forms.Button btnLast;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.GroupBox grpLastVerified;
        private System.Windows.Forms.GroupBox grpPendingWagers;
        private System.Windows.Forms.GroupBox grpPendingFreePlays;
        private System.Windows.Forms.Button btnBalanceInfo;
        private System.Windows.Forms.Button btnDailyFigure;
        private System.Windows.Forms.Button btnRetrieve;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblMessages;
        private System.Windows.Forms.TextBox txtBalance;
        private System.Windows.Forms.Label lblBalance;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.Label lblCurrentBalance;
        private System.Windows.Forms.TextBox txtCurrentBalance;
        private System.Windows.Forms.TextBox txtFreePlayBalance;
        private System.Windows.Forms.Label lblFreePlayBalance;
        private System.Windows.Forms.Button btnHistory;
        private System.Windows.Forms.Button btnVerified;
        private System.Windows.Forms.TextBox txtPendingWagersCount;
        private System.Windows.Forms.Label lblPendingWagersCount;
        private System.Windows.Forms.TextBox txtPendingWagersDebitAmount;
        private System.Windows.Forms.Label lblPendingWagersDebitAmount;
        private System.Windows.Forms.TextBox txtPendingWagersCreditAmount;
        private System.Windows.Forms.Label lblPendingWagersCreditAmount;
        private System.Windows.Forms.TextBox txtPendingFreePlaysAmount;
        private System.Windows.Forms.TextBox txtPendingFreePlaysCount;
        private System.Windows.Forms.Label lblPendingFreePlaysCount;
        private System.Windows.Forms.Label lblFreeplayAmt;
    }
}