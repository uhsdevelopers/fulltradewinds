﻿using System;
using System.Collections.Generic;
using System.Configuration;
using GUILibraries;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Utilities;
using TicketWriter.UI;


namespace TicketWriter {

  static class Program {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main(string[] args) {


      using (var appLoader = new AppLoader(InstanceManager.BusinessLayer.InstanceManager.Applications.TicketWriter, args, ConfigurationManager.AppSettings["SystemId"])) {
        using (var ticketWriterApp = new MdiTicketWriter(appLoader.InstanceManager.InstanceModuleInfo)) {
          if (appLoader.IsSafeToStart() && appLoader.UserHasPermission(LoginsAndProfiles.ACCEPT_WAGERS)) {
            ticketWriterApp.CurrentUserPermissions = appLoader.UserPermissions;
            ticketWriterApp.LoginInUse = appLoader.LoginInUse;

            var formParameters = new List<FormParameter> {
                                      new FormParameter("AppName", appLoader.InstanceManager.InstanceModuleInfo.ShortName),
                                      new FormParameter("DataBaseName", appLoader.InstanceManager.InstanceModuleInfo.InstanceInfo.DatabaseName),
                                      new FormParameter("ModuleName", appLoader.InstanceManager.InstanceModuleInfo.Description)
                                    };
            ticketWriterApp.ParameterList = new FormParameterList(formParameters);

            appLoader.StartApp(ticketWriterApp);
          }
        }
        appLoader.KillApp();
      }
    }

  }
}