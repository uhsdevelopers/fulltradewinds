﻿using InstanceManager.BusinessLayer;
using SIDLibraries.UI;

// ReSharper disable once CheckNamespace
namespace TicketWriter.Extensions {
  public class ByPassParlayAndTeaserQuickWageringLimit : ExtensionCore {
    public ByPassParlayAndTeaserQuickWageringLimit(ModuleInfo moduleInfo)
      : base(moduleInfo, "LimitParlaysAndTeasersKeyRule") { }
  }
}
