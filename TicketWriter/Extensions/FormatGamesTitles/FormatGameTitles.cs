﻿using System.Windows.Forms;
using InstanceManager.BusinessLayer;
using SIDLibraries.UI;
using TicketWriter.UI;
using GUILibraries.Utilities;
using SIDLibraries.Entities;

// ReSharper disable once CheckNamespace
namespace TicketWriter.Extensions {
  class FormatGameTitles : ExtensionCore {


    public FormatGameTitles(ModuleInfo module)
      : base(module, "FormatGameTitles") {
    }

    public void FormatTitle(spGLGetActiveGamesByCustomer_Result game, DataGridView dgvw) {
      if (!IsActive() || !TwUtilities.IsTitle(game)) return;
      var row1 = dgvw.Rows[dgvw.Rows.Count - 2];
      var row2 = dgvw.Rows[dgvw.Rows.Count - 1];
      var row1GroupNumber = row1.Cells[FrmSportAndGameSelection.ROWGROUPNUM_INDEX].Value;
      //var row2GroupNumber = row2.Cells[FrmSportAndGameSelection.ROWGROUPNUM_INDEX].Value;
      for (var j = 0; j < 9; j++) {
        row1.Cells[j] = new HMergedCell();
        row2.Cells[j] = new HMergedCell();
        ((HMergedCell)row1.Cells[j]).LeftColumn = 0;
        ((HMergedCell)row1.Cells[j]).RightColumn = 8;
        ((HMergedCell)row2.Cells[j]).LeftColumn = 0;
        ((HMergedCell)row2.Cells[j]).RightColumn = 8;
      }

      row1.Cells[FrmSportAndGameSelection.ROTATION_INDEX].Value = game.Team1ID.Replace("*", "");
      row2.Cells[FrmSportAndGameSelection.ROTATION_INDEX].Value = game.Team2ID.Replace("*", "");
      row1.Cells[FrmSportAndGameSelection.TEAMS_NAMES_INDEX].Value = game.Team1ID.Replace("*", "#");
      row2.Cells[FrmSportAndGameSelection.TEAMS_NAMES_INDEX].Value = game.Team2ID.Replace("*", "#");
      row1.Cells[FrmSportAndGameSelection.GAME_NUM_INDEX].Value = game.GameNum;
      row2.Cells[FrmSportAndGameSelection.GAME_NUM_INDEX].Value = game.GameNum;
      row1.Cells[FrmSportAndGameSelection.PERIOD_NUM_INDEX].Value = game.PeriodNumber;
      row2.Cells[FrmSportAndGameSelection.PERIOD_NUM_INDEX].Value = game.PeriodNumber;
      row1.Cells[FrmSportAndGameSelection.ROWGROUPNUM_INDEX].Value = row1GroupNumber;
      row2.Cells[FrmSportAndGameSelection.ROWGROUPNUM_INDEX].Value = row1GroupNumber;
      row1.Cells[FrmSportAndGameSelection.SPORT_INDEX].Value = game.SportType;
      row2.Cells[FrmSportAndGameSelection.SPORT_INDEX].Value = game.SportType;
      row1.Cells[FrmSportAndGameSelection.SUBSPORT_INDEX].Value = game.SportSubType;
      row2.Cells[FrmSportAndGameSelection.SUBSPORT_INDEX].Value = game.SportSubType;
      row1.Cells[FrmSportAndGameSelection.PERIOD_CUTOFF_INDEX].Value = game.PeriodWagerCutoff;
      row2.Cells[FrmSportAndGameSelection.PERIOD_CUTOFF_INDEX].Value = game.PeriodWagerCutoff;
      row1.Cells[FrmSportAndGameSelection.GAME_STATUS_INDEX].Value = null;
      row2.Cells[FrmSportAndGameSelection.GAME_STATUS_INDEX].Value = null;
      row1.Cells[FrmSportAndGameSelection.TEAM1_NAME_INDEX].Value = game.Team1ID;
      row2.Cells[FrmSportAndGameSelection.TEAM1_NAME_INDEX].Value = game.Team2ID;
      row1.Cells[FrmSportAndGameSelection.CUSTPROFILE_INDEX].Value = game.CustProfile;
      row2.Cells[FrmSportAndGameSelection.CUSTPROFILE_INDEX].Value = game.CustProfile;
      row1.Cells[FrmSportAndGameSelection.SCHEDULEDATE_INDEX].Value = game.ScheduleDate;
      row2.Cells[FrmSportAndGameSelection.SCHEDULEDATE_INDEX].Value = game.ScheduleDate;
    }

    public bool IsTitle(DataGridViewRow row) {
      return IsActive() && (row.Cells[FrmSportAndGameSelection.GAME_STATUS_INDEX].Value == null); //gameStatus
    }
  }
}