﻿using SIDLibraries.UI;
using TicketWriter.UI;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;
using System;
using System.Windows.Forms;
using System.Linq;
using SIDLibraries.Utilities;
using GUILibraries.Utilities;
using System.Globalization;
using System.Collections.Generic;
using SIDLibraries.Entities;

// ReSharper disable once CheckNamespace
namespace TicketWriter.Extensions {
  public class ShowWagerLimitsInFrm : ExtensionCore {
    public FrmMakeAWager FrmMakeAWager;
    public FrmManualGameWager FrmManualGameWager;
    public FrmPlaceContestOrProp FrmPlaceContestOrProp;
    public FrmManualContestWager FrmManualContestWager;
    public FrmPlaceTeaser FrmPlaceTeaser;
    public FrmPlaceParlay FrmPlaceParlay;
    private readonly Boolean _showCents;
    private WagerLimitValidator.LimitTypes _maxWagerLimitTypeCode;
    spLOGetParlayPayCard_Result _parlayPayCard;

    public ShowWagerLimitsInFrm(ModuleInfo moduleInfo, Boolean showCents)
      : base(moduleInfo, "ShowWagerLimitsInFrm") { _showCents = showCents; }

    public void DisplayMaxManualWagerLimits() {
      if (FrmManualGameWager != null) {
        string riskAmount;
        var lblMaxRiskDisplay = (Label)FrmManualGameWager.Controls.Find("lblMaxRiskDisplay", true).FirstOrDefault();
        var lblMaxWinDisplay = (Label)FrmManualGameWager.Controls.Find("lblMaxWinDisplay", true).FirstOrDefault();
        if (lblMaxRiskDisplay == null || lblMaxWinDisplay == null)
          return;
        var americanOdds = GetAmericanOdds();
        switch (FrmManualGameWager.CurrentWagerTypeName) {
          case "Straight Bet":
            riskAmount = GetMaxManualRiskAmount();
            if (FrmManualGameWager.WagerTypeMode != null && FrmManualGameWager.WagerTypeMode == "Risk") {
              lblMaxRiskDisplay.Text = @"Max: $" + riskAmount;
              lblMaxWinDisplay.Text = GetMaxManualToWinAmount(riskAmount);
            }
            else {
              lblMaxRiskDisplay.Text = @"Max: $" + FormatNumber(LineOffering.DetermineRiskAmount(double.Parse(riskAmount), americanOdds, _showCents), true, 1, true);//DisplayMaxToWinAmount(riskAmount);
              lblMaxWinDisplay.Text = @"Max: $" + riskAmount;
            }
            ShowToolTip(lblMaxRiskDisplay);
            ShowToolTip(lblMaxWinDisplay);
            break;
          case "If-Bet":
            var ifBetFrm = (FrmPlaceIfBet)FormF.GetFormByName("FrmPlaceIfBet", FrmManualGameWager);
            if (ifBetFrm != null) {
              var chbAr = (CheckBox)ifBetFrm.Controls.Find("chbActionReverse", true).FirstOrDefault();
              var chbArbc = (CheckBox)ifBetFrm.Controls.Find("chbARBC", true).FirstOrDefault();

              if (chbAr != null && !chbAr.Checked && chbArbc != null && !chbArbc.Checked) {
                riskAmount = GetMaxManualRiskAmount();
                if (FrmManualGameWager.WagerTypeMode != null && FrmManualGameWager.WagerTypeMode == "Risk") {
                  lblMaxRiskDisplay.Text = @"Max: $" + riskAmount;
                  lblMaxWinDisplay.Text = GetMaxManualToWinAmount(riskAmount);
                }
                else {
                  lblMaxRiskDisplay.Text = @"Max: $" + FormatNumber(LineOffering.DetermineRiskAmount(double.Parse(riskAmount), americanOdds, _showCents), true, 1, true);//DisplayMaxToWinAmount(riskAmount);
                  lblMaxWinDisplay.Text = @"Max: $" + riskAmount;
                }
                ShowToolTip(lblMaxRiskDisplay);
                ShowToolTip(lblMaxWinDisplay);
              }
              else {
                lblMaxRiskDisplay.Text = "";
                lblMaxWinDisplay.Text = "";
              }
            }
            break;
        }
      }
    }

    public void DisplayMaxWagerLimits() {
      if (FrmMakeAWager == null) return;
      var wagerTypeName = FrmMakeAWager.CurrentWagerTypeName;
      var wagerTypeMode = FrmMakeAWager.GetWagerTypeAdjustment() < 0 ? "ToWin" : "Risk";//FrmMakeAWager.WagerTypeMode;
      var lblMaxRiskDisplay = (Label)FrmMakeAWager.Controls.Find("lblMaxRiskDisplay", true).FirstOrDefault();
      var lblMaxWinDisplay = (Label)FrmMakeAWager.Controls.Find("lblMaxWinDisplay", true).FirstOrDefault();
      if (lblMaxRiskDisplay == null || lblMaxWinDisplay == null)
        return;

      string riskAmount;
      lblMaxRiskDisplay.Text = "";
      lblMaxWinDisplay.Text = "";
      switch (wagerTypeName) {
        case "Straight Bet":
          riskAmount = GetMaxRiskAmount();
          if (wagerTypeMode == "Risk") {
            lblMaxRiskDisplay.Text = @"Max: $" + riskAmount;
            lblMaxWinDisplay.Text = GetMaxToWinAmount(riskAmount);
          }
          else {
            lblMaxRiskDisplay.Text = @"Max: $" + FormatNumber(LineOffering.DetermineRiskAmount(double.Parse(riskAmount), FrmMakeAWager.AmericanOdds.GetValueOrDefault(), _showCents), true, 1, true);
            lblMaxWinDisplay.Text = @"Max: $" + riskAmount;
          }
          ShowToolTip(lblMaxRiskDisplay);
          ShowToolTip(lblMaxWinDisplay);
          break;
        case "If-Bet":
          var ifBetFrm = (FrmPlaceIfBet)FormF.GetFormByName("FrmPlaceIfBet", FrmMakeAWager);
          if (ifBetFrm != null) {
            var chbAr = (CheckBox)ifBetFrm.Controls.Find("chbActionReverse", true).FirstOrDefault();
            var chbArbc = (CheckBox)ifBetFrm.Controls.Find("chbARBC", true).FirstOrDefault();

            if (chbAr != null && !chbAr.Checked && chbArbc != null && !chbArbc.Checked) {
              riskAmount = GetMaxRiskAmount();
              if (wagerTypeMode == "Risk") {
                lblMaxRiskDisplay.Text = @"Max: $" + riskAmount;
                lblMaxWinDisplay.Text = GetMaxToWinAmount(riskAmount);
              }
              else {
                lblMaxRiskDisplay.Text = @"Max: $" + FormatNumber(LineOffering.DetermineRiskAmount(double.Parse(riskAmount), FrmMakeAWager.AmericanOdds.GetValueOrDefault(), _showCents), true, 1, true);//DisplayMaxToWinAmount(riskAmount);
                lblMaxWinDisplay.Text = @"Max: $" + riskAmount;
              }
              ShowToolTip(lblMaxRiskDisplay);
              ShowToolTip(lblMaxWinDisplay);
            }
            else {
              lblMaxRiskDisplay.Text = "";
              lblMaxWinDisplay.Text = "";
            }
          }
          break;
      }
    }

    public void DisplayMaxContestWagerLimits() {
      var lblMaxRiskDisplay = (Label)FrmPlaceContestOrProp.Controls.Find("lblMaxRiskDisplay", true).FirstOrDefault();
      var lblMaxWinDisplay = (Label)FrmPlaceContestOrProp.Controls.Find("lblMaxWinDisplay", true).FirstOrDefault();

      if (lblMaxRiskDisplay == null || lblMaxWinDisplay == null)
        return;

      lblMaxRiskDisplay.Text = "";
      lblMaxWinDisplay.Text = "";

      WagerLimitValidator.LimitTypes limitType;
      var riskAmount = GetMaxContestRiskAmount(out limitType);
      if (FrmPlaceContestOrProp.WagerTypeMode != null && FrmPlaceContestOrProp.WagerTypeMode == "Risk") {
        lblMaxRiskDisplay.Text = @"Max: $" + FormatNumber(double.Parse(riskAmount), true, 100, true);
        lblMaxWinDisplay.Text = limitType == WagerLimitValidator.LimitTypes.ExposureLimit ? @"Max: $" + FormatNumber(FrmPlaceContestOrProp.SelectedContest.ExposureLimit ?? 0, true, 100, false) : GetMaxContestToWinAmount(riskAmount);
      }
      else {
        if (FrmPlaceContestOrProp.SelectedContest.XtoYLineRep == "Y") {
          lblMaxRiskDisplay.Text = @"Max: $" + FormatNumber(double.Parse(TwUtilities.DetermineXtoYRiskAmount(double.Parse(riskAmount), FrmPlaceContestOrProp.SelectedAdjustment, FrmPlaceContestOrProp.SelectedContest.ToBase.GetValueOrDefault()).ToString(CultureInfo.InvariantCulture)), true, 100, true);
        }
        else {
          lblMaxRiskDisplay.Text = @"Max: $" + FormatNumber(double.Parse(LineOffering.DetermineRiskAmount(double.Parse(riskAmount), FrmPlaceContestOrProp.SelectedAdjustment, _showCents).ToString(CultureInfo.InvariantCulture)), true, 100, true);
        }
        lblMaxWinDisplay.Text = @"Max: $" + FormatNumber(double.Parse(riskAmount), true, 100, true);
      }
      ShowToolTip(lblMaxRiskDisplay);
      ShowToolTip(lblMaxWinDisplay);
    }

    public void DisplayMaxManualContestWagerLimits() {
      var lblMaxRiskDisplay = (Label)FrmManualContestWager.Controls.Find("lblMaxRiskDisplay", true).FirstOrDefault();
      var lblMaxWinDisplay = (Label)FrmManualContestWager.Controls.Find("lblMaxWinDisplay", true).FirstOrDefault();

      if (lblMaxRiskDisplay == null || lblMaxWinDisplay == null)
        return;

      lblMaxRiskDisplay.Text = "";
      lblMaxWinDisplay.Text = "";

      var riskAmount = GetMaxManualContestRiskAmount();
      if (FrmManualContestWager.WagerTypeMode != null && FrmManualContestWager.WagerTypeMode == "Risk") {
        lblMaxRiskDisplay.Text = @"Max: $" + FormatNumber(double.Parse(riskAmount), false, 1, true);
        lblMaxWinDisplay.Text = GetMaxManualContestToWinAmount(riskAmount);
      }
      else {
        if (FrmManualContestWager.SelectedContest.XtoYLineRep == "Y") {
          lblMaxRiskDisplay.Text = @"Max: $" + FormatNumber(double.Parse(TwUtilities.DetermineXtoYRiskAmount(double.Parse(riskAmount), FrmManualContestWager.GetAdjustment(), FrmManualContestWager.GetToBaseValue()).ToString(CultureInfo.InvariantCulture)), false, 1, true);
        }
        else {
          lblMaxRiskDisplay.Text = @"Max: $" + FormatNumber(double.Parse(LineOffering.DetermineRiskAmount(double.Parse(riskAmount), FrmManualContestWager.GetAdjustment(), _showCents).ToString(CultureInfo.InvariantCulture)), false, 1, true);
        }
        lblMaxWinDisplay.Text = @"Max: $" + FormatNumber(double.Parse(riskAmount), false, 1, true);
      }
      ShowToolTip(lblMaxRiskDisplay);
      ShowToolTip(lblMaxWinDisplay);
    }

    public void DisplayTeaserMaxWagerLimits(int ticketNumber, int? wagerNumber = null) {
      if (FrmPlaceTeaser != null) {
        var wagerLimit = FrmPlaceTeaser.FrmSportAndGameSelection.GetMaxWagerLimits(WagerType.TEASER, ticketNumber, wagerNumber);
        _maxWagerLimitTypeCode = wagerLimit.LimitType;
        var riskAmount = FormatNumber(wagerLimit.Limit, true, 100, true);

        var lblMaxRiskDisplay = (Label)FrmPlaceTeaser.Controls.Find("lblMaxRiskDisplay", true).FirstOrDefault();
        var lblMaxWinDisplay = (Label)FrmPlaceTeaser.Controls.Find("lblMaxWinDisplay", true).FirstOrDefault();

        if (lblMaxRiskDisplay == null || lblMaxWinDisplay == null || wagerNumber == null) return;

        var wagerTypeMode = FrmPlaceTeaser.WagerTypeModeTeaser;

        if (wagerTypeMode != null && wagerTypeMode == "Risk") {
          lblMaxRiskDisplay.Text = @"Max: $" + riskAmount;
          lblMaxWinDisplay.Text = @"Max: $" + CalculateTeaserMaxToWinAmount(riskAmount);
        }
        else {
          lblMaxRiskDisplay.Text = @"Max: $" + CalculateTeaserMaxRiskAmount(riskAmount);
          lblMaxWinDisplay.Text = @"Max: $" + riskAmount;
        }
        ShowToolTip(lblMaxRiskDisplay);
        ShowToolTip(lblMaxWinDisplay);
      }
    }

    public void DisplayParlayMaxWagerLimits(int ticketNumber, int? wagerNumber = null) {
      if (FrmPlaceParlay == null) return;
      var wagerLimit = FrmPlaceParlay.FrmSportAndGameSelection.GetMaxWagerLimits(WagerType.PARLAY, ticketNumber, wagerNumber);
      _maxWagerLimitTypeCode = wagerLimit.LimitType;
      var riskAmount = FormatNumber(wagerLimit.Limit, true, 100, true);

      var lblMaxRiskDisplay = (Label)FrmPlaceParlay.Controls.Find("lblMaxRiskDisplay", true).FirstOrDefault();

      if (lblMaxRiskDisplay == null || wagerNumber == null) return;

      lblMaxRiskDisplay.Text = @"Max: $" + riskAmount;
      ShowToolTip(lblMaxRiskDisplay);
      ShowMaxParlayPayout();
    }

    private void ShowMaxParlayPayout() {
      var lblMaxRiskToMaxPayout = (Label)FrmPlaceParlay.Controls.Find("lblMaxRiskToMaxPayout", true).FirstOrDefault();
      var lblMaxPayoutDisplay = (Label)FrmPlaceParlay.Controls.Find("lblMaxPayout", true).FirstOrDefault();
      if (lblMaxPayoutDisplay == null || lblMaxRiskToMaxPayout == null) return;

      lblMaxRiskToMaxPayout.Text = "MRisk For Max Payout";
      lblMaxPayoutDisplay.Text = "Max Payout";

      var custMaxParlayPayout = (int)(FrmPlaceParlay.FrmSportAndGameSelection.Customer.ParlayMaxPayout ?? 0) / 100;
      var systemMaxParlayPayout = ModuleInfo.Parameters.MaxParlayPayout / 100;
      var parlay = FrmPlaceParlay.Parlay;
      var maxWagerLimit = FrmPlaceParlay.FrmSportAndGameSelection.GetMaxWagerLimits(WagerType.PARLAY, parlay.TicketNumber, parlay.Number);
      int maxToWinAmount = CalculateParlayToWin(parlay, maxWagerLimit.Limit) / 100;
      var payouts = new List<int> { custMaxParlayPayout, systemMaxParlayPayout, maxToWinAmount };
      if (maxToWinAmount >= payouts.Max()) {
        lblMaxPayoutDisplay.Text = FormatNumber(payouts.Min(), false, 1, false);
        if (payouts.Min() > 0) {
          lblMaxRiskToMaxPayout.Text = FormatNumber(CalculateMinRiskToReachMaxPayout(parlay, payouts.Min(), maxWagerLimit.Limit / 100), false, 1, false);
          if (lblMaxRiskToMaxPayout.Text == "")
            lblMaxPayoutDisplay.Text = "";
        }
      }
    }

    #region Parlay Calculations

    private double CalculateMinRiskToReachMaxPayout(Parlay parlay, int targetToWinAmount, double maxWagerLimit) {
      int startingRisk = (FrmPlaceParlay.FrmSportAndGameSelection.Customer.CUMinimumWager ?? 0) / 100;
      int riskAmount = startingRisk;
      var wiAry = parlay.CreateWiAry();
      if (wiAry.Count < 3)
        return 0;
      var parlayName = parlay.Name;
      var parlayDefaultPrice = parlay.DefaultPrice;
      var custMaxParlayPayout = (int)(FrmPlaceParlay.FrmSportAndGameSelection.Customer.ParlayMaxPayout ?? 0);
      var playCount = 0;
      double maxParlayPayout;
      var exchangeRate = parlay.ExchangeRate;
      var i = 0;
      int guessedToWinAmt = 0;
      _parlayPayCard = null;

      while (guessedToWinAmt < targetToWinAmount && riskAmount < maxWagerLimit && i < 5000) {
        using (var innerParlay = new LineOffering.Parlay(ModuleInfo)) {
          guessedToWinAmt = (int)innerParlay.CalculateParlayToWin(wiAry, riskAmount, parlayDefaultPrice, GetPayCardMaxPayout(parlayName, wiAry.Count, riskAmount), custMaxParlayPayout, parlayName, out maxParlayPayout, exchangeRate);

          /*guessedToWinAmt = (int)double.Parse(parlay.ShowCents ? (innerParlay.CalculateToWinAmount(wiAry, parlayName, riskAmount, "", 0, parlayDefaultPrice, custMaxParlayPayout, out playCount, exchangeRate)).ToString(CultureInfo.InvariantCulture) :
              Math.Round(innerParlay.CalculateToWinAmount(wiAry, parlayName, riskAmount, "", 0, parlayDefaultPrice, custMaxParlayPayout, out playCount, exchangeRate)).ToString(CultureInfo.InvariantCulture));
          */
        }
        if (guessedToWinAmt > targetToWinAmount)
          return riskAmount;
        riskAmount += startingRisk;
        i++;
      }
      return riskAmount;
    }

    public double GetPayCardMaxPayout(string sParlayName, int selCount, double riskAmt) {
      if (selCount < 2)
        return 0;
      if (_parlayPayCard == null) {
        using (var lo = new LineOffering.Parlay(ModuleInfo)) {
          _parlayPayCard = lo.GetParlayPayCard(selCount, sParlayName).FirstOrDefault();
        }
      }

      double toWinAmt = 0;

      if (_parlayPayCard == null || !_parlayPayCard.MaxPayoutMoneyLine.HasValue || !_parlayPayCard.MaxPayoutToBase.HasValue) return toWinAmt;
      var moneyLine = (double)_parlayPayCard.MaxPayoutMoneyLine;
      var toBase = (double)_parlayPayCard.MaxPayoutToBase;
      toWinAmt = riskAmt * moneyLine / toBase;

      return toWinAmt;
    }

    private int CalculateParlayToWin(Parlay parlay, double wagerLimit) {
      var wiAry = parlay.CreateWiAry();
      if (wiAry.Count < 2)
        return 0;
      var parlayName = parlay.Name;
      var parlayDefaultPrice = parlay.DefaultPrice;
      var custMaxParlayPayout = (int)(FrmPlaceParlay.FrmSportAndGameSelection.Customer.ParlayMaxPayout ?? 0);
      double maxParlayPayout;
      var exchangeRate = parlay.ExchangeRate;
      using (var innerParlay = new LineOffering.Parlay(ModuleInfo)) {
        return (int)innerParlay.CalculateParlayToWin(wiAry, wagerLimit, parlayDefaultPrice, GetPayCardMaxPayout(parlayName, wiAry.Count, wagerLimit), custMaxParlayPayout, parlayName, out maxParlayPayout, exchangeRate);
      }
    }

    #endregion


    private String GetMaxManualContestToWinAmount(String maxRisk) {
      if (FrmManualContestWager.SelectedContest.XtoYLineRep == "Y") {
        return "Max: $" + FormatNumber(TwUtilities.DetermineXtoYToWinAmount(double.Parse(maxRisk), FrmManualContestWager.GetAdjustment(), FrmManualContestWager.GetToBaseValue()), false, 1, true);
      }
      return "Max: $" + FormatNumber(LineOffering.DetermineToWinAmount(double.Parse(maxRisk), FrmManualContestWager.GetAdjustment(), _showCents), false, 1, true);
    }

    private String GetMaxManualContestRiskAmount() {
      var lResult = FrmManualContestWager._frmSportAndGameSelection.GetLimitForCurrentContestWager(FrmManualContestWager.SelectedContest);
      var retValue = FormatNumber(lResult.Limit, true, 100, true);
      _maxWagerLimitTypeCode = lResult.LimitType;
      return retValue;
    }

    private String GetMaxToWinAmount(String maxRisk) {
      return "Max: $" + FormatNumber(LineOffering.DetermineToWinAmount(double.Parse(maxRisk), FrmMakeAWager.GetWagerTypeAdjustment(), _showCents), false, 1, true);
    }

    private String GetMaxRiskAmount() {
      var wagerType = FrmMakeAWager.GetCheckedWagerTypeCode();
      var retValue = FormatNumber(FrmMakeAWager.ParentFrm.GetLimitForCurrentWager(FrmMakeAWager.SelectedGamePeriodInfo, wagerType, wagerType), true, 100, true);
      _maxWagerLimitTypeCode = FrmMakeAWager.ParentFrm.MaxWagerLimitTypeCode;
      return retValue;
    }

    private String GetMaxManualToWinAmount(String maxRisk) {
      return "Max: $" + FormatNumber(LineOffering.DetermineToWinAmount(double.Parse(maxRisk), GetAmericanOdds(), _showCents), false, 1, true);
    }

    private String GetMaxManualRiskAmount() {
      var wagerType = TwUtilities.GetSelectedWagerOption(FrmManualGameWager.SelectedWagerType, FrmManualGameWager.SelectedRowNumberInGroup, out FrmManualGameWager.TotalsOu);
      var retValue = FormatNumber(FrmManualGameWager.ParentFrm.GetLimitForCurrentWager(FrmManualGameWager.SelectedGamePeriodInfo, wagerType, wagerType), true, 100, true);
      _maxWagerLimitTypeCode = FrmManualGameWager.ParentFrm.MaxWagerLimitTypeCode;
      return retValue;
    }

    private String GetMaxContestToWinAmount(String maxRisk) {
      if (FrmPlaceContestOrProp.SelectedContest.XtoYLineRep == "Y") {
        return "Max: $" + FormatNumber(TwUtilities.DetermineXtoYToWinAmount(double.Parse(maxRisk), FrmPlaceContestOrProp.SelectedAdjustment, FrmPlaceContestOrProp.SelectedContest.ToBase.GetValueOrDefault()), true, 100, true);
      }
      return "Max: $" + FormatNumber(LineOffering.DetermineToWinAmount(double.Parse(maxRisk), FrmPlaceContestOrProp.SelectedAdjustment, _showCents), true, 100, true);
    }

    private String GetMaxContestRiskAmount(out WagerLimitValidator.LimitTypes limitType) {
      var limit = FrmPlaceContestOrProp._frmSportAndGameSelection.GetLimitForCurrentContestWager(FrmPlaceContestOrProp.SelectedContest);
      limitType = limit.LimitType;
      _maxWagerLimitTypeCode = limitType;
      return FormatNumber(limit.Limit);
    }

    private double GetAmericanOdds() {
      double adj = 0;
      double convertedToAmerican = 0;
      if (FrmManualGameWager == null) return convertedToAmerican;
      var txtPrice = (TextBox)FrmManualGameWager.Controls.Find("txtPrice", true).FirstOrDefault();
      if (txtPrice == null)
        return adj;
      switch (FrmManualGameWager.SelectedPrice) {
        case "American":
        case LineOffering.PRICE_AMERICAN:
          double.TryParse(txtPrice.Text, out adj);
          FrmManualGameWager.WagerTypeMode = adj >= 0 ? "Risk" : "ToWin";
          convertedToAmerican = adj;
          break;
        case "Decimal":
        case LineOffering.PRICE_DECIMAL:
          double.TryParse(txtPrice.Text, out adj);
          convertedToAmerican = OddsTypes.DecimalToUs(adj);
          FrmManualGameWager.WagerTypeMode = convertedToAmerican >= 0 ? "Risk" : "ToWin";
          break;
        case "Fractional":
        case LineOffering.PRICE_FRACTIONAL:
          int numerator;
          int denominator;
          int.TryParse(txtPrice.Text.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries)[0].Trim(), out numerator);
          int.TryParse(txtPrice.Text.Split(new[] { "/" }, StringSplitOptions.RemoveEmptyEntries)[1].Trim(), out denominator);
          var convertedToDecimal = OddsTypes.FractionalToDecimal(numerator, denominator);
          convertedToAmerican = OddsTypes.DecimalToUs(convertedToDecimal);
          FrmManualGameWager.WagerTypeMode = convertedToAmerican >= 0 ? "Risk" : "ToWin";
          break;
      }
      return convertedToAmerican;
    }

    private String CalculateTeaserMaxRiskAmount(String maxToWinAmount) {
      var frm = FormF.GetFormByName("MdiTicketWriter", FrmPlaceTeaser);
      var nudTeams = (NumericUpDown)FrmPlaceTeaser.Controls.Find("nudTeams", true).FirstOrDefault();
      if (nudTeams == null)
        return String.Empty;
      var cmbTeaser = (ComboBox)FrmPlaceTeaser.Controls.Find("cmbTeaser", true).FirstOrDefault();
      if (cmbTeaser == null)
        return String.Empty;

      if (frm == null || maxToWinAmount == "") {
        return String.Empty;
      }

      var mdi = (MdiTicketWriter)frm;

      if (mdi.CustomerAvailableTeasers != null && mdi.CustomerAvailableTeasers.Count > 0) {
        double riskAmount;
        if (mdi.TeaserInfo == null) {
          using (var teaserInfo = new LineOffering.Teaser(FrmPlaceTeaser.Mdi.AppModuleInfo)) {
            riskAmount = teaserInfo.CalculateRisk(NumberF.ParseDouble(maxToWinAmount), cmbTeaser.Text, int.Parse(nudTeams.Value.ToString(CultureInfo.InvariantCulture)));
          }

        }
        else {
          riskAmount = mdi.TeaserInfo.CalculateRisk(NumberF.ParseDouble(maxToWinAmount), cmbTeaser.Text, int.Parse(nudTeams.Value.ToString(CultureInfo.InvariantCulture)));
        }
        return FormatNumber(riskAmount, false, 1, true);
      }
      return String.Empty;
    }

    private String CalculateTeaserMaxToWinAmount(String maxRiskAmount) {
      var frm = FormF.GetFormByName("MdiTicketWriter", FrmPlaceTeaser);
      var nudTeams = (NumericUpDown)FrmPlaceTeaser.Controls.Find("nudTeams", true).FirstOrDefault();
      if (nudTeams == null)
        return String.Empty;
      var cmbTeaser = (ComboBox)FrmPlaceTeaser.Controls.Find("cmbTeaser", true).FirstOrDefault();
      if (cmbTeaser == null)
        return String.Empty;

      if (frm == null || maxRiskAmount == "") {
        return String.Empty;
      }

      var mdi = (MdiTicketWriter)frm;

      if (mdi.CustomerAvailableTeasers != null && mdi.CustomerAvailableTeasers.Count > 0) {
        double toWinAmount;
        if (mdi.TeaserInfo == null) {
          using (var teaserInfo = new LineOffering.Teaser(FrmPlaceTeaser.Mdi.AppModuleInfo)) {
            toWinAmount = teaserInfo.CalculateToWin(NumberF.ParseDouble(maxRiskAmount), cmbTeaser.Text, int.Parse(nudTeams.Value.ToString(CultureInfo.InvariantCulture)));
          }

        }
        else {
          toWinAmount = mdi.TeaserInfo.CalculateRisk(NumberF.ParseDouble(maxRiskAmount), cmbTeaser.Text, int.Parse(nudTeams.Value.ToString(CultureInfo.InvariantCulture)));
        }
        return FormatNumber(toWinAmount, false, 1, true);
      }
      return String.Empty;
    }

    private void ShowToolTip(Label targetLabel) {
      targetLabel.MouseHover += targetLabel_MouseHover;
    }

    private void targetLabel_MouseHover(object sender, EventArgs e) {
      var tp = new ToolTip();
      tp.SetToolTip(((Label)sender), "Limited By " + WagerLimitValidator.GetLimitTypeDescription(_maxWagerLimitTypeCode));
    }

  }
}