﻿using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;
using System.Collections.Generic;
using SIDLibraries.UI;

// ReSharper disable once CheckNamespace
namespace TicketWriter.Extensions {
  public class LimitTeasersOnlyKeyRule : ExtensionCore {

    /*
     1. Stop an identical teaser (using same teams) to only one time.
     2. Allow only one team to be keyed.
     */

    readonly ModuleInfo _appModuleInfo;

    public LimitTeasersOnlyKeyRule(ModuleInfo moduleInfo)
      : base(moduleInfo, "LimitTeasersOnlyKeyRule") {
      _appModuleInfo = moduleInfo;
    }


    internal bool LimitTeaserOnlyKeyRulePassed(GamesAndLines gl, string currentCustomerId, bool isRoundRobinWager, Ticket ticket, int ticketNumber, int? wagerNumber, List<Ticket.WagerItem> multiSelectedItems) {
      return TeaserRulePassedInDb(gl, currentCustomerId, isRoundRobinWager, multiSelectedItems) && TeaserRulePassedInReadback(ticket, ticketNumber, wagerNumber, multiSelectedItems);
    }

    private bool TeaserRulePassedInReadback(Ticket ticket, int ticketNumber, int? wagerNumber, List<Ticket.WagerItem> multiSelectedItems) {
      using (var teaser = new LineOffering.Teaser(_appModuleInfo)) {
        var wagersInReadback = GamesAndLines.GetItemsInReadback(teaser, ticket, ticketNumber, wagerNumber, multiSelectedItems, false);
        var t = GamesAndLines.CheckIfSelectionAlreadyExists(wagersInReadback, multiSelectedItems);
        return !t;
      }
    }

    private static bool TeaserRulePassedInDb(GamesAndLines gl, string currentCustomerId, bool isRoundRobinWager, List<Ticket.WagerItem> multiSelectedItems) {
      var teaserItemsInDb = gl.GetAccumulatedMultiSelectionWagerCount(currentCustomerId, "T", isRoundRobinWager, multiSelectedItems);

      var t = GamesAndLines.CheckIfSelectionAlreadyExists(teaserItemsInDb, multiSelectedItems);
      return !t;
    }
  }
}