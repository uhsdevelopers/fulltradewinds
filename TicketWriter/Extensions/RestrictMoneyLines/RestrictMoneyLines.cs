﻿using SIDLibraries.UI;
using TicketWriter.UI;
using InstanceManager.BusinessLayer;
using SIDLibraries.Entities;
using SIDLibraries.BusinessLayer;
using System;

// ReSharper disable once CheckNamespace
namespace TicketWriter.Extensions {
  public class RestrictMoneyLines : ExtensionCore {
    private readonly FrmSportAndGameSelection _frmSportAndGameSelection;

    public RestrictMoneyLines(ModuleInfo moduleInfo, FrmSportAndGameSelection frmSportAndGameSelection)
      : base(moduleInfo, "RestrictMoneyLines") {
      _frmSportAndGameSelection = frmSportAndGameSelection;
    }

    public string CheckForMlRestriction(string moneyLine, spGLGetActiveGamesByCustomer_Result res) {
      if (res == null)
        return moneyLine;
      string retMoneyLine = moneyLine;
      if (IsActive()) {
        if (res.SportType.ToLower() == SportTypes.BASKETBALL.ToLower() || res.SportType.ToLower() == SportTypes.FOOTBALL.ToLower()) {
          if (res.SportType.ToLower() == SportTypes.BASKETBALL.ToLower()) {
            if (_frmSportAndGameSelection.MaxSpreadForMlBasketballRestriction != null && _frmSportAndGameSelection.MaxSpreadForMlBasketballRestriction > 0 && res.Spread != null && Math.Abs((double)res.Spread) >= _frmSportAndGameSelection.MaxSpreadForMlBasketballRestriction) {
              retMoneyLine = String.Empty;
            }
          }
          if (res.SportType.ToLower() == SportTypes.FOOTBALL.ToLower()) {
            if (_frmSportAndGameSelection.MaxSpreadForMlFootballRestriction != null && _frmSportAndGameSelection.MaxSpreadForMlFootballRestriction > 0 && res.Spread != null && Math.Abs((double)res.Spread) >= _frmSportAndGameSelection.MaxSpreadForMlFootballRestriction) {
              retMoneyLine = String.Empty;
            }
          }
        }
      }
      return retMoneyLine;
    }

    public bool WagerTypeNotRestricted(spGLGetActiveGamesByCustomer_Result res) {
      if (res == null || !IsActive()) return true;
      var wagerTypeNotRestricted = true;
      var sportType = res.SportType.Trim();
      if (sportType.ToLower() != SportTypes.BASKETBALL.ToLower() && sportType.ToLower() != SportTypes.FOOTBALL.ToLower()) return true;
      var spreadValue = res.Spread;
      if (sportType.ToLower() == SportTypes.BASKETBALL.ToLower()) {
        if (_frmSportAndGameSelection.MaxSpreadForMlBasketballRestriction != null && _frmSportAndGameSelection.MaxSpreadForMlBasketballRestriction > 0 && spreadValue != null && Math.Abs((double)spreadValue) >= _frmSportAndGameSelection.MaxSpreadForMlBasketballRestriction) {
          wagerTypeNotRestricted = false;
        }
      }
      if (sportType.ToLower() != SportTypes.FOOTBALL.ToLower()) return wagerTypeNotRestricted;
      if (_frmSportAndGameSelection.MaxSpreadForMlFootballRestriction != null && _frmSportAndGameSelection.MaxSpreadForMlFootballRestriction > 0 && spreadValue != null && Math.Abs((double)spreadValue) >= _frmSportAndGameSelection.MaxSpreadForMlFootballRestriction) {
        wagerTypeNotRestricted = false;
      }
      return wagerTypeNotRestricted;
    }
  }
}