﻿using System.Collections.Generic;
using System.Windows.Forms;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;
using SIDLibraries.UI;
using TicketWriter.UI;

// ReSharper disable once CheckNamespace
namespace TicketWriter.Extensions {
  public class LimitParlaysAndTeasersKeyRule : ExtensionCore {
    readonly ModuleInfo _appModuleInfo;
    readonly FrmSportAndGameSelection _sportAndGameSelectionForm;
    public LimitParlaysAndTeasersKeyRule(ModuleInfo moduleInfo, FrmSportAndGameSelection frmSportAndGameSelection)
      : base(moduleInfo, "LimitParlaysAndTeasersKeyRule") {
      _appModuleInfo = moduleInfo;
      _sportAndGameSelectionForm = frmSportAndGameSelection;
    }

    internal double? GetAccumulatedStageWageredAmount(Form caller, Ticket ticket, int ticketNumber, int? wagerNumber, List<Ticket.WagerItem> multiSelectedItems, bool isForKeyRule) {
      double? stageWageredAmt = 0;
      using (var gl = new GamesAndLines(_appModuleInfo)) {
        switch (caller.AccessibleName.Substring(0, 1)) {
          case WagerType.PARLAY:
            using (var parlay = new LineOffering.Parlay(_appModuleInfo)) {
              var defaultPrice = ((FrmPlaceParlay)caller).Parlay.DefaultPrice;
              var customerMaxPayout = ((FrmPlaceParlay)caller).Parlay.CustomerMaxPayout;
              stageWageredAmt = gl.GetReadbackAccumulatedMultiSelectionWagerAmount(parlay, defaultPrice, customerMaxPayout, ticket, ticketNumber, wagerNumber, multiSelectedItems, (double)_sportAndGameSelectionForm.Customer.ExchangeRate, isForKeyRule);
            }
            break;
          case WagerType.TEASER:
            using (var teaser = new LineOffering.Teaser(_appModuleInfo)) {
              stageWageredAmt = gl.GetReadbackAccumulatedMultiSelectionWagerAmount(teaser, ticket, ticketNumber, wagerNumber, multiSelectedItems, isForKeyRule);
            }
            break;
        }
      }
      return stageWageredAmt;
    }

    internal double AppplyParlaysAndTeasersKeyRule(double currentWagerLimitAmt) {
      if (_sportAndGameSelectionForm.ParlayAndTeaserRestrictionKeyFactor != null)
        return currentWagerLimitAmt * (int)_sportAndGameSelectionForm.ParlayAndTeaserRestrictionKeyFactor;
      return currentWagerLimitAmt;
    }
  }
}