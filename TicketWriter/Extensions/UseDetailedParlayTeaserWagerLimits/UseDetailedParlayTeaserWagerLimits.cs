﻿using InstanceManager.BusinessLayer;
using SIDLibraries.Entities;
using SIDLibraries.BusinessLayer;
using System;
using System.Collections.Generic;
using SIDLibraries.UI;
using TicketWriter.UI;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace TicketWriter.Extensions {
  public class UseDetailedParlayTeaserWagerLimits : ExtensionCore {
    readonly ModuleInfo _appModuleInfo;
    readonly FrmSportAndGameSelection _sportAndGameSelectionForm;
    List<spCstGetCustomerParlayTeaserWagerLimits_Result> _customerParlayTeaserLimits;

    public UseDetailedParlayTeaserWagerLimits(ModuleInfo moduleInfo, FrmSportAndGameSelection sportAndGameSelectionForm)
      : base(moduleInfo, "UseDetailedParlayTeaserWagerLimits") {
      _appModuleInfo = moduleInfo;
      _sportAndGameSelectionForm = sportAndGameSelectionForm;
    }

    private List<spCstGetCustomerParlayTeaserWagerLimits_Result> GetCustomerParlayTeaserLimits(String wagerType) {
      List<spCstGetCustomerParlayTeaserWagerLimits_Result> wagerLimits;
      using (var cust = new Customers(_appModuleInfo)) {
        wagerLimits = cust.GetCustomerParlayTeaserWagerLimits(_sportAndGameSelectionForm.CurrentCustomerId, wagerType).ToList();
      }
      return wagerLimits;
    }

    internal double? GetDetailedParlayTeaserWagerLimit(double? wagerLimit, List<Ticket.WagerItem> items, double? quickLimit, IEnumerable<spCstGetStoreWagerLimits_Result> storeLimits, String wagerType, String enforceParlayTeaserLimit) {
      var wagerLimits = new List<double>();

      if (_customerParlayTeaserLimits == null || !ParlayTeaserLimitsLoaded(_customerParlayTeaserLimits, wagerType)) {
        _customerParlayTeaserLimits = GetCustomerParlayTeaserLimits(wagerType);
      }

      foreach (var item in items) {
        var tmpWagerLimits = new List<double>();

        var itemWagerType = item.WagerType;
        var circleLimitType = "";
        var gameInfo = (from a in _sportAndGameSelectionForm.ActiveGames where a.GameNum == item.GameNum && a.PeriodNumber == item.PeriodNumber select a).FirstOrDefault();
        double? circleLimitAmount = null;
        if (gameInfo != null) {
          circleLimitAmount = TwUtilities.GetPeriodGameCircleLimit(itemWagerType, gameInfo, out circleLimitType);
        }
        if (circleLimitAmount != null && circleLimitAmount > 0 && circleLimitType == "L") {
          tmpWagerLimits.Add((double)circleLimitAmount);
        }

        if (enforceParlayTeaserLimit == "Y") {
          tmpWagerLimits.Add((double)wagerLimit);
        }

        if (enforceParlayTeaserLimit != "Y") {
          if (itemWagerType == WagerType.TOTALPOINTS) itemWagerType = WagerType.TEASER;
          var itemWagerLimit = (from spCstGetCustomerParlayTeaserWagerLimits_Result pl in _customerParlayTeaserLimits
                                where pl.sportType.Trim() == item.SportType.Trim()
                                    && pl.SportSubType.Trim() == item.SportSubType.Trim() && pl.PeriodNumber == item.PeriodNumber.GetValueOrDefault() && pl.WagerType == itemWagerType
                                select pl.CuLimit).FirstOrDefault();

          if (itemWagerLimit == 0) {
            if (wagerLimit != null && wagerLimit > 0) {
              tmpWagerLimits.Add((double)wagerLimit);
            }
          }
          else {
            tmpWagerLimits.Add(itemWagerLimit);
          }
          var storeLimit = (from spCstGetStoreWagerLimits_Result sl in storeLimits
                            where sl.SportType.Trim() == item.SportType.Trim()
                                && sl.SportSubType.Trim() == item.SportSubType.Trim() && sl.PeriodNumber == item.PeriodNumber.GetValueOrDefault() && sl.WagerType == itemWagerType
                            select sl.MaximumWager).FirstOrDefault();

          if (storeLimit != null)
            tmpWagerLimits.Add((double)storeLimit);
          tmpWagerLimits.Add((double)quickLimit);
        }
        wagerLimits.Add(tmpWagerLimits.Min());
      }

      return wagerLimits.Min();
    }

    private static bool ParlayTeaserLimitsLoaded(IEnumerable<spCstGetCustomerParlayTeaserWagerLimits_Result> customerParlayTeaserLimits, string wagerType) {
      var rowsCnt = (from cpt in customerParlayTeaserLimits where cpt.WagerTypePT == wagerType select cpt).Count();
      return rowsCnt > 0;
    }
  }
}