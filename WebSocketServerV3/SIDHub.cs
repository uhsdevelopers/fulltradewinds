﻿using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using WebSocketServerV3.Models;
using System.Collections.Generic;
using System;

namespace WebSocketServerV3 {

  [HubName("sidHub")]
  public class SIDHub : Hub {

    #region Privates

    private readonly SIDSocketServer _sidSocketServer;
    private const string WEB = "WEB";
    private int _commandCounter;

    #endregion

    #region Constructor

    public SIDHub() : this(SIDSocketServer.Instance) { }

    public SIDHub(SIDSocketServer changesServer) {
      _sidSocketServer = changesServer;
      _commandCounter = 0;
    }

    #endregion

    #region Hub events

    public override Task OnConnected() {
      //SubscribeManager();
      BroadcastCustomers();
      return base.OnConnected();
    }


    public override Task OnDisconnected(bool stopCalled) {
      _sidSocketServer.RemoveUser(Context.ConnectionId);
      BroadcastCustomers();
      return base.OnDisconnected(stopCalled);
    }

    #endregion

    #region Server - Admin Users Actions

    public void SubscribeAdminUser() {
      var manager = new WebUser { Context = Context, Type = User.UserType.Admin };
      _sidSocketServer.AddUser(manager);
      BroadcastCustomers();
    }

    public void CloseCustomerSession(string customerId) {
      var manager = new WebUser { Context = Context, Type = User.UserType.Admin };
      _sidSocketServer.CloseCustomerSession(customerId);
      _sidSocketServer.RemoveCustomer(customerId);
      BroadcastCustomers();
    }

    #endregion

    #region Server - Web Customer Actions

    public void SubscribeCustomer(WebUser user) {
      _sidSocketServer.AddUser(new WebUser {
        Context = Context,
        LastActivity = DateTime.Now,
        CustomerId = user.CustomerId,
        AgentId = user.AgentId,
        Store = user.Store,
        Platform = (String.IsNullOrEmpty(user.Platform) ? WEB : user.Platform),
        Type = User.UserType.Customer
      });
      BroadcastCustomers();
    }

    public void SubscribeSport(Subscription subscription) {
      _sidSocketServer.AddSportOrContest(subscription, Context);
      BroadcastCustomers();
    }

    public void SubscribeSports(List<Subscription> subscriptionList) {
      foreach (var sub in subscriptionList) {
        _sidSocketServer.AddSportOrContest(sub, Context);
      }
      BroadcastCustomers();
    }

    public void UnsubscribeSport(Subscription subscription) {
      _sidSocketServer.RemoveSport(subscription, Context.ConnectionId);
      BroadcastCustomers();
    }

    public void UnsubscribeSports(List<Subscription> subscriptionList) {
      foreach (var sub in subscriptionList) {
        _sidSocketServer.RemoveSport(sub, Context.ConnectionId);
      }
      BroadcastCustomers();
    }

    public void SubscribeContest(Subscription subscription) {
      _sidSocketServer.AddSportOrContest(subscription, Context.ConnectionId);
      BroadcastCustomers();
    }

    public void UnsubscribeContest(Subscription subscription) {
      _sidSocketServer.RemoveContest(subscription, Context.ConnectionId);
      BroadcastCustomers();
    }
    /*
    public void SetAsiansAndRestrictedParlay(Subscription subscription) {
      _sidSocketServer.SetAsiansAndRestrictedParlay(Context);
    }

    public void RemoveAsiansAndRestrictedParlay(Subscription subscription) {
      _sidSocketServer.RemoveAsiansAndRestrictedParlay(Context);
    }
     */

    #endregion

    /*
    public void Send(string command) {
      // Call the broadcastMessage method to update clients.
      //SubscribeManager();
      switch (command.ToLower()) {
        case "users":
          BroadcastCustomers();
          break;
        case "user details":
          _sidSocketServer.PrintUsersDetails();
          break;
        case "show activity":
          _sidSocketServer.SwitchActivity();
          break;
        default: return;

      }
    }
    */

    private void BroadcastCustomers() {
      _sidSocketServer.BroadcastCustomers();
    }

    /*
    public List<WebUser> GetCustomers() {
      return Clients.All.broadcastMessage(_sidSocketServer.GetCustomers());
    }
    */

  }
}