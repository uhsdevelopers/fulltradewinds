﻿using System;
using System.Collections.Generic;
using System.Linq;
using WebSocketServerV3.Models;

namespace WebSocketServerV3 {
  public class UsersAndCustomersManager {

    private readonly List<WebUser> _usersAndCustomers;

    public UsersAndCustomersManager() {
      _usersAndCustomers = new List<WebUser>();
    }

    #region Generic

    public void Add(WebUser user) {
      if (_usersAndCustomers == null) return;
      if (_usersAndCustomers.All(m => m.Context.ConnectionId != user.Context.ConnectionId)) {
        _usersAndCustomers.Add(user);
      }
    }

    public WebUser Get(string connectionId) {
      return _usersAndCustomers.FirstOrDefault(u => u.Context.ConnectionId.Equals(connectionId));
    }

    public void Remove(string connectionId) {
      if (_usersAndCustomers == null) return;
      lock (_usersAndCustomers) {
        _usersAndCustomers.RemoveAll(u => u.Context.ConnectionId == connectionId);
      }
    }

    #endregion

    #region Admin Users

    public bool AnyUser() {
      try {
        return _usersAndCustomers != null && GetUsers().Any();
      }
      catch (Exception ex) {
        return false;
      }
    }

    public List<WebUser> GetUsers() {
      return _usersAndCustomers.Where(c => c.Type == User.UserType.Admin && c.Context != null).ToList();
    }

    #endregion

    #region Customers

    public bool AnyCustomer() { return _usersAndCustomers.Any(c => c.Type == User.UserType.Customer && c.Context != null); }

    public void RemoveCustomer(string customerId) {
      if (_usersAndCustomers == null) return;
      lock (_usersAndCustomers) {
        _usersAndCustomers.RemoveAll(u => u.CustomerId == customerId);
      }
    }

    public WebUser GetCustomer(string customerId) {
      return _usersAndCustomers.FirstOrDefault(u => u.CustomerId.Equals(customerId));
    }

    public List<WebUser> GetCustomers() {
      return _usersAndCustomers.Where(c => c.Type == User.UserType.Customer && c.Context != null).ToList();
    }

    public List<WebUser> GetCustomers(string store) {
      return (from u in _usersAndCustomers
              from s in u.Subscriptions
              where s.SportType != null
                    && u.Context != null
                    && u.Store == store.Trim()
                    && u.Type == User.UserType.Customer
              select u).ToList();
    }

    public List<WebUser> GetCustomers(string store, string sportType, string sportSubType) {
      return (from u in _usersAndCustomers.Where(u => u.Type == User.UserType.Customer && u.Context != null)
       from s in u.Subscriptions
       where (u.Store ?? "") == store.Trim()
             && s != null
             && s.SportType != null
             && s.Type == Subscription.SubscriptionType.Game
             && (s.SportType ?? "") == sportType.Trim()
             && (s.SportSubType ?? "") == sportSubType.Trim()
       select u).ToList();

    }

    public List<WebUser> GetCustomers(string store, string contestType) {
      return (from u in _usersAndCustomers
              from s in u.Subscriptions
              where s.SportType != null
                    && u.Context != null
                    && u.Store == store.Trim()
                    && u.Type == User.UserType.Customer
                    && s.ContestType == contestType.Trim()
                    && s.Type == Subscription.SubscriptionType.Contest
              select u).ToList();

    }

    #endregion

    #region Agents

    public List<WebUser> GetAgents() {
      return _usersAndCustomers.Where(c => c.Type == User.UserType.Agent && c.Context != null).ToList();
    }

    #endregion

  }
}