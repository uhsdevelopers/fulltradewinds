﻿'use strict';

app.controller('SIDSocketAdminController', ['$rootScope', '$scope', '$webSocketService',
    function ServerTimeController($rootScope, $scope, $webSocketService) {
      $scope.users = $webSocketService.users;

      $rootScope.$on('usersUpdated', function () {
        $scope.users = $webSocketService.users;
      });

      $scope.CloseCustomerSession = function (customerId) {
        console.log('Test1');
        $webSocketService.CloseCustomerSession(customerId);
      };
    }
]);