﻿app.factory('$webSocketService', ['$rootScope', function ($rootScope) {

  var webSocketService = {};
  var connectionSate = 0;
  var _servers = ['signalr'];
  var _selectedServer;
  var customerSubscribed = false;
  var _GetSelectServer = function () {
    if (!_servers || _servers.length === 0) return "";
    if (_selectedServer) return _servers[_selectedServer];
    else _selectedServer = Math.floor(Math.random() * _servers.length);
    return _servers[_selectedServer];
  };

  jQuery.support.cors = true;

  var ws = $ != null ? $.connection : null;

  var _server = _GetSelectServer();

  if (ws && _server !== "") ws.hub.url = _server;

  var sidHub = ws != null ? ws.sidHub : null;

  //var hubReady = null;

  var hubIsActive = function () {
    return sidHub;
  };

  if (hubIsActive()) {
      //ws.hub.logging = true;

    sidHub.client.broadcastMessage = function (message) {
      //console.log(message);
    }

    sidHub.client.returnCustomers = function (users) {
      webSocketService.users = users;
      //console.log(users);
      $rootScope.$broadcast('usersUpdated');
      $rootScope.$apply();
      //console.log(users);
    }

    ws.hub.start(function () {
      //console.log(ws.sidHub);
      webSocketService.SubscribeAdminUser();
    }).fail(function (error) {
      console.log(error);
      //_ReportError("Connection fail: " + error);
    });
    ws.hub.stateChanged(function (change) {
      connectionSate = change.newState;
    });
  }


  webSocketService.IsSupported = function () {
    if ("WebSocket" in window) return true;
    //_ReportError("WebSocket NOT supported by your Browser!");
    return false;
  };

  webSocketService.IsConnected = function () {
    return connectionSate !== 4;
  };

  webSocketService.GetState = function () {
    return connectionSate;
  };

  webSocketService.GetCurrentServer = function () {
    return _servers[_selectedServer];
  }

  webSocketService.SubscribeAdminUser = function() {
    sidHub.server.subscribeAdminUser();
  }

  webSocketService.CloseCustomerSession = function (customerId) {
    console.log('Test2');
    sidHub.server.closeCustomerSession(customerId);
  }

  return webSocketService;

}]);