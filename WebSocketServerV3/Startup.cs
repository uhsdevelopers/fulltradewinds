﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;

namespace WebSocketServerV3 {
  [assembly: OwinStartup(typeof(Startup))]
  public class Startup {

    public void Configuration(IAppBuilder app) {
      // Any connection or hub wire up and configuration should go here
      //app.MapSignalR();

      app.Map("/signalr", map => {
        map.UseCors(CorsOptions.AllowAll);
        var hubConfiguration = new HubConfiguration { };
        map.RunSignalR(hubConfiguration);
      });


    }
  }
}