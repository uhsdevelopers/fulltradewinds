﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Timers;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using SIDWebLibraries.HelperClasses;
using WebSocketServerV3.Helpers;
using WebSocketServerV3.Models;

namespace WebSocketServerV3 {
  public class SIDSocketServer {

    #region Consts

    private const string LINE_CHANGES = "LineChanges";

    #endregion

    private readonly InstanceManager.BusinessLayer.InstanceManager _im;
    private readonly DbContextManager _dbContext;
    private readonly static Lazy<SIDSocketServer> _instance = new Lazy<SIDSocketServer>(() => new SIDSocketServer(GlobalHost.ConnectionManager.GetHubContext<SIDHub>().Clients));
    private readonly UsersAndCustomersManager _usersAndCustomersManager;

    private readonly TreeManager _treeChangesManager;

    private DbChangesManager _dbChangesManager;

    private IHubConnectionContext<dynamic> Clients {
      get;
      set;
    }

    public static SIDSocketServer Instance {
      get {
        return _instance.Value;
      }
    }

    public SIDSocketServer(IHubConnectionContext<dynamic> clients) {
      if (_im != null) return;
      _im = CommonFunctions.GetInstance();
      _dbContext = new DbContextManager(_im);
      _usersAndCustomersManager = new UsersAndCustomersManager();

      if (_treeChangesManager == null) {
        _treeChangesManager = new TreeManager(_im, new DataManager(_im.InstanceModuleInfo, DataManager.UpdatingType.LinesOnly) { RecordNewElements = true });
      }

      //_users = new List<WebUser>();
      Clients = clients;
      SetTimers();
    }

    private void SetTimers() {
      /*
      var tLines = new Timer { Interval = 1000 };
      tLines.Elapsed += ReviewLineChanges;
      tLines.Enabled = true;
      */
      _dbChangesManager = new DbChangesManager(_im.InstanceModuleInfo, false, true, true);
      _dbChangesManager.OnLinesChanged += dbChangesManager_OnLinesChanged;
      _dbChangesManager.OnCustomerInfoChanged += dbChangesManager_OnCustomerInfoChanged;
      _dbChangesManager.OnSportsTreeChanged += _dbChangesManager_OnSportsTreeChanged;
      _dbChangesManager.OnGamesContestsChanged += _dbChangesManager_OnGamesContestsChanged;

      _dbChangesManager.Start(_im.InstanceModuleInfo.ServerDateTime);

      /*
      var tCustomerChanges = new Timer { Interval = 1000 };
      tCustomerChanges.Elapsed += ReviewCustomerChanges;
      tCustomerChanges.Enabled = true;

      
      var tSportsTreeChanges = new Timer { Interval = 1000 };
      tSportsTreeChanges.Elapsed += ReviewSportsTreeChanges;
      tSportsTreeChanges.Enabled = true;
      */

      var tMessageChanges = new Timer { Interval = 1000 };
      tMessageChanges.Elapsed += ReviewMessageChanges;
      tMessageChanges.Enabled = true;

      var tReviewWagerChanges = new Timer { Interval = 1000 };
      tReviewWagerChanges.Elapsed += ReviewWagerChanges;
      tReviewWagerChanges.Enabled = true;
    }

    void _dbChangesManager_OnGamesContestsChanged(object sender, EventArgs e) {
      if (!UsersConnected()) return;
      var changedGames = ((DbChangesManager.DbChangesEventArgs)e).GameChanges;
      var customers = _usersAndCustomersManager.GetCustomers();

      foreach (var subSport in (from cg in changedGames group cg by new { cg.SportType, cg.SportSubType } into g select new { g.Key.SportType, g.Key.SportSubType })) {
        foreach (var user in customers.Where(
          c => c.Subscriptions.Any(s => s.Type == Subscription.SubscriptionType.Game &&
                                        s.SportType == subSport.SportType && s.SportSubType == subSport.SportSubType))) {

          Clients.Client(user.Context.ConnectionId).gamesChanged(changedGames.Where(g => g.SportType == subSport.SportType && g.SportSubType == subSport.SportSubType).ToList());
        }
      }
    }

    private void _dbChangesManager_OnSportsTreeChanged(object sender, EventArgs e) {
      if (!UsersConnected()) return;
      var args = (DbChangesManager.DbChangesEventArgs)e;

      var activeLeagues = args.ActiveLeagues;

      var customers = _usersAndCustomersManager.GetCustomers();
      foreach (var store in customers.Select(l => l.Store.Trim()).Distinct()) {
        var localStore = store;
        foreach (var user in customers.Where(c => c.Store == localStore)) {
          try {
            var data = new {
              activeLeagues = activeLeagues.Where(l => l.Store == store).OrderBy(al=>al.SportType).ThenBy(al => al.SportSubType).ThenBy(al => al.ContestType).ThenBy(al => al.ContestType2).ToList()
            };
            Clients.Client(user.Context.ConnectionId).leagueChange(data);
          }
          catch (Exception ex) {
            DebugError(ex);
          }
        }
      }
    }

    private void dbChangesManager_OnCustomerInfoChanged(object sender, EventArgs e) {
      try {
        if (!UsersConnected()) return;
        var args = (DbChangesManager.DbChangesEventArgs)e;
        if (args.CustomerInfo == null || !args.CustomerInfo.Any()) return;

        foreach (var change in args.CustomerInfo) {
          //lock (_users) {
          var usr = _usersAndCustomersManager.GetCustomer((change.CustomerID ?? "").Trim());
          if (usr == null) continue;
          try {
            Broadcast(DbContextManager.SerializeChange(new {
              Context = "CustomerInfoChanges",
              Data = new { change.ChangeNum, CustomerID = (change.CustomerID ?? "").Trim(), change.CustomerChangeDateTime }
            }), new List<WebUser> { usr });
          }
          catch (Exception ex) {
            DebugError(ex);
          }
        }

      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        DebugError(ex);
      }
    }

    private void dbChangesManager_OnLinesChanged(object sender, EventArgs e) {
      try {
        if (!UsersConnected()) return;
        var args = (DbChangesManager.DbChangesEventArgs)e;
        if (args.LineChanges == null || !args.IsLineChange || !args.LineChanges.Any()) return;

        switch (args.ChangeType) {
          case DbChangesManager.DbChangesEventArgs.ChangeTypes.GameLine:
            BroadcastGameLineChanges((from c in args.LineChanges
                                      where c.ContestantNum == 0
                                      select c).ToList());
            break;
          case DbChangesManager.DbChangesEventArgs.ChangeTypes.ContestLine:
            BroadcastContestChanges((from c in args.LineChanges
                                     where c.ContestantNum > 0
                                     select c).ToList());
            break;
        }
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        DebugError(ex);
      }

    }

    public void AddUser(WebUser user) {
      _usersAndCustomersManager.Add(user);
    }

    public void AddSportOrContest(Subscription subscription, dynamic context) {
      var cst = _usersAndCustomersManager.Get(context.ConnectionId);
      if (subscription.SportType != null) subscription.SportType = subscription.SportType.Trim();
      if (subscription.SportSubType != null) subscription.SportSubType = subscription.SportSubType.Trim();
      if (subscription.Store != null) subscription.Store = subscription.Store.Trim();
      if (subscription.ContestType != null) subscription.ContestType = subscription.ContestType.Trim();
      if (subscription.ContestType2 != null) subscription.ContestType2 = subscription.ContestType2.Trim();
      if (subscription.ContestType3 != null) subscription.ContestType3 = subscription.ContestType3.Trim();
      cst.Subscriptions.Add(subscription);
    }

    public void RemoveSport(Subscription subscription, string connectionId) {
      var cst = _usersAndCustomersManager.Get(connectionId);
      cst.Subscriptions.RemoveAll(
            s => s.SportType == subscription.SportType
            && s.SportSubType == subscription.SportSubType
            && s.Type == Subscription.SubscriptionType.Game);
    }

    public void RemoveContest(Subscription subscription, string connectionId) {
      var cst = _usersAndCustomersManager.Get(connectionId);

      cst.Subscriptions.RemoveAll(
            x => x.ContestType == subscription.ContestType
            && x.ContestType2 == subscription.ContestType2
            && x.Type == Subscription.SubscriptionType.Contest);
    }

    public void RemoveUser(string connectionId) {
      _usersAndCustomersManager.Remove(connectionId);
    }

    public void RemoveCustomer(string customerId) {
      _usersAndCustomersManager.RemoveCustomer(customerId);
    }

    #region Timers

    private void ReviewGameChanges(object sender, ElapsedEventArgs e) {
      if (!UsersConnected()) return;
    }

    /*private void ReviewLineChanges(object sender, ElapsedEventArgs e) {
      try {
        if (!UsersConnected()) return;
        var changes = _dbContext.GetLastLineChanges();
        if (changes == null || !changes.Any()) return;
        //if (ShowActivity) Broadcast(DbContextManager.SerializeChange(new { Context = "lineChanged", Data = "Dblines Changes: " + changes.Count() + ". Users Online: " + _webUsers.Count(x => x.Type == User.UserType.Customer) }), _adminUsers);

        BroadcastGameLineChanges((from c in changes
                              where c.ContestantNum == 0
                              select c).ToList());
        BroadcastContestChanges((from c in changes
                                 where c.ContestantNum > 0
                                 select c).ToList());
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        DebugError(ex);
      }

    }

    
    private void ReviewSportsTreeChanges(object sender, ElapsedEventArgs e) {
      try {
        if (!UsersConnected() || _treeChangesManager == null) return;
        using (var cntSvc = new Contests(_im.InstanceModuleInfo)) {
          if (_users != null) {
            var webUsers = _webUsers.Where(u => u.Type == User.UserType.Customer).ToList();
            if (webUsers.Any()) _treeChangesManager.Refresh(webUsers, cntSvc);
          }
        }
        FillSequenceNumbers(_treeChangesManager.Notifications);
        BroadcastNotifications(_treeChangesManager.Notifications);
      }
      catch (Exception ex) { DebugError(ex); }
    }

    private void ReviewCustomerChanges(object sender, ElapsedEventArgs e) {
      try {
        if (_dbContext == null) return;
        var changes = _dbContext.GetLastCustomerChanges();

        if (changes == null || !changes.Any() || !UsersConnected()) return;
        //if (ShowActivity) Broadcast(DbContextManager.SerializeChange("Dbcustomers Changes: " + changes.Count() + ". Users Online: " + _webUsers.Count()), _adminUsers);
        foreach (var change in changes) {
          //lock (_users) {
          var usr = _usersAndCustomersManager.GetCustomer(change.CustomerID);
          if (usr == null) continue;
          try {
            Broadcast(DbContextManager.SerializeChange(new {
              Context = "CustomerInfoChanges",
              Data = new { change.ChangeNum, CustomerID = (change.CustomerID ?? "").Trim(), change.CustomerChangeDateTime }
            }), new List<WebUser> { usr });
          }
          catch (Exception ex) {
            DebugError(ex);
          }
        }
      }
      catch (Exception ex) {
        DebugError(ex);
      }
    }
    */
    private void ReviewWagerChanges(object sender, ElapsedEventArgs e) {
      if (ConfigurationManager.AppSettings["AgentsService"] == "0") return;
      try {
        if (ConfigurationManager.AppSettings["AgentsService"] == "0") return;
        if (_dbContext == null) return;
        var changes = _dbContext.GetLastPostedWagers();

        if (changes == null || !changes.Any() || !UsersConnected()) return;
        //if (ShowActivity) Broadcast(DbContextManager.SerializeChange(new { Context = "lineChanged", Data = "DbWagers Inserted: " + changes.Count() + ". Agents Online: " + _webUsers.Count(x => x.Type == User.UserType.Agent) }), _adminUsers);
        var targetAgents = new List<WebUser>();
        var tempUsers = new List<WebUser>();
        tempUsers.AddRange(_usersAndCustomersManager.GetCustomers());
        foreach (var change in changes) {
          var change1 = change;
          var usr = (from u in tempUsers
                     where u.Type == User.UserType.Agent && u.CustomerId == change1.AgentID.Trim()
                     select u);
          targetAgents.AddRange(usr);
        }

        foreach (var ag in targetAgents.Distinct()) {
          var data = (from w in changes where w.AgentID.Trim() == ag.CustomerId select w).ToList();
          var userCount = tempUsers.Count(x => x.AgentId == ag.CustomerId);
          try {
            Broadcast(DbContextManager.SerializeChange(new {
              Context = "WagersInfoChange",
              Data = new { data, userCount }
            }), new List<WebUser> { ag });
          }
          catch (Exception ex) {
            DebugError(ex);
          }

        }
      }
      catch (Exception ex) {
        Console.WriteLine("OnTimedCustInfoEvent: {0}", ex.Message);
        DebugError(ex);
      }

    }

    private void ReviewMessageChanges(object sender, ElapsedEventArgs e) {
      try {
        if (_dbContext == null) return;
        var changes = _dbContext.GetLastMessagingChanges();

        if (changes == null || !changes.Any() || !UsersConnected()) return;
        //if (ShowActivity) Broadcast(DbContextManager.SerializeChange(new { Context = "messageChange", Data = "Db message Changes: " + changes.Count() + ". Users Online: " + _webUsers.Count(x => x.Type == User.UserType.Customer) }), _adminUsers);

        foreach (var change in changes) {
          var usr = _usersAndCustomersManager.GetCustomer(change.CustomerID);
          if (usr == null) continue;
          try {
            Broadcast(DbContextManager.SerializeChange(new {
              Context = "CustomerInboxChanges",
              Data = change
            }), new List<WebUser> { usr });
          }
          catch (Exception ex) {
            DebugError(ex);
          }
        }
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        DebugError(ex);
      }
    }

    #endregion

    /*
    private void FillSequenceNumbers(List<SportTreeNotification> notifications) {
      try {
        if (!notifications.Any()) return;

        using (var sportTypeSvc = new SportTypes(_im.InstanceModuleInfo)) {
          foreach (var notification in notifications.Where(notification => notification.Context.Contains("New") && notification.Data != null)) {
            notification.Data.SequenceNumber = sportTypeSvc.GetSequenceNum(notification.Data.SportType, notification.Data.SportSubType);
          }
        }
      }
      catch (Exception ex) { DebugError(ex); }
    }
    */

    private void BroadcastGameLineChanges(IEnumerable<spGLGetLastLinesChange_Result> lines) {
      if (!UsersConnected() || lines == null) return;
      try {

        foreach (var lineGroup in (from l in lines group l by new { l.Store, l.SportType, l.SportSubType } into lg select new { lg.Key.Store, lg.Key.SportType, lg.Key.SportSubType })) {
          var usrs = _usersAndCustomersManager.GetCustomers(lineGroup.Store, lineGroup.SportType, lineGroup.SportSubType);
          if (!usrs.Any()) continue;
          try {
            Broadcast(DbContextManager.SerializeChange(new {
              Context = LINE_CHANGES,
              Data = lines.Where(l => l.Store == lineGroup.Store && l.SportType == lineGroup.SportType && l.SportSubType == lineGroup.SportSubType)
            }),
              usrs);
          }
          catch (Exception ex) {
            DebugError(ex);
          }
        }
      }
      catch (Exception ex) { DebugError(ex); }
    }

    private void BroadcastContestChanges(IEnumerable<spGLGetLastLinesChange_Result> contestantLines) {
      if (!UsersConnected() || contestantLines == null) return;
      foreach (var line in contestantLines) {
        try {
          var usrs = _usersAndCustomersManager.GetCustomers(line.Store, line.ContestType);
          /*
          var usrs = (from u in _webUsers
                      from s in u.Subscriptions
                      where u.Type == User.UserType.Customer && ln.Store == u.Store.Trim()
                        && u.Context != null
                        && !string.IsNullOrEmpty(u.CustomerId)
                        && ((s.ContestType != null && s.ContestType == ln.ContestType.Trim() && s.Type == Subscription.SubscriptionType.Contest)
                        || (s.SportType != null && s.SportType == ln.ContestType.Trim() && s.Type == Subscription.SubscriptionType.Game))
                      select u).ToList();
          */
          if (!usrs.Any()) continue;
          try {
            Broadcast(DbContextManager.SerializeChange(new { Context = LINE_CHANGES, Data = line }), usrs);
          }
          catch (Exception ex) {
            DebugError(ex);
          }
        }
        catch (Exception ex) { DebugError(ex); }
      }
    }

    /*private void BroadcastNotifications(List<SportTreeNotification> notifications) {
      if (!UsersConnected() || notifications == null) return;
      try {
        foreach (var n in notifications) {
          var storeName = n.Data.Store;
          if (_users == null) continue;
          var cst = _webUsers.Where(x => x.Store != null && x.Type == User.UserType.Customer
                                      && x.Store.Trim() == storeName).ToList();
          switch (n.BroadcastType) {
            case BroadcastType.AllUsersInStore:
              BroadcastNotification(n, users);
              break;
            case BroadcastType.SubscribedUsers:
              BroadcastNotification(n, (from u in users
                                        from s in u.Subscriptions
                                        where s.Type == Subscription.SubscriptionType.Game
                                              && n.Data.ContestType == null
                                              && n.Data.ContestType2 == null
                                              && StringF.ToStringOrNull(s.SportType) == n.Data.SportType
                                              && (n.Data.SportSubType == null
                                                  || StringF.ToStringOrNull(s.SportSubType) == n.Data.SportSubType)
                                        select u).ToList());
              BroadcastNotification(n, (from u in users
                                        from s in u.Subscriptions
                                        where s.Type == Subscription.SubscriptionType.Contest
                                              && n.Data.ContestType != null
                                              && n.Data.ContestType2 != null
                                              && n.Data.ContestType == StringF.ToStringOrNull(s.ContestType)
                                              && n.Data.ContestType2 == StringF.ToStringOrNull(s.ContestType2)
                                        select u).ToList());
              break;
          }
        }
      }
      catch (Exception ex) {
        DebugError(ex);
      }
    }*/

    private void BroadcastNotification(SportTreeNotification notification, List<WebUser> usrs) {
      if (!UsersConnected()) return;
      try {
        Broadcast(DbContextManager.SerializeChange(new { notification.Context, notification.Data }), usrs);
      }
      catch (Exception ex) {
        DebugError(ex);
      }
    }

    private void Broadcast(string message, List<WebUser> users = null) {
      try {
        if (users == null) {
          if (!_usersAndCustomersManager.AnyCustomer()) return;
          users = new List<WebUser>();
          users.AddRange(_usersAndCustomersManager.GetCustomers());
          foreach (var u in users.Where(u => u.Context != null)) {
            Clients.Client(u.Context.ConnectionId).broadcastMessage(message);
          }
        }
        else {
          foreach (var u in users.Where(u => u.Context != null))
            Clients.Client(u.Context.ConnectionId).broadcastMessage(message);
        }
      }
      catch (Exception ex) { DebugError(ex); }
    }

    public void BroadcastCustomers() {
      var users = _usersAndCustomersManager.GetUsers();
      if (!users.Any()) return;
      var customers = _usersAndCustomersManager.GetCustomers().Select(u => new { u.CustomerId, u.Type, u.Subscriptions, u.Store, u.Platform }).ToList();
      foreach (var u in users) {
        Clients.Client(u.Context.ConnectionId).returnCustomers(customers);
      }
    }

    public void CloseCustomerSession(string customerId) {
      var cst = _usersAndCustomersManager.GetCustomer(customerId);
      if (cst != null) Clients.Client(cst.Context.ConnectionId).closeSession();
    }

    public List<WebUser> GetCustomers() {
      return _usersAndCustomersManager.GetCustomers();
    }

    #region Clients Manager

    private bool UsersConnected() {
      try {
        return _usersAndCustomersManager.AnyCustomer();
      }
      catch (Exception ex) {
        DebugError(ex);
        return false;
      }
    }

    #endregion

    #region Error Handling & Debug

    private void DebugError(Exception e) {
      CConsole.WriteLine(e);
    }

    #endregion

  }
}