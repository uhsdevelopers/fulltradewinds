﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using SIDLibraries.Utilities;

namespace WebSocketServerV3.Helpers {
  public class DbContextManager : IDisposable {
    private int _lastChangeNum;
    private int? _lastCustInfoChangeNum;
    private int _lastCustMsgChangeNum;
    private readonly InstanceManager.BusinessLayer.InstanceManager _im;
    private int? _lastPendingTicketNum;
    private int? _lastDeletedPendingTicketNum;

    public DbContextManager(InstanceManager.BusinessLayer.InstanceManager im) {
      _im = im;
    }

    public string GetLasLineChangesJson() {
      List<spGLGetLastLinesChange_Result> result;
      try {
        using (var gl = new GamesAndLines(_im.InstanceModuleInfo)) {
          result = gl.GetLastLinesChange(_lastChangeNum, ".");
        }
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return null;
      }

      if (result == null || result.Count <= 0) return String.Empty;
      _lastChangeNum = (from r in result select r.ChangeNum).Max();
      return JsonConvert.SerializeObject(result);
    }

    public static string SerializeChange(object change) {
      try {
        return JsonConvert.SerializeObject(change);
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return string.Empty;
      }
    }

    public List<spCstGetLastCustomersChange_Result> GetLastCustomerChanges() {
      try {
        using (var cst = new Customers(_im.InstanceModuleInfo)) {
          if ((_lastCustInfoChangeNum ?? 0) == 0) {
            _lastCustInfoChangeNum = cst.GetLastCustomerChangeNumber();
            return null;
          }
          var result = cst.GetLastCustomersChangeNoFilter(_lastCustInfoChangeNum);

          if (result == null || result.Count == 0)
            return null;
          _lastCustInfoChangeNum = (from r in result
                                    select r.ChangeNum).Max();
          return result;
        }
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return null;
      }
    }

    public List<spTkWGetLiveTickerWagerByTicketNumber_Result> GetLastPostedWagers() {
      try {
        using (var tkw = new TicketsAndWagers(_im.InstanceModuleInfo)) {
          if (_lastPendingTicketNum == null || _lastPendingTicketNum == 0) {
            _lastPendingTicketNum = tkw.GetLastPendingWagerNumber(0);
            _lastDeletedPendingTicketNum = 0;
            return null;
          }
          var result = tkw.GetLastPendingWagers((int)_lastPendingTicketNum, _lastDeletedPendingTicketNum ?? 0);

          if (result == null || result.Count == 0)
            return null;

          if (result.Any(r => r.Inserted ?? false)) {
            _lastPendingTicketNum = result.Where(r => r.Inserted ?? false).Max(r => r.TicketNumber);
          }
          if (result.Any(r => !(r.Inserted ?? false))) {
            _lastDeletedPendingTicketNum = result.Where(r => !(r.Inserted ?? false)).Max(r => r.TicketNumber);
          }
          return result;
        }
      }
      catch (Exception ex) {
        Console.Write(ex.Message);
        return null;
      }
    }

    public List<spGLGetLastLinesChange_Result> GetLastLineChanges() {
      try {
        using (var gl = new GamesAndLines(_im.InstanceModuleInfo)) {
          if (_lastChangeNum == 0) {
            _lastChangeNum = gl.GetLastLineChangeNumber();
            return null;
          }
          var result = gl.GetLastLinesChangeNoFilter(_lastChangeNum).Where(c => c.CircledFlag == null).ToList();

          if (result.Count == 0)
            return null;
          _lastChangeNum = (from r in result
                            select r.ChangeNum).Max();

          return result;
        }
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return null;
      }
    }

    public List<spGLGetLastGamesChange_Result> GetLastGameChanges() {
      try {
        using (var gl = new GamesAndLines(_im.InstanceModuleInfo)) {
          if (_lastChangeNum == 0) {
            _lastChangeNum = gl.GetLastGameChangeNumber();
            return null;
          }
          var result = gl.GetLastGamesChange(_lastChangeNum).ToList();

          if (result.Count == 0)
            return null;
          _lastChangeNum = (from r in result
                            select r.ChangeNum).Max();

          return result;
        }
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return null;
      }
    }

    public List<spCstWebGetCustomerMessages_Result> GetLastMessagingChanges() {
      try {
        using (var cst = new Customers(_im.InstanceModuleInfo)) {
          if (_lastCustMsgChangeNum == 0) {
            _lastCustMsgChangeNum = cst.GetLastCustMsgChangeNumber();
            return null;
          }
          var result = cst.GetLastCustMsgChange(_lastCustMsgChangeNum);

          if (result == null || result.Count == 0)
            return null;
          _lastCustMsgChangeNum = (from r in result
                                   select r.ChangeNum).Max();
          return (from r in result
                  select cst.GetCustomerMsgById(r.CustomerMessageId, r.Action)).Where(r => r != null).ToList();
        }
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return null;
      }
    }

    public void Error(Exception ex) {
      using (var lg = new LogWriter(_im.InstanceModuleInfo)) {
        lg.WriteToSystemLog(ex);
      }
    }

    public spGLGetGame_Result GetGameInfo(int gamenum) {
      spGLGetGame_Result result;
      try {
        using (var gl = new GamesAndLines(_im.InstanceModuleInfo)) {
          result = gl.GetGame(gamenum);
        }
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return null;
      }
      return result;
    }

    public void Dispose() {
    }
  }
}