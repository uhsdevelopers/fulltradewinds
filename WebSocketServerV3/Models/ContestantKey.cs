﻿namespace WebSocketServerV3.Models {
  public class ContestantKey {
    public int ContestNum { get; private set; }
    public int ContestantNum { get; private set; }

    public ContestantKey(int contestNum, int contestantNum) {
      ContestNum = contestNum;
      ContestantNum = contestantNum;
    }
  }
}