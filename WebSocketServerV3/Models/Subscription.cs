﻿namespace WebSocketServerV3.Models {
  public class Subscription {

    public enum SubscriptionType { Game = 1, Contest = 2 };

    public string SportType { get; set; }
    public string SportSubType { get; set; }
    public string ContestType { get; set; }
    public string ContestType2 { get; set; }
    public string ContestType3 { get; set; }
    public SubscriptionType Type { get; set; }
    public string Store { get; set; }
  }
}