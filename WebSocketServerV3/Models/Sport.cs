﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace WebSocketServerV3.Models {
  public class Sport : ReportableChanges {
    #region Constructors

    public Sport(string name, List<League> leagues, List<Contest> contests)
      : base(null) {
      Contests = new ObservableCollection<Contest>(contests);
      Contests.CollectionChanged += ContestsChanged;

      Leagues = new ObservableCollection<League>(leagues);
      Leagues.CollectionChanged += LeaguesChanged;
      Name = name;
      Notifications = new List<SportTreeNotification>();
    }

    #endregion

    #region Public Properties

    public string Name { get; private set; }

    public ObservableCollection<League> Leagues { get; private set; }

    public ObservableCollection<Contest> Contests { get; private set; }

    #endregion

    #region Private Methods

    private void ContestsChanged(object sender, NotifyCollectionChangedEventArgs e) {
      if (Notifications == null || !MonitorChanges)
        return;
      if (e.NewItems != null) {
        foreach (var item in e.NewItems)
          Notifications.Add(new SportTreeNotification {
            BroadcastType = BroadcastType.AllUsersInStore,
            Context = "New Contest",
            Data = new SportTreeNotification.NotificationData {
              SportType = Name,
              SportSubType = null,
              ContestType = ((Contest)item).ContestType,
              ContestType2 = ((Contest)item).ContestType2,
              ContestType3 = ((Contest)item).ContestType3
            }
          });
      }
      if (e.OldItems == null)
        return;
      foreach (var item in e.OldItems)
        Notifications.Add(new SportTreeNotification {
          BroadcastType = BroadcastType.AllUsersInStore,
          Context = "Removed Contest",
          Data = new SportTreeNotification.NotificationData {
            SportType = Name,
            SportSubType = null,
            ContestType = ((Contest)item).ContestType,
            ContestType2 = ((Contest)item).ContestType2,
            ContestType3 = ((Contest)item).ContestType3
          }
        });
    }

    private void LeaguesChanged(object sender, NotifyCollectionChangedEventArgs e) {
      if (Notifications == null || !MonitorChanges)
        return;
      if (e.NewItems != null) {
        foreach (var item in e.NewItems)
          Notifications.Add(new SportTreeNotification {
            BroadcastType = BroadcastType.AllUsersInStore,
            Context = "New League",
            Data = new SportTreeNotification.NotificationData {
              SportType = Name,
              SportSubType = ((League)item).Name
            }
          });
      }
      if (e.OldItems == null)
        return;
      foreach (var item in e.OldItems)
        Notifications.Add(new SportTreeNotification {
          BroadcastType = BroadcastType.AllUsersInStore,
          Context = "Removed League",
          Data = new SportTreeNotification.NotificationData {
            SportType = Name,
            SportSubType = ((League)item).Name
          }
        });
    }

    #endregion

  }
}