﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using InstanceManager.BusinessLayer;
using SIDLibraries.BusinessLayer;

namespace WebSocketServerV3.Models {
  public class League : ReportableChanges {
    #region Constructors

    public League(ModuleInfo im, string sportType, string name, IEnumerable<Period> periods, IEnumerable<Contest> contests, int sequenceNumber = 0)
      : base(im) {

      Name = name;
      Notifications = new List<SportTreeNotification>();
      SportType = sportType;
      SequenceNumber = sequenceNumber;

      Contests = new ObservableCollection<Contest>();
      Contests.CollectionChanged += ContestsChanged;
      foreach (var contest in contests) {
        Contests.Add(contest);
      }

      Periods = new ObservableCollection<Period>();
      Periods.CollectionChanged += PeriodsChanged;
      foreach (var period in periods) {
        Periods.Add(period);
      }
    }

    #endregion

    #region Public Properties

    public string Name { get; private set; }

    public string SportType { get; private set; }

    public int SequenceNumber { get; private set; }

    public ObservableCollection<Contest> Contests { get; private set; }

    public ObservableCollection<Period> Periods { get; private set; }

    #endregion

    #region Private Methods

    private void ContestsChanged(object sender, NotifyCollectionChangedEventArgs e) {
      if (Notifications == null || !MonitorChanges)
        return;
      if (e.NewItems != null) {
        foreach (var item in e.NewItems)
          Notifications.Add(new SportTreeNotification {
            BroadcastType = BroadcastType.AllUsersInStore,
            Context = "New Contest",
            Data = new SportTreeNotification.NotificationData {
              SportType = SportType,
              SportSubType = Name,
              ContestType = ((Contest)item).ContestType,
              ContestType2 = ((Contest)item).ContestType2,
              ContestType3 = ((Contest)item).ContestType3
            }
          });
      }
      if (e.OldItems == null)
        return;
      foreach (var item in e.OldItems)
        Notifications.Add(new SportTreeNotification {
          BroadcastType = BroadcastType.AllUsersInStore,
          Context = "Removed Contest",
          Data = new SportTreeNotification.NotificationData {
            SportType = SportType,
            SportSubType = Name,
            ContestType = ((Contest)item).ContestType,
            ContestType2 = ((Contest)item).ContestType2,
            ContestType3 = ((Contest)item).ContestType3
          }
        });
    }

    private void PeriodsChanged(object sender, NotifyCollectionChangedEventArgs e) {
      if (Notifications == null || !MonitorChanges)
        return;
      if (e.NewItems != null) {
        using (var sportTypeSvc = new SportTypes(Im)) {
          foreach (var item in e.NewItems) {
            var periodDescs = sportTypeSvc.GetPeriodDescriptions(SportType, Name, ((Period)item).Number);

            Notifications.Add(new SportTreeNotification {
              BroadcastType = BroadcastType.AllUsersInStore,
              Context = "New Period",
              Data = new SportTreeNotification.NotificationData {
                ShortDescription = periodDescs == null ? "" : periodDescs.ShortDescription,
                SportType = SportType,
                SportSubType = Name,
                PeriodNumber = ((Period)item).Number,
                PeriodDescription = periodDescs == null ? "" : periodDescs.PeriodDescription ?? ""
              }
            });
          }
        }
      }
      if (e.OldItems == null)
        return;
      foreach (var item in e.OldItems)
        Notifications.Add(new SportTreeNotification {
          BroadcastType = BroadcastType.AllUsersInStore,
          Context = "Removed Period",
          Data = new SportTreeNotification.NotificationData {
            SportType = SportType,
            SportSubType = Name,
            PeriodNumber = ((Period)item).Number
          }
        });
    }

    #endregion
  }
}