﻿using System.Collections.Generic;
using SIDLibraries.BusinessLayer;

namespace WebSocketServerV3.Models {
  public interface ITreeManager {
    IDataManager LinesDataMng { get; }
    List<Store> Memory { get; }
    void Refresh(List<WebUser> users, IContests cntSvc);
  }
}