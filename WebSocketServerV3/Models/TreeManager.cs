﻿using System;
using System.Collections.Generic;
using System.Linq;
using SIDLibraries.BusinessLayer;
using SIDLibraries.Entities;
using SIDLibraries.Utilities;

namespace WebSocketServerV3.Models {
  public class TreeManager : ITreeManager {
    #region Private Properties/Fields

    private List<spCnGetSportLinks_Result> _contestSportLinks;
    //private TimeSpan _dateTimeDiff;
    private List<WebUser> _users;

    private readonly InstanceManager.BusinessLayer.InstanceManager _im;

    private const string HORSEO_FUTURE = "HORSEO / FUTURE";
    private const string OTHER_PROPS = "Other props";

    #endregion

    #region Public Properties

    public IDataManager LinesDataMng { get; private set; }
    public List<Store> Memory { get; private set; }
    public List<SportTreeNotification> Notifications { get; private set; }
    public bool Active { get; set; }

    #endregion

    #region Constructors

    public TreeManager(InstanceManager.BusinessLayer.InstanceManager im, IDataManager dataMng, bool active = true) {
      _im = im;
      //using (var p = new SystemParameters(_im.InstanceModuleInfo)) _dateTimeDiff = DateTime.Now - p.GetServerDateTime();

      Notifications = new List<SportTreeNotification>();
      Memory = new List<Store>();
      LinesDataMng = dataMng;
      Active = active;
    }

    #endregion

    #region Public Methods

    public void Refresh(List<WebUser> users, IContests cntSvc) {
      try {
        if (!Active) return;
        _users = users;
        var stores = GetStoresFromUsers();
        if (stores == null) return;
        var shouldSetup = (stores.Any() && !Memory.Any()) || stores.Count != Memory.Count;
        ClearNotifications();
        var shouldUpdate = LinesDataMng.CallGetDbUpdates() && stores.Any();

        DeletedUnusedStoresFromMemory(stores);
        if ((!shouldUpdate) && !shouldSetup) return;

        SetContestSportLinks(cntSvc);
        SetStoresInMemory(GetNewStoresFromUsers(stores));

        //lock (Memory) {
        foreach (var store in Memory) {
          store.MonitorChanges = !shouldSetup;
          RemoveNodes(store);
          AddNodes(store);
        }
        RemoveEmptyNodes();
        //}
        if (!shouldSetup) CollectTreeChangesNotifications();
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
      }
    }

    #endregion

    #region Private Methods

    /*
    private bool AreStoresInMemory(List<string> stores) {
      if (stores == null || !stores.Any() || Memory == null) return false;
      return stores.All(store => !Memory.All(w => w.Name != store));
    }
    */

    #region Build node Methods

    /// <summary>
    /// Creates a new Contest object if found any contestant for it
    /// </summary>
    /// <param name="storeName"></param>
    /// <param name="sportType"></param>
    /// <param name="sportSubtype"></param>
    /// <param name="contestType"></param>
    /// <param name="contestType2"></param>
    /// <param name="contestType3"></param>
    /// <returns></returns>
    private Contest BuildContest(string storeName, string sportType, string sportSubtype, string contestType, string contestType2, string contestType3) {
      var contestants = GetContestants(storeName, contestType, contestType2, contestType3);
      return !contestants.Any() ? null : new Contest(sportType, sportSubtype, contestType, contestType2, contestType3) { Contestants = contestants };
    }

    /// <summary>
    /// Build a contestant if valid contestant lines found
    /// </summary>
    /// <param name="storeName"></param>
    /// <param name="contestNum"></param>
    /// <param name="contestantNum"></param>
    /// <param name="contestanName"></param>
    /// <param name="contestType"></param>
    /// <param name="contestType2"></param>
    /// <param name="contestType3"></param>
    /// <returns></returns>
    private Contestant BuildContestant(string storeName, int contestNum, int contestantNum, string contestanName, string contestType, string contestType2, string contestType3) {
      var lines = GetContestantLines(storeName, contestNum, contestantNum).ToList();
      return !lines.Any() ? null : new Contestant(contestNum, contestantNum, contestanName, contestType, contestType2, contestType3) {
        Lines = lines,
        LinesCount = lines.Count
      };
    }

    private static ContestantLine BuildContestantLine(string storeName, int contestNum, int contestantNum, string custProfile) {
      return new ContestantLine(contestNum, contestantNum, storeName, custProfile);
    }

    private Game BuildGame(string storeName, int periodNum, int gameNum) {
      var gameLines = GetGameLines(storeName, gameNum, periodNum);
      return !gameLines.Any() ? null : new Game(gameNum, periodNum) { LinesCount = gameLines.Count };
    }

    private static GameLine BuildGameLine(string storeName, int periodNum, int gameNum, string custProfile) {
      return new GameLine(gameNum, periodNum, storeName, custProfile);
    }

    private League BuildLeague(string storeName, string sportName, string leagueName) {
      var contests = GetContests(storeName, sportName, leagueName);
      var periods = GetPeriods(storeName, sportName, leagueName);
      return !contests.Any() && !periods.Any() ? null : new League(_im.InstanceModuleInfo, sportName, leagueName, periods, contests);
    }

    private Period BuildPeriod(string storeName, string sportName, string leagueName, int periodNumber) {
      var games = GetGames(storeName, sportName, leagueName, periodNumber);
      return !games.Any() ? null : new Period(periodNumber, games, sportName, leagueName);
    }

    /// <summary>
    /// Creates a new Sport object if found any league or contest for it
    /// </summary>
    /// <param name="storeName"></param>
    /// <param name="sportName"></param>
    /// <returns></returns>
    private Sport BuildSport(string storeName, string sportName) {
      var contests = GetContests(storeName, sportName, null);
      var leagues = GetLeagues(storeName, sportName);
      return !contests.Any() && !leagues.Any() ? null : new Sport(sportName, leagues, contests);
    }

    #endregion

    #region Check presence in Data Manager Methods
    #endregion

    #region Check presence in Tree Methods

    private bool ContestantExistsInTree(string storeName, string contestType, string contestType2, string contestType3, string contestantName) {
      try {
        var store = Memory.FirstOrDefault(x => x.Name == storeName);
        if (store == null) return false;

        var contest = store.Sports.SelectMany(x => x.Contests)
          .FirstOrDefault(y => y.ContestType == contestType
                               && y.ContestType2 == contestType2 && y.ContestType3 == contestType3);
        var exists = contest != null && contest.Contestants.Any(x => x.ContestantName == contestantName);
        if (exists) return true;

        contest = store.Sports.SelectMany(x => x.Leagues)
          .SelectMany(y => y.Contests)
          .FirstOrDefault(z => z.ContestType == contestType
                               && z.ContestType2 == contestType2 && z.ContestType3 == contestType3);
        return contest != null && contest.Contestants.Any(x => x.ContestantName == contestantName);
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return false;
      }
    }

    private bool ContestantLineExists(string storeName, string contestType, string contestType2, string contestType3, string contestantName, int contestantNum) {
      try {
        var objStore = Memory.FirstOrDefault(x => x.Name == storeName);
        if (objStore == null) return false;

        var contest = objStore.Sports.SelectMany(x => x.Contests)
          .FirstOrDefault(y => y.ContestType == contestType
                               && y.ContestType2 == contestType2 && y.ContestType3 == contestType3);
        var exists = false;
        Contestant contestant;

        if (contest != null) {
          contestant = contest.Contestants.FirstOrDefault(x => x.ContestantName == contestantName);
          exists = contestant != null && contestant.Lines != null && contestant.Lines.Any(x => x.Key.ContestantNum == contestantNum) && contestant.LinesCount > 0;
        }
        if (exists) return true;

        contest = objStore.Sports.SelectMany(x => x.Leagues)
          .SelectMany(y => y.Contests)
          .FirstOrDefault(z => z.ContestType == contestType
                               && z.ContestType2 == contestType2 && z.ContestType3 == contestType3);
        if (contest == null) return false;

        contestant = contest.Contestants.FirstOrDefault(x => x.ContestantName == contestantName);
        return contestant != null && contestant.Lines.Any(x => x.Key.ContestantNum == contestantNum) && contestant.LinesCount > 0;
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return false;
      }
    }

    /// <summary>
    /// Checks if the contest exists in the Sports Tree
    /// </summary>
    /// <param name="storeName"></param>
    /// <param name="sportType"></param>
    /// <param name="sportSubType"></param>
    /// <param name="contestType"></param>
    /// <param name="contestType2"></param>
    /// <param name="contestType3"></param>
    /// <returns></returns>
    private bool ContestExists(string storeName, string sportType, string sportSubType, string contestType, string contestType2, string contestType3) {
      try {
        var store = Memory.FirstOrDefault(x => x.Name == storeName);
        if (store == null) return false;

        var sport = store.Sports.FirstOrDefault(x => x.Name == StringF.ToStringOrNull(sportType));

        if (sportSubType == null) {
          return sport != null && sport.Contests.Any(x => x.ContestType == contestType
                                                          && x.ContestType2 == contestType2 && x.ContestType3 == contestType3);
        }
        if (sport == null) return false;

        var league = sport.Leagues.FirstOrDefault(x => x.Name == StringF.ToStringOrNull(sportSubType));
        return league != null && league.Contests.Any(x => x.ContestType == contestType
                                                          && x.ContestType2 == contestType2 && x.ContestType3 == contestType3);
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return false;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="storeName"></param>
    /// <param name="sportType"></param>
    /// <param name="sportSubType"></param>
    /// <param name="periodNum"></param>
    /// <param name="gameNum"></param>
    /// <returns></returns>
    private bool GameExists(string storeName, string sportType, string sportSubType, int periodNum, int gameNum) {
      try {
        var store = Memory.FirstOrDefault(x => x.Name == storeName);
        if (store == null) return false;

        var sport = store.Sports.FirstOrDefault(y => y.Name == sportType);
        if (sport == null) return false;

        var league = sport.Leagues.FirstOrDefault(z => z.Name == sportSubType);
        if (league == null) return false;

        var period = league.Periods.FirstOrDefault(a => a.Number == periodNum);
        return period != null && (period.Games.Any(b => b.GameNum == gameNum));
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return false;
      }
      /* var allGames = new List<Game>();
          foreach (var period in store.Sports.SelectMany(
              sport => sport.Leagues.SelectMany(
                  league => league.Periods)))
              allGames.AddRange(period.Games);
          List<spGLGetActiveGames_Result> newGames;
          if (allGames.Count > 0)
          {
              newGames = (from ag in _linesDataMng.ActiveGames
                          where allGames.Count > 0
                                  && allGames.All(alg => alg.GameNum != ag.GameNum)
                                  && ag.WagerCutoff >= CurrentDateTime()
                          select ag).ToList();
              allGames.Clear();
          }
          else
          {
              newGames = (from ag in _linesDataMng.ActiveGames
                          where ag.WagerCutoff >= CurrentDateTime()
                          select ag).ToList();
          }
          foreach (var newGame in newGames)
          {
              var g = newGame;
              foreach (var sport in store.Sports.Where(s => s.Name == StringF.ToStringOrNull(g.SportType)))
                  foreach (var league in sport.Leagues.Where(l => l.Name == StringF.ToStringOrNull(g.SportSubType)))
                      foreach (var period in league.Periods)
                      {
                          var game = BuildGame(storeName, period.Number, g.GameNum);
                          if (game != null) period.Games.Add(game);
                      }
          } */
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="storeName"></param>
    /// <param name="sportType"></param>
    /// <param name="sportSubType"></param>
    /// <returns></returns>
    private bool LeagueExists(string storeName, string sportType, string sportSubType) {
      try {
        var store = Memory.FirstOrDefault(x => x.Name == storeName);

        if (store == null) return false;
        var sport = store.Sports.FirstOrDefault(y => y.Name == sportType);
        return sport != null && (sport.Leagues.Any(z => z.Name == sportSubType));
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return false;
      }
      /* var allLeaguesBySport = new List<SportAndLeague>();

      foreach (var sport in store.Sports)
          allLeaguesBySport.AddRange(sport.Leagues.Select(league => new SportAndLeague(sport.Name, league.Name)));

      List<SportAndLeague> newLeaguesBySport;

      if (allLeaguesBySport.Count > 0)
      {
          newLeaguesBySport = (from   ag in _linesDataMng.ActiveGames
                                  where  !allLeaguesBySport.Any(al => al.Key      == ag.SportType.Trim()
                                                                      && al.Value == ag.SportSubType.Trim())
                                                                      && store.Sports.Any(s => s.Name == ag.SportType.Trim())
                                  group  ag by new { Sport = ag.SportType.Trim(), League = ag.SportSubType.Trim() } into agg
                                  select new SportAndLeague(agg.Key.Sport, agg.Key.League)).ToList();
          allLeaguesBySport.Clear();
      }
      else
      {
          newLeaguesBySport = (from   ag in _linesDataMng.ActiveGames
                                  group  ag by new { Sport = ag.SportType.Trim(), League = ag.SportSubType.Trim() } into agg
                                  select new SportAndLeague(agg.Key.Sport, agg.Key.League)).ToList();
      } */
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="storeName"></param>
    /// <param name="periodNum"></param>
    /// <param name="gameNum"></param>
    /// <returns></returns>
    private bool LineExists(string storeName, int periodNum, int gameNum) {
      try {
        var store = Memory.FirstOrDefault(x => x.Name == storeName);

        if (store == null) return false;
        var game = store
          .Sports.SelectMany(y => y.Leagues)
          .SelectMany(z => z.Periods)
          .Where(a => a.Number == periodNum)
          .SelectMany(b => b.Games)
          .FirstOrDefault(b => b.GameNum == gameNum);
        return game != null && game.LinesCount > 0;
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return false;
      }
      /*
             
var allLines = new List<GameLine>();

foreach (var game in store.Sports.SelectMany(
  sport => sport.Leagues.SelectMany(
      league => league.Periods.SelectMany(
          period => period.Games))))
  allLines.AddRange(game.Lines);

List<spGLGetActiveGamesAndLines_Result> newLines;

if (allLines.Count > 0)
{
  newLines = (from agl in _linesDataMng.ActiveGamesLines
              join ag in _linesDataMng.ActiveGames on agl.GameNum equals ag.GameNum
              where agl.Store.Trim() == storeName
                      && !allLines.Any(al => al.Key.GameNum == agl.GameNum
                                              && al.Key.PeriodNumber == agl.PeriodNumber
                                              && al.Key.Store == agl.Store.Trim()
                                              && al.Key.CustProfile == agl.CustProfile.Trim())
                      && ag.WagerCutoff >= CurrentDateTime()
              select agl).ToList();
  allLines.Clear();
}
else
{
  newLines = (from agl in _linesDataMng.ActiveGamesLines
              join ag in _linesDataMng.ActiveGames on agl.GameNum equals ag.GameNum
              where agl.Store.Trim() == storeName
                      && ag.WagerCutoff >= CurrentDateTime()
              select agl).ToList();
}
foreach (var newLine in newLines)
{
  var line = newLine;

  foreach (var game in store.Sports.SelectMany(sport => sport.Leagues.SelectMany(
      league => league.Periods.Where(p => p.Number == line.PeriodNumber).SelectMany(
          period => period.Games.Where(g => g.GameNum == line.GameNum)))))
      game.Lines.Add(BuildGameLine(storeName, line.PeriodNumber, line.GameNum, line.CustProfile.Trim()));
}
       */
      //}
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="storeName"></param>
    /// <param name="sportType"></param>
    /// <param name="sportSubType"></param>
    /// <param name="periodNum"></param>
    /// <returns></returns>
    private bool PeriodExists(string storeName, string sportType, string sportSubType, int periodNum) {
      try {
        var store = Memory.FirstOrDefault(x => x.Name == storeName);
        if (store == null) return false;

        var sport = store.Sports.FirstOrDefault(y => y.Name == sportType);
        if (sport == null) return false;
        var league = sport.Leagues.FirstOrDefault(z => z.Name == sportSubType);
        return league != null && (league.Periods.Any(a => a.Number == periodNum));
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return false;
      }

      /* var allPeriodsBySportAndLeague = new List<KeyValuePair<SportAndLeague, int>>();
      foreach (var sport in store.Sports)
          foreach (var league in sport.Leagues)
              allPeriodsBySportAndLeague.AddRange(
                  league.Periods.Select(
                      period => new KeyValuePair<SportAndLeague, int>(new SportAndLeague(sport.Name, league.Name), period.Number)));
      List<KeyValuePair<SportAndLeague, int>> newPeriodsBySportAndLeague;
      if (allPeriodsBySportAndLeague.Count > 0)
      {
          newPeriodsBySportAndLeague = (from      ag in _linesDataMng.ActiveGames
                                          join      agl in _linesDataMng.ActiveGamesLines on ag.GameNum equals agl.GameNum
                                          where     !allPeriodsBySportAndLeague.Any(ap => ap.Key.Key      == ag.SportType.Trim()
                                                                                          && ap.Key.Value == ag.SportSubType
                                                                                          && ap.Value     == agl.PeriodNumber)
                                          group     new { Sport  = ag.SportType.Trim(),
                                                          League = ag.SportSubType.Trim(),
                                                          agl.PeriodNumber }
                                                  by new { Sport  = ag.SportType.Trim(),
                                                              League = ag.SportSubType.Trim(),
                                                              agl.PeriodNumber } into pg
                                          select    new KeyValuePair<SportAndLeague, int>(new SportAndLeague(pg.Key.Sport, pg.Key.League), pg.Key.PeriodNumber)).ToList();
          allPeriodsBySportAndLeague.Clear();
      }
      else
      {
          newPeriodsBySportAndLeague = (from      ag in _linesDataMng.ActiveGames
                                          join      agl in _linesDataMng.ActiveGamesLines on ag.GameNum equals agl.GameNum
                                          group     new { Sport  = ag.SportType.Trim(),
                                                          League = ag.SportSubType.Trim(),
                                                          agl.PeriodNumber }
                                                  by new { Sport  = ag.SportType.Trim(),
                                                              League = ag.SportSubType.Trim(),
                                                              agl.PeriodNumber } into pg
                                          select    new KeyValuePair<SportAndLeague, int>(new SportAndLeague(pg.Key.Sport, pg.Key.League), pg.Key.PeriodNumber)).ToList();
      } */
    }

    /// <summary>
    /// Check if sport exists in Memory Tree for the given store
    /// </summary>
    /// <param name="storeName"></param>
    /// <param name="sportName"></param>
    /// <returns></returns>
    private bool SportExists(string storeName, string sportName) {
      try {
        var targetStore = Memory.FirstOrDefault(store => store.Name == storeName);
        var exists = targetStore != null && targetStore.Sports.Any(sport => sport.Name == sportName);
        return exists;
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return false;
      }
    }

    #endregion

    #region Get elements Methods

    #region Current elements

    private List<Contestant> GetContestants(string storeName, string contestType, string contestType2, string contestType3) {
      try {
        return (from ac in LinesDataMng.ActiveContestants
                where StringF.ToStringOrNull(ac.ContestType) == contestType
                      && StringF.ToStringOrNull(ac.ContestType2) == contestType2
                      && StringF.ToStringOrNull(ac.ContestType3) == contestType3
                select BuildContestant(storeName, ac.ContestNum, ac.ContestantNum, ac.ContestantName, contestType, contestType2, contestType3))
          .Where(contestant => contestant != null).ToList();
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return new List<Contestant>();
      }
    }

    private IEnumerable<ContestantLine> GetContestantLines(string storeName, int contestNum, int contestantNum) {
      try {
        return (from acl in LinesDataMng.ActiveContestantsLines
                where StringF.ToStringOrNull(acl.Store) == storeName
                      && acl.ContestNum == contestNum
                      && acl.ContestantNum == contestantNum
                      && !acl.IsEmptyLine()
                select BuildContestantLine(storeName, acl.ContestNum, acl.ContestantNum, acl.CustProfile));
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return new List<ContestantLine>();
      }
    }

    private List<Contest> GetContests(string storeName, string sportName, string leagueName) {
      if (sportName == OTHER_PROPS) return GetOtherProps(storeName);
      try {
        return (from ac in LinesDataMng.ActiveContests
                join csl in _contestSportLinks on StringF.ToStringOrNull(ac.ContestType) equals StringF.ToStringOrNull(csl.ContestType)
                where StringF.ToStringOrNull(csl.SportType) == sportName
                      && StringF.ToStringOrNull(csl.SportSubType) == leagueName
                      && StringF.ToStringOrNull(ac.ContestType) != HORSEO_FUTURE
                      && ac.WagerCutoff >= CurrentDateTime()
                      && !ac.Deleted
                      && IsActiveStatus(ac.Status)
                group ac by new {
                  ContestType = StringF.ToStringOrNull(ac.ContestType),
                  ContestType2 = StringF.ToStringOrNull(ac.ContestType2),
                  ContestType3 = StringF.ToStringOrNull(ac.ContestType3)
                }
                  into grp
                  select BuildContest(storeName, sportName, leagueName, grp.Key.ContestType, grp.Key.ContestType2, grp.Key.ContestType3))
          .Where(contest => contest != null).ToList();
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return new List<Contest>();
      }
    }

    private List<GameLine> GetGameLines(string storeName, int gameNum, int periodNumber) {
      try {
        return (from agl in LinesDataMng.ActiveGamesLines
                where agl.Store.Trim() == storeName
                       && agl.GameNum == gameNum
                       && agl.PeriodNumber == periodNumber
                       && !agl.IsEmptyLine()
                select BuildGameLine(storeName, periodNumber, gameNum, StringF.ToStringOrNull(agl.CustProfile))).ToList();

      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return new List<GameLine>();
      }

    }

    private List<Game> GetGames(string storeName, string sportName, string leagueName, int periodNumber) {
      try {
        return (from ag in LinesDataMng.ActiveGames
                where StringF.ToStringOrNull(ag.SportType) == sportName
                      && StringF.ToStringOrNull(ag.SportSubType) == leagueName
                      && ag.WagerCutoff >= CurrentDateTime()
                      && IsActiveStatus(ag.Status)
                      && !ag.Deleted
                select BuildGame(storeName, periodNumber, ag.GameNum))
          .Where(game => game != null).ToList();
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return new List<Game>();
      }
    }

    private List<League> GetLeagues(string storeName, string sportName) {
      try {
        var leagues = (from l in GetSportAndLeaguesNames(storeName)
                       where l.Key == sportName
                       select l.Value)
                       .Select(leagueName => BuildLeague(storeName, sportName, leagueName))
                       .Where(league => league != null)
                       .ToList();
        return leagues;
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return new List<League>();
      }

      /* return (from ac in LinesDataMng.ActiveGames
              where StringF.ToStringOrNull(ac.SportType) == sportName
              select StringF.ToStringOrNull(ac.SportSubType))
                  .Distinct()
                  .Select(leagueName => BuildLeague(storeName, sportName, leagueName))
                  .Where(league => league != null)
                  .ToList(); */
    }

    private List<Period> GetPeriods(string storeName, string sportName, string leagueName) {
      try {
        return (from gl in LinesDataMng.ActiveGamesLines
                join ag in LinesDataMng.ActiveGames on gl.GameNum equals ag.GameNum
                where ag.WagerCutoff >= CurrentDateTime()
                      && StringF.ToStringOrNull(ag.SportType) == sportName
                      && StringF.ToStringOrNull(ag.SportSubType) == leagueName
                      && IsActiveStatus(ag.Status)
                      && !gl.IsEmptyLine()
                      && !ag.Deleted
                select gl.PeriodNumber)
            .Distinct()
            .Select(periodNumber => BuildPeriod(storeName, sportName, leagueName, periodNumber))
            .Where(period => period != null)
            .ToList();
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return new List<Period>();
      }
    }

    private IEnumerable<KeyValuePair<string, string>> GetSportAndLeaguesNames(string storeName) {
      try {
        var sls = (from ag in LinesDataMng.ActiveGames
                   join agl in LinesDataMng.ActiveGamesLines on ag.GameNum equals agl.GameNum
                   where StringF.ToStringOrNull(agl.Store) == storeName
                         && ag.WagerCutoff >= CurrentDateTime()
                         && !ag.Deleted
                         && !agl.IsEmptyLine()
                   group ag by new {
                     Sport = StringF.ToStringOrNull(ag.SportType),
                     League = StringF.ToStringOrNull(ag.SportSubType)
                   }
                     into grp
                     select new KeyValuePair<string, string>(grp.Key.Sport, grp.Key.League)).ToList();

        var slLnkContests = (from ac in LinesDataMng.ActiveContests
                             join csl in _contestSportLinks on StringF.ToStringOrNull(ac.ContestType) equals StringF.ToStringOrNull(csl.ContestType)
                             join acl in LinesDataMng.ActiveContestantsLines on ac.ContestNum equals acl.ContestNum
                             where StringF.ToStringOrNull(acl.Store) == storeName
                                   && StringF.ToStringOrNull(csl.SportSubType) != null
                                   && ac.WagerCutoff >= CurrentDateTime()
                                   && !acl.IsEmptyLine()
                                   && !ac.Deleted
                                   && IsActiveStatus(ac.Status)
                             group csl by new {
                               Sport = StringF.ToStringOrNull(csl.SportType),
                               League = StringF.ToStringOrNull(csl.SportSubType)
                             }
                               into grp
                               select new KeyValuePair<string, string>(grp.Key.Sport, grp.Key.League)).ToList();

        foreach (var c in slLnkContests.Where(c => sls.Any(s => s.Key != c.Key && s.Value != c.Value))) {
          sls.Add(c);
        }

        //sls.AddRange(slLnkContests
        //.Where(slLnk => sls.All(sl => sl.Key != slLnk.Key && sl.Value != slLnk.Value)));
        return sls;
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return new List<KeyValuePair<string, string>>();
      }
    }

    private IEnumerable<string> GetSportsNames(string storeName) {
      try {
        var storeSports =
            LinesDataMng.ActiveGamesLines.Where(s => StringF.ToStringOrNull(s.Store) == storeName).ToList();

        var sportsNames = (from ag in LinesDataMng.ActiveGames
                           join agl in storeSports on ag.GameNum equals agl.GameNum
                           where //StringF.ToStringOrNull(agl.Store) == storeName && 
                                ag.WagerCutoff >= CurrentDateTime()
                                 && !ag.Deleted
                                 && !agl.IsEmptyLine()
                                 && IsActiveStatus(ag.Status)
                           select StringF.ToStringOrNull(ag.SportType)).Distinct().ToList();


        var sportsNamesLinkedToContests = (from ac in LinesDataMng.ActiveContests
                                           join csl in _contestSportLinks on StringF.ToStringOrNull(ac.ContestType) equals StringF.ToStringOrNull(csl.ContestType)
                                           join acl in LinesDataMng.ActiveContestantsLines on ac.ContestNum equals acl.ContestNum
                                           where StringF.ToStringOrNull(acl.Store) == storeName
                                                 && ac.WagerCutoff >= CurrentDateTime()
                                                 && !acl.IsEmptyLine()
                                                 && !ac.Deleted
                                                 && IsActiveStatus(ac.Status)
                                           select new { SportType = StringF.ToStringOrNull(csl.SportType), csl.ContestType }).Distinct().ToList();
        if (!sportsNames.Contains(OTHER_PROPS)) {
          var sportsNamesNotLinkedToContests = (from ac in LinesDataMng.ActiveContests
                                                join acl in LinesDataMng.ActiveContestantsLines on ac.ContestNum equals acl.ContestNum
                                                where StringF.ToStringOrNull(acl.Store) == storeName
                                                      && ac.WagerCutoff >= CurrentDateTime()
                                                      && !acl.IsEmptyLine()
                                                      && !ac.Deleted
                                                      && IsActiveStatus(ac.Status)
                                                select ac.ContestType).Distinct().ToList();
          sportsNamesNotLinkedToContests.RemoveAll(sportsNamesLinkedToContests.Select(c => c.ContestType).Contains);
          if (sportsNamesNotLinkedToContests.Any()) sportsNames.Add(OTHER_PROPS);

        }
        sportsNames.AddRange(sportsNamesLinkedToContests.Select(c => c.SportType).Distinct().Where(c => !sportsNames.Contains(c)));
        return sportsNames;
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return new List<string>();
      }
    }

    #endregion

    #region New elements

    private IEnumerable<LeagueContests> GetNewContestsByLeague(string storeName) {
      try {
        var contests = (from ac in LinesDataMng.ActiveContests
                        join csl in _contestSportLinks on StringF.ToStringOrNull(ac.ContestType) equals
                            StringF.ToStringOrNull(csl.ContestType)
                        where
                            !ContestExists(storeName, StringF.ToStringOrNull(csl.SportType),
                                StringF.ToStringOrNull(csl.SportSubType), StringF.ToStringOrNull(ac.ContestType),
                                StringF.ToStringOrNull(ac.ContestType2), StringF.ToStringOrNull(ac.ContestType3))
                            && StringF.ToStringOrNull(ac.ContestType) != HORSEO_FUTURE
                            && ac.WagerCutoff >= CurrentDateTime()
                            && IsActive(ac, storeName)
                            && !ac.Deleted
                        group ac by new {
                          SportType = StringF.ToStringOrNull(csl.SportType),
                          SportSubType = StringF.ToStringOrNull(csl.SportSubType),
                          ContestType = StringF.ToStringOrNull(ac.ContestType),
                          ContestType2 = StringF.ToStringOrNull(ac.ContestType2),
                          ContestType3 = StringF.ToStringOrNull(ac.ContestType3)
                        }
                          into grp
                          select new LeagueContests {
                            SportType = StringF.ToStringOrNull(grp.Key.SportType),
                            SportSubType = StringF.ToStringOrNull(grp.Key.SportSubType),
                            ContestType = StringF.ToStringOrNull(grp.Key.ContestType),
                            ContestType2 = StringF.ToStringOrNull(grp.Key.ContestType2),
                            ContestType3 = StringF.ToStringOrNull(grp.Key.ContestType3)
                          }).ToList();
        return contests;
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return new List<LeagueContests>();
      }
    }

    private IEnumerable<LeagueContests> GetNewContestsBySport(string storeName) {
      var activeContests = LinesDataMng.ActiveContests;
      try {
        var newContestsBySport = (from ac in activeContests
                                  join csl in _contestSportLinks on ac.ContestType.Trim() equals csl.ContestType.Trim()
                                  where !ContestExists(storeName, StringF.ToStringOrNull(csl.SportType), null, StringF.ToStringOrNull(ac.ContestType), StringF.ToStringOrNull(ac.ContestType2), StringF.ToStringOrNull(ac.ContestType3))
                                         && StringF.ToStringOrNull(csl.SportSubType) == null
                                         && ac.ContestType.Trim() != HORSEO_FUTURE
                                         && ac.WagerCutoff >= CurrentDateTime()
                                         && IsActive(ac, storeName)
                                         && !ac.Deleted
                                  group ac by new {
                                    SportType = StringF.ToStringOrNull(csl.SportType),
                                    ContestType = StringF.ToStringOrNull(ac.ContestType),
                                    ContestType2 = StringF.ToStringOrNull(ac.ContestType2),
                                    ContestType3 = StringF.ToStringOrNull(ac.ContestType3)
                                  } into grp
                                  select new LeagueContests {
                                    SportType = StringF.ToStringOrNull(grp.Key.SportType),
                                    ContestType = StringF.ToStringOrNull(grp.Key.ContestType),
                                    ContestType2 = StringF.ToStringOrNull(grp.Key.ContestType2),
                                    ContestType3 = StringF.ToStringOrNull(grp.Key.ContestType3)
                                  }).ToList();

        newContestsBySport.AddRange((from ac in activeContests
                                     where !ContestExists(storeName, StringF.ToStringOrNull(OTHER_PROPS), null, StringF.ToStringOrNull(ac.ContestType), StringF.ToStringOrNull(ac.ContestType2), StringF.ToStringOrNull(ac.ContestType3))
                                            && ac.ContestType.Trim() != HORSEO_FUTURE
                                            && ac.WagerCutoff >= CurrentDateTime()
                                            && IsActive(ac, storeName)
                                            && !ac.Deleted
                                            && !IsLinked(ac.ContestType, _contestSportLinks)
                                     group ac by new {
                                       SportType = StringF.ToStringOrNull(OTHER_PROPS),
                                       ContestType = StringF.ToStringOrNull(ac.ContestType),
                                       ContestType2 = StringF.ToStringOrNull(ac.ContestType2),
                                       ContestType3 = StringF.ToStringOrNull(ac.ContestType3)
                                     } into grp

                                     select new LeagueContests {
                                       SportType = StringF.ToStringOrNull(grp.Key.SportType),
                                       ContestType = StringF.ToStringOrNull(grp.Key.ContestType),
                                       ContestType2 = StringF.ToStringOrNull(grp.Key.ContestType2),
                                       ContestType3 = StringF.ToStringOrNull(grp.Key.ContestType3)
                                     }).ToList());

        return newContestsBySport;
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return new List<LeagueContests>();
      }
    }

    private bool IsLinked(string contestType, IEnumerable<spCnGetSportLinks_Result> contestSportLinks) {
      try {
        return contestSportLinks.Any(csl => csl.ContestType.Trim() == contestType.Trim());
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return false;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="storeName"></param>
    /// <returns></returns>
    private List<spCnGetActiveContestants_Result> GetNewContestants(string storeName) {
      try {
        var contestants = (from acn in LinesDataMng.ActiveContestants
                           where !ContestantExistsInTree(storeName, StringF.ToStringOrNull(acn.ContestType), StringF.ToStringOrNull(acn.ContestType2), StringF.ToStringOrNull(acn.ContestType3), StringF.ToStringOrNull(acn.ContestantName))
                                  && StringF.ToStringOrNull(acn.ContestType) != HORSEO_FUTURE
                           select acn).ToList();
        return contestants;
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return new List<spCnGetActiveContestants_Result>();
      }
    }

    private IEnumerable<KeyValuePair<KeyValuePair<int, int>, string>> GetNewContestantLines(string storeName) {
      try {
        var contestantLines = (from acl in LinesDataMng.ActiveContestantsLines
                               join acn in LinesDataMng.ActiveContestants on acl.ContestantNum equals acn.ContestantNum
                               where !ContestantLineExists(storeName, acn.ContestType, acn.ContestType2, acn.ContestType3, acl.ContestantName, acl.ContestantNum)
                                     && !acl.IsEmptyLine()
                               select new KeyValuePair<KeyValuePair<int, int>, string>(new KeyValuePair<int, int>(acl.ContestNum, acl.ContestantNum),
                                                                                       acl.CustProfile)).ToList();
        return contestantLines;
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return new List<KeyValuePair<KeyValuePair<int, int>, string>>();
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="storeName"></param>
    /// <returns></returns>
    private IEnumerable<KeyValuePair<KeyValuePair<string, string>, KeyValuePair<int, int>>> GetNewGames(string storeName) {
      try {
        var newGames = (from ag in LinesDataMng.ActiveGames
                        where !GameExists(storeName, ag.SportType.Trim(), ag.SportSubType.Trim(), ag.PeriodNumber, ag.GameNum)
                               && ag.WagerCutoff >= CurrentDateTime()
                        select new KeyValuePair<KeyValuePair<string, string>, KeyValuePair<int, int>>(new KeyValuePair<string, string>(StringF.ToStringOrNull(ag.SportType), StringF.ToStringOrNull(ag.SportSubType)),
                                                                                  new KeyValuePair<int, int>(ag.PeriodNumber, ag.GameNum))).ToList();

        return newGames;
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return new List<KeyValuePair<KeyValuePair<string, string>, KeyValuePair<int, int>>>();
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="storeName"></param>
    /// <returns></returns>
    private IEnumerable<KeyValuePair<string, string>> GetNewLeagues(string storeName) {
      try {
        var newSls = GetSportAndLeaguesNames(storeName)
                        .Where(sls => !LeagueExists(storeName, sls.Key, sls.Value)).ToList();
        return newSls;
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return new List<KeyValuePair<string, string>>();
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="storeName"></param>
    /// <returns></returns>

    private IEnumerable<KeyValuePair<KeyValuePair<int, int>, string>> GetNewLines(string storeName) {
      try {
        var newLines = (from agl in LinesDataMng.ActiveGamesLines
                        where !LineExists(storeName, agl.PeriodNumber, agl.GameNum)
                              && !agl.IsEmptyLine()
                        select new KeyValuePair<KeyValuePair<int, int>, string>(new KeyValuePair<int, int>(agl.PeriodNumber, agl.GameNum),
                          agl.CustProfile.Trim())).ToList();
        return newLines;
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return new List<KeyValuePair<KeyValuePair<int, int>, string>>();
      }
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="storeName"></param>
    /// <returns></returns>
    private IEnumerable<KeyValuePair<KeyValuePair<string, string>, int>> GetNewPeriods(string storeName) {
      try {
        var newPeriods = (from ag in LinesDataMng.ActiveGames
                          join agl in LinesDataMng.ActiveGamesLines on ag.GameNum equals agl.GameNum
                          where !PeriodExists(storeName, ag.SportType.Trim(), ag.SportSubType.Trim(), agl.PeriodNumber)
                                && ag.WagerCutoff >= CurrentDateTime()
                          group new { Sport = ag.SportType.Trim(), League = ag.SportSubType.Trim(), agl.PeriodNumber }
                            by new { Sport = ag.SportType.Trim(), League = ag.SportSubType.Trim(), agl.PeriodNumber }
                            into grp
                            select new KeyValuePair<KeyValuePair<string, string>, int>(new KeyValuePair<string, string>(grp.Key.Sport, grp.Key.League),
                              grp.Key.PeriodNumber)).ToList();
        return newPeriods;
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return new List<KeyValuePair<KeyValuePair<string, string>, int>>();
      }
    }

    /// <summary>
    /// Get valid sports for a given store that do not exist in Memory Tree
    /// </summary>
    /// <param name="storeName"></param>
    /// <returns></returns>
    private IEnumerable<string> GetNewSports(string storeName) {
      try {
        var newSports = GetSportsNames(storeName)
            .Where(sportName => !SportExists(storeName, sportName)).ToList();
        return newSports;
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return new List<string>();
      }
    }

    #endregion

    #region Old Elements

    private IEnumerable<KeyValuePair<string, string>> GetOldLeagues(Store store) {
      try {
        //lock (store.Sports) {
        return store.Sports
               .SelectMany(sport => sport.Leagues)
               .Where(league => league.Periods.Count == 0 && league.Contests.Count == 0)
               .Select(league => new KeyValuePair<string, string>(league.SportType, league.Name)).ToList();
        //}
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return new List<KeyValuePair<string, string>>();
      }
    }

    private IEnumerable<string> GetOldSports(Store store) {
      try {
        //lock (store.Sports) {
        return store.Sports
                .Where(sport => sport.Contests.Count == 0 && sport.Leagues.Count == 0)
                .Select(sport => sport.Name).ToList();
        //}
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return new List<string>();
      }
    }

    #endregion

    #endregion

    #region Get Node By Methods

    /// <summary>
    /// 
    /// </summary>
    /// <param name="store"></param>
    /// <param name="contestType"></param>
    /// <param name="contestType2"></param>
    /// <param name="contestType3"></param>
    /// <returns></returns>
    private static Contest GetContestBy(Store store, string contestType, string contestType2, string contestType3) {
      try {
        var contest = store.Sports.SelectMany(x => x.Contests)
          .FirstOrDefault(y => y.ContestType == contestType
                               && y.ContestType2 == contestType2 && y.ContestType3 == contestType3);
        if (contest != null) return contest;
        return store.Sports.SelectMany(x => x.Leagues)
          .SelectMany(y => y.Contests)
          .FirstOrDefault(z => z.ContestType == contestType
                               && z.ContestType2 == contestType2 && z.ContestType3 == contestType3);
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return null;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="store"></param>
    /// <param name="contestNum"></param>
    /// <param name="contestantNum"></param>
    /// <returns></returns>
    private static Contestant GetContestantBy(Store store, int contestNum, int contestantNum) {
      try {
        var contestant = store.Sports.SelectMany(x => x.Contests)
          .SelectMany(y => y.Contestants)
          .FirstOrDefault(z => z.Key.ContestNum == contestNum
                               && z.Key.ContestantNum == contestantNum);
        if (contestant != null) return contestant;
        return store.Sports.SelectMany(x => x.Leagues)
          .SelectMany(y => y.Contests)
          .SelectMany(z => z.Contestants)
          .FirstOrDefault(a => a.Key.ContestNum == contestNum
                               && a.Key.ContestantNum == contestantNum);
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return null;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="store"></param>
    /// <param name="gameNum"></param>
    /// <returns></returns>
    private static Game GetGameBy(Store store, int gameNum) {
      try {
        return store.Sports.SelectMany(x => x.Leagues)
          .SelectMany(y => y.Periods)
          .SelectMany(z => z.Games)
          .FirstOrDefault(a => a.GameNum == gameNum);
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return null;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="store"></param>
    /// <param name="sportName"></param>
    /// <param name="leagueName"></param>
    /// <returns></returns>
    private static League GetLeagueBy(Store store, string sportName, string leagueName) {
      try {
        var sport = store.Sports.FirstOrDefault(x => x.Name == sportName);
        return sport == null ? null : sport.Leagues.FirstOrDefault(y => y.Name == leagueName);
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return null;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="store"></param>
    /// <param name="sportName"></param>
    /// <param name="leagueName"></param>
    /// <param name="periodNum"></param>
    /// <returns></returns>
    private static Period GetPeriodBy(Store store, string sportName, string leagueName, int periodNum) {
      try {
        var sport = store.Sports.FirstOrDefault(x => x.Name == sportName);
        if (sport == null) return null;

        var league = sport.Leagues.FirstOrDefault(y => y.Name == leagueName);
        return league == null ? null : league.Periods.FirstOrDefault(z => z.Number == periodNum);
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return null;
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="store"></param>
    /// <param name="sportName"></param>
    /// <returns></returns>
    private static Sport GetSportBy(Store store, string sportName) {
      try {
        return store.Sports.FirstOrDefault(x => x.Name == sportName);
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return null;
      }
    }

    #endregion

    #region Tree related Methods

    /// <summary>
    /// 
    /// </summary>
    /// <param name="newStoresNames"></param>
    private void ActivateTreeChangesMonitor(IEnumerable<string> newStoresNames) {
      ToggleTreeChangesMonitor(newStoresNames, true);
    }

    /// <summary>
    /// Removes both empty nodes and their related notifications
    /// </summary>
    private void RemoveEmptyNodes() {
      try {
        foreach (var store in Memory) {
          foreach (var sport in store.Sports) {
            foreach (var contest in sport.Contests) contest.Contestants.RemoveAll(x => x.Lines == null || !x.Lines.Any() || x.LinesCount == 0);

            Notifications.RemoveAll(x => x.Data.SportType == sport.Name
                                          && sport.Contests
                                             .Where(y => !y.Contestants.Any())
                                             .Any(z => z.ContestType == x.Data.ContestType
                                                       && z.ContestType2 == x.Data.ContestType2
                                                       && z.ContestType3 == x.Data.ContestType3));
            sport.Contests.RemoveAll(x => !x.Contestants.Any());

            foreach (var league in sport.Leagues) {
              foreach (var contest in league.Contests) contest.Contestants.RemoveAll(x => !x.Lines.Any() || x.LinesCount == 0);

              Notifications.RemoveAll(x => x.Data.SportType == sport.Name
                                              && x.Data.SportSubType == league.Name
                                              && league.Contests
                                                  .Where(y => !y.Contestants.Any())
                                                  .Any(z => z.ContestType == x.Data.ContestType
                                                          && z.ContestType2 == x.Data.ContestType2
                                                          && z.ContestType3 == x.Data.ContestType3));
              league.Contests.RemoveAll(x => !x.Contestants.Any());

              foreach (var period in league.Periods) {
                Notifications.RemoveAll(x => x.Data.SportType == sport.Name
                                                && x.Data.SportSubType == league.Name
                                                && x.Data.PeriodNumber == period.Number
                                                && period.Games
                                                    .Where(y => y.LinesCount == 0)
                                                    .Any(z => z.GameNum == x.Data.GameNumber));
                period.Games.RemoveAll(x => x.LinesCount == 0);
              }
              Notifications.RemoveAll(x => x.Data.SportType == sport.Name
                                              && x.Data.SportSubType == league.Name
                                              && league.Periods
                                                  .Where(y => !y.Games.Any())
                                                  .Any(z => z.Number == x.Data.PeriodNumber));
              league.Periods.RemoveAll(x => !x.Games.Any());
            }
            Notifications.RemoveAll(x => x.Data.SportType == sport.Name
                                            && sport.Leagues
                                                .Where(y => !y.Periods.Any()
                                                            && !y.Contests.Any())
                                                .Any(z => z.Name == x.Data.SportSubType));
            sport.Leagues.RemoveAll(x => !x.Periods.Any()
                                            && !x.Contests.Any());

          }
          Notifications.RemoveAll(x => store.Sports.Any(y => y.Name == x.Data.SportType
                                                              && !y.Leagues.Any()
                                                              && !y.Contests.Any()));
          store.Sports.RemoveAll(x => !x.Leagues.Any()
                                      && !x.Contests.Any());
        }
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="store"></param>
    private void AddNodes(Store store) {
      try {
        AddSports(store);
        AddLeagues(store);

        AddContests(store);
        AddContestants(store);
        AddContestantLines(store);

        AddPeriods(store);
        AddGames(store);
        AddLines(store);
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
      }
    }

    #region Add nodes methods

    /// <summary>
    /// 
    /// </summary>
    /// <param name="store"></param>
    private void AddContestants(Store store) {
      try {
        foreach (var contestant in GetNewContestants(store.Name)) {
          var c = GetContestBy(store, contestant.ContestType, contestant.ContestType2, contestant.ContestType3);

          if (c == null || c.Contestants == null) continue;
          var cnt = BuildContestant(store.Name, contestant.ContestNum, contestant.ContestantNum, contestant.ContestantName, contestant.ContestType, contestant.ContestType2, contestant.ContestType3);
          if (cnt != null) c.Contestants.Add(cnt);
        }
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="store"></param>
    private void AddContestantLines(Store store) {
      try {
        foreach (var contestantLine in GetNewContestantLines(store.Name)) {
          var cnt = GetContestantBy(store, contestantLine.Key.Key, contestantLine.Key.Value);

          if (cnt == null || cnt.Lines == null || cnt.LinesCount == 0) continue;
          var cl = BuildContestantLine(store.Name, cnt.Key.ContestNum, cnt.Key.ContestantNum, contestantLine.Value);
          if (cl != null) cnt.Lines.Add(cl);
        }
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="store"></param>
    private void AddContests(Store store) {
      AddContestsBySport(store);
      AddContestsByLeague(store);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="store"></param>
    private void AddContestsByLeague(Store store) {
      try {
        foreach (var contest in GetNewContestsByLeague(store.Name)) {
          var l = GetLeagueBy(store, contest.SportType, contest.SportSubType);

          if (l != null && l.Contests != null)
            l.Contests.AddIfNotNull(BuildContest(store.Name, l.SportType, l.Name, contest.ContestType, contest.ContestType2, contest.ContestType3));
        }
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="store"></param>
    private void AddContestsBySport(Store store) {
      try {
        foreach (var contest in GetNewContestsBySport(store.Name)) {
          var s = store.Sports.FirstOrDefault(x => x.Name == contest.SportType);

          if (s != null && s.Contests != null)
            s.Contests.AddIfNotNull(BuildContest(store.Name, s.Name, null, contest.ContestType, contest.ContestType2, contest.ContestType3));
        }
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="store"></param>
    private void AddGames(Store store) {
      foreach (var game in GetNewGames(store.Name)) {
        var p = GetPeriodBy(store, game.Key.Key, game.Key.Value, game.Value.Key);
        if (p == null || p.Games == null) continue;

        var g = BuildGame(store.Name, p.Number, game.Value.Value);
        if (g != null) p.Games.Add(g);
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="store"></param>
    private void AddLeagues(Store store) {
      try {
        foreach (var league in GetNewLeagues(store.Name)) {
          var s = GetSportBy(store, league.Key);

          if (s != null && s.Leagues != null) s.Leagues.AddIfNotNull(BuildLeague(store.Name, s.Name, league.Value));
        }
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="store"></param>

    private void AddLines(Store store) {
      try {
        foreach (var line in GetNewLines(store.Name)) {
          var g = GetGameBy(store, line.Key.Value);
          if (g == null || g.Lines == null) continue;

          var l = BuildGameLine(store.Name, line.Key.Key, g.GameNum, line.Value);
          if (l != null) g.Lines.Add(l);
        }
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
      }
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="store"></param>
    private void AddPeriods(Store store) {
      try {
        foreach (var period in GetNewPeriods(store.Name)) {
          var l = GetLeagueBy(store, period.Key.Key, period.Key.Value);

          if (l != null && l.Periods != null) l.Periods.AddIfNotNull(BuildPeriod(store.Name, period.Key.Key, l.Name, period.Value));
        }
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="store"></param>
    private void AddSports(Store store) {
      try {
        foreach (var s in GetNewSports(store.Name)) store.Sports.AddIfNotNull(BuildSport(store.Name, s));
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
      }
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    /// <param name="store"></param>
    private void RemoveNodes(Store store) {
      //RemoveLines(store);
      UpdateLinesCount(store); //RemoveLines(store);
      RemoveGames(store);
      RemovePeriods(store);

      UpdateContestantLinesCount(store);//RemoveContestantLines(store);
      RemoveContestantsLgr(store);//RemoveContestants(store);
      RemoveContests(store);

      RemoveLeagues(store);
      RemoveSports(store);

    }

    #region Remove Nodes Methods

    private void UpdateContestantLinesCount(Store store) {

      try {
        var contests = store.Sports.SelectMany(c => c.Contests).ToList();

        contests.AddRange(store.Sports.SelectMany(l => l.Leagues).SelectMany(c => c.Contests).ToList());
        foreach (var contest in contests) {
          foreach (var contestant in contest.Contestants) {
            contestant.LinesCount = (from c in LinesDataMng.ActiveContests
                                     join ca in LinesDataMng.ActiveContestants on c.ContestNum equals ca.ContestNum
                                     join cal in LinesDataMng.ActiveContestantsLines on ca.ContestantNum equals cal.ContestantNum
                                     where ca.ContestNum == contestant.Key.ContestNum && ca.ContestantNum == contestant.Key.ContestantNum &&
                                           !c.Deleted && IsActiveStatus(c.Status) && !cal.IsEmptyLine() && cal.Store.Trim() == store.Name
                                     select cal.ContestantNum).Count();
          }
        }
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
      }
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="store"></param>
    private void UpdateLinesCount(Store store) {
      try {
        var leagues = store.Sports.SelectMany(l => l.Leagues).ToList();

        foreach (var league in leagues) {
          foreach (var period in league.Periods) {
            foreach (var game in period.Games) {
              var activeGame =
                  (from g in LinesDataMng.ActiveGames
                   where g.GameNum == game.GameNum && !g.Deleted && IsActiveStatus(g.Status)
                   select g).FirstOrDefault();
              if (activeGame == null) game.LinesCount = 0;
              else {
                game.LinesCount = (from gl in LinesDataMng.ActiveGamesLines
                                   where
                                       gl.GameNum == activeGame.GameNum && gl.PeriodNumber == period.Number &&
                                       !gl.IsEmptyLine() && gl.Store.Trim() == store.Name && IsActiveStatus(activeGame.Status)
                                   select gl).Count();
              }
            }
          }

        }
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="store"></param>
    private void RemoveContests(Store store) {
      RemoveContestsFromSportsLgr(store);  //RemoveContestsFromSports(store);
      RemoveContestsFromLeaguesLgr(store); //RemoveContestsFromLeagues(store);
    }

    private void RemoveContestantsLgr(Store store) {
      try {
        foreach (var sport in store.Sports) {
          foreach (var contest in sport.Contests) {
            contest.Contestants.RemoveAll(ca => ca.LinesCount == 0);
          }
        }
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
      }
    }


    private void RemoveContestsFromLeaguesLgr(Store store) {
      try {
        foreach (var s in store.Sports) {
          foreach (var l in s.Leagues)
            l.Contests.RemoveAll(c => c.Contestants.Count == 0);
        }
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
      }
    }

    private void RemoveContestsFromSportsLgr(Store store) {
      try {
        foreach (var s in store.Sports) {
          s.Contests.RemoveAll(c => c.Contestants.Count == 0);
        }
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="store"></param>
    private void RemoveGames(Store store) {
      try {
        foreach (var sport in store.Sports) {
          foreach (var league in sport.Leagues) {
            foreach (var period in league.Periods) {
              period.Games.RemoveAll(ga => ga.LinesCount == 0);

            }
          }
        }
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="store"></param>
    private void RemoveLeagues(Store store) {
      try {
        var oldLeagues = GetOldLeagues(store).ToList();
        foreach (var league in oldLeagues) {
          var sportType = league.Key;
          var name = league.Value;
          foreach (var s in store.Sports)
            //lock (s.Leagues) {
            s.Leagues.RemoveAll(x => x.SportType == sportType
                                     && x.Name == name && x.Contests.Count == 0 && x.Periods.Count == 0);
          //}
        }
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="store"></param>
    private void RemovePeriods(Store store) {
      try {
        foreach (var sport in store.Sports) {
          foreach (var league in sport.Leagues) {
            league.Periods.RemoveAll(ga => ga.Games.Count == 0);

          }
        }
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="store"></param>
    private void RemoveSports(Store store) {
      try {
        var oldSports = GetOldSports(store).ToList();
        foreach (var s in oldSports) {
          var sportType = s;
          //lock (store.Sports) {
          store.Sports.RemoveAll(x => x.Name == sportType && x.Leagues.Count == 0 && x.Contests.Count == 0);
          //}
        }
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
      }
    }

    #endregion

    #endregion

    #region Utility Methods

    /// <summary>
    /// 
    /// </summary>
    private void ClearNotifications() {
      try {
        Notifications.Clear();
        foreach (var store in Memory) {
          store.Notifications.Clear();
          foreach (var sport in store.Sports) {
            sport.Notifications.Clear();
            foreach (var league in sport.Leagues) {
              league.Notifications.Clear();
            }
          }
        }
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
      }
    }

    /*
    private IEnumerable<Contest> CollectOldContests(IEnumerable<Contest> contests) {
      return contests.Where(contest => contest.Contestants.Count == 0);
    }
    */

    /// <summary>
    /// 
    /// </summary>
    private void CollectTreeChangesNotifications() {
      try {
        foreach (var st in Memory) {
          //Allan*:
          //st.Notifications llega vacio
          // -> VerifyNotifications: PASSED
          Notifications.AddRange(st.Notifications);
          st.Notifications.Clear();

          //foreach (var s in st.Sports.Where(x => Notifications.All(y => y.Data.SportType != x.Name)))
          foreach (var s in st.Sports) {
            foreach (var n in s.Notifications) n.Data.Store = st.Name;
            Notifications.AddRange(s.Notifications);
            s.Notifications.Clear();

            foreach (var l in s.Leagues) {
              foreach (var n in l.Notifications) {
                n.Data.Store = st.Name;
                n.Data.SportType = s.Name;
              }
              if (Notifications.Any(x => x.Data.SportType == s.Name
                                          && x.Data.SportSubType == l.Name)) continue;
              Notifications.AddRange(l.Notifications);
              l.Notifications.Clear();
            }

          }
        }
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private DateTime CurrentDateTime() {
      try {
        return _im.InstanceModuleInfo.ServerDateTime;
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return DateTime.Now;
      }
    }

    private List<string> GetNewStoresFromUsers(IEnumerable<string> storesNames) {
      try {
        //lock (Memory) {
        var newStoresNames = (from storeName in storesNames
                              where Memory.All(store => store.Name != storeName)
                              select storeName).ToList();
        return newStoresNames;
        //}
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return new List<string>();
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="storeName"></param>
    /// <returns></returns>
    private List<Contest> GetOtherProps(string storeName) {
      try {
        var contests = (from ac in LinesDataMng.ActiveContests
                        where StringF.ToStringOrNull(ac.ContestType) != HORSEO_FUTURE
                              && ac.WagerCutoff >= CurrentDateTime()
                              && !ac.Deleted
                              && IsActiveStatus(ac.Status)
                              && _contestSportLinks.All(csl => StringF.ToStringOrNull(csl.ContestType) != StringF.ToStringOrNull(ac.ContestType))
                        select BuildContest(storeName, OTHER_PROPS, null, StringF.ToStringOrNull(ac.ContestType), StringF.ToStringOrNull(ac.ContestType2), StringF.ToStringOrNull(ac.ContestType3)))
                                        .Where(contest => contest != null).ToList();
        return contests;
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return new List<Contest>();
      }
    }

    public List<string> GetStoresFromUsers() {
      try {
        if (_users == null || !Active) return null;
        //lock (_users) {
        return (from u in _users
                group u by StringF.ToStringOrNull(u.Store) into grp
                select grp.Key)
                    .Where(x => x != null).ToList();
        //}
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return new List<string>();
      }
    }

    private static bool IsActiveStatus(string status) {
      return status != GamesAndLines.EVENT_COMPLETED && status != GamesAndLines.EVENT_CANCELLED && status != GamesAndLines.EVENT_OFFLINE;
    }

    private bool IsActive(spCnGetActiveContestsAdm_Result contest, string store) {
      try {
        if (!IsActiveStatus(contest.Status)) return false;
        return (from c in LinesDataMng.ActiveContestants
                join cal in LinesDataMng.ActiveContestantsLines on c.ContestantNum equals cal.ContestantNum
                where c.ContestNum == contest.ContestNum && cal.Store == store && !cal.IsEmptyLine()
                select c.ContestNum).Any();
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
        return false;
      }
    }

    private void SetContestSportLinks(IContests cntSvc) {
      _contestSportLinks = cntSvc.GetSportLinks();
    }

    private void SetStoresInMemory(List<string> storesNames) {
      try {
        foreach (var storeName in storesNames) {
          var sports = GetSportsNames(storeName)
                       .Select(sportName => BuildSport(storeName, sportName))
                       .Where(sport => sport != null)
                       .ToList();
          if (!sports.Any()) continue;
          //lock (Memory) {
          Memory.Add(new Store(_im.InstanceModuleInfo, storeName, sports));
          //}
        }
        ActivateTreeChangesMonitor(storesNames);
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
      }
    }

    public void DeletedUnusedStoresFromMemory(List<string> storesNames) {
      try {
        if (!Memory.Any() || storesNames == null || !storesNames.Any() || !Active) return;
        //lock (Memory) {
        Memory.RemoveAll(m => !storesNames.Contains(m.Name));
        //}
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="newStoresNames"></param>
    /// <param name="monitorChanges"></param>
    private void ToggleTreeChangesMonitor(IEnumerable<string> newStoresNames, bool monitorChanges) {
      try {
        foreach (var targetStore in Memory.Where(store => newStoresNames.Contains(store.Name))) {
          foreach (var sport in targetStore.Sports) {
            foreach (var league in sport.Leagues) league.MonitorChanges = monitorChanges;
            sport.MonitorChanges = monitorChanges;
          }
          targetStore.MonitorChanges = monitorChanges;
        }
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
      }
    }

    public void PrintTree() {
      if (!Active) return;
      try {
        foreach (var store in Memory) {
          Console.WriteLine(store.Name);
          foreach (var sport in store.Sports) {
            Console.WriteLine("--" + sport.Name + " [" + sport.Leagues.Count + " leagues, " + sport.Contests.Count + " contests]");
            foreach (var l in sport.Leagues) {
              Console.WriteLine("----" + l.Name + " [" + l.Periods.Count + " periods, " + l.Contests.Count + " contests]");
              foreach (var c in l.Contests) {
                Console.WriteLine("------" + c.ContestType + " [" + c.Contestants.Count + " contestants]");
              }
            }
            foreach (var c in sport.Contests) {
              Console.WriteLine("----" + c.ContestType + " [" + c.Contestants.Count + " contestants]");
            }
          }
        }
      }
      catch (Exception ex) {
        CConsole.WriteLine(ex);
      }
    }


    #endregion

    #endregion

  }
}