﻿using System.Collections.Generic;

namespace WebSocketServerV3.Models {
  public class Game {
    public int GameNum { get; private set; }

    /* public DateTime? GameDateTime { get; set; }

    public DateTime? WagerCutoff { get; set; }

    public char? Status { get; set; }

    public string SportType { get; set; }

    public string Team1Id { get; set; }

    public int? Team1RotNum { get; set; }

    public string Team2Id { get; set; }

    public int? Team2RotNum { get; set; }

    public string Comments { get; set; }

    public string SportSubType { get; set; }

    public char? OnTv { get; set; }

    public int? Team1FinalScore { get; set; }

    public int? Team2FinalScore { get; set; }

    public string FinalWinnerId { get; set; }

    public int? ScheduleId { get; set; }

    public string BroadcastInfo { get; set; }

    public DateTime? ScheduleDate { get; set; }

    public int? Team1Nss { get; set; }

    public int? Team2Nss { get; set; }

    public int? TeamActionLinePos { get; set; }

    public double? CircledMaxWager { get; set; }

    public string ListedPitcher1 { get; set; }

    public string ListedPitcher2 { get; set; }

    public char? Pitcher1StartedFlag { get; set; }

    public char? Pitcher2StartedFlag { get; set; }

    public DateTime? GradeDateTime { get; set; }

    public char? ParlayRestriction { get; set; }

    public int? DrawRotNum { get; set; }

    public string ScheduleText { get; set; }

    public char? TimeChangeFlag { get; set; }

    public string CorrelationId { get; set; }

    public char? PreventPointBuyingFlag { get; set; } */

    public List<GameLine> Lines { get; set; }

    public int LinesCount { get; set; }


    #region Constructors

    public Game(int gameNum, int periodNumber) {
      GameNum = gameNum;
      PeriodNumber = periodNumber;
    }

    #endregion


    public int PeriodNumber { get; private set; }
  }
}