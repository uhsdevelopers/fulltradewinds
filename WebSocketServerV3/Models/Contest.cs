﻿using System.Collections.Generic;

namespace WebSocketServerV3.Models {
  public class Contest {
    public Contest(string sportType, string sportSubtype, string contestType, string contestType2, string contestType3) {
      SportType = sportType;
      SportSubtype = sportSubtype;
      ContestType = contestType;
      ContestType2 = contestType2;
      ContestType3 = contestType3;
    }

    public string SportType { get; private set; }

    public string SportSubtype { get; private set; }

    public string ContestType { get; private set; }

    public string ContestType2 { get; private set; }

    public string ContestType3 { get; private set; }

    public List<Contestant> Contestants { get; set; }

    public string Status { get; set; }

  }
}