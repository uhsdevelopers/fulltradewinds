﻿using System.Collections.Generic;
using InstanceManager.BusinessLayer;

namespace WebSocketServerV3.Models {
  public class ReportableChanges {
    protected ReportableChanges(ModuleInfo im) {
      Im = im;
    }

    private bool _monitorChanges = true;
    public bool MonitorChanges {
      get { return _monitorChanges; }
      set { _monitorChanges = value; }
    }

    public List<SportTreeNotification> Notifications { get; protected set; }

    protected readonly ModuleInfo Im;
  }
}