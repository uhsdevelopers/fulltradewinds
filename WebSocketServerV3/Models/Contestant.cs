﻿using System.Collections.Generic;

namespace WebSocketServerV3.Models {
  public class Contestant {
    #region Public Properties

    public ContestantKey Key { get; set; }

    public string ContestantName { get; set; }

    public string ContestType { get; private set; }

    public string ContestType2 { get; private set; }

    public string ContestType3 { get; private set; }

    public int? RotNum { get; set; }

    public char? Outcome { get; set; }

    public int? GradeNum { get; set; }

    public char? Scratched { get; set; }

    public int? ThresholdOutcome { get; set; }

    public double? ContestantMaxWager { get; set; }

    public int? ContestantSeq { get; set; }

    public List<ContestantLine> Lines { get; set; }

    public int LinesCount { get; set; }

    #endregion

    #region Constructors

    public Contestant(int contestNum, int contestantNum, string contestanName, string contestType, string contestType2, string contestType3) {
      Key = new ContestantKey(contestNum, contestantNum);
      ContestType = contestType;
      ContestType2 = contestType2;
      ContestType3 = contestType3;
      ContestantName = contestanName;
    }

    #endregion
  }
}