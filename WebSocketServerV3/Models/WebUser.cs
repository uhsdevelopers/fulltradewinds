﻿using System;
using System.Collections.Generic;

namespace WebSocketServerV3.Models {

  public class WebUser : User {

    //public bool AsiansAndRestrictedParlay { get; set; }
    public string CustomerId = String.Empty;
    public string AgentId = String.Empty;
    public string Store = String.Empty;
    public string Platform = String.Empty;
    public List<Subscription> Subscriptions;
    //public bool IsAgent { get; set; }

    public WebUser()
      : base() {
      Subscriptions = new List<Subscription>();
      //Subscriptions = new List<Subscription> {MockSubscription()};
    }

    /*public bool IsCustomer {
      get { return Type == UserType.Customer; }
    }*/

    private Subscription MockSubscription() { return new Subscription { SportType = "Baseball", SportSubType = "MLB", Type = Subscription.SubscriptionType.Game }; }

  }
}