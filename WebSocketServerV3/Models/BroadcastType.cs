﻿namespace WebSocketServerV3.Models {
  public enum BroadcastType {
    AllUsersInStore, SubscribedUsers
  }
}