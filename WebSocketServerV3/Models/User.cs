﻿using System;
using Microsoft.AspNet.SignalR.Hubs;

namespace WebSocketServerV3.Models {
  public class User {

    public enum UserType {Customer, Agent, Admin, Unknown}

    public HubCallerContext Context { get; set; }
    public DateTime LastActivity { get; set; }
    public bool Removed { get; set; }
    public UserType Type { get; set; }

    public User() {
      Type = UserType.Unknown;
    }

  }

}