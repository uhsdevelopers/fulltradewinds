﻿namespace WebSocketServerV3.Models {
  public class ContestantLineKey {
    public int ContestNum { get; private set; }
    public int ContestantNum { get; private set; }
    public string Store { get; private set; }
    public string CustProfile { get; private set; }

    public ContestantLineKey(int contestNum, int contestantNum, string store, string custProfile) {
      ContestNum = contestNum;
      ContestantNum = contestantNum;
      Store = store.Trim();
      CustProfile = custProfile.Trim();
    }
  }
}