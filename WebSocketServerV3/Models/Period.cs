﻿using System.Collections.Generic;

namespace WebSocketServerV3.Models {
  public class Period {

    #region Constructors

    public Period(int number, List<Game> games, string sportType, string sportSubtype) {
      Games = games;
      Number = number;
      SportType = sportType;
      SportSubtype = sportSubtype;
    }

    #endregion

    #region Public Properties

    public int Number { get; private set; }

    public List<Game> Games { get; private set; }

    public string SportType { get; private set; }

    public string SportSubtype { get; private set; }

    #endregion

  }
}