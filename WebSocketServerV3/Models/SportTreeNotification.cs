﻿namespace WebSocketServerV3.Models {
  public class SportTreeNotification {
    public BroadcastType BroadcastType { get; set; }

    public string Context { get; set; }

    public NotificationData Data { get; set; }

    public class NotificationData {
      public int Active = 1;
      public int FavoriteSport = 0;
      public string SportType;
      public string SportSubType;
      public string ContestType;
      public string ContestType2;
      public string ContestType3;
      public string Store;
      public int? PeriodNumber;
      public int? GameNumber;
      public string PeriodDescription;
      public int? SequenceNumber;
      public string ShortDescription;
    }
  }
}