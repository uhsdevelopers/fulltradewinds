﻿namespace WebSocketServerV3.Models {
  public class GameLineKey {
    public int GameNum { get; private set; }
    public int PeriodNumber { get; private set; }
    public string Store { get; private set; }
    public string CustProfile { get; private set; }

    public GameLineKey(int gameNum, int periodNumber, string store, string custProfile) {
      GameNum = gameNum;
      PeriodNumber = periodNumber;
      Store = store.Trim();
      CustProfile = !string.IsNullOrWhiteSpace(custProfile) ? custProfile.Trim() : null;
    }
  }
}