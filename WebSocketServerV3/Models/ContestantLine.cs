﻿using System;

namespace WebSocketServerV3.Models {
  public class ContestantLine {
    #region Public Properties

    public ContestantLineKey Key { get; set; }

    public string ContestantName { get; set; }

    public int? LineSeq { get; set; }

    public double? MoneyLine { get; set; }

    public double? ToBase { get; set; }

    public double? ThresholdLine { get; set; }

    public DateTime? LastLineChange { get; set; }

    public double? DecimalOdds { get; set; }

    public int? Numerator { get; set; }

    public int? Denominator { get; set; }

    #endregion

    #region Constructors

    public ContestantLine(int contestNum, int contestantNum, string store, string custProfile) {
      Key = new ContestantLineKey(contestNum, contestantNum, store, custProfile);
    }

    #endregion

  }
}