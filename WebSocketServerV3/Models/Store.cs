﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using InstanceManager.BusinessLayer;

namespace WebSocketServerV3.Models {
  public class Store : ReportableChanges {
    #region Constructors

    public Store(ModuleInfo im, string name, IEnumerable<Sport> sports)
      : base(im) {
      Sports = new ObservableCollection<Sport>(sports);
      Sports.CollectionChanged += SportsChanged;
      Name = name;
      Notifications = new List<SportTreeNotification>();
    }

    #endregion

    #region Public Properties

    public string Name { get; private set; }

    public ObservableCollection<Sport> Sports { get; private set; }

    #endregion

    #region Private Methods

    private void SportsChanged(object sender, NotifyCollectionChangedEventArgs e) {
      if (!MonitorChanges)
        return;
      if (e.NewItems != null) {
        // using (var sportTypeSvc = new SportTypes(Im))
        // {
        foreach (var item in e.NewItems) {
          var item1 = (Sport)item;
          var selSport = (from s in Sports
                          where s.Name == item1.Name
                          select s).First();
          if (selSport.Leagues != null && selSport.Leagues.Count > 0) {

            //Pending for review by LGR, removed doing debug
            /*
            Notifications.Add(new SportTreeNotification
            {
                BroadcastType = BroadcastType.AllUsersInStore,
                Context = "New Sport",
                Data = new SportTreeNotification.NotificationData
                {
                    SportType = ((Sport) item).Name,
                    SportSubType = selSport.Leagues.First().Name,
                    Store = Name // ,
                    // SequenceNumber = sportTypeSvc.GetSequenceNum(((Sport) item).Name, selSport.Leagues.First().Name)
                }
            });
             */
          }
          else if (selSport.Contests != null && selSport.Contests.Count > 0) {
            //Pending for review by LGR, removed doing debug
            /*
            Notifications.Add(new SportTreeNotification
            {
                BroadcastType = BroadcastType.AllUsersInStore,
                Context = "New Sport",
                Data = new SportTreeNotification.NotificationData
                {
                    ContestType = selSport.Contests.First().ContestType,
                    ContestType2 = selSport.Contests.First().ContestType2,
                    SportType = ((Sport) item).Name,
                    Store = Name,
                    SequenceNumber = 9999
                }
            });
              */
          }
        }
        // }
      }
      if (e.OldItems == null)
        return;

      foreach (var item in e.OldItems)
        Notifications.Add(new SportTreeNotification {
          BroadcastType = BroadcastType.AllUsersInStore,
          Context = "Removed Sport",
          Data = new SportTreeNotification.NotificationData {
            SportType = ((Sport)item).Name,
            Store = Name
          }
        });
    }

    #endregion
  }
}