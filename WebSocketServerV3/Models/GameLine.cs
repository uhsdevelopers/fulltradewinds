﻿using System;

namespace WebSocketServerV3.Models {
  public class GameLine {
    #region Public Properties

    public GameLineKey Key { get; set; }

    public string FavoredTeamId { get; set; }
    public double? Spread { get; set; }
    public int? SpreadAdj1 { get; set; }
    public int? SpreadAdj2 { get; set; }
    public double? TotalPoints { get; set; }
    public int? TtlPtsAdj1 { get; set; }
    public int? TtlPtsAdj2 { get; set; }
    public int? MoneyLine1 { get; set; }
    public int? MoneyLine2 { get; set; }
    public double? Team1TotalPoints { get; set; }
    public int? Team1TtlPtsAdj1 { get; set; }
    public int? Team1TtlPtsAdj2 { get; set; }
    public double? Team2TotalPoints { get; set; }
    public int? Team2TtlPtsAdj1 { get; set; }
    public int? Team2TtlPtsAdj2 { get; set; }
    public double? PuckLine { get; set; }
    public string OnlineFeedSpread { get; set; }
    public string OnlineFeedMoneyLine { get; set; }
    public string OnlineFeedTotalPoints { get; set; }
    public int? TeamActionLinePos { get; set; }
    public DateTime? PeriodWagerCutoff { get; set; }
    public int? MoneyLineDraw { get; set; }
    public double? CircledMaxWagerSpread { get; set; }
    public double? CircledMaxWagerTotal { get; set; }
    public double? CircledMaxWagerMoneyLine { get; set; }
    public double? CircledMaxWagerTeamTotal { get; set; }
    public char? LinkedToStoreFlag { get; set; }
    public double? SpreadDecimal1 { get; set; }
    public int? SpreadNumerator1 { get; set; }
    public int? SpreadDenominator1 { get; set; }
    public double? SpreadDecimal2 { get; set; }
    public int? SpreadNumerator2 { get; set; }
    public int? SpreadDenominator2 { get; set; }
    public double? MoneyLineDecimal1 { get; set; }
    public int? MoneyLineNumerator1 { get; set; }
    public int? MoneyLineDenominator1 { get; set; }
    public double? MoneyLineDecimal2 { get; set; }
    public int? MoneyLineNumerator2 { get; set; }
    public int? MoneyLineDenominator2 { get; set; }
    public double? MoneyLineDecimalDraw { get; set; }
    public int? MoneyLineNumeratorDraw { get; set; }
    public int? MoneyLineDenominatorDraw { get; set; }
    public double? TtlPointsDecimal1 { get; set; }
    public int? TtlPointsNumerator1 { get; set; }
    public int? TtlPointsDenominator1 { get; set; }
    public double? TtlPointsDecimal2 { get; set; }
    public int? TtlPointsNumerator2 { get; set; }
    public int? TtlPointsDenominator2 { get; set; }
    public double? Team1TtlPtsDecimal1 { get; set; }
    public int? Team1TtlPtsNumerator1 { get; set; }
    public int? Team1TtlPtsDenominator1 { get; set; }
    public double? Team1TtlPtsDecimal2 { get; set; }
    public int? Team1TtlPtsNumerator2 { get; set; }
    public int? Team1TtlPtsDenominator2 { get; set; }
    public double? Team2TtlPtsDecimal1 { get; set; }
    public int? Team2TtlPtsNumerator1 { get; set; }
    public int? Team2TtlPtsDenominator1 { get; set; }
    public double? Team2TtlPtsDecimal2 { get; set; }
    public int? Team2TtlPtsNumerator2 { get; set; }
    public int? Team2TtlPtsDenominator2 { get; set; }

    #endregion

    #region Constructors

    public GameLine(int gameNum, int periodNumber, string store, string custProfile) {
      Key = new GameLineKey(gameNum, periodNumber, store, custProfile);
    }

    #endregion
  }
}