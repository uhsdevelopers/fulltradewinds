﻿using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace WebSocketServerV3.Models {
  public static class ExtensionMethods {
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="coll"></param>
    /// <param name="condition"></param>
    public static void RemoveAll<T>(this ObservableCollection<T> coll, Func<T, bool> condition) {
      try {
        var itemsToRemove = coll.Where(condition).ToList();
        foreach (var itemToRemove in itemsToRemove) coll.Remove(itemToRemove);
      }
      catch {
        // ignored
      }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="coll"></param>
    /// <param name="elem"></param>
    public static void AddIfNotNull<T>(this ObservableCollection<T> coll, T elem) {
      if (elem != null && !elem.Equals(null)) coll.Add(elem);
    }
  }
}