﻿using System;
using System.Windows.Forms;
using FormUtilities.UI;

namespace FormUtilities {
  static class Program {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main() {
      //String instanceToUse = ConfigurationManager.AppSettings["DefaultInstance"];
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      Application.Run(new FrmEncryptor());
    }
  }
}
