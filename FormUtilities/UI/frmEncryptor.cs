﻿using System;
using System.Windows.Forms;

using SIDLibraries.Utilities;

namespace FormUtilities.UI {
  public partial class FrmEncryptor : Form {

    private const String KEY = "3nd2p2nd2nc2Pr4j2ct";

    #region Private Vars

    #endregion

    #region Public Properties

    #endregion

    #region Private Properties

    #endregion

    #region Structures

    #endregion

    #region Constructors

    public FrmEncryptor() {
      InitializeComponent();
    }

    #endregion

    #region Public Methods

    #endregion

    #region Private Methods

    #endregion

    #region Protected Methods

    #endregion

    #region Events

    private void btnCancel_Click(object sender, EventArgs e) {
      Close();
    }

    private void btnDoIt_Click(object sender, EventArgs e) {
      string textToEncrypt = txtTextToEncrypt.Text;

      if (textToEncrypt.Length > 0) {
        txtEncryptedResult.Text = Encryptor.EncryptString(textToEncrypt, KEY);
      }
    }

    private void btnDeCrypt_Click(object sender, EventArgs e) {
      if (textToDecrypt.Text.Length > 0) {
        txtDecryptResult.Text = Encryptor.DecryptString(textToDecrypt.Text, KEY);
      }
    }

    private void frmEncryptor_Load(object sender, EventArgs e) {
      btnLoadSystemInMaintenance.Visible = false;
    }

    private void btnLoadSystemInMaintenance_Click(object sender, EventArgs e) {
      using (var sim = new FrmSystemInMaintenance()) {
        sim.ShowDialog();
      }
    }

    private void frmEncryptor_KeyDown(object sender, KeyEventArgs e) {
      if (e.Control && e.KeyCode == Keys.M) {
        btnLoadSystemInMaintenance.Visible = !btnLoadSystemInMaintenance.Visible;
      }
    }

    #endregion
  }
}
