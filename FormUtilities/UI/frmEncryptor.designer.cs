﻿namespace FormUtilities.UI
{
    partial class FrmEncryptor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTextToEncrypt = new System.Windows.Forms.Label();
            this.lblEncryptedResult = new System.Windows.Forms.Label();
            this.txtTextToEncrypt = new System.Windows.Forms.TextBox();
            this.txtEncryptedResult = new System.Windows.Forms.TextBox();
            this.btnDoIt = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.txtDecryptResult = new System.Windows.Forms.TextBox();
            this.textToDecrypt = new System.Windows.Forms.TextBox();
            this.lblDecryptResult = new System.Windows.Forms.Label();
            this.lbltextToDecrypt = new System.Windows.Forms.Label();
            this.btnDeCrypt = new System.Windows.Forms.Button();
            this.btnLoadSystemInMaintenance = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblTextToEncrypt
            // 
            this.lblTextToEncrypt.AutoSize = true;
            this.lblTextToEncrypt.Location = new System.Drawing.Point(18, 34);
            this.lblTextToEncrypt.Name = "lblTextToEncrypt";
            this.lblTextToEncrypt.Size = new System.Drawing.Size(86, 13);
            this.lblTextToEncrypt.TabIndex = 0;
            this.lblTextToEncrypt.Text = "Text To Encrypt:";
            // 
            // lblEncryptedResult
            // 
            this.lblEncryptedResult.AutoSize = true;
            this.lblEncryptedResult.Location = new System.Drawing.Point(64, 69);
            this.lblEncryptedResult.Name = "lblEncryptedResult";
            this.lblEncryptedResult.Size = new System.Drawing.Size(40, 13);
            this.lblEncryptedResult.TabIndex = 1;
            this.lblEncryptedResult.Text = "Result:";
            // 
            // txtTextToEncrypt
            // 
            this.txtTextToEncrypt.Location = new System.Drawing.Point(120, 31);
            this.txtTextToEncrypt.Name = "txtTextToEncrypt";
            this.txtTextToEncrypt.Size = new System.Drawing.Size(138, 20);
            this.txtTextToEncrypt.TabIndex = 2;
            // 
            // txtEncryptedResult
            // 
            this.txtEncryptedResult.Location = new System.Drawing.Point(120, 65);
            this.txtEncryptedResult.Multiline = true;
            this.txtEncryptedResult.Name = "txtEncryptedResult";
            this.txtEncryptedResult.ReadOnly = true;
            this.txtEncryptedResult.Size = new System.Drawing.Size(138, 40);
            this.txtEncryptedResult.TabIndex = 3;
            // 
            // btnDoIt
            // 
            this.btnDoIt.Location = new System.Drawing.Point(39, 85);
            this.btnDoIt.Name = "btnDoIt";
            this.btnDoIt.Size = new System.Drawing.Size(75, 23);
            this.btnDoIt.TabIndex = 4;
            this.btnDoIt.Text = "Encrypt";
            this.btnDoIt.UseVisualStyleBackColor = true;
            this.btnDoIt.Click += new System.EventHandler(this.btnDoIt_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(155, 227);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 5;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // txtDecryptResult
            // 
            this.txtDecryptResult.Location = new System.Drawing.Point(120, 163);
            this.txtDecryptResult.Multiline = true;
            this.txtDecryptResult.Name = "txtDecryptResult";
            this.txtDecryptResult.ReadOnly = true;
            this.txtDecryptResult.Size = new System.Drawing.Size(138, 40);
            this.txtDecryptResult.TabIndex = 7;
            // 
            // textToDecrypt
            // 
            this.textToDecrypt.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textToDecrypt.Location = new System.Drawing.Point(120, 129);
            this.textToDecrypt.Name = "textToDecrypt";
            this.textToDecrypt.Size = new System.Drawing.Size(138, 20);
            this.textToDecrypt.TabIndex = 6;
            // 
            // lblDecryptResult
            // 
            this.lblDecryptResult.AutoSize = true;
            this.lblDecryptResult.Location = new System.Drawing.Point(64, 167);
            this.lblDecryptResult.Name = "lblDecryptResult";
            this.lblDecryptResult.Size = new System.Drawing.Size(40, 13);
            this.lblDecryptResult.TabIndex = 9;
            this.lblDecryptResult.Text = "Result:";
            // 
            // lbltextToDecrypt
            // 
            this.lbltextToDecrypt.AutoSize = true;
            this.lbltextToDecrypt.Location = new System.Drawing.Point(18, 132);
            this.lbltextToDecrypt.Name = "lbltextToDecrypt";
            this.lbltextToDecrypt.Size = new System.Drawing.Size(86, 13);
            this.lbltextToDecrypt.TabIndex = 8;
            this.lbltextToDecrypt.Text = "Text To deCrypt:";
            // 
            // btnDeCrypt
            // 
            this.btnDeCrypt.Location = new System.Drawing.Point(39, 183);
            this.btnDeCrypt.Name = "btnDeCrypt";
            this.btnDeCrypt.Size = new System.Drawing.Size(75, 23);
            this.btnDeCrypt.TabIndex = 10;
            this.btnDeCrypt.Text = "Decrypt";
            this.btnDeCrypt.UseVisualStyleBackColor = true;
            this.btnDeCrypt.Click += new System.EventHandler(this.btnDeCrypt_Click);
            // 
            // btnLoadSystemInMaintenance
            // 
            this.btnLoadSystemInMaintenance.Location = new System.Drawing.Point(12, 267);
            this.btnLoadSystemInMaintenance.Name = "btnLoadSystemInMaintenance";
            this.btnLoadSystemInMaintenance.Size = new System.Drawing.Size(260, 23);
            this.btnLoadSystemInMaintenance.TabIndex = 11;
            this.btnLoadSystemInMaintenance.Text = "Load System In Maintenance";
            this.btnLoadSystemInMaintenance.UseVisualStyleBackColor = true;
            this.btnLoadSystemInMaintenance.Click += new System.EventHandler(this.btnLoadSystemInMaintenance_Click);
            // 
            // frmEncryptor
            // 
            this.AcceptButton = this.btnLoadSystemInMaintenance;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(284, 311);
            this.Controls.Add(this.btnLoadSystemInMaintenance);
            this.Controls.Add(this.btnDeCrypt);
            this.Controls.Add(this.lblDecryptResult);
            this.Controls.Add(this.lbltextToDecrypt);
            this.Controls.Add(this.txtDecryptResult);
            this.Controls.Add(this.textToDecrypt);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnDoIt);
            this.Controls.Add(this.txtEncryptedResult);
            this.Controls.Add(this.txtTextToEncrypt);
            this.Controls.Add(this.lblEncryptedResult);
            this.Controls.Add(this.lblTextToEncrypt);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmEncryptor";
            this.Text = "EncryptorForm";
            this.Load += new System.EventHandler(this.frmEncryptor_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmEncryptor_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTextToEncrypt;
        private System.Windows.Forms.Label lblEncryptedResult;
        private System.Windows.Forms.TextBox txtTextToEncrypt;
        private System.Windows.Forms.TextBox txtEncryptedResult;
        private System.Windows.Forms.Button btnDoIt;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox txtDecryptResult;
        private System.Windows.Forms.TextBox textToDecrypt;
        private System.Windows.Forms.Label lblDecryptResult;
        private System.Windows.Forms.Label lbltextToDecrypt;
        private System.Windows.Forms.Button btnDeCrypt;
        private System.Windows.Forms.Button btnLoadSystemInMaintenance;
    }
}